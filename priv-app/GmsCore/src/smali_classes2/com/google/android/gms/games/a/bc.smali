.class final Lcom/google/android/gms/games/a/bc;
.super Lcom/google/android/gms/games/a/bb;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/a/ax;


# static fields
.field static final a:[Ljava/lang/String;

.field static final b:Ljava/util/List;

.field private static final c:Ljava/util/concurrent/locks/ReentrantLock;


# instance fields
.field private final d:Lcom/google/android/gms/games/h/a/ck;

.field private e:J

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 75
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/bc;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 79
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "match_sync_token"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/gms/games/a/bc;->a:[Ljava/lang/String;

    .line 89
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "real_time"

    aput-object v1, v0, v2

    const-string v1, "turn_based"

    aput-object v1, v0, v3

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/a/bc;->b:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;)V
    .locals 2

    .prologue
    .line 136
    const-string v0, "MultiplayerAgent"

    sget-object v1, Lcom/google/android/gms/games/a/bc;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    .line 96
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/games/a/bc;->e:J

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/a/bc;->f:Z

    .line 137
    new-instance v0, Lcom/google/android/gms/games/h/a/ck;

    invoke-direct {v0, p2}, Lcom/google/android/gms/games/h/a/ck;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bc;->d:Lcom/google/android/gms/games/h/a/ck;

    .line 138
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 884
    invoke-static {p1, p2}, Lcom/google/android/gms/games/provider/ae;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 885
    invoke-static {p0, v0}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;Landroid/net/Uri;)J

    move-result-wide v0

    .line 886
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/games/a/au;II)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/provider/ae;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    .line 193
    new-instance v1, Lcom/google/android/gms/common/e/b;

    invoke-direct {v1, v0}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 194
    const-string v0, "metadata_version"

    const-string v2, "0"

    const-string v3, ">=?"

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 196
    const-string v0, "external_game_id"

    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 211
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown sort order "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 203
    :pswitch_0
    const-string v0, "last_modified_timestamp DESC"

    .line 215
    :goto_0
    new-instance v2, Lcom/google/android/gms/games/a/o;

    invoke-direct {v2, p0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    iput-object v1, v2, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    iput-object v0, v2, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    iput p2, v2, Lcom/google/android/gms/games/a/o;->d:I

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0

    .line 207
    :pswitch_1
    const-string v0, "inviter_in_circles DESC, CASE WHEN inviter_in_circles=0 THEN most_recent_invitation ELSE NULL END DESC, CASE WHEN inviter_in_circles=0 THEN external_inviter_id ELSE NULL END,last_modified_timestamp DESC"

    goto :goto_0

    .line 201
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/provider/ae;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 236
    new-instance v1, Lcom/google/android/gms/games/a/o;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    const-string v1, "last_modified_timestamp DESC"

    iput-object v1, v0, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/games/h/a/bi;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 528
    invoke-virtual {p0}, Lcom/google/android/gms/games/h/a/bi;->getRoom()Lcom/google/android/gms/games/h/a/ej;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 529
    invoke-virtual {p0}, Lcom/google/android/gms/games/h/a/bi;->getRoom()Lcom/google/android/gms/games/h/a/ej;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ej;->e()Ljava/lang/String;

    move-result-object v0

    .line 535
    :goto_0
    return-object v0

    .line 530
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/h/a/bi;->getTurnBasedMatch()Lcom/google/android/gms/games/h/a/fl;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 531
    invoke-virtual {p0}, Lcom/google/android/gms/games/h/a/bi;->getTurnBasedMatch()Lcom/google/android/gms/games/h/a/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/fl;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 534
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unexpected entity: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->b(Ljava/lang/Object;)V

    .line 535
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 6

    .prologue
    .line 250
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 251
    new-instance v2, Lcom/google/android/gms/games/multiplayer/a;

    new-instance v0, Lcom/google/android/gms/games/a/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/google/android/gms/games/provider/ae;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/multiplayer/a;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 256
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/gms/games/multiplayer/a;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    .line 257
    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->g()Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v4

    .line 258
    if-eqz v4, :cond_0

    .line 259
    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gms/games/provider/ae;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 267
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "inviter_in_circles"

    invoke-interface {v4}, Lcom/google/android/gms/games/Player;->k()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v4

    invoke-virtual {v0, v4}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 273
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/games/multiplayer/a;->w_()V

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/android/gms/games/multiplayer/a;->w_()V

    .line 277
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 278
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "MultiplayerAgent"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 280
    :cond_2
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/a/bd;Ljava/util/HashMap;Z)V
    .locals 22

    .prologue
    .line 439
    invoke-static/range {p1 .. p2}, Lcom/google/android/gms/games/a/bc;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/util/HashSet;

    move-result-object v13

    .line 443
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 444
    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/gms/games/a/bd;->b:Ljava/lang/String;

    .line 445
    if-eqz v4, :cond_0

    .line 446
    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/games/provider/m;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v5

    .line 447
    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v6, "match_sync_token"

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 454
    :cond_0
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 455
    move-object/from16 v0, p3

    iget-object v15, v0, Lcom/google/android/gms/games/a/bd;->a:Ljava/util/ArrayList;

    .line 456
    const/4 v4, 0x0

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v16

    move v12, v4

    :goto_0
    move/from16 v0, v16

    if-ge v12, v0, :cond_f

    .line 457
    invoke-virtual {v15, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/gms/games/h/a/bi;

    .line 458
    const/4 v4, 0x0

    .line 459
    invoke-virtual {v7}, Lcom/google/android/gms/games/h/a/bi;->getRoom()Lcom/google/android/gms/games/h/a/ej;

    move-result-object v5

    if-eqz v5, :cond_6

    .line 460
    invoke-virtual {v7}, Lcom/google/android/gms/games/h/a/bi;->getRoom()Lcom/google/android/gms/games/h/a/ej;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/games/h/a/ej;->e()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v4}, Lcom/google/android/gms/games/a/bc;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I

    move-result v17

    invoke-virtual {v5}, Lcom/google/android/gms/games/h/a/ej;->f()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/4 v8, 0x4

    if-ne v6, v8, :cond_2

    const-string v5, "MultiplayerAgent"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "Received tombstone for "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Lcom/google/android/gms/games/provider/ae;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Lcom/google/android/gms/games/a/be;

    const/4 v5, -0x1

    const/4 v6, 0x1

    move/from16 v0, v17

    invoke-direct {v4, v0, v5, v6}, Lcom/google/android/gms/games/a/be;-><init>(IIZ)V

    :goto_1
    move-object v5, v4

    .line 468
    :goto_2
    if-eqz v5, :cond_1

    iget v4, v5, Lcom/google/android/gms/games/a/be;->a:I

    iget v6, v5, Lcom/google/android/gms/games/a/be;->b:I

    if-eq v4, v6, :cond_e

    const/4 v4, 0x1

    :goto_3
    if-eqz v4, :cond_1

    .line 469
    invoke-static {v7}, Lcom/google/android/gms/games/a/bc;->a(Lcom/google/android/gms/games/h/a/bi;)Ljava/lang/String;

    move-result-object v4

    .line 470
    invoke-virtual {v14, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 456
    :cond_1
    add-int/lit8 v4, v12, 0x1

    move v12, v4

    goto :goto_0

    .line 460
    :cond_2
    invoke-virtual {v5}, Lcom/google/android/gms/games/h/a/ej;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    if-nez v4, :cond_4

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v6}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/16 v18, -0x1

    cmp-long v8, v8, v18

    if-nez v8, :cond_3

    const-string v4, "MultiplayerAgent"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "No game found matching external game ID "

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    goto :goto_1

    :cond_3
    move-object/from16 v0, p4

    invoke-virtual {v0, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    invoke-virtual {v5}, Lcom/google/android/gms/games/h/a/ej;->f()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-nez v5, :cond_5

    const/4 v5, 0x1

    :goto_4
    invoke-static {v5}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v10

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    invoke-static/range {v5 .. v11}, Lcom/google/android/gms/games/a/bf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/bi;JLcom/google/android/gms/common/util/p;Ljava/util/ArrayList;)I

    new-instance v4, Lcom/google/android/gms/games/a/be;

    const/4 v5, 0x1

    const/4 v6, 0x1

    move/from16 v0, v17

    invoke-direct {v4, v0, v5, v6}, Lcom/google/android/gms/games/a/be;-><init>(IIZ)V

    goto :goto_1

    :cond_5
    const/4 v5, 0x0

    goto :goto_4

    .line 461
    :cond_6
    invoke-virtual {v7}, Lcom/google/android/gms/games/h/a/bi;->getTurnBasedMatch()Lcom/google/android/gms/games/h/a/fl;

    move-result-object v5

    if-eqz v5, :cond_d

    .line 462
    invoke-virtual {v7}, Lcom/google/android/gms/games/h/a/bi;->getTurnBasedMatch()Lcom/google/android/gms/games/h/a/fl;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gms/games/h/a/fl;->d()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v5}, Lcom/google/android/gms/games/a/bc;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I

    move-result v6

    const/4 v4, -0x1

    const/4 v8, -0x1

    if-ne v6, v8, :cond_7

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Lcom/google/android/gms/games/provider/ak;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const-string v8, "version"

    const/4 v9, -0x1

    move-object/from16 v0, p1

    invoke-static {v0, v4, v8, v9}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)I

    move-result v4

    :cond_7
    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    move-result v18

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gms/games/h/a/fl;->f()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v8, 0x5

    if-ne v4, v8, :cond_9

    move-object/from16 v0, p2

    invoke-static {v0, v5, v11}, Lcom/google/android/gms/games/a/bf;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/ArrayList;)V

    const/4 v4, -0x1

    if-eq v6, v4, :cond_8

    const/4 v4, 0x1

    :goto_5
    new-instance v5, Lcom/google/android/gms/games/a/be;

    const/4 v6, -0x1

    move/from16 v0, v18

    invoke-direct {v5, v0, v6, v4}, Lcom/google/android/gms/games/a/be;-><init>(IIZ)V

    move-object v4, v5

    :goto_6
    move-object v5, v4

    goto/16 :goto_2

    :cond_8
    const/4 v4, 0x0

    goto :goto_5

    :cond_9
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gms/games/h/a/fl;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    if-nez v4, :cond_b

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v6}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/16 v20, -0x1

    cmp-long v8, v8, v20

    if-nez v8, :cond_a

    const-string v4, "MultiplayerAgent"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "No game found matching external game ID "

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    goto :goto_6

    :cond_a
    move-object/from16 v0, p4

    invoke-virtual {v0, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_b
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gms/games/h/a/fl;->g()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-nez v6, :cond_c

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v10

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    invoke-static/range {v5 .. v11}, Lcom/google/android/gms/games/a/bf;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/bi;JLcom/google/android/gms/common/util/p;Ljava/util/ArrayList;)I

    new-instance v4, Lcom/google/android/gms/games/a/be;

    const/4 v5, 0x1

    const/4 v6, 0x1

    move/from16 v0, v18

    invoke-direct {v4, v0, v5, v6}, Lcom/google/android/gms/games/a/be;-><init>(IIZ)V

    goto :goto_6

    :cond_c
    move-object/from16 v0, p2

    invoke-static {v0, v5}, Lcom/google/android/gms/games/provider/ae;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v10

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    invoke-static/range {v5 .. v11}, Lcom/google/android/gms/games/a/bf;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/bi;JLcom/google/android/gms/common/util/p;Ljava/util/ArrayList;)I

    new-instance v4, Lcom/google/android/gms/games/a/be;

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gms/games/h/a/fl;->e()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v6, 0x0

    move/from16 v0, v18

    invoke-direct {v4, v0, v5, v6}, Lcom/google/android/gms/games/a/be;-><init>(IIZ)V

    goto/16 :goto_6

    .line 465
    :cond_d
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unexpected entity: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/common/internal/e;->b(Ljava/lang/Object;)V

    move-object v5, v4

    goto/16 :goto_2

    .line 468
    :cond_e
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 475
    :cond_f
    const/4 v4, 0x1

    .line 476
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_10

    .line 477
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "MultiplayerAgent"

    invoke-static {v4, v11, v5}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v4

    .line 479
    :cond_10
    if-nez v4, :cond_11

    .line 480
    const-string v4, "MultiplayerAgent"

    const-string v5, "Failed to store matches"

    invoke-static {v4, v5}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    :goto_7
    return-void

    .line 485
    :cond_11
    invoke-virtual {v14}, Ljava/util/HashMap;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_12

    .line 486
    sget-object v4, Lcom/google/android/gms/games/internal/c/a;->a:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/google/android/gms/games/a/l;->c(Landroid/content/Context;Landroid/net/Uri;)V

    .line 490
    :cond_12
    invoke-static/range {p1 .. p2}, Lcom/google/android/gms/games/a/bc;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/util/HashSet;

    move-result-object v4

    .line 491
    invoke-virtual {v4, v13}, Ljava/util/HashSet;->removeAll(Ljava/util/Collection;)Z

    .line 492
    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v4

    if-lez v4, :cond_13

    .line 493
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/gms/games/a/bc;->f:Z

    .line 497
    :cond_13
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move/from16 v3, p5

    invoke-static {v0, v1, v2, v3, v14}, Lcom/google/android/gms/games/a/bc;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/a/bd;ZLjava/util/HashMap;)V

    .line 499
    invoke-static/range {p1 .. p2}, Lcom/google/android/gms/games/a/bf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    .line 500
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/google/android/gms/games/a/bc;->e:J

    goto :goto_7
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/a/bd;ZLjava/util/HashMap;)V
    .locals 15

    .prologue
    .line 567
    invoke-static/range {p0 .. p1}, Lcom/google/android/gms/games/a/bc;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/util/HashMap;

    move-result-object v9

    .line 568
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 569
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/games/provider/an;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v11

    .line 570
    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/google/android/gms/games/a/bd;->a:Ljava/util/ArrayList;

    .line 571
    const/4 v2, 0x0

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v13

    move v8, v2

    :goto_0
    if-ge v8, v13, :cond_a

    .line 572
    invoke-virtual {v12, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/bi;

    .line 573
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bi;->getRoom()Lcom/google/android/gms/games/h/a/ej;

    move-result-object v3

    .line 574
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bi;->getTurnBasedMatch()Lcom/google/android/gms/games/h/a/fl;

    move-result-object v6

    .line 575
    if-nez v3, :cond_1

    if-nez v6, :cond_1

    .line 576
    const-string v3, "MultiplayerAgent"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown type of entity. Ignoring "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    :cond_0
    :goto_1
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_0

    .line 580
    :cond_1
    invoke-static {v2}, Lcom/google/android/gms/games/a/bc;->a(Lcom/google/android/gms/games/h/a/bi;)Ljava/lang/String;

    move-result-object v7

    .line 581
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bi;->getRoom()Lcom/google/android/gms/games/h/a/ej;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bi;->getRoom()Lcom/google/android/gms/games/h/a/ej;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/ej;->b()Ljava/lang/String;

    move-result-object v3

    .line 582
    :goto_2
    move-object/from16 v0, p4

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/games/a/be;

    .line 583
    if-nez v4, :cond_4

    .line 585
    const-string v2, "MultiplayerAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Already processed entity. Ignoring "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 581
    :cond_2
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bi;->getTurnBasedMatch()Lcom/google/android/gms/games/h/a/fl;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bi;->getTurnBasedMatch()Lcom/google/android/gms/games/h/a/fl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/fl;->b()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected entity: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/e;->b(Ljava/lang/Object;)V

    const/4 v3, 0x0

    goto :goto_2

    .line 591
    :cond_4
    if-eqz p3, :cond_8

    .line 592
    const/4 v5, 0x0

    .line 593
    iget-boolean v14, v4, Lcom/google/android/gms/games/a/be;->c:Z

    if-eqz v14, :cond_6

    .line 594
    invoke-virtual {v4}, Lcom/google/android/gms/games/a/be;->a()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-static {p0, v0, v3, v7, v5}, Lcom/google/android/gms/games/a/bc;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v5

    .line 601
    :cond_5
    :goto_3
    if-eqz v5, :cond_8

    .line 603
    const-string v5, "MultiplayerAgent"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v14, "Notification "

    invoke-direct {v6, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v14, " consumed by listener for game "

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v14, ". Deleting."

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    new-instance v5, Lcom/google/android/gms/common/e/b;

    invoke-direct {v5, v11}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 606
    const-string v6, "external_sub_id"

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    invoke-static {v11}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v5}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v7

    iget-object v5, v5, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v6}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v5

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 613
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bi;->getNotification()Lcom/google/android/gms/games/h/a/bj;

    move-result-object v2

    .line 614
    if-eqz v2, :cond_0

    .line 615
    iget-boolean v4, v4, Lcom/google/android/gms/games/a/be;->c:Z

    if-eqz v4, :cond_7

    const/4 v5, 0x1

    .line 617
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x6

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bj;->b()Ljava/lang/String;

    move-result-object v7

    move-object v2, p0

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    goto/16 :goto_1

    .line 596
    :cond_6
    if-eqz v6, :cond_5

    .line 597
    invoke-virtual {v4}, Lcom/google/android/gms/games/a/be;->a()Z

    move-result v5

    move-object/from16 v0, p1

    invoke-static {p0, v0, v3, v6, v5}, Lcom/google/android/gms/games/a/bc;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/games/h/a/fl;Z)Z

    move-result v5

    goto :goto_3

    .line 615
    :cond_7
    const/4 v5, 0x2

    goto :goto_4

    .line 627
    :cond_8
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bi;->getNotification()Lcom/google/android/gms/games/h/a/bj;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 628
    new-instance v3, Lcom/google/android/gms/common/e/b;

    invoke-direct {v3, v11}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 629
    const-string v5, "external_sub_id"

    invoke-virtual {v3, v5, v7}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    const-string v5, "notification_id"

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bi;->getNotification()Lcom/google/android/gms/games/h/a/bj;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v5, v2}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    invoke-static {v11}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v3, v3, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    invoke-virtual {v2, v5, v3}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    .line 635
    iget-boolean v2, v4, Lcom/google/android/gms/games/a/be;->c:Z

    if-eqz v2, :cond_0

    .line 637
    invoke-virtual {v9, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 638
    if-nez v2, :cond_9

    const/4 v2, 0x0

    .line 639
    :goto_5
    const-string v4, "image_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 640
    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 638
    :cond_9
    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_5

    .line 646
    :cond_a
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_b

    .line 647
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "MultiplayerAgent"

    invoke-static {v2, v10, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 649
    :cond_b
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/games/h/a/fl;Z)Z
    .locals 6

    .prologue
    .line 720
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v0

    .line 721
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    .line 722
    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/games/a/am;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 723
    const/4 v0, 0x0

    .line 739
    :goto_0
    return v0

    .line 727
    :cond_0
    invoke-virtual {p3}, Lcom/google/android/gms/games/h/a/fl;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/google/android/gms/games/provider/aj;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 729
    new-instance v3, Lcom/google/android/gms/games/a/o;

    invoke-direct {v3, p0}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v4

    .line 733
    :try_start_0
    invoke-virtual {p3}, Lcom/google/android/gms/games/h/a/fl;->d()Ljava/lang/String;

    move-result-object v3

    move-object v2, p2

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/am;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/data/DataHolder;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 736
    invoke-virtual {v4}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 6

    .prologue
    .line 683
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v0

    .line 684
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    .line 685
    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 686
    const/4 v0, 0x0

    .line 702
    :goto_0
    return v0

    .line 690
    :cond_0
    invoke-static {p1, p3}, Lcom/google/android/gms/games/provider/ae;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 692
    new-instance v3, Lcom/google/android/gms/games/a/o;

    invoke-direct {v3, p0}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v4

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    .line 696
    :try_start_0
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/data/DataHolder;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 699
    invoke-virtual {v4}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method private b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/games/a/bd;
    .locals 13

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 350
    :goto_0
    iget-object v9, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 353
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->h:Z

    if-nez v0, :cond_0

    .line 356
    invoke-static {p1}, Lcom/google/android/gms/games/a/l;->a(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/au;->c()Lcom/google/android/gms/games/a/av;

    move-result-object v0

    iput-boolean v7, v0, Lcom/google/android/gms/games/a/av;->g:Z

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object p1

    .line 361
    :cond_0
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 364
    :try_start_0
    iget-object v4, p0, Lcom/google/android/gms/games/a/bc;->d:Lcom/google/android/gms/games/h/a/ck;

    invoke-static {v9}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    sget-object v5, Lcom/google/android/gms/games/a/bc;->b:Ljava/util/List;

    sget-object v0, Lcom/google/android/gms/games/c/a;->r:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const-string v10, "ANDROID"

    iget-object v11, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    const-string v2, "multiplayerentities/sync"

    if-eqz v3, :cond_1

    const-string v12, "language"

    invoke-static {v3}, Lcom/google/android/gms/games/h/a/ck;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v12, v3}, Lcom/google/android/gms/games/h/a/ck;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_1
    if-eqz v5, :cond_2

    const-string v3, "matchType"

    const-string v12, "&matchType="

    invoke-static {v12, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v5}, Lcom/google/android/gms/games/h/a/ck;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_2
    if-eqz v0, :cond_a

    const-string v3, "maxCompletedMatchesPerApp"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lcom/google/android/gms/games/h/a/ck;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    if-eqz p2, :cond_3

    const-string v2, "pageToken"

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/ck;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/games/h/a/ck;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_3
    const-string v2, "platformType"

    invoke-static {v10}, Lcom/google/android/gms/games/h/a/ck;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/games/h/a/ck;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v11, :cond_4

    const-string v0, "requestingApplicationId"

    invoke-static {v11}, Lcom/google/android/gms/games/h/a/ck;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/games/h/a/ck;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_4
    iget-object v0, v4, Lcom/google/android/gms/games/h/a/ck;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/cl;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/cl;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 384
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/cl;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    .line 385
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/cl;->c()Ljava/lang/String;

    move-result-object v2

    .line 386
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/cl;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 387
    invoke-static {p2, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v7

    :goto_2
    const-string v3, "Server claims to have more data, yet sync tokens match!"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 389
    invoke-direct {p0, p1, v2}, Lcom/google/android/gms/games/a/bc;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/games/a/bd;

    move-result-object v0

    .line 390
    iget v8, v0, Lcom/google/android/gms/games/a/bd;->c:I

    .line 391
    if-nez v8, :cond_5

    .line 392
    iget-object v2, v0, Lcom/google/android/gms/games/a/bd;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 393
    iget-object v0, v0, Lcom/google/android/gms/games/a/bd;->b:Ljava/lang/String;

    move-object v2, v0

    .line 397
    :cond_5
    if-nez v1, :cond_9

    .line 399
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 401
    :goto_3
    new-instance v1, Lcom/google/android/gms/games/a/bd;

    invoke-direct {v1, v0, v2, v8}, Lcom/google/android/gms/games/a/bd;-><init>(Ljava/util/ArrayList;Ljava/lang/String;I)V

    move-object v0, v1

    :goto_4
    return-object v0

    .line 367
    :catch_0
    move-exception v0

    .line 368
    const/16 v2, 0x19a

    invoke-static {v0, v2}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 371
    new-instance v0, Ljava/util/ArrayList;

    const/4 v2, 0x3

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v1}, Lcom/google/android/gms/games/provider/m;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "match_sync_token"

    invoke-virtual {v2, v3, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v1}, Lcom/google/android/gms/games/provider/ae;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v1}, Lcom/google/android/gms/games/provider/ak;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "MultiplayerAgent"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 372
    const-string v0, "MultiplayerAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Token "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is invalid. Retrying with no token."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object p2, v6

    .line 373
    goto/16 :goto_0

    .line 376
    :cond_6
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 377
    const-string v1, "MultiplayerAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 379
    :cond_7
    new-instance v0, Lcom/google/android/gms/games/a/bd;

    invoke-direct {v0}, Lcom/google/android/gms/games/a/bd;-><init>()V

    goto :goto_4

    :cond_8
    move v0, v8

    .line 387
    goto/16 :goto_2

    :cond_9
    move-object v0, v1

    goto/16 :goto_3

    :cond_a
    move-object v0, v2

    goto/16 :goto_1
.end method

.method private static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/util/HashSet;
    .locals 6

    .prologue
    .line 504
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 507
    invoke-static {p1}, Lcom/google/android/gms/games/provider/ae;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    .line 508
    const-string v2, "external_invitation_id"

    invoke-static {p0, v1, v2}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v1

    .line 510
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 513
    invoke-static {p1}, Lcom/google/android/gms/games/provider/aj;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    .line 514
    new-instance v2, Lcom/google/android/gms/common/e/b;

    invoke-direct {v2, v1}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 515
    const-string v3, "metadata_version"

    const-string v4, "0"

    const-string v5, ">=?"

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    const-string v3, "user_match_status"

    const-string v4, "1"

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    const-string v2, "external_match_id"

    invoke-static {p0, v1, v2}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v1

    .line 519
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 521
    return-object v0
.end method

.method private static c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/util/HashMap;
    .locals 5

    .prologue
    .line 652
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 653
    new-instance v2, Lcom/google/android/gms/games/multiplayer/a;

    new-instance v0, Lcom/google/android/gms/games/a/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/google/android/gms/games/provider/ae;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/multiplayer/a;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 657
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/gms/games/multiplayer/a;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    .line 658
    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->f()Ljava/lang/String;

    move-result-object v4

    .line 659
    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->g()Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->h()Landroid/net/Uri;

    move-result-object v0

    .line 660
    invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 663
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/games/multiplayer/a;->w_()V

    throw v0

    :cond_0
    invoke-virtual {v2}, Lcom/google/android/gms/games/multiplayer/a;->w_()V

    .line 665
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/a/au;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 153
    invoke-static {p1, v0, v0}, Lcom/google/android/gms/games/a/bc;->a(Lcom/google/android/gms/games/a/au;II)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    .line 157
    :try_start_0
    new-instance v0, Lcom/google/android/gms/games/multiplayer/a;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/multiplayer/a;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 158
    invoke-virtual {v0}, Lcom/google/android/gms/games/multiplayer/a;->c()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 160
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 162
    return v0

    .line 160
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method public final a()Lcom/google/android/gms/games/a/bb;
    .locals 0

    .prologue
    .line 142
    return-object p0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 173
    and-int/lit8 v0, p1, 0x3

    if-eqz v0, :cond_0

    .line 174
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/a/bc;->f:Z

    .line 176
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/a/au;)I
    .locals 12

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 298
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 299
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 302
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v8

    .line 303
    iget-wide v10, p0, Lcom/google/android/gms/games/a/bc;->e:J

    sub-long/2addr v8, v10

    sget-object v0, Lcom/google/android/gms/games/c/a;->v:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v0, v8, v10

    if-gtz v0, :cond_0

    .line 304
    const-string v0, "MultiplayerAgent"

    const-string v1, "Returning cached entities"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    :goto_0
    return v6

    .line 309
    :cond_0
    sget-object v0, Lcom/google/android/gms/games/a/bc;->a:[Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 310
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/a/bc;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/games/a/bd;

    move-result-object v3

    .line 311
    const-string v0, "MultiplayerAgent"

    const-string v4, "Received %s multiplayer entities during sync"

    new-array v7, v5, [Ljava/lang/Object;

    iget-object v8, v3, Lcom/google/android/gms/games/a/bd;->a:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v6

    invoke-static {v4, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    iget v0, v3, Lcom/google/android/gms/games/a/bd;->c:I

    if-eqz v0, :cond_1

    .line 314
    iget v6, v3, Lcom/google/android/gms/games/a/bd;->c:I

    goto :goto_0

    .line 318
    :cond_1
    iget-object v7, v3, Lcom/google/android/gms/games/a/bd;->a:Ljava/util/ArrayList;

    .line 319
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 320
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9, v8}, Ljava/util/ArrayList;-><init>(I)V

    move v4, v6

    .line 321
    :goto_1
    if-ge v4, v8, :cond_5

    .line 322
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/bi;

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bi;->getRoom()Lcom/google/android/gms/games/h/a/ej;

    move-result-object v10

    if-eqz v10, :cond_3

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bi;->getRoom()Lcom/google/android/gms/games/h/a/ej;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ej;->b()Ljava/lang/String;

    move-result-object v0

    .line 323
    :goto_2
    if-eqz v0, :cond_2

    .line 324
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 321
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 322
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bi;->getTurnBasedMatch()Lcom/google/android/gms/games/h/a/fl;

    move-result-object v10

    if-eqz v10, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bi;->getTurnBasedMatch()Lcom/google/android/gms/games/h/a/fl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/fl;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_4
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Malformed entity: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 327
    :cond_5
    invoke-static {v1, v2, v9}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Collection;)Ljava/util/HashMap;

    move-result-object v4

    .line 333
    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->g()Z

    move-result v0

    if-nez v0, :cond_6

    :goto_3
    move-object v0, p0

    .line 334
    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/a/bc;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/a/bd;Ljava/util/HashMap;Z)V

    goto/16 :goto_0

    :cond_6
    move v5, v6

    .line 333
    goto :goto_3
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    const-string v0, "inbox_matches_count"

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Lcom/google/android/gms/games/a/bc;->f:Z

    return v0
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 286
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/games/a/bc;->e:J

    .line 287
    return-void
.end method
