.class final Lcom/google/android/gms/car/senderprotocol/ad;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:I

.field final b:Ljava/util/concurrent/BlockingQueue;

.field final synthetic c:Lcom/google/android/gms/car/senderprotocol/y;

.field private final d:Ljava/nio/ByteBuffer;

.field private e:[B


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/senderprotocol/y;I)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 454
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/ad;->c:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 422
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ad;->d:Ljava/nio/ByteBuffer;

    .line 423
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ad;->e:[B

    .line 455
    const/16 v0, 0x3f00

    iput v0, p0, Lcom/google/android/gms/car/senderprotocol/ad;->a:I

    .line 456
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ad;->b:Ljava/util/concurrent/BlockingQueue;

    .line 457
    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/gms/car/senderprotocol/ae;)V
    .locals 13
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 533
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 534
    invoke-static {}, Lcom/google/android/gms/car/senderprotocol/y;->d()Lcom/google/android/gms/car/ii;

    .line 539
    const/4 v1, 0x0

    .line 544
    iget-boolean v0, p1, Lcom/google/android/gms/car/senderprotocol/ae;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/gms/car/senderprotocol/ae;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    iget v2, p0, Lcom/google/android/gms/car/senderprotocol/ad;->a:I

    add-int/lit8 v2, v2, -0x4

    if-le v0, v2, :cond_1

    const/4 v0, 0x1

    .line 547
    :goto_0
    iget-object v2, p1, Lcom/google/android/gms/car/senderprotocol/ae;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v8

    move v7, v1

    .line 548
    :goto_1
    if-ge v7, v8, :cond_d

    .line 551
    if-nez v7, :cond_2

    const/4 v1, 0x1

    move v6, v1

    .line 556
    :goto_2
    if-eqz v6, :cond_3

    if-eqz v0, :cond_3

    .line 557
    const/16 v1, 0x8

    .line 563
    :goto_3
    sub-int v2, v8, v7

    iget v3, p0, Lcom/google/android/gms/car/senderprotocol/ad;->a:I

    sub-int/2addr v3, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 564
    if-eqz v0, :cond_0

    add-int v2, v7, v5

    if-lt v2, v8, :cond_4

    :cond_0
    const/4 v2, 0x1

    move v3, v2

    .line 567
    :goto_4
    if-eqz v6, :cond_5

    const/4 v2, 0x1

    :goto_5
    or-int/lit8 v2, v2, 0x0

    int-to-byte v4, v2

    .line 569
    if-eqz v3, :cond_6

    const/4 v2, 0x2

    :goto_6
    or-int/2addr v2, v4

    int-to-byte v3, v2

    .line 570
    iget-boolean v2, p1, Lcom/google/android/gms/car/senderprotocol/ae;->d:Z

    if-eqz v2, :cond_7

    const/4 v2, 0x4

    :goto_7
    or-int/2addr v2, v3

    int-to-byte v3, v2

    .line 571
    iget-boolean v2, p1, Lcom/google/android/gms/car/senderprotocol/ae;->e:Z

    if-eqz v2, :cond_8

    const/16 v2, 0x8

    :goto_8
    or-int/2addr v2, v3

    int-to-byte v9, v2

    .line 573
    iget-object v3, p1, Lcom/google/android/gms/car/senderprotocol/ae;->b:Ljava/nio/ByteBuffer;

    .line 574
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v2

    add-int/2addr v2, v7

    .line 576
    iget-boolean v4, p1, Lcom/google/android/gms/car/senderprotocol/ae;->e:Z

    if-eqz v4, :cond_e

    .line 577
    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/ad;->c:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-static {v2}, Lcom/google/android/gms/car/senderprotocol/y;->l(Lcom/google/android/gms/car/senderprotocol/y;)Lcom/google/android/gms/car/senderprotocol/SslWrapper;

    move-result-object v2

    if-nez v2, :cond_9

    .line 578
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot send an encrypted frame with no ssl context."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 544
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 551
    :cond_2
    const/4 v1, 0x0

    move v6, v1

    goto :goto_2

    .line 559
    :cond_3
    const/4 v1, 0x4

    goto :goto_3

    .line 564
    :cond_4
    const/4 v2, 0x0

    move v3, v2

    goto :goto_4

    .line 567
    :cond_5
    const/4 v2, 0x0

    goto :goto_5

    .line 569
    :cond_6
    const/4 v2, 0x0

    goto :goto_6

    .line 570
    :cond_7
    const/4 v2, 0x0

    goto :goto_7

    .line 571
    :cond_8
    const/4 v2, 0x0

    goto :goto_8

    .line 581
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/ad;->c:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-static {v2}, Lcom/google/android/gms/car/senderprotocol/y;->l(Lcom/google/android/gms/car/senderprotocol/y;)Lcom/google/android/gms/car/senderprotocol/SslWrapper;

    move-result-object v2

    invoke-virtual {v2, v3, v7, v5}, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->a(Ljava/nio/ByteBuffer;II)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 582
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    .line 583
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v2

    .line 586
    :goto_9
    iget-object v10, p0, Lcom/google/android/gms/car/senderprotocol/ad;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v10}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 587
    iget-object v10, p0, Lcom/google/android/gms/car/senderprotocol/ad;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v10, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 588
    iget-object v10, p0, Lcom/google/android/gms/car/senderprotocol/ad;->d:Ljava/nio/ByteBuffer;

    iget v11, p1, Lcom/google/android/gms/car/senderprotocol/ae;->a:I

    and-int/lit16 v11, v11, 0xff

    int-to-byte v11, v11

    invoke-virtual {v10, v11}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 589
    iget-object v10, p0, Lcom/google/android/gms/car/senderprotocol/ad;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v10, v9}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 590
    iget-object v9, p0, Lcom/google/android/gms/car/senderprotocol/ad;->d:Ljava/nio/ByteBuffer;

    int-to-short v10, v4

    invoke-virtual {v9, v10}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 593
    if-eqz v6, :cond_a

    if-eqz v0, :cond_a

    .line 594
    iget-object v6, p0, Lcom/google/android/gms/car/senderprotocol/ad;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v6, v8}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 598
    :cond_a
    invoke-static {}, Lcom/google/android/gms/car/senderprotocol/y;->d()Lcom/google/android/gms/car/ii;

    .line 600
    add-int v6, v1, v4

    .line 603
    iget-object v9, p0, Lcom/google/android/gms/car/senderprotocol/ad;->e:[B

    array-length v9, v9

    if-ge v9, v6, :cond_b

    .line 604
    new-array v9, v6, [B

    iput-object v9, p0, Lcom/google/android/gms/car/senderprotocol/ad;->e:[B

    .line 606
    :cond_b
    iget-object v9, p0, Lcom/google/android/gms/car/senderprotocol/ad;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/gms/car/senderprotocol/ad;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v10}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v10

    iget-object v11, p0, Lcom/google/android/gms/car/senderprotocol/ad;->e:[B

    const/4 v12, 0x0

    invoke-static {v9, v10, v11, v12, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 608
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/gms/car/senderprotocol/ad;->e:[B

    invoke-static {v9, v2, v10, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 610
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ad;->c:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-static {v1}, Lcom/google/android/gms/car/senderprotocol/y;->m(Lcom/google/android/gms/car/senderprotocol/y;)Ljava/io/OutputStream;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/ad;->e:[B

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v6}, Ljava/io/OutputStream;->write([BII)V

    .line 611
    iget-boolean v1, p1, Lcom/google/android/gms/car/senderprotocol/ae;->e:Z

    if-eqz v1, :cond_c

    .line 612
    invoke-static {v3}, Lcom/google/android/c/a/a;->a(Ljava/nio/ByteBuffer;)V

    .line 614
    :cond_c
    invoke-static {}, Lcom/google/android/gms/car/senderprotocol/y;->d()Lcom/google/android/gms/car/ii;

    .line 615
    add-int v1, v7, v5

    move v7, v1

    .line 620
    goto/16 :goto_1

    .line 621
    :cond_d
    iget-object v0, p1, Lcom/google/android/gms/car/senderprotocol/ae;->b:Ljava/nio/ByteBuffer;

    invoke-static {v0}, Lcom/google/android/c/a/a;->a(Ljava/nio/ByteBuffer;)V

    .line 622
    invoke-static {}, Lcom/google/android/gms/car/senderprotocol/y;->d()Lcom/google/android/gms/car/ii;

    .line 623
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 631
    return-void

    :cond_e
    move v4, v5

    goto :goto_9
.end method
