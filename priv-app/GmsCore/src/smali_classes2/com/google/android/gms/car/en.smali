.class public final Lcom/google/android/gms/car/en;
.super Landroid/app/Fragment;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/car/eq;

.field private final b:Landroid/os/Handler;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 34
    new-instance v0, Lcom/google/android/gms/car/eq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/car/eq;-><init>(Lcom/google/android/gms/car/en;B)V

    iput-object v0, p0, Lcom/google/android/gms/car/en;->a:Lcom/google/android/gms/car/eq;

    .line 35
    new-instance v0, Lcom/google/android/gms/car/ep;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ep;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/car/en;->b:Landroid/os/Handler;

    .line 105
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/gms/car/en;->d:Landroid/widget/ImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 96
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/en;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/gms/car/en;->a()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/car/en;)Lcom/google/android/gms/car/eq;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/gms/car/en;->a:Lcom/google/android/gms/car/eq;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/car/en;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/gms/car/en;->b:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/car/en;)V
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/gms/car/en;->d:Landroid/widget/ImageView;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    return-void
.end method


# virtual methods
.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 43
    sget v0, Lcom/google/android/gms/l;->M:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 44
    sget v0, Lcom/google/android/gms/j;->kW:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/en;->c:Landroid/view/View;

    .line 45
    sget v0, Lcom/google/android/gms/j;->aB:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/car/en;->d:Landroid/widget/ImageView;

    .line 46
    invoke-virtual {p0}, Lcom/google/android/gms/car/en;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v2, "android_auto_logo_large"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/jp;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 48
    iget-object v2, p0, Lcom/google/android/gms/car/en;->d:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/car/en;->c:Landroid/view/View;

    new-instance v2, Lcom/google/android/gms/car/eo;

    invoke-direct {v2, p0}, Lcom/google/android/gms/car/eo;-><init>(Lcom/google/android/gms/car/en;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 60
    return-object v1
.end method

.method public final onDestroyView()V
    .locals 0

    .prologue
    .line 85
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 87
    invoke-direct {p0}, Lcom/google/android/gms/car/en;->a()V

    .line 88
    return-void
.end method

.method public final onPause()V
    .locals 2

    .prologue
    .line 76
    const-string v0, "CAR.HOME"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    const-string v0, "CAR.HOME"

    const-string v1, "CarHomeFragment::onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/en;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/en;->a:Lcom/google/android/gms/car/eq;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 80
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 81
    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    .line 65
    const-string v0, "CAR.HOME"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    const-string v0, "CAR.HOME"

    const-string v1, "CarHomeFragment::onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/car/en;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/en;->a:Lcom/google/android/gms/car/eq;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/car/en;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/en;->a:Lcom/google/android/gms/car/eq;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 72
    return-void
.end method
