.class public Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/gw;
.implements Lcom/google/android/gms/car/senderprotocol/b;


# instance fields
.field private final a:Lcom/google/android/gms/car/aw;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private g:I

.field private h:Lcom/google/android/gms/car/senderprotocol/a;

.field private final i:Landroid/os/HandlerThread;

.field private j:Lcom/google/android/gms/car/j;

.field private k:Lcom/google/android/gms/car/nz;

.field private l:Lcom/google/android/gms/car/nz;

.field private final m:Ljava/lang/Object;

.field private volatile n:Z

.field private o:Z

.field private p:Lcom/google/android/gms/car/g;

.field private q:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private volatile r:Z

.field private s:Lcom/google/android/gms/car/e;

.field private t:Z

.field private final u:Lcom/google/android/gms/car/gx;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-string v0, "sslwrapper_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/car/aw;Lcom/google/android/gms/car/gx;III)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->m:Ljava/lang/Object;

    .line 67
    iput-boolean v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->n:Z

    .line 69
    iput-boolean v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->o:Z

    .line 74
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 77
    iput-boolean v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->r:Z

    .line 84
    iput-boolean v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->t:Z

    .line 90
    iput-object p1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a:Lcom/google/android/gms/car/aw;

    .line 91
    iput-object p2, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->u:Lcom/google/android/gms/car/gx;

    .line 92
    iput p3, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->b:I

    .line 93
    iput p4, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->c:I

    .line 95
    iget v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->b:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 96
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->d:I

    .line 100
    :goto_0
    iput p5, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->f:I

    .line 101
    iget v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->d:I

    invoke-static {v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->b(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->e:I

    .line 102
    new-instance v0, Landroid/os/HandlerThread;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AUDIO_BH-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, -0x13

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->i:Landroid/os/HandlerThread;

    .line 104
    return-void

    .line 98
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->d:I

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;Lcom/google/android/gms/car/nz;)Lcom/google/android/gms/car/nz;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->k:Lcom/google/android/gms/car/nz;

    return-object p1
.end method

.method private a(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 235
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 264
    :cond_0
    :goto_0
    return-void

    .line 238
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->j:Lcom/google/android/gms/car/j;

    invoke-static {v0}, Lcom/google/android/gms/car/j;->c(Lcom/google/android/gms/car/j;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 239
    iget-boolean v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->r:Z

    if-eqz v0, :cond_2

    .line 240
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->l:Lcom/google/android/gms/car/nz;

    invoke-virtual {v0}, Lcom/google/android/gms/car/nz;->a()Lcom/google/android/gms/car/e;

    move-result-object v0

    .line 241
    iget-object v1, v0, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v1

    .line 242
    iget-object v2, v0, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    add-int/2addr v2, v1

    .line 243
    iget v3, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->g:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_5

    iget-boolean v3, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->t:Z

    if-eqz v3, :cond_5

    .line 244
    iget-object v3, v0, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-static {v3, v1, v2, v5}, Ljava/util/Arrays;->fill([BIIB)V

    .line 245
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->c(Lcom/google/android/gms/car/e;)Z

    .line 246
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->j:Lcom/google/android/gms/car/j;

    invoke-static {v0}, Lcom/google/android/gms/car/j;->d(Lcom/google/android/gms/car/j;)V

    .line 253
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->j:Lcom/google/android/gms/car/j;

    invoke-static {v0}, Lcom/google/android/gms/car/j;->e(Lcom/google/android/gms/car/j;)V

    .line 255
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->m:Ljava/lang/Object;

    monitor-enter v1

    .line 256
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->o:Z

    .line 257
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 258
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 259
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "audio stream stopped "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    :cond_4
    if-eqz p1, :cond_0

    .line 262
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a:Lcom/google/android/gms/car/aw;

    iget v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->b:I

    iget-object v0, v0, Lcom/google/android/gms/car/aw;->e:Lcom/google/android/gms/car/bb;

    invoke-static {v0}, Lcom/google/android/gms/car/bb;->c(Lcom/google/android/gms/car/bb;)V

    goto :goto_0

    .line 247
    :cond_5
    iget-object v3, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->s:Lcom/google/android/gms/car/e;

    if-eqz v3, :cond_2

    .line 248
    iget-object v3, v0, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-static {v3, v1, v2, v5}, Ljava/util/Arrays;->fill([BIIB)V

    .line 249
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->c(Lcom/google/android/gms/car/e;)Z

    .line 250
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->j:Lcom/google/android/gms/car/j;

    invoke-static {v0}, Lcom/google/android/gms/car/j;->d(Lcom/google/android/gms/car/j;)V

    goto :goto_1

    .line 257
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->n:Z

    return v0
.end method

.method private static b(I)I
    .locals 2

    .prologue
    .line 443
    const/16 v0, 0x800

    .line 444
    const/4 v1, 0x1

    if-ne p0, v1, :cond_0

    .line 445
    const/16 v0, 0x2000

    .line 447
    :cond_0
    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->c:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)Lcom/google/android/gms/car/senderprotocol/a;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->h:Lcom/google/android/gms/car/senderprotocol/a;

    return-object v0
.end method

.method private c(Lcom/google/android/gms/car/e;)Z
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 324
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 325
    iget v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->g:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 328
    iget-boolean v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->t:Z

    if-nez v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->k:Lcom/google/android/gms/car/nz;

    invoke-virtual {v0}, Lcom/google/android/gms/car/nz;->a()Lcom/google/android/gms/car/e;

    move-result-object v9

    .line 330
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->k:Lcom/google/android/gms/car/nz;

    invoke-virtual {v0}, Lcom/google/android/gms/car/nz;->a()Lcom/google/android/gms/car/e;

    move-result-object v10

    .line 331
    iget-object v0, p1, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    .line 332
    iget-object v0, p1, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v2

    .line 333
    iget-object v0, v9, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    .line 334
    iget-object v0, v9, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v4

    .line 335
    iget-object v0, v10, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    .line 336
    iget-object v0, v10, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v6

    move-object v0, p0

    .line 337
    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->native16000MonoTo48000StereoFirst([BI[BI[BI)V

    .line 339
    iget-object v0, v9, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 340
    iget-object v0, v9, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->e:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 341
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->k:Lcom/google/android/gms/car/nz;

    invoke-virtual {v0, v9}, Lcom/google/android/gms/car/nz;->a(Lcom/google/android/gms/car/e;)V

    .line 342
    iput-object v10, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->s:Lcom/google/android/gms/car/e;

    .line 343
    iput-boolean v7, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->t:Z

    .line 364
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->l:Lcom/google/android/gms/car/nz;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/nz;->b(Lcom/google/android/gms/car/e;)V

    .line 365
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    move v0, v7

    .line 402
    :goto_1
    return v0

    .line 345
    :cond_0
    iget-object v9, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->s:Lcom/google/android/gms/car/e;

    .line 346
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->k:Lcom/google/android/gms/car/nz;

    invoke-virtual {v0}, Lcom/google/android/gms/car/nz;->a()Lcom/google/android/gms/car/e;

    move-result-object v10

    .line 347
    iget-object v0, p1, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    .line 348
    iget-object v0, p1, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v2

    .line 349
    iget-object v0, v9, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    .line 350
    iget-object v0, v9, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v4

    .line 351
    iget-object v0, v10, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    .line 352
    iget-object v0, v10, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v6

    move-object v0, p0

    .line 353
    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->native16000MonoTo48000StereoSecond([BI[BI[BI)V

    .line 355
    iget-object v0, v9, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 356
    iget-object v0, v9, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->e:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 357
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->k:Lcom/google/android/gms/car/nz;

    invoke-virtual {v0, v9}, Lcom/google/android/gms/car/nz;->a(Lcom/google/android/gms/car/e;)V

    .line 358
    iget-object v0, v10, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 359
    iget-object v0, v10, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->e:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 360
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->k:Lcom/google/android/gms/car/nz;

    invoke-virtual {v0, v10}, Lcom/google/android/gms/car/nz;->a(Lcom/google/android/gms/car/e;)V

    .line 361
    iput-object v11, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->s:Lcom/google/android/gms/car/e;

    .line 362
    iput-boolean v8, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->t:Z

    goto :goto_0

    .line 370
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->s:Lcom/google/android/gms/car/e;

    if-nez v0, :cond_2

    .line 371
    iput-object p1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->s:Lcom/google/android/gms/car/e;

    .line 372
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    move v0, v8

    .line 373
    goto :goto_1

    .line 377
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->s:Lcom/google/android/gms/car/e;

    iget-object v0, v0, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    .line 378
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->s:Lcom/google/android/gms/car/e;

    iget-object v0, v0, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v2

    .line 379
    iget-object v0, p1, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    .line 380
    iget-object v0, p1, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v4

    .line 381
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->k:Lcom/google/android/gms/car/nz;

    invoke-virtual {v0}, Lcom/google/android/gms/car/nz;->a()Lcom/google/android/gms/car/e;

    move-result-object v9

    .line 382
    iget-object v0, v9, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    .line 383
    iget-object v0, v9, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v6

    .line 384
    iget-boolean v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->t:Z

    if-nez v0, :cond_3

    move-object v0, p0

    .line 385
    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->native48000StereoTo16000MonoFirst([BI[BI[BI)V

    .line 387
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->l:Lcom/google/android/gms/car/nz;

    iget-object v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->s:Lcom/google/android/gms/car/e;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/nz;->b(Lcom/google/android/gms/car/e;)V

    .line 388
    iput-object p1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->s:Lcom/google/android/gms/car/e;

    .line 389
    iput-boolean v7, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->t:Z

    .line 398
    :goto_2
    iget-object v0, v9, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 399
    iget-object v0, v9, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->e:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 400
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->k:Lcom/google/android/gms/car/nz;

    invoke-virtual {v0, v9}, Lcom/google/android/gms/car/nz;->a(Lcom/google/android/gms/car/e;)V

    .line 401
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    move v0, v7

    .line 402
    goto/16 :goto_1

    :cond_3
    move-object v0, p0

    .line 391
    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->native48000StereoTo16000MonoSecond([BI[BI[BI)V

    .line 393
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->l:Lcom/google/android/gms/car/nz;

    iget-object v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->s:Lcom/google/android/gms/car/e;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/nz;->b(Lcom/google/android/gms/car/e;)V

    .line 394
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->l:Lcom/google/android/gms/car/nz;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/nz;->b(Lcom/google/android/gms/car/e;)V

    .line 395
    iput-object v11, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->s:Lcom/google/android/gms/car/e;

    .line 396
    iput-boolean v8, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->t:Z

    goto :goto_2
.end method

.method static synthetic d(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->f:I

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->d:I

    return v0
.end method

.method static synthetic f(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->b:I

    return v0
.end method

.method static synthetic g(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->l:Lcom/google/android/gms/car/nz;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/car/nz;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->k:Lcom/google/android/gms/car/nz;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/google/android/gms/car/nz;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    :cond_1
    return-void
.end method

.method static synthetic h(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->n()V

    return-void
.end method

.method static synthetic i(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)Lcom/google/android/gms/car/nz;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->k:Lcom/google/android/gms/car/nz;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->e:I

    return v0
.end method

.method private declared-synchronized n()V
    .locals 1

    .prologue
    .line 435
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 436
    monitor-exit p0

    return-void

    .line 435
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private native native16000MonoTo48000StereoFirst([BI[BI[BI)V
.end method

.method private native native16000MonoTo48000StereoSecond([BI[BI[BI)V
.end method

.method private native native48000StereoTo16000MonoFirst([BI[BI[BI)V
.end method

.method private native native48000StereoTo16000MonoSecond([BI[BI[BI)V
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->b:I

    return v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 205
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    iput p1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->g:I

    .line 209
    iget v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->g:I

    iget v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->d:I

    if-eq v0, v1, :cond_2

    .line 211
    iput-boolean v2, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->r:Z

    .line 212
    invoke-static {p1}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->b(I)I

    move-result v0

    .line 213
    new-instance v1, Lcom/google/android/gms/car/nz;

    const/16 v2, 0x8

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/car/nz;-><init>(II)V

    iput-object v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->l:Lcom/google/android/gms/car/nz;

    .line 218
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->j:Lcom/google/android/gms/car/j;

    invoke-static {v0}, Lcom/google/android/gms/car/j;->b(Lcom/google/android/gms/car/j;)V

    .line 219
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "audio stream started "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resampling:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->r:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " client stream "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->p:Lcom/google/android/gms/car/g;

    iget v2, v2, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " client format "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 216
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->r:Z

    goto :goto_1
.end method

.method public final a(J)V
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, 0x3

    .line 409
    cmp-long v0, p1, v6

    if-lez v0, :cond_3

    .line 410
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 411
    iget-object v2, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->j:Lcom/google/android/gms/car/j;

    invoke-static {v2}, Lcom/google/android/gms/car/j;->c(Lcom/google/android/gms/car/j;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 412
    const-string v2, "CAR.AUDIO"

    invoke-static {v2, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 413
    const-string v2, "CAR.AUDIO"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "wait for stream to stop, stream type:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->b:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    :cond_0
    monitor-enter p0

    .line 417
    :try_start_0
    invoke-virtual {p0, p1, p2}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 421
    :goto_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 423
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 424
    sub-long v0, v2, v0

    sub-long v0, p1, v0

    .line 425
    cmp-long v2, v0, v6

    if-lez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->h:Lcom/google/android/gms/car/senderprotocol/a;

    invoke-virtual {v2}, Lcom/google/android/gms/car/senderprotocol/a;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 426
    const-string v2, "CAR.AUDIO"

    invoke-static {v2, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 427
    const-string v2, "CAR.AUDIO"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "will wait for full ACK "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->h:Lcom/google/android/gms/car/senderprotocol/a;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/car/senderprotocol/a;->a(J)V

    .line 432
    :cond_3
    return-void

    .line 421
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/e;)V
    .locals 1

    .prologue
    .line 275
    iget-boolean v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->r:Z

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->l:Lcom/google/android/gms/car/nz;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/nz;->b(Lcom/google/android/gms/car/e;)V

    .line 280
    :goto_0
    return-void

    .line 278
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->k:Lcom/google/android/gms/car/nz;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/nz;->b(Lcom/google/android/gms/car/e;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/senderprotocol/az;)V
    .locals 0

    .prologue
    .line 142
    check-cast p1, Lcom/google/android/gms/car/senderprotocol/a;

    iput-object p1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->h:Lcom/google/android/gms/car/senderprotocol/a;

    .line 143
    return-void
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 2

    .prologue
    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "stream type:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " protocolAudioFormat:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " codec type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " channel used:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->o:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " needs resampling:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->r:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " client audio format:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->j:Lcom/google/android/gms/car/j;

    .line 158
    if-eqz v0, :cond_0

    .line 159
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/j;->a(Ljava/io/PrintWriter;)V

    .line 161
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/g;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 185
    iget-object v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->m:Ljava/lang/Object;

    monitor-enter v1

    .line 186
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->g()Z

    move-result v2

    if-nez v2, :cond_0

    .line 187
    const/4 v0, 0x0

    monitor-exit v1

    .line 191
    :goto_0
    return v0

    .line 189
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->o:Z

    .line 190
    iput-object p1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->p:Lcom/google/android/gms/car/g;

    .line 191
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 192
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()V
    .locals 7

    .prologue
    .line 112
    iget-object v6, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->m:Ljava/lang/Object;

    monitor-enter v6

    .line 113
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->n:Z

    if-eqz v0, :cond_0

    .line 114
    monitor-exit v6

    .line 123
    :goto_0
    return-void

    .line 116
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->n:Z

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->i:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->u:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->J()Lcom/google/android/gms/car/id;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/id;->c()Z

    move-result v4

    .line 119
    new-instance v0, Lcom/google/android/gms/car/j;

    iget-object v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->i:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->u:Lcom/google/android/gms/car/gx;

    invoke-virtual {v1}, Lcom/google/android/gms/car/gx;->G()Landroid/content/Context;

    move-result-object v3

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/j;-><init>(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;Landroid/os/Looper;Landroid/content/Context;ZB)V

    iput-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->j:Lcom/google/android/gms/car/j;

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->j:Lcom/google/android/gms/car/j;

    iget v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->d:I

    iget v2, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->f:I

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/car/j;->a(Lcom/google/android/gms/car/j;II)V

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a:Lcom/google/android/gms/car/aw;

    iget v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->b:I

    invoke-static {v1}, Lcom/google/android/gms/car/bg;->a(I)I

    move-result v1

    iget-object v2, v0, Lcom/google/android/gms/car/aw;->g:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v3, v0, Lcom/google/android/gms/car/aw;->h:[I

    aget v4, v3, v1

    or-int/lit8 v4, v4, 0x1

    aput v4, v3, v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v2, v0, Lcom/google/android/gms/car/aw;->a:[Lcom/google/android/gms/car/g;

    aget-object v1, v2, v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/car/g;->a()V

    :cond_1
    iget-object v0, v0, Lcom/google/android/gms/car/aw;->e:Lcom/google/android/gms/car/bb;

    const/4 v1, -0x1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/car/bb;->a(Lcom/google/android/gms/car/bb;IZ)V

    .line 123
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    .line 122
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public final b(Lcom/google/android/gms/car/e;)V
    .locals 2

    .prologue
    .line 283
    const/4 v0, 0x1

    .line 284
    iget-boolean v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->r:Z

    if-eqz v1, :cond_1

    .line 285
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->c(Lcom/google/android/gms/car/e;)Z

    move-result v0

    .line 289
    :goto_0
    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->j:Lcom/google/android/gms/car/j;

    invoke-static {v0}, Lcom/google/android/gms/car/j;->d(Lcom/google/android/gms/car/j;)V

    .line 292
    :cond_0
    return-void

    .line 287
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->k:Lcom/google/android/gms/car/nz;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/car/nz;->a(Lcom/google/android/gms/car/e;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 128
    iget-object v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->m:Ljava/lang/Object;

    monitor-enter v1

    .line 129
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->n:Z

    if-nez v0, :cond_0

    .line 130
    monitor-exit v1

    .line 137
    :goto_0
    return-void

    .line 132
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->n:Z

    .line 133
    invoke-virtual {p0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->e()V

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->j:Lcom/google/android/gms/car/j;

    invoke-static {v0}, Lcom/google/android/gms/car/j;->a(Lcom/google/android/gms/car/j;)V

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->i:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a:Lcom/google/android/gms/car/aw;

    iget v2, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->b:I

    invoke-static {v2}, Lcom/google/android/gms/car/bg;->a(I)I

    move-result v2

    iget-object v3, v0, Lcom/google/android/gms/car/aw;->g:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v4, v0, Lcom/google/android/gms/car/aw;->h:[I

    aget v5, v4, v2

    and-int/lit8 v5, v5, -0x2

    aput v5, v4, v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v0, v0, Lcom/google/android/gms/car/aw;->e:Lcom/google/android/gms/car/bb;

    const/4 v2, -0x1

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/car/bb;->a(Lcom/google/android/gms/car/bb;IZ)V

    .line 137
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 136
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->c()V

    .line 148
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->h()Lcom/google/android/gms/car/g;

    move-result-object v0

    .line 165
    if-eqz v0, :cond_0

    .line 166
    invoke-virtual {v0}, Lcom/google/android/gms/car/g;->b()V

    .line 167
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a(Z)V

    .line 169
    :cond_0
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->f:I

    return v0
.end method

.method public final g()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 176
    iget-object v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->m:Ljava/lang/Object;

    monitor-enter v1

    .line 177
    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->n:Z

    if-nez v2, :cond_0

    .line 178
    monitor-exit v1

    .line 180
    :goto_0
    return v0

    :cond_0
    iget-boolean v2, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->o:Z

    if-nez v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 181
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final h()Lcom/google/android/gms/car/g;
    .locals 2

    .prologue
    .line 196
    iget-object v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->m:Ljava/lang/Object;

    monitor-enter v1

    .line 197
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->n:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->o:Z

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->p:Lcom/google/android/gms/car/g;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    :goto_0
    return-object v0

    .line 200
    :cond_0
    monitor-exit v1

    .line 201
    const/4 v0, 0x0

    goto :goto_0

    .line 200
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 227
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a(Z)V

    .line 228
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a(Z)V

    .line 232
    return-void
.end method

.method public final k()Lcom/google/android/gms/car/e;
    .locals 1

    .prologue
    .line 267
    iget-boolean v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->r:Z

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->l:Lcom/google/android/gms/car/nz;

    invoke-virtual {v0}, Lcom/google/android/gms/car/nz;->a()Lcom/google/android/gms/car/e;

    move-result-object v0

    .line 270
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->k:Lcom/google/android/gms/car/nz;

    invoke-virtual {v0}, Lcom/google/android/gms/car/nz;->a()Lcom/google/android/gms/car/e;

    move-result-object v0

    goto :goto_0
.end method

.method public final l()I
    .locals 2

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->k:Lcom/google/android/gms/car/nz;

    iget-object v0, v0, Lcom/google/android/gms/car/nz;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->h:Lcom/google/android/gms/car/senderprotocol/a;

    invoke-virtual {v1}, Lcom/google/android/gms/car/senderprotocol/a;->d()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final m()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 299
    iget-object v2, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->m:Ljava/lang/Object;

    monitor-enter v2

    .line 300
    :try_start_0
    iget-boolean v3, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->n:Z

    if-nez v3, :cond_0

    .line 301
    monitor-exit v2

    .line 305
    :goto_0
    return v0

    .line 302
    :cond_0
    iget-boolean v3, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->o:Z

    if-eqz v3, :cond_1

    .line 303
    monitor-exit v2

    move v0, v1

    goto :goto_0

    .line 305
    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->h:Lcom/google/android/gms/car/senderprotocol/a;

    invoke-virtual {v3}, Lcom/google/android/gms/car/senderprotocol/a;->b()Z

    move-result v3

    if-nez v3, :cond_2

    :goto_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 307
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_2
    move v0, v1

    .line 305
    goto :goto_1
.end method
