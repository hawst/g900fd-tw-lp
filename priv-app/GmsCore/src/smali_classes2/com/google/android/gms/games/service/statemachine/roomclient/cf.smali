.class public final Lcom/google/android/gms/games/service/statemachine/roomclient/cf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/util/ArrayList;

.field private final b:Lcom/google/android/gms/games/g/ah;

.field private final c:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 324
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 314
    new-instance v0, Lcom/google/android/gms/games/g/ah;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/ah;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;->b:Lcom/google/android/gms/games/g/ah;

    .line 315
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;->a:Ljava/util/ArrayList;

    .line 317
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;->c:Ljava/util/HashMap;

    .line 325
    invoke-static {p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 326
    if-eqz v0, :cond_0

    .line 327
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;->b:Lcom/google/android/gms/games/g/ah;

    iput-object v0, v1, Lcom/google/android/gms/games/g/ah;->a:Ljava/lang/String;

    .line 329
    :cond_0
    return-void
.end method

.method public static a([B)Lcom/google/android/gms/games/g/ah;
    .locals 2

    .prologue
    .line 374
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/games/g/ah;->a([B)Lcom/google/android/gms/games/g/ah;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 377
    :goto_0
    return-object v0

    .line 376
    :catch_0
    move-exception v0

    const-string v0, "RtmpSessionLog"

    const-string v1, "Couldn\'t parse proto!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    new-instance v0, Lcom/google/android/gms/games/g/ah;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/ah;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/games/g/ah;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 353
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;->b:Lcom/google/android/gms/games/g/ah;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/gms/games/g/aj;

    iput-object v1, v0, Lcom/google/android/gms/games/g/ah;->b:[Lcom/google/android/gms/games/g/aj;

    .line 354
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v2

    :goto_0
    if-ge v4, v5, :cond_1

    .line 355
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;->b:Lcom/google/android/gms/games/g/ah;

    iget-object v6, v0, Lcom/google/android/gms/games/g/ah;->b:[Lcom/google/android/gms/games/g/aj;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ch;->a:Lcom/google/android/gms/games/g/aj;

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ch;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Lcom/google/android/gms/games/g/ak;

    iput-object v3, v1, Lcom/google/android/gms/games/g/aj;->d:[Lcom/google/android/gms/games/g/ak;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ch;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v3, v2

    :goto_1
    if-ge v3, v7, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ch;->a:Lcom/google/android/gms/games/g/aj;

    iget-object v8, v1, Lcom/google/android/gms/games/g/aj;->d:[Lcom/google/android/gms/games/g/ak;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ch;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/g/ak;

    aput-object v1, v8, v3

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ch;->a:Lcom/google/android/gms/games/g/aj;

    aput-object v0, v6, v4

    .line 354
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 357
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;->b:Lcom/google/android/gms/games/g/ah;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;->c:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/gms/games/g/ai;

    iput-object v1, v0, Lcom/google/android/gms/games/g/ah;->c:[Lcom/google/android/gms/games/g/ai;

    .line 360
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 361
    new-instance v2, Lcom/google/android/gms/games/g/ai;

    invoke-direct {v2}, Lcom/google/android/gms/games/g/ai;-><init>()V

    .line 363
    iput-object v0, v2, Lcom/google/android/gms/games/g/ai;->a:Ljava/lang/String;

    .line 364
    iget-object v4, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;->c:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cg;

    .line 365
    iget v4, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cg;->a:I

    iput v4, v2, Lcom/google/android/gms/games/g/ai;->b:I

    .line 366
    iget v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cg;->b:I

    iput v0, v2, Lcom/google/android/gms/games/g/ai;->c:I

    .line 367
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;->b:Lcom/google/android/gms/games/g/ah;

    iget-object v4, v0, Lcom/google/android/gms/games/g/ah;->c:[Lcom/google/android/gms/games/g/ai;

    add-int/lit8 v0, v1, 0x1

    aput-object v2, v4, v1

    move v1, v0

    .line 368
    goto :goto_2

    .line 369
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;->b:Lcom/google/android/gms/games/g/ah;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/games/service/statemachine/roomclient/cg;
    .locals 2

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cg;

    .line 341
    if-nez v0, :cond_0

    .line 342
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cg;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/cg;-><init>(B)V

    .line 343
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;->c:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 345
    :cond_0
    return-object v0
.end method
