.class public final Lcom/google/android/gms/car/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# static fields
.field private static final a:Landroid/util/SparseLongArray;

.field private static final b:Lcom/google/android/gms/car/a/f;

.field private static final c:Lcom/google/android/gms/car/a/f;

.field private static final d:Ljava/lang/Object;

.field private static final e:Ljava/lang/Object;


# instance fields
.field private final f:Landroid/os/Handler;

.field private final g:Lcom/google/android/gms/car/a/k;

.field private final h:Lcom/google/android/gms/car/a/b;

.field private final i:Landroid/util/SparseArray;

.field private j:I

.field private k:Lcom/google/android/gms/car/a/f;

.field private l:Lcom/google/android/gms/car/a/g;

.field private m:Z

.field private n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const-wide/32 v10, 0xea60

    const-wide/16 v8, 0xfa0

    const-wide/16 v6, -0x1

    const-wide/16 v4, 0x2710

    .line 123
    new-instance v0, Landroid/util/SparseLongArray;

    const/16 v1, 0xd

    invoke-direct {v0, v1}, Landroid/util/SparseLongArray;-><init>(I)V

    .line 125
    sput-object v0, Lcom/google/android/gms/car/a/a;->a:Landroid/util/SparseLongArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v6, v7}, Landroid/util/SparseLongArray;->put(IJ)V

    .line 126
    sget-object v0, Lcom/google/android/gms/car/a/a;->a:Landroid/util/SparseLongArray;

    const/4 v1, 0x1

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/util/SparseLongArray;->put(IJ)V

    .line 127
    sget-object v0, Lcom/google/android/gms/car/a/a;->a:Landroid/util/SparseLongArray;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v8, v9}, Landroid/util/SparseLongArray;->put(IJ)V

    .line 128
    sget-object v0, Lcom/google/android/gms/car/a/a;->a:Landroid/util/SparseLongArray;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v8, v9}, Landroid/util/SparseLongArray;->put(IJ)V

    .line 129
    sget-object v0, Lcom/google/android/gms/car/a/a;->a:Landroid/util/SparseLongArray;

    const/4 v1, 0x4

    const-wide/32 v2, 0x1d4c0

    invoke-virtual {v0, v1, v2, v3}, Landroid/util/SparseLongArray;->put(IJ)V

    .line 130
    sget-object v0, Lcom/google/android/gms/car/a/a;->a:Landroid/util/SparseLongArray;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v10, v11}, Landroid/util/SparseLongArray;->put(IJ)V

    .line 131
    sget-object v0, Lcom/google/android/gms/car/a/a;->a:Landroid/util/SparseLongArray;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v4, v5}, Landroid/util/SparseLongArray;->put(IJ)V

    .line 132
    sget-object v0, Lcom/google/android/gms/car/a/a;->a:Landroid/util/SparseLongArray;

    const/4 v1, 0x7

    invoke-virtual {v0, v1, v4, v5}, Landroid/util/SparseLongArray;->put(IJ)V

    .line 133
    sget-object v0, Lcom/google/android/gms/car/a/a;->a:Landroid/util/SparseLongArray;

    const/16 v1, 0x8

    invoke-virtual {v0, v1, v10, v11}, Landroid/util/SparseLongArray;->put(IJ)V

    .line 134
    sget-object v0, Lcom/google/android/gms/car/a/a;->a:Landroid/util/SparseLongArray;

    const/16 v1, 0x9

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/util/SparseLongArray;->put(IJ)V

    .line 135
    sget-object v0, Lcom/google/android/gms/car/a/a;->a:Landroid/util/SparseLongArray;

    const/16 v1, 0xa

    invoke-virtual {v0, v1, v4, v5}, Landroid/util/SparseLongArray;->put(IJ)V

    .line 136
    sget-object v0, Lcom/google/android/gms/car/a/a;->a:Landroid/util/SparseLongArray;

    const/16 v1, 0xb

    const-wide/16 v2, 0x1770

    invoke-virtual {v0, v1, v2, v3}, Landroid/util/SparseLongArray;->put(IJ)V

    .line 137
    sget-object v0, Lcom/google/android/gms/car/a/a;->a:Landroid/util/SparseLongArray;

    const/16 v1, 0x64

    invoke-virtual {v0, v1, v6, v7}, Landroid/util/SparseLongArray;->put(IJ)V

    .line 206
    new-instance v0, Lcom/google/android/gms/car/a/f;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    .line 212
    new-instance v0, Lcom/google/android/gms/car/a/f;

    const/4 v1, -0x2

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    .line 214
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/a/a;->d:Ljava/lang/Object;

    .line 215
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/a/a;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;Lcom/google/android/gms/car/a/k;Lcom/google/android/gms/car/a/b;)V
    .locals 12

    .prologue
    const/16 v11, 0x11

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/16 v8, 0x64

    const/4 v7, 0x2

    .line 229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 220
    new-instance v0, Landroid/util/SparseArray;

    const/16 v1, 0xd

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/a/a;->i:Landroid/util/SparseArray;

    .line 222
    iput v9, p0, Lcom/google/android/gms/car/a/a;->j:I

    .line 225
    iput-boolean v9, p0, Lcom/google/android/gms/car/a/a;->m:Z

    .line 226
    iput-boolean v9, p0, Lcom/google/android/gms/car/a/a;->n:Z

    .line 230
    new-instance v0, Lcom/google/android/gms/car/a/c;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/a/c;-><init>(Lcom/google/android/gms/car/a/a;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/car/a/a;->f:Landroid/os/Handler;

    .line 231
    iput-object p2, p0, Lcom/google/android/gms/car/a/a;->g:Lcom/google/android/gms/car/a/k;

    .line 232
    iput-object p3, p0, Lcom/google/android/gms/car/a/a;->h:Lcom/google/android/gms/car/a/b;

    .line 233
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v11}, Landroid/util/SparseArray;-><init>(I)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    invoke-direct {v1, v8}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v9, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    invoke-direct {v1, v8}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v10, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v7, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    invoke-direct {v1, v8}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v8, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x65

    new-instance v2, Lcom/google/android/gms/car/a/f;

    invoke-direct {v2, v10}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x66

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x67

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x68

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x69

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc8

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc9

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xca

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12c

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12d

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x190

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x191

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x192

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/car/a/a;->i:Landroid/util/SparseArray;

    invoke-virtual {v1, v9, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v11}, Landroid/util/SparseArray;-><init>(I)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    invoke-direct {v1, v8}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v9, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v10, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    invoke-direct {v1, v10}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v7, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v8, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x65

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x66

    new-instance v2, Lcom/google/android/gms/car/a/f;

    invoke-direct {v2, v7}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x67

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x68

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x69

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc8

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc9

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xca

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12c

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12d

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x190

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x191

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x192

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/car/a/a;->i:Landroid/util/SparseArray;

    invoke-virtual {v1, v10, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v11}, Landroid/util/SparseArray;-><init>(I)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    invoke-direct {v1, v8}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v9, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v10, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    invoke-direct {v1, v7}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v7, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v8, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x65

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x66

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x67

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/16 v3, 0x2710

    invoke-direct {v2, v7, v3}, Lcom/google/android/gms/car/a/f;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x68

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x69

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc8

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc9

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xca

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12c

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12d

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x190

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/16 v3, 0x2710

    invoke-direct {v2, v7, v3}, Lcom/google/android/gms/car/a/f;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x191

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x192

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/car/a/a;->i:Landroid/util/SparseArray;

    invoke-virtual {v1, v7, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v11}, Landroid/util/SparseArray;-><init>(I)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    invoke-direct {v1, v8}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v9, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v10, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    const/4 v2, 0x3

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3, v7, v9}, Lcom/google/android/gms/car/a/f;-><init>(IIII)V

    invoke-virtual {v0, v7, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v8, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x65

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x66

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x67

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x68

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x69

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/4 v3, 0x4

    invoke-direct {v2, v3}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc8

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc9

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xca

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12c

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12d

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x190

    new-instance v2, Lcom/google/android/gms/car/a/f;

    invoke-direct {v2, v7}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x191

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x192

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/car/a/a;->i:Landroid/util/SparseArray;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v11}, Landroid/util/SparseArray;-><init>(I)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    invoke-direct {v1, v8}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v9, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v10, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v7, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v8, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x65

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x66

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x67

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x68

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x69

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc8

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/4 v3, 0x4

    const/16 v4, 0x3e8

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/car/a/f;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc9

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/4 v3, 0x4

    const v4, 0x1d4c0

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/car/a/f;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xca

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/4 v3, 0x5

    invoke-direct {v2, v3}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12c

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12d

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x190

    new-instance v2, Lcom/google/android/gms/car/a/f;

    invoke-direct {v2, v7}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x191

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x192

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/car/a/a;->i:Landroid/util/SparseArray;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v11}, Landroid/util/SparseArray;-><init>(I)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    invoke-direct {v1, v8}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v9, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v10, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    const/4 v2, 0x5

    const/16 v3, 0xb

    invoke-direct {v1, v2, v7, v3, v9}, Lcom/google/android/gms/car/a/f;-><init>(IIII)V

    invoke-virtual {v0, v7, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v8, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x65

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x66

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x67

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x68

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x69

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc8

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc9

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xca

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12c

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12d

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/16 v3, 0x9

    invoke-direct {v2, v3}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x190

    new-instance v2, Lcom/google/android/gms/car/a/f;

    invoke-direct {v2, v7}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x191

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x192

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/4 v3, 0x6

    invoke-direct {v2, v3}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/car/a/a;->i:Landroid/util/SparseArray;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v11}, Landroid/util/SparseArray;-><init>(I)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    invoke-direct {v1, v8}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v9, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v10, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    const/16 v2, 0xb

    invoke-direct {v1, v2}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v7, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v8, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x65

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x66

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x67

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x68

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x69

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc8

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc9

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xca

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12c

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/16 v3, 0x8

    invoke-direct {v2, v3}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12d

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/16 v3, 0x9

    invoke-direct {v2, v3}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x190

    new-instance v2, Lcom/google/android/gms/car/a/f;

    invoke-direct {v2, v7}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x191

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/4 v3, 0x7

    invoke-direct {v2, v3}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x192

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/car/a/a;->i:Landroid/util/SparseArray;

    const/4 v2, 0x6

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v11}, Landroid/util/SparseArray;-><init>(I)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    invoke-direct {v1, v8}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v9, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v10, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    const/16 v2, 0xb

    invoke-direct {v1, v2}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v7, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v8, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x65

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x66

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x67

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x68

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x69

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc8

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc9

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xca

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12c

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/16 v3, 0x8

    invoke-direct {v2, v3}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12d

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/16 v3, 0x9

    invoke-direct {v2, v3}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x190

    new-instance v2, Lcom/google/android/gms/car/a/f;

    invoke-direct {v2, v7}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x191

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/16 v3, 0xb

    invoke-direct {v2, v3}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x192

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/car/a/a;->i:Landroid/util/SparseArray;

    const/4 v2, 0x7

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v11}, Landroid/util/SparseArray;-><init>(I)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    invoke-direct {v1, v8}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v9, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v10, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    const/16 v2, 0x8

    const/16 v3, 0xb

    invoke-direct {v1, v2, v7, v3, v9}, Lcom/google/android/gms/car/a/f;-><init>(IIII)V

    invoke-virtual {v0, v7, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v8, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x65

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x66

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x67

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x68

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x69

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc8

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc9

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xca

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12c

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12d

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/16 v3, 0x9

    invoke-direct {v2, v3}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x190

    new-instance v2, Lcom/google/android/gms/car/a/f;

    invoke-direct {v2, v7}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x191

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/4 v3, 0x4

    invoke-direct {v2, v3}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x192

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/16 v3, 0x8

    const/4 v4, 0x3

    const/16 v5, 0xb

    const/16 v6, 0xfa0

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/android/gms/car/a/f;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/car/a/a;->i:Landroid/util/SparseArray;

    const/16 v2, 0x8

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v11}, Landroid/util/SparseArray;-><init>(I)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    invoke-direct {v1, v8}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v9, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v10, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    const/16 v2, 0xa

    invoke-direct {v1, v2}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v7, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v8, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x65

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x66

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x67

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x68

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x69

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc8

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc9

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xca

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12c

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12d

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x190

    new-instance v2, Lcom/google/android/gms/car/a/f;

    invoke-direct {v2, v7}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x191

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/4 v3, 0x4

    invoke-direct {v2, v3}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x192

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/4 v3, 0x5

    const/16 v4, 0x3e8

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/car/a/f;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/car/a/a;->i:Landroid/util/SparseArray;

    const/16 v2, 0x9

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v11}, Landroid/util/SparseArray;-><init>(I)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    invoke-direct {v1, v8}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v9, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v10, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    const/16 v2, 0xa

    invoke-direct {v1, v2}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v7, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v8, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x65

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x66

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x67

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x68

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x69

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc8

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc9

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xca

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12c

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12d

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x190

    new-instance v2, Lcom/google/android/gms/car/a/f;

    invoke-direct {v2, v7}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x191

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/4 v3, 0x4

    invoke-direct {v2, v3}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x192

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/4 v3, 0x5

    const/16 v4, 0x3e8

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/car/a/f;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/car/a/a;->i:Landroid/util/SparseArray;

    const/16 v2, 0xa

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v11}, Landroid/util/SparseArray;-><init>(I)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    invoke-direct {v1, v8}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v9, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v10, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v1, Lcom/google/android/gms/car/a/f;

    const/16 v2, 0xb

    const/4 v3, 0x4

    invoke-direct {v1, v2, v7, v3, v9}, Lcom/google/android/gms/car/a/f;-><init>(IIII)V

    invoke-virtual {v0, v7, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v8, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x65

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x66

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x67

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x68

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x69

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc8

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc9

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xca

    sget-object v2, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12c

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/16 v3, 0xb

    const/4 v4, 0x4

    invoke-direct {v2, v3, v7, v4, v9}, Lcom/google/android/gms/car/a/f;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12d

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/16 v3, 0x9

    invoke-direct {v2, v3}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x190

    new-instance v2, Lcom/google/android/gms/car/a/f;

    invoke-direct {v2, v7}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x191

    new-instance v2, Lcom/google/android/gms/car/a/f;

    const/4 v3, 0x4

    invoke-direct {v2, v3}, Lcom/google/android/gms/car/a/f;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x192

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/car/a/a;->i:Landroid/util/SparseArray;

    const/16 v2, 0xb

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v11}, Landroid/util/SparseArray;-><init>(I)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v9, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v10, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v7, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v8, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x65

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x66

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x67

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x68

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x69

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc8

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xc9

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0xca

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12c

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x12d

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x190

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x191

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v1, 0x192

    sget-object v2, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/car/a/a;->i:Landroid/util/SparseArray;

    invoke-virtual {v1, v8, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 234
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/a/a;I)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/a/a;->f(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/a/a;)Z
    .locals 1

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/google/android/gms/car/a/a;->n:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/car/a/a;)I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/google/android/gms/car/a/a;->j:I

    return v0
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 975
    const-string v0, ""

    return-object v0
.end method

.method static synthetic b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    invoke-static {p0}, Lcom/google/android/gms/car/a/a;->d(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static c(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 58
    sparse-switch p0, :sswitch_data_0

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WRONG-EVENT-CODE("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 59
    :sswitch_0
    const-string v0, "EVENT_UNKNOWN_ERROR"

    goto :goto_0

    .line 60
    :sswitch_1
    const-string v0, "EVENT_SKIP_REQUESTED"

    goto :goto_0

    .line 61
    :sswitch_2
    const-string v0, "EVENT_STATE_TIMEOUT"

    goto :goto_0

    .line 63
    :sswitch_3
    const-string v0, "EVENT_SERVICE_INITIALIZATION_FAILED"

    goto :goto_0

    .line 64
    :sswitch_4
    const-string v0, "EVENT_SERVICE_INITIALIZED"

    goto :goto_0

    .line 65
    :sswitch_5
    const-string v0, "EVENT_ENDPOINT_READY"

    goto :goto_0

    .line 66
    :sswitch_6
    const-string v0, "EVENT_ENABLING_FAILED"

    goto :goto_0

    .line 67
    :sswitch_7
    const-string v0, "EVENT_ENABLED"

    goto :goto_0

    .line 68
    :sswitch_8
    const-string v0, "EVENT_BLUETOOTH_PROFILE_UTIL_READY"

    goto :goto_0

    .line 70
    :sswitch_9
    const-string v0, "EVENT_CAR_CANNOT_BE_PAIRED"

    goto :goto_0

    .line 71
    :sswitch_a
    const-string v0, "EVENT_CAR_DELAYING_PAIRING"

    goto :goto_0

    .line 72
    :sswitch_b
    const-string v0, "EVENT_CAR_READY_FOR_PAIRING"

    goto :goto_0

    .line 74
    :sswitch_c
    const-string v0, "EVENT_PAIRED"

    goto :goto_0

    .line 75
    :sswitch_d
    const-string v0, "EVENT_HFP_CONNECTED"

    goto :goto_0

    .line 77
    :sswitch_e
    const-string v0, "EVENT_DISABLED"

    goto :goto_0

    .line 78
    :sswitch_f
    const-string v0, "EVENT_UNPAIRED"

    goto :goto_0

    .line 79
    :sswitch_10
    const-string v0, "EVENT_HFP_DISCONNECTED"

    goto :goto_0

    .line 58
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x64 -> :sswitch_3
        0x65 -> :sswitch_4
        0x66 -> :sswitch_5
        0x67 -> :sswitch_6
        0x68 -> :sswitch_7
        0x69 -> :sswitch_8
        0xc8 -> :sswitch_9
        0xc9 -> :sswitch_a
        0xca -> :sswitch_b
        0x12c -> :sswitch_c
        0x12d -> :sswitch_d
        0x190 -> :sswitch_e
        0x191 -> :sswitch_f
        0x192 -> :sswitch_10
    .end sparse-switch
.end method

.method private static d(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 102
    sparse-switch p0, :sswitch_data_0

    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WRONG-STATE-CODE("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 103
    :sswitch_0
    const-string v0, "STATE_INITIAL"

    goto :goto_0

    .line 104
    :sswitch_1
    const-string v0, "STATE_WAITING_FOR_ENDPOINT_READY"

    goto :goto_0

    .line 105
    :sswitch_2
    const-string v0, "STATE_ENABLING"

    goto :goto_0

    .line 107
    :sswitch_3
    const-string v0, "STATE_WAITING_FOR_BLUETOOTH_PROFILE_UTIL"

    goto :goto_0

    .line 109
    :sswitch_4
    const-string v0, "STATE_REQUESTING_CAR_PAIRING_PREPARATION"

    goto :goto_0

    .line 110
    :sswitch_5
    const-string v0, "STATE_HFP_CONNECTION_CHECKING"

    goto :goto_0

    .line 111
    :sswitch_6
    const-string v0, "STATE_PREPARING_FOR_PAIRING"

    goto :goto_0

    .line 112
    :sswitch_7
    const-string v0, "STATE_PAIRING"

    goto :goto_0

    .line 113
    :sswitch_8
    const-string v0, "STATE_HFP_CONNECTING"

    goto :goto_0

    .line 114
    :sswitch_9
    const-string v0, "STATE_HFP_CONNECTED"

    goto :goto_0

    .line 115
    :sswitch_a
    const-string v0, "STATE_HFP_MONITORING"

    goto :goto_0

    .line 116
    :sswitch_b
    const-string v0, "STATE_UNPAIRING"

    goto :goto_0

    .line 117
    :sswitch_c
    const-string v0, "STATE_FAILED"

    goto :goto_0

    .line 102
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0x64 -> :sswitch_c
    .end sparse-switch
.end method

.method private e(I)V
    .locals 3

    .prologue
    .line 757
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 758
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "switchToState: from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/a/a;->j:I

    invoke-static {v2}, Lcom/google/android/gms/car/a/a;->d(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/gms/car/a/a;->d(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 761
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/a/a;->k:Lcom/google/android/gms/car/a/f;

    .line 763
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->f:Landroid/os/Handler;

    sget-object v1, Lcom/google/android/gms/car/a/a;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 764
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/a/a;->f(I)V

    .line 765
    return-void
.end method

.method private f(I)V
    .locals 12

    .prologue
    const/16 v11, 0x9

    const/4 v10, 0x6

    const/4 v0, 0x0

    const/4 v9, 0x4

    const/4 v8, 0x3

    .line 768
    const-string v1, "CAR.BT"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 769
    const-string v1, "CAR.BT"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "handleTransition: Entering into state "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/car/a/a;->d(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 773
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/a/a;->f:Landroid/os/Handler;

    sget-object v2, Lcom/google/android/gms/car/a/a;->e:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 775
    iput p1, p0, Lcom/google/android/gms/car/a/a;->j:I

    .line 777
    sget-object v1, Lcom/google/android/gms/car/a/a;->a:Landroid/util/SparseLongArray;

    iget v2, p0, Lcom/google/android/gms/car/a/a;->j:I

    invoke-virtual {v1, v2}, Landroid/util/SparseLongArray;->get(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 778
    if-eqz v1, :cond_4

    .line 779
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 780
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_2

    .line 781
    const-string v4, "CAR.BT"

    invoke-static {v4, v8}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 782
    const-string v4, "CAR.BT"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "state "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/gms/car/a/a;->j:I

    invoke-static {v6}, Lcom/google/android/gms/car/a/a;->d(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " timeout="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " - starting timeout checker"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 785
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/car/a/a;->f:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/gms/car/a/e;

    invoke-direct {v4, p0, v0}, Lcom/google/android/gms/car/a/e;-><init>(Lcom/google/android/gms/car/a/a;B)V

    sget-object v5, Lcom/google/android/gms/car/a/a;->e:Ljava/lang/Object;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    add-long/2addr v2, v6

    invoke-virtual {v1, v4, v5, v2, v3}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;Ljava/lang/Object;J)Z

    .line 795
    :cond_2
    :goto_0
    iget v1, p0, Lcom/google/android/gms/car/a/a;->j:I

    sparse-switch v1, :sswitch_data_0

    .line 954
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleTransition: Wrong state "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/a/a;->j:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 957
    :cond_3
    :goto_1
    :sswitch_0
    return-void

    .line 790
    :cond_4
    const-string v1, "CAR.BT"

    invoke-static {v1, v8}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 791
    const-string v1, "CAR.BT"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Timeout value not defined for state "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/car/a/a;->j:I

    invoke-static {v3}, Lcom/google/android/gms/car/a/a;->d(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 808
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->g:Lcom/google/android/gms/car/a/k;

    invoke-virtual {v0}, Lcom/google/android/gms/car/a/k;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 809
    invoke-direct {p0, v8}, Lcom/google/android/gms/car/a/a;->e(I)V

    goto :goto_1

    .line 812
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/a/a;->l:Lcom/google/android/gms/car/a/g;

    .line 813
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->g:Lcom/google/android/gms/car/a/k;

    invoke-virtual {v0}, Lcom/google/android/gms/car/a/k;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 816
    const/16 v0, 0x67

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/a/a;->a(I)V

    goto :goto_1

    .line 822
    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->h:Lcom/google/android/gms/car/a/b;

    invoke-interface {v0}, Lcom/google/android/gms/car/a/b;->b()V

    goto :goto_1

    .line 827
    :sswitch_3
    iput-boolean v0, p0, Lcom/google/android/gms/car/a/a;->m:Z

    .line 828
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->g:Lcom/google/android/gms/car/a/k;

    invoke-virtual {v0}, Lcom/google/android/gms/car/a/k;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 829
    const-string v0, "CAR.BT"

    invoke-static {v0, v8}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 830
    const-string v0, "CAR.BT"

    const-string v1, "Already pairing. Should cancel bonding process"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 832
    :cond_6
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/a/a;->e(I)V

    goto :goto_1

    .line 835
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->h:Lcom/google/android/gms/car/a/b;

    invoke-interface {v0}, Lcom/google/android/gms/car/a/b;->c()V

    goto :goto_1

    .line 840
    :sswitch_4
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->l:Lcom/google/android/gms/car/a/g;

    invoke-interface {v0}, Lcom/google/android/gms/car/a/g;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 841
    invoke-direct {p0, v11}, Lcom/google/android/gms/car/a/a;->e(I)V

    goto :goto_1

    .line 843
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->l:Lcom/google/android/gms/car/a/g;

    invoke-interface {v0}, Lcom/google/android/gms/car/a/g;->a()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 844
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->g:Lcom/google/android/gms/car/a/k;

    invoke-virtual {v0}, Lcom/google/android/gms/car/a/k;->d()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Lcom/google/android/gms/car/a/a;->m:Z

    if-eqz v0, :cond_9

    .line 845
    const-string v0, "CAR.BT"

    invoke-static {v0, v8}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 846
    const-string v0, "CAR.BT"

    const-string v1, "Already connecting. Waiting"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 849
    :cond_9
    const-string v0, "CAR.BT"

    invoke-static {v0, v8}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 850
    const-string v0, "CAR.BT"

    const-string v1, "Already connecting but will fail. Disconnecting"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 852
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->l:Lcom/google/android/gms/car/a/g;

    invoke-interface {v0}, Lcom/google/android/gms/car/a/g;->c()V

    .line 853
    invoke-direct {p0, v10}, Lcom/google/android/gms/car/a/a;->e(I)V

    goto/16 :goto_1

    .line 857
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->l:Lcom/google/android/gms/car/a/g;

    invoke-interface {v0}, Lcom/google/android/gms/car/a/g;->c()V

    .line 858
    invoke-direct {p0, v10}, Lcom/google/android/gms/car/a/a;->e(I)V

    goto/16 :goto_1

    .line 865
    :sswitch_5
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->g:Lcom/google/android/gms/car/a/k;

    invoke-virtual {v0}, Lcom/google/android/gms/car/a/k;->c()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 866
    const-string v0, "CAR.BT"

    invoke-static {v0, v8}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 867
    const-string v0, "CAR.BT"

    const-string v1, "Already pairing. Cancelling bonding process"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 871
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->g:Lcom/google/android/gms/car/a/k;

    invoke-virtual {v0}, Lcom/google/android/gms/car/a/k;->f()Z

    goto/16 :goto_1

    .line 872
    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->g:Lcom/google/android/gms/car/a/k;

    invoke-virtual {v0}, Lcom/google/android/gms/car/a/k;->d()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 873
    iget-boolean v0, p0, Lcom/google/android/gms/car/a/a;->m:Z

    if-nez v0, :cond_f

    .line 874
    const-string v0, "CAR.BT"

    invoke-static {v0, v8}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 875
    const-string v0, "CAR.BT"

    const-string v1, "Phone thinks it\'s paired but car doesn\'t think so. Unpairing and re-pairing"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 880
    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->g:Lcom/google/android/gms/car/a/k;

    invoke-virtual {v0}, Lcom/google/android/gms/car/a/k;->f()Z

    goto/16 :goto_1

    .line 882
    :cond_f
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/a/a;->e(I)V

    goto/16 :goto_1

    .line 886
    :cond_10
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/a/a;->e(I)V

    goto/16 :goto_1

    .line 893
    :sswitch_6
    iget-object v1, p0, Lcom/google/android/gms/car/a/a;->g:Lcom/google/android/gms/car/a/k;

    const-string v2, "CAR.BT"

    invoke-static {v2, v8}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_11

    const-string v2, "CAR.BT"

    const-string v3, "pair"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_11
    iget v2, v1, Lcom/google/android/gms/car/a/k;->k:I

    if-eqz v2, :cond_14

    const-string v1, "CAR.BT"

    invoke-static {v1, v8}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_12

    const-string v1, "CAR.BT"

    const-string v2, "pair: This object wasn\'t initialized successfully."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_12
    :goto_2
    if-nez v0, :cond_3

    .line 894
    const-string v0, "CAR.BT"

    invoke-static {v0, v9}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 895
    const-string v0, "CAR.BT"

    const-string v1, "pair failed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 897
    :cond_13
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/a/a;->e(I)V

    goto/16 :goto_1

    .line 893
    :cond_14
    iget-object v0, v1, Lcom/google/android/gms/car/a/k;->b:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, v1, Lcom/google/android/gms/car/a/k;->b:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    :cond_15
    iget-object v0, v1, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->createBond()Z

    move-result v0

    goto :goto_2

    .line 904
    :sswitch_7
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->g:Lcom/google/android/gms/car/a/k;

    invoke-virtual {v0}, Lcom/google/android/gms/car/a/k;->d()Z

    move-result v0

    if-nez v0, :cond_16

    .line 905
    invoke-direct {p0, v9}, Lcom/google/android/gms/car/a/a;->e(I)V

    goto/16 :goto_1

    .line 908
    :cond_16
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/a/a;->m:Z

    .line 909
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->l:Lcom/google/android/gms/car/a/g;

    invoke-interface {v0}, Lcom/google/android/gms/car/a/g;->b()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 910
    invoke-direct {p0, v11}, Lcom/google/android/gms/car/a/a;->e(I)V

    goto/16 :goto_1

    .line 912
    :cond_17
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->l:Lcom/google/android/gms/car/a/g;

    invoke-interface {v0}, Lcom/google/android/gms/car/a/g;->a()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 913
    const-string v0, "CAR.BT"

    invoke-static {v0, v8}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 914
    const-string v0, "CAR.BT"

    const-string v1, "Already started connecting. Waiting"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 917
    :cond_18
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->l:Lcom/google/android/gms/car/a/g;

    invoke-interface {v0}, Lcom/google/android/gms/car/a/g;->d()Z

    move-result v0

    if-nez v0, :cond_3

    .line 920
    const-string v0, "CAR.BT"

    invoke-static {v0, v9}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 921
    const-string v0, "CAR.BT"

    const-string v1, "connectHfp failed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 923
    :cond_19
    const/16 v0, 0x192

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/a/a;->a(I)V

    goto/16 :goto_1

    .line 930
    :sswitch_8
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->h:Lcom/google/android/gms/car/a/b;

    invoke-interface {v0}, Lcom/google/android/gms/car/a/b;->d()V

    goto/16 :goto_1

    .line 935
    :sswitch_9
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->l:Lcom/google/android/gms/car/a/g;

    invoke-interface {v0}, Lcom/google/android/gms/car/a/g;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 936
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/a/a;->e(I)V

    goto/16 :goto_1

    .line 942
    :sswitch_a
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->g:Lcom/google/android/gms/car/a/k;

    invoke-virtual {v0}, Lcom/google/android/gms/car/a/k;->c()Z

    move-result v0

    if-nez v0, :cond_1a

    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->g:Lcom/google/android/gms/car/a/k;

    invoke-virtual {v0}, Lcom/google/android/gms/car/a/k;->d()Z

    move-result v0

    if-nez v0, :cond_1a

    .line 943
    invoke-direct {p0, v9}, Lcom/google/android/gms/car/a/a;->e(I)V

    .line 945
    :cond_1a
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->g:Lcom/google/android/gms/car/a/k;

    invoke-virtual {v0}, Lcom/google/android/gms/car/a/k;->f()Z

    goto/16 :goto_1

    .line 950
    :sswitch_b
    const-string v0, "CAR.BT"

    const-string v1, "Failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 795
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0x7 -> :sswitch_6
        0x8 -> :sswitch_7
        0x9 -> :sswitch_8
        0xa -> :sswitch_9
        0xb -> :sswitch_a
        0x64 -> :sswitch_b
    .end sparse-switch
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 969
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/a/a;->n:Z

    .line 970
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->f:Landroid/os/Handler;

    sget-object v1, Lcom/google/android/gms/car/a/a;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 971
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->f:Landroid/os/Handler;

    sget-object v1, Lcom/google/android/gms/car/a/a;->e:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 972
    return-void
.end method

.method public final a(I)V
    .locals 8

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x4

    .line 667
    const-string v0, "CAR.BT"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 668
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fireEvent: currentState="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/a/a;->j:I

    invoke-static {v2}, Lcom/google/android/gms/car/a/a;->d(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " event="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/gms/car/a/a;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/a/a;->n:Z

    if-eqz v0, :cond_2

    .line 673
    const-string v0, "CAR.BT"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 674
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "event fired after cleanup. currentState="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/a/a;->j:I

    invoke-static {v2}, Lcom/google/android/gms/car/a/a;->d(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " event"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/gms/car/a/a;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 754
    :cond_1
    :goto_0
    return-void

    .line 680
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/a/a;->i:Landroid/util/SparseArray;

    iget v1, p0, Lcom/google/android/gms/car/a/a;->j:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    .line 681
    if-nez v0, :cond_3

    .line 682
    const-string v0, "CAR.BT"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 683
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No transition map defined for state "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/a/a;->j:I

    invoke-static {v2}, Lcom/google/android/gms/car/a/a;->d(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 688
    :cond_3
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/a/f;

    .line 689
    if-nez v0, :cond_4

    .line 690
    const-string v0, "CAR.BT"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 691
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No transition defined. state="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/a/a;->j:I

    invoke-static {v2}, Lcom/google/android/gms/car/a/a;->d(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " event="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/gms/car/a/a;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 697
    :cond_4
    sget-object v1, Lcom/google/android/gms/car/a/a;->b:Lcom/google/android/gms/car/a/f;

    if-ne v0, v1, :cond_5

    .line 698
    const-string v0, "CAR.BT"

    invoke-static {v0, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 699
    const-string v0, "CAR.BT"

    const-string v1, "No transition"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 702
    :cond_5
    sget-object v1, Lcom/google/android/gms/car/a/a;->c:Lcom/google/android/gms/car/a/f;

    if-ne v0, v1, :cond_6

    .line 703
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Wrong transition. state="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/a/a;->j:I

    invoke-static {v2}, Lcom/google/android/gms/car/a/a;->d(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " event="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/gms/car/a/a;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 710
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/car/a/a;->k:Lcom/google/android/gms/car/a/f;

    if-eq v0, v1, :cond_8

    .line 711
    iget-object v1, p0, Lcom/google/android/gms/car/a/a;->k:Lcom/google/android/gms/car/a/f;

    if-eqz v1, :cond_7

    .line 712
    iget-object v1, p0, Lcom/google/android/gms/car/a/a;->k:Lcom/google/android/gms/car/a/f;

    const/4 v2, 0x0

    iput v2, v1, Lcom/google/android/gms/car/a/f;->f:I

    .line 714
    :cond_7
    iput-object v0, p0, Lcom/google/android/gms/car/a/a;->k:Lcom/google/android/gms/car/a/f;

    .line 716
    :cond_8
    iget v1, v0, Lcom/google/android/gms/car/a/f;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/gms/car/a/f;->f:I

    .line 718
    const-string v1, "CAR.BT"

    invoke-static {v1, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 719
    const-string v2, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Transition: nextState="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v0, Lcom/google/android/gms/car/a/f;->a:I

    invoke-static {v3}, Lcom/google/android/gms/car/a/a;->d(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " delayMs="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, v0, Lcom/google/android/gms/car/a/f;->b:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " maxCount="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, v0, Lcom/google/android/gms/car/a/f;->c:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " count="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, v0, Lcom/google/android/gms/car/a/f;->f:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v1, v0, Lcom/google/android/gms/car/a/f;->c:I

    if-lez v1, :cond_b

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, " nextStateAfterMax="

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v0, Lcom/google/android/gms/car/a/f;->d:I

    invoke-static {v4}, Lcom/google/android/gms/car/a/a;->d(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " delayMsAfterMax="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, v0, Lcom/google/android/gms/car/a/f;->e:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/car/a/a;->f:Landroid/os/Handler;

    sget-object v2, Lcom/google/android/gms/car/a/a;->d:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 735
    iget v1, v0, Lcom/google/android/gms/car/a/f;->c:I

    if-eqz v1, :cond_a

    iget v1, v0, Lcom/google/android/gms/car/a/f;->f:I

    iget v2, v0, Lcom/google/android/gms/car/a/f;->c:I

    if-gt v1, v2, :cond_c

    .line 737
    :cond_a
    iget v2, v0, Lcom/google/android/gms/car/a/f;->a:I

    .line 738
    iget-wide v0, v0, Lcom/google/android/gms/car/a/f;->b:J

    .line 745
    :goto_2
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-nez v3, :cond_d

    .line 747
    invoke-direct {p0, v2}, Lcom/google/android/gms/car/a/a;->f(I)V

    goto/16 :goto_0

    .line 719
    :cond_b
    const-string v1, ""

    goto :goto_1

    .line 741
    :cond_c
    iget v2, v0, Lcom/google/android/gms/car/a/f;->d:I

    .line 742
    iget v0, v0, Lcom/google/android/gms/car/a/f;->e:I

    int-to-long v0, v0

    goto :goto_2

    .line 750
    :cond_d
    iget-object v3, p0, Lcom/google/android/gms/car/a/a;->f:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/gms/car/a/d;

    invoke-direct {v4, p0, v2}, Lcom/google/android/gms/car/a/d;-><init>(Lcom/google/android/gms/car/a/a;I)V

    sget-object v2, Lcom/google/android/gms/car/a/a;->d:Ljava/lang/Object;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    add-long/2addr v0, v6

    invoke-virtual {v3, v4, v2, v0, v1}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;Ljava/lang/Object;J)Z

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/a/g;)V
    .locals 0

    .prologue
    .line 960
    iput-object p1, p0, Lcom/google/android/gms/car/a/a;->l:Lcom/google/android/gms/car/a/g;

    .line 961
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 964
    iput-boolean p1, p0, Lcom/google/android/gms/car/a/a;->m:Z

    .line 966
    return-void
.end method
