.class final Lcom/google/android/gms/car/cf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/ky;

.field final synthetic b:Lcom/google/android/gms/car/br;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/br;Lcom/google/android/gms/car/ky;)V
    .locals 0

    .prologue
    .line 370
    iput-object p1, p0, Lcom/google/android/gms/car/cf;->b:Lcom/google/android/gms/car/br;

    iput-object p2, p0, Lcom/google/android/gms/car/cf;->a:Lcom/google/android/gms/car/ky;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Ljava/lang/Boolean;
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 373
    const-string v0, "CAR.BT"

    invoke-static {v0, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "registerClient on main thread. client="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/cf;->a:Lcom/google/android/gms/car/ky;

    invoke-interface {v2}, Lcom/google/android/gms/car/ky;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/cf;->b:Lcom/google/android/gms/car/br;

    invoke-static {v0}, Lcom/google/android/gms/car/br;->g(Lcom/google/android/gms/car/br;)V

    .line 378
    iget-object v0, p0, Lcom/google/android/gms/car/cf;->b:Lcom/google/android/gms/car/br;

    invoke-static {v0}, Lcom/google/android/gms/car/br;->h(Lcom/google/android/gms/car/br;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/cp;

    .line 379
    iget-object v0, v0, Lcom/google/android/gms/car/cp;->a:Lcom/google/android/gms/car/ky;

    invoke-interface {v0}, Lcom/google/android/gms/car/ky;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/car/cf;->a:Lcom/google/android/gms/car/ky;

    invoke-interface {v2}, Lcom/google/android/gms/car/ky;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    if-ne v0, v2, :cond_1

    .line 380
    const-string v0, "CAR.BT"

    invoke-static {v0, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 381
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Client "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/cf;->a:Lcom/google/android/gms/car/ky;

    invoke-interface {v2}, Lcom/google/android/gms/car/ky;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " already registered"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    :cond_2
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 395
    :goto_0
    return-object v0

    .line 388
    :cond_3
    :try_start_0
    new-instance v0, Lcom/google/android/gms/car/cp;

    iget-object v1, p0, Lcom/google/android/gms/car/cf;->b:Lcom/google/android/gms/car/br;

    iget-object v2, p0, Lcom/google/android/gms/car/cf;->a:Lcom/google/android/gms/car/ky;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/cp;-><init>(Lcom/google/android/gms/car/br;Lcom/google/android/gms/car/ky;)V

    .line 389
    iget-object v1, p0, Lcom/google/android/gms/car/cf;->b:Lcom/google/android/gms/car/br;

    invoke-static {v1}, Lcom/google/android/gms/car/br;->h(Lcom/google/android/gms/car/br;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 395
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 391
    :catch_0
    move-exception v0

    const-string v0, "CAR.BT"

    const-string v1, "Client already dead?"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 370
    invoke-direct {p0}, Lcom/google/android/gms/car/cf;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
