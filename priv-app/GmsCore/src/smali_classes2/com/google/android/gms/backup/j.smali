.class public final Lcom/google/android/gms/backup/j;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:Lcom/google/android/gms/backup/l;

.field private g:Ljava/util/List;

.field private h:Ljava/util/List;

.field private i:Ljava/util/List;

.field private j:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 390
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/backup/j;->b:Ljava/lang/String;

    .line 407
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/backup/j;->d:Ljava/lang/String;

    .line 424
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/backup/j;->f:Lcom/google/android/gms/backup/l;

    .line 443
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/j;->g:Ljava/util/List;

    .line 476
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/j;->h:Ljava/util/List;

    .line 509
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/j;->i:Ljava/util/List;

    .line 585
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/backup/j;->j:I

    .line 13
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 588
    iget v0, p0, Lcom/google/android/gms/backup/j;->j:I

    if-gez v0, :cond_0

    .line 590
    invoke-virtual {p0}, Lcom/google/android/gms/backup/j;->b()I

    .line 592
    :cond_0
    iget v0, p0, Lcom/google/android/gms/backup/j;->j:I

    return v0
.end method

.method public final a(Lcom/google/android/gms/backup/k;)Lcom/google/android/gms/backup/j;
    .locals 1

    .prologue
    .line 493
    if-nez p1, :cond_0

    .line 494
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 496
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/backup/j;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 497
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/backup/j;->h:Ljava/util/List;

    .line 499
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/backup/j;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 500
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/backup/l;)Lcom/google/android/gms/backup/j;
    .locals 1

    .prologue
    .line 428
    if-nez p1, :cond_0

    .line 429
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 431
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/backup/j;->e:Z

    .line 432
    iput-object p1, p0, Lcom/google/android/gms/backup/j;->f:Lcom/google/android/gms/backup/l;

    .line 433
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/backup/m;)Lcom/google/android/gms/backup/j;
    .locals 1

    .prologue
    .line 460
    if-nez p1, :cond_0

    .line 461
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 463
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/backup/j;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 464
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/backup/j;->g:Ljava/util/List;

    .line 466
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/backup/j;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 467
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/backup/x;)Lcom/google/android/gms/backup/j;
    .locals 1

    .prologue
    .line 526
    if-nez p1, :cond_0

    .line 527
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 529
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/backup/j;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 530
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/backup/j;->i:Ljava/util/List;

    .line 532
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/backup/j;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 533
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/backup/j;
    .locals 1

    .prologue
    .line 394
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/backup/j;->a:Z

    .line 395
    iput-object p1, p0, Lcom/google/android/gms/backup/j;->b:Ljava/lang/String;

    .line 396
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 2

    .prologue
    .line 10
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/backup/j;->a(Ljava/lang/String;)Lcom/google/android/gms/backup/j;

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/android/gms/backup/l;

    invoke-direct {v0}, Lcom/google/android/gms/backup/l;-><init>()V

    const/4 v1, 0x4

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/backup/j;->a(Lcom/google/android/gms/backup/l;)Lcom/google/android/gms/backup/j;

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/android/gms/backup/x;

    invoke-direct {v0}, Lcom/google/android/gms/backup/x;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/backup/j;->a(Lcom/google/android/gms/backup/x;)Lcom/google/android/gms/backup/j;

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/android/gms/backup/m;

    invoke-direct {v0}, Lcom/google/android/gms/backup/m;-><init>()V

    const/4 v1, 0x7

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/backup/j;->a(Lcom/google/android/gms/backup/m;)Lcom/google/android/gms/backup/j;

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/android/gms/backup/k;

    invoke-direct {v0}, Lcom/google/android/gms/backup/k;-><init>()V

    const/16 v1, 0xa

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/backup/j;->a(Lcom/google/android/gms/backup/k;)Lcom/google/android/gms/backup/j;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/backup/j;->b(Ljava/lang/String;)Lcom/google/android/gms/backup/j;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1a -> :sswitch_1
        0x23 -> :sswitch_2
        0x32 -> :sswitch_3
        0x3b -> :sswitch_4
        0x53 -> :sswitch_5
        0xa2 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 3

    .prologue
    .line 565
    iget-boolean v0, p0, Lcom/google/android/gms/backup/j;->a:Z

    if-eqz v0, :cond_0

    .line 566
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/backup/j;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 568
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/backup/j;->e:Z

    if-eqz v0, :cond_1

    .line 569
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/backup/j;->f:Lcom/google/android/gms/backup/l;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/f;)V

    .line 571
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/backup/j;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/x;

    .line 572
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    goto :goto_0

    .line 574
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/backup/j;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/m;

    .line 575
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/f;)V

    goto :goto_1

    .line 577
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/backup/j;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/k;

    .line 578
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/f;)V

    goto :goto_2

    .line 580
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/backup/j;->c:Z

    if-eqz v0, :cond_5

    .line 581
    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/android/gms/backup/j;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 583
    :cond_5
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 597
    const/4 v0, 0x0

    .line 598
    iget-boolean v1, p0, Lcom/google/android/gms/backup/j;->a:Z

    if-eqz v1, :cond_0

    .line 599
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/backup/j;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 602
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/backup/j;->e:Z

    if-eqz v1, :cond_1

    .line 603
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/backup/j;->f:Lcom/google/android/gms/backup/l;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->c(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 606
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/backup/j;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/x;

    .line 607
    const/4 v3, 0x6

    invoke-static {v3, v0}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 609
    goto :goto_0

    .line 610
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/backup/j;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/m;

    .line 611
    const/4 v3, 0x7

    invoke-static {v3, v0}, Lcom/google/protobuf/a/c;->c(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/2addr v1, v0

    .line 613
    goto :goto_1

    .line 614
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/backup/j;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/k;

    .line 615
    const/16 v3, 0xa

    invoke-static {v3, v0}, Lcom/google/protobuf/a/c;->c(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/2addr v1, v0

    .line 617
    goto :goto_2

    .line 618
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/backup/j;->c:Z

    if-eqz v0, :cond_5

    .line 619
    const/16 v0, 0x14

    iget-object v2, p0, Lcom/google/android/gms/backup/j;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 622
    :cond_5
    iput v1, p0, Lcom/google/android/gms/backup/j;->j:I

    .line 623
    return v1
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/backup/j;
    .locals 1

    .prologue
    .line 411
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/backup/j;->c:Z

    .line 412
    iput-object p1, p0, Lcom/google/android/gms/backup/j;->d:Ljava/lang/String;

    .line 413
    return-object p0
.end method
