.class final Lcom/google/android/gms/car/senderprotocol/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/c/a/b;

.field final b:Lcom/google/android/gms/car/senderprotocol/z;

.field final c:[Ljava/nio/ByteBuffer;

.field final d:Ljava/nio/ByteBuffer;

.field final e:Lcom/google/android/gms/car/senderprotocol/y;

.field final synthetic f:Lcom/google/android/gms/car/senderprotocol/y;

.field private final g:Lcom/google/android/c/a/c;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/senderprotocol/y;Lcom/google/android/gms/car/senderprotocol/y;Lcom/google/android/gms/car/senderprotocol/z;)V
    .locals 2

    .prologue
    .line 662
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/ab;->f:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 650
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/ac;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/senderprotocol/ac;-><init>(Lcom/google/android/gms/car/senderprotocol/ab;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ab;->g:Lcom/google/android/c/a/c;

    .line 663
    iput-object p2, p0, Lcom/google/android/gms/car/senderprotocol/ab;->e:Lcom/google/android/gms/car/senderprotocol/y;

    .line 664
    new-instance v0, Lcom/google/android/c/a/b;

    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ab;->g:Lcom/google/android/c/a/c;

    invoke-direct {v0, v1}, Lcom/google/android/c/a/b;-><init>(Lcom/google/android/c/a/c;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ab;->a:Lcom/google/android/c/a/b;

    .line 665
    iput-object p3, p0, Lcom/google/android/gms/car/senderprotocol/ab;->b:Lcom/google/android/gms/car/senderprotocol/z;

    .line 666
    const/16 v0, 0x100

    new-array v0, v0, [Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ab;->c:[Ljava/nio/ByteBuffer;

    .line 667
    const/16 v0, 0x8

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ab;->d:Ljava/nio/ByteBuffer;

    .line 668
    return-void
.end method

.method static a(Lcom/google/android/c/a/b;Ljava/nio/ByteBuffer;I)I
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 685
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    add-int/2addr v1, v0

    iget v0, p0, Lcom/google/android/c/a/b;->c:I

    iget v4, p0, Lcom/google/android/c/a/b;->d:I

    sub-int v4, v0, v4

    if-lez v4, :cond_8

    iget-object v0, p0, Lcom/google/android/c/a/b;->b:[B

    iget v5, p0, Lcom/google/android/c/a/b;->d:I

    invoke-static {p2, v4}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-static {v0, v5, v3, v1, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    if-lt v4, p2, :cond_0

    iget v0, p0, Lcom/google/android/c/a/b;->d:I

    add-int/2addr v0, p2

    iput v0, p0, Lcom/google/android/c/a/b;->d:I

    move v0, p2

    :goto_0
    add-int/2addr v0, v2

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 689
    return p2

    .line 685
    :cond_0
    sub-int v0, p2, v4

    add-int/2addr v1, v4

    :goto_1
    iput v9, p0, Lcom/google/android/c/a/b;->c:I

    iput v9, p0, Lcom/google/android/c/a/b;->d:I

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    :cond_1
    :goto_2
    iget v5, p0, Lcom/google/android/c/a/b;->a:I

    if-le v0, v5, :cond_5

    invoke-virtual {v4}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v5

    if-eqz v5, :cond_2

    new-instance v0, Ljava/io/IOException;

    const-string v1, "interrupted"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v5, p0, Lcom/google/android/c/a/b;->e:Lcom/google/android/c/a/c;

    invoke-interface {v5, v3, v1, v0}, Lcom/google/android/c/a/c;->a([BII)I

    move-result v5

    if-lez v5, :cond_3

    add-int/2addr v1, v5

    sub-int/2addr v0, v5

    goto :goto_2

    :cond_3
    if-gez v5, :cond_1

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "read failed "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-object v5, p0, Lcom/google/android/c/a/b;->e:Lcom/google/android/c/a/c;

    iget-object v6, p0, Lcom/google/android/c/a/b;->b:[B

    iget v7, p0, Lcom/google/android/c/a/b;->c:I

    iget v8, p0, Lcom/google/android/c/a/b;->a:I

    invoke-interface {v5, v6, v7, v8}, Lcom/google/android/c/a/c;->a([BII)I

    move-result v5

    if-lez v5, :cond_6

    iget v6, p0, Lcom/google/android/c/a/b;->c:I

    add-int/2addr v5, v6

    iput v5, p0, Lcom/google/android/c/a/b;->c:I

    :cond_5
    iget v5, p0, Lcom/google/android/c/a/b;->c:I

    if-le v0, v5, :cond_7

    invoke-virtual {v4}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v5

    if-eqz v5, :cond_4

    new-instance v0, Ljava/io/IOException;

    const-string v1, "interrupted"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    if-gez v5, :cond_5

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "read failed "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    iget-object v4, p0, Lcom/google/android/c/a/b;->b:[B

    invoke-static {v4, v9, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v1, p0, Lcom/google/android/c/a/b;->d:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/c/a/b;->d:I

    move v0, p2

    goto/16 :goto_0

    :cond_8
    move v0, p2

    goto/16 :goto_1
.end method

.method static a(II)Z
    .locals 1

    .prologue
    .line 832
    and-int v0, p0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
