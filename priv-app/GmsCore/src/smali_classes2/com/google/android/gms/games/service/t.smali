.class final Lcom/google/android/gms/games/service/t;
.super Lcom/google/android/gms/games/service/s;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lcom/google/android/gms/games/internal/dx;

.field private c:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dx;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/google/android/gms/games/service/s;-><init>()V

    .line 81
    iput-object p1, p0, Lcom/google/android/gms/games/service/t;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 82
    iput-object p2, p0, Lcom/google/android/gms/games/service/t;->b:Lcom/google/android/gms/games/internal/dx;

    .line 83
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 113
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/t;->b:Lcom/google/android/gms/games/internal/dx;

    iget-object v1, p0, Lcom/google/android/gms/games/service/t;->c:Landroid/content/Intent;

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/games/internal/dx;->a(ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 87
    new-instance v2, Lcom/google/android/gms/common/server/a/a;

    iget-object v1, p0, Lcom/google/android/gms/games/service/t;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v2, v1, v0}, Lcom/google/android/gms/common/server/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;Z)V

    .line 89
    const/16 v1, 0x3e8

    .line 93
    :try_start_0
    invoke-virtual {v2, p1}, Lcom/google/android/gms/common/server/a/a;->b(Landroid/content/Context;)Ljava/lang/String;

    .line 94
    iget-object v2, p0, Lcom/google/android/gms/games/service/t;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2}, Lcom/google/android/gms/games/a/l;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v2

    .line 95
    invoke-virtual {p2, p1, v2}, Lcom/google/android/gms/games/a/t;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 96
    if-eqz v1, :cond_0

    const/4 v0, 0x0

    .line 107
    :cond_0
    :goto_0
    return v0

    .line 99
    :catch_0
    move-exception v0

    const/4 v0, 0x6

    .line 105
    goto :goto_0

    .line 100
    :catch_1
    move-exception v0

    .line 101
    instance-of v2, v0, Lcom/google/android/gms/auth/ae;

    if-eqz v2, :cond_1

    .line 102
    check-cast v0, Lcom/google/android/gms/auth/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/t;->c:Landroid/content/Intent;

    .line 103
    const/16 v0, 0x3e9

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
