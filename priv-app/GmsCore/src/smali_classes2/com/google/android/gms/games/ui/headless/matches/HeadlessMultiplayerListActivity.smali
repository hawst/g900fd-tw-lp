.class public final Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;
.super Lcom/google/android/gms/games/ui/headless/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/b/a/d;
.implements Lcom/google/android/gms/games/ui/b/a/j;
.implements Lcom/google/android/gms/games/ui/b/a/m;
.implements Lcom/google/android/gms/games/ui/ch;
.implements Lcom/google/android/gms/games/ui/common/matches/v;


# static fields
.field private static final i:I

.field private static final j:I


# instance fields
.field private k:Lcom/google/android/gms/games/ui/headless/matches/a;

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    sget v0, Lcom/google/android/gms/l;->aR:I

    sput v0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->i:I

    .line 41
    sget v0, Lcom/google/android/gms/m;->o:I

    sput v0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->j:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 55
    sget v0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->i:I

    sget v1, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->j:I

    invoke-direct {p0, v0, v1, v2, v2}, Lcom/google/android/gms/games/ui/headless/a;-><init>(IIZZ)V

    .line 57
    return-void
.end method

.method private N()Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 101
    iget v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->l:I

    packed-switch v0, :pswitch_data_0

    .line 115
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCurrentFragment: unexpected index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->l:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :pswitch_0
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/k;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/common/matches/k;-><init>()V

    .line 112
    :goto_0
    return-object v0

    .line 106
    :pswitch_1
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/q;->b(I)Lcom/google/android/gms/games/ui/common/matches/q;

    move-result-object v0

    goto :goto_0

    .line 109
    :pswitch_2
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/q;->b(I)Lcom/google/android/gms/games/ui/common/matches/q;

    move-result-object v0

    goto :goto_0

    .line 112
    :pswitch_3
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/q;->b(I)Lcom/google/android/gms/games/ui/common/matches/q;

    move-result-object v0

    goto :goto_0

    .line 101
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method protected final K()V
    .locals 2

    .prologue
    .line 81
    invoke-super {p0}, Lcom/google/android/gms/games/ui/headless/a;->K()V

    .line 84
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Landroid/support/v7/app/a;->f()V

    .line 86
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    .line 87
    return-void
.end method

.method public final K_()V
    .locals 2

    .prologue
    .line 154
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->N()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 155
    instance-of v1, v0, Lcom/google/android/gms/games/ui/ch;

    if-eqz v1, :cond_0

    .line 156
    check-cast v0, Lcom/google/android/gms/games/ui/ch;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/ch;->K_()V

    .line 158
    :cond_0
    return-void
.end method

.method public final N_()Lcom/google/android/gms/games/ui/b/a/l;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->k:Lcom/google/android/gms/games/ui/headless/matches/a;

    return-object v0
.end method

.method public final T()Lcom/google/android/gms/games/ui/common/matches/u;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->k:Lcom/google/android/gms/games/ui/headless/matches/a;

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/games/ui/b/a/c;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->k:Lcom/google/android/gms/games/ui/headless/matches/a;

    return-object v0
.end method

.method public final e()Lcom/google/android/gms/games/ui/b/a/i;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->k:Lcom/google/android/gms/games/ui/headless/matches/a;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 61
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/headless/a;->onCreate(Landroid/os/Bundle;)V

    .line 62
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    :goto_0
    return-void

    .line 66
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/headless/matches/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/headless/matches/a;-><init>(Lcom/google/android/gms/games/ui/headless/a;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->k:Lcom/google/android/gms/games/ui/headless/matches/a;

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->a(Ljava/lang/CharSequence;)V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.FRAGMENT_INDEX"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->l:I

    .line 71
    iget v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->l:I

    if-ne v0, v2, :cond_1

    .line 72
    const-string v0, "HeadlessMultiplayerAct"

    const-string v1, "Fragment Index not found in the Intent! Bailing!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->finish()V

    goto :goto_0

    .line 76
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->N()Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v2, Lcom/google/android/gms/j;->dw:I

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    iget v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->l:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setCurrentTitle: unexpected index: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->l:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    sget v0, Lcom/google/android/gms/p;->iN:I

    :goto_1
    if-lez v0, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->setTitle(I)V

    goto :goto_0

    :pswitch_1
    sget v0, Lcom/google/android/gms/p;->iO:I

    goto :goto_1

    :pswitch_2
    sget v0, Lcom/google/android/gms/p;->iR:I

    goto :goto_1

    :pswitch_3
    sget v0, Lcom/google/android/gms/p;->iM:I

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/matches/HeadlessMultiplayerListActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
