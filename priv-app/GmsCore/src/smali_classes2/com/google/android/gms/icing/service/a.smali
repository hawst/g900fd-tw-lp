.class public final Lcom/google/android/gms/icing/service/a;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/icing/b/a;

.field private final d:Lcom/google/android/gms/icing/impl/u;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/b/a;Lcom/google/android/gms/icing/impl/u;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p3}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 36
    iput-object p1, p0, Lcom/google/android/gms/icing/service/a;->a:Lcom/google/android/gms/icing/b/a;

    .line 37
    iput-object p2, p0, Lcom/google/android/gms/icing/service/a;->d:Lcom/google/android/gms/icing/impl/u;

    .line 38
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/icing/service/a;)Lcom/google/android/gms/icing/impl/u;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/gms/icing/service/a;->d:Lcom/google/android/gms/icing/impl/u;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/common/internal/bg;ILandroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 141
    if-nez p2, :cond_0

    .line 142
    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p1, p2, p3, v0}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    .line 151
    :goto_0
    return-void

    .line 145
    :cond_0
    const/4 v0, 0x0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-interface {p1, p2, v0, v1}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 147
    :catch_0
    move-exception v0

    const-string v1, "Service broker callback failed"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/icing/service/a;->d:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->F()Lcom/google/android/gms/icing/impl/m;

    move-result-object v0

    const-string v1, "postinit_failed"

    invoke-interface {v0, v1}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/common/internal/bg;Ljava/lang/String;Landroid/os/IBinder;)V
    .locals 4

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/icing/service/a;->d:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->z()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/icing/service/a;->a:Lcom/google/android/gms/icing/b/a;

    new-instance v1, Lcom/google/android/gms/icing/service/b;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/gms/icing/service/b;-><init>(Lcom/google/android/gms/icing/service/a;Lcom/google/android/gms/common/internal/bg;Ljava/lang/String;Landroid/os/IBinder;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    .line 83
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/internal/bg;Lcom/google/android/gms/common/internal/GetServiceRequest;)V
    .locals 3

    .prologue
    .line 46
    invoke-virtual {p2}, Lcom/google/android/gms/common/internal/GetServiceRequest;->c()Ljava/lang/String;

    move-result-object v0

    .line 47
    invoke-virtual {p2}, Lcom/google/android/gms/common/internal/GetServiceRequest;->b()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 68
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid service id in the request."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :sswitch_0
    iget-object v1, p0, Lcom/google/android/gms/icing/service/a;->d:Lcom/google/android/gms/icing/impl/u;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/icing/service/a;->b(Lcom/google/android/gms/common/internal/bg;Ljava/lang/String;Landroid/os/IBinder;)V

    .line 66
    :goto_0
    return-void

    .line 52
    :sswitch_1
    new-instance v1, Lcom/google/android/gms/search/administration/e;

    iget-object v2, p0, Lcom/google/android/gms/icing/service/a;->d:Lcom/google/android/gms/icing/impl/u;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/search/administration/e;-><init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/icing/service/a;->b(Lcom/google/android/gms/common/internal/bg;Ljava/lang/String;Landroid/os/IBinder;)V

    goto :goto_0

    .line 56
    :sswitch_2
    new-instance v1, Lcom/google/android/gms/search/global/l;

    iget-object v2, p0, Lcom/google/android/gms/icing/service/a;->d:Lcom/google/android/gms/icing/impl/u;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/search/global/l;-><init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/icing/service/a;->b(Lcom/google/android/gms/common/internal/bg;Ljava/lang/String;Landroid/os/IBinder;)V

    goto :goto_0

    .line 60
    :sswitch_3
    new-instance v1, Lcom/google/android/gms/search/queries/o;

    iget-object v2, p0, Lcom/google/android/gms/icing/service/a;->d:Lcom/google/android/gms/icing/impl/u;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/search/queries/o;-><init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/icing/service/a;->b(Lcom/google/android/gms/common/internal/bg;Ljava/lang/String;Landroid/os/IBinder;)V

    goto :goto_0

    .line 64
    :sswitch_4
    new-instance v1, Lcom/google/android/gms/search/corpora/m;

    iget-object v2, p0, Lcom/google/android/gms/icing/service/a;->d:Lcom/google/android/gms/icing/impl/u;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/search/corpora/m;-><init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/icing/service/a;->b(Lcom/google/android/gms/common/internal/bg;Ljava/lang/String;Landroid/os/IBinder;)V

    goto :goto_0

    .line 47
    nop

    :sswitch_data_0
    .sparse-switch
        0x15 -> :sswitch_0
        0x1e -> :sswitch_1
        0x20 -> :sswitch_3
        0x21 -> :sswitch_2
        0x24 -> :sswitch_4
    .end sparse-switch
.end method

.method final a(Lcom/google/android/gms/common/internal/bg;Ljava/lang/String;Landroid/os/IBinder;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/icing/service/a;->a:Lcom/google/android/gms/icing/b/a;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/icing/service/a;->d:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 89
    const-string v0, "Not initialized"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 90
    const/16 v0, 0x10

    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/gms/icing/service/a;->a(Lcom/google/android/gms/common/internal/bg;ILandroid/os/IBinder;)V

    .line 124
    :goto_0
    return-void

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/service/a;->d:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->A()Lcom/google/android/gms/icing/impl/a/f;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/icing/impl/a/f;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v3

    .line 95
    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/a/e;->g()Z

    move-result v0

    if-nez v0, :cond_1

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "App "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " not allowed to use icing"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/icing/service/a;->d:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->F()Lcom/google/android/gms/icing/impl/m;

    move-result-object v0

    const-string v1, "app_disallowed"

    invoke-interface {v0, v1}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    .line 100
    const/16 v0, 0x8

    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/gms/icing/service/a;->a(Lcom/google/android/gms/common/internal/bg;ILandroid/os/IBinder;)V

    goto :goto_0

    .line 104
    :cond_1
    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/a/e;->e()V

    .line 106
    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/a/e;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 108
    iget-object v8, p0, Lcom/google/android/gms/icing/service/a;->a:Lcom/google/android/gms/icing/b/a;

    new-instance v0, Lcom/google/android/gms/icing/service/c;

    move-object v1, p0

    move-object v2, p2

    move-object v4, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/icing/service/c;-><init>(Lcom/google/android/gms/icing/service/a;Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/e;Lcom/google/android/gms/common/internal/bg;Landroid/os/IBinder;)V

    invoke-virtual {v8, v0, v10, v11}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    .line 120
    :goto_1
    sget-object v0, Lcom/google/android/gms/icing/a/a;->E:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/icing/service/a;->d:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->A()Lcom/google/android/gms/icing/impl/a/f;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/icing/impl/a/f;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v6

    .line 122
    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/icing/service/a;->a:Lcom/google/android/gms/icing/b/a;

    new-instance v2, Lcom/google/android/gms/icing/impl/bl;

    iget-object v3, p0, Lcom/google/android/gms/icing/service/a;->d:Lcom/google/android/gms/icing/impl/u;

    invoke-direct {v2, v3, v7, v0}, Lcom/google/android/gms/icing/impl/bl;-><init>(Lcom/google/android/gms/icing/impl/u;ZZ)V

    invoke-virtual {v1, v2, v10, v11}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    goto :goto_0

    .line 117
    :cond_2
    invoke-virtual {p0, v3, p1, p3}, Lcom/google/android/gms/icing/service/a;->a(Lcom/google/android/gms/icing/impl/a/e;Lcom/google/android/gms/common/internal/bg;Landroid/os/IBinder;)V

    goto :goto_1

    :cond_3
    move v0, v7

    .line 120
    goto :goto_2
.end method

.method final a(Lcom/google/android/gms/icing/impl/a/e;Lcom/google/android/gms/common/internal/bg;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 129
    :try_start_0
    iget-object v1, p1, Lcom/google/android/gms/icing/impl/a/e;->f:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Lcom/google/android/gms/icing/impl/b/a; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p1, Lcom/google/android/gms/icing/impl/a/e;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/icing/impl/b/a;

    iget-object v2, p1, Lcom/google/android/gms/icing/impl/a/e;->e:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/google/android/gms/icing/impl/b/a;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1

    throw v0
    :try_end_2
    .catch Lcom/google/android/gms/icing/impl/b/a; {:try_start_2 .. :try_end_2} :catch_0

    .line 131
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/b/a;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 133
    const/16 v0, 0xa

    invoke-direct {p0, p2, v0, p3}, Lcom/google/android/gms/icing/service/a;->a(Lcom/google/android/gms/common/internal/bg;ILandroid/os/IBinder;)V

    .line 135
    :goto_0
    return-void

    .line 129
    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 130
    const/4 v0, 0x0

    :try_start_4
    invoke-direct {p0, p2, v0, p3}, Lcom/google/android/gms/icing/service/a;->a(Lcom/google/android/gms/common/internal/bg;ILandroid/os/IBinder;)V
    :try_end_4
    .catch Lcom/google/android/gms/icing/impl/b/a; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0
.end method
