.class final Lcom/google/android/gms/games/ui/client/players/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/client/players/e;

.field final synthetic b:Lcom/google/android/gms/games/ui/client/players/a;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/client/players/a;Lcom/google/android/gms/games/ui/client/players/e;)V
    .locals 0

    .prologue
    .line 647
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/players/c;->b:Lcom/google/android/gms/games/ui/client/players/a;

    iput-object p2, p0, Lcom/google/android/gms/games/ui/client/players/c;->a:Lcom/google/android/gms/games/ui/client/players/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 649
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/c;->b:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/a;->a(Lcom/google/android/gms/games/ui/client/players/a;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 650
    if-eqz v0, :cond_0

    .line 651
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/c;->b:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/client/players/a;->a(Lcom/google/android/gms/games/ui/client/players/a;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 653
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/c;->a:Lcom/google/android/gms/games/ui/client/players/e;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/client/players/e;->b:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/c;->b:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/client/players/a;->a(Lcom/google/android/gms/games/ui/client/players/a;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 654
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/c;->b:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/a;->d(Lcom/google/android/gms/games/ui/client/players/a;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    .line 661
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-eq v0, v1, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-eq v0, v1, :cond_1

    .line 663
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/c;->b:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/a;->d(Lcom/google/android/gms/games/ui/client/players/a;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    .line 665
    :cond_1
    return-void
.end method
