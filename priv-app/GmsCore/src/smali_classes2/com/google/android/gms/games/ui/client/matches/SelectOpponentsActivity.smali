.class public final Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;
.super Lcom/google/android/gms/games/ui/client/a;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Lcom/google/android/gms/games/ui/client/players/p;
.implements Lcom/google/android/gms/games/ui/client/players/w;


# static fields
.field private static final n:I

.field private static final o:I


# instance fields
.field private A:Ljava/util/HashMap;

.field private B:I

.field private p:I

.field private q:I

.field private r:I

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Ljava/util/ArrayList;

.field private x:Landroid/view/View;

.field private y:Landroid/view/animation/Animation;

.field private z:Landroid/view/animation/Animation;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    sget v0, Lcom/google/android/gms/l;->bD:I

    sput v0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->n:I

    .line 56
    sget v0, Lcom/google/android/gms/m;->m:I

    sput v0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->o:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 86
    const/4 v0, 0x0

    sget v1, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->o:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/client/a;-><init>(II)V

    .line 87
    return-void
.end method

.method private ac()Z
    .locals 4

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->A:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->B:I

    add-int/2addr v0, v1

    .line 273
    iget v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->q:I

    if-le v0, v1, :cond_0

    .line 277
    const-string v1, "SelectOpponentsActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "hasMinPlayers: numSelected too large ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " > "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->q:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    :cond_0
    iget v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->p:I

    if-lt v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(I)Landroid/view/animation/Animation;
    .locals 2

    .prologue
    .line 333
    invoke-static {p0, p1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 334
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 335
    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 336
    return-object v0
.end method


# virtual methods
.method protected final N()I
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x7

    return v0
.end method

.method public final T()I
    .locals 1

    .prologue
    .line 363
    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->p:I

    return v0
.end method

.method public final U()I
    .locals 1

    .prologue
    .line 368
    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->q:I

    return v0
.end method

.method public final V()Z
    .locals 1

    .prologue
    .line 373
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->s:Z

    return v0
.end method

.method public final W()Z
    .locals 1

    .prologue
    .line 378
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->t:Z

    return v0
.end method

.method public final X()I
    .locals 2

    .prologue
    .line 383
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->v:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/g;->C:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Y()Z
    .locals 1

    .prologue
    .line 389
    const/4 v0, 0x1

    return v0
.end method

.method public final Z()Z
    .locals 1

    .prologue
    .line 395
    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->r:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/common/api/w;)V
    .locals 2

    .prologue
    .line 144
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->a(Lcom/google/android/gms/common/api/w;)V

    .line 145
    new-instance v0, Lcom/google/android/gms/people/ad;

    invoke-direct {v0}, Lcom/google/android/gms/people/ad;-><init>()V

    .line 146
    const/16 v1, 0x76

    iput v1, v0, Lcom/google/android/gms/people/ad;->a:I

    .line 147
    sget-object v1, Lcom/google/android/gms/people/x;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0}, Lcom/google/android/gms/people/ad;->a()Lcom/google/android/gms/people/ac;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    .line 148
    return-void
.end method

.method protected final a(Lcom/google/android/gms/games/i;)V
    .locals 1

    .prologue
    .line 153
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->a(Lcom/google/android/gms/games/i;)V

    .line 154
    const-string v0, "copresence"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/i;->a(Ljava/lang/String;)Lcom/google/android/gms/games/i;

    .line 155
    return-void
.end method

.method public final a(Ljava/util/HashMap;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 241
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->A:Ljava/util/HashMap;

    .line 242
    iput p2, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->B:I

    .line 244
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->ac()Z

    move-result v0

    .line 246
    if-nez v0, :cond_2

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->v:Z

    if-eqz v1, :cond_2

    .line 247
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->u:Z

    if-nez v0, :cond_0

    .line 248
    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->u:Z

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->x:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->z:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->x:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 252
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->v:Z

    .line 261
    :cond_1
    :goto_0
    return-void

    .line 253
    :cond_2
    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->v:Z

    if-nez v0, :cond_1

    .line 254
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->u:Z

    if-nez v0, :cond_3

    .line 255
    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->u:Z

    .line 256
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->x:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->y:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 258
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->x:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 259
    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->v:Z

    goto :goto_0
.end method

.method public final aa()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->w:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final ab()I
    .locals 1

    .prologue
    .line 405
    const/4 v0, 0x1

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x1

    return v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x2

    return v0
.end method

.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 346
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->u:Z

    .line 347
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->y:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->v:Z

    if-nez v0, :cond_1

    .line 349
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->x:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->z:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 354
    :cond_0
    :goto_0
    return-void

    .line 350
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->z:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->v:Z

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->x:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->y:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 359
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 342
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    .line 164
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->re:I

    if-ne v0, v1, :cond_2

    .line 166
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->ac()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SelectOpponentsActivity"

    const-string v1, "onPlay: \'Play\' action shouldn\'t have been enabled; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :goto_0
    return-void

    .line 166
    :cond_0
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->A:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v4, Ljava/util/HashMap;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->A:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/HashMap;-><init>(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->A:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->A:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    const-string v0, "players"

    invoke-virtual {v6, v0, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v0, "min_automatch_players"

    iget v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->B:I

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "max_automatch_players"

    iget v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->B:I

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/v;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/a;->k:Lcom/google/android/gms/games/GameEntity;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->Q()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v2

    iget v5, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->B:I

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;I)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0, v6}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->finish()V

    goto :goto_0

    .line 168
    :cond_2
    const-string v0, "SelectOpponentsActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onClick: unexpected view: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x7

    const/4 v4, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 91
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->onCreate(Landroid/os/Bundle;)V

    .line 94
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->R()V

    .line 98
    sget v0, Lcom/google/android/gms/p;->kX:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->setTitle(I)V

    .line 101
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->l:Z

    .line 105
    sget v0, Lcom/google/android/gms/j;->ck:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->x:Landroid/view/View;

    .line 106
    sget v0, Lcom/google/android/gms/j;->re:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    sget v0, Lcom/google/android/gms/b;->h:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->c(I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->y:Landroid/view/animation/Animation;

    .line 108
    sget v0, Lcom/google/android/gms/b;->i:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->c(I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->z:Landroid/view/animation/Animation;

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->x:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 111
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->u:Z

    .line 112
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->v:Z

    .line 114
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v0, "com.google.android.gms.games.MIN_SELECTIONS"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->p:I

    const-string v0, "com.google.android.gms.games.MAX_SELECTIONS"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->q:I

    const-string v0, "com.google.android.gms.games.SHOW_AUTOMATCH"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->s:Z

    const-string v0, "com.google.android.gms.games.SHOW_NEARBY_SEARCH"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->t:Z

    sget-object v0, Lcom/google/android/gms/games/ui/n;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->t:Z

    :cond_0
    const-string v0, "com.google.android.gms.games.MULTIPLAYER_TYPE"

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->r:I

    const-string v0, "players"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->w:Ljava/util/ArrayList;

    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->r:I

    if-ne v0, v4, :cond_1

    const-string v0, "SelectOpponentsActivity"

    const-string v3, "parseIntent: missing intent extra \'EXTRA_MULTIPLAYER_TYPE\'... defaulting type to to RTMP"

    invoke-static {v0, v3}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    iput v2, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->r:I

    :cond_1
    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->p:I

    if-gtz v0, :cond_3

    const-string v0, "SelectOpponentsActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "minPlayers was "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->p:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", but must be greater than or equal to 1."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    .line 116
    const-string v0, "SelectOpponentsActivity"

    const-string v1, "Error parsing intent; bailing out..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->finish()V

    .line 121
    :cond_2
    sget v0, Lcom/google/android/gms/j;->oJ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 122
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 123
    invoke-virtual {p0, p0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->a(Landroid/content/Context;)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 124
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 125
    return-void

    .line 114
    :cond_3
    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->q:I

    if-le v0, v5, :cond_4

    const-string v0, "SelectOpponentsActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "maxPlayers was "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->q:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", but is currently limited to 7 (for 8"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " players total, counting the current player)."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    iput v5, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->q:I

    :cond_4
    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->q:I

    iget v3, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->p:I

    if-ge v0, v3, :cond_5

    const-string v0, "SelectOpponentsActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Max must be greater than or equal to min. Max: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->q:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Min: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->p:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_0
.end method

.method public final onSearchRequested()Z
    .locals 1

    .prologue
    .line 410
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->S()Z

    move-result v0

    return v0
.end method

.method protected final u()I
    .locals 1

    .prologue
    .line 134
    sget v0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->n:I

    return v0
.end method
