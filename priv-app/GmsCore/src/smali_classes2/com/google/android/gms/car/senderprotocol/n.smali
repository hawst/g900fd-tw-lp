.class final Lcom/google/android/gms/car/senderprotocol/n;
.super Lcom/google/android/gms/car/pf;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/senderprotocol/k;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/senderprotocol/k;Landroid/os/Looper;)V
    .locals 1

    .prologue
    .line 790
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/n;->a:Lcom/google/android/gms/car/senderprotocol/k;

    .line 791
    const/4 v0, 0x0

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/car/pf;-><init>(Ljava/lang/Object;Landroid/os/Looper;)V

    .line 792
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 797
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 798
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    .line 802
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 810
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown message "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 804
    :pswitch_0
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/car/senderprotocol/e;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/senderprotocol/e;->a(Ljava/nio/ByteBuffer;)V

    .line 812
    :goto_0
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 813
    return-void

    .line 807
    :pswitch_1
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/car/senderprotocol/e;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/senderprotocol/e;->b(Ljava/nio/ByteBuffer;)V

    goto :goto_0

    .line 802
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
