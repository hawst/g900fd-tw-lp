.class public final Lcom/google/android/gms/icing/ar;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/android/gms/icing/ar;


# instance fields
.field public a:[I

.field public b:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5780
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 5781
    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/android/gms/icing/ar;->a:[I

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/gms/icing/ar;->b:[B

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/ar;->cachedSize:I

    .line 5782
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/ar;
    .locals 2

    .prologue
    .line 5763
    sget-object v0, Lcom/google/android/gms/icing/ar;->c:[Lcom/google/android/gms/icing/ar;

    if-nez v0, :cond_1

    .line 5764
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 5766
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/ar;->c:[Lcom/google/android/gms/icing/ar;

    if-nez v0, :cond_0

    .line 5767
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/ar;

    sput-object v0, Lcom/google/android/gms/icing/ar;->c:[Lcom/google/android/gms/icing/ar;

    .line 5769
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5771
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/ar;->c:[Lcom/google/android/gms/icing/ar;

    return-object v0

    .line 5769
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 5843
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v2

    .line 5844
    iget-object v1, p0, Lcom/google/android/gms/icing/ar;->a:[I

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/icing/ar;->a:[I

    array-length v1, v1

    if-lez v1, :cond_2

    move v1, v0

    .line 5846
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/icing/ar;->a:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 5847
    iget-object v3, p0, Lcom/google/android/gms/icing/ar;->a:[I

    aget v3, v3, v0

    .line 5848
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 5846
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5851
    :cond_0
    add-int v0, v2, v1

    .line 5852
    add-int/lit8 v0, v0, 0x1

    .line 5853
    invoke-static {v1}, Lcom/google/protobuf/nano/b;->e(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 5856
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/icing/ar;->b:[B

    sget-object v2, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_1

    .line 5857
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/ar;->b:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 5860
    :cond_1
    return v0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5793
    if-ne p1, p0, :cond_1

    .line 5807
    :cond_0
    :goto_0
    return v0

    .line 5796
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/ar;

    if-nez v2, :cond_2

    move v0, v1

    .line 5797
    goto :goto_0

    .line 5799
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/ar;

    .line 5800
    iget-object v2, p0, Lcom/google/android/gms/icing/ar;->a:[I

    iget-object v3, p1, Lcom/google/android/gms/icing/ar;->a:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 5802
    goto :goto_0

    .line 5804
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/ar;->b:[B

    iget-object v3, p1, Lcom/google/android/gms/icing/ar;->b:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 5805
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 5812
    iget-object v0, p0, Lcom/google/android/gms/icing/ar;->a:[I

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 5815
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/ar;->b:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    add-int/2addr v0, v1

    .line 5816
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 5757
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/ar;->a:[I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/ar;->a:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/ar;->a:[I

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Lcom/google/android/gms/icing/ar;->a:[I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v2, p0, Lcom/google/android/gms/icing/ar;->a:[I

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_5

    iget-object v4, p0, Lcom/google/android/gms/icing/ar;->a:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/icing/ar;->a:[I

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Lcom/google/android/gms/icing/ar;->a:[I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/ar;->b:[B

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x12 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5822
    iget-object v0, p0, Lcom/google/android/gms/icing/ar;->a:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/ar;->a:[I

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    move v2, v1

    .line 5824
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/icing/ar;->a:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 5825
    iget-object v3, p0, Lcom/google/android/gms/icing/ar;->a:[I

    aget v3, v3, v0

    .line 5826
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 5824
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5829
    :cond_0
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/b;->d(I)V

    .line 5830
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/b;->d(I)V

    .line 5831
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/icing/ar;->a:[I

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 5832
    iget-object v0, p0, Lcom/google/android/gms/icing/ar;->a:[I

    aget v0, v0, v1

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/b;->a(I)V

    .line 5831
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 5835
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/ar;->b:[B

    sget-object v1, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_2

    .line 5836
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/ar;->b:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 5838
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5839
    return-void
.end method
