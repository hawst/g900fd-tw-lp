.class public Lcom/google/android/gms/games/ui/signin/e;
.super Lcom/google/android/gms/games/ui/signin/g;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private b:Ljava/util/ArrayList;

.field private c:Z

.field private d:Z

.field private final e:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/g;-><init>()V

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/e;->b:Ljava/util/ArrayList;

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/e;->d:Z

    .line 53
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/e;->e:Ljava/lang/Object;

    .line 152
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method

.method protected final a(Lcom/google/android/gms/games/i/b;)V
    .locals 3

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/e;->h()V

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    sget v1, Lcom/google/android/gms/j;->gr:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 83
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    new-instance v0, Lcom/google/android/gms/games/ui/signin/f;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/signin/f;-><init>()V

    .line 87
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/games/ui/signin/f;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 88
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/e;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    const-string v2, "connectingDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    .line 89
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x1

    return v0
.end method

.method final c()V
    .locals 3

    .prologue
    .line 108
    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/e;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 110
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/e;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 111
    if-nez v0, :cond_0

    .line 114
    monitor-exit v1

    .line 133
    :goto_0
    return-void

    .line 117
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/gms/games/ui/signin/e;->d:Z

    if-nez v2, :cond_2

    .line 118
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/signin/e;->d:Z

    .line 121
    invoke-super {p0}, Lcom/google/android/gms/games/ui/signin/g;->i()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->p()V

    .line 124
    :cond_1
    const-string v2, "connectingDialog"

    invoke-static {v0, v2}, Lcom/google/android/gms/games/ui/d/a;->dismiss(Landroid/support/v4/app/q;Ljava/lang/String;)V

    .line 129
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/e;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 130
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/e;->b(I)V

    .line 133
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/e;->e:Ljava/lang/Object;

    return-object v0
.end method

.method final e()Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/e;->c:Z

    return v0
.end method

.method final f()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/e;->b:Ljava/util/ArrayList;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 94
    iget-object v2, p0, Lcom/google/android/gms/games/ui/signin/e;->e:Ljava/lang/Object;

    monitor-enter v2

    .line 95
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/e;->c:Z

    .line 96
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Animation;

    .line 98
    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 99
    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 96
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 102
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/e;->c()V

    .line 104
    return-void

    .line 102
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0}, Lcom/google/android/gms/games/ui/signin/g;->onPause()V

    .line 63
    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/e;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 65
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/e;->c:Z

    .line 66
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
