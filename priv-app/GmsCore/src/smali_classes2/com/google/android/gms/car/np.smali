.class public abstract Lcom/google/android/gms/car/np;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Z

.field private volatile b:Z

.field private c:Ljava/lang/Thread;

.field private d:Lcom/google/android/gms/car/ns;

.field final g:Z

.field h:Lcom/google/android/gms/car/nr;

.field volatile i:Z

.field protected j:Landroid/media/MediaCodec;


# direct methods
.method protected constructor <init>(ZZLcom/google/android/gms/car/ns;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-boolean v0, p0, Lcom/google/android/gms/car/np;->i:Z

    .line 31
    iput-boolean v0, p0, Lcom/google/android/gms/car/np;->b:Z

    .line 70
    iput-boolean p1, p0, Lcom/google/android/gms/car/np;->g:Z

    .line 71
    iput-boolean p2, p0, Lcom/google/android/gms/car/np;->a:Z

    .line 72
    iput-object p3, p0, Lcom/google/android/gms/car/np;->d:Lcom/google/android/gms/car/ns;

    .line 73
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/np;)V
    .locals 12

    .prologue
    const/4 v9, 0x3

    const/4 v2, 0x0

    .line 23
    const-string v0, "CAR.MEDIA"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CAR.MEDIA"

    const-string v1, "doEncoding"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/np;->j:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    new-instance v8, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v8}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    move-object v7, v0

    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/np;->i:Z

    if-eqz v0, :cond_e

    iget-boolean v0, p0, Lcom/google/android/gms/car/np;->b:Z

    if-eqz v0, :cond_4

    const-string v0, "CAR.MEDIA"

    invoke-static {v0, v9}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "CAR.MEDIA"

    const-string v1, "quitting encoding"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/car/np;->a:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/car/np;->j:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->signalEndOfInputStream()V

    :cond_3
    :goto_1
    iput-boolean v2, p0, Lcom/google/android/gms/car/np;->b:Z

    :cond_4
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    iget-object v0, p0, Lcom/google/android/gms/car/np;->j:Landroid/media/MediaCodec;

    const-wide/32 v4, 0x7a120

    invoke-virtual {v0, v8, v4, v5}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v0

    sget-object v1, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    const/4 v1, -0x3

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/car/np;->j:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    move-object v7, v0

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/car/np;->j:Landroid/media/MediaCodec;

    const-wide/16 v4, -0x1

    invoke-virtual {v0, v4, v5}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v1

    if-ltz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/car/np;->j:Landroid/media/MediaCodec;

    const-wide/16 v4, 0x0

    const/4 v6, 0x4

    move v3, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    goto :goto_1

    :cond_6
    const/4 v1, -0x2

    if-ne v0, v1, :cond_8

    const-string v0, "CAR.MEDIA"

    invoke-static {v0, v9}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "CAR.MEDIA"

    const-string v1, "MediaCodec format change"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/car/np;->d:Lcom/google/android/gms/car/ns;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/np;->j:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/car/np;->g:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/car/np;->d:Lcom/google/android/gms/car/ns;

    iget-object v3, v1, Lcom/google/android/gms/car/ns;->a:Landroid/media/MediaMuxer;

    invoke-virtual {v3, v0}, Landroid/media/MediaMuxer;->addTrack(Landroid/media/MediaFormat;)I

    move-result v0

    iput v0, v1, Lcom/google/android/gms/car/ns;->b:I

    goto :goto_0

    :cond_8
    if-gez v0, :cond_9

    new-instance v1, Ljava/lang/Exception;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "dequeueOutputBuffer returned "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    aget-object v1, v7, v0

    iget v3, v8, Landroid/media/MediaCodec$BufferInfo;->offset:I

    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget v3, v8, Landroid/media/MediaCodec$BufferInfo;->offset:I

    iget v4, v8, Landroid/media/MediaCodec$BufferInfo;->size:I

    add-int/2addr v3, v4

    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    iget v3, v8, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_c

    const-string v3, "CAR.MEDIA"

    invoke-static {v3, v9}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_a

    const-string v3, "CAR.MEDIA"

    const-string v4, "codec config"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    iget-object v3, p0, Lcom/google/android/gms/car/np;->h:Lcom/google/android/gms/car/nr;

    invoke-interface {v3, v1, v8}, Lcom/google/android/gms/car/nr;->a(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    :cond_b
    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/car/np;->j:Landroid/media/MediaCodec;

    invoke-virtual {v1, v0, v2}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    goto/16 :goto_0

    :cond_c
    iget v3, v8, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v3, v3, 0x4

    if-eqz v3, :cond_d

    iput-boolean v2, p0, Lcom/google/android/gms/car/np;->i:Z

    iget-object v1, p0, Lcom/google/android/gms/car/np;->h:Lcom/google/android/gms/car/nr;

    goto :goto_2

    :cond_d
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    const-wide/16 v10, 0x3e8

    mul-long/2addr v4, v10

    iput-wide v4, v8, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iget-object v3, p0, Lcom/google/android/gms/car/np;->h:Lcom/google/android/gms/car/nr;

    invoke-interface {v3, v1, v8}, Lcom/google/android/gms/car/nr;->b(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    iget-object v3, p0, Lcom/google/android/gms/car/np;->d:Lcom/google/android/gms/car/ns;

    if-eqz v3, :cond_b

    iget v3, v8, Landroid/media/MediaCodec$BufferInfo;->size:I

    if-eqz v3, :cond_b

    iget-boolean v3, p0, Lcom/google/android/gms/car/np;->g:Z

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/google/android/gms/car/np;->d:Lcom/google/android/gms/car/ns;

    invoke-virtual {v3, v1, v8}, Lcom/google/android/gms/car/ns;->a(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    goto :goto_2

    :cond_e
    return-void
.end method


# virtual methods
.method protected abstract a()Landroid/media/MediaCodec;
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/car/nr;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 169
    monitor-enter p0

    :try_start_0
    const-string v2, "CAR.MEDIA"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 170
    const-string v2, "CAR.MEDIA"

    const-string v3, "startEncoding"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/car/np;->h:Lcom/google/android/gms/car/nr;

    .line 173
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/car/np;->i:Z

    .line 174
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/gms/car/np;->b:Z

    .line 175
    new-instance v3, Ljava/util/concurrent/Semaphore;

    const/4 v2, 0x0

    invoke-direct {v3, v2}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 176
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/google/android/gms/car/nq;

    invoke-direct {v5, p0, v3}, Lcom/google/android/gms/car/nq;-><init>(Lcom/google/android/gms/car/np;Ljava/util/concurrent/Semaphore;)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v2, p0, Lcom/google/android/gms/car/np;->g:Z

    if-eqz v2, :cond_3

    const-string v2, "Video"

    :goto_0
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "EncodingThread"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v5, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/google/android/gms/car/np;->c:Ljava/lang/Thread;

    .line 204
    iget-object v2, p0, Lcom/google/android/gms/car/np;->c:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 206
    :try_start_1
    const-string v2, "CAR.MEDIA"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 207
    const-string v2, "CAR.MEDIA"

    const-string v4, "wait for encoder init"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    move v2, v0

    .line 211
    :cond_2
    const-wide/16 v4, 0x1f4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4, v5, v6}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 212
    iget-object v4, p0, Lcom/google/android/gms/car/np;->c:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Thread;->isAlive()Z

    move-result v4

    if-nez v4, :cond_4

    .line 215
    const-string v1, "CAR.MEDIA"

    const-string v2, "encoding thread dead while starting"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 232
    :goto_1
    monitor-exit p0

    return v0

    .line 176
    :cond_3
    :try_start_2
    const-string v2, "Audio"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 219
    :cond_4
    add-int/lit8 v2, v2, 0x1

    .line 220
    const/16 v4, 0xa

    if-le v2, v4, :cond_2

    .line 221
    :try_start_3
    const-string v1, "CAR.MEDIA"

    const-string v2, "failed to start encoding with timeout"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 228
    :catch_0
    move-exception v1

    .line 229
    :try_start_4
    const-string v2, "CAR.MEDIA"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "InterruptedException while starting encoding:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 225
    :cond_5
    :try_start_5
    const-string v2, "CAR.MEDIA"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 226
    const-string v2, "CAR.MEDIA"

    const-string v3, "encoder init done"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_6
    move v0, v1

    .line 232
    goto :goto_1
.end method

.method public declared-synchronized b()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 237
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.MEDIA"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    const-string v0, "CAR.MEDIA"

    const-string v1, "stopEncoding"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/np;->b:Z

    .line 241
    iget-object v0, p0, Lcom/google/android/gms/car/np;->c:Ljava/lang/Thread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 243
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/np;->c:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 247
    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/google/android/gms/car/np;->c:Ljava/lang/Thread;

    .line 249
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/np;->j:Landroid/media/MediaCodec;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_2

    .line 251
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/car/np;->j:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 255
    :goto_1
    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/car/np;->j:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V

    .line 256
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/np;->j:Landroid/media/MediaCodec;

    .line 258
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/np;->d:Lcom/google/android/gms/car/ns;

    if-eqz v0, :cond_3

    .line 259
    iget-object v0, p0, Lcom/google/android/gms/car/np;->d:Lcom/google/android/gms/car/ns;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ns;->a()V

    .line 260
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/np;->d:Lcom/google/android/gms/car/ns;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 262
    :cond_3
    monitor-exit p0

    return-void

    .line 237
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0
.end method
