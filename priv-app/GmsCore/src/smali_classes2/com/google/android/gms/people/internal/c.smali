.class public interface abstract Lcom/google/android/gms/people/internal/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 81
    const/16 v0, 0x31

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "owner_account"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "owner_page_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "directory_account"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "directory_account_type"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "person_key"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "people_v2_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "data_source"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "container_type"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "profile_type"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "gaia_id"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "avatar_source"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "avatar_location"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "nickname"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "primary_affinity_sorted"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "primary_logging_id_sorted"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "person_affinity_sorted"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "person_affinity1"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "person_affinity2"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "person_affinity3"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "person_affinity4"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "person_affinity5"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "person_logging_id_sorted"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "person_logging_id1"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "person_logging_id2"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "person_logging_id3"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "person_logging_id4"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "person_logging_id5"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "item_affinity_sorted"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "item_affinity1"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "item_affinity2"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "item_affinity3"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "item_affinity4"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "item_affinity5"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "item_logging_id_sorted"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "item_logging_id1"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "item_logging_id2"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "item_logging_id3"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "item_logging_id4"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "item_logging_id5"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "item_type"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "match_type"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "value"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "value2"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "value_type"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "custom_label"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "cp2_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "cp2_data_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/people/internal/c;->a:[Ljava/lang/String;

    return-void
.end method
