.class final Lcom/google/android/gms/games/ui/ba;
.super Lcom/google/android/gms/games/ui/b;
.source "SourceFile"


# instance fields
.field final synthetic d:Lcom/google/android/gms/games/ui/aj;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/aj;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 929
    iput-object p1, p0, Lcom/google/android/gms/games/ui/ba;->d:Lcom/google/android/gms/games/ui/aj;

    .line 930
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/ui/b;-><init>(Landroid/content/Context;)V

    .line 931
    return-void
.end method


# virtual methods
.method public final synthetic a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 927
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->be:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Landroid/view/View;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 927
    check-cast p2, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    if-eqz p2, :cond_0

    sget v0, Lcom/google/android/gms/j;->aH:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-interface {p2}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->a()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->s_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v0, Lcom/google/android/gms/j;->aG:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {p2}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->a()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v1

    sget v2, Lcom/google/android/gms/h;->af:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    :cond_0
    return-void
.end method
