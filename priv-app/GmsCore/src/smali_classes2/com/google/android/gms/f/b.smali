.class public final Lcom/google/android/gms/f/b;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(Ljava/lang/Exception;)I
    .locals 6

    .prologue
    const/16 v2, 0x1f

    const/16 v1, 0xa

    const/16 v4, 0xd2

    const/4 v0, 0x0

    const/16 v3, 0xfe

    .line 562
    instance-of v5, p0, Lcom/google/android/gms/auth/s;

    if-eqz v5, :cond_1

    .line 563
    check-cast p0, Lcom/google/android/gms/auth/s;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/s;->a()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    move v0, v2

    .line 603
    :cond_0
    :goto_0
    :pswitch_1
    return v0

    :pswitch_2
    move v0, v1

    .line 563
    goto :goto_0

    :pswitch_3
    const/16 v0, 0x78

    goto :goto_0

    :pswitch_4
    const/16 v0, 0x37

    goto :goto_0

    :pswitch_5
    const/16 v0, 0xc7

    goto :goto_0

    :pswitch_6
    move v0, v2

    goto :goto_0

    :pswitch_7
    const/16 v0, 0x19

    goto :goto_0

    :pswitch_8
    const/16 v0, 0x46

    goto :goto_0

    :pswitch_9
    move v0, v3

    goto :goto_0

    :pswitch_a
    const/16 v0, 0xcf

    goto :goto_0

    :pswitch_b
    move v0, v3

    goto :goto_0

    .line 568
    :cond_1
    instance-of v2, p0, Lcom/google/android/gms/auth/ae;

    if-eqz v2, :cond_2

    .line 570
    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/f/b;->a(Ljava/lang/String;Z)I

    move-result v0

    goto :goto_0

    .line 571
    :cond_2
    instance-of v2, p0, Ljava/io/IOException;

    if-eqz v2, :cond_5

    .line 573
    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 574
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    .line 575
    const-string v5, "Could not bind to service"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v4

    .line 576
    goto :goto_0

    .line 578
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/gms/f/b;->a(Ljava/lang/String;Z)I

    move-result v0

    .line 582
    if-ne v0, v3, :cond_0

    :cond_4
    move v0, v1

    .line 587
    goto :goto_0

    .line 588
    :cond_5
    instance-of v1, p0, Lcom/google/android/gms/auth/q;

    if-eqz v1, :cond_8

    .line 589
    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 590
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 591
    :cond_6
    const/16 v0, 0xc6

    goto :goto_0

    .line 593
    :cond_7
    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/f/b;->a(Ljava/lang/String;Z)I

    move-result v0

    goto :goto_0

    .line 594
    :cond_8
    instance-of v0, p0, Landroid/accounts/AuthenticatorException;

    if-eqz v0, :cond_9

    move v0, v4

    .line 595
    goto :goto_0

    .line 596
    :cond_9
    instance-of v0, p0, Ljava/lang/SecurityException;

    if-eqz v0, :cond_a

    move v0, v4

    .line 597
    goto :goto_0

    .line 598
    :cond_a
    instance-of v0, p0, Landroid/accounts/OperationCanceledException;

    if-eqz v0, :cond_b

    .line 599
    const/16 v0, 0x78

    goto :goto_0

    .line 600
    :cond_b
    instance-of v0, p0, Ljava/lang/IllegalStateException;

    if-eqz v0, :cond_c

    .line 601
    const/16 v0, 0xcd

    goto/16 :goto_0

    :cond_c
    move v0, v3

    .line 603
    goto/16 :goto_0

    .line 563
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_a
        :pswitch_4
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_2
        :pswitch_9
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method private static a(Ljava/lang/String;Z)I
    .locals 3

    .prologue
    const/16 v0, 0x1f

    .line 518
    const/4 v1, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 545
    if-eqz p1, :cond_1

    :goto_1
    :pswitch_0
    return v0

    .line 518
    :sswitch_0
    const-string v2, "Interrupted"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "UserCancel"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "Timeout"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "BadAuthentication"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v2, "NetworkError"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_5
    const-string v2, "CaptchaRequired"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_6
    const-string v2, "AppDownloadRequired"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x6

    goto :goto_0

    :sswitch_7
    const-string v2, "NeedPermission"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x7

    goto :goto_0

    :sswitch_8
    const-string v2, "DeviceManagementRequiredOrSyncDisabled"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x8

    goto :goto_0

    .line 524
    :pswitch_1
    const/16 v0, 0x78

    goto :goto_1

    .line 528
    :pswitch_2
    const/16 v0, 0x19

    goto :goto_1

    .line 530
    :pswitch_3
    const/16 v0, 0xa

    goto :goto_1

    .line 534
    :pswitch_4
    const/16 v0, 0xcf

    goto :goto_1

    .line 536
    :pswitch_5
    const/16 v0, 0xc7

    goto :goto_1

    .line 538
    :pswitch_6
    const/16 v0, 0x46

    goto :goto_1

    .line 545
    :cond_1
    const/16 v0, 0xfe

    goto :goto_1

    .line 518
    :sswitch_data_0
    .sparse-switch
        -0x46a361bb -> :sswitch_1
        -0x24174fe7 -> :sswitch_5
        -0x10ad29de -> :sswitch_0
        -0xfacc00e -> :sswitch_8
        0x733089a -> :sswitch_4
        0x12c47468 -> :sswitch_6
        0x14e7e541 -> :sswitch_2
        0x3bb47dbd -> :sswitch_3
        0x75a8b325 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private static a(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/gms/f/a/d;)V
    .locals 4

    .prologue
    .line 305
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, Lcom/google/android/gms/f/a/d;->b:Ljava/lang/Integer;

    .line 306
    :try_start_0
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v1, "oauth2: email"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {p0, v0, v1, v2, v3}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Boolean;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 319
    const/16 v1, 0x8

    :try_start_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, Lcom/google/android/gms/f/a/d;->b:Ljava/lang/Integer;

    .line 320
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 321
    :cond_0
    const/16 v0, 0xd

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, Lcom/google/android/gms/f/a/d;->b:Ljava/lang/Integer;

    .line 322
    const/16 v0, 0xe6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, Lcom/google/android/gms/f/a/d;->a:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 335
    :goto_0
    return-void

    .line 310
    :catch_0
    move-exception v0

    .line 314
    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, Lcom/google/android/gms/f/a/d;->b:Ljava/lang/Integer;

    .line 315
    invoke-static {v0}, Lcom/google/android/gms/f/b;->a(Ljava/lang/Exception;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, Lcom/google/android/gms/f/a/d;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 324
    :cond_1
    const/16 v0, 0xf

    :try_start_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, Lcom/google/android/gms/f/a/d;->b:Ljava/lang/Integer;

    .line 325
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, Lcom/google/android/gms/f/a/d;->a:Ljava/lang/Integer;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 327
    :catch_1
    move-exception v0

    .line 333
    invoke-static {v0}, Lcom/google/android/gms/f/b;->a(Ljava/lang/Exception;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, Lcom/google/android/gms/f/a/d;->a:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/accounts/AccountManager;Ljava/lang/String;Lcom/google/android/gms/f/a/b;)V
    .locals 11

    .prologue
    .line 269
    invoke-virtual {p1, p2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v5

    .line 270
    array-length v0, v5

    if-nez v0, :cond_0

    .line 282
    :goto_0
    return-void

    .line 274
    :cond_0
    iget-object v0, p3, Lcom/google/android/gms/f/a/b;->a:[Lcom/google/android/gms/f/a/c;

    iget-object v1, p3, Lcom/google/android/gms/f/a/b;->a:[Lcom/google/android/gms/f/a/c;

    array-length v1, v1

    array-length v2, v5

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/f/a/c;

    .line 277
    iget-object v1, p3, Lcom/google/android/gms/f/a/b;->a:[Lcom/google/android/gms/f/a/c;

    array-length v2, v1

    .line 278
    array-length v6, v5

    const/4 v1, 0x0

    move v3, v2

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_3

    aget-object v7, v5, v2

    .line 279
    add-int/lit8 v4, v3, 0x1

    new-instance v8, Lcom/google/android/gms/f/a/c;

    invoke-direct {v8}, Lcom/google/android/gms/f/a/c;-><init>()V

    new-instance v1, Lcom/google/android/gms/f/a/d;

    invoke-direct {v1}, Lcom/google/android/gms/f/a/d;-><init>()V

    iput-object v1, v8, Lcom/google/android/gms/f/a/c;->a:Lcom/google/android/gms/f/a/d;

    iget-object v1, v8, Lcom/google/android/gms/f/a/c;->a:Lcom/google/android/gms/f/a/d;

    const/16 v9, 0xfe

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    iput-object v9, v1, Lcom/google/android/gms/f/a/d;->a:Ljava/lang/Integer;

    iget-object v9, v7, Landroid/accounts/Account;->type:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_1
    :goto_2
    packed-switch v1, :pswitch_data_0

    const/16 v1, 0xf

    :goto_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v8, Lcom/google/android/gms/f/a/c;->c:Ljava/lang/Integer;

    iget-object v1, v8, Lcom/google/android/gms/f/a/c;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v9, 0xf

    if-ne v1, v9, :cond_2

    iget-object v1, v7, Landroid/accounts/Account;->type:Ljava/lang/String;

    iput-object v1, v8, Lcom/google/android/gms/f/a/c;->d:Ljava/lang/String;

    :cond_2
    iget-object v1, v8, Lcom/google/android/gms/f/a/c;->a:Lcom/google/android/gms/f/a/d;

    invoke-static {p0, v7, v1}, Lcom/google/android/gms/f/b;->a(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/gms/f/a/d;)V

    aput-object v8, v0, v3

    .line 278
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v3, v4

    goto :goto_1

    .line 279
    :sswitch_0
    const-string v10, "com.google"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v1, 0x0

    goto :goto_2

    :sswitch_1
    const-string v10, "com.sidewinder"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v1, 0x1

    goto :goto_2

    :pswitch_0
    const/4 v1, 0x0

    goto :goto_3

    :pswitch_1
    const/4 v1, 0x1

    goto :goto_3

    .line 281
    :cond_3
    iput-object v0, p3, Lcom/google/android/gms/f/a/b;->a:[Lcom/google/android/gms/f/a/c;

    goto :goto_0

    .line 279
    :sswitch_data_0
    .sparse-switch
        -0x63c992a7 -> :sswitch_1
        0x3464ff46 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
