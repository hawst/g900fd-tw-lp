.class public final Lcom/google/android/gms/games/realtime/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public c:Z

.field public d:I

.field public e:I

.field public final f:Ljava/util/HashMap;

.field public final g:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/realtime/b;->f:Ljava/util/HashMap;

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/realtime/b;->g:Ljava/util/HashMap;

    .line 42
    invoke-interface {p2, p1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->b_(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 43
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 44
    invoke-interface {p2}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->l()Ljava/util/ArrayList;

    move-result-object v9

    .line 45
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v10

    move v6, v4

    :goto_0
    if-ge v6, v10, :cond_0

    .line 46
    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/gms/games/multiplayer/Participant;

    .line 47
    new-instance v0, Lcom/google/android/gms/games/realtime/c;

    invoke-interface {v5}, Lcom/google/android/gms/games/multiplayer/Participant;->l()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5}, Lcom/google/android/gms/games/multiplayer/Participant;->A_()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5}, Lcom/google/android/gms/games/multiplayer/Participant;->a()I

    move-result v3

    invoke-interface {v5}, Lcom/google/android/gms/games/multiplayer/Participant;->e()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/realtime/c;-><init>(Ljava/lang/String;Ljava/lang/String;IZI)V

    .line 53
    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 56
    :cond_0
    invoke-interface {p2}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/realtime/b;->a:Ljava/lang/String;

    .line 57
    iput-object v7, p0, Lcom/google/android/gms/games/realtime/b;->b:Ljava/lang/String;

    .line 58
    invoke-interface {p2}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/realtime/b;->d:I

    .line 59
    iput-boolean v4, p0, Lcom/google/android/gms/games/realtime/b;->c:Z

    .line 60
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/realtime/b;->e:I

    .line 62
    invoke-direct {p0, v8}, Lcom/google/android/gms/games/realtime/b;->a(Ljava/util/ArrayList;)V

    .line 63
    return-void
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 5

    .prologue
    .line 76
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 77
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/realtime/c;

    .line 78
    iget-object v3, v0, Lcom/google/android/gms/games/realtime/c;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/games/realtime/b;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 79
    iget-object v3, p0, Lcom/google/android/gms/games/realtime/b;->f:Ljava/util/HashMap;

    iget-object v4, v0, Lcom/google/android/gms/games/realtime/c;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    iget-object v3, v0, Lcom/google/android/gms/games/realtime/c;->b:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 81
    iget-object v3, p0, Lcom/google/android/gms/games/realtime/b;->g:Ljava/util/HashMap;

    iget-object v4, v0, Lcom/google/android/gms/games/realtime/c;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 85
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/b;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/realtime/c;

    .line 101
    if-eqz v0, :cond_0

    .line 102
    iget-object v0, v0, Lcom/google/android/gms/games/realtime/c;->a:Ljava/lang/String;

    .line 104
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/util/ArrayList;
    .locals 2

    .prologue
    .line 96
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/b;->f:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/b;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/realtime/c;

    .line 109
    if-nez v0, :cond_0

    .line 111
    const-string v0, "RoomData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received bad participant ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const/4 v0, 0x0

    .line 114
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/games/realtime/c;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/b;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d(Ljava/lang/String;)Lcom/google/android/gms/games/realtime/c;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/b;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/realtime/c;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)Lcom/google/android/gms/games/realtime/c;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/b;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/realtime/c;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 183
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/b;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/realtime/c;

    .line 185
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v3

    const-string v4, "participant"

    invoke-virtual {v0}, Lcom/google/android/gms/games/realtime/c;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/bv;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 190
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v2, "mRoomId"

    iget-object v3, p0, Lcom/google/android/gms/games/realtime/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v2, "mCurrentParticipantId"

    iget-object v3, p0, Lcom/google/android/gms/games/realtime/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v2, "mRoomStatus"

    iget v3, p0, Lcom/google/android/gms/games/realtime/b;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v2, "mCurrentParticipantInConnectedSet"

    iget-boolean v3, p0, Lcom/google/android/gms/games/realtime/b;->c:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v2, "mStatusVersion"

    iget v3, p0, Lcom/google/android/gms/games/realtime/b;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v2, "mParticipants"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/bv;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
