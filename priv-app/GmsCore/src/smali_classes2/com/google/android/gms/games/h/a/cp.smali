.class public final Lcom/google/android/gms/games/h/a/cp;
.super Lcom/google/android/gms/common/server/response/c;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/HashMap;


# instance fields
.field private final c:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 50
    sput-object v0, Lcom/google/android/gms/games/h/a/cp;->b:Ljava/util/HashMap;

    const-string v1, "connectedTimestampMillis"

    const-string v2, "connectedTimestampMillis"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->c(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/google/android/gms/games/h/a/cp;->b:Ljava/util/HashMap;

    const-string v1, "participantId"

    const-string v2, "participantId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/google/android/gms/games/h/a/cp;->b:Ljava/util/HashMap;

    const-string v1, "reliableChannel"

    const-string v2, "reliableChannel"

    const-class v3, Lcom/google/android/gms/games/h/a/co;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/google/android/gms/games/h/a/cp;->b:Ljava/util/HashMap;

    const-string v1, "unreliableChannel"

    const-string v2, "unreliableChannel"

    const-class v3, Lcom/google/android/gms/games/h/a/co;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 87
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/h/a/cp;->c:Ljava/util/HashMap;

    .line 65
    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/String;Lcom/google/android/gms/games/h/a/co;Lcom/google/android/gms/games/h/a/co;)V
    .locals 4

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 87
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/h/a/cp;->c:Ljava/util/HashMap;

    .line 73
    if-eqz p1, :cond_0

    .line 74
    const-string v0, "connectedTimestampMillis"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/games/h/a/cp;->a(Ljava/lang/String;J)V

    .line 76
    :cond_0
    if-eqz p2, :cond_1

    .line 77
    const-string v0, "participantId"

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/games/h/a/cp;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    :cond_1
    if-eqz p3, :cond_2

    .line 80
    const-string v0, "reliableChannel"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/games/h/a/cp;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    .line 82
    :cond_2
    if-eqz p4, :cond_3

    .line 83
    const-string v0, "unreliableChannel"

    invoke-virtual {p0, v0, p4}, Lcom/google/android/gms/games/h/a/cp;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    .line 85
    :cond_3
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/google/android/gms/games/h/a/cp;->b:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/cp;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    return-void
.end method

.method protected final d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/cp;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final getReliableChannel()Lcom/google/android/gms/games/h/a/co;
    .locals 2
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/cp;->c:Ljava/util/HashMap;

    const-string v1, "reliableChannel"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/co;

    return-object v0
.end method

.method public final getUnreliableChannel()Lcom/google/android/gms/games/h/a/co;
    .locals 2
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/cp;->c:Ljava/util/HashMap;

    const-string v1, "unreliableChannel"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/co;

    return-object v0
.end method
