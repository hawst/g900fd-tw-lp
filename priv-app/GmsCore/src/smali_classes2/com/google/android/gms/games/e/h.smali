.class public final Lcom/google/android/gms/games/e/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:I

.field d:I

.field e:I

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/google/android/gms/games/e/h;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/games/e/g;
    .locals 2

    .prologue
    .line 154
    new-instance v0, Lcom/google/android/gms/games/e/g;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/games/e/g;-><init>(Lcom/google/android/gms/games/e/h;B)V

    return-object v0
.end method

.method public final a(I)Lcom/google/android/gms/games/e/h;
    .locals 1

    .prologue
    .line 123
    invoke-static {p1}, Lcom/google/android/gms/games/internal/b/i;->b(I)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 124
    iput p1, p0, Lcom/google/android/gms/games/e/h;->c:I

    .line 125
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/games/e/h;
    .locals 1

    .prologue
    .line 113
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/e/h;->a:Ljava/lang/String;

    .line 114
    return-object p0
.end method

.method public final b(I)Lcom/google/android/gms/games/e/h;
    .locals 1

    .prologue
    .line 129
    invoke-static {p1}, Lcom/google/android/gms/games/internal/b/c;->b(I)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 130
    iput p1, p0, Lcom/google/android/gms/games/e/h;->d:I

    .line 131
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/games/e/h;
    .locals 1

    .prologue
    .line 118
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/e/h;->b:Ljava/lang/String;

    .line 119
    return-object p0
.end method

.method public final c(I)Lcom/google/android/gms/games/e/h;
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 135
    if-eqz p1, :cond_0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 137
    iput p1, p0, Lcom/google/android/gms/games/e/h;->e:I

    .line 138
    return-object p0

    .line 135
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
