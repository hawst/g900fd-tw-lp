.class public final Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final a:I

.field private static final b:Ljava/util/HashMap;


# instance fields
.field private c:Landroid/widget/ListView;

.field private d:I

.field private e:Landroid/view/View;

.field private f:Landroid/widget/CheckBox;

.field private g:Landroid/view/View;

.field private h:Landroid/widget/CheckBox;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    sget v0, Lcom/google/android/gms/l;->bH:I

    sput v0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->a:I

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->b:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    .line 471
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 459
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.gservices.intent.action.GSERVICES_OVERRIDE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 460
    sget-object v1, Lcom/google/android/gms/games/c/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 462
    sget-object v1, Lcom/google/android/gms/common/a/b;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 464
    sput-boolean p1, Lcom/android/volley/ad;->b:Z

    .line 465
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 466
    return-void
.end method

.method public static a()Z
    .locals 2

    .prologue
    .line 260
    const-string v0, "Prod"

    invoke-static {}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static b()Ljava/lang/String;
    .locals 5

    .prologue
    .line 270
    invoke-static {}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->c()V

    .line 272
    new-instance v4, Lcom/google/android/gms/games/ui/ai;

    sget-object v0, Lcom/google/android/gms/games/c/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/games/c/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/games/c/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/drive/ai;->T:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/ai;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    sget-object v0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 280
    sget-object v1, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/ui/ai;

    .line 281
    if-eqz v1, :cond_0

    invoke-virtual {v1, v4}, Lcom/google/android/gms/games/ui/ai;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 282
    const-string v1, "DebugSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Found current selected server as "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :goto_0
    return-object v0

    .line 287
    :cond_1
    const-string v0, "DebugSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Current selected server descriptor could not be found: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c()V
    .locals 5

    .prologue
    .line 231
    sget-object v0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 254
    :goto_0
    return-void

    .line 238
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/ai;

    const-string v1, "https://www-googleapis-staging.sandbox.google.com"

    const-string v2, "vdev"

    const-string v3, "vwhitelisteddev"

    const-string v4, "https://www-googleapis-test.sandbox.google.com"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/games/ui/ai;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    sget-object v1, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->b:Ljava/util/HashMap;

    const-string v2, "Dev"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    const-string v1, "DebugSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SERVER_MAP[Dev] => "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    new-instance v0, Lcom/google/android/gms/games/ui/ai;

    const-string v1, "https://www-googleapis-staging.sandbox.google.com"

    const-string v2, "v1"

    const-string v3, "v1whitelisted"

    const-string v4, "https://www-googleapis-test.sandbox.google.com"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/games/ui/ai;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    sget-object v1, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->b:Ljava/util/HashMap;

    const-string v2, "Staging"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    const-string v1, "DebugSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SERVER_MAP[Staging] => "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    new-instance v0, Lcom/google/android/gms/games/ui/ai;

    const-string v1, "https://www.googleapis.com"

    const-string v2, "v1"

    const-string v3, "v1whitelisted"

    const-string v4, "https://www.googleapis.com"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/games/ui/ai;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    sget-object v1, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->b:Ljava/util/HashMap;

    const-string v2, "Prod"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    const-string v1, "DebugSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SERVER_MAP[Prod] => "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static d()Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 349
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 350
    sget-object v0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 351
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 353
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 304
    sget-object v0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/ai;

    .line 305
    if-nez v0, :cond_0

    .line 306
    const-string v0, "DebugSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not find server \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' in SERVER_MAP"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 337
    :goto_0
    return v0

    .line 311
    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->c:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v4

    .line 312
    invoke-interface {v4}, Landroid/widget/ListAdapter;->getCount()I

    move-result v5

    move v3, v1

    :goto_1
    if-ge v3, v5, :cond_3

    .line 313
    invoke-interface {v4, v3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 314
    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 316
    iget v4, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->d:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1

    .line 317
    iget-object v4, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->c:Landroid/widget/ListView;

    iget v5, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->d:I

    invoke-virtual {v4, v5, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 319
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->c:Landroid/widget/ListView;

    invoke-virtual {v1, v3, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 320
    iget-object v1, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->c:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setSelection(I)V

    .line 321
    iput v3, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->d:I

    .line 322
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.google.gservices.intent.action.GSERVICES_OVERRIDE"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 323
    sget-object v3, Lcom/google/android/gms/games/c/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/gms/games/ui/ai;->a:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 324
    sget-object v3, Lcom/google/android/gms/games/c/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/gms/games/ui/ai;->b:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 325
    sget-object v3, Lcom/google/android/gms/games/c/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/gms/games/ui/ai;->c:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 326
    sget-object v3, Lcom/google/android/gms/appstate/b/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/gms/games/ui/ai;->a:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 328
    sget-object v3, Lcom/google/android/gms/drive/ai;->T:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/gms/games/ui/ai;->d:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 330
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 331
    const-string v1, "DebugSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Sent new server description: {baseServerUrl = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Lcom/google/android/gms/games/ui/ai;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", serverVersion = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/gms/games/ui/ai;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", internalServerVersion = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/gms/games/ui/ai;->c:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "}"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 334
    goto/16 :goto_0

    .line 312
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_3
    move v0, v1

    .line 337
    goto/16 :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 410
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->e:Landroid/view/View;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->f:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1

    .line 412
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    .line 413
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.gservices.intent.action.GSERVICES_OVERRIDE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 414
    sget-object v1, Lcom/google/android/gms/games/c/a;->q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->f:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 416
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 426
    :cond_0
    :goto_0
    return-void

    .line 417
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->g:Landroid/view/View;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->h:Landroid/widget/CheckBox;

    if-eqz v0, :cond_2

    .line 419
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    .line 420
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->a(Z)V

    goto :goto_0

    .line 421
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->gV:I

    if-ne v0, v1, :cond_3

    .line 422
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.google.android.gms"

    const-string v3, "com.google.android.location.copresence.debug.DebugUIActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "Cannot launch copresence debug - do you have GmsCore-internal?"

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 423
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->gU:I

    if-ne v0, v1, :cond_0

    .line 424
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.CLEAR_DATA"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.games.CLEAR_TRANSIENT_DATA"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 86
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 89
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {v0, v3}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    .line 90
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->finish()V

    .line 111
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 89
    goto :goto_0

    .line 94
    :cond_1
    sget v0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->setContentView(I)V

    .line 95
    invoke-static {}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->c()V

    .line 98
    sget v0, Lcom/google/android/gms/j;->gX:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->e:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->e:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->e:Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v3, Lcom/google/android/gms/p;->lp:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->e:Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->ui:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->e:Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->cE:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->f:Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->f:Landroid/widget/CheckBox;

    sget-object v0, Lcom/google/android/gms/games/c/a;->q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 101
    sget v0, Lcom/google/android/gms/j;->gW:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v3, "Available Servers"

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->d:I

    sget v0, Lcom/google/android/gms/j;->rm:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->c:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->c:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->c:Landroid/widget/ListView;

    new-instance v3, Landroid/widget/ArrayAdapter;

    const v4, 0x109000f

    invoke-static {}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->d()Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct {v3, p0, v4, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-static {}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->a(Ljava/lang/CharSequence;)Z

    .line 104
    sget v0, Lcom/google/android/gms/j;->gY:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->g:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->g:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->g:Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v3, Lcom/google/android/gms/p;->lu:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->g:Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->ui:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    sget-object v0, Lcom/google/android/gms/games/c/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/gms/common/a/b;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/android/volley/ad;->b:Z

    if-eqz v0, :cond_2

    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->g:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->cE:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->h:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->a(Z)V

    .line 107
    sget v0, Lcom/google/android/gms/j;->gV:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/p;->lt:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    sget v0, Lcom/google/android/gms/j;->ui:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 110
    sget v0, Lcom/google/android/gms/j;->gU:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/p;->lq:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    sget v0, Lcom/google/android/gms/j;->ui:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    :cond_2
    move v1, v2

    .line 104
    goto :goto_2
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->c:Landroid/widget/ListView;

    if-ne p1, v0, :cond_0

    .line 369
    check-cast p2, Landroid/widget/CheckedTextView;

    invoke-virtual {p2}, Landroid/widget/CheckedTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 370
    sget v1, Lcom/google/android/gms/p;->ls:I

    invoke-static {p0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v1

    .line 372
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 373
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 374
    sget v1, Lcom/google/android/gms/p;->lr:I

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v3, 0x104000a

    new-instance v4, Lcom/google/android/gms/games/ui/ah;

    invoke-direct {v4, p0, v0}, Lcom/google/android/gms/games/ui/ah;-><init>(Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    new-instance v3, Lcom/google/android/gms/games/ui/ag;

    invoke-direct {v3, p0}, Lcom/google/android/gms/games/ui/ag;-><init>(Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;)V

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 394
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 395
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 396
    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;Landroid/app/Dialog;)V

    .line 398
    :cond_0
    return-void
.end method
