.class public final Lcom/google/android/gms/common/a/m;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Landroid/content/Context;


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/gms/common/a/m;->b:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public static a(Landroid/content/SharedPreferences$Editor;)Z
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 85
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-ge v0, v1, :cond_0

    .line 86
    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    .line 89
    :goto_0
    return v0

    .line 88
    :cond_0
    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 89
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/content/SharedPreferences;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    .prologue
    .line 67
    sget-object v0, Lcom/google/android/gms/common/a/m;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/common/a/m;->b:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/gms/common/a/r;
    .locals 6

    .prologue
    .line 136
    new-instance v0, Lcom/google/android/gms/common/a/p;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/a/p;-><init>(Lcom/google/android/gms/common/a/m;Lcom/google/android/gms/common/a/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/r;
    .locals 6

    .prologue
    .line 157
    new-instance v0, Lcom/google/android/gms/common/a/q;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/a/q;-><init>(Lcom/google/android/gms/common/a/m;Lcom/google/android/gms/common/a/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/r;
    .locals 6

    .prologue
    .line 94
    new-instance v0, Lcom/google/android/gms/common/a/n;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/a/n;-><init>(Lcom/google/android/gms/common/a/m;Lcom/google/android/gms/common/a/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/r;
    .locals 6

    .prologue
    .line 115
    new-instance v0, Lcom/google/android/gms/common/a/o;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/a/o;-><init>(Lcom/google/android/gms/common/a/m;Lcom/google/android/gms/common/a/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
