.class final Lcom/google/android/gms/people/internal/ab;
.super Lcom/google/android/gms/people/internal/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 0

    .prologue
    .line 451
    invoke-direct {p0}, Lcom/google/android/gms/people/internal/a;-><init>()V

    .line 452
    iput-object p1, p0, Lcom/google/android/gms/people/internal/ab;->a:Lcom/google/android/gms/common/api/m;

    .line 453
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 4

    .prologue
    .line 457
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 458
    const-string v0, "PeopleClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Circles callback: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nresolution="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nholder="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    :cond_0
    invoke-static {p1, p2}, Lcom/google/android/gms/people/internal/p;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    .line 463
    if-nez p3, :cond_1

    const/4 v0, 0x0

    .line 465
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/people/internal/ab;->a:Lcom/google/android/gms/common/api/m;

    new-instance v3, Lcom/google/android/gms/people/internal/q;

    invoke-direct {v3, v1, v0}, Lcom/google/android/gms/people/internal/q;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/people/model/e;)V

    invoke-interface {v2, v3}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 466
    return-void

    .line 463
    :cond_1
    new-instance v0, Lcom/google/android/gms/people/model/e;

    invoke-direct {v0, p3}, Lcom/google/android/gms/people/model/e;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method
