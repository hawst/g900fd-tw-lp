.class public Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentPersonalUnlockingSettings;
.super Lcom/google/android/gms/auth/trustagent/as;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/as;-><init>()V

    return-void
.end method


# virtual methods
.method protected final e()Landroid/preference/PreferenceFragment;
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/gms/auth/trustagent/af;

    invoke-direct {v0}, Lcom/google/android/gms/auth/trustagent/af;-><init>()V

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentPersonalUnlockingSettings;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 27
    sget v1, Lcom/google/android/gms/m;->B:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 28
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/trustagent/as;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 33
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->D:I

    if-ne v0, v1, :cond_0

    .line 34
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "https://support.google.com/mobile/?p=personal_unlocking"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 36
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentPersonalUnlockingSettings;->startActivity(Landroid/content/Intent;)V

    .line 37
    const/4 v0, 0x1

    .line 39
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/trustagent/as;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
