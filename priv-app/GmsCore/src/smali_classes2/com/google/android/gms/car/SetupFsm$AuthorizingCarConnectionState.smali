.class public Lcom/google/android/gms/car/SetupFsm$AuthorizingCarConnectionState;
.super Lcom/google/android/car/fsm/j;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/car/fsm/l;
    a = {
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_CAR_STARTED_MOVING"
            b = Lcom/google/android/gms/car/SetupFsm$CarMovingState;
        .end subannotation,
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_ERROR"
            b = Lcom/google/android/gms/car/SetupFsm$ErrorState;
            c = Lcom/google/android/gms/car/SetupFsm$AuthorizingCarConnectionState;
        .end subannotation,
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_CAR_DISCONNECTED"
            b = Lcom/google/android/gms/car/SetupFsm$SetupFailedState;
            c = Lcom/google/android/gms/car/SetupFsm$AuthorizingCarConnectionState;
        .end subannotation,
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_CAR_CONNECTION_ALLOWED"
            b = Lcom/google/android/gms/car/SetupFsm$PromptInstallApplicationsState;
            c = Lcom/google/android/gms/car/SetupFsm$AuthorizingCarConnectionState;
        .end subannotation,
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_CAR_CONNECTION_DISALLOWED"
            b = Lcom/google/android/gms/car/SetupFsm$SetupFailedState;
            c = Lcom/google/android/gms/car/SetupFsm$AuthorizingCarConnectionState;
        .end subannotation
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 298
    invoke-direct {p0}, Lcom/google/android/car/fsm/j;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/android/car/fsm/j;->a:Lcom/google/android/car/fsm/FsmController;

    invoke-virtual {v0}, Lcom/google/android/car/fsm/FsmController;->c()Lcom/google/android/car/fsm/g;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/pg;

    .line 302
    invoke-interface {v0}, Lcom/google/android/gms/car/pg;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 303
    iget-object v1, p0, Lcom/google/android/car/fsm/j;->a:Lcom/google/android/car/fsm/FsmController;

    const-string v2, "EVENT_CAR_CONNECTION_ALLOWED"

    invoke-virtual {v1, v2}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/String;)V

    .line 311
    :goto_0
    invoke-interface {v0}, Lcom/google/android/gms/car/pg;->a()V

    .line 312
    return-void

    .line 304
    :cond_0
    invoke-interface {v0}, Lcom/google/android/gms/car/pg;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 305
    iget-object v1, p0, Lcom/google/android/car/fsm/j;->a:Lcom/google/android/car/fsm/FsmController;

    const-class v2, Lcom/google/android/gms/car/po;

    invoke-virtual {v1, v2}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/Class;)V

    goto :goto_0

    .line 308
    :cond_1
    iget-object v1, p0, Lcom/google/android/car/fsm/j;->a:Lcom/google/android/car/fsm/FsmController;

    const-string v2, "EVENT_CAR_CONNECTION_DISALLOWED"

    invoke-virtual {v1, v2}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 316
    const-string v0, "EVENT_CAR_STARTED_MOVING"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "EVENT_ERROR"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "EVENT_CAR_DISCONNECTED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "EVENT_CAR_CONNECTION_ALLOWED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "EVENT_CAR_CONNECTION_DISALLOWED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 322
    :cond_0
    const/4 v0, 0x0

    .line 326
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
