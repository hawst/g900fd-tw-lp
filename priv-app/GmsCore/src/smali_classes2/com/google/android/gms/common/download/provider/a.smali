.class public final Lcom/google/android/gms/common/download/provider/a;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SourceFile"


# static fields
.field private static volatile a:Lcom/google/android/gms/common/download/provider/a;

.field private static final b:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/download/provider/a;->b:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 54
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 55
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/common/download/provider/a;
    .locals 3

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/gms/common/download/provider/a;->a:Lcom/google/android/gms/common/download/provider/a;

    if-nez v0, :cond_1

    .line 28
    sget-object v1, Lcom/google/android/gms/common/download/provider/a;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 29
    :try_start_0
    sget-object v0, Lcom/google/android/gms/common/download/provider/a;->a:Lcom/google/android/gms/common/download/provider/a;

    if-nez v0, :cond_0

    .line 30
    sget-boolean v0, Lcom/google/android/gms/common/download/DownloadService;->b:Z

    if-eqz v0, :cond_2

    .line 32
    new-instance v0, Lcom/google/android/gms/common/download/provider/a;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/common/download/provider/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/common/download/provider/a;->a:Lcom/google/android/gms/common/download/provider/a;

    .line 38
    :cond_0
    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    :cond_1
    sget-object v0, Lcom/google/android/gms/common/download/provider/a;->a:Lcom/google/android/gms/common/download/provider/a;

    return-object v0

    .line 34
    :cond_2
    :try_start_1
    new-instance v0, Lcom/google/android/gms/common/download/provider/a;

    const-string v2, "downloads.db"

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/common/download/provider/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/common/download/provider/a;->a:Lcom/google/android/gms/common/download/provider/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 38
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 59
    const-string v0, "CREATE TABLE downloads (_id INTEGER PRIMARY KEY AUTOINCREMENT,filename TEXT NOT NULL UNIQUE,url TEXT NOT NULL,sizeBytes INTEGER NOT NULL,sha1 TEXT NOT NULL,destination TEXT,minVersion INTEGER,maxVersion INTEGER,notificationTitle TEXT,notificationDescription TEXT,enabled INTEGER DEFAULT 0,retries INTEGER DEFAULT 0,dm_id INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1

    .prologue
    .line 82
    if-gtz p2, :cond_0

    .line 83
    const-string v0, "DROP TABLE IF EXISTS downloads"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 84
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/download/provider/a;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 86
    :cond_0
    return-void
.end method
