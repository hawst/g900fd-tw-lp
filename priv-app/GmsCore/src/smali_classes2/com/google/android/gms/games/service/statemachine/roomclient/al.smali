.class public final Lcom/google/android/gms/games/service/statemachine/roomclient/al;
.super Lcom/google/android/gms/games/service/statemachine/e;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/games/service/statemachine/roomclient/u;

.field public final b:J

.field public final c:Lcom/google/android/gms/common/server/ClientContext;

.field public final d:Lcom/google/android/gms/games/internal/ConnectionInfo;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/u;JLcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/ConnectionInfo;)V
    .locals 2

    .prologue
    .line 745
    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/statemachine/e;-><init>(I)V

    .line 746
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/al;->b:J

    .line 747
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/al;->c:Lcom/google/android/gms/common/server/ClientContext;

    .line 748
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/ConnectionInfo;

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/al;->d:Lcom/google/android/gms/games/internal/ConnectionInfo;

    .line 749
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/u;

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/al;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/u;

    .line 750
    return-void
.end method
