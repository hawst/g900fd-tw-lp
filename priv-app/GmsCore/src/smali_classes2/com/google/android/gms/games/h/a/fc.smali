.class public final Lcom/google/android/gms/games/h/a/fc;
.super Lcom/google/android/gms/common/server/response/c;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 70
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 72
    sput-object v0, Lcom/google/android/gms/games/h/a/fc;->b:Ljava/util/HashMap;

    const-string v1, "applicationId"

    const-string v2, "applicationId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/google/android/gms/games/h/a/fc;->b:Ljava/util/HashMap;

    const-string v1, "expirationDurationDays"

    const-string v2, "expirationDurationDays"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/google/android/gms/games/h/a/fc;->b:Ljava/util/HashMap;

    const-string v1, "payload"

    const-string v2, "payload"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcom/google/android/gms/games/h/a/fc;->b:Ljava/util/HashMap;

    const-string v1, "recipientPlayerIds"

    const-string v2, "recipientPlayerIds"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcom/google/android/gms/games/h/a/fc;->b:Ljava/util/HashMap;

    const-string v1, "requestDefinitionId"

    const-string v2, "requestDefinitionId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lcom/google/android/gms/games/h/a/fc;->b:Ljava/util/HashMap;

    const-string v1, "requestId"

    const-string v2, "requestId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->c(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/google/android/gms/games/h/a/fc;->b:Ljava/util/HashMap;

    const-string v1, "requestType"

    const-string v2, "requestType"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 97
    if-eqz p1, :cond_0

    .line 98
    const-string v0, "applicationId"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/h/a/fc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :cond_0
    if-eqz p2, :cond_1

    .line 101
    const-string v0, "expirationDurationDays"

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/h/a/fc;->a(Ljava/lang/String;I)V

    .line 103
    :cond_1
    if-eqz p3, :cond_2

    .line 104
    const-string v0, "payload"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/games/h/a/fc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    :cond_2
    if-eqz p4, :cond_3

    .line 107
    const-string v0, "recipientPlayerIds"

    invoke-virtual {p0, v0, p4}, Lcom/google/android/gms/games/h/a/fc;->i(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 109
    :cond_3
    if-eqz p5, :cond_4

    .line 113
    const-string v0, "requestId"

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/games/h/a/fc;->a(Ljava/lang/String;J)V

    .line 115
    :cond_4
    if-eqz p6, :cond_5

    .line 116
    const-string v0, "requestType"

    invoke-virtual {p0, v0, p6}, Lcom/google/android/gms/games/h/a/fc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_5
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/google/android/gms/games/h/a/fc;->b:Ljava/util/HashMap;

    return-object v0
.end method
