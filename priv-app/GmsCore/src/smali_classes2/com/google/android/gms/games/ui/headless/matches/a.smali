.class public final Lcom/google/android/gms/games/ui/headless/matches/a;
.super Lcom/google/android/gms/games/ui/common/matches/u;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/b/a/c;
.implements Lcom/google/android/gms/games/ui/b/a/i;
.implements Lcom/google/android/gms/games/ui/b/a/l;


# instance fields
.field private final a:Lcom/google/android/gms/games/ui/headless/a;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/gms/games/ui/headless/a/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/headless/a;)V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/u;-><init>()V

    .line 60
    invoke-static {p1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 61
    iput-object p1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    .line 62
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/headless/a;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->b:Ljava/lang/String;

    .line 64
    new-instance v0, Lcom/google/android/gms/games/ui/headless/a/a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/headless/a/a;-><init>(Lcom/google/android/gms/games/ui/headless/a;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->c:Lcom/google/android/gms/games/ui/headless/a/a;

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    const-string v1, "com.google.android.gms.games.ui.dialog.progressDialogStartingTbmp"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/a;->dismiss(Landroid/support/v4/app/q;Ljava/lang/String;)V

    .line 68
    return-void
.end method

.method private f(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 6

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 113
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    const-string v0, "HeadlessMultiplayerInboxHelper"

    const-string v1, "acceptInvitation: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :goto_0
    return-void

    .line 119
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->z_()Z

    move-result v1

    if-nez v1, :cond_1

    .line 120
    const-string v0, "HeadlessMultiplayerInboxHelper"

    const-string v1, "acceptInvitation: invitation not valid anymore..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 124
    :cond_1
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->i()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 125
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    sget v2, Lcom/google/android/gms/p;->ks:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/headless/a;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 127
    invoke-static {v1}, Lcom/google/android/gms/games/ui/b/f;->a(Ljava/lang/String;)Lcom/google/android/gms/games/ui/b/f;

    move-result-object v1

    .line 128
    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    const-string v3, "com.google.android.gms.games.ui.dialog.progressDialogStartingTbmp"

    invoke-static {v2, v1, v3}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    .line 133
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->e()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v1

    .line 134
    sget-object v2, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->f()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v1, v3}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/headless/matches/b;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/headless/matches/b;-><init>(Lcom/google/android/gms/games/ui/headless/matches/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0

    .line 143
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->b:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->e()Lcom/google/android/gms/games/Game;

    move-result-object v3

    invoke-static {v2}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    invoke-static {v3}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    invoke-static {p1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v5, "invitation"

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-static {v4, v2}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    const/16 v1, 0x4e21

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/headless/a;->setResult(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->finish()V

    goto :goto_0
.end method

.method private f(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 6

    .prologue
    .line 487
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->b:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Lcom/google/android/gms/games/Game;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    invoke-static {p1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v5, "turn_based_match"

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-static {v4, v2}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V

    .line 488
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    const/16 v1, 0x4e21

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/headless/a;->setResult(I)V

    .line 489
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->finish()V

    .line 490
    return-void
.end method

.method private g(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 4

    .prologue
    .line 493
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 494
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 496
    const-string v0, "HeadlessMultiplayerInboxHelper"

    const-string v1, "launchGameForRematch: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    :goto_0
    return-void

    .line 500
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    sget v2, Lcom/google/android/gms/p;->kx:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/headless/a;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 501
    invoke-static {v1}, Lcom/google/android/gms/games/ui/b/f;->a(Ljava/lang/String;)Lcom/google/android/gms/games/ui/b/f;

    move-result-object v1

    .line 502
    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    const-string v3, "com.google.android.gms.games.ui.dialog.progressDialogStartingTbmp"

    invoke-static {v2, v1, v3}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    .line 505
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v1

    .line 506
    sget-object v2, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v1, v3}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/headless/matches/c;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/headless/matches/c;-><init>(Lcom/google/android/gms/games/ui/headless/matches/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/Game;)V
    .locals 3

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 204
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 206
    const-string v0, "HeadlessMultiplayerInboxHelper"

    const-string v1, "onInvitationGameInfoClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    :goto_0
    return-void

    .line 210
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/d/al;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 211
    const-string v1, "com.google.android.gms"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 213
    if-nez v1, :cond_1

    .line 216
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/headless/matches/a;->d(Lcom/google/android/gms/games/Game;)V

    goto :goto_0

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 222
    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->b:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Game;

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/ui/b/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/Game;)Lcom/google/android/gms/games/ui/b/a/b;

    move-result-object v0

    .line 224
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    const-string v2, "com.google.android.gms.games.ui.dialog.changeAccountDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    goto :goto_0

    .line 228
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->b:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Ljava/lang/String;)V

    goto :goto_0

    .line 231
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V
    .locals 4

    .prologue
    .line 272
    invoke-virtual {p1}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->d()Ljava/util/ArrayList;

    move-result-object v2

    .line 273
    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 274
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/matches/a;->c(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 273
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 276
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 259
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 260
    const-string v0, "HeadlessMultiplayerInboxHelper"

    const-string v1, "onInvitationClusterSeeMoreClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    :goto_0
    return-void

    .line 264
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->k:Lcom/google/android/gms/games/multiplayer/d;

    invoke-interface {v1, v0, p1, p2, p3}, Lcom/google/android/gms/games/multiplayer/d;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 266
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    const/16 v2, 0x384

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/games/ui/headless/a;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75
    const-string v0, "HeadlessMultiplayerInboxHelper"

    const-string v1, "onInvitationAccepted: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :goto_0
    return-void

    .line 80
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->e()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 82
    if-nez v1, :cond_1

    .line 85
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/headless/matches/a;->e(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    goto :goto_0

    .line 86
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 91
    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->b:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/ui/b/a/h;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/multiplayer/Invitation;)Lcom/google/android/gms/games/ui/b/a/h;

    move-result-object v0

    .line 93
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    const-string v2, "com.google.android.gms.games.ui.dialog.changeAccountDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    goto :goto_0

    .line 96
    :cond_2
    invoke-static {p1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/games/multiplayer/Invitation;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v0, p1, p0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/app/Activity;Lcom/google/android/gms/games/multiplayer/Invitation;Lcom/google/android/gms/games/ui/d/aq;)V

    goto :goto_0

    .line 100
    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/headless/matches/a;->f(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    .line 244
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v1, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    const-string v0, "HeadlessMultiplayerInboxHelper"

    const-string v1, "onInvitationParticipantListClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    :goto_0
    return-void

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->l()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->k()I

    move-result v3

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->e()Lcom/google/android/gms/games/Game;

    move-result-object v6

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;Lcom/google/android/gms/common/api/v;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/Game;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 4

    .prologue
    .line 313
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Lcom/google/android/gms/games/Game;

    move-result-object v0

    .line 314
    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->r()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 315
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    .line 316
    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v1, v2}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 318
    const-string v0, "HeadlessMultiplayerInboxHelper"

    const-string v1, "onMatchClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    :goto_0
    return-void

    .line 323
    :cond_0
    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 325
    if-nez v1, :cond_1

    .line 328
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/headless/matches/a;->d(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0

    .line 329
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 334
    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->b:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/gms/games/ui/b/a/k;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Z)Lcom/google/android/gms/games/ui/b/a/k;

    move-result-object v0

    .line 337
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    const-string v2, "com.google.android.gms.games.ui.dialog.changeAccountDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    goto :goto_0

    .line 341
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/headless/matches/a;->f(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0

    .line 344
    :cond_3
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/matches/a;->a(Lcom/google/android/gms/games/Game;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 402
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    .line 403
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v1, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    const-string v0, "HeadlessMultiplayerInboxHelper"

    const-string v1, "onMatchParticipantListClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    :goto_0
    return-void

    .line 409
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->l()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->v()I

    move-result v3

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Lcom/google/android/gms/games/Game;

    move-result-object v6

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;Lcom/google/android/gms/common/api/v;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/Game;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/turnbased/f;)V
    .locals 4

    .prologue
    .line 287
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/f;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    .line 288
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/f;->c()Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    move-result-object v1

    .line 290
    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    const-string v3, "com.google.android.gms.games.ui.dialog.progressDialogStartingTbmp"

    invoke-static {v2, v3}, Lcom/google/android/gms/games/ui/d/a;->dismiss(Landroid/support/v4/app/q;Ljava/lang/String;)V

    .line 292
    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v2

    .line 293
    iget-object v3, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v2, v3}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 295
    const-string v0, "HeadlessMultiplayerInboxHelper"

    const-string v1, "onTurnBasedMatchInitiated: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    :goto_0
    return-void

    .line 298
    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/games/ui/d/al;->a(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 299
    sget v0, Lcom/google/android/gms/p;->iU:I

    invoke-static {v0}, Lcom/google/android/gms/games/ui/b/d;->a(I)Lcom/google/android/gms/games/ui/b/d;

    move-result-object v0

    .line 301
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    const-string v2, "com.google.android.gms.games.ui.dialog.alertDialogNetworkError"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    goto :goto_0

    .line 303
    :cond_1
    if-nez v1, :cond_2

    .line 304
    const-string v1, "HeadlessMultiplayerInboxHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No turn-based match received after accepting invite: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 308
    :cond_2
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/headless/matches/a;->f(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->c:Lcom/google/android/gms/games/ui/headless/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/headless/a/a;->a(Lcom/google/android/gms/games/Game;)V

    .line 238
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V
    .locals 4

    .prologue
    .line 280
    invoke-virtual {p1}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->d()Ljava/util/ArrayList;

    move-result-object v2

    .line 281
    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 282
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/matches/a;->d(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 281
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 284
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/headless/matches/a;->f(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 108
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 4

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 351
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 353
    const-string v0, "HeadlessMultiplayerInboxHelper"

    const-string v1, "onMatchDismissed: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    :goto_0
    return-void

    .line 357
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v1

    .line 358
    sget-object v2, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v1, v3}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->e(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->F()V

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->c:Lcom/google/android/gms/games/ui/headless/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/headless/a/a;->a(Lcom/google/android/gms/games/Game;)V

    .line 397
    return-void
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 4

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 150
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 152
    const-string v0, "HeadlessMultiplayerInboxHelper"

    const-string v1, "onInvitationDeclined: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    :goto_0
    return-void

    .line 156
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->e()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v1

    .line 157
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->i()I

    move-result v2

    .line 158
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->f()Ljava/lang/String;

    move-result-object v3

    .line 159
    packed-switch v2, :pswitch_data_0

    .line 169
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unknown invitation type "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 161
    :pswitch_0
    sget-object v2, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {v2, v0, v1, v3}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->c(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->F()V

    goto :goto_0

    .line 165
    :pswitch_1
    sget-object v2, Lcom/google/android/gms/games/d;->m:Lcom/google/android/gms/games/multiplayer/realtime/b;

    invoke-interface {v2, v0, v1, v3}, Lcom/google/android/gms/games/multiplayer/realtime/b;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 159
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 4

    .prologue
    .line 365
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 366
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 368
    const-string v0, "HeadlessMultiplayerInboxHelper"

    const-string v1, "onMatchRematch: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    :goto_0
    return-void

    .line 373
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 375
    if-nez v1, :cond_1

    .line 378
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/headless/matches/a;->e(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0

    .line 379
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 384
    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->b:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    const/4 v3, 0x1

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/gms/games/ui/b/a/k;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Z)Lcom/google/android/gms/games/ui/b/a/k;

    move-result-object v0

    .line 387
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    const-string v2, "com.google.android.gms.games.ui.dialog.changeAccountDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    goto :goto_0

    .line 390
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/headless/matches/a;->g(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/games/Game;)V
    .locals 3

    .prologue
    .line 466
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 467
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 469
    const-string v0, "HeadlessMultiplayerInboxHelper"

    const-string v1, "switchAccountForGameDetail: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    :goto_0
    return-void

    .line 473
    :cond_0
    const-string v1, "com.google.android.gms"

    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->b:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 4

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 177
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 179
    const-string v0, "HeadlessMultiplayerInboxHelper"

    const-string v1, "onInvitationDismissed: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    :goto_0
    return-void

    .line 183
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->e()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v1

    .line 184
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->i()I

    move-result v2

    .line 185
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->f()Ljava/lang/String;

    move-result-object v3

    .line 186
    packed-switch v2, :pswitch_data_0

    .line 196
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unknown invitation type "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 188
    :pswitch_0
    sget-object v2, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {v2, v0, v1, v3}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->d(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->F()V

    goto :goto_0

    .line 192
    :pswitch_1
    sget-object v2, Lcom/google/android/gms/games/d;->m:Lcom/google/android/gms/games/multiplayer/realtime/b;

    invoke-interface {v2, v0, v1, v3}, Lcom/google/android/gms/games/multiplayer/realtime/b;->b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 186
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final d(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 3

    .prologue
    .line 436
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 437
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 439
    const-string v0, "HeadlessMultiplayerInboxHelper"

    const-string v1, "switchAccountForTurnBasedMatch: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    :goto_0
    return-void

    .line 443
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/headless/matches/a;->f(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0
.end method

.method public final e(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 3

    .prologue
    .line 416
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 417
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 419
    const-string v0, "HeadlessMultiplayerInboxHelper"

    const-string v1, "switchAccountForInvitation: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    :goto_0
    return-void

    .line 423
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->e()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    invoke-static {p1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/games/multiplayer/Invitation;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 427
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v0, p1, p0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/app/Activity;Lcom/google/android/gms/games/multiplayer/Invitation;Lcom/google/android/gms/games/ui/d/aq;)V

    goto :goto_0

    .line 430
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/headless/matches/a;->f(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    goto :goto_0
.end method

.method public final e(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 3

    .prologue
    .line 451
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 452
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 454
    const-string v0, "HeadlessMultiplayerInboxHelper"

    const-string v1, "switchAccountForRematch: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    :goto_0
    return-void

    .line 458
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/matches/a;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/headless/matches/a;->g(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0
.end method
