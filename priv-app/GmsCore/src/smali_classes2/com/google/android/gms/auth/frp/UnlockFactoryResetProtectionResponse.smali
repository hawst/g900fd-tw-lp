.class public Lcom/google/android/gms/auth/frp/UnlockFactoryResetProtectionResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/auth/frp/k;


# instance fields
.field final a:I

.field public final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/gms/auth/frp/k;

    invoke-direct {v0}, Lcom/google/android/gms/auth/frp/k;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/frp/UnlockFactoryResetProtectionResponse;->CREATOR:Lcom/google/android/gms/auth/frp/k;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/auth/frp/UnlockFactoryResetProtectionResponse;-><init>(II)V

    .line 46
    return-void
.end method

.method constructor <init>(II)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput p1, p0, Lcom/google/android/gms/auth/frp/UnlockFactoryResetProtectionResponse;->a:I

    .line 41
    iput p2, p0, Lcom/google/android/gms/auth/frp/UnlockFactoryResetProtectionResponse;->b:I

    .line 42
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 63
    invoke-static {p0, p1}, Lcom/google/android/gms/auth/frp/k;->a(Lcom/google/android/gms/auth/frp/UnlockFactoryResetProtectionResponse;Landroid/os/Parcel;)V

    .line 64
    return-void
.end method
