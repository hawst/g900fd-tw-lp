.class public Lcom/google/android/gms/car/FirstActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field private a:Landroid/hardware/usb/UsbManager;

.field private b:Lcom/google/android/gms/car/jk;

.field private c:Z

.field private d:I

.field private e:I

.field private f:Landroid/os/PowerManager$WakeLock;

.field private g:Ljava/io/Closeable;

.field private h:Lcom/google/android/gms/car/gx;

.field private i:Ljava/io/InputStream;

.field private j:Ljava/io/OutputStream;

.field private k:Landroid/net/wifi/p2p/WifiP2pManager;

.field private l:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

.field private m:Z

.field private n:Z

.field private o:Lcom/google/android/gms/car/ea;

.field private final p:Lcom/google/android/gms/common/api/x;

.field private final q:Lcom/google/android/gms/common/api/y;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 62
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 85
    iput-boolean v0, p0, Lcom/google/android/gms/car/FirstActivity;->m:Z

    .line 86
    iput-boolean v0, p0, Lcom/google/android/gms/car/FirstActivity;->n:Z

    .line 91
    new-instance v0, Lcom/google/android/gms/car/jc;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/jc;-><init>(Lcom/google/android/gms/car/FirstActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->p:Lcom/google/android/gms/common/api/x;

    .line 151
    new-instance v0, Lcom/google/android/gms/car/jd;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/jd;-><init>(Lcom/google/android/gms/car/FirstActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->q:Lcom/google/android/gms/common/api/y;

    .line 499
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/FirstActivity;)Lcom/google/android/gms/car/ea;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->o:Lcom/google/android/gms/car/ea;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/car/FirstActivity;Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/gx;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/gms/car/FirstActivity;->h:Lcom/google/android/gms/car/gx;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/FirstActivity;Ljava/io/Closeable;)Ljava/io/Closeable;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/gms/car/FirstActivity;->g:Ljava/io/Closeable;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/FirstActivity;Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/gms/car/FirstActivity;->i:Ljava/io/InputStream;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/FirstActivity;Ljava/io/OutputStream;)Ljava/io/OutputStream;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/gms/car/FirstActivity;->j:Ljava/io/OutputStream;

    return-object p1
.end method

.method private declared-synchronized a()V
    .locals 2

    .prologue
    .line 299
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.FIRST"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    const-string v0, "CAR.FIRST"

    const-string v1, "stopEmulatedCarConnection will disconnect"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->h:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->B()V

    .line 303
    invoke-direct {p0}, Lcom/google/android/gms/car/FirstActivity;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 304
    monitor-exit p0

    return-void

    .line 299
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Landroid/hardware/usb/UsbAccessory;)V
    .locals 3

    .prologue
    .line 337
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.FIRST"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338
    const-string v0, "CAR.FIRST"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "USB accessory attached: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    :cond_0
    new-instance v0, Lcom/google/android/gms/car/jg;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/jg;-><init>(Lcom/google/android/gms/car/FirstActivity;Landroid/hardware/usb/UsbAccessory;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/FirstActivity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 347
    monitor-exit p0

    return-void

    .line 337
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/car/FirstActivity;Landroid/content/Intent;I)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v3, -0x1

    const/4 v4, 0x0

    .line 62
    invoke-virtual {p0}, Lcom/google/android/gms/car/FirstActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/FirstActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CAR.FIRST"

    const-string v1, "emulator command not from a package from Google"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v3}, Lcom/google/android/gms/car/FirstActivity;->b(I)V

    :goto_0
    return-void

    :cond_0
    iput p2, p0, Lcom/google/android/gms/car/FirstActivity;->e:I

    const-string v0, "CAR.FIRST"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "CAR.FIRST"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleEmulatorCommand with command: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    packed-switch p2, :pswitch_data_0

    const-string v0, "CAR.FIRST"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown emulator command "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v3}, Lcom/google/android/gms/car/FirstActivity;->b(I)V

    goto :goto_0

    :pswitch_0
    const-string v0, "emul_info"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/car/emulator/CarEmulatorInfo;

    invoke-virtual {v0}, Lcom/google/android/car/emulator/CarEmulatorInfo;->a()Lcom/google/android/car/emulator/b;

    move-result-object v0

    :try_start_0
    invoke-interface {v0}, Lcom/google/android/car/emulator/b;->a()Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/car/emulator/b;->b()Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    new-instance v2, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;

    invoke-direct {v2, v1}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    new-instance v1, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    invoke-direct {v1, v0}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    invoke-direct {p0, v2, v1}, Lcom/google/android/gms/car/FirstActivity;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0, v3}, Lcom/google/android/gms/car/FirstActivity;->b(I)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gms/car/FirstActivity;->a()V

    invoke-direct {p0, v4}, Lcom/google/android/gms/car/FirstActivity;->b(I)V

    goto :goto_0

    :pswitch_2
    const-string v0, "package_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "component_name"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/FirstActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v4}, Lcom/google/android/gms/car/FirstActivity;->b(I)V

    goto :goto_0

    :pswitch_3
    const-string v0, "package_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "component_name"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/FirstActivity;->h:Lcom/google/android/gms/car/gx;

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "emulation not started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/car/FirstActivity;->h:Lcom/google/android/gms/car/gx;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/car/gx;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v4}, Lcom/google/android/gms/car/FirstActivity;->b(I)V

    goto/16 :goto_0

    :pswitch_4
    const-string v0, "byebye_reason_code"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/car/FirstActivity;->h:Lcom/google/android/gms/car/gx;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/gx;->c(I)V

    invoke-direct {p0, v4}, Lcom/google/android/gms/car/FirstActivity;->b(I)V

    goto/16 :goto_0

    :pswitch_5
    const-string v0, "CAR.FIRST"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "CAR.FIRST"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "last critical error:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/FirstActivity;->h:Lcom/google/android/gms/car/gx;

    invoke-virtual {v2}, Lcom/google/android/gms/car/gx;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->h:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->c()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/FirstActivity;->b(I)V

    goto/16 :goto_0

    :pswitch_6
    const-string v0, "package_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "component_name"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/FirstActivity;->h:Lcom/google/android/gms/car/gx;

    new-instance v3, Landroid/content/ComponentName;

    invoke-direct {v3, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/car/gx;->a(Landroid/content/ComponentName;)V

    invoke-direct {p0, v4}, Lcom/google/android/gms/car/FirstActivity;->b(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/gms/car/FirstActivity;Landroid/hardware/usb/UsbAccessory;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/FirstActivity;->a(Landroid/hardware/usb/UsbAccessory;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/FirstActivity;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 62
    new-instance v0, Lcom/google/android/gms/car/no;

    const-string v1, "ConnectToSink"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/no;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/car/no;->start()V

    new-instance v1, Lcom/google/android/gms/car/jj;

    const/16 v2, 0x7733

    invoke-direct {v1, p0, p1, v2, v0}, Lcom/google/android/gms/car/jj;-><init>(Lcom/google/android/gms/car/FirstActivity;Ljava/lang/String;ILcom/google/android/gms/car/no;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/no;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private declared-synchronized a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 286
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.FIRST"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 287
    const-string v0, "CAR.FIRST"

    const-string v1, "startEmulatedCarConnection"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/FirstActivity;->c:Z

    .line 290
    iput-object p1, p0, Lcom/google/android/gms/car/FirstActivity;->i:Ljava/io/InputStream;

    .line 291
    iput-object p2, p0, Lcom/google/android/gms/car/FirstActivity;->j:Ljava/io/OutputStream;

    .line 292
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/FirstActivity;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 293
    monitor-exit p0

    return-void

    .line 286
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 311
    const-string v0, "CAR.FIRST"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312
    const-string v0, "CAR.FIRST"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Starting projection client pkg: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cmp: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->h:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/car/gx;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/car/FirstActivity;)Lcom/google/android/gms/car/gx;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->h:Lcom/google/android/gms/car/gx;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 350
    const-string v0, "CAR.FIRST"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351
    const-string v0, "CAR.FIRST"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "trying to finish, isFinishing="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/car/FirstActivity;->isFinishing()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/FirstActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 354
    invoke-virtual {p0}, Lcom/google/android/gms/car/FirstActivity;->finish()V

    .line 356
    :cond_1
    return-void
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 273
    new-instance v0, Lcom/google/android/gms/car/jf;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/jf;-><init>(Lcom/google/android/gms/car/FirstActivity;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/FirstActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 280
    return-void
.end method

.method private declared-synchronized b(Landroid/hardware/usb/UsbAccessory;)V
    .locals 3

    .prologue
    .line 359
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/FirstActivity;->c:Z

    if-eqz v0, :cond_1

    .line 360
    const-string v0, "CAR.FIRST"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    const-string v0, "CAR.FIRST"

    const-string v1, "already connected ignoring this one"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 399
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 367
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->a:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v0, p1}, Landroid/hardware/usb/UsbManager;->hasPermission(Landroid/hardware/usb/UsbAccessory;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 370
    const-string v0, "CAR.FIRST"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 371
    const-string v0, "CAR.FIRST"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "no permission for accessory "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/car/FirstActivity;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 359
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 377
    :cond_3
    :try_start_2
    const-string v0, "CAR.FIRST"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 378
    const-string v0, "CAR.FIRST"

    const-string v1, "openAccessory"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->a:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v0, p1}, Landroid/hardware/usb/UsbManager;->openAccessory(Landroid/hardware/usb/UsbAccessory;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 381
    if-nez v0, :cond_6

    .line 384
    const-string v0, "CAR.FIRST"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 385
    const-string v0, "CAR.FIRST"

    const-string v1, "Could not obtain accessory connection."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    :cond_5
    invoke-direct {p0}, Lcom/google/android/gms/car/FirstActivity;->b()V

    goto :goto_0

    .line 390
    :cond_6
    iput-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->g:Ljava/io/Closeable;

    .line 391
    new-instance v1, Ljava/io/FileInputStream;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    iput-object v1, p0, Lcom/google/android/gms/car/FirstActivity;->i:Ljava/io/InputStream;

    .line 392
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    iput-object v1, p0, Lcom/google/android/gms/car/FirstActivity;->j:Ljava/io/OutputStream;

    .line 394
    const-string v0, "CAR.FIRST"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 395
    const-string v0, "CAR.FIRST"

    const-string v1, "Connected."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    :cond_7
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/FirstActivity;->c:Z

    .line 398
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/FirstActivity;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/car/FirstActivity;Landroid/hardware/usb/UsbAccessory;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/FirstActivity;->b(Landroid/hardware/usb/UsbAccessory;)V

    return-void
.end method

.method private declared-synchronized c()V
    .locals 4

    .prologue
    .line 402
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.FIRST"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 403
    const-string v0, "CAR.FIRST"

    const-string v1, "disconnecting from car service"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/FirstActivity;->c:Z

    .line 406
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->i:Ljava/io/InputStream;

    .line 407
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->j:Ljava/io/OutputStream;

    .line 408
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->g:Ljava/io/Closeable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 410
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->o:Lcom/google/android/gms/car/ea;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ea;->c_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 411
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->o:Lcom/google/android/gms/car/ea;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ea;->b()V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 418
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 413
    :catch_0
    move-exception v0

    .line 414
    :try_start_2
    const-string v1, "CAR.FIRST"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 415
    const-string v1, "CAR.FIRST"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to unbind service: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 402
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c(Lcom/google/android/gms/car/FirstActivity;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/gms/car/FirstActivity;->b()V

    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 626
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->k:Landroid/net/wifi/p2p/WifiP2pManager;

    if-nez v0, :cond_0

    .line 627
    const-string v0, "wifip2p"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/FirstActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->k:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 629
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->l:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-nez v0, :cond_1

    .line 630
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->k:Landroid/net/wifi/p2p/WifiP2pManager;

    invoke-virtual {p0}, Lcom/google/android/gms/car/FirstActivity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/car/ji;

    invoke-direct {v2, p0}, Lcom/google/android/gms/car/ji;-><init>(Lcom/google/android/gms/car/FirstActivity;)V

    invoke-virtual {v0, p0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->l:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 638
    :cond_1
    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/car/FirstActivity;)V
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->f:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/FirstActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x6

    const-string v2, "CAR.FIRST"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->f:Landroid/os/PowerManager$WakeLock;

    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->f:Landroid/os/PowerManager$WakeLock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->f:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    return-void
.end method

.method static synthetic e(Lcom/google/android/gms/car/FirstActivity;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 62
    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/FirstActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "CAR.FIRST"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "CAR.FIRST"

    const-string v2, "Wifi is disabled, enabling."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-boolean v3, p0, Lcom/google/android/gms/car/FirstActivity;->m:Z

    invoke-virtual {v0, v3}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    :cond_1
    new-instance v0, Lcom/google/android/gms/car/jk;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/jk;-><init>(Lcom/google/android/gms/car/FirstActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->b:Lcom/google/android/gms/car/jk;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.net.wifi.p2p.DISCOVERY_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.net.wifi.p2p.THIS_DEVICE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/car/FirstActivity;->b:Lcom/google/android/gms/car/jk;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/FirstActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/gms/car/FirstActivity;->d()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/gms/car/FirstActivity;)Landroid/hardware/usb/UsbManager;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->a:Landroid/hardware/usb/UsbManager;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/car/FirstActivity;)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->f:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->f:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->f:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_0
    return-void
.end method

.method static synthetic h(Lcom/google/android/gms/car/FirstActivity;)I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/google/android/gms/car/FirstActivity;->d:I

    return v0
.end method

.method static synthetic i(Lcom/google/android/gms/car/FirstActivity;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/gms/car/FirstActivity;->c()V

    return-void
.end method

.method static synthetic j(Lcom/google/android/gms/car/FirstActivity;)V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/FirstActivity;->b(I)V

    return-void
.end method

.method static synthetic k(Lcom/google/android/gms/car/FirstActivity;)Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/android/gms/car/FirstActivity;->n:Z

    return v0
.end method

.method static synthetic l(Lcom/google/android/gms/car/FirstActivity;)Z
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/FirstActivity;->n:Z

    return v0
.end method

.method static synthetic m(Lcom/google/android/gms/car/FirstActivity;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/gms/car/FirstActivity;->d()V

    return-void
.end method

.method static synthetic n(Lcom/google/android/gms/car/FirstActivity;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->l:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    return-object v0
.end method

.method static synthetic o(Lcom/google/android/gms/car/FirstActivity;)Landroid/net/wifi/p2p/WifiP2pManager;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->k:Landroid/net/wifi/p2p/WifiP2pManager;

    return-object v0
.end method

.method static synthetic p(Lcom/google/android/gms/car/FirstActivity;)Lcom/google/android/gms/car/jk;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->b:Lcom/google/android/gms/car/jk;

    return-object v0
.end method

.method static synthetic q(Lcom/google/android/gms/car/FirstActivity;)Lcom/google/android/gms/car/jk;
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->b:Lcom/google/android/gms/car/jk;

    return-object v0
.end method

.method static synthetic r(Lcom/google/android/gms/car/FirstActivity;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->l:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    return-object v0
.end method

.method static synthetic s(Lcom/google/android/gms/car/FirstActivity;)Z
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/FirstActivity;->c:Z

    return v0
.end method


# virtual methods
.method public final a(I)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v1, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 421
    iput p1, p0, Lcom/google/android/gms/car/FirstActivity;->d:I

    .line 425
    iget v0, p0, Lcom/google/android/gms/car/FirstActivity;->d:I

    if-nez v0, :cond_7

    .line 426
    iget v0, p0, Lcom/google/android/gms/car/FirstActivity;->e:I

    if-nez v0, :cond_1

    .line 427
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->h:Lcom/google/android/gms/car/gx;

    iget-object v1, p0, Lcom/google/android/gms/car/FirstActivity;->g:Ljava/io/Closeable;

    iget-object v2, p0, Lcom/google/android/gms/car/FirstActivity;->i:Ljava/io/InputStream;

    iget-object v3, p0, Lcom/google/android/gms/car/FirstActivity;->j:Ljava/io/OutputStream;

    iget v4, p0, Lcom/google/android/gms/car/FirstActivity;->d:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/gms/car/gx;->a(Ljava/io/Closeable;Ljava/io/InputStream;Ljava/io/OutputStream;I)V

    .line 428
    invoke-direct {p0, v5}, Lcom/google/android/gms/car/FirstActivity;->b(I)V

    .line 476
    :cond_0
    :goto_0
    return-void

    .line 429
    :cond_1
    iget v0, p0, Lcom/google/android/gms/car/FirstActivity;->e:I

    if-ne v0, v6, :cond_2

    .line 430
    invoke-direct {p0}, Lcom/google/android/gms/car/FirstActivity;->a()V

    .line 431
    invoke-direct {p0, v5}, Lcom/google/android/gms/car/FirstActivity;->b(I)V

    goto :goto_0

    .line 432
    :cond_2
    iget v0, p0, Lcom/google/android/gms/car/FirstActivity;->e:I

    if-ne v0, v1, :cond_5

    .line 433
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->h:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->g()Z

    move-result v0

    if-nez v0, :cond_3

    .line 434
    const-string v0, "CAR.FIRST"

    const-string v1, "emulator client start requested while car is not connected"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/FirstActivity;->b(I)V

    goto :goto_0

    .line 438
    :cond_3
    const-string v0, "CAR.FIRST"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 439
    const-string v0, "CAR.FIRST"

    const-string v1, "in onServiceConnected, grabbing intent"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/car/FirstActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 442
    const-string v1, "package_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 443
    const-string v2, "component_name"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 445
    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/car/FirstActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    invoke-direct {p0, v5}, Lcom/google/android/gms/car/FirstActivity;->b(I)V

    goto :goto_0

    .line 447
    :cond_5
    iget v0, p0, Lcom/google/android/gms/car/FirstActivity;->e:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 448
    invoke-virtual {p0}, Lcom/google/android/gms/car/FirstActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "finish_time"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 449
    iget-object v1, p0, Lcom/google/android/gms/car/FirstActivity;->h:Lcom/google/android/gms/car/gx;

    iget-object v2, p0, Lcom/google/android/gms/car/FirstActivity;->g:Ljava/io/Closeable;

    iget-object v3, p0, Lcom/google/android/gms/car/FirstActivity;->i:Ljava/io/InputStream;

    iget-object v4, p0, Lcom/google/android/gms/car/FirstActivity;->j:Ljava/io/OutputStream;

    iget v5, p0, Lcom/google/android/gms/car/FirstActivity;->d:I

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/gms/car/gx;->a(Ljava/io/Closeable;Ljava/io/InputStream;Ljava/io/OutputStream;I)V

    .line 450
    const-string v1, "CAR.FIRST"

    invoke-static {v1, v7}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 451
    const-string v1, "CAR.FIRST"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "emulator will disconnect after "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    :cond_6
    new-instance v1, Lcom/google/android/gms/car/jh;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/jh;-><init>(Lcom/google/android/gms/car/FirstActivity;)V

    int-to-long v2, v0

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/car/of;->a(Ljava/lang/Runnable;J)V

    goto/16 :goto_0

    .line 466
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->h:Lcom/google/android/gms/car/gx;

    iget-object v1, p0, Lcom/google/android/gms/car/FirstActivity;->g:Ljava/io/Closeable;

    iget-object v2, p0, Lcom/google/android/gms/car/FirstActivity;->i:Ljava/io/InputStream;

    iget-object v3, p0, Lcom/google/android/gms/car/FirstActivity;->j:Ljava/io/OutputStream;

    iget v4, p0, Lcom/google/android/gms/car/FirstActivity;->d:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/gms/car/gx;->a(Ljava/io/Closeable;Ljava/io/InputStream;Ljava/io/OutputStream;I)V

    .line 468
    iget v0, p0, Lcom/google/android/gms/car/FirstActivity;->d:I

    if-ne v0, v6, :cond_0

    .line 469
    const-string v0, "CAR.FIRST"

    invoke-static {v0, v7}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 470
    const-string v0, "CAR.FIRST"

    const-string v1, "USB connection, finish activity after launch"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    :cond_8
    invoke-direct {p0}, Lcom/google/android/gms/car/FirstActivity;->c()V

    .line 473
    invoke-direct {p0}, Lcom/google/android/gms/car/FirstActivity;->b()V

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/16 v2, 0x400

    .line 161
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 163
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 165
    invoke-virtual {p0}, Lcom/google/android/gms/car/FirstActivity;->finish()V

    .line 168
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/FirstActivity;->requestWindowFeature(I)Z

    .line 169
    invoke-virtual {p0}, Lcom/google/android/gms/car/FirstActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 172
    const-string v0, "usb"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/FirstActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/usb/UsbManager;

    iput-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->a:Landroid/hardware/usb/UsbManager;

    .line 174
    sget v0, Lcom/google/android/gms/l;->L:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/FirstActivity;->setContentView(I)V

    .line 176
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.car.service.START"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 177
    invoke-virtual {p0}, Lcom/google/android/gms/car/FirstActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 179
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/FirstActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 180
    new-instance v0, Lcom/google/android/gms/car/ea;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/car/je;

    invoke-direct {v3, p0}, Lcom/google/android/gms/car/je;-><init>(Lcom/google/android/gms/car/FirstActivity;)V

    iget-object v4, p0, Lcom/google/android/gms/car/FirstActivity;->p:Lcom/google/android/gms/common/api/x;

    iget-object v5, p0, Lcom/google/android/gms/car/FirstActivity;->q:Lcom/google/android/gms/common/api/y;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/ea;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/car/t;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V

    iput-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->o:Lcom/google/android/gms/car/ea;

    .line 194
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->o:Lcom/google/android/gms/car/ea;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ea;->a()V

    .line 195
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 329
    const-string v0, "CAR.FIRST"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 330
    const-string v0, "CAR.FIRST"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 333
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->b:Lcom/google/android/gms/car/jk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->b:Lcom/google/android/gms/car/jk;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/FirstActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->k:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->l:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->k:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/google/android/gms/car/FirstActivity;->l:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->stopPeerDiscovery(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->o:Lcom/google/android/gms/car/ea;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ea;->c_()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/car/FirstActivity;->o:Lcom/google/android/gms/car/ea;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ea;->b()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/FirstActivity;->m:Z

    if-eqz v0, :cond_4

    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/FirstActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 334
    :cond_4
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
