.class public final Lcom/google/android/gms/udc/d/b;
.super Lcom/google/android/gms/udc/d/a;
.source "SourceFile"


# instance fields
.field private g:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/udc/c/c;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/udc/a/a;[B)V
    .locals 1

    .prologue
    .line 47
    invoke-direct/range {p0 .. p5}, Lcom/google/android/gms/udc/d/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/udc/c/c;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/udc/a/a;[B)V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/d/b;->g:Ljava/util/List;

    .line 49
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/gms/udc/d/b;->b:Lcom/google/android/gms/udc/c/c;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/udc/c/c;->a(Lcom/google/android/gms/common/api/Status;Ljava/util/List;)V

    .line 101
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 14

    .prologue
    const/16 v3, 0x1194

    const/4 v4, 0x1

    const/4 v0, 0x0

    const/16 v2, 0x1199

    const/4 v1, 0x0

    .line 39
    new-instance v6, Lcom/google/android/gms/udc/e/d;

    invoke-direct {v6}, Lcom/google/android/gms/udc/e/d;-><init>()V

    :try_start_0
    iget-object v5, p0, Lcom/google/android/gms/udc/d/b;->e:[B

    invoke-static {v6, v5}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    iget-object v5, v6, Lcom/google/android/gms/udc/e/d;->a:[I

    iget-object v7, v6, Lcom/google/android/gms/udc/e/d;->d:Lcom/google/android/gms/udc/e/u;

    new-instance v8, Lcom/google/android/gms/udc/e/h;

    invoke-direct {v8}, Lcom/google/android/gms/udc/e/h;-><init>()V

    iput-object v5, v8, Lcom/google/android/gms/udc/e/h;->b:[I

    if-eqz v5, :cond_0

    array-length v5, v5

    if-nez v5, :cond_2

    :cond_0
    move v5, v4

    :goto_0
    iput-boolean v5, v8, Lcom/google/android/gms/udc/e/h;->a:Z

    iget-object v5, p0, Lcom/google/android/gms/udc/d/b;->a:Landroid/content/Context;

    iget-object v9, p0, Lcom/google/android/gms/udc/d/b;->f:Ljava/lang/String;

    invoke-static {v5, v9}, Lcom/google/android/gms/auth/r;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v9, p0, Lcom/google/android/gms/udc/d/b;->a:Landroid/content/Context;

    invoke-static {v9, v5}, Lcom/google/android/gms/udc/util/h;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/udc/e/a;

    move-result-object v9

    iput-object v9, v8, Lcom/google/android/gms/udc/e/h;->apiHeader:Lcom/google/android/gms/udc/e/a;

    iput-object v7, v8, Lcom/google/android/gms/udc/e/h;->c:Lcom/google/android/gms/udc/e/u;

    iget-object v7, p0, Lcom/google/android/gms/udc/d/b;->c:Lcom/google/android/gms/udc/a/a;

    iget-object v9, p0, Lcom/google/android/gms/udc/d/b;->d:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v7, v9, v8}, Lcom/google/android/gms/udc/a/a;->a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/udc/e/h;)Lcom/google/android/gms/udc/e/i;

    move-result-object v7

    iget-object v8, v7, Lcom/google/android/gms/udc/e/i;->apiHeader:Lcom/google/android/gms/udc/e/b;

    iget-object v9, p0, Lcom/google/android/gms/udc/d/b;->a:Landroid/content/Context;

    invoke-static {v8, v9, v5}, Lcom/google/android/gms/udc/util/h;->a(Lcom/google/android/gms/udc/e/b;Landroid/content/Context;Ljava/lang/String;)V

    iget-object v5, v7, Lcom/google/android/gms/udc/e/i;->a:[Lcom/google/android/gms/udc/e/p;

    array-length v5, v5

    if-nez v5, :cond_3

    const-string v1, "CheckConsentOperation"

    const-string v4, "retrieved empty setting states list"

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    :cond_1
    :goto_1
    if-nez v1, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/udc/d/b;->b:Lcom/google/android/gms/udc/c/c;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    iget-object v3, p0, Lcom/google/android/gms/udc/d/b;->g:Ljava/util/List;

    invoke-interface {v0, v1, v3}, Lcom/google/android/gms/udc/c/c;->a(Lcom/google/android/gms/common/api/Status;Ljava/util/List;)V

    :goto_2
    return-void

    :cond_2
    move v5, v1

    goto :goto_0

    :cond_3
    iget-object v8, v7, Lcom/google/android/gms/udc/e/i;->a:[Lcom/google/android/gms/udc/e/p;

    array-length v9, v8

    move v5, v1

    :goto_3
    if-ge v5, v9, :cond_5

    aget-object v10, v8, v5

    iget v11, v10, Lcom/google/android/gms/udc/e/p;->b:I

    const/4 v12, 0x2

    if-eq v11, v12, :cond_4

    move v4, v1

    :cond_4
    iget-object v11, p0, Lcom/google/android/gms/udc/d/b;->g:Ljava/util/List;

    new-instance v12, Lcom/google/android/gms/udc/SettingState;

    iget v13, v10, Lcom/google/android/gms/udc/e/p;->a:I

    iget v10, v10, Lcom/google/android/gms/udc/e/p;->b:I

    invoke-direct {v12, v13, v10}, Lcom/google/android/gms/udc/SettingState;-><init>(II)V

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_5
    if-nez v4, :cond_1

    iget-object v4, v7, Lcom/google/android/gms/udc/e/i;->b:Lcom/google/android/gms/udc/e/j;

    if-nez v4, :cond_6

    :goto_4
    if-eqz v1, :cond_7

    move v1, v3

    goto :goto_1

    :cond_6
    iget-object v1, v7, Lcom/google/android/gms/udc/e/i;->b:Lcom/google/android/gms/udc/e/j;

    iget-boolean v1, v1, Lcom/google/android/gms/udc/e/j;->a:Z

    goto :goto_4

    :cond_7
    const/16 v1, 0x1195

    goto :goto_1

    :cond_8
    if-ne v1, v3, :cond_a

    iget-object v3, p0, Lcom/google/android/gms/udc/d/b;->a:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/udc/d/b;->d:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v4}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v6}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/udc/e/d;)Landroid/content/Intent;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/udc/d/b;->b:Lcom/google/android/gms/udc/c/c;

    new-instance v5, Lcom/google/android/gms/common/api/Status;

    const-string v6, "User consent is required"

    iget-object v7, p0, Lcom/google/android/gms/udc/d/b;->a:Landroid/content/Context;

    if-nez v3, :cond_9

    :goto_5
    invoke-direct {v5, v1, v6, v0}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    iget-object v0, p0, Lcom/google/android/gms/udc/d/b;->g:Ljava/util/List;

    invoke-interface {v4, v5, v0}, Lcom/google/android/gms/udc/c/c;->a(Lcom/google/android/gms/common/api/Status;Ljava/util/List;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_2

    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x1198

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/d/b;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_2

    :cond_9
    const/high16 v0, 0x8000000

    :try_start_1
    invoke-static {v7, v3, v0}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_5

    :cond_a
    const/16 v0, 0x1195

    if-ne v1, v0, :cond_b

    iget-object v0, p0, Lcom/google/android/gms/udc/d/b;->b:Lcom/google/android/gms/udc/c/c;

    new-instance v3, Lcom/google/android/gms/common/api/Status;

    const-string v4, "Settings unavailable"

    const/4 v5, 0x0

    invoke-direct {v3, v1, v4, v5}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    iget-object v1, p0, Lcom/google/android/gms/udc/d/b;->g:Ljava/util/List;

    invoke-interface {v0, v3, v1}, Lcom/google/android/gms/udc/c/c;->a(Lcom/google/android/gms/common/api/Status;Ljava/util/List;)V
    :try_end_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/protobuf/nano/i; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_2

    :catch_1
    move-exception v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/d/b;->a(Lcom/android/volley/ac;)V

    goto :goto_2

    :cond_b
    if-ne v1, v2, :cond_c

    :try_start_2
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/d/b;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_2
    .catch Lcom/google/android/gms/auth/q; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/protobuf/nano/i; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    goto/16 :goto_2

    :catch_2
    move-exception v0

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x1197

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/d/b;->a(Lcom/google/android/gms/common/api/Status;)V

    goto/16 :goto_2

    :cond_c
    :try_start_3
    const-string v0, "CheckConsentOperation"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unkown status:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0xd

    const-string v3, "Unknown status code"

    const/4 v4, 0x0

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/d/b;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_3
    .catch Lcom/google/android/gms/auth/q; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/google/protobuf/nano/i; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_2

    :catch_3
    move-exception v0

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v0, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/d/b;->a(Lcom/google/android/gms/common/api/Status;)V

    goto/16 :goto_2
.end method
