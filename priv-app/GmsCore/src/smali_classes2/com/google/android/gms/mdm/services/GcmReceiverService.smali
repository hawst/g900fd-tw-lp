.class public Lcom/google/android/gms/mdm/services/GcmReceiverService;
.super Landroid/app/IntentService;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/w;
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;
.implements Lcom/google/android/gms/location/n;


# instance fields
.field protected a:Ljava/lang/String;

.field private b:Ljava/util/concurrent/Semaphore;

.field private c:Landroid/os/HandlerThread;

.field private d:Lcom/google/android/gms/location/d;

.field private e:Lcom/google/android/gms/common/api/v;

.field private f:Z

.field private g:[Ljava/lang/String;

.field private h:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 119
    const-class v0, Lcom/google/android/gms/mdm/services/GcmReceiverService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 109
    iput-boolean v1, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->f:Z

    .line 116
    iput-boolean v1, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->h:Z

    .line 120
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->setIntentRedelivery(Z)V

    .line 121
    return-void
.end method

.method private static a(Landroid/content/Intent;)Lcom/google/af/a/c/a/i;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 280
    new-instance v0, Lcom/google/af/a/c/a/i;

    invoke-direct {v0}, Lcom/google/af/a/c/a/i;-><init>()V

    .line 283
    :try_start_0
    const-string v2, "rp"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 285
    const-string v2, "rp"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    const/4 v3, 0x0

    array-length v4, v2

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/nano/a;->a([BII)Lcom/google/protobuf/nano/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/af/a/c/a/i;->a(Lcom/google/protobuf/nano/a;)Lcom/google/af/a/c/a/i;

    .line 313
    :goto_0
    return-object v0

    .line 287
    :cond_0
    const-string v2, "payload"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 289
    const-string v2, "payload"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v2

    const/4 v3, 0x0

    array-length v4, v2

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/nano/a;->a([BII)Lcom/google/protobuf/nano/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/af/a/c/a/i;->a(Lcom/google/protobuf/nano/a;)Lcom/google/af/a/c/a/i;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 306
    :catch_0
    move-exception v0

    const-string v0, "Invalid remote policy proto. Ignoring"

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "MDM"

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/mdm/f/b;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 307
    goto :goto_0

    .line 293
    :cond_1
    :try_start_1
    const-string v2, "action"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, v0, Lcom/google/af/a/c/a/i;->a:I

    .line 294
    const-string v2, "token"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "token"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    iput-object v2, v0, Lcom/google/af/a/c/a/i;->b:Ljava/lang/String;

    .line 296
    const-string v2, "email"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "email"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    const-string v3, "SHA-256"

    invoke-static {v2, v3}, Lcom/google/android/gms/common/util/e;->a(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v2

    iput-object v2, v0, Lcom/google/af/a/c/a/i;->c:[B

    .line 299
    const-string v2, "locate"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v0, Lcom/google/af/a/c/a/i;->d:Z

    .line 300
    const-string v2, "new_password"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "new_password"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_3
    iput-object v2, v0, Lcom/google/af/a/c/a/i;->e:Ljava/lang/String;

    .line 302
    const-string v2, "lock_message"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "lock_message"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_4
    iput-object v2, v0, Lcom/google/af/a/c/a/i;->f:Ljava/lang/String;
    :try_end_1
    .catch Lcom/google/protobuf/nano/i; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 309
    :catch_1
    move-exception v0

    const-string v0, "IOException parsing remote policy proto. Ignoring"

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "MDM"

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/mdm/f/b;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 310
    goto/16 :goto_0

    .line 294
    :cond_2
    :try_start_2
    const-string v2, ""

    goto :goto_1

    .line 296
    :cond_3
    const-string v2, ""

    goto :goto_2

    .line 300
    :cond_4
    const-string v2, ""

    goto :goto_3

    .line 302
    :cond_5
    const-string v2, ""
    :try_end_2
    .catch Lcom/google/protobuf/nano/i; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_4
.end method

.method private a()Lcom/google/android/gms/common/api/v;
    .locals 2

    .prologue
    .line 455
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/location/p;->a:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/x;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    return-object v0
.end method

.method private static a(ILandroid/location/Location;Lcom/google/af/a/c/a/a;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V
    .locals 7

    .prologue
    .line 622
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p0, v0, v1

    const/4 v4, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a([ILandroid/location/Location;Lcom/google/af/a/c/a/a;Ljava/lang/String;Lcom/google/af/a/c/a/h;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 624
    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 484
    invoke-static {p0}, Lcom/google/android/gms/mdm/f/d;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 485
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c(I)Lcom/google/af/a/c/a/k;

    .line 521
    :goto_0
    return-void

    .line 490
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/common/util/y;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 491
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c(I)Lcom/google/af/a/c/a/k;

    goto :goto_0

    .line 496
    :cond_1
    sget-object v0, Lcom/google/android/gms/mdm/e/a;->d:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/r;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 497
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c(I)Lcom/google/af/a/c/a/k;

    goto :goto_0

    .line 501
    :cond_2
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b:Ljava/util/concurrent/Semaphore;

    .line 502
    iput-boolean v1, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->f:Z

    .line 504
    sget-object v0, Lcom/google/android/gms/location/p;->b:Lcom/google/android/gms/location/d;

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->d:Lcom/google/android/gms/location/d;

    .line 505
    invoke-direct {p0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->e:Lcom/google/android/gms/common/api/v;

    .line 506
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->e:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 509
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b:Ljava/util/concurrent/Semaphore;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2, v1}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 516
    iget-boolean v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->f:Z

    if-nez v0, :cond_3

    .line 518
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c(I)Lcom/google/af/a/c/a/k;

    .line 520
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b()V

    goto :goto_0

    .line 511
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 512
    invoke-direct {p0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b()V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 336
    invoke-static {p0}, Lcom/google/android/gms/mdm/f/c;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 337
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b(I)V

    .line 338
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->d(I)V

    .line 381
    :cond_0
    :goto_0
    return-void

    .line 342
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 344
    const-string v0, "device_policy"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1, v2}, Landroid/app/admin/DevicePolicyManager;->resetPassword(Ljava/lang/String;I)Z

    move-result v3

    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->lockNow()V

    if-nez v3, :cond_7

    const-string v3, "Unable to reset. Password was not strong enough"

    new-array v4, v2, [Ljava/lang/Object;

    const-string v6, "MDM"

    invoke-static {v6, v3, v4}, Lcom/google/android/gms/mdm/f/b;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v4, Lcom/google/af/a/c/a/h;

    invoke-direct {v4}, Lcom/google/af/a/c/a/h;-><init>()V

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPasswordQuality(Landroid/content/ComponentName;)I

    move-result v3

    iput v3, v4, Lcom/google/af/a/c/a/h;->a:I

    iget v3, v4, Lcom/google/af/a/c/a/h;->a:I

    invoke-virtual {v0, v3}, Landroid/app/admin/DevicePolicyManager;->getPasswordMaximumLength(I)I

    move-result v3

    iput v3, v4, Lcom/google/af/a/c/a/h;->b:I

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumLength(Landroid/content/ComponentName;)I

    move-result v3

    iput v3, v4, Lcom/google/af/a/c/a/h;->c:I

    const/16 v3, 0xb

    invoke-static {v3}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumLetters(Landroid/content/ComponentName;)I

    move-result v3

    iput v3, v4, Lcom/google/af/a/c/a/h;->d:I

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumLowerCase(Landroid/content/ComponentName;)I

    move-result v3

    iput v3, v4, Lcom/google/af/a/c/a/h;->e:I

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumNonLetter(Landroid/content/ComponentName;)I

    move-result v3

    iput v3, v4, Lcom/google/af/a/c/a/h;->f:I

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumNumeric(Landroid/content/ComponentName;)I

    move-result v3

    iput v3, v4, Lcom/google/af/a/c/a/h;->g:I

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumSymbols(Landroid/content/ComponentName;)I

    move-result v3

    iput v3, v4, Lcom/google/af/a/c/a/h;->h:I

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumUpperCase(Landroid/content/ComponentName;)I

    move-result v0

    iput v0, v4, Lcom/google/af/a/c/a/h;->i:I

    .line 345
    :cond_2
    :goto_1
    if-eqz v4, :cond_3

    .line 346
    const/16 v0, 0x9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 349
    :cond_3
    invoke-static {p4}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {p4}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 351
    :cond_4
    const/16 v0, 0x11

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object p4, v1

    .line 355
    :cond_5
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 356
    const/16 v0, 0x12

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object p4, v1

    .line 360
    :cond_6
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 361
    invoke-direct {p0, v2}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->d(I)V

    .line 371
    :goto_2
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 373
    invoke-static {p0}, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->a(Landroid/content/Context;)V

    .line 378
    :goto_3
    if-eqz p2, :cond_0

    .line 379
    sget-object v0, Lcom/google/android/gms/mdm/b/a;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(J)V

    goto/16 :goto_0

    :cond_7
    move-object v4, v1

    .line 344
    goto :goto_1

    .line 363
    :cond_8
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [I

    move v3, v2

    .line 364
    :goto_4
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_9

    .line 365
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v0, v3

    .line 364
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_4

    .line 367
    :cond_9
    invoke-direct {p0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c()Lcom/google/af/a/c/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a:Ljava/lang/String;

    move-object v5, v1

    move-object v6, v1

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a([ILandroid/location/Location;Lcom/google/af/a/c/a/a;Ljava/lang/String;Lcom/google/af/a/c/a/h;Lcom/android/volley/x;Lcom/android/volley/w;)V

    goto :goto_2

    .line 375
    :cond_a
    invoke-static {p0, p3, p4}, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 384
    invoke-static {p0, p1}, Lcom/google/android/gms/mdm/f/c;->b(Landroid/content/Context;Z)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->d(I)V

    .line 385
    return-void
.end method

.method private static a([ILandroid/location/Location;Lcom/google/af/a/c/a/a;Ljava/lang/String;Lcom/google/af/a/c/a/h;Lcom/android/volley/x;Lcom/android/volley/w;)V
    .locals 0

    .prologue
    .line 631
    invoke-static/range {p0 .. p6}, Lcom/google/android/gms/mdm/a/b;->a([ILandroid/location/Location;Lcom/google/af/a/c/a/a;Ljava/lang/String;Lcom/google/af/a/c/a/h;Lcom/android/volley/x;Lcom/android/volley/w;)Lcom/android/volley/p;

    .line 633
    return-void
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 637
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    move v0, v1

    .line 638
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 640
    aget-char v3, v2, v0

    invoke-static {v3}, Landroid/telephony/PhoneNumberUtils;->isISODigit(C)Z

    move-result v3

    if-nez v3, :cond_1

    aget-char v3, v2, v0

    const/16 v4, 0x2b

    if-eq v3, v4, :cond_1

    .line 641
    const/4 v1, 0x1

    .line 644
    :cond_0
    return v1

    .line 638
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;[B)Z
    .locals 2

    .prologue
    .line 234
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SHA-256"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/e;->a(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    .line 235
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 554
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->e:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    .line 555
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->d:Lcom/google/android/gms/location/d;

    iget-object v1, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->e:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1, p0}, Lcom/google/android/gms/location/d;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/n;)Lcom/google/android/gms/common/api/am;

    .line 556
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->e:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 557
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->e:Lcom/google/android/gms/common/api/v;

    .line 558
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 560
    :cond_0
    return-void
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 394
    invoke-static {}, Lcom/google/android/gms/mdm/services/SitrepService;->a()V

    .line 395
    const/4 v0, 0x1

    invoke-static {p0, v0, p1}, Lcom/google/android/gms/mdm/services/SitrepService;->a(Landroid/content/Context;ZI)Landroid/content/Intent;

    move-result-object v0

    .line 396
    if-eqz v0, :cond_0

    .line 397
    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 399
    :cond_0
    return-void
.end method

.method private c()Lcom/google/af/a/c/a/a;
    .locals 1

    .prologue
    .line 605
    iget-boolean v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->h:Z

    if-eqz v0, :cond_0

    .line 606
    invoke-static {p0}, Lcom/google/android/gms/mdm/f/a;->a(Landroid/content/Context;)Lcom/google/af/a/c/a/a;

    move-result-object v0

    .line 608
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(I)Lcom/google/af/a/c/a/k;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 525
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 526
    const-string v0, "Don\'t call on the main thread"

    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 529
    :cond_0
    invoke-static {}, Lcom/android/volley/toolbox/aa;->a()Lcom/android/volley/toolbox/aa;

    move-result-object v4

    .line 531
    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c()Lcom/google/af/a/c/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a:Ljava/lang/String;

    move v0, p1

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(ILandroid/location/Location;Lcom/google/af/a/c/a/a;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 535
    :try_start_0
    invoke-virtual {v4}, Lcom/android/volley/toolbox/aa;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/af/a/c/a/k;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 542
    :goto_0
    return-object v0

    .line 536
    :catch_0
    move-exception v0

    const-string v1, "Unable to send response"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/mdm/f/f;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 538
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 539
    new-instance v0, Lcom/google/af/a/c/a/k;

    invoke-direct {v0}, Lcom/google/af/a/c/a/k;-><init>()V

    goto :goto_0

    .line 540
    :catch_1
    move-exception v0

    const-string v1, "Unable to send response"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/mdm/f/f;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 542
    new-instance v0, Lcom/google/af/a/c/a/k;

    invoke-direct {v0}, Lcom/google/af/a/c/a/k;-><init>()V

    goto :goto_0
.end method

.method private d(I)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 547
    invoke-direct {p0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c()Lcom/google/af/a/c/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a:Ljava/lang/String;

    move v0, p1

    move-object v4, v1

    move-object v5, v1

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(ILandroid/location/Location;Lcom/google/af/a/c/a/a;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 550
    return-void
.end method


# virtual methods
.method public final a(Landroid/location/Location;)V
    .locals 6

    .prologue
    .line 593
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->f:Z

    .line 595
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    sget-object v0, Lcom/google/android/gms/mdm/b/a;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_0

    .line 596
    invoke-direct {p0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b()V

    .line 600
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c()Lcom/google/af/a/c/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a:Ljava/lang/String;

    const/4 v4, 0x0

    move-object v1, p1

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(ILandroid/location/Location;Lcom/google/af/a/c/a/a;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 602
    return-void
.end method

.method public final a(Lcom/android/volley/ac;)V
    .locals 2

    .prologue
    .line 614
    const-string v0, "Unable to send response"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/mdm/f/f;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 615
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 4

    .prologue
    .line 564
    const-string v0, "Unable to connect to GMS Client: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->d()Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 565
    invoke-direct {p0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b()V

    .line 566
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 571
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->d:Lcom/google/android/gms/location/d;

    iget-object v1, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->e:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1}, Lcom/google/android/gms/location/d;->a(Lcom/google/android/gms/common/api/v;)Landroid/location/Location;

    move-result-object v1

    .line 572
    if-eqz v1, :cond_0

    .line 573
    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c()Lcom/google/af/a/c/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a:Ljava/lang/String;

    const/4 v4, 0x0

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(ILandroid/location/Location;Lcom/google/af/a/c/a/a;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 577
    :cond_0
    new-instance v0, Lcom/google/android/gms/location/LocationRequest;

    invoke-direct {v0}, Lcom/google/android/gms/location/LocationRequest;-><init>()V

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationRequest;->a(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/mdm/b/a;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->c(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->a(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/mdm/b/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/location/LocationRequest;->b(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    .line 582
    iget-object v1, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->d:Lcom/google/android/gms/location/d;

    iget-object v2, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->e:Lcom/google/android/gms/common/api/v;

    iget-object v3, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-interface {v1, v2, v0, p0, v3}, Lcom/google/android/gms/location/d;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/n;Landroid/os/Looper;)Lcom/google/android/gms/common/api/am;

    .line 584
    return-void
.end method

.method public final f_(I)V
    .locals 3

    .prologue
    .line 588
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 589
    return-void
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 125
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 126
    sget-object v0, Lcom/google/android/gms/mdm/b/a;->n:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, ","

    invoke-static {v0, v1}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->g:[Ljava/lang/String;

    .line 127
    new-instance v0, Landroid/os/HandlerThread;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " callbacks"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c:Landroid/os/HandlerThread;

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 129
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 135
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c:Landroid/os/HandlerThread;

    .line 137
    :cond_0
    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    .line 138
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 14
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "Wakelock"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 143
    .line 145
    :try_start_0
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 146
    const/4 v4, 0x1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    .line 148
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 150
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->h:Z

    invoke-static {p1}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(Landroid/content/Intent;)Lcom/google/af/a/c/a/i;

    move-result-object v5

    if-nez v5, :cond_2

    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c(I)Lcom/google/af/a/c/a/k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 153
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 155
    :cond_1
    invoke-static {p1}, Lcom/google/android/gms/gcm/GcmReceiver;->b(Landroid/content/Intent;)V

    .line 156
    return-void

    .line 150
    :cond_2
    :try_start_1
    const-string v0, "processing remote instruction: [%d, %s, %s, %s]"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, v5, Lcom/google/af/a/c/a/i;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x1

    iget-object v7, v5, Lcom/google/af/a/c/a/i;->b:Ljava/lang/String;

    aput-object v7, v4, v6

    const/4 v6, 0x2

    iget-object v7, v5, Lcom/google/af/a/c/a/i;->c:[B

    invoke-static {v7}, Lcom/google/android/gms/common/util/m;->b([B)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x3

    iget-boolean v7, v5, Lcom/google/af/a/c/a/i;->d:Z

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v4, v6

    const-string v6, "MDM"

    invoke-static {v6, v0, v4}, Lcom/google/android/gms/mdm/f/b;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, v5, Lcom/google/af/a/c/a/i;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a:Ljava/lang/String;

    iget-boolean v0, v5, Lcom/google/af/a/c/a/i;->i:Z

    iput-boolean v0, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->h:Z

    sget-object v0, Lcom/google/android/gms/mdm/b/a;->l:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c(I)Lcom/google/af/a/c/a/k;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 152
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 153
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 155
    :cond_3
    invoke-static {p1}, Lcom/google/android/gms/gcm/GcmReceiver;->b(Landroid/content/Intent;)V

    throw v0

    .line 150
    :cond_4
    :try_start_2
    iget-object v6, v5, Lcom/google/af/a/c/a/i;->c:[B

    if-eqz v6, :cond_5

    array-length v0, v6

    if-nez v0, :cond_6

    :cond_5
    move v0, v3

    :goto_1
    if-nez v0, :cond_c

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c(I)Lcom/google/af/a/c/a/k;

    goto :goto_0

    :cond_6
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v4, "com.google"

    invoke-virtual {v0, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v7

    array-length v8, v7

    move v4, v3

    :goto_2
    if-ge v4, v8, :cond_b

    aget-object v0, v7, v4

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_9

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v6}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(Ljava/lang/String;[B)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v2

    :goto_3
    if-eqz v0, :cond_a

    move v0, v2

    goto :goto_1

    :cond_7
    iget-object v10, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->g:[Ljava/lang/String;

    array-length v11, v10

    move v0, v3

    :goto_4
    if-ge v0, v11, :cond_9

    aget-object v12, v10, v0

    invoke-virtual {v9, v12}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_8

    const-string v13, "@gmail.com"

    invoke-virtual {v9, v12, v13}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12, v6}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(Ljava/lang/String;[B)Z

    move-result v12

    if-eqz v12, :cond_8

    move v0, v2

    goto :goto_3

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_9
    move v0, v3

    goto :goto_3

    :cond_a
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    :cond_b
    move v0, v3

    goto :goto_1

    :cond_c
    iget-boolean v0, v5, Lcom/google/af/a/c/a/i;->d:Z

    iget v2, v5, Lcom/google/af/a/c/a/i;->a:I

    packed-switch v2, :pswitch_data_0

    const-string v0, "Unrecognized action passed with tickle. Ignoring."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "MDM"

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/mdm/f/b;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c(I)Lcom/google/af/a/c/a/k;

    goto/16 :goto_0

    :pswitch_0
    invoke-static {p0}, Lcom/google/android/gms/mdm/f/c;->c(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_d

    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b(I)V

    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->d(I)V

    goto/16 :goto_0

    :cond_d
    if-eqz v0, :cond_e

    sget-object v0, Lcom/google/android/gms/mdm/b/a;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(J)V

    sget-object v0, Lcom/google/android/gms/mdm/b/a;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    :cond_e
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c(I)Lcom/google/af/a/c/a/k;

    const-string v0, "device_policy"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/admin/DevicePolicyManager;->wipeData(I)V

    goto/16 :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/gms/mdm/b/a;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(J)V

    goto/16 :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/gms/mdm/services/GcmReceiverService;->h:Z

    invoke-static {p0, v2, v3}, Lcom/google/android/gms/mdm/services/RingService;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/mdm/b/a;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(J)V

    goto/16 :goto_0

    :pswitch_3
    iget-boolean v0, v5, Lcom/google/af/a/c/a/i;->h:Z

    invoke-static {p0}, Lcom/google/android/gms/mdm/f/c;->c(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b(I)V

    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->d(I)V

    const-string v0, "Device administrator is already enabled; not showing notification."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "MDM"

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/mdm/f/b;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_f
    if-eqz v0, :cond_10

    const-class v0, Lcom/google/android/gms/mdm/receivers/ActivateDeviceAdminUponUnlockReceiver;

    const/4 v2, 0x1

    invoke-static {p0, v0, v2}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Ljava/lang/Class;Z)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->d(I)V

    goto/16 :goto_0

    :cond_10
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.settings.SECURITY_SETTINGS"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "show_device_admin"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {p0, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v2, Landroid/support/v4/app/bk;

    invoke-direct {v2, p0}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/google/android/gms/h;->cd:I

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->eY:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->rc:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v2

    iput-object v0, v2, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    invoke-virtual {v2}, Landroid/support/v4/app/bk;->a()Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/f;->O:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v0, Landroid/support/v4/app/bk;->y:I

    const-string v2, "recommendation"

    iput-object v2, v0, Landroid/support/v4/app/bk;->w:Ljava/lang/String;

    const/4 v2, 0x1

    iput v2, v0, Landroid/support/v4/app/bk;->z:I

    invoke-virtual {v0}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v2

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const-string v3, "mdm.notification_reminder"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->d(I)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v2, v5, Lcom/google/af/a/c/a/i;->e:Ljava/lang/String;

    iget-object v3, v5, Lcom/google/af/a/c/a/i;->f:Ljava/lang/String;

    iget-object v4, v5, Lcom/google/af/a/c/a/i;->g:Ljava/lang/String;

    invoke-direct {p0, v2, v0, v3, v4}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_5
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(Z)V

    goto/16 :goto_0

    :pswitch_6
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->a(Z)V

    goto/16 :goto_0

    :pswitch_7
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->b(I)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c(I)Lcom/google/af/a/c/a/k;

    goto/16 :goto_0

    :pswitch_8
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/mdm/services/GcmReceiverService;->c(I)Lcom/google/af/a/c/a/k;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
