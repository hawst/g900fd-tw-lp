.class public final Lcom/google/android/gms/games/service/a/e/d;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:Lcom/google/android/gms/games/a/au;

.field private final e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Z)V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 23
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/e/d;->c:Lcom/google/android/gms/games/internal/dr;

    .line 24
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/e/d;->d:Lcom/google/android/gms/games/a/au;

    .line 25
    iput-boolean p3, p0, Lcom/google/android/gms/games/service/a/e/d;->e:Z

    .line 26
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/android/gms/games/service/a/e/d;->e:Z

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/e/d;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->h(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 46
    :goto_0
    return-void

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/e/d;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->g(Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/google/android/gms/games/service/a/e/d;->e:Z

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/e/d;->d:Lcom/google/android/gms/games/a/au;

    invoke-virtual {p2, v0}, Lcom/google/android/gms/games/a/t;->o(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 36
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/e/d;->d:Lcom/google/android/gms/games/a/au;

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/games/a/t;->h(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method
