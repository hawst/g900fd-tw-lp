.class public final Lcom/google/android/gms/udc/d/d;
.super Lcom/google/android/gms/udc/d/a;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/udc/c/c;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/udc/a/a;[B)V
    .locals 0

    .prologue
    .line 36
    invoke-direct/range {p0 .. p5}, Lcom/google/android/gms/udc/d/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/udc/c/c;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/udc/a/a;[B)V

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/udc/d/d;->b:Lcom/google/android/gms/udc/c/c;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/udc/c/c;->b(Lcom/google/android/gms/common/api/Status;[B)V

    .line 65
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 7

    .prologue
    .line 30
    new-instance v4, Lcom/google/android/gms/udc/e/f;

    invoke-direct {v4}, Lcom/google/android/gms/udc/e/f;-><init>()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/udc/d/d;->e:[B

    invoke-static {v4, v0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    iget-object v0, p0, Lcom/google/android/gms/udc/d/d;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/udc/d/d;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/auth/r;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/gms/udc/d/d;->a:Landroid/content/Context;

    invoke-static {v0, v6}, Lcom/google/android/gms/udc/util/h;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/udc/e/a;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/udc/e/f;->apiHeader:Lcom/google/android/gms/udc/e/a;

    iget-object v0, p0, Lcom/google/android/gms/udc/d/d;->c:Lcom/google/android/gms/udc/a/a;

    iget-object v1, p0, Lcom/google/android/gms/udc/d/d;->d:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, v0, Lcom/google/android/gms/udc/a/a;->a:Lcom/google/android/gms/common/server/q;

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/users/me/overviewConfig?alt=proto"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v4

    new-instance v5, Lcom/google/android/gms/udc/e/g;

    invoke-direct {v5}, Lcom/google/android/gms/udc/e/g;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/udc/e/g;

    iget-object v1, v0, Lcom/google/android/gms/udc/e/g;->apiHeader:Lcom/google/android/gms/udc/e/b;

    iget-object v2, p0, Lcom/google/android/gms/udc/d/d;->a:Landroid/content/Context;

    invoke-static {v1, v2, v6}, Lcom/google/android/gms/udc/util/h;->a(Lcom/google/android/gms/udc/e/b;Landroid/content/Context;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/udc/d/d;->b:Lcom/google/android/gms/udc/c/c;

    sget-object v2, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/udc/c/c;->b(Lcom/google/android/gms/common/api/Status;[B)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x1198

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/d/d;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/d/d;->a(Lcom/android/volley/ac;)V

    goto :goto_0

    :catch_2
    move-exception v0

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x1197

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/d/d;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0

    :catch_3
    move-exception v0

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x1199

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/d/d;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0
.end method
