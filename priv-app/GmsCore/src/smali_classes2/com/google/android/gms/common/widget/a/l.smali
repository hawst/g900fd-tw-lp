.class public final Lcom/google/android/gms/common/widget/a/l;
.super Landroid/support/v7/widget/bv;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/widget/a/j;


# instance fields
.field public final c:Lcom/google/android/gms/common/widget/a/i;

.field private final d:Landroid/content/Context;

.field private final e:Ljava/util/ArrayList;

.field private final f:Landroid/util/SparseArray;

.field private final g:Lcom/google/android/gms/common/widget/a/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/support/v7/widget/bv;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/google/android/gms/common/widget/a/l;->d:Landroid/content/Context;

    .line 59
    new-instance v0, Lcom/google/android/gms/common/widget/a/i;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/widget/a/i;-><init>(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->c:Lcom/google/android/gms/common/widget/a/i;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->e:Ljava/util/ArrayList;

    .line 61
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->f:Landroid/util/SparseArray;

    .line 62
    new-instance v0, Lcom/google/android/gms/common/widget/a/d;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/widget/a/d;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->g:Lcom/google/android/gms/common/widget/a/d;

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->c:Lcom/google/android/gms/common/widget/a/i;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/widget/a/i;->a(Lcom/google/android/gms/common/widget/a/j;)V

    .line 65
    return-void
.end method

.method private g(I)Lcom/google/android/gms/common/widget/a/e;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, -0x1

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->c:Lcom/google/android/gms/common/widget/a/i;

    invoke-virtual {v0}, Lcom/google/android/gms/common/widget/a/i;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    new-instance v0, Lcom/google/android/gms/common/widget/a/n;

    invoke-direct {v0, v6, p1}, Lcom/google/android/gms/common/widget/a/n;-><init>(II)V

    move-object v2, v0

    .line 168
    :goto_0
    if-nez v2, :cond_5

    move-object v0, v1

    .line 180
    :cond_0
    :goto_1
    return-object v0

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v3, v0

    :goto_2
    if-ge v3, v2, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/widget/a/m;

    iget v4, v0, Lcom/google/android/gms/common/widget/a/m;->b:I

    invoke-virtual {v0}, Lcom/google/android/gms/common/widget/a/m;->a()I

    move-result v5

    add-int/2addr v4, v5

    if-ge p1, v4, :cond_3

    iget v2, v0, Lcom/google/android/gms/common/widget/a/m;->b:I

    if-ne p1, v2, :cond_2

    new-instance v0, Lcom/google/android/gms/common/widget/a/n;

    invoke-direct {v0, v3, v6}, Lcom/google/android/gms/common/widget/a/n;-><init>(II)V

    move-object v2, v0

    goto :goto_0

    :cond_2
    new-instance v2, Lcom/google/android/gms/common/widget/a/n;

    iget v0, v0, Lcom/google/android/gms/common/widget/a/m;->b:I

    add-int/lit8 v0, v0, 0x1

    sub-int v0, p1, v0

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/common/widget/a/n;-><init>(II)V

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_4
    move-object v2, v1

    goto :goto_0

    .line 172
    :cond_5
    iget v0, v2, Lcom/google/android/gms/common/widget/a/n;->a:I

    if-gez v0, :cond_6

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->c:Lcom/google/android/gms/common/widget/a/i;

    invoke-virtual {v0}, Lcom/google/android/gms/common/widget/a/i;->g()Ljava/util/List;

    move-result-object v0

    iget v1, v2, Lcom/google/android/gms/common/widget/a/n;->b:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/widget/a/e;

    goto :goto_1

    .line 176
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->e:Ljava/util/ArrayList;

    iget v1, v2, Lcom/google/android/gms/common/widget/a/n;->a:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/widget/a/m;

    iget-object v0, v0, Lcom/google/android/gms/common/widget/a/m;->a:Lcom/google/android/gms/common/widget/a/i;

    .line 177
    iget v1, v2, Lcom/google/android/gms/common/widget/a/n;->b:I

    if-ltz v1, :cond_0

    .line 180
    invoke-virtual {v0}, Lcom/google/android/gms/common/widget/a/i;->g()Ljava/util/List;

    move-result-object v0

    iget v1, v2, Lcom/google/android/gms/common/widget/a/n;->b:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/widget/a/e;

    goto :goto_1
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->c:Lcom/google/android/gms/common/widget/a/i;

    invoke-virtual {v0}, Lcom/google/android/gms/common/widget/a/i;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 157
    :goto_0
    return v0

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->e:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/common/widget/a/l;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/widget/a/m;

    .line 157
    iget v1, v0, Lcom/google/android/gms/common/widget/a/m;->b:I

    invoke-virtual {v0}, Lcom/google/android/gms/common/widget/a/m;->a()I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final a(I)I
    .locals 1

    .prologue
    .line 163
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/widget/a/l;->g(I)Lcom/google/android/gms/common/widget/a/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/widget/a/e;->e()I

    move-result v0

    return v0
.end method

.method public final synthetic a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/cs;
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/widget/a/h;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No factory for viewType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/common/widget/a/h;->a(Landroid/view/ViewGroup;I)Lcom/google/android/gms/common/widget/a/g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;)Lcom/google/android/gms/common/widget/a/i;
    .locals 4

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->c:Lcom/google/android/gms/common/widget/a/i;

    invoke-virtual {v0}, Lcom/google/android/gms/common/widget/a/i;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 110
    :goto_0
    new-instance v1, Lcom/google/android/gms/common/widget/a/i;

    invoke-direct {v1, p1}, Lcom/google/android/gms/common/widget/a/i;-><init>(Ljava/lang/CharSequence;)V

    .line 111
    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/widget/a/l;->a(Lcom/google/android/gms/common/widget/a/e;)V

    .line 112
    iget-object v2, p0, Lcom/google/android/gms/common/widget/a/l;->e:Ljava/util/ArrayList;

    new-instance v3, Lcom/google/android/gms/common/widget/a/m;

    invoke-direct {v3, p0, v1, v0}, Lcom/google/android/gms/common/widget/a/m;-><init>(Lcom/google/android/gms/common/widget/a/l;Lcom/google/android/gms/common/widget/a/i;I)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    return-object v1

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->e:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/common/widget/a/l;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/widget/a/m;

    .line 107
    iget v1, v0, Lcom/google/android/gms/common/widget/a/m;->b:I

    invoke-virtual {v0}, Lcom/google/android/gms/common/widget/a/m;->a()I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 128
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    invoke-direct {v0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>()V

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/ce;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->g:Lcom/google/android/gms/common/widget/a/d;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/cc;)V

    .line 130
    invoke-virtual {p1, p0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/bv;)V

    .line 131
    return-void
.end method

.method public final synthetic a(Landroid/support/v7/widget/cs;I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 47
    check-cast p1, Lcom/google/android/gms/common/widget/a/g;

    invoke-direct {p0, p2}, Lcom/google/android/gms/common/widget/a/l;->g(I)Lcom/google/android/gms/common/widget/a/e;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/gms/common/widget/a/g;->a(Lcom/google/android/gms/common/widget/a/e;)V

    add-int/lit8 v2, p2, 0x1

    invoke-direct {p0, v2}, Lcom/google/android/gms/common/widget/a/l;->g(I)Lcom/google/android/gms/common/widget/a/e;

    move-result-object v2

    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    iput-boolean v0, p1, Lcom/google/android/gms/common/widget/a/g;->k:Z

    return-void

    :cond_1
    invoke-interface {v1}, Lcom/google/android/gms/common/widget/a/e;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Lcom/google/android/gms/common/widget/a/e;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method final a(Lcom/google/android/gms/common/widget/a/e;)V
    .locals 3

    .prologue
    .line 269
    invoke-interface {p1}, Lcom/google/android/gms/common/widget/a/e;->e()I

    move-result v0

    .line 270
    iget-object v1, p0, Lcom/google/android/gms/common/widget/a/l;->f:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v1

    if-gez v1, :cond_0

    .line 271
    iget-object v1, p0, Lcom/google/android/gms/common/widget/a/l;->f:Landroid/util/SparseArray;

    invoke-interface {p1}, Lcom/google/android/gms/common/widget/a/e;->f()Lcom/google/android/gms/common/widget/a/h;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 273
    :cond_0
    return-void
.end method

.method public final a_(II)V
    .locals 1

    .prologue
    .line 225
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/common/widget/a/l;->b(II)V

    .line 226
    if-ge p1, p2, :cond_2

    .line 227
    if-lez p1, :cond_0

    .line 228
    add-int/lit8 v0, p1, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/widget/a/l;->c(I)V

    .line 230
    :cond_0
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/widget/a/l;->c(I)V

    .line 237
    :cond_1
    :goto_0
    return-void

    .line 232
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/widget/a/l;->c(I)V

    .line 233
    if-lez p2, :cond_1

    .line 234
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/widget/a/l;->c(I)V

    goto :goto_0
.end method

.method final b()V
    .locals 4

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->c:Lcom/google/android/gms/common/widget/a/i;

    invoke-virtual {v0}, Lcom/google/android/gms/common/widget/a/i;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 260
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 261
    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 262
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/widget/a/m;

    .line 263
    iput v2, v0, Lcom/google/android/gms/common/widget/a/m;->b:I

    .line 264
    invoke-virtual {v0}, Lcom/google/android/gms/common/widget/a/m;->a()I

    move-result v0

    add-int/2addr v2, v0

    .line 261
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 266
    :cond_0
    return-void
.end method

.method public final b_(I)V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->c:Lcom/google/android/gms/common/widget/a/i;

    invoke-virtual {v0}, Lcom/google/android/gms/common/widget/a/i;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/widget/a/e;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/widget/a/l;->a(Lcom/google/android/gms/common/widget/a/e;)V

    .line 207
    invoke-virtual {p0}, Lcom/google/android/gms/common/widget/a/l;->b()V

    .line 208
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/widget/a/l;->d(I)V

    .line 209
    if-lez p1, :cond_0

    .line 210
    add-int/lit8 v0, p1, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/widget/a/l;->c(I)V

    .line 212
    :cond_0
    return-void
.end method

.method public final c_(I)V
    .locals 2

    .prologue
    .line 216
    if-lez p1, :cond_0

    .line 217
    add-int/lit8 v0, p1, -0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/common/widget/a/l;->a(II)V

    .line 221
    :goto_0
    return-void

    .line 219
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/widget/a/l;->c(I)V

    goto :goto_0
.end method

.method public final f(I)Lcom/google/android/gms/common/widget/a/i;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/l;->d:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/widget/a/l;->a(Ljava/lang/CharSequence;)Lcom/google/android/gms/common/widget/a/i;

    move-result-object v0

    return-object v0
.end method
