.class public abstract Lcom/google/android/gms/common/data/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/data/d;


# instance fields
.field protected final a_:Lcom/google/android/gms/common/data/d;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/d;)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {p1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 25
    instance-of v0, p1, Lcom/google/android/gms/common/data/t;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Not possible to have nested FilteredDataBuffers."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 27
    iput-object p1, p0, Lcom/google/android/gms/common/data/t;->a_:Lcom/google/android/gms/common/data/d;

    .line 28
    return-void

    .line 25
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/common/data/t;->a_:Lcom/google/android/gms/common/data/d;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/data/t;->b(I)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/data/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected abstract b(I)I
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/google/android/gms/common/data/t;->w_()V

    .line 75
    return-void
.end method

.method public final e()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/common/data/t;->a_:Lcom/google/android/gms/common/data/d;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->e()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 58
    new-instance v0, Lcom/google/android/gms/common/data/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/data/e;-><init>(Lcom/google/android/gms/common/data/d;)V

    return-object v0
.end method

.method public w_()V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/common/data/t;->a_:Lcom/google/android/gms/common/data/d;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->w_()V

    .line 48
    return-void
.end method
