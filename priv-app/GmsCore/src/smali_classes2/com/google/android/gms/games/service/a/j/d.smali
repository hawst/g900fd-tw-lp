.class public final Lcom/google/android/gms/games/service/a/j/d;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/a/au;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/google/android/gms/games/internal/el;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Lcom/google/android/gms/games/internal/el;)V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 23
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/j/d;->c:Lcom/google/android/gms/games/a/au;

    .line 24
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/j/d;->d:Ljava/lang/String;

    .line 25
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/j/d;->e:Lcom/google/android/gms/games/internal/el;

    .line 26
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 36
    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 0

    .prologue
    .line 39
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/j/d;->c:Lcom/google/android/gms/games/a/au;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/j/d;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/j/d;->e:Lcom/google/android/gms/games/internal/el;

    invoke-virtual {p2, v0, v1, v2}, Lcom/google/android/gms/games/a/t;->c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Lcom/google/android/gms/games/internal/el;)V

    .line 32
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
