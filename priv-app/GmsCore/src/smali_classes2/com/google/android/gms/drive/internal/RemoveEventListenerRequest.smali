.class public Lcom/google/android/gms/drive/internal/RemoveEventListenerRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field final b:Lcom/google/android/gms/drive/DriveId;

.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/gms/drive/internal/dd;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/dd;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/internal/RemoveEventListenerRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/drive/DriveId;I)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput p1, p0, Lcom/google/android/gms/drive/internal/RemoveEventListenerRequest;->a:I

    .line 58
    iput-object p2, p0, Lcom/google/android/gms/drive/internal/RemoveEventListenerRequest;->b:Lcom/google/android/gms/drive/DriveId;

    .line 59
    iput p3, p0, Lcom/google/android/gms/drive/internal/RemoveEventListenerRequest;->c:I

    .line 60
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/DriveId;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/RemoveEventListenerRequest;->b:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/google/android/gms/drive/internal/RemoveEventListenerRequest;->c:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 44
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/drive/internal/dd;->a(Lcom/google/android/gms/drive/internal/RemoveEventListenerRequest;Landroid/os/Parcel;I)V

    .line 45
    return-void
.end method
