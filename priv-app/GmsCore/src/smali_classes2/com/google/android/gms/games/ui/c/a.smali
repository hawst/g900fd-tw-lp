.class public final Lcom/google/android/gms/games/ui/c/a;
.super Lcom/google/android/gms/games/ui/c/b;
.source "SourceFile"


# instance fields
.field private final p:Lcom/google/android/gms/games/achievement/Achievement;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/TextView;

.field private s:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 65
    const-wide/16 v4, 0xbb8

    const/4 v6, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/games/ui/c/b;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;JZ)V

    .line 66
    const-string v0, "com.google.android.gms.games.ACHIEVEMENT"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/achievement/Achievement;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->p:Lcom/google/android/gms/games/achievement/Achievement;

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->p:Lcom/google/android/gms/games/achievement/Achievement;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 58
    new-instance v0, Lcom/google/android/gms/games/ui/c/a;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/games/ui/c/a;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Landroid/os/Bundle;)V

    .line 60
    sget-object v1, Lcom/google/android/gms/games/ui/c/a;->b:Lcom/google/android/gms/games/ui/c/e;

    sget-object v2, Lcom/google/android/gms/games/ui/c/a;->b:Lcom/google/android/gms/games/ui/c/e;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/games/ui/c/e;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/c/e;->sendMessage(Landroid/os/Message;)Z

    .line 61
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 72
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/c/a;->j()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->n:Landroid/view/View;

    sget v2, Lcom/google/android/gms/h;->r:I

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->n:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->pz:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->q:Landroid/widget/TextView;

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->q:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/a;->p:Lcom/google/android/gms/games/achievement/Achievement;

    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->q:Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/f;->u:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->q:Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/g;->aA:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v6, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->q:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->n:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->py:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->r:Landroid/widget/TextView;

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->p:Lcom/google/android/gms/games/achievement/Achievement;

    invoke-interface {v0}, Lcom/google/android/gms/games/achievement/Achievement;->r()J

    move-result-wide v2

    .line 87
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    .line 88
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    .line 89
    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/a;->r:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/c/a;->j()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/gms/p;->hE:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->r:Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/f;->v:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->r:Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/g;->aB:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v6, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 99
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->m:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->pw:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 101
    sget v2, Lcom/google/android/gms/p;->hD:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    sget v2, Lcom/google/android/gms/f;->u:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 103
    sget v2, Lcom/google/android/gms/g;->aA:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v6, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 105
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->q:Landroid/widget/TextView;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/widget/TextView;Ljava/lang/Boolean;)V

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->r:Landroid/widget/TextView;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/widget/TextView;Ljava/lang/Boolean;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->l:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->bD:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->s:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    .line 114
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->s:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/a;->p:Lcom/google/android/gms/games/achievement/Achievement;

    invoke-interface {v1}, Lcom/google/android/gms/games/achievement/Achievement;->g()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/c/a;->a(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v1

    sget v2, Lcom/google/android/gms/h;->aq:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/c/a;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Landroid/graphics/Bitmap;Landroid/graphics/drawable/Drawable;)V

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->m:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->m:Landroid/view/View;

    sget v1, Lcom/google/android/gms/h;->r:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 120
    return-void

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->r:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected final b()V
    .locals 4

    .prologue
    .line 152
    invoke-super {p0}, Lcom/google/android/gms/games/ui/c/b;->b()V

    .line 154
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/c/a;->o:Z

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->m:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->pw:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 158
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/c/a;->j()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/gms/b;->k:I

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 160
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 161
    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/a;->i:Landroid/view/animation/Animation;

    invoke-virtual {v2}, Landroid/view/animation/Animation;->getDuration()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 162
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->q:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/a;->i:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->r:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/a;->i:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/a;->s:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/a;->j:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 168
    :cond_0
    return-void
.end method

.method protected final c()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 177
    sget-object v3, Lcom/google/android/gms/games/ui/c/a;->a:Ljava/util/ArrayList;

    monitor-enter v3

    move v1, v2

    .line 181
    :goto_0
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/ui/c/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 182
    sget-object v0, Lcom/google/android/gms/games/ui/c/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/c/b;

    .line 183
    instance-of v4, v0, Lcom/google/android/gms/games/ui/c/a;

    if-eqz v4, :cond_2

    .line 184
    check-cast v0, Lcom/google/android/gms/games/ui/c/a;

    .line 188
    if-eq v1, v2, :cond_0

    .line 189
    sget-object v2, Lcom/google/android/gms/games/ui/c/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 190
    sget-object v1, Lcom/google/android/gms/games/ui/c/a;->a:Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 193
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/a;->k:Landroid/view/animation/Animation;

    invoke-virtual {v1}, Landroid/view/animation/Animation;->getDuration()J

    move-result-wide v4

    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    invoke-direct {v1, v2, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/c/a;->k:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/a;->k:Landroid/view/animation/Animation;

    invoke-virtual {v1, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/a;->k:Landroid/view/animation/Animation;

    invoke-virtual {v1, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/a;->k:Landroid/view/animation/Animation;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 194
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/games/ui/c/a;->o:Z

    iget-object v1, v0, Lcom/google/android/gms/games/ui/c/a;->m:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/c/a;->g:Landroid/view/animation/Animation;

    invoke-virtual {v1}, Landroid/view/animation/Animation;->getDuration()J

    move-result-wide v4

    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v1, v2, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v1, v0, Lcom/google/android/gms/games/ui/c/a;->g:Landroid/view/animation/Animation;

    iget-object v1, v0, Lcom/google/android/gms/games/ui/c/a;->g:Landroid/view/animation/Animation;

    invoke-virtual {v1, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/c/a;->g:Landroid/view/animation/Animation;

    invoke-virtual {v1, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, v0, Lcom/google/android/gms/games/ui/c/a;->g:Landroid/view/animation/Animation;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 198
    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    invoke-super {p0}, Lcom/google/android/gms/games/ui/c/b;->c()V

    .line 200
    return-void

    .line 181
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 198
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method protected final d()V
    .locals 4

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/c/a;->j()Landroid/content/Context;

    move-result-object v0

    .line 206
    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/a;->c:Lcom/google/android/gms/games/a/au;

    iget-object v1, v1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    .line 207
    const-string v2, "com.google.android.gms.games.VIEW_ACHIEVEMENT"

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 209
    const-string v2, "com.google.android.gms.games.ACHIEVEMENT"

    iget-object v3, p0, Lcom/google/android/gms/games/ui/c/a;->p:Lcom/google/android/gms/games/achievement/Achievement;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 210
    const-string v2, "com.google.android.gms.games.SHOW_SEE_MORE"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 213
    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/client/ClientUiProxyActivity;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 214
    return-void
.end method

.method protected final e()I
    .locals 1

    .prologue
    .line 218
    const/16 v0, 0x18

    return v0
.end method

.method protected final f()I
    .locals 1

    .prologue
    .line 223
    const/16 v0, 0x19

    return v0
.end method
