.class public abstract Lcom/google/android/gms/games/ui/headless/b;
.super Lcom/google/android/gms/games/ui/headless/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/d/l;


# instance fields
.field public i:Landroid/support/v4/view/ViewPager;

.field protected j:Lcom/google/android/gms/games/ui/d/c;

.field private final k:Lcom/google/android/gms/games/ui/d/aj;

.field private l:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

.field private m:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/d/aj;II)V
    .locals 6

    .prologue
    .line 71
    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/ui/headless/b;-><init>(Lcom/google/android/gms/games/ui/d/aj;IIZZ)V

    .line 73
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/ui/d/aj;IIZ)V
    .locals 6

    .prologue
    .line 97
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/ui/headless/b;-><init>(Lcom/google/android/gms/games/ui/d/aj;IIZZ)V

    .line 99
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/ui/d/aj;IIZZ)V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0, p2, p3, p5, p4}, Lcom/google/android/gms/games/ui/headless/a;-><init>(IIZZ)V

    .line 124
    invoke-static {p1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 128
    iput-object p1, p0, Lcom/google/android/gms/games/ui/headless/b;->k:Lcom/google/android/gms/games/ui/d/aj;

    .line 129
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/headless/b;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/b;->i:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method private a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 268
    instance-of v0, p1, Lcom/google/android/gms/games/ui/s;

    if-eqz v0, :cond_0

    .line 269
    check-cast p1, Lcom/google/android/gms/games/ui/s;

    .line 271
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/b;->e:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/s;->b(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    .line 273
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/headless/b;I)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/headless/b;->d(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/headless/b;Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/headless/b;->a(Landroid/support/v4/app/Fragment;)V

    return-void
.end method

.method private d(I)V
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/b;->j:Lcom/google/android/gms/games/ui/d/c;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/d/c;->c(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 264
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/headless/b;->a(Landroid/support/v4/app/Fragment;)V

    .line 265
    return-void
.end method


# virtual methods
.method protected final N()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/b;->j:Lcom/google/android/gms/games/ui/d/c;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/d/c;->f()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/content/Context;)Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;
    .locals 1

    .prologue
    .line 277
    new-instance v0, Lcom/google/android/gms/games/ui/headless/e;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/games/ui/headless/e;-><init>(Lcom/google/android/gms/games/ui/headless/b;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/b;->m:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    .line 292
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/b;->m:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    return-object v0
.end method

.method public c(I)I
    .locals 1

    .prologue
    .line 228
    const/4 v0, -0x1

    return v0
.end method

.method public onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 201
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/headless/a;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    .line 206
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 133
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/headless/a;->onCreate(Landroid/os/Bundle;)V

    .line 136
    sget v0, Lcom/google/android/gms/j;->nn:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/b;->i:Landroid/support/v4/view/ViewPager;

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/b;->i:Landroid/support/v4/view/ViewPager;

    const-string v1, "layout resource did not include include a ViewPager with id \'pager\'"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/b;->i:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->c()V

    .line 151
    new-instance v0, Lcom/google/android/gms/games/ui/d/c;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/b;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/b;->k:Lcom/google/android/gms/games/ui/d/aj;

    iget-object v3, v1, Lcom/google/android/gms/games/ui/d/aj;->a:[Lcom/google/android/gms/games/ui/d/ak;

    const/4 v4, 0x0

    move-object v1, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/ui/d/c;-><init>(Landroid/content/Context;Landroid/support/v4/app/v;[Lcom/google/android/gms/games/ui/d/ak;Lcom/google/android/gms/games/ui/d/g;Lcom/google/android/gms/games/ui/d/l;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/b;->j:Lcom/google/android/gms/games/ui/d/c;

    .line 154
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/b;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/b;->j:Lcom/google/android/gms/games/ui/d/c;

    new-instance v1, Lcom/google/android/gms/games/ui/headless/c;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/headless/c;-><init>(Lcom/google/android/gms/games/ui/headless/b;)V

    iput-object v1, v0, Lcom/google/android/gms/games/ui/d/c;->b:Lcom/google/android/gms/games/ui/d/e;

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/b;->i:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/b;->j:Lcom/google/android/gms/games/ui/d/c;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Landroid/support/v4/view/at;)V

    .line 165
    sget v0, Lcom/google/android/gms/j;->oK:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/b;->l:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    .line 167
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/b;->f()Z

    move-result v0

    if-nez v0, :cond_2

    .line 168
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/b;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/f;->V:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 169
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/b;->l:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->c(I)V

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/b;->l:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/b;->i:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a(Landroid/support/v4/view/ViewPager;)V

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/b;->i:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/google/android/gms/games/ui/d/d;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/b;->i:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/headless/b;->j:Lcom/google/android/gms/games/ui/d/c;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/headless/b;->l:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/games/ui/d/d;-><init>(Landroid/support/v4/view/ViewPager;Lcom/google/android/gms/games/ui/d/c;Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Landroid/support/v4/view/ch;)V

    .line 190
    :goto_0
    if-nez p1, :cond_1

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/b;->k:Lcom/google/android/gms/games/ui/d/aj;

    iget v0, v0, Lcom/google/android/gms/games/ui/d/aj;->b:I

    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/b;->i:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->b()I

    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/b;->i:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/b;->f()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/b;->l:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a(I)V

    .line 197
    :cond_1
    :goto_1
    return-void

    .line 175
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/b;->l:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->setVisibility(I)V

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/b;->e:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/b;->e:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->f()V

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/b;->e:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Lcom/google/android/gms/games/ui/headless/d;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/b;->i:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/headless/b;->j:Lcom/google/android/gms/games/ui/d/c;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/headless/b;->l:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/google/android/gms/games/ui/headless/d;-><init>(Lcom/google/android/gms/games/ui/headless/b;Landroid/support/v4/view/ViewPager;Lcom/google/android/gms/games/ui/d/c;Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/support/v4/view/ch;)V

    goto :goto_0

    .line 191
    :cond_3
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/headless/b;->d(I)V

    goto :goto_1
.end method
