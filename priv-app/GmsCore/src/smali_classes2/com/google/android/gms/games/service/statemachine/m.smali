.class public abstract Lcom/google/android/gms/games/service/statemachine/m;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field protected static f:Z

.field protected static g:Z


# instance fields
.field private final a:Lcom/google/android/gms/common/util/a/b;

.field protected final h:Lcom/google/android/gms/games/service/statemachine/d;

.field i:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/gms/games/service/statemachine/m;->f:Z

    .line 22
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/games/service/statemachine/m;->g:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/d;)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/n;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/service/statemachine/n;-><init>(Lcom/google/android/gms/games/service/statemachine/m;)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/m;->a:Lcom/google/android/gms/common/util/a/b;

    .line 71
    iput-object p1, p0, Lcom/google/android/gms/games/service/statemachine/m;->h:Lcom/google/android/gms/games/service/statemachine/d;

    .line 72
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 111
    return-void
.end method

.method protected final a(IILjava/util/concurrent/TimeUnit;)V
    .locals 7

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/m;->i:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 81
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/m;->i:Ljava/util/HashMap;

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/m;->i:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/k;

    .line 84
    if-nez v0, :cond_1

    .line 85
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/k;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/m;->h:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/games/service/statemachine/k;-><init>(ILcom/google/android/gms/games/service/statemachine/d;)V

    .line 86
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/m;->i:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    :cond_1
    int-to-long v2, p2

    invoke-virtual {p3, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_2

    const-wide/32 v4, 0x7fffffff

    cmp-long v1, v2, v4

    if-gtz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/k;->b:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v4, Lcom/google/android/gms/games/service/statemachine/l;

    iget v5, v0, Lcom/google/android/gms/games/service/statemachine/k;->a:I

    iget-object v6, v0, Lcom/google/android/gms/games/service/statemachine/k;->c:Lcom/google/android/gms/games/service/statemachine/f;

    invoke-virtual {v6}, Lcom/google/android/gms/games/service/statemachine/f;->a()Lcom/google/android/gms/games/service/statemachine/g;

    move-result-object v6

    invoke-direct {v4, v0, v5, v6}, Lcom/google/android/gms/games/service/statemachine/l;-><init>(Lcom/google/android/gms/games/service/statemachine/k;ILcom/google/android/gms/games/service/statemachine/g;)V

    long-to-int v0, v2

    invoke-interface {v1, v4, v0}, Lcom/google/android/gms/games/service/statemachine/d;->a(Lcom/google/android/gms/games/service/statemachine/j;I)V

    .line 89
    return-void

    .line 88
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/service/statemachine/m;)V
    .locals 3

    .prologue
    .line 133
    iget-object v0, p1, Lcom/google/android/gms/games/service/statemachine/m;->a:Lcom/google/android/gms/common/util/a/b;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/m;->h:Lcom/google/android/gms/games/service/statemachine/d;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/m;->a:Lcom/google/android/gms/common/util/a/b;

    iget-object v2, p1, Lcom/google/android/gms/games/service/statemachine/m;->a:Lcom/google/android/gms/common/util/a/b;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/games/service/statemachine/d;->a(Lcom/google/android/gms/common/util/a/b;Lcom/google/android/gms/common/util/a/b;)V

    .line 135
    return-void
.end method

.method public a(Landroid/os/Message;)Z
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method protected final c()V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/m;->h:Lcom/google/android/gms/games/service/statemachine/d;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/m;->a:Lcom/google/android/gms/common/util/a/b;

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Lcom/google/android/gms/common/util/a/a;)V

    .line 99
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 105
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/m;->h:Lcom/google/android/gms/games/service/statemachine/d;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/m;->a:Lcom/google/android/gms/common/util/a/b;

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Lcom/google/android/gms/common/util/a/b;)V

    .line 125
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/m;->h:Lcom/google/android/gms/games/service/statemachine/d;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/m;->a:Lcom/google/android/gms/common/util/a/b;

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/service/statemachine/d;->b(Lcom/google/android/gms/common/util/a/b;)V

    .line 142
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/m;->a:Lcom/google/android/gms/common/util/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/common/util/a/b;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
