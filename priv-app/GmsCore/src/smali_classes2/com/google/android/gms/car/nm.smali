.class public final Lcom/google/android/gms/car/nm;
.super Lcom/google/android/gms/car/nn;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 6
    const/16 v0, 0x64

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/nn;-><init>(II)V

    .line 7
    return-void
.end method


# virtual methods
.method public final a(FFF)F
    .locals 4

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 11
    cmpg-float v1, p1, p2

    if-gtz v1, :cond_1

    .line 12
    const/4 v0, 0x0

    .line 17
    :cond_0
    :goto_0
    return v0

    .line 13
    :cond_1
    cmpl-float v1, p1, p3

    if-gez v1, :cond_0

    .line 16
    sub-float v1, p1, p2

    sub-float v2, p3, p2

    div-float/2addr v1, v2

    .line 17
    sub-float v1, v0, v1

    iget v2, p0, Lcom/google/android/gms/car/nm;->a:I

    iget v3, p0, Lcom/google/android/gms/car/nm;->b:I

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/car/nm;->a(FII)F

    move-result v1

    iget v2, p0, Lcom/google/android/gms/car/nm;->c:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    goto :goto_0
.end method
