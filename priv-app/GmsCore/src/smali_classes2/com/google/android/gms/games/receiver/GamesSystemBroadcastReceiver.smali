.class public final Lcom/google/android/gms/games/receiver/GamesSystemBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 37
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 38
    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 39
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Z)V

    .line 40
    invoke-static {}, Lcom/google/android/gms/common/server/ab;->a()V

    .line 51
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    const-string v1, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 42
    invoke-static {p1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 43
    :cond_2
    const-string v1, "android.provider.Telephony.SECRET_CODE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 44
    invoke-static {}, Lcom/google/android/gms/games/service/ac;->a()V

    goto :goto_0

    .line 45
    :cond_3
    const-string v1, "com.google.android.gms.people.BROADCAST_CIRCLES_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    invoke-static {p1}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;)V

    goto :goto_0
.end method
