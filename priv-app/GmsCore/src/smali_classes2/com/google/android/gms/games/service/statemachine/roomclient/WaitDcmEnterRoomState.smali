.class final Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;
.super Lcom/google/android/gms/games/service/statemachine/roomclient/ca;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field private e:Lcom/google/android/gms/games/internal/ConnectionInfo;

.field private j:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

.field private k:Ljava/lang/String;

.field private l:Lcom/google/android/gms/common/data/DataHolder;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    .line 33
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->l:Lcom/google/android/gms/common/data/DataHolder;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->b()Lcom/google/android/gms/games/service/statemachine/roomclient/ad;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ad;->a(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->l:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 55
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/ConnectionInfo;Lcom/google/android/gms/games/service/statemachine/roomclient/ag;Ljava/lang/String;Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 40
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/ConnectionInfo;

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->e:Lcom/google/android/gms/games/internal/ConnectionInfo;

    .line 41
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    .line 42
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->k:Ljava/lang/String;

    .line 43
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->l:Lcom/google/android/gms/common/data/DataHolder;

    .line 44
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->c()V

    .line 45
    return-void
.end method

.method public final a(Landroid/os/Message;)Z
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 59
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 109
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->f:Z

    :goto_0
    return v0

    .line 62
    :pswitch_0
    :try_start_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ar;

    .line 63
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    invoke-virtual {v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->b()Lcom/google/android/gms/games/service/statemachine/roomclient/ad;

    move-result-object v1

    .line 64
    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ar;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->l:Lcom/google/android/gms/common/data/DataHolder;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ad;->a(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->e:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    iget-wide v2, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->k:J

    iget-object v4, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    iget-object v4, v4, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->d:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->k:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    iget-object v6, v6, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->h:Landroid/os/IBinder;

    iget-object v7, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    iget-object v7, v7, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->g:Lcom/google/android/gms/games/internal/dr;

    iget-object v8, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->e:Lcom/google/android/gms/games/internal/ConnectionInfo;

    iget-object v9, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    iget-object v9, v9, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a(Lcom/google/android/gms/common/server/ClientContext;JLandroid/content/Context;Ljava/lang/String;Landroid/os/IBinder;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/internal/ConnectionInfo;Lcom/google/android/gms/games/service/statemachine/roomclient/ce;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->l:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 103
    iput-object v10, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->l:Lcom/google/android/gms/common/data/DataHolder;

    .line 105
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->f:Z

    goto :goto_0

    .line 77
    :cond_0
    const/4 v0, 0x6

    :try_start_1
    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ad;->a(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->c()V

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    iget-object v4, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    .line 82
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->b(I)V

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->c:Lcom/google/android/gms/games/service/statemachine/roomclient/bz;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->e:Lcom/google/android/gms/common/server/ClientContext;

    new-instance v3, Lcom/google/android/gms/games/service/statemachine/roomclient/ck;

    invoke-direct {v3, p0, v4}, Lcom/google/android/gms/games/service/statemachine/roomclient/ck;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;Lcom/google/android/gms/games/service/statemachine/roomclient/ce;)V

    iget-object v4, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->k:Ljava/lang/String;

    const-string v5, "REALTIME_SERVER_CONNECTION_FAILURE"

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/games/service/statemachine/roomclient/bz;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/service/h;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/h/a/ep;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->f:Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    iget-wide v2, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->k:J

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->e:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v4, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->e:Lcom/google/android/gms/games/internal/ConnectionInfo;

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->a(JLcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/ConnectionInfo;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 102
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->l:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 103
    iput-object v10, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->l:Lcom/google/android/gms/common/data/DataHolder;

    throw v0

    .line 59
    nop

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_0
    .end packed-switch
.end method
