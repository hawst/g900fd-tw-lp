.class final Lcom/google/android/gms/games/ui/client/quests/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/client/quests/b;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/client/quests/b;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/quests/c;->a:Lcom/google/android/gms/games/ui/client/quests/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 5

    .prologue
    .line 62
    check-cast p1, Lcom/google/android/gms/games/quest/f;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/quests/c;->a:Lcom/google/android/gms/games/ui/client/quests/b;

    invoke-interface {p1}, Lcom/google/android/gms/games/quest/f;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v1

    invoke-interface {p1}, Lcom/google/android/gms/games/quest/f;->c()Lcom/google/android/gms/games/quest/Quest;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/games/ui/client/quests/b;->a:Lcom/google/android/gms/games/ui/client/a;

    const-string v4, "com.google.android.gms.games.ui.dialog.progressDialogAcceptingQuest"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/ui/d/a;->dismiss(Landroid/support/v4/app/q;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/gms/games/ui/d/al;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    sget v1, Lcom/google/android/gms/p;->kC:I

    invoke-static {v1}, Lcom/google/android/gms/games/ui/b/d;->a(I)Lcom/google/android/gms/games/ui/b/d;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/games/ui/client/quests/b;->a:Lcom/google/android/gms/games/ui/client/a;

    const-string v2, "com.google.android.gms.games.ui.dialog.alertDialogNetworkError"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    if-nez v2, :cond_1

    const-string v0, "ClientQuestHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Quest not received after accepting: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/client/quests/b;->b(Lcom/google/android/gms/games/quest/Quest;)V

    goto :goto_0
.end method
