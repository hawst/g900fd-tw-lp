.class public final Lcom/google/android/gms/games/provider/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Lcom/google/android/gms/games/internal/ci;


# direct methods
.method private static a(Landroid/net/Uri$Builder;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1147
    const-string v0, "ext_achievement"

    invoke-virtual {p0, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1141
    const-string v0, "achievement_definitions"

    invoke-static {p0, v0}, Lcom/google/android/gms/games/provider/ac;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/games/provider/n;->a(Landroid/net/Uri$Builder;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1111
    const-string v0, "achievement_definitions"

    invoke-static {p0, v0}, Lcom/google/android/gms/games/provider/ac;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 1158
    const-string v0, "achievement_definitions"

    invoke-static {p0, v0}, Lcom/google/android/gms/games/provider/ac;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "game"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1130
    const-string v0, "achievement_definitions"

    invoke-static {p0, v0}, Lcom/google/android/gms/games/provider/ac;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/games/provider/n;->a(Landroid/net/Uri$Builder;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 1171
    const-string v0, "achievement_definitions"

    invoke-static {p0, v0}, Lcom/google/android/gms/games/provider/ac;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "ext_game"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
