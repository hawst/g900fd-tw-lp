.class public final Lcom/google/android/gms/drive/ae;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Z

.field private b:Ljava/lang/String;

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 187
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/drive/ae;->c:I

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/ae;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 248
    invoke-static {v1}, Lcom/google/android/gms/drive/ad;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 249
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized value for conflict strategy: 1"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 253
    :cond_0
    iput v1, p0, Lcom/google/android/gms/drive/ae;->c:I

    .line 254
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/drive/ae;
    .locals 5

    .prologue
    .line 205
    invoke-static {p1}, Lcom/google/android/gms/drive/ad;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "trackingTag must not be null nor empty, and the length must be <= the maximum length (%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/high16 v4, 0x10000

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 211
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/drive/ae;->b:Ljava/lang/String;

    .line 212
    return-object p0
.end method

.method public final b()Lcom/google/android/gms/drive/ad;
    .locals 4

    .prologue
    .line 258
    iget v0, p0, Lcom/google/android/gms/drive/ae;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/drive/ae;->a:Z

    if-nez v0, :cond_0

    .line 259
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot use CONFLICT_STRATEGY_KEEP_REMOTE without requesting completion notifications"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 262
    :cond_0
    new-instance v0, Lcom/google/android/gms/drive/ad;

    iget-object v1, p0, Lcom/google/android/gms/drive/ae;->b:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/gms/drive/ae;->a:Z

    iget v3, p0, Lcom/google/android/gms/drive/ae;->c:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/ad;-><init>(Ljava/lang/String;ZI)V

    return-object v0
.end method
