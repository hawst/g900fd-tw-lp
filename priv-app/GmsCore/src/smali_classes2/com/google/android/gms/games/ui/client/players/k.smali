.class final Lcom/google/android/gms/games/ui/client/players/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

.field private b:Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)V
    .locals 1

    .prologue
    .line 190
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/players/k;->a:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 192
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/k;->b:Ljava/lang/CharSequence;

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 196
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 201
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 205
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/k;->b:Ljava/lang/CharSequence;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/k;->a:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Lcom/google/android/gms/games/ui/client/players/q;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/client/players/q;->a(Ljava/lang/String;)V

    .line 207
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 208
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/k;->a:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->b(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 209
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/k;->a:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->c(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 210
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/k;->a:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Lcom/google/android/gms/games/ui/client/players/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/q;->k()V

    .line 221
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/k;->b:Ljava/lang/CharSequence;

    .line 222
    return-void

    .line 212
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/k;->a:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Lcom/google/android/gms/games/ui/client/players/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/q;->n()I

    move-result v0

    if-nez v0, :cond_2

    .line 213
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/k;->a:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->b(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 214
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/k;->a:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->c(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 216
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/k;->a:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->b(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 217
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/k;->a:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->c(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method
