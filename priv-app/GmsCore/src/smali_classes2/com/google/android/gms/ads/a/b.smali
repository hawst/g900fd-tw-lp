.class public final Lcom/google/android/gms/ads/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/a/d;

.field public static final b:Lcom/google/android/gms/common/a/d;

.field public static final c:Lcom/google/android/gms/common/a/d;

.field public static final d:Lcom/google/android/gms/common/a/d;

.field public static final e:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 24
    const-string v0, "gads:enable_adid_content_provider"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/a/b;->a:Lcom/google/android/gms/common/a/d;

    .line 26
    const-string v0, "gads:enable_adid_info"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/a/b;->b:Lcom/google/android/gms/common/a/d;

    .line 28
    const-string v0, "gads:adid_js_url"

    const-string v1, "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/adinfo.js"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/a/b;->c:Lcom/google/android/gms/common/a/d;

    .line 31
    const-string v0, "gads:adid_encryption_key"

    const-string v1, "94D9D46D06080C41C80F687718080E53A43FB8F2A3D04A84A22DB2EACB998DE34983C52195029D09DF8A8D670D6AE7500634B2A2E71EDBD858DEC2710DA1D2399BECEDBF1FF2A76823AE385C5CA7F866E17C959F126112DF889607D1E4D5C8B9160D8C86F5F69F229206AC8D665140F17004359B89369D2621FFE1108418E112B6FBF1ADDC8AC926673EF21FB086EEADD06F38D987B3D1D5512142AFC77282265A22DE2F3E868F3C31AA1373AFCA51FB7532D2A2D4E0E08F196DDA818BD1A17A03A08FA16A306CACAD5B40CF99A3A0315299BC9517E8631D54489223691B5A15163FB62C5F77DE01DEF298F85F89FF3F65EB43F43AE6E6F8BB4F5777B2A4AD05"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/a/b;->d:Lcom/google/android/gms/common/a/d;

    .line 42
    const-string v0, "gads:adid_encryption_key_exponent"

    const-string v1, "010001"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/a/b;->e:Lcom/google/android/gms/common/a/d;

    return-void
.end method
