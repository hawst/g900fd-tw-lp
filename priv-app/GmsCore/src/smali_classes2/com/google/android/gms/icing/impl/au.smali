.class final Lcom/google/android/gms/icing/impl/au;
.super Lcom/google/android/gms/icing/impl/ay;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/gms/icing/impl/a/e;

.field final synthetic c:Ljava/util/List;

.field final synthetic d:Ljava/util/List;

.field final synthetic e:Lcom/google/android/gms/icing/impl/u;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/e;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 796
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/au;->e:Lcom/google/android/gms/icing/impl/u;

    iput-object p2, p0, Lcom/google/android/gms/icing/impl/au;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/icing/impl/au;->b:Lcom/google/android/gms/icing/impl/a/e;

    iput-object p4, p0, Lcom/google/android/gms/icing/impl/au;->c:Ljava/util/List;

    iput-object p5, p0, Lcom/google/android/gms/icing/impl/au;->d:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/ay;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    return-void
.end method


# virtual methods
.method public final c()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 799
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/au;->e:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->j()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v2

    .line 800
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/au;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 801
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/au;->b:Lcom/google/android/gms/icing/impl/a/e;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/au;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/impl/a/e;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 802
    invoke-virtual {v2, v1}, Lcom/google/android/gms/icing/impl/a/j;->e(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v0

    .line 803
    if-eqz v0, :cond_0

    .line 804
    invoke-virtual {v0, v6}, Lcom/google/android/gms/icing/impl/a/ac;->a(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 805
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unregister: cannot "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Lcom/google/android/gms/icing/impl/a/ac;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " when previously "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/ac;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 808
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/au;->c:Ljava/util/List;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 826
    :cond_0
    :goto_0
    return-void

    .line 810
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/au;->d:Ljava/util/List;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 811
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/au;->c:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/au;->e:Lcom/google/android/gms/icing/impl/u;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/au;->b:Lcom/google/android/gms/icing/impl/a/e;

    invoke-virtual {v2, v1, v3}, Lcom/google/android/gms/icing/impl/u;->a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/e;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 815
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/au;->b:Lcom/google/android/gms/icing/impl/a/e;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/impl/a/e;)Ljava/util/List;

    move-result-object v0

    .line 816
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 817
    invoke-virtual {v2, v0}, Lcom/google/android/gms/icing/impl/a/j;->e(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v4

    .line 818
    invoke-virtual {v4, v6}, Lcom/google/android/gms/icing/impl/a/ac;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 819
    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/g;

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/a/j;->b(Lcom/google/android/gms/icing/g;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 820
    iget-object v5, p0, Lcom/google/android/gms/icing/impl/au;->d:Ljava/util/List;

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/g;

    iget-object v1, v1, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 822
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/au;->c:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/au;->e:Lcom/google/android/gms/icing/impl/u;

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/au;->b:Lcom/google/android/gms/icing/impl/a/e;

    invoke-virtual {v4, v0, v5}, Lcom/google/android/gms/icing/impl/u;->a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/e;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
