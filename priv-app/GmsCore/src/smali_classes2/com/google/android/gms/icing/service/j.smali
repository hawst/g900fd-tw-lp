.class final Lcom/google/android/gms/icing/service/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/appdatasearch/a/h;

.field final synthetic b:Lcom/google/android/gms/icing/service/g;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/service/g;Lcom/google/android/gms/appdatasearch/a/h;)V
    .locals 0

    .prologue
    .line 420
    iput-object p1, p0, Lcom/google/android/gms/icing/service/j;->b:Lcom/google/android/gms/icing/service/g;

    iput-object p2, p0, Lcom/google/android/gms/icing/service/j;->a:Lcom/google/android/gms/appdatasearch/a/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 423
    iget-object v0, p0, Lcom/google/android/gms/icing/service/j;->b:Lcom/google/android/gms/icing/service/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/service/g;->a:Lcom/google/android/gms/icing/impl/e/h;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/e/h;->b()Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 425
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/service/j;->a:Lcom/google/android/gms/appdatasearch/a/h;

    sget-object v2, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-interface {v0, v2, v1}, Lcom/google/android/gms/appdatasearch/a/h;->a(Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 435
    :cond_0
    :goto_0
    return-void

    .line 426
    :catch_0
    move-exception v0

    const-string v2, "Client died during getFileDescriptorAndDelete"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 429
    if-eqz v1, :cond_0

    .line 430
    :try_start_1
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 435
    :catch_1
    move-exception v0

    goto :goto_0
.end method
