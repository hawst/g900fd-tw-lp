.class public Lcom/google/android/gms/search/global/s;
.super Lcom/google/android/gms/search/a;
.source "SourceFile"


# instance fields
.field private final d:Lcom/google/r/a/a/b;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;)V
    .locals 6

    .prologue
    .line 24
    const/4 v1, 0x1

    const/4 v2, 0x2

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/a;-><init>(IILcom/google/android/gms/icing/impl/u;Ljava/lang/String;Ljava/lang/Object;)V

    .line 27
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/search/global/s;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;

    iget-object v0, v0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;->a:[B

    invoke-static {v0}, Lcom/google/r/a/a/b;->a([B)Lcom/google/r/a/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/search/global/s;->d:Lcom/google/r/a/a/b;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    .line 31
    return-void

    .line 28
    :catch_0
    move-exception v0

    const-string v1, "Unable to parse experiment config."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 30
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unable to parse experiment config."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/gms/search/global/s;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->B()Lcom/google/android/gms/icing/impl/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/search/global/s;->d:Lcom/google/r/a/a/b;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/q;->a(Lcom/google/r/a/a/b;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/search/global/s;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;

    iget-boolean v0, v0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/search/global/s;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->o()Lcom/google/android/gms/icing/b/j;

    :cond_0
    new-instance v2, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Response;

    invoke-direct {v2}, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Response;-><init>()V

    if-eqz v1, :cond_1

    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    :goto_0
    iput-object v0, v2, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    return-object v2

    :cond_1
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x8

    const-string v3, "Cannot save settings."

    const/4 v4, 0x0

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    goto :goto_0
.end method
