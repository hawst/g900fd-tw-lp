.class public final Lcom/google/android/gms/people/identity/s;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final e:Lcom/google/android/gms/people/identity/s;


# instance fields
.field public final a:[B

.field public final b:I

.field public final c:I

.field public final d:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 60
    new-instance v0, Lcom/google/android/gms/people/identity/s;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/google/android/gms/people/identity/s;-><init>(II[BLjava/util/Map;)V

    sput-object v0, Lcom/google/android/gms/people/identity/s;->e:Lcom/google/android/gms/people/identity/s;

    return-void
.end method

.method private constructor <init>(II[BLjava/util/Map;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput p1, p0, Lcom/google/android/gms/people/identity/s;->c:I

    .line 78
    iput p2, p0, Lcom/google/android/gms/people/identity/s;->b:I

    .line 79
    iput-object p3, p0, Lcom/google/android/gms/people/identity/s;->a:[B

    .line 80
    iput-object p4, p0, Lcom/google/android/gms/people/identity/s;->d:Ljava/util/Map;

    .line 81
    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lcom/google/android/gms/people/identity/s;
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 86
    if-nez p0, :cond_0

    .line 87
    sget-object v0, Lcom/google/android/gms/people/identity/s;->e:Lcom/google/android/gms/people/identity/s;

    .line 102
    :goto_0
    return-object v0

    .line 90
    :cond_0
    const-string v0, "get.server_blob.code"

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 93
    if-ne v2, v1, :cond_1

    .line 94
    sget-object v0, Lcom/google/android/gms/people/identity/s;->e:Lcom/google/android/gms/people/identity/s;

    goto :goto_0

    .line 97
    :cond_1
    const-string v0, "get.server_blob.body"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v3

    .line 98
    const-string v0, "get.server_blob.format"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 99
    const-string v0, "get.server_blob.headers"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 102
    new-instance v1, Lcom/google/android/gms/people/identity/s;

    invoke-direct {v1, v2, v4, v3, v0}, Lcom/google/android/gms/people/identity/s;-><init>(II[BLjava/util/Map;)V

    move-object v0, v1

    goto :goto_0
.end method
