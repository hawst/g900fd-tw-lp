.class public final Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;
.super Lcom/google/android/gms/games/ui/client/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/bo;
.implements Lcom/google/android/gms/games/ui/common/requests/m;


# static fields
.field private static final n:I

.field private static final o:I


# instance fields
.field private p:I

.field private q:Lcom/google/android/gms/games/ui/client/requests/a;

.field private r:Landroid/support/v4/app/Fragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    sget v0, Lcom/google/android/gms/l;->aJ:I

    sput v0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->n:I

    .line 42
    sget v0, Lcom/google/android/gms/m;->o:I

    sput v0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->o:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 60
    sget v0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->n:I

    sget v1, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->o:I

    invoke-direct {p0, v0, v1, v2, v2}, Lcom/google/android/gms/games/ui/client/a;-><init>(IIZZ)V

    .line 62
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    .line 129
    if-eqz p1, :cond_0

    .line 130
    iget v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->p:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCurrentFragment: unexpected index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->p:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    new-instance v0, Lcom/google/android/gms/games/ui/common/requests/k;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/common/requests/k;-><init>()V

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->r:Landroid/support/v4/app/Fragment;

    .line 131
    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 132
    sget v1, Lcom/google/android/gms/j;->dw:I

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->r:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 133
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 140
    :goto_1
    iget v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->p:I

    packed-switch v0, :pswitch_data_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setCurrentTitle: unexpected index: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->p:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130
    :pswitch_1
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/requests/j;->b(I)Lcom/google/android/gms/games/ui/common/requests/j;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/requests/j;->b(I)Lcom/google/android/gms/games/ui/common/requests/j;

    move-result-object v0

    goto :goto_0

    .line 135
    :cond_0
    sget v0, Lcom/google/android/gms/j;->dw:I

    invoke-virtual {v1, v0}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->r:Landroid/support/v4/app/Fragment;

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->r:Landroid/support/v4/app/Fragment;

    const-string v1, "Failed to find fragment during resume!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 140
    :pswitch_3
    sget v0, Lcom/google/android/gms/p;->kS:I

    :goto_2
    if-lez v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->setTitle(I)V

    .line 141
    :goto_3
    return-void

    .line 140
    :pswitch_4
    sget v0, Lcom/google/android/gms/p;->kO:I

    goto :goto_2

    :pswitch_5
    sget v0, Lcom/google/android/gms/p;->kQ:I

    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 130
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 140
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method protected final N()I
    .locals 1

    .prologue
    .line 124
    const/16 v0, 0xc

    return v0
.end method

.method public final O_()Lcom/google/android/gms/games/ui/common/requests/l;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->q:Lcom/google/android/gms/games/ui/client/requests/a;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 216
    const/4 v0, -0x1

    .line 217
    const-string v2, "giftsButton"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 223
    :cond_0
    :goto_0
    if-ltz v0, :cond_1

    iget v2, p0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->p:I

    if-eq v0, v2, :cond_1

    .line 224
    iput v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->p:I

    .line 225
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->b(Z)V

    .line 227
    :cond_1
    return-void

    .line 219
    :cond_2
    const-string v2, "wishesButton"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 220
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x1

    return v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 98
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_2

    const/16 v0, 0x384

    if-ne p2, v0, :cond_2

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->r:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/requests/k;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->r:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/games/ui/common/requests/k;

    const-string v1, "com.google.android.gms.games.GAME_REQUEST_CLUSTER"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    const-string v2, "com.google.android.gms.games.REMOVE_CLUSTER"

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    const-string v3, "com.google.android.gms.games.REMOVED_ID_LIST"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/common/requests/k;->a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;ZLjava/util/ArrayList;)V

    .line 120
    :goto_0
    return-void

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->r:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/requests/j;

    if-eqz v0, :cond_1

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->r:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/games/ui/common/requests/j;

    const-string v1, "com.google.android.gms.games.GAME_REQUEST_CLUSTER"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    const-string v2, "com.google.android.gms.games.REMOVE_CLUSTER"

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    const-string v3, "com.google.android.gms.games.REMOVED_ID_LIST"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/common/requests/j;->a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;ZLjava/util/ArrayList;)V

    goto :goto_0

    .line 113
    :cond_1
    const-string v0, "ClientReqInboxAct"

    const-string v1, "onActivityResult received coming from the Public Invitation UI but the current fragment cannot go to this UI. Something is really weird."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 118
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/client/a;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 200
    iget v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->p:I

    if-eqz v0, :cond_0

    move v0, v1

    .line 201
    :goto_0
    if-eqz v0, :cond_1

    .line 202
    iput v2, p0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->p:I

    .line 203
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->b(Z)V

    .line 207
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 200
    goto :goto_0

    .line 205
    :cond_1
    invoke-super {p0}, Lcom/google/android/gms/games/ui/client/a;->onBackPressed()V

    goto :goto_1
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->onCreate(Landroid/os/Bundle;)V

    .line 69
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->l:Z

    .line 71
    new-instance v0, Lcom/google/android/gms/games/ui/client/requests/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/client/requests/a;-><init>(Lcom/google/android/gms/games/ui/client/a;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->q:Lcom/google/android/gms/games/ui/client/requests/a;

    .line 75
    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 76
    :goto_0
    if-eqz v0, :cond_1

    .line 77
    iput v1, p0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->p:I

    .line 81
    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->b(Z)V

    .line 82
    return-void

    :cond_0
    move v0, v1

    .line 75
    goto :goto_0

    .line 79
    :cond_1
    const-string v1, "savedStateCurrentFragmentIndex"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->p:I

    goto :goto_1
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 86
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 88
    const-string v0, "savedStateCurrentFragmentIndex"

    iget v1, p0, Lcom/google/android/gms/games/ui/client/requests/ClientRequestInboxActivity;->p:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 89
    return-void
.end method

.method protected final u()I
    .locals 1

    .prologue
    .line 195
    sget v0, Lcom/google/android/gms/l;->aS:I

    return v0
.end method
