.class public final Lcom/google/android/gms/icing/impl/e/e;
.super Lcom/google/k/c/b;
.source "SourceFile"


# instance fields
.field private final a:Ljava/io/DataInputStream;

.field private final b:J

.field private final c:Ljava/lang/Class;

.field private d:J

.field private e:[B


# direct methods
.method public constructor <init>(Ljava/io/FileInputStream;JLjava/lang/Class;)V
    .locals 2

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/google/k/c/b;-><init>()V

    .line 161
    new-instance v0, Ljava/io/DataInputStream;

    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, p1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/e/e;->a:Ljava/io/DataInputStream;

    .line 162
    iput-wide p2, p0, Lcom/google/android/gms/icing/impl/e/e;->b:J

    .line 163
    iput-object p4, p0, Lcom/google/android/gms/icing/impl/e/e;->c:Ljava/lang/Class;

    .line 165
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/icing/impl/e/e;->d:J

    .line 166
    const/16 v0, 0x400

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/e/e;->e:[B

    .line 167
    return-void
.end method

.method private c()Lcom/google/protobuf/nano/j;
    .locals 12

    .prologue
    const-wide/16 v10, 0x8

    const-wide/16 v8, 0x4

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 172
    :try_start_0
    iget-wide v4, p0, Lcom/google/android/gms/icing/impl/e/e;->d:J

    iget-wide v6, p0, Lcom/google/android/gms/icing/impl/e/e;->b:J

    cmp-long v3, v4, v6

    if-gez v3, :cond_8

    .line 173
    iget-wide v4, p0, Lcom/google/android/gms/icing/impl/e/e;->b:J

    cmp-long v3, v4, v8

    if-gez v3, :cond_0

    const-string v0, "File too short to contain valid data"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    move-object v0, v1

    .line 174
    :goto_0
    if-eqz v0, :cond_7

    .line 183
    :goto_1
    return-object v0

    .line 173
    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/e/e;->a:Ljava/io/DataInputStream;

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    iget-wide v4, p0, Lcom/google/android/gms/icing/impl/e/e;->d:J

    add-long/2addr v4, v8

    iput-wide v4, p0, Lcom/google/android/gms/icing/impl/e/e;->d:J

    if-lez v3, :cond_1

    iget-wide v4, p0, Lcom/google/android/gms/icing/impl/e/e;->d:J

    int-to-long v6, v3

    add-long/2addr v4, v6

    add-long/2addr v4, v10

    iget-wide v6, p0, Lcom/google/android/gms/icing/impl/e/e;->b:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    :cond_1
    const-string v0, "Read bad message size: %d. (pos=%d, len=%d)"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v4, v5

    const/4 v3, 0x1

    iget-wide v6, p0, Lcom/google/android/gms/icing/impl/e/e;->d:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v3

    const/4 v3, 0x2

    iget-wide v6, p0, Lcom/google/android/gms/icing/impl/e/e;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {v0, v4}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v1

    goto :goto_0

    :cond_2
    if-lez v3, :cond_3

    :goto_2
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/e;->e:[B

    array-length v0, v0

    if-ge v0, v3, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/e;->e:[B

    array-length v0, v0

    :goto_3
    if-ge v0, v3, :cond_4

    mul-int/lit8 v0, v0, 0x2

    goto :goto_3

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/e/e;->e:[B

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/e;->a:Ljava/io/DataInputStream;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/e/e;->e:[B

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5, v3}, Ljava/io/DataInputStream;->read([BII)I

    iget-wide v4, p0, Lcom/google/android/gms/icing/impl/e/e;->d:J

    int-to-long v6, v3

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/gms/icing/impl/e/e;->d:J

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/e;->a:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/gms/icing/impl/e/e;->d:J

    add-long/2addr v6, v10

    iput-wide v6, p0, Lcom/google/android/gms/icing/impl/e/e;->d:J

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/e;->e:[B

    const/4 v6, 0x0

    invoke-static {v0, v6, v3, v4, v5}, Lcom/google/android/gms/icing/impl/e/d;->a([BIIJ)Z

    move-result v0

    if-nez v0, :cond_6

    move-object v0, v1

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/e;->e:[B

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/e/e;->c:Ljava/lang/Class;

    const/4 v5, 0x0

    invoke-static {v0, v5, v3, v4}, Lcom/google/android/gms/icing/impl/e/d;->a([BIILjava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    goto/16 :goto_0

    .line 177
    :cond_7
    const-string v0, "Failed to read a valid message from the file."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    :cond_8
    :goto_4
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/e/e;->b()Ljava/lang/Object;

    move-object v0, v1

    .line 183
    goto/16 :goto_1

    .line 179
    :catch_0
    move-exception v0

    const-string v3, "Exception while reading from file."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_4
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/e/e;->c()Lcom/google/protobuf/nano/j;

    move-result-object v0

    return-object v0
.end method
