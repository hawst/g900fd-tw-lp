.class final Lcom/google/android/gms/games/realtime/network/DcmStateMachine;
.super Lcom/google/android/gms/games/service/statemachine/h;
.source "SourceFile"


# static fields
.field protected static final h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/gms/games/c/a;->p:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->h:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/google/android/gms/games/realtime/network/a;)V
    .locals 3

    .prologue
    .line 48
    const-string v0, "DcmStateMachine"

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/statemachine/h;-><init>(Ljava/lang/String;)V

    .line 49
    new-instance v0, Lcom/google/android/gms/games/realtime/network/t;

    invoke-direct {v0}, Lcom/google/android/gms/games/realtime/network/t;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->a(Ljava/lang/Object;)V

    .line 50
    new-instance v0, Lcom/google/android/gms/games/realtime/network/u;

    invoke-direct {v0}, Lcom/google/android/gms/games/realtime/network/u;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/realtime/network/u;

    new-instance v1, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DefaultHandlerState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DefaultHandlerState;-><init>(Lcom/google/android/gms/games/realtime/network/DcmStateMachine;)V

    iput-object v1, v0, Lcom/google/android/gms/games/realtime/network/u;->a:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DefaultHandlerState;

    new-instance v1, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;-><init>(Lcom/google/android/gms/games/realtime/network/DcmStateMachine;)V

    iput-object v1, v0, Lcom/google/android/gms/games/realtime/network/u;->b:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;

    new-instance v1, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectingState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectingState;-><init>(Lcom/google/android/gms/games/realtime/network/DcmStateMachine;)V

    iput-object v1, v0, Lcom/google/android/gms/games/realtime/network/u;->c:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectingState;

    new-instance v1, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;-><init>(Lcom/google/android/gms/games/realtime/network/DcmStateMachine;)V

    iput-object v1, v0, Lcom/google/android/gms/games/realtime/network/u;->d:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;

    new-instance v1, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;-><init>(Lcom/google/android/gms/games/realtime/network/DcmStateMachine;)V

    iput-object v1, v0, Lcom/google/android/gms/games/realtime/network/u;->e:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;

    new-instance v1, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectingState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectingState;-><init>(Lcom/google/android/gms/games/realtime/network/DcmStateMachine;)V

    iput-object v1, v0, Lcom/google/android/gms/games/realtime/network/u;->f:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectingState;

    .line 51
    invoke-virtual {p0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/realtime/network/t;

    .line 52
    iput p2, v0, Lcom/google/android/gms/games/realtime/network/t;->b:I

    .line 53
    new-instance v1, Lcom/google/android/gms/games/jingle/Libjingle;

    iget-object v2, p0, Lcom/google/android/gms/common/util/a/c;->f:Lcom/google/android/gms/common/util/a/f;

    invoke-direct {v1, p1, v2}, Lcom/google/android/gms/games/jingle/Libjingle;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v1, v0, Lcom/google/android/gms/games/realtime/network/t;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    .line 54
    iget-object v1, v0, Lcom/google/android/gms/games/realtime/network/t;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-static {}, Lcom/google/android/gms/games/internal/ej;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lcom/google/android/gms/games/jingle/Libjingle;->a(Ljava/lang/String;I)V

    .line 55
    iput-object p3, v0, Lcom/google/android/gms/games/realtime/network/t;->c:Lcom/google/android/gms/games/realtime/network/a;

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/u;

    iget-object v1, v0, Lcom/google/android/gms/games/realtime/network/u;->b:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;

    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/u;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/u;->a:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/u;

    iget-object v1, v0, Lcom/google/android/gms/games/realtime/network/u;->c:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectingState;

    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/u;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/u;->a:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectingState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/u;

    iget-object v1, v0, Lcom/google/android/gms/games/realtime/network/u;->d:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;

    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/u;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/u;->a:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/u;

    iget-object v1, v0, Lcom/google/android/gms/games/realtime/network/u;->e:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;

    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/u;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/u;->a:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/u;

    iget-object v1, v0, Lcom/google/android/gms/games/realtime/network/u;->f:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectingState;

    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/u;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/u;->a:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectingState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/u;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/u;->b:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;->f()V

    .line 62
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->h:Z

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->b(Z)V

    .line 63
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->h:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x1000

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->a(I)V

    .line 64
    return-void

    .line 63
    :cond_0
    const/16 v0, 0x80

    goto :goto_0
.end method


# virtual methods
.method protected final b(I)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 78
    sparse-switch p1, :sswitch_data_0

    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 83
    :cond_0
    :goto_1
    return-object v0

    .line 78
    :sswitch_0
    const-string v0, "XMPP_SESSION_STANZA_OP"

    goto :goto_0

    :sswitch_1
    const-string v0, "SIGNIN_STATE_CHANGED_OP"

    goto :goto_0

    :sswitch_2
    const-string v0, "CALL_STATE_CHANGED_OP"

    goto :goto_0

    :sswitch_3
    const-string v0, "STATS_OP"

    goto :goto_0

    :sswitch_4
    const-string v0, "ENDPOINT_EVENT_OP"

    goto :goto_0

    :sswitch_5
    const-string v0, "MEDIA_SOURCES_OP"

    goto :goto_0

    :sswitch_6
    const-string v0, "AUDIO_LEVELS_OP"

    goto :goto_0

    :sswitch_7
    const-string v0, "IBB_DATA_RECEIVE"

    goto :goto_0

    :sswitch_8
    const-string v0, "IBB_SEND_RESULT"

    goto :goto_0

    :sswitch_9
    const-string v0, "BUZZ_SUBSCRIPTION_RESULT"

    goto :goto_0

    :sswitch_a
    const-string v0, "BUZZ_NOTIF_RECEIVE"

    goto :goto_0

    :sswitch_b
    const-string v0, "P2P_DATA_CONNECTION_RESULT"

    goto :goto_0

    :sswitch_c
    const-string v0, "P2P_DATA_RECEIVE"

    goto :goto_0

    :sswitch_d
    const-string v0, "DIRECTED_PRESENCE_RECEIPT"

    goto :goto_0

    .line 80
    :cond_1
    packed-switch p1, :pswitch_data_0

    move-object v0, v1

    :goto_2
    if-nez v0, :cond_0

    .line 83
    invoke-super {p0, p1}, Lcom/google/android/gms/games/service/statemachine/h;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 80
    :pswitch_0
    const-string v0, "CLEANUP"

    goto :goto_2

    :pswitch_1
    const-string v0, "PREPARE_NETWORK"

    goto :goto_2

    :pswitch_2
    const-string v0, "TEAR_DOWN"

    goto :goto_2

    :pswitch_3
    const-string v0, "GET_DIAGNOSTICS_FOR_PEER"

    goto :goto_2

    :pswitch_4
    const-string v0, "CREATE_NATIVE_SOCKET"

    goto :goto_2

    :pswitch_5
    const-string v0, "CREATE_OBSOLETE_SOCKET"

    goto :goto_2

    :pswitch_6
    const-string v0, "SEND_RELIABLE"

    goto :goto_2

    :pswitch_7
    const-string v0, "SEND_UNRELIABLE"

    goto :goto_2

    :pswitch_8
    const-string v0, "CONNECT_PEER"

    goto :goto_2

    :pswitch_9
    const-string v0, "DISCONNECT_PEER"

    goto :goto_2

    :pswitch_a
    const-string v0, "REGISTER_BUZZBOT"

    goto :goto_2

    :pswitch_b
    const-string v0, "UNREGISTER_BUZZBOT"

    goto :goto_2

    :pswitch_c
    const-string v0, "SOCKET_PROXY_CLOSED_SOCKET"

    goto :goto_2

    .line 78
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_0
        0x65 -> :sswitch_7
        0x66 -> :sswitch_8
        0xc9 -> :sswitch_9
        0xca -> :sswitch_a
        0x12d -> :sswitch_b
        0x12e -> :sswitch_c
        0x191 -> :sswitch_d
    .end sparse-switch

    .line 80
    :pswitch_data_0
    .packed-switch 0x1771
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method
