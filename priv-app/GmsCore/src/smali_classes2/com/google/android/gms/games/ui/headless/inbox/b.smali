.class public final Lcom/google/android/gms/games/ui/headless/inbox/b;
.super Lcom/google/android/gms/games/ui/common/quests/i;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/b/a/o;


# instance fields
.field private final a:Lcom/google/android/gms/games/ui/headless/a;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/headless/a;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/quests/i;-><init>()V

    .line 33
    invoke-static {p1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 34
    iput-object p1, p0, Lcom/google/android/gms/games/ui/headless/inbox/b;->a:Lcom/google/android/gms/games/ui/headless/a;

    .line 35
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/headless/a;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/inbox/b;->b:Ljava/lang/String;

    .line 36
    return-void
.end method

.method private c(Lcom/google/android/gms/games/quest/Quest;)V
    .locals 6

    .prologue
    .line 74
    invoke-interface {p1}, Lcom/google/android/gms/games/quest/Quest;->l()Lcom/google/android/gms/games/Game;

    move-result-object v1

    .line 77
    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/inbox/b;->a:Lcom/google/android/gms/games/ui/headless/a;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/headless/inbox/b;->b:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    invoke-static {v1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    invoke-static {p1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v5, "quest"

    invoke-interface {p1}, Lcom/google/android/gms/games/quest/Quest;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-static {v4, v3}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    invoke-static {v2, v1, v4}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/inbox/b;->a:Lcom/google/android/gms/games/ui/headless/a;

    const/16 v1, 0x4e21

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/headless/a;->setResult(I)V

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/inbox/b;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->finish()V

    .line 80
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/quest/Quest;)V
    .locals 3

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/inbox/b;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/inbox/b;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v1, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    const-string v0, "HeadlessQuestHelper"

    const-string v1, "onPlayQuestClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :goto_0
    return-void

    .line 48
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/quest/Quest;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/quest/Quest;

    .line 51
    invoke-interface {v0}, Lcom/google/android/gms/games/quest/Quest;->l()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v2

    .line 52
    invoke-static {v1, v2}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 54
    if-nez v1, :cond_1

    .line 57
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/inbox/b;->b(Lcom/google/android/gms/games/quest/Quest;)V

    goto :goto_0

    .line 58
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/inbox/b;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 64
    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/inbox/b;->b:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/ui/b/a/n;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/quest/Quest;)Lcom/google/android/gms/games/ui/b/a/n;

    move-result-object v0

    .line 67
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/inbox/b;->a:Lcom/google/android/gms/games/ui/headless/a;

    const-string v2, "com.google.android.gms.games.ui.dialog.changeAccountDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    goto :goto_0

    .line 69
    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/headless/inbox/b;->c(Lcom/google/android/gms/games/quest/Quest;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/games/quest/Quest;)V
    .locals 3

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/inbox/b;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/inbox/b;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87
    const-string v0, "HeadlessQuestHelper"

    const-string v1, "switchAccountForQuest: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :goto_0
    return-void

    .line 91
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/quest/Quest;->l()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v1

    .line 92
    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/inbox/b;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/headless/inbox/b;->c(Lcom/google/android/gms/games/quest/Quest;)V

    goto :goto_0
.end method
