.class public final Lcom/google/android/gms/icing/s;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile f:[Lcom/google/android/gms/icing/s;


# instance fields
.field public a:J

.field public b:J

.field public c:J

.field public d:Z

.field public e:Lcom/google/android/gms/icing/aa;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 12317
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 12318
    iput-wide v0, p0, Lcom/google/android/gms/icing/s;->a:J

    iput-wide v0, p0, Lcom/google/android/gms/icing/s;->b:J

    iput-wide v0, p0, Lcom/google/android/gms/icing/s;->c:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/s;->d:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/s;->e:Lcom/google/android/gms/icing/aa;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/s;->cachedSize:I

    .line 12319
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/s;
    .locals 2

    .prologue
    .line 12291
    sget-object v0, Lcom/google/android/gms/icing/s;->f:[Lcom/google/android/gms/icing/s;

    if-nez v0, :cond_1

    .line 12292
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 12294
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/s;->f:[Lcom/google/android/gms/icing/s;

    if-nez v0, :cond_0

    .line 12295
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/s;

    sput-object v0, Lcom/google/android/gms/icing/s;->f:[Lcom/google/android/gms/icing/s;

    .line 12297
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 12299
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/s;->f:[Lcom/google/android/gms/icing/s;

    return-object v0

    .line 12297
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 12402
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 12403
    iget-wide v2, p0, Lcom/google/android/gms/icing/s;->a:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 12404
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/icing/s;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 12407
    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/icing/s;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 12408
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/icing/s;->b:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 12411
    :cond_1
    iget-wide v2, p0, Lcom/google/android/gms/icing/s;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 12412
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/icing/s;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 12415
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/icing/s;->d:Z

    if-eqz v1, :cond_3

    .line 12416
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/gms/icing/s;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12419
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/s;->e:Lcom/google/android/gms/icing/aa;

    if-eqz v1, :cond_4

    .line 12420
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/icing/s;->e:Lcom/google/android/gms/icing/aa;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12423
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 12333
    if-ne p1, p0, :cond_1

    .line 12361
    :cond_0
    :goto_0
    return v0

    .line 12336
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/s;

    if-nez v2, :cond_2

    move v0, v1

    .line 12337
    goto :goto_0

    .line 12339
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/s;

    .line 12340
    iget-wide v2, p0, Lcom/google/android/gms/icing/s;->a:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/s;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 12341
    goto :goto_0

    .line 12343
    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/icing/s;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/s;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    .line 12344
    goto :goto_0

    .line 12346
    :cond_4
    iget-wide v2, p0, Lcom/google/android/gms/icing/s;->c:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/s;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move v0, v1

    .line 12347
    goto :goto_0

    .line 12349
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/gms/icing/s;->d:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/s;->d:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 12350
    goto :goto_0

    .line 12352
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/icing/s;->e:Lcom/google/android/gms/icing/aa;

    if-nez v2, :cond_7

    .line 12353
    iget-object v2, p1, Lcom/google/android/gms/icing/s;->e:Lcom/google/android/gms/icing/aa;

    if-eqz v2, :cond_0

    move v0, v1

    .line 12354
    goto :goto_0

    .line 12357
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/icing/s;->e:Lcom/google/android/gms/icing/aa;

    iget-object v3, p1, Lcom/google/android/gms/icing/s;->e:Lcom/google/android/gms/icing/aa;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/aa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 12358
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 12366
    iget-wide v0, p0, Lcom/google/android/gms/icing/s;->a:J

    iget-wide v2, p0, Lcom/google/android/gms/icing/s;->a:J

    ushr-long/2addr v2, v6

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    .line 12369
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/s;->b:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/s;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 12371
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/s;->c:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/s;->c:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 12373
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/icing/s;->d:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v1

    .line 12374
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/s;->e:Lcom/google/android/gms/icing/aa;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    add-int/2addr v0, v1

    .line 12376
    return v0

    .line 12373
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0

    .line 12374
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/s;->e:Lcom/google/android/gms/icing/aa;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/aa;->hashCode()I

    move-result v0

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 12285
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/s;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/s;->b:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/s;->c:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/s;->d:Z

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/gms/icing/s;->e:Lcom/google/android/gms/icing/aa;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/icing/aa;

    invoke-direct {v0}, Lcom/google/android/gms/icing/aa;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/s;->e:Lcom/google/android/gms/icing/aa;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/s;->e:Lcom/google/android/gms/icing/aa;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 12382
    iget-wide v0, p0, Lcom/google/android/gms/icing/s;->a:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 12383
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/icing/s;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 12385
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/icing/s;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 12386
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/icing/s;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 12388
    :cond_1
    iget-wide v0, p0, Lcom/google/android/gms/icing/s;->c:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 12389
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/icing/s;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 12391
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/icing/s;->d:Z

    if-eqz v0, :cond_3

    .line 12392
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/gms/icing/s;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 12394
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/s;->e:Lcom/google/android/gms/icing/aa;

    if-eqz v0, :cond_4

    .line 12395
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/icing/s;->e:Lcom/google/android/gms/icing/aa;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 12397
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 12398
    return-void
.end method
