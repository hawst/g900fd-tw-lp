.class public final Lcom/google/android/gms/games/ui/signin/c;
.super Lcom/google/android/gms/games/ui/signin/g;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/i/p;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/g;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x5

    return v0
.end method

.method public final a(ILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/c;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    :goto_0
    return-void

    .line 45
    :cond_0
    if-nez p1, :cond_1

    .line 47
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/c;->b(I)V

    goto :goto_0

    .line 51
    :cond_1
    if-eqz p2, :cond_2

    .line 52
    const/16 v0, 0x11

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/ui/signin/c;->b(II)V

    .line 53
    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0}, Lcom/google/android/gms/games/ui/signin/c;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 59
    :cond_2
    const/16 v0, 0x12

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/ui/signin/c;->b(II)V

    .line 60
    invoke-static {p1}, Lcom/google/android/gms/games/ui/signin/c;->c(I)I

    move-result v0

    .line 61
    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/ui/signin/c;->a(II)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/i/b;)V
    .locals 5

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/games/i/b;->c()V

    :try_start_0
    iget-object v1, p1, Lcom/google/android/gms/games/i/b;->f:Lcom/google/android/gms/games/internal/ea;

    new-instance v2, Lcom/google/android/gms/games/i/g;

    invoke-direct {v2, p1, p0}, Lcom/google/android/gms/games/i/g;-><init>(Lcom/google/android/gms/games/i/b;Lcom/google/android/gms/games/i/p;)V

    iget-object v3, p1, Lcom/google/android/gms/games/i/b;->d:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/gms/games/i/b;->e:[Ljava/lang/String;

    invoke-interface {v1, v2, v0, v3, v4}, Lcom/google/android/gms/games/internal/ea;->a(Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    :goto_0
    return-void

    .line 35
    :catch_0
    move-exception v0

    const-string v0, "SignInClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 30
    const/16 v0, 0x8

    return v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 66
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/signin/g;->onActivityResult(IILandroid/content/Intent;)V

    .line 67
    if-nez p1, :cond_0

    .line 68
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 70
    const/16 v0, 0x13

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/c;->e(I)V

    .line 71
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/c;->b(I)V

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    const/16 v0, 0x15

    .line 79
    if-nez p2, :cond_2

    .line 80
    const/16 v0, 0x14

    .line 82
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/c;->e(I)V

    .line 83
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/c;->b(I)V

    goto :goto_0
.end method
