.class public final Lcom/google/android/gms/icing/impl/bf;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Ljava/lang/String;

.field static final b:Ljava/lang/String;

.field private static final c:[Lcom/google/android/gms/common/download/f;


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/gms/icing/impl/q;

.field private final f:Lcom/google/android/gms/icing/impl/m;

.field private final g:Lcom/google/android/gms/common/download/f;

.field private final h:Ljava/io/File;

.field private final i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 30
    sget-object v0, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-static {v0, v9}, Lcom/google/android/gms/icing/impl/bf;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/impl/bf;->a:Ljava/lang/String;

    .line 31
    sget-object v0, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-static {v0, v9}, Lcom/google/android/gms/icing/impl/bf;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/impl/bf;->b:Ljava/lang/String;

    .line 39
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/gms/common/download/f;

    const/4 v8, 0x0

    new-instance v1, Lcom/google/android/gms/common/download/f;

    const-string v2, "armeabi"

    invoke-static {v2, v9}, Lcom/google/android/gms/icing/impl/bf;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "https://redirector.gvt1.com/edgedl/android/appdatasearch/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "armeabi"

    const-string v5, "6"

    invoke-static {v4, v5}, Lcom/google/android/gms/icing/impl/bf;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-wide/32 v4, 0x576580

    const-string v6, "ef855c23e5c2e5071dbb51f7a7046eee8a7f6855"

    const-string v7, ""

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/common/download/f;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    aput-object v1, v0, v8

    const/4 v8, 0x1

    new-instance v1, Lcom/google/android/gms/common/download/f;

    const-string v2, "armeabi-v7a"

    invoke-static {v2, v9}, Lcom/google/android/gms/icing/impl/bf;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "https://redirector.gvt1.com/edgedl/android/appdatasearch/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "armeabi-v7a"

    const-string v5, "6"

    invoke-static {v4, v5}, Lcom/google/android/gms/icing/impl/bf;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-wide/32 v4, 0x574588

    const-string v6, "7c70843870d3cd38d9503867dc30817282c59e05"

    const-string v7, ""

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/common/download/f;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    aput-object v1, v0, v8

    const/4 v8, 0x2

    new-instance v1, Lcom/google/android/gms/common/download/f;

    const-string v2, "mips"

    invoke-static {v2, v9}, Lcom/google/android/gms/icing/impl/bf;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "https://redirector.gvt1.com/edgedl/android/appdatasearch/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "mips"

    const-string v5, "6"

    invoke-static {v4, v5}, Lcom/google/android/gms/icing/impl/bf;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-wide/32 v4, 0x5db8d4

    const-string v6, "7327154bf9401336f53ba93bdb1394a6b17dc30a"

    const-string v7, ""

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/common/download/f;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    aput-object v1, v0, v8

    const/4 v8, 0x3

    new-instance v1, Lcom/google/android/gms/common/download/f;

    const-string v2, "x86"

    invoke-static {v2, v9}, Lcom/google/android/gms/icing/impl/bf;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "https://redirector.gvt1.com/edgedl/android/appdatasearch/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "x86"

    const-string v5, "6"

    invoke-static {v4, v5}, Lcom/google/android/gms/icing/impl/bf;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-wide/32 v4, 0x5b951c

    const-string v6, "e40fb4cef406259bba4e94eb9b11dba85ae455b5"

    const-string v7, ""

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/common/download/f;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    aput-object v1, v0, v8

    const/4 v8, 0x4

    new-instance v1, Lcom/google/android/gms/common/download/f;

    const-string v2, "arm64-v8a"

    invoke-static {v2, v9}, Lcom/google/android/gms/icing/impl/bf;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "https://redirector.gvt1.com/edgedl/android/appdatasearch/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "arm64-v8a"

    const-string v5, "6"

    invoke-static {v4, v5}, Lcom/google/android/gms/icing/impl/bf;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-wide/32 v4, 0x5b5680

    const-string v6, "00555a62b7cf3a03c6856c836b56232abc188919"

    const-string v7, ""

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/common/download/f;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    aput-object v1, v0, v8

    const/4 v8, 0x5

    new-instance v1, Lcom/google/android/gms/common/download/f;

    const-string v2, "x86_64"

    invoke-static {v2, v9}, Lcom/google/android/gms/icing/impl/bf;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "https://redirector.gvt1.com/edgedl/android/appdatasearch/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "x86_64"

    const-string v5, "6"

    invoke-static {v4, v5}, Lcom/google/android/gms/icing/impl/bf;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-wide/32 v4, 0x5c77a0

    const-string v6, "32a7434be80d4e2e3b540e1c64feae3624ef3472"

    const-string v7, ""

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/common/download/f;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    aput-object v1, v0, v8

    sput-object v0, Lcom/google/android/gms/icing/impl/bf;->c:[Lcom/google/android/gms/common/download/f;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/icing/impl/q;Lcom/google/android/gms/icing/impl/m;)V
    .locals 3

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/bf;->d:Landroid/content/Context;

    .line 89
    iput-object p2, p0, Lcom/google/android/gms/icing/impl/bf;->e:Lcom/google/android/gms/icing/impl/q;

    .line 90
    iput-object p3, p0, Lcom/google/android/gms/icing/impl/bf;->f:Lcom/google/android/gms/icing/impl/m;

    .line 91
    invoke-static {}, Lcom/google/android/gms/icing/impl/bf;->g()Lcom/google/android/gms/common/download/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/bf;->g:Lcom/google/android/gms/common/download/f;

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bf;->g:Lcom/google/android/gms/common/download/f;

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/bf;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/icing/impl/bf;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/icing/impl/bf;->h:Ljava/io/File;

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bf;->h:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/bf;->i:Ljava/lang/String;

    .line 97
    return-void

    .line 93
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/bf;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/bf;->g:Lcom/google/android/gms/common/download/f;

    iget-object v2, v2, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 132
    if-nez p0, :cond_0

    .line 133
    const/4 v0, 0x0

    .line 136
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "libAppDataSearchExt_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x2d

    const/16 v2, 0x5f

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p1, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ".v"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".so"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method private static a(Lcom/google/android/gms/icing/b;Ljava/io/PrintWriter;)V
    .locals 4

    .prologue
    .line 245
    if-nez p0, :cond_0

    .line 246
    const-string v0, "\tnull\n"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 252
    :goto_0
    return-void

    .line 248
    :cond_0
    const-string v0, "\tv%d policy %d min %d/%d\n"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/gms/icing/b;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/gms/icing/b;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/gms/icing/b;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const v3, 0x6768a8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    goto :goto_0
.end method

.method private static g()Lcom/google/android/gms/common/download/f;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 116
    sget-object v3, Lcom/google/android/gms/icing/impl/bf;->c:[Lcom/google/android/gms/common/download/f;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v0, v3, v2

    .line 117
    iget-object v5, v0, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    sget-object v6, Lcom/google/android/gms/icing/impl/bf;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 128
    :cond_0
    :goto_1
    return-object v0

    .line 116
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 122
    :cond_2
    sget-object v2, Lcom/google/android/gms/icing/impl/bf;->c:[Lcom/google/android/gms/common/download/f;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v0, v2, v1

    .line 123
    iget-object v4, v0, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    sget-object v5, Lcom/google/android/gms/icing/impl/bf;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 122
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 127
    :cond_3
    const-string v0, "Unrecognized CPU_ABIs for extension: %s, %s"

    sget-object v1, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    sget-object v2, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 128
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static h()Z
    .locals 1

    .prologue
    .line 316
    sget-object v0, Lcom/google/android/gms/icing/a/a;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()Ljava/io/File;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bf;->h:Ljava/io/File;

    return-object v0
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 3

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bf;->i:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->b(Ljava/lang/String;)Lcom/google/android/gms/icing/b;

    move-result-object v0

    .line 261
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/bf;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/q;->v()Lcom/google/android/gms/icing/b;

    move-result-object v1

    .line 263
    const-string v2, "Enabled: "

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 264
    invoke-static {}, Lcom/google/android/gms/icing/impl/bf;->h()Z

    move-result v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Z)V

    .line 265
    const-string v2, "\n"

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 266
    const-string v2, "Downloaded:\n"

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 267
    invoke-static {v0, p1}, Lcom/google/android/gms/icing/impl/bf;->a(Lcom/google/android/gms/icing/b;Ljava/io/PrintWriter;)V

    .line 268
    const-string v0, "Current:\n"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 269
    invoke-static {v1, p1}, Lcom/google/android/gms/icing/impl/bf;->a(Lcom/google/android/gms/icing/b;Ljava/io/PrintWriter;)V

    .line 270
    return-void
.end method

.method public final a(Z)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 150
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/bf;->i:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->b(Ljava/lang/String;)Lcom/google/android/gms/icing/b;

    move-result-object v1

    .line 152
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 154
    invoke-static {}, Lcom/google/android/gms/icing/impl/bf;->h()Z

    move-result v4

    .line 155
    if-eqz v4, :cond_3

    .line 156
    iget-object v5, p0, Lcom/google/android/gms/icing/impl/bf;->g:Lcom/google/android/gms/common/download/f;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/bf;->d:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/gms/icing/impl/bf;->g:Lcom/google/android/gms/common/download/f;

    invoke-static {v5, v6}, Lcom/google/android/gms/common/download/DownloadService;->a(Landroid/content/Context;Lcom/google/android/gms/common/download/f;)Z

    .line 160
    :cond_0
    :goto_0
    if-eqz p1, :cond_5

    if-eqz v4, :cond_5

    .line 161
    if-eqz v1, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/bf;->i:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 163
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/bf;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/q;->v()Lcom/google/android/gms/icing/b;

    move-result-object v2

    .line 164
    if-eqz v2, :cond_1

    iget v2, v2, Lcom/google/android/gms/icing/b;->a:I

    iget v3, v1, Lcom/google/android/gms/icing/b;->a:I

    if-eq v2, v3, :cond_2

    .line 165
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/bf;->f:Lcom/google/android/gms/icing/impl/m;

    const-string v3, "ext_state_change"

    invoke-interface {v2, v3}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    .line 166
    iget v2, v1, Lcom/google/android/gms/icing/b;->b:I

    packed-switch v2, :pswitch_data_0

    .line 176
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/bf;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/icing/impl/q;->a(Lcom/google/android/gms/icing/b;)V

    .line 196
    :cond_2
    :goto_1
    :pswitch_0
    return v0

    .line 158
    :cond_3
    iget-object v5, p0, Lcom/google/android/gms/icing/impl/bf;->g:Lcom/google/android/gms/common/download/f;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/bf;->d:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/gms/icing/impl/bf;->g:Lcom/google/android/gms/common/download/f;

    iget-object v6, v6, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/android/gms/common/download/DownloadService;->d(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 169
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_1

    .line 181
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/bf;->f:Lcom/google/android/gms/icing/impl/m;

    const-string v2, "ext_download_enabled"

    invoke-interface {v1, v2}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 186
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/bf;->h:Ljava/io/File;

    .line 187
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 188
    iget-object v4, p0, Lcom/google/android/gms/icing/impl/bf;->f:Lcom/google/android/gms/icing/impl/m;

    const-string v5, "ext_download_disabled"

    invoke-interface {v4, v5}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    .line 189
    iget-object v4, p0, Lcom/google/android/gms/icing/impl/bf;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/q;->w()V

    .line 190
    iget-object v4, p0, Lcom/google/android/gms/icing/impl/bf;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v4, v2, v3}, Lcom/google/android/gms/icing/impl/q;->a(J)V

    .line 191
    invoke-static {}, Lcom/google/android/gms/icing/impl/NativeIndex;->a()V

    .line 192
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 166
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bf;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Z)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 208
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/bf;->i:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->b(Ljava/lang/String;)Lcom/google/android/gms/icing/b;

    move-result-object v1

    .line 209
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/bf;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/q;->v()Lcom/google/android/gms/icing/b;

    move-result-object v2

    .line 211
    if-eqz v1, :cond_1

    if-eqz v2, :cond_0

    iget v2, v2, Lcom/google/android/gms/icing/b;->a:I

    iget v3, v1, Lcom/google/android/gms/icing/b;->a:I

    if-eq v2, v3, :cond_1

    .line 213
    :cond_0
    iget v2, v1, Lcom/google/android/gms/icing/b;->b:I

    packed-switch v2, :pswitch_data_0

    .line 223
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/bf;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/icing/impl/q;->a(Lcom/google/android/gms/icing/b;)V

    move p1, v0

    .line 228
    :goto_0
    :pswitch_0
    return p1

    .line 217
    :pswitch_1
    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    move p1, v0

    .line 228
    goto :goto_0

    .line 213
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bf;->g:Lcom/google/android/gms/common/download/f;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/icing/impl/bf;->a:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bf;->g:Lcom/google/android/gms/common/download/f;

    iget-object v0, v0, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bf;->i:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->b(Ljava/lang/String;)Lcom/google/android/gms/icing/b;

    move-result-object v0

    .line 237
    if-nez v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bf;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/q;->w()V

    .line 242
    :goto_0
    return-void

    .line 240
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/bf;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Lcom/google/android/gms/icing/b;)V

    goto :goto_0
.end method

.method public final e()Lcom/google/android/gms/icing/b;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bf;->i:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->b(Ljava/lang/String;)Lcom/google/android/gms/icing/b;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bf;->i:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->b(Ljava/lang/String;)Lcom/google/android/gms/icing/b;

    move-result-object v0

    .line 284
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/bf;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/q;->v()Lcom/google/android/gms/icing/b;

    move-result-object v1

    .line 285
    if-nez v0, :cond_0

    .line 286
    const/4 v0, 0x0

    .line 290
    :goto_0
    return v0

    .line 287
    :cond_0
    if-eqz v1, :cond_1

    iget v1, v1, Lcom/google/android/gms/icing/b;->a:I

    iget v0, v0, Lcom/google/android/gms/icing/b;->a:I

    if-eq v1, v0, :cond_2

    .line 288
    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    .line 290
    :cond_2
    const/4 v0, 0x3

    goto :goto_0
.end method
