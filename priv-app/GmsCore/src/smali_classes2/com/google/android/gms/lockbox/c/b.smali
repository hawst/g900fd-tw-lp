.class public abstract Lcom/google/android/gms/lockbox/c/b;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/lockbox/c/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 20
    const-string v0, "com.google.android.gms.lockbox.internal.ILockboxService"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/gms/lockbox/c/b;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 21
    return-void
.end method

.method public static a(Landroid/os/IBinder;)Lcom/google/android/gms/lockbox/c/a;
    .locals 2

    .prologue
    .line 28
    if-nez p0, :cond_0

    .line 29
    const/4 v0, 0x0

    .line 35
    :goto_0
    return-object v0

    .line 31
    :cond_0
    const-string v0, "com.google.android.gms.lockbox.internal.ILockboxService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 32
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/lockbox/c/a;

    if-eqz v1, :cond_1

    .line 33
    check-cast v0, Lcom/google/android/gms/lockbox/c/a;

    goto :goto_0

    .line 35
    :cond_1
    new-instance v0, Lcom/google/android/gms/lockbox/c/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/lockbox/c/c;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 43
    sparse-switch p1, :sswitch_data_0

    .line 138
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 47
    :sswitch_0
    const-string v0, "com.google.android.gms.lockbox.internal.ILockboxService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v1

    .line 48
    goto :goto_0

    .line 52
    :sswitch_1
    const-string v0, "com.google.android.gms.lockbox.internal.ILockboxService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 55
    invoke-virtual {p0, v0}, Lcom/google/android/gms/lockbox/c/b;->a(Ljava/lang/String;)Lcom/google/android/gms/lockbox/c/o;

    move-result-object v0

    .line 56
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 57
    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 59
    invoke-virtual {v0, p3, v1}, Lcom/google/android/gms/lockbox/c/o;->writeToParcel(Landroid/os/Parcel;I)V

    :goto_1
    move v0, v1

    .line 64
    goto :goto_0

    .line 62
    :cond_0
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1

    .line 68
    :sswitch_2
    const-string v2, "com.google.android.gms.lockbox.internal.ILockboxService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 70
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1

    .line 71
    sget-object v0, Lcom/google/android/gms/lockbox/c/o;->a:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/lockbox/c/o;

    .line 76
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/lockbox/c/b;->a(Lcom/google/android/gms/lockbox/c/o;)V

    .line 77
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 78
    goto :goto_0

    .line 82
    :sswitch_3
    const-string v2, "com.google.android.gms.lockbox.internal.ILockboxService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 84
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    if-nez v2, :cond_2

    .line 85
    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/gms/lockbox/c/b;->a(Lcom/google/android/gms/lockbox/c/d;)V

    .line 86
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 87
    goto :goto_0

    .line 84
    :cond_2
    const-string v0, "com.google.android.gms.lockbox.internal.ILockboxStatusChangedListener"

    invoke-interface {v2, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_3

    instance-of v3, v0, Lcom/google/android/gms/lockbox/c/d;

    if-eqz v3, :cond_3

    check-cast v0, Lcom/google/android/gms/lockbox/c/d;

    goto :goto_2

    :cond_3
    new-instance v0, Lcom/google/android/gms/lockbox/c/f;

    invoke-direct {v0, v2}, Lcom/google/android/gms/lockbox/c/f;-><init>(Landroid/os/IBinder;)V

    goto :goto_2

    .line 91
    :sswitch_4
    const-string v0, "com.google.android.gms.lockbox.internal.ILockboxService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 92
    invoke-virtual {p0}, Lcom/google/android/gms/lockbox/c/b;->a()V

    .line 93
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 94
    goto :goto_0

    .line 98
    :sswitch_5
    const-string v2, "com.google.android.gms.lockbox.internal.ILockboxService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 100
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 102
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_4

    .line 103
    sget-object v0, Lcom/google/android/gms/lockbox/g;->d:Lcom/google/android/gms/lockbox/i;

    invoke-static {p2}, Lcom/google/android/gms/lockbox/i;->a(Landroid/os/Parcel;)Lcom/google/android/gms/lockbox/g;

    move-result-object v0

    .line 108
    :cond_4
    invoke-virtual {p0, v2, v0}, Lcom/google/android/gms/lockbox/c/b;->a(Ljava/lang/String;Lcom/google/android/gms/lockbox/g;)V

    .line 109
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 110
    goto/16 :goto_0

    .line 114
    :sswitch_6
    const-string v0, "com.google.android.gms.lockbox.internal.ILockboxService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 116
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 118
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 119
    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/lockbox/c/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 121
    goto/16 :goto_0

    .line 125
    :sswitch_7
    const-string v0, "com.google.android.gms.lockbox.internal.ILockboxService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 126
    invoke-virtual {p0}, Lcom/google/android/gms/lockbox/c/b;->b()Lcom/google/android/gms/lockbox/c/q;

    move-result-object v0

    .line 127
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 128
    if-eqz v0, :cond_5

    .line 129
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 130
    invoke-virtual {v0, p3, v1}, Lcom/google/android/gms/lockbox/c/q;->writeToParcel(Landroid/os/Parcel;I)V

    :goto_3
    move v0, v1

    .line 135
    goto/16 :goto_0

    .line 133
    :cond_5
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_3

    .line 43
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0x7 -> :sswitch_6
        0x8 -> :sswitch_7
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
