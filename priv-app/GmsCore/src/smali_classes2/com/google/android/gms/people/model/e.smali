.class public final Lcom/google/android/gms/people/model/e;
.super Lcom/google/android/gms/people/model/f;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/model/f;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 18
    return-void
.end method


# virtual methods
.method public final synthetic a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/model/e;->b(I)Lcom/google/android/gms/people/model/d;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Lcom/google/android/gms/people/model/d;
    .locals 3

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/people/internal/d;

    iget-object v1, p0, Lcom/google/android/gms/people/model/e;->a:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {p0}, Lcom/google/android/gms/people/model/e;->e()Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/gms/people/internal/d;-><init>(Lcom/google/android/gms/common/data/DataHolder;ILandroid/os/Bundle;)V

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Circles:size="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/people/model/e;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
