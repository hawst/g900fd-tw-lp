.class public abstract Lcom/google/android/gms/common/app/a;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/Runnable;

.field private final b:Ljava/util/HashMap;

.field private c:I

.field private d:Landroid/os/Handler;


# direct methods
.method public constructor <init>(I)V
    .locals 3

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/common/app/a;->c:I

    .line 67
    new-instance v0, Lcom/google/android/gms/common/app/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/app/b;-><init>(Lcom/google/android/gms/common/app/a;)V

    iput-object v0, p0, Lcom/google/android/gms/common/app/a;->a:Ljava/lang/Runnable;

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/app/a;->b:Ljava/util/HashMap;

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/common/app/a;->b:Ljava/util/HashMap;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    return-void
.end method

.method public constructor <init>(Ljava/util/HashMap;)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/common/app/a;->c:I

    .line 67
    new-instance v0, Lcom/google/android/gms/common/app/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/app/b;-><init>(Lcom/google/android/gms/common/app/a;)V

    iput-object v0, p0, Lcom/google/android/gms/common/app/a;->a:Ljava/lang/Runnable;

    .line 62
    iput-object p1, p0, Lcom/google/android/gms/common/app/a;->b:Ljava/util/HashMap;

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/common/app/a;->b:Ljava/util/HashMap;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Must provide a default affinity pool!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 65
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/common/app/a;)I
    .locals 2

    .prologue
    .line 34
    iget v0, p0, Lcom/google/android/gms/common/app/a;->c:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/google/android/gms/common/app/a;->c:I

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/common/app/a;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/google/android/gms/common/app/a;->c:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/common/app/a;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/common/app/a;->d:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected abstract a(Landroid/content/Intent;)V
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 81
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/app/a;->d:Landroid/os/Handler;

    .line 82
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/common/app/a;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 87
    iget-object v2, p0, Lcom/google/android/gms/common/app/a;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    goto :goto_0

    .line 89
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 90
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 95
    const-string v0, "This can only be called in the Main thread!"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 96
    iget v0, p0, Lcom/google/android/gms/common/app/a;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/common/app/a;->c:I

    .line 97
    const-string v0, "intent_thread_affinity"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 98
    iget-object v1, p0, Lcom/google/android/gms/common/app/a;->b:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/gms/common/app/c;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/common/app/c;-><init>(Lcom/google/android/gms/common/app/a;Landroid/content/Intent;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 105
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0, p1, p3}, Lcom/google/android/gms/common/app/a;->onStart(Landroid/content/Intent;I)V

    .line 110
    const/4 v0, 0x2

    return v0
.end method
