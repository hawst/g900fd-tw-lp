.class public abstract Lcom/google/android/gms/common/internal/aj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/h;
.implements Lcom/google/android/gms/common/internal/as;


# static fields
.field public static final f:[Ljava/lang/String;


# instance fields
.field private a:Lcom/google/android/gms/common/internal/ClientSettings;

.field public final b:Landroid/content/Context;

.field final c:Landroid/os/Handler;

.field public final d:[Ljava/lang/String;

.field e:Z

.field private final g:Landroid/os/Looper;

.field private final h:Lcom/google/android/gms/common/internal/at;

.field private final i:Ljava/lang/Object;

.field private j:Landroid/os/IInterface;

.field private final k:Ljava/util/ArrayList;

.field private l:Lcom/google/android/gms/common/internal/ao;

.field private m:I

.field private final n:Lcom/google/android/gms/common/internal/ar;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 395
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "service_esmobile"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "service_googleme"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/common/internal/aj;->f:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 321
    iput-object v4, p0, Lcom/google/android/gms/common/internal/aj;->a:Lcom/google/android/gms/common/internal/ClientSettings;

    .line 341
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->i:Ljava/lang/Object;

    .line 357
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->k:Ljava/util/ArrayList;

    .line 370
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/common/internal/aj;->m:I

    .line 384
    iput-boolean v3, p0, Lcom/google/android/gms/common/internal/aj;->e:Z

    .line 484
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->b:Landroid/content/Context;

    .line 485
    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->g:Landroid/os/Looper;

    .line 486
    invoke-static {p1}, Lcom/google/android/gms/common/internal/at;->a(Landroid/content/Context;)Lcom/google/android/gms/common/internal/at;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->h:Lcom/google/android/gms/common/internal/at;

    .line 487
    new-instance v0, Lcom/google/android/gms/common/internal/ar;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/aj;->g:Landroid/os/Looper;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/common/internal/ar;-><init>(Landroid/os/Looper;Lcom/google/android/gms/common/internal/as;)V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->n:Lcom/google/android/gms/common/internal/ar;

    .line 488
    new-instance v0, Lcom/google/android/gms/common/internal/ak;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/aj;->g:Landroid/os/Looper;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/common/internal/ak;-><init>(Lcom/google/android/gms/common/internal/aj;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->c:Landroid/os/Handler;

    .line 489
    new-array v0, v3, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->d:[Ljava/lang/String;

    .line 490
    new-instance v0, Lcom/google/android/gms/common/internal/ClientSettings;

    const-string v1, "TEST_ACCOUNT"

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    const-string v5, "TEST_PACKAGE"

    const-string v6, "TEST_CLIENT"

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/common/internal/ClientSettings;-><init>(Ljava/lang/String;Ljava/util/Collection;ILandroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->a:Lcom/google/android/gms/common/internal/ClientSettings;

    .line 492
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;Lcom/google/android/gms/common/internal/ClientSettings;)V
    .locals 7

    .prologue
    .line 525
    invoke-static {p1}, Lcom/google/android/gms/common/internal/at;->a(Landroid/content/Context;)Lcom/google/android/gms/common/internal/at;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p5

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/common/internal/aj;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/at;Lcom/google/android/gms/common/internal/ClientSettings;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V

    .line 531
    return-void
.end method

.method public varargs constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;[Ljava/lang/String;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 321
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->a:Lcom/google/android/gms/common/internal/ClientSettings;

    .line 341
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->i:Ljava/lang/Object;

    .line 357
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->k:Ljava/util/ArrayList;

    .line 370
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/common/internal/aj;->m:I

    .line 384
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/internal/aj;->e:Z

    .line 427
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->b:Landroid/content/Context;

    .line 428
    const-string v0, "Looper must not be null"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Looper;

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->g:Landroid/os/Looper;

    .line 429
    invoke-static {p1}, Lcom/google/android/gms/common/internal/at;->a(Landroid/content/Context;)Lcom/google/android/gms/common/internal/at;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->h:Lcom/google/android/gms/common/internal/at;

    .line 430
    new-instance v0, Lcom/google/android/gms/common/internal/ar;

    invoke-direct {v0, p2, p0}, Lcom/google/android/gms/common/internal/ar;-><init>(Landroid/os/Looper;Lcom/google/android/gms/common/internal/as;)V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->n:Lcom/google/android/gms/common/internal/ar;

    .line 434
    new-instance v0, Lcom/google/android/gms/common/internal/ak;

    invoke-direct {v0, p0, p2}, Lcom/google/android/gms/common/internal/ak;-><init>(Lcom/google/android/gms/common/internal/aj;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->c:Landroid/os/Handler;

    .line 435
    invoke-virtual {p0, p5}, Lcom/google/android/gms/common/internal/aj;->a([Ljava/lang/String;)V

    .line 436
    iput-object p5, p0, Lcom/google/android/gms/common/internal/aj;->d:[Ljava/lang/String;

    .line 438
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/x;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/internal/aj;->a(Lcom/google/android/gms/common/api/x;)V

    .line 439
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/y;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/internal/aj;->a(Lcom/google/android/gms/common/api/y;)V

    .line 440
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/at;Lcom/google/android/gms/common/internal/ClientSettings;)V
    .locals 1

    .prologue
    .line 558
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 321
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->a:Lcom/google/android/gms/common/internal/ClientSettings;

    .line 341
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->i:Ljava/lang/Object;

    .line 357
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->k:Ljava/util/ArrayList;

    .line 370
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/common/internal/aj;->m:I

    .line 384
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/internal/aj;->e:Z

    .line 559
    const-string v0, "Context must not be null"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->b:Landroid/content/Context;

    .line 560
    const-string v0, "Looper must not be null"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Looper;

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->g:Landroid/os/Looper;

    .line 561
    const-string v0, "Supervisor must not be null"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/internal/at;

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->h:Lcom/google/android/gms/common/internal/at;

    .line 562
    new-instance v0, Lcom/google/android/gms/common/internal/ar;

    invoke-direct {v0, p2, p0}, Lcom/google/android/gms/common/internal/ar;-><init>(Landroid/os/Looper;Lcom/google/android/gms/common/internal/as;)V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->n:Lcom/google/android/gms/common/internal/ar;

    .line 566
    new-instance v0, Lcom/google/android/gms/common/internal/ak;

    invoke-direct {v0, p0, p2}, Lcom/google/android/gms/common/internal/ak;-><init>(Lcom/google/android/gms/common/internal/aj;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->c:Landroid/os/Handler;

    .line 568
    invoke-virtual {p4}, Lcom/google/android/gms/common/internal/ClientSettings;->a()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->d:[Ljava/lang/String;

    .line 569
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->d:[Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/internal/aj;->a([Ljava/lang/String;)V

    .line 570
    iput-object p4, p0, Lcom/google/android/gms/common/internal/aj;->a:Lcom/google/android/gms/common/internal/ClientSettings;

    .line 571
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/at;Lcom/google/android/gms/common/internal/ClientSettings;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V
    .locals 1

    .prologue
    .line 544
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/common/internal/aj;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/at;Lcom/google/android/gms/common/internal/ClientSettings;)V

    .line 546
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/x;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/internal/aj;->a(Lcom/google/android/gms/common/api/x;)V

    .line 547
    invoke-static {p6}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/y;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/internal/aj;->a(Lcom/google/android/gms/common/api/y;)V

    .line 548
    return-void
.end method

.method public varargs constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;[Ljava/lang/String;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 450
    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/common/internal/am;

    invoke-direct {v3, p2}, Lcom/google/android/gms/common/internal/am;-><init>(Lcom/google/android/gms/common/es;)V

    new-instance v4, Lcom/google/android/gms/common/internal/ap;

    invoke-direct {v4, p3}, Lcom/google/android/gms/common/internal/ap;-><init>(Lcom/google/android/gms/common/et;)V

    move-object v0, p0

    move-object v1, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/aj;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;[Ljava/lang/String;)V

    .line 452
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/common/internal/aj;)Lcom/google/android/gms/common/internal/ar;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->n:Lcom/google/android/gms/common/internal/ar;

    return-object v0
.end method

.method private a(ILandroid/os/IInterface;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 734
    const/4 v2, 0x3

    if-ne p1, v2, :cond_0

    move v3, v0

    :goto_0
    if-eqz p2, :cond_1

    move v2, v0

    :goto_1
    if-ne v3, v2, :cond_2

    :goto_2
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 735
    iget-object v1, p0, Lcom/google/android/gms/common/internal/aj;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 736
    :try_start_0
    iput p1, p0, Lcom/google/android/gms/common/internal/aj;->m:I

    .line 737
    iput-object p2, p0, Lcom/google/android/gms/common/internal/aj;->j:Landroid/os/IInterface;

    .line 738
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_0
    move v3, v1

    .line 734
    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    .line 738
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/common/internal/aj;I)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/common/internal/aj;->a(ILandroid/os/IInterface;)V

    return-void
.end method

.method private a(IILandroid/os/IInterface;)Z
    .locals 2

    .prologue
    .line 747
    iget-object v1, p0, Lcom/google/android/gms/common/internal/aj;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 748
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/common/internal/aj;->m:I

    if-eq v0, p1, :cond_0

    .line 749
    const/4 v0, 0x0

    monitor-exit v1

    .line 752
    :goto_0
    return v0

    .line 751
    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/common/internal/aj;->a(ILandroid/os/IInterface;)V

    .line 752
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 753
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/common/internal/aj;IILandroid/os/IInterface;)Z
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/common/internal/aj;->a(IILandroid/os/IInterface;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/common/internal/aj;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->k:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/common/internal/aj;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/common/internal/aj;)Lcom/google/android/gms/common/internal/ao;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->l:Lcom/google/android/gms/common/internal/ao;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/common/internal/aj;)Lcom/google/android/gms/common/internal/at;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->h:Lcom/google/android/gms/common/internal/at;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/common/internal/aj;)Lcom/google/android/gms/common/internal/ao;
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->l:Lcom/google/android/gms/common/internal/ao;

    return-object v0
.end method


# virtual methods
.method protected abstract a(Landroid/os/IBinder;)Landroid/os/IInterface;
.end method

.method public a()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x3

    const/4 v1, 0x1

    .line 791
    iput-boolean v1, p0, Lcom/google/android/gms/common/internal/aj;->e:Z

    .line 793
    const/4 v0, 0x2

    invoke-direct {p0, v0, v2}, Lcom/google/android/gms/common/internal/aj;->a(ILandroid/os/IInterface;)V

    .line 795
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/ew;->a(Landroid/content/Context;)I

    move-result v0

    .line 796
    if-eqz v0, :cond_1

    .line 799
    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/common/internal/aj;->a(ILandroid/os/IInterface;)V

    .line 800
    iget-object v1, p0, Lcom/google/android/gms/common/internal/aj;->c:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/gms/common/internal/aj;->c:Landroid/os/Handler;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 823
    :cond_0
    :goto_0
    return-void

    .line 805
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->l:Lcom/google/android/gms/common/internal/ao;

    if-eqz v0, :cond_2

    .line 806
    const-string v0, "GmsClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Calling connect() while still connected, missing disconnect() for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/aj;->a_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 808
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->h:Lcom/google/android/gms/common/internal/at;

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/aj;->a_()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/internal/aj;->l:Lcom/google/android/gms/common/internal/ao;

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/aj;->g_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/internal/at;->b(Ljava/lang/String;Landroid/content/ServiceConnection;Ljava/lang/String;)V

    .line 811
    :cond_2
    new-instance v0, Lcom/google/android/gms/common/internal/ao;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/internal/ao;-><init>(Lcom/google/android/gms/common/internal/aj;)V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aj;->l:Lcom/google/android/gms/common/internal/ao;

    .line 812
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->h:Lcom/google/android/gms/common/internal/at;

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/aj;->a_()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/internal/aj;->l:Lcom/google/android/gms/common/internal/ao;

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/aj;->g_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/internal/at;->a(Ljava/lang/String;Landroid/content/ServiceConnection;Ljava/lang/String;)Z

    move-result v0

    .line 816
    if-nez v0, :cond_0

    .line 817
    const-string v0, "GmsClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unable to connect to service: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/aj;->a_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 818
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/aj;->c:Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 933
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/aj;->c:Landroid/os/Handler;

    const/4 v2, 0x1

    new-instance v3, Lcom/google/android/gms/common/internal/aq;

    invoke-direct {v3, p0, p1, p2, p3}, Lcom/google/android/gms/common/internal/aq;-><init>(Lcom/google/android/gms/common/internal/aj;ILandroid/os/IBinder;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 935
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/x;)V
    .locals 1

    .prologue
    .line 1033
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->n:Lcom/google/android/gms/common/internal/ar;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/ar;->a(Lcom/google/android/gms/common/api/x;)V

    .line 1034
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/y;)V
    .locals 1

    .prologue
    .line 1039
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->n:Lcom/google/android/gms/common/internal/ar;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/ar;->a(Lcom/google/android/gms/common/et;)V

    .line 1040
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/es;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1045
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->n:Lcom/google/android/gms/common/internal/ar;

    new-instance v1, Lcom/google/android/gms/common/internal/am;

    invoke-direct {v1, p1}, Lcom/google/android/gms/common/internal/am;-><init>(Lcom/google/android/gms/common/es;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/ar;->a(Lcom/google/android/gms/common/api/x;)V

    .line 1046
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/et;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1063
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->n:Lcom/google/android/gms/common/internal/ar;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/ar;->a(Lcom/google/android/gms/common/et;)V

    .line 1064
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/internal/al;)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1013
    iget-object v1, p0, Lcom/google/android/gms/common/internal/aj;->k:Ljava/util/ArrayList;

    monitor-enter v1

    .line 1014
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1015
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1016
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/aj;->c:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1017
    return-void

    .line 1015
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected abstract a(Lcom/google/android/gms/common/internal/bj;Lcom/google/android/gms/common/internal/an;)V
.end method

.method public final a(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 4

    .prologue
    .line 1081
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "GmsClient:"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1083
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1084
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v1

    const-string v2, "mStartServiceAction="

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/aj;->a_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1089
    iget-object v1, p0, Lcom/google/android/gms/common/internal/aj;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 1090
    :try_start_0
    iget v2, p0, Lcom/google/android/gms/common/internal/aj;->m:I

    .line 1091
    iget-object v3, p0, Lcom/google/android/gms/common/internal/aj;->j:Landroid/os/IInterface;

    .line 1092
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1094
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "mConnectState="

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 1095
    packed-switch v2, :pswitch_data_0

    .line 1109
    const-string v0, "UNKNOWN"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1111
    :goto_0
    const-string v0, " mService="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 1112
    if-nez v3, :cond_0

    .line 1113
    const-string v0, "null"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1118
    :goto_1
    return-void

    .line 1092
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1097
    :pswitch_0
    const-string v0, "CONNECTING"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0

    .line 1100
    :pswitch_1
    const-string v0, "CONNECTED"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0

    .line 1103
    :pswitch_2
    const-string v0, "DISCONNECTING"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0

    .line 1106
    :pswitch_3
    const-string v0, "DISCONNECTED"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0

    .line 1115
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/aj;->b_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    invoke-interface {v3}, Landroid/os/IInterface;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 1095
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected varargs a([Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 602
    return-void
.end method

.method protected abstract a_()Ljava/lang/String;
.end method

.method public b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 844
    iput-boolean v0, p0, Lcom/google/android/gms/common/internal/aj;->e:Z

    .line 846
    iget-object v2, p0, Lcom/google/android/gms/common/internal/aj;->k:Ljava/util/ArrayList;

    monitor-enter v2

    .line 848
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/common/internal/aj;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    .line 849
    :goto_0
    if-ge v1, v3, :cond_0

    .line 850
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/internal/al;

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/al;->d()V

    .line 849
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 852
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 853
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 856
    const/4 v0, 0x1

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/common/internal/aj;->a(ILandroid/os/IInterface;)V

    .line 857
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->l:Lcom/google/android/gms/common/internal/ao;

    if-eqz v0, :cond_1

    .line 858
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->h:Lcom/google/android/gms/common/internal/at;

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/aj;->a_()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/internal/aj;->l:Lcom/google/android/gms/common/internal/ao;

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/aj;->g_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/internal/at;->b(Ljava/lang/String;Landroid/content/ServiceConnection;Ljava/lang/String;)V

    .line 860
    iput-object v4, p0, Lcom/google/android/gms/common/internal/aj;->l:Lcom/google/android/gms/common/internal/ao;

    .line 862
    :cond_1
    return-void

    .line 853
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method protected final b(Landroid/os/IBinder;)V
    .locals 4

    .prologue
    .line 901
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bk;->a(Landroid/os/IBinder;)Lcom/google/android/gms/common/internal/bj;

    move-result-object v0

    .line 902
    new-instance v1, Lcom/google/android/gms/common/internal/an;

    invoke-direct {v1, p0}, Lcom/google/android/gms/common/internal/an;-><init>(Lcom/google/android/gms/common/internal/aj;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/common/internal/aj;->a(Lcom/google/android/gms/common/internal/bj;Lcom/google/android/gms/common/internal/an;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 911
    :goto_0
    return-void

    .line 904
    :catch_0
    move-exception v0

    const-string v0, "GmsClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 906
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/aj;->c:Landroid/os/Handler;

    const/4 v2, 0x4

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 908
    :catch_1
    move-exception v0

    .line 909
    const-string v1, "GmsClient"

    const-string v2, "Remote exception occurred"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/es;)Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1051
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->n:Lcom/google/android/gms/common/internal/ar;

    new-instance v1, Lcom/google/android/gms/common/internal/am;

    invoke-direct {v1, p1}, Lcom/google/android/gms/common/internal/am;-><init>(Lcom/google/android/gms/common/es;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/ar;->b(Lcom/google/android/gms/common/api/x;)Z

    move-result v0

    return v0
.end method

.method protected abstract b_()Ljava/lang/String;
.end method

.method public final c(Lcom/google/android/gms/common/es;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1057
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->n:Lcom/google/android/gms/common/internal/ar;

    new-instance v1, Lcom/google/android/gms/common/internal/am;

    invoke-direct {v1, p1}, Lcom/google/android/gms/common/internal/am;-><init>(Lcom/google/android/gms/common/es;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/ar;->c(Lcom/google/android/gms/common/api/x;)V

    .line 1058
    return-void
.end method

.method public final c_()Z
    .locals 3

    .prologue
    .line 827
    iget-object v1, p0, Lcom/google/android/gms/common/internal/aj;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 828
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/common/internal/aj;->m:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 829
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public d_()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 970
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e_()Z
    .locals 1

    .prologue
    .line 1028
    iget-boolean v0, p0, Lcom/google/android/gms/common/internal/aj;->e:Z

    return v0
.end method

.method protected g_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 625
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->a:Lcom/google/android/gms/common/internal/ClientSettings;

    if-nez v0, :cond_0

    .line 626
    const-string v0, "GmsClient"

    .line 628
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->a:Lcom/google/android/gms/common/internal/ClientSettings;

    iget-object v0, v0, Lcom/google/android/gms/common/internal/ClientSettings;->a:Lcom/google/android/gms/common/internal/ClientSettings$ParcelableClientSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/ClientSettings$ParcelableClientSettings;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()Landroid/content/Context;
    .locals 1

    .prologue
    .line 876
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->b:Landroid/content/Context;

    return-object v0
.end method

.method public final i()Landroid/os/Looper;
    .locals 1

    .prologue
    .line 882
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->g:Landroid/os/Looper;

    return-object v0
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 944
    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/aj;->c_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 945
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 948
    :cond_0
    return-void
.end method

.method public final k()Landroid/os/IInterface;
    .locals 3

    .prologue
    .line 983
    iget-object v1, p0, Lcom/google/android/gms/common/internal/aj;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 984
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/common/internal/aj;->m:I

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    .line 985
    new-instance v0, Landroid/os/DeadObjectException;

    invoke-direct {v0}, Landroid/os/DeadObjectException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 990
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 987
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/aj;->j()V

    .line 988
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->j:Landroid/os/IInterface;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Client is connected but service is null"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 989
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->j:Landroid/os/IInterface;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0

    .line 988
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n_()Z
    .locals 3

    .prologue
    .line 837
    iget-object v1, p0, Lcom/google/android/gms/common/internal/aj;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 838
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/common/internal/aj;->m:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 839
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
