.class public final Lcom/google/android/gms/auth/trustagent/trustlet/az;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)V
    .locals 0

    .prologue
    .line 357
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/trustlet/az;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 357
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->a(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "auth_trust_agent_pref_trusted_place_account_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v1}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->a(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v3, ""

    invoke-interface {v1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ak;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v1}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->a(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "auth_trust_agent_pref_trusted_place_name_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_1
    return-object v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_1
.end method

.method private a()Z
    .locals 3

    .prologue
    .line 548
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 550
    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 551
    const-class v2, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 552
    const/4 v0, 0x1

    .line 555
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 520
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->g(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {p2}, Lcom/google/android/gms/auth/trustagent/ak;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 523
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->g(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "auth_trust_agent_pref_trusted_places_home_key"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 525
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->g(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/ak;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 526
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->g(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/ak;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 527
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->g(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/ak;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 528
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->g(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/ak;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 529
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->g(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 530
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 535
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->g(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {p2}, Lcom/google/android/gms/auth/trustagent/ak;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 538
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->g(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "auth_trust_agent_pref_trusted_places_work_key"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 540
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->g(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/ak;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 541
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->g(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/ak;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 542
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->g(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/ak;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 543
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->g(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/ak;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 544
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->g(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 545
    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    .line 361
    const-string v0, "Coffee - PlaceTrustlet"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    const-string v0, "Coffee - PlaceTrustlet"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "User Present broadcast receiver action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->d(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Lcom/google/android/gms/common/api/v;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->d(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Lcom/google/android/gms/common/api/v;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->e(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 404
    :cond_1
    :goto_0
    return-void

    .line 370
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->a(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "auth_trust_agent_pref_trusted_place_home_work_account"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 372
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 373
    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ak;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 375
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 376
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->a(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-wide/16 v4, 0x0

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 377
    sget-object v0, Lcom/google/android/gms/auth/b/a;->ae:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 379
    sub-long v4, v2, v4

    .line 380
    cmp-long v0, v4, v6

    if-ltz v0, :cond_4

    .line 381
    const-string v0, "Coffee - PlaceTrustlet"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 382
    const-string v0, "Coffee - PlaceTrustlet"

    const-string v4, "fetch home and work address!"

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    :cond_3
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/ba;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ba;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/az;)V

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/auth/trustagent/trustlet/ba;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 385
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->a(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 393
    :cond_4
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v1

    sget-object v0, Lcom/google/android/gms/auth/b/a;->x:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v1, v0

    if-gtz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 395
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    const-class v2, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 396
    const-string v2, "auth_trust_agent_trusted_places_id_list_key"

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->b(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Ljava/util/Set;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v3}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->b(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 398
    const-string v2, "auth_trust_agent_entered_trusted_places_id_list_key"

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->f(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Ljava/util/Set;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v3}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->f(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 402
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0
.end method
