.class public final Lcom/google/android/gms/cast/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public b:I

.field public c:Ljava/lang/String;

.field public d:Lcom/google/android/gms/cast/i;

.field public e:J

.field private f:Ljava/util/List;

.field private g:Lcom/google/android/gms/cast/m;

.field private h:Lorg/json/JSONObject;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "content ID cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/cast/g;->a:Ljava/lang/String;

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/cast/g;->b:I

    .line 73
    return-void
.end method

.method constructor <init>(Lorg/json/JSONObject;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x2

    const/4 v1, 0x0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    const-string v0, "contentId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/g;->a:Ljava/lang/String;

    .line 83
    const-string v0, "streamType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 84
    const-string v2, "NONE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 85
    iput v1, p0, Lcom/google/android/gms/cast/g;->b:I

    .line 94
    :goto_0
    const-string v0, "contentType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/g;->c:Ljava/lang/String;

    .line 96
    const-string v0, "metadata"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    const-string v0, "metadata"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 98
    const-string v2, "metadataType"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 99
    new-instance v3, Lcom/google/android/gms/cast/i;

    invoke-direct {v3, v2}, Lcom/google/android/gms/cast/i;-><init>(I)V

    iput-object v3, p0, Lcom/google/android/gms/cast/g;->d:Lcom/google/android/gms/cast/i;

    .line 100
    iget-object v2, p0, Lcom/google/android/gms/cast/g;->d:Lcom/google/android/gms/cast/i;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/i;->a(Lorg/json/JSONObject;)V

    .line 103
    :cond_0
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/gms/cast/g;->e:J

    .line 106
    const-string v0, "duration"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "duration"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 107
    const-string v0, "duration"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v2

    .line 108
    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v2, v3}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-nez v0, :cond_1

    .line 109
    const-wide v4, 0x408f400000000000L    # 1000.0

    mul-double/2addr v2, v4

    double-to-long v2, v2

    iput-wide v2, p0, Lcom/google/android/gms/cast/g;->e:J

    .line 113
    :cond_1
    const-string v0, "tracks"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/g;->f:Ljava/util/List;

    .line 115
    const-string v0, "tracks"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    move v0, v1

    .line 116
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_6

    .line 117
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 118
    new-instance v4, Lcom/google/android/gms/cast/l;

    invoke-direct {v4, v3}, Lcom/google/android/gms/cast/l;-><init>(Lorg/json/JSONObject;)V

    .line 119
    iget-object v3, p0, Lcom/google/android/gms/cast/g;->f:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 86
    :cond_2
    const-string v2, "BUFFERED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 87
    iput v7, p0, Lcom/google/android/gms/cast/g;->b:I

    goto/16 :goto_0

    .line 88
    :cond_3
    const-string v2, "LIVE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 89
    iput v6, p0, Lcom/google/android/gms/cast/g;->b:I

    goto/16 :goto_0

    .line 91
    :cond_4
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/cast/g;->b:I

    goto/16 :goto_0

    .line 122
    :cond_5
    iput-object v9, p0, Lcom/google/android/gms/cast/g;->f:Ljava/util/List;

    .line 125
    :cond_6
    const-string v0, "textTrackStyle"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 126
    const-string v0, "textTrackStyle"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 127
    new-instance v2, Lcom/google/android/gms/cast/m;

    invoke-direct {v2}, Lcom/google/android/gms/cast/m;-><init>()V

    .line 128
    invoke-virtual {v2}, Lcom/google/android/gms/cast/m;->a()V

    const-string v3, "fontScale"

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v3, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    double-to-float v3, v4

    iput v3, v2, Lcom/google/android/gms/cast/m;->a:F

    const-string v3, "foregroundColor"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/cast/m;->a(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/google/android/gms/cast/m;->b:I

    const-string v3, "backgroundColor"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/cast/m;->a(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/google/android/gms/cast/m;->c:I

    const-string v3, "edgeType"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    const-string v3, "edgeType"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "NONE"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    iput v1, v2, Lcom/google/android/gms/cast/m;->d:I

    :cond_7
    :goto_2
    const-string v3, "edgeColor"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/cast/m;->a(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/google/android/gms/cast/m;->e:I

    const-string v3, "windowType"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, "windowType"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "NONE"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    iput v1, v2, Lcom/google/android/gms/cast/m;->f:I

    :cond_8
    :goto_3
    const-string v3, "windowColor"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/cast/m;->a(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/google/android/gms/cast/m;->g:I

    iget v3, v2, Lcom/google/android/gms/cast/m;->f:I

    if-ne v3, v6, :cond_9

    const-string v3, "windowRoundedCornerRadius"

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, v2, Lcom/google/android/gms/cast/m;->h:I

    :cond_9
    const-string v3, "fontFamily"

    invoke-virtual {v0, v3, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/cast/m;->i:Ljava/lang/String;

    const-string v3, "fontGenericFamily"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    const-string v3, "fontGenericFamily"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "SANS_SERIF"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    iput v1, v2, Lcom/google/android/gms/cast/m;->j:I

    :cond_a
    :goto_4
    const-string v3, "fontStyle"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    const-string v3, "fontStyle"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "NORMAL"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_18

    iput v1, v2, Lcom/google/android/gms/cast/m;->k:I

    :cond_b
    :goto_5
    const-string v1, "customData"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/cast/m;->l:Lorg/json/JSONObject;

    .line 129
    iput-object v2, p0, Lcom/google/android/gms/cast/g;->g:Lcom/google/android/gms/cast/m;

    .line 134
    :goto_6
    const-string v0, "customData"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/g;->h:Lorg/json/JSONObject;

    .line 135
    return-void

    .line 128
    :cond_c
    const-string v4, "OUTLINE"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    iput v7, v2, Lcom/google/android/gms/cast/m;->d:I

    goto/16 :goto_2

    :cond_d
    const-string v4, "DROP_SHADOW"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    iput v6, v2, Lcom/google/android/gms/cast/m;->d:I

    goto/16 :goto_2

    :cond_e
    const-string v4, "RAISED"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    iput v8, v2, Lcom/google/android/gms/cast/m;->d:I

    goto/16 :goto_2

    :cond_f
    const-string v4, "DEPRESSED"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v3, 0x4

    iput v3, v2, Lcom/google/android/gms/cast/m;->d:I

    goto/16 :goto_2

    :cond_10
    const-string v4, "NORMAL"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    iput v7, v2, Lcom/google/android/gms/cast/m;->f:I

    goto/16 :goto_3

    :cond_11
    const-string v4, "ROUNDED_CORNERS"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    iput v6, v2, Lcom/google/android/gms/cast/m;->f:I

    goto/16 :goto_3

    :cond_12
    const-string v4, "MONOSPACED_SANS_SERIF"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    iput v7, v2, Lcom/google/android/gms/cast/m;->j:I

    goto :goto_4

    :cond_13
    const-string v4, "SERIF"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    iput v6, v2, Lcom/google/android/gms/cast/m;->j:I

    goto/16 :goto_4

    :cond_14
    const-string v4, "MONOSPACED_SERIF"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    iput v8, v2, Lcom/google/android/gms/cast/m;->j:I

    goto/16 :goto_4

    :cond_15
    const-string v4, "CASUAL"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    const/4 v3, 0x4

    iput v3, v2, Lcom/google/android/gms/cast/m;->j:I

    goto/16 :goto_4

    :cond_16
    const-string v4, "CURSIVE"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    const/4 v3, 0x5

    iput v3, v2, Lcom/google/android/gms/cast/m;->j:I

    goto/16 :goto_4

    :cond_17
    const-string v4, "SMALL_CAPITALS"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    const/4 v3, 0x6

    iput v3, v2, Lcom/google/android/gms/cast/m;->j:I

    goto/16 :goto_4

    :cond_18
    const-string v1, "BOLD"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    iput v7, v2, Lcom/google/android/gms/cast/m;->k:I

    goto/16 :goto_5

    :cond_19
    const-string v1, "ITALIC"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    iput v6, v2, Lcom/google/android/gms/cast/m;->k:I

    goto/16 :goto_5

    :cond_1a
    const-string v1, "BOLD_ITALIC"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    iput v8, v2, Lcom/google/android/gms/cast/m;->k:I

    goto/16 :goto_5

    .line 131
    :cond_1b
    iput-object v9, p0, Lcom/google/android/gms/cast/g;->g:Lcom/google/android/gms/cast/m;

    goto/16 :goto_6
.end method


# virtual methods
.method public final a()Lorg/json/JSONObject;
    .locals 6

    .prologue
    .line 355
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 357
    :try_start_0
    const-string v0, "contentId"

    iget-object v2, p0, Lcom/google/android/gms/cast/g;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 360
    iget v0, p0, Lcom/google/android/gms/cast/g;->b:I

    packed-switch v0, :pswitch_data_0

    .line 371
    const-string v0, "NONE"

    .line 374
    :goto_0
    const-string v2, "streamType"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 376
    iget-object v0, p0, Lcom/google/android/gms/cast/g;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 377
    const-string v0, "contentType"

    iget-object v2, p0, Lcom/google/android/gms/cast/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 380
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/g;->d:Lcom/google/android/gms/cast/i;

    if-eqz v0, :cond_1

    .line 381
    const-string v0, "metadata"

    iget-object v2, p0, Lcom/google/android/gms/cast/g;->d:Lcom/google/android/gms/cast/i;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/i;->a()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 384
    :cond_1
    const-string v0, "duration"

    iget-wide v2, p0, Lcom/google/android/gms/cast/g;->e:J

    long-to-double v2, v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    invoke-virtual {v1, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 386
    iget-object v0, p0, Lcom/google/android/gms/cast/g;->f:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 387
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 388
    iget-object v0, p0, Lcom/google/android/gms/cast/g;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/l;

    .line 389
    invoke-virtual {v0}, Lcom/google/android/gms/cast/l;->a()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_1

    :catch_0
    move-exception v0

    .line 402
    :cond_2
    :goto_2
    return-object v1

    .line 362
    :pswitch_0
    const-string v0, "BUFFERED"

    goto :goto_0

    .line 366
    :pswitch_1
    const-string v0, "LIVE"

    goto :goto_0

    .line 391
    :cond_3
    const-string v0, "tracks"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 394
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/cast/g;->g:Lcom/google/android/gms/cast/m;

    if-eqz v0, :cond_5

    .line 395
    const-string v0, "textTrackStyle"

    iget-object v2, p0, Lcom/google/android/gms/cast/g;->g:Lcom/google/android/gms/cast/m;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/m;->b()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 398
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/cast/g;->h:Lorg/json/JSONObject;

    if-eqz v0, :cond_2

    .line 399
    const-string v0, "customData"

    iget-object v2, p0, Lcom/google/android/gms/cast/g;->h:Lorg/json/JSONObject;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 360
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 407
    if-ne p0, p1, :cond_1

    move v2, v1

    .line 425
    :cond_0
    :goto_0
    return v2

    .line 410
    :cond_1
    instance-of v0, p1, Lcom/google/android/gms/cast/g;

    if-eqz v0, :cond_0

    .line 414
    check-cast p1, Lcom/google/android/gms/cast/g;

    .line 416
    iget-object v0, p0, Lcom/google/android/gms/cast/g;->h:Lorg/json/JSONObject;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    iget-object v3, p1, Lcom/google/android/gms/cast/g;->h:Lorg/json/JSONObject;

    if-nez v3, :cond_4

    move v3, v1

    :goto_2
    if-ne v0, v3, :cond_0

    .line 419
    iget-object v0, p0, Lcom/google/android/gms/cast/g;->h:Lorg/json/JSONObject;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/gms/cast/g;->h:Lorg/json/JSONObject;

    if-eqz v0, :cond_2

    .line 420
    iget-object v0, p0, Lcom/google/android/gms/cast/g;->h:Lorg/json/JSONObject;

    iget-object v3, p1, Lcom/google/android/gms/cast/g;->h:Lorg/json/JSONObject;

    invoke-static {v0, v3}, Lcom/google/android/gms/common/util/af;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 425
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/cast/g;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/cast/g;->a:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/gms/cast/internal/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/cast/g;->b:I

    iget v3, p1, Lcom/google/android/gms/cast/g;->b:I

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/g;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/cast/g;->c:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/gms/cast/internal/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/g;->d:Lcom/google/android/gms/cast/i;

    iget-object v3, p1, Lcom/google/android/gms/cast/g;->d:Lcom/google/android/gms/cast/i;

    invoke-static {v0, v3}, Lcom/google/android/gms/cast/internal/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v4, p0, Lcom/google/android/gms/cast/g;->e:J

    iget-wide v6, p1, Lcom/google/android/gms/cast/g;->e:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 416
    goto :goto_1

    :cond_4
    move v3, v2

    goto :goto_2
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 434
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/cast/g;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/cast/g;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/cast/g;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/cast/g;->d:Lcom/google/android/gms/cast/i;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/cast/g;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/cast/g;->h:Lorg/json/JSONObject;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
