.class public Lcom/google/android/gms/common/util/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/w;
.implements Lcom/android/volley/x;


# instance fields
.field public final a:Ljava/util/ArrayList;

.field private b:Lcom/android/volley/ac;

.field private final c:Lcom/google/android/gms/common/util/f;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/util/f;-><init>(Lcom/google/android/gms/common/util/f;)V

    .line 41
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/common/util/f;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/util/f;->a:Ljava/util/ArrayList;

    .line 45
    iput-object p1, p0, Lcom/google/android/gms/common/util/f;->c:Lcom/google/android/gms/common/util/f;

    .line 46
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/gms/common/util/f;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse;

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/common/util/f;
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/google/android/gms/common/util/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/util/f;-><init>(Lcom/google/android/gms/common/util/f;)V

    return-object v0
.end method

.method public final a(Lcom/android/volley/ac;)V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/gms/common/util/f;->c:Lcom/google/android/gms/common/util/f;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/common/util/f;->c:Lcom/google/android/gms/common/util/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/util/f;->a(Lcom/android/volley/ac;)V

    .line 79
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/common/util/f;->b:Lcom/android/volley/ac;

    .line 80
    return-void
.end method

.method public a(Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/common/util/f;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lcom/google/android/gms/common/server/response/FastJsonResponse;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/util/f;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/common/util/f;->b:Lcom/android/volley/ac;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/common/util/f;->b:Lcom/android/volley/ac;

    throw v0

    .line 93
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/gms/common/util/f;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
