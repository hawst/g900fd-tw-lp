.class public final Lcom/google/android/gms/lockbox/c/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Lcom/google/android/gms/lockbox/e;


# static fields
.field public static final a:Landroid/os/Parcelable$Creator;

.field public static final b:[I


# instance fields
.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Z

.field private final f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/android/gms/lockbox/c/p;

    invoke-direct {v0}, Lcom/google/android/gms/lockbox/c/p;-><init>()V

    sput-object v0, Lcom/google/android/gms/lockbox/c/o;->a:Landroid/os/Parcelable$Creator;

    .line 69
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/lockbox/c/o;->b:[I

    return-void

    :array_0
    .array-data 4
        0x1
        0x2
    .end array-data
.end method

.method constructor <init>(ILjava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput p1, p0, Lcom/google/android/gms/lockbox/c/o;->c:I

    .line 39
    iput-object p2, p0, Lcom/google/android/gms/lockbox/c/o;->d:Ljava/lang/String;

    .line 40
    iput-boolean p3, p0, Lcom/google/android/gms/lockbox/c/o;->e:Z

    .line 41
    iput-boolean p4, p0, Lcom/google/android/gms/lockbox/c/o;->f:Z

    .line 42
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZZ)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/gms/lockbox/c/o;-><init>(ILjava/lang/String;ZZ)V

    .line 52
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/google/android/gms/lockbox/c/o;->c:I

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/google/android/gms/lockbox/c/o;->e:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/google/android/gms/lockbox/c/o;->f:Z

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/gms/lockbox/c/o;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 100
    invoke-static {p0, p1}, Lcom/google/android/gms/lockbox/c/p;->a(Lcom/google/android/gms/lockbox/c/o;Landroid/os/Parcel;)V

    .line 101
    return-void
.end method
