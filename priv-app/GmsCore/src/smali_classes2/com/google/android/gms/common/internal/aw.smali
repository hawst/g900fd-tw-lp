.class final Lcom/google/android/gms/common/internal/aw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/common/internal/ax;

.field final b:Ljava/util/Set;

.field c:I

.field d:Z

.field e:Landroid/os/IBinder;

.field final f:Lcom/google/android/gms/common/internal/av;

.field g:Landroid/content/ComponentName;

.field final synthetic h:Lcom/google/android/gms/common/internal/au;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/internal/au;Lcom/google/android/gms/common/internal/av;)V
    .locals 1

    .prologue
    .line 255
    iput-object p1, p0, Lcom/google/android/gms/common/internal/aw;->h:Lcom/google/android/gms/common/internal/au;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    iput-object p2, p0, Lcom/google/android/gms/common/internal/aw;->f:Lcom/google/android/gms/common/internal/av;

    .line 257
    new-instance v0, Lcom/google/android/gms/common/internal/ax;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/internal/ax;-><init>(Lcom/google/android/gms/common/internal/aw;)V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aw;->a:Lcom/google/android/gms/common/internal/ax;

    .line 258
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/aw;->b:Ljava/util/Set;

    .line 259
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/common/internal/aw;->c:I

    .line 260
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/ServiceConnection;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aw;->h:Lcom/google/android/gms/common/internal/au;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/au;->c(Lcom/google/android/gms/common/internal/au;)Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/internal/aw;->h:Lcom/google/android/gms/common/internal/au;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/au;->b(Lcom/google/android/gms/common/internal/au;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/internal/aw;->f:Lcom/google/android/gms/common/internal/av;

    invoke-virtual {v2}, Lcom/google/android/gms/common/internal/av;->a()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;Ljava/lang/String;Landroid/content/Intent;)V

    .line 287
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aw;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 288
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aw;->h:Lcom/google/android/gms/common/internal/au;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/au;->c(Lcom/google/android/gms/common/internal/au;)Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/internal/aw;->h:Lcom/google/android/gms/common/internal/au;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/au;->b(Lcom/google/android/gms/common/internal/au;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/internal/aw;->f:Lcom/google/android/gms/common/internal/av;

    invoke-virtual {v2}, Lcom/google/android/gms/common/internal/av;->a()Landroid/content/Intent;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/common/internal/aw;->a:Lcom/google/android/gms/common/internal/ax;

    const/16 v5, 0x81

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/common/internal/aw;->d:Z

    .line 269
    iget-boolean v0, p0, Lcom/google/android/gms/common/internal/aw;->d:Z

    if-eqz v0, :cond_0

    .line 270
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/common/internal/aw;->c:I

    .line 275
    :goto_0
    return-void

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aw;->h:Lcom/google/android/gms/common/internal/au;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/au;->c(Lcom/google/android/gms/common/internal/au;)Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/internal/aw;->h:Lcom/google/android/gms/common/internal/au;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/au;->b(Lcom/google/android/gms/common/internal/au;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/internal/aw;->a:Lcom/google/android/gms/common/internal/ax;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aw;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/content/ServiceConnection;)Z
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aw;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
