.class public final Lcom/google/android/gms/backup/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Landroid/app/backup/BackupDataOutput;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Landroid/app/backup/BackupDataOutput;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/backup/g;->b:Ljava/lang/String;

    .line 21
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/backup/g;->c:I

    .line 22
    iput v1, p0, Lcom/google/android/gms/backup/g;->d:I

    .line 23
    iput v1, p0, Lcom/google/android/gms/backup/g;->e:I

    .line 37
    iput-object p1, p0, Lcom/google/android/gms/backup/g;->a:Landroid/app/backup/BackupDataOutput;

    .line 38
    return-void
.end method

.method private a(Ljava/lang/String;II[B)V
    .locals 3

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/backup/g;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/backup/g;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_0

    .line 75
    new-instance v0, Lcom/google/android/gms/backup/h;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Out of order: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/backup/g;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/backup/h;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/backup/g;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 79
    iget v0, p0, Lcom/google/android/gms/backup/g;->d:I

    iget v1, p0, Lcom/google/android/gms/backup/g;->e:I

    if-eq v0, v1, :cond_1

    .line 80
    new-instance v0, Lcom/google/android/gms/backup/h;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Insufficient data: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " follows "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/backup/g;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " written="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/backup/g;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " total="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/backup/g;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/backup/h;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_1
    iput-object p1, p0, Lcom/google/android/gms/backup/g;->b:Ljava/lang/String;

    .line 86
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/backup/g;->c:I

    .line 90
    :cond_2
    iget v0, p0, Lcom/google/android/gms/backup/g;->c:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_3

    .line 115
    :goto_0
    return-void

    .line 92
    :cond_3
    iget v0, p0, Lcom/google/android/gms/backup/g;->c:I

    add-int/lit8 v0, v0, 0x1

    if-eq p2, v0, :cond_4

    .line 93
    new-instance v0, Lcom/google/android/gms/backup/h;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Out of order: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " part="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " follows part="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/backup/g;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/backup/h;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_4
    if-nez p2, :cond_5

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/backup/g;->a:Landroid/app/backup/BackupDataOutput;

    invoke-virtual {v0, p1, p3}, Landroid/app/backup/BackupDataOutput;->writeEntityHeader(Ljava/lang/String;I)I

    .line 99
    iput p3, p0, Lcom/google/android/gms/backup/g;->d:I

    .line 100
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/backup/g;->e:I

    .line 102
    :cond_5
    iget v0, p0, Lcom/google/android/gms/backup/g;->e:I

    array-length v1, p4

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/backup/g;->d:I

    if-le v0, v1, :cond_6

    .line 103
    new-instance v0, Lcom/google/android/gms/backup/h;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Excess data: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " part="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " written="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/backup/g;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " total="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/backup/g;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/backup/h;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_6
    iget v0, p0, Lcom/google/android/gms/backup/g;->d:I

    if-eq p3, v0, :cond_7

    .line 108
    new-instance v0, Lcom/google/android/gms/backup/h;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Inconsistent size: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " part="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " total="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " previous total="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/backup/g;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/backup/h;-><init>(Ljava/lang/String;)V

    throw v0

    .line 112
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/backup/g;->a:Landroid/app/backup/BackupDataOutput;

    array-length v1, p4

    invoke-virtual {v0, p4, v1}, Landroid/app/backup/BackupDataOutput;->writeEntityData([BI)I

    .line 113
    iput p2, p0, Lcom/google/android/gms/backup/g;->c:I

    .line 114
    iget v0, p0, Lcom/google/android/gms/backup/g;->e:I

    array-length v1, p4

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/backup/g;->e:I

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/Map;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 53
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 54
    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    move v2, v3

    .line 56
    :goto_0
    array-length v1, v0

    if-ge v2, v1, :cond_1

    .line 57
    aget-object v4, v0, v2

    .line 58
    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    .line 59
    const-string v5, ":#"

    invoke-virtual {v4, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    .line 60
    const/4 v6, -0x1

    if-eq v5, v6, :cond_0

    .line 61
    new-instance v6, Ljava/text/ParsePosition;

    invoke-direct {v6, v5}, Ljava/text/ParsePosition;-><init>(I)V

    .line 62
    sget-object v7, Lcom/google/android/gms/backup/f;->a:Ljava/text/DecimalFormat;

    invoke-virtual {v7, v4, v6}, Ljava/text/DecimalFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Number;->intValue()I

    move-result v7

    .line 63
    sget-object v8, Lcom/google/android/gms/backup/f;->b:Ljava/text/DecimalFormat;

    invoke-virtual {v8, v4, v6}, Ljava/text/DecimalFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Number;->intValue()I

    move-result v6

    .line 64
    invoke-virtual {v4, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4, v7, v6, v1}, Lcom/google/android/gms/backup/g;->a(Ljava/lang/String;II[B)V

    .line 56
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 66
    :cond_0
    array-length v5, v1

    invoke-direct {p0, v4, v3, v5, v1}, Lcom/google/android/gms/backup/g;->a(Ljava/lang/String;II[B)V

    .line 67
    const/4 v1, -0x2

    iput v1, p0, Lcom/google/android/gms/backup/g;->c:I

    goto :goto_1

    .line 70
    :cond_1
    return-void
.end method
