.class public final Lcom/google/android/gms/games/service/a/f/c;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:Lcom/google/android/gms/games/a/au;

.field private final e:I

.field private final f:Z

.field private final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 25
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/f/c;->c:Lcom/google/android/gms/games/internal/dr;

    .line 26
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/f/c;->d:Lcom/google/android/gms/games/a/au;

    .line 27
    iput p3, p0, Lcom/google/android/gms/games/service/a/f/c;->e:I

    .line 28
    iput-boolean p4, p0, Lcom/google/android/gms/games/service/a/f/c;->f:Z

    .line 29
    iput-object p5, p0, Lcom/google/android/gms/games/service/a/f/c;->g:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/f/c;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->k(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 45
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/f/c;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/f/c;->d:Lcom/google/android/gms/games/a/au;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/f/c;->g:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/games/a/t;->g(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 38
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/f/c;->d:Lcom/google/android/gms/games/a/au;

    iget v1, p0, Lcom/google/android/gms/games/service/a/f/c;->e:I

    iget-boolean v2, p0, Lcom/google/android/gms/games/service/a/f/c;->f:Z

    invoke-virtual {p2, v0, v1, v2}, Lcom/google/android/gms/games/a/t;->h(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method
