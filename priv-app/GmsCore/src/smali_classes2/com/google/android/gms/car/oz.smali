.class final Lcom/google/android/gms/car/oz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/om;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/oy;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/oy;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/gms/car/oz;->a:Lcom/google/android/gms/car/oy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method public final a(Landroid/view/KeyEvent;)V
    .locals 2

    .prologue
    .line 85
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Key event in watermark window"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 80
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Touch event in watermark window"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/car/ol;II)V
    .locals 2

    .prologue
    .line 55
    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/car/oi;

    .line 56
    iget-object v1, p0, Lcom/google/android/gms/car/oz;->a:Lcom/google/android/gms/car/oy;

    invoke-static {v1, v0, p2, p3}, Lcom/google/android/gms/car/oy;->a(Lcom/google/android/gms/car/oy;Lcom/google/android/gms/car/oi;II)V

    .line 57
    iget-object v1, p0, Lcom/google/android/gms/car/oz;->a:Lcom/google/android/gms/car/oy;

    invoke-static {v1}, Lcom/google/android/gms/car/oy;->a(Lcom/google/android/gms/car/oy;)Lcom/google/android/gms/car/on;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/on;->d(Lcom/google/android/gms/car/oi;)V

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/car/oz;->a:Lcom/google/android/gms/car/oy;

    invoke-static {v0}, Lcom/google/android/gms/car/oy;->a(Lcom/google/android/gms/car/oy;)Lcom/google/android/gms/car/on;

    move-result-object v0

    check-cast p1, Lcom/google/android/gms/car/oi;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/on;->e(Lcom/google/android/gms/car/oi;)V

    .line 59
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 90
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "InputFocusChange in watermark window"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 72
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    const-class v0, Lcom/google/android/gms/car/oy;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
