.class public final Lcom/google/android/gms/people/identity/internal/models/DefaultAvatarRefImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Lcom/google/android/gms/people/identity/models/AvatarRef;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/people/identity/internal/models/b;


# instance fields
.field final a:I

.field b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/gms/people/identity/internal/models/b;

    invoke-direct {v0}, Lcom/google/android/gms/people/identity/internal/models/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultAvatarRefImpl;->CREATOR:Lcom/google/android/gms/people/identity/internal/models/b;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput p1, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultAvatarRefImpl;->a:I

    .line 45
    iput-object p2, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultAvatarRefImpl;->b:Ljava/lang/String;

    .line 46
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultAvatarRefImpl;->CREATOR:Lcom/google/android/gms/people/identity/internal/models/b;

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultAvatarRefImpl;->CREATOR:Lcom/google/android/gms/people/identity/internal/models/b;

    invoke-static {p0, p1}, Lcom/google/android/gms/people/identity/internal/models/b;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultAvatarRefImpl;Landroid/os/Parcel;)V

    .line 71
    return-void
.end method
