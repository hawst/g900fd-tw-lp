.class public final Lcom/google/android/gms/search/corpora/m;
.super Lcom/google/android/gms/search/corpora/a/e;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/icing/impl/u;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/gms/search/corpora/a/e;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/gms/search/corpora/m;->a:Lcom/google/android/gms/icing/impl/u;

    .line 21
    iput-object p2, p0, Lcom/google/android/gms/search/corpora/m;->b:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/search/corpora/ClearCorpusCall$Request;Lcom/google/android/gms/search/corpora/a/a;)V
    .locals 7

    .prologue
    .line 40
    iget-object v6, p0, Lcom/google/android/gms/search/corpora/m;->a:Lcom/google/android/gms/icing/impl/u;

    new-instance v0, Lcom/google/android/gms/search/corpora/o;

    iget-object v2, p0, Lcom/google/android/gms/search/corpora/m;->a:Lcom/google/android/gms/icing/impl/u;

    iget-object v3, p0, Lcom/google/android/gms/search/corpora/m;->b:Ljava/lang/String;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/corpora/o;-><init>(Lcom/google/android/gms/search/corpora/m;Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/corpora/ClearCorpusCall$Request;Lcom/google/android/gms/search/corpora/a/a;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    .line 47
    return-void
.end method

.method public final a(Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Request;Lcom/google/android/gms/search/corpora/a/a;)V
    .locals 7

    .prologue
    .line 65
    iget-object v6, p0, Lcom/google/android/gms/search/corpora/m;->a:Lcom/google/android/gms/icing/impl/u;

    new-instance v0, Lcom/google/android/gms/search/corpora/q;

    iget-object v2, p0, Lcom/google/android/gms/search/corpora/m;->a:Lcom/google/android/gms/icing/impl/u;

    iget-object v3, p0, Lcom/google/android/gms/search/corpora/m;->b:Ljava/lang/String;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/corpora/q;-><init>(Lcom/google/android/gms/search/corpora/m;Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Request;Lcom/google/android/gms/search/corpora/a/a;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    .line 72
    return-void
.end method

.method public final a(Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;Lcom/google/android/gms/search/corpora/a/a;)V
    .locals 7

    .prologue
    .line 52
    iget-object v6, p0, Lcom/google/android/gms/search/corpora/m;->a:Lcom/google/android/gms/icing/impl/u;

    new-instance v0, Lcom/google/android/gms/search/corpora/p;

    iget-object v2, p0, Lcom/google/android/gms/search/corpora/m;->a:Lcom/google/android/gms/icing/impl/u;

    iget-object v3, p0, Lcom/google/android/gms/search/corpora/m;->b:Ljava/lang/String;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/corpora/p;-><init>(Lcom/google/android/gms/search/corpora/m;Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;Lcom/google/android/gms/search/corpora/a/a;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    .line 60
    return-void
.end method

.method public final a(Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;Lcom/google/android/gms/search/corpora/a/a;)V
    .locals 7

    .prologue
    .line 27
    iget-object v6, p0, Lcom/google/android/gms/search/corpora/m;->a:Lcom/google/android/gms/icing/impl/u;

    new-instance v0, Lcom/google/android/gms/search/corpora/n;

    iget-object v2, p0, Lcom/google/android/gms/search/corpora/m;->a:Lcom/google/android/gms/icing/impl/u;

    iget-object v3, p0, Lcom/google/android/gms/search/corpora/m;->b:Ljava/lang/String;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/corpora/n;-><init>(Lcom/google/android/gms/search/corpora/m;Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;Lcom/google/android/gms/search/corpora/a/a;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    .line 35
    return-void
.end method
