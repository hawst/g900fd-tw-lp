.class final Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;
.super Lcom/google/android/gms/games/service/statemachine/roomservice/u;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field protected final a:Ljava/util/Map;

.field protected b:Lcom/google/android/gms/games/realtime/b;

.field protected c:Ljava/lang/String;

.field protected d:Z

.field private m:Ljava/util/Hashtable;

.field private n:Ljava/lang/String;

.field private o:Lcom/google/android/gms/games/service/statemachine/roomclient/cf;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V
    .locals 1

    .prologue
    .line 117
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomservice/u;-><init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V

    .line 88
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a:Ljava/util/Map;

    .line 118
    return-void
.end method

.method private static a(Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;)Lcom/google/android/gms/games/h/a/co;
    .locals 8

    .prologue
    .line 829
    invoke-virtual {p0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->getNumBytesReceived()Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a(Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;)Lcom/google/android/gms/games/h/a/r;

    move-result-object v1

    .line 830
    invoke-virtual {p0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->getNumBytesSent()Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a(Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;)Lcom/google/android/gms/games/h/a/r;

    move-result-object v2

    .line 831
    invoke-virtual {p0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->getRoundTripLatencyMs()Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a(Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;)Lcom/google/android/gms/games/h/a/r;

    move-result-object v7

    .line 832
    new-instance v0, Lcom/google/android/gms/games/h/a/co;

    invoke-virtual {p0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->getNumMessagesLost()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->getNumMessagesReceived()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->getNumMessagesSent()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/games/h/a/co;-><init>(Lcom/google/android/gms/games/h/a/r;Lcom/google/android/gms/games/h/a/r;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/android/gms/games/h/a/r;)V

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;)Lcom/google/android/gms/games/h/a/r;
    .locals 6

    .prologue
    .line 838
    const/4 v0, 0x0

    .line 839
    if-eqz p0, :cond_0

    .line 840
    new-instance v0, Lcom/google/android/gms/games/h/a/r;

    invoke-virtual {p0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;->getCount()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;->getMax()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;->getMin()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;->getSum()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/games/h/a/r;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;)V

    .line 843
    :cond_0
    return-object v0
.end method

.method private a([Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 5

    .prologue
    .line 142
    if-nez p1, :cond_1

    .line 143
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/realtime/b;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 152
    :cond_0
    return-object v0

    .line 145
    :cond_1
    array-length v1, p1

    .line 146
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 147
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    .line 148
    iget-object v4, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    invoke-virtual {v4, v3}, Lcom/google/android/gms/games/realtime/b;->e(Ljava/lang/String;)Lcom/google/android/gms/games/realtime/c;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private a(ILjava/util/ArrayList;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 694
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->h:Z

    if-eqz v0, :cond_0

    .line 695
    const-string v0, "RoomServiceStateMachine"

    const-string v1, "Checking participantStatus %s for %d participants."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/gms/games/internal/b/d;->a(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    :cond_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 699
    invoke-static {p2}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a(Ljava/util/ArrayList;)[Ljava/lang/String;

    move-result-object v5

    .line 701
    packed-switch p1, :pswitch_data_0

    .line 718
    :try_start_0
    const-string v0, "RoomServiceStateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal Peer Status : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    :cond_1
    :goto_0
    return-void

    .line 703
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/b;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v5}, Lcom/google/android/gms/games/internal/eg;->a(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 721
    :catch_0
    move-exception v0

    const-string v0, "RoomServiceStateMachine"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 707
    :pswitch_1
    :try_start_1
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b(Ljava/util/ArrayList;)V

    .line 708
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/b;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v5}, Lcom/google/android/gms/games/internal/eg;->b(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    .line 711
    :pswitch_2
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v3

    :goto_1
    if-ge v4, v6, :cond_3

    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/realtime/c;

    iget-object v1, v0, Lcom/google/android/gms/games/realtime/c;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->b:Lcom/google/android/gms/games/realtime/network/b;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/c;->b:Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/realtime/network/b;->d(Ljava/lang/String;)V

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_2
    move v1, v3

    goto :goto_2

    .line 712
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/b;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v5}, Lcom/google/android/gms/games/internal/eg;->c(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    .line 715
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/b;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v5}, Lcom/google/android/gms/games/internal/eg;->d(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 701
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private a(Lcom/google/android/gms/games/h/a/ev;)V
    .locals 19

    .prologue
    .line 530
    sget-boolean v1, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->h:Z

    if-eqz v1, :cond_0

    .line 531
    const-string v1, "RoomServiceStateMachine"

    const-string v2, "HandleStatusNotification"

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/b;->a:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/h/a/ev;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 534
    const-string v1, "RoomServiceStateMachine"

    const-string v2, "Current room %s does not match room %s from notification. Ignoring the notification."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v5, v5, Lcom/google/android/gms/games/realtime/b;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/h/a/ev;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    :cond_1
    :goto_0
    :pswitch_0
    return-void

    .line 539
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/h/a/ev;->d()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 540
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget v1, v1, Lcom/google/android/gms/games/realtime/b;->e:I

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/h/a/ev;->d()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lt v1, v2, :cond_3

    .line 541
    const-string v1, "RoomServiceStateMachine"

    const-string v2, "Current status version %d which is the same or later than the version received %d. Ignoring the notification."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget v5, v5, Lcom/google/android/gms/games/realtime/b;->e:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/h/a/ev;->d()Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 547
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/h/a/ev;->d()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/google/android/gms/games/realtime/b;->e:I

    .line 555
    :cond_4
    :goto_1
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/h/a/ev;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/gms/games/internal/eg;->h(Ljava/lang/String;)V

    .line 556
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/h/a/ev;->getParticipants()Ljava/util/ArrayList;

    move-result-object v11

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v18

    move v10, v1

    :goto_2
    move/from16 v0, v18

    if-ge v10, v0, :cond_16

    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/h/a/eu;

    invoke-virtual {v1}, Lcom/google/android/gms/games/h/a/eu;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/gms/games/h/a/eu;->b()Ljava/lang/Boolean;

    move-result-object v3

    if-nez v3, :cond_9

    const/4 v5, 0x0

    :goto_3
    invoke-virtual {v1}, Lcom/google/android/gms/games/h/a/eu;->d()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v3, 0x0

    invoke-virtual {v1}, Lcom/google/android/gms/games/h/a/eu;->getClientAddress()Lcom/google/android/gms/games/h/a/em;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v1}, Lcom/google/android/gms/games/h/a/eu;->getClientAddress()Lcom/google/android/gms/games/h/a/em;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/em;->b()Ljava/lang/String;

    move-result-object v3

    :cond_5
    iget-object v1, v1, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    const-string v6, "capabilities"

    invoke-virtual {v1, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/lang/Integer;

    move-object v6, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/b;->b:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    sget-boolean v1, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->h:Z

    if-eqz v1, :cond_6

    const-string v1, "RoomServiceStateMachine"

    const-string v2, "Current Participant"

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-boolean v1, v1, Lcom/google/android/gms/games/realtime/b;->c:Z

    if-eq v5, v1, :cond_7

    if-nez v5, :cond_a

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v2, v2, Lcom/google/android/gms/games/realtime/b;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/gms/games/internal/eg;->e(Ljava/lang/String;)V

    :cond_7
    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iput-boolean v5, v1, Lcom/google/android/gms/games/realtime/b;->c:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_5
    add-int/lit8 v1, v10, 0x1

    move v10, v1

    goto :goto_2

    .line 550
    :cond_8
    sget-boolean v1, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->h:Z

    if-eqz v1, :cond_4

    .line 551
    const-string v1, "RoomServiceStateMachine"

    const-string v2, "roomStatus returned null status version."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 556
    :cond_9
    :try_start_1
    invoke-virtual {v1}, Lcom/google/android/gms/games/h/a/eu;->b()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    goto :goto_3

    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v2, v2, Lcom/google/android/gms/games/realtime/b;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/gms/games/internal/eg;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    .line 558
    :catch_0
    move-exception v1

    .line 559
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->e:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    invoke-static {v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Landroid/os/RemoteException;)V

    goto/16 :goto_0

    .line 556
    :cond_b
    :try_start_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/realtime/b;->e(Ljava/lang/String;)Lcom/google/android/gms/games/realtime/c;

    move-result-object v7

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v1, 0x0

    if-eqz v7, :cond_f

    iget v6, v7, Lcom/google/android/gms/games/realtime/c;->c:I

    iget-boolean v2, v7, Lcom/google/android/gms/games/realtime/c;->d:Z

    iput-object v3, v7, Lcom/google/android/gms/games/realtime/c;->b:Ljava/lang/String;

    iput v4, v7, Lcom/google/android/gms/games/realtime/c;->c:I

    iput-boolean v5, v7, Lcom/google/android/gms/games/realtime/c;->d:Z

    move v3, v6

    move-object v6, v7

    :goto_6
    if-nez v1, :cond_c

    if-eq v3, v4, :cond_d

    :cond_c
    packed-switch v4, :pswitch_data_0

    const-string v1, "RoomServiceStateMachine"

    const-string v3, "Received participant with invalid status %d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v8

    invoke-static {v3, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    :goto_7
    if-eq v2, v5, :cond_12

    if-eqz v5, :cond_11

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_e
    :goto_8
    iget v1, v6, Lcom/google/android/gms/games/realtime/c;->c:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_13

    const/4 v1, 0x1

    :goto_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v3, v2, Lcom/google/android/gms/games/realtime/b;->f:Ljava/util/HashMap;

    iget-object v4, v6, Lcom/google/android/gms/games/realtime/c;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v1, :cond_15

    iget-object v1, v6, Lcom/google/android/gms/games/realtime/c;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_14

    const/4 v1, 0x1

    :goto_a
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    iget-object v1, v2, Lcom/google/android/gms/games/realtime/b;->g:Ljava/util/HashMap;

    iget-object v2, v6, Lcom/google/android/gms/games/realtime/c;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_5

    :cond_f
    const/4 v7, 0x1

    new-instance v1, Lcom/google/android/gms/games/realtime/c;

    if-eqz v6, :cond_10

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    :goto_b
    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/games/realtime/c;-><init>(Ljava/lang/String;Ljava/lang/String;IZI)V

    move v2, v8

    move v3, v9

    move-object v6, v1

    move v1, v7

    goto :goto_6

    :cond_10
    const/4 v6, 0x0

    goto :goto_b

    :pswitch_1
    invoke-virtual {v13, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :pswitch_2
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :pswitch_3
    invoke-virtual {v15, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :pswitch_4
    invoke-virtual {v12, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :cond_11
    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_12
    iget-boolean v1, v6, Lcom/google/android/gms/games/realtime/c;->e:Z

    if-eqz v1, :cond_e

    if-nez v2, :cond_e

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Adding participant: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v6, Lcom/google/android/gms/games/realtime/c;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to disconnected set."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_13
    const/4 v1, 0x0

    goto :goto_9

    :cond_14
    const/4 v1, 0x0

    goto :goto_a

    :cond_15
    iget-object v1, v2, Lcom/google/android/gms/games/realtime/b;->g:Ljava/util/HashMap;

    iget-object v2, v6, Lcom/google/android/gms/games/realtime/c;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_5

    :cond_16
    const/4 v1, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v13}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a(ILjava/util/ArrayList;)V

    const/4 v1, 0x3

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v14}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a(ILjava/util/ArrayList;)V

    const/4 v1, 0x4

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v15}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a(ILjava/util/ArrayList;)V

    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v12}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a(ILjava/util/ArrayList;)V

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v2, v2, Lcom/google/android/gms/games/realtime/b;->a:Ljava/lang/String;

    invoke-static/range {v16 .. v16}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a(Ljava/util/ArrayList;)[Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/gms/games/internal/eg;->e(Ljava/lang/String;[Ljava/lang/String;)V

    :cond_17
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_18

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v2, v2, Lcom/google/android/gms/games/realtime/b;->a:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a(Ljava/util/ArrayList;)[Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/gms/games/internal/eg;->f(Ljava/lang/String;[Ljava/lang/String;)V

    .line 557
    :cond_18
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget v1, v1, Lcom/google/android/gms/games/realtime/b;->d:I

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/h/a/ev;->c()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v1, v2, :cond_19

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/h/a/ev;->c()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/google/android/gms/games/realtime/b;->d:I

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/h/a/ev;->c()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    const-string v1, "RoomServiceStateMachine"

    const-string v2, "Received room with invalid status %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/h/a/ev;->c()Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/h/a/ev;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/gms/games/internal/eg;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/h/a/ev;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/gms/games/internal/eg;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/h/a/ev;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/gms/games/internal/eg;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_19
    sget-boolean v1, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->h:Z

    if-eqz v1, :cond_1

    const-string v1, "RoomServiceStateMachine"

    const-string v2, "Received room with same status %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/h/a/ev;->c()Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 556
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 557
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private a(Lcom/google/android/gms/games/realtime/c;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 743
    iget-object v4, p1, Lcom/google/android/gms/games/realtime/c;->a:Ljava/lang/String;

    .line 744
    iget-object v5, p1, Lcom/google/android/gms/games/realtime/c;->b:Ljava/lang/String;

    .line 745
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 748
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_1

    move v0, v1

    .line 750
    :goto_1
    iget-object v6, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    if-eqz v0, :cond_2

    const-string v3, "Initiating connection with %s"

    :goto_2
    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v2

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V

    .line 752
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->b:Lcom/google/android/gms/games/realtime/network/b;

    iget v2, p1, Lcom/google/android/gms/games/realtime/c;->f:I

    invoke-interface {v1, v5, v0, v2}, Lcom/google/android/gms/games/realtime/network/b;->a(Ljava/lang/String;ZI)V

    .line 754
    return-void

    :cond_0
    move v0, v2

    .line 745
    goto :goto_0

    :cond_1
    move v0, v2

    .line 748
    goto :goto_1

    .line 750
    :cond_2
    const-string v3, "Waiting for connection from %s"

    goto :goto_2
.end method

.method private a(Lcom/google/android/gms/games/realtime/c;Ljava/lang/String;Lcom/google/android/gms/games/realtime/a;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 498
    const-string v1, "CONNECTION_FAILED"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 499
    iput-boolean v0, p1, Lcom/google/android/gms/games/realtime/c;->e:Z

    .line 501
    :cond_0
    iget-object v3, p1, Lcom/google/android/gms/games/realtime/c;->a:Ljava/lang/String;

    .line 502
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/b;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_2

    .line 508
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Connection status: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " reporting ? "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V

    .line 509
    const-string v1, "CONNECTION_FAILED"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_4

    .line 510
    :cond_1
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 512
    new-instance v0, Lcom/google/android/gms/games/h/a/es;

    iget-object v1, p3, Lcom/google/android/gms/games/realtime/a;->b:Ljava/lang/Integer;

    iget-object v2, p3, Lcom/google/android/gms/games/realtime/a;->c:Ljava/lang/String;

    iget-object v5, p3, Lcom/google/android/gms/games/realtime/a;->d:Ljava/lang/Integer;

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/h/a/es;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 516
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 518
    :try_start_0
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v1, v0, [Ljava/lang/String;

    .line 519
    :goto_1
    array-length v0, v1

    if-ge v6, v0, :cond_3

    .line 520
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/es;

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/es;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v6

    .line 519
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_2
    move v0, v6

    .line 502
    goto :goto_0

    .line 522
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v2, v2, Lcom/google/android/gms/games/realtime/b;->a:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Lcom/google/android/gms/games/internal/eg;->g(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 527
    :cond_4
    :goto_2
    return-void

    .line 524
    :catch_0
    move-exception v0

    const-string v0, "RoomServiceStateMachine"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private static a(Ljava/util/ArrayList;)[Ljava/lang/String;
    .locals 4

    .prologue
    .line 727
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    .line 728
    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 729
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/realtime/c;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/c;->a:Ljava/lang/String;

    aput-object v0, v2, v1

    .line 728
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 731
    :cond_0
    return-object v2
.end method

.method private b()V
    .locals 18

    .prologue
    .line 767
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->e:Landroid/content/Context;

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    .line 769
    const/4 v4, 0x0

    .line 770
    const/4 v3, 0x0

    .line 771
    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 772
    if-eqz v2, :cond_0

    .line 773
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v3

    .line 774
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v2

    move v4, v3

    move v3, v2

    .line 776
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 777
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    invoke-virtual {v2}, Lcom/google/android/gms/games/realtime/b;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 778
    const/4 v2, 0x0

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v5, v2

    :goto_0
    if-ge v5, v8, :cond_3

    .line 780
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/realtime/c;

    .line 781
    iget-object v9, v2, Lcom/google/android/gms/games/realtime/c;->b:Ljava/lang/String;

    .line 782
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 783
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v10, v10, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->b:Lcom/google/android/gms/games/realtime/network/b;

    invoke-interface {v10, v9}, Lcom/google/android/gms/games/realtime/network/b;->a(Ljava/lang/String;)Lcom/google/android/gms/games/realtime/network/c;

    move-result-object v9

    .line 787
    if-eqz v9, :cond_1

    iget-object v10, v9, Lcom/google/android/gms/games/realtime/network/c;->a:Lcom/google/android/gms/games/jingle/PeerDiagnostics;

    if-eqz v10, :cond_1

    .line 789
    iget-object v10, v9, Lcom/google/android/gms/games/realtime/network/c;->a:Lcom/google/android/gms/games/jingle/PeerDiagnostics;

    invoke-virtual {v10}, Lcom/google/android/gms/games/jingle/PeerDiagnostics;->getReliableChannelMetrics()Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a(Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;)Lcom/google/android/gms/games/h/a/co;

    move-result-object v10

    .line 791
    iget-object v11, v9, Lcom/google/android/gms/games/realtime/network/c;->a:Lcom/google/android/gms/games/jingle/PeerDiagnostics;

    invoke-virtual {v11}, Lcom/google/android/gms/games/jingle/PeerDiagnostics;->getUnreliableChannelMetrics()Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;

    move-result-object v11

    .line 793
    invoke-static {v11}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a(Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;)Lcom/google/android/gms/games/h/a/co;

    move-result-object v12

    .line 800
    invoke-virtual {v11}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->getConnectionStartTimestampMs()I

    move-result v11

    int-to-long v14, v11

    .line 801
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v14, v16, v14

    .line 802
    new-instance v11, Lcom/google/android/gms/games/h/a/cp;

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    iget-object v2, v2, Lcom/google/android/gms/games/realtime/c;->a:Ljava/lang/String;

    invoke-direct {v11, v13, v2, v10, v12}, Lcom/google/android/gms/games/h/a/cp;-><init>(Ljava/lang/Long;Ljava/lang/String;Lcom/google/android/gms/games/h/a/co;Lcom/google/android/gms/games/h/a/co;)V

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 809
    :cond_1
    if-eqz v9, :cond_2

    iget-object v2, v9, Lcom/google/android/gms/games/realtime/network/c;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    if-eqz v2, :cond_2

    .line 810
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->o:Lcom/google/android/gms/games/service/statemachine/roomclient/cf;

    iget-object v9, v9, Lcom/google/android/gms/games/realtime/network/c;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 779
    :cond_2
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_0

    .line 814
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->e:Landroid/content/Context;

    const-string v5, "phone"

    invoke-virtual {v2, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    move-object v6, v2

    check-cast v6, Landroid/telephony/TelephonyManager;

    .line 816
    new-instance v2, Lcom/google/android/gms/games/h/a/ep;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->d:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lcom/google/android/gms/games/h/a/ep;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Boolean;)V

    .line 820
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v3, v3, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/ep;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->o:Lcom/google/android/gms/games/service/statemachine/roomclient/cf;

    invoke-virtual {v4}, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;->a()Lcom/google/android/gms/games/g/ah;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/games/g/ah;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v4

    invoke-interface {v3, v2, v4}, Lcom/google/android/gms/games/internal/eg;->a(Ljava/lang/String;[B)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 825
    :goto_1
    return-void

    .line 822
    :catch_0
    move-exception v2

    .line 823
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->e:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    invoke-static {v2}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Landroid/os/RemoteException;)V

    goto :goto_1
.end method

.method private b(Ljava/util/ArrayList;)V
    .locals 3

    .prologue
    .line 735
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 737
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/realtime/c;

    .line 738
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a(Lcom/google/android/gms/games/realtime/c;)V

    .line 736
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 740
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/google/android/gms/games/realtime/b;ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 125
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/realtime/b;

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    .line 126
    iput-boolean p3, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->d:Z

    .line 127
    iput-object p1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->c:Ljava/lang/String;

    .line 128
    invoke-static {p1}, Lcom/google/android/gms/games/multiplayer/k;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Expecting player id!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 130
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->m:Ljava/util/Hashtable;

    .line 131
    iput-object p4, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->n:Ljava/lang/String;

    .line 132
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;

    invoke-direct {v0, p4}, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->o:Lcom/google/android/gms/games/service/statemachine/roomclient/cf;

    .line 133
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->c()V

    .line 134
    return-void

    .line 128
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/os/Message;)Z
    .locals 10

    .prologue
    const/4 v7, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 162
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 431
    :pswitch_0
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->g:Z

    :goto_0
    return v0

    .line 165
    :pswitch_1
    :try_start_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/c;

    .line 167
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/b;->b:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/c;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 168
    const-string v1, "RoomServiceStateMachine"

    const-string v2, "Attempting to create a socket connection to self."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    const/4 v2, 0x0

    iget v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/c;->a:I

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/games/internal/eg;->a(Landroid/os/ParcelFileDescriptor;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    :goto_1
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->f:Z

    goto :goto_0

    .line 171
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/c;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/realtime/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 172
    if-nez v1, :cond_1

    .line 173
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    const/4 v2, 0x0

    iget v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/c;->a:I

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/games/internal/eg;->a(Landroid/os/ParcelFileDescriptor;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 187
    :catch_0
    move-exception v0

    .line 188
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->e:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Landroid/os/RemoteException;)V

    goto :goto_1

    .line 175
    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->b:Lcom/google/android/gms/games/realtime/network/b;

    invoke-interface {v2, v1}, Lcom/google/android/gms/games/realtime/network/b;->b(Ljava/lang/String;)I

    move-result v1

    .line 177
    if-lez v1, :cond_2

    .line 178
    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    invoke-static {v1}, Landroid/os/ParcelFileDescriptor;->adoptFd(I)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    iget v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/c;->a:I

    invoke-interface {v2, v1, v0}, Lcom/google/android/gms/games/internal/eg;->a(Landroid/os/ParcelFileDescriptor;I)V

    goto :goto_1

    .line 181
    :cond_2
    const-string v1, "RoomServiceStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Native socket creation failed for participant: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/c;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    const/4 v2, 0x0

    iget v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/c;->a:I

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/games/internal/eg;->a(Landroid/os/ParcelFileDescriptor;I)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 194
    :pswitch_2
    :try_start_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/d;

    .line 195
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/b;->b:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/d;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 196
    const-string v1, "RoomServiceStateMachine"

    const-string v2, "Attempting to create a socket connection to self."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    const/4 v2, 0x0

    iget v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/d;->a:I

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/games/internal/eg;->a(Ljava/lang/String;I)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1

    .line 211
    :goto_2
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->f:Z

    goto/16 :goto_0

    .line 199
    :cond_3
    :try_start_4
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/d;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/realtime/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 200
    if-nez v1, :cond_4

    .line 201
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    const/4 v2, 0x0

    iget v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/d;->a:I

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/games/internal/eg;->a(Ljava/lang/String;I)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    .line 208
    :catch_1
    move-exception v0

    .line 209
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->e:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Landroid/os/RemoteException;)V

    goto :goto_2

    .line 203
    :cond_4
    :try_start_5
    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    iget-object v3, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v3, v3, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->b:Lcom/google/android/gms/games/realtime/network/b;

    invoke-interface {v3, v1}, Lcom/google/android/gms/games/realtime/network/b;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/d;->a:I

    invoke-interface {v2, v1, v0}, Lcom/google/android/gms/games/internal/eg;->a(Ljava/lang/String;I)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2

    .line 214
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/s;

    .line 215
    sget-boolean v1, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->h:Z

    if-eqz v1, :cond_5

    .line 216
    const-string v1, "RoomServiceStateMachine"

    const-string v2, "Received statusNotification for room %s"

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    invoke-virtual {v5}, Lcom/google/android/gms/games/realtime/b;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    :cond_5
    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/s;->a:Lcom/google/android/gms/games/h/a/ev;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a(Lcom/google/android/gms/games/h/a/ev;)V

    .line 220
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->f:Z

    goto/16 :goto_0

    .line 223
    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    .line 224
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b(Ljava/util/ArrayList;)V

    .line 226
    :try_start_6
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v2, v2, Lcom/google/android/gms/games/realtime/b;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a(Ljava/util/ArrayList;)[Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/games/internal/eg;->b(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_2

    .line 231
    :goto_3
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->f:Z

    goto/16 :goto_0

    .line 229
    :catch_2
    move-exception v0

    const-string v0, "RoomServiceStateMachine"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 234
    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/h/a/ev;

    .line 235
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a(Lcom/google/android/gms/games/h/a/ev;)V

    .line 236
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->f:Z

    goto/16 :goto_0

    .line 239
    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/o;

    .line 240
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/o;->a:Lcom/google/android/gms/games/realtime/a;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/a;->a:Ljava/lang/String;

    .line 241
    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/realtime/b;->d(Ljava/lang/String;)Lcom/google/android/gms/games/realtime/c;

    move-result-object v5

    .line 243
    if-eqz v5, :cond_a

    .line 244
    sget-object v1, Lcom/google/android/gms/games/c/a;->M:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_8

    :cond_6
    :goto_4
    if-nez v3, :cond_7

    .line 246
    :try_start_7
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    iget-object v2, v5, Lcom/google/android/gms/games/realtime/c;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/gms/games/internal/eg;->g(Ljava/lang/String;)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_3

    .line 251
    :goto_5
    const-string v1, "CONNECTION_FAILED"

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/o;->a:Lcom/google/android/gms/games/realtime/a;

    invoke-direct {p0, v5, v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a(Lcom/google/android/gms/games/realtime/c;Ljava/lang/String;Lcom/google/android/gms/games/realtime/a;)V

    .line 259
    :cond_7
    :goto_6
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->f:Z

    goto/16 :goto_0

    .line 244
    :cond_8
    iget v1, v5, Lcom/google/android/gms/games/realtime/c;->c:I

    if-ne v1, v7, :cond_6

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->m:Ljava/util/Hashtable;

    iget-object v2, v5, Lcom/google/android/gms/games/realtime/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-nez v1, :cond_1f

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object v2, v1

    :goto_7
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    sget-object v1, Lcom/google/android/gms/games/c/a;->M:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ge v6, v1, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Attempting to reconnect to: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v5, Lcom/google/android/gms/games/realtime/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v3, v1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V

    const-string v3, "RoomServiceStateMachine"

    invoke-static {v3, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v5}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a(Lcom/google/android/gms/games/realtime/c;)V

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->m:Ljava/util/Hashtable;

    iget-object v3, v5, Lcom/google/android/gms/games/realtime/c;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->o:Lcom/google/android/gms/games/service/statemachine/roomclient/cf;

    iget-object v2, v5, Lcom/google/android/gms/games/realtime/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;->a(Ljava/lang/String;)Lcom/google/android/gms/games/service/statemachine/roomclient/cg;

    move-result-object v1

    iget v2, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/cg;->a:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/cg;->a:I

    move v3, v4

    goto :goto_4

    :cond_9
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Could not reconnect to: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v5, Lcom/google/android/gms/games/realtime/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " after "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " tries"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v2, v1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V

    const-string v2, "RoomServiceStateMachine"

    invoke-static {v2, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 249
    :catch_3
    move-exception v1

    const-string v1, "RoomServiceStateMachine"

    const-string v2, "Room client is not connected."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 255
    :cond_a
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->h:Z

    if-eqz v0, :cond_7

    .line 256
    const-string v0, "RoomServiceStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "failedParticipant is null for: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 262
    :pswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/p;

    .line 263
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/p;->a:Lcom/google/android/gms/games/realtime/a;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/a;->a:Ljava/lang/String;

    .line 264
    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/realtime/b;->d(Ljava/lang/String;)Lcom/google/android/gms/games/realtime/c;

    move-result-object v2

    .line 266
    if-eqz v2, :cond_c

    .line 268
    :try_start_8
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    iget-object v3, v2, Lcom/google/android/gms/games/realtime/c;->a:Ljava/lang/String;

    invoke-interface {v1, v3}, Lcom/google/android/gms/games/internal/eg;->f(Ljava/lang/String;)V
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_4

    .line 272
    :goto_8
    const-string v1, "CONNECTION_ESTABLISHED"

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/p;->a:Lcom/google/android/gms/games/realtime/a;

    invoke-direct {p0, v2, v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a(Lcom/google/android/gms/games/realtime/c;Ljava/lang/String;Lcom/google/android/gms/games/realtime/a;)V

    .line 274
    sget-object v0, Lcom/google/android/gms/games/c/a;->M:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->m:Ljava/util/Hashtable;

    iget-object v1, v2, Lcom/google/android/gms/games/realtime/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_b

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Reestablished peer connection to "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v2, Lcom/google/android/gms/games/realtime/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " after "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " tries"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->o:Lcom/google/android/gms/games/service/statemachine/roomclient/cf;

    iget-object v3, v2, Lcom/google/android/gms/games/realtime/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;->a(Ljava/lang/String;)Lcom/google/android/gms/games/service/statemachine/roomclient/cg;

    move-result-object v1

    iget v3, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/cg;->b:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/cg;->b:I

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V

    const-string v1, "RoomServiceStateMachine"

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->m:Ljava/util/Hashtable;

    iget-object v1, v2, Lcom/google/android/gms/games/realtime/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    :cond_b
    :goto_9
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->f:Z

    goto/16 :goto_0

    .line 270
    :catch_4
    move-exception v1

    const-string v1, "RoomServiceStateMachine"

    const-string v3, "Room client is not connected."

    invoke-static {v1, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    .line 276
    :cond_c
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->h:Z

    if-eqz v0, :cond_b

    .line 277
    const-string v0, "RoomServiceStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "successParticipant is null for: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9

    .line 283
    :pswitch_8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/l;

    .line 284
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/l;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/realtime/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 285
    iget v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/l;->c:I

    if-eqz v1, :cond_d

    const-string v1, "Reliable"

    .line 286
    :goto_a
    if-nez v2, :cond_e

    .line 287
    const-string v2, "RoomServiceStateMachine"

    const-string v5, "%s message received: %s, who is not a participant in the room"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v1, v6, v3

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/l;->b:Ljava/lang/String;

    aput-object v0, v6, v4

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    :goto_b
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->f:Z

    goto/16 :goto_0

    .line 285
    :cond_d
    const-string v1, "Unreliable"

    goto :goto_a

    .line 291
    :cond_e
    const-string v5, "RoomServiceStateMachine"

    const-string v6, "%s message received : %s"

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v1, v7, v3

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/l;->b:Ljava/lang/String;

    aput-object v1, v7, v4

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    :try_start_9
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/l;->a:[B

    iget v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/l;->c:I

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/games/internal/eg;->a(Ljava/lang/String;[BI)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_5

    goto :goto_b

    .line 297
    :catch_5
    move-exception v0

    const-string v0, "RoomServiceStateMachine"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_b

    .line 303
    :pswitch_9
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/q;

    .line 304
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/b;->b:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/q;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/q;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/realtime/b;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_10

    .line 309
    :cond_f
    :try_start_a
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    const/16 v2, 0x1b59

    iget v3, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/q;->a:I

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/q;->c:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/games/internal/eg;->a(IILjava/lang/String;)V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_a} :catch_6

    .line 344
    :goto_c
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->f:Z

    goto/16 :goto_0

    .line 312
    :catch_6
    move-exception v0

    .line 313
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->e:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Landroid/os/RemoteException;)V

    goto :goto_c

    .line 316
    :cond_10
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/q;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/realtime/b;->e(Ljava/lang/String;)Lcom/google/android/gms/games/realtime/c;

    move-result-object v1

    .line 318
    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-boolean v2, v2, Lcom/google/android/gms/games/realtime/b;->c:Z

    if-eqz v2, :cond_12

    if-eqz v1, :cond_12

    iget-object v2, v1, Lcom/google/android/gms/games/realtime/c;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_12

    iget-boolean v2, v1, Lcom/google/android/gms/games/realtime/c;->d:Z

    if-eqz v2, :cond_12

    .line 321
    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->b:Lcom/google/android/gms/games/realtime/network/b;

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/q;->b:[B

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/c;->b:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Lcom/google/android/gms/games/realtime/network/b;->a([BLjava/lang/String;)I

    move-result v1

    .line 323
    const/4 v2, -0x1

    if-eq v1, v2, :cond_11

    .line 324
    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/q;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_c

    .line 327
    :cond_11
    :try_start_b
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    const/16 v2, 0x1b59

    iget v3, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/q;->a:I

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/q;->c:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/games/internal/eg;->a(IILjava/lang/String;)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_b} :catch_7

    goto :goto_c

    .line 331
    :catch_7
    move-exception v0

    const-string v0, "RoomServiceStateMachine"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c

    .line 336
    :cond_12
    :try_start_c
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    const/16 v2, 0x1b5b

    iget v3, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/q;->a:I

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/q;->c:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/games/internal/eg;->a(IILjava/lang/String;)V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_c} :catch_8

    goto :goto_c

    .line 340
    :catch_8
    move-exception v0

    const-string v0, "RoomServiceStateMachine"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c

    .line 347
    :pswitch_a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/r;

    .line 350
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/r;->b:[Ljava/lang/String;

    if-eqz v1, :cond_16

    .line 352
    iget-object v5, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/r;->b:[Ljava/lang/String;

    array-length v6, v5

    move v2, v3

    move v1, v4

    :goto_d
    if-ge v2, v6, :cond_15

    aget-object v7, v5, v2

    .line 353
    iget-object v8, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v8, v8, Lcom/google/android/gms/games/realtime/b;->b:Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_13

    iget-object v8, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    invoke-virtual {v8, v7}, Lcom/google/android/gms/games/realtime/b;->c(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_14

    .line 356
    :cond_13
    const-string v1, "RoomServiceStateMachine"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Can\'t send message to self or to invalid peer "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v3

    .line 352
    :cond_14
    add-int/lit8 v2, v2, 0x1

    goto :goto_d

    .line 360
    :cond_15
    if-nez v1, :cond_16

    .line 361
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->f:Z

    goto/16 :goto_0

    .line 364
    :cond_16
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-boolean v1, v1, Lcom/google/android/gms/games/realtime/b;->c:Z

    if-eqz v1, :cond_1b

    .line 365
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/r;->b:[Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a([Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 367
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/r;->b:[Ljava/lang/String;

    if-eqz v1, :cond_18

    move v2, v4

    .line 368
    :goto_e
    new-instance v4, Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v4, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 369
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    :goto_f
    if-ge v3, v6, :cond_1a

    .line 370
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/realtime/c;

    .line 371
    if-eqz v1, :cond_19

    iget-object v7, v1, Lcom/google/android/gms/games/realtime/c;->b:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_19

    iget-boolean v7, v1, Lcom/google/android/gms/games/realtime/c;->d:Z

    if-eqz v7, :cond_19

    .line 374
    iget-object v1, v1, Lcom/google/android/gms/games/realtime/c;->b:Ljava/lang/String;

    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 369
    :cond_17
    :goto_10
    add-int/lit8 v3, v3, 0x1

    goto :goto_f

    :cond_18
    move v2, v3

    .line 367
    goto :goto_e

    .line 375
    :cond_19
    if-eqz v2, :cond_17

    .line 376
    const-string v1, "RoomServiceStateMachine"

    const-string v7, "Attempting to send an unreliable message to participant who is not in connected set."

    invoke-static {v1, v7}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_10

    .line 380
    :cond_1a
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->b:Lcom/google/android/gms/games/realtime/network/b;

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/r;->a:[B

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/games/realtime/network/b;->a([B[Ljava/lang/String;)V

    .line 386
    :goto_11
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->f:Z

    goto/16 :goto_0

    .line 383
    :cond_1b
    const-string v0, "RoomServiceStateMachine"

    const-string v1, "Attempting to send an unreliable message to participants when not in connected set."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_11

    .line 389
    :pswitch_b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/m;

    .line 390
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a:Ljava/util/Map;

    iget v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/m;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 391
    if-eqz v1, :cond_1d

    .line 392
    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/m;->c:Ljava/lang/String;

    .line 393
    iget-object v3, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/games/realtime/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 394
    if-eqz v2, :cond_1c

    .line 396
    :try_start_d
    iget-object v3, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v3, v3, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    iget v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/m;->b:I

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v3, v0, v1, v2}, Lcom/google/android/gms/games/internal/eg;->a(IILjava/lang/String;)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_d} :catch_9

    .line 402
    :cond_1c
    :goto_12
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->f:Z

    goto/16 :goto_0

    .line 399
    :catch_9
    move-exception v0

    const-string v0, "RoomServiceStateMachine"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_12

    .line 404
    :cond_1d
    const-string v0, "RoomServiceStateMachine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got null token for messageId "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->f:Z

    goto/16 :goto_0

    .line 409
    :pswitch_c
    invoke-direct {p0}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b()V

    .line 410
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    const-string v1, "Disconnecting all peers"

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V

    .line 411
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b:Lcom/google/android/gms/games/realtime/b;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/b;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_13
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 412
    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->b:Lcom/google/android/gms/games/realtime/network/b;

    invoke-interface {v2, v0}, Lcom/google/android/gms/games/realtime/network/b;->d(Ljava/lang/String;)V

    goto :goto_13

    .line 414
    :cond_1e
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->l:Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->c:Lcom/google/android/gms/games/service/statemachine/roomservice/LeavingRoomState;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomservice/LeavingRoomState;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->f:Z

    goto/16 :goto_0

    .line 418
    :pswitch_d
    invoke-direct {p0}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->b()V

    .line 419
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 422
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->l:Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->h:Lcom/google/android/gms/games/service/statemachine/roomservice/WaitNetworkDisconnectState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/WaitNetworkDisconnectState;->b()V

    .line 423
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->f:Z

    goto/16 :goto_0

    .line 425
    :pswitch_e
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->b:Lcom/google/android/gms/games/realtime/network/b;

    invoke-interface {v0}, Lcom/google/android/gms/games/realtime/network/b;->b()V

    .line 427
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->l:Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->h:Lcom/google/android/gms/games/service/statemachine/roomservice/WaitNetworkDisconnectState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/WaitNetworkDisconnectState;->b()V

    .line 429
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->f:Z

    goto/16 :goto_0

    :cond_1f
    move-object v2, v1

    goto/16 :goto_7

    .line 162
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_d
        :pswitch_c
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_6
        :pswitch_8
        :pswitch_b
        :pswitch_9
        :pswitch_a
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_e
    .end packed-switch
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 138
    return-void
.end method
