.class final Lcom/google/android/gms/games/a/ab;
.super Lcom/google/android/gms/games/a/bb;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/a/ce;


# static fields
.field private static final A:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final B:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final C:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final D:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final E:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final F:[Ljava/lang/String;

.field private static final G:[Ljava/lang/String;

.field private static final H:[Ljava/lang/String;

.field private static final I:Ljava/util/HashMap;

.field private static final J:Lcom/google/android/gms/games/provider/a;

.field static final o:Ljava/util/HashMap;

.field private static final q:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final r:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final s:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final t:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final u:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final v:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final w:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final x:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final y:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final z:Ljava/util/concurrent/locks/ReentrantLock;


# instance fields
.field private final K:Lcom/google/android/gms/games/h/a/af;

.field private final L:Lcom/google/android/gms/games/h/a/ag;

.field private final M:Lcom/google/android/gms/games/h/a/n;

.field private final N:Lcom/google/android/gms/common/server/n;

.field private final O:Lcom/google/android/gms/games/b/j;

.field private final P:Lcom/google/android/gms/games/b/j;

.field private final Q:Lcom/google/android/gms/games/b/k;

.field private final R:Lcom/google/android/gms/games/b/n;

.field private final S:Ljava/util/Random;

.field final a:Lcom/google/android/gms/games/a/bb;

.field final b:Lcom/google/android/gms/games/a/bb;

.field final c:Lcom/google/android/gms/games/a/bb;

.field final d:Lcom/google/android/gms/games/a/bb;

.field final e:Lcom/google/android/gms/games/a/bb;

.field final f:Lcom/google/android/gms/games/a/bb;

.field final g:Lcom/google/android/gms/games/a/bb;

.field final h:Lcom/google/android/gms/games/a/bb;

.field final i:Lcom/google/android/gms/games/a/bb;

.field final j:Lcom/google/android/gms/games/a/bb;

.field final k:Lcom/google/android/gms/games/a/bb;

.field final l:Lcom/google/android/gms/games/a/bb;

.field final m:Lcom/google/android/gms/games/a/bb;

.field final n:Lcom/google/android/gms/games/a/bb;

.field final p:[Lcom/google/android/gms/games/a/bb;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 123
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/ab;->q:Ljava/util/concurrent/locks/ReentrantLock;

    .line 126
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/ab;->r:Ljava/util/concurrent/locks/ReentrantLock;

    .line 127
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/ab;->s:Ljava/util/concurrent/locks/ReentrantLock;

    .line 128
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/ab;->t:Ljava/util/concurrent/locks/ReentrantLock;

    .line 129
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/ab;->u:Ljava/util/concurrent/locks/ReentrantLock;

    .line 130
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/ab;->v:Ljava/util/concurrent/locks/ReentrantLock;

    .line 131
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/ab;->w:Ljava/util/concurrent/locks/ReentrantLock;

    .line 132
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/ab;->x:Ljava/util/concurrent/locks/ReentrantLock;

    .line 133
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/ab;->y:Ljava/util/concurrent/locks/ReentrantLock;

    .line 134
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/ab;->z:Ljava/util/concurrent/locks/ReentrantLock;

    .line 135
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/ab;->A:Ljava/util/concurrent/locks/ReentrantLock;

    .line 136
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/ab;->B:Ljava/util/concurrent/locks/ReentrantLock;

    .line 137
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/ab;->C:Ljava/util/concurrent/locks/ReentrantLock;

    .line 138
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/ab;->D:Ljava/util/concurrent/locks/ReentrantLock;

    .line 141
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/ab;->E:Ljava/util/concurrent/locks/ReentrantLock;

    .line 168
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "external_game_id"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/gms/games/a/ab;->F:[Ljava/lang/String;

    .line 192
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "REPLACE_ME"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/gms/games/a/ab;->G:[Ljava/lang/String;

    .line 194
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "external_game_id"

    aput-object v1, v0, v2

    const-string v1, "muted"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/gms/games/a/ab;->H:[Ljava/lang/String;

    .line 243
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/ab;->o:Ljava/util/HashMap;

    .line 245
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/ab;->I:Ljava/util/HashMap;

    .line 249
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 250
    new-instance v1, Lcom/google/android/gms/games/a/ah;

    const-string v2, "game_icon_image_url"

    sget v3, Lcom/google/android/gms/g;->K:I

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/games/a/ah;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 252
    new-instance v1, Lcom/google/android/gms/games/a/ah;

    const-string v2, "game_hi_res_image_url"

    sget v3, Lcom/google/android/gms/g;->J:I

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/games/a/ah;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 254
    sget-object v1, Lcom/google/android/gms/games/a/ab;->o:Ljava/util/HashMap;

    const-string v2, "ICON"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    new-instance v0, Lcom/google/android/gms/games/a/ah;

    const-string v1, "badge_icon_image_url"

    sget v2, Lcom/google/android/gms/g;->K:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/games/a/ah;-><init>(Ljava/lang/String;I)V

    .line 259
    sget-object v1, Lcom/google/android/gms/games/a/ab;->I:Ljava/util/HashMap;

    const-string v2, "ICON"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    invoke-static {}, Lcom/google/android/gms/games/provider/a;->a()Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/k/a;->c:Lcom/google/android/gms/games/provider/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/c;->a(Lcom/google/android/gms/games/provider/a;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/k/a;->i:Lcom/google/android/gms/games/provider/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/c;->a(Lcom/google/android/gms/games/provider/a;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/k/a;->e:Lcom/google/android/gms/games/provider/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/c;->a(Lcom/google/android/gms/games/provider/a;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/c;->a()Lcom/google/android/gms/games/provider/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/a/ab;->J:Lcom/google/android/gms/games/provider/a;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;)V
    .locals 4

    .prologue
    .line 431
    const-string v0, "GameAgent"

    sget-object v1, Lcom/google/android/gms/games/a/ab;->q:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    .line 432
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->S:Ljava/util/Random;

    .line 433
    new-instance v0, Lcom/google/android/gms/games/h/a/af;

    invoke-direct {v0, p2}, Lcom/google/android/gms/games/h/a/af;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->K:Lcom/google/android/gms/games/h/a/af;

    .line 434
    new-instance v0, Lcom/google/android/gms/games/h/a/ag;

    invoke-direct {v0, p3}, Lcom/google/android/gms/games/h/a/ag;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->L:Lcom/google/android/gms/games/h/a/ag;

    .line 435
    new-instance v0, Lcom/google/android/gms/games/h/a/n;

    invoke-direct {v0, p3}, Lcom/google/android/gms/games/h/a/n;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->M:Lcom/google/android/gms/games/h/a/n;

    .line 436
    iput-object p4, p0, Lcom/google/android/gms/games/a/ab;->N:Lcom/google/android/gms/common/server/n;

    .line 437
    new-instance v0, Lcom/google/android/gms/games/b/j;

    sget-object v1, Lcom/google/android/gms/games/a/ab;->J:Lcom/google/android/gms/games/provider/a;

    iget-object v1, v1, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/b/j;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->O:Lcom/google/android/gms/games/b/j;

    .line 438
    new-instance v0, Lcom/google/android/gms/games/b/j;

    sget-object v1, Lcom/google/android/gms/games/a/ab;->J:Lcom/google/android/gms/games/provider/a;

    iget-object v1, v1, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/b/j;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->P:Lcom/google/android/gms/games/b/j;

    .line 439
    new-instance v0, Lcom/google/android/gms/games/b/n;

    sget-object v1, Lcom/google/android/gms/games/a/ab;->J:Lcom/google/android/gms/games/provider/a;

    iget-object v1, v1, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/b/n;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->R:Lcom/google/android/gms/games/b/n;

    .line 440
    new-instance v0, Lcom/google/android/gms/games/b/k;

    sget-object v1, Lcom/google/android/gms/games/a/ab;->J:Lcom/google/android/gms/games/provider/a;

    iget-object v1, v1, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/b/k;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->Q:Lcom/google/android/gms/games/b/k;

    .line 443
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "GamesApplicationSession"

    sget-object v2, Lcom/google/android/gms/games/a/ab;->E:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->n:Lcom/google/android/gms/games/a/bb;

    .line 445
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "FeaturedGames"

    sget-object v2, Lcom/google/android/gms/games/a/ab;->r:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->a:Lcom/google/android/gms/games/a/bb;

    .line 446
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "MultiplayerGames"

    sget-object v2, Lcom/google/android/gms/games/a/ab;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->b:Lcom/google/android/gms/games/a/bb;

    .line 447
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "PopularGames"

    sget-object v2, Lcom/google/android/gms/games/a/ab;->t:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->c:Lcom/google/android/gms/games/a/bb;

    .line 448
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "RecentlyPlayedGames"

    sget-object v2, Lcom/google/android/gms/games/a/ab;->u:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->d:Lcom/google/android/gms/games/a/bb;

    .line 450
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "RecommendedGames"

    sget-object v2, Lcom/google/android/gms/games/a/ab;->v:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->e:Lcom/google/android/gms/games/a/bb;

    .line 451
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "DownloadedGames"

    sget-object v2, Lcom/google/android/gms/games/a/ab;->w:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->f:Lcom/google/android/gms/games/a/bb;

    .line 452
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "InstalledGames"

    sget-object v2, Lcom/google/android/gms/games/a/ab;->x:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->g:Lcom/google/android/gms/games/a/bb;

    .line 453
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "HiddenGames"

    sget-object v2, Lcom/google/android/gms/games/a/ab;->y:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->h:Lcom/google/android/gms/games/a/bb;

    .line 454
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "GameSearch"

    sget-object v2, Lcom/google/android/gms/games/a/ab;->z:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->i:Lcom/google/android/gms/games/a/bb;

    .line 455
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "GameSearchSuggestions"

    sget-object v2, Lcom/google/android/gms/games/a/ab;->A:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->j:Lcom/google/android/gms/games/a/bb;

    .line 457
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "NamedGamesCollection"

    sget-object v2, Lcom/google/android/gms/games/a/ab;->B:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->k:Lcom/google/android/gms/games/a/bb;

    .line 458
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "CommonGames"

    sget-object v2, Lcom/google/android/gms/games/a/ab;->C:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->l:Lcom/google/android/gms/games/a/bb;

    .line 459
    new-instance v0, Lcom/google/android/gms/games/a/bb;

    const-string v1, "DisjointGames"

    sget-object v2, Lcom/google/android/gms/games/a/ab;->D:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->m:Lcom/google/android/gms/games/a/bb;

    .line 461
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/android/gms/games/a/bb;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/a/ab;->a:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/a/ab;->b:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/games/a/ab;->c:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/games/a/ab;->e:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/games/a/ab;->f:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/games/a/ab;->g:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/games/a/ab;->h:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/gms/games/a/ab;->l:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/gms/games/a/ab;->m:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/gms/games/a/ab;->p:[Lcom/google/android/gms/games/a/bb;

    .line 473
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;JJLjava/lang/String;Z)I
    .locals 14

    .prologue
    .line 2788
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 2789
    iget-object v3, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 2790
    invoke-static {v3}, Lcom/google/android/gms/games/provider/s;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v4

    .line 2791
    sub-long v6, p4, p2

    .line 2792
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 2796
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 2797
    const-string v10, "client_context_id"

    invoke-static {v2, v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v5, v10, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2799
    const-string v3, "end_time"

    add-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2800
    const-string v3, "external_game_id"

    iget-object v6, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v5, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2801
    const-string v3, "session_id"

    invoke-virtual {v5, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2802
    const-string v3, "start_time"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2803
    const-string v3, "ad_id"

    move-object/from16 v0, p6

    invoke-virtual {v5, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2804
    const-string v3, "limit_ad_tracking"

    invoke-static/range {p7 .. p7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2805
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 2807
    const/4 v2, 0x0

    return v2
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)J
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1014
    const-wide/16 v0, -0x1

    .line 1015
    invoke-static {p1}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    .line 1016
    new-instance v3, Lcom/google/android/gms/games/a/o;

    invoke-direct {v3, p0}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "last_connection_local_time"

    aput-object v4, v3, v5

    iput-object v3, v2, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    const-string v3, "last_connection_local_time DESC"

    iput-object v3, v2, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v2

    .line 1022
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1023
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 1026
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1028
    return-wide v0

    .line 1026
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/bg;Lcom/google/android/gms/games/h/a/u;Lcom/google/android/gms/games/h/a/ce;Ljava/lang/Long;ZZZLjava/util/ArrayList;)J
    .locals 16

    .prologue
    .line 2179
    if-nez p4, :cond_0

    if-nez p5, :cond_0

    .line 2180
    const-wide/16 v2, -0x1

    .line 2223
    :goto_0
    return-wide v2

    .line 2183
    :cond_0
    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v13

    .line 2187
    invoke-virtual/range {p10 .. p10}, Ljava/util/ArrayList;->size()I

    move-result v14

    .line 2188
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/gms/games/a/ab;->a(Landroid/content/Context;Lcom/google/android/gms/games/h/a/u;)Lcom/google/android/gms/games/h/a/br;

    move-result-object v5

    .line 2189
    move-object/from16 v0, p5

    invoke-static {v5, v0}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/h/a/br;Lcom/google/android/gms/games/h/a/ce;)Lcom/google/android/gms/games/h/a/cg;

    move-result-object v6

    .line 2190
    if-nez p3, :cond_3

    const/4 v2, 0x0

    move-object v12, v2

    .line 2191
    :goto_1
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v2, p1

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-static/range {v2 .. v11}, Lcom/google/android/gms/games/a/ab;->a(Landroid/content/Context;Lcom/google/android/gms/games/h/a/bg;Lcom/google/android/gms/games/h/a/u;Lcom/google/android/gms/games/h/a/br;Lcom/google/android/gms/games/h/a/cg;ZZZLjava/lang/String;Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v2

    .line 2194
    const-string v3, "external_game_id"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2198
    const-string v3, "gameplay_acl_status"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 2202
    if-eqz p9, :cond_1

    .line 2203
    const-string v3, "muted"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2207
    :cond_1
    invoke-static {v13}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    move-object/from16 v0, p10

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2213
    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v7

    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/games/provider/z;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v8

    invoke-static {v8}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v9, "instance_game_id=?"

    sget-object v10, Lcom/google/android/gms/games/a/ab;->G:[Ljava/lang/String;

    invoke-virtual {v3, v9, v10}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const/4 v9, 0x0

    invoke-virtual {v3, v9, v14}, Landroid/content/ContentProviderOperation$Builder;->withSelectionBackReference(II)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    move-object/from16 v0, p10

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {p10 .. p10}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-eqz p4, :cond_7

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/games/h/a/u;->getInstances()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_5

    invoke-static {v5}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v5

    const/4 v2, 0x0

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v11

    move v3, v2

    :goto_2
    if-ge v3, v11, :cond_4

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/br;

    new-instance v13, Landroid/content/ContentValues;

    iget-object v15, v2, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-direct {v13, v15}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/br;->getAndroidInstance()Lcom/google/android/gms/games/h/a/bs;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, v2, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-virtual {v13, v2}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    const-string v2, "package_name"

    invoke-virtual {v13, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/gms/games/ui/d/aa;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    const-string v15, "installed"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v13, v15, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    :cond_2
    invoke-static {v8}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "instance_game_id"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, p10

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 2190
    :cond_3
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/games/h/a/bg;->getSnapshot()Lcom/google/android/gms/games/h/a/ff;

    move-result-object v2

    move-object v12, v2

    goto/16 :goto_1

    .line 2213
    :cond_4
    invoke-static {v7}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "_id=?"

    sget-object v7, Lcom/google/android/gms/games/a/ab;->G:[Ljava/lang/String;

    invoke-virtual {v2, v3, v7}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v14}, Landroid/content/ContentProviderOperation$Builder;->withSelectionBackReference(II)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "target_instance"

    add-int/2addr v5, v9

    invoke-virtual {v2, v3, v5}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, p10

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2217
    :cond_5
    :goto_3
    move-object/from16 v0, p2

    move-object/from16 v1, p10

    invoke-static {v0, v6, v4, v1, v14}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/cg;Ljava/lang/String;Ljava/util/ArrayList;I)V

    .line 2220
    if-eqz v12, :cond_6

    if-nez p6, :cond_8

    .line 2223
    :cond_6
    :goto_4
    if-nez p4, :cond_9

    const-wide/16 v2, -0x1

    goto/16 :goto_0

    .line 2213
    :cond_7
    if-eqz v6, :cond_5

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "platform_type"

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v3, v5, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "instance_display_name"

    const-string v10, "display_name"

    invoke-virtual {v2, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/google/android/gms/games/h/a/cg;->b()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/gms/games/ui/d/aa;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    const-string v10, "package_name"

    invoke-virtual {v3, v10, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "installed"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-static {v8}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "instance_game_id"

    invoke-virtual {v2, v3, v14}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, p10

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v7}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "_id=?"

    sget-object v5, Lcom/google/android/gms/games/a/ab;->G:[Ljava/lang/String;

    invoke-virtual {v2, v3, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v14}, Landroid/content/ContentProviderOperation$Builder;->withSelectionBackReference(II)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "target_instance"

    invoke-virtual {v2, v3, v9}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, p10

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2220
    :cond_8
    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/games/provider/ax;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    iget-object v3, v12, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-virtual {v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "owner_id"

    move-object/from16 v0, p6

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "game_id"

    invoke-virtual {v2, v3, v14}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, p10

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 2223
    :cond_9
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/games/h/a/u;->c()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto/16 :goto_0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/bg;Lcom/google/android/gms/games/h/a/u;Lcom/google/android/gms/games/h/a/ce;ZZLjava/util/ArrayList;)J
    .locals 11

    .prologue
    .line 2143
    const/4 v6, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v10, p8

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/games/a/ab;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/bg;Lcom/google/android/gms/games/h/a/u;Lcom/google/android/gms/games/h/a/ce;Ljava/lang/Long;ZZZLjava/util/ArrayList;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a(Lcom/google/android/gms/games/a/ab;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/bg;Lcom/google/android/gms/games/h/a/u;Lcom/google/android/gms/games/h/a/ce;Ljava/lang/Long;ZLjava/util/ArrayList;)J
    .locals 11

    .prologue
    .line 117
    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/games/a/ab;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/bg;Lcom/google/android/gms/games/h/a/u;Lcom/google/android/gms/games/h/a/ce;Ljava/lang/Long;ZZZLjava/util/ArrayList;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/games/h/a/bg;Lcom/google/android/gms/games/h/a/u;Lcom/google/android/gms/games/h/a/br;Lcom/google/android/gms/games/h/a/cg;ZZZLjava/lang/String;Landroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 5

    .prologue
    .line 3013
    if-eqz p2, :cond_7

    .line 3016
    iget-object v0, p2, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "external_game_id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "display_name"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "game_description"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "developer_name"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "achievement_total_count"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "leaderboard_count"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "muted"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "identity_sharing_confirmed"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "snapshots_enabled"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "theme_color"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentValues;[Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    .line 3027
    const-string v1, "primary_category"

    invoke-virtual {p2}, Lcom/google/android/gms/games/h/a/u;->getCategory()Lcom/google/android/gms/games/h/a/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/v;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3028
    const-string v1, "secondary_category"

    invoke-virtual {p2}, Lcom/google/android/gms/games/h/a/u;->getCategory()Lcom/google/android/gms/games/h/a/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/v;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3029
    const-string v1, "play_enabled_game"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3030
    if-eqz p5, :cond_0

    .line 3031
    const-string v1, "metadata_version"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3033
    :cond_0
    if-eqz p6, :cond_1

    .line 3034
    const-string v1, "metadata_sync_requested"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3036
    :cond_1
    if-eqz p7, :cond_2

    .line 3037
    const-string v1, "achievement_unlocked_count"

    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/bg;->c()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3076
    :cond_2
    :goto_0
    const-string v1, "gameplay_acl_status"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3079
    if-nez p1, :cond_b

    const/4 v1, 0x0

    .line 3081
    :goto_1
    if-eqz v1, :cond_3

    .line 3082
    const-string v2, "last_played_server_time"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3086
    :cond_3
    if-eqz p4, :cond_c

    .line 3087
    iget-object v1, p4, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "availability"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "owned"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "formatted_price"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "price_micros"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "formatted_full_price"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "full_price_micros"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentValues;[Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 3099
    :goto_2
    if-eqz p7, :cond_6

    .line 3102
    const/4 v1, 0x0

    .line 3103
    if-eqz p3, :cond_4

    .line 3105
    iget-object v2, p3, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 3106
    invoke-virtual {p3}, Lcom/google/android/gms/games/h/a/br;->getAndroidInstance()Lcom/google/android/gms/games/h/a/bs;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 3107
    invoke-virtual {p3}, Lcom/google/android/gms/games/h/a/br;->getAndroidInstance()Lcom/google/android/gms/games/h/a/bs;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 3108
    const-string v1, "package_name"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3111
    :cond_4
    if-nez v1, :cond_5

    if-eqz p4, :cond_5

    .line 3113
    invoke-virtual {p4}, Lcom/google/android/gms/games/h/a/cg;->b()Ljava/lang/String;

    move-result-object v1

    .line 3114
    const-string v2, "package_name"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3118
    :cond_5
    const-string v2, "installed"

    if-eqz v1, :cond_d

    invoke-static {p0, v1}, Lcom/google/android/gms/games/ui/d/aa;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    const/4 v1, 0x1

    :goto_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 3121
    if-nez p1, :cond_e

    const/4 v1, 0x0

    .line 3122
    :goto_4
    if-eqz v1, :cond_6

    .line 3123
    iget-object v1, v1, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 3124
    if-nez p9, :cond_f

    const/4 v1, 0x0

    .line 3126
    :goto_5
    if-eqz v1, :cond_6

    invoke-virtual {v1, p8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 3127
    invoke-virtual {v0, p9}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 3136
    :cond_6
    if-eqz p2, :cond_10

    .line 3137
    invoke-virtual {p2}, Lcom/google/android/gms/games/h/a/u;->getAssets()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/google/android/gms/games/a/ab;->a(Landroid/content/Context;Ljava/util/ArrayList;Landroid/content/ContentValues;)V

    .line 3142
    :goto_6
    return-object v0

    .line 3040
    :cond_7
    if-eqz p4, :cond_a

    .line 3041
    iget-object v0, p4, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "external_game_id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "display_name"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "game_description"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "developer_name"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentValues;[Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    .line 3048
    iget-object v1, p4, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    const-string v2, "primary_category"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3049
    if-eqz v1, :cond_8

    .line 3050
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 3051
    array-length v2, v1

    if-lez v2, :cond_8

    .line 3052
    const-string v2, "primary_category"

    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3053
    array-length v2, v1

    const/4 v3, 0x1

    if-le v2, v3, :cond_8

    .line 3054
    const-string v2, "secondary_category"

    const/4 v3, 0x1

    aget-object v1, v1, v3

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3060
    :cond_8
    const-string v1, "play_enabled_game"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3061
    const-string v1, "achievement_total_count"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3062
    if-eqz p7, :cond_9

    .line 3064
    const-string v1, "achievement_unlocked_count"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3066
    :cond_9
    const-string v1, "leaderboard_count"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3067
    const-string v1, "metadata_version"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3068
    const-string v1, "identity_sharing_confirmed"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 3071
    :cond_a
    const-string v0, "GameAgent"

    const-string v1, "Received application with no app data and no Market data!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3072
    const/4 v0, 0x0

    goto/16 :goto_6

    .line 3079
    :cond_b
    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/bg;->b()Ljava/lang/Long;

    move-result-object v1

    goto/16 :goto_1

    .line 3095
    :cond_c
    const-string v1, "availability"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3096
    const-string v1, "owned"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_2

    .line 3118
    :cond_d
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 3121
    :cond_e
    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/bg;->getSnapshot()Lcom/google/android/gms/games/h/a/ff;

    move-result-object v1

    goto/16 :goto_4

    .line 3124
    :cond_f
    const-string v1, "external_player_id"

    invoke-virtual {p9, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5

    .line 3139
    :cond_10
    invoke-virtual {p4}, Lcom/google/android/gms/games/h/a/cg;->getImages()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/google/android/gms/games/a/ab;->a(Landroid/content/Context;Ljava/util/ArrayList;Landroid/content/ContentValues;)V

    goto/16 :goto_6
.end method

.method private a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/b/j;Ljava/lang/String;ZLjava/lang/String;ZIZLcom/google/android/gms/games/a/af;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 18

    .prologue
    .line 2529
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 2530
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    move-object/from16 v17, v0

    .line 2531
    if-eqz p4, :cond_1

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    .line 2534
    :goto_0
    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Lcom/google/android/gms/games/b/j;->a(Ljava/lang/Object;)V

    .line 2535
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    .line 2538
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/gms/games/a/au;->g:Z

    if-eqz v2, :cond_0

    .line 2539
    invoke-virtual/range {p2 .. p3}, Lcom/google/android/gms/games/b/j;->c(Ljava/lang/Object;)V

    :cond_0
    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v6, p7

    move/from16 v7, p8

    .line 2543
    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/games/b/j;->a(Ljava/lang/Object;JIZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2544
    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    .line 2591
    :goto_1
    return-object v2

    .line 2531
    :cond_1
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    goto :goto_0

    .line 2549
    :cond_2
    const/4 v15, 0x0

    .line 2550
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v4, v5}, Lcom/google/android/gms/games/b/j;->a(Ljava/lang/Object;J)Z

    move-result v3

    .line 2551
    if-nez p8, :cond_3

    if-eqz v3, :cond_4

    .line 2555
    :cond_3
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v4, v5}, Lcom/google/android/gms/games/b/j;->b(Ljava/lang/Object;J)Ljava/lang/String;

    move-result-object v15

    .line 2557
    :cond_4
    invoke-static {v9}, Lcom/google/android/gms/games/d/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    .line 2561
    if-eqz p4, :cond_6

    .line 2562
    :try_start_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/games/a/ab;->L:Lcom/google/android/gms/games/h/a/ag;

    const-string v10, "android:12"

    invoke-static/range {p6 .. p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    invoke-static {v9}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v13

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    const-string v16, "ANDROID"

    move-object/from16 v7, v17

    move-object/from16 v9, p5

    invoke-virtual/range {v6 .. v16}, Lcom/google/android/gms/games/h/a/ag;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/h/a/ab;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 2581
    :goto_2
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/ab;->getItems()Ljava/util/ArrayList;

    move-result-object v8

    .line 2584
    if-eqz p9, :cond_5

    .line 2585
    move-object/from16 v0, p9

    invoke-interface {v0, v8}, Lcom/google/android/gms/games/a/af;->a(Ljava/util/ArrayList;)V

    .line 2589
    :cond_5
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/ab;->b()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-wide v12, v4

    invoke-direct/range {v6 .. v13}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Ljava/lang/String;Lcom/google/android/gms/games/b/u;Ljava/lang/Object;J)V

    .line 2591
    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto :goto_1

    .line 2567
    :cond_6
    :try_start_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/games/a/ab;->L:Lcom/google/android/gms/games/h/a/ag;

    const-string v2, "android:12"

    invoke-static/range {p6 .. p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-static {v9}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const-string v10, "ANDROID"

    const-string v12, "applications/%1$s"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static/range {p5 .. p5}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "clientRevision"

    invoke-static {v2}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v12, v13, v2}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v11, :cond_7

    const-string v12, "deviceType"

    invoke-static {v11}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v2, v12, v11}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_7
    if-eqz v7, :cond_8

    const-string v11, "filterPlayable"

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v11, v7}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_8
    if-eqz v8, :cond_9

    const-string v7, "language"

    invoke-static {v8}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v7, v8}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_9
    if-eqz v9, :cond_a

    const-string v7, "maxResults"

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v7, v8}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_a
    if-eqz v15, :cond_b

    const-string v7, "pageToken"

    invoke-static {v15}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v7, v8}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_b
    const-string v7, "platformType"

    invoke-static {v10}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v7, v8}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iget-object v6, v6, Lcom/google/android/gms/games/h/a/ag;->a:Lcom/google/android/gms/common/server/n;

    const/4 v8, 0x0

    const/4 v10, 0x0

    const-class v11, Lcom/google/android/gms/games/h/a/ab;

    move-object/from16 v7, v17

    invoke-virtual/range {v6 .. v11}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/ab;
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    .line 2572
    :catch_0
    move-exception v2

    const-string v4, "GameAgent"

    invoke-static {v2, v4}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 2574
    if-eqz v3, :cond_c

    .line 2576
    const/4 v2, 0x3

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/b/j;->a(Ljava/lang/Object;I)V

    .line 2578
    :cond_c
    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto/16 :goto_1
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;Ljava/util/ArrayList;)Lcom/google/android/gms/games/a/ac;
    .locals 11

    .prologue
    const/4 v9, 0x0

    .line 861
    const-string v7, "ANDROID"

    .line 862
    const/4 v6, 0x0

    .line 863
    const/4 v0, 0x1

    .line 864
    new-instance v10, Lcom/google/android/gms/games/a/ac;

    invoke-direct {v10}, Lcom/google/android/gms/games/a/ac;-><init>()V

    .line 865
    invoke-static {p4}, Lcom/google/android/gms/games/a/l;->a(Ljava/util/ArrayList;)Lcom/google/android/gms/games/h/a/y;

    move-result-object v8

    .line 868
    :goto_0
    if-nez v0, :cond_0

    if-eqz v6, :cond_3

    .line 870
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->L:Lcom/google/android/gms/games/h/a/ag;

    const-string v2, "android:12"

    invoke-static {p1}, Lcom/google/android/gms/games/d/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {p1}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    move-object v1, p2

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/games/h/a/ag;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/h/a/y;)Lcom/google/android/gms/games/h/a/z;

    move-result-object v0

    .line 875
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/z;->b()Ljava/lang/String;

    move-result-object v6

    .line 876
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/z;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 877
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/z;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, v10, Lcom/google/android/gms/games/a/ac;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 879
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/z;->getInvalidIds()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 880
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/z;->getInvalidIds()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v10, v0}, Lcom/google/android/gms/games/a/ac;->a(Ljava/util/ArrayList;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    move v0, v9

    .line 882
    goto :goto_0

    .line 884
    :catch_0
    move-exception v0

    iget-object v0, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    .line 885
    const-string v0, "GameAgent"

    const-string v1, "Unable to retrieve applications from network"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 888
    :cond_3
    return-object v10
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;Ljava/util/HashMap;)Lcom/google/android/gms/games/a/ac;
    .locals 14

    .prologue
    .line 819
    const-string v9, "ANDROID"

    .line 820
    const/4 v5, 0x0

    .line 821
    const/4 v4, 0x1

    .line 822
    new-instance v10, Lcom/google/android/gms/games/a/ac;

    invoke-direct {v10}, Lcom/google/android/gms/games/a/ac;-><init>()V

    .line 823
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p4 .. p4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    new-instance v8, Lcom/google/android/gms/games/h/a/ad;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v8, v2, v3}, Lcom/google/android/gms/games/h/a/ad;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v6, Lcom/google/android/gms/games/h/a/ae;

    invoke-direct {v6, v7}, Lcom/google/android/gms/games/h/a/ae;-><init>(Ljava/util/ArrayList;)V

    move v2, v4

    move-object v3, v5

    .line 826
    :goto_1
    if-nez v2, :cond_1

    if-eqz v3, :cond_8

    .line 827
    :cond_1
    const/4 v8, 0x0

    .line 828
    :try_start_0
    iget-object v4, p0, Lcom/google/android/gms/games/a/ab;->L:Lcom/google/android/gms/games/h/a/ag;

    const-string v2, "android:12"

    invoke-static {p1}, Lcom/google/android/gms/games/d/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-static {p1}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "applicationUpdates"

    const-string v13, "clientRevision"

    invoke-static {v2}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v12, v13, v2}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v5, :cond_2

    const-string v12, "deviceType"

    invoke-static {v5}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v12, v5}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_2
    if-eqz v7, :cond_3

    const-string v5, "includeMarketData"

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v5, v7}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_3
    if-eqz v11, :cond_4

    const-string v5, "language"

    invoke-static {v11}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v5, v7}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_4
    if-eqz v3, :cond_5

    const-string v5, "pageToken"

    invoke-static {v3}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v5, v3}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_5
    const-string v3, "platformType"

    invoke-static {v9}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v5}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v2, v4, Lcom/google/android/gms/games/h/a/ag;->a:Lcom/google/android/gms/common/server/n;

    const/4 v4, 0x1

    const-class v7, Lcom/google/android/gms/games/h/a/aa;

    move-object/from16 v3, p2

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/aa;

    .line 833
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/aa;->b()Ljava/lang/String;

    move-result-object v3

    .line 834
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/aa;->getItems()Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 835
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/aa;->getItems()Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, v10, Lcom/google/android/gms/games/a/ac;->c:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 837
    :cond_6
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/aa;->getInvalidIds()Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 838
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/aa;->getInvalidIds()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/google/android/gms/games/a/ac;->a(Ljava/util/ArrayList;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    :cond_7
    move v2, v8

    .line 841
    goto/16 :goto_1

    .line 843
    :catch_0
    move-exception v2

    move-object/from16 v0, p3

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    .line 844
    const-string v2, "GameAgent"

    const-string v3, "Unable to retrieve applications from network"

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 847
    :cond_8
    return-object v10
.end method

.method static synthetic a(Lcom/google/android/gms/games/a/ab;)Lcom/google/android/gms/games/h/a/af;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->K:Lcom/google/android/gms/games/h/a/af;

    return-object v0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/games/h/a/u;)Lcom/google/android/gms/games/h/a/br;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 2366
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/u;->getInstances()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/u;->getInstances()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 2368
    :cond_0
    const/4 v0, 0x0

    .line 2397
    :cond_1
    :goto_0
    return-object v0

    .line 2373
    :cond_2
    const/4 v2, -0x1

    .line 2374
    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/u;->getInstances()Ljava/util/ArrayList;

    move-result-object v4

    .line 2375
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v3

    :goto_1
    if-ge v1, v5, :cond_3

    .line 2376
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/br;

    .line 2377
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/br;->getAndroidInstance()Lcom/google/android/gms/games/h/a/bs;

    move-result-object v6

    .line 2378
    if-eqz v6, :cond_4

    .line 2379
    iget-object v7, v6, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    const-string v8, "package_name"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2380
    invoke-static {p0, v7}, Lcom/google/android/gms/games/ui/d/aa;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 2386
    if-gez v2, :cond_4

    .line 2387
    invoke-virtual {v6}, Lcom/google/android/gms/games/h/a/bs;->b()Ljava/lang/Boolean;

    move-result-object v0

    .line 2388
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 2375
    :goto_2
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_1

    .line 2396
    :cond_3
    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2397
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/br;

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method private static a(Lcom/google/android/gms/games/h/a/br;Lcom/google/android/gms/games/h/a/ce;)Lcom/google/android/gms/games/h/a/cg;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2412
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/ce;->getInstances()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/ce;->getInstances()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 2414
    :cond_0
    const/4 v0, 0x0

    .line 2434
    :cond_1
    :goto_0
    return-object v0

    .line 2417
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/ce;->getInstances()Ljava/util/ArrayList;

    move-result-object v3

    .line 2418
    if-eqz p0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/games/h/a/br;->getAndroidInstance()Lcom/google/android/gms/games/h/a/bs;

    move-result-object v0

    if-nez v0, :cond_4

    .line 2419
    :cond_3
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/cg;

    goto :goto_0

    .line 2424
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/games/h/a/br;->getAndroidInstance()Lcom/google/android/gms/games/h/a/bs;

    move-result-object v0

    .line 2425
    iget-object v0, v0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    const-string v1, "package_name"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2426
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v2

    :goto_1
    if-ge v1, v5, :cond_5

    .line 2427
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/cg;

    .line 2428
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/cg;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 2426
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2434
    :cond_5
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/cg;

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;
    .locals 25

    .prologue
    .line 2613
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 2614
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 2616
    invoke-static {v2, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/content/ContentValues;

    move-result-object v11

    .line 2619
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v13

    .line 2620
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 2622
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 2623
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 2624
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 2625
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 2626
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 2627
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 2628
    invoke-static {v3}, Lcom/google/android/gms/games/provider/ad;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v21

    .line 2629
    const/4 v3, 0x0

    move v12, v3

    :goto_0
    if-ge v12, v13, :cond_3

    .line 2630
    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/games/h/a/bg;

    .line 2631
    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/bg;->getGamesData()Lcom/google/android/gms/games/h/a/u;

    move-result-object v4

    .line 2632
    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/bg;->getMarketData()Lcom/google/android/gms/games/h/a/ce;

    move-result-object v6

    .line 2633
    invoke-static {v2, v4}, Lcom/google/android/gms/games/a/ab;->a(Landroid/content/Context;Lcom/google/android/gms/games/h/a/u;)Lcom/google/android/gms/games/h/a/br;

    move-result-object v5

    .line 2634
    invoke-static {v5, v6}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/h/a/br;Lcom/google/android/gms/games/h/a/ce;)Lcom/google/android/gms/games/h/a/cg;

    move-result-object v6

    .line 2637
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-static/range {v2 .. v11}, Lcom/google/android/gms/games/a/ab;->a(Landroid/content/Context;Lcom/google/android/gms/games/h/a/bg;Lcom/google/android/gms/games/h/a/u;Lcom/google/android/gms/games/h/a/br;Lcom/google/android/gms/games/h/a/cg;ZZZLjava/lang/String;Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v5

    .line 2641
    if-eqz v5, :cond_2

    .line 2646
    if-eqz p3, :cond_0

    .line 2647
    const-string v3, "installed"

    invoke-virtual {v5, v3}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2648
    :cond_0
    const-string v3, "game_icon_image_url"

    invoke-virtual {v5, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2654
    const-string v4, "game_hi_res_image_url"

    invoke-virtual {v5, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2655
    const-string v7, "featured_image_url"

    invoke-virtual {v5, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2656
    const-string v8, "cover_icon_image_url"

    invoke-virtual {v5, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2657
    move-object/from16 v0, v21

    invoke-static {v0, v3, v14}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v9

    .line 2658
    move-object/from16 v0, v21

    invoke-static {v0, v4, v14}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v10

    .line 2659
    move-object/from16 v0, v21

    invoke-static {v0, v7, v14}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v7

    .line 2660
    move-object/from16 v0, v21

    invoke-static {v0, v8, v14}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v8

    .line 2667
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Lcom/google/android/gms/games/h/a/cg;->getBadges()Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v6}, Lcom/google/android/gms/games/h/a/cg;->getBadges()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 2672
    invoke-virtual {v6}, Lcom/google/android/gms/games/h/a/cg;->getBadges()Ljava/util/ArrayList;

    move-result-object v6

    .line 2673
    const/4 v3, 0x0

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v22

    move v4, v3

    :goto_1
    move/from16 v0, v22

    if-ge v4, v0, :cond_2

    .line 2674
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/games/h/a/cf;

    .line 2675
    new-instance v23, Landroid/content/ContentValues;

    iget-object v0, v3, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    move-object/from16 v24, v0

    invoke-direct/range {v23 .. v24}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 2676
    move-object/from16 v0, v23

    invoke-static {v2, v3, v0}, Lcom/google/android/gms/games/a/ab;->a(Landroid/content/Context;Lcom/google/android/gms/games/h/a/cf;Landroid/content/ContentValues;)V

    .line 2677
    const-string v3, "badge_icon_image_url"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2678
    move-object/from16 v0, v21

    invoke-static {v0, v3, v14}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v3

    .line 2680
    new-instance v24, Landroid/content/ContentValues;

    move-object/from16 v0, v24

    invoke-direct {v0, v5}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 2681
    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 2682
    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2683
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2684
    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2685
    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2686
    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2687
    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2673
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 2691
    :cond_1
    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2692
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2693
    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2694
    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2695
    const/4 v3, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2696
    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2629
    :cond_2
    add-int/lit8 v3, v12, 0x1

    move v12, v3

    goto/16 :goto_0

    .line 2700
    :cond_3
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne v3, v4, :cond_4

    const/4 v3, 0x1

    :goto_2
    invoke-static {v3}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 2701
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne v3, v4, :cond_5

    const/4 v3, 0x1

    :goto_3
    invoke-static {v3}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 2702
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne v3, v4, :cond_6

    const/4 v3, 0x1

    :goto_4
    invoke-static {v3}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 2703
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne v3, v4, :cond_7

    const/4 v3, 0x1

    :goto_5
    invoke-static {v3}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 2704
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne v3, v4, :cond_8

    const/4 v3, 0x1

    :goto_6
    invoke-static {v3}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 2709
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "GameAgent"

    invoke-static {v2, v14, v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 2711
    const/4 v2, 0x0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v2

    :goto_7
    if-ge v4, v6, :cond_9

    .line 2712
    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ContentValues;

    .line 2713
    const-string v7, "game_icon_image_url"

    const-string v8, "game_icon_image_uri"

    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-static {v2, v7, v8, v5, v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 2715
    const-string v7, "game_hi_res_image_url"

    const-string v8, "game_hi_res_image_uri"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-static {v2, v7, v8, v5, v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 2717
    const-string v7, "featured_image_url"

    const-string v8, "featured_image_uri"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-static {v2, v7, v8, v5, v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 2719
    const-string v7, "badge_icon_image_url"

    const-string v8, "badge_icon_image_uri"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-static {v2, v7, v8, v5, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 2724
    const-string v7, "cover_icon_image_url"

    const-string v8, "cover_icon_image_uri"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-static {v2, v7, v8, v5, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 2711
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_7

    .line 2700
    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 2701
    :cond_5
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 2702
    :cond_6
    const/4 v3, 0x0

    goto/16 :goto_4

    .line 2703
    :cond_7
    const/4 v3, 0x0

    goto/16 :goto_5

    .line 2704
    :cond_8
    const/4 v3, 0x0

    goto :goto_6

    .line 2729
    :cond_9
    return-object v20
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 926
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 927
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_0

    .line 928
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/a/ag;

    .line 929
    iget-object v5, v0, Lcom/google/android/gms/games/a/ag;->a:Ljava/lang/String;

    invoke-static {p1, v5}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 930
    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v6, "metadata_version"

    iget-wide v8, v0, Lcom/google/android/gms/games/a/ag;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v6, "sync_token"

    iget-object v0, v0, Lcom/google/android/gms/games/a/ag;->c:Ljava/lang/String;

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "metadata_sync_requested"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-static {v1}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v5

    invoke-virtual {v0, v5}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 927
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 937
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "GameAgent"

    invoke-static {v0, v3, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 938
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/games/h/a/cf;Landroid/content/ContentValues;)V
    .locals 6

    .prologue
    .line 2753
    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/cf;->getImages()Ljava/util/ArrayList;

    move-result-object v3

    .line 2754
    const/4 v0, 0x0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    .line 2755
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/bp;

    .line 2756
    sget-object v1, Lcom/google/android/gms/games/a/ab;->I:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bp;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/a/ah;

    .line 2757
    if-eqz v1, :cond_0

    .line 2758
    new-instance v5, Lcom/google/android/gms/common/internal/bq;

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bp;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Lcom/google/android/gms/common/internal/bq;-><init>(Ljava/lang/String;)V

    iget v0, v1, Lcom/google/android/gms/games/a/ah;->b:I

    invoke-virtual {v5, p0, v0}, Lcom/google/android/gms/common/internal/bq;->a(Landroid/content/Context;I)Lcom/google/android/gms/common/internal/br;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/br;->a()Ljava/lang/String;

    move-result-object v0

    .line 2761
    iget-object v1, v1, Lcom/google/android/gms/games/a/ah;->a:Ljava/lang/String;

    invoke-virtual {p2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2754
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2764
    :cond_1
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/util/ArrayList;Landroid/content/ContentValues;)V
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 2440
    if-nez p1, :cond_1

    .line 2467
    :cond_0
    return-void

    .line 2445
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 2446
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 2448
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v5, v4

    :goto_0
    if-ge v5, v7, :cond_0

    .line 2449
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/bp;

    .line 2450
    sget-object v1, Lcom/google/android/gms/games/a/ab;->o:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bp;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 2451
    if-eqz v1, :cond_2

    .line 2452
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v3, v4

    :goto_1
    if-ge v3, v8, :cond_3

    .line 2453
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/a/ah;

    .line 2454
    new-instance v9, Lcom/google/android/gms/common/internal/bq;

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bp;->d()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/google/android/gms/common/internal/bq;-><init>(Ljava/lang/String;)V

    iget v10, v2, Lcom/google/android/gms/games/a/ah;->b:I

    invoke-virtual {v9, p0, v10}, Lcom/google/android/gms/common/internal/bq;->a(Landroid/content/Context;I)Lcom/google/android/gms/common/internal/br;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/gms/common/internal/br;->a()Ljava/lang/String;

    move-result-object v9

    .line 2457
    iget-object v2, v2, Lcom/google/android/gms/games/a/ah;->a:Ljava/lang/String;

    invoke-virtual {p2, v2, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2452
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 2459
    :cond_2
    const-string v1, "FEATURE_GRAPHIC"

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bp;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2461
    new-instance v1, Lcom/google/android/gms/common/internal/bq;

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bp;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/common/internal/bq;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Lcom/google/android/gms/common/internal/bq;->a(I)Lcom/google/android/gms/common/internal/br;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/br;->a()Ljava/lang/String;

    move-result-object v0

    .line 2464
    const-string v1, "featured_image_url"

    invoke-virtual {p2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2448
    :cond_3
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/cg;Ljava/lang/String;Ljava/util/ArrayList;I)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 2317
    invoke-static {p0, p2}, Lcom/google/android/gms/games/provider/y;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2318
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2320
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/cg;->getBadges()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/cg;->getBadges()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 2349
    :cond_0
    return-void

    .line 2326
    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/games/provider/y;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v3

    .line 2327
    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/cg;->getBadges()Ljava/util/ArrayList;

    move-result-object v4

    .line 2328
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_0

    .line 2329
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/cf;

    .line 2330
    new-instance v6, Landroid/content/ContentValues;

    iget-object v7, v0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-direct {v6, v7}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 2334
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/cf;->getImages()Ljava/util/ArrayList;

    move-result-object v7

    .line 2335
    if-eqz v7, :cond_2

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    const/4 v9, 0x1

    if-eq v8, v9, :cond_3

    .line 2336
    :cond_2
    const-string v6, "GameAgent"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Badge "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " does not have exactly one image"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2328
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2341
    :cond_3
    const-string v8, "badge_icon_image_url"

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/bp;

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bp;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v8, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2344
    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v6, "badge_game_id"

    invoke-virtual {v0, v6, p4}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Ljava/lang/String;Lcom/google/android/gms/games/b/u;Ljava/lang/Object;J)V
    .locals 10

    .prologue
    .line 2745
    const-string v0, "installed"

    invoke-virtual {p5, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2746
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v3

    .line 2748
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v1, p4

    move-object v2, p5

    move-object v6, p3

    move-wide/from16 v8, p6

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJ)V

    .line 2750
    return-void
.end method

.method private a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/games/a/ad;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2904
    .line 2907
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/games/a/ab;->L:Lcom/google/android/gms/games/h/a/ag;

    iget-object v3, p3, Lcom/google/android/gms/games/a/ad;->d:Lcom/google/android/gms/games/h/a/fe;

    const-string v4, "applications/%1$s/sessions"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v2, v2, Lcom/google/android/gms/games/h/a/ag;->a:Lcom/google/android/gms/common/server/n;

    const/4 v5, 0x1

    invoke-virtual {v2, p1, v5, v4, v3}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 2923
    :goto_0
    return v0

    .line 2909
    :catch_0
    move-exception v2

    .line 2910
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2911
    const-string v3, "GameAgent"

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 2913
    :cond_0
    invoke-static {v2}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 2917
    goto :goto_0

    .line 2919
    :cond_1
    const-string v1, "GameAgent"

    const-string v2, "Encountered hard error while submitting pending operations."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b(Lcom/google/android/gms/games/a/au;)J
    .locals 15

    .prologue
    const-wide/16 v2, -0x1

    const/4 v14, 0x0

    const/4 v1, 0x1

    .line 950
    iget-object v6, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 951
    iget-object v7, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 954
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v7, v0}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 956
    const-string v0, "last_connection_local_time"

    invoke-static {v6, v8, v0}, Lcom/google/android/gms/games/a/l;->e(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v4

    .line 958
    cmp-long v0, v4, v2

    if-nez v0, :cond_0

    .line 959
    const-string v0, "GameAgent"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Attempting to update connection time for game "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " which has no local record!"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v0, v9}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 963
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/a/au;->j()J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 970
    :cond_0
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    .line 971
    sub-long v10, v2, v4

    const-wide/32 v12, 0x2932e00

    cmp-long v0, v10, v12

    if-lez v0, :cond_3

    move v0, v1

    .line 974
    :goto_0
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 975
    const-string v10, "last_connection_local_time"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v9, v10, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 976
    if-eqz v0, :cond_1

    .line 977
    const-string v2, "metadata_sync_requested"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 979
    :cond_1
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v8, v9, v14, v14}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 982
    if-eqz v0, :cond_2

    .line 983
    invoke-static {v7}, Lcom/google/android/gms/games/service/ac;->a(Lcom/google/android/gms/common/server/ClientContext;)V

    :cond_2
    move-wide v0, v4

    .line 985
    :goto_1
    return-wide v0

    .line 965
    :catch_0
    move-exception v0

    move-wide v0, v2

    goto :goto_1

    .line 971
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 19

    .prologue
    .line 1253
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 1254
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 1255
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    .line 1256
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    .line 1257
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ab;->Q:Lcom/google/android/gms/games/b/k;

    invoke-virtual {v2, v10}, Lcom/google/android/gms/games/b/k;->a(Ljava/lang/Object;)V

    .line 1258
    new-instance v3, Lcom/google/android/gms/games/b/l;

    move-object/from16 v0, p2

    invoke-direct {v3, v9, v0}, Lcom/google/android/gms/games/b/l;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1259
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    .line 1262
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/gms/games/a/au;->g:Z

    if-eqz v2, :cond_0

    .line 1263
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ab;->Q:Lcom/google/android/gms/games/b/k;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/b/k;->c(Ljava/lang/Object;)V

    .line 1267
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ab;->Q:Lcom/google/android/gms/games/b/k;

    move/from16 v6, p3

    move/from16 v7, p4

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/games/b/k;->a(Ljava/lang/Object;JIZ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1268
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ab;->Q:Lcom/google/android/gms/games/b/k;

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    .line 1299
    :goto_0
    return-object v2

    .line 1272
    :cond_1
    const/4 v2, 0x0

    .line 1273
    if-nez p4, :cond_2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/games/a/ab;->Q:Lcom/google/android/gms/games/b/k;

    invoke-virtual {v6, v3, v4, v5}, Lcom/google/android/gms/games/b/k;->a(Ljava/lang/Object;J)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1277
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ab;->Q:Lcom/google/android/gms/games/b/k;

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gms/games/b/k;->b(Ljava/lang/Object;J)Ljava/lang/String;

    move-result-object v2

    move-object v6, v2

    .line 1281
    :goto_1
    :try_start_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/games/a/ab;->L:Lcom/google/android/gms/games/h/a/ag;

    const-string v2, "android:12"

    invoke-static {v8}, Lcom/google/android/gms/games/d/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    invoke-static {v8}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    const-string v15, "ANDROID"

    const-string v16, "players/%1$s/players/%2$s/applications/%3$s"

    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static {v10}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v17, v18

    const/4 v10, 0x1

    invoke-static {v9}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v17, v10

    const/4 v9, 0x2

    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v17, v9

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "clientRevision"

    invoke-static {v2}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v9, v10, v2}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v11, :cond_3

    const-string v9, "deviceType"

    invoke-static {v11}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v2, v9, v10}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_3
    if-eqz v13, :cond_4

    const-string v9, "filterPlayable"

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v2, v9, v10}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_4
    if-eqz v8, :cond_5

    const-string v9, "language"

    invoke-static {v8}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v9, v8}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_5
    if-eqz v14, :cond_6

    const-string v8, "maxResults"

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v8, v9}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_6
    if-eqz v6, :cond_7

    const-string v8, "pageToken"

    invoke-static {v6}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v8, v6}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_7
    const-string v6, "platformType"

    invoke-static {v15}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v6, v8}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iget-object v6, v7, Lcom/google/android/gms/games/h/a/ag;->a:Lcom/google/android/gms/common/server/n;

    const/4 v8, 0x0

    const/4 v10, 0x0

    const-class v11, Lcom/google/android/gms/games/h/a/ab;

    move-object v7, v12

    invoke-virtual/range {v6 .. v11}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/ab;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 1296
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/ab;->getItems()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/ab;->b()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/games/a/ab;->Q:Lcom/google/android/gms/games/b/k;

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-object v11, v3

    move-wide v12, v4

    invoke-direct/range {v6 .. v13}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Ljava/lang/String;Lcom/google/android/gms/games/b/u;Ljava/lang/Object;J)V

    .line 1299
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ab;->Q:Lcom/google/android/gms/games/b/k;

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto/16 :goto_0

    .line 1285
    :catch_0
    move-exception v2

    const-string v6, "GameAgent"

    invoke-static {v2, v6}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 1287
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ab;->Q:Lcom/google/android/gms/games/b/k;

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gms/games/b/k;->a(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1289
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ab;->Q:Lcom/google/android/gms/games/b/k;

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/games/b/k;->a(Ljava/lang/Object;I)V

    .line 1292
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ab;->Q:Lcom/google/android/gms/games/b/k;

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto/16 :goto_0

    :cond_9
    move-object v6, v2

    goto/16 :goto_1
.end method

.method protected static b()Lcom/google/android/gms/common/util/p;
    .locals 1

    .prologue
    .line 3164
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/google/android/gms/games/a/au;Z)Lcom/google/android/gms/games/h/a/bg;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v6, 0x0

    .line 2479
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 2480
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/games/d/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 2481
    const-string v7, "ANDROID"

    .line 2482
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->a(Ljava/lang/String;)Lcom/google/android/gms/games/h/a/y;

    move-result-object v8

    .line 2485
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->L:Lcom/google/android/gms/games/h/a/ag;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    const-string v2, "android:12"

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/games/h/a/ag;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/h/a/y;)Lcom/google/android/gms/games/h/a/z;

    move-result-object v0

    .line 2488
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/z;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    .line 2489
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 2493
    :cond_0
    new-instance v0, Lcom/android/volley/ac;

    new-instance v1, Lcom/android/volley/m;

    const/16 v2, 0x194

    invoke-direct {v1, v2, v6, v6, v9}, Lcom/android/volley/m;-><init>(I[BLjava/util/Map;Z)V

    invoke-direct {v0, v1}, Lcom/android/volley/ac;-><init>(Lcom/android/volley/m;)V

    throw v0

    .line 2496
    :cond_1
    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/bg;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/a/ab;)Ljava/util/Random;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->S:Ljava/util/Random;

    return-object v0
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1411
    invoke-static {p1}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    .line 1412
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1415
    const-string v2, "metadata_version"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1416
    const-string v2, "sync_token"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1417
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1418
    invoke-static {p1}, Lcom/google/android/gms/games/service/ac;->a(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 1419
    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 1619
    .line 1620
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1621
    invoke-static {p1, p2}, Lcom/google/android/gms/games/provider/z;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1622
    sget-object v2, Lcom/google/android/gms/games/a/ai;->a:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    move v2, v7

    .line 1624
    :goto_0
    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1625
    const/4 v1, 0x2

    invoke-interface {v4, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_0

    move v1, v6

    .line 1626
    :goto_1
    or-int/2addr v1, v2

    move v2, v1

    .line 1627
    goto :goto_0

    :cond_0
    move v1, v7

    .line 1625
    goto :goto_1

    .line 1629
    :cond_1
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 1633
    if-nez v2, :cond_2

    .line 1634
    invoke-static {p1, p2}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1635
    invoke-virtual {v0, v1, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1637
    :cond_2
    if-nez v2, :cond_3

    :goto_2
    return v6

    .line 1629
    :catchall_0
    move-exception v0

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    move v6, v7

    .line 1637
    goto :goto_2
.end method

.method public static c(Lcom/google/android/gms/games/a/au;)I
    .locals 4

    .prologue
    .line 1038
    const/4 v1, 0x0

    .line 1039
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    .line 1040
    new-instance v2, Lcom/google/android/gms/games/a/o;

    invoke-direct {v2, p0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    .line 1042
    :try_start_0
    new-instance v0, Lcom/google/android/gms/games/a;

    invoke-direct {v0, v2}, Lcom/google/android/gms/games/a;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 1043
    invoke-virtual {v0}, Lcom/google/android/gms/games/a;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Game;

    .line 1044
    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->r()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 1045
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 1047
    goto :goto_0

    .line 1049
    :cond_0
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 1051
    return v1

    .line 1049
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private static c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 2076
    .line 2077
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/gms/games/provider/z;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/a/ab;->F:[Ljava/lang/String;

    const-string v3, "package_name=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    aput-object p2, v4, v6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2081
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2082
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 2085
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2087
    return-object v5

    .line 2085
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2772
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->P:Lcom/google/android/gms/games/b/j;

    const-string v1, "played"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/b/j;->c(Ljava/lang/Object;)V

    .line 2773
    return-void
.end method

.method public static d(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    .prologue
    .line 1086
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/provider/z;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1088
    new-instance v1, Lcom/google/android/gms/games/a/o;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method private d(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 20

    .prologue
    .line 1812
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    move-object/from16 v17, v0

    .line 1813
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    move-object/from16 v18, v0

    .line 1814
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    move-object/from16 v19, v0

    .line 1817
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/gms/games/a/au;->g:Z

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ab;->P:Lcom/google/android/gms/games/b/j;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/b/j;->b(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1819
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/games/a/ab;->c()V

    .line 1823
    :cond_0
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    .line 1824
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ab;->P:Lcom/google/android/gms/games/b/j;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/b/j;->b(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ab;->P:Lcom/google/android/gms/games/b/j;

    const-string v3, "played"

    move/from16 v6, p2

    move/from16 v7, p3

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/games/b/j;->a(Ljava/lang/Object;JIZ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1827
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ab;->P:Lcom/google/android/gms/games/b/j;

    const-string v3, "played"

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    .line 1892
    :goto_0
    return-object v2

    .line 1831
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ab;->P:Lcom/google/android/gms/games/b/j;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/b/j;->a(Ljava/lang/Object;)V

    .line 1834
    const/4 v15, 0x0

    .line 1835
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ab;->P:Lcom/google/android/gms/games/b/j;

    const-string v3, "played"

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gms/games/b/j;->a(Ljava/lang/Object;J)Z

    move-result v3

    .line 1837
    if-eqz v3, :cond_2

    .line 1840
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ab;->P:Lcom/google/android/gms/games/b/j;

    const-string v6, "played"

    invoke-virtual {v2, v6, v4, v5}, Lcom/google/android/gms/games/b/j;->b(Ljava/lang/Object;J)Ljava/lang/String;

    move-result-object v15

    .line 1845
    :cond_2
    :try_start_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/games/a/ab;->L:Lcom/google/android/gms/games/h/a/ag;

    const-string v8, "me"

    const-string v9, "played"

    const-string v10, "android:12"

    invoke-static/range {v18 .. v18}, Lcom/google/android/gms/games/d/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    invoke-static/range {v18 .. v18}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v13

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    const-string v16, "ANDROID"

    move-object/from16 v7, v19

    invoke-virtual/range {v6 .. v16}, Lcom/google/android/gms/games/h/a/ag;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/h/a/ab;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    .line 1874
    invoke-virtual {v13}, Lcom/google/android/gms/games/h/a/ab;->getItems()Ljava/util/ArrayList;

    move-result-object v2

    .line 1875
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_6

    :cond_3
    if-nez v3, :cond_6

    .line 1877
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto :goto_0

    .line 1850
    :catch_0
    move-exception v2

    .line 1851
    const-string v4, "GameAgent"

    const-string v5, "Unable to retrieve list of recently played games"

    invoke-static {v4, v5}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1852
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1853
    const-string v4, "GameAgent"

    invoke-static {v2, v4}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 1857
    :cond_4
    if-eqz v3, :cond_5

    .line 1858
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ab;->P:Lcom/google/android/gms/games/b/j;

    const-string v3, "played"

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/games/b/j;->a(Ljava/lang/Object;I)V

    .line 1861
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ab;->P:Lcom/google/android/gms/games/b/j;

    const-string v3, "played"

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto/16 :goto_0

    .line 1864
    :cond_5
    new-instance v2, Lcom/google/android/gms/games/a/o;

    move-object/from16 v0, v18

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    invoke-static/range {v19 .. v19}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "last_played_server_time > 0"

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/games/a/o;

    move-result-object v2

    const-string v3, "last_played_server_time DESC"

    iput-object v3, v2, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    const/16 v3, 0x1f4

    iput v3, v2, Lcom/google/android/gms/games/a/o;->d:I

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto/16 :goto_0

    .line 1882
    :cond_6
    new-instance v6, Lcom/google/android/gms/games/a/aj;

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v7, p0

    move-object/from16 v8, v18

    move-object/from16 v9, v19

    move-object/from16 v10, v17

    invoke-direct/range {v6 .. v12}, Lcom/google/android/gms/games/a/aj;-><init>(Lcom/google/android/gms/games/a/ab;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZB)V

    invoke-virtual {v6, v2}, Lcom/google/android/gms/games/a/aj;->a(Ljava/util/ArrayList;)V

    .line 1886
    if-eqz v13, :cond_7

    invoke-virtual {v13}, Lcom/google/android/gms/games/h/a/ab;->getItems()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v13}, Lcom/google/android/gms/games/h/a/ab;->getItems()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_8

    .line 1889
    :cond_7
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ab;->P:Lcom/google/android/gms/games/b/j;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/b/j;->a(Ljava/lang/Object;)V

    .line 1890
    invoke-virtual {v13}, Lcom/google/android/gms/games/h/a/ab;->getItems()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v13}, Lcom/google/android/gms/games/h/a/ab;->b()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/games/a/ab;->P:Lcom/google/android/gms/games/b/j;

    const-string v11, "played"

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-wide v12, v4

    invoke-direct/range {v6 .. v13}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Ljava/lang/String;Lcom/google/android/gms/games/b/u;Ljava/lang/Object;J)V

    .line 1892
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ab;->P:Lcom/google/android/gms/games/b/j;

    const-string v3, "played"

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto/16 :goto_0

    .line 1886
    :cond_8
    invoke-virtual {v13}, Lcom/google/android/gms/games/h/a/ab;->getItems()Ljava/util/ArrayList;

    move-result-object v14

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v15, Ljava/util/HashMap;

    invoke-direct {v15}, Ljava/util/HashMap;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v3, v2

    :goto_2
    if-ge v3, v7, :cond_a

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/bg;

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bg;->c()Ljava/lang/Integer;

    move-result-object v8

    if-nez v8, :cond_9

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bg;->getGamesData()Lcom/google/android/gms/games/h/a/u;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/u;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v15, v2, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    :cond_a
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eqz v2, :cond_7

    invoke-static {v6}, Lcom/google/android/gms/games/a/l;->a(Ljava/util/ArrayList;)Lcom/google/android/gms/games/h/a/y;

    move-result-object v12

    const/4 v11, 0x0

    const/4 v2, 0x1

    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    :goto_3
    if-nez v11, :cond_b

    if-eqz v2, :cond_c

    :cond_b
    :try_start_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/games/a/ab;->M:Lcom/google/android/gms/games/h/a/n;

    const-string v8, "me"

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/google/android/gms/games/internal/b/a;->a(I)Ljava/lang/String;

    move-result-object v9

    invoke-static/range {v18 .. v18}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v7, v19

    invoke-virtual/range {v6 .. v12}, Lcom/google/android/gms/games/h/a/n;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/h/a/y;)Lcom/google/android/gms/games/h/a/cu;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/cu;->getItems()Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const/4 v2, 0x0

    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/cu;->b()Ljava/lang/String;
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v11

    goto :goto_3

    :catch_1
    move-exception v2

    const-string v3, "GameAgent"

    const-string v6, "Unable to retrieve list of achievements"

    invoke-static {v3, v6}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v3

    if-eqz v3, :cond_7

    const-string v3, "GameAgent"

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    goto/16 :goto_1

    :cond_c
    const/4 v2, 0x0

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v3, v2

    :goto_4
    if-ge v3, v6, :cond_e

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/ct;

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/ct;->b()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_d

    invoke-virtual {v15, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v15, v7, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_d
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_4

    :cond_e
    const/4 v2, 0x0

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v6, v2

    :goto_5
    if-ge v6, v7, :cond_7

    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/bg;

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bg;->getGamesData()Lcom/google/android/gms/games/h/a/u;

    move-result-object v3

    if-eqz v3, :cond_f

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bg;->getGamesData()Lcom/google/android/gms/games/h/a/u;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/u;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v2, v2, Lcom/google/android/gms/common/server/response/c;->a:Ljava/util/HashMap;

    const-string v8, "unlockedAchievementCount"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_f
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_5
.end method

.method public static g(Lcom/google/android/gms/games/a/au;)I
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x1

    .line 1782
    const/4 v1, 0x0

    .line 1783
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1786
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1787
    const-string v4, "identity_sharing_confirmed"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1788
    iget-object v4, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, v2, v3, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 1790
    if-eq v2, v0, :cond_0

    .line 1793
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/a/au;Z)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1691
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    .line 1692
    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 1693
    iget-object v4, p0, Lcom/google/android/gms/games/a/ab;->h:Lcom/google/android/gms/games/a/bb;

    invoke-virtual {v4}, Lcom/google/android/gms/games/a/bb;->d()V

    .line 1696
    :try_start_0
    invoke-static {v2}, Lcom/google/android/gms/games/a/l;->a(Ljava/lang/String;)Lcom/google/android/gms/games/h/a/y;

    move-result-object v4

    .line 1697
    if-eqz p2, :cond_0

    .line 1698
    iget-object v5, p0, Lcom/google/android/gms/games/a/ab;->L:Lcom/google/android/gms/games/h/a/ag;

    const-string v6, "players/me/applications/mute"

    iget-object v5, v5, Lcom/google/android/gms/games/h/a/ag;->a:Lcom/google/android/gms/common/server/n;

    const/4 v7, 0x1

    invoke-virtual {v5, v3, v7, v6, v4}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    .line 1704
    :goto_0
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 1705
    const-string v5, "muted"

    if-eqz p2, :cond_2

    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1706
    invoke-static {v3, v2}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1707
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v1, v4, v3, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1709
    iget-object v1, p0, Lcom/google/android/gms/games/a/ab;->O:Lcom/google/android/gms/games/b/j;

    const-string v2, "hidden"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/b/j;->c(Ljava/lang/Object;)V

    .line 1716
    :goto_2
    return v0

    .line 1700
    :cond_0
    iget-object v5, p0, Lcom/google/android/gms/games/a/ab;->L:Lcom/google/android/gms/games/h/a/ag;

    const-string v6, "players/me/applications/unmute"

    iget-object v5, v5, Lcom/google/android/gms/games/h/a/ag;->a:Lcom/google/android/gms/common/server/n;

    const/4 v7, 0x1

    invoke-virtual {v5, v3, v7, v6, v4}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1710
    :catch_0
    move-exception v0

    .line 1711
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1712
    const-string v1, "GameAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 1714
    :cond_1
    const/4 v0, 0x6

    goto :goto_2

    :cond_2
    move v1, v0

    .line 1705
    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x3

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 1342
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->j:Lcom/google/android/gms/games/a/bb;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bb;->d()V

    .line 1346
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v1, :cond_0

    .line 1347
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 1381
    :goto_0
    return-object v0

    .line 1352
    :cond_0
    :try_start_0
    const-string v0, "UTF-8"

    invoke-static {p3, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object p3

    .line 1356
    :goto_1
    invoke-static {p1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v0

    .line 1360
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/a/ab;->N:Lcom/google/android/gms/common/server/n;

    const-string v3, "/SuggRequest?json=1&query=%1$s&hl=%2$s&gl=%3$s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p3, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v3, Lcom/google/android/gms/games/h/d/a;

    invoke-virtual {v1, p2, v0, v3}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/d/a;
    :try_end_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_1

    .line 1373
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/d/a;->b()Ljava/util/ArrayList;

    move-result-object v3

    .line 1374
    new-instance v4, Lcom/google/android/gms/common/data/ai;

    sget-object v0, Lcom/google/android/gms/games/provider/aa;->a:[Ljava/lang/String;

    const-string v1, "suggestion"

    invoke-direct {v4, v0, v1, v8, v8}, Lcom/google/android/gms/common/data/ai;-><init>([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1376
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v2

    :goto_2
    if-ge v1, v5, :cond_1

    .line 1377
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 1378
    const-string v7, "suggestion"

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/d/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/d/b;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1379
    invoke-virtual {v4, v6}, Lcom/google/android/gms/common/data/ai;->a(Landroid/content/ContentValues;)V

    .line 1376
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1367
    :catch_0
    move-exception v0

    invoke-static {v7}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 1369
    :catch_1
    move-exception v0

    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 1381
    :cond_1
    invoke-virtual {v4, v2}, Lcom/google/android/gms/common/data/ai;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 552
    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/a/ab;->b(Lcom/google/android/gms/games/a/au;Z)Lcom/google/android/gms/games/h/a/bg;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 575
    new-instance v3, Lcom/google/android/gms/common/data/ai;

    sget-object v1, Lcom/google/android/gms/games/a/ab;->J:Lcom/google/android/gms/games/provider/a;

    iget-object v1, v1, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    invoke-direct {v3, v1}, Lcom/google/android/gms/common/data/ai;-><init>([Ljava/lang/String;)V

    .line 577
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 578
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 579
    invoke-direct {p0, p1, v1, v2}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v4

    .line 581
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_2

    .line 582
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/common/data/ai;->a(Landroid/content/ContentValues;)V

    .line 581
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 553
    :catch_0
    move-exception v0

    .line 554
    const-string v1, "GameAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to retrieve 1P application "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " from network"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 557
    const-string v1, "GameAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 563
    :cond_0
    const/16 v1, 0x194

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 564
    const-string v0, "GameAgent"

    const-string v1, "Game ID (%s) was not found on server"

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    aput-object v4, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    const/16 v0, 0x9

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 584
    :goto_1
    return-object v0

    .line 571
    :cond_1
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_1

    .line 584
    :cond_2
    invoke-virtual {v3, v2}, Lcom/google/android/gms/common/data/ai;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/a/au;IIZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1140
    invoke-virtual {p0, p3}, Lcom/google/android/gms/games/a/ab;->a(I)Lcom/google/android/gms/games/a/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bb;->d()V

    .line 1145
    const/4 v9, 0x0

    .line 1146
    iget-object v7, p0, Lcom/google/android/gms/games/a/ab;->O:Lcom/google/android/gms/games/b/j;

    .line 1147
    packed-switch p3, :pswitch_data_0

    .line 1190
    const-string v0, "GameAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected collection type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1191
    invoke-static {v5}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 1204
    :goto_0
    return-object v0

    .line 1149
    :pswitch_0
    const-string v3, "featured"

    move-object v2, v7

    move v4, v6

    move v6, v5

    .line 1198
    :goto_1
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    if-eqz v4, :cond_0

    .line 1199
    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->c()Lcom/google/android/gms/games/a/av;

    move-result-object v0

    const-string v1, "me"

    iput-object v1, v0, Lcom/google/android/gms/games/a/av;->f:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object p1

    move-object v1, p1

    :goto_2
    move-object v0, p0

    move-object v5, v3

    move v7, p2

    move v8, p4

    .line 1204
    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/b/j;Ljava/lang/String;ZLjava/lang/String;ZIZLcom/google/android/gms/games/a/af;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 1153
    :pswitch_1
    const-string v3, "multiplayer"

    move-object v2, v7

    move v4, v6

    move v6, v5

    .line 1155
    goto :goto_1

    .line 1157
    :pswitch_2
    const-string v3, "all"

    move-object v2, v7

    move v4, v6

    move v6, v5

    .line 1159
    goto :goto_1

    .line 1163
    :pswitch_3
    const-string v3, "played"

    .line 1164
    iget-object v2, p0, Lcom/google/android/gms/games/a/ab;->P:Lcom/google/android/gms/games/b/j;

    move v4, v5

    .line 1167
    goto :goto_1

    .line 1169
    :pswitch_4
    const-string v3, "recommended"

    move-object v2, v7

    move v6, v5

    move v4, v5

    .line 1171
    goto :goto_1

    .line 1173
    :pswitch_5
    const-string v3, "downloaded"

    move-object v2, v7

    move v6, v5

    move v4, v5

    .line 1175
    goto :goto_1

    .line 1177
    :pswitch_6
    const-string v3, "installed"

    move-object v2, v7

    move v6, v5

    move v4, v5

    .line 1179
    goto :goto_1

    .line 1181
    :pswitch_7
    const-string v8, "hidden"

    .line 1184
    new-instance v0, Lcom/google/android/gms/games/a/aj;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->f()Ljava/lang/String;

    move-result-object v4

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/a/aj;-><init>(Lcom/google/android/gms/games/a/ab;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZB)V

    move-object v2, v7

    move-object v9, v0

    move v4, v5

    move-object v3, v8

    .line 1187
    goto :goto_1

    :cond_0
    move-object v1, p1

    goto :goto_2

    .line 1147
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    .line 1224
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->l:Lcom/google/android/gms/games/a/bb;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bb;->d()V

    .line 1225
    const-string v0, "common"

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/gms/games/a/ab;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 13

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 494
    .line 498
    iget-object v10, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 499
    iget-object v11, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    .line 501
    :try_start_0
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->h:Z

    if-eqz v0, :cond_2

    .line 502
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/a/ab;->b(Lcom/google/android/gms/games/a/au;Z)Lcom/google/android/gms/games/h/a/bg;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 504
    :try_start_1
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bg;->getGamesData()Lcom/google/android/gms/games/h/a/u;
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 505
    :try_start_2
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bg;->getMarketData()Lcom/google/android/gms/games/h/a/ce;
    :try_end_2
    .catch Lcom/android/volley/ac; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v8

    move-object v5, v8

    move-object v4, v1

    move-object v3, v2

    move v9, v6

    .line 518
    :goto_0
    if-eqz v4, :cond_0

    .line 519
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 520
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    move-object v0, p0

    move-object v2, v10

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/games/a/ab;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/bg;Lcom/google/android/gms/games/h/a/u;Lcom/google/android/gms/games/h/a/ce;ZZLjava/util/ArrayList;)J

    .line 523
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "GameAgent"

    invoke-static {v0, v8, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 527
    :cond_0
    if-nez p2, :cond_5

    :cond_1
    :goto_1
    if-nez v7, :cond_6

    .line 528
    const-string v0, "GameAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Application ID "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not associated with package "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Check the application ID in your manifest."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    const/16 v0, 0x8

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 535
    :goto_2
    return-object v0

    .line 507
    :cond_2
    :try_start_3
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ANDROID"

    iget-object v4, p0, Lcom/google/android/gms/games/a/ab;->K:Lcom/google/android/gms/games/h/a/af;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    const-string v5, "applications/%1$s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v0}, Lcom/google/android/gms/games/h/a/af;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v9, v12

    invoke-static {v5, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v2, :cond_3

    const-string v5, "language"

    invoke-static {v2}, Lcom/google/android/gms/games/h/a/af;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v5, v2}, Lcom/google/android/gms/games/h/a/af;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_3
    const-string v2, "platformType"

    invoke-static {v3}, Lcom/google/android/gms/games/h/a/af;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/games/h/a/af;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v4, Lcom/google/android/gms/games/h/a/af;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/u;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/u;
    :try_end_3
    .catch Lcom/android/volley/ac; {:try_start_3 .. :try_end_3} :catch_0

    move-object v5, v8

    move-object v4, v0

    move-object v3, v8

    move v9, v6

    .line 515
    goto/16 :goto_0

    .line 509
    :catch_0
    move-exception v0

    move-object v1, v8

    move-object v2, v8

    .line 510
    :goto_3
    const-string v3, "GameAgent"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unable to retrieve application "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " from network"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 512
    const-string v3, "GameAgent"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 514
    :cond_4
    const/4 v0, 0x3

    move-object v5, v8

    move-object v4, v1

    move-object v3, v2

    move v9, v0

    goto/16 :goto_0

    .line 527
    :cond_5
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/provider/z;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const-string v2, "package_name=?"

    new-array v3, v7, [Ljava/lang/String;

    aput-object p2, v3, v6

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    move v7, v6

    goto/16 :goto_1

    .line 534
    :cond_6
    invoke-static {v10, v11}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 535
    new-instance v1, Lcom/google/android/gms/games/a/o;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    iput v9, v0, Lcom/google/android/gms/games/a/o;->d:I

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto/16 :goto_2

    .line 509
    :catch_1
    move-exception v0

    move-object v1, v8

    goto :goto_3

    :catch_2
    move-exception v0

    goto :goto_3
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 12

    .prologue
    .line 771
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->i:Lcom/google/android/gms/games/a/bb;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bb;->d()V

    .line 772
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->R:Lcom/google/android/gms/games/b/n;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/b/n;->a(Ljava/lang/Object;)V

    .line 773
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    .line 776
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->g:Z

    if-eqz v0, :cond_0

    .line 777
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->R:Lcom/google/android/gms/games/b/n;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/b/n;->c(Ljava/lang/Object;)V

    .line 781
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->R:Lcom/google/android/gms/games/b/n;

    move-object v1, p2

    move v4, p3

    move/from16 v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/b/n;->a(Ljava/lang/Object;JIZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 782
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->R:Lcom/google/android/gms/games/b/n;

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {v0, p2, v1, v2}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 814
    :goto_0
    return-object v0

    .line 787
    :cond_1
    const/4 v0, 0x0

    .line 788
    if-nez p4, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/games/a/ab;->R:Lcom/google/android/gms/games/b/n;

    invoke-virtual {v1, p2, v2, v3}, Lcom/google/android/gms/games/b/n;->a(Ljava/lang/Object;J)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 792
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->R:Lcom/google/android/gms/games/b/n;

    invoke-virtual {v0, p2, v2, v3}, Lcom/google/android/gms/games/b/n;->b(Ljava/lang/Object;J)Ljava/lang/String;

    move-result-object v0

    .line 795
    :cond_3
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 798
    :try_start_0
    iget-object v4, p0, Lcom/google/android/gms/games/a/ab;->L:Lcom/google/android/gms/games/h/a/ag;

    iget-object v5, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    const-string v6, "android:12"

    invoke-static {v1}, Lcom/google/android/gms/games/d/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-static {v1}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const-string v7, "applications/search"

    const-string v11, "clientRevision"

    invoke-static {v6}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v11, v6}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v8, :cond_4

    const-string v6, "deviceType"

    invoke-static {v8}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v6, v8}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    :cond_4
    if-eqz v9, :cond_5

    const-string v6, "filterPlayable"

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v6, v8}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    :cond_5
    if-eqz v1, :cond_6

    const-string v6, "language"

    invoke-static {v1}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v6, v1}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    :cond_6
    if-eqz v10, :cond_7

    const-string v1, "maxResults"

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v1, v6}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    :cond_7
    if-eqz v0, :cond_8

    const-string v1, "pageToken"

    invoke-static {v0}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v1, v0}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    :cond_8
    if-eqz p2, :cond_9

    const-string v0, "q"

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v0, v1}, Lcom/google/android/gms/games/h/a/ag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    :cond_9
    iget-object v4, v4, Lcom/google/android/gms/games/h/a/ag;->a:Lcom/google/android/gms/common/server/n;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const-class v9, Lcom/google/android/gms/games/h/a/ac;

    invoke-virtual/range {v4 .. v9}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/ac;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 812
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ac;->getItems()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ac;->b()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/gms/games/a/ab;->R:Lcom/google/android/gms/games/b/n;

    move-object v4, p0

    move-object v5, p1

    move-object v9, p2

    move-wide v10, v2

    invoke-direct/range {v4 .. v11}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Ljava/lang/String;Lcom/google/android/gms/games/b/u;Ljava/lang/Object;J)V

    .line 814
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->R:Lcom/google/android/gms/games/b/n;

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {v0, p2, v1, v2}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto/16 :goto_0

    .line 802
    :catch_0
    move-exception v0

    const-string v1, "GameAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 804
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->R:Lcom/google/android/gms/games/b/n;

    invoke-virtual {v0, p2, v2, v3}, Lcom/google/android/gms/games/b/n;->a(Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 806
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->R:Lcom/google/android/gms/games/b/n;

    const/4 v1, 0x3

    invoke-virtual {v0, p2, v1}, Lcom/google/android/gms/games/b/n;->a(Ljava/lang/Object;I)V

    .line 808
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->R:Lcom/google/android/gms/games/b/n;

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {v0, p2, v1, v2}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 10

    .prologue
    .line 1325
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->k:Lcom/google/android/gms/games/a/bb;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bb;->d()V

    .line 1326
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "custom_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1327
    iget-object v2, p0, Lcom/google/android/gms/games/a/ab;->O:Lcom/google/android/gms/games/b/j;

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move v4, p4

    move-object v5, p2

    move/from16 v6, p6

    move v7, p3

    move v8, p5

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/b/j;Ljava/lang/String;ZLjava/lang/String;ZIZLcom/google/android/gms/games/a/af;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/google/android/gms/games/a/bb;
    .locals 1

    .prologue
    .line 1100
    packed-switch p1, :pswitch_data_0

    .line 1119
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->k:Lcom/google/android/gms/games/a/bb;

    :goto_0
    return-object v0

    .line 1102
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->a:Lcom/google/android/gms/games/a/bb;

    goto :goto_0

    .line 1104
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->b:Lcom/google/android/gms/games/a/bb;

    goto :goto_0

    .line 1106
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->c:Lcom/google/android/gms/games/a/bb;

    goto :goto_0

    .line 1108
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->d:Lcom/google/android/gms/games/a/bb;

    goto :goto_0

    .line 1110
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->e:Lcom/google/android/gms/games/a/bb;

    goto :goto_0

    .line 1112
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->f:Lcom/google/android/gms/games/a/bb;

    goto :goto_0

    .line 1114
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->g:Lcom/google/android/gms/games/a/bb;

    goto :goto_0

    .line 1116
    :pswitch_7
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->h:Lcom/google/android/gms/games/a/bb;

    goto :goto_0

    .line 1100
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)Ljava/util/ArrayList;
    .locals 27

    .prologue
    .line 623
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    move-object/from16 v20, v0

    .line 624
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    move-object/from16 v21, v0

    .line 628
    new-instance v22, Ljava/util/HashMap;

    invoke-direct/range {v22 .. v22}, Ljava/util/HashMap;-><init>()V

    .line 629
    new-instance v23, Ljava/util/HashMap;

    invoke-direct/range {v23 .. v23}, Ljava/util/HashMap;-><init>()V

    .line 630
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 631
    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 632
    invoke-static/range {v21 .. v21}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v7

    .line 633
    sget-object v8, Lcom/google/android/gms/games/a/al;->a:[Ljava/lang/String;

    const-string v9, "metadata_sync_requested=1"

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 636
    :goto_0
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 637
    const/4 v8, 0x1

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 638
    const/4 v9, 0x2

    invoke-interface {v7, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 639
    const/4 v9, 0x3

    invoke-interface {v7, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 640
    invoke-virtual {v12, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 641
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 642
    move-object/from16 v0, v23

    invoke-virtual {v0, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 645
    :catchall_0
    move-exception v6

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v6

    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 649
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 650
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_1

    move-object/from16 v6, v18

    .line 712
    :goto_1
    return-object v6

    .line 657
    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, p2

    move-object/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/games/a/ab;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;Ljava/util/HashMap;)Lcom/google/android/gms/games/a/ac;

    move-result-object v24

    .line 658
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 659
    const/4 v7, 0x0

    move-object/from16 v0, v24

    iget-object v8, v0, Lcom/google/android/gms/games/a/ac;->c:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v25

    move/from16 v19, v7

    :goto_2
    move/from16 v0, v19

    move/from16 v1, v25

    if-ge v0, v1, :cond_8

    .line 660
    move-object/from16 v0, v24

    iget-object v7, v0, Lcom/google/android/gms/games/a/ac;->c:Ljava/util/ArrayList;

    move/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    move-object/from16 v16, v7

    check-cast v16, Lcom/google/android/gms/games/h/a/bh;

    .line 661
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/games/h/a/bh;->b()Ljava/lang/String;

    move-result-object v26

    .line 662
    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    move-object/from16 v17, v7

    check-cast v17, Ljava/lang/Long;

    .line 667
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/games/h/a/bh;->d()Ljava/lang/Long;

    move-result-object v7

    .line 668
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/games/h/a/bh;->getApplication()Lcom/google/android/gms/games/h/a/bg;

    move-result-object v10

    .line 669
    if-eqz v10, :cond_2

    .line 670
    invoke-virtual {v10}, Lcom/google/android/gms/games/h/a/bg;->getGamesData()Lcom/google/android/gms/games/h/a/u;

    move-result-object v11

    .line 671
    invoke-virtual {v10}, Lcom/google/android/gms/games/h/a/bg;->getMarketData()Lcom/google/android/gms/games/h/a/ce;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v7, p0

    move-object/from16 v8, v20

    move-object/from16 v9, v21

    invoke-direct/range {v7 .. v15}, Lcom/google/android/gms/games/a/ab;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/bg;Lcom/google/android/gms/games/h/a/u;Lcom/google/android/gms/games/h/a/ce;ZZLjava/util/ArrayList;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 677
    :cond_2
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/games/h/a/bh;->c()Ljava/util/ArrayList;

    move-result-object v8

    if-nez v8, :cond_6

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 683
    :goto_3
    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 684
    if-eqz v17, :cond_3

    if-eqz v7, :cond_7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v8, v8, v10

    if-lez v8, :cond_7

    .line 687
    :cond_3
    sget-object v8, Lcom/google/android/gms/games/d/a;->c:[Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/gms/common/util/h;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v8

    .line 695
    :cond_4
    :goto_4
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lez v9, :cond_5

    .line 696
    new-instance v9, Lcom/google/android/gms/games/a/ag;

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/games/h/a/bh;->e()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v26

    invoke-direct {v9, v0, v7, v10, v8}, Lcom/google/android/gms/games/a/ag;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/util/ArrayList;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 659
    :cond_5
    add-int/lit8 v7, v19, 0x1

    move/from16 v19, v7

    goto/16 :goto_2

    .line 677
    :cond_6
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/games/h/a/bh;->c()Ljava/util/ArrayList;

    move-result-object v8

    goto :goto_3

    .line 690
    :cond_7
    sget-object v8, Lcom/google/android/gms/games/d/a;->b:[Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/gms/common/util/h;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v8

    goto :goto_4

    .line 702
    :cond_8
    const/4 v7, 0x0

    move-object/from16 v0, v24

    iget-object v8, v0, Lcom/google/android/gms/games/a/ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v8, v7

    :goto_5
    if-ge v8, v9, :cond_9

    .line 703
    move-object/from16 v0, v24

    iget-object v7, v0, Lcom/google/android/gms/games/a/ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/gms/games/h/a/bv;

    invoke-virtual {v7}, Lcom/google/android/gms/games/h/a/bv;->b()Ljava/lang/String;

    move-result-object v7

    .line 704
    move-object/from16 v0, v21

    invoke-static {v0, v7}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 705
    invoke-static {v7}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v7

    invoke-virtual {v15, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 702
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_5

    .line 709
    :cond_9
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_a

    .line 710
    const-string v7, "GameAgent"

    invoke-static {v6, v15, v7}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    :cond_a
    move-object/from16 v6, v18

    .line 712
    goto/16 :goto_1
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->O:Lcom/google/android/gms/games/b/j;

    iget-object v0, v0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0}, Landroid/support/v4/g/h;->b()V

    .line 478
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->P:Lcom/google/android/gms/games/b/j;

    iget-object v0, v0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0}, Landroid/support/v4/g/h;->b()V

    .line 479
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->R:Lcom/google/android/gms/games/b/n;

    iget-object v0, v0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0}, Landroid/support/v4/g/h;->b()V

    .line 480
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 2

    .prologue
    .line 998
    new-instance v0, Lcom/google/android/gms/games/a/ak;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/games/a/ak;-><init>(Lcom/google/android/gms/games/a/ab;Lcom/google/android/gms/common/server/ClientContext;)V

    .line 999
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 1002
    invoke-direct {p0}, Lcom/google/android/gms/games/a/ab;->c()V

    .line 1003
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Z
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1437
    move v0, v2

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/a/ab;->p:[Lcom/google/android/gms/games/a/bb;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1438
    iget-object v1, p0, Lcom/google/android/gms/games/a/ab;->p:[Lcom/google/android/gms/games/a/bb;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/bb;->d()V

    .line 1437
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1443
    :cond_0
    invoke-static {p1, p2, p3}, Lcom/google/android/gms/games/a/ab;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    move v2, v3

    .line 1517
    :cond_1
    :goto_1
    return v2

    .line 1449
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 1451
    const/16 v0, 0x80

    :try_start_0
    invoke-virtual {v1, p3, v0}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 1456
    if-eqz v4, :cond_1

    .line 1460
    iget-object v0, v4, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 1461
    if-eqz v0, :cond_3

    const-string v5, "com.google.android.gms.games.APP_ID"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 1463
    :cond_3
    if-eqz p4, :cond_1

    .line 1464
    const-string v0, "GameAgent"

    const-string v1, "Using Google Play games services requires a metadata tag with the name \"com.google.android.gms.games.APP_ID\" in the application tag of your manifest"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1452
    :catch_0
    move-exception v0

    .line 1453
    const-string v1, "GameAgent"

    const-string v3, "Caller attempted to insert game data for non-existent package."

    invoke-static {v1, v3, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1471
    :cond_4
    const-string v5, "com.google.android.gms.games.APP_ID"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1472
    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1473
    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1476
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1477
    const-string v0, "GameAgent"

    const-string v1, "Using Google Play games services requires a metadata tag with the name \"com.google.android.gms.games.APP_ID\" in the application tag of your manifest"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1482
    :cond_5
    if-nez v0, :cond_6

    .line 1483
    const-string v0, "GameAgent"

    const-string v1, "Your application doesn\'t have a name associated to it."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1486
    :cond_6
    if-nez v1, :cond_7

    .line 1487
    const-string v0, "GameAgent"

    const-string v1, "Your application doesn\'t have an icon associated to it."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1492
    :cond_7
    invoke-static {p1, p2, v6}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v4

    .line 1493
    cmp-long v7, v4, v8

    if-nez v7, :cond_8

    .line 1494
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/util/q;->a(Landroid/graphics/Bitmap;)[B

    move-result-object v1

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "external_game_id"

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "display_name"

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "primary_category"

    const-string v7, "unknown"

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "icon_image_bytes"

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v1, "metadata_version"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "metadata_sync_requested"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "play_enabled_game"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "availability"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {p2}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {v5, v1, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    .line 1497
    :cond_8
    cmp-long v1, v4, v8

    if-nez v1, :cond_9

    .line 1498
    const-string v0, "GameAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Failed to insert game record for "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1503
    :cond_9
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1504
    const-string v6, "instance_game_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1505
    const-string v4, "instance_display_name"

    invoke-virtual {v1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1506
    const-string v0, "package_name"

    invoke-virtual {v1, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1507
    const-string v0, "platform_type"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1508
    invoke-static {p2}, Lcom/google/android/gms/games/provider/z;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    .line 1509
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1513
    invoke-virtual {p0, p1, p2, p3, v3}, Lcom/google/android/gms/games/a/ab;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Ljava/lang/String;

    .line 1516
    invoke-static {p2}, Lcom/google/android/gms/games/service/ac;->a(Lcom/google/android/gms/common/server/ClientContext;)V

    move v2, v3

    .line 1517
    goto/16 :goto_1
.end method

.method public final b(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    .line 1246
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->m:Lcom/google/android/gms/games/a/bb;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bb;->d()V

    .line 1247
    const-string v0, "disjoint"

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/gms/games/a/ab;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 16

    .prologue
    .line 1535
    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/a/ab;->p:[Lcom/google/android/gms/games/a/bb;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 1536
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/a/ab;->p:[Lcom/google/android/gms/games/a/bb;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lcom/google/android/gms/games/a/bb;->d()V

    .line 1535
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1538
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ab;->O:Lcom/google/android/gms/games/b/j;

    iget-object v2, v2, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v2}, Landroid/support/v4/g/h;->b()V

    .line 1541
    invoke-static/range {p1 .. p3}, Lcom/google/android/gms/games/a/ab;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1542
    if-nez v8, :cond_1

    .line 1543
    const/4 v2, 0x0

    .line 1604
    :goto_1
    return-object v2

    .line 1547
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1548
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1549
    const-wide/16 v10, -0x1

    .line 1552
    move-object/from16 v0, p2

    invoke-static {v0, v8}, Lcom/google/android/gms/games/provider/z;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1553
    sget-object v4, Lcom/google/android/gms/games/a/ai;->a:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    move-wide v6, v10

    .line 1557
    :goto_2
    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1558
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1559
    const/4 v3, 0x1

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1562
    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1563
    move-object/from16 v0, p2

    invoke-static {v0, v4, v5}, Lcom/google/android/gms/games/provider/z;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v3

    .line 1564
    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v11, "installed"

    invoke-static/range {p4 .. p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v3, v11, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1572
    :cond_2
    const-wide/16 v14, 0x0

    cmp-long v3, v6, v14

    if-gez v3, :cond_9

    .line 1573
    const/4 v3, 0x2

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-lez v3, :cond_4

    const/4 v3, 0x1

    .line 1574
    :goto_3
    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    move/from16 v3, p4

    .line 1578
    :cond_3
    if-eqz v3, :cond_9

    :goto_4
    move-wide v6, v4

    .line 1582
    goto :goto_2

    .line 1573
    :cond_4
    const/4 v3, 0x0

    goto :goto_3

    .line 1585
    :cond_5
    const-wide/16 v4, 0x0

    cmp-long v3, v6, v4

    if-gez v3, :cond_6

    .line 1586
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1587
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v6

    .line 1590
    :cond_6
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 1594
    const-wide/16 v4, 0x0

    cmp-long v3, v6, v4

    if-lez v3, :cond_8

    const/4 v3, 0x1

    :goto_5
    invoke-static {v3}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 1595
    move-object/from16 v0, p2

    invoke-static {v0, v8}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1596
    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "target_instance"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1601
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_7

    .line 1602
    const-string v3, "GameAgent"

    invoke-static {v2, v9, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    :cond_7
    move-object v2, v8

    .line 1604
    goto/16 :goto_1

    .line 1590
    :catchall_0
    move-exception v2

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v2

    .line 1594
    :cond_8
    const/4 v3, 0x0

    goto :goto_5

    :cond_9
    move-wide v4, v6

    goto :goto_4
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)V
    .locals 17

    .prologue
    .line 725
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 726
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 729
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 730
    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 731
    invoke-static {v14}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v3

    .line 732
    sget-object v4, Lcom/google/android/gms/games/a/al;->a:[Ljava/lang/String;

    const-string v5, "metadata_version<0"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 735
    :goto_0
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 736
    const/4 v4, 0x1

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 737
    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 740
    :catchall_0
    move-exception v2

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_0
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 744
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 767
    :cond_1
    :goto_1
    return-void

    .line 749
    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v13, v14, v1, v8}, Lcom/google/android/gms/games/a/ab;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/content/SyncResult;Ljava/util/ArrayList;)Lcom/google/android/gms/games/a/ac;

    move-result-object v15

    .line 751
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 752
    const/4 v3, 0x0

    iget-object v4, v15, Lcom/google/android/gms/games/a/ac;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v16

    move v12, v3

    :goto_2
    move/from16 v0, v16

    if-ge v12, v0, :cond_3

    .line 753
    iget-object v3, v15, Lcom/google/android/gms/games/a/ac;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/games/h/a/bg;

    .line 754
    invoke-virtual {v6}, Lcom/google/android/gms/games/h/a/bg;->getGamesData()Lcom/google/android/gms/games/h/a/u;

    move-result-object v7

    .line 755
    invoke-virtual {v6}, Lcom/google/android/gms/games/h/a/bg;->getMarketData()Lcom/google/android/gms/games/h/a/ce;

    move-result-object v8

    const/4 v9, 0x1

    const/4 v10, 0x1

    move-object/from16 v3, p0

    move-object v4, v13

    move-object v5, v14

    invoke-direct/range {v3 .. v11}, Lcom/google/android/gms/games/a/ab;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/bg;Lcom/google/android/gms/games/h/a/u;Lcom/google/android/gms/games/h/a/ce;ZZLjava/util/ArrayList;)J

    .line 752
    add-int/lit8 v3, v12, 0x1

    move v12, v3

    goto :goto_2

    .line 759
    :cond_3
    const/4 v3, 0x0

    iget-object v4, v15, Lcom/google/android/gms/games/a/ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v3

    :goto_3
    if-ge v4, v5, :cond_4

    .line 760
    iget-object v3, v15, Lcom/google/android/gms/games/a/ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/games/h/a/bv;

    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/bv;->b()Ljava/lang/String;

    move-result-object v3

    .line 761
    invoke-static {v14, v3}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 762
    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 759
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_3

    .line 764
    :cond_4
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 765
    const-string v3, "GameAgent"

    invoke-static {v2, v11, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    goto :goto_1
.end method

.method public final c(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    .line 1667
    iget-object v0, p0, Lcom/google/android/gms/games/a/ab;->d:Lcom/google/android/gms/games/a/bb;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bb;->d()V

    .line 1670
    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1671
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/games/a/ab;->d(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 1676
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/a/au;IIZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method

.method public final e(Lcom/google/android/gms/games/a/au;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1390
    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 1391
    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 1394
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v5

    .line 1397
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    move v1, v2

    :goto_0
    if-ge v1, v6, :cond_0

    .line 1398
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    .line 1399
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p0, v3, v4, v0, v2}, Lcom/google/android/gms/games/a/ab;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Z

    .line 1397
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1401
    :cond_0
    return-void
.end method

.method public final f(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1729
    iget-object v1, p0, Lcom/google/android/gms/games/a/ab;->h:Lcom/google/android/gms/games/a/bb;

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/bb;->d()V

    .line 1736
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v6

    .line 1738
    iget-object v1, p0, Lcom/google/android/gms/games/a/ab;->O:Lcom/google/android/gms/games/b/j;

    const-string v2, "hidden"

    invoke-virtual {v1, v2, v6, v7}, Lcom/google/android/gms/games/b/j;->b(Ljava/lang/Object;J)Ljava/lang/String;

    move-result-object v1

    move v4, v0

    move v5, v3

    move v2, v0

    .line 1739
    :goto_0
    if-nez v2, :cond_1

    if-nez v5, :cond_0

    if-eqz v1, :cond_1

    .line 1741
    :cond_0
    const/4 v1, 0x0

    .line 1743
    const/16 v5, 0x19

    const/4 v8, 0x7

    const/4 v9, 0x1

    :try_start_0
    invoke-virtual {p0, p1, v5, v8, v9}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/a/au;IIZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    .line 1745
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v5

    if-eqz v5, :cond_2

    .line 1747
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->f()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1760
    if-eqz v1, :cond_1

    .line 1761
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 1767
    :cond_1
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1768
    const-string v3, "external_game_id"

    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1769
    const-string v3, "muted"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1770
    sget-object v2, Lcom/google/android/gms/games/a/ab;->H:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lcom/google/android/gms/common/data/m;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/data/m;->a(Ljava/util/HashMap;)Lcom/google/android/gms/common/data/m;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/data/m;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0

    .line 1750
    :cond_2
    :try_start_1
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v5

    :goto_1
    if-ge v4, v5, :cond_3

    .line 1751
    const-string v8, "external_game_id"

    invoke-virtual {v1, v4}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v9

    invoke-virtual {v1, v8, v4, v9}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v8

    .line 1753
    iget-object v9, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    move v2, v3

    .line 1758
    :cond_3
    iget-object v5, p0, Lcom/google/android/gms/games/a/ab;->O:Lcom/google/android/gms/games/b/j;

    const-string v8, "hidden"

    invoke-virtual {v5, v8, v6, v7}, Lcom/google/android/gms/games/b/j;->b(Ljava/lang/Object;J)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    .line 1760
    if-eqz v1, :cond_6

    .line 1761
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    move-object v1, v5

    move v5, v0

    goto :goto_0

    .line 1750
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1760
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_5

    .line 1761
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :cond_5
    throw v0

    :cond_6
    move-object v1, v5

    move v5, v0

    .line 1764
    goto :goto_0
.end method

.method public final h(Lcom/google/android/gms/games/a/au;)I
    .locals 22

    .prologue
    .line 2822
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 2823
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 2826
    invoke-static {v9}, Lcom/google/android/gms/games/provider/s;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v10

    .line 2827
    invoke-virtual {v9}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    .line 2828
    const/4 v8, 0x0

    .line 2830
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 2832
    new-instance v3, Lcom/google/android/gms/games/a/o;

    invoke-direct {v3, v4}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    const-string v5, "account_name=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    invoke-virtual {v3, v10, v5, v6}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/games/a/o;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/games/a/ae;->a:[Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    const-string v3, "external_game_id"

    iput-object v3, v2, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v12

    .line 2838
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 2840
    :goto_0
    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2842
    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 2843
    const/4 v2, 0x3

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 2844
    const/16 v2, 0x8

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2845
    const/4 v2, 0x6

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2846
    const/4 v2, 0x7

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-lez v2, :cond_1

    const/4 v7, 0x1

    .line 2848
    :goto_1
    const/4 v2, 0x2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 2849
    const/16 v17, 0x5

    move/from16 v0, v17

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 2850
    sub-long v20, v2, v18

    .line 2853
    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2854
    new-instance v2, Lcom/google/android/gms/games/a/ad;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/games/a/ad;-><init>(Lcom/google/android/gms/games/a/ab;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2856
    move-object/from16 v0, v16

    invoke-virtual {v13, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2860
    :cond_0
    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/a/ad;

    new-instance v3, Lcom/google/android/gms/games/h/a/fd;

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Lcom/google/android/gms/games/h/a/fd;-><init>(Ljava/lang/Long;Ljava/lang/Long;)V

    iget-object v2, v2, Lcom/google/android/gms/games/a/ad;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2865
    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/a/ad;

    .line 2866
    invoke-static {v10, v14, v15}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 2867
    iget-object v5, v2, Lcom/google/android/gms/games/a/ad;->c:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v5

    .line 2868
    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    iget-object v2, v2, Lcom/google/android/gms/games/a/ad;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 2873
    :catchall_0
    move-exception v2

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v2

    .line 2846
    :cond_1
    const/4 v7, 0x0

    goto :goto_1

    .line 2873
    :cond_2
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 2876
    invoke-virtual {v13}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v5, v8

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2877
    invoke-virtual {v13, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/games/a/ad;

    .line 2879
    move-object/from16 v0, p0

    invoke-direct {v0, v9, v2, v3}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/games/a/ad;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2880
    iget-object v2, v3, Lcom/google/android/gms/games/a/ad;->c:Ljava/util/ArrayList;

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 2883
    :cond_3
    const/4 v2, 0x5

    move v5, v2

    .line 2885
    goto :goto_2

    .line 2888
    :cond_4
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "GameAgent"

    invoke-static {v2, v11, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 2889
    return v5
.end method
