.class public final Lcom/google/android/gms/car/dr;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static volatile a:I

.field static final b:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    sput v0, Lcom/google/android/gms/car/dr;->a:I

    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/dr;->b:Ljava/util/HashMap;

    return-void
.end method

.method public static a(Lcom/google/android/gms/car/CarCall;)Landroid/telecom/Call;
    .locals 4

    .prologue
    .line 102
    sget-object v2, Lcom/google/android/gms/car/dr;->b:Ljava/util/HashMap;

    monitor-enter v2

    .line 103
    :try_start_0
    sget-object v0, Lcom/google/android/gms/car/dr;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Call;

    .line 104
    sget-object v1, Lcom/google/android/gms/car/dr;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/CarCall;

    invoke-virtual {v1, p0}, Lcom/google/android/gms/car/CarCall;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    :goto_0
    return-object v0

    .line 108
    :cond_1
    monitor-exit v2

    .line 109
    const/4 v0, 0x0

    goto :goto_0

    .line 108
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public static a(Landroid/telecom/Call;)Lcom/google/android/gms/car/CarCall;
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 33
    const-string v0, "CAR.TEL.CarCall"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    const-string v0, "CAR.TEL.CarCall"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "forCall "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    :cond_0
    if-nez p0, :cond_1

    move-object v0, v7

    .line 45
    :goto_0
    return-object v0

    .line 39
    :cond_1
    sget-object v9, Lcom/google/android/gms/car/dr;->b:Ljava/util/HashMap;

    monitor-enter v9

    .line 40
    :try_start_0
    sget-object v0, Lcom/google/android/gms/car/dr;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCall;

    .line 41
    if-nez v0, :cond_2

    .line 42
    sget v10, Lcom/google/android/gms/car/dr;->a:I

    add-int/lit8 v0, v10, 0x1

    sput v0, Lcom/google/android/gms/car/dr;->a:I

    invoke-virtual {p0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v4

    invoke-virtual {v4}, Landroid/telecom/Call$Details;->getDisconnectCause()Landroid/telecom/DisconnectCause;

    move-result-object v0

    if-nez v0, :cond_3

    move-object v3, v7

    :goto_1
    invoke-virtual {v4}, Landroid/telecom/Call$Details;->getGatewayInfo()Landroid/telecom/GatewayInfo;

    move-result-object v8

    new-instance v0, Lcom/google/android/gms/car/CarCall$Details;

    invoke-virtual {v4}, Landroid/telecom/Call$Details;->getHandle()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v4}, Landroid/telecom/Call$Details;->getCallerDisplayName()Ljava/lang/String;

    move-result-object v2

    if-nez v3, :cond_4

    move-object v3, v7

    :goto_2
    invoke-virtual {v4}, Landroid/telecom/Call$Details;->getConnectTimeMillis()J

    move-result-wide v4

    if-nez v8, :cond_5

    move-object v6, v7

    :goto_3
    if-nez v8, :cond_6

    :goto_4
    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/car/CarCall$Details;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;JLandroid/net/Uri;Landroid/net/Uri;)V

    new-instance v1, Lcom/google/android/gms/car/CarCall;

    invoke-virtual {p0}, Landroid/telecom/Call;->getParent()Landroid/telecom/Call;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/car/dr;->a(Landroid/telecom/Call;)Lcom/google/android/gms/car/CarCall;

    move-result-object v3

    invoke-virtual {p0}, Landroid/telecom/Call;->getCannedTextResponses()Ljava/util/List;

    move-result-object v4

    invoke-virtual {p0}, Landroid/telecom/Call;->getRemainingPostDialSequence()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Landroid/telecom/Call;->getState()I

    move-result v6

    invoke-virtual {p0}, Landroid/telecom/Call;->getChildren()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    const/4 v8, 0x1

    :goto_5
    move v2, v10

    move-object v7, v0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/car/CarCall;-><init>(ILcom/google/android/gms/car/CarCall;Ljava/util/List;Ljava/lang/String;ILcom/google/android/gms/car/CarCall$Details;Z)V

    .line 43
    sget-object v0, Lcom/google/android/gms/car/dr;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 45
    :cond_2
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit v9

    throw v0

    .line 42
    :cond_3
    :try_start_1
    invoke-virtual {v0}, Landroid/telecom/DisconnectCause;->getLabel()Ljava/lang/CharSequence;

    move-result-object v0

    move-object v3, v0

    goto :goto_1

    :cond_4
    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_5
    invoke-virtual {v8}, Landroid/telecom/GatewayInfo;->getOriginalAddress()Landroid/net/Uri;

    move-result-object v6

    goto :goto_3

    :cond_6
    invoke-virtual {v8}, Landroid/telecom/GatewayInfo;->getGatewayAddress()Landroid/net/Uri;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    goto :goto_4

    :cond_7
    const/4 v8, 0x0

    goto :goto_5
.end method

.method public static a()Ljava/util/List;
    .locals 4

    .prologue
    .line 70
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 71
    sget-object v0, Lcom/google/android/gms/car/dr;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Call;

    .line 72
    sget-object v3, Lcom/google/android/gms/car/dr;->b:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 74
    :cond_0
    return-object v1
.end method

.method public static b()V
    .locals 2

    .prologue
    .line 78
    const-string v0, "CAR.TEL.CarCall"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    const-string v0, "CAR.TEL.CarCall"

    const-string v1, "clearCalls"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    :cond_0
    sget-object v0, Lcom/google/android/gms/car/dr;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 82
    return-void
.end method

.method public static b(Landroid/telecom/Call;)V
    .locals 3

    .prologue
    .line 85
    sget-object v0, Lcom/google/android/gms/car/dr;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCall;

    .line 86
    if-eqz v0, :cond_0

    .line 87
    sget-object v0, Lcom/google/android/gms/car/dr;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    const-string v0, "CAR.TEL.CarCall"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to remove CarCall for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
