.class final Lcom/google/android/gms/auth/trustagent/trustlet/bm;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/trustagent/trustlet/bk;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bm;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 112
    const-string v0, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bm;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bm;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a(Lcom/google/android/gms/auth/trustagent/trustlet/bk;Ljava/util/List;)V

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    const-string v0, "android.intent.action.USER_PRESENT"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bm;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;

    .line 116
    instance-of v2, v0, Lcom/google/android/gms/auth/trustagent/trustlet/ap;

    if-nez v2, :cond_2

    iget-boolean v2, v0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->g:Z

    if-eqz v2, :cond_2

    .line 117
    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bm;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->e()I

    move-result v0

    new-instance v3, Lcom/google/android/gms/auth/e/f;

    iget-object v2, v2, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a:Landroid/content/Context;

    invoke-direct {v3, v2}, Lcom/google/android/gms/auth/e/f;-><init>(Landroid/content/Context;)V

    iput v0, v3, Lcom/google/android/gms/auth/e/f;->d:I

    const/4 v0, 0x1

    iput v0, v3, Lcom/google/android/gms/auth/e/f;->e:I

    invoke-virtual {v3}, Lcom/google/android/gms/auth/e/f;->a()V

    goto :goto_1

    .line 120
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bm;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->c:Z

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bm;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->c:Z

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bm;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->c()V

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bm;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a(Lcom/google/android/gms/auth/trustagent/trustlet/bk;)V

    goto :goto_0
.end method
