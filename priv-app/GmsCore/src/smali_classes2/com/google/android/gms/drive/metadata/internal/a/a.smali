.class public final Lcom/google/android/gms/drive/metadata/internal/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final A:Lcom/google/android/gms/drive/metadata/internal/a/i;

.field public static final B:Lcom/google/android/gms/drive/metadata/internal/a/j;

.field public static final C:Lcom/google/android/gms/drive/metadata/f;

.field public static final D:Lcom/google/android/gms/drive/metadata/f;

.field public static final E:Lcom/google/android/gms/drive/metadata/f;

.field public static final F:Lcom/google/android/gms/drive/metadata/internal/c;

.field public static final G:Lcom/google/android/gms/drive/metadata/f;

.field public static final a:Lcom/google/android/gms/drive/metadata/f;

.field public static final b:Lcom/google/android/gms/drive/metadata/f;

.field public static final c:Lcom/google/android/gms/drive/metadata/internal/a/c;

.field public static final d:Lcom/google/android/gms/drive/metadata/f;

.field public static final e:Lcom/google/android/gms/drive/metadata/f;

.field public static final f:Lcom/google/android/gms/drive/metadata/f;

.field public static final g:Lcom/google/android/gms/drive/metadata/f;

.field public static final h:Lcom/google/android/gms/drive/metadata/f;

.field public static final i:Lcom/google/android/gms/drive/metadata/f;

.field public static final j:Lcom/google/android/gms/drive/metadata/f;

.field public static final k:Lcom/google/android/gms/drive/metadata/f;

.field public static final l:Lcom/google/android/gms/drive/metadata/f;

.field public static final m:Lcom/google/android/gms/drive/metadata/internal/a/d;

.field public static final n:Lcom/google/android/gms/drive/metadata/f;

.field public static final o:Lcom/google/android/gms/drive/metadata/f;

.field public static final p:Lcom/google/android/gms/drive/metadata/f;

.field public static final q:Lcom/google/android/gms/drive/metadata/f;

.field public static final r:Lcom/google/android/gms/drive/metadata/internal/a/e;

.field public static final s:Lcom/google/android/gms/drive/metadata/f;

.field public static final t:Lcom/google/android/gms/drive/metadata/b;

.field public static final u:Lcom/google/android/gms/drive/metadata/internal/n;

.field public static final v:Lcom/google/android/gms/drive/metadata/internal/n;

.field public static final w:Lcom/google/android/gms/drive/metadata/internal/a/f;

.field public static final x:Lcom/google/android/gms/drive/metadata/internal/a/g;

.field public static final y:Lcom/google/android/gms/drive/metadata/internal/a/h;

.field public static final z:Lcom/google/android/gms/drive/metadata/f;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const v5, 0x5b8d80

    const v4, 0x419ce0

    .line 40
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/r;->c:Lcom/google/android/gms/drive/metadata/internal/a/r;

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->a:Lcom/google/android/gms/drive/metadata/f;

    .line 42
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string v1, "alternateLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->b:Lcom/google/android/gms/drive/metadata/f;

    .line 44
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a/c;

    invoke-direct {v0}, Lcom/google/android/gms/drive/metadata/internal/a/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->c:Lcom/google/android/gms/drive/metadata/internal/a/c;

    .line 46
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string v1, "description"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->d:Lcom/google/android/gms/drive/metadata/f;

    .line 48
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string v1, "embedLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->e:Lcom/google/android/gms/drive/metadata/f;

    .line 50
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string v1, "fileExtension"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->f:Lcom/google/android/gms/drive/metadata/f;

    .line 52
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/h;

    const-string v1, "fileSize"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->g:Lcom/google/android/gms/drive/metadata/f;

    .line 54
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/c;

    const-string v1, "hasThumbnail"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->h:Lcom/google/android/gms/drive/metadata/f;

    .line 56
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string v1, "indexableText"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->i:Lcom/google/android/gms/drive/metadata/f;

    .line 59
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/c;

    const-string v1, "isAppData"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->j:Lcom/google/android/gms/drive/metadata/f;

    .line 61
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/c;

    const-string v1, "isCopyable"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->k:Lcom/google/android/gms/drive/metadata/f;

    .line 63
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/c;

    const-string v1, "isEditable"

    const v2, 0x3e8fa0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->l:Lcom/google/android/gms/drive/metadata/f;

    .line 65
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a/d;

    const-string v1, "isPinned"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/a/d;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->m:Lcom/google/android/gms/drive/metadata/internal/a/d;

    .line 67
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/c;

    const-string v1, "isRestricted"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->n:Lcom/google/android/gms/drive/metadata/f;

    .line 69
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/c;

    const-string v1, "isShared"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->o:Lcom/google/android/gms/drive/metadata/f;

    .line 71
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/c;

    const-string v1, "isTrashable"

    const v2, 0x432380

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->p:Lcom/google/android/gms/drive/metadata/f;

    .line 73
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/c;

    const-string v1, "isViewed"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->q:Lcom/google/android/gms/drive/metadata/f;

    .line 75
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a/e;

    const-string v1, "mimeType"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/a/e;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->r:Lcom/google/android/gms/drive/metadata/internal/a/e;

    .line 77
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string v1, "originalFilename"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->s:Lcom/google/android/gms/drive/metadata/f;

    .line 79
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/l;

    const-string v1, "ownerNames"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->t:Lcom/google/android/gms/drive/metadata/b;

    .line 81
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/n;

    const-string v1, "lastModifyingUser"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/n;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->u:Lcom/google/android/gms/drive/metadata/internal/n;

    .line 83
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/n;

    const-string v1, "sharingUser"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/n;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->v:Lcom/google/android/gms/drive/metadata/internal/n;

    .line 86
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a/f;

    const-string v1, "parents"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/a/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->w:Lcom/google/android/gms/drive/metadata/internal/a/f;

    .line 88
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a/g;

    const-string v1, "quotaBytesUsed"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/a/g;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->x:Lcom/google/android/gms/drive/metadata/internal/a/g;

    .line 90
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a/h;

    const-string v1, "starred"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/a/h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->y:Lcom/google/android/gms/drive/metadata/internal/a/h;

    .line 93
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a/b;

    const-string v1, "thumbnail"

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/metadata/internal/a/b;-><init>(Ljava/lang/String;Ljava/util/Collection;Ljava/util/Collection;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->z:Lcom/google/android/gms/drive/metadata/f;

    .line 104
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a/i;

    const-string v1, "title"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/a/i;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->A:Lcom/google/android/gms/drive/metadata/internal/a/i;

    .line 106
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a/j;

    const-string v1, "trashed"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/a/j;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->B:Lcom/google/android/gms/drive/metadata/internal/a/j;

    .line 108
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string v1, "webContentLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->C:Lcom/google/android/gms/drive/metadata/f;

    .line 110
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string v1, "webViewLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->D:Lcom/google/android/gms/drive/metadata/f;

    .line 112
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string v1, "uniqueIdentifier"

    const v2, 0x4c4b40

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    .line 114
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/c;

    const-string v1, "writersCanShare"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/drive/metadata/internal/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->F:Lcom/google/android/gms/drive/metadata/internal/c;

    .line 116
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string v1, "role"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->G:Lcom/google/android/gms/drive/metadata/f;

    return-void
.end method
