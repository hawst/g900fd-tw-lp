.class public final Lcom/google/android/gms/games/ui/c/e;
.super Landroid/os/Handler;
.source "SourceFile"


# direct methods
.method private constructor <init>(Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 95
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Looper;B)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/c/e;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 99
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 110
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown message code"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/ui/c/b;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/c/b;->a(Lcom/google/android/gms/games/ui/c/b;)V

    .line 108
    :goto_0
    return-void

    .line 104
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/ui/c/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/c/b;->b()V

    goto :goto_0

    .line 107
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/ui/c/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/c/b;->c()V

    goto :goto_0

    .line 99
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
