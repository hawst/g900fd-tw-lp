.class public final Lcom/google/android/gms/icing/av;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile m:[Lcom/google/android/gms/icing/av;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:I

.field public d:I

.field public e:Z

.field public f:Ljava/lang/String;

.field public g:[I

.field public h:I

.field public i:I

.field public j:Ljava/lang/String;

.field public k:I

.field public l:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 83
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    iput-boolean v3, p0, Lcom/google/android/gms/icing/av;->b:Z

    iput v1, p0, Lcom/google/android/gms/icing/av;->c:I

    iput v3, p0, Lcom/google/android/gms/icing/av;->d:I

    iput-boolean v1, p0, Lcom/google/android/gms/icing/av;->e:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/av;->f:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/android/gms/icing/av;->g:[I

    iput v1, p0, Lcom/google/android/gms/icing/av;->h:I

    iput v1, p0, Lcom/google/android/gms/icing/av;->i:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/av;->j:Ljava/lang/String;

    iput v2, p0, Lcom/google/android/gms/icing/av;->k:I

    iput v1, p0, Lcom/google/android/gms/icing/av;->l:I

    iput v2, p0, Lcom/google/android/gms/icing/av;->cachedSize:I

    .line 84
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/av;
    .locals 2

    .prologue
    .line 35
    sget-object v0, Lcom/google/android/gms/icing/av;->m:[Lcom/google/android/gms/icing/av;

    if-nez v0, :cond_1

    .line 36
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 38
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/av;->m:[Lcom/google/android/gms/icing/av;

    if-nez v0, :cond_0

    .line 39
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/av;

    sput-object v0, Lcom/google/android/gms/icing/av;->m:[Lcom/google/android/gms/icing/av;

    .line 41
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/av;->m:[Lcom/google/android/gms/icing/av;

    return-object v0

    .line 41
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 232
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 233
    iget-object v2, p0, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 234
    iget-object v2, p0, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    invoke-static {v4, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 237
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/gms/icing/av;->b:Z

    if-eq v2, v4, :cond_1

    .line 238
    const/4 v2, 0x2

    iget-boolean v3, p0, Lcom/google/android/gms/icing/av;->b:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 241
    :cond_1
    iget v2, p0, Lcom/google/android/gms/icing/av;->c:I

    if-eqz v2, :cond_2

    .line 242
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/android/gms/icing/av;->c:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 245
    :cond_2
    iget v2, p0, Lcom/google/android/gms/icing/av;->d:I

    if-eq v2, v4, :cond_3

    .line 246
    const/4 v2, 0x4

    iget v3, p0, Lcom/google/android/gms/icing/av;->d:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 249
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/gms/icing/av;->e:Z

    if-eqz v2, :cond_4

    .line 250
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/android/gms/icing/av;->e:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 253
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/icing/av;->f:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 254
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/gms/icing/av;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 257
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/icing/av;->g:[I

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/gms/icing/av;->g:[I

    array-length v2, v2

    if-lez v2, :cond_7

    move v2, v1

    .line 259
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/icing/av;->g:[I

    array-length v3, v3

    if-ge v1, v3, :cond_6

    .line 260
    iget-object v3, p0, Lcom/google/android/gms/icing/av;->g:[I

    aget v3, v3, v1

    .line 261
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 259
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 264
    :cond_6
    add-int/2addr v0, v2

    .line 265
    iget-object v1, p0, Lcom/google/android/gms/icing/av;->g:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 267
    :cond_7
    iget v1, p0, Lcom/google/android/gms/icing/av;->h:I

    if-eqz v1, :cond_8

    .line 268
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/gms/icing/av;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 271
    :cond_8
    iget v1, p0, Lcom/google/android/gms/icing/av;->i:I

    if-eqz v1, :cond_9

    .line 272
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/gms/icing/av;->i:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 275
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/icing/av;->j:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 276
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/gms/icing/av;->j:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 279
    :cond_a
    iget v1, p0, Lcom/google/android/gms/icing/av;->k:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_b

    .line 280
    const/16 v1, 0xc

    iget v2, p0, Lcom/google/android/gms/icing/av;->k:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 283
    :cond_b
    iget v1, p0, Lcom/google/android/gms/icing/av;->l:I

    if-eqz v1, :cond_c

    .line 284
    const/16 v1, 0xd

    iget v2, p0, Lcom/google/android/gms/icing/av;->l:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 287
    :cond_c
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 105
    if-ne p1, p0, :cond_1

    .line 161
    :cond_0
    :goto_0
    return v0

    .line 108
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/av;

    if-nez v2, :cond_2

    move v0, v1

    .line 109
    goto :goto_0

    .line 111
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/av;

    .line 112
    iget-object v2, p0, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 113
    iget-object v2, p1, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 114
    goto :goto_0

    .line 116
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 117
    goto :goto_0

    .line 119
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/gms/icing/av;->b:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/av;->b:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 120
    goto :goto_0

    .line 122
    :cond_5
    iget v2, p0, Lcom/google/android/gms/icing/av;->c:I

    iget v3, p1, Lcom/google/android/gms/icing/av;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 123
    goto :goto_0

    .line 125
    :cond_6
    iget v2, p0, Lcom/google/android/gms/icing/av;->d:I

    iget v3, p1, Lcom/google/android/gms/icing/av;->d:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 126
    goto :goto_0

    .line 128
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/gms/icing/av;->e:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/av;->e:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 129
    goto :goto_0

    .line 131
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/icing/av;->f:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 132
    iget-object v2, p1, Lcom/google/android/gms/icing/av;->f:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    .line 133
    goto :goto_0

    .line 135
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/icing/av;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/av;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 136
    goto :goto_0

    .line 138
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/icing/av;->g:[I

    iget-object v3, p1, Lcom/google/android/gms/icing/av;->g:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 140
    goto :goto_0

    .line 142
    :cond_b
    iget v2, p0, Lcom/google/android/gms/icing/av;->h:I

    iget v3, p1, Lcom/google/android/gms/icing/av;->h:I

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 143
    goto :goto_0

    .line 145
    :cond_c
    iget v2, p0, Lcom/google/android/gms/icing/av;->i:I

    iget v3, p1, Lcom/google/android/gms/icing/av;->i:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 146
    goto :goto_0

    .line 148
    :cond_d
    iget-object v2, p0, Lcom/google/android/gms/icing/av;->j:Ljava/lang/String;

    if-nez v2, :cond_e

    .line 149
    iget-object v2, p1, Lcom/google/android/gms/icing/av;->j:Ljava/lang/String;

    if-eqz v2, :cond_f

    move v0, v1

    .line 150
    goto :goto_0

    .line 152
    :cond_e
    iget-object v2, p0, Lcom/google/android/gms/icing/av;->j:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/av;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    .line 153
    goto/16 :goto_0

    .line 155
    :cond_f
    iget v2, p0, Lcom/google/android/gms/icing/av;->k:I

    iget v3, p1, Lcom/google/android/gms/icing/av;->k:I

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 156
    goto/16 :goto_0

    .line 158
    :cond_10
    iget v2, p0, Lcom/google/android/gms/icing/av;->l:I

    iget v3, p1, Lcom/google/android/gms/icing/av;->l:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 159
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 169
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/icing/av;->b:Z

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    add-int/2addr v0, v4

    .line 170
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/icing/av;->c:I

    add-int/2addr v0, v4

    .line 171
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/icing/av;->d:I

    add-int/2addr v0, v4

    .line 172
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lcom/google/android/gms/icing/av;->e:Z

    if-eqz v4, :cond_2

    :goto_2
    add-int/2addr v0, v2

    .line 173
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/av;->f:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 175
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/av;->g:[I

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v2

    add-int/2addr v0, v2

    .line 177
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/icing/av;->h:I

    add-int/2addr v0, v2

    .line 178
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/icing/av;->i:I

    add-int/2addr v0, v2

    .line 179
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/av;->j:Ljava/lang/String;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 181
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/av;->k:I

    add-int/2addr v0, v1

    .line 182
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/av;->l:I

    add-int/2addr v0, v1

    .line 183
    return v0

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v3

    .line 169
    goto :goto_1

    :cond_2
    move v2, v3

    .line 172
    goto :goto_2

    .line 173
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/av;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 179
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/av;->j:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 13
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/av;->b:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/icing/av;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/av;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/av;->e:Z

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/av;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x38

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_1

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_1
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/av;->g:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Lcom/google/android/gms/icing/av;->g:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/av;->g:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Lcom/google/android/gms/icing/av;->g:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/android/gms/icing/av;->g:[I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_2

    goto :goto_4

    :pswitch_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/android/gms/icing/av;->g:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/icing/av;->g:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_3

    goto :goto_6

    :pswitch_3
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/icing/av;->g:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Lcom/google/android/gms/icing/av;->g:[I

    :cond_a
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/av;->h:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/av;->i:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/av;->j:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/av;->k:I

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_4

    goto/16 :goto_0

    :pswitch_4
    iput v0, p0, Lcom/google/android/gms/icing/av;->l:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x3a -> :sswitch_8
        0x40 -> :sswitch_9
        0x48 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 192
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/icing/av;->b:Z

    if-eq v0, v2, :cond_1

    .line 193
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/gms/icing/av;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 195
    :cond_1
    iget v0, p0, Lcom/google/android/gms/icing/av;->c:I

    if-eqz v0, :cond_2

    .line 196
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/icing/av;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 198
    :cond_2
    iget v0, p0, Lcom/google/android/gms/icing/av;->d:I

    if-eq v0, v2, :cond_3

    .line 199
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/icing/av;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 201
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/icing/av;->e:Z

    if-eqz v0, :cond_4

    .line 202
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/gms/icing/av;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 204
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/icing/av;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 205
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/gms/icing/av;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 207
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/av;->g:[I

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/icing/av;->g:[I

    array-length v0, v0

    if-lez v0, :cond_6

    .line 208
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/icing/av;->g:[I

    array-length v1, v1

    if-ge v0, v1, :cond_6

    .line 209
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/icing/av;->g:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 208
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 212
    :cond_6
    iget v0, p0, Lcom/google/android/gms/icing/av;->h:I

    if-eqz v0, :cond_7

    .line 213
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/android/gms/icing/av;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 215
    :cond_7
    iget v0, p0, Lcom/google/android/gms/icing/av;->i:I

    if-eqz v0, :cond_8

    .line 216
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/android/gms/icing/av;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 218
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/icing/av;->j:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 219
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/gms/icing/av;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 221
    :cond_9
    iget v0, p0, Lcom/google/android/gms/icing/av;->k:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_a

    .line 222
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/android/gms/icing/av;->k:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 224
    :cond_a
    iget v0, p0, Lcom/google/android/gms/icing/av;->l:I

    if-eqz v0, :cond_b

    .line 225
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/android/gms/icing/av;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 227
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 228
    return-void
.end method
