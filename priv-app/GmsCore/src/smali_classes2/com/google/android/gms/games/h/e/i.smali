.class public final Lcom/google/android/gms/games/h/e/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/server/response/s;


# static fields
.field private static final a:I

.field private static final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    sget v0, Lcom/google/android/gms/g;->O:I

    sput v0, Lcom/google/android/gms/games/h/e/i;->a:I

    .line 19
    sget v0, Lcom/google/android/gms/g;->P:I

    sput v0, Lcom/google/android/gms/games/h/e/i;->b:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/server/response/FastJsonResponse;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 16
    check-cast p1, Lcom/google/android/gms/games/h/a/dl;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    const-string v2, "quest_banner_image_url"

    sget v3, Lcom/google/android/gms/games/h/e/i;->a:I

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/games/h/e/g;->a(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string v2, "quest_icon_image_url"

    sget v3, Lcom/google/android/gms/games/h/e/i;->b:I

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/games/h/e/g;->a(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string v0, "quest_type"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "accepted_ts"

    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "accepted_ts"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_0
    const-string v0, "notification_ts"

    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "notification_ts"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_1
    return-object p1
.end method
