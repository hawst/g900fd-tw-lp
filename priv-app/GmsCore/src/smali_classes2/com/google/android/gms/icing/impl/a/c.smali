.class final Lcom/google/android/gms/icing/impl/a/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lorg/xmlpull/v1/XmlPullParser;

.field private final b:I

.field private c:I


# direct methods
.method constructor <init>(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 1

    .prologue
    .line 694
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 692
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/impl/a/c;->c:I

    .line 695
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/a/c;->a:Lorg/xmlpull/v1/XmlPullParser;

    .line 696
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/c;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/impl/a/c;->b:I

    .line 697
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 700
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/c;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/icing/impl/a/c;->c:I

    .line 702
    :goto_0
    iget v1, p0, Lcom/google/android/gms/icing/impl/a/c;->c:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/c;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v1

    iget v2, p0, Lcom/google/android/gms/icing/impl/a/c;->b:I

    if-le v1, v2, :cond_1

    :cond_0
    iget v1, p0, Lcom/google/android/gms/icing/impl/a/c;->c:I

    if-eq v1, v0, :cond_1

    .line 703
    iget v1, p0, Lcom/google/android/gms/icing/impl/a/c;->c:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    .line 704
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/c;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/icing/impl/a/c;->c:I

    goto :goto_0

    .line 709
    :cond_1
    const/4 v0, 0x0

    :cond_2
    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 713
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/c;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
