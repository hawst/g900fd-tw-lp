.class public final Lcom/google/android/gms/games/h/a/k;
.super Lcom/google/android/gms/common/server/response/c;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/HashMap;


# instance fields
.field private final c:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 53
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 55
    sput-object v0, Lcom/google/android/gms/games/h/a/k;->b:Ljava/util/HashMap;

    const-string v1, "achievementId"

    const-string v2, "achievementId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/google/android/gms/games/h/a/k;->b:Ljava/util/HashMap;

    const-string v1, "incrementPayload"

    const-string v2, "incrementPayload"

    const-class v3, Lcom/google/android/gms/games/h/a/bm;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/google/android/gms/games/h/a/k;->b:Ljava/util/HashMap;

    const-string v1, "setStepsAtLeastPayload"

    const-string v2, "setStepsAtLeastPayload"

    const-class v3, Lcom/google/android/gms/games/h/a/bn;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/google/android/gms/games/h/a/k;->b:Ljava/util/HashMap;

    const-string v1, "updateType"

    const-string v2, "updateType"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 92
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/h/a/k;->c:Ljava/util/HashMap;

    .line 70
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/gms/games/h/a/bm;Lcom/google/android/gms/games/h/a/bn;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 92
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/h/a/k;->c:Ljava/util/HashMap;

    .line 78
    if-eqz p1, :cond_0

    .line 79
    const-string v0, "achievementId"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/h/a/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    :cond_0
    if-eqz p2, :cond_1

    .line 82
    const-string v0, "incrementPayload"

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/games/h/a/k;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    .line 84
    :cond_1
    if-eqz p3, :cond_2

    .line 85
    const-string v0, "setStepsAtLeastPayload"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/games/h/a/k;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    .line 87
    :cond_2
    if-eqz p4, :cond_3

    .line 88
    const-string v0, "updateType"

    invoke-virtual {p0, v0, p4}, Lcom/google/android/gms/games/h/a/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :cond_3
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/google/android/gms/games/h/a/k;->b:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/k;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    return-void
.end method

.method protected final d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/k;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final getIncrementPayload()Lcom/google/android/gms/games/h/a/bm;
    .locals 2
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/k;->c:Ljava/util/HashMap;

    const-string v1, "incrementPayload"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/bm;

    return-object v0
.end method

.method public final getSetStepsAtLeastPayload()Lcom/google/android/gms/games/h/a/bn;
    .locals 2
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/k;->c:Ljava/util/HashMap;

    const-string v1, "setStepsAtLeastPayload"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/bn;

    return-object v0
.end method
