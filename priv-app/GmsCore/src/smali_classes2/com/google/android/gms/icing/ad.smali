.class public final Lcom/google/android/gms/icing/ad;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/android/gms/icing/ae;

.field public b:I

.field public c:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3551
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 3552
    invoke-static {}, Lcom/google/android/gms/icing/ae;->a()[Lcom/google/android/gms/icing/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/ad;->a:[Lcom/google/android/gms/icing/ae;

    iput v1, p0, Lcom/google/android/gms/icing/ad;->b:I

    iput-boolean v1, p0, Lcom/google/android/gms/icing/ad;->c:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/ad;->cachedSize:I

    .line 3553
    return-void
.end method

.method public static a([B)Lcom/google/android/gms/icing/ad;
    .locals 1

    .prologue
    .line 3694
    new-instance v0, Lcom/google/android/gms/icing/ad;

    invoke-direct {v0}, Lcom/google/android/gms/icing/ad;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/ad;

    return-object v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 3617
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v1

    .line 3618
    iget-object v0, p0, Lcom/google/android/gms/icing/ad;->a:[Lcom/google/android/gms/icing/ae;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/ad;->a:[Lcom/google/android/gms/icing/ae;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 3619
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/ad;->a:[Lcom/google/android/gms/icing/ae;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 3620
    iget-object v2, p0, Lcom/google/android/gms/icing/ad;->a:[Lcom/google/android/gms/icing/ae;

    aget-object v2, v2, v0

    .line 3621
    if-eqz v2, :cond_0

    .line 3622
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3619
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3627
    :cond_1
    iget v0, p0, Lcom/google/android/gms/icing/ad;->b:I

    if-eqz v0, :cond_2

    .line 3628
    const/4 v0, 0x2

    iget v2, p0, Lcom/google/android/gms/icing/ad;->b:I

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 3631
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/icing/ad;->c:Z

    if-eqz v0, :cond_3

    .line 3632
    const/4 v0, 0x3

    iget-boolean v2, p0, Lcom/google/android/gms/icing/ad;->c:Z

    invoke-static {v0}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 3635
    :cond_3
    return v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3565
    if-ne p1, p0, :cond_1

    .line 3582
    :cond_0
    :goto_0
    return v0

    .line 3568
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/ad;

    if-nez v2, :cond_2

    move v0, v1

    .line 3569
    goto :goto_0

    .line 3571
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/ad;

    .line 3572
    iget-object v2, p0, Lcom/google/android/gms/icing/ad;->a:[Lcom/google/android/gms/icing/ae;

    iget-object v3, p1, Lcom/google/android/gms/icing/ad;->a:[Lcom/google/android/gms/icing/ae;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 3574
    goto :goto_0

    .line 3576
    :cond_3
    iget v2, p0, Lcom/google/android/gms/icing/ad;->b:I

    iget v3, p1, Lcom/google/android/gms/icing/ad;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 3577
    goto :goto_0

    .line 3579
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/gms/icing/ad;->c:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/ad;->c:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 3580
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 3587
    iget-object v0, p0, Lcom/google/android/gms/icing/ad;->a:[Lcom/google/android/gms/icing/ae;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 3590
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/ad;->b:I

    add-int/2addr v0, v1

    .line 3591
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/icing/ad;->c:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v1

    .line 3592
    return v0

    .line 3591
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3394
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/ad;->a:[Lcom/google/android/gms/icing/ae;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/ae;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/ad;->a:[Lcom/google/android/gms/icing/ae;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/icing/ae;

    invoke-direct {v3}, Lcom/google/android/gms/icing/ae;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/ad;->a:[Lcom/google/android/gms/icing/ae;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/icing/ae;

    invoke-direct {v3}, Lcom/google/android/gms/icing/ae;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/ad;->a:[Lcom/google/android/gms/icing/ae;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/icing/ad;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/ad;->c:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 3598
    iget-object v0, p0, Lcom/google/android/gms/icing/ad;->a:[Lcom/google/android/gms/icing/ae;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/ad;->a:[Lcom/google/android/gms/icing/ae;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 3599
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/icing/ad;->a:[Lcom/google/android/gms/icing/ae;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 3600
    iget-object v1, p0, Lcom/google/android/gms/icing/ad;->a:[Lcom/google/android/gms/icing/ae;

    aget-object v1, v1, v0

    .line 3601
    if-eqz v1, :cond_0

    .line 3602
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3599
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3606
    :cond_1
    iget v0, p0, Lcom/google/android/gms/icing/ad;->b:I

    if-eqz v0, :cond_2

    .line 3607
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/icing/ad;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3609
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/icing/ad;->c:Z

    if-eqz v0, :cond_3

    .line 3610
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/gms/icing/ad;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 3612
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3613
    return-void
.end method
