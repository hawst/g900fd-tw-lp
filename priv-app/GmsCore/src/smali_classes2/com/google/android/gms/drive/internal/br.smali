.class final Lcom/google/android/gms/drive/internal/br;
.super Lcom/google/android/gms/drive/internal/z;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/am;

.field final synthetic b:Lcom/google/android/gms/drive/DriveId;

.field final synthetic d:I

.field final synthetic e:I

.field final synthetic f:Lcom/google/android/gms/drive/internal/bo;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/internal/bo;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/am;Lcom/google/android/gms/drive/DriveId;II)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/br;->f:Lcom/google/android/gms/drive/internal/bo;

    iput-object p3, p0, Lcom/google/android/gms/drive/internal/br;->a:Lcom/google/android/gms/drive/am;

    iput-object p4, p0, Lcom/google/android/gms/drive/internal/br;->b:Lcom/google/android/gms/drive/DriveId;

    iput p5, p0, Lcom/google/android/gms/drive/internal/br;->d:I

    iput p6, p0, Lcom/google/android/gms/drive/internal/br;->e:I

    invoke-direct {p0, p2}, Lcom/google/android/gms/drive/internal/z;-><init>(Lcom/google/android/gms/common/api/v;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Lcom/google/android/gms/common/api/h;)V
    .locals 6

    .prologue
    .line 114
    check-cast p1, Lcom/google/android/gms/drive/internal/aj;

    iget-object v0, p0, Lcom/google/android/gms/drive/internal/br;->a:Lcom/google/android/gms/drive/am;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/am;->e()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/aj;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/gms/drive/ae;

    invoke-direct {v0}, Lcom/google/android/gms/drive/ae;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ae;->b()Lcom/google/android/gms/drive/ad;

    move-result-object v5

    new-instance v0, Lcom/google/android/gms/drive/internal/CreateFileRequest;

    iget-object v1, p0, Lcom/google/android/gms/drive/internal/br;->b:Lcom/google/android/gms/drive/DriveId;

    iget-object v2, p0, Lcom/google/android/gms/drive/internal/br;->a:Lcom/google/android/gms/drive/am;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/am;->e()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/drive/internal/br;->d:I

    iget v4, p0, Lcom/google/android/gms/drive/internal/br;->e:I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/internal/CreateFileRequest;-><init>(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;IILcom/google/android/gms/drive/ad;)V

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/aj;->f()Lcom/google/android/gms/drive/internal/bx;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/drive/internal/x;

    invoke-direct {v2, p0}, Lcom/google/android/gms/drive/internal/x;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/drive/internal/bx;->a(Lcom/google/android/gms/drive/internal/CreateFileRequest;Lcom/google/android/gms/drive/internal/ca;)V

    return-void
.end method
