.class public abstract Lcom/google/android/gms/games/ui/client/quests/a;
.super Lcom/google/android/gms/games/ui/client/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/common/quests/j;
.implements Lcom/google/android/gms/games/ui/common/quests/l;


# static fields
.field private static final n:I


# instance fields
.field private o:I

.field private p:Lcom/google/android/gms/games/ui/common/quests/a;

.field private q:I

.field private r:Lcom/google/android/gms/games/ui/client/quests/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    sget v0, Lcom/google/android/gms/m;->e:I

    sput v0, Lcom/google/android/gms/games/ui/client/quests/a;->n:I

    return-void
.end method

.method public constructor <init>(III)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 34
    sget v0, Lcom/google/android/gms/games/ui/client/quests/a;->n:I

    invoke-direct {p0, p1, v0, v1, v1}, Lcom/google/android/gms/games/ui/client/a;-><init>(IIZZ)V

    .line 36
    iput p2, p0, Lcom/google/android/gms/games/ui/client/quests/a;->o:I

    .line 37
    iput p3, p0, Lcom/google/android/gms/games/ui/client/quests/a;->q:I

    .line 38
    new-instance v0, Lcom/google/android/gms/games/ui/client/quests/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/client/quests/b;-><init>(Lcom/google/android/gms/games/ui/client/a;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/quests/a;->r:Lcom/google/android/gms/games/ui/client/quests/b;

    .line 39
    return-void
.end method


# virtual methods
.method public final Q_()Lcom/google/android/gms/games/ui/common/quests/i;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/quests/a;->r:Lcom/google/android/gms/games/ui/client/quests/b;

    return-object v0
.end method

.method public final U()Z
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    return v0
.end method

.method protected a(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->onCreate(Landroid/os/Bundle;)V

    .line 48
    iget v0, p0, Lcom/google/android/gms/games/ui/client/quests/a;->q:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/quests/a;->setTitle(I)V

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/quests/a;->l:Z

    .line 53
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/quests/a;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/games/ui/client/quests/a;->o:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/quests/a;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/quests/a;->p:Lcom/google/android/gms/games/ui/common/quests/a;

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/quests/a;->p:Lcom/google/android/gms/games/ui/common/quests/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/quests/a;->y()V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/quests/a;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/quests/a;->a(Landroid/content/Intent;)V

    .line 58
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 78
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/google/android/gms/j;->lV:I

    if-ne v1, v2, :cond_0

    .line 79
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/quests/a;->p:Lcom/google/android/gms/games/ui/common/quests/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/common/quests/a;->b(Z)V

    .line 82
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
