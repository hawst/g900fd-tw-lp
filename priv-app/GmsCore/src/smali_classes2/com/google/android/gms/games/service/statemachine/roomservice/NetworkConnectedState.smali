.class final Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;
.super Lcom/google/android/gms/games/service/statemachine/roomservice/u;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomservice/u;-><init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V

    .line 33
    return-void
.end method

.method private a(Lcom/google/android/gms/games/realtime/b;)V
    .locals 6

    .prologue
    .line 89
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 90
    invoke-virtual {p1}, Lcom/google/android/gms/games/realtime/b;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/realtime/c;

    .line 92
    iget-object v3, v0, Lcom/google/android/gms/games/realtime/c;->a:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/gms/games/realtime/b;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget v3, v0, Lcom/google/android/gms/games/realtime/c;->c:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 94
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    sget-boolean v3, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->h:Z

    if-eqz v3, :cond_0

    .line 96
    const-string v3, "RoomServiceStateMachine"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Participant Joined = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/c;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 101
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->e:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    const/4 v2, 0x6

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->b(ILjava/lang/Object;)V

    .line 104
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->a:Ljava/lang/String;

    .line 40
    iput-object p2, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->b:Ljava/lang/String;

    .line 41
    invoke-static {p1}, Lcom/google/android/gms/games/multiplayer/k;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Expecting player id!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 43
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->c()V

    .line 44
    return-void

    .line 41
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/os/Message;)Z
    .locals 5

    .prologue
    .line 48
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 84
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->g:Z

    :goto_0
    return v0

    .line 50
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->l:Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->h:Lcom/google/android/gms/games/service/statemachine/roomservice/WaitNetworkDisconnectState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/WaitNetworkDisconnectState;->b()V

    .line 52
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->f:Z

    goto :goto_0

    .line 54
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 55
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->f:Z

    goto :goto_0

    .line 57
    :sswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/i;

    .line 58
    new-instance v2, Lcom/google/android/gms/games/realtime/b;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->a:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/i;->a:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-direct {v2, v1, v3}, Lcom/google/android/gms/games/realtime/b;-><init>(Ljava/lang/String;Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    .line 59
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mPlayerId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Enter room: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/gms/games/realtime/b;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V

    .line 67
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->b:Lcom/google/android/gms/games/realtime/network/b;

    iget-object v3, v2, Lcom/google/android/gms/games/realtime/b;->a:Ljava/lang/String;

    invoke-interface {v1, v3}, Lcom/google/android/gms/games/realtime/network/b;->e(Ljava/lang/String;)V

    .line 68
    invoke-direct {p0, v2}, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->a(Lcom/google/android/gms/games/realtime/b;)V

    .line 70
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/i;->a:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v3}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->a()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v1, v3, v4}, Lcom/google/android/gms/games/internal/eg;->a(Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->l:Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->b:Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;

    iget-object v3, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->a:Ljava/lang/String;

    iget-boolean v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/i;->b:Z

    iget-object v4, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->b:Ljava/lang/String;

    invoke-virtual {v1, v3, v2, v0, v4}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a(Ljava/lang/String;Lcom/google/android/gms/games/realtime/b;ZLjava/lang/String;)V

    .line 75
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->f:Z

    goto :goto_0

    .line 71
    :catch_0
    move-exception v1

    .line 72
    iget-object v3, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->e:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    invoke-static {v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Landroid/os/RemoteException;)V

    goto :goto_1

    .line 78
    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->b:Lcom/google/android/gms/games/realtime/network/b;

    invoke-interface {v0}, Lcom/google/android/gms/games/realtime/network/b;->b()V

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->l:Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->h:Lcom/google/android/gms/games/service/statemachine/roomservice/WaitNetworkDisconnectState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/WaitNetworkDisconnectState;->b()V

    .line 81
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->f:Z

    goto/16 :goto_0

    .line 48
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x4 -> :sswitch_2
        0x5 -> :sswitch_1
        0x13 -> :sswitch_3
    .end sparse-switch
.end method
