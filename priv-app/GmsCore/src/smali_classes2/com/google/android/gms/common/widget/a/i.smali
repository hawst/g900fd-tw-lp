.class public final Lcom/google/android/gms/common/widget/a/i;
.super Lcom/google/android/gms/common/widget/a/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/widget/a/f;


# instance fields
.field private final a:Ljava/util/ArrayList;

.field private b:Lcom/google/android/gms/common/widget/a/j;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/common/widget/a/i;-><init>(Ljava/lang/CharSequence;B)V

    .line 22
    return-void
.end method

.method private constructor <init>(Ljava/lang/CharSequence;B)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/gms/common/widget/a/a;-><init>()V

    .line 25
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/widget/a/i;->a(Ljava/lang/CharSequence;)V

    .line 26
    const v0, 0x7fffffff

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/widget/a/i;->a(I)V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/widget/a/i;->a:Ljava/util/ArrayList;

    .line 28
    return-void
.end method

.method private d(Lcom/google/android/gms/common/widget/a/e;)I
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/i;->a:Ljava/util/ArrayList;

    invoke-static {v0, p1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    .line 182
    if-gez v0, :cond_0

    .line 186
    neg-int v0, v0

    add-int/lit8 v0, v0, -0x1

    .line 188
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/common/widget/a/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 189
    return v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/widget/a/e;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 96
    if-gez v3, :cond_1

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/i;->b:Lcom/google/android/gms/common/widget/a/j;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/i;->b:Lcom/google/android/gms/common/widget/a/j;

    invoke-interface {v0, v3}, Lcom/google/android/gms/common/widget/a/j;->c_(I)V

    .line 102
    :cond_2
    const/4 v1, 0x0

    .line 103
    if-lez v3, :cond_3

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/i;->a:Ljava/util/ArrayList;

    add-int/lit8 v4, v3, -0x1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/widget/a/e;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/widget/a/e;->compareTo(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_3

    move v1, v2

    .line 108
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v3, v0, :cond_4

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/i;->a:Ljava/util/ArrayList;

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/widget/a/e;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/widget/a/e;->compareTo(Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_4

    move v1, v2

    .line 114
    :cond_4
    if-eqz v1, :cond_0

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 116
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/widget/a/i;->d(Lcom/google/android/gms/common/widget/a/e;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/common/widget/a/i;->b:Lcom/google/android/gms/common/widget/a/j;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/common/widget/a/i;->b:Lcom/google/android/gms/common/widget/a/j;

    invoke-interface {v1, v3, v0}, Lcom/google/android/gms/common/widget/a/j;->a_(II)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/widget/a/j;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/gms/common/widget/a/i;->b:Lcom/google/android/gms/common/widget/a/j;

    .line 32
    return-void
.end method

.method public final varargs a([Lcom/google/android/gms/common/widget/a/e;)V
    .locals 3

    .prologue
    .line 88
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 89
    invoke-virtual {p0, v2}, Lcom/google/android/gms/common/widget/a/i;->c(Lcom/google/android/gms/common/widget/a/e;)V

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 91
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/widget/a/e;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 72
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/widget/a/i;->c(Lcom/google/android/gms/common/widget/a/e;)V

    .line 73
    return-void
.end method

.method public final c(Lcom/google/android/gms/common/widget/a/e;)V
    .locals 2

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/widget/a/i;->d(Lcom/google/android/gms/common/widget/a/e;)I

    move-result v0

    .line 83
    iget-object v1, p0, Lcom/google/android/gms/common/widget/a/i;->b:Lcom/google/android/gms/common/widget/a/j;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/common/widget/a/i;->b:Lcom/google/android/gms/common/widget/a/j;

    invoke-interface {v1, v0}, Lcom/google/android/gms/common/widget/a/j;->b_(I)V

    .line 84
    :cond_0
    invoke-interface {p1, p0}, Lcom/google/android/gms/common/widget/a/e;->a(Lcom/google/android/gms/common/widget/a/f;)V

    .line 85
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 37
    sget v0, Lcom/google/android/gms/common/widget/e;->a:I

    return v0
.end method

.method public final f()Lcom/google/android/gms/common/widget/a/h;
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/google/android/gms/common/widget/a/c;->a()Lcom/google/android/gms/common/widget/a/c;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/util/List;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/i;->a:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
