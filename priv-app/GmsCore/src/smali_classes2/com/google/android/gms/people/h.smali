.class public final Lcom/google/android/gms/people/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/people/h;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/util/Collection;

.field private d:I

.field private e:Z

.field private f:J

.field private g:Ljava/lang/String;

.field private h:I

.field private i:I

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 232
    new-instance v0, Lcom/google/android/gms/people/h;

    invoke-direct {v0}, Lcom/google/android/gms/people/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/h;->a:Lcom/google/android/gms/people/h;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 238
    const v0, 0x1fffff

    iput v0, p0, Lcom/google/android/gms/people/h;->d:I

    .line 246
    const/4 v0, 0x7

    iput v0, p0, Lcom/google/android/gms/people/h;->h:I

    .line 248
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/people/h;->i:I

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/gms/people/h;
    .locals 0

    .prologue
    .line 312
    iput-object p1, p0, Lcom/google/android/gms/people/h;->g:Ljava/lang/String;

    .line 313
    return-object p0
.end method

.method public final a(Ljava/util/Collection;)Lcom/google/android/gms/people/h;
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Lcom/google/android/gms/people/h;->c:Ljava/util/Collection;

    .line 280
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/gms/people/h;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/android/gms/people/h;->c:Ljava/util/Collection;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 350
    iget v0, p0, Lcom/google/android/gms/people/h;->d:I

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 354
    iget-boolean v0, p0, Lcom/google/android/gms/people/h;->e:Z

    return v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 358
    iget-wide v0, p0, Lcom/google/android/gms/people/h;->f:J

    return-wide v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/gms/people/h;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 366
    iget v0, p0, Lcom/google/android/gms/people/h;->h:I

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 370
    iget v0, p0, Lcom/google/android/gms/people/h;->i:I

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 374
    iget v0, p0, Lcom/google/android/gms/people/h;->j:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 254
    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "mCircleId"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/people/h;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "mQualifiedIds"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/people/h;->c:Ljava/util/Collection;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "mProjection"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/people/h;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "mPeopleOnly"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/android/gms/people/h;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "mChangedSince"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-wide v2, p0, Lcom/google/android/gms/people/h;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "mQuery"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/gms/people/h;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "mSearchFields"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget v2, p0, Lcom/google/android/gms/people/h;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "mSortOrder"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget v2, p0, Lcom/google/android/gms/people/h;->i:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "mExtraColumns"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget v2, p0, Lcom/google/android/gms/people/h;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/people/internal/n;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
