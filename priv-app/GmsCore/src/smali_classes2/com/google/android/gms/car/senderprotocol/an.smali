.class public final Lcom/google/android/gms/car/senderprotocol/an;
.super Lcom/google/android/gms/car/senderprotocol/az;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/car/senderprotocol/ao;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/fh;Lcom/google/android/gms/car/gx;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/senderprotocol/az;-><init>(Lcom/google/android/gms/car/gw;Lcom/google/android/gms/car/gx;)V

    .line 34
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/an;->a:Lcom/google/android/gms/car/senderprotocol/ao;

    .line 35
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;IZZZ)V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lcom/google/android/c/b/ba;

    invoke-direct {v0}, Lcom/google/android/c/b/ba;-><init>()V

    .line 40
    invoke-virtual {v0, p1}, Lcom/google/android/c/b/ba;->a(I)Lcom/google/android/c/b/ba;

    .line 41
    if-eqz p2, :cond_0

    .line 42
    invoke-virtual {v0, p2}, Lcom/google/android/c/b/ba;->a(Ljava/lang/String;)Lcom/google/android/c/b/ba;

    .line 44
    :cond_0
    invoke-virtual {v0, p3}, Lcom/google/android/c/b/ba;->b(I)Lcom/google/android/c/b/ba;

    .line 45
    invoke-virtual {v0, p4}, Lcom/google/android/c/b/ba;->a(Z)Lcom/google/android/c/b/ba;

    .line 46
    invoke-virtual {v0, p5}, Lcom/google/android/c/b/ba;->b(Z)Lcom/google/android/c/b/ba;

    .line 47
    invoke-virtual {v0, p6}, Lcom/google/android/c/b/ba;->c(Z)Lcom/google/android/c/b/ba;

    .line 48
    const v1, 0x8001

    invoke-static {v0}, Lcom/google/android/c/b/ba;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/an;->a(I[B)V

    .line 49
    return-void
.end method

.method protected final a(ILjava/nio/ByteBuffer;)V
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/an;->a:Lcom/google/android/gms/car/senderprotocol/ao;

    if-nez v0, :cond_0

    .line 86
    :goto_0
    return-void

    .line 79
    :cond_0
    const v0, 0x8002

    if-ne p1, v0, :cond_1

    .line 80
    new-instance v0, Lcom/google/android/c/b/an;

    invoke-direct {v0}, Lcom/google/android/c/b/an;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/an;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/c/b/an;

    .line 82
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/an;->a:Lcom/google/android/gms/car/senderprotocol/ao;

    iget v0, v0, Lcom/google/android/c/b/an;->a:I

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/senderprotocol/ao;->a(I)V

    goto :goto_0

    .line 84
    :cond_1
    const-string v0, "CAR.INST"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid message type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;II)V
    .locals 2

    .prologue
    .line 53
    new-instance v0, Lcom/google/android/c/b/az;

    invoke-direct {v0}, Lcom/google/android/c/b/az;-><init>()V

    .line 54
    if-eqz p1, :cond_0

    .line 55
    invoke-virtual {v0, p1}, Lcom/google/android/c/b/az;->a(Ljava/lang/String;)Lcom/google/android/c/b/az;

    .line 57
    :cond_0
    if-eqz p2, :cond_1

    .line 58
    invoke-virtual {v0, p2}, Lcom/google/android/c/b/az;->b(Ljava/lang/String;)Lcom/google/android/c/b/az;

    .line 60
    :cond_1
    if-eqz p3, :cond_2

    .line 61
    invoke-virtual {v0, p3}, Lcom/google/android/c/b/az;->c(Ljava/lang/String;)Lcom/google/android/c/b/az;

    .line 63
    :cond_2
    if-eqz p4, :cond_3

    .line 64
    invoke-virtual {v0, p4}, Lcom/google/android/c/b/az;->a([B)Lcom/google/android/c/b/az;

    .line 66
    :cond_3
    if-eqz p5, :cond_4

    .line 67
    invoke-virtual {v0, p5}, Lcom/google/android/c/b/az;->d(Ljava/lang/String;)Lcom/google/android/c/b/az;

    .line 69
    :cond_4
    invoke-virtual {v0, p6}, Lcom/google/android/c/b/az;->a(I)Lcom/google/android/c/b/az;

    .line 70
    invoke-virtual {v0, p7}, Lcom/google/android/c/b/az;->b(I)Lcom/google/android/c/b/az;

    .line 71
    const v1, 0x8003

    invoke-static {v0}, Lcom/google/android/c/b/ba;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/an;->a(I[B)V

    .line 72
    return-void
.end method
