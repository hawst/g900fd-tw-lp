.class public final Lcom/google/android/gms/cast/internal/p;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/Object;

.field private static final f:Lcom/google/android/gms/cast/internal/k;


# instance fields
.field private b:J

.field private c:J

.field private d:J

.field private e:Lcom/google/android/gms/cast/internal/o;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/gms/cast/internal/k;

    const-string v1, "RequestTracker"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/internal/k;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/cast/internal/p;->f:Lcom/google/android/gms/cast/internal/k;

    .line 27
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/cast/internal/p;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(J)V
    .locals 3

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-wide p1, p0, Lcom/google/android/gms/cast/internal/p;->b:J

    .line 31
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/cast/internal/p;->c:J

    .line 32
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/cast/internal/p;->d:J

    .line 33
    return-void
.end method

.method private a(I)Z
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 137
    .line 139
    const-wide/16 v0, 0x0

    .line 141
    sget-object v6, Lcom/google/android/gms/cast/internal/p;->a:Ljava/lang/Object;

    monitor-enter v6

    .line 142
    :try_start_0
    iget-wide v8, p0, Lcom/google/android/gms/cast/internal/p;->c:J

    const-wide/16 v10, -0x1

    cmp-long v2, v8, v10

    if-eqz v2, :cond_1

    .line 143
    sget-object v0, Lcom/google/android/gms/cast/internal/p;->f:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "clearing request %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-wide v8, p0, Lcom/google/android/gms/cast/internal/p;->c:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v2, v7

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 144
    iget-object v2, p0, Lcom/google/android/gms/cast/internal/p;->e:Lcom/google/android/gms/cast/internal/o;

    .line 145
    iget-wide v0, p0, Lcom/google/android/gms/cast/internal/p;->c:J

    .line 146
    invoke-direct {p0}, Lcom/google/android/gms/cast/internal/p;->d()V

    .line 149
    :goto_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    if-eqz v2, :cond_0

    .line 152
    invoke-interface {v2, v0, v1, v5, v3}, Lcom/google/android/gms/cast/internal/o;->a(JILorg/json/JSONObject;)V

    .line 155
    :cond_0
    return v4

    .line 149
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_1
    move-object v2, v3

    move v4, v5

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 68
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/cast/internal/p;->c:J

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/internal/p;->e:Lcom/google/android/gms/cast/internal/o;

    .line 70
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/cast/internal/p;->d:J

    .line 71
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 60
    sget-object v1, Lcom/google/android/gms/cast/internal/p;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 61
    :try_start_0
    iget-wide v2, p0, Lcom/google/android/gms/cast/internal/p;->c:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 62
    invoke-direct {p0}, Lcom/google/android/gms/cast/internal/p;->d()V

    .line 64
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(JLcom/google/android/gms/cast/internal/o;)V
    .locals 5

    .prologue
    .line 39
    sget-object v1, Lcom/google/android/gms/cast/internal/p;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 43
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/p;->e:Lcom/google/android/gms/cast/internal/o;

    .line 44
    iget-wide v2, p0, Lcom/google/android/gms/cast/internal/p;->c:J

    .line 46
    iput-wide p1, p0, Lcom/google/android/gms/cast/internal/p;->c:J

    .line 47
    iput-object p3, p0, Lcom/google/android/gms/cast/internal/p;->e:Lcom/google/android/gms/cast/internal/o;

    .line 48
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/cast/internal/p;->d:J

    .line 49
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(J)Z
    .locals 7

    .prologue
    .line 86
    sget-object v1, Lcom/google/android/gms/cast/internal/p;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 87
    :try_start_0
    iget-wide v2, p0, Lcom/google/android/gms/cast/internal/p;->c:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    iget-wide v2, p0, Lcom/google/android/gms/cast/internal/p;->c:J

    cmp-long v0, v2, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(JI)Z
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/gms/cast/internal/p;->a(JILorg/json/JSONObject;)Z

    move-result v0

    return v0
.end method

.method public final a(JILorg/json/JSONObject;)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 105
    .line 106
    const/4 v0, 0x0

    .line 108
    sget-object v3, Lcom/google/android/gms/cast/internal/p;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 109
    :try_start_0
    iget-wide v4, p0, Lcom/google/android/gms/cast/internal/p;->c:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    iget-wide v4, p0, Lcom/google/android/gms/cast/internal/p;->c:J

    cmp-long v4, v4, p1

    if-nez v4, :cond_1

    .line 111
    sget-object v0, Lcom/google/android/gms/cast/internal/p;->f:Lcom/google/android/gms/cast/internal/k;

    const-string v2, "request %d completed"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-wide v6, p0, Lcom/google/android/gms/cast/internal/p;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v2, v4}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/p;->e:Lcom/google/android/gms/cast/internal/o;

    .line 113
    invoke-direct {p0}, Lcom/google/android/gms/cast/internal/p;->d()V

    .line 116
    :goto_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    if-eqz v0, :cond_0

    .line 119
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/gms/cast/internal/o;->a(JILorg/json/JSONObject;)V

    .line 122
    :cond_0
    return v1

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public final b()Z
    .locals 6

    .prologue
    .line 77
    sget-object v1, Lcom/google/android/gms/cast/internal/p;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 78
    :try_start_0
    iget-wide v2, p0, Lcom/google/android/gms/cast/internal/p;->c:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(JI)Z
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 166
    .line 168
    const-wide/16 v0, 0x0

    .line 170
    sget-object v5, Lcom/google/android/gms/cast/internal/p;->a:Ljava/lang/Object;

    monitor-enter v5

    .line 171
    :try_start_0
    iget-wide v6, p0, Lcom/google/android/gms/cast/internal/p;->c:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-eqz v6, :cond_1

    iget-wide v6, p0, Lcom/google/android/gms/cast/internal/p;->d:J

    sub-long v6, p1, v6

    iget-wide v8, p0, Lcom/google/android/gms/cast/internal/p;->b:J

    cmp-long v6, v6, v8

    if-ltz v6, :cond_1

    .line 173
    sget-object v0, Lcom/google/android/gms/cast/internal/p;->f:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "request %d timed out"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-wide v8, p0, Lcom/google/android/gms/cast/internal/p;->c:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 174
    iget-wide v0, p0, Lcom/google/android/gms/cast/internal/p;->c:J

    .line 175
    iget-object v2, p0, Lcom/google/android/gms/cast/internal/p;->e:Lcom/google/android/gms/cast/internal/o;

    .line 176
    invoke-direct {p0}, Lcom/google/android/gms/cast/internal/p;->d()V

    .line 179
    :goto_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    if-eqz v2, :cond_0

    .line 182
    invoke-interface {v2, v0, v1, p3, v3}, Lcom/google/android/gms/cast/internal/o;->a(JILorg/json/JSONObject;)V

    .line 185
    :cond_0
    return v4

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    :cond_1
    move v4, v2

    move-object v2, v3

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/internal/p;->a(I)Z

    move-result v0

    return v0
.end method
