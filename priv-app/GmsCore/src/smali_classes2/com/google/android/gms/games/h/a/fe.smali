.class public final Lcom/google/android/gms/games/h/a/fe;
.super Lcom/google/android/gms/common/server/response/c;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/HashMap;


# instance fields
.field private final c:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 65
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 67
    sput-object v0, Lcom/google/android/gms/games/h/a/fe;->b:Ljava/util/HashMap;

    const-string v1, "adId"

    const-string v2, "adId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/google/android/gms/games/h/a/fe;->b:Ljava/util/HashMap;

    const-string v1, "clientReportTimeMillis"

    const-string v2, "clientReportTimeMillis"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->c(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v0, Lcom/google/android/gms/games/h/a/fe;->b:Ljava/util/HashMap;

    const-string v1, "instanceId"

    const-string v2, "instanceId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sget-object v0, Lcom/google/android/gms/games/h/a/fe;->b:Ljava/util/HashMap;

    const-string v1, "limitAdTracking"

    const-string v2, "limitAdTracking"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lcom/google/android/gms/games/h/a/fe;->b:Ljava/util/HashMap;

    const-string v1, "requestId"

    const-string v2, "requestId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->c(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcom/google/android/gms/games/h/a/fe;->b:Ljava/util/HashMap;

    const-string v1, "sessions"

    const-string v2, "sessions"

    const-class v3, Lcom/google/android/gms/games/h/a/fd;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 111
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/h/a/fe;->c:Ljava/util/HashMap;

    .line 81
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 111
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/h/a/fe;->c:Ljava/util/HashMap;

    .line 91
    if-eqz p1, :cond_0

    .line 92
    const-string v0, "adId"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/h/a/fe;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :cond_0
    if-eqz p2, :cond_1

    .line 95
    const-string v0, "clientReportTimeMillis"

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/games/h/a/fe;->a(Ljava/lang/String;J)V

    .line 97
    :cond_1
    if-eqz p3, :cond_2

    .line 98
    const-string v0, "instanceId"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/games/h/a/fe;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :cond_2
    if-eqz p4, :cond_3

    .line 101
    const-string v0, "limitAdTracking"

    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/h/a/fe;->a(Ljava/lang/String;Z)V

    .line 103
    :cond_3
    if-eqz p5, :cond_4

    .line 104
    const-string v0, "requestId"

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/games/h/a/fe;->a(Ljava/lang/String;J)V

    .line 106
    :cond_4
    if-eqz p6, :cond_5

    .line 107
    const-string v0, "sessions"

    invoke-virtual {p0, v0, p6}, Lcom/google/android/gms/games/h/a/fe;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 109
    :cond_5
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/google/android/gms/games/h/a/fe;->b:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/fe;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    return-void
.end method

.method protected final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/fe;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final getSessions()Ljava/util/ArrayList;
    .locals 2
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/fe;->c:Ljava/util/HashMap;

    const-string v1, "sessions"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method
