.class final Lcom/google/android/gms/common/internal/c;
.super Lcom/google/android/gms/common/internal/bh;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/internal/bg;

.field final synthetic b:Lcom/google/android/gms/common/internal/GetServiceRequest;

.field final synthetic c:Lcom/google/android/gms/common/internal/b;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/internal/b;Lcom/google/android/gms/common/internal/bg;Lcom/google/android/gms/common/internal/GetServiceRequest;)V
    .locals 0

    .prologue
    .line 533
    iput-object p1, p0, Lcom/google/android/gms/common/internal/c;->c:Lcom/google/android/gms/common/internal/b;

    iput-object p2, p0, Lcom/google/android/gms/common/internal/c;->a:Lcom/google/android/gms/common/internal/bg;

    iput-object p3, p0, Lcom/google/android/gms/common/internal/c;->b:Lcom/google/android/gms/common/internal/GetServiceRequest;

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/bh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 545
    iget-object v2, p0, Lcom/google/android/gms/common/internal/c;->c:Lcom/google/android/gms/common/internal/b;

    iget-object v2, v2, Lcom/google/android/gms/common/internal/b;->c:Lcom/google/android/gms/common/internal/cb;

    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 546
    iget-object v2, p0, Lcom/google/android/gms/common/internal/c;->c:Lcom/google/android/gms/common/internal/b;

    iget-object v2, p0, Lcom/google/android/gms/common/internal/c;->b:Lcom/google/android/gms/common/internal/GetServiceRequest;

    iget v2, v2, Lcom/google/android/gms/common/internal/GetServiceRequest;->b:I

    packed-switch v2, :pswitch_data_0

    move v2, v1

    :goto_0
    packed-switch v2, :pswitch_data_1

    move v0, v1

    :pswitch_0
    if-eqz v0, :cond_0

    .line 547
    iget-object v0, p0, Lcom/google/android/gms/common/internal/c;->c:Lcom/google/android/gms/common/internal/b;

    iget-object v0, v0, Lcom/google/android/gms/common/internal/b;->c:Lcom/google/android/gms/common/internal/cb;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/c;->c:Lcom/google/android/gms/common/internal/b;

    iget-object v1, v1, Lcom/google/android/gms/common/internal/b;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/common/internal/cb;->a(Landroid/content/Context;I)V

    .line 549
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/c;->a:Lcom/google/android/gms/common/internal/bg;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    .line 550
    return-void

    .line 546
    :pswitch_1
    const/4 v2, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v2, 0x3

    goto :goto_0

    :pswitch_3
    move v2, v0

    goto :goto_0

    :pswitch_4
    const/4 v2, 0x4

    goto :goto_0

    :pswitch_5
    const/4 v2, 0x5

    goto :goto_0

    :pswitch_6
    const/4 v2, 0x6

    goto :goto_0

    :pswitch_7
    const/4 v2, 0x7

    goto :goto_0

    :pswitch_8
    const/16 v2, 0x8

    goto :goto_0

    :pswitch_9
    const/16 v2, 0x9

    goto :goto_0

    :pswitch_a
    const/16 v2, 0xa

    goto :goto_0

    :pswitch_b
    const/16 v2, 0xb

    goto :goto_0

    :pswitch_c
    const/16 v2, 0xc

    goto :goto_0

    :pswitch_d
    const/16 v2, 0xd

    goto :goto_0

    :pswitch_e
    const/16 v2, 0xe

    goto :goto_0

    :pswitch_f
    const/16 v2, 0xf

    goto :goto_0

    :pswitch_10
    const/16 v2, 0x10

    goto :goto_0

    :pswitch_11
    const/16 v2, 0x11

    goto :goto_0

    :pswitch_12
    const/16 v2, 0x12

    goto :goto_0

    :pswitch_13
    const/16 v2, 0x13

    goto :goto_0

    :pswitch_14
    const/16 v2, 0x14

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final asBinder()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 539
    iget-object v0, p0, Lcom/google/android/gms/common/internal/c;->a:Lcom/google/android/gms/common/internal/bg;

    invoke-interface {v0}, Lcom/google/android/gms/common/internal/bg;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method
