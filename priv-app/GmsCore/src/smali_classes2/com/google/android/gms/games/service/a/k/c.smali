.class public final Lcom/google/android/gms/games/service/a/k/c;
.super Lcom/google/android/gms/games/service/a/c;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/games/service/h;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/google/android/gms/games/h/a/ep;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/service/h;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/h/a/ep;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/a/c;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 23
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/k/c;->b:Lcom/google/android/gms/games/service/h;

    .line 24
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/k/c;->c:Ljava/lang/String;

    .line 25
    iput-object p4, p0, Lcom/google/android/gms/games/service/a/k/c;->d:Ljava/lang/String;

    .line 26
    iput-object p5, p0, Lcom/google/android/gms/games/service/a/k/c;->e:Lcom/google/android/gms/games/h/a/ep;

    .line 27
    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k/c;->b:Lcom/google/android/gms/games/service/h;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k/c;->b:Lcom/google/android/gms/games/service/h;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/k/c;->c:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/games/service/h;->a(ILjava/lang/String;)V

    .line 41
    :cond_0
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)I
    .locals 6

    .prologue
    .line 32
    iget-object v2, p0, Lcom/google/android/gms/games/service/a/k/c;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Lcom/google/android/gms/games/service/a/k/c;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/games/service/a/k/c;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/games/service/a/k/c;->e:Lcom/google/android/gms/games/h/a/ep;

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/t;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/h/a/ep;)I

    move-result v0

    return v0
.end method
