.class final Lcom/google/android/gms/backup/b/c;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/io/InputStream;

.field final synthetic b:Lcom/google/android/gms/drive/b/a/i;

.field final synthetic c:Ljava/io/PipedOutputStream;

.field final synthetic d:Lcom/google/android/gms/backup/b/d;


# direct methods
.method constructor <init>(Ljava/io/InputStream;Lcom/google/android/gms/drive/b/a/i;Ljava/io/PipedOutputStream;Lcom/google/android/gms/backup/b/d;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/gms/backup/b/c;->a:Ljava/io/InputStream;

    iput-object p2, p0, Lcom/google/android/gms/backup/b/c;->b:Lcom/google/android/gms/drive/b/a/i;

    iput-object p3, p0, Lcom/google/android/gms/backup/b/c;->c:Ljava/io/PipedOutputStream;

    iput-object p4, p0, Lcom/google/android/gms/backup/b/c;->d:Lcom/google/android/gms/backup/b/d;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 51
    :try_start_0
    new-instance v0, Lcom/google/c/c/a/a/f;

    invoke-direct {v0}, Lcom/google/c/c/a/a/f;-><init>()V

    iget-object v4, p0, Lcom/google/android/gms/backup/b/c;->a:Ljava/io/InputStream;

    iput-object v4, v0, Lcom/google/c/c/a/a/f;->c:Ljava/io/InputStream;

    iget-object v4, p0, Lcom/google/android/gms/backup/b/c;->b:Lcom/google/android/gms/drive/b/a/i;

    iput-object v4, v0, Lcom/google/c/c/a/a/f;->b:Lcom/google/android/gms/drive/b/a/i;

    iget-object v4, v0, Lcom/google/c/c/a/a/f;->b:Lcom/google/android/gms/drive/b/a/i;

    invoke-static {v4}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, v0, Lcom/google/c/c/a/a/f;->c:Ljava/io/InputStream;

    invoke-static {v4}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Lcom/google/c/c/a/a/b;

    new-instance v5, Lcom/google/c/c/a/a/h;

    iget v6, v0, Lcom/google/c/c/a/a/f;->d:I

    invoke-direct {v5, v6}, Lcom/google/c/c/a/a/h;-><init>(I)V

    iget-object v6, v0, Lcom/google/c/c/a/a/f;->a:Lcom/google/android/gms/drive/b/a/n;

    invoke-direct {v4, v5, v6}, Lcom/google/c/c/a/a/b;-><init>(Lcom/google/c/c/a/a/d;Lcom/google/android/gms/drive/b/a/n;)V

    new-instance v5, Lcom/google/c/c/a/a/e;

    iget-object v6, v0, Lcom/google/c/c/a/a/f;->c:Ljava/io/InputStream;

    iget-object v0, v0, Lcom/google/c/c/a/a/f;->b:Lcom/google/android/gms/drive/b/a/i;

    invoke-direct {v5, v4, v6, v0}, Lcom/google/c/c/a/a/e;-><init>(Lcom/google/c/c/a/a/b;Ljava/io/InputStream;Lcom/google/android/gms/drive/b/a/i;)V

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/backup/b/c;->c:Ljava/io/PipedOutputStream;

    iget-object v4, v5, Lcom/google/c/c/a/a/e;->a:Lcom/google/c/c/a/a/b;

    iget-object v6, v5, Lcom/google/c/c/a/a/e;->c:Ljava/io/InputStream;

    iget-object v7, v5, Lcom/google/c/c/a/a/e;->b:Lcom/google/android/gms/drive/b/a/i;

    iget-object v5, v4, Lcom/google/c/c/a/a/b;->b:Lcom/google/c/c/a/a/d;

    invoke-interface {v5, v0}, Lcom/google/c/c/a/a/d;->a(Ljava/io/OutputStream;)Lcom/google/c/c/a/a/c;

    move-result-object v8

    iget-object v0, v4, Lcom/google/c/c/a/a/b;->c:Lcom/google/android/gms/drive/b/a/n;

    iget v5, v7, Lcom/google/android/gms/drive/b/a/i;->b:I

    invoke-interface {v0, v5}, Lcom/google/android/gms/drive/b/a/n;->a(I)Lcom/google/android/gms/drive/b/a/m;

    move-result-object v9

    iget v0, v4, Lcom/google/c/c/a/a/b;->a:I

    new-array v10, v0, [B

    :cond_0
    invoke-virtual {v6, v10}, Ljava/io/InputStream;->read([B)I

    move-result v11

    const/4 v0, -0x1

    if-eq v11, v0, :cond_6

    move v5, v2

    :goto_0
    if-ge v5, v11, :cond_0

    aget-byte v0, v10, v5

    invoke-interface {v9, v0}, Lcom/google/android/gms/drive/b/a/m;->a(B)V

    invoke-interface {v9}, Lcom/google/android/gms/drive/b/a/m;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v9}, Lcom/google/android/gms/drive/b/a/m;->f()B

    move-result v0

    invoke-interface {v8, v0}, Lcom/google/c/c/a/a/c;->a(B)V

    :cond_1
    invoke-interface {v9}, Lcom/google/android/gms/drive/b/a/m;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, v7, Lcom/google/android/gms/drive/b/a/i;->a:Lcom/google/android/gms/drive/b/a/d;

    invoke-interface {v9}, Lcom/google/android/gms/drive/b/a/m;->d()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/gms/drive/b/a/d;->b(I)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, v7, Lcom/google/android/gms/drive/b/a/i;->a:Lcom/google/android/gms/drive/b/a/d;

    invoke-interface {v9}, Lcom/google/android/gms/drive/b/a/m;->d()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/gms/drive/b/a/d;->a(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    move-object v4, v3

    :cond_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/a/c;

    if-nez v4, :cond_3

    invoke-interface {v9}, Lcom/google/android/gms/drive/b/a/m;->e()Ljava/lang/String;

    move-result-object v4

    :cond_3
    iget-object v13, v0, Lcom/google/android/gms/drive/b/a/c;->b:Ljava/lang/String;

    invoke-virtual {v13, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    :goto_1
    if-eqz v0, :cond_4

    iget-wide v12, v0, Lcom/google/android/gms/drive/b/a/c;->a:J

    iget v0, v7, Lcom/google/android/gms/drive/b/a/i;->b:I

    invoke-interface {v8, v12, v13, v0}, Lcom/google/c/c/a/a/c;->a(JI)V

    invoke-interface {v9}, Lcom/google/android/gms/drive/b/a/m;->c()V

    :cond_4
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    :cond_5
    move-object v0, v3

    goto :goto_1

    :cond_6
    invoke-interface {v9}, Lcom/google/android/gms/drive/b/a/m;->h()I

    move-result v0

    if-eqz v0, :cond_9

    iget v3, v7, Lcom/google/android/gms/drive/b/a/i;->c:I

    if-eqz v3, :cond_7

    iget v3, v7, Lcom/google/android/gms/drive/b/a/i;->c:I

    invoke-interface {v9, v3}, Lcom/google/android/gms/drive/b/a/m;->a(I)I

    move-result v3

    iget-object v4, v7, Lcom/google/android/gms/drive/b/a/i;->d:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v3, v4, :cond_7

    iget v3, v7, Lcom/google/android/gms/drive/b/a/i;->c:I

    invoke-interface {v9, v3}, Lcom/google/android/gms/drive/b/a/m;->b(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v7, Lcom/google/android/gms/drive/b/a/i;->e:Lcom/google/android/gms/drive/b/a/c;

    iget-object v4, v4, Lcom/google/android/gms/drive/b/a/c;->b:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    move v3, v1

    :goto_2
    if-eqz v3, :cond_a

    iget v3, v7, Lcom/google/android/gms/drive/b/a/i;->c:I

    sub-int/2addr v0, v3

    move v14, v1

    move v1, v0

    move v0, v14

    :goto_3
    if-ge v2, v1, :cond_8

    invoke-interface {v9, v2}, Lcom/google/android/gms/drive/b/a/m;->c(I)B

    move-result v3

    invoke-interface {v8, v3}, Lcom/google/c/c/a/a/c;->a(B)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_7
    move v3, v2

    goto :goto_2

    :cond_8
    if-eqz v0, :cond_9

    iget-object v0, v7, Lcom/google/android/gms/drive/b/a/i;->e:Lcom/google/android/gms/drive/b/a/c;

    iget-wide v0, v0, Lcom/google/android/gms/drive/b/a/c;->a:J

    iget v2, v7, Lcom/google/android/gms/drive/b/a/i;->c:I

    invoke-interface {v8, v0, v1, v2}, Lcom/google/c/c/a/a/c;->a(JI)V

    :cond_9
    invoke-interface {v8}, Lcom/google/c/c/a/a/c;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/backup/b/c;->c:Ljava/io/PipedOutputStream;

    invoke-virtual {v0}, Ljava/io/PipedOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 66
    :goto_4
    return-void

    .line 54
    :catch_0
    move-exception v0

    .line 55
    :try_start_2
    const-string v1, "DiffBuilder"

    const-string v2, "Exception "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 59
    iget-object v1, p0, Lcom/google/android/gms/backup/b/c;->d:Lcom/google/android/gms/backup/b/d;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/backup/b/d;->a(Ljava/io/IOException;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 62
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/backup/b/c;->c:Ljava/io/PipedOutputStream;

    invoke-virtual {v0}, Ljava/io/PipedOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_4

    .line 66
    :catch_1
    move-exception v0

    goto :goto_4

    .line 61
    :catchall_0
    move-exception v0

    .line 62
    :try_start_4
    iget-object v1, p0, Lcom/google/android/gms/backup/b/c;->c:Ljava/io/PipedOutputStream;

    invoke-virtual {v1}, Ljava/io/PipedOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 65
    :goto_5
    throw v0

    :catch_2
    move-exception v1

    goto :goto_5

    .line 66
    :catch_3
    move-exception v0

    goto :goto_4

    :cond_a
    move v1, v0

    move v0, v2

    goto :goto_3
.end method
