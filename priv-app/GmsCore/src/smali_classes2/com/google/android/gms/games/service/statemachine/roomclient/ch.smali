.class public final Lcom/google/android/gms/games/service/statemachine/roomclient/ch;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/games/g/aj;

.field final b:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 279
    new-instance v0, Lcom/google/android/gms/games/g/aj;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/aj;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ch;->a:Lcom/google/android/gms/games/g/aj;

    .line 281
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ch;->b:Ljava/util/ArrayList;

    .line 284
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ch;->a:Lcom/google/android/gms/games/g/aj;

    invoke-static {p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/g/aj;->a:Ljava/lang/String;

    .line 285
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ch;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0x100

    if-ge v0, v1, :cond_0

    .line 305
    new-instance v0, Lcom/google/android/gms/games/g/ak;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/ak;-><init>()V

    .line 306
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/gms/games/g/ak;->b:J

    .line 307
    iput p1, v0, Lcom/google/android/gms/games/g/ak;->a:I

    .line 308
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ch;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 310
    :cond_0
    return-void
.end method
