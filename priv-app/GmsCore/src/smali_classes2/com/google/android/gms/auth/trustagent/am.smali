.class public final Lcom/google/android/gms/auth/trustagent/am;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Ljava/lang/String;

.field final c:Lcom/google/android/gms/auth/trustagent/aq;

.field d:Landroid/os/AsyncTask;

.field public e:Landroid/os/Bundle;

.field f:Lcom/google/android/gms/auth/trustagent/q;

.field final g:Ljava/lang/Object;

.field h:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/auth/trustagent/aq;)V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/am;->g:Ljava/lang/Object;

    .line 64
    new-instance v0, Lcom/google/android/gms/auth/trustagent/an;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/trustagent/an;-><init>(Lcom/google/android/gms/auth/trustagent/am;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/am;->h:Landroid/content/ServiceConnection;

    .line 81
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/am;->a:Landroid/content/Context;

    .line 82
    iput-object p2, p0, Lcom/google/android/gms/auth/trustagent/am;->b:Ljava/lang/String;

    .line 83
    iput-object p3, p0, Lcom/google/android/gms/auth/trustagent/am;->c:Lcom/google/android/gms/auth/trustagent/aq;

    .line 84
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/am;->e:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 184
    new-instance v0, Lcom/google/android/gms/auth/trustagent/ap;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/android/gms/auth/trustagent/ap;-><init>(Lcom/google/android/gms/auth/trustagent/am;Ljava/lang/Object;Ljava/lang/String;)V

    .line 218
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 219
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/am;->e:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 129
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 130
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/am;->e:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/am;->e:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/am;->d:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/am;->d:Landroid/os/AsyncTask;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 153
    :cond_0
    new-instance v0, Lcom/google/android/gms/auth/trustagent/ao;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/trustagent/ao;-><init>(Lcom/google/android/gms/auth/trustagent/am;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/am;->d:Landroid/os/AsyncTask;

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/am;->d:Landroid/os/AsyncTask;

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 181
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/am;->e:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 135
    return-void
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/am;->e:Landroid/os/Bundle;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/am;->e:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 147
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/am;->e:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 146
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method
