.class public final Lcom/google/android/gms/car/do;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/Object;

.field private static volatile b:Lcom/google/android/gms/car/do;


# instance fields
.field private final c:Ljava/lang/Object;

.field private final d:Landroid/content/Context;

.field private e:Ljava/lang/ref/WeakReference;

.field private f:Lcom/google/android/gms/car/nj;

.field private final g:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 459
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/do;->a:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 483
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/do;->c:Ljava/lang/Object;

    .line 570
    new-instance v0, Lcom/google/android/gms/car/dp;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/dp;-><init>(Lcom/google/android/gms/car/do;)V

    iput-object v0, p0, Lcom/google/android/gms/car/do;->g:Landroid/content/ServiceConnection;

    .line 490
    iput-object p1, p0, Lcom/google/android/gms/car/do;->d:Landroid/content/Context;

    .line 491
    invoke-direct {p0}, Lcom/google/android/gms/car/do;->c()V

    .line 492
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/car/do;
    .locals 3

    .prologue
    .line 463
    sget-object v1, Lcom/google/android/gms/car/do;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 464
    :try_start_0
    sget-object v0, Lcom/google/android/gms/car/do;->b:Lcom/google/android/gms/car/do;

    if-nez v0, :cond_2

    .line 465
    const-string v0, "CAR.TEL.PhoneAdapter"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 466
    const-string v0, "CAR.TEL.PhoneAdapter"

    const-string v2, "Creating a new PhoneAdapter instance"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    :cond_0
    new-instance v0, Lcom/google/android/gms/car/do;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/do;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/car/do;->b:Lcom/google/android/gms/car/do;

    .line 474
    :cond_1
    :goto_0
    sget-object v0, Lcom/google/android/gms/car/do;->b:Lcom/google/android/gms/car/do;

    monitor-exit v1

    return-object v0

    .line 470
    :cond_2
    const-string v0, "CAR.TEL.PhoneAdapter"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 471
    const-string v0, "CAR.TEL.PhoneAdapter"

    const-string v2, "Returning an existing PhoneAdapter instance."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 475
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/car/do;)Lcom/google/android/gms/car/nj;
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/google/android/gms/car/do;->f:Lcom/google/android/gms/car/nj;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/car/do;Lcom/google/android/gms/car/nj;)Lcom/google/android/gms/car/nj;
    .locals 0

    .prologue
    .line 456
    iput-object p1, p0, Lcom/google/android/gms/car/do;->f:Lcom/google/android/gms/car/nj;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/gms/car/do;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/google/android/gms/car/do;->e:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    .line 561
    const-string v0, "CAR.TEL.PhoneAdapter"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 562
    const-string v0, "CAR.TEL.PhoneAdapter"

    const-string v1, "tryBindToInCallService"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/car/do;->d:Landroid/content/Context;

    const-class v2, Lcom/google/android/gms/car/InCallServiceImpl;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 565
    const-string v1, "local_bind"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 566
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/do;->d:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/car/do;->g:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 568
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 507
    const-string v0, "CAR.TEL.PhoneAdapter"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 508
    const-string v1, "CAR.TEL.PhoneAdapter"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "onPhoneDestroyed: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " Listener: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/car/do;->e:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_2

    const-string v0, "null"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/do;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 512
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/do;->e:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/do;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 513
    iget-object v0, p0, Lcom/google/android/gms/car/do;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/dq;

    invoke-interface {v0}, Lcom/google/android/gms/car/dq;->a()V

    .line 515
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 508
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/do;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 515
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Landroid/telecom/Phone;)V
    .locals 3

    .prologue
    .line 495
    const-string v0, "CAR.TEL.PhoneAdapter"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496
    const-string v1, "CAR.TEL.PhoneAdapter"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "onPhoneCreated: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " Listener: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/car/do;->e:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_2

    const-string v0, "null"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/do;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 500
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/do;->e:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/do;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 501
    iget-object v0, p0, Lcom/google/android/gms/car/do;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/dq;

    invoke-interface {v0, p1}, Lcom/google/android/gms/car/dq;->a(Landroid/telecom/Phone;)V

    .line 503
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 496
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/do;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 503
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/car/dq;)V
    .locals 3

    .prologue
    .line 519
    const-string v0, "CAR.TEL.PhoneAdapter"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 520
    const-string v0, "CAR.TEL.PhoneAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setListener: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 522
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/car/do;->c()V

    .line 523
    iget-object v1, p0, Lcom/google/android/gms/car/do;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 524
    :try_start_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/car/do;->e:Ljava/lang/ref/WeakReference;

    .line 525
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()Landroid/telecom/Phone;
    .locals 3

    .prologue
    const/4 v1, 0x3

    .line 543
    iget-object v0, p0, Lcom/google/android/gms/car/do;->f:Lcom/google/android/gms/car/nj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/do;->f:Lcom/google/android/gms/car/nj;

    invoke-virtual {v0}, Lcom/google/android/gms/car/nj;->isBinderAlive()Z

    move-result v0

    if-nez v0, :cond_2

    .line 544
    :cond_0
    const-string v0, "CAR.TEL.PhoneAdapter"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 545
    const-string v0, "CAR.TEL.PhoneAdapter"

    const-string v1, "getActivePhone. InCallService is null or dead."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    :cond_1
    const/4 v0, 0x0

    .line 552
    :goto_0
    return-object v0

    .line 549
    :cond_2
    const-string v0, "CAR.TEL.PhoneAdapter"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 550
    const-string v0, "CAR.TEL.PhoneAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getActivePhone "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/do;->f:Lcom/google/android/gms/car/nj;

    invoke-virtual {v2}, Lcom/google/android/gms/car/nj;->a()Lcom/google/android/gms/car/InCallServiceImpl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/car/InCallServiceImpl;->getPhone()Landroid/telecom/Phone;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/do;->f:Lcom/google/android/gms/car/nj;

    invoke-virtual {v0}, Lcom/google/android/gms/car/nj;->a()Lcom/google/android/gms/car/InCallServiceImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/InCallServiceImpl;->getPhone()Landroid/telecom/Phone;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/car/dq;)V
    .locals 3

    .prologue
    .line 529
    const-string v0, "CAR.TEL.PhoneAdapter"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 530
    const-string v0, "CAR.TEL.PhoneAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "removeListener: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 532
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/do;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 533
    if-eqz p1, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/do;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 534
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/do;->e:Ljava/lang/ref/WeakReference;

    .line 536
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
