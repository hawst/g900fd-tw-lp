.class public Lcom/google/android/gms/auth/trustagent/trustlet/d;
.super Lcom/google/android/gms/auth/trustagent/trustlet/bh;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/auth/trustagent/trustlet/c;


# instance fields
.field private a:Lcom/google/android/gms/auth/trustagent/trustlet/a;

.field private final b:Ljava/util/Set;

.field private c:Lcom/google/android/gms/auth/trustagent/d;

.field private d:Landroid/content/SharedPreferences;

.field private e:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;-><init>()V

    .line 50
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->b:Ljava/util/Set;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/trustlet/d;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->d:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private static c(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;
    .locals 3

    .prologue
    .line 173
    :try_start_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 176
    :goto_0
    return-object v0

    .line 175
    :catch_0
    move-exception v0

    const-string v0, "Coffee - BluetoothTrustlet"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal Bluetooth address in preferences: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 193
    new-instance v0, Lcom/google/android/gms/auth/e/f;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/e/f;-><init>(Landroid/content/Context;)V

    .line 194
    iput v2, v0, Lcom/google/android/gms/auth/e/f;->d:I

    .line 195
    if-eqz p1, :cond_0

    .line 196
    iput v2, v0, Lcom/google/android/gms/auth/e/f;->f:I

    .line 200
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    iput v1, v0, Lcom/google/android/gms/auth/e/f;->g:I

    .line 201
    invoke-virtual {v0}, Lcom/google/android/gms/auth/e/f;->a()V

    .line 202
    return-void

    .line 198
    :cond_0
    const/4 v1, 0x2

    iput v1, v0, Lcom/google/android/gms/auth/e/f;->f:I

    goto :goto_0
.end method

.method private f()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 103
    .line 104
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 105
    iget-object v3, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->a:Lcom/google/android/gms/auth/trustagent/trustlet/a;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/a;->a(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 111
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->g:Z

    if-eqz v2, :cond_2

    if-nez v0, :cond_2

    .line 112
    const-string v0, "Coffee - BluetoothTrustlet"

    const-string v2, "No trusted connected device, revoking trust."

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/d;->e(Ljava/lang/String;)V

    .line 125
    :cond_1
    :goto_1
    return-void

    .line 116
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->g:Z

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    .line 117
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/p;->cv:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 120
    const-string v2, "Coffee - BluetoothTrustlet"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Enabling trust, connected to trusted device: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method private h()V
    .locals 5

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 129
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 130
    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 131
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->d:Landroid/content/SharedPreferences;

    const/4 v4, 0x0

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/trustagent/trustlet/d;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 135
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/d;->f()V

    .line 218
    return-void
.end method

.method final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 139
    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/trustlet/d;->c(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    .line 140
    if-nez v2, :cond_0

    .line 154
    :goto_0
    return-void

    .line 142
    :cond_0
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v0

    const/16 v3, 0xc

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_1
    if-nez v0, :cond_2

    .line 143
    const-string v0, "Coffee - BluetoothTrustlet"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Adding a non bonded Bluetooth device "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ignored."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 142
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 147
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->b:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 148
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v0, v1, :cond_3

    .line 150
    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/d;->b(Z)V

    .line 152
    :cond_3
    invoke-direct {p0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/d;->c(Z)V

    .line 153
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/d;->f()V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 206
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/d;->h()V

    .line 211
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/d;->f()V

    .line 213
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ar;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->d:Landroid/content/SharedPreferences;

    .line 61
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/e;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/d;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->e:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->d:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->e:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 80
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/a;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/auth/trustagent/trustlet/c;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->a:Lcom/google/android/gms/auth/trustagent/trustlet/a;

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->a:Lcom/google/android/gms/auth/trustagent/trustlet/a;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v2, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/google/android/gms/auth/trustagent/trustlet/a;->a:Landroid/content/Context;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/a;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 83
    new-instance v0, Lcom/google/android/gms/auth/trustagent/d;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/trustagent/d;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->c:Lcom/google/android/gms/auth/trustagent/d;

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->c:Lcom/google/android/gms/auth/trustagent/d;

    new-instance v1, Lcom/google/android/gms/auth/trustagent/e;

    invoke-direct {v1, v0}, Lcom/google/android/gms/auth/trustagent/e;-><init>(Lcom/google/android/gms/auth/trustagent/d;)V

    iput-object v1, v0, Lcom/google/android/gms/auth/trustagent/d;->b:Landroid/content/BroadcastReceiver;

    iget-object v1, v0, Lcom/google/android/gms/auth/trustagent/d;->a:Landroid/content/Context;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/d;->b:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 86
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/d;->h()V

    .line 87
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/d;->f()V

    .line 88
    return-void
.end method

.method final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 158
    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/trustlet/d;->c(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    .line 159
    if-nez v0, :cond_1

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    invoke-direct {p0, v2}, Lcom/google/android/gms/auth/trustagent/trustlet/d;->c(Z)V

    .line 163
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/d;->f()V

    .line 164
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/trustagent/trustlet/d;->b(Z)V

    goto :goto_0
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 92
    invoke-super {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->c()V

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->c:Lcom/google/android/gms/auth/trustagent/d;

    iget-object v1, v0, Lcom/google/android/gms/auth/trustagent/d;->b:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/auth/trustagent/d;->a:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/android/gms/auth/trustagent/d;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v3, v0, Lcom/google/android/gms/auth/trustagent/d;->b:Landroid/content/BroadcastReceiver;

    .line 95
    :cond_0
    iput-object v3, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->c:Lcom/google/android/gms/auth/trustagent/d;

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->d:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->e:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/d;->a:Lcom/google/android/gms/auth/trustagent/trustlet/a;

    iget-object v1, v0, Lcom/google/android/gms/auth/trustagent/trustlet/a;->a:Landroid/content/Context;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/a;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 99
    return-void
.end method

.method protected final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 222
    const-string v0, "Bluetooth"

    return-object v0
.end method

.method protected final e()I
    .locals 1

    .prologue
    .line 227
    const/4 v0, 0x1

    return v0
.end method
