.class public final Lcom/google/android/gms/games/service/a/n/c;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:Lcom/google/android/gms/games/a/au;

.field private final e:I

.field private final f:I

.field private final g:Landroid/os/Bundle;

.field private final h:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;II[Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 26
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 27
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/n/c;->c:Lcom/google/android/gms/games/internal/dr;

    .line 28
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/n/c;->d:Lcom/google/android/gms/games/a/au;

    .line 29
    iput p3, p0, Lcom/google/android/gms/games/service/a/n/c;->e:I

    .line 30
    iput p4, p0, Lcom/google/android/gms/games/service/a/n/c;->f:I

    .line 31
    iput-object p5, p0, Lcom/google/android/gms/games/service/a/n/c;->h:[Ljava/lang/String;

    .line 32
    iput-object p6, p0, Lcom/google/android/gms/games/service/a/n/c;->g:Landroid/os/Bundle;

    .line 33
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/n/c;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->o(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 45
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/n/c;->d:Lcom/google/android/gms/games/a/au;

    iget v1, p0, Lcom/google/android/gms/games/service/a/n/c;->e:I

    iget v2, p0, Lcom/google/android/gms/games/service/a/n/c;->f:I

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/n/c;->h:[Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/service/a/n/c;->g:Landroid/os/Bundle;

    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;I[Ljava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
