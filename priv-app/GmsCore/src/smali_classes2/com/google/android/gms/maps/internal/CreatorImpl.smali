.class public Lcom/google/android/gms/maps/internal/CreatorImpl;
.super Lcom/google/android/gms/maps/internal/i;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/maps/internal/i;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;)V
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.google.android.gms"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    const/4 v1, 0x4

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Google Play services package version: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/maps/api/android/lib6/c/by;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/maps/internal/b;
    .locals 1

    new-instance v0, Lcom/google/maps/api/android/lib6/c/dl;

    invoke-direct {v0}, Lcom/google/maps/api/android/lib6/c/dl;-><init>()V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/b/l;Lcom/google/android/gms/maps/StreetViewPanoramaOptions;)Lcom/google/android/gms/maps/internal/cl;
    .locals 2

    invoke-static {p1}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/maps/internal/CreatorImpl;->a(Landroid/content/Context;)V

    new-instance v1, Lcom/google/maps/api/android/lib6/c/bu;

    invoke-direct {v1, v0, p2}, Lcom/google/maps/api/android/lib6/c/bu;-><init>(Landroid/content/Context;Lcom/google/android/gms/maps/StreetViewPanoramaOptions;)V

    return-object v1
.end method

.method public final a(Lcom/google/android/gms/b/l;Lcom/google/android/gms/maps/GoogleMapOptions;)Lcom/google/android/gms/maps/internal/z;
    .locals 2

    invoke-static {p1}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/maps/internal/CreatorImpl;->a(Landroid/content/Context;)V

    new-instance v1, Lcom/google/maps/api/android/lib6/c/ag;

    invoke-direct {v1, v0, p2}, Lcom/google/maps/api/android/lib6/c/ag;-><init>(Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V

    return-object v1
.end method

.method public final a(Lcom/google/android/gms/b/l;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/maps/internal/CreatorImpl;->a(Lcom/google/android/gms/b/l;I)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/b/l;I)V
    .locals 3

    const/4 v0, 0x4

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Google Play services client version: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/c/by;->a(ILjava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    sput-object v0, Lcom/google/maps/api/android/lib6/c/m;->a:Landroid/content/res/Resources;

    const/4 v0, 0x0

    invoke-static {p2, v0}, Lcom/google/maps/api/android/lib6/c/ck;->a(IZ)V

    invoke-static {p2}, Lcom/google/android/gms/maps/internal/cv;->a(I)V

    invoke-static {p0}, Lcom/google/android/gms/maps/m;->a(Lcom/google/android/gms/maps/internal/h;)V

    return-void
.end method

.method public final b(Lcom/google/android/gms/b/l;)Lcom/google/android/gms/maps/internal/w;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/gms/maps/internal/CreatorImpl;->a(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/c/aa;->a(Landroid/app/Activity;)Lcom/google/maps/api/android/lib6/c/aa;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/maps/model/internal/d;
    .locals 1

    new-instance v0, Lcom/google/maps/api/android/lib6/c/cs;

    invoke-direct {v0}, Lcom/google/maps/api/android/lib6/c/cs;-><init>()V

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/b/l;)Lcom/google/android/gms/maps/internal/ci;
    .locals 3

    invoke-static {p1}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/gms/maps/internal/CreatorImpl;->a(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/c/by;->a(Landroid/app/Activity;)Z

    move-result v0

    new-instance v1, Lcom/google/maps/api/android/lib6/c/bk;

    new-instance v2, Lcom/google/maps/api/android/lib6/c/bl;

    invoke-direct {v2, v0}, Lcom/google/maps/api/android/lib6/c/bl;-><init>(Z)V

    invoke-direct {v1, v2}, Lcom/google/maps/api/android/lib6/c/bk;-><init>(Lcom/google/maps/api/android/lib6/c/bn;)V

    return-object v1
.end method
