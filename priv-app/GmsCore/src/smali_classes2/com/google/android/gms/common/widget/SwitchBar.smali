.class public Lcom/google/android/gms/common/widget/SwitchBar;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/Checkable;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private a:Lcom/google/android/gms/common/widget/h;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/CompoundButton;

.field private d:Ljava/lang/CharSequence;

.field private e:Ljava/lang/CharSequence;

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 41
    invoke-static {p1}, Lcom/google/android/gms/common/widget/SwitchBar;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 42
    invoke-virtual {p0}, Lcom/google/android/gms/common/widget/SwitchBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    sget v2, Lcom/google/android/gms/common/widget/b;->a:I

    sget v3, Lcom/google/android/gms/common/widget/f;->a:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/gms/common/widget/SwitchBar;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 46
    invoke-static {p1}, Lcom/google/android/gms/common/widget/SwitchBar;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    invoke-virtual {p0}, Lcom/google/android/gms/common/widget/SwitchBar;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/gms/common/widget/b;->a:I

    sget v2, Lcom/google/android/gms/common/widget/f;->a:I

    invoke-direct {p0, v0, p2, v1, v2}, Lcom/google/android/gms/common/widget/SwitchBar;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    sget v0, Lcom/google/android/gms/common/widget/f;->a:I

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/common/widget/SwitchBar;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 59
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/common/widget/SwitchBar;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 60
    return-void
.end method

.method private static a(Landroid/content/Context;)Landroid/content/Context;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 68
    const/4 v0, 0x0

    const/4 v1, 0x1

    new-array v1, v1, [I

    sget v2, Lcom/google/android/gms/common/widget/b;->a:I

    aput v2, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 69
    sget v1, Lcom/google/android/gms/common/widget/f;->a:I

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 70
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 72
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-direct {v0, p0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method

.method private a()Landroid/widget/CompoundButton;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->c:Landroid/widget/CompoundButton;

    if-nez v0, :cond_0

    .line 184
    invoke-direct {p0}, Lcom/google/android/gms/common/widget/SwitchBar;->b()V

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->c:Landroid/widget/CompoundButton;

    return-object v0
.end method

.method private static a(IFII)Landroid/widget/LinearLayout$LayoutParams;
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 78
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, p0, v1, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 79
    const/16 v1, 0x10

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 80
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_0

    .line 81
    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    .line 82
    invoke-virtual {v0, p3}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    .line 87
    :goto_0
    return-object v0

    .line 84
    :cond_0
    iput p2, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 85
    iput p3, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 92
    sget-object v0, Lcom/google/android/gms/common/widget/g;->s:[I

    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 93
    sget v0, Lcom/google/android/gms/common/widget/g;->u:I

    const/high16 v4, -0x1000000

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    .line 94
    sget v0, Lcom/google/android/gms/common/widget/g;->y:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->d:Ljava/lang/CharSequence;

    .line 95
    sget v0, Lcom/google/android/gms/common/widget/g;->x:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->e:Ljava/lang/CharSequence;

    .line 96
    sget v0, Lcom/google/android/gms/common/widget/g;->w:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->f:I

    .line 97
    sget v0, Lcom/google/android/gms/common/widget/g;->v:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->g:I

    .line 98
    sget v0, Lcom/google/android/gms/common/widget/g;->t:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 99
    sget v0, Lcom/google/android/gms/common/widget/g;->t:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->h:I

    .line 104
    :goto_1
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 106
    invoke-virtual {p0, v2}, Lcom/google/android/gms/common/widget/SwitchBar;->setOrientation(I)V

    .line 107
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v0, v3, :cond_3

    .line 108
    sget v0, Lcom/google/android/gms/common/widget/c;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/widget/SwitchBar;->setBackgroundResource(I)V

    .line 113
    :goto_2
    iget v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->h:I

    if-eqz v0, :cond_4

    :goto_3
    if-eqz v1, :cond_0

    .line 114
    invoke-direct {p0}, Lcom/google/android/gms/common/widget/SwitchBar;->b()V

    .line 116
    :cond_0
    return-void

    .line 99
    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    .line 102
    :cond_2
    iput v2, p0, Lcom/google/android/gms/common/widget/SwitchBar;->h:I

    goto :goto_1

    .line 110
    :cond_3
    invoke-virtual {p0, v4}, Lcom/google/android/gms/common/widget/SwitchBar;->setBackgroundColor(I)V

    goto :goto_2

    :cond_4
    move v1, v2

    .line 113
    goto :goto_3
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 210
    invoke-virtual {p0}, Lcom/google/android/gms/common/widget/SwitchBar;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 211
    invoke-virtual {p0}, Lcom/google/android/gms/common/widget/SwitchBar;->isChecked()Z

    move-result v2

    .line 213
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->b:Landroid/widget/TextView;

    .line 214
    iget-object v3, p0, Lcom/google/android/gms/common/widget/SwitchBar;->b:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->d:Ljava/lang/CharSequence;

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    iget-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->b:Landroid/widget/TextView;

    const/high16 v3, 0x3f800000    # 1.0f

    iget v4, p0, Lcom/google/android/gms/common/widget/SwitchBar;->f:I

    invoke-static {v5, v3, v4, v5}, Lcom/google/android/gms/common/widget/SwitchBar;->a(IFII)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Lcom/google/android/gms/common/widget/SwitchBar;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 220
    sget v0, Lcom/google/android/gms/common/widget/e;->d:I

    const/4 v3, 0x0

    invoke-static {v1, v0, v3}, Lcom/google/android/gms/common/widget/SwitchBar;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->c:Landroid/widget/CompoundButton;

    .line 221
    iget-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->c:Landroid/widget/CompoundButton;

    invoke-virtual {p0}, Lcom/google/android/gms/common/widget/SwitchBar;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    .line 223
    iget-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 224
    iget-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->c:Landroid/widget/CompoundButton;

    const/4 v1, -0x2

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/gms/common/widget/SwitchBar;->g:I

    invoke-static {v1, v2, v5, v3}, Lcom/google/android/gms/common/widget/SwitchBar;->a(IFII)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/common/widget/SwitchBar;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 228
    return-void

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->e:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 231
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->h:I

    .line 232
    iget-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->b:Landroid/widget/TextView;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/common/widget/SwitchBar;->b()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/common/widget/SwitchBar;->b:Landroid/widget/TextView;

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->d:Ljava/lang/CharSequence;

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 233
    return-void

    .line 231
    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    .line 232
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->e:Ljava/lang/CharSequence;

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/widget/h;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/google/android/gms/common/widget/SwitchBar;->a:Lcom/google/android/gms/common/widget/h;

    .line 180
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 165
    iput-object p1, p0, Lcom/google/android/gms/common/widget/SwitchBar;->d:Ljava/lang/CharSequence;

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/widget/SwitchBar;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/google/android/gms/common/widget/SwitchBar;->a()Landroid/widget/CompoundButton;

    move-result-object v0

    .line 144
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 145
    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 146
    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 147
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/widget/SwitchBar;->b(Z)V

    .line 148
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 172
    iput-object p1, p0, Lcom/google/android/gms/common/widget/SwitchBar;->e:Ljava/lang/CharSequence;

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/widget/SwitchBar;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    :cond_0
    return-void
.end method

.method public isChecked()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 125
    iget v1, p0, Lcom/google/android/gms/common/widget/SwitchBar;->h:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .prologue
    .line 205
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/widget/SwitchBar;->b(Z)V

    .line 206
    iget-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->a:Lcom/google/android/gms/common/widget/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->a:Lcom/google/android/gms/common/widget/h;

    invoke-interface {v0, p0, p2}, Lcom/google/android/gms/common/widget/h;->a(Lcom/google/android/gms/common/widget/SwitchBar;Z)V

    .line 207
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/google/android/gms/common/widget/SwitchBar;->toggle()V

    .line 200
    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/gms/common/widget/SwitchBar;->a()Landroid/widget/CompoundButton;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 134
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 157
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 158
    if-eqz p1, :cond_1

    move-object v0, p0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/widget/SwitchBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->c:Landroid/widget/CompoundButton;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/common/widget/SwitchBar;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    .line 162
    :cond_0
    return-void

    .line 158
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/google/android/gms/common/widget/SwitchBar;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/widget/SwitchBar;->setChecked(Z)V

    .line 153
    return-void

    .line 152
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
