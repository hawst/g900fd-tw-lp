.class public final Lcom/google/android/gms/games/ui/client/matches/j;
.super Lcom/google/android/gms/games/ui/cb;
.source "SourceFile"


# instance fields
.field private final g:Landroid/content/Context;

.field private final h:Landroid/view/LayoutInflater;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/client/a;)V
    .locals 1

    .prologue
    .line 1254
    sget v0, Lcom/google/android/gms/k;->g:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/cb;-><init>(Landroid/content/Context;I)V

    .line 1255
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/matches/j;->g:Landroid/content/Context;

    .line 1256
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/client/a;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/j;->h:Landroid/view/LayoutInflater;

    .line 1257
    return-void
.end method


# virtual methods
.method public final synthetic a(Landroid/view/View;ILjava/lang/Object;)V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1243
    check-cast p3, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/client/matches/k;

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->g:Lcom/google/android/gms/games/ui/client/matches/j;

    iget-object v1, v1, Lcom/google/android/gms/games/ui/client/matches/j;->i:Ljava/lang/String;

    const-string v4, "You must call setCurrentPlayerId() before any bindView() calls come in"

    invoke-static {v1, v4}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->g:Lcom/google/android/gms/games/ui/client/matches/j;

    iget-object v1, v1, Lcom/google/android/gms/games/ui/client/matches/j;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    if-nez p3, :cond_1

    move v7, v2

    :goto_0
    if-eqz p3, :cond_10

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Lcom/google/android/gms/games/ui/client/matches/k;->g:Lcom/google/android/gms/games/ui/client/matches/j;

    iget-object v5, v5, Lcom/google/android/gms/games/ui/client/matches/j;->i:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v4, v2

    :goto_1
    if-nez v1, :cond_3

    move v1, v2

    :goto_2
    move v5, v1

    move v6, v4

    :goto_3
    if-eqz v6, :cond_4

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->c:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/google/android/gms/games/ui/client/matches/k;->c:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v1, v4, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    :goto_4
    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->d:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/google/android/gms/games/ui/client/matches/k;->e:Landroid/database/CharArrayBuffer;

    iget-object v4, v4, Landroid/database/CharArrayBuffer;->data:[C

    iget-object v8, v0, Lcom/google/android/gms/games/ui/client/matches/k;->e:Landroid/database/CharArrayBuffer;

    iget v8, v8, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v1, v4, v3, v8}, Landroid/widget/TextView;->setText([CII)V

    if-eqz p3, :cond_6

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/Participant;->h()Landroid/net/Uri;

    move-result-object v1

    :goto_5
    iget-object v4, v0, Lcom/google/android/gms/games/ui/client/matches/k;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v8, Lcom/google/android/gms/h;->ag:I

    invoke-virtual {v0, v4, v1, v8}, Lcom/google/android/gms/games/ui/client/matches/k;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    if-eqz p3, :cond_7

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/Participant;->a()I

    move-result v1

    move v4, v1

    :goto_6
    if-eqz p3, :cond_0

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/Participant;->f()Z

    :cond_0
    if-eqz v6, :cond_8

    move v1, v3

    :goto_7
    if-lez v1, :cond_c

    iget-object v5, v0, Lcom/google/android/gms/games/ui/client/matches/k;->f:Landroid/widget/TextView;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_8
    const/4 v1, 0x2

    if-ne v4, v1, :cond_d

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->d:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/google/android/gms/games/ui/client/matches/k;->d:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->d:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/google/android/gms/games/ui/client/matches/k;->g:Lcom/google/android/gms/games/ui/client/matches/j;

    iget-object v2, v2, Lcom/google/android/gms/games/ui/client/matches/j;->g:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/f;->E:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->f:Landroid/widget/TextView;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/client/matches/k;->g:Lcom/google/android/gms/games/ui/client/matches/j;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/client/matches/j;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/gms/f;->G:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_9
    return-void

    :cond_1
    move v7, v3

    goto/16 :goto_0

    :cond_2
    move v4, v3

    goto/16 :goto_1

    :cond_3
    move v1, v3

    goto/16 :goto_2

    :cond_4
    if-eqz v7, :cond_5

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    sget v1, Lcom/google/android/gms/p;->la:I

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v4, v0, Lcom/google/android/gms/games/ui/client/matches/k;->e:Landroid/database/CharArrayBuffer;

    invoke-static {v1, v4}, Lcom/google/android/gms/common/util/q;->a(Ljava/lang/String;Landroid/database/CharArrayBuffer;)V

    goto/16 :goto_4

    :cond_5
    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->e:Landroid/database/CharArrayBuffer;

    invoke-interface {p3, v1}, Lcom/google/android/gms/games/multiplayer/Participant;->a(Landroid/database/CharArrayBuffer;)V

    goto/16 :goto_4

    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_7
    move v4, v2

    goto/16 :goto_6

    :cond_8
    if-eqz v7, :cond_9

    move v1, v3

    goto/16 :goto_7

    :cond_9
    packed-switch v4, :pswitch_data_0

    const-string v1, "WaitingRoomAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "bindView: unexpected participant status: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "bindView: unexpected participant status: "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/e;->b(Ljava/lang/Object;)V

    move v1, v3

    goto/16 :goto_7

    :pswitch_0
    sget v1, Lcom/google/android/gms/p;->mx:I

    goto/16 :goto_7

    :pswitch_1
    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->g:Lcom/google/android/gms/games/ui/client/matches/j;

    iget-object v1, v1, Lcom/google/android/gms/games/ui/client/matches/j;->j:Ljava/lang/String;

    invoke-interface {p3}, Lcom/google/android/gms/games/multiplayer/Participant;->l()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    sget v1, Lcom/google/android/gms/p;->mv:I

    goto/16 :goto_7

    :cond_a
    if-eqz v5, :cond_b

    sget v1, Lcom/google/android/gms/p;->mu:I

    goto/16 :goto_7

    :cond_b
    sget v1, Lcom/google/android/gms/p;->mz:I

    goto/16 :goto_7

    :pswitch_2
    sget v1, Lcom/google/android/gms/p;->mw:I

    goto/16 :goto_7

    :pswitch_3
    sget v1, Lcom/google/android/gms/p;->my:I

    goto/16 :goto_7

    :pswitch_4
    move v1, v3

    goto/16 :goto_7

    :cond_c
    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_8

    :cond_d
    const/4 v1, 0x3

    if-eq v4, v1, :cond_e

    const/4 v1, 0x4

    if-eq v4, v1, :cond_e

    const/4 v1, -0x1

    if-ne v4, v1, :cond_f

    :cond_e
    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v2, Lcom/google/android/gms/f;->D:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->d:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/google/android/gms/games/ui/client/matches/k;->d:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->d:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/google/android/gms/games/ui/client/matches/k;->g:Lcom/google/android/gms/games/ui/client/matches/j;

    iget-object v2, v2, Lcom/google/android/gms/games/ui/client/matches/j;->g:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/f;->D:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->f:Landroid/widget/TextView;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/client/matches/k;->g:Lcom/google/android/gms/games/ui/client/matches/j;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/client/matches/j;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/gms/f;->D:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_9

    :cond_f
    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v2, Lcom/google/android/gms/f;->H:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->d:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/google/android/gms/games/ui/client/matches/k;->d:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->d:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/google/android/gms/games/ui/client/matches/k;->g:Lcom/google/android/gms/games/ui/client/matches/j;

    iget-object v2, v2, Lcom/google/android/gms/games/ui/client/matches/j;->g:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/f;->E:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/matches/k;->f:Landroid/widget/TextView;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/client/matches/k;->g:Lcom/google/android/gms/games/ui/client/matches/j;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/client/matches/j;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/gms/f;->G:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_9

    :cond_10
    move v5, v3

    move v6, v3

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1261
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1263
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->B_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/j;->j:Ljava/lang/String;

    .line 1265
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->l()Ljava/util/ArrayList;

    move-result-object v3

    .line 1266
    new-instance v4, Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1271
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/j;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    move v1, v2

    .line 1272
    :goto_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1273
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    .line 1274
    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v5

    .line 1275
    if-eqz v5, :cond_3

    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/games/ui/client/matches/j;->i:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1276
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    move v1, v2

    .line 1286
    :goto_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 1287
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    .line 1288
    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v5

    .line 1289
    iget-object v6, p0, Lcom/google/android/gms/games/ui/client/matches/j;->i:Ljava/lang/String;

    if-eqz v6, :cond_1

    if-eqz v5, :cond_1

    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/games/ui/client/matches/j;->i:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1292
    :cond_1
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1286
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1272
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1304
    :cond_4
    invoke-static {p1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->m(Lcom/google/android/gms/games/multiplayer/realtime/Room;)I

    move-result v0

    .line 1305
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1306
    :goto_2
    if-ge v2, v0, :cond_5

    .line 1307
    const/4 v1, 0x0

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1306
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1313
    :cond_5
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/games/multiplayer/Participant;

    .line 1315
    new-instance v1, Lcom/google/android/gms/common/data/w;

    invoke-direct {v1, v0}, Lcom/google/android/gms/common/data/w;-><init>([Ljava/lang/Object;)V

    .line 1321
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/j;->a(Lcom/google/android/gms/common/data/d;)V

    .line 1322
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1345
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/j;->i:Ljava/lang/String;

    .line 1346
    return-void
.end method

.method public final g()Landroid/view/View;
    .locals 3

    .prologue
    .line 1350
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/j;->h:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->bY:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1351
    new-instance v1, Lcom/google/android/gms/games/ui/client/matches/k;

    invoke-direct {v1, p0, p0, v0}, Lcom/google/android/gms/games/ui/client/matches/k;-><init>(Lcom/google/android/gms/games/ui/client/matches/j;Lcom/google/android/gms/games/ui/o;Landroid/view/View;)V

    .line 1352
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1353
    return-object v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 1545
    const/4 v0, 0x0

    return v0
.end method
