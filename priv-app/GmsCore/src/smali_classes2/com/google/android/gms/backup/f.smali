.class public final Lcom/google/android/gms/backup/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/text/DecimalFormat;

.field public static final b:Ljava/text/DecimalFormat;


# instance fields
.field private c:Landroid/app/backup/BackupDataInput;

.field private d:Lcom/google/android/gms/backup/q;

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "\':#\'00000"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/backup/f;->a:Ljava/text/DecimalFormat;

    .line 24
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "/0"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/backup/f;->b:Ljava/text/DecimalFormat;

    return-void
.end method

.method public constructor <init>(Landroid/app/backup/BackupDataInput;Lcom/google/android/gms/backup/q;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/backup/f;->e:I

    .line 36
    iput-object p1, p0, Lcom/google/android/gms/backup/f;->c:Landroid/app/backup/BackupDataInput;

    .line 37
    iput-object p2, p0, Lcom/google/android/gms/backup/f;->d:Lcom/google/android/gms/backup/q;

    .line 38
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x1

    const v7, 0x7d000

    const/4 v0, 0x0

    .line 52
    iget v2, p0, Lcom/google/android/gms/backup/f;->e:I

    if-ltz v2, :cond_0

    iget v2, p0, Lcom/google/android/gms/backup/f;->e:I

    iget-object v3, p0, Lcom/google/android/gms/backup/f;->c:Landroid/app/backup/BackupDataInput;

    invoke-virtual {v3}, Landroid/app/backup/BackupDataInput;->getDataSize()I

    move-result v3

    if-lt v2, v3, :cond_2

    .line 54
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/backup/f;->c:Landroid/app/backup/BackupDataInput;

    invoke-virtual {v2}, Landroid/app/backup/BackupDataInput;->readNextHeader()Z

    move-result v2

    if-nez v2, :cond_1

    .line 109
    :goto_0
    return v0

    .line 55
    :cond_1
    iput v0, p0, Lcom/google/android/gms/backup/f;->e:I

    .line 72
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/backup/f;->c:Landroid/app/backup/BackupDataInput;

    invoke-virtual {v2}, Landroid/app/backup/BackupDataInput;->getKey()Ljava/lang/String;

    move-result-object v2

    .line 73
    iget-object v3, p0, Lcom/google/android/gms/backup/f;->c:Landroid/app/backup/BackupDataInput;

    invoke-virtual {v3}, Landroid/app/backup/BackupDataInput;->getDataSize()I

    move-result v3

    .line 74
    if-gez v3, :cond_3

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/backup/f;->d:Lcom/google/android/gms/backup/q;

    invoke-virtual {v0, v2, v8}, Lcom/google/android/gms/backup/q;->a(Ljava/lang/String;[B)V

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/backup/f;->d:Lcom/google/android/gms/backup/q;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/backup/q;->a(Ljava/lang/String;)V

    move v0, v1

    .line 77
    goto :goto_0

    .line 80
    :cond_3
    iget v4, p0, Lcom/google/android/gms/backup/f;->e:I

    sub-int v4, v3, v4

    invoke-static {v7, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    new-array v4, v4, [B

    .line 81
    iget-object v5, p0, Lcom/google/android/gms/backup/f;->c:Landroid/app/backup/BackupDataInput;

    array-length v6, v4

    invoke-virtual {v5, v4, v0, v6}, Landroid/app/backup/BackupDataInput;->readEntityData([BII)I

    .line 86
    iget v0, p0, Lcom/google/android/gms/backup/f;->e:I

    if-nez v0, :cond_4

    if-gt v3, v7, :cond_4

    const-string v0, ":#"

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/backup/f;->d:Lcom/google/android/gms/backup/q;

    invoke-virtual {v0, v2, v4}, Lcom/google/android/gms/backup/q;->a(Ljava/lang/String;[B)V

    .line 108
    :goto_1
    iget v0, p0, Lcom/google/android/gms/backup/f;->e:I

    array-length v2, v4

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/gms/backup/f;->e:I

    move v0, v1

    .line 109
    goto :goto_0

    .line 97
    :cond_4
    iget v0, p0, Lcom/google/android/gms/backup/f;->e:I

    div-int/2addr v0, v7

    .line 98
    if-nez v0, :cond_6

    .line 101
    const-string v5, ":#"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/google/android/gms/backup/f;->d:Lcom/google/android/gms/backup/q;

    invoke-virtual {v5, v2, v8}, Lcom/google/android/gms/backup/q;->a(Ljava/lang/String;[B)V

    .line 102
    :cond_5
    iget-object v5, p0, Lcom/google/android/gms/backup/f;->d:Lcom/google/android/gms/backup/q;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":#"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/backup/q;->a(Ljava/lang/String;)V

    .line 105
    :cond_6
    iget-object v5, p0, Lcom/google/android/gms/backup/f;->d:Lcom/google/android/gms/backup/q;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v6, Lcom/google/android/gms/backup/f;->a:Ljava/text/DecimalFormat;

    int-to-long v8, v0

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/backup/f;->b:Ljava/text/DecimalFormat;

    int-to-long v6, v3

    invoke-virtual {v2, v6, v7}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0, v4}, Lcom/google/android/gms/backup/q;->a(Ljava/lang/String;[B)V

    goto :goto_1
.end method
