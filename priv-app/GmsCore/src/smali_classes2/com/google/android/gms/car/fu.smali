.class public final Lcom/google/android/gms/car/fu;
.super Lcom/google/android/gms/car/md;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/gw;
.implements Lcom/google/android/gms/car/senderprotocol/au;


# instance fields
.field final a:Ljava/util/List;

.field volatile b:Z

.field private c:Lcom/google/android/gms/car/senderprotocol/at;

.field private d:Lcom/google/android/gms/car/CarInstrumentClusterInfo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/gms/car/md;-><init>()V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/fu;->a:Ljava/util/List;

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/fu;->d:Lcom/google/android/gms/car/CarInstrumentClusterInfo;

    .line 195
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/fu;Lcom/google/android/gms/car/fv;)Z
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/fu;->a(Lcom/google/android/gms/car/fv;)Z

    move-result v0

    return v0
.end method

.method private a(Lcom/google/android/gms/car/fv;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 155
    iget-object v2, p0, Lcom/google/android/gms/car/fu;->a:Ljava/util/List;

    monitor-enter v2

    .line 156
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/fu;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/fv;

    .line 158
    if-ne v0, p1, :cond_0

    .line 159
    iget-object v1, v0, Lcom/google/android/gms/car/fv;->a:Lcom/google/android/gms/car/mf;

    invoke-interface {v1}, Lcom/google/android/gms/car/mf;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1, v0, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 160
    iget-object v1, p0, Lcom/google/android/gms/car/fu;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    :goto_0
    return v0

    .line 163
    :cond_1
    monitor-exit v2

    move v0, v1

    .line 164
    goto :goto_0

    .line 163
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private c(Lcom/google/android/gms/car/mf;)Lcom/google/android/gms/car/fv;
    .locals 4

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/gms/car/fu;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/fv;

    .line 170
    iget-object v2, v0, Lcom/google/android/gms/car/fv;->a:Lcom/google/android/gms/car/mf;

    invoke-interface {v2}, Lcom/google/android/gms/car/mf;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/car/mf;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 174
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 188
    iget-boolean v0, p0, Lcom/google/android/gms/car/fu;->b:Z

    if-nez v0, :cond_0

    .line 189
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotConnected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/fu;->c:Lcom/google/android/gms/car/senderprotocol/at;

    if-nez v0, :cond_1

    .line 191
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "InstrumentClusterEndpoint not set."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 193
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 64
    iget-object v6, p0, Lcom/google/android/gms/car/fu;->a:Ljava/util/List;

    monitor-enter v6

    .line 65
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/fu;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/fv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    :try_start_1
    iget-object v0, v0, Lcom/google/android/gms/car/fv;->a:Lcom/google/android/gms/car/mf;

    iget-object v1, p0, Lcom/google/android/gms/car/fu;->d:Lcom/google/android/gms/car/CarInstrumentClusterInfo;

    iget v1, v1, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->b:I

    iget-object v2, p0, Lcom/google/android/gms/car/fu;->d:Lcom/google/android/gms/car/CarInstrumentClusterInfo;

    iget v2, v2, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->c:I

    iget-object v3, p0, Lcom/google/android/gms/car/fu;->d:Lcom/google/android/gms/car/CarInstrumentClusterInfo;

    iget v3, v3, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->e:I

    iget-object v4, p0, Lcom/google/android/gms/car/fu;->d:Lcom/google/android/gms/car/CarInstrumentClusterInfo;

    iget v4, v4, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->d:I

    iget-object v5, p0, Lcom/google/android/gms/car/fu;->d:Lcom/google/android/gms/car/CarInstrumentClusterInfo;

    iget v5, v5, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->f:I

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/car/mf;->a(IIIII)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 72
    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "CAR.INST"

    const-string v1, "Error calling onStart()"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/google/android/gms/car/fu;->e()V

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/car/fu;->c:Lcom/google/android/gms/car/senderprotocol/at;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/senderprotocol/at;->b(I)V

    .line 95
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/google/android/gms/car/fu;->e()V

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/car/fu;->c:Lcom/google/android/gms/car/senderprotocol/at;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/car/senderprotocol/at;->a(II)V

    .line 109
    return-void
.end method

.method public final a(IILcom/google/android/c/b/br;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 34
    new-instance v0, Lcom/google/android/gms/car/CarInstrumentClusterInfo;

    invoke-direct {v0}, Lcom/google/android/gms/car/CarInstrumentClusterInfo;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/fu;->d:Lcom/google/android/gms/car/CarInstrumentClusterInfo;

    .line 35
    iget-object v0, p0, Lcom/google/android/gms/car/fu;->d:Lcom/google/android/gms/car/CarInstrumentClusterInfo;

    iput p1, v0, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->b:I

    .line 36
    iget-object v0, p0, Lcom/google/android/gms/car/fu;->d:Lcom/google/android/gms/car/CarInstrumentClusterInfo;

    iput p2, v0, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->c:I

    .line 37
    if-ne p2, v2, :cond_1

    .line 38
    if-nez p3, :cond_0

    .line 39
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No image options set for image capable instrument cluster."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/fu;->d:Lcom/google/android/gms/car/CarInstrumentClusterInfo;

    iget v1, p3, Lcom/google/android/c/b/br;->a:I

    iput v1, v0, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->e:I

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/car/fu;->d:Lcom/google/android/gms/car/CarInstrumentClusterInfo;

    iget v1, p3, Lcom/google/android/c/b/br;->b:I

    iput v1, v0, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->d:I

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/car/fu;->d:Lcom/google/android/gms/car/CarInstrumentClusterInfo;

    iget v1, p3, Lcom/google/android/c/b/br;->c:I

    iput v1, v0, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->f:I

    .line 46
    :cond_1
    iput-boolean v2, p0, Lcom/google/android/gms/car/fu;->b:Z

    .line 47
    return-void
.end method

.method public final a(ILjava/lang/String;II[BI)V
    .locals 7

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/google/android/gms/car/fu;->e()V

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/car/fu;->c:Lcom/google/android/gms/car/senderprotocol/at;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/car/senderprotocol/at;->a(ILjava/lang/String;II[BI)V

    .line 103
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/senderprotocol/az;)V
    .locals 0

    .prologue
    .line 51
    check-cast p1, Lcom/google/android/gms/car/senderprotocol/at;

    iput-object p1, p0, Lcom/google/android/gms/car/fu;->c:Lcom/google/android/gms/car/senderprotocol/at;

    .line 52
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/mf;)Z
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 113
    invoke-direct {p0}, Lcom/google/android/gms/car/fu;->e()V

    .line 114
    iget-object v7, p0, Lcom/google/android/gms/car/fu;->a:Ljava/util/List;

    monitor-enter v7

    .line 115
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/fu;->c(Lcom/google/android/gms/car/mf;)Lcom/google/android/gms/car/fv;

    move-result-object v0

    if-nez v0, :cond_0

    .line 116
    new-instance v0, Lcom/google/android/gms/car/fv;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/fv;-><init>(Lcom/google/android/gms/car/fu;Lcom/google/android/gms/car/mf;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    :try_start_1
    invoke-interface {p1}, Lcom/google/android/gms/car/mf;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 124
    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/car/fu;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 128
    :try_start_3
    iget-object v0, v0, Lcom/google/android/gms/car/fv;->a:Lcom/google/android/gms/car/mf;

    iget-object v1, p0, Lcom/google/android/gms/car/fu;->d:Lcom/google/android/gms/car/CarInstrumentClusterInfo;

    iget v1, v1, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->b:I

    iget-object v2, p0, Lcom/google/android/gms/car/fu;->d:Lcom/google/android/gms/car/CarInstrumentClusterInfo;

    iget v2, v2, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->c:I

    iget-object v3, p0, Lcom/google/android/gms/car/fu;->d:Lcom/google/android/gms/car/CarInstrumentClusterInfo;

    iget v3, v3, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->e:I

    iget-object v4, p0, Lcom/google/android/gms/car/fu;->d:Lcom/google/android/gms/car/CarInstrumentClusterInfo;

    iget v4, v4, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->d:I

    iget-object v5, p0, Lcom/google/android/gms/car/fu;->d:Lcom/google/android/gms/car/CarInstrumentClusterInfo;

    iget v5, v5, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->f:I

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/car/mf;->a(IIIII)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 138
    :cond_0
    monitor-exit v7

    .line 139
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 121
    :catch_0
    move-exception v0

    :try_start_4
    const-string v0, "CAR.INST"

    const-string v1, "Adding listener failed."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    monitor-exit v7

    move v0, v6

    goto :goto_0

    .line 134
    :catch_1
    move-exception v0

    const-string v0, "CAR.INST"

    const-string v1, "listener.onStart failed."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v0, v6

    goto :goto_0

    .line 138
    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 80
    iget-object v1, p0, Lcom/google/android/gms/car/fu;->a:Ljava/util/List;

    monitor-enter v1

    .line 81
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/fu;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/fv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    :try_start_1
    iget-object v0, v0, Lcom/google/android/gms/car/fv;->a:Lcom/google/android/gms/car/mf;

    invoke-interface {v0}, Lcom/google/android/gms/car/mf;->a()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 85
    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "CAR.INST"

    const-string v3, "Error calling onStop()"

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public final b(Lcom/google/android/gms/car/mf;)Z
    .locals 2

    .prologue
    .line 144
    iget-object v1, p0, Lcom/google/android/gms/car/fu;->a:Ljava/util/List;

    monitor-enter v1

    .line 146
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/fu;->c(Lcom/google/android/gms/car/mf;)Lcom/google/android/gms/car/fv;

    move-result-object v0

    .line 147
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    if-nez v0, :cond_0

    .line 149
    const/4 v0, 0x0

    .line 151
    :goto_0
    return v0

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 151
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/fu;->a(Lcom/google/android/gms/car/fv;)Z

    move-result v0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/gms/car/CarInstrumentClusterInfo;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/gms/car/fu;->d:Lcom/google/android/gms/car/CarInstrumentClusterInfo;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/gms/car/fu;->d:Lcom/google/android/gms/car/CarInstrumentClusterInfo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
