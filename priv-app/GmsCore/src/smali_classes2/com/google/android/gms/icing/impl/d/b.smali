.class public abstract Lcom/google/android/gms/icing/impl/d/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/icing/impl/m;


# instance fields
.field private final a:Ljava/util/Random;

.field private final b:Lcom/google/android/gms/common/api/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/d/b;->a:Ljava/util/Random;

    .line 35
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/usagereporting/a;->a:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/d/b;->b:Lcom/google/android/gms/common/api/v;

    .line 36
    return-void
.end method

.method private a(Lcom/google/android/gms/common/a/d;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-virtual {p1}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 46
    const/4 v2, 0x0

    cmpg-float v2, v0, v2

    if-ltz v2, :cond_0

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v0, v2

    if-lez v2, :cond_1

    .line 48
    :cond_0
    const-string v2, "Bad sample ratio: %s"

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    move v0, v1

    .line 51
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/d/b;->a:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    cmpg-float v0, v2, v0

    if-gez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    .prologue
    .line 137
    const-string v0, "Timing: %d = %dms"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 139
    sget-object v0, Lcom/google/android/gms/icing/a/a;->m:Lcom/google/android/gms/common/a/d;

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/d/b;->a(Lcom/google/android/gms/common/a/d;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    :goto_0
    return-void

    .line 143
    :cond_0
    new-instance v0, Lcom/google/k/f/an;

    invoke-direct {v0}, Lcom/google/k/f/an;-><init>()V

    .line 144
    new-instance v1, Lcom/google/k/f/ar;

    invoke-direct {v1}, Lcom/google/k/f/ar;-><init>()V

    iput-object v1, v0, Lcom/google/k/f/an;->d:Lcom/google/k/f/ar;

    .line 145
    iget-object v1, v0, Lcom/google/k/f/an;->d:Lcom/google/k/f/ar;

    iput p1, v1, Lcom/google/k/f/ar;->a:I

    .line 146
    iget-object v1, v0, Lcom/google/k/f/an;->d:Lcom/google/k/f/ar;

    iput p2, v1, Lcom/google/k/f/ar;->b:I

    .line 147
    const-string v1, "tstats"

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/icing/impl/d/b;->a(Ljava/lang/String;Lcom/google/k/f/an;Z)V

    goto :goto_0
.end method

.method public final a(IJJJZI)V
    .locals 4

    .prologue
    const/16 v2, 0x15

    .line 82
    sget-object v0, Lcom/google/android/gms/icing/a/a;->k:Lcom/google/android/gms/common/a/d;

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/d/b;->a(Lcom/google/android/gms/common/a/d;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 87
    :cond_1
    invoke-static {v2}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d/b;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->c()Lcom/google/android/gms/common/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    :try_start_0
    sget-object v0, Lcom/google/android/gms/usagereporting/a;->b:Lcom/google/android/gms/usagereporting/d;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/d/b;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1}, Lcom/google/android/gms/usagereporting/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/usagereporting/g;

    .line 95
    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/google/android/gms/usagereporting/g;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 96
    const-string v0, "Skipping storage state: opt-out"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d/b;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d/b;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 104
    :cond_3
    new-instance v1, Lcom/google/k/f/an;

    invoke-direct {v1}, Lcom/google/k/f/an;-><init>()V

    .line 105
    new-instance v0, Lcom/google/k/f/aq;

    invoke-direct {v0}, Lcom/google/k/f/aq;-><init>()V

    iput-object v0, v1, Lcom/google/k/f/an;->b:Lcom/google/k/f/aq;

    .line 106
    iget-object v0, v1, Lcom/google/k/f/an;->b:Lcom/google/k/f/aq;

    iput p1, v0, Lcom/google/k/f/aq;->a:I

    .line 107
    iget-object v0, v1, Lcom/google/k/f/an;->b:Lcom/google/k/f/aq;

    iput-wide p2, v0, Lcom/google/k/f/aq;->b:J

    .line 108
    iget-object v0, v1, Lcom/google/k/f/an;->b:Lcom/google/k/f/aq;

    iput-wide p4, v0, Lcom/google/k/f/aq;->c:J

    .line 109
    iget-object v0, v1, Lcom/google/k/f/an;->b:Lcom/google/k/f/aq;

    iput-wide p6, v0, Lcom/google/k/f/aq;->d:J

    .line 110
    iget-object v0, v1, Lcom/google/k/f/an;->b:Lcom/google/k/f/aq;

    iput-boolean p8, v0, Lcom/google/k/f/aq;->e:Z

    .line 111
    iget-object v0, v1, Lcom/google/k/f/an;->b:Lcom/google/k/f/aq;

    iput p9, v0, Lcom/google/k/f/aq;->f:I

    .line 114
    invoke-static {v2}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    .line 115
    :goto_1
    const-string v2, "sstate"

    invoke-virtual {p0, v2, v1, v0}, Lcom/google/android/gms/icing/impl/d/b;->a(Ljava/lang/String;Lcom/google/k/f/an;Z)V

    goto :goto_0

    .line 100
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/d/b;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/v;->d()V

    throw v0

    .line 114
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/google/k/f/ap;)V
    .locals 3

    .prologue
    .line 123
    sget-object v0, Lcom/google/android/gms/icing/a/a;->l:Lcom/google/android/gms/common/a/d;

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/d/b;->a(Lcom/google/android/gms/common/a/d;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    :goto_0
    return-void

    .line 127
    :cond_0
    new-instance v0, Lcom/google/k/f/an;

    invoke-direct {v0}, Lcom/google/k/f/an;-><init>()V

    .line 128
    iput-object p1, v0, Lcom/google/k/f/an;->c:Lcom/google/k/f/ap;

    .line 129
    const-string v1, "qstats"

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/icing/impl/d/b;->a(Ljava/lang/String;Lcom/google/k/f/an;Z)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 66
    sget-object v0, Lcom/google/android/gms/icing/a/a;->k:Lcom/google/android/gms/common/a/d;

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/d/b;->a(Lcom/google/android/gms/common/a/d;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    :goto_0
    return-void

    .line 70
    :cond_0
    new-instance v0, Lcom/google/k/f/an;

    invoke-direct {v0}, Lcom/google/k/f/an;-><init>()V

    .line 71
    new-instance v1, Lcom/google/k/f/ao;

    invoke-direct {v1}, Lcom/google/k/f/ao;-><init>()V

    iput-object v1, v0, Lcom/google/k/f/an;->a:Lcom/google/k/f/ao;

    .line 72
    iget-object v1, v0, Lcom/google/k/f/an;->a:Lcom/google/k/f/ao;

    iput-object p1, v1, Lcom/google/k/f/ao;->a:Ljava/lang/String;

    .line 73
    const-string v1, "error"

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/icing/impl/d/b;->a(Ljava/lang/String;Lcom/google/k/f/an;Z)V

    goto :goto_0
.end method

.method protected abstract a(Ljava/lang/String;Lcom/google/k/f/an;Z)V
.end method
