.class public final Lcom/google/android/gms/icing/u;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 7376
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 7377
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/icing/u;->a:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/u;->b:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/u;->cachedSize:I

    .line 7378
    return-void
.end method

.method public static a([B)Lcom/google/android/gms/icing/u;
    .locals 1

    .prologue
    .line 7469
    new-instance v0, Lcom/google/android/gms/icing/u;

    invoke-direct {v0}, Lcom/google/android/gms/icing/u;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/u;

    return-object v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 7428
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 7429
    iget-wide v2, p0, Lcom/google/android/gms/icing/u;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 7430
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/icing/u;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7433
    :cond_0
    iget v1, p0, Lcom/google/android/gms/icing/u;->b:I

    if-eqz v1, :cond_1

    .line 7434
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/icing/u;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7437
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 7389
    if-ne p1, p0, :cond_1

    .line 7402
    :cond_0
    :goto_0
    return v0

    .line 7392
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/u;

    if-nez v2, :cond_2

    move v0, v1

    .line 7393
    goto :goto_0

    .line 7395
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/u;

    .line 7396
    iget-wide v2, p0, Lcom/google/android/gms/icing/u;->a:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/u;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 7397
    goto :goto_0

    .line 7399
    :cond_3
    iget v2, p0, Lcom/google/android/gms/icing/u;->b:I

    iget v3, p1, Lcom/google/android/gms/icing/u;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 7400
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    .line 7407
    iget-wide v0, p0, Lcom/google/android/gms/icing/u;->a:J

    iget-wide v2, p0, Lcom/google/android/gms/icing/u;->a:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    .line 7410
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/u;->b:I

    add-int/2addr v0, v1

    .line 7411
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 7353
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/u;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/u;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 7417
    iget-wide v0, p0, Lcom/google/android/gms/icing/u;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 7418
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/icing/u;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 7420
    :cond_0
    iget v0, p0, Lcom/google/android/gms/icing/u;->b:I

    if-eqz v0, :cond_1

    .line 7421
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/icing/u;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 7423
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 7424
    return-void
.end method
