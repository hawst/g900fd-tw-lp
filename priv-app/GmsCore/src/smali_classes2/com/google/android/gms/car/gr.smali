.class final Lcom/google/android/gms/car/gr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/LinkedList;

.field b:I


# direct methods
.method constructor <init>(I)V
    .locals 1

    .prologue
    .line 793
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 788
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/gr;->a:Ljava/util/LinkedList;

    .line 794
    iput p1, p0, Lcom/google/android/gms/car/gr;->b:I

    .line 795
    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/gms/car/gp;)Lcom/google/android/gms/car/gq;
    .locals 3

    .prologue
    .line 834
    iget-object v0, p0, Lcom/google/android/gms/car/gr;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/gq;

    .line 835
    iget-object v2, v0, Lcom/google/android/gms/car/gq;->a:Lcom/google/android/gms/car/gp;

    if-ne v2, p1, :cond_0

    .line 839
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final a(Lcom/google/android/gms/car/CarSensorEvent;)V
    .locals 3

    .prologue
    .line 843
    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 844
    const-string v0, "CAR.SENSOR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSensorUpdate to clients: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/gr;->a:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 846
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gr;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/gq;

    .line 847
    iget-object v0, v0, Lcom/google/android/gms/car/gq;->a:Lcom/google/android/gms/car/gp;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/gp;->a(Lcom/google/android/gms/car/CarSensorEvent;)V

    goto :goto_0

    .line 849
    :cond_1
    return-void
.end method
