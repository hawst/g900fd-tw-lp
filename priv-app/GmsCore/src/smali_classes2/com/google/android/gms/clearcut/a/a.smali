.class public final Lcom/google/android/gms/clearcut/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/clearcut/e;


# instance fields
.field private final a:Ljava/lang/Object;

.field private b:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/clearcut/a/a;->a:Ljava/lang/Object;

    .line 117
    return-void
.end method

.method private final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/clearcut/a/e;)Lcom/google/android/gms/common/api/am;
    .locals 2

    .prologue
    .line 46
    iget-object v1, p0, Lcom/google/android/gms/clearcut/a/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 47
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/clearcut/a/a;->b:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_0

    .line 48
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/clearcut/a/a;->b:Ljava/util/concurrent/ExecutorService;

    .line 50
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/clearcut/a/a;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/gms/clearcut/a/b;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/clearcut/a/b;-><init>(Lcom/google/android/gms/clearcut/a/a;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/clearcut/a/e;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 58
    return-object p2

    .line 50
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static a(Lcom/google/android/gms/clearcut/LogEventParcelable;)V
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->e:Lcom/google/android/gms/clearcut/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->d:Lcom/google/af/a/d/a/a/j;

    iget-object v0, v0, Lcom/google/af/a/d/a/a/j;->h:[B

    array-length v0, v0

    if-nez v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->d:Lcom/google/af/a/d/a/a/j;

    iget-object v1, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->e:Lcom/google/android/gms/clearcut/d;

    invoke-interface {v1}, Lcom/google/android/gms/clearcut/d;->a()[B

    move-result-object v1

    iput-object v1, v0, Lcom/google/af/a/d/a/a/j;->h:[B

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->d:Lcom/google/af/a/d/a/a/j;

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->c:[B

    .line 174
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/clearcut/LogEventParcelable;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 113
    invoke-static {p2}, Lcom/google/android/gms/clearcut/a/a;->a(Lcom/google/android/gms/clearcut/LogEventParcelable;)V

    .line 114
    new-instance v0, Lcom/google/android/gms/clearcut/a/f;

    invoke-direct {v0, p2, p1}, Lcom/google/android/gms/clearcut/a/f;-><init>(Lcom/google/android/gms/clearcut/LogEventParcelable;Lcom/google/android/gms/common/api/v;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;JLjava/util/concurrent/TimeUnit;)Z
    .locals 6

    .prologue
    .line 63
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 64
    new-instance v1, Lcom/google/android/gms/clearcut/a/c;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/gms/clearcut/a/c;-><init>(Lcom/google/android/gms/clearcut/a/a;Lcom/google/android/gms/common/api/v;Ljava/util/concurrent/CountDownLatch;)V

    .line 78
    iget-object v2, p0, Lcom/google/android/gms/clearcut/a/a;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 79
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/clearcut/a/a;->b:Ljava/util/concurrent/ExecutorService;

    if-nez v3, :cond_0

    .line 81
    invoke-interface {p1, v1}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    .line 92
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    :try_start_1
    invoke-virtual {v0, p2, p3, p4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    .line 99
    :goto_1
    return v0

    .line 85
    :cond_0
    :try_start_2
    iget-object v3, p0, Lcom/google/android/gms/clearcut/a/a;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/google/android/gms/clearcut/a/d;

    invoke-direct {v4, p0, p1, v1}, Lcom/google/android/gms/clearcut/a/d;-><init>(Lcom/google/android/gms/clearcut/a/a;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/clearcut/a/e;)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 98
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 99
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/clearcut/LogEventParcelable;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 107
    new-instance v0, Lcom/google/android/gms/clearcut/a/f;

    invoke-direct {v0, p2, p1}, Lcom/google/android/gms/clearcut/a/f;-><init>(Lcom/google/android/gms/clearcut/LogEventParcelable;Lcom/google/android/gms/common/api/v;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/clearcut/a/a;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/clearcut/a/e;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    return-object v0
.end method
