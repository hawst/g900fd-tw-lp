.class final enum Lcom/google/android/gms/common/internal/af;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/common/internal/af;

.field public static final enum b:Lcom/google/android/gms/common/internal/af;

.field public static final enum c:Lcom/google/android/gms/common/internal/af;

.field public static final enum d:Lcom/google/android/gms/common/internal/af;

.field private static final synthetic e:[Lcom/google/android/gms/common/internal/af;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 540
    new-instance v0, Lcom/google/android/gms/common/internal/af;

    const-string v1, "READY"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/internal/af;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/common/internal/af;->a:Lcom/google/android/gms/common/internal/af;

    new-instance v0, Lcom/google/android/gms/common/internal/af;

    const-string v1, "NOT_READY"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/common/internal/af;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/common/internal/af;->b:Lcom/google/android/gms/common/internal/af;

    new-instance v0, Lcom/google/android/gms/common/internal/af;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/common/internal/af;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/common/internal/af;->c:Lcom/google/android/gms/common/internal/af;

    new-instance v0, Lcom/google/android/gms/common/internal/af;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/common/internal/af;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/common/internal/af;->d:Lcom/google/android/gms/common/internal/af;

    .line 539
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/gms/common/internal/af;

    sget-object v1, Lcom/google/android/gms/common/internal/af;->a:Lcom/google/android/gms/common/internal/af;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/common/internal/af;->b:Lcom/google/android/gms/common/internal/af;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/gms/common/internal/af;->c:Lcom/google/android/gms/common/internal/af;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/gms/common/internal/af;->d:Lcom/google/android/gms/common/internal/af;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/gms/common/internal/af;->e:[Lcom/google/android/gms/common/internal/af;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 539
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/common/internal/af;
    .locals 1

    .prologue
    .line 539
    const-class v0, Lcom/google/android/gms/common/internal/af;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/internal/af;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/common/internal/af;
    .locals 1

    .prologue
    .line 539
    sget-object v0, Lcom/google/android/gms/common/internal/af;->e:[Lcom/google/android/gms/common/internal/af;

    invoke-virtual {v0}, [Lcom/google/android/gms/common/internal/af;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/common/internal/af;

    return-object v0
.end method
