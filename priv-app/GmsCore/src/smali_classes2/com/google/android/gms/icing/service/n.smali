.class final Lcom/google/android/gms/icing/service/n;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/icing/impl/e/h;

.field private final d:Lcom/google/android/gms/icing/impl/e/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/icing/impl/e/h;Lcom/google/android/gms/icing/impl/e/a;)V
    .locals 0

    .prologue
    .line 140
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 141
    iput-object p2, p0, Lcom/google/android/gms/icing/service/n;->a:Lcom/google/android/gms/icing/impl/e/h;

    .line 142
    iput-object p3, p0, Lcom/google/android/gms/icing/service/n;->d:Lcom/google/android/gms/icing/impl/e/a;

    .line 143
    return-void
.end method


# virtual methods
.method public final b(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/icing/service/n;->b:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/icing/service/n;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Landroid/content/Context;)V

    .line 153
    new-instance v0, Lcom/google/android/gms/icing/service/g;

    iget-object v1, p0, Lcom/google/android/gms/icing/service/n;->b:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/icing/service/n;->a:Lcom/google/android/gms/icing/impl/e/h;

    iget-object v4, p0, Lcom/google/android/gms/icing/service/n;->d:Lcom/google/android/gms/icing/impl/e/a;

    invoke-static {}, Lcom/google/android/gms/common/ew;->a()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/google/android/gms/icing/a/a;->x:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v5, 0x1

    :goto_0
    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/icing/service/g;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/icing/impl/e/h;Lcom/google/android/gms/icing/impl/e/a;Z)V

    const/4 v1, 0x0

    invoke-interface {p1, v6, v0, v1}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    .line 156
    return-void

    :cond_0
    move v5, v6

    .line 153
    goto :goto_0
.end method
