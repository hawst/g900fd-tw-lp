.class public final Lcom/google/android/gms/games/h/b;
.super Lcom/google/android/gms/common/server/n;
.source "SourceFile"


# static fields
.field private static final f:Ljava/util/HashMap;

.field private static final g:Ljava/lang/Object;


# instance fields
.field private final h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/h/b;->f:Ljava/util/HashMap;

    .line 65
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/h/b;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZZZ)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 85
    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move v4, p3

    move v5, p4

    move-object v6, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/common/server/n;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    .line 87
    iput-boolean p2, p0, Lcom/google/android/gms/games/h/b;->h:Z

    .line 88
    return-void
.end method

.method public static a(Lcom/google/android/gms/common/server/ClientContext;I)V
    .locals 3

    .prologue
    .line 195
    sget-object v1, Lcom/google/android/gms/games/h/b;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 196
    const/16 v0, 0x1110

    if-ne p1, v0, :cond_0

    .line 197
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/h/b;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    :goto_0
    monitor-exit v1

    return-void

    .line 199
    :cond_0
    sget-object v0, Lcom/google/android/gms/games/h/b;->f:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, p0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 201
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static e(Lcom/google/android/gms/common/server/ClientContext;)I
    .locals 2

    .prologue
    .line 205
    sget-object v1, Lcom/google/android/gms/games/h/b;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 206
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/h/b;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 207
    if-nez v0, :cond_0

    const/16 v0, 0x1110

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    goto :goto_0

    .line 208
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/util/HashMap;)Lcom/google/android/gms/common/server/h;
    .locals 12

    .prologue
    .line 176
    new-instance v0, Lcom/google/android/gms/games/h/a;

    iget-boolean v10, p0, Lcom/google/android/gms/games/h/b;->b:Z

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gms/games/h/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 9

    .prologue
    const/4 v6, -0x1

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 149
    :try_start_0
    invoke-super/range {p0 .. p5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    .line 151
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "Null response received for path "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 152
    :catch_0
    move-exception v0

    move-object v3, v0

    .line 153
    iget-object v0, v3, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v0, :cond_0

    iget-object v0, v3, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v0, v0, Lcom/android/volley/m;->a:I

    packed-switch v0, :pswitch_data_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    :cond_0
    move-object v0, v4

    .line 154
    :goto_1
    if-eqz v0, :cond_7

    .line 160
    iget-object v1, p0, Lcom/google/android/gms/games/h/b;->a:Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/h/b;->b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 161
    iget-object v1, p0, Lcom/google/android/gms/games/h/b;->a:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/common/server/ClientContext;->b(Landroid/content/Context;)V

    .line 162
    throw v0

    :pswitch_0
    move v0, v1

    .line 153
    goto :goto_0

    :cond_1
    invoke-static {v3, v4}, Lcom/google/android/gms/common/server/b/c;->b(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/a;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/b/a;->b()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_3

    :cond_2
    move-object v0, v4

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/b/a;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/b/a;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_4
    move v0, v6

    :goto_2
    packed-switch v0, :pswitch_data_1

    move-object v5, v4

    :goto_3
    if-nez v5, :cond_5

    move-object v0, v4

    goto :goto_1

    :sswitch_0
    const-string v0, "NotLicensed"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    goto :goto_2

    :sswitch_1
    const-string v0, "UserRegistrationIncomplete"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    goto :goto_2

    :sswitch_2
    const-string v0, "UnregisteredClientId"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v5

    goto :goto_2

    :sswitch_3
    const-string v0, "accessNotConfigured"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x3

    goto :goto_2

    :sswitch_4
    const-string v0, "userRateLimitExceeded"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x4

    goto :goto_2

    :sswitch_5
    const-string v0, "ApiAccessDenied"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    goto :goto_2

    :sswitch_6
    const-string v0, "UserNotAllowed"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x6

    goto :goto_2

    :sswitch_7
    const-string v0, "ApplicationRequestNotAllowed"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x7

    goto :goto_2

    :sswitch_8
    const-string v0, "RestrictedDomainUser"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0x8

    goto :goto_2

    :pswitch_1
    invoke-static {}, Lcom/google/android/gms/games/h/c/a;->d()Lcom/google/android/gms/games/h/c/b;

    move-result-object v0

    const/4 v5, 0x7

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/h/c/b;->a(I)Lcom/google/android/gms/games/h/c/b;

    move-result-object v0

    move-object v5, v0

    goto :goto_3

    :pswitch_2
    invoke-static {}, Lcom/google/android/gms/games/h/c/a;->d()Lcom/google/android/gms/games/h/c/b;

    move-result-object v0

    const/16 v7, 0x5dc

    iput v7, v0, Lcom/google/android/gms/games/h/c/b;->a:I

    iput v5, v0, Lcom/google/android/gms/games/h/c/b;->b:I

    move-object v5, v0

    goto :goto_3

    :pswitch_3
    invoke-static {}, Lcom/google/android/gms/games/h/c/a;->d()Lcom/google/android/gms/games/h/c/b;

    move-result-object v0

    const/16 v7, 0x3ea

    iput v7, v0, Lcom/google/android/gms/games/h/c/b;->a:I

    iput v5, v0, Lcom/google/android/gms/games/h/c/b;->b:I

    move-object v5, v0

    goto/16 :goto_3

    :pswitch_4
    invoke-static {}, Lcom/google/android/gms/games/h/c/a;->d()Lcom/google/android/gms/games/h/c/b;

    move-result-object v0

    const/16 v5, 0x3eb

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/h/c/b;->a(I)Lcom/google/android/gms/games/h/c/b;

    move-result-object v0

    move-object v5, v0

    goto/16 :goto_3

    :pswitch_5
    const-string v0, "GamesServer"

    const-string v5, "Attempting to access a resource for another application. Check your resource IDs."

    invoke-static {v0, v5}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/games/h/c/a;->d()Lcom/google/android/gms/games/h/c/b;

    move-result-object v0

    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/h/c/b;->a(I)Lcom/google/android/gms/games/h/c/b;

    move-result-object v0

    move-object v5, v0

    goto/16 :goto_3

    :pswitch_6
    invoke-static {}, Lcom/google/android/gms/games/h/c/a;->d()Lcom/google/android/gms/games/h/c/b;

    move-result-object v0

    const/16 v5, 0x3ec

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/h/c/b;->a(I)Lcom/google/android/gms/games/h/c/b;

    move-result-object v0

    move-object v5, v0

    goto/16 :goto_3

    :cond_5
    iput-object v8, v5, Lcom/google/android/gms/games/h/c/b;->c:Ljava/lang/String;

    iget v0, v5, Lcom/google/android/gms/games/h/c/b;->b:I

    if-eq v0, v6, :cond_6

    move v0, v1

    :goto_4
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    new-instance v0, Lcom/google/android/gms/games/h/c/a;

    invoke-direct {v0, v5, v2}, Lcom/google/android/gms/games/h/c/a;-><init>(Lcom/google/android/gms/games/h/c/b;B)V

    goto/16 :goto_1

    :cond_6
    move v0, v2

    goto :goto_4

    .line 166
    :cond_7
    throw v3

    .line 153
    :pswitch_data_0
    .packed-switch 0x191
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        -0x58c0ba9f -> :sswitch_2
        -0x4f7cc7ea -> :sswitch_0
        -0x290a755d -> :sswitch_4
        -0x23314ec7 -> :sswitch_5
        -0x1cfb940c -> :sswitch_7
        0x186db26d -> :sswitch_3
        0x227cb4c2 -> :sswitch_1
        0x49c1d58a -> :sswitch_8
        0x71693da0 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method protected final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    invoke-static {}, Lcom/google/android/gms/games/c/b;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 114
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/h/b;->c(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/a/a;

    move-result-object v1

    .line 116
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/games/h/b;->a:Landroid/content/Context;

    invoke-interface {v1, v2}, Lcom/google/android/gms/common/server/a/c;->b(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 127
    :goto_0
    return-object v0

    .line 117
    :catch_0
    move-exception v1

    .line 118
    const-string v2, "GamesServer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Auth related exception is being ignored: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 120
    :catch_1
    move-exception v1

    .line 121
    const-string v2, "GamesServer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Auth related exception is being ignored: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/auth/q;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    iget-object v1, p0, Lcom/google/android/gms/games/h/b;->a:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/common/server/ClientContext;->b(Landroid/content/Context;)V

    goto :goto_0
.end method

.method protected final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;
    .locals 6

    .prologue
    .line 183
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/common/server/n;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    .line 184
    const-string v1, "X-Device-ID"

    invoke-static {}, Lcom/google/android/gms/common/util/e;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    const-string v1, "User-Agent"

    const-string v2, "Games Android SDK/1.0-%s"

    invoke-static {p1, v2}, Lcom/google/android/gms/common/server/ab;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/b;->e(Lcom/google/android/gms/common/server/ClientContext;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    return-object v0
.end method

.method protected final a(Lcom/android/volley/p;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 219
    new-instance v0, Lcom/google/android/gms/common/server/b;

    iget-object v1, p0, Lcom/google/android/gms/games/h/b;->a:Landroid/content/Context;

    const/16 v3, 0x1388

    const/4 v4, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/server/b;-><init>(Landroid/content/Context;Ljava/lang/String;IIF)V

    invoke-virtual {p1, v0}, Lcom/android/volley/p;->a(Lcom/android/volley/z;)Lcom/android/volley/p;

    .line 222
    return-void
.end method

.method protected final b()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 108
    iget-boolean v0, p0, Lcom/google/android/gms/games/h/b;->h:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/h/b;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/games/n;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "serverApiVersion1p"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/google/android/gms/games/c/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    sget-object v0, Lcom/google/android/gms/games/c/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    invoke-static {}, Lcom/google/android/gms/games/c/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/games/c/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/h/b;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/games/n;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "serverApiVersion3p"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    sget-object v0, Lcom/google/android/gms/games/c/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    sget-object v0, Lcom/google/android/gms/games/c/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_2
    invoke-static {}, Lcom/google/android/gms/games/c/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/games/c/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 135
    :try_start_0
    invoke-super {p0, p1}, Lcom/google/android/gms/common/server/n;->b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 136
    :catch_0
    move-exception v0

    .line 139
    iget-object v1, p0, Lcom/google/android/gms/games/h/b;->a:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/common/server/ClientContext;->b(Landroid/content/Context;)V

    .line 140
    throw v0
.end method

.method protected final c(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/a/a;
    .locals 2

    .prologue
    .line 92
    new-instance v0, Lcom/google/android/gms/common/server/a/a;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/common/server/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;Z)V

    return-object v0
.end method
