.class public final Lcom/google/android/gms/common/server/f;
.super Lcom/google/android/gms/common/server/h;
.source "SourceFile"


# instance fields
.field private final f:[Lcom/google/android/gms/common/server/g;


# direct methods
.method public constructor <init>(Ljava/lang/String;[Lcom/google/android/gms/common/server/g;Ljava/lang/Class;Ljava/lang/Object;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;)V
    .locals 12

    .prologue
    .line 96
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v4, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gms/common/server/h;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;)V

    .line 99
    iput-object p2, p0, Lcom/google/android/gms/common/server/f;->f:[Lcom/google/android/gms/common/server/g;

    .line 100
    return-void
.end method


# virtual methods
.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    const-string v0, "multipart/related; boundary=END_OF_PART"

    return-object v0
.end method

.method public final m()[B
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 109
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 110
    const/4 v2, 0x0

    .line 112
    :try_start_0
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v5}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 113
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/server/f;->f:[Lcom/google/android/gms/common/server/g;

    array-length v6, v0

    move v2, v4

    .line 114
    :goto_0
    if-ge v2, v6, :cond_4

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/common/server/f;->f:[Lcom/google/android/gms/common/server/g;

    aget-object v7, v0, v2

    .line 116
    const-string v0, "--END_OF_PART\n"

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v8, "Content-Type: "

    invoke-direct {v0, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v7, Lcom/google/android/gms/common/server/g;->a:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, "\n"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 118
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 119
    iget-object v0, v7, Lcom/google/android/gms/common/server/g;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v3

    :goto_1
    if-eqz v0, :cond_1

    .line 120
    iget-object v0, v7, Lcom/google/android/gms/common/server/g;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 126
    :goto_2
    const-string v0, "\n\n"

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 114
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v0, v4

    .line 119
    goto :goto_1

    .line 121
    :cond_1
    iget-object v0, v7, Lcom/google/android/gms/common/server/g;->c:[B

    if-eqz v0, :cond_2

    iget-object v0, v7, Lcom/google/android/gms/common/server/g;->c:[B

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v3

    :goto_3
    if-eqz v0, :cond_3

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/common/server/f;->f:[Lcom/google/android/gms/common/server/g;

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/google/android/gms/common/server/g;->c:[B

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 130
    :catch_0
    move-exception v0

    .line 131
    :goto_4
    :try_start_2
    const-string v2, "ApiaryMRR"

    const-string v3, "Failed to write parts to output stream"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 133
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 135
    :goto_5
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v4

    .line 121
    goto :goto_3

    .line 124
    :cond_3
    :try_start_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Multipart/related part has no content"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 133
    :catchall_0
    move-exception v0

    :goto_6
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    throw v0

    .line 128
    :cond_4
    :try_start_4
    const-string v0, "--END_OF_PART--\n"

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 129
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 133
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    goto :goto_5

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_6

    .line 130
    :catch_1
    move-exception v0

    move-object v1, v2

    goto :goto_4
.end method
