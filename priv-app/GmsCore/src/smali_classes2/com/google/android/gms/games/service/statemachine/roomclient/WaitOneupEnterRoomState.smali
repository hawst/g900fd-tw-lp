.class final Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;
.super Lcom/google/android/gms/games/service/statemachine/roomclient/ca;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field private e:Lcom/google/android/gms/games/internal/ConnectionInfo;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    .line 30
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/internal/ConnectionInfo;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;->e:Lcom/google/android/gms/games/internal/ConnectionInfo;

    .line 37
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;->c()V

    .line 38
    return-void
.end method

.method public final a(Landroid/os/Message;)Z
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 42
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 102
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;->g:Z

    :goto_0
    return v0

    .line 45
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 46
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;->f:Z

    goto :goto_0

    .line 51
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 52
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;->f:Z

    goto :goto_0

    .line 55
    :sswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bi;

    .line 56
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bi;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    invoke-virtual {v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->b()Lcom/google/android/gms/games/service/statemachine/roomclient/ad;

    move-result-object v1

    .line 57
    iget-object v4, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bi;->a:Lcom/google/android/gms/common/data/DataHolder;

    .line 60
    :try_start_0
    invoke-virtual {v4}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v5

    if-eqz v5, :cond_1

    .line 61
    invoke-interface {v1, v4}, Lcom/google/android/gms/games/service/statemachine/roomclient/ad;->a(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v5, v3

    .line 70
    :goto_1
    if-eqz v5, :cond_3

    .line 72
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->b:Lcom/google/android/gms/games/internal/ed;

    iget-object v6, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bi;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    iget-boolean v6, v6, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->i:Z

    invoke-interface {v1, v4, v6}, Lcom/google/android/gms/games/internal/ed;->a(Lcom/google/android/gms/common/data/DataHolder;Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    :goto_2
    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->q:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;

    iget-object v6, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;->e:Lcom/google/android/gms/games/internal/ConnectionInfo;

    iget-object v7, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bi;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    invoke-virtual {v1, v6, v7, v5, v4}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->a(Lcom/google/android/gms/games/internal/ConnectionInfo;Lcom/google/android/gms/games/service/statemachine/roomclient/ag;Ljava/lang/String;Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v1, v3

    .line 88
    :goto_3
    if-eqz v1, :cond_0

    .line 89
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 93
    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bi;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    if-eqz v5, :cond_4

    const/4 v0, 0x1

    :goto_4
    iget-object v2, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    invoke-virtual {v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->b()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/android/gms/games/g/ag;->o:J

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    iput-boolean v0, v1, Lcom/google/android/gms/games/g/ag;->p:Z

    .line 94
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;->f:Z

    goto :goto_0

    .line 63
    :cond_1
    :try_start_3
    new-instance v1, Lcom/google/android/gms/games/multiplayer/realtime/c;

    invoke-direct {v1, v4}, Lcom/google/android/gms/games/multiplayer/realtime/c;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 64
    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/realtime/c;->c()I

    move-result v5

    if-lez v5, :cond_5

    .line 65
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Lcom/google/android/gms/games/multiplayer/realtime/c;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/multiplayer/realtime/Room;

    .line 66
    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->a()Ljava/lang/String;

    move-result-object v5

    .line 67
    iget-object v6, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Entering room: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 88
    :catchall_0
    move-exception v0

    if-eqz v4, :cond_2

    .line 89
    invoke-virtual {v4}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :cond_2
    throw v0

    .line 74
    :catch_0
    move-exception v1

    .line 75
    :try_start_4
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;->a(Landroid/os/RemoteException;)V

    goto :goto_2

    .line 81
    :cond_3
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bi;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    invoke-virtual {v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a()V

    .line 82
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->f:Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bi;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    iget-wide v6, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->k:J

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bi;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    iget-object v3, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->e:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v8, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;->e:Lcom/google/android/gms/games/internal/ConnectionInfo;

    invoke-virtual {v1, v6, v7, v3, v8}, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->a(JLcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/ConnectionInfo;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v1, v4

    goto :goto_3

    :cond_4
    move v0, v2

    .line 93
    goto :goto_4

    .line 99
    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 100
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;->f:Z

    goto/16 :goto_0

    :cond_5
    move-object v5, v3

    goto/16 :goto_1

    .line 42
    nop

    :sswitch_data_0
    .sparse-switch
        0xf -> :sswitch_2
        0x1c -> :sswitch_0
        0x1d -> :sswitch_3
        0x27 -> :sswitch_1
    .end sparse-switch
.end method
