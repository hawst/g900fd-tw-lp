.class final Lcom/google/android/gms/car/c;
.super Landroid/content/pm/PackageInstaller$SessionCallback;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/b;

.field private final b:Ljava/util/Set;

.field private final c:Lcom/google/android/car/fsm/FsmController;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/b;Ljava/util/Set;Lcom/google/android/car/fsm/FsmController;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/google/android/gms/car/c;->a:Lcom/google/android/gms/car/b;

    invoke-direct {p0}, Landroid/content/pm/PackageInstaller$SessionCallback;-><init>()V

    .line 131
    iput-object p2, p0, Lcom/google/android/gms/car/c;->b:Ljava/util/Set;

    .line 132
    iput-object p3, p0, Lcom/google/android/gms/car/c;->c:Lcom/google/android/car/fsm/FsmController;

    .line 133
    return-void
.end method


# virtual methods
.method public final onActiveChanged(IZ)V
    .locals 2

    .prologue
    .line 138
    if-nez p2, :cond_0

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/car/c;->c:Lcom/google/android/car/fsm/FsmController;

    const-string v1, "EVENT_APPLICATION_INSTALLATION_FAILED"

    invoke-virtual {v0, v1}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/car/c;->a:Lcom/google/android/gms/car/b;

    invoke-static {v0}, Lcom/google/android/gms/car/b;->a(Lcom/google/android/gms/car/b;)Landroid/content/pm/PackageInstaller;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/pm/PackageInstaller;->unregisterSessionCallback(Landroid/content/pm/PackageInstaller$SessionCallback;)V

    .line 142
    :cond_0
    return-void
.end method

.method public final onBadgingChanged(I)V
    .locals 0

    .prologue
    .line 147
    return-void
.end method

.method public final onCreated(I)V
    .locals 0

    .prologue
    .line 152
    return-void
.end method

.method public final onFinished(IZ)V
    .locals 2

    .prologue
    .line 161
    if-eqz p2, :cond_1

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/car/c;->b:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 164
    iget-object v0, p0, Lcom/google/android/gms/car/c;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/car/c;->c:Lcom/google/android/car/fsm/FsmController;

    const-string v1, "EVENT_APPLICATIONS_UP_TO_DATE"

    invoke-virtual {v0, v1}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/String;)V

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/car/c;->a:Lcom/google/android/gms/car/b;

    invoke-static {v0}, Lcom/google/android/gms/car/b;->a(Lcom/google/android/gms/car/b;)Landroid/content/pm/PackageInstaller;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/pm/PackageInstaller;->unregisterSessionCallback(Landroid/content/pm/PackageInstaller$SessionCallback;)V

    .line 172
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/c;->c:Lcom/google/android/car/fsm/FsmController;

    const-string v1, "EVENT_APPLICATION_INSTALLATION_FAILED"

    invoke-virtual {v0, v1}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/String;)V

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/car/c;->a:Lcom/google/android/gms/car/b;

    invoke-static {v0}, Lcom/google/android/gms/car/b;->a(Lcom/google/android/gms/car/b;)Landroid/content/pm/PackageInstaller;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/pm/PackageInstaller;->unregisterSessionCallback(Landroid/content/pm/PackageInstaller$SessionCallback;)V

    goto :goto_0
.end method

.method public final onProgressChanged(IF)V
    .locals 0

    .prologue
    .line 178
    return-void
.end method
