.class public final Lcom/google/android/gms/icing/ab;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/android/gms/icing/ab;


# instance fields
.field public a:I

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11973
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 11974
    iput v0, p0, Lcom/google/android/gms/icing/ab;->a:I

    iput v0, p0, Lcom/google/android/gms/icing/ab;->b:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/ab;->cachedSize:I

    .line 11975
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/ab;
    .locals 2

    .prologue
    .line 11956
    sget-object v0, Lcom/google/android/gms/icing/ab;->c:[Lcom/google/android/gms/icing/ab;

    if-nez v0, :cond_1

    .line 11957
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 11959
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/ab;->c:[Lcom/google/android/gms/icing/ab;

    if-nez v0, :cond_0

    .line 11960
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/ab;

    sput-object v0, Lcom/google/android/gms/icing/ab;->c:[Lcom/google/android/gms/icing/ab;

    .line 11962
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 11964
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/ab;->c:[Lcom/google/android/gms/icing/ab;

    return-object v0

    .line 11962
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 12024
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 12025
    iget v1, p0, Lcom/google/android/gms/icing/ab;->a:I

    if-eqz v1, :cond_0

    .line 12026
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/icing/ab;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12029
    :cond_0
    iget v1, p0, Lcom/google/android/gms/icing/ab;->b:I

    if-eqz v1, :cond_1

    .line 12030
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/icing/ab;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12033
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 11986
    if-ne p1, p0, :cond_1

    .line 11999
    :cond_0
    :goto_0
    return v0

    .line 11989
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/ab;

    if-nez v2, :cond_2

    move v0, v1

    .line 11990
    goto :goto_0

    .line 11992
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/ab;

    .line 11993
    iget v2, p0, Lcom/google/android/gms/icing/ab;->a:I

    iget v3, p1, Lcom/google/android/gms/icing/ab;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 11994
    goto :goto_0

    .line 11996
    :cond_3
    iget v2, p0, Lcom/google/android/gms/icing/ab;->b:I

    iget v3, p1, Lcom/google/android/gms/icing/ab;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 11997
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 12004
    iget v0, p0, Lcom/google/android/gms/icing/ab;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 12006
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/ab;->b:I

    add-int/2addr v0, v1

    .line 12007
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 11950
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/ab;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/ab;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 12013
    iget v0, p0, Lcom/google/android/gms/icing/ab;->a:I

    if-eqz v0, :cond_0

    .line 12014
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/icing/ab;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 12016
    :cond_0
    iget v0, p0, Lcom/google/android/gms/icing/ab;->b:I

    if-eqz v0, :cond_1

    .line 12017
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/icing/ab;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 12019
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 12020
    return-void
.end method
