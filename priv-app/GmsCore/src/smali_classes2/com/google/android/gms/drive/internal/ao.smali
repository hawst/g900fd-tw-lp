.class final Lcom/google/android/gms/drive/internal/ao;
.super Lcom/google/android/gms/drive/internal/ai;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/am;

.field final synthetic b:Lcom/google/android/gms/drive/ad;

.field final synthetic d:Lcom/google/android/gms/drive/internal/an;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/internal/an;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/am;Lcom/google/android/gms/drive/ad;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/ao;->d:Lcom/google/android/gms/drive/internal/an;

    iput-object p3, p0, Lcom/google/android/gms/drive/internal/ao;->a:Lcom/google/android/gms/drive/am;

    iput-object p4, p0, Lcom/google/android/gms/drive/internal/ao;->b:Lcom/google/android/gms/drive/ad;

    invoke-direct {p0, p2}, Lcom/google/android/gms/drive/internal/ai;-><init>(Lcom/google/android/gms/common/api/v;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Lcom/google/android/gms/common/api/h;)V
    .locals 6

    .prologue
    .line 153
    check-cast p1, Lcom/google/android/gms/drive/internal/aj;

    iget-object v0, p0, Lcom/google/android/gms/drive/internal/ao;->a:Lcom/google/android/gms/drive/am;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/am;->e()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/aj;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Landroid/content/Context;)V

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/aj;->f()Lcom/google/android/gms/drive/internal/bx;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;

    iget-object v2, p0, Lcom/google/android/gms/drive/internal/ao;->d:Lcom/google/android/gms/drive/internal/an;

    iget-object v2, v2, Lcom/google/android/gms/drive/internal/an;->a:Lcom/google/android/gms/drive/Contents;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/Contents;->b()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/drive/internal/ao;->a:Lcom/google/android/gms/drive/am;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/am;->e()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/drive/internal/ao;->d:Lcom/google/android/gms/drive/internal/an;

    iget-object v4, v4, Lcom/google/android/gms/drive/internal/an;->a:Lcom/google/android/gms/drive/Contents;

    iget-object v5, p0, Lcom/google/android/gms/drive/internal/ao;->b:Lcom/google/android/gms/drive/ad;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;-><init>(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/Contents;Lcom/google/android/gms/drive/ad;)V

    new-instance v2, Lcom/google/android/gms/drive/internal/dh;

    invoke-direct {v2, p0}, Lcom/google/android/gms/drive/internal/dh;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/internal/bx;->a(Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;Lcom/google/android/gms/drive/internal/ca;)V

    return-void
.end method
