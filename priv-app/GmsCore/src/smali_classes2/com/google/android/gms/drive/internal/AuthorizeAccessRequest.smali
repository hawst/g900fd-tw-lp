.class public Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field final b:J

.field final c:Lcom/google/android/gms/drive/DriveId;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/gms/drive/internal/b;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IJLcom/google/android/gms/drive/DriveId;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput p1, p0, Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;->a:I

    .line 55
    iput-wide p2, p0, Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;->b:J

    .line 56
    iput-object p4, p0, Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;->c:Lcom/google/android/gms/drive/DriveId;

    .line 57
    return-void
.end method

.method public constructor <init>(JLcom/google/android/gms/drive/DriveId;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;-><init>(IJLcom/google/android/gms/drive/DriveId;)V

    .line 62
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;->b:J

    return-wide v0
.end method

.method public final b()Lcom/google/android/gms/drive/DriveId;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;->c:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 41
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/drive/internal/b;->a(Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;Landroid/os/Parcel;I)V

    .line 42
    return-void
.end method
