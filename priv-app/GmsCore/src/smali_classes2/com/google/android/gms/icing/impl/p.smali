.class public final Lcom/google/android/gms/icing/impl/p;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Lcom/google/android/gms/icing/aa;

.field static final b:Lcom/google/android/gms/icing/ab;

.field static final c:Lcom/google/android/gms/icing/aa;

.field static final d:I

.field private static final e:Landroid/util/SparseBooleanArray;

.field private static final f:Landroid/util/SparseArray;

.field private static final g:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0xec

    const/16 v7, 0xea

    const/16 v6, 0x14

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 23
    new-instance v0, Lcom/google/android/gms/icing/aa;

    invoke-direct {v0}, Lcom/google/android/gms/icing/aa;-><init>()V

    sput-object v0, Lcom/google/android/gms/icing/impl/p;->a:Lcom/google/android/gms/icing/aa;

    .line 26
    new-instance v0, Lcom/google/android/gms/icing/ab;

    invoke-direct {v0}, Lcom/google/android/gms/icing/ab;-><init>()V

    sput-object v0, Lcom/google/android/gms/icing/impl/p;->b:Lcom/google/android/gms/icing/ab;

    .line 31
    new-instance v0, Lcom/google/android/gms/icing/aa;

    invoke-direct {v0}, Lcom/google/android/gms/icing/aa;-><init>()V

    .line 32
    sget-object v1, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xe

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, v0, Lcom/google/android/gms/icing/aa;->a:I

    .line 33
    iput v5, v0, Lcom/google/android/gms/icing/aa;->b:I

    .line 34
    iput v6, v0, Lcom/google/android/gms/icing/aa;->c:I

    .line 35
    iput v6, v0, Lcom/google/android/gms/icing/aa;->d:I

    .line 36
    sput-object v0, Lcom/google/android/gms/icing/impl/p;->c:Lcom/google/android/gms/icing/aa;

    .line 39
    sget-object v0, Lcom/google/android/gms/icing/impl/p;->c:Lcom/google/android/gms/icing/aa;

    iget v0, v0, Lcom/google/android/gms/icing/aa;->c:I

    mul-int/lit8 v0, v0, 0xa

    mul-int/lit8 v0, v0, 0x64

    sput v0, Lcom/google/android/gms/icing/impl/p;->d:I

    .line 43
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    sput-object v0, Lcom/google/android/gms/icing/impl/p;->e:Landroid/util/SparseBooleanArray;

    .line 46
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/android/gms/icing/impl/p;->f:Landroid/util/SparseArray;

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/icing/impl/p;->g:Ljava/util/Map;

    .line 55
    const-string v0, "icing__disable_icing_results_for_query_universal"

    invoke-static {v7, v4, v4}, Lcom/google/android/gms/icing/impl/p;->a(IZZ)V

    sget-object v1, Lcom/google/android/gms/icing/impl/p;->g:Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    const/16 v0, 0xeb

    const-string v1, ""

    const-string v2, "icing__section_weights_override_json"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/impl/p;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v0, "icing__scoring_normalization_algo"

    invoke-static {v8, v4, v4}, Lcom/google/android/gms/icing/impl/p;->a(IZI)V

    sget-object v1, Lcom/google/android/gms/icing/impl/p;->g:Ljava/util/Map;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    const/16 v0, 0xed

    const-string v1, ""

    const-string v2, "icing__package_push_index_flags"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/impl/p;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 73
    const/16 v0, 0x10a

    const/16 v1, 0x80

    invoke-static {v0, v5, v1}, Lcom/google/android/gms/icing/impl/p;->a(IZI)V

    .line 74
    const/16 v0, 0x10b

    const/16 v1, 0x10

    invoke-static {v0, v5, v1}, Lcom/google/android/gms/icing/impl/p;->a(IZI)V

    .line 75
    const/16 v0, 0x10c

    const/16 v1, 0x8

    invoke-static {v0, v5, v1}, Lcom/google/android/gms/icing/impl/p;->a(IZI)V

    .line 76
    const/16 v0, 0x10d

    const/16 v1, 0xc0

    invoke-static {v0, v5, v1}, Lcom/google/android/gms/icing/impl/p;->a(IZI)V

    .line 77
    const/16 v0, 0x10e

    const/16 v1, 0x40

    invoke-static {v0, v5, v1}, Lcom/google/android/gms/icing/impl/p;->a(IZI)V

    .line 80
    const/16 v0, 0x116

    const/16 v1, 0x3e8

    invoke-static {v0, v5, v1}, Lcom/google/android/gms/icing/impl/p;->a(IZI)V

    .line 83
    const/16 v0, 0x134

    const-string v1, ""

    invoke-static {v0, v5, v1}, Lcom/google/android/gms/icing/impl/p;->a(IZLjava/lang/String;)V

    .line 88
    const/16 v0, 0x115

    sget v1, Lcom/google/android/gms/icing/impl/p;->d:I

    invoke-static {v0, v5, v1}, Lcom/google/android/gms/icing/impl/p;->a(IZI)V

    .line 93
    const/16 v0, 0x13f

    const-string v1, ""

    invoke-static {v0, v4, v1}, Lcom/google/android/gms/icing/impl/p;->a(IZLjava/lang/String;)V

    .line 96
    const/16 v0, 0x151

    invoke-static {v0, v4, v4}, Lcom/google/android/gms/icing/impl/p;->a(IZZ)V

    .line 98
    const/16 v0, 0x158

    invoke-static {v0, v4, v4}, Lcom/google/android/gms/icing/impl/p;->a(IZI)V

    .line 99
    return-void
.end method

.method static a(I)I
    .locals 3

    .prologue
    .line 141
    sget-object v0, Lcom/google/android/gms/icing/impl/p;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/a/a/c;

    .line 142
    if-eqz v0, :cond_0

    .line 143
    iget v0, v0, Lcom/google/r/a/a/c;->e:I

    return v0

    .line 145
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Flag not found: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a()Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 199
    sget-object v0, Lcom/google/android/gms/icing/impl/p;->f:Landroid/util/SparseArray;

    return-object v0
.end method

.method public static a(Lcom/google/r/a/a/b;)Lcom/google/r/a/a/b;
    .locals 6

    .prologue
    .line 208
    iget-object v2, p0, Lcom/google/r/a/a/b;->a:[Lcom/google/r/a/a/c;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 209
    iget-object v0, v4, Lcom/google/r/a/a/c;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    sget-object v0, Lcom/google/android/gms/icing/impl/p;->g:Ljava/util/Map;

    iget-object v5, v4, Lcom/google/r/a/a/c;->a:Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 211
    sget-object v0, Lcom/google/android/gms/icing/impl/p;->g:Ljava/util/Map;

    iget-object v5, v4, Lcom/google/r/a/a/c;->a:Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v4, Lcom/google/r/a/a/c;->b:I

    .line 212
    const-string v0, ""

    iput-object v0, v4, Lcom/google/r/a/a/c;->a:Ljava/lang/String;

    .line 208
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 216
    :cond_1
    return-object p0
.end method

.method private static a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 109
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/google/android/gms/icing/impl/p;->a(IZLjava/lang/String;)V

    .line 110
    sget-object v0, Lcom/google/android/gms/icing/impl/p;->g:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    return-void
.end method

.method private static a(IZI)V
    .locals 1

    .prologue
    .line 127
    new-instance v0, Lcom/google/r/a/a/c;

    invoke-direct {v0}, Lcom/google/r/a/a/c;-><init>()V

    .line 128
    iput p0, v0, Lcom/google/r/a/a/c;->b:I

    .line 129
    iput p2, v0, Lcom/google/r/a/a/c;->e:I

    .line 130
    invoke-static {p0, p1, v0}, Lcom/google/android/gms/icing/impl/p;->a(IZLcom/google/r/a/a/c;)V

    .line 131
    return-void
.end method

.method private static a(IZLcom/google/r/a/a/c;)V
    .locals 1

    .prologue
    .line 169
    sget-object v0, Lcom/google/android/gms/icing/impl/p;->e:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p0, p1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 170
    sget-object v0, Lcom/google/android/gms/icing/impl/p;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, p0, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 171
    return-void
.end method

.method private static a(IZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 134
    new-instance v0, Lcom/google/r/a/a/c;

    invoke-direct {v0}, Lcom/google/r/a/a/c;-><init>()V

    .line 135
    iput p0, v0, Lcom/google/r/a/a/c;->b:I

    .line 136
    iput-object p2, v0, Lcom/google/r/a/a/c;->d:Ljava/lang/String;

    .line 137
    invoke-static {p0, p1, v0}, Lcom/google/android/gms/icing/impl/p;->a(IZLcom/google/r/a/a/c;)V

    .line 138
    return-void
.end method

.method private static a(IZZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 120
    new-instance v0, Lcom/google/r/a/a/c;

    invoke-direct {v0}, Lcom/google/r/a/a/c;-><init>()V

    .line 121
    iput p0, v0, Lcom/google/r/a/a/c;->b:I

    .line 122
    iput-boolean v1, v0, Lcom/google/r/a/a/c;->c:Z

    .line 123
    invoke-static {p0, v1, v0}, Lcom/google/android/gms/icing/impl/p;->a(IZLcom/google/r/a/a/c;)V

    .line 124
    return-void
.end method

.method public static a(Ljava/lang/Integer;)Z
    .locals 2

    .prologue
    .line 192
    sget-object v0, Lcom/google/android/gms/icing/impl/p;->e:Landroid/util/SparseBooleanArray;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    return v0
.end method

.method static b(I)Z
    .locals 3

    .prologue
    .line 150
    sget-object v0, Lcom/google/android/gms/icing/impl/p;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/a/a/c;

    .line 151
    if-eqz v0, :cond_0

    .line 152
    iget-boolean v0, v0, Lcom/google/r/a/a/c;->c:Z

    return v0

    .line 154
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Flag not found: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static c(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 159
    sget-object v0, Lcom/google/android/gms/icing/impl/p;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/a/a/c;

    .line 160
    if-eqz v0, :cond_0

    .line 161
    iget-object v0, v0, Lcom/google/r/a/a/c;->d:Ljava/lang/String;

    return-object v0

    .line 163
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Flag not found: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static d(I)Z
    .locals 1

    .prologue
    .line 184
    sget-object v0, Lcom/google/android/gms/icing/impl/p;->e:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseBooleanArray;->indexOfKey(I)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
