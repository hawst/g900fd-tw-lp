.class public final Lcom/google/android/gms/icing/n;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:J

.field public c:I

.field public d:J

.field public e:J

.field public f:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    .line 3041
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 3042
    iput v2, p0, Lcom/google/android/gms/icing/n;->a:I

    iput-wide v0, p0, Lcom/google/android/gms/icing/n;->b:J

    iput v2, p0, Lcom/google/android/gms/icing/n;->c:I

    iput-wide v0, p0, Lcom/google/android/gms/icing/n;->d:J

    iput-wide v0, p0, Lcom/google/android/gms/icing/n;->e:J

    iput-wide v0, p0, Lcom/google/android/gms/icing/n;->f:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/n;->cachedSize:I

    .line 3043
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 3128
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 3129
    iget v1, p0, Lcom/google/android/gms/icing/n;->a:I

    if-eqz v1, :cond_0

    .line 3130
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/icing/n;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3133
    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/icing/n;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 3134
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/icing/n;->b:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3137
    :cond_1
    iget v1, p0, Lcom/google/android/gms/icing/n;->c:I

    if-eqz v1, :cond_2

    .line 3138
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/icing/n;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3141
    :cond_2
    iget-wide v2, p0, Lcom/google/android/gms/icing/n;->d:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 3142
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/icing/n;->d:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3145
    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/icing/n;->e:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_4

    .line 3146
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/android/gms/icing/n;->e:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3149
    :cond_4
    iget-wide v2, p0, Lcom/google/android/gms/icing/n;->f:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 3150
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/google/android/gms/icing/n;->f:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3153
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3058
    if-ne p1, p0, :cond_1

    .line 3083
    :cond_0
    :goto_0
    return v0

    .line 3061
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/n;

    if-nez v2, :cond_2

    move v0, v1

    .line 3062
    goto :goto_0

    .line 3064
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/n;

    .line 3065
    iget v2, p0, Lcom/google/android/gms/icing/n;->a:I

    iget v3, p1, Lcom/google/android/gms/icing/n;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 3066
    goto :goto_0

    .line 3068
    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/icing/n;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/n;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    .line 3069
    goto :goto_0

    .line 3071
    :cond_4
    iget v2, p0, Lcom/google/android/gms/icing/n;->c:I

    iget v3, p1, Lcom/google/android/gms/icing/n;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 3072
    goto :goto_0

    .line 3074
    :cond_5
    iget-wide v2, p0, Lcom/google/android/gms/icing/n;->d:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/n;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    .line 3075
    goto :goto_0

    .line 3077
    :cond_6
    iget-wide v2, p0, Lcom/google/android/gms/icing/n;->e:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/n;->e:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    move v0, v1

    .line 3078
    goto :goto_0

    .line 3080
    :cond_7
    iget-wide v2, p0, Lcom/google/android/gms/icing/n;->f:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/n;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 3081
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 3088
    iget v0, p0, Lcom/google/android/gms/icing/n;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 3090
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/n;->b:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/n;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 3092
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/n;->c:I

    add-int/2addr v0, v1

    .line 3093
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/n;->d:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/n;->d:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 3095
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/n;->e:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/n;->e:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 3097
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/n;->f:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/n;->f:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 3099
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 3006
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/n;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/n;->b:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/n;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/n;->d:J

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/n;->e:J

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/n;->f:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 3105
    iget v0, p0, Lcom/google/android/gms/icing/n;->a:I

    if-eqz v0, :cond_0

    .line 3106
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/icing/n;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 3108
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/icing/n;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 3109
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/icing/n;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 3111
    :cond_1
    iget v0, p0, Lcom/google/android/gms/icing/n;->c:I

    if-eqz v0, :cond_2

    .line 3112
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/icing/n;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 3114
    :cond_2
    iget-wide v0, p0, Lcom/google/android/gms/icing/n;->d:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 3115
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/icing/n;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 3117
    :cond_3
    iget-wide v0, p0, Lcom/google/android/gms/icing/n;->e:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_4

    .line 3118
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/android/gms/icing/n;->e:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 3120
    :cond_4
    iget-wide v0, p0, Lcom/google/android/gms/icing/n;->f:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_5

    .line 3121
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/android/gms/icing/n;->f:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 3123
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3124
    return-void
.end method
