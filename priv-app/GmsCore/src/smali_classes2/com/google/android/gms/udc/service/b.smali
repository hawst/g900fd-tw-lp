.class final Lcom/google/android/gms/udc/service/b;
.super Lcom/google/android/gms/udc/c/g;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Lcom/google/android/gms/common/server/ClientContext;

.field final c:Lcom/google/android/gms/udc/a/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/gms/udc/c/g;-><init>()V

    .line 80
    iput-object p1, p0, Lcom/google/android/gms/udc/service/b;->a:Landroid/content/Context;

    .line 82
    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    const-string v2, "com.google.android.gms"

    invoke-direct {v0, v1, p2, p2, v2}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/udc/service/b;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 84
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 85
    iget-object v1, p0, Lcom/google/android/gms/udc/service/b;->b:Lcom/google/android/gms/common/server/ClientContext;

    sget-object v0, Lcom/google/android/gms/udc/b/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    .line 87
    :try_start_0
    new-instance v0, Lcom/google/android/gms/common/server/q;

    iget-object v1, p0, Lcom/google/android/gms/udc/service/b;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/udc/b/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/udc/b/a;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    sget-object v4, Lcom/google/android/gms/udc/b/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    sget-object v5, Lcom/google/android/gms/udc/b/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v5}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    sget-object v6, Lcom/google/android/gms/udc/b/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v6}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    sget-object v7, Lcom/google/android/gms/udc/b/a;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v7}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/common/server/q;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    .line 94
    new-instance v1, Lcom/google/android/gms/udc/a/a;

    invoke-direct {v1, v0}, Lcom/google/android/gms/udc/a/a;-><init>(Lcom/google/android/gms/common/server/q;)V

    iput-object v1, p0, Lcom/google/android/gms/udc/service/b;->c:Lcom/google/android/gms/udc/a/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 97
    return-void

    .line 96
    :catchall_0
    move-exception v0

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/udc/c/c;[B)V
    .locals 6

    .prologue
    .line 110
    new-instance v0, Lcom/google/android/gms/udc/d/e;

    iget-object v1, p0, Lcom/google/android/gms/udc/service/b;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/udc/service/b;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v4, p0, Lcom/google/android/gms/udc/service/b;->c:Lcom/google/android/gms/udc/a/a;

    move-object v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/udc/d/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/udc/c/c;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/udc/a/a;[B)V

    .line 112
    iget-object v1, p0, Lcom/google/android/gms/udc/service/b;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/udc/service/UdcWorkService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 113
    return-void
.end method

.method public final b(Lcom/google/android/gms/udc/c/c;[B)V
    .locals 6

    .prologue
    .line 118
    new-instance v0, Lcom/google/android/gms/udc/d/d;

    iget-object v1, p0, Lcom/google/android/gms/udc/service/b;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/udc/service/b;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v4, p0, Lcom/google/android/gms/udc/service/b;->c:Lcom/google/android/gms/udc/a/a;

    move-object v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/udc/d/d;-><init>(Landroid/content/Context;Lcom/google/android/gms/udc/c/c;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/udc/a/a;[B)V

    .line 120
    iget-object v1, p0, Lcom/google/android/gms/udc/service/b;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/udc/service/UdcWorkService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 121
    return-void
.end method

.method public final c(Lcom/google/android/gms/udc/c/c;[B)V
    .locals 6

    .prologue
    .line 126
    new-instance v0, Lcom/google/android/gms/udc/d/c;

    iget-object v1, p0, Lcom/google/android/gms/udc/service/b;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/udc/service/b;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v4, p0, Lcom/google/android/gms/udc/service/b;->c:Lcom/google/android/gms/udc/a/a;

    move-object v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/udc/d/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/udc/c/c;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/udc/a/a;[B)V

    .line 128
    iget-object v1, p0, Lcom/google/android/gms/udc/service/b;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/udc/service/UdcWorkService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 129
    return-void
.end method

.method public final d(Lcom/google/android/gms/udc/c/c;[B)V
    .locals 6

    .prologue
    .line 134
    new-instance v0, Lcom/google/android/gms/udc/d/f;

    iget-object v1, p0, Lcom/google/android/gms/udc/service/b;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/udc/service/b;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v4, p0, Lcom/google/android/gms/udc/service/b;->c:Lcom/google/android/gms/udc/a/a;

    move-object v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/udc/d/f;-><init>(Landroid/content/Context;Lcom/google/android/gms/udc/c/c;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/udc/a/a;[B)V

    .line 136
    iget-object v1, p0, Lcom/google/android/gms/udc/service/b;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/udc/service/UdcWorkService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 137
    return-void
.end method

.method public final e(Lcom/google/android/gms/udc/c/c;[B)V
    .locals 6

    .prologue
    .line 102
    new-instance v0, Lcom/google/android/gms/udc/d/b;

    iget-object v1, p0, Lcom/google/android/gms/udc/service/b;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/udc/service/b;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v4, p0, Lcom/google/android/gms/udc/service/b;->c:Lcom/google/android/gms/udc/a/a;

    move-object v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/udc/d/b;-><init>(Landroid/content/Context;Lcom/google/android/gms/udc/c/c;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/udc/a/a;[B)V

    .line 104
    iget-object v1, p0, Lcom/google/android/gms/udc/service/b;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/udc/service/UdcWorkService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 105
    return-void
.end method
