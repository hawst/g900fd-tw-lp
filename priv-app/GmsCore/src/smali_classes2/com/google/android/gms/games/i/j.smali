.class final Lcom/google/android/gms/games/i/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/i/f;
.implements Lcom/google/android/gms/games/m;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/i/b;

.field private final b:Lcom/google/android/gms/common/api/aq;

.field private final c:Lcom/google/android/gms/common/api/Status;

.field private final d:Lcom/google/android/gms/games/a;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/i/b;Lcom/google/android/gms/common/api/aq;Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 233
    iput-object p1, p0, Lcom/google/android/gms/games/i/j;->a:Lcom/google/android/gms/games/i/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 234
    iput-object p2, p0, Lcom/google/android/gms/games/i/j;->b:Lcom/google/android/gms/common/api/aq;

    .line 235
    invoke-virtual {p3}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/games/o;->a(I)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/i/j;->c:Lcom/google/android/gms/common/api/Status;

    .line 236
    new-instance v0, Lcom/google/android/gms/games/a;

    invoke-direct {v0, p3}, Lcom/google/android/gms/games/a;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    iput-object v0, p0, Lcom/google/android/gms/games/i/j;->d:Lcom/google/android/gms/games/a;

    .line 237
    return-void
.end method


# virtual methods
.method public final C_()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/gms/games/i/j;->c:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/gms/games/i/j;->b:Lcom/google/android/gms/common/api/aq;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aq;->a(Lcom/google/android/gms/common/api/ap;)V

    .line 242
    return-void
.end method

.method public final c()Lcom/google/android/gms/games/a;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/gms/games/i/j;->d:Lcom/google/android/gms/games/a;

    return-object v0
.end method

.method public final w_()V
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/gms/games/i/j;->d:Lcom/google/android/gms/games/a;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/games/i/j;->d:Lcom/google/android/gms/games/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a;->w_()V

    .line 254
    :cond_0
    return-void
.end method
