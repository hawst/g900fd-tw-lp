.class final Lcom/google/android/gms/car/oj;
.super Lcom/google/android/gms/car/pf;
.source "SourceFile"


# direct methods
.method private constructor <init>(Lcom/google/android/gms/car/oi;)V
    .locals 1

    .prologue
    .line 667
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/car/pf;-><init>(Ljava/lang/Object;Landroid/os/Looper;)V

    .line 668
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/car/oi;B)V
    .locals 0

    .prologue
    .line 657
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/oj;-><init>(Lcom/google/android/gms/car/oi;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/oj;)V
    .locals 1

    .prologue
    .line 657
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/oj;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/oj;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/oj;II)V
    .locals 1

    .prologue
    .line 657
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/gms/car/oj;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/oj;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/oj;Landroid/view/KeyEvent;)V
    .locals 1

    .prologue
    .line 657
    const/16 v0, 0x8

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/car/oj;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/oj;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/oj;Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 657
    const/4 v0, 0x7

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/car/oj;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/oj;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/oj;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 657
    const/4 v2, 0x6

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v2, v0, v1}, Lcom/google/android/gms/car/oj;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/oj;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/car/oj;)V
    .locals 1

    .prologue
    .line 657
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/oj;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/oj;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/car/oj;)V
    .locals 1

    .prologue
    .line 657
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/oj;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/oj;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 672
    iget-object v0, p0, Lcom/google/android/gms/car/pf;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/oi;

    .line 673
    if-nez v0, :cond_0

    .line 703
    :goto_0
    return-void

    .line 677
    :cond_0
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 679
    :pswitch_1
    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/car/oi;->b(II)V

    goto :goto_0

    .line 682
    :pswitch_2
    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->t()V

    goto :goto_0

    .line 685
    :pswitch_3
    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->u()V

    goto :goto_0

    .line 688
    :pswitch_4
    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->v()V

    goto :goto_0

    .line 691
    :pswitch_5
    iget v2, p1, Landroid/os/Message;->arg1:I

    if-ne v2, v1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/oi;->b(Z)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 694
    :pswitch_6
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/view/MotionEvent;

    .line 695
    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/oi;->c(Landroid/view/MotionEvent;)V

    .line 697
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    goto :goto_0

    .line 700
    :pswitch_7
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/view/KeyEvent;

    .line 701
    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/oi;->c(Landroid/view/KeyEvent;)V

    goto :goto_0

    .line 677
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
