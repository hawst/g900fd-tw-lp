.class public final Lcom/google/android/gms/games/ui/headless/requests/a;
.super Lcom/google/android/gms/games/ui/common/requests/l;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/b/a/r;


# instance fields
.field private final a:Lcom/google/android/gms/games/ui/headless/a;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/gms/games/ui/headless/a/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/headless/a;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/requests/l;-><init>()V

    .line 45
    invoke-static {p1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 46
    iput-object p1, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    .line 47
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/headless/a;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->b:Ljava/lang/String;

    .line 49
    new-instance v0, Lcom/google/android/gms/games/ui/headless/a/a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/headless/a/a;-><init>(Lcom/google/android/gms/games/ui/headless/a;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->c:Lcom/google/android/gms/games/ui/headless/a/a;

    .line 50
    return-void
.end method

.method private b(Ljava/util/ArrayList;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 90
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->f()Lcom/google/android/gms/games/Game;

    move-result-object v2

    .line 93
    iget-object v3, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->b:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    invoke-static {v2}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    invoke-static {p1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    :goto_1
    if-ge v1, v5, :cond_1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->c()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "requests"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-static {v0, v4}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    invoke-static {v3, v2, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    const/16 v1, 0x4e21

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/headless/a;->setResult(I)V

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->finish()V

    .line 96
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->c:Lcom/google/android/gms/games/ui/headless/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/headless/a/a;->a(Lcom/google/android/gms/games/Game;)V

    .line 117
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;)V
    .locals 4

    .prologue
    .line 139
    invoke-virtual {p1}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->d()Ljava/util/ArrayList;

    move-result-object v2

    .line 140
    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 141
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/requests/a;->a(Lcom/google/android/gms/games/request/GameRequest;)V

    .line 140
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 143
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 123
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 124
    const-string v0, "HeadlessReqInboxHelper"

    const-string v1, "onRequestClusterSeeMoreClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :goto_0
    return-void

    .line 128
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    invoke-interface {v1, v0, p1, p2}, Lcom/google/android/gms/games/request/d;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 130
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    .line 131
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/headless/a;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/bh;->f()Ljava/lang/String;

    move-result-object v1

    .line 132
    const-string v2, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 133
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    const/16 v2, 0x384

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/games/ui/headless/a;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 5

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 101
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 103
    const-string v0, "HeadlessReqInboxHelper"

    const-string v1, "onRequestDismissed: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :goto_0
    return-void

    .line 107
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/request/GameRequest;->f()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v1

    .line 108
    sget-object v2, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/headless/a;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/bh;->f()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/android/gms/games/request/GameRequest;->e()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v0, v1, v3, v4}, Lcom/google/android/gms/games/request/d;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->F()V

    goto :goto_0
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 3

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v1, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    const-string v0, "HeadlessReqInboxHelper"

    const-string v1, "switchAccountForRequest: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    :goto_0
    return-void

    .line 154
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->f()Lcom/google/android/gms/games/Game;

    move-result-object v0

    .line 155
    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->b:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/headless/requests/a;->b(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public final varargs a([Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 3

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    invoke-static {v1, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    const-string v0, "HeadlessReqInboxHelper"

    const-string v1, "onRequestClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    :goto_0
    return-void

    .line 62
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/common/data/v;->a([Lcom/google/android/gms/common/data/u;)Ljava/util/ArrayList;

    move-result-object v2

    .line 64
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->f()Lcom/google/android/gms/games/Game;

    move-result-object v0

    .line 67
    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 69
    if-nez v0, :cond_1

    .line 72
    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/headless/requests/a;->a(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 73
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 79
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/ui/b/a/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Lcom/google/android/gms/games/ui/b/a/q;

    move-result-object v0

    .line 82
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/requests/a;->a:Lcom/google/android/gms/games/ui/headless/a;

    const-string v2, "com.google.android.gms.games.ui.dialog.changeAccountDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    goto :goto_0

    .line 84
    :cond_2
    invoke-direct {p0, v2}, Lcom/google/android/gms/games/ui/headless/requests/a;->b(Ljava/util/ArrayList;)V

    goto :goto_0
.end method
