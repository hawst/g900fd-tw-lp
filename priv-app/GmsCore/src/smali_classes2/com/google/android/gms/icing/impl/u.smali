.class public final Lcom/google/android/gms/icing/impl/u;
.super Lcom/google/android/gms/appdatasearch/a/c;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Lcom/google/android/gms/icing/impl/a;

.field final c:Lcom/google/android/gms/icing/b/a;

.field final d:Ljava/util/concurrent/Semaphore;

.field e:Lcom/google/android/gms/icing/impl/q;

.field f:Lcom/google/android/gms/icing/impl/bn;

.field g:Lcom/google/android/gms/icing/impl/a/p;

.field h:Lcom/google/android/gms/icing/impl/e/i;

.field i:Lcom/google/android/gms/icing/impl/a/f;

.field j:Lcom/google/android/gms/icing/impl/NativeIndex;

.field k:Lcom/google/android/gms/icing/impl/e;

.field final l:Lcom/google/android/gms/icing/impl/m;

.field final m:Lcom/google/android/gms/icing/impl/k;

.field final n:Z

.field final o:Lcom/google/android/gms/icing/impl/a/i;

.field final p:Ljava/lang/Runnable;

.field private final q:Ljava/lang/String;

.field private final r:Lcom/google/android/gms/icing/impl/aw;

.field private final s:Lcom/google/android/gms/icing/c/b;

.field private t:Lcom/google/android/gms/icing/impl/bf;

.field private final u:Lcom/google/android/gms/icing/impl/a/z;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/icing/b/a;Ljava/lang/String;Lcom/google/android/gms/icing/impl/aw;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 386
    invoke-direct {p0}, Lcom/google/android/gms/appdatasearch/a/c;-><init>()V

    .line 229
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/u;->d:Ljava/util/concurrent/Semaphore;

    .line 250
    new-instance v0, Lcom/google/android/gms/icing/impl/v;

    invoke-direct {v0, p0}, Lcom/google/android/gms/icing/impl/v;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/u;->o:Lcom/google/android/gms/icing/impl/a/i;

    .line 282
    new-instance v0, Lcom/google/android/gms/icing/impl/af;

    invoke-direct {v0, p0}, Lcom/google/android/gms/icing/impl/af;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/u;->u:Lcom/google/android/gms/icing/impl/a/z;

    .line 314
    new-instance v0, Lcom/google/android/gms/icing/impl/ap;

    invoke-direct {v0, p0}, Lcom/google/android/gms/icing/impl/ap;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/u;->p:Ljava/lang/Runnable;

    .line 387
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    .line 388
    iput-object p2, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    .line 389
    iput-object p4, p0, Lcom/google/android/gms/icing/impl/u;->r:Lcom/google/android/gms/icing/impl/aw;

    .line 390
    new-instance v0, Lcom/google/android/gms/icing/service/f;

    invoke-direct {v0, p1}, Lcom/google/android/gms/icing/service/f;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/u;->b:Lcom/google/android/gms/icing/impl/a;

    .line 392
    sget-object v0, Lcom/google/android/gms/icing/a/a;->n:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/gms/icing/impl/d/a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/icing/impl/d/a;-><init>(Landroid/content/Context;)V

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    .line 393
    new-instance v0, Lcom/google/android/gms/icing/impl/l;

    invoke-direct {v0, p1}, Lcom/google/android/gms/icing/impl/l;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/u;->m:Lcom/google/android/gms/icing/impl/k;

    .line 394
    iput-object p3, p0, Lcom/google/android/gms/icing/impl/u;->q:Ljava/lang/String;

    .line 395
    iput-boolean v1, p0, Lcom/google/android/gms/icing/impl/u;->n:Z

    .line 396
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->p:Ljava/lang/Runnable;

    iget-object v2, v0, Lcom/google/android/gms/icing/b/a;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v3, v0, Lcom/google/android/gms/icing/b/a;->c:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/google/android/gms/icing/b/a;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/icing/b/a;->b:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 397
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->r:Lcom/google/android/gms/icing/impl/aw;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/icing/c/a;

    invoke-direct {v1, v0}, Lcom/google/android/gms/icing/c/a;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/icing/impl/u;->s:Lcom/google/android/gms/icing/c/b;

    .line 398
    return-void

    .line 392
    :cond_1
    new-instance v0, Lcom/google/android/gms/icing/impl/d/c;

    invoke-direct {v0, p1}, Lcom/google/android/gms/icing/impl/d/c;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 396
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private J()Z
    .locals 1

    .prologue
    .line 522
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private K()V
    .locals 2

    .prologue
    .line 549
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 550
    new-instance v0, Lcom/google/android/gms/icing/impl/b/c;

    const-string v1, "Not initialized"

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/impl/b/c;-><init>(Ljava/lang/String;)V

    throw v0

    .line 552
    :cond_0
    return-void
.end method

.method private L()Lcom/google/android/gms/icing/b/j;
    .locals 1

    .prologue
    .line 1572
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->k()V

    .line 1573
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/u;->a(Z)Lcom/google/android/gms/icing/b/j;

    move-result-object v0

    return-object v0
.end method

.method private M()Lcom/google/android/gms/icing/b/j;
    .locals 4

    .prologue
    .line 2026
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    .line 2028
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v1, Lcom/google/android/gms/icing/impl/ah;

    invoke-direct {v1, p0}, Lcom/google/android/gms/icing/impl/ah;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    move-result-object v0

    return-object v0
.end method

.method private N()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    .line 2635
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    invoke-virtual {v0, v8}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 2638
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->j()Lcom/google/android/gms/icing/f;

    move-result-object v1

    .line 2640
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/j;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2641
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/f;->b()Ljava/util/Set;

    move-result-object v0

    .line 2642
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/a/f;->d()Ljava/util/Set;

    move-result-object v2

    .line 2643
    invoke-interface {v2, v0}, Ljava/util/Collection;->removeAll(Ljava/util/Collection;)Z

    .line 2644
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/a/e;

    .line 2645
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/e;->i()V

    goto :goto_0

    .line 2649
    :cond_0
    iget v0, v1, Lcom/google/android/gms/icing/f;->e:I

    if-lez v0, :cond_1

    .line 2650
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    const-string v2, "compaction_with_errors"

    invoke-interface {v0, v2}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    .line 2652
    :cond_1
    const-string v0, "Done compaction min disk %.3f%% min index %.3f%% num docs %d old %d trimmed %d err %d"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/u;->f:Lcom/google/android/gms/icing/impl/bn;

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/bn;->c()D

    move-result-wide v4

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/NativeIndex;->o()D

    move-result-wide v4

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    iget v3, v1, Lcom/google/android/gms/icing/f;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    const/4 v3, 0x3

    iget v4, v1, Lcom/google/android/gms/icing/f;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget v4, v1, Lcom/google/android/gms/icing/f;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget v1, v1, Lcom/google/android/gms/icing/f;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 2659
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/ac;)Lcom/google/android/gms/icing/g;
    .locals 2

    .prologue
    .line 755
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 756
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v0

    .line 760
    new-instance v1, Lcom/google/android/gms/icing/impl/at;

    invoke-direct {v1, p0}, Lcom/google/android/gms/icing/impl/at;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    .line 770
    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/ac;Lcom/google/android/gms/icing/impl/a/s;)V

    .line 773
    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/j;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/g;

    move-result-object v0

    .line 774
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/a/j;->b(Lcom/google/android/gms/icing/g;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 775
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->k:Lcom/google/android/gms/icing/impl/e;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/gms/icing/impl/e;->a(Ljava/lang/String;Lcom/google/android/gms/icing/g;)V

    .line 777
    :cond_0
    return-object v0
.end method

.method public static a(Ljava/util/Set;[Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 857
    invoke-interface {p0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 858
    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    .line 859
    const-string v0, "Not authorized to read requested corpora"

    .line 864
    :goto_0
    return-object v0

    .line 861
    :cond_0
    const-string v0, "Found no matching corpora for package"

    goto :goto_0

    .line 864
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)V
    .locals 6

    .prologue
    .line 1108
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    .line 1109
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/f;->a(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/aa;

    move-result-object v0

    .line 1110
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v2, Lcom/google/android/gms/icing/impl/w;

    invoke-direct {v2, p0, v0, p2}, Lcom/google/android/gms/icing/impl/w;-><init>(Lcom/google/android/gms/icing/impl/u;Lcom/google/android/gms/icing/impl/a/aa;Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)V

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/w;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/w;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    .line 1124
    instance-of v1, v0, Lcom/google/android/gms/icing/impl/b/a;

    if-eqz v1, :cond_0

    .line 1125
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1126
    :cond_0
    instance-of v1, v0, Ljava/lang/SecurityException;

    if-eqz v1, :cond_1

    .line 1127
    check-cast v0, Ljava/lang/SecurityException;

    throw v0

    .line 1129
    :cond_1
    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 1130
    return-void

    .line 1129
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lcom/google/android/gms/icing/c/a/t;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1862
    invoke-static {}, Lcom/google/android/gms/common/ew;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "<redacted>"

    .line 1864
    :goto_0
    new-instance v3, Lcom/google/android/gms/lockbox/h;

    invoke-direct {v3}, Lcom/google/android/gms/lockbox/h;-><init>()V

    .line 1865
    iget-object v1, p2, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    array-length v1, v1

    if-lez v1, :cond_2

    .line 1867
    const-string v1, "Notifying GSA of setting change for account:%s changed."

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 1868
    iget-object v1, p2, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    array-length v1, v1

    new-array v4, v1, [I

    move v1, v2

    .line 1869
    :goto_1
    iget-object v5, p2, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    array-length v5, v5

    if-ge v1, v5, :cond_1

    .line 1870
    iget-object v5, p2, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    aget-object v5, v5, v1

    iget v5, v5, Lcom/google/android/gms/udc/e/p;->a:I

    aput v5, v4, v1

    .line 1869
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move-object v0, p1

    .line 1862
    goto :goto_0

    .line 1872
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    invoke-static {v1, p1, v4}, Lcom/google/android/gms/common/internal/ay;->a(Landroid/content/Context;Ljava/lang/String;[I)V

    :cond_2
    move v1, v2

    .line 1874
    :goto_2
    iget-object v4, p2, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    array-length v4, v4

    if-ge v1, v4, :cond_4

    .line 1875
    iget-object v4, p2, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    aget-object v4, v4, v1

    .line 1876
    iget v5, v4, Lcom/google/android/gms/udc/e/p;->b:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_3

    .line 1877
    iget v4, v4, Lcom/google/android/gms/udc/e/p;->a:I

    packed-switch v4, :pswitch_data_0

    .line 1874
    :cond_3
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1882
    :pswitch_0
    const-string v4, "Opting out of web & app history for account:%s"

    invoke-static {v4, v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 1883
    invoke-virtual {v3, v2}, Lcom/google/android/gms/lockbox/h;->a(Z)Lcom/google/android/gms/lockbox/h;

    goto :goto_3

    .line 1886
    :pswitch_1
    const-string v4, "Opting out of device state & content for account:%s"

    invoke-static {v4, v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 1888
    invoke-virtual {v3, v2}, Lcom/google/android/gms/lockbox/h;->b(Z)Lcom/google/android/gms/lockbox/h;

    goto :goto_3

    .line 1892
    :cond_4
    invoke-virtual {v3}, Lcom/google/android/gms/lockbox/h;->a()Lcom/google/android/gms/lockbox/g;

    move-result-object v0

    .line 1893
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->m:Lcom/google/android/gms/icing/impl/k;

    invoke-interface {v1, p1, v0}, Lcom/google/android/gms/icing/impl/k;->a(Ljava/lang/String;Lcom/google/android/gms/lockbox/g;)V

    .line 1894
    return-void

    .line 1877
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private a(I)[Lcom/google/android/gms/icing/c/a/b;
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 1474
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    .line 1477
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->m:Lcom/google/android/gms/icing/impl/k;

    invoke-interface {v0}, Lcom/google/android/gms/icing/impl/k;->a()Ljava/lang/String;

    move-result-object v0

    .line 1478
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/u;->m:Lcom/google/android/gms/icing/impl/k;

    invoke-interface {v2, v0}, Lcom/google/android/gms/icing/impl/k;->a(Ljava/lang/String;)Z

    move-result v0

    .line 1480
    if-nez v0, :cond_0

    move-object v0, v1

    .line 1509
    :goto_0
    return-object v0

    .line 1482
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/a/p;->b:Lcom/google/android/gms/icing/impl/a/j;

    .line 1483
    invoke-interface {v2}, Lcom/google/android/gms/icing/impl/a/x;->b()Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 1484
    :try_start_0
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 1485
    invoke-interface {v2}, Lcom/google/android/gms/icing/impl/a/x;->h()Ljava/util/Set;

    move-result-object v0

    .line 1486
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1487
    invoke-interface {v2, v0}, Lcom/google/android/gms/icing/impl/a/x;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/g;

    move-result-object v0

    .line 1488
    iget v7, v0, Lcom/google/android/gms/icing/g;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1510
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    .line 1490
    :cond_1
    :try_start_1
    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v0

    new-array v6, v0, [I

    .line 1492
    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v3

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 1493
    add-int/lit8 v0, v2, 0x1

    aput v8, v6, v2

    move v2, v0

    .line 1494
    goto :goto_2

    .line 1496
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0, p1, v6}, Lcom/google/android/gms/icing/impl/NativeIndex;->b(I[I)[Lcom/google/android/gms/icing/o;

    move-result-object v6

    .line 1497
    if-nez v6, :cond_3

    monitor-exit v4

    move-object v0, v1

    goto :goto_0

    .line 1498
    :cond_3
    array-length v0, v6

    new-array v2, v0, [Lcom/google/android/gms/icing/c/a/b;

    .line 1499
    :goto_3
    array-length v0, v6

    if-ge v3, v0, :cond_4

    .line 1500
    aget-object v7, v6, v3

    .line 1501
    iget v0, v7, Lcom/google/android/gms/icing/o;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    .line 1502
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v8, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-virtual {v1, v8}, Lcom/google/android/gms/icing/impl/a/f;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/a/e;->a()Landroid/support/v4/g/o;

    move-result-object v8

    .line 1504
    iget-object v1, v8, Landroid/support/v4/g/o;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iget-object v1, v8, Landroid/support/v4/g/o;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v9, v1, v0, v7, v8}, Lcom/google/android/gms/icing/impl/a/j;->a(ILjava/lang/String;Lcom/google/android/gms/icing/g;Lcom/google/android/gms/icing/o;Lcom/google/android/gms/icing/ag;)Lcom/google/android/gms/icing/c/a/b;

    move-result-object v0

    aput-object v0, v2, v3

    .line 1499
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 1508
    :cond_4
    const-string v0, "Returned %d app history events."

    array-length v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 1509
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v2

    goto/16 :goto_0
.end method

.method private b(Lcom/google/android/gms/icing/impl/ax;)Z
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v12, 0x35

    .line 2107
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 2109
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/q;->c()I

    move-result v4

    .line 2111
    if-le v4, v12, :cond_0

    .line 2112
    const-string v0, "Version going backward from %d to %d"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    move v0, v2

    .line 2212
    :goto_0
    return v0

    .line 2115
    :cond_0
    const/4 v0, -0x1

    if-ne v4, v0, :cond_1

    .line 2116
    const-string v0, "Version not set, assuming clear data."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;)I

    move v0, v2

    .line 2119
    goto :goto_0

    .line 2122
    :cond_1
    if-ge v4, v12, :cond_a

    .line 2123
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 2126
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    if-nez v0, :cond_2

    .line 2127
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->r:Lcom/google/android/gms/icing/impl/aw;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->f:Lcom/google/android/gms/icing/impl/bn;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/bn;->b()Ljava/io/File;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/q;->u()Lcom/google/android/gms/icing/af;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(Ljava/io/File;Lcom/google/android/gms/icing/af;)Lcom/google/android/gms/icing/impl/NativeIndex;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    .line 2130
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    if-nez v0, :cond_3

    move v0, v2

    .line 2131
    goto :goto_0

    .line 2134
    :cond_3
    const-string v0, "Upgrading from version %d to %d"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v0, v3, v5}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 2138
    add-int/lit8 v0, v4, 0x1

    move v3, v0

    :goto_1
    if-gt v3, v12, :cond_6

    .line 2139
    sparse-switch v3, :sswitch_data_0

    .line 2182
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 2143
    :sswitch_0
    iput-boolean v1, p1, Lcom/google/android/gms/icing/impl/ax;->b:Z

    goto :goto_2

    .line 2147
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/j;->e()Ljava/util/Set;

    move-result-object v0

    .line 2148
    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v8

    invoke-direct {v5, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 2150
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2151
    iget-object v9, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v9}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v9

    invoke-virtual {v9, v0}, Lcom/google/android/gms/icing/impl/a/j;->b(Ljava/lang/String;)Lcom/google/android/gms/icing/i;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2156
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->j()Lcom/google/android/gms/icing/f;

    move-result-object v0

    iget-wide v8, v0, Lcom/google/android/gms/icing/f;->a:J

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/j;->q()Lcom/google/android/gms/icing/f;

    move-result-object v0

    iget-wide v10, v0, Lcom/google/android/gms/icing/f;->a:J

    cmp-long v0, v8, v10

    if-eqz v0, :cond_5

    .line 2158
    const-string v0, "Interrupted compact, skipping upgrade33"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;)I

    goto :goto_2

    .line 2160
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/j;->p()Lcom/google/android/gms/icing/n;

    move-result-object v0

    iget-object v8, p0, Lcom/google/android/gms/icing/impl/u;->f:Lcom/google/android/gms/icing/impl/bn;

    invoke-virtual {v8}, Lcom/google/android/gms/icing/impl/bn;->b()Ljava/io/File;

    move-result-object v8

    invoke-static {v5, v0, v8}, Lcom/google/android/gms/icing/d/a;->a(Ljava/util/List;Lcom/google/android/gms/icing/n;Ljava/io/File;)Z

    goto :goto_2

    .line 2167
    :sswitch_2
    iput-boolean v1, p1, Lcom/google/android/gms/icing/impl/ax;->c:Z

    goto :goto_2

    .line 2170
    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/j;->d()Ljava/util/List;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/u;->f:Lcom/google/android/gms/icing/impl/bn;

    invoke-virtual {v5}, Lcom/google/android/gms/icing/impl/bn;->b()Ljava/io/File;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/google/android/gms/icing/d/a;->a(Ljava/util/List;Ljava/io/File;)Z

    goto :goto_2

    .line 2175
    :sswitch_4
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/j;->d()Ljava/util/List;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    iget-object v8, p0, Lcom/google/android/gms/icing/impl/u;->f:Lcom/google/android/gms/icing/impl/bn;

    invoke-virtual {v8}, Lcom/google/android/gms/icing/impl/bn;->b()Ljava/io/File;

    move-result-object v8

    invoke-static {v0, v5, v8}, Lcom/google/android/gms/icing/d/a;->a(Ljava/util/List;Lcom/google/android/gms/icing/impl/q;Ljava/io/File;)Z

    goto/16 :goto_2

    .line 2190
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/icing/impl/a/j;->a(I)Z

    move-result v0

    .line 2191
    if-nez v0, :cond_7

    .line 2192
    const-string v0, "Couldn\'t upgrade corpus map from version %d to %d"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 2197
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    if-nez v0, :cond_8

    move v0, v2

    .line 2202
    :goto_4
    if-eqz v0, :cond_9

    .line 2203
    const-string v1, "Successfully upgraded native from version %d to %d"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 2205
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/q;->d()V

    .line 2210
    :goto_5
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v6, v7}, Lcom/google/android/gms/icing/impl/u;->a(IJ)V

    goto/16 :goto_0

    .line 2200
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(I)Z

    move-result v0

    goto :goto_4

    .line 2207
    :cond_9
    const-string v1, "Couldn\'t upgrade native from version %d to %d"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    goto :goto_5

    :cond_a
    move v0, v1

    goto/16 :goto_0

    .line 2139
    :sswitch_data_0
    .sparse-switch
        0x14 -> :sswitch_0
        0x21 -> :sswitch_1
        0x25 -> :sswitch_2
        0x30 -> :sswitch_3
        0x31 -> :sswitch_4
    .end sparse-switch
.end method

.method private m(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1648
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 1649
    iget-boolean v0, v1, Landroid/content/pm/ApplicationInfo;->enabled:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1652
    :goto_0
    return v0

    .line 1650
    :catch_0
    move-exception v1

    .line 1651
    const-string v2, "Could not get app info for %s"

    invoke-static {v2, p1, v1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    goto :goto_0
.end method

.method private n(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1699
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    .line 1700
    const-string v0, "doRemovePackageData %s"

    invoke-static {v0, p1}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;Ljava/lang/Object;)I

    .line 1701
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/f;->c(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v0

    .line 1702
    if-eqz v0, :cond_1

    .line 1703
    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/impl/a/e;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1704
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->p()Lcom/google/android/gms/icing/bi;

    move-result-object v0

    .line 1705
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->f:Lcom/google/android/gms/icing/impl/bn;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bn;->a(Lcom/google/android/gms/icing/bi;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1706
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->v()V

    .line 1712
    :cond_0
    :goto_0
    return-void

    .line 1710
    :cond_1
    const-string v0, "doRemovePackageData %s: not a known client"

    invoke-static {v0, p1}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0
.end method

.method private o(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1899
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/a;->f(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 1901
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/auth/r;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1902
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1903
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1912
    :goto_1
    return-object v0

    .line 1905
    :catch_0
    move-exception v0

    .line 1906
    const-string v2, "Failed to get account ID. %s"

    invoke-virtual {v0}, Lcom/google/android/gms/auth/q;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0

    .line 1907
    :catch_1
    move-exception v0

    .line 1908
    const-string v2, "Failed to get account ID. %s"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0

    .line 1911
    :cond_1
    const-string v0, "Failed to find account name for ID %s"

    invoke-static {v0, p1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 1912
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final A()Lcom/google/android/gms/icing/impl/a/f;
    .locals 1

    .prologue
    .line 2902
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    return-object v0
.end method

.method public final B()Lcom/google/android/gms/icing/impl/q;
    .locals 1

    .prologue
    .line 2906
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    return-object v0
.end method

.method public final C()Lcom/google/android/gms/icing/impl/NativeIndex;
    .locals 1

    .prologue
    .line 2910
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    return-object v0
.end method

.method public final D()Lcom/google/android/gms/icing/impl/bn;
    .locals 1

    .prologue
    .line 2914
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->f:Lcom/google/android/gms/icing/impl/bn;

    return-object v0
.end method

.method public final E()Lcom/google/android/gms/icing/impl/e;
    .locals 1

    .prologue
    .line 2927
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->k:Lcom/google/android/gms/icing/impl/e;

    return-object v0
.end method

.method public final F()Lcom/google/android/gms/icing/impl/m;
    .locals 1

    .prologue
    .line 2931
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    return-object v0
.end method

.method public final G()Lcom/google/android/gms/icing/impl/k;
    .locals 1

    .prologue
    .line 2935
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->m:Lcom/google/android/gms/icing/impl/k;

    return-object v0
.end method

.method public final H()Lcom/google/android/gms/icing/impl/bf;
    .locals 1

    .prologue
    .line 2939
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->t:Lcom/google/android/gms/icing/impl/bf;

    return-object v0
.end method

.method public final I()Lcom/google/android/gms/icing/c/b;
    .locals 1

    .prologue
    .line 2943
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->s:Lcom/google/android/gms/icing/c/b;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x0

    const-wide/16 v6, 0x0

    .line 1929
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    .line 1930
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/a/f;->a()Lcom/google/android/gms/icing/impl/a/h;

    move-result-object v1

    .line 1932
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1934
    const-string v3, "name"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1938
    iget-boolean v1, v1, Lcom/google/android/gms/icing/impl/a/h;->b:Z

    if-nez v1, :cond_0

    .line 1939
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1940
    :cond_0
    if-nez v3, :cond_2

    .line 1941
    const-string v1, "No operation named"

    .line 1970
    :goto_0
    if-eqz v0, :cond_1

    const-string v3, "block"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1971
    invoke-interface {v0}, Lcom/google/android/gms/icing/b/j;->e()Ljava/lang/Object;

    .line 1974
    :cond_1
    const-string v0, "error_message"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1975
    return-object v2

    .line 1942
    :cond_2
    const-string v1, "flush"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1943
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v3, Lcom/google/android/gms/icing/impl/ae;

    invoke-direct {v3, p0}, Lcom/google/android/gms/icing/impl/ae;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    invoke-virtual {v1, v3, v6, v7}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    move-result-object v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    goto :goto_0

    .line 1944
    :cond_3
    const-string v1, "clear"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1945
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->L()Lcom/google/android/gms/icing/b/j;

    move-result-object v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    goto :goto_0

    .line 1946
    :cond_4
    const-string v1, "compact"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1947
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->M()Lcom/google/android/gms/icing/b/j;

    move-result-object v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    goto :goto_0

    .line 1948
    :cond_5
    const-string v1, "rebuild"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1949
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v3, Lcom/google/android/gms/icing/impl/ag;

    invoke-direct {v3, p0}, Lcom/google/android/gms/icing/impl/ag;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    invoke-virtual {v1, v3, v6, v7}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    move-result-object v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    goto :goto_0

    .line 1950
    :cond_6
    const-string v1, "compactAndPurge"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1951
    const-string v1, "target"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1952
    const-string v1, "target"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 1953
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v3, Lcom/google/android/gms/icing/impl/ai;

    invoke-direct {v3, p0, v4, v5}, Lcom/google/android/gms/icing/impl/ai;-><init>(Lcom/google/android/gms/icing/impl/u;D)V

    invoke-virtual {v1, v3, v6, v7}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    move-result-object v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    .line 1954
    goto/16 :goto_0

    .line 1955
    :cond_7
    const-string v1, "No target free for compactAndPurge specified"

    goto/16 :goto_0

    .line 1957
    :cond_8
    const-string v1, "getDebugString"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1958
    const-string v1, "debug"

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    new-instance v3, Ljava/io/StringWriter;

    invoke-direct {v3}, Ljava/io/StringWriter;-><init>()V

    new-instance v4, Ljava/io/PrintWriter;

    invoke-direct {v4, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const-string v6, "native"

    aput-object v6, v5, v8

    invoke-virtual {p0, v0, v4, v5}, Lcom/google/android/gms/icing/impl/u;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    goto/16 :goto_0

    .line 1959
    :cond_9
    const-string v1, "readresources"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1960
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v3, Lcom/google/android/gms/icing/impl/an;

    invoke-direct {v3, p0}, Lcom/google/android/gms/icing/impl/an;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    invoke-virtual {v1, v3, v6, v7}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    move-result-object v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    goto/16 :goto_0

    .line 1961
    :cond_a
    const-string v1, "maintenance"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1962
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->o()Lcom/google/android/gms/icing/b/j;

    move-result-object v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    goto/16 :goto_0

    .line 1963
    :cond_b
    const-string v1, "slurpUsageReports"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1964
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v3, Lcom/google/android/gms/icing/impl/bl;

    invoke-direct {v3, p0, v8, v8}, Lcom/google/android/gms/icing/impl/bl;-><init>(Lcom/google/android/gms/icing/impl/u;ZZ)V

    invoke-virtual {v1, v3, v6, v7}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    move-result-object v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    goto/16 :goto_0

    .line 1967
    :cond_c
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Unknown operation \""

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/CorpusStatus;
    .locals 2

    .prologue
    .line 980
    new-instance v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;

    invoke-direct {v0}, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;-><init>()V

    .line 981
    iput-object p1, v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;->a:Ljava/lang/String;

    .line 982
    iput-object p2, v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;->b:Ljava/lang/String;

    .line 983
    new-instance v1, Lcom/google/android/gms/search/corpora/i;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/gms/search/corpora/i;-><init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;)V

    .line 984
    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/corpora/i;

    invoke-virtual {v0}, Lcom/google/android/gms/search/corpora/i;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Response;

    iget-object v0, v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Response;->b:Lcom/google/android/gms/appdatasearch/CorpusStatus;

    return-object v0
.end method

.method public final a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/DocumentResults;
    .locals 3

    .prologue
    .line 955
    new-instance v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;

    invoke-direct {v0}, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;-><init>()V

    .line 956
    iput-object p1, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->c:[Ljava/lang/String;

    .line 957
    iput-object p2, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->a:Ljava/lang/String;

    .line 958
    iput-object p3, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->b:Ljava/lang/String;

    .line 959
    iput-object p4, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->d:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    .line 960
    new-instance v1, Lcom/google/android/gms/search/queries/c;

    iget-object v2, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->a:Ljava/lang/String;

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/gms/search/queries/c;-><init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;)V

    .line 961
    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/queries/c;

    invoke-virtual {v0}, Lcom/google/android/gms/search/queries/c;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Response;

    iget-object v0, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Response;->b:Lcom/google/android/gms/appdatasearch/DocumentResults;

    return-object v0
.end method

.method public final a(Ljava/lang/String;I[B)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;
    .locals 9

    .prologue
    .line 1329
    invoke-static {p2}, Lcom/google/android/gms/icing/impl/bb;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 1330
    if-eqz v0, :cond_0

    .line 1331
    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/aq;->c(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;

    move-result-object v0

    .line 1368
    :goto_0
    return-object v0

    .line 1336
    :cond_0
    if-nez p3, :cond_1

    .line 1337
    new-instance v0, Lcom/google/android/gms/icing/u;

    invoke-direct {v0}, Lcom/google/android/gms/icing/u;-><init>()V

    move-object v4, v0

    .line 1346
    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    .line 1347
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/f;->a(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/aa;

    move-result-object v0

    .line 1350
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/aa;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1351
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1340
    :cond_1
    :try_start_0
    invoke-static {p3}, Lcom/google/android/gms/icing/u;->a([B)Lcom/google/android/gms/icing/u;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v4, v0

    .line 1343
    goto :goto_1

    .line 1342
    :catch_0
    move-exception v0

    const-string v0, "Bad iter token"

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/aq;->c(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;

    move-result-object v0

    goto :goto_0

    .line 1354
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/p;->b:Lcom/google/android/gms/icing/impl/a/j;

    .line 1356
    invoke-interface {v0}, Lcom/google/android/gms/icing/impl/a/x;->b()Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 1357
    :try_start_1
    invoke-interface {v0}, Lcom/google/android/gms/icing/impl/a/x;->t()Landroid/util/SparseArray;

    move-result-object v8

    .line 1359
    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v0

    new-array v6, v0, [Lcom/google/android/gms/icing/x;

    .line 1360
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    array-length v0, v6

    if-ge v1, v0, :cond_3

    .line 1361
    invoke-virtual {v8, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/a/y;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/y;->e:Lcom/google/android/gms/icing/x;

    aput-object v0, v6, v1

    .line 1360
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1363
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    iget-wide v2, v4, Lcom/google/android/gms/icing/u;->a:J

    iget v4, v4, Lcom/google/android/gms/icing/u;->b:I

    move v5, p2

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(JII[Lcom/google/android/gms/icing/x;)Lcom/google/android/gms/icing/z;

    move-result-object v0

    .line 1368
    invoke-static {v0, v8}, Lcom/google/android/gms/appdatasearch/aq;->a(Lcom/google/android/gms/icing/z;Landroid/util/SparseArray;)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;

    move-result-object v0

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1369
    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;I[B)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;
    .locals 6

    .prologue
    .line 1377
    sget-object v0, Lcom/google/android/gms/icing/a/a;->p:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1378
    const-string v0, "Disabled"

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/aq;->c(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;

    move-result-object v0

    .line 1420
    :goto_0
    return-object v0

    .line 1381
    :cond_0
    invoke-static {p3}, Lcom/google/android/gms/icing/impl/bb;->b(I)Ljava/lang/String;

    move-result-object v0

    .line 1383
    if-eqz v0, :cond_1

    .line 1384
    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/aq;->c(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;

    move-result-object v0

    goto :goto_0

    .line 1389
    :cond_1
    if-nez p4, :cond_2

    .line 1390
    new-instance v0, Lcom/google/android/gms/icing/u;

    invoke-direct {v0}, Lcom/google/android/gms/icing/u;-><init>()V

    .line 1399
    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    .line 1400
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/f;->a(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/aa;

    move-result-object v0

    .line 1403
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/aa;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1404
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1393
    :cond_2
    :try_start_0
    invoke-static {p4}, Lcom/google/android/gms/icing/u;->a([B)Lcom/google/android/gms/icing/u;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1395
    :catch_0
    move-exception v0

    const-string v0, "Bad iter token"

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/aq;->c(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;

    move-result-object v0

    goto :goto_0

    .line 1407
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 1408
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->s:Lcom/google/android/gms/icing/c/b;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->m:Lcom/google/android/gms/icing/impl/k;

    invoke-interface {v0}, Lcom/google/android/gms/icing/impl/k;->a()Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/icing/a/a;->v:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/u;->a(I)[Lcom/google/android/gms/icing/c/a/b;

    move-result-object v0

    invoke-interface {v1, v4, p2, v0}, Lcom/google/android/gms/icing/c/b;->a(Ljava/lang/String;Ljava/lang/String;[Lcom/google/android/gms/icing/c/a/b;)Lcom/google/android/gms/icing/c/a/x;

    move-result-object v0

    .line 1411
    const/16 v1, 0x8

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/gms/icing/impl/u;->a(IJ)V

    .line 1413
    const-string v1, "Fetching contextual suggestions took %d ms"

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 1415
    if-nez v0, :cond_4

    .line 1416
    const-string v0, "Failed to get contextual suggestions."

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/aq;->c(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;

    move-result-object v0

    goto :goto_0

    .line 1420
    :cond_4
    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/aq;->a(Lcom/google/android/gms/icing/c/a/x;)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a([Ljava/lang/String;Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;)Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;
    .locals 3

    .prologue
    .line 1426
    new-instance v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;

    invoke-direct {v0}, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;-><init>()V

    .line 1427
    iput-object p1, v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;->a:[Ljava/lang/String;

    .line 1428
    invoke-virtual {p2}, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->a()[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;->b:[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

    .line 1429
    new-instance v1, Lcom/google/android/gms/search/queries/f;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/gms/search/queries/f;-><init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;)V

    .line 1430
    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/queries/f;

    invoke-virtual {v0}, Lcom/google/android/gms/search/queries/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Response;

    iget-object v0, v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Response;->b:Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;

    return-object v0
.end method

.method public final a(Ljava/lang/String;IILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 3

    .prologue
    .line 1033
    new-instance v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;

    invoke-direct {v0}, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;-><init>()V

    .line 1034
    iput-object p1, v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;->a:Ljava/lang/String;

    .line 1035
    iput p2, v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;->b:I

    .line 1036
    iput p3, v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;->c:I

    .line 1037
    iput-object p4, v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;->d:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    .line 1038
    new-instance v1, Lcom/google/android/gms/search/queries/i;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/gms/search/queries/i;-><init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;)V

    .line 1039
    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/queries/i;

    invoke-virtual {v0}, Lcom/google/android/gms/search/queries/i;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Response;

    iget-object v0, v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Response;->b:Lcom/google/android/gms/appdatasearch/SearchResults;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 3

    .prologue
    .line 843
    invoke-static {p6}, Lcom/google/android/gms/search/queries/m;->a(Lcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/icing/impl/e/g;

    move-result-object v0

    .line 844
    new-instance v1, Lcom/google/android/gms/search/queries/QueryCall$Request;

    invoke-direct {v1}, Lcom/google/android/gms/search/queries/QueryCall$Request;-><init>()V

    .line 845
    iput-object p1, v1, Lcom/google/android/gms/search/queries/QueryCall$Request;->a:Ljava/lang/String;

    .line 846
    iput-object p2, v1, Lcom/google/android/gms/search/queries/QueryCall$Request;->b:Ljava/lang/String;

    .line 847
    iput-object p3, v1, Lcom/google/android/gms/search/queries/QueryCall$Request;->c:[Ljava/lang/String;

    .line 848
    iput p4, v1, Lcom/google/android/gms/search/queries/QueryCall$Request;->d:I

    .line 849
    iput p5, v1, Lcom/google/android/gms/search/queries/QueryCall$Request;->e:I

    .line 850
    iput-object p6, v1, Lcom/google/android/gms/search/queries/QueryCall$Request;->f:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    .line 851
    new-instance v2, Lcom/google/android/gms/search/queries/m;

    invoke-direct {v2, p0, p2, v1, v0}, Lcom/google/android/gms/search/queries/m;-><init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/queries/QueryCall$Request;Lcom/google/android/gms/icing/impl/e/g;)V

    .line 852
    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/queries/m;

    invoke-virtual {v0}, Lcom/google/android/gms/search/queries/m;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/queries/QueryCall$Response;

    iget-object v0, v0, Lcom/google/android/gms/search/queries/QueryCall$Response;->b:Lcom/google/android/gms/appdatasearch/SearchResults;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;ILcom/google/android/gms/appdatasearch/SuggestSpecification;)Lcom/google/android/gms/appdatasearch/SuggestionResults;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 893
    new-instance v0, Lcom/google/android/gms/icing/impl/e/g;

    const/4 v1, 0x2

    const/4 v3, 0x3

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/icing/impl/e/g;-><init>(II)V

    .line 895
    invoke-static {p1, p2, p3, p4}, Lcom/google/android/gms/icing/impl/bb;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 896
    if-eqz v1, :cond_0

    .line 897
    new-instance v0, Lcom/google/android/gms/appdatasearch/SuggestionResults;

    invoke-direct {v0, v1}, Lcom/google/android/gms/appdatasearch/SuggestionResults;-><init>(Ljava/lang/String;)V

    .line 945
    :goto_0
    return-object v0

    .line 900
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    .line 901
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/e/g;->a()V

    .line 903
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v1, p2}, Lcom/google/android/gms/icing/impl/a/f;->a(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/aa;

    move-result-object v1

    .line 905
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    iget-object v5, v3, Lcom/google/android/gms/icing/impl/a/p;->b:Lcom/google/android/gms/icing/impl/a/j;

    .line 906
    invoke-interface {v5}, Lcom/google/android/gms/icing/impl/a/x;->b()Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 908
    const/4 v3, 0x1

    :try_start_0
    invoke-interface {v5, v1, p3, v3}, Lcom/google/android/gms/icing/impl/a/x;->a(Lcom/google/android/gms/icing/impl/a/aa;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v1

    .line 909
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v3

    new-array v6, v3, [I

    .line 911
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v3, v2

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 912
    add-int/lit8 v4, v3, 0x1

    invoke-interface {v5, v1}, Lcom/google/android/gms/icing/impl/a/x;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/g;

    move-result-object v1

    iget v1, v1, Lcom/google/android/gms/icing/g;->a:I

    aput v1, v6, v3

    move v3, v4

    .line 913
    goto :goto_1

    .line 915
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/e/g;->b()V

    .line 916
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v1, p1, v6, p4}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(Ljava/lang/String;[II)Lcom/google/android/gms/icing/ax;

    move-result-object v3

    .line 918
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/e/g;->c()V

    .line 920
    iget-object v1, v3, Lcom/google/android/gms/icing/ax;->a:[Lcom/google/android/gms/icing/ay;

    array-length v1, v1

    new-array v4, v1, [Ljava/lang/String;

    .line 921
    iget-object v1, v3, Lcom/google/android/gms/icing/ax;->a:[Lcom/google/android/gms/icing/ay;

    array-length v1, v1

    new-array v5, v1, [Ljava/lang/String;

    .line 922
    :goto_2
    iget-object v1, v3, Lcom/google/android/gms/icing/ax;->a:[Lcom/google/android/gms/icing/ay;

    array-length v1, v1

    if-ge v2, v1, :cond_3

    .line 923
    iget-object v1, v3, Lcom/google/android/gms/icing/ax;->a:[Lcom/google/android/gms/icing/ay;

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/google/android/gms/icing/ay;->a:Ljava/lang/String;

    aput-object v1, v4, v2

    .line 924
    iget-object v1, v3, Lcom/google/android/gms/icing/ax;->a:[Lcom/google/android/gms/icing/ay;

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/google/android/gms/icing/ay;->b:Ljava/lang/String;

    .line 925
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v1, 0x0

    :cond_2
    aput-object v1, v5, v2

    .line 922
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 928
    :cond_3
    new-instance v6, Lcom/google/android/gms/appdatasearch/SuggestionResults;

    invoke-direct {v6, v4, v5}, Lcom/google/android/gms/appdatasearch/SuggestionResults;-><init>([Ljava/lang/String;[Ljava/lang/String;)V

    .line 931
    array-length v3, v4

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->t:Lcom/google/android/gms/icing/impl/bf;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/bf;->f()I

    move-result v5

    move-object v1, p1

    move v2, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/icing/impl/e/g;->a(Ljava/lang/String;IIII)Lcom/google/k/f/ap;

    move-result-object v1

    .line 933
    sget-object v0, Lcom/google/android/gms/icing/a/a;->o:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 934
    new-instance v0, Lcom/google/android/gms/icing/impl/av;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/icing/impl/av;-><init>(Lcom/google/android/gms/icing/impl/u;Lcom/google/k/f/ap;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    .line 945
    :goto_3
    monitor-exit v7

    move-object v0, v6

    goto/16 :goto_0

    .line 942
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    invoke-interface {v0, v1}, Lcom/google/android/gms/icing/impl/m;->a(Lcom/google/k/f/ap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 946
    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0
.end method

.method public final a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;
    .locals 4

    .prologue
    .line 2918
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, p1, v2, v3}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)Lcom/google/android/gms/icing/b/j;
    .locals 4

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 474
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v1, Lcom/google/android/gms/icing/impl/aq;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/icing/impl/aq;-><init>(Lcom/google/android/gms/icing/impl/u;Z)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    move-result-object v0

    return-object v0
.end method

.method final a(Lcom/google/android/gms/icing/impl/a/aa;Lcom/google/android/gms/icing/impl/a/e;ZLcom/google/android/gms/icing/impl/a/ac;)Lcom/google/android/gms/icing/g;
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 674
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 675
    invoke-virtual {p2}, Lcom/google/android/gms/icing/impl/a/e;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 676
    new-instance v0, Lcom/google/android/gms/icing/impl/b/d;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Package "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p2, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is blocked."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/impl/b/d;-><init>(Ljava/lang/String;)V

    throw v0

    .line 678
    :cond_0
    invoke-virtual {p4}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 679
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    invoke-virtual {p4}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v2

    if-nez v2, :cond_1

    new-instance v1, Lcom/google/android/gms/icing/impl/b/a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ContentProvider "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " does not exist"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/icing/impl/b/a;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget-boolean v3, p1, Lcom/google/android/gms/icing/impl/a/h;->b:Z

    if-nez v3, :cond_2

    iget v3, p1, Lcom/google/android/gms/icing/impl/a/h;->a:I

    iget-object v4, v2, Landroid/content/pm/ProviderInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    if-eq v3, v4, :cond_2

    new-instance v1, Ljava/lang/SecurityException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ContentProvider "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " authority "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " uid "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v2, Landroid/content/pm/ProviderInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " does not match calling uid "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p1, Lcom/google/android/gms/icing/impl/a/h;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    iget-boolean v3, p1, Lcom/google/android/gms/icing/impl/a/h;->b:Z

    if-nez v3, :cond_3

    iget-object v2, v2, Landroid/content/pm/ProviderInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget v3, p1, Lcom/google/android/gms/icing/impl/a/h;->a:I

    invoke-static {v1, v3, v2}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/SecurityException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ContentProvider "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " package name "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " does not match client package names"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 683
    :cond_3
    invoke-virtual {p2, p3}, Lcom/google/android/gms/icing/impl/a/e;->c(Z)I

    move-result v0

    .line 684
    if-ne v0, v5, :cond_4

    .line 686
    const-string v0, "App %s registering with different sigs, clearing old corpora"

    iget-object v1, p2, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;Ljava/lang/Object;)I

    .line 688
    invoke-virtual {p0, p2}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/impl/a/e;)Z

    .line 689
    invoke-virtual {p2, p3}, Lcom/google/android/gms/icing/impl/a/e;->c(Z)I

    move-result v0

    .line 691
    :cond_4
    if-eqz v0, :cond_5

    .line 692
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    const-string v2, "register_auth_fail"

    invoke-interface {v1, v2}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    .line 693
    new-instance v1, Lcom/google/android/gms/icing/impl/b/d;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Package "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p2, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " cannot register: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    packed-switch v0, :pswitch_data_0

    const-string v0, "Unknwown error"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/icing/impl/b/d;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    const-string v0, "ok"

    goto :goto_0

    :pswitch_1
    const-string v0, "App not allowed"

    goto :goto_0

    :pswitch_2
    const-string v0, "Has different fingerprint"

    goto :goto_0

    :pswitch_3
    const-string v0, "App unknown"

    goto :goto_0

    .line 698
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v1

    .line 699
    invoke-virtual {p4}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-virtual {v1, p2, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/impl/a/e;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 700
    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/impl/a/j;->e(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v0

    .line 701
    if-eqz v0, :cond_8

    .line 702
    invoke-virtual {v0, p4}, Lcom/google/android/gms/icing/impl/a/ac;->a(Lcom/google/android/gms/icing/impl/a/ac;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 703
    new-instance v1, Lcom/google/android/gms/icing/impl/b/a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CorpusConfig: cannot "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p4}, Lcom/google/android/gms/icing/impl/a/ac;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " when previously "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/ac;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/icing/impl/b/a;-><init>(Ljava/lang/String;)V

    throw v1

    .line 706
    :cond_6
    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/impl/a/j;->f(Ljava/lang/String;)Lcom/google/android/gms/icing/j;

    move-result-object v3

    .line 707
    if-eqz v3, :cond_7

    iget v3, v3, Lcom/google/android/gms/icing/j;->d:I

    if-nez v3, :cond_7

    .line 708
    invoke-virtual {v1, v2, v0, p4}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/ac;Lcom/google/android/gms/icing/impl/a/ac;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 710
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    .line 723
    :goto_1
    return-object v0

    .line 714
    :cond_7
    const-string v0, "Corpus registration info changed, replacing corpus"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;)I

    .line 715
    invoke-virtual {p0, v2, p2}, Lcom/google/android/gms/icing/impl/u;->a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/e;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 716
    new-instance v0, Lcom/google/android/gms/icing/impl/b/d;

    const-string v1, "Could not unregister old corpus"

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/impl/b/d;-><init>(Ljava/lang/String;)V

    throw v0

    .line 718
    :cond_8
    invoke-virtual {p4}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/impl/a/h;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 719
    new-instance v1, Lcom/google/android/gms/icing/impl/b/a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "Corpus "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p4}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "already exists in a different package from this uid"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/icing/impl/b/a;-><init>(Ljava/lang/String;)V

    throw v1

    .line 723
    :cond_9
    invoke-direct {p0, v2, p4}, Lcom/google/android/gms/icing/impl/u;->a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/ac;)Lcom/google/android/gms/icing/g;

    move-result-object v0

    goto :goto_1

    .line 693
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ljava/util/List;Ljava/util/Set;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 869
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/p;->b:Lcom/google/android/gms/icing/impl/a/j;

    invoke-interface {v0}, Lcom/google/android/gms/icing/impl/a/x;->b()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 871
    if-eqz p1, :cond_1

    :try_start_0
    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 872
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/a/p;->b:Lcom/google/android/gms/icing/impl/a/j;

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v0}, Lcom/google/android/gms/icing/impl/a/x;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/g;

    move-result-object v2

    .line 874
    invoke-static {v2}, Lcom/google/android/gms/icing/impl/a/j;->c(Lcom/google/android/gms/icing/g;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 875
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/Section;

    .line 876
    iget-object v5, v0, Lcom/google/android/gms/appdatasearch/Section;->b:Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 877
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Corpus "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v2, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " doesn\'t contain section "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/Section;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 882
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 883
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/icing/impl/a/h;Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;Z)Ljava/util/List;
    .locals 5

    .prologue
    .line 1048
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/p;->b:Lcom/google/android/gms/icing/impl/a/j;

    invoke-interface {v0, p2, p3}, Lcom/google/android/gms/icing/impl/a/x;->a(Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;Z)Ljava/util/List;

    move-result-object v1

    .line 1052
    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/a/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 1071
    :goto_0
    return-object v0

    .line 1056
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/f;->a(Lcom/google/android/gms/icing/impl/a/h;)Ljava/util/Set;

    move-result-object v0

    .line 1057
    new-instance v3, Ljava/util/HashSet;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    invoke-direct {v3, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 1058
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/a/e;

    .line 1059
    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1063
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1065
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    .line 1066
    iget-object v4, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1067
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    move-object v0, v2

    .line 1071
    goto :goto_0
.end method

.method final a(D)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    .line 2617
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    invoke-virtual {v0, v8}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 2618
    const-string v0, "Starting purge with target free %.3f%% min disk %.3f%% min index %.3f%%"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    mul-double v4, p1, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->f:Lcom/google/android/gms/icing/impl/bn;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/bn;->c()D

    move-result-wide v4

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->o()D

    move-result-wide v2

    mul-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 2623
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    sget v4, Lcom/google/android/gms/icing/impl/a/j;->a:I

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/j;->i()[J

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/j;->s()[I

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/j;->k()[I

    move-result-object v7

    move-wide v2, p1

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(DI[J[I[I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2627
    const-string v0, "Compact and purge failed"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 2628
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    const-string v1, "compaction_failed"

    invoke-interface {v0, v1}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    .line 2632
    :goto_0
    return-void

    .line 2631
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->N()V

    goto :goto_0
.end method

.method final a(IJ)V
    .locals 4

    .prologue
    .line 2088
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, p2

    long-to-int v1, v2

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/icing/impl/m;->a(II)V

    .line 2089
    return-void
.end method

.method public final a(J)V
    .locals 5

    .prologue
    .line 2073
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v1, Lcom/google/android/gms/icing/impl/ak;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/icing/impl/ak;-><init>(Lcom/google/android/gms/icing/impl/u;J)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    .line 2083
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1584
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Got package manager broadcast: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;)I

    .line 1585
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 1586
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->l()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1590
    const-string v0, "Couldn\'t handle %s intent due to initialization failure."

    invoke-static {v0, v2}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    .line 1644
    :goto_0
    return-void

    .line 1593
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v3

    .line 1594
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 1599
    const-string v5, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1600
    const-string v2, "android.intent.extra.REPLACING"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    .line 1601
    :goto_1
    if-nez v2, :cond_3

    move v5, v0

    .line 1602
    :goto_2
    if-nez v2, :cond_1

    const-string v6, "android.intent.extra.DATA_REMOVED"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_1
    :goto_3
    move v4, v0

    move v6, v1

    .line 1628
    :goto_4
    iget-object v7, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v0, Lcom/google/android/gms/icing/impl/aa;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/icing/impl/aa;-><init>(Lcom/google/android/gms/icing/impl/u;ZLjava/lang/String;ZZZ)V

    const-wide/16 v2, 0x0

    invoke-virtual {v7, v0, v2, v3}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    goto :goto_0

    :cond_2
    move v2, v1

    .line 1600
    goto :goto_1

    :cond_3
    move v5, v1

    .line 1601
    goto :goto_2

    :cond_4
    move v0, v1

    .line 1602
    goto :goto_3

    .line 1604
    :cond_5
    const-string v4, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    move v4, v0

    move v6, v1

    move v5, v1

    move v2, v1

    .line 1608
    goto :goto_4

    .line 1609
    :cond_6
    const-string v4, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    move v4, v1

    move v6, v0

    move v5, v1

    move v2, v1

    .line 1613
    goto :goto_4

    .line 1614
    :cond_7
    const-string v4, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    move v4, v1

    move v6, v0

    move v5, v1

    move v2, v1

    .line 1618
    goto :goto_4

    .line 1619
    :cond_8
    const-string v4, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 1620
    invoke-direct {p0, v3}, Lcom/google/android/gms/icing/impl/u;->m(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9

    move v2, v0

    .line 1623
    :goto_5
    if-nez v2, :cond_a

    :goto_6
    move v4, v1

    move v6, v0

    move v5, v1

    goto :goto_4

    :cond_9
    move v2, v1

    .line 1620
    goto :goto_5

    :cond_a
    move v0, v1

    .line 1623
    goto :goto_6

    .line 1625
    :cond_b
    new-instance v0, Lcom/google/android/gms/icing/impl/b/c;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unknown intent action "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/impl/b/c;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)V
    .locals 1

    .prologue
    .line 1096
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1097
    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/icing/impl/u;->a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)V

    .line 1098
    return-void
.end method

.method public final a(Lcom/google/android/gms/icing/c/a/r;)V
    .locals 4

    .prologue
    .line 1777
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1778
    const-string v0, "Couldn\'t handle gcm msg due to initialization failure"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 1793
    :goto_0
    return-void

    .line 1782
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v1, Lcom/google/android/gms/icing/impl/ad;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/icing/impl/ad;-><init>(Lcom/google/android/gms/icing/impl/u;Lcom/google/android/gms/icing/c/a/r;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    goto :goto_0
.end method

.method final a(Lcom/google/android/gms/icing/c/a/s;)V
    .locals 12

    .prologue
    .line 1796
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    iget-object v7, v0, Lcom/google/android/gms/icing/impl/a/p;->b:Lcom/google/android/gms/icing/impl/a/j;

    .line 1797
    invoke-interface {v7}, Lcom/google/android/gms/icing/impl/a/x;->b()Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8

    .line 1801
    :try_start_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v2, p1, Lcom/google/android/gms/icing/c/a/s;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    long-to-int v5, v0

    .line 1803
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v2, p1, Lcom/google/android/gms/icing/c/a/s;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    long-to-int v6, v0

    .line 1805
    iget-object v0, p1, Lcom/google/android/gms/icing/c/a/s;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1807
    invoke-interface {v7}, Lcom/google/android/gms/icing/impl/a/x;->h()Ljava/util/Set;

    move-result-object v0

    .line 1808
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1810
    invoke-interface {v7, v0}, Lcom/google/android/gms/icing/impl/a/x;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/g;

    move-result-object v2

    .line 1811
    if-eqz v2, :cond_0

    .line 1813
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v4, v2, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/impl/a/f;->c(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v3

    .line 1814
    iget-object v4, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    iget-wide v10, v2, Lcom/google/android/gms/icing/g;->i:J

    invoke-virtual {v4, v10, v11, v5, v6}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(JII)V

    .line 1816
    invoke-virtual {p0, v0, v3}, Lcom/google/android/gms/icing/impl/u;->a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/e;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1845
    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0

    .line 1820
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/s;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/a/f;->c(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v9

    .line 1822
    if-eqz v9, :cond_4

    .line 1823
    invoke-virtual {v9}, Lcom/google/android/gms/icing/impl/a/e;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_2
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1824
    invoke-interface {v7, v0}, Lcom/google/android/gms/icing/impl/a/x;->b(Ljava/lang/String;)Lcom/google/android/gms/icing/i;

    move-result-object v11

    .line 1825
    iget-object v1, v11, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget v1, v1, Lcom/google/android/gms/icing/j;->d:I

    if-nez v1, :cond_2

    .line 1826
    iget-object v1, v11, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/a/j;->f(Lcom/google/android/gms/icing/g;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1828
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/s;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1830
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    iget-object v2, v11, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-wide v2, v2, Lcom/google/android/gms/icing/g;->i:J

    invoke-virtual {v1, v2, v3, v5, v6}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(JII)V

    .line 1833
    invoke-virtual {p0, v0, v9}, Lcom/google/android/gms/icing/impl/u;->a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/e;)Z

    goto :goto_1

    .line 1836
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    iget-object v0, v11, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-wide v2, v0, Lcom/google/android/gms/icing/g;->i:J

    iget-object v4, p1, Lcom/google/android/gms/icing/c/a/s;->b:Ljava/lang/String;

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(JLjava/lang/String;II)V

    .line 1839
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    const-wide/16 v2, 0x0

    iget-object v1, v11, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget v1, v1, Lcom/google/android/gms/icing/g;->a:I

    iget-object v4, p1, Lcom/google/android/gms/icing/c/a/s;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(JILjava/lang/String;)I

    goto :goto_1

    .line 1845
    :cond_4
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method final a(Lcom/google/android/gms/icing/c/a/t;)V
    .locals 1

    .prologue
    .line 1854
    iget-object v0, p1, Lcom/google/android/gms/icing/c/a/t;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 1858
    :cond_0
    :goto_0
    return-void

    .line 1855
    :cond_1
    iget-object v0, p1, Lcom/google/android/gms/icing/c/a/t;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/u;->o(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1856
    if-eqz v0, :cond_0

    .line 1857
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/icing/impl/u;->a(Ljava/lang/String;Lcom/google/android/gms/icing/c/a/t;)V

    goto :goto_0
.end method

.method final a(Lcom/google/android/gms/icing/impl/a/aa;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;J)V
    .locals 3

    .prologue
    .line 556
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    iget-object v1, p1, Lcom/google/android/gms/icing/impl/a/aa;->e:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/google/android/gms/icing/impl/bb;->a(Landroid/content/Context;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Ljava/lang/String;

    move-result-object v0

    .line 558
    if-eqz v0, :cond_0

    .line 559
    new-instance v1, Lcom/google/android/gms/icing/impl/b/a;

    invoke-direct {v1, v0}, Lcom/google/android/gms/icing/impl/b/a;-><init>(Ljava/lang/String;)V

    throw v1

    .line 562
    :cond_0
    :try_start_0
    invoke-static {p2, p3, p4}, Lcom/google/android/gms/icing/impl/a/ac;->b(Ljava/lang/Object;J)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/impl/a/aa;Lcom/google/android/gms/icing/impl/a/ac;)V
    :try_end_0
    .catch Lcom/google/android/gms/icing/impl/b/d; {:try_start_0 .. :try_end_0} :catch_0

    .line 567
    :goto_0
    return-void

    .line 563
    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to register corpus from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/gms/icing/impl/a/aa;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resources"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method final a(Lcom/google/android/gms/icing/impl/a/aa;Lcom/google/android/gms/icing/impl/a/ac;)V
    .locals 5

    .prologue
    .line 653
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 654
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/f;->a(Lcom/google/android/gms/icing/impl/a/aa;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v2

    .line 655
    new-instance v1, Lcom/google/android/gms/icing/impl/c/c;

    iget-object v3, v2, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/a/e;->j()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v1, v3, v0, v4}, Lcom/google/android/gms/icing/impl/c/c;-><init>(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;Landroid/content/res/Resources;)V

    .line 658
    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/c/c;->a()V

    .line 660
    invoke-virtual {p2}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    invoke-static {v2, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/impl/a/e;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Lcom/google/android/gms/icing/g;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/android/gms/icing/impl/a/ac;->a(Ljava/lang/Object;)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v3

    .line 663
    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/c/c;->b()Ljava/util/List;

    move-result-object v4

    .line 664
    if-eqz v4, :cond_0

    .line 665
    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/g;

    iget-object v1, v1, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    invoke-interface {v4, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/android/gms/icing/az;

    iput-object v1, v0, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    .line 668
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v2, v0, v3}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/impl/a/aa;Lcom/google/android/gms/icing/impl/a/e;ZLcom/google/android/gms/icing/impl/a/ac;)Lcom/google/android/gms/icing/g;

    .line 669
    return-void
.end method

.method public final a(Lcom/google/android/gms/icing/impl/a/aa;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 728
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 729
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/f;->a(Lcom/google/android/gms/icing/impl/a/aa;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v0

    .line 730
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/e;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 731
    new-instance v1, Lcom/google/android/gms/icing/impl/b/d;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Package "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is blocked."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/icing/impl/b/d;-><init>(Ljava/lang/String;)V

    throw v1

    .line 735
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v1

    .line 736
    invoke-virtual {v1, v0, p2}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/impl/a/e;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 737
    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/impl/a/j;->e(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v1

    .line 739
    if-nez v1, :cond_1

    .line 740
    new-instance v1, Lcom/google/android/gms/icing/impl/b/a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No CorpusConfig for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/icing/impl/b/a;-><init>(Ljava/lang/String;)V

    throw v1

    .line 744
    :cond_1
    invoke-virtual {p0, v2, v0}, Lcom/google/android/gms/icing/impl/u;->a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/e;)Z

    .line 745
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    iget-wide v4, v0, Lcom/google/android/gms/icing/g;->i:J

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(J)Z

    move-result v0

    if-nez v0, :cond_2

    .line 746
    new-instance v0, Lcom/google/android/gms/icing/impl/b/d;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to clear usage report data for corpus "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/impl/b/d;-><init>(Ljava/lang/String;)V

    throw v0

    .line 750
    :cond_2
    invoke-direct {p0, v2, v1}, Lcom/google/android/gms/icing/impl/u;->a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/ac;)Lcom/google/android/gms/icing/g;

    .line 751
    return-void
.end method

.method final a(Lcom/google/android/gms/icing/impl/a/e;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 570
    const-string v0, "unregisterFromResources: %s corpus %s"

    iget-object v1, p1, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-static {v0, v1, p2}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 571
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 572
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/p;->b:Lcom/google/android/gms/icing/impl/a/j;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/icing/impl/a/x;->a(Lcom/google/android/gms/icing/impl/a/e;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 573
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/p;->b:Lcom/google/android/gms/icing/impl/a/j;

    invoke-interface {v0, v1}, Lcom/google/android/gms/icing/impl/a/x;->c(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v0

    .line 574
    if-nez v0, :cond_0

    .line 575
    const-string v2, "Request to unregister non-existant resources corpus %s from package %s"

    iget-object v3, p1, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-static {v2, p2, v3}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 579
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/ac;->b()I

    move-result v0

    if-ne v0, v4, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 580
    invoke-virtual {p0, v1, p1}, Lcom/google/android/gms/icing/impl/u;->a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/e;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 581
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to unregister corpus from client "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 583
    :cond_1
    return-void

    .line 579
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 590
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/impl/u;->b(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Z

    .line 591
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 1547
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    .line 1550
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/f;->a()Lcom/google/android/gms/icing/impl/a/h;

    move-result-object v0

    .line 1551
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/h;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1552
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1555
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/f;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v0

    .line 1556
    iget-object v1, v0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/gms/icing/impl/q;->b(Ljava/lang/String;Z)V

    .line 1557
    return-void
.end method

.method public final a([Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1260
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    .line 1261
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/f;->a()Lcom/google/android/gms/icing/impl/a/h;

    move-result-object v0

    .line 1262
    iget-boolean v0, v0, Lcom/google/android/gms/icing/impl/a/h;->b:Z

    if-nez v0, :cond_0

    .line 1263
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1265
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v1, Lcom/google/android/gms/icing/impl/x;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/icing/impl/x;-><init>(Lcom/google/android/gms/icing/impl/u;[Ljava/lang/String;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    .line 1279
    return-void
.end method

.method public final a(Lcom/google/android/gms/appdatasearch/ResultClickInfo;)Z
    .locals 2

    .prologue
    .line 1078
    invoke-static {p1}, Lcom/google/android/gms/icing/impl/bb;->a(Lcom/google/android/gms/appdatasearch/ResultClickInfo;)Ljava/lang/String;

    move-result-object v0

    .line 1079
    if-eqz v0, :cond_0

    .line 1080
    const-string v1, "Bad ResultClickInfo: %s"

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    .line 1081
    const/4 v0, 0x0

    .line 1091
    :goto_0
    return v0

    .line 1084
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/f;->a()Lcom/google/android/gms/icing/impl/a/h;

    .line 1091
    const/4 v0, 0x1

    goto :goto_0
.end method

.method final a(Lcom/google/android/gms/icing/impl/a/e;)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2581
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 2584
    iget-object v0, p1, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/icing/impl/a/f;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->m:Lcom/google/android/gms/icing/impl/k;

    invoke-interface {v0}, Lcom/google/android/gms/icing/impl/k;->c()V

    .line 2587
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/impl/a/e;)Ljava/util/List;

    move-result-object v0

    .line 2588
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2589
    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/icing/impl/u;->a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/e;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz v1, :cond_2

    :cond_1
    move v0, v3

    :goto_1
    move v1, v0

    .line 2590
    goto :goto_0

    :cond_2
    move v0, v2

    .line 2589
    goto :goto_1

    .line 2591
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v4, v0, Lcom/google/android/gms/icing/impl/a/f;->b:Ljava/util/Map;

    monitor-enter v4

    if-eqz p1, :cond_4

    :try_start_0
    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/f;->b:Ljava/util/Map;

    iget-object v5, p1, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_5

    :goto_2
    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    iget-object v2, p1, Lcom/google/android/gms/icing/impl/a/e;->f:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p1, Lcom/google/android/gms/icing/impl/a/e;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {}, Lcom/google/android/gms/icing/impl/a/ac;->a()Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/icing/impl/a/e;->a(Lcom/google/android/gms/icing/impl/a/ac;)V
    :try_end_2
    .catch Lcom/google/android/gms/icing/impl/b/a; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/a/e;->i()V

    :cond_4
    monitor-exit v4

    .line 2593
    return v1

    :cond_5
    move v3, v2

    .line 2591
    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v4

    throw v0

    :catch_0
    move-exception v0

    :try_start_4
    new-instance v1, Lcom/google/android/gms/icing/impl/b/c;

    invoke-direct {v1, v0}, Lcom/google/android/gms/icing/impl/b/c;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1
.end method

.method final a(Lcom/google/android/gms/icing/impl/ax;)Z
    .locals 14

    .prologue
    .line 2216
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 2218
    const/4 v6, 0x1

    .line 2221
    iget-boolean v0, p1, Lcom/google/android/gms/icing/impl/ax;->a:Z

    if-eqz v0, :cond_1

    .line 2222
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->p()V

    .line 2228
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 2229
    invoke-static {}, Landroid/os/Debug;->threadCpuTimeNanos()J

    move-result-wide v12

    .line 2231
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->r:Lcom/google/android/gms/icing/impl/aw;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->q:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/impl/aw;->a(Ljava/lang/String;Landroid/content/Context;)Lcom/google/android/gms/icing/impl/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/u;->f:Lcom/google/android/gms/icing/impl/bn;

    .line 2232
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->f:Lcom/google/android/gms/icing/impl/bn;

    if-nez v0, :cond_2

    .line 2233
    const/4 v0, 0x0

    .line 2467
    :goto_0
    return v0

    .line 2223
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2225
    const/4 v0, 0x1

    goto :goto_0

    .line 2236
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AppDataSearch-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2237
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->r:Lcom/google/android/gms/icing/impl/aw;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/icing/impl/q;

    invoke-direct {v1, v0, v7}, Lcom/google/android/gms/icing/impl/q;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    .line 2238
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->r:Lcom/google/android/gms/icing/impl/aw;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/u;->u:Lcom/google/android/gms/icing/impl/a/z;

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/u;->m:Lcom/google/android/gms/icing/impl/k;

    new-instance v8, Lcom/google/android/gms/icing/impl/a/f;

    new-instance v0, Lcom/google/android/gms/icing/impl/a/g;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/icing/impl/a/g;-><init>(Lcom/google/android/gms/icing/impl/q;Landroid/content/Context;Landroid/content/pm/PackageManager;Lcom/google/android/gms/icing/impl/a/z;Lcom/google/android/gms/icing/impl/k;)V

    invoke-direct {v8, v0}, Lcom/google/android/gms/icing/impl/a/f;-><init>(Lcom/google/android/gms/icing/impl/a/g;)V

    iput-object v8, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    .line 2240
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->r:Lcom/google/android/gms/icing/impl/aw;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "-config"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->f:Lcom/google/android/gms/icing/impl/bn;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/bn;->b()Ljava/io/File;

    move-result-object v5

    new-instance v0, Lcom/google/android/gms/icing/impl/a/j;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/icing/impl/a/j;-><init>(Lcom/google/android/gms/icing/impl/n;Lcom/google/android/gms/icing/impl/a/f;Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)V

    .line 2242
    new-instance v1, Lcom/google/android/gms/icing/impl/al;

    invoke-direct {v1, p0}, Lcom/google/android/gms/icing/impl/al;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/Runnable;)Lcom/google/android/gms/icing/impl/a/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    .line 2248
    new-instance v0, Lcom/google/android/gms/icing/impl/e/i;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    iget-object v2, v2, Lcom/google/android/gms/icing/impl/a/p;->b:Lcom/google/android/gms/icing/impl/a/j;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/icing/impl/e/i;-><init>(Landroid/content/pm/PackageManager;Lcom/google/android/gms/icing/impl/a/x;Lcom/google/android/gms/icing/impl/a/f;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/u;->h:Lcom/google/android/gms/icing/impl/e/i;

    .line 2251
    :try_start_0
    new-instance v0, Lcom/google/android/gms/icing/impl/bf;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/icing/impl/bf;-><init>(Landroid/content/Context;Lcom/google/android/gms/icing/impl/q;Lcom/google/android/gms/icing/impl/m;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/u;->t:Lcom/google/android/gms/icing/impl/bf;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2258
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v10, v11}, Lcom/google/android/gms/icing/impl/u;->a(IJ)V

    .line 2260
    iget-boolean v0, p1, Lcom/google/android/gms/icing/impl/ax;->a:Z

    if-nez v0, :cond_3

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/u;->b(Lcom/google/android/gms/icing/impl/ax;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p1, Lcom/google/android/gms/icing/impl/ax;->a:Z

    .line 2262
    iget-boolean v0, p1, Lcom/google/android/gms/icing/impl/ax;->a:Z

    if-eqz v0, :cond_19

    .line 2263
    const-string v0, "Clearing storage"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;)I

    .line 2264
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->f:Lcom/google/android/gms/icing/impl/bn;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/bn;->a()Z

    move-result v0

    .line 2265
    if-nez v0, :cond_5

    .line 2266
    const-string v0, "Unable to clear storage, can\'t init index"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 2267
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    const-string v1, "clear_storage_failed"

    invoke-interface {v0, v1}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    .line 2268
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->p()V

    .line 2269
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2252
    :catch_0
    move-exception v0

    const-string v1, "Could not create extension manager"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 2254
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->p()V

    .line 2255
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2260
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 2271
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/a/j;->a()V

    .line 2272
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v2, v1, Lcom/google/android/gms/icing/impl/a/f;->b:Ljava/util/Map;

    monitor-enter v2

    :try_start_1
    iget-object v3, v1, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v3, v3, Lcom/google/android/gms/icing/impl/a/g;->a:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/q;->r()V

    iget-object v1, v1, Lcom/google/android/gms/icing/impl/a/f;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2273
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/q;->b()V

    .line 2274
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/q;->d()V

    .line 2277
    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->f:Lcom/google/android/gms/icing/impl/bn;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/bn;->b()Ljava/io/File;

    move-result-object v2

    .line 2279
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/google/android/gms/icing/impl/bm;->a(Landroid/content/Context;Ljava/io/File;)V

    .line 2283
    const/4 v1, 0x0

    .line 2284
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 2287
    :cond_6
    :try_start_2
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    if-nez v3, :cond_7

    .line 2288
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->r:Lcom/google/android/gms/icing/impl/aw;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/q;->u()Lcom/google/android/gms/icing/af;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(Ljava/io/File;Lcom/google/android/gms/icing/af;)Lcom/google/android/gms/icing/impl/NativeIndex;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    .line 2290
    :cond_7
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    if-nez v3, :cond_9

    .line 2291
    new-instance v0, Ljava/io/IOException;

    const-string v3, "Could not create native index"

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 2314
    :catch_1
    move-exception v0

    .line 2316
    const-string v3, "Error initializing, resetting corpora: %s"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    .line 2317
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    const-string v3, "init_io_exception"

    invoke-interface {v0, v3}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    .line 2319
    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/google/android/gms/icing/impl/ax;->b:Z

    .line 2320
    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/google/android/gms/icing/impl/ax;->c:Z

    .line 2322
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->f()Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    .line 2324
    :goto_3
    add-int/lit8 v1, v1, 0x1

    .line 2325
    const/4 v3, 0x1

    if-gt v1, v3, :cond_8

    if-nez v0, :cond_6

    .line 2327
    :cond_8
    :goto_4
    if-nez v0, :cond_c

    .line 2329
    const-string v0, "Internal init failed"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 2330
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->p()V

    .line 2331
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2272
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 2294
    :cond_9
    :try_start_3
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->b()Lcom/google/android/gms/icing/ad;

    move-result-object v3

    .line 2295
    if-nez v3, :cond_a

    .line 2296
    new-instance v0, Ljava/io/IOException;

    const-string v3, "Index init failed"

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2299
    :cond_a
    iget v6, v3, Lcom/google/android/gms/icing/ad;->b:I

    packed-switch v6, :pswitch_data_0

    .line 2307
    :goto_5
    iget-boolean v3, v3, Lcom/google/android/gms/icing/ad;->c:Z

    if-eqz v3, :cond_8

    .line 2308
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    const-string v6, "init_docstore_recovery"

    invoke-interface {v3, v6}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    .line 2309
    const/4 v3, 0x1

    iput-boolean v3, p1, Lcom/google/android/gms/icing/impl/ax;->b:Z

    goto :goto_4

    .line 2301
    :pswitch_0
    iget-object v6, p0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    const-string v7, "init_lite_lost"

    invoke-interface {v6, v7}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    goto :goto_5

    .line 2304
    :pswitch_1
    iget-object v6, p0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    const-string v7, "init_full_lost"

    invoke-interface {v6, v7}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_5

    .line 2322
    :cond_b
    const/4 v0, 0x0

    goto :goto_3

    .line 2335
    :cond_c
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->t:Lcom/google/android/gms/icing/impl/bf;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    const/4 v2, 0x0

    new-instance v3, Landroid/content/IntentFilter;

    const-string v6, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-direct {v3, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_11

    const/4 v0, 0x1

    :goto_6
    invoke-virtual {v1, v0}, Lcom/google/android/gms/icing/impl/bf;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2338
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->s()V

    .line 2342
    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->h()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2343
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->t:Lcom/google/android/gms/icing/impl/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/bf;->d()V

    .line 2346
    :cond_e
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v4, v5}, Lcom/google/android/gms/icing/impl/u;->a(IJ)V

    .line 2351
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->r:Lcom/google/android/gms/icing/impl/aw;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/u;->b:Lcom/google/android/gms/icing/impl/a;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    iget-object v6, p0, Lcom/google/android/gms/icing/impl/u;->f:Lcom/google/android/gms/icing/impl/bn;

    iget-object v7, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    iget-object v8, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    new-instance v0, Lcom/google/android/gms/icing/impl/e;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/icing/impl/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/icing/impl/a;Lcom/google/android/gms/icing/b/a;Lcom/google/android/gms/icing/impl/m;Lcom/google/android/gms/icing/impl/NativeIndex;Lcom/google/android/gms/icing/impl/bn;Lcom/google/android/gms/icing/impl/a/p;Lcom/google/android/gms/icing/impl/a/f;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/u;->k:Lcom/google/android/gms/icing/impl/e;

    .line 2355
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/a/f;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/e;->e()V

    .line 2358
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/q;->p()Z

    move-result v0

    if-nez v0, :cond_12

    const/4 v0, 0x1

    move v1, v0

    .line 2361
    :goto_7
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 2362
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/f;->b()Ljava/util/Set;

    move-result-object v0

    .line 2363
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_f
    :goto_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/a/e;

    .line 2364
    iget-object v2, v0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v6, v0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v2, v6}, Lcom/google/android/gms/icing/impl/q;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/e;->h()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_10

    invoke-static {v6, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_13

    :cond_10
    const/4 v2, 0x0

    :goto_9
    if-nez v2, :cond_14

    .line 2365
    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/impl/a/e;)Z

    goto :goto_8

    .line 2335
    :cond_11
    const/4 v0, 0x0

    goto/16 :goto_6

    .line 2358
    :cond_12
    const/4 v0, 0x0

    move v1, v0

    goto :goto_7

    .line 2364
    :cond_13
    const/4 v2, 0x1

    goto :goto_9

    .line 2366
    :cond_14
    if-eqz v1, :cond_f

    .line 2368
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/icing/impl/a/f;->f(Ljava/lang/String;)V

    goto :goto_8

    .line 2372
    :cond_15
    if-eqz v1, :cond_16

    .line 2373
    const-string v0, "Committed os upgrade: %s"

    sget-object v1, Landroid/os/Build;->ID:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 2374
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/q;->o()V

    .line 2377
    :cond_16
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v4, v5}, Lcom/google/android/gms/icing/impl/u;->a(IJ)V

    .line 2380
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/j;->m()Ljava/util/Map;

    move-result-object v0

    .line 2382
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2383
    const-string v3, "Found corpus [%s] in limbo"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/g;

    iget-object v1, v1, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 2384
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/g;

    iget-object v1, v1, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-virtual {v3, v1}, Lcom/google/android/gms/icing/impl/a/f;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v1

    .line 2385
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/icing/impl/u;->a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/e;)Z

    goto :goto_a

    .line 2388
    :cond_17
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 2389
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->t()V

    .line 2390
    const/4 v2, 0x7

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/android/gms/icing/impl/u;->a(IJ)V

    .line 2393
    const-string v0, "Internal init done: storage state %d"

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->f:Lcom/google/android/gms/icing/impl/bn;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->o()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/icing/impl/bn;->a(D)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;Ljava/lang/Object;)I

    .line 2399
    iget-boolean v0, p1, Lcom/google/android/gms/icing/impl/ax;->b:Z

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/j;->e()Ljava/util/Set;

    move-result-object v0

    .line 2401
    :goto_b
    iget-boolean v1, p1, Lcom/google/android/gms/icing/impl/ax;->a:Z

    .line 2402
    iget-boolean v2, p1, Lcom/google/android/gms/icing/impl/ax;->c:Z

    .line 2403
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v4, Lcom/google/android/gms/icing/impl/am;

    invoke-direct {v4, p0, v1, v2, v0}, Lcom/google/android/gms/icing/impl/am;-><init>(Lcom/google/android/gms/icing/impl/u;ZZLjava/util/Set;)V

    const-wide/16 v0, 0x0

    invoke-virtual {v3, v4, v0, v1}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    .line 2463
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v10, v11}, Lcom/google/android/gms/icing/impl/u;->a(IJ)V

    .line 2465
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    const/4 v1, 0x2

    invoke-static {}, Landroid/os/Debug;->threadCpuTimeNanos()J

    move-result-wide v2

    sub-long/2addr v2, v12

    const-wide/32 v4, 0xf4240

    div-long/2addr v2, v4

    long-to-int v2, v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/icing/impl/m;->a(II)V

    .line 2467
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2399
    :cond_18
    const/4 v0, 0x0

    goto :goto_b

    :cond_19
    move v0, v6

    goto/16 :goto_2

    .line 2299
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method final a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/e;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2544
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 2546
    const-string v2, "Removing corpus key %s for package %s"

    iget-object v3, p2, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-static {v2, p1, v3}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 2548
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v2

    .line 2551
    invoke-virtual {v2, p1}, Lcom/google/android/gms/icing/impl/a/j;->g(Ljava/lang/String;)Z

    move-result v3

    .line 2553
    invoke-virtual {v2, p1, p2}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/e;)I

    move-result v4

    .line 2554
    if-gez v4, :cond_1

    .line 2576
    :cond_0
    :goto_0
    return v0

    .line 2559
    :cond_1
    if-eqz v3, :cond_2

    .line 2560
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v2, p1}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.google.android.gms.icing.IME_NOTIFICATION"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v7, "type"

    invoke-virtual {v6, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "corpus"

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/android/gms/icing/impl/a/f;->a(Landroid/content/Intent;)V

    .line 2564
    :cond_2
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/impl/NativeIndex;->d(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2565
    const-string v0, "Failed to delete corpus key %s"

    invoke-static {v0, p1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    .line 2566
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    const-string v2, "unregister_failed"

    invoke-interface {v0, v2}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2567
    goto :goto_0

    .line 2570
    :cond_3
    invoke-virtual {v2, p1, p2}, Lcom/google/android/gms/icing/impl/a/j;->b(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/e;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2571
    const-string v0, "Failed to completely deactivate corpus key %s"

    invoke-static {v0, p1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    .line 2572
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    const-string v2, "unregister_failed"

    invoke-interface {v0, v2}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2573
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/appdatasearch/RequestIndexingSpecification;)Z
    .locals 3

    .prologue
    .line 970
    new-instance v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;

    invoke-direct {v0}, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;-><init>()V

    .line 971
    iput-object p1, v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;->a:Ljava/lang/String;

    .line 972
    iput-object p2, v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;->b:Ljava/lang/String;

    .line 973
    iput-wide p3, v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;->c:J

    .line 974
    new-instance v1, Lcom/google/android/gms/search/corpora/l;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/gms/search/corpora/l;-><init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;)V

    .line 975
    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/corpora/l;

    invoke-virtual {v0}, Lcom/google/android/gms/search/corpora/l;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;

    iget-object v0, v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    return v0
.end method

.method public final a([BZ)Z
    .locals 3

    .prologue
    .line 2860
    new-instance v0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;

    invoke-direct {v0}, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;-><init>()V

    .line 2861
    iput-object p1, v0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;->a:[B

    .line 2862
    iput-boolean p2, v0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;->b:Z

    .line 2863
    new-instance v1, Lcom/google/android/gms/search/global/s;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/gms/search/global/s;-><init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;)V

    .line 2864
    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/global/s;

    invoke-virtual {v0}, Lcom/google/android/gms/search/global/s;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Response;

    iget-object v0, v0, Lcom/google/android/gms/search/global/SetExperimentIdsCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    return v0
.end method

.method public final a()[Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;
    .locals 4

    .prologue
    .line 1136
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->d()[Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;

    move-result-object v1

    .line 1137
    array-length v0, v1

    new-array v2, v0, [Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    .line 1138
    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 1139
    aget-object v3, v1, v0

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->b:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    aput-object v3, v2, v0

    .line 1138
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1141
    :cond_0
    return-object v2
.end method

.method public final a(Ljava/lang/String;)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 990
    invoke-static {}, Lcom/google/android/gms/icing/impl/bb;->a()Ljava/lang/String;

    .line 991
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    .line 997
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/p;->b:Lcom/google/android/gms/icing/impl/a/j;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/icing/impl/a/f;->a(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/aa;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/icing/impl/a/x;->b(Lcom/google/android/gms/icing/impl/a/aa;I)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 7

    .prologue
    .line 783
    invoke-static {p2}, Lcom/google/android/gms/icing/impl/bb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 784
    if-eqz v0, :cond_0

    .line 785
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 788
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    .line 790
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/f;->a(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/aa;

    move-result-object v0

    .line 791
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/icing/impl/a/f;->a(Lcom/google/android/gms/icing/impl/a/aa;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v3

    .line 794
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 795
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 796
    iget-object v6, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v0, Lcom/google/android/gms/icing/impl/au;

    move-object v1, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/icing/impl/au;-><init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/e;Ljava/util/List;Ljava/util/List;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v6, v0, v2, v3}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/au;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/au;->e()Ljava/lang/Object;

    .line 829
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 830
    const-string v1, "content_provider_uris"

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 831
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [Z

    .line 832
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 833
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    aput-boolean v0, v3, v1

    .line 832
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 835
    :cond_1
    const-string v0, "success"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 836
    return-object v2
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1980
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    .line 1981
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/f;->a()Lcom/google/android/gms/icing/impl/a/h;

    move-result-object v0

    .line 1982
    iget-boolean v0, v0, Lcom/google/android/gms/icing/impl/a/h;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1983
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->M()Lcom/google/android/gms/icing/b/j;

    .line 1984
    return-void
.end method

.method public final b(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1727
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Got storage broadcast: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;)I

    .line 1728
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1729
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1730
    const-string v1, "Couldn\'t handle %s intent due to initialization failure."

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    .line 1742
    :goto_0
    return-void

    .line 1734
    :cond_0
    const-string v1, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1735
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v1, Lcom/google/android/gms/icing/impl/ab;

    invoke-direct {v1, p0}, Lcom/google/android/gms/icing/impl/ab;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    goto :goto_0

    .line 1741
    :cond_1
    const-string v1, "android.intent.action.DEVICE_STORAGE_OK"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1742
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v1, Lcom/google/android/gms/icing/impl/ac;

    invoke-direct {v1, p0}, Lcom/google/android/gms/icing/impl/ac;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    goto :goto_0

    .line 1749
    :cond_2
    new-instance v1, Lcom/google/android/gms/icing/impl/b/c;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown intent action "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/icing/impl/b/c;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method final b(Lcom/google/android/gms/icing/impl/a/e;)V
    .locals 6

    .prologue
    .line 2777
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 2778
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v2

    .line 2779
    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/a/e;->b()Ljava/util/Set;

    move-result-object v0

    .line 2780
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2781
    invoke-virtual {v2, v0}, Lcom/google/android/gms/icing/impl/a/j;->c(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v4

    .line 2783
    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/i;

    iget-object v1, v1, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget v1, v1, Lcom/google/android/gms/icing/j;->d:I

    if-nez v1, :cond_0

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/i;

    iget-object v1, v1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/a/j;->f(Lcom/google/android/gms/icing/g;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2785
    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/i;

    iget-object v1, v1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    .line 2786
    invoke-static {}, Lcom/google/android/gms/icing/az;->a()[Lcom/google/android/gms/icing/az;

    move-result-object v5

    iput-object v5, v1, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    .line 2788
    :try_start_0
    invoke-virtual {v4, v1}, Lcom/google/android/gms/icing/impl/a/ac;->a(Ljava/lang/Object;)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/ac;)V
    :try_end_0
    .catch Lcom/google/android/gms/icing/impl/b/a; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2789
    :catch_0
    move-exception v0

    const-string v1, "Failed to set corpus config on reparse"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v4}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 2792
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    const-string v1, "reparse_sourcecheck_failed"

    invoke-interface {v0, v1}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2796
    :cond_1
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1102
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1103
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/icing/impl/u;->a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)V

    .line 1104
    return-void
.end method

.method public final b([Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1283
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    .line 1284
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/f;->a()Lcom/google/android/gms/icing/impl/a/h;

    move-result-object v0

    .line 1285
    iget-boolean v0, v0, Lcom/google/android/gms/icing/impl/a/h;->b:Z

    if-nez v0, :cond_0

    .line 1286
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1288
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v1, Lcom/google/android/gms/icing/impl/y;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/icing/impl/y;-><init>(Lcom/google/android/gms/icing/impl/u;[Ljava/lang/String;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    .line 1299
    return-void
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 597
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/google/android/gms/icing/impl/bb;->a(Landroid/content/Context;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Ljava/lang/String;

    move-result-object v0

    .line 598
    if-eqz v0, :cond_0

    .line 599
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 602
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    .line 604
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/f;->a(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/aa;

    move-result-object v0

    .line 606
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {p2, v4, v5}, Lcom/google/android/gms/icing/impl/a/ac;->a(Ljava/lang/Object;J)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v3

    .line 610
    iget-object v4, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v5, Lcom/google/android/gms/icing/impl/as;

    invoke-direct {v5, p0, v0, v3}, Lcom/google/android/gms/icing/impl/as;-><init>(Lcom/google/android/gms/icing/impl/u;Lcom/google/android/gms/icing/impl/a/aa;Lcom/google/android/gms/icing/impl/a/ac;)V

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/as;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/as;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    .line 626
    if-eqz v0, :cond_3

    .line 627
    const-string v3, "Client exception"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 628
    instance-of v3, v0, Lcom/google/android/gms/icing/impl/b/a;

    if-eqz v3, :cond_1

    .line 629
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 630
    :cond_1
    instance-of v3, v0, Ljava/lang/SecurityException;

    if-eqz v3, :cond_2

    .line 631
    check-cast v0, Ljava/lang/SecurityException;

    throw v0

    .line 632
    :cond_2
    instance-of v3, v0, Lcom/google/android/gms/icing/impl/b/d;

    if-eqz v3, :cond_3

    .line 633
    const-string v1, "Internal error"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 638
    :goto_0
    return v2

    .line 637
    :cond_3
    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    move v2, v1

    .line 638
    goto :goto_0

    :cond_4
    move v0, v2

    .line 637
    goto :goto_1
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;
    .locals 2

    .prologue
    .line 1016
    new-instance v0, Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Request;

    invoke-direct {v0}, Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Request;-><init>()V

    .line 1017
    iput-object p1, v0, Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Request;->a:Ljava/lang/String;

    .line 1018
    iput-object p2, v0, Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Request;->b:Ljava/lang/String;

    .line 1019
    new-instance v1, Lcom/google/android/gms/search/corpora/f;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/gms/search/corpora/f;-><init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Request;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/corpora/f;

    invoke-virtual {v0}, Lcom/google/android/gms/search/corpora/f;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Response;

    iget-object v0, v0, Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Response;->b:Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    return-object v0
.end method

.method public final c()Lcom/google/android/gms/appdatasearch/StorageStats;
    .locals 10

    .prologue
    .line 1252
    new-instance v0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Request;

    invoke-direct {v0}, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Request;-><init>()V

    .line 1253
    new-instance v1, Lcom/google/android/gms/search/administration/d;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/gms/search/administration/d;-><init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/administration/GetStorageStatsCall$Request;)V

    .line 1254
    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/administration/d;

    invoke-virtual {v0}, Lcom/google/android/gms/search/administration/d;->e()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;

    .line 1255
    iget-object v0, v7, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->b:[Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;

    array-length v0, v0

    new-array v9, v0, [Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;

    const/4 v0, 0x0

    move v8, v0

    :goto_0
    iget-object v0, v7, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->b:[Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;

    array-length v0, v0

    if-ge v8, v0, :cond_0

    iget-object v0, v7, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->b:[Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;

    aget-object v5, v0, v8

    new-instance v0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;

    iget-object v1, v5, Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;->a:Ljava/lang/String;

    iget-wide v2, v5, Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;->b:J

    iget-boolean v4, v5, Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;->c:Z

    iget-wide v5, v5, Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;->d:J

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;-><init>(Ljava/lang/String;JZJ)V

    aput-object v0, v9, v8

    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/android/gms/appdatasearch/StorageStats;

    iget-wide v2, v7, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->c:J

    iget-wide v4, v7, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->d:J

    iget-wide v6, v7, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->e:J

    move-object v1, v9

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/appdatasearch/StorageStats;-><init>([Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;JJJ)V

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1304
    const/4 v0, 0x0

    return v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1310
    return-void
.end method

.method public final d()[Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;
    .locals 13

    .prologue
    const/4 v10, 0x0

    .line 1146
    new-instance v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;

    invoke-direct {v0}, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;-><init>()V

    .line 1147
    new-instance v1, Lcom/google/android/gms/search/global/h;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/gms/search/global/h;-><init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;)V

    .line 1149
    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/global/h;

    invoke-virtual {v0}, Lcom/google/android/gms/search/global/h;->e()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;

    .line 1150
    iget-object v0, v8, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;->b:[Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;

    array-length v0, v0

    new-array v11, v0, [Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;

    move v9, v10

    :goto_0
    array-length v0, v11

    if-ge v9, v0, :cond_1

    iget-object v0, v8, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;->b:[Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;

    aget-object v12, v0, v9

    new-instance v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    iget-object v1, v12, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->a:Ljava/lang/String;

    iget v2, v12, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->b:I

    iget v3, v12, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->c:I

    iget v4, v12, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->d:I

    iget-object v5, v12, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->e:Ljava/lang/String;

    iget-object v6, v12, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->f:Ljava/lang/String;

    iget-object v7, v12, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->g:Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;-><init>(Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/gms/appdatasearch/r;

    invoke-direct {v1}, Lcom/google/android/gms/appdatasearch/r;-><init>()V

    iput-object v0, v1, Lcom/google/android/gms/appdatasearch/r;->a:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    iget-object v2, v12, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->h:[Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;

    array-length v3, v2

    move v0, v10

    :goto_1
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    iget-object v5, v4, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;->a:Ljava/lang/String;

    iget-object v4, v4, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;->b:[Lcom/google/android/gms/appdatasearch/Feature;

    iget-object v6, v1, Lcom/google/android/gms/appdatasearch/r;->b:Ljava/util/Map;

    invoke-static {v5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v6, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    iget-boolean v0, v12, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->i:Z

    iput-boolean v0, v1, Lcom/google/android/gms/appdatasearch/r;->c:Z

    new-instance v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;

    iget-object v2, v1, Lcom/google/android/gms/appdatasearch/r;->a:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    iget-boolean v3, v1, Lcom/google/android/gms/appdatasearch/r;->c:Z

    iget-object v1, v1, Lcom/google/android/gms/appdatasearch/r;->b:Ljava/util/Map;

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;-><init>(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;ZLjava/util/Map;)V

    aput-object v0, v11, v9

    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_0

    :cond_1
    return-object v11
.end method

.method public final dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 16

    .prologue
    .line 1157
    const-string v2, "Icing on the Cake"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1158
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/icing/impl/u;->l()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1159
    const-string v2, "Init failed"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1247
    :cond_0
    :goto_0
    return-void

    .line 1164
    :cond_1
    const/4 v3, 0x0

    .line 1165
    const/4 v2, 0x0

    .line 1166
    if-eqz p3, :cond_4

    .line 1167
    move-object/from16 v0, p3

    array-length v5, v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_4

    aget-object v6, p3, v4

    .line 1168
    const-string v7, "native"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1169
    const/4 v3, 0x1

    .line 1167
    :cond_2
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1170
    :cond_3
    const-string v7, "verbose"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1171
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    move v4, v3

    move v3, v2

    .line 1177
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 1179
    const-string v5, "Apk version code: %d\n"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v6}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 1180
    const-string v5, "Apk version name: %s\n"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    aput-object v2, v6, v7

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v6}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1184
    :goto_3
    const-string v2, "Version: %d\n"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v7}, Lcom/google/android/gms/icing/impl/q;->c()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v5}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 1185
    const-string v2, "Extension:\n"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v5}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 1186
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/u;->t:Lcom/google/android/gms/icing/impl/bf;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/icing/impl/bf;->a(Ljava/io/PrintWriter;)V

    .line 1187
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/u;->f:Lcom/google/android/gms/icing/impl/bn;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v5}, Lcom/google/android/gms/icing/impl/NativeIndex;->o()D

    move-result-wide v6

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v6, v7}, Lcom/google/android/gms/icing/impl/bn;->a(Ljava/io/PrintWriter;D)V

    .line 1188
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->j()Lcom/google/android/gms/icing/f;

    move-result-object v2

    .line 1189
    new-instance v5, Ljava/util/Date;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    iget-object v6, v6, Lcom/google/android/gms/icing/impl/a/p;->b:Lcom/google/android/gms/icing/impl/a/j;

    invoke-interface {v6}, Lcom/google/android/gms/icing/impl/a/x;->r()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 1190
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v6}, Lcom/google/android/gms/icing/impl/NativeIndex;->k()Lcom/google/android/gms/icing/r;

    move-result-object v6

    .line 1191
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v7}, Lcom/google/android/gms/icing/impl/NativeIndex;->l()Lcom/google/android/gms/icing/r;

    move-result-object v7

    .line 1192
    new-instance v8, Ljava/util/Date;

    iget-wide v10, v7, Lcom/google/android/gms/icing/r;->a:J

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    invoke-direct {v8, v10, v11}, Ljava/util/Date;-><init>(J)V

    .line 1193
    new-instance v9, Ljava/util/Date;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v10}, Lcom/google/android/gms/icing/impl/q;->f()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    .line 1194
    new-instance v10, Ljava/util/Date;

    iget-wide v12, v7, Lcom/google/android/gms/icing/r;->a:J

    const-wide/16 v14, 0x3e8

    mul-long/2addr v12, v14

    invoke-direct {v10, v12, v13}, Ljava/util/Date;-><init>(J)V

    .line 1195
    new-instance v11, Ljava/util/Date;

    iget-wide v12, v2, Lcom/google/android/gms/icing/f;->a:J

    const-wide/16 v14, 0x3e8

    mul-long/2addr v12, v14

    invoke-direct {v11, v12, v13}, Ljava/util/Date;-><init>(J)V

    .line 1196
    const/4 v12, 0x2

    const/4 v13, 0x2

    sget-object v14, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v12, v13, v14}, Ljava/text/SimpleDateFormat;->getDateTimeInstance(IILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v12

    .line 1199
    const-string v13, "Created \"%s\"\n"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-virtual {v12, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v14, v15

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 1200
    const-string v5, "Committed \"%s\"\n"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-virtual {v12, v8}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v13, v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v13}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 1201
    const-string v5, "Maintained \"%s\"\n"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-virtual {v12, v9}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v8}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 1202
    const-string v5, "Flushed \"%s\" num docs %d\n"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v12, v10}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget v10, v7, Lcom/google/android/gms/icing/r;->d:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v8}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 1204
    const-string v5, "Compacted \"%s\" num docs %d old %d trimmed %d err %d\n"

    const/4 v8, 0x5

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v12, v11}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget v10, v2, Lcom/google/android/gms/icing/f;->b:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    iget v10, v2, Lcom/google/android/gms/icing/f;->c:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    iget v10, v2, Lcom/google/android/gms/icing/f;->d:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x4

    iget v2, v2, Lcom/google/android/gms/icing/f;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v8, v9

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v8}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 1210
    const-string v5, "App params %sinp: %s lu: %s ttl: %d\n"

    const/4 v2, 0x4

    new-array v8, v2, [Ljava/lang/Object;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/q;->z()Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, ""

    :goto_4
    aput-object v2, v8, v9

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v9}, Lcom/google/android/gms/icing/impl/q;->i()Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v8, v2

    const/4 v2, 0x2

    new-instance v9, Ljava/util/Date;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v10}, Lcom/google/android/gms/icing/impl/q;->j()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v12, v9}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v2

    const/4 v2, 0x3

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v9}, Lcom/google/android/gms/icing/impl/q;->k()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v8}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 1215
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/q;->m()Lcom/google/android/gms/icing/e;

    move-result-object v5

    .line 1216
    const-string v8, "App history upl %sp: %s ls: %d/%d\n"

    const/4 v2, 0x4

    new-array v9, v2, [Ljava/lang/Object;

    const/4 v10, 0x0

    sget-object v2, Lcom/google/android/gms/icing/a/a;->D:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, ""

    :goto_5
    aput-object v2, v9, v10

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v10}, Lcom/google/android/gms/icing/impl/q;->n()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v9, v2

    const/4 v2, 0x2

    iget-wide v10, v5, Lcom/google/android/gms/icing/e;->a:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v9, v2

    const/4 v2, 0x3

    iget-wide v10, v5, Lcom/google/android/gms/icing/e;->c:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v9, v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v8, v9}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 1221
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/icing/impl/q;->a(Ljava/io/PrintWriter;)V

    .line 1222
    const-string v2, "\nCorpora:"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1223
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    iget-object v2, v2, Lcom/google/android/gms/icing/impl/a/p;->b:Lcom/google/android/gms/icing/impl/a/j;

    iget-object v5, v6, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    iget-object v6, v7, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    move-object/from16 v0, p2

    invoke-interface {v2, v0, v5, v6}, Lcom/google/android/gms/icing/impl/a/x;->a(Ljava/io/PrintWriter;[Lcom/google/android/gms/icing/s;[Lcom/google/android/gms/icing/s;)V

    .line 1224
    const-string v2, "\nClientInfo:"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1225
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/icing/impl/a/f;->a(Ljava/io/PrintWriter;)V

    .line 1227
    const-string v2, "\nCorpus Usage Stats:"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1228
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->p()Lcom/google/android/gms/icing/bi;

    move-result-object v2

    .line 1229
    if-eqz v2, :cond_7

    .line 1230
    iget-object v5, v2, Lcom/google/android/gms/icing/bi;->a:[Lcom/google/android/gms/icing/bj;

    array-length v6, v5

    const/4 v2, 0x0

    :goto_6
    if-ge v2, v6, :cond_8

    aget-object v7, v5, v2

    .line 1231
    const-string v8, "id: %d\n"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget v11, v7, Lcom/google/android/gms/icing/bj;->a:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    move-object/from16 v0, p2

    invoke-virtual {v0, v8, v9}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 1232
    const-string v8, "docs: %d\n"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget v11, v7, Lcom/google/android/gms/icing/bj;->b:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    move-object/from16 v0, p2

    invoke-virtual {v0, v8, v9}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 1233
    const-string v8, "size: %s\n"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-wide v12, v7, Lcom/google/android/gms/icing/bj;->d:J

    invoke-static {v12, v13}, Lcom/google/android/gms/icing/impl/bp;->a(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    move-object/from16 v0, p2

    invoke-virtual {v0, v8, v9}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 1234
    const-string v8, "deleted docs: %d\n"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget v11, v7, Lcom/google/android/gms/icing/bj;->c:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    move-object/from16 v0, p2

    invoke-virtual {v0, v8, v9}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 1235
    const-string v8, "deleted size: %d\n"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-wide v12, v7, Lcom/google/android/gms/icing/bj;->e:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v9, v10

    move-object/from16 v0, p2

    invoke-virtual {v0, v8, v9}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 1236
    invoke-virtual/range {p2 .. p2}, Ljava/io/PrintWriter;->println()V

    .line 1230
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 1210
    :cond_5
    const-string v2, "DISABLED "

    goto/16 :goto_4

    .line 1216
    :cond_6
    const-string v2, "DISABLED "

    goto/16 :goto_5

    .line 1239
    :cond_7
    const-string v2, "\nError getting usage stats"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1243
    :cond_8
    if-eqz v4, :cond_0

    .line 1244
    const-string v2, "\nNative Index:"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1245
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_0
    move-exception v2

    goto/16 :goto_3
.end method

.method public final e()[I
    .locals 3

    .prologue
    .line 2869
    new-instance v0, Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Request;

    invoke-direct {v0}, Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Request;-><init>()V

    .line 2870
    new-instance v1, Lcom/google/android/gms/search/global/c;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/gms/search/global/c;-><init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Request;)V

    .line 2871
    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/global/c;

    invoke-virtual {v0}, Lcom/google/android/gms/search/global/c;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Response;

    iget-object v0, v0, Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Response;->b:[I

    return-object v0
.end method

.method public final e(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2

    .prologue
    .line 1314
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    .line 1315
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/f;->a(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/aa;

    move-result-object v0

    .line 1318
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/aa;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1319
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1322
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/p;->b:Lcom/google/android/gms/icing/impl/a/j;

    invoke-interface {v0}, Lcom/google/android/gms/icing/impl/a/x;->u()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()[I
    .locals 3

    .prologue
    .line 2876
    new-instance v0, Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Request;

    invoke-direct {v0}, Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Request;-><init>()V

    .line 2877
    new-instance v1, Lcom/google/android/gms/search/global/k;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/gms/search/global/k;-><init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Request;)V

    .line 2878
    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/global/k;

    invoke-virtual {v0}, Lcom/google/android/gms/search/global/k;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Response;

    iget-object v0, v0, Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Response;->b:[I

    return-object v0
.end method

.method public final f(Ljava/lang/String;)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 1004
    invoke-static {}, Lcom/google/android/gms/icing/impl/bb;->a()Ljava/lang/String;

    .line 1005
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    .line 1011
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/p;->b:Lcom/google/android/gms/icing/impl/a/j;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/icing/impl/a/f;->a(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/aa;

    move-result-object v1

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/icing/impl/a/x;->b(Lcom/google/android/gms/icing/impl/a/aa;I)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final g(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1660
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 1661
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/u;->n(Ljava/lang/String;)V

    .line 1662
    return-void
.end method

.method public final g()Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1436
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    .line 1437
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/f;->a()Lcom/google/android/gms/icing/impl/a/h;

    move-result-object v0

    .line 1438
    iget-boolean v0, v0, Lcom/google/android/gms/icing/impl/a/h;->b:Z

    if-nez v0, :cond_0

    .line 1439
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1441
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v1, Lcom/google/android/gms/icing/impl/bl;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/gms/icing/impl/bl;-><init>(Lcom/google/android/gms/icing/impl/u;ZZ)V

    invoke-virtual {v0, v1, v4, v5}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    .line 1443
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v1, Lcom/google/android/gms/icing/impl/z;

    invoke-direct {v1, p0}, Lcom/google/android/gms/icing/impl/z;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    invoke-virtual {v0, v1, v4, v5}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/z;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/z;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 1470
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final h()Lcom/google/android/gms/appdatasearch/NativeApiInfo;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1515
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    .line 1517
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/f;->a()Lcom/google/android/gms/icing/impl/a/h;

    move-result-object v0

    .line 1518
    iget-boolean v0, v0, Lcom/google/android/gms/icing/impl/a/h;->d:Z

    if-nez v0, :cond_0

    .line 1519
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1522
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/libAppDataSearch.so"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1524
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1528
    invoke-virtual {v1, v4, v3}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1529
    const-string v1, "Unable to set readable to all permission for icing shared library."

    invoke-static {v1}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;)I

    .line 1532
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->t:Lcom/google/android/gms/icing/impl/bf;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/bf;->a()Ljava/io/File;

    move-result-object v1

    .line 1535
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1536
    invoke-virtual {v1, v4, v3}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1537
    const-string v1, "Unable to set readable to all permission for extension file."

    invoke-static {v1}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;)I

    .line 1540
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->t:Lcom/google/android/gms/icing/impl/bf;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/bf;->b()Ljava/lang/String;

    move-result-object v1

    .line 1541
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/u;->t:Lcom/google/android/gms/icing/impl/bf;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/bf;->c()Ljava/lang/String;

    move-result-object v2

    .line 1542
    new-instance v3, Lcom/google/android/gms/appdatasearch/NativeApiInfo;

    invoke-direct {v3, v0, v1, v2}, Lcom/google/android/gms/appdatasearch/NativeApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v3
.end method

.method final h(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1668
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 1669
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    .line 1670
    const-string v0, "handlePackageUpdating %s"

    invoke-static {v0, p1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 1671
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/f;->f(Ljava/lang/String;)V

    .line 1672
    return-void
.end method

.method public final i()Lcom/google/android/gms/icing/impl/a/x;
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/p;->b:Lcom/google/android/gms/icing/impl/a/j;

    return-object v0
.end method

.method final i(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1679
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 1680
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/u;->n(Ljava/lang/String;)V

    .line 1681
    return-void
.end method

.method public final j()Lcom/google/android/gms/icing/impl/a/j;
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v0

    return-object v0
.end method

.method final j(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1688
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 1689
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    .line 1691
    const-string v0, "handlePackageAdded %s"

    invoke-static {v0, p1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 1692
    new-instance v0, Lcom/google/android/gms/icing/impl/a/ab;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->o:Lcom/google/android/gms/icing/impl/a/i;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/icing/impl/a/ab;-><init>(Landroid/content/Context;Lcom/google/android/gms/icing/impl/a/f;Lcom/google/android/gms/icing/impl/a/i;)V

    .line 1693
    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/ab;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1694
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Package "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " not found"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 1696
    :cond_0
    return-void
.end method

.method public final k()V
    .locals 4

    .prologue
    .line 495
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 502
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    iget-object v1, v0, Lcom/google/android/gms/icing/b/a;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/icing/b/a;->e()V

    iget v2, v0, Lcom/google/android/gms/icing/b/a;->e:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/android/gms/icing/b/a;->e:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, v0, Lcom/google/android/gms/icing/b/a;->d:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V

    iget-object v1, v0, Lcom/google/android/gms/icing/b/a;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget v2, v0, Lcom/google/android/gms/icing/b/a;->e:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Lcom/google/android/gms/icing/b/a;->e:I

    invoke-virtual {v0}, Lcom/google/android/gms/icing/b/a;->e()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 504
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v1, Lcom/google/android/gms/icing/impl/ar;

    invoke-direct {v1, p0}, Lcom/google/android/gms/icing/impl/ar;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    .line 514
    return-void

    .line 502
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final k(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2527
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 2528
    new-instance v0, Lcom/google/android/gms/icing/impl/a/ab;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->o:Lcom/google/android/gms/icing/impl/a/i;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/icing/impl/a/ab;-><init>(Landroid/content/Context;Lcom/google/android/gms/icing/impl/a/f;Lcom/google/android/gms/icing/impl/a/i;)V

    .line 2530
    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/ab;->a(Ljava/lang/String;)Z

    .line 2531
    return-void
.end method

.method public final l(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/h;
    .locals 1

    .prologue
    .line 2895
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->K()V

    .line 2896
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/f;->a(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/aa;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/f;->a()Lcom/google/android/gms/icing/impl/a/h;

    move-result-object v0

    goto :goto_0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 533
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 534
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->J()Z

    move-result v0

    .line 540
    :goto_0
    return v0

    .line 536
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 538
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->J()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 540
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
.end method

.method final m()V
    .locals 2

    .prologue
    .line 1754
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 1757
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->t:Lcom/google/android/gms/icing/impl/bf;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/bf;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1758
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->s()V

    .line 1760
    :cond_0
    return-void
.end method

.method final n()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    .line 1763
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 1766
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->t:Lcom/google/android/gms/icing/impl/bf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/bf;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1767
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->s()V

    .line 1770
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->k:Lcom/google/android/gms/icing/impl/e;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/a/j;->f()Ljava/util/Set;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/e;->b:Lcom/google/android/gms/icing/b/a;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/b/a;->b(I)V

    const-wide/16 v2, 0x3e8

    const-wide/32 v4, 0x493e0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/icing/impl/e;->a(Ljava/util/Set;JJ)V

    .line 1771
    return-void
.end method

.method public final o()Lcom/google/android/gms/icing/b/j;
    .locals 4

    .prologue
    .line 2052
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v1, Lcom/google/android/gms/icing/impl/aj;

    invoke-direct {v1, p0}, Lcom/google/android/gms/icing/impl/aj;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    move-result-object v0

    return-object v0
.end method

.method final p()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2471
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 2473
    iput-object v2, p0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    .line 2474
    iput-object v2, p0, Lcom/google/android/gms/icing/impl/u;->f:Lcom/google/android/gms/icing/impl/bn;

    .line 2475
    iput-object v2, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    .line 2476
    iput-object v2, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    .line 2477
    iput-object v2, p0, Lcom/google/android/gms/icing/impl/u;->t:Lcom/google/android/gms/icing/impl/bf;

    .line 2478
    iput-object v2, p0, Lcom/google/android/gms/icing/impl/u;->h:Lcom/google/android/gms/icing/impl/e/i;

    .line 2479
    iput-object v2, p0, Lcom/google/android/gms/icing/impl/u;->k:Lcom/google/android/gms/icing/impl/e;

    .line 2481
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2483
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->e()V

    .line 2484
    iput-object v2, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    .line 2486
    :cond_0
    return-void
.end method

.method final q()V
    .locals 7

    .prologue
    .line 2489
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/j;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    .line 2490
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/icing/impl/q;->a(Lcom/google/android/gms/icing/g;)Lcom/google/android/gms/icing/aa;

    move-result-object v2

    .line 2491
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    iget-wide v4, v0, Lcom/google/android/gms/icing/g;->i:J

    iget v6, v0, Lcom/google/android/gms/icing/g;->a:I

    invoke-virtual {v3, v4, v5, v6, v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(JILcom/google/android/gms/icing/aa;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2492
    const-string v2, "Add corpus from %s failed"

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0

    .line 2495
    :cond_1
    return-void
.end method

.method final r()V
    .locals 2

    .prologue
    .line 2499
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 2500
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2501
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->g()V

    .line 2502
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/j;->o()V

    .line 2504
    :cond_0
    return-void
.end method

.method final s()V
    .locals 2

    .prologue
    .line 2507
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 2509
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->d()Z

    .line 2510
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->t:Lcom/google/android/gms/icing/impl/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/bf;->d()V

    .line 2511
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    const-string v1, "index_rebuilt"

    invoke-interface {v0, v1}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    .line 2512
    return-void
.end method

.method final t()V
    .locals 4

    .prologue
    .line 2515
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 2516
    new-instance v0, Lcom/google/android/gms/icing/impl/a/ab;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->o:Lcom/google/android/gms/icing/impl/a/i;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/icing/impl/a/ab;-><init>(Landroid/content/Context;Lcom/google/android/gms/icing/impl/a/f;Lcom/google/android/gms/icing/impl/a/i;)V

    .line 2517
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/ab;->b()V

    .line 2518
    return-void
.end method

.method final u()V
    .locals 2

    .prologue
    .line 2597
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 2598
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->i()V

    .line 2599
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/j;->l()V

    .line 2600
    return-void
.end method

.method final v()V
    .locals 6

    .prologue
    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    .line 2603
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 2604
    const-string v0, "Starting compaction min disk %.3f%% min index %.3f%%"

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->f:Lcom/google/android/gms/icing/impl/bn;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/bn;->c()D

    move-result-wide v2

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->o()D

    move-result-wide v2

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 2607
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    sget v1, Lcom/google/android/gms/icing/impl/a/j;->a:I

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/a/j;->k()[I

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(I[I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2609
    const-string v0, "Compaction failed"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 2610
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    const-string v1, "compaction_failed"

    invoke-interface {v0, v1}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    .line 2614
    :goto_0
    return-void

    .line 2613
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/u;->N()V

    goto :goto_0
.end method

.method final w()V
    .locals 13

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 2662
    const-string v0, "Performing maintenance."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;)I

    .line 2663
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 2665
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/p;->b:Lcom/google/android/gms/icing/impl/a/j;

    invoke-interface {v0}, Lcom/google/android/gms/icing/impl/a/x;->b()Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 2666
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/q;->x()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2668
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/q;->e()Z

    move-result v6

    .line 2671
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lcom/google/android/gms/icing/b/a;->b(I)V

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->m:Lcom/google/android/gms/icing/impl/k;

    invoke-interface {v0}, Lcom/google/android/gms/icing/impl/k;->a()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    new-instance v8, Lcom/google/android/gms/icing/c/a/t;

    invoke-direct {v8}, Lcom/google/android/gms/icing/c/a/t;-><init>()V

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x2

    new-array v10, v0, [I

    fill-array-data v10, :array_0

    move v3, v1

    :goto_0
    array-length v0, v10

    if-ge v3, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->s:Lcom/google/android/gms/icing/c/b;

    aget v11, v10, v3

    invoke-interface {v0, v7, v11}, Lcom/google/android/gms/icing/c/b;->a(Ljava/lang/String;I)Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v11, Lcom/google/android/gms/udc/e/p;

    invoke-direct {v11}, Lcom/google/android/gms/udc/e/p;-><init>()V

    aget v12, v10, v3

    iput v12, v11, Lcom/google/android/gms/udc/e/p;->a:I

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    iput v0, v11, Lcom/google/android/gms/udc/e/p;->b:I

    invoke-interface {v9, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x3

    goto :goto_1

    :cond_2
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/gms/udc/e/p;

    invoke-interface {v9, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/udc/e/p;

    iput-object v0, v8, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    invoke-direct {p0, v7, v8}, Lcom/google/android/gms/icing/impl/u;->a(Ljava/lang/String;Lcom/google/android/gms/icing/c/a/t;)V

    .line 2677
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/b/a;->b(I)V

    new-instance v0, Lcom/google/android/gms/icing/impl/a/ab;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v7, p0, Lcom/google/android/gms/icing/impl/u;->o:Lcom/google/android/gms/icing/impl/a/i;

    invoke-direct {v0, v2, v3, v7}, Lcom/google/android/gms/icing/impl/a/ab;-><init>(Landroid/content/Context;Lcom/google/android/gms/icing/impl/a/f;Lcom/google/android/gms/icing/impl/a/i;)V

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/ab;->a()V

    .line 2680
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/q;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2681
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v2, Lcom/google/android/gms/search/queries/t;

    invoke-direct {v2, p0}, Lcom/google/android/gms/search/queries/t;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    const-wide/16 v8, 0x0

    invoke-virtual {v0, v2, v8, v9}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    .line 2684
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->j()Lcom/google/android/gms/icing/f;

    move-result-object v0

    iget-wide v2, v0, Lcom/google/android/gms/icing/f;->a:J

    const-wide/16 v8, 0x3e8

    mul-long/2addr v2, v8

    .line 2685
    sget-object v0, Lcom/google/android/gms/icing/a/a;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 2686
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    add-long/2addr v2, v8

    cmp-long v0, v10, v2

    if-lez v0, :cond_7

    const/4 v0, 0x1

    .line 2689
    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->f:Lcom/google/android/gms/icing/impl/bn;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->o()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/icing/impl/bn;->b(D)D

    move-result-wide v2

    .line 2693
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->t:Lcom/google/android/gms/icing/impl/bf;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/icing/impl/bf;->b(Z)Z

    move-result v1

    if-nez v1, :cond_5

    if-eqz v6, :cond_6

    .line 2698
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->q()V

    .line 2699
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    iget-object v7, p0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v7}, Lcom/google/android/gms/icing/impl/q;->u()Lcom/google/android/gms/icing/af;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(Lcom/google/android/gms/icing/af;)Z

    .line 2702
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->s()V

    .line 2705
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(Z)V

    .line 2707
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v1, v6}, Lcom/google/android/gms/icing/impl/q;->a(Z)V

    .line 2710
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->t:Lcom/google/android/gms/icing/impl/bf;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/bf;->e()Lcom/google/android/gms/icing/b;

    move-result-object v1

    .line 2711
    iget-object v6, p0, Lcom/google/android/gms/icing/impl/u;->f:Lcom/google/android/gms/icing/impl/bn;

    iget-object v7, p0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    iget-object v8, p0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v8}, Lcom/google/android/gms/icing/impl/NativeIndex;->o()D

    move-result-wide v8

    invoke-virtual {v6, v7, v1, v8, v9}, Lcom/google/android/gms/icing/impl/bn;->a(Lcom/google/android/gms/icing/impl/m;Lcom/google/android/gms/icing/b;D)V

    .line 2713
    const-wide/16 v6, 0x0

    cmpl-double v1, v2, v6

    if-eqz v1, :cond_8

    .line 2714
    invoke-virtual {p0, v2, v3}, Lcom/google/android/gms/icing/impl/u;->a(D)V

    .line 2720
    :goto_3
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2721
    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    :cond_7
    move v0, v1

    .line 2686
    goto :goto_2

    .line 2715
    :cond_8
    if-eqz v0, :cond_9

    .line 2716
    :try_start_3
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->v()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 2720
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v5

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2721
    :catchall_1
    move-exception v0

    monitor-exit v4

    throw v0

    .line 2718
    :cond_9
    :try_start_5
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->u()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    .line 2671
    nop

    :array_0
    .array-data 4
        0x8
        0x7
    .end array-data
.end method

.method public final x()V
    .locals 6

    .prologue
    .line 2758
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 2759
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2760
    const-string v0, "Global Search Section Mappings reparsing skipped because init failed"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 2774
    :goto_0
    return-void

    .line 2763
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    new-instance v2, Lcom/google/android/gms/icing/impl/ao;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/icing/impl/ao;-><init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;)V

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    goto :goto_0
.end method

.method final y()V
    .locals 9

    .prologue
    .line 2804
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->c:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 2805
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->i:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/f;->b()Ljava/util/Set;

    move-result-object v0

    .line 2807
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->g:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v2

    .line 2808
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/a/e;

    .line 2809
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/e;->j()Landroid/content/res/Resources;

    move-result-object v4

    .line 2810
    if-nez v4, :cond_1

    .line 2811
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Can\'t get resources for package: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    goto :goto_0

    .line 2814
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/e;->b()Ljava/util/Set;

    move-result-object v1

    .line 2815
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2816
    invoke-virtual {v2, v1}, Lcom/google/android/gms/icing/impl/a/j;->c(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v6

    .line 2818
    invoke-virtual {v6}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/i;

    iget-object v1, v1, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget v1, v1, Lcom/google/android/gms/icing/j;->d:I

    if-nez v1, :cond_2

    .line 2819
    invoke-virtual {v6}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/i;

    iget-object v7, v1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    .line 2820
    iget-object v1, v7, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    array-length v1, v1

    if-eqz v1, :cond_2

    .line 2821
    new-instance v1, Lcom/google/android/gms/icing/impl/c/c;

    invoke-direct {v1, v7, v4}, Lcom/google/android/gms/icing/impl/c/c;-><init>(Lcom/google/android/gms/icing/g;Landroid/content/res/Resources;)V

    .line 2826
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/c/c;->a()V

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/c/c;->b()Ljava/util/List;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/icing/az;->a()[Lcom/google/android/gms/icing/az;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    if-eqz v1, :cond_3

    iget-object v8, v7, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    invoke-interface {v1, v8}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/android/gms/icing/az;

    iput-object v1, v7, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;
    :try_end_0
    .catch Lcom/google/android/gms/icing/impl/b/b; {:try_start_0 .. :try_end_0} :catch_1

    .line 2833
    :cond_3
    iget-object v1, v7, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/impl/a/e;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2835
    :try_start_1
    invoke-virtual {v6, v7}, Lcom/google/android/gms/icing/impl/a/ac;->a(Ljava/lang/Object;)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v6

    invoke-virtual {v2, v1, v6}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/ac;)V
    :try_end_1
    .catch Lcom/google/android/gms/icing/impl/b/a; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 2836
    :catch_0
    move-exception v1

    const-string v6, "Failed to set corpus config on reparse"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v1, v6, v7}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 2839
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    const-string v6, "reparse_sourcecheck_failed"

    invoke-interface {v1, v6}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 2827
    :catch_1
    move-exception v1

    .line 2828
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "Error while reparsing mapping for packageName = "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", corpusName = "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v7, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", error = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/b/b;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    goto/16 :goto_1

    .line 2844
    :cond_4
    return-void
.end method

.method public final z()Landroid/content/Context;
    .locals 1

    .prologue
    .line 2884
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    return-object v0
.end method
