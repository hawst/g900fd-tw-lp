.class final Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;
.super Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/games/service/statemachine/roomclient/ch;)V
    .locals 2

    .prologue
    .line 515
    iput-object p1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    .line 516
    invoke-direct/range {p0 .. p5}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;-><init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/games/service/statemachine/roomclient/ch;)V

    .line 517
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->f:Ljava/lang/String;

    .line 518
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ch;->a(I)V

    .line 519
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 523
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 559
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 526
    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v3, v0

    check-cast v3, Lcom/google/android/gms/games/realtime/network/f;

    .line 527
    iget-boolean v0, v3, Lcom/google/android/gms/games/realtime/network/f;->b:Z

    if-eqz v0, :cond_0

    .line 529
    iget-object v8, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    new-instance v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->b:Ljava/lang/String;

    iget v3, v3, Lcom/google/android/gms/games/realtime/network/f;->c:I

    iget-object v4, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->f:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->c:I

    iget-object v6, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;-><init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;ILjava/lang/String;ILcom/google/android/gms/games/service/statemachine/roomclient/ch;)V

    iput-object v0, v8, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->d:Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

    :goto_1
    move v0, v7

    .line 540
    goto :goto_0

    .line 532
    :cond_0
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    const-string v1, "PeerStateMachine"

    const-string v2, "Confused about who is connecting to who"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/internal/dq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Landroid/os/Message;)V

    .line 537
    iget-object v8, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    new-instance v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->b:Ljava/lang/String;

    iget v3, v3, Lcom/google/android/gms/games/realtime/network/f;->c:I

    iget-object v4, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->f:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->c:I

    iget-object v6, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;-><init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;ILjava/lang/String;ILcom/google/android/gms/games/service/statemachine/roomclient/ch;)V

    iput-object v0, v8, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->d:Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

    goto :goto_1

    .line 545
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v5, v0

    check-cast v5, Lcom/google/android/gms/games/jingle/i;

    .line 546
    iget-boolean v0, v5, Lcom/google/android/gms/games/jingle/i;->b:Z

    if-eqz v0, :cond_1

    .line 547
    iget-object v8, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    new-instance v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedToUnsolicitedRemote;

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->f:Ljava/lang/String;

    new-instance v4, Lcom/google/android/gms/games/realtime/a;

    iget-object v6, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->b:Ljava/lang/String;

    iget v5, v5, Lcom/google/android/gms/games/jingle/i;->c:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v9, 0x0

    invoke-direct {v4, v6, v5, v9}, Lcom/google/android/gms/games/realtime/a;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    iget v5, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->c:I

    iget-object v6, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedToUnsolicitedRemote;-><init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/realtime/a;ILcom/google/android/gms/games/service/statemachine/roomclient/ch;)V

    iput-object v0, v8, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->d:Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

    :goto_2
    move v0, v7

    .line 556
    goto :goto_0

    .line 554
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    new-instance v1, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;

    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v3, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$AcceptingUnsolicitedRemoteConnection;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;-><init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;Lcom/google/android/gms/games/service/statemachine/roomclient/ch;)V

    iput-object v1, v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->d:Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

    goto :goto_2

    .line 523
    :sswitch_data_0
    .sparse-switch
        0x12d -> :sswitch_1
        0x1779 -> :sswitch_0
    .end sparse-switch
.end method
