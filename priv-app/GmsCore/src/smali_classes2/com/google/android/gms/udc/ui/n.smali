.class public final Lcom/google/android/gms/udc/ui/n;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Lcom/google/android/gms/common/ui/n;
.implements Lcom/google/android/gms/common/ui/o;


# instance fields
.field a:Lcom/google/android/gms/udc/util/f;

.field private b:Lcom/android/volley/toolbox/m;

.field private c:Lcom/google/android/gms/udc/ui/q;

.field private d:Lcom/google/android/gms/udc/ui/r;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Ljava/lang/CharSequence;

.field private j:Ljava/lang/CharSequence;

.field private k:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

.field private l:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 348
    return-void
.end method

.method private a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/google/android/gms/udc/e/c;I)Landroid/view/View;
    .locals 4

    .prologue
    .line 276
    sget v0, Lcom/google/android/gms/l;->fA:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 277
    iget-object v1, p0, Lcom/google/android/gms/udc/ui/n;->a:Lcom/google/android/gms/udc/util/f;

    sget v2, Lcom/google/android/gms/j;->tt:I

    iget-object v3, p3, Lcom/google/android/gms/udc/e/c;->a:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;ILcom/google/android/gms/udc/e/s;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/n;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 279
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 280
    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/gms/udc/e/e;Lcom/google/android/gms/udc/ConsentFlowConfig;Ljava/lang/Integer;Z)Lcom/google/android/gms/udc/ui/n;
    .locals 3

    .prologue
    .line 75
    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 76
    const-string v1, "UdcConsentConfig"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/udc/util/f;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 77
    const-string v1, "UdcAccountName"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const-string v1, "UdcConsentFlowConfig"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 79
    const-string v1, "UdcConsentViewType"

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 80
    const-string v1, "UdcConsentHasScrolledToEnd"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 82
    new-instance v1, Lcom/google/android/gms/udc/ui/n;

    invoke-direct {v1}, Lcom/google/android/gms/udc/ui/n;-><init>()V

    .line 83
    invoke-virtual {v1, v0}, Lcom/google/android/gms/udc/ui/n;->setArguments(Landroid/os/Bundle;)V

    .line 85
    return-object v1
.end method

.method private static a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 289
    sget v0, Lcom/google/android/gms/j;->tt:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 290
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 291
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/udc/ui/n;)Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/google/android/gms/udc/ui/n;->f:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/udc/ui/n;)Lcom/google/android/gms/udc/ui/q;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/n;->c:Lcom/google/android/gms/udc/ui/q;

    return-object v0
.end method

.method private b(Z)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 303
    iget-boolean v1, p0, Lcom/google/android/gms/udc/ui/n;->g:Z

    if-eqz v1, :cond_1

    if-eqz p1, :cond_1

    .line 304
    iget-boolean v1, p0, Lcom/google/android/gms/udc/ui/n;->h:Z

    if-nez v1, :cond_0

    .line 305
    iget-object v1, p0, Lcom/google/android/gms/udc/ui/n;->d:Lcom/google/android/gms/udc/ui/r;

    invoke-interface {v1}, Lcom/google/android/gms/udc/ui/r;->c()V

    .line 307
    :cond_0
    iput-boolean v0, p0, Lcom/google/android/gms/udc/ui/n;->h:Z

    .line 309
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/udc/ui/n;->h:Z

    if-nez v1, :cond_2

    if-eqz p1, :cond_4

    .line 310
    :cond_2
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/gms/udc/ui/n;->f:Z

    if-eq v0, v1, :cond_3

    .line 311
    iput-boolean v0, p0, Lcom/google/android/gms/udc/ui/n;->f:Z

    .line 312
    iget-boolean v0, p0, Lcom/google/android/gms/udc/ui/n;->f:Z

    if-eqz v0, :cond_5

    .line 313
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/n;->l:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/n;->i:Ljava/lang/CharSequence;

    sget v2, Lcom/google/android/gms/j;->tt:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 318
    :cond_3
    :goto_1
    return-void

    .line 309
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 315
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/n;->l:Landroid/view/View;

    sget v1, Lcom/google/android/gms/p;->zc:I

    invoke-static {v0, v1}, Lcom/google/android/gms/udc/ui/n;->a(Landroid/view/View;I)V

    goto :goto_1
.end method

.method static synthetic c(Lcom/google/android/gms/udc/ui/n;)Lcom/google/android/gms/common/ui/ScrollViewWithEvents;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/n;->k:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 327
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/udc/ui/n;->g:Z

    .line 328
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 322
    invoke-direct {p0, p1}, Lcom/google/android/gms/udc/ui/n;->b(Z)V

    .line 323
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 90
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    move-object v0, p1

    .line 91
    check-cast v0, Lcom/google/android/gms/udc/ui/q;

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/n;->c:Lcom/google/android/gms/udc/ui/q;

    .line 92
    check-cast p1, Lcom/google/android/gms/udc/ui/r;

    iput-object p1, p0, Lcom/google/android/gms/udc/ui/n;->d:Lcom/google/android/gms/udc/ui/r;

    .line 93
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 97
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 98
    iput-boolean v0, p0, Lcom/google/android/gms/udc/ui/n;->f:Z

    .line 99
    iput-boolean v0, p0, Lcom/google/android/gms/udc/ui/n;->g:Z

    .line 100
    sget-object v0, Lcom/google/android/gms/udc/util/c;->a:Lcom/android/volley/toolbox/m;

    if-nez v0, :cond_0

    const/high16 v0, 0x300000

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v2

    const-wide/16 v4, 0x8

    div-long/2addr v2, v4

    long-to-int v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    new-instance v1, Lcom/android/volley/toolbox/m;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lcom/android/volley/s;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/udc/util/d;

    invoke-direct {v3, v0}, Lcom/google/android/gms/udc/util/d;-><init>(I)V

    invoke-direct {v1, v2, v3}, Lcom/android/volley/toolbox/m;-><init>(Lcom/android/volley/s;Lcom/android/volley/toolbox/r;)V

    sput-object v1, Lcom/google/android/gms/udc/util/c;->a:Lcom/android/volley/toolbox/m;

    :cond_0
    sget-object v0, Lcom/google/android/gms/udc/util/c;->a:Lcom/android/volley/toolbox/m;

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/n;->b:Lcom/android/volley/toolbox/m;

    .line 101
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 19

    .prologue
    .line 107
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/udc/ui/n;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    .line 108
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/udc/ui/n;->getActivity()Landroid/support/v4/app/q;

    move-result-object v13

    .line 109
    const-string v2, "UdcConsentConfig"

    new-instance v4, Lcom/google/android/gms/udc/e/e;

    invoke-direct {v4}, Lcom/google/android/gms/udc/e/e;-><init>()V

    invoke-static {v3, v2, v4}, Lcom/google/android/gms/udc/util/f;->b(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lcom/google/android/gms/udc/e/e;

    .line 111
    const-string v2, "Fragment requires consent configuration"

    invoke-static {v8, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    const-string v2, "UdcConsentFlowConfig"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lcom/google/android/gms/udc/ConsentFlowConfig;

    .line 114
    const-string v2, "UdcConsentViewType"

    const/4 v4, 0x2

    invoke-virtual {v3, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    .line 117
    const-string v2, "UdcAccountName"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/udc/ui/n;->e:Ljava/lang/String;

    .line 118
    const-string v2, "UdcConsentHasScrolledToEnd"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/gms/udc/ui/n;->h:Z

    .line 120
    sget v2, Lcom/google/android/gms/l;->fC:I

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Landroid/view/ViewGroup;

    .line 122
    if-nez v10, :cond_0

    .line 123
    const-string v2, "UdcConsent"

    const-string v3, "Unable to inflate fragment view"

    invoke-static {v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    const/4 v10, 0x0

    .line 227
    :goto_0
    return-object v10

    .line 126
    :cond_0
    sget v2, Lcom/google/android/gms/j;->tv:I

    invoke-virtual {v10, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/udc/ui/n;->k:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    .line 127
    sget v2, Lcom/google/android/gms/j;->tu:I

    invoke-virtual {v10, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 130
    new-instance v15, Lcom/google/android/gms/udc/util/e;

    move-object/from16 v0, p1

    invoke-direct {v15, v0, v2}, Lcom/google/android/gms/udc/util/e;-><init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 131
    new-instance v2, Lcom/google/android/gms/udc/util/f;

    invoke-direct {v2}, Lcom/google/android/gms/udc/util/f;-><init>()V

    .line 132
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v2, Lcom/google/android/gms/udc/util/f;->a:Ljava/util/ArrayList;

    .line 133
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/udc/ui/n;->a:Lcom/google/android/gms/udc/util/f;

    .line 135
    const/4 v11, 0x0

    .line 137
    iget-object v3, v8, Lcom/google/android/gms/udc/e/e;->d:Lcom/google/android/gms/udc/e/k;

    if-eqz v3, :cond_f

    .line 138
    sget v3, Lcom/google/android/gms/l;->fE:I

    invoke-virtual {v15, v3}, Lcom/google/android/gms/udc/util/e;->a(I)Landroid/view/View;

    move-result-object v3

    .line 139
    sget v4, Lcom/google/android/gms/j;->jZ:I

    iget-object v5, v8, Lcom/google/android/gms/udc/e/e;->d:Lcom/google/android/gms/udc/e/k;

    iget-object v6, v8, Lcom/google/android/gms/udc/e/e;->n:Lcom/google/android/gms/udc/e/k;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/udc/ui/n;->b:Lcom/android/volley/toolbox/m;

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;ILcom/google/android/gms/udc/e/k;Lcom/google/android/gms/udc/e/k;Lcom/android/volley/toolbox/m;)Lcom/android/volley/toolbox/NetworkImageView;

    move-result-object v3

    .line 141
    if-eqz v3, :cond_f

    .line 142
    const/4 v3, 0x1

    .line 147
    :goto_1
    iget-object v4, v8, Lcom/google/android/gms/udc/e/e;->e:Lcom/google/android/gms/udc/e/s;

    if-eqz v4, :cond_1

    .line 148
    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_6

    .line 149
    sget v3, Lcom/google/android/gms/l;->fF:I

    invoke-virtual {v15, v3}, Lcom/google/android/gms/udc/util/e;->a(I)Landroid/view/View;

    move-result-object v3

    .line 158
    :goto_2
    sget v4, Lcom/google/android/gms/j;->jw:I

    iget-object v5, v8, Lcom/google/android/gms/udc/e/e;->e:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;ILcom/google/android/gms/udc/e/s;)Landroid/widget/TextView;

    move-result-object v4

    .line 159
    sget v5, Lcom/google/android/gms/j;->sy:I

    iget-object v6, v8, Lcom/google/android/gms/udc/e/e;->f:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v2, v3, v5, v6}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;ILcom/google/android/gms/udc/e/s;)Landroid/widget/TextView;

    .line 160
    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/gms/udc/ui/n;->j:Ljava/lang/CharSequence;

    .line 164
    :cond_1
    iget-object v3, v8, Lcom/google/android/gms/udc/e/e;->g:Lcom/google/android/gms/udc/e/s;

    if-eqz v3, :cond_2

    .line 165
    sget v3, Lcom/google/android/gms/l;->fD:I

    invoke-virtual {v15, v3}, Lcom/google/android/gms/udc/util/e;->a(I)Landroid/view/View;

    move-result-object v3

    .line 166
    sget v4, Lcom/google/android/gms/j;->jw:I

    iget-object v5, v8, Lcom/google/android/gms/udc/e/e;->g:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;ILcom/google/android/gms/udc/e/s;)Landroid/widget/TextView;

    .line 168
    sget v3, Lcom/google/android/gms/l;->fG:I

    invoke-virtual {v15, v3}, Lcom/google/android/gms/udc/util/e;->a(I)Landroid/view/View;

    .line 172
    :cond_2
    iget-object v3, v8, Lcom/google/android/gms/udc/e/e;->h:[Lcom/google/android/gms/udc/e/q;

    if-eqz v3, :cond_9

    iget-object v3, v8, Lcom/google/android/gms/udc/e/e;->h:[Lcom/google/android/gms/udc/e/q;

    array-length v3, v3

    if-lez v3, :cond_9

    .line 173
    const/4 v4, 0x1

    .line 174
    iget-object v0, v8, Lcom/google/android/gms/udc/e/e;->h:[Lcom/google/android/gms/udc/e/q;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    const/4 v3, 0x0

    move v12, v3

    :goto_3
    move/from16 v0, v17

    if-ge v12, v0, :cond_8

    aget-object v18, v16, v12

    .line 175
    if-eqz v4, :cond_3

    .line 176
    sget v3, Lcom/google/android/gms/l;->fI:I

    invoke-virtual {v15, v3}, Lcom/google/android/gms/udc/util/e;->a(I)Landroid/view/View;

    .line 178
    :cond_3
    sget v3, Lcom/google/android/gms/l;->fH:I

    invoke-virtual {v15, v3}, Lcom/google/android/gms/udc/util/e;->a(I)Landroid/view/View;

    move-result-object v3

    .line 179
    if-eqz v3, :cond_e

    .line 180
    const/4 v11, 0x0

    .line 182
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/google/android/gms/udc/e/q;->c:Lcom/google/android/gms/udc/e/k;

    if-eqz v4, :cond_4

    invoke-virtual {v9}, Lcom/google/android/gms/udc/ConsentFlowConfig;->c()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 183
    sget v4, Lcom/google/android/gms/j;->jV:I

    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/google/android/gms/udc/e/q;->c:Lcom/google/android/gms/udc/e/k;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/udc/ui/n;->b:Lcom/android/volley/toolbox/m;

    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;ILcom/google/android/gms/udc/e/k;Lcom/google/android/gms/udc/e/k;Lcom/android/volley/toolbox/m;)Lcom/android/volley/toolbox/NetworkImageView;

    .line 185
    :cond_4
    sget v4, Lcom/google/android/gms/j;->jw:I

    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/google/android/gms/udc/e/q;->b:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;ILcom/google/android/gms/udc/e/s;)Landroid/widget/TextView;

    .line 186
    sget v4, Lcom/google/android/gms/j;->sy:I

    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/google/android/gms/udc/e/q;->d:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;ILcom/google/android/gms/udc/e/s;)Landroid/widget/TextView;

    .line 188
    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_5

    .line 189
    sget v4, Lcom/google/android/gms/j;->un:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 190
    new-instance v5, Lcom/google/android/gms/udc/ui/s;

    invoke-direct {v5, v3, v4}, Lcom/google/android/gms/udc/ui/s;-><init>(Landroid/view/View;Landroid/widget/CompoundButton;)V

    .line 191
    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 192
    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 193
    invoke-virtual {v9}, Lcom/google/android/gms/udc/ConsentFlowConfig;->b()Z

    move-result v3

    invoke-virtual {v4, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 194
    const/4 v3, 0x0

    invoke-virtual {v9}, Lcom/google/android/gms/udc/ConsentFlowConfig;->b()Z

    move-result v6

    invoke-virtual {v5, v3, v6}, Lcom/google/android/gms/udc/ui/s;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    .line 195
    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    :cond_5
    move v3, v11

    .line 174
    :goto_4
    add-int/lit8 v4, v12, 0x1

    move v12, v4

    move v4, v3

    goto :goto_3

    .line 151
    :cond_6
    if-eqz v3, :cond_7

    .line 152
    sget v3, Lcom/google/android/gms/l;->fK:I

    invoke-virtual {v15, v3}, Lcom/google/android/gms/udc/util/e;->a(I)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_2

    .line 155
    :cond_7
    sget v3, Lcom/google/android/gms/l;->fJ:I

    invoke-virtual {v15, v3}, Lcom/google/android/gms/udc/util/e;->a(I)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_2

    .line 199
    :cond_8
    sget v3, Lcom/google/android/gms/l;->fI:I

    invoke-virtual {v15, v3}, Lcom/google/android/gms/udc/util/e;->a(I)Landroid/view/View;

    .line 200
    sget v3, Lcom/google/android/gms/l;->fG:I

    invoke-virtual {v15, v3}, Lcom/google/android/gms/udc/util/e;->a(I)Landroid/view/View;

    .line 204
    :cond_9
    iget-object v11, v8, Lcom/google/android/gms/udc/e/e;->i:[Lcom/google/android/gms/udc/e/s;

    array-length v12, v11

    const/4 v3, 0x0

    move v9, v3

    :goto_5
    if-ge v9, v12, :cond_a

    aget-object v5, v11, v9

    .line 205
    sget v3, Lcom/google/android/gms/l;->fB:I

    invoke-virtual {v15, v3}, Lcom/google/android/gms/udc/util/e;->a(I)Landroid/view/View;

    move-result-object v3

    .line 206
    sget v4, Lcom/google/android/gms/j;->sy:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/udc/ui/n;->e:Ljava/lang/String;

    move-object v6, v13

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;ILcom/google/android/gms/udc/e/s;Landroid/support/v4/app/q;Ljava/lang/String;)Landroid/widget/TextView;

    .line 204
    add-int/lit8 v3, v9, 0x1

    move v9, v3

    goto :goto_5

    .line 210
    :cond_a
    iget-object v3, v8, Lcom/google/android/gms/udc/e/e;->j:Lcom/google/android/gms/udc/e/s;

    if-eqz v3, :cond_b

    .line 211
    sget v3, Lcom/google/android/gms/l;->fB:I

    invoke-virtual {v15, v3}, Lcom/google/android/gms/udc/util/e;->a(I)Landroid/view/View;

    move-result-object v3

    .line 212
    sget v4, Lcom/google/android/gms/j;->sy:I

    iget-object v5, v8, Lcom/google/android/gms/udc/e/e;->j:Lcom/google/android/gms/udc/e/s;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/udc/ui/n;->e:Ljava/lang/String;

    move-object v6, v13

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;ILcom/google/android/gms/udc/e/s;Landroid/support/v4/app/q;Ljava/lang/String;)Landroid/widget/TextView;

    .line 217
    :cond_b
    sget v2, Lcom/google/android/gms/j;->C:I

    invoke-virtual {v10, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 218
    iget-object v3, v8, Lcom/google/android/gms/udc/e/e;->l:Lcom/google/android/gms/udc/e/c;

    if-eqz v3, :cond_c

    iget-object v3, v8, Lcom/google/android/gms/udc/e/e;->l:Lcom/google/android/gms/udc/e/c;

    sget v4, Lcom/google/android/gms/f;->aC:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/udc/ui/n;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/google/android/gms/udc/e/c;I)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/udc/ui/p;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/gms/udc/ui/p;-><init>(Lcom/google/android/gms/udc/ui/n;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 219
    :cond_c
    sget v3, Lcom/google/android/gms/l;->fz:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 221
    iget-object v3, v8, Lcom/google/android/gms/udc/e/e;->k:Lcom/google/android/gms/udc/e/c;

    if-eqz v3, :cond_d

    iget-object v3, v8, Lcom/google/android/gms/udc/e/e;->k:Lcom/google/android/gms/udc/e/c;

    iget-object v3, v3, Lcom/google/android/gms/udc/e/c;->a:Lcom/google/android/gms/udc/e/s;

    invoke-static {v3}, Lcom/google/android/gms/udc/util/f;->a(Lcom/google/android/gms/udc/e/s;)Landroid/text/Spanned;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/gms/udc/ui/n;->i:Ljava/lang/CharSequence;

    iget-object v3, v8, Lcom/google/android/gms/udc/e/e;->k:Lcom/google/android/gms/udc/e/c;

    sget v4, Lcom/google/android/gms/f;->aD:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/udc/ui/n;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/google/android/gms/udc/e/c;I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/udc/ui/n;->l:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/udc/ui/n;->l:Landroid/view/View;

    sget v3, Lcom/google/android/gms/p;->zc:I

    invoke-static {v2, v3}, Lcom/google/android/gms/udc/ui/n;->a(Landroid/view/View;I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/udc/ui/n;->l:Landroid/view/View;

    new-instance v3, Lcom/google/android/gms/udc/ui/o;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/gms/udc/ui/o;-><init>(Lcom/google/android/gms/udc/ui/n;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 223
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/udc/ui/n;->k:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;->a(Lcom/google/android/gms/common/ui/o;)V

    .line 224
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/udc/ui/n;->k:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;->a(Lcom/google/android/gms/common/ui/n;)V

    .line 225
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/udc/ui/n;->k:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    invoke-virtual {v2}, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto/16 :goto_0

    :cond_e
    move v3, v4

    goto/16 :goto_4

    :cond_f
    move v3, v11

    goto/16 :goto_1
.end method

.method public final onGlobalLayout()V
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/n;->k:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    invoke-virtual {v0}, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;->a()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/udc/ui/n;->b(Z)V

    .line 333
    return-void
.end method

.method public final onResume()V
    .locals 2

    .prologue
    .line 232
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 233
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/n;->k:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/n;->j:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 234
    return-void
.end method
