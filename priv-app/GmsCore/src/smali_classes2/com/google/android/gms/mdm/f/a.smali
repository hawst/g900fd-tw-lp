.class public final Lcom/google/android/gms/mdm/f/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Landroid/content/IntentFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/mdm/f/a;->a:Landroid/content/IntentFilter;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/af/a/c/a/a;
    .locals 9

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x3

    const/4 v1, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 22
    const/4 v0, 0x0

    sget-object v6, Lcom/google/android/gms/mdm/f/a;->a:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v6

    .line 23
    new-instance v7, Lcom/google/af/a/c/a/a;

    invoke-direct {v7}, Lcom/google/af/a/c/a/a;-><init>()V

    .line 24
    const-string v0, "present"

    invoke-virtual {v6, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, v7, Lcom/google/af/a/c/a/a;->a:Z

    .line 26
    const-string v0, "level"

    invoke-virtual {v6, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, v7, Lcom/google/af/a/c/a/a;->b:I

    .line 27
    const-string v0, "scale"

    invoke-virtual {v6, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, v7, Lcom/google/af/a/c/a/a;->c:I

    .line 28
    const-string v0, "plugged"

    const/4 v8, -0x1

    invoke-virtual {v6, v0, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v5

    :goto_0
    iput v0, v7, Lcom/google/af/a/c/a/a;->d:I

    .line 29
    const-string v0, "status"

    invoke-virtual {v6, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    packed-switch v0, :pswitch_data_1

    move v0, v5

    :goto_1
    iput v0, v7, Lcom/google/af/a/c/a/a;->e:I

    .line 30
    const-string v0, "health"

    invoke-virtual {v6, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    packed-switch v0, :pswitch_data_2

    move v2, v5

    :goto_2
    :pswitch_1
    iput v2, v7, Lcom/google/af/a/c/a/a;->f:I

    .line 31
    return-object v7

    :pswitch_2
    move v0, v1

    .line 28
    goto :goto_0

    :pswitch_3
    move v0, v2

    goto :goto_0

    :pswitch_4
    move v0, v3

    goto :goto_0

    :pswitch_5
    move v0, v4

    goto :goto_0

    :pswitch_6
    move v0, v4

    .line 29
    goto :goto_1

    :pswitch_7
    move v0, v1

    goto :goto_1

    :pswitch_8
    move v0, v3

    goto :goto_1

    :pswitch_9
    move v0, v2

    goto :goto_1

    .line 30
    :pswitch_a
    const/4 v2, 0x6

    goto :goto_2

    :pswitch_b
    move v2, v4

    goto :goto_2

    :pswitch_c
    move v2, v1

    goto :goto_2

    :pswitch_d
    move v2, v3

    goto :goto_2

    :pswitch_e
    const/4 v2, 0x5

    goto :goto_2

    .line 28
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 29
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_6
        :pswitch_7
        :pswitch_9
        :pswitch_8
    .end packed-switch

    .line 30
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_b
        :pswitch_c
        :pswitch_1
        :pswitch_d
        :pswitch_e
        :pswitch_a
    .end packed-switch
.end method
