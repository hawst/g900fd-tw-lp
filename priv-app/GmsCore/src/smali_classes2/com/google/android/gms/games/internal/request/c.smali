.class public final Lcom/google/android/gms/games/internal/request/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/HashMap;

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/internal/request/c;->a:Ljava/util/HashMap;

    .line 135
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/internal/request/c;->b:I

    .line 136
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/games/internal/request/b;
    .locals 4

    .prologue
    .line 184
    new-instance v0, Lcom/google/android/gms/games/internal/request/b;

    iget v1, p0, Lcom/google/android/gms/games/internal/request/c;->b:I

    iget-object v2, p0, Lcom/google/android/gms/games/internal/request/c;->a:Ljava/util/HashMap;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/games/internal/request/b;-><init>(ILjava/util/HashMap;B)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;I)Lcom/google/android/gms/games/internal/request/c;
    .locals 2

    .prologue
    .line 148
    packed-switch p2, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/games/internal/request/c;->a:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    :cond_0
    return-object p0

    .line 148
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
