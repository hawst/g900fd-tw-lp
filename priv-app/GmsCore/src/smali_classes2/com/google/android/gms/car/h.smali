.class final Lcom/google/android/gms/car/h;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/g;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/g;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 392
    iput-object p1, p0, Lcom/google/android/gms/car/h;->a:Lcom/google/android/gms/car/g;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    .line 395
    const/16 v0, -0x13

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 396
    iget-object v0, p0, Lcom/google/android/gms/car/h;->a:Lcom/google/android/gms/car/g;

    iget-boolean v0, v0, Lcom/google/android/gms/car/g;->a:Z

    if-eqz v0, :cond_14

    .line 397
    iget-object v7, p0, Lcom/google/android/gms/car/h;->a:Lcom/google/android/gms/car/g;

    const/high16 v0, 0x40000

    const/high16 v1, 0x10000

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-virtual {v7, v5}, Lcom/google/android/gms/car/g;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CAR.AUDIO"

    const-string v1, "Failed to setup fixed volume audio capture"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/media/AudioRecord;

    const/16 v1, 0x8

    const v2, 0xbb80

    const/16 v3, 0xc

    const/4 v4, 0x2

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    iput-object v0, v7, Lcom/google/android/gms/car/g;->d:Landroid/media/AudioRecord;

    :cond_0
    iget-object v0, v7, Lcom/google/android/gms/car/g;->d:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->startRecording()V

    iget-object v0, v7, Lcom/google/android/gms/car/g;->h:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->G()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    const/4 v1, 0x3

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    iget-boolean v3, v7, Lcom/google/android/gms/car/g;->c:Z

    if-nez v3, :cond_11

    const/4 v3, 0x0

    iget-boolean v4, v7, Lcom/google/android/gms/car/g;->f:Z

    if-eqz v4, :cond_1

    iget-object v3, v7, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-virtual {v3}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->k()Lcom/google/android/gms/car/e;

    move-result-object v4

    const/4 v3, 0x1

    :goto_1
    iget-object v5, v4, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v8

    array-length v6, v8

    const/4 v5, 0x0

    :goto_2
    if-lez v6, :cond_3

    sget-object v9, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    iget-object v9, v7, Lcom/google/android/gms/car/g;->d:Landroid/media/AudioRecord;

    invoke-virtual {v9, v8, v5, v6}, Landroid/media/AudioRecord;->read([BII)I

    move-result v9

    sget-object v10, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    iget-boolean v10, v7, Lcom/google/android/gms/car/g;->c:Z

    if-nez v10, :cond_3

    if-gez v9, :cond_2

    const-string v10, "CAR.AUDIO"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "AudioRecord read returned "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v11, " stream "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v11, v7, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v10, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_1
    iget-object v4, v7, Lcom/google/android/gms/car/g;->j:Lcom/google/android/gms/car/nz;

    invoke-virtual {v4}, Lcom/google/android/gms/car/nz;->a()Lcom/google/android/gms/car/e;

    move-result-object v4

    goto :goto_1

    :cond_2
    sub-int/2addr v6, v9

    add-int/2addr v5, v9

    goto :goto_2

    :cond_3
    invoke-static {v8}, Lcom/google/android/gms/car/g;->a([B)Z

    move-result v5

    if-eqz v5, :cond_6

    const/4 v1, 0x0

    :goto_3
    const/4 v5, 0x4

    if-lt v1, v5, :cond_9

    if-eqz v2, :cond_5

    const-string v2, "CAR.AUDIO"

    const/4 v5, 0x3

    invoke-static {v2, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "CAR.AUDIO"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "stopping streaming due to zero data "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, v7, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", frames captured "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    const/4 v2, 0x0

    iget-boolean v5, v7, Lcom/google/android/gms/car/g;->f:Z

    if-eqz v5, :cond_7

    iget-object v5, v7, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-virtual {v5}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->i()V

    const/4 v5, 0x0

    iput-boolean v5, v7, Lcom/google/android/gms/car/g;->f:Z

    const/4 v5, 0x0

    iput-boolean v5, v7, Lcom/google/android/gms/car/g;->e:Z

    :cond_5
    :goto_4
    if-eqz v3, :cond_8

    iget-object v3, v7, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a(Lcom/google/android/gms/car/e;)V

    goto/16 :goto_0

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_7
    iget-object v5, v7, Lcom/google/android/gms/car/g;->i:Lcom/google/android/gms/car/aw;

    iget v6, v7, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v5, v6}, Lcom/google/android/gms/car/aw;->c(I)V

    iget-object v5, v7, Lcom/google/android/gms/car/g;->j:Lcom/google/android/gms/car/nz;

    iget-object v5, v5, Lcom/google/android/gms/car/nz;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    goto :goto_4

    :cond_8
    iget-object v3, v7, Lcom/google/android/gms/car/g;->j:Lcom/google/android/gms/car/nz;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/car/nz;->b(Lcom/google/android/gms/car/e;)V

    goto/16 :goto_0

    :cond_9
    iget-object v3, v4, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v3, v4, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    array-length v5, v8

    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    iput-wide v8, v4, Lcom/google/android/gms/car/e;->a:J

    if-nez v2, :cond_b

    const/4 v0, 0x0

    const-string v2, "CAR.AUDIO"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v2, "CAR.AUDIO"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "starting streaming due to non-zero data "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v7, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    const/4 v2, 0x1

    iget-boolean v3, v7, Lcom/google/android/gms/car/g;->f:Z

    if-eqz v3, :cond_d

    const/4 v3, 0x1

    iput-boolean v3, v7, Lcom/google/android/gms/car/g;->e:Z

    iget-object v3, v7, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a(I)V

    :cond_b
    :goto_5
    add-int/lit8 v0, v0, 0x1

    iget-boolean v3, v7, Lcom/google/android/gms/car/g;->f:Z

    if-eqz v3, :cond_f

    invoke-virtual {v7}, Lcom/google/android/gms/car/g;->f()V

    sget-object v3, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    iget-object v3, v7, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->b(Lcom/google/android/gms/car/e;)V

    sget-object v3, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    :cond_c
    :goto_6
    invoke-virtual {v7}, Lcom/google/android/gms/car/g;->g()V

    goto/16 :goto_0

    :cond_d
    iget-object v3, v7, Lcom/google/android/gms/car/g;->i:Lcom/google/android/gms/car/aw;

    iget v5, v7, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v3, v5}, Lcom/google/android/gms/car/aw;->b(I)Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    move-result-object v3

    if-eqz v3, :cond_e

    iput-object v3, v7, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    iget-object v3, v7, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a(I)V

    const/4 v3, 0x1

    iput-boolean v3, v7, Lcom/google/android/gms/car/g;->f:Z

    const/4 v3, 0x1

    iput-boolean v3, v7, Lcom/google/android/gms/car/g;->e:Z

    :cond_e
    iget-object v3, v7, Lcom/google/android/gms/car/g;->j:Lcom/google/android/gms/car/nz;

    iget-object v3, v3, Lcom/google/android/gms/car/nz;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    goto :goto_5

    :cond_f
    const-string v3, "CAR.AUDIO"

    const/4 v5, 0x2

    invoke-static {v3, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_10

    const-string v3, "CAR.AUDIO"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Non-zero data. Keep until receiving focus "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, v7, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_10
    iget-object v3, v7, Lcom/google/android/gms/car/g;->j:Lcom/google/android/gms/car/nz;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/car/nz;->a(Lcom/google/android/gms/car/e;)V

    iget-object v3, v7, Lcom/google/android/gms/car/g;->j:Lcom/google/android/gms/car/nz;

    iget-object v3, v3, Lcom/google/android/gms/car/nz;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v3

    const/16 v4, 0xc

    if-le v3, v4, :cond_c

    iget-object v3, v7, Lcom/google/android/gms/car/g;->j:Lcom/google/android/gms/car/nz;

    invoke-virtual {v3}, Lcom/google/android/gms/car/nz;->c()V

    goto :goto_6

    :cond_11
    iget-object v0, v7, Lcom/google/android/gms/car/g;->d:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getRecordingState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_12

    iget-object v0, v7, Lcom/google/android/gms/car/g;->d:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->stop()V

    :cond_12
    iget-object v0, v7, Lcom/google/android/gms/car/g;->d:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    iget-boolean v0, v7, Lcom/google/android/gms/car/g;->f:Z

    if-eqz v0, :cond_13

    iget-object v0, v7, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-virtual {v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->i()V

    const/4 v0, 0x0

    iput-boolean v0, v7, Lcom/google/android/gms/car/g;->f:Z

    .line 401
    :cond_13
    :goto_7
    return-void

    .line 399
    :cond_14
    iget-object v0, p0, Lcom/google/android/gms/car/h;->a:Lcom/google/android/gms/car/g;

    invoke-static {v0}, Lcom/google/android/gms/car/g;->a(Lcom/google/android/gms/car/g;)V

    goto :goto_7
.end method
