.class public Lcom/google/android/gms/icing/service/IcingGcmTaskService;
.super Lcom/google/android/gms/gcm/ae;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/android/gms/gcm/ae;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/gcm/be;)I
    .locals 3

    .prologue
    .line 17
    const-string v0, "Running gcm task %s"

    iget-object v1, p1, Lcom/google/android/gms/gcm/be;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 20
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.icing.GCM_TASK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 21
    const-class v1, Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 22
    const-string v1, "tag"

    iget-object v2, p1, Lcom/google/android/gms/gcm/be;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 23
    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/service/IcingGcmTaskService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 25
    const/4 v0, 0x0

    return v0
.end method
