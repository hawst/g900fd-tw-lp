.class public final Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;
.super Lcom/google/android/gms/games/ui/client/a;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/common/api/aq;
.implements Lcom/google/android/gms/games/ui/client/players/p;
.implements Lcom/google/android/gms/games/ui/client/players/w;


# static fields
.field private static final n:I

.field private static final o:I


# instance fields
.field private p:Ljava/util/HashMap;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/view/View;

.field private s:Landroid/widget/ImageView;

.field private t:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field private u:Ljava/util/ArrayList;

.field private v:Z

.field private w:I

.field private x:[B

.field private y:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    sget v0, Lcom/google/android/gms/l;->bF:I

    sput v0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->n:I

    .line 62
    sget v0, Lcom/google/android/gms/m;->m:I

    sput v0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->o:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 88
    const/4 v0, 0x0

    sget v1, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->o:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/client/a;-><init>(II)V

    .line 89
    return-void
.end method


# virtual methods
.method protected final N()I
    .locals 1

    .prologue
    .line 151
    const/16 v0, 0xb

    return v0
.end method

.method public final T()I
    .locals 1

    .prologue
    .line 335
    const/4 v0, 0x1

    return v0
.end method

.method public final U()I
    .locals 1

    .prologue
    .line 340
    const/16 v0, 0x8

    return v0
.end method

.method public final V()Z
    .locals 1

    .prologue
    .line 345
    const/4 v0, 0x0

    return v0
.end method

.method public final W()Z
    .locals 1

    .prologue
    .line 350
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->v:Z

    return v0
.end method

.method public final X()I
    .locals 3

    .prologue
    .line 355
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 356
    sget v1, Lcom/google/android/gms/g;->aD:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 358
    sget v2, Lcom/google/android/gms/g;->aC:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 360
    add-int/2addr v0, v1

    return v0
.end method

.method public final Y()Z
    .locals 1

    .prologue
    .line 365
    const/4 v0, 0x0

    return v0
.end method

.method public final Z()Z
    .locals 1

    .prologue
    .line 371
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 3

    .prologue
    .line 51
    check-cast p1, Lcom/google/android/gms/games/request/f;

    invoke-interface {p1}, Lcom/google/android/gms/games/request/f;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    const-string v1, "com.google.android.gms.games.ui.dialog.progressDialogSendRequest"

    invoke-static {p0, v1}, Lcom/google/android/gms/games/ui/d/a;->dismiss(Landroid/support/v4/app/q;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/games/ui/d/al;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->w:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid request type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->w:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    sget v0, Lcom/google/android/gms/p;->jY:I

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/games/ui/b/d;->a(I)Lcom/google/android/gms/games/ui/b/d;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ui.dialog.networkErrorDialog"

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    :goto_1
    return-void

    :pswitch_1
    sget v0, Lcom/google/android/gms/p;->jX:I

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->finish()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/api/w;)V
    .locals 2

    .prologue
    .line 137
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->a(Lcom/google/android/gms/common/api/w;)V

    .line 138
    new-instance v0, Lcom/google/android/gms/people/ad;

    invoke-direct {v0}, Lcom/google/android/gms/people/ad;-><init>()V

    .line 139
    const/16 v1, 0x76

    iput v1, v0, Lcom/google/android/gms/people/ad;->a:I

    .line 140
    sget-object v1, Lcom/google/android/gms/people/x;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0}, Lcom/google/android/gms/people/ad;->a()Lcom/google/android/gms/people/ac;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    .line 141
    return-void
.end method

.method protected final a(Lcom/google/android/gms/games/i;)V
    .locals 1

    .prologue
    .line 145
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->a(Lcom/google/android/gms/games/i;)V

    .line 146
    const-string v0, "copresence"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/i;->a(Ljava/lang/String;)Lcom/google/android/gms/games/i;

    .line 147
    return-void
.end method

.method public final a(Ljava/util/HashMap;I)V
    .locals 2

    .prologue
    .line 322
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->p:Ljava/util/HashMap;

    .line 324
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->p:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->s:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/gms/h;->bZ:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 326
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->r:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 331
    :goto_0
    return-void

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->s:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/gms/h;->bY:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 329
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->r:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final aa()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->u:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final ab()I
    .locals 1

    .prologue
    .line 381
    const/4 v0, -0x1

    return v0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 228
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->b_(Landroid/os/Bundle;)V

    .line 230
    sget-object v0, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/request/d;->b(Lcom/google/android/gms/common/api/v;)I

    move-result v0

    .line 231
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->x:[B

    array-length v1, v1

    if-le v1, v0, :cond_0

    .line 232
    const-string v1, "SendRequestActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Payload size cannot be greater than "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->finish()V

    .line 235
    :cond_0
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x1

    return v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x2

    return v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    .line 239
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->kD:I

    if-ne v0, v1, :cond_2

    .line 240
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v7

    invoke-static {v7, p0}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SendRequestActivity"

    const-string v1, "onSend: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    :goto_0
    return-void

    .line 240
    :cond_0
    new-instance v8, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->p:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v4, Ljava/util/HashMap;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->p:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/HashMap;-><init>(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->p:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->p:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/v;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/a;->k:Lcom/google/android/gms/games/GameEntity;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->Q()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;I)V

    iget v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->w:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid request type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->w:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    sget v0, Lcom/google/android/gms/p;->kw:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v0}, Lcom/google/android/gms/games/ui/b/f;->a(Ljava/lang/String;)Lcom/google/android/gms/games/ui/b/f;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ui.dialog.progressDialogSendRequest"

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/a;->k:Lcom/google/android/gms/games/GameEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/GameEntity;->a()Ljava/lang/String;

    move-result-object v2

    iget v4, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->w:I

    iget-object v5, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->x:[B

    iget v6, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->y:I

    move-object v1, v7

    move-object v3, v8

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/games/request/d;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/util/List;I[BI)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto/16 :goto_0

    :pswitch_1
    sget v0, Lcom/google/android/gms/p;->kv:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 242
    :cond_2
    const-string v0, "SendRequestActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onClick: unexpected view: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 240
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x0

    .line 93
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->onCreate(Landroid/os/Bundle;)V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->R()V

    .line 100
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->l:Z

    .line 102
    sget v0, Lcom/google/android/gms/j;->kG:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->q:Landroid/widget/TextView;

    .line 103
    sget v0, Lcom/google/android/gms/j;->kD:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->r:Landroid/view/View;

    .line 104
    sget v0, Lcom/google/android/gms/j;->rj:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->s:Landroid/widget/ImageView;

    .line 105
    sget v0, Lcom/google/android/gms/j;->kF:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->t:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    .line 107
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v0, "com.google.android.gms.games.REQUEST_TYPE"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->w:I

    iget v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->w:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "SendRequestActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid request type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->w:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    .line 109
    const-string v0, "SendRequestActivity"

    const-string v1, "Error parsing intent; bailing out..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->finish()V

    .line 114
    :cond_0
    sget v0, Lcom/google/android/gms/j;->oJ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 115
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 116
    invoke-virtual {p0, p0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->a(Landroid/content/Context;)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 117
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 118
    return-void

    .line 107
    :pswitch_0
    sget v0, Lcom/google/android/gms/p;->ln:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->setTitle(I)V

    :goto_1
    const-string v0, "com.google.android.gms.games.PAYLOAD"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->x:[B

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->x:[B

    if-nez v0, :cond_1

    const-string v0, "SendRequestActivity"

    const-string v2, "Payload cannot be null!"

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :pswitch_1
    sget v0, Lcom/google/android/gms/p;->lm:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->setTitle(I)V

    goto :goto_1

    :cond_1
    const-string v0, "com.google.android.gms.games.REQUEST_LIFETIME"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->y:I

    iget v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->y:I

    if-eq v0, v3, :cond_2

    iget v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->y:I

    if-gtz v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const-string v0, "com.google.android.gms.games.REQUEST_ITEM_NAME"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v0, "SendRequestActivity"

    const-string v2, "The item name cannot be empty!"

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->q:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v0, "com.google.android.gms.games.REQUEST_ITEM_ICON"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-nez v0, :cond_4

    const-string v0, "SendRequestActivity"

    const-string v2, "The item icon cannot be null!"

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->t:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const-string v0, "players"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->u:Ljava/util/ArrayList;

    const-string v0, "com.google.android.gms.games.SHOW_NEARBY_SEARCH"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->v:Z

    sget-object v0, Lcom/google/android/gms/games/ui/n;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_5

    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->v:Z

    :cond_5
    const/4 v0, 0x1

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onSearchRequested()Z
    .locals 1

    .prologue
    .line 386
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->S()Z

    move-result v0

    return v0
.end method

.method protected final u()I
    .locals 1

    .prologue
    .line 127
    sget v0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->n:I

    return v0
.end method
