.class public final Lcom/google/android/gms/games/service/a/a/a;
.super Lcom/google/android/gms/games/service/a/c;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/games/internal/dr;

.field private final c:Lcom/google/android/gms/games/a/au;

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:Lcom/google/android/gms/games/internal/el;

.field private final g:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;ILcom/google/android/gms/games/internal/el;)V
    .locals 1

    .prologue
    .line 26
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/c;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 27
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/a/a;->b:Lcom/google/android/gms/games/internal/dr;

    .line 28
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/a/a;->c:Lcom/google/android/gms/games/a/au;

    .line 29
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/a/a;->d:Ljava/lang/String;

    .line 30
    iput p4, p0, Lcom/google/android/gms/games/service/a/a/a;->e:I

    .line 31
    iput-object p5, p0, Lcom/google/android/gms/games/service/a/a/a;->f:Lcom/google/android/gms/games/internal/el;

    .line 32
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/games/service/a/a/a;->g:Z

    .line 33
    return-void

    .line 32
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a(I)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/a/a;->b:Lcom/google/android/gms/games/internal/dr;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/a/a;->b:Lcom/google/android/gms/games/internal/dr;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/a/a;->d:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/games/internal/dr;->b(ILjava/lang/String;)V

    .line 47
    :cond_0
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)I
    .locals 6

    .prologue
    .line 38
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/a/a;->c:Lcom/google/android/gms/games/a/au;

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/a/a;->d:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/games/service/a/a/a;->e:I

    iget-object v4, p0, Lcom/google/android/gms/games/service/a/a/a;->f:Lcom/google/android/gms/games/internal/el;

    iget-boolean v5, p0, Lcom/google/android/gms/games/service/a/a/a;->g:Z

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;ILcom/google/android/gms/games/internal/el;Z)I

    move-result v0

    return v0
.end method
