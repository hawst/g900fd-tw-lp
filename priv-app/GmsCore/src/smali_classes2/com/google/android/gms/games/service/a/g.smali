.class public final Lcom/google/android/gms/games/service/a/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/service/e;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/g;->a:Ljava/lang/String;

    .line 16
    iput-boolean p2, p0, Lcom/google/android/gms/games/service/a/g;->b:Z

    .line 17
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 34
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)V
    .locals 2

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/google/android/gms/games/service/a/g;->b:Z

    if-eqz v0, :cond_0

    .line 24
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/g;->a:Ljava/lang/String;

    invoke-virtual {p2, p1, v0}, Lcom/google/android/gms/games/a/t;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 30
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/g;->a:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/gms/games/service/a/g;->b:Z

    invoke-virtual {p2, p1, v0, v1}, Lcom/google/android/gms/games/a/t;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 31
    return-void

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/g;->a:Ljava/lang/String;

    invoke-virtual {p2, p1, v0}, Lcom/google/android/gms/games/a/t;->d(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method
