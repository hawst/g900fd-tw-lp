.class public final Lcom/google/android/gms/wallet/shared/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/google/android/gms/wallet/shared/d;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;B)V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/shared/d;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/d;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v0, v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/d;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b:Ljava/lang/String;

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/d;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)Lcom/google/android/gms/wallet/shared/d;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/d;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object p1, v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    .line 121
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/d;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/d;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object p1, v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b:Ljava/lang/String;

    .line 116
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/d;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/d;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object p1, v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d:Ljava/lang/String;

    .line 126
    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/d;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/d;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object p1, v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e:Ljava/lang/String;

    .line 131
    return-object p0
.end method

.method public final d(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/d;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/d;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object p1, v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f:Ljava/lang/String;

    .line 136
    return-object p0
.end method
