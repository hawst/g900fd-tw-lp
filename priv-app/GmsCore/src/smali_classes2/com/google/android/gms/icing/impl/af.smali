.class final Lcom/google/android/gms/icing/impl/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/icing/impl/a/z;


# instance fields
.field final synthetic a:Lcom/google/android/gms/icing/impl/u;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/impl/u;)V
    .locals 0

    .prologue
    .line 282
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/af;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/icing/impl/a/aa;)Ljava/util/Set;
    .locals 4

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/af;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->i()Lcom/google/android/gms/icing/impl/a/x;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/icing/impl/a/x;->a(Lcom/google/android/gms/icing/impl/a/aa;I)Ljava/util/List;

    move-result-object v2

    .line 287
    new-instance v3, Ljava/util/HashSet;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 288
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 289
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    iget-object v0, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 288
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 291
    :cond_0
    return-object v3
.end method

.method public final a(Lcom/google/android/gms/icing/impl/a/e;)V
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/af;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/u;->b(Lcom/google/android/gms/icing/impl/a/e;)V

    .line 297
    return-void
.end method

.method public final b(Lcom/google/android/gms/icing/impl/a/e;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/af;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->i()Lcom/google/android/gms/icing/impl/a/x;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/gms/icing/impl/a/x;->c(Lcom/google/android/gms/icing/impl/a/e;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/icing/impl/a/e;)Z
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/af;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->i()Lcom/google/android/gms/icing/impl/a/x;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/gms/icing/impl/a/x;->b(Lcom/google/android/gms/icing/impl/a/e;)Z

    move-result v0

    return v0
.end method
