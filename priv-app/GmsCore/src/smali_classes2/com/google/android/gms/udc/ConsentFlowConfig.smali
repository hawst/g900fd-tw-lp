.class public Lcom/google/android/gms/udc/ConsentFlowConfig;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/udc/a;


# instance fields
.field private final a:I

.field private b:Z

.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/gms/udc/a;

    invoke-direct {v0}, Lcom/google/android/gms/udc/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/udc/ConsentFlowConfig;->CREATOR:Lcom/google/android/gms/udc/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0, v1}, Lcom/google/android/gms/udc/ConsentFlowConfig;-><init>(IZZ)V

    .line 39
    return-void
.end method

.method constructor <init>(IZZ)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput p1, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->a:I

    .line 50
    iput-boolean p2, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->b:Z

    .line 51
    iput-boolean p3, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->c:Z

    .line 52
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->a:I

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->b:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->c:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/google/android/gms/udc/ConsentFlowConfig;->CREATOR:Lcom/google/android/gms/udc/a;

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 104
    if-ne p0, p1, :cond_1

    .line 111
    :cond_0
    :goto_0
    return v0

    .line 107
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/udc/ConsentFlowConfig;

    if-nez v2, :cond_2

    move v0, v1

    .line 108
    goto :goto_0

    .line 110
    :cond_2
    check-cast p1, Lcom/google/android/gms/udc/ConsentFlowConfig;

    .line 111
    iget v2, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->a:I

    iget v3, p1, Lcom/google/android/gms/udc/ConsentFlowConfig;->a:I

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->c:Z

    iget-boolean v3, p1, Lcom/google/android/gms/udc/ConsentFlowConfig;->c:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->b:Z

    iget-boolean v3, p1, Lcom/google/android/gms/udc/ConsentFlowConfig;->b:Z

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 96
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/google/android/gms/udc/ConsentFlowConfig;->CREATOR:Lcom/google/android/gms/udc/a;

    invoke-static {p0, p1}, Lcom/google/android/gms/udc/a;->a(Lcom/google/android/gms/udc/ConsentFlowConfig;Landroid/os/Parcel;)V

    .line 68
    return-void
.end method
