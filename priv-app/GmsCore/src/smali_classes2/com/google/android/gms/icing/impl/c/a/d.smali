.class public final Lcom/google/android/gms/icing/impl/c/a/d;
.super Lcom/google/android/gms/icing/impl/c/a/a;
.source "SourceFile"


# instance fields
.field private final c:Ljava/util/Map;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/c/a/a;-><init>(Ljava/lang/String;)V

    .line 24
    iput-object p2, p0, Lcom/google/android/gms/icing/impl/c/a/d;->c:Ljava/util/Map;

    .line 25
    return-void
.end method

.method private a(Landroid/util/Pair;)I
    .locals 3

    .prologue
    .line 219
    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 220
    const-string v1, "length must specify a value"

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/c;

    iget v0, v0, Lcom/google/android/gms/icing/impl/c/a/c;->a:I

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;I)V

    .line 222
    :cond_0
    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/c;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 223
    const-string v1, "Expected an integer"

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/c;

    iget v0, v0, Lcom/google/android/gms/icing/impl/c/a/c;->a:I

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;I)V

    .line 225
    :cond_1
    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/c;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 226
    if-gtz v1, :cond_2

    .line 227
    const-string v2, "length must be greater than zero"

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/c;

    iget v0, v0, Lcom/google/android/gms/icing/impl/c/a/c;->a:I

    invoke-virtual {p0, v2, v0}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;I)V

    .line 229
    :cond_2
    return v1
.end method

.method private a(Lcom/google/android/gms/icing/impl/c/a/c;)I
    .locals 3

    .prologue
    .line 107
    iget-object v0, p1, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/e;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/c/a/e;->a:Ljava/lang/String;

    .line 108
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/c/a/d;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 109
    const-string v1, "Corpus doesn\'t have section with such name."

    iget v2, p1, Lcom/google/android/gms/icing/impl/c/a/c;->a:I

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;I)V

    .line 111
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/c/a/d;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private a(Ljava/util/List;)V
    .locals 5

    .prologue
    .line 96
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 97
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 98
    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/c;

    .line 99
    iget-object v1, v0, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/icing/impl/c/a/e;

    iget-object v1, v1, Lcom/google/android/gms/icing/impl/c/a/e;->a:Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, v0, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/icing/impl/c/a/e;

    iget-object v1, v1, Lcom/google/android/gms/icing/impl/c/a/e;->a:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " has already been specified"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget v4, v0, Lcom/google/android/gms/icing/impl/c/a/c;->a:I

    invoke-virtual {p0, v1, v4}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;I)V

    .line 102
    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/e;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/c/a/e;->a:Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 104
    :cond_1
    return-void
.end method

.method private static b(Ljava/lang/String;)Lcom/google/android/gms/icing/bb;
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lcom/google/android/gms/icing/bb;

    invoke-direct {v0}, Lcom/google/android/gms/icing/bb;-><init>()V

    .line 43
    iput-object p0, v0, Lcom/google/android/gms/icing/bb;->a:Ljava/lang/String;

    .line 44
    return-object v0
.end method

.method private b(Ljava/util/List;)Lcom/google/android/gms/icing/bc;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 147
    new-instance v2, Lcom/google/android/gms/icing/bc;

    invoke-direct {v2}, Lcom/google/android/gms/icing/bc;-><init>()V

    .line 148
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 149
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/icing/impl/c/a/c;

    iget-object v1, v1, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/icing/impl/c/a/e;

    iget-object v1, v1, Lcom/google/android/gms/icing/impl/c/a/e;->a:Ljava/lang/String;

    .line 150
    const-string v4, "length"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 151
    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Landroid/util/Pair;)I

    move-result v0

    iput v0, v2, Lcom/google/android/gms/icing/bc;->b:I

    goto :goto_0

    .line 152
    :cond_0
    const-string v4, "snippeted"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 153
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/c;

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/c/a/d;->b(Lcom/google/android/gms/icing/impl/c/a/c;)Z

    move-result v0

    iput-boolean v0, v2, Lcom/google/android/gms/icing/bc;->c:Z

    goto :goto_0

    .line 154
    :cond_1
    const-string v4, "default"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 155
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v1, :cond_2

    .line 156
    const-string v4, "default must specify a value"

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/icing/impl/c/a/c;

    iget v1, v1, Lcom/google/android/gms/icing/impl/c/a/c;->a:I

    invoke-virtual {p0, v4, v1}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;I)V

    .line 158
    :cond_2
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/icing/impl/c/a/c;

    iget-object v1, v1, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;

    if-nez v1, :cond_3

    .line 159
    const-string v4, "Expected string literal"

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/icing/impl/c/a/c;

    iget v1, v1, Lcom/google/android/gms/icing/impl/c/a/c;->a:I

    invoke-virtual {p0, v4, v1}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;I)V

    .line 161
    :cond_3
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/c;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/gms/icing/bc;->a:Ljava/lang/String;

    goto :goto_0

    .line 162
    :cond_4
    const-string v4, "except"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 163
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v1, :cond_5

    .line 164
    const-string v4, "except must specify a value"

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/icing/impl/c/a/c;

    iget v1, v1, Lcom/google/android/gms/icing/impl/c/a/c;->a:I

    invoke-virtual {p0, v4, v1}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;I)V

    .line 166
    :cond_5
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/icing/impl/c/a/c;

    iget-object v1, v1, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    instance-of v1, v1, Lcom/google/android/gms/icing/impl/c/a/e;

    if-eqz v1, :cond_6

    .line 167
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/c;

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Lcom/google/android/gms/icing/impl/c/a/c;)I

    move-result v0

    shl-int v0, v7, v0

    iput v0, v2, Lcom/google/android/gms/icing/bc;->d:I

    goto/16 :goto_0

    .line 168
    :cond_6
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/icing/impl/c/a/c;

    iget-object v1, v1, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    instance-of v1, v1, Ljava/util/List;

    if-eqz v1, :cond_9

    .line 169
    const/4 v1, 0x0

    .line 170
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/c;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/c;

    .line 171
    iget-object v5, v0, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    instance-of v5, v5, Lcom/google/android/gms/icing/impl/c/a/e;

    if-nez v5, :cond_7

    .line 172
    const-string v5, "Expected section name"

    iget v6, v0, Lcom/google/android/gms/icing/impl/c/a/c;->a:I

    invoke-virtual {p0, v5, v6}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;I)V

    .line 174
    :cond_7
    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Lcom/google/android/gms/icing/impl/c/a/c;)I

    move-result v0

    shl-int v0, v7, v0

    or-int/2addr v0, v1

    move v1, v0

    .line 175
    goto :goto_1

    .line 176
    :cond_8
    iput v1, v2, Lcom/google/android/gms/icing/bc;->d:I

    goto/16 :goto_0

    .line 178
    :cond_9
    const-string v1, "Expected name or list of values"

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/c;

    iget v0, v0, Lcom/google/android/gms/icing/impl/c/a/c;->a:I

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 181
    :cond_a
    const-string v1, "Unknown key"

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/c;

    iget v0, v0, Lcom/google/android/gms/icing/impl/c/a/c;->a:I

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 184
    :cond_b
    return-object v2
.end method

.method public static b(C)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 115
    const/16 v2, 0x61

    if-lt p0, v2, :cond_0

    const/16 v2, 0x7a

    if-le p0, v2, :cond_1

    :cond_0
    const/16 v2, 0x41

    if-lt p0, v2, :cond_4

    const/16 v2, 0x5a

    if-gt p0, v2, :cond_4

    :cond_1
    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    const/16 v2, 0x5f

    if-ne p0, v2, :cond_3

    :cond_2
    move v0, v1

    :cond_3
    return v0

    :cond_4
    move v2, v0

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/icing/impl/c/a/c;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 233
    if-nez p1, :cond_0

    move v0, v1

    .line 259
    :goto_0
    return v0

    .line 236
    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    .line 237
    instance-of v3, v0, Ljava/lang/Integer;

    if-eqz v3, :cond_3

    .line 238
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 239
    if-nez v0, :cond_1

    move v0, v2

    .line 240
    goto :goto_0

    .line 241
    :cond_1
    if-ne v0, v1, :cond_2

    move v0, v1

    .line 242
    goto :goto_0

    .line 244
    :cond_2
    const-string v0, "Expected a boolean"

    iget v1, p1, Lcom/google/android/gms/icing/impl/c/a/c;->a:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;I)V

    move v0, v2

    .line 245
    goto :goto_0

    .line 247
    :cond_3
    instance-of v3, v0, Lcom/google/android/gms/icing/impl/c/a/e;

    if-eqz v3, :cond_6

    .line 248
    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/e;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/c/a/e;->a:Ljava/lang/String;

    .line 249
    const-string v3, "true"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v0, v1

    .line 250
    goto :goto_0

    .line 251
    :cond_4
    const-string v1, "false"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    .line 252
    goto :goto_0

    .line 254
    :cond_5
    const-string v0, "Expected a boolean"

    iget v1, p1, Lcom/google/android/gms/icing/impl/c/a/c;->a:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;I)V

    move v0, v2

    .line 255
    goto :goto_0

    .line 258
    :cond_6
    const-string v0, "Expected a boolean"

    iget v1, p1, Lcom/google/android/gms/icing/impl/c/a/c;->a:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;I)V

    move v0, v2

    .line 259
    goto :goto_0
.end method

.method private c(Ljava/util/List;)Lcom/google/android/gms/icing/bd;
    .locals 5

    .prologue
    .line 196
    new-instance v2, Lcom/google/android/gms/icing/bd;

    invoke-direct {v2}, Lcom/google/android/gms/icing/bd;-><init>()V

    .line 197
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 198
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/icing/impl/c/a/c;

    iget-object v1, v1, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/icing/impl/c/a/e;

    iget-object v1, v1, Lcom/google/android/gms/icing/impl/c/a/e;->a:Ljava/lang/String;

    .line 199
    const-string v4, "length"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 200
    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Landroid/util/Pair;)I

    move-result v0

    iput v0, v2, Lcom/google/android/gms/icing/bd;->b:I

    goto :goto_0

    .line 201
    :cond_0
    const-string v4, "snippeted"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 202
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/c;

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/c/a/d;->b(Lcom/google/android/gms/icing/impl/c/a/c;)Z

    move-result v0

    iput-boolean v0, v2, Lcom/google/android/gms/icing/bd;->c:Z

    goto :goto_0

    .line 203
    :cond_1
    const-string v4, "default"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 204
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v1, :cond_2

    .line 205
    const-string v4, "default must specify a value"

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/icing/impl/c/a/c;

    iget v1, v1, Lcom/google/android/gms/icing/impl/c/a/c;->a:I

    invoke-virtual {p0, v4, v1}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;I)V

    .line 207
    :cond_2
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/icing/impl/c/a/c;

    iget-object v1, v1, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;

    if-nez v1, :cond_3

    .line 208
    const-string v4, "Expected string literal"

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/icing/impl/c/a/c;

    iget v1, v1, Lcom/google/android/gms/icing/impl/c/a/c;->a:I

    invoke-virtual {p0, v4, v1}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;I)V

    .line 210
    :cond_3
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/c;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/gms/icing/bd;->d:Ljava/lang/String;

    goto :goto_0

    .line 212
    :cond_4
    const-string v1, "Unknown key"

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/c;

    iget v0, v0, Lcom/google/android/gms/icing/impl/c/a/c;->a:I

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 215
    :cond_5
    return-object v2
.end method

.method public static c(C)Z
    .locals 1

    .prologue
    .line 119
    invoke-static {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->b(C)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->a(C)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()Lcom/google/android/gms/icing/impl/c/a/c;
    .locals 4

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    const-string v0, "Expected a name or $"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;)V

    .line 129
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->b()V

    .line 130
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->e()C

    move-result v0

    const/16 v1, 0x24

    if-ne v0, v1, :cond_1

    .line 131
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->d()C

    .line 133
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->e()C

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/c/a/d;->b(C)Z

    move-result v0

    if-nez v0, :cond_3

    .line 134
    :cond_2
    const-string v0, "Expected a name"

    iget v1, p0, Lcom/google/android/gms/icing/impl/c/a/a;->b:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/icing/impl/c/a/a;->a(Ljava/lang/String;I)V

    .line 136
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->d()C

    .line 137
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->a()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->e()C

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/c/a/d;->c(C)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 138
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->d()C

    goto :goto_0

    .line 140
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->c()Lcom/google/android/gms/icing/impl/c/a/c;

    move-result-object v0

    .line 141
    new-instance v1, Lcom/google/android/gms/icing/impl/c/a/c;

    iget v2, v0, Lcom/google/android/gms/icing/impl/c/a/c;->a:I

    new-instance v3, Lcom/google/android/gms/icing/impl/c/a/e;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v3, v0}, Lcom/google/android/gms/icing/impl/c/a/e;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/icing/impl/c/a/c;-><init>(ILjava/lang/Object;)V

    return-object v1
.end method

.method private j()Lcom/google/android/gms/icing/impl/c/a/c;
    .locals 5

    .prologue
    const/16 v4, 0x5c

    const/16 v3, 0x22

    .line 321
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->f()V

    .line 322
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    const-string v0, "Expected a value"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;)V

    .line 325
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->e()C

    move-result v0

    .line 326
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/c/a/d;->a(C)Z

    move-result v1

    if-nez v1, :cond_1

    const/16 v1, 0x2d

    if-ne v0, v1, :cond_2

    .line 327
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->g()Lcom/google/android/gms/icing/impl/c/a/c;

    move-result-object v0

    .line 338
    :goto_0
    return-object v0

    .line 328
    :cond_2
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/c/a/d;->b(C)Z

    move-result v1

    if-nez v1, :cond_3

    const/16 v1, 0x24

    if-ne v0, v1, :cond_4

    .line 329
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->i()Lcom/google/android/gms/icing/impl/c/a/c;

    move-result-object v0

    goto :goto_0

    .line 330
    :cond_4
    if-ne v0, v3, :cond_a

    .line 331
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->d()C

    .line 332
    iget v1, p0, Lcom/google/android/gms/icing/impl/c/a/a;->a:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->b()V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->a()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->e()C

    move-result v0

    if-eq v0, v3, :cond_8

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->e()C

    move-result v0

    if-ne v0, v4, :cond_7

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->c()Lcom/google/android/gms/icing/impl/c/a/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->d()C

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "Expected an escape sequence"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/c/a/a;->a(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->e()C

    move-result v0

    if-eq v0, v3, :cond_6

    if-eq v0, v4, :cond_6

    const-string v0, "Expected \" or \\"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/c/a/a;->a(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->b()V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->d()C

    goto :goto_1

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "Expected string literal end"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/c/a/a;->a(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->c()Lcom/google/android/gms/icing/impl/c/a/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->d()C

    new-instance v0, Lcom/google/android/gms/icing/impl/c/a/c;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/icing/impl/c/a/c;-><init>(ILjava/lang/Object;)V

    goto :goto_0

    .line 333
    :cond_a
    const/16 v1, 0x28

    if-ne v0, v1, :cond_b

    .line 334
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->k()Lcom/google/android/gms/icing/impl/c/a/c;

    move-result-object v0

    goto/16 :goto_0

    .line 336
    :cond_b
    const-string v0, "Expected a value"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;)V

    .line 338
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private k()Lcom/google/android/gms/icing/impl/c/a/c;
    .locals 6

    .prologue
    const/16 v5, 0x2c

    const/16 v4, 0x29

    .line 343
    iget v1, p0, Lcom/google/android/gms/icing/impl/c/a/a;->a:I

    .line 344
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 346
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->a()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->e()C

    move-result v0

    const/16 v3, 0x28

    if-eq v0, v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->e()C

    move-result v0

    if-ne v0, v5, :cond_5

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 347
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->d()C

    .line 348
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->f()V

    .line 349
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->e()C

    move-result v0

    if-eq v0, v4, :cond_2

    .line 350
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->j()Lcom/google/android/gms/icing/impl/c/a/c;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 353
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->f()V

    .line 354
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->e()C

    move-result v0

    if-eq v0, v5, :cond_0

    .line 355
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->a()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->e()C

    move-result v0

    if-eq v0, v4, :cond_4

    .line 356
    :cond_3
    const-string v0, "Expected )"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;)V

    .line 359
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->d()C

    .line 360
    new-instance v0, Lcom/google/android/gms/icing/impl/c/a/c;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/icing/impl/c/a/c;-><init>(ILjava/lang/Object;)V

    return-object v0

    .line 346
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final h()Lcom/google/android/gms/icing/bb;
    .locals 8

    .prologue
    const/16 v7, 0x29

    const/4 v6, 0x0

    const/16 v1, 0x25

    const/16 v5, 0x24

    .line 48
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->b()V

    .line 49
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->e()C

    move-result v0

    .line 50
    if-ne v0, v1, :cond_13

    .line 51
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->d()C

    .line 52
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    const-string v0, "Expected % or a section name"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;)V

    .line 55
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->e()C

    move-result v0

    if-ne v0, v1, :cond_1

    .line 56
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->d()C

    .line 57
    const-string v0, "%"

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/c/a/d;->b(Ljava/lang/String;)Lcom/google/android/gms/icing/bb;

    move-result-object v0

    .line 65
    :goto_0
    return-object v0

    .line 59
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->e()C

    move-result v0

    if-eq v0, v5, :cond_2

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/c/a/d;->b(C)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "Expected a section name or special section name"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;)V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->i()Lcom/google/android/gms/icing/impl/c/a/c;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->a()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->e()C

    move-result v0

    const/16 v1, 0x28

    if-eq v0, v1, :cond_5

    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    :goto_1
    invoke-direct {p0, v1}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/util/List;)V

    new-instance v2, Lcom/google/android/gms/icing/bb;

    invoke-direct {v2}, Lcom/google/android/gms/icing/bb;-><init>()V

    iget-object v0, v3, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/e;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/c/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v5, :cond_12

    const-string v4, "$uri"

    iget-object v0, v3, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/e;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/c/a/e;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const-string v3, "Unknown key"

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/c;

    iget v0, v0, Lcom/google/android/gms/icing/impl/c/a/c;->a:I

    invoke-virtual {p0, v3, v0}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;I)V

    :cond_4
    new-instance v0, Lcom/google/android/gms/icing/be;

    invoke-direct {v0}, Lcom/google/android/gms/icing/be;-><init>()V

    iput-object v0, v2, Lcom/google/android/gms/icing/bb;->d:Lcom/google/android/gms/icing/be;

    :goto_2
    move-object v0, v2

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->d()C

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_6
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->a()Z

    move-result v1

    if-nez v1, :cond_e

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->e()C

    move-result v1

    if-eq v1, v7, :cond_e

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->e()C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->d()C

    goto :goto_3

    :cond_7
    if-eq v1, v5, :cond_8

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/c/a/d;->b(C)Z

    move-result v1

    if-eqz v1, :cond_d

    :cond_8
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->i()Lcom/google/android/gms/icing/impl/c/a/c;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->f()V

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->a()Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "Expected a ) or :"

    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->e()C

    move-result v2

    const/16 v4, 0x3a

    if-ne v2, v4, :cond_b

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->d()C

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->j()Lcom/google/android/gms/icing/impl/c/a/c;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_4
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->f()V

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->a()Z

    move-result v1

    if-eqz v1, :cond_a

    const-string v1, "Expected a , or )"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->e()C

    move-result v1

    const/16 v2, 0x2c

    if-ne v1, v2, :cond_c

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->d()C

    goto :goto_3

    :cond_b
    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_c
    if-eq v1, v7, :cond_6

    const-string v1, "Expected a , or )"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;)V

    goto :goto_3

    :cond_d
    const-string v1, "Expected a name"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;)V

    goto :goto_3

    :cond_e
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->a()Z

    move-result v1

    if-eqz v1, :cond_f

    const-string v1, "Expected a )"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->d()C

    move-object v1, v0

    goto/16 :goto_1

    :cond_10
    const-string v4, "$bestmatch"

    iget-object v0, v3, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/icing/impl/c/a/e;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/c/a/e;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-direct {p0, v1}, Lcom/google/android/gms/icing/impl/c/a/d;->b(Ljava/util/List;)Lcom/google/android/gms/icing/bc;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/icing/bb;->c:Lcom/google/android/gms/icing/bc;

    goto/16 :goto_2

    :cond_11
    const-string v0, "Not a valid special section name"

    iget v1, v3, Lcom/google/android/gms/icing/impl/c/a/c;->a:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Ljava/lang/String;I)V

    goto/16 :goto_2

    :cond_12
    invoke-direct {p0, v3}, Lcom/google/android/gms/icing/impl/c/a/d;->a(Lcom/google/android/gms/icing/impl/c/a/c;)I

    move-result v0

    invoke-direct {p0, v1}, Lcom/google/android/gms/icing/impl/c/a/d;->c(Ljava/util/List;)Lcom/google/android/gms/icing/bd;

    move-result-object v1

    iput v0, v1, Lcom/google/android/gms/icing/bd;->a:I

    iput-object v1, v2, Lcom/google/android/gms/icing/bb;->b:Lcom/google/android/gms/icing/bd;

    goto/16 :goto_2

    .line 62
    :cond_13
    :goto_5
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->a()Z

    move-result v0

    if-nez v0, :cond_14

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->e()C

    move-result v0

    if-eq v0, v1, :cond_14

    .line 63
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->d()C

    goto :goto_5

    .line 65
    :cond_14
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/d;->c()Lcom/google/android/gms/icing/impl/c/a/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/c/a/d;->b(Ljava/lang/String;)Lcom/google/android/gms/icing/bb;

    move-result-object v0

    goto/16 :goto_0
.end method
