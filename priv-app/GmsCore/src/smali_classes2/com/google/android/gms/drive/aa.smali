.class public final enum Lcom/google/android/gms/drive/aa;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/aa;

.field public static final enum b:Lcom/google/android/gms/drive/aa;

.field public static final enum c:Lcom/google/android/gms/drive/aa;

.field public static final enum d:Lcom/google/android/gms/drive/aa;

.field public static final e:Ljava/util/Set;

.field private static final f:Landroid/util/SparseArray;

.field private static final synthetic j:[Lcom/google/android/gms/drive/aa;


# instance fields
.field private final g:Ljava/lang/String;

.field private final h:I

.field private final i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .prologue
    const/4 v11, 0x4

    const/4 v14, 0x3

    const/4 v7, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23
    new-instance v0, Lcom/google/android/gms/drive/aa;

    const-string v1, "FILE"

    const-string v4, "https://www.googleapis.com/auth/drive.file"

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/aa;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v0, Lcom/google/android/gms/drive/aa;->a:Lcom/google/android/gms/drive/aa;

    .line 29
    new-instance v4, Lcom/google/android/gms/drive/aa;

    const-string v5, "APPDATA"

    const-string v8, "https://www.googleapis.com/auth/drive.appdata"

    move v6, v3

    move v9, v2

    invoke-direct/range {v4 .. v9}, Lcom/google/android/gms/drive/aa;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v4, Lcom/google/android/gms/drive/aa;->b:Lcom/google/android/gms/drive/aa;

    .line 34
    new-instance v5, Lcom/google/android/gms/drive/aa;

    const-string v6, "FULL"

    const-string v9, "https://www.googleapis.com/auth/drive"

    move v8, v14

    move v10, v3

    invoke-direct/range {v5 .. v10}, Lcom/google/android/gms/drive/aa;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v5, Lcom/google/android/gms/drive/aa;->c:Lcom/google/android/gms/drive/aa;

    .line 39
    new-instance v8, Lcom/google/android/gms/drive/aa;

    const-string v9, "APPS"

    const-string v12, "https://www.googleapis.com/auth/drive.apps"

    move v10, v14

    move v13, v3

    invoke-direct/range {v8 .. v13}, Lcom/google/android/gms/drive/aa;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    sput-object v8, Lcom/google/android/gms/drive/aa;->d:Lcom/google/android/gms/drive/aa;

    .line 18
    new-array v0, v11, [Lcom/google/android/gms/drive/aa;

    sget-object v1, Lcom/google/android/gms/drive/aa;->a:Lcom/google/android/gms/drive/aa;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/drive/aa;->b:Lcom/google/android/gms/drive/aa;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/gms/drive/aa;->c:Lcom/google/android/gms/drive/aa;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/gms/drive/aa;->d:Lcom/google/android/gms/drive/aa;

    aput-object v1, v0, v14

    sput-object v0, Lcom/google/android/gms/drive/aa;->j:[Lcom/google/android/gms/drive/aa;

    .line 44
    sget-object v0, Lcom/google/android/gms/drive/aa;->c:Lcom/google/android/gms/drive/aa;

    sget-object v1, Lcom/google/android/gms/drive/aa;->b:Lcom/google/android/gms/drive/aa;

    sget-object v3, Lcom/google/android/gms/drive/aa;->d:Lcom/google/android/gms/drive/aa;

    invoke-static {v0, v1, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/aa;->e:Ljava/util/Set;

    .line 46
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/aa;->f:Landroid/util/SparseArray;

    .line 49
    invoke-static {}, Lcom/google/android/gms/drive/aa;->values()[Lcom/google/android/gms/drive/aa;

    move-result-object v0

    array-length v1, v0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 50
    sget-object v4, Lcom/google/android/gms/drive/aa;->f:Landroid/util/SparseArray;

    iget v5, v3, Lcom/google/android/gms/drive/aa;->h:I

    invoke-virtual {v4, v5, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 49
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 52
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;Z)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 93
    iput p3, p0, Lcom/google/android/gms/drive/aa;->h:I

    .line 94
    iput-object p4, p0, Lcom/google/android/gms/drive/aa;->g:Ljava/lang/String;

    .line 95
    iput-boolean p5, p0, Lcom/google/android/gms/drive/aa;->i:Z

    .line 96
    return-void
.end method

.method public static a(I)Lcom/google/android/gms/drive/aa;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/google/android/gms/drive/aa;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/aa;

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;)Ljava/util/Set;
    .locals 3

    .prologue
    .line 81
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 82
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/aa;

    .line 83
    iget-object v0, v0, Lcom/google/android/gms/drive/aa;->g:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 85
    :cond_0
    return-object v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/aa;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/google/android/gms/drive/aa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/aa;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/aa;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/google/android/gms/drive/aa;->j:[Lcom/google/android/gms/drive/aa;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/aa;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/aa;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcom/google/android/gms/drive/aa;->h:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/gms/drive/aa;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/google/android/gms/drive/aa;->i:Z

    return v0
.end method
