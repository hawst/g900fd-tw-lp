.class public Lcom/google/android/gms/reminders/model/TaskEntity;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Lcom/google/android/gms/reminders/model/Task;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field private final b:Lcom/google/android/gms/reminders/model/TaskIdEntity;

.field private final c:Lcom/google/android/gms/reminders/model/TaskListEntity;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/Long;

.field private final f:Ljava/lang/Long;

.field private final g:Ljava/lang/Boolean;

.field private final h:Ljava/lang/Boolean;

.field private final i:Ljava/lang/Boolean;

.field private final j:Ljava/lang/Boolean;

.field private final k:Ljava/lang/Long;

.field private final l:Lcom/google/android/gms/reminders/model/DateTimeEntity;

.field private final m:Lcom/google/android/gms/reminders/model/DateTimeEntity;

.field private final n:Lcom/google/android/gms/reminders/model/LocationEntity;

.field private final o:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/gms/reminders/model/f;

    invoke-direct {v0}, Lcom/google/android/gms/reminders/model/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/reminders/model/TaskEntity;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/reminders/model/TaskIdEntity;Lcom/google/android/gms/reminders/model/TaskListEntity;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/google/android/gms/reminders/model/DateTimeEntity;Lcom/google/android/gms/reminders/model/DateTimeEntity;Lcom/google/android/gms/reminders/model/LocationEntity;Ljava/lang/Long;)V
    .locals 2

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    iput-object p2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->b:Lcom/google/android/gms/reminders/model/TaskIdEntity;

    .line 158
    iput-object p3, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->c:Lcom/google/android/gms/reminders/model/TaskListEntity;

    .line 159
    iput-object p4, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->d:Ljava/lang/String;

    .line 160
    iput-object p5, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->e:Ljava/lang/Long;

    .line 161
    iput-object p6, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->f:Ljava/lang/Long;

    .line 162
    if-nez p7, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->g:Ljava/lang/Boolean;

    .line 163
    if-nez p8, :cond_1

    const/4 v1, 0x0

    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->h:Ljava/lang/Boolean;

    .line 164
    if-nez p9, :cond_2

    const/4 v1, 0x0

    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->i:Ljava/lang/Boolean;

    .line 165
    if-nez p10, :cond_3

    const/4 v1, 0x0

    :goto_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->j:Ljava/lang/Boolean;

    .line 166
    iput-object p11, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->k:Ljava/lang/Long;

    .line 167
    iput-object p12, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->l:Lcom/google/android/gms/reminders/model/DateTimeEntity;

    .line 168
    iput-object p13, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->m:Lcom/google/android/gms/reminders/model/DateTimeEntity;

    .line 169
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->n:Lcom/google/android/gms/reminders/model/LocationEntity;

    .line 170
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->o:Ljava/lang/Long;

    .line 171
    iput p1, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->a:I

    .line 172
    return-void

    .line 162
    :cond_0
    invoke-virtual {p7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0

    .line 163
    :cond_1
    invoke-virtual {p8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_1

    .line 164
    :cond_2
    invoke-virtual {p9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_2

    .line 165
    :cond_3
    invoke-virtual {p10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_3
.end method

.method public constructor <init>(Lcom/google/android/gms/reminders/model/Task;)V
    .locals 15

    .prologue
    .line 82
    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->d()Lcom/google/android/gms/reminders/model/TaskId;

    move-result-object v1

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->e()Lcom/google/android/gms/reminders/model/TaskList;

    move-result-object v2

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->f()Ljava/lang/String;

    move-result-object v3

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->g()Ljava/lang/Long;

    move-result-object v4

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->h()Ljava/lang/Long;

    move-result-object v5

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->i()Ljava/lang/Boolean;

    move-result-object v6

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->j()Ljava/lang/Boolean;

    move-result-object v7

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->k()Ljava/lang/Boolean;

    move-result-object v8

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->l()Ljava/lang/Boolean;

    move-result-object v9

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->m()Ljava/lang/Long;

    move-result-object v10

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->n()Lcom/google/android/gms/reminders/model/DateTime;

    move-result-object v11

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->o()Lcom/google/android/gms/reminders/model/DateTime;

    move-result-object v12

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->p()Lcom/google/android/gms/reminders/model/Location;

    move-result-object v13

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->q()Ljava/lang/Long;

    move-result-object v14

    move-object v0, p0

    invoke-direct/range {v0 .. v14}, Lcom/google/android/gms/reminders/model/TaskEntity;-><init>(Lcom/google/android/gms/reminders/model/TaskId;Lcom/google/android/gms/reminders/model/TaskList;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/google/android/gms/reminders/model/DateTime;Lcom/google/android/gms/reminders/model/DateTime;Lcom/google/android/gms/reminders/model/Location;Ljava/lang/Long;)V

    .line 97
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/reminders/model/TaskId;Lcom/google/android/gms/reminders/model/TaskList;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/google/android/gms/reminders/model/DateTime;Lcom/google/android/gms/reminders/model/DateTime;Lcom/google/android/gms/reminders/model/Location;Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->a:I

    .line 116
    iput-object p3, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->d:Ljava/lang/String;

    .line 117
    iput-object p4, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->e:Ljava/lang/Long;

    .line 118
    iput-object p5, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->f:Ljava/lang/Long;

    .line 119
    if-nez p6, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->g:Ljava/lang/Boolean;

    .line 120
    if-nez p7, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->h:Ljava/lang/Boolean;

    .line 121
    if-nez p8, :cond_2

    const/4 v0, 0x0

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->i:Ljava/lang/Boolean;

    .line 122
    if-nez p9, :cond_3

    const/4 v0, 0x0

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->j:Ljava/lang/Boolean;

    .line 123
    iput-object p10, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->k:Ljava/lang/Long;

    .line 124
    iput-object p14, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->o:Ljava/lang/Long;

    .line 125
    if-nez p1, :cond_4

    const/4 v0, 0x0

    :goto_4
    iput-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->b:Lcom/google/android/gms/reminders/model/TaskIdEntity;

    .line 133
    if-nez p2, :cond_5

    const/4 v0, 0x0

    :goto_5
    iput-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->c:Lcom/google/android/gms/reminders/model/TaskListEntity;

    .line 134
    if-nez p11, :cond_6

    const/4 v0, 0x0

    :goto_6
    iput-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->l:Lcom/google/android/gms/reminders/model/DateTimeEntity;

    .line 135
    if-nez p12, :cond_7

    const/4 v0, 0x0

    :goto_7
    iput-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->m:Lcom/google/android/gms/reminders/model/DateTimeEntity;

    .line 136
    if-nez p13, :cond_8

    const/4 v0, 0x0

    :goto_8
    iput-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->n:Lcom/google/android/gms/reminders/model/LocationEntity;

    .line 138
    return-void

    .line 119
    :cond_0
    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    .line 120
    :cond_1
    invoke-virtual {p7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_1

    .line 121
    :cond_2
    invoke-virtual {p8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_2

    .line 122
    :cond_3
    invoke-virtual {p9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_3

    .line 125
    :cond_4
    new-instance v0, Lcom/google/android/gms/reminders/model/TaskIdEntity;

    invoke-direct {v0, p1}, Lcom/google/android/gms/reminders/model/TaskIdEntity;-><init>(Lcom/google/android/gms/reminders/model/TaskId;)V

    goto :goto_4

    .line 133
    :cond_5
    new-instance v0, Lcom/google/android/gms/reminders/model/TaskListEntity;

    invoke-direct {v0, p2}, Lcom/google/android/gms/reminders/model/TaskListEntity;-><init>(Lcom/google/android/gms/reminders/model/TaskList;)V

    goto :goto_5

    .line 134
    :cond_6
    new-instance v0, Lcom/google/android/gms/reminders/model/DateTimeEntity;

    invoke-direct {v0, p11}, Lcom/google/android/gms/reminders/model/DateTimeEntity;-><init>(Lcom/google/android/gms/reminders/model/DateTime;)V

    goto :goto_6

    .line 135
    :cond_7
    new-instance v0, Lcom/google/android/gms/reminders/model/DateTimeEntity;

    invoke-direct {v0, p12}, Lcom/google/android/gms/reminders/model/DateTimeEntity;-><init>(Lcom/google/android/gms/reminders/model/DateTime;)V

    goto :goto_7

    .line 136
    :cond_8
    new-instance v0, Lcom/google/android/gms/reminders/model/LocationEntity;

    invoke-direct {v0, p13}, Lcom/google/android/gms/reminders/model/LocationEntity;-><init>(Lcom/google/android/gms/reminders/model/Location;)V

    goto :goto_8
.end method

.method public static a(Lcom/google/android/gms/reminders/model/Task;Lcom/google/android/gms/reminders/model/Task;)Z
    .locals 2

    .prologue
    .line 413
    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->d()Lcom/google/android/gms/reminders/model/TaskId;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->d()Lcom/google/android/gms/reminders/model/TaskId;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->e()Lcom/google/android/gms/reminders/model/TaskList;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->e()Lcom/google/android/gms/reminders/model/TaskList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->f()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->g()Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->g()Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->h()Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->h()Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->i()Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->i()Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->j()Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->j()Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->k()Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->k()Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->l()Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->l()Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->m()Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->m()Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->n()Lcom/google/android/gms/reminders/model/DateTime;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->n()Lcom/google/android/gms/reminders/model/DateTime;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->o()Lcom/google/android/gms/reminders/model/DateTime;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->o()Lcom/google/android/gms/reminders/model/DateTime;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->p()Lcom/google/android/gms/reminders/model/Location;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->p()Lcom/google/android/gms/reminders/model/Location;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->q()Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->q()Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 368
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 369
    const-string v1, "title"

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    const-string v1, "created_time_millis"

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->e:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 371
    const-string v1, "archived_time_ms"

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->f:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 372
    const-string v1, "archived"

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->g:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 373
    const-string v1, "deleted"

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->h:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 374
    const-string v1, "pinned"

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->i:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 375
    const-string v1, "snoozed"

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->j:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 376
    const-string v1, "snoozed_time_millis"

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->k:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 377
    const-string v1, "location_snoozed_until_ms"

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->o:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 378
    return-object v0
.end method

.method public final bridge synthetic c()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 22
    return-object p0
.end method

.method public final d()Lcom/google/android/gms/reminders/model/TaskId;
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->b:Lcom/google/android/gms/reminders/model/TaskIdEntity;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 292
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/google/android/gms/reminders/model/TaskList;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->c:Lcom/google/android/gms/reminders/model/TaskListEntity;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 383
    instance-of v0, p1, Lcom/google/android/gms/reminders/model/Task;

    if-nez v0, :cond_0

    .line 384
    const/4 v0, 0x0

    .line 390
    :goto_0
    return v0

    .line 386
    :cond_0
    if-ne p0, p1, :cond_1

    .line 387
    const/4 v0, 0x1

    goto :goto_0

    .line 390
    :cond_1
    check-cast p1, Lcom/google/android/gms/reminders/model/Task;

    invoke-static {p0, p1}, Lcom/google/android/gms/reminders/model/TaskEntity;->a(Lcom/google/android/gms/reminders/model/Task;Lcom/google/android/gms/reminders/model/Task;)Z

    move-result v0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->e:Ljava/lang/Long;

    return-object v0
.end method

.method public final h()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->f:Ljava/lang/Long;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 395
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->b:Lcom/google/android/gms/reminders/model/TaskIdEntity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->c:Lcom/google/android/gms/reminders/model/TaskListEntity;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->e:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->f:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->g:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->h:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->i:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->j:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->k:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->l:Lcom/google/android/gms/reminders/model/DateTimeEntity;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->m:Lcom/google/android/gms/reminders/model/DateTimeEntity;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->n:Lcom/google/android/gms/reminders/model/LocationEntity;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->o:Ljava/lang/Long;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->g:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final j()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->h:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final k()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->i:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final l()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->j:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final m()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->k:Ljava/lang/Long;

    return-object v0
.end method

.method public final n()Lcom/google/android/gms/reminders/model/DateTime;
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->l:Lcom/google/android/gms/reminders/model/DateTimeEntity;

    return-object v0
.end method

.method public final o()Lcom/google/android/gms/reminders/model/DateTime;
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->m:Lcom/google/android/gms/reminders/model/DateTimeEntity;

    return-object v0
.end method

.method public final p()Lcom/google/android/gms/reminders/model/Location;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->n:Lcom/google/android/gms/reminders/model/LocationEntity;

    return-object v0
.end method

.method public final q()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/google/android/gms/reminders/model/TaskEntity;->o:Ljava/lang/Long;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 297
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/reminders/model/f;->a(Lcom/google/android/gms/reminders/model/TaskEntity;Landroid/os/Parcel;I)V

    .line 298
    return-void
.end method

.method public final z_()Z
    .locals 1

    .prologue
    .line 308
    const/4 v0, 0x1

    return v0
.end method
