.class public final Lcom/google/android/gms/udc/ui/c;
.super Lcom/google/android/gms/common/api/ai;
.source "SourceFile"


# instance fields
.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/api/ai;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/v;)V

    .line 28
    iput-object p3, p0, Lcom/google/android/gms/udc/ui/c;->f:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 3

    .prologue
    .line 40
    new-instance v0, Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/c;->f:Ljava/lang/String;

    const-string v2, "com.google"

    invoke-direct {v0, v1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    sget-object v1, Lcom/google/android/gms/lockbox/a;->b:Lcom/google/android/gms/lockbox/d;

    invoke-interface {v1, p1, v0}, Lcom/google/android/gms/lockbox/d;->a(Lcom/google/android/gms/common/api/v;Landroid/accounts/Account;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/ap;
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/gms/udc/ui/d;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/udc/ui/d;-><init>(Lcom/google/android/gms/udc/ui/c;Lcom/google/android/gms/common/api/Status;)V

    return-object v0
.end method

.method protected final k()Lcom/google/android/gms/common/api/l;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getApiMethod must not be called since onStartRequest/createFailedResult are overridden."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
