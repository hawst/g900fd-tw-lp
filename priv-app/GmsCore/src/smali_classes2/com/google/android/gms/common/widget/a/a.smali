.class public abstract Lcom/google/android/gms/common/widget/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/widget/a/e;


# instance fields
.field private a:I

.field private b:Ljava/lang/CharSequence;

.field private c:Lcom/google/android/gms/common/widget/a/f;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/gms/common/widget/a/a;->a:I

    return-void
.end method

.method public static a(Lcom/google/android/gms/common/widget/a/e;Lcom/google/android/gms/common/widget/a/e;)I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v0, 0x0

    .line 19
    if-ne p0, p1, :cond_1

    .line 38
    :cond_0
    :goto_0
    return v0

    .line 23
    :cond_1
    invoke-interface {p0}, Lcom/google/android/gms/common/widget/a/e;->b()I

    move-result v1

    .line 24
    invoke-interface {p1}, Lcom/google/android/gms/common/widget/a/e;->b()I

    move-result v4

    .line 26
    if-ge v1, v4, :cond_2

    move v1, v2

    .line 27
    :goto_1
    if-eqz v1, :cond_4

    move v0, v1

    .line 28
    goto :goto_0

    .line 26
    :cond_2
    if-ne v1, v4, :cond_3

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v3

    goto :goto_1

    .line 31
    :cond_4
    invoke-interface {p0}, Lcom/google/android/gms/common/widget/a/e;->a()Ljava/lang/CharSequence;

    move-result-object v1

    .line 32
    invoke-interface {p1}, Lcom/google/android/gms/common/widget/a/e;->a()Ljava/lang/CharSequence;

    move-result-object v4

    .line 34
    if-eqz v1, :cond_5

    if-nez v4, :cond_7

    .line 35
    :cond_5
    if-eq v1, v4, :cond_0

    if-nez v1, :cond_6

    move v0, v2

    goto :goto_0

    :cond_6
    move v0, v3

    goto :goto_0

    .line 38
    :cond_7
    sget-object v0, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/a;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method protected a(I)V
    .locals 0

    .prologue
    .line 76
    iput p1, p0, Lcom/google/android/gms/common/widget/a/a;->a:I

    .line 77
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/widget/a/f;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/google/android/gms/common/widget/a/a;->c:Lcom/google/android/gms/common/widget/a/f;

    .line 104
    return-void
.end method

.method protected a(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/google/android/gms/common/widget/a/a;->b:Ljava/lang/CharSequence;

    .line 57
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/google/android/gms/common/widget/a/a;->a:I

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 7
    check-cast p1, Lcom/google/android/gms/common/widget/a/e;

    invoke-static {p0, p1}, Lcom/google/android/gms/common/widget/a/a;->a(Lcom/google/android/gms/common/widget/a/e;Lcom/google/android/gms/common/widget/a/e;)I

    move-result v0

    return v0
.end method

.method protected final d()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/a;->c:Lcom/google/android/gms/common/widget/a/f;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/a;->c:Lcom/google/android/gms/common/widget/a/f;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/widget/a/f;->a(Lcom/google/android/gms/common/widget/a/e;)V

    .line 113
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p0}, Lcom/google/android/gms/common/widget/a/e;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p0}, Lcom/google/android/gms/common/widget/a/e;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
