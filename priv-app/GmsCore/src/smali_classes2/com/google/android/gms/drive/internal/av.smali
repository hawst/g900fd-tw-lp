.class public final Lcom/google/android/gms/drive/internal/av;
.super Lcom/google/android/gms/drive/internal/bg;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/q;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/DriveId;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/internal/bg;-><init>(Lcom/google/android/gms/drive/DriveId;)V

    .line 36
    return-void
.end method

.method private a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/am;Lcom/google/android/gms/drive/m;Lcom/google/android/gms/drive/ad;)Lcom/google/android/gms/common/api/am;
    .locals 7

    .prologue
    .line 125
    if-eqz p3, :cond_3

    .line 126
    instance-of v0, p3, Lcom/google/android/gms/drive/internal/an;

    if-nez v0, :cond_0

    .line 127
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only DriveContents obtained from the Drive API are accepted."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130
    :cond_0
    invoke-interface {p3}, Lcom/google/android/gms/drive/m;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 131
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only DriveContents obtained through DriveApi.newDriveContents are accepted for file creation."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134
    :cond_1
    invoke-interface {p3}, Lcom/google/android/gms/drive/m;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 135
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "DriveContents are already closed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_2
    invoke-interface {p3}, Lcom/google/android/gms/drive/m;->e()Lcom/google/android/gms/drive/Contents;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/Contents;->e()I

    move-result v4

    .line 138
    invoke-interface {p3}, Lcom/google/android/gms/drive/m;->f()V

    .line 142
    :goto_0
    if-nez p2, :cond_4

    .line 143
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MetadataChangeSet must be provided."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :cond_3
    const/4 v4, 0x1

    goto :goto_0

    .line 145
    :cond_4
    const-string v0, "application/vnd.google-apps.folder"

    invoke-virtual {p2}, Lcom/google/android/gms/drive/am;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 146
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "May not create folders (mimetype: application/vnd.google-apps.folder) using this method. Use DriveFolder.createFolder() instead."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 150
    :cond_5
    invoke-static {p1, p4}, Lcom/google/android/gms/drive/ad;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/ad;)V

    new-instance v0, Lcom/google/android/gms/drive/internal/aw;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/internal/aw;-><init>(Lcom/google/android/gms/drive/internal/av;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/am;IILcom/google/android/gms/drive/ad;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 231
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid Unique identifier provided."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 234
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/high16 v1, 0x10000

    if-le v0, v1, :cond_1

    .line 235
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unique identifier length exceeds the max length allowed(65536)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 238
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/am;)Lcom/google/android/gms/common/api/am;
    .locals 2

    .prologue
    .line 198
    if-nez p2, :cond_0

    .line 199
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MetadataChangeSet must be provided."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 201
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/drive/am;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/google/android/gms/drive/am;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "application/vnd.google-apps.folder"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 203
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The mimetype must be of type application/vnd.google-apps.folder"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 207
    :cond_1
    new-instance v0, Lcom/google/android/gms/drive/internal/ax;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/drive/internal/ax;-><init>(Lcom/google/android/gms/drive/internal/av;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/am;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/am;Lcom/google/android/gms/drive/m;)Lcom/google/android/gms/common/api/am;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Lcom/google/android/gms/drive/ae;

    invoke-direct {v0}, Lcom/google/android/gms/drive/ae;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ae;->b()Lcom/google/android/gms/drive/ad;

    move-result-object v0

    iget v1, v0, Lcom/google/android/gms/drive/ad;->c:I

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "May not set a conflict strategy for calls to createFile."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/drive/internal/av;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/am;Lcom/google/android/gms/drive/m;Lcom/google/android/gms/drive/ad;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Lcom/google/android/gms/drive/am;)Lcom/google/android/gms/common/api/am;
    .locals 3

    .prologue
    .line 221
    invoke-static {p2}, Lcom/google/android/gms/drive/internal/av;->a(Ljava/lang/String;)V

    .line 222
    if-nez p3, :cond_0

    .line 223
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MetadataChangeSet must be provided."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 225
    :cond_0
    invoke-static {p3}, Lcom/google/android/gms/drive/am;->a(Lcom/google/android/gms/drive/am;)Lcom/google/android/gms/drive/am;

    move-result-object v0

    .line 226
    invoke-virtual {v0}, Lcom/google/android/gms/drive/am;->e()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v1, v2, p2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)V

    .line 227
    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/drive/internal/av;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/am;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Lcom/google/android/gms/drive/am;Lcom/google/android/gms/drive/ad;)Lcom/google/android/gms/common/api/am;
    .locals 3

    .prologue
    .line 92
    if-nez p4, :cond_0

    .line 93
    new-instance v0, Lcom/google/android/gms/drive/ae;

    invoke-direct {v0}, Lcom/google/android/gms/drive/ae;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ae;->b()Lcom/google/android/gms/drive/ad;

    move-result-object p4

    .line 95
    :cond_0
    invoke-static {p2}, Lcom/google/android/gms/drive/internal/av;->a(Ljava/lang/String;)V

    iget v0, p4, Lcom/google/android/gms/drive/ad;->c:I

    invoke-static {v0}, Lcom/google/android/gms/drive/ad;->b(I)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid createStrategy."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MetadataChangeSet must be provided."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p4, Lcom/google/android/gms/drive/ad;->a:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-static {v0}, Lcom/google/android/gms/drive/ad;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid tracking tag"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-static {p3}, Lcom/google/android/gms/drive/am;->a(Lcom/google/android/gms/drive/am;)Lcom/google/android/gms/drive/am;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/am;->e()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v1, v2, p2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)V

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p4}, Lcom/google/android/gms/drive/internal/av;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/am;Lcom/google/android/gms/drive/m;Lcom/google/android/gms/drive/ad;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    return-object v0
.end method
