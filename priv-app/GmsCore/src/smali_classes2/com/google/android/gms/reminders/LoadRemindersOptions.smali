.class public Lcom/google/android/gms/reminders/LoadRemindersOptions;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field private final b:Ljava/util/List;

.field private final c:Ljava/util/List;

.field private final d:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/gms/reminders/a;

    invoke-direct {v0}, Lcom/google/android/gms/reminders/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/reminders/LoadRemindersOptions;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput p1, p0, Lcom/google/android/gms/reminders/LoadRemindersOptions;->a:I

    .line 52
    iput-object p2, p0, Lcom/google/android/gms/reminders/LoadRemindersOptions;->b:Ljava/util/List;

    .line 53
    iput-object p3, p0, Lcom/google/android/gms/reminders/LoadRemindersOptions;->c:Ljava/util/List;

    .line 54
    iput-object p4, p0, Lcom/google/android/gms/reminders/LoadRemindersOptions;->d:Ljava/util/List;

    .line 55
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/reminders/LoadRemindersOptions;->b:Ljava/util/List;

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/reminders/LoadRemindersOptions;->c:Ljava/util/List;

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/reminders/LoadRemindersOptions;->d:Ljava/util/List;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 81
    invoke-static {p0, p1}, Lcom/google/android/gms/reminders/a;->a(Lcom/google/android/gms/reminders/LoadRemindersOptions;Landroid/os/Parcel;)V

    .line 82
    return-void
.end method
