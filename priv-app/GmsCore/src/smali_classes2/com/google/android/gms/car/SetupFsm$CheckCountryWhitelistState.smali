.class public Lcom/google/android/gms/car/SetupFsm$CheckCountryWhitelistState;
.super Lcom/google/android/car/fsm/j;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/car/fsm/l;
    a = {
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_ERROR"
            b = Lcom/google/android/gms/car/SetupFsm$ErrorState;
            c = Lcom/google/android/gms/car/SetupFsm$CheckCountryWhitelistState;
        .end subannotation,
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_CAR_DISCONNECTED"
            b = Lcom/google/android/gms/car/SetupFsm$SetupFailedState;
            c = Lcom/google/android/gms/car/SetupFsm$CheckCountryWhitelistState;
        .end subannotation,
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_COUNTRY_NOT_WHITELISTED"
            b = Lcom/google/android/gms/car/SetupFsm$SetupFailedState;
            c = Lcom/google/android/gms/car/SetupFsm$CheckCountryWhitelistState;
        .end subannotation,
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_COUNTRY_WHITELISTED"
            b = Lcom/google/android/gms/car/SetupFsm$CheckPhonesBlacklistState;
            c = Lcom/google/android/gms/car/SetupFsm$CheckCountryWhitelistState;
        .end subannotation
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 202
    invoke-direct {p0}, Lcom/google/android/car/fsm/j;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v2, 0x3

    .line 205
    iget-object v0, p0, Lcom/google/android/car/fsm/j;->a:Lcom/google/android/car/fsm/FsmController;

    invoke-virtual {v0}, Lcom/google/android/car/fsm/FsmController;->c()Lcom/google/android/car/fsm/g;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/pg;

    .line 206
    invoke-interface {v0}, Lcom/google/android/gms/car/pg;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 207
    const-string v1, "CAR.SETUP.SetupFsm"

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 208
    const-string v1, "CAR.SETUP.SetupFsm"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Country is whitelisted: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/google/android/gms/car/pg;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/google/android/car/fsm/j;->a:Lcom/google/android/car/fsm/FsmController;

    const-string v1, "EVENT_COUNTRY_WHITELISTED"

    invoke-virtual {v0, v1}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/String;)V

    .line 217
    :goto_0
    return-void

    .line 212
    :cond_1
    const-string v1, "CAR.SETUP.SetupFsm"

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 213
    const-string v1, "CAR.SETUP.SetupFsm"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Country is not whitelisted: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/google/android/gms/car/pg;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    :cond_2
    iget-object v0, p0, Lcom/google/android/car/fsm/j;->a:Lcom/google/android/car/fsm/FsmController;

    const-string v1, "EVENT_COUNTRY_NOT_WHITELISTED"

    invoke-virtual {v0, v1}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 221
    const-string v0, "EVENT_ERROR"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "EVENT_CAR_DISCONNECTED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "EVENT_COUNTRY_NOT_WHITELISTED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "EVENT_COUNTRY_WHITELISTED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 226
    :cond_0
    const/4 v0, 0x0

    .line 230
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
