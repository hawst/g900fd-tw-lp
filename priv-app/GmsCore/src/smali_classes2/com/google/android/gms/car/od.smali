.class public final Lcom/google/android/gms/car/od;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final f:Ljava/lang/Object;

.field private static g:[Landroid/view/MotionEvent$PointerCoords;

.field private static h:[Landroid/view/MotionEvent$PointerProperties;

.field private static final i:Ljava/lang/Object;

.field private static j:I

.field private static k:Lcom/google/android/gms/car/od;


# instance fields
.field public a:Lcom/google/android/gms/car/od;

.field public b:I

.field public c:I

.field public d:I

.field public e:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/od;->f:Ljava/lang/Object;

    .line 28
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/od;->i:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    return-void
.end method

.method public static a(JJILjava/util/ArrayList;IIII)Landroid/view/MotionEvent;
    .locals 20
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 99
    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 100
    sget-object v18, Lcom/google/android/gms/car/od;->f:Ljava/lang/Object;

    monitor-enter v18

    .line 101
    :try_start_0
    sget-object v2, Lcom/google/android/gms/car/od;->g:[Landroid/view/MotionEvent$PointerCoords;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/gms/car/od;->g:[Landroid/view/MotionEvent$PointerCoords;

    array-length v2, v2

    if-ge v2, v7, :cond_4

    :cond_0
    sget-object v2, Lcom/google/android/gms/car/od;->g:[Landroid/view/MotionEvent$PointerCoords;

    if-eqz v2, :cond_1

    sget-object v2, Lcom/google/android/gms/car/od;->g:[Landroid/view/MotionEvent$PointerCoords;

    array-length v2, v2

    move v3, v2

    :goto_0
    if-ge v3, v7, :cond_2

    mul-int/lit8 v2, v3, 0x2

    move v3, v2

    goto :goto_0

    :cond_1
    const/16 v2, 0x8

    move v3, v2

    goto :goto_0

    :cond_2
    new-array v4, v3, [Landroid/view/MotionEvent$PointerCoords;

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_3

    new-instance v5, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct {v5}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    aput-object v5, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    sput-object v4, Lcom/google/android/gms/car/od;->g:[Landroid/view/MotionEvent$PointerCoords;

    invoke-static {v3}, Lcom/google/android/gms/car/od;->a(I)[Landroid/view/MotionEvent$PointerProperties;

    move-result-object v2

    sput-object v2, Lcom/google/android/gms/car/od;->h:[Landroid/view/MotionEvent$PointerProperties;

    .line 102
    :cond_4
    sget-object v8, Lcom/google/android/gms/car/od;->h:[Landroid/view/MotionEvent$PointerProperties;

    .line 103
    sget-object v9, Lcom/google/android/gms/car/od;->g:[Landroid/view/MotionEvent$PointerCoords;

    .line 104
    add-int/lit8 v2, v7, -0x1

    move v3, v2

    :goto_2
    if-ltz v3, :cond_5

    .line 105
    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/car/oe;

    .line 106
    aget-object v4, v8, v3

    iget v5, v2, Lcom/google/android/gms/car/oe;->c:I

    iput v5, v4, Landroid/view/MotionEvent$PointerProperties;->id:I

    .line 107
    aget-object v4, v8, v3

    move/from16 v0, p9

    iput v0, v4, Landroid/view/MotionEvent$PointerProperties;->toolType:I

    .line 108
    aget-object v4, v9, v3

    iget v5, v2, Lcom/google/android/gms/car/oe;->a:I

    add-int v5, v5, p6

    int-to-float v5, v5

    iput v5, v4, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 109
    aget-object v4, v9, v3

    iget v2, v2, Lcom/google/android/gms/car/oe;->b:I

    add-int v2, v2, p7

    int-to-float v2, v2

    iput v2, v4, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 110
    aget-object v2, v9, v3

    const v4, 0x3f4ccccd    # 0.8f

    iput v4, v2, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 104
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    goto :goto_2

    .line 113
    :cond_5
    const/4 v10, 0x0

    const/4 v11, 0x0

    const/high16 v12, 0x3f800000    # 1.0f

    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v17, 0x0

    move-wide/from16 v2, p0

    move-wide/from16 v4, p2

    move/from16 v6, p4

    move/from16 v16, p8

    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v2

    .line 127
    monitor-exit v18
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v2

    .line 128
    :catchall_0
    move-exception v2

    monitor-exit v18

    throw v2
.end method

.method public static a(III)Lcom/google/android/gms/car/od;
    .locals 2

    .prologue
    .line 55
    invoke-static {}, Lcom/google/android/gms/car/od;->b()Lcom/google/android/gms/car/od;

    move-result-object v0

    .line 56
    iput p0, v0, Lcom/google/android/gms/car/od;->b:I

    .line 57
    iput p1, v0, Lcom/google/android/gms/car/od;->c:I

    .line 58
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, v0, Lcom/google/android/gms/car/od;->e:Ljava/util/ArrayList;

    .line 59
    return-object v0
.end method

.method private static a(I)[Landroid/view/MotionEvent$PointerProperties;
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 153
    new-array v1, p0, [Landroid/view/MotionEvent$PointerProperties;

    .line 154
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p0, :cond_0

    .line 155
    new-instance v2, Landroid/view/MotionEvent$PointerProperties;

    invoke-direct {v2}, Landroid/view/MotionEvent$PointerProperties;-><init>()V

    aput-object v2, v1, v0

    .line 154
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 157
    :cond_0
    return-object v1
.end method

.method private static b()Lcom/google/android/gms/car/od;
    .locals 3

    .prologue
    .line 42
    sget-object v1, Lcom/google/android/gms/car/od;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 43
    :try_start_0
    sget-object v0, Lcom/google/android/gms/car/od;->k:Lcom/google/android/gms/car/od;

    .line 44
    if-nez v0, :cond_0

    .line 45
    new-instance v0, Lcom/google/android/gms/car/od;

    invoke-direct {v0}, Lcom/google/android/gms/car/od;-><init>()V

    monitor-exit v1

    .line 51
    :goto_0
    return-object v0

    .line 47
    :cond_0
    iget-object v2, v0, Lcom/google/android/gms/car/od;->a:Lcom/google/android/gms/car/od;

    sput-object v2, Lcom/google/android/gms/car/od;->k:Lcom/google/android/gms/car/od;

    .line 48
    sget v2, Lcom/google/android/gms/car/od;->j:I

    add-int/lit8 v2, v2, -0x1

    sput v2, Lcom/google/android/gms/car/od;->j:I

    .line 49
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/car/od;->a:Lcom/google/android/gms/car/od;

    goto :goto_0

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b(III)Lcom/google/android/gms/car/oe;
    .locals 1

    .prologue
    .line 63
    invoke-static {}, Lcom/google/android/gms/car/oe;->a()Lcom/google/android/gms/car/oe;

    move-result-object v0

    .line 64
    iput p0, v0, Lcom/google/android/gms/car/oe;->a:I

    .line 65
    iput p1, v0, Lcom/google/android/gms/car/oe;->b:I

    .line 66
    iput p2, v0, Lcom/google/android/gms/car/oe;->c:I

    .line 67
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 71
    sget-object v2, Lcom/google/android/gms/car/od;->i:Ljava/lang/Object;

    monitor-enter v2

    .line 72
    :try_start_0
    sget v0, Lcom/google/android/gms/car/od;->j:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 73
    sget v0, Lcom/google/android/gms/car/od;->j:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/gms/car/od;->j:I

    .line 74
    sget-object v0, Lcom/google/android/gms/car/od;->k:Lcom/google/android/gms/car/od;

    iput-object v0, p0, Lcom/google/android/gms/car/od;->a:Lcom/google/android/gms/car/od;

    .line 75
    sput-object p0, Lcom/google/android/gms/car/od;->k:Lcom/google/android/gms/car/od;

    .line 77
    :cond_0
    iget v0, p0, Lcom/google/android/gms/car/od;->d:I

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/car/od;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/oe;

    invoke-virtual {v0}, Lcom/google/android/gms/car/oe;->b()V

    .line 77
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 80
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method
