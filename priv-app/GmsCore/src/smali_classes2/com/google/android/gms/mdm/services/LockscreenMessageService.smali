.class public Lcom/google/android/gms/mdm/services/LockscreenMessageService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:Landroid/content/IntentFilter;


# instance fields
.field private final b:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->a:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 41
    new-instance v0, Lcom/google/android/gms/mdm/services/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/mdm/services/c;-><init>(Lcom/google/android/gms/mdm/services/LockscreenMessageService;)V

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->b:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 68
    sget-object v0, Lcom/google/android/gms/mdm/e/a;->e:Lcom/google/android/gms/common/a/r;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/a/r;->a(Ljava/lang/Object;)V

    .line 69
    sget-object v0, Lcom/google/android/gms/mdm/e/a;->f:Lcom/google/android/gms/common/a/r;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/a/r;->a(Ljava/lang/Object;)V

    .line 70
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/mdm/services/LockscreenMessageService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 71
    invoke-virtual {p0, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 72
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 56
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/mdm/services/LockscreenMessageService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 57
    const-string v1, "lock_message"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 58
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 59
    const-string v1, "lock_phone_number"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 62
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->b:Landroid/content/BroadcastReceiver;

    sget-object v1, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->a:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 83
    const-class v0, Lcom/google/android/gms/mdm/receivers/LockscreenUnlockedReceiver;

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Ljava/lang/Class;Z)V

    .line 84
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 123
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.mdm.DISMISS_MESSAGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 124
    invoke-static {p0}, Landroid/support/v4/a/m;->a(Landroid/content/Context;)Landroid/support/v4/a/m;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/a/m;->a(Landroid/content/Intent;)Z

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 127
    const-class v0, Lcom/google/android/gms/mdm/receivers/LockscreenUnlockedReceiver;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Ljava/lang/Class;Z)V

    .line 128
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->stopForeground(Z)V

    .line 129
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6

    .prologue
    const/4 v0, 0x2

    .line 88
    const-string v1, "lock_message"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 89
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 90
    invoke-virtual {p0}, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->stopSelf()V

    .line 116
    :goto_0
    return v0

    .line 93
    :cond_0
    sget-object v2, Lcom/google/android/gms/mdm/e/a;->e:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/a/r;->a(Ljava/lang/Object;)V

    .line 94
    const-string v2, "lock_phone_number"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 95
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 96
    sget-object v3, Lcom/google/android/gms/mdm/e/a;->f:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/common/a/r;->a(Ljava/lang/Object;)V

    .line 99
    :cond_1
    new-instance v3, Landroid/support/v4/app/bk;

    invoke-direct {v3, p0}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/google/android/gms/h;->cd:I

    invoke-virtual {v3, v4}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    move-result-object v3

    sget v4, Lcom/google/android/gms/p;->eY:I

    invoke-virtual {p0, v4}, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v3

    new-instance v4, Landroid/support/v4/app/bj;

    invoke-direct {v4}, Landroid/support/v4/app/bj;-><init>()V

    invoke-virtual {v4, v1}, Landroid/support/v4/app/bj;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/bj;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/bk;->a(Landroid/support/v4/app/bv;)Landroid/support/v4/app/bk;

    move-result-object v3

    invoke-static {p0, v1, v2}, Lcom/google/android/gms/mdm/LockscreenActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v4

    iput-object v4, v3, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    invoke-virtual {p0}, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/gms/f;->O:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    iput v4, v3, Landroid/support/v4/app/bk;->y:I

    iput v0, v3, Landroid/support/v4/app/bk;->j:I

    const-string v0, "msg"

    iput-object v0, v3, Landroid/support/v4/app/bk;->w:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, v3, Landroid/support/v4/app/bk;->z:I

    invoke-virtual {v3}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v0

    .line 112
    sget v3, Lcom/google/android/gms/mdm/f/e;->b:I

    invoke-virtual {p0, v3, v0}, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->startForeground(ILandroid/app/Notification;)V

    .line 113
    const/4 v0, 0x0

    invoke-static {p0, v1, v2, v0}, Lcom/google/android/gms/mdm/LockscreenActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->startActivity(Landroid/content/Intent;)V

    .line 116
    const/4 v0, 0x3

    goto :goto_0
.end method
