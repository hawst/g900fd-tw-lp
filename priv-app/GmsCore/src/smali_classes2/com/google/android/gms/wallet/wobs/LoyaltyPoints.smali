.class public final Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field a:Ljava/lang/String;

.field b:Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;

.field c:Ljava/lang/String;

.field d:Lcom/google/android/gms/wallet/wobs/TimeInterval;

.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/gms/wallet/wobs/f;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/wobs/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->e:I

    .line 73
    return-void
.end method

.method constructor <init>(ILjava/lang/String;Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;Ljava/lang/String;Lcom/google/android/gms/wallet/wobs/TimeInterval;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput p1, p0, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->e:I

    .line 62
    iput-object p2, p0, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->a:Ljava/lang/String;

    .line 63
    iput-object p3, p0, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->b:Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;

    .line 64
    iput-object p4, p0, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->c:Ljava/lang/String;

    .line 65
    iput-object p5, p0, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->d:Lcom/google/android/gms/wallet/wobs/TimeInterval;

    .line 66
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->e:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->b:Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/google/android/gms/wallet/wobs/TimeInterval;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->d:Lcom/google/android/gms/wallet/wobs/TimeInterval;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 39
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/wallet/wobs/f;->a(Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;Landroid/os/Parcel;I)V

    .line 40
    return-void
.end method
