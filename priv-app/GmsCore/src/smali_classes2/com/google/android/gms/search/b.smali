.class public final Lcom/google/android/gms/search/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:[Ljava/lang/String;

.field public static final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 31
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "LOCAL_SEARCH"

    aput-object v2, v0, v1

    const-string v1, "GLOBAL_SEARCH"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, "MAINTENANCE"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "ADMINISTRATION"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/search/b;->a:[Ljava/lang/String;

    .line 33
    sget-object v0, Lcom/google/android/gms/search/b;->a:[Ljava/lang/String;

    array-length v0, v0

    shl-int v0, v3, v0

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/google/android/gms/search/b;->b:I

    return-void
.end method

.method public static a(I)Ljava/util/List;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 60
    sget v0, Lcom/google/android/gms/search/b;->b:I

    xor-int/lit8 v0, v0, -0x1

    and-int/2addr v0, p0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Permission bits out of range: %d"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 63
    :goto_1
    sget-object v3, Lcom/google/android/gms/search/b;->a:[Ljava/lang/String;

    array-length v3, v3

    if-ge v2, v3, :cond_2

    .line 64
    shl-int v3, v1, v2

    and-int/2addr v3, p0

    if-eqz v3, :cond_0

    .line 65
    sget-object v3, Lcom/google/android/gms/search/b;->a:[Ljava/lang/String;

    aget-object v3, v3, v2

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move v0, v2

    .line 60
    goto :goto_0

    .line 68
    :cond_2
    return-object v0
.end method
