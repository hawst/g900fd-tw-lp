.class public final Lcom/google/android/gms/search/global/l;
.super Lcom/google/android/gms/search/global/a/e;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/icing/impl/u;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/gms/search/global/a/e;-><init>()V

    .line 21
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/u;

    iput-object v0, p0, Lcom/google/android/gms/search/global/l;->a:Lcom/google/android/gms/icing/impl/u;

    .line 22
    iput-object p2, p0, Lcom/google/android/gms/search/global/l;->b:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Request;Lcom/google/android/gms/search/global/a/a;)V
    .locals 7

    .prologue
    .line 54
    iget-object v6, p0, Lcom/google/android/gms/search/global/l;->a:Lcom/google/android/gms/icing/impl/u;

    new-instance v0, Lcom/google/android/gms/search/global/o;

    iget-object v2, p0, Lcom/google/android/gms/search/global/l;->a:Lcom/google/android/gms/icing/impl/u;

    iget-object v3, p0, Lcom/google/android/gms/search/global/l;->b:Ljava/lang/String;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/global/o;-><init>(Lcom/google/android/gms/search/global/l;Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Request;Lcom/google/android/gms/search/global/a/a;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    .line 62
    return-void
.end method

.method public final a(Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;Lcom/google/android/gms/search/global/a/a;)V
    .locals 7

    .prologue
    .line 28
    iget-object v6, p0, Lcom/google/android/gms/search/global/l;->a:Lcom/google/android/gms/icing/impl/u;

    new-instance v0, Lcom/google/android/gms/search/global/m;

    iget-object v2, p0, Lcom/google/android/gms/search/global/l;->a:Lcom/google/android/gms/icing/impl/u;

    iget-object v3, p0, Lcom/google/android/gms/search/global/l;->b:Ljava/lang/String;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/global/m;-><init>(Lcom/google/android/gms/search/global/l;Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;Lcom/google/android/gms/search/global/a/a;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    .line 36
    return-void
.end method

.method public final a(Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Request;Lcom/google/android/gms/search/global/a/a;)V
    .locals 7

    .prologue
    .line 67
    iget-object v6, p0, Lcom/google/android/gms/search/global/l;->a:Lcom/google/android/gms/icing/impl/u;

    new-instance v0, Lcom/google/android/gms/search/global/p;

    iget-object v2, p0, Lcom/google/android/gms/search/global/l;->a:Lcom/google/android/gms/icing/impl/u;

    iget-object v3, p0, Lcom/google/android/gms/search/global/l;->b:Ljava/lang/String;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/global/p;-><init>(Lcom/google/android/gms/search/global/l;Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Request;Lcom/google/android/gms/search/global/a/a;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    .line 75
    return-void
.end method

.method public final a(Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;Lcom/google/android/gms/search/global/a/a;)V
    .locals 7

    .prologue
    .line 41
    iget-object v6, p0, Lcom/google/android/gms/search/global/l;->a:Lcom/google/android/gms/icing/impl/u;

    new-instance v0, Lcom/google/android/gms/search/global/n;

    iget-object v2, p0, Lcom/google/android/gms/search/global/l;->a:Lcom/google/android/gms/icing/impl/u;

    iget-object v3, p0, Lcom/google/android/gms/search/global/l;->b:Ljava/lang/String;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/global/n;-><init>(Lcom/google/android/gms/search/global/l;Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/global/SetExperimentIdsCall$Request;Lcom/google/android/gms/search/global/a/a;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    .line 49
    return-void
.end method
