.class public final Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field a:I

.field b:Landroid/accounts/Account;

.field c:Ljava/lang/String;

.field d:I

.field e:Z

.field f:Z

.field g:Z

.field h:Ljava/lang/String;

.field i:Z

.field j:[Lcom/google/android/gms/wallet/CountrySpecification;

.field k:Ljava/util/ArrayList;

.field private final l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/google/android/gms/wallet/j;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->l:I

    .line 120
    return-void
.end method

.method constructor <init>(IILandroid/accounts/Account;Ljava/lang/String;IZZZLjava/lang/String;Z[Lcom/google/android/gms/wallet/CountrySpecification;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput p1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->l:I

    .line 105
    iput p2, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->a:I

    .line 106
    iput-object p3, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->b:Landroid/accounts/Account;

    .line 107
    iput-object p4, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->c:Ljava/lang/String;

    .line 108
    iput p5, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->d:I

    .line 109
    iput-boolean p6, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->e:Z

    .line 110
    iput-boolean p7, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->f:Z

    .line 111
    iput-boolean p8, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->g:Z

    .line 112
    iput-object p9, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h:Ljava/lang/String;

    .line 113
    iput-boolean p10, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->i:Z

    .line 114
    iput-object p11, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->j:[Lcom/google/android/gms/wallet/CountrySpecification;

    .line 115
    iput-object p12, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k:Ljava/util/ArrayList;

    .line 116
    return-void
.end method

.method public static a(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;)Lcom/google/android/gms/wallet/i;
    .locals 4

    .prologue
    .line 39
    new-instance v0, Lcom/google/android/gms/wallet/i;

    new-instance v1, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-direct {v1}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/i;-><init>(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;B)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->b:Landroid/accounts/Account;

    iget-object v2, v0, Lcom/google/android/gms/wallet/i;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-object v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->b:Landroid/accounts/Account;

    iget v1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->a:I

    iget-object v2, v0, Lcom/google/android/gms/wallet/i;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->a:I

    iget-object v1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->c:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/gms/wallet/i;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-object v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->c:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->e:Z

    iget-object v2, v0, Lcom/google/android/gms/wallet/i;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-boolean v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->e:Z

    iget-object v1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/gms/wallet/i;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-object v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->f:Z

    iget-object v2, v0, Lcom/google/android/gms/wallet/i;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-boolean v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->f:Z

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->i:Z

    iget-object v2, v0, Lcom/google/android/gms/wallet/i;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-boolean v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->i:Z

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->g:Z

    iget-object v2, v0, Lcom/google/android/gms/wallet/i;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-boolean v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->g:Z

    iget-object v1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v2, v0, Lcom/google/android/gms/wallet/i;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iget-object v2, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/google/android/gms/wallet/i;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k:Ljava/util/ArrayList;

    :cond_0
    iget-object v2, v0, Lcom/google/android/gms/wallet/i;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iget-object v2, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_1
    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->l:I

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->a:I

    return v0
.end method

.method public final c()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->b:Landroid/accounts/Account;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->e:Z

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 171
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->f:Z

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 178
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->g:Z

    return v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 193
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->i:Z

    return v0
.end method

.method public final j()[Lcom/google/android/gms/wallet/CountrySpecification;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->j:[Lcom/google/android/gms/wallet/CountrySpecification;

    return-object v0
.end method

.method public final k()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 129
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/wallet/j;->a(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;Landroid/os/Parcel;I)V

    .line 130
    return-void
.end method
