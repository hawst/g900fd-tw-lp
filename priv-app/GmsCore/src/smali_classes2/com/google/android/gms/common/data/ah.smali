.class public final Lcom/google/android/gms/common/data/ah;
.super Lcom/google/android/gms/common/data/t;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/data/ac;


# instance fields
.field private d:Lcom/google/android/gms/common/data/a;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/util/ArrayList;

.field private g:Ljava/lang/String;

.field private h:Lcom/google/android/gms/common/data/ag;

.field private i:Ljava/util/Locale;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/a;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/data/t;-><init>(Lcom/google/android/gms/common/data/d;)V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/ah;->f:Ljava/util/ArrayList;

    .line 39
    iput-object p1, p0, Lcom/google/android/gms/common/data/ah;->d:Lcom/google/android/gms/common/data/a;

    .line 40
    iput-object p2, p0, Lcom/google/android/gms/common/data/ah;->e:Ljava/lang/String;

    .line 41
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/gms/common/data/ah;->i:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 96
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 97
    const/4 v0, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    :goto_0
    if-ge v0, v3, :cond_1

    .line 98
    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->isIdentifierIgnorable(C)Z

    move-result v4

    if-nez v4, :cond_0

    .line 99
    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 97
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 102
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/data/ag;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 81
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    iput-object p3, p0, Lcom/google/android/gms/common/data/ah;->g:Ljava/lang/String;

    .line 83
    iput-object p2, p0, Lcom/google/android/gms/common/data/ah;->h:Lcom/google/android/gms/common/data/ag;

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/common/data/ah;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 85
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iput-object v0, p0, Lcom/google/android/gms/common/data/ah;->i:Ljava/util/Locale;

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/common/data/ah;->g:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/data/ah;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/data/ah;->g:Ljava/lang/String;

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/common/data/ah;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/common/data/ah;->d:Lcom/google/android/gms/common/data/a;

    iget-object v2, v0, Lcom/google/android/gms/common/data/a;->a:Lcom/google/android/gms/common/data/DataHolder;

    iget-object v3, p0, Lcom/google/android/gms/common/data/ah;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/common/data/ah;->d:Lcom/google/android/gms/common/data/a;

    instance-of v4, v0, Lcom/google/android/gms/common/data/q;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/common/data/ah;->d:Lcom/google/android/gms/common/data/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/a;->c()I

    move-result v5

    :goto_0
    if-ge v1, v5, :cond_3

    if-eqz v4, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/data/ah;->d:Lcom/google/android/gms/common/data/a;

    check-cast v0, Lcom/google/android/gms/common/data/q;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/data/q;->b(I)I

    move-result v0

    :goto_1
    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v6

    invoke-virtual {v2, v3, v0, v6}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/google/android/gms/common/data/ah;->h:Lcom/google/android/gms/common/data/ag;

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/data/ah;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v7, p0, Lcom/google/android/gms/common/data/ah;->g:Ljava/lang/String;

    invoke-interface {v6, v0, v7}, Lcom/google/android/gms/common/data/ag;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/data/ah;->f:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 89
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/data/ah;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 91
    :cond_3
    return-void
.end method

.method public final b(I)I
    .locals 3

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/common/data/ah;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    :goto_0
    return p1

    .line 53
    :cond_0
    if-ltz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/data/ah;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 54
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is out of bounds for this buffer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/data/ah;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/common/data/ah;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/data/ah;->a_:Lcom/google/android/gms/common/data/d;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/data/ah;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method
