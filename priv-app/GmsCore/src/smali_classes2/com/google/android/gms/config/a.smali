.class public final Lcom/google/android/gms/config/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Landroid/content/Context;

.field private static b:Lcom/google/android/gms/config/a/a;

.field private static c:Ljava/lang/String;

.field private static d:I

.field private static e:Landroid/content/Intent;

.field private static final f:Ljava/nio/charset/Charset;

.field private static g:Landroid/content/ServiceConnection;

.field private static h:Landroid/content/BroadcastReceiver;

.field private static final i:Ljava/util/regex/Pattern;

.field private static final j:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 36
    sput-object v1, Lcom/google/android/gms/config/a;->a:Landroid/content/Context;

    .line 37
    sput-object v1, Lcom/google/android/gms/config/a;->b:Lcom/google/android/gms/config/a/a;

    .line 38
    sput-object v1, Lcom/google/android/gms/config/a;->c:Ljava/lang/String;

    .line 39
    const/4 v0, 0x0

    sput v0, Lcom/google/android/gms/config/a;->d:I

    .line 41
    sput-object v1, Lcom/google/android/gms/config/a;->e:Landroid/content/Intent;

    .line 46
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/config/a;->f:Ljava/nio/charset/Charset;

    .line 48
    new-instance v0, Lcom/google/android/gms/config/b;

    invoke-direct {v0}, Lcom/google/android/gms/config/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/config/a;->g:Landroid/content/ServiceConnection;

    .line 67
    new-instance v0, Lcom/google/android/gms/config/c;

    invoke-direct {v0}, Lcom/google/android/gms/config/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/config/a;->h:Landroid/content/BroadcastReceiver;

    .line 514
    const-string v0, "^(1|true|t|yes|y|on)$"

    invoke-static {v0, v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/config/a;->i:Ljava/util/regex/Pattern;

    .line 516
    const-string v0, "^(0|false|f|no|n|off|)$"

    invoke-static {v0, v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/config/a;->j:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)J
    .locals 3

    .prologue
    const-wide/32 v0, 0xa8c0

    .line 485
    :try_start_0
    sget-object v2, Lcom/google/android/gms/config/a;->c:Ljava/lang/String;

    invoke-static {v2, p0}, Lcom/google/android/gms/config/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 489
    :goto_0
    return-wide v0

    .line 485
    :cond_0
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    .line 489
    :catch_0
    move-exception v2

    goto :goto_0

    .line 487
    :catch_1
    move-exception v2

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/config/a/a;)Lcom/google/android/gms/config/a/a;
    .locals 0

    .prologue
    .line 30
    sput-object p0, Lcom/google/android/gms/config/a;->b:Lcom/google/android/gms/config/a/a;

    return-object p0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 363
    sget-object v1, Lcom/google/android/gms/config/a;->g:Landroid/content/ServiceConnection;

    monitor-enter v1

    .line 364
    :try_start_0
    sget-object v0, Lcom/google/android/gms/config/a;->b:Lcom/google/android/gms/config/a/a;

    if-nez v0, :cond_1

    const/16 v0, 0xf

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/RemoteException;

    const-string v2, "no connection to config service"

    invoke-direct {v0, v2}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 369
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 364
    :cond_0
    :try_start_1
    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0

    .line 365
    :cond_1
    sget-object v0, Lcom/google/android/gms/config/a;->b:Lcom/google/android/gms/config/a/a;

    invoke-interface {v0, p0, p1}, Lcom/google/android/gms/config/a/a;->a(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v2

    .line 367
    if-nez v2, :cond_2

    const/4 v0, 0x0

    monitor-exit v1

    .line 368
    :goto_0
    return-object v0

    :cond_2
    new-instance v0, Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/config/a;->f:Ljava/nio/charset/Charset;

    invoke-direct {v0, v2, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 102
    const-class v1, Lcom/google/android/gms/config/a;

    monitor-enter v1

    .line 103
    :try_start_0
    sget v0, Lcom/google/android/gms/config/a;->d:I

    add-int/lit8 v0, v0, 0x1

    .line 104
    sput v0, Lcom/google/android/gms/config/a;->d:I

    if-ne v0, v2, :cond_1

    .line 108
    sput-object p0, Lcom/google/android/gms/config/a;->a:Landroid/content/Context;

    .line 109
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/config/a;->c:Ljava/lang/String;

    .line 111
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.config.START"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 112
    sput-object v0, Lcom/google/android/gms/config/a;->e:Landroid/content/Intent;

    const-string v2, "com.google.android.gms"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 115
    const-string v2, "package"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 116
    sget-object v2, Lcom/google/android/gms/config/a;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 118
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/config/a;->e:Landroid/content/Intent;

    sget-object v3, Lcom/google/android/gms/config/a;->g:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v0, p0, v2, v3, v4}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 135
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 128
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/config/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 132
    const-string v0, "ConfigClient"

    const-string v2, "connecting to Config Service with different package names"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a()Z
    .locals 10

    .prologue
    const-wide/16 v4, 0xbb8

    const/4 v0, 0x1

    .line 175
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 178
    sget-object v1, Lcom/google/android/gms/config/a;->g:Landroid/content/ServiceConnection;

    monitor-enter v1

    move-wide v2, v4

    .line 179
    :cond_0
    :try_start_0
    sget-object v8, Lcom/google/android/gms/config/a;->b:Lcom/google/android/gms/config/a/a;

    if-nez v8, :cond_2

    .line 180
    sget-object v8, Lcom/google/android/gms/config/a;->g:Landroid/content/ServiceConnection;

    invoke-virtual {v8, v2, v3}, Ljava/lang/Object;->wait(J)V

    .line 181
    sget-object v2, Lcom/google/android/gms/config/a;->b:Lcom/google/android/gms/config/a/a;

    if-eqz v2, :cond_1

    monitor-exit v1

    .line 187
    :goto_0
    return v0

    .line 182
    :cond_1
    add-long v2, v6, v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v2, v8

    .line 184
    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-gtz v8, :cond_0

    const/4 v0, 0x0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 186
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    monitor-exit v1

    goto :goto_0
.end method

.method public static b()V
    .locals 3

    .prologue
    .line 200
    const-class v1, Lcom/google/android/gms/config/a;

    monitor-enter v1

    .line 201
    :try_start_0
    sget v0, Lcom/google/android/gms/config/a;->d:I

    if-nez v0, :cond_0

    .line 202
    const-string v0, "ConfigClient"

    const-string v2, "attempt to disconnect() when not connected"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    monitor-exit v1

    .line 211
    :goto_0
    return-void

    .line 205
    :cond_0
    sget v0, Lcom/google/android/gms/config/a;->d:I

    add-int/lit8 v0, v0, -0x1

    .line 206
    sput v0, Lcom/google/android/gms/config/a;->d:I

    if-lez v0, :cond_1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 207
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    monitor-exit v1

    .line 208
    sget-object v0, Lcom/google/android/gms/config/a;->a:Landroid/content/Context;

    sget-object v1, Lcom/google/android/gms/config/a;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 209
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/config/a;->a:Landroid/content/Context;

    sget-object v2, Lcom/google/android/gms/config/a;->g:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    .line 210
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/config/a;->b:Lcom/google/android/gms/config/a/a;

    goto :goto_0
.end method

.method static synthetic c()Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/gms/config/a;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d()Landroid/content/ServiceConnection;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/gms/config/a;->g:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method static synthetic e()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/gms/config/a;->e:Landroid/content/Intent;

    return-object v0
.end method
