.class public final Lcom/google/android/gms/icing/r;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:[Lcom/google/android/gms/icing/s;

.field public c:I

.field public d:I

.field public e:J

.field public f:J

.field public g:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 12515
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 12516
    iput-wide v2, p0, Lcom/google/android/gms/icing/r;->a:J

    invoke-static {}, Lcom/google/android/gms/icing/s;->a()[Lcom/google/android/gms/icing/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    iput v1, p0, Lcom/google/android/gms/icing/r;->c:I

    iput v1, p0, Lcom/google/android/gms/icing/r;->d:I

    iput-wide v2, p0, Lcom/google/android/gms/icing/r;->e:J

    iput-wide v2, p0, Lcom/google/android/gms/icing/r;->f:J

    iput-wide v2, p0, Lcom/google/android/gms/icing/r;->g:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/r;->cachedSize:I

    .line 12517
    return-void
.end method

.method public static a(Lcom/google/protobuf/nano/a;)Lcom/google/android/gms/icing/r;
    .locals 1

    .prologue
    .line 12725
    new-instance v0, Lcom/google/android/gms/icing/r;

    invoke-direct {v0}, Lcom/google/android/gms/icing/r;-><init>()V

    invoke-direct {v0, p0}, Lcom/google/android/gms/icing/r;->b(Lcom/google/protobuf/nano/a;)Lcom/google/android/gms/icing/r;

    move-result-object v0

    return-object v0
.end method

.method public static a([B)Lcom/google/android/gms/icing/r;
    .locals 1

    .prologue
    .line 12719
    new-instance v0, Lcom/google/android/gms/icing/r;

    invoke-direct {v0}, Lcom/google/android/gms/icing/r;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/r;

    return-object v0
.end method

.method private b(Lcom/google/protobuf/nano/a;)Lcom/google/android/gms/icing/r;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 12659
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    .line 12660
    sparse-switch v0, :sswitch_data_0

    .line 12664
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 12665
    :sswitch_0
    return-object p0

    .line 12670
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/icing/r;->a:J

    goto :goto_0

    .line 12674
    :sswitch_2
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    .line 12676
    iget-object v0, p0, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    if-nez v0, :cond_2

    move v0, v1

    .line 12677
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/s;

    .line 12679
    if-eqz v0, :cond_1

    .line 12680
    iget-object v3, p0, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 12682
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 12683
    new-instance v3, Lcom/google/android/gms/icing/s;

    invoke-direct {v3}, Lcom/google/android/gms/icing/s;-><init>()V

    aput-object v3, v2, v0

    .line 12684
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    .line 12685
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    .line 12682
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 12676
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    array-length v0, v0

    goto :goto_1

    .line 12688
    :cond_3
    new-instance v3, Lcom/google/android/gms/icing/s;

    invoke-direct {v3}, Lcom/google/android/gms/icing/s;-><init>()V

    aput-object v3, v2, v0

    .line 12689
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    .line 12690
    iput-object v2, p0, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    goto :goto_0

    .line 12694
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/r;->c:I

    goto :goto_0

    .line 12698
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/r;->d:I

    goto :goto_0

    .line 12702
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/icing/r;->e:J

    goto :goto_0

    .line 12706
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/icing/r;->f:J

    goto :goto_0

    .line 12710
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/icing/r;->g:J

    goto :goto_0

    .line 12660
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x1a -> :sswitch_2
        0x25 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x38 -> :sswitch_6
        0x40 -> :sswitch_7
    .end sparse-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 12617
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 12618
    iget-wide v2, p0, Lcom/google/android/gms/icing/r;->a:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 12619
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/icing/r;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 12622
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 12623
    const/4 v1, 0x0

    move v6, v1

    move v1, v0

    move v0, v6

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 12624
    iget-object v2, p0, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    aget-object v2, v2, v0

    .line 12625
    if-eqz v2, :cond_1

    .line 12626
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 12623
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 12631
    :cond_3
    iget v1, p0, Lcom/google/android/gms/icing/r;->c:I

    if-eqz v1, :cond_4

    .line 12632
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/icing/r;->c:I

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 12635
    :cond_4
    iget v1, p0, Lcom/google/android/gms/icing/r;->d:I

    if-eqz v1, :cond_5

    .line 12636
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/icing/r;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12639
    :cond_5
    iget-wide v2, p0, Lcom/google/android/gms/icing/r;->e:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_6

    .line 12640
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/google/android/gms/icing/r;->e:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 12643
    :cond_6
    iget-wide v2, p0, Lcom/google/android/gms/icing/r;->f:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_7

    .line 12644
    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/google/android/gms/icing/r;->f:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 12647
    :cond_7
    iget-wide v2, p0, Lcom/google/android/gms/icing/r;->g:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_8

    .line 12648
    const/16 v1, 0x8

    iget-wide v2, p0, Lcom/google/android/gms/icing/r;->g:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 12651
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 12533
    if-ne p1, p0, :cond_1

    .line 12562
    :cond_0
    :goto_0
    return v0

    .line 12536
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/r;

    if-nez v2, :cond_2

    move v0, v1

    .line 12537
    goto :goto_0

    .line 12539
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/r;

    .line 12540
    iget-wide v2, p0, Lcom/google/android/gms/icing/r;->a:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/r;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 12541
    goto :goto_0

    .line 12543
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    iget-object v3, p1, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 12545
    goto :goto_0

    .line 12547
    :cond_4
    iget v2, p0, Lcom/google/android/gms/icing/r;->c:I

    iget v3, p1, Lcom/google/android/gms/icing/r;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 12548
    goto :goto_0

    .line 12550
    :cond_5
    iget v2, p0, Lcom/google/android/gms/icing/r;->d:I

    iget v3, p1, Lcom/google/android/gms/icing/r;->d:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 12551
    goto :goto_0

    .line 12553
    :cond_6
    iget-wide v2, p0, Lcom/google/android/gms/icing/r;->e:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/r;->e:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    move v0, v1

    .line 12554
    goto :goto_0

    .line 12556
    :cond_7
    iget-wide v2, p0, Lcom/google/android/gms/icing/r;->f:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/r;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    move v0, v1

    .line 12557
    goto :goto_0

    .line 12559
    :cond_8
    iget-wide v2, p0, Lcom/google/android/gms/icing/r;->g:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/r;->g:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 12560
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 12567
    iget-wide v0, p0, Lcom/google/android/gms/icing/r;->a:J

    iget-wide v2, p0, Lcom/google/android/gms/icing/r;->a:J

    ushr-long/2addr v2, v6

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    .line 12570
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12572
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/r;->c:I

    add-int/2addr v0, v1

    .line 12573
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/r;->d:I

    add-int/2addr v0, v1

    .line 12574
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/r;->e:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/r;->e:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 12576
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/r;->f:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/r;->f:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 12578
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/r;->g:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/r;->g:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 12580
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 12282
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/r;->b(Lcom/google/protobuf/nano/a;)Lcom/google/android/gms/icing/r;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 12586
    iget-wide v0, p0, Lcom/google/android/gms/icing/r;->a:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 12587
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/icing/r;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 12589
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 12590
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 12591
    iget-object v1, p0, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    aget-object v1, v1, v0

    .line 12592
    if-eqz v1, :cond_1

    .line 12593
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 12590
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 12597
    :cond_2
    iget v0, p0, Lcom/google/android/gms/icing/r;->c:I

    if-eqz v0, :cond_3

    .line 12598
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/icing/r;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->b(II)V

    .line 12600
    :cond_3
    iget v0, p0, Lcom/google/android/gms/icing/r;->d:I

    if-eqz v0, :cond_4

    .line 12601
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/gms/icing/r;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 12603
    :cond_4
    iget-wide v0, p0, Lcom/google/android/gms/icing/r;->e:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_5

    .line 12604
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/android/gms/icing/r;->e:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 12606
    :cond_5
    iget-wide v0, p0, Lcom/google/android/gms/icing/r;->f:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_6

    .line 12607
    const/4 v0, 0x7

    iget-wide v2, p0, Lcom/google/android/gms/icing/r;->f:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 12609
    :cond_6
    iget-wide v0, p0, Lcom/google/android/gms/icing/r;->g:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_7

    .line 12610
    const/16 v0, 0x8

    iget-wide v2, p0, Lcom/google/android/gms/icing/r;->g:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 12612
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 12613
    return-void
.end method
