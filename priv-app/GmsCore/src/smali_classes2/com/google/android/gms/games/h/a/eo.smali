.class public final Lcom/google/android/gms/games/h/a/eo;
.super Lcom/google/android/gms/common/server/response/c;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/HashMap;


# instance fields
.field private final c:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 47
    sput-object v0, Lcom/google/android/gms/games/h/a/eo;->b:Ljava/util/HashMap;

    const-string v1, "capabilities"

    const-string v2, "capabilities"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/google/android/gms/games/h/a/eo;->b:Ljava/util/HashMap;

    const-string v1, "clientAddress"

    const-string v2, "clientAddress"

    const-class v3, Lcom/google/android/gms/games/h/a/em;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/google/android/gms/games/h/a/eo;->b:Ljava/util/HashMap;

    const-string v1, "networkDiagnostics"

    const-string v2, "networkDiagnostics"

    const-class v3, Lcom/google/android/gms/games/h/a/cm;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 79
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/h/a/eo;->c:Ljava/util/HashMap;

    .line 61
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;Lcom/google/android/gms/games/h/a/em;Lcom/google/android/gms/games/h/a/cm;)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 79
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/h/a/eo;->c:Ljava/util/HashMap;

    .line 68
    if-eqz p1, :cond_0

    .line 69
    const-string v0, "capabilities"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/h/a/eo;->i(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 71
    :cond_0
    if-eqz p2, :cond_1

    .line 72
    const-string v0, "clientAddress"

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/games/h/a/eo;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    .line 74
    :cond_1
    if-eqz p3, :cond_2

    .line 75
    const-string v0, "networkDiagnostics"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/games/h/a/eo;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    .line 77
    :cond_2
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/gms/games/h/a/eo;->b:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/eo;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    return-void
.end method

.method protected final d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/eo;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final getClientAddress()Lcom/google/android/gms/games/h/a/em;
    .locals 2
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/eo;->c:Ljava/util/HashMap;

    const-string v1, "clientAddress"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/em;

    return-object v0
.end method

.method public final getNetworkDiagnostics()Lcom/google/android/gms/games/h/a/cm;
    .locals 2
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/eo;->c:Ljava/util/HashMap;

    const-string v1, "networkDiagnostics"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/cm;

    return-object v0
.end method
