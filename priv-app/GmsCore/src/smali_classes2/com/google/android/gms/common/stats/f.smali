.class public final Lcom/google/android/gms/common/stats/f;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(Ljava/lang/String;)Lcom/google/android/gms/common/analytics/a/d;
    .locals 10

    .prologue
    const/4 v4, 0x5

    const/4 v5, 0x0

    const/4 v0, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 131
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v6, "\t"

    invoke-virtual {v1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 132
    array-length v1, v7

    if-ge v1, v4, :cond_1

    .line 133
    const-string v1, "InternalStatsUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invaid data: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    :cond_0
    :goto_0
    return-object v0

    .line 137
    :cond_1
    new-instance v1, Lcom/google/android/gms/common/analytics/a/d;

    invoke-direct {v1}, Lcom/google/android/gms/common/analytics/a/d;-><init>()V

    .line 140
    const/4 v6, 0x0

    :try_start_0
    aget-object v6, v7, v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v1, Lcom/google/android/gms/common/analytics/a/d;->a:Ljava/lang/Long;

    .line 141
    const/4 v6, 0x1

    aget-object v8, v7, v6

    const/4 v6, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_2
    move v5, v6

    :goto_1
    packed-switch v5, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Wrong event type"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 165
    :catch_0
    move-exception v1

    goto :goto_0

    .line 141
    :sswitch_0
    const-string v9, "BIND"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    goto :goto_1

    :sswitch_1
    const-string v5, "UNBIND"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v5, v2

    goto :goto_1

    :sswitch_2
    const-string v5, "CONNECT"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v5, v3

    goto :goto_1

    :sswitch_3
    const-string v5, "DISCONNECT"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v5, 0x3

    goto :goto_1

    :sswitch_4
    const-string v5, "TIME_OUT"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v5, 0x4

    goto :goto_1

    :pswitch_0
    move v2, v3

    :goto_2
    :pswitch_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/common/analytics/a/d;->b:Ljava/lang/Integer;

    .line 143
    const/4 v2, 0x2

    aget-object v2, v7, v2

    const-string v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 144
    array-length v5, v2

    if-ne v5, v3, :cond_0

    .line 150
    const/4 v5, 0x0

    aget-object v5, v2, v5

    iput-object v5, v1, Lcom/google/android/gms/common/analytics/a/d;->c:Ljava/lang/String;

    .line 151
    const/4 v5, 0x1

    aget-object v2, v2, v5

    iput-object v2, v1, Lcom/google/android/gms/common/analytics/a/d;->d:Ljava/lang/String;

    .line 153
    const/4 v2, 0x3

    aget-object v2, v7, v2

    const-string v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 154
    array-length v5, v2

    if-ne v5, v3, :cond_0

    .line 160
    const/4 v3, 0x0

    aget-object v3, v2, v3

    iput-object v3, v1, Lcom/google/android/gms/common/analytics/a/d;->e:Ljava/lang/String;

    .line 161
    const/4 v3, 0x1

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/google/android/gms/common/analytics/a/d;->f:Ljava/lang/String;

    .line 163
    const/4 v2, 0x4

    aget-object v2, v7, v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/common/analytics/a/d;->h:Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 169
    array-length v0, v7

    if-le v0, v4, :cond_3

    .line 170
    aget-object v0, v7, v4

    iput-object v0, v1, Lcom/google/android/gms/common/analytics/a/d;->g:Ljava/lang/String;

    :cond_3
    move-object v0, v1

    .line 172
    goto/16 :goto_0

    .line 141
    :pswitch_2
    const/4 v2, 0x3

    goto :goto_2

    :pswitch_3
    const/4 v2, 0x4

    goto :goto_2

    :pswitch_4
    move v2, v4

    goto :goto_2

    .line 167
    :catch_1
    move-exception v1

    goto/16 :goto_0

    .line 141
    :sswitch_data_0
    .sparse-switch
        -0x6a89cdaa -> :sswitch_1
        -0x4d188144 -> :sswitch_4
        0x1f1c3d -> :sswitch_0
        0x3c87449c -> :sswitch_3
        0x638004ca -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(Ljava/io/File;)Ljava/util/List;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 75
    .line 80
    :try_start_0
    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_b
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    :try_start_1
    invoke-virtual {v7}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 82
    const-wide/16 v2, 0x0

    const-wide v4, 0x7fffffffffffffffL

    const/4 v6, 0x1

    :try_start_2
    invoke-virtual/range {v1 .. v6}, Ljava/nio/channels/FileChannel;->tryLock(JJZ)Ljava/nio/channels/FileLock;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_d
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v4

    .line 83
    if-nez v4, :cond_3

    .line 84
    :try_start_3
    const-string v2, "InternalStatsUploader"

    const-string v3, "Not able to lock fileservice.connections.internal"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_e
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 85
    if-eqz v4, :cond_0

    .line 112
    :try_start_4
    invoke-virtual {v4}, Ljava/nio/channels/FileLock;->release()V

    .line 114
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->isOpen()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 115
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 117
    :cond_1
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 127
    :cond_2
    :goto_0
    return-object v0

    .line 123
    :catch_0
    move-exception v1

    .line 124
    const-string v2, "InternalStatsUploader"

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 87
    :cond_3
    :try_start_5
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-direct {v2, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_e
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_9
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 90
    :try_start_6
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 91
    :cond_4
    :goto_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_8

    .line 92
    invoke-static {v5}, Lcom/google/android/gms/common/stats/f;->a(Ljava/lang/String;)Lcom/google/android/gms/common/analytics/a/d;

    move-result-object v5

    .line 93
    if-eqz v5, :cond_4

    .line 97
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_a
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    goto :goto_1

    .line 105
    :catch_1
    move-exception v2

    move-object v5, v7

    move-object v8, v3

    move-object v3, v4

    move-object v4, v1

    move-object v1, v2

    move-object v2, v8

    :goto_2
    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_5

    .line 111
    if-eqz v3, :cond_5

    .line 112
    :try_start_8
    invoke-virtual {v3}, Ljava/nio/channels/FileLock;->release()V

    .line 114
    :cond_5
    if-eqz v4, :cond_6

    invoke-virtual {v4}, Ljava/nio/channels/FileChannel;->isOpen()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 115
    invoke-virtual {v4}, Ljava/nio/channels/FileChannel;->close()V

    .line 117
    :cond_6
    if-eqz v5, :cond_7

    .line 118
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    .line 120
    :cond_7
    if-eqz v2, :cond_2

    .line 121
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    goto :goto_0

    .line 123
    :catch_2
    move-exception v1

    .line 124
    const-string v2, "InternalStatsUploader"

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 111
    :cond_8
    if-eqz v4, :cond_9

    .line 112
    :try_start_9
    invoke-virtual {v4}, Ljava/nio/channels/FileLock;->release()V

    .line 114
    :cond_9
    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 115
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 117
    :cond_a
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V

    .line 120
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    :goto_3
    move-object v0, v2

    .line 125
    goto :goto_0

    .line 123
    :catch_3
    move-exception v0

    .line 124
    const-string v1, "InternalStatsUploader"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 107
    :catch_4
    move-exception v1

    move-object v3, v0

    move-object v4, v0

    move-object v2, v0

    move-object v7, v0

    :goto_4
    :try_start_a
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_6

    .line 111
    if-eqz v4, :cond_b

    .line 112
    :try_start_b
    invoke-virtual {v4}, Ljava/nio/channels/FileLock;->release()V

    .line 114
    :cond_b
    if-eqz v2, :cond_c

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->isOpen()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 115
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 117
    :cond_c
    if-eqz v7, :cond_d

    .line 118
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V

    .line 120
    :cond_d
    if-eqz v3, :cond_2

    .line 121
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5

    goto/16 :goto_0

    .line 123
    :catch_5
    move-exception v1

    .line 124
    const-string v2, "InternalStatsUploader"

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 110
    :catchall_0
    move-exception v1

    move-object v3, v0

    move-object v4, v0

    move-object v7, v0

    move-object v8, v0

    move-object v0, v1

    move-object v1, v8

    .line 111
    :goto_5
    if-eqz v4, :cond_e

    .line 112
    :try_start_c
    invoke-virtual {v4}, Ljava/nio/channels/FileLock;->release()V

    .line 114
    :cond_e
    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->isOpen()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 115
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 117
    :cond_f
    if-eqz v7, :cond_10

    .line 118
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V

    .line 120
    :cond_10
    if-eqz v3, :cond_11

    .line 121
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_6

    .line 125
    :cond_11
    :goto_6
    throw v0

    .line 123
    :catch_6
    move-exception v1

    .line 124
    const-string v2, "InternalStatsUploader"

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 110
    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v4, v0

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_5

    :catchall_2
    move-exception v2

    move-object v3, v0

    move-object v4, v0

    move-object v0, v2

    goto :goto_5

    :catchall_3
    move-exception v2

    move-object v3, v0

    move-object v0, v2

    goto :goto_5

    :catchall_4
    move-exception v0

    goto :goto_5

    :catchall_5
    move-exception v0

    move-object v1, v4

    move-object v7, v5

    move-object v4, v3

    move-object v3, v2

    goto :goto_5

    :catchall_6
    move-exception v0

    move-object v1, v2

    goto :goto_5

    .line 107
    :catch_7
    move-exception v1

    move-object v3, v0

    move-object v4, v0

    move-object v2, v0

    goto :goto_4

    :catch_8
    move-exception v2

    move-object v3, v0

    move-object v4, v0

    move-object v8, v2

    move-object v2, v1

    move-object v1, v8

    goto :goto_4

    :catch_9
    move-exception v2

    move-object v3, v0

    move-object v8, v2

    move-object v2, v1

    move-object v1, v8

    goto/16 :goto_4

    :catch_a
    move-exception v2

    move-object v8, v2

    move-object v2, v1

    move-object v1, v8

    goto/16 :goto_4

    .line 105
    :catch_b
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v4, v0

    move-object v5, v0

    goto/16 :goto_2

    :catch_c
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v4, v0

    move-object v5, v7

    goto/16 :goto_2

    :catch_d
    move-exception v2

    move-object v3, v0

    move-object v4, v1

    move-object v5, v7

    move-object v1, v2

    move-object v2, v0

    goto/16 :goto_2

    :catch_e
    move-exception v2

    move-object v3, v4

    move-object v5, v7

    move-object v4, v1

    move-object v1, v2

    move-object v2, v0

    goto/16 :goto_2
.end method
