.class public final Lcom/google/android/gms/common/app/GmsApplication;
.super Landroid/app/Application;
.source "SourceFile"


# static fields
.field private static a:I

.field private static volatile b:Lcom/google/android/gms/common/app/GmsApplication;


# instance fields
.field private c:Lcom/android/volley/s;

.field private d:Lcom/android/volley/s;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, -0x1

    sput v0, Lcom/google/android/gms/common/app/GmsApplication;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 74
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 75
    const-class v1, Lcom/google/android/gms/common/app/GmsApplication;

    monitor-enter v1

    .line 76
    :try_start_0
    sput-object p0, Lcom/google/android/gms/common/app/GmsApplication;->b:Lcom/google/android/gms/common/app/GmsApplication;

    .line 77
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 173
    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v3, "user"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 182
    :cond_0
    return-void

    .line 177
    :cond_1
    sget v0, Lcom/google/android/gms/common/app/GmsApplication;->a:I

    const/4 v3, -0x1

    if-eq v0, v3, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "GMS Core PID not set yet! You must be calling this before onCreate..."

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 179
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    sget v3, Lcom/google/android/gms/common/app/GmsApplication;->a:I

    if-ne v0, v3, :cond_3

    move v0, v1

    :goto_1
    const-string v3, "Current process (%d) is not the GMS Core main process (%d)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    sget v2, Lcom/google/android/gms/common/app/GmsApplication;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v1

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v2

    .line 177
    goto :goto_0

    :cond_3
    move v0, v2

    .line 179
    goto :goto_1
.end method

.method public static b()Lcom/google/android/gms/common/app/GmsApplication;
    .locals 1

    .prologue
    .line 190
    sget-object v0, Lcom/google/android/gms/common/app/GmsApplication;->b:Lcom/google/android/gms/common/app/GmsApplication;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(Z)Lcom/android/volley/s;
    .locals 4

    .prologue
    .line 218
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/app/GmsApplication;->d:Lcom/android/volley/s;

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/google/android/gms/common/app/GmsApplication;->d:Lcom/android/volley/s;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    :goto_0
    monitor-exit p0

    return-object v0

    .line 221
    :cond_0
    :try_start_1
    new-instance v0, Lcom/android/volley/toolbox/a;

    new-instance v1, Lcom/google/android/gms/auth/be/a;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/auth/be/a;-><init>(Landroid/content/Context;Z)V

    invoke-direct {v0, v1}, Lcom/android/volley/toolbox/a;-><init>(Lcom/android/volley/toolbox/j;)V

    .line 222
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/gms/common/app/GmsApplication;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "volleyApiary"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 223
    new-instance v2, Lcom/android/volley/s;

    new-instance v3, Lcom/android/volley/toolbox/d;

    invoke-direct {v3, v1}, Lcom/android/volley/toolbox/d;-><init>(Ljava/io/File;)V

    const/4 v1, 0x0

    invoke-direct {v2, v3, v0, v1}, Lcom/android/volley/s;-><init>(Lcom/android/volley/b;Lcom/android/volley/j;B)V

    iput-object v2, p0, Lcom/google/android/gms/common/app/GmsApplication;->d:Lcom/android/volley/s;

    .line 224
    iget-object v0, p0, Lcom/google/android/gms/common/app/GmsApplication;->d:Lcom/android/volley/s;

    invoke-virtual {v0}, Lcom/android/volley/s;->a()V

    .line 225
    iget-object v0, p0, Lcom/google/android/gms/common/app/GmsApplication;->d:Lcom/android/volley/s;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 218
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final attachBaseContext(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/app/Application;->attachBaseContext(Landroid/content/Context;)V

    .line 84
    invoke-static {p1}, Lcom/google/android/gms/common/app/e;->a(Landroid/content/Context;)V

    .line 87
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_0

    :try_start_0
    invoke-static {p0}, Landroid/support/a/a;->a(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    const-wide/16 v2, 0x5

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    .line 88
    :cond_0
    return-void
.end method

.method public final declared-synchronized c()Lcom/android/volley/s;
    .locals 6

    .prologue
    .line 199
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/gms/common/a/b;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 201
    sget-object v0, Lcom/google/android/gms/common/a/b;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/32 v4, 0x6768a8

    cmp-long v0, v2, v4

    if-gtz v0, :cond_0

    .line 203
    const-string v0, "GmsApplication"

    const-string v2, "Using Auth Proxy for data requests."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/app/GmsApplication;->a(Z)Lcom/android/volley/s;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 214
    :goto_0
    monitor-exit p0

    return-object v0

    .line 206
    :cond_0
    :try_start_1
    const-string v0, "GmsApplication"

    const-string v2, "Using standard stack for data requests."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    iget-object v0, p0, Lcom/google/android/gms/common/app/GmsApplication;->c:Lcom/android/volley/s;

    if-eqz v0, :cond_1

    .line 208
    iget-object v0, p0, Lcom/google/android/gms/common/app/GmsApplication;->c:Lcom/android/volley/s;

    goto :goto_0

    .line 210
    :cond_1
    new-instance v0, Lcom/android/volley/toolbox/a;

    new-instance v2, Lcom/google/android/gms/common/server/t;

    invoke-direct {v2, p0, v1}, Lcom/google/android/gms/common/server/t;-><init>(Landroid/content/Context;Z)V

    invoke-direct {v0, v2}, Lcom/android/volley/toolbox/a;-><init>(Lcom/android/volley/toolbox/j;)V

    .line 211
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/gms/common/app/GmsApplication;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "volley"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 212
    new-instance v2, Lcom/android/volley/s;

    new-instance v3, Lcom/android/volley/toolbox/d;

    invoke-direct {v3, v1}, Lcom/android/volley/toolbox/d;-><init>(Ljava/io/File;)V

    const/4 v1, 0x0

    invoke-direct {v2, v3, v0, v1}, Lcom/android/volley/s;-><init>(Lcom/android/volley/b;Lcom/android/volley/j;B)V

    iput-object v2, p0, Lcom/google/android/gms/common/app/GmsApplication;->c:Lcom/android/volley/s;

    .line 213
    iget-object v0, p0, Lcom/google/android/gms/common/app/GmsApplication;->c:Lcom/android/volley/s;

    invoke-virtual {v0}, Lcom/android/volley/s;->a()V

    .line 214
    iget-object v0, p0, Lcom/google/android/gms/common/app/GmsApplication;->c:Lcom/android/volley/s;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    .line 92
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 93
    invoke-static {p0}, Lcom/google/android/gsf/f;->a(Landroid/content/Context;)V

    .line 99
    invoke-static {p0}, Lcom/google/android/gms/common/a/d;->a(Landroid/content/Context;)V

    .line 100
    sput-object p0, Lcom/google/android/gms/common/a/m;->a:Landroid/content/Context;

    .line 104
    :try_start_0
    sget-object v0, Lcom/google/android/gms/common/security/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-static {p0, v0}, Lcom/google/android/gms/common/security/ProviderInstallerImpl;->insertProviderGated(Landroid/content/Context;Lcom/google/android/gms/common/a/d;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.INITIALIZE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 113
    invoke-virtual {p0}, Lcom/google/android/gms/common/app/GmsApplication;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/app/GmsApplication;->sendBroadcast(Landroid/content/Intent;)V

    .line 118
    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v1, "user"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget v0, Lcom/google/android/gms/common/app/GmsApplication;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 119
    invoke-virtual {p0}, Lcom/google/android/gms/common/app/GmsApplication;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v1, v0, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    .line 120
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/app/GmsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 122
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 123
    iget-object v3, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 124
    iget v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    sput v0, Lcom/google/android/gms/common/app/GmsApplication;->a:I

    .line 131
    :cond_1
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 132
    invoke-static {}, Lcom/google/android/gms/common/util/ay;->e()I

    move-result v0

    .line 133
    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 134
    const/4 v0, 0x2

    const-string v1, "NOT_LMP"

    invoke-static {v0, p0, v1}, Lcom/google/android/gms/common/ew;->a(ILandroid/content/Context;Ljava/lang/String;)V

    .line 144
    :cond_2
    :goto_1
    return-void

    .line 106
    :catch_0
    move-exception v0

    .line 107
    const-string v1, "GmsApplication"

    const-string v2, "Unable to install secure provider, due to exception:"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 137
    :cond_3
    const/16 v1, 0x9

    if-ne v0, v1, :cond_2

    .line 138
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/app/GmsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 140
    const-string v1, "NOT_LMP"

    const v2, 0x9b6d

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    goto :goto_1
.end method
