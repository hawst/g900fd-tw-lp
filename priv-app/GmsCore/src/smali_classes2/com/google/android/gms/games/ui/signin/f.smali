.class public final Lcom/google/android/gms/games/ui/signin/f;
.super Lcom/google/android/gms/games/ui/b/b;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field private j:Lcom/google/android/gms/games/ui/signin/e;

.field private k:Landroid/view/View;

.field private l:Landroid/view/animation/Animation;

.field private m:Landroid/view/animation/Animation;

.field private n:Landroid/view/animation/Animation;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/b/b;-><init>()V

    return-void
.end method

.method private a(I)Landroid/view/animation/Animation;
    .locals 3

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->j:Lcom/google/android/gms/games/ui/signin/e;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/e;->d()Ljava/lang/Object;

    move-result-object v1

    .line 300
    monitor-enter v1

    .line 301
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 302
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 303
    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 304
    iget-object v2, p0, Lcom/google/android/gms/games/ui/signin/f;->j:Lcom/google/android/gms/games/ui/signin/e;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/signin/e;->f()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 305
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 306
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    const-wide/16 v6, 0x5dc

    .line 173
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/f;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/signin/e;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->j:Lcom/google/android/gms/games/ui/signin/e;

    .line 176
    new-instance v2, Landroid/app/Dialog;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/q;->d:I

    invoke-direct {v2, v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 177
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 178
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 179
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 180
    sget v1, Lcom/google/android/gms/l;->aG:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->k:Landroid/view/View;

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->k:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->k:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 183
    invoke-virtual {v2, p0}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 186
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->j:Lcom/google/android/gms/games/ui/signin/e;

    if-nez v0, :cond_0

    const-string v0, "GamesConnectingFrag"

    const-string v1, "bindViewData: mParentFragment not set; bailing out..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    :goto_0
    return-object v2

    .line 186
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "GamesConnectingFrag"

    const-string v1, "bindViewData: no Activity; bailing out"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/support/v4/app/q;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->j:Lcom/google/android/gms/games/ui/signin/e;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->h()Ljava/lang/String;

    move-result-object v3

    :try_start_0
    invoke-virtual {v1, v3}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->k:Landroid/view/View;

    sget v5, Lcom/google/android/gms/j;->px:I

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->k:Landroid/view/View;

    sget v4, Lcom/google/android/gms/j;->pv:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v4, 0x80

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    if-nez v4, :cond_2

    const-string v1, ""

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    sget v0, Lcom/google/android/gms/b;->f:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/signin/f;->a(I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->l:Landroid/view/animation/Animation;

    sget v0, Lcom/google/android/gms/b;->e:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/signin/f;->a(I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->m:Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->l:Landroid/view/animation/Animation;

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setStartOffset(J)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->m:Landroid/view/animation/Animation;

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setStartOffset(J)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->k:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->gu:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/f;->l:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->k:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->pz:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/f;->m:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "GamesConnectingFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Couldn\'t find icon for package "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->j:Lcom/google/android/gms/games/ui/signin/e;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/e;->d()Ljava/lang/Object;

    move-result-object v1

    .line 317
    monitor-enter v1

    .line 320
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->j:Lcom/google/android/gms/games/ui/signin/e;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/e;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->l:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_1

    .line 323
    sget v0, Lcom/google/android/gms/b;->g:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/signin/f;->a(I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->n:Landroid/view/animation/Animation;

    .line 324
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->k:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/signin/f;->n:Landroid/view/animation/Animation;

    invoke-virtual {v0, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 330
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 325
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->n:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->j:Lcom/google/android/gms/games/ui/signin/e;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/e;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 330
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 336
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 312
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/f;->dismiss()V

    .line 212
    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 280
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/b/b;->onDismiss(Landroid/content/DialogInterface;)V

    .line 284
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->j:Lcom/google/android/gms/games/ui/signin/e;

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->j:Lcom/google/android/gms/games/ui/signin/e;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/e;->c()V

    .line 289
    :goto_0
    return-void

    .line 287
    :cond_0
    const-string v0, "GamesConnectingFrag"

    const-string v1, "ConnectingDialogFragment.onDismiss: null mParentFragment"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 218
    const/16 v0, 0x60

    if-eq p2, v0, :cond_0

    const/16 v0, 0x42

    if-ne p2, v0, :cond_1

    .line 219
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/f;->dismiss()V

    .line 220
    const/4 v0, 0x1

    .line 222
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onStart()V
    .locals 3

    .prologue
    .line 193
    invoke-super {p0}, Lcom/google/android/gms/games/ui/b/b;->onStart()V

    .line 197
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/f;->j:Lcom/google/android/gms/games/ui/signin/e;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Landroid/support/v4/app/m;->f:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 200
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 201
    iget-object v2, p0, Lcom/google/android/gms/games/ui/signin/f;->j:Lcom/google/android/gms/games/ui/signin/e;

    iget-object v2, v2, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->m()I

    move-result v2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 202
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 206
    :goto_0
    return-void

    .line 204
    :cond_0
    const-string v0, "GamesConnectingFrag"

    const-string v1, "ConnectingDialogFragment.onStart: null mParentFragment"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
