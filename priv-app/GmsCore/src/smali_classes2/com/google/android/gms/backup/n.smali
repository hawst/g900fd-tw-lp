.class public final Lcom/google/android/gms/backup/n;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Z

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 760
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 765
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/backup/n;->b:Z

    .line 797
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/backup/n;->c:I

    .line 760
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 800
    iget v0, p0, Lcom/google/android/gms/backup/n;->c:I

    if-gez v0, :cond_0

    .line 802
    invoke-virtual {p0}, Lcom/google/android/gms/backup/n;->b()I

    .line 804
    :cond_0
    iget v0, p0, Lcom/google/android/gms/backup/n;->c:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 2

    .prologue
    .line 757
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->d()Z

    move-result v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/backup/n;->a:Z

    iput-boolean v0, p0, Lcom/google/android/gms/backup/n;->b:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x90 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 792
    iget-boolean v0, p0, Lcom/google/android/gms/backup/n;->a:Z

    if-eqz v0, :cond_0

    .line 793
    const/16 v0, 0x12

    iget-boolean v1, p0, Lcom/google/android/gms/backup/n;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(IZ)V

    .line 795
    :cond_0
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 809
    const/4 v0, 0x0

    .line 810
    iget-boolean v1, p0, Lcom/google/android/gms/backup/n;->a:Z

    if-eqz v1, :cond_0

    .line 811
    const/16 v0, 0x12

    iget-boolean v1, p0, Lcom/google/android/gms/backup/n;->b:Z

    invoke-static {v0}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 814
    :cond_0
    iput v0, p0, Lcom/google/android/gms/backup/n;->c:I

    .line 815
    return v0
.end method
