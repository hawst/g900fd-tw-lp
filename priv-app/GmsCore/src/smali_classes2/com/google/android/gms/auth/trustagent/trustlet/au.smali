.class public final Lcom/google/android/gms/auth/trustagent/trustlet/au;
.super Lcom/google/android/gms/auth/trustagent/trustlet/bh;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/auth/trustagent/trustlet/as;
.implements Lcom/google/android/gms/auth/trustagent/trustlet/be;


# static fields
.field private static c:Z


# instance fields
.field private a:Lcom/google/android/gms/auth/trustagent/trustlet/ar;

.field private b:Lcom/google/android/gms/auth/trustagent/trustlet/bd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/gms/auth/trustagent/trustlet/au;->c:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;-><init>()V

    .line 35
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->g:Z

    if-eqz v0, :cond_0

    .line 105
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/au;->e(Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/au;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ar;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->a()V

    .line 108
    :cond_0
    return-void
.end method

.method protected final b()V
    .locals 2

    .prologue
    .line 60
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/ar;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ar;-><init>(Landroid/content/Context;Lcom/google/android/gms/auth/trustagent/trustlet/as;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/au;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ar;

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/au;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ar;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/bd;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/bd;-><init>(Landroid/content/Context;Lcom/google/android/gms/auth/trustagent/trustlet/be;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/au;->b:Lcom/google/android/gms/auth/trustagent/trustlet/bd;

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/au;->b:Lcom/google/android/gms/auth/trustagent/trustlet/bd;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/bd;->a()V

    .line 64
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/au;->b(Z)V

    .line 68
    :goto_0
    return-void

    .line 66
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/au;->b(Z)V

    goto :goto_0
.end method

.method protected final c()V
    .locals 1

    .prologue
    .line 72
    invoke-super {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->c()V

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/au;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ar;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/au;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ar;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->a()V

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/au;->b:Lcom/google/android/gms/auth/trustagent/trustlet/bd;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/bd;->b()V

    .line 77
    :cond_0
    return-void
.end method

.method protected final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    const-string v0, "PhonePosition"

    return-object v0
.end method

.method protected final e()I
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x5

    return v0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 82
    const-string v0, "Coffee - PhonePositionTrustlet"

    const-string v1, "Screen is unlocked"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/gms/auth/trustagent/trustlet/au;->c:Z

    .line 84
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 92
    sget-boolean v0, Lcom/google/android/gms/auth/trustagent/trustlet/au;->c:Z

    if-eqz v0, :cond_1

    .line 93
    const-string v0, "Coffee - PhonePositionTrustlet"

    const-string v1, "screen from on to off, reset on-person state"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/au;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ar;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->c()V

    .line 95
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->g:Z

    if-nez v0, :cond_0

    .line 96
    const/4 v0, 0x0

    const-string v1, "user-present"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/au;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/auth/trustagent/trustlet/au;->c:Z

    .line 100
    :cond_1
    return-void
.end method
