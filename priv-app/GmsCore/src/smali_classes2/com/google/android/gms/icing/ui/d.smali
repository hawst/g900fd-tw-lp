.class final Lcom/google/android/gms/icing/ui/d;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 2

    .prologue
    .line 114
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 115
    iput-object p2, p0, Lcom/google/android/gms/icing/ui/d;->a:Ljava/util/List;

    .line 116
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/d;->a:Ljava/util/List;

    new-instance v1, Lcom/google/android/gms/icing/ui/e;

    invoke-direct {v1, p0}, Lcom/google/android/gms/icing/ui/e;-><init>(Lcom/google/android/gms/icing/ui/d;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 123
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/d;->b:Landroid/view/LayoutInflater;

    .line 124
    iput-object p1, p0, Lcom/google/android/gms/icing/ui/d;->c:Landroid/content/Context;

    .line 125
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/gms/icing/ui/m;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/d;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/ui/m;

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 108
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/ui/d;->a(I)Lcom/google/android/gms/icing/ui/m;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 139
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 144
    if-nez p2, :cond_0

    .line 145
    iget-object v0, p0, Lcom/google/android/gms/icing/ui/d;->b:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/gms/l;->cJ:I

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 146
    new-instance v0, Lcom/google/android/gms/icing/ui/f;

    iget-object v2, p0, Lcom/google/android/gms/icing/ui/d;->c:Landroid/content/Context;

    invoke-direct {v0, v2, p2}, Lcom/google/android/gms/icing/ui/f;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 148
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/ui/f;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/ui/d;->a(I)Lcom/google/android/gms/icing/ui/m;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/icing/ui/f;->a:Landroid/widget/TextView;

    iget-object v4, v2, Lcom/google/android/gms/icing/ui/m;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v0, Lcom/google/android/gms/icing/ui/f;->b:Landroid/widget/TextView;

    iget-wide v4, v2, Lcom/google/android/gms/icing/ui/m;->d:J

    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-ltz v6, :cond_1

    iget-object v1, v0, Lcom/google/android/gms/icing/ui/f;->d:Landroid/content/Context;

    invoke-static {v1, v4, v5}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    :cond_1
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v0, Lcom/google/android/gms/icing/ui/f;->c:Landroid/widget/ImageView;

    iget-object v1, v2, Lcom/google/android/gms/icing/ui/m;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 150
    return-object p2
.end method
