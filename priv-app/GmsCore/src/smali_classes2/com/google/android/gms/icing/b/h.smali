.class public abstract Lcom/google/android/gms/icing/b/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/icing/b/j;


# instance fields
.field private final a:Landroid/os/ConditionVariable;

.field private volatile b:Ljava/lang/Object;

.field private final c:Ljava/util/concurrent/atomic/AtomicInteger;

.field final i:I


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/b/h;->a:Landroid/os/ConditionVariable;

    .line 59
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/icing/b/h;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 62
    invoke-static {p1}, Lcom/google/android/gms/icing/b/i;->a(I)V

    .line 63
    iput p1, p0, Lcom/google/android/gms/icing/b/h;->i:I

    .line 64
    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
.end method

.method final a(II)V
    .locals 4

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/gms/icing/b/h;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 178
    const-string v0, "Invalid task state for %s. Expected %d, was %d."

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/icing/b/h;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 180
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 182
    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 127
    return-void
.end method

.method protected b()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 167
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method final d()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x2

    .line 71
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/icing/b/h;->a(II)V

    .line 73
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/b/h;->a()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/b/h;->b:Ljava/lang/Object;

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/icing/b/h;->b:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/b/h;->a(Ljava/lang/Object;)V

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/icing/b/h;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/icing/b/h;->a(II)V

    .line 78
    return-void

    .line 77
    :catchall_0
    move-exception v0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/icing/b/h;->a(II)V

    throw v0
.end method

.method public final e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/gms/icing/b/h;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/icing/b/h;->b:Ljava/lang/Object;

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/icing/b/h;->b:Ljava/lang/Object;

    return-object v0
.end method

.method protected finalize()V
    .locals 2

    .prologue
    .line 189
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/b/h;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    .line 190
    if-eqz v0, :cond_0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 191
    const-string v1, "Incomplete task %s with state %d."

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, p0, v0}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 195
    return-void

    .line 194
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 172
    const-string v0, "%s(scheduling=%d)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/gms/icing/b/h;->i:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
