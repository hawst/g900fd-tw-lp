.class public final Lcom/google/android/gms/wallet/firstparty/GetInstrumentsResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field a:[Ljava/lang/String;

.field b:[[B

.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/gms/wallet/firstparty/c;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/firstparty/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/firstparty/GetInstrumentsResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 62
    const/4 v0, 0x1

    new-array v1, v2, [Ljava/lang/String;

    new-array v2, v2, [[B

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/wallet/firstparty/GetInstrumentsResponse;-><init>(I[Ljava/lang/String;[[B)V

    .line 63
    return-void
.end method

.method constructor <init>(I[Ljava/lang/String;[[B)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput p1, p0, Lcom/google/android/gms/wallet/firstparty/GetInstrumentsResponse;->c:I

    .line 57
    iput-object p2, p0, Lcom/google/android/gms/wallet/firstparty/GetInstrumentsResponse;->a:[Ljava/lang/String;

    .line 58
    iput-object p3, p0, Lcom/google/android/gms/wallet/firstparty/GetInstrumentsResponse;->b:[[B

    .line 59
    return-void
.end method

.method public static a()Lcom/google/android/gms/wallet/firstparty/b;
    .locals 3

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/gms/wallet/firstparty/b;

    new-instance v1, Lcom/google/android/gms/wallet/firstparty/GetInstrumentsResponse;

    invoke-direct {v1}, Lcom/google/android/gms/wallet/firstparty/GetInstrumentsResponse;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/firstparty/b;-><init>(Lcom/google/android/gms/wallet/firstparty/GetInstrumentsResponse;B)V

    return-object v0
.end method


# virtual methods
.method public final b()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/google/android/gms/wallet/firstparty/GetInstrumentsResponse;->c:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 42
    invoke-static {p0, p1}, Lcom/google/android/gms/wallet/firstparty/c;->a(Lcom/google/android/gms/wallet/firstparty/GetInstrumentsResponse;Landroid/os/Parcel;)V

    .line 43
    return-void
.end method
