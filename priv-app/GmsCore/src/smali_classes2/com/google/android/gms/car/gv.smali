.class final Lcom/google/android/gms/car/gv;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/CarService;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/CarService;)V
    .locals 0

    .prologue
    .line 253
    iput-object p1, p0, Lcom/google/android/gms/car/gv;->a:Lcom/google/android/gms/car/CarService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 256
    const-string v0, "CAR.SERVICE"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    const-string v0, "CAR.SERVICE"

    const-string v1, "onReceive"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    :cond_0
    const-string v0, "accessory"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/hardware/usb/UsbAccessory;

    .line 261
    if-eqz v0, :cond_2

    .line 262
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 263
    const-string v1, "android.hardware.usb.action.USB_ACCESSORY_DETACHED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 264
    const-string v0, "CAR.SERVICE"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 265
    const-string v0, "CAR.SERVICE"

    const-string v1, "car disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/gv;->a:Lcom/google/android/gms/car/CarService;

    invoke-static {v0}, Lcom/google/android/gms/car/CarService;->a(Lcom/google/android/gms/car/CarService;)Lcom/google/android/gms/car/gx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->B()V

    .line 270
    :cond_2
    return-void
.end method
