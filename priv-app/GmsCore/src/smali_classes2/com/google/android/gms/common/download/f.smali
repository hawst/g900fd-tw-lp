.class public final Lcom/google/android/gms/common/download/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:J

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:I

.field public final g:I


# direct methods
.method private constructor <init>(Lcom/google/android/gms/common/download/g;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    iget-wide v4, p1, Lcom/google/android/gms/common/download/g;->c:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sizeBytes ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p1, Lcom/google/android/gms/common/download/g;->c:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") must be greater than zero"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 123
    iget v0, p1, Lcom/google/android/gms/common/download/g;->f:I

    iget v3, p1, Lcom/google/android/gms/common/download/g;->g:I

    if-gt v0, v3, :cond_1

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "The minVersion ("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Lcom/google/android/gms/common/download/g;->f:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") must be less than or equal to the maxVersion ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p1, Lcom/google/android/gms/common/download/g;->g:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 126
    iget-object v0, p1, Lcom/google/android/gms/common/download/g;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    .line 127
    iget-object v0, p1, Lcom/google/android/gms/common/download/g;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/common/download/f;->b:Ljava/lang/String;

    .line 128
    iget-wide v0, p1, Lcom/google/android/gms/common/download/g;->c:J

    iput-wide v0, p0, Lcom/google/android/gms/common/download/f;->c:J

    .line 129
    iget-object v0, p1, Lcom/google/android/gms/common/download/g;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/common/download/f;->d:Ljava/lang/String;

    .line 130
    iget-object v0, p1, Lcom/google/android/gms/common/download/g;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/common/download/f;->e:Ljava/lang/String;

    .line 131
    iget v0, p1, Lcom/google/android/gms/common/download/g;->f:I

    iput v0, p0, Lcom/google/android/gms/common/download/f;->f:I

    .line 132
    iget v0, p1, Lcom/google/android/gms/common/download/g;->g:I

    iput v0, p0, Lcom/google/android/gms/common/download/f;->g:I

    .line 133
    return-void

    :cond_0
    move v0, v2

    .line 121
    goto :goto_0

    :cond_1
    move v1, v2

    .line 123
    goto :goto_1
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/common/download/g;B)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/download/f;-><init>(Lcom/google/android/gms/common/download/g;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    .locals 9
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 95
    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/common/download/f;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;B)V

    .line 96
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;B)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The minVersion (0"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ") must be less than or equal to the maxVersion (2147483647"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 109
    const-wide/16 v2, 0x0

    cmp-long v2, p3, v2

    if-lez v2, :cond_0

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sizeBytes ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") must be greater than zero"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 111
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    .line 112
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/common/download/f;->b:Ljava/lang/String;

    .line 113
    iput-wide p3, p0, Lcom/google/android/gms/common/download/f;->c:J

    .line 114
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/common/download/f;->d:Ljava/lang/String;

    .line 115
    iput-object p6, p0, Lcom/google/android/gms/common/download/f;->e:Ljava/lang/String;

    .line 116
    iput v1, p0, Lcom/google/android/gms/common/download/f;->f:I

    .line 117
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/gms/common/download/f;->g:I

    .line 118
    return-void

    :cond_0
    move v0, v1

    .line 109
    goto :goto_0
.end method

.method protected constructor <init>(Lorg/json/JSONObject;)V
    .locals 2

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    const-string v0, "filename"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    .line 137
    const-string v0, "url"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/download/f;->b:Ljava/lang/String;

    .line 138
    const-string v0, "sizeBytes"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/common/download/f;->c:J

    .line 139
    const-string v0, "sha1"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/download/f;->d:Ljava/lang/String;

    .line 142
    const-string v0, "destination"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    const-string v0, "destination"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/download/f;->e:Ljava/lang/String;

    .line 147
    :goto_0
    const-string v0, "minVersion"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 148
    const-string v0, "minVersion"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/download/f;->f:I

    .line 152
    :goto_1
    const-string v0, "maxVersion"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 153
    const-string v0, "maxVersion"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/download/f;->g:I

    .line 157
    :goto_2
    return-void

    .line 145
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/download/f;->e:Ljava/lang/String;

    goto :goto_0

    .line 150
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/common/download/f;->f:I

    goto :goto_1

    .line 155
    :cond_2
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/gms/common/download/f;->g:I

    goto :goto_2
.end method


# virtual methods
.method public final a()Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 219
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 220
    const-string v1, "filename"

    iget-object v2, p0, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const-string v1, "url"

    iget-object v2, p0, Lcom/google/android/gms/common/download/f;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const-string v1, "sizeBytes"

    iget-wide v2, p0, Lcom/google/android/gms/common/download/f;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 223
    const-string v1, "sha1"

    iget-object v2, p0, Lcom/google/android/gms/common/download/f;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    iget-object v1, p0, Lcom/google/android/gms/common/download/f;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 225
    const-string v1, "destination"

    iget-object v2, p0, Lcom/google/android/gms/common/download/f;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    :cond_0
    iget v1, p0, Lcom/google/android/gms/common/download/f;->g:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 228
    const-string v1, "minVersion"

    iget v2, p0, Lcom/google/android/gms/common/download/f;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 230
    :cond_1
    iget v1, p0, Lcom/google/android/gms/common/download/f;->g:I

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_2

    .line 231
    const-string v1, "maxVersion"

    iget v2, p0, Lcom/google/android/gms/common/download/f;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 233
    :cond_2
    return-object v0
.end method

.method final a(Landroid/content/Context;)Ljava/io/File;
    .locals 3

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/gms/common/download/f;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 184
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/download/f;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 189
    :goto_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 190
    return-object v1

    .line 186
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-static {p1}, Lcom/google/android/gms/common/download/DownloadService;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method final b(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 194
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/download/f;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 195
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 196
    invoke-static {v0}, Lcom/google/android/gms/common/download/DownloadService;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    .line 197
    iget-object v2, p0, Lcom/google/android/gms/common/download/f;->d:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 198
    const/4 v0, 0x1

    .line 205
    :goto_0
    return v0

    .line 201
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 202
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 205
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 238
    instance-of v0, p1, Lcom/google/android/gms/common/download/f;

    if-eqz v0, :cond_2

    .line 239
    check-cast p1, Lcom/google/android/gms/common/download/f;

    .line 240
    iget-object v0, p0, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/common/download/f;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/common/download/f;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lcom/google/android/gms/common/download/f;->c:J

    iget-wide v2, p1, Lcom/google/android/gms/common/download/f;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/common/download/f;->d:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/common/download/f;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/common/download/f;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/common/download/f;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/download/f;->e:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/common/download/f;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget v0, p0, Lcom/google/android/gms/common/download/f;->f:I

    iget v1, p1, Lcom/google/android/gms/common/download/f;->f:I

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/google/android/gms/common/download/f;->g:I

    iget v1, p1, Lcom/google/android/gms/common/download/f;->g:I

    if-ne v0, v1, :cond_2

    .line 248
    const/4 v0, 0x1

    .line 251
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 256
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/common/download/f;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/common/download/f;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/common/download/f;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/common/download/f;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/common/download/f;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/gms/common/download/f;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
