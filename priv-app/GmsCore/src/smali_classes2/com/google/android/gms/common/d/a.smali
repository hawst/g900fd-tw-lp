.class public final Lcom/google/android/gms/common/d/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/google/android/gms/common/internal/aj;->f:[Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/common/d/a;->a:[Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;)Landroid/accounts/AccountManagerFuture;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 234
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "The accountName is required"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 235
    if-eqz p2, :cond_2

    move v0, v1

    :goto_1
    const-string v3, "The requiredFeatures parameter is required"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 238
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    .line 242
    const-string v0, "com.google"

    invoke-virtual {v3, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v4

    array-length v5, v4

    move v0, v2

    :goto_2
    if-ge v2, v5, :cond_3

    aget-object v6, v4, v2

    .line 243
    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v0, v1

    .line 242
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    move v0, v2

    .line 234
    goto :goto_0

    :cond_2
    move v0, v2

    .line 235
    goto :goto_1

    .line 248
    :cond_3
    if-nez v0, :cond_4

    .line 249
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Given account does not exist on the device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 252
    :cond_4
    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, p1, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v3, v0, p2, p3, v1}, Landroid/accounts/AccountManager;->hasFeatures(Landroid/accounts/Account;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v0

    return-object v0
.end method
