.class final Lcom/google/android/gms/cast/internal/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/internal/l;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/cast/internal/l;)V
    .locals 0

    .prologue
    .line 760
    iput-object p1, p0, Lcom/google/android/gms/cast/internal/m;->a:Lcom/google/android/gms/cast/internal/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/cast/internal/l;B)V
    .locals 0

    .prologue
    .line 760
    invoke-direct {p0, p1}, Lcom/google/android/gms/cast/internal/m;-><init>(Lcom/google/android/gms/cast/internal/l;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 763
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/m;->a:Lcom/google/android/gms/cast/internal/l;

    invoke-static {v0}, Lcom/google/android/gms/cast/internal/l;->a(Lcom/google/android/gms/cast/internal/l;)Z

    .line 764
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 765
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/m;->a:Lcom/google/android/gms/cast/internal/l;

    invoke-static {v0}, Lcom/google/android/gms/cast/internal/l;->b(Lcom/google/android/gms/cast/internal/l;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/internal/p;

    .line 766
    const/16 v4, 0x836

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/gms/cast/internal/p;->b(JI)Z

    goto :goto_0

    .line 770
    :cond_0
    const/4 v1, 0x0

    .line 771
    sget-object v2, Lcom/google/android/gms/cast/internal/p;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 772
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/m;->a:Lcom/google/android/gms/cast/internal/l;

    invoke-static {v0}, Lcom/google/android/gms/cast/internal/l;->b(Lcom/google/android/gms/cast/internal/l;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/internal/p;

    .line 773
    invoke-virtual {v0}, Lcom/google/android/gms/cast/internal/p;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 774
    const/4 v0, 0x1

    :goto_2
    move v1, v0

    .line 776
    goto :goto_1

    .line 777
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 778
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/m;->a:Lcom/google/android/gms/cast/internal/l;

    invoke-static {v0, v1}, Lcom/google/android/gms/cast/internal/l;->a(Lcom/google/android/gms/cast/internal/l;Z)V

    .line 779
    return-void

    .line 777
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_2
    move v0, v1

    goto :goto_2
.end method
