.class public final Lcom/google/android/gms/games/service/a/i/m;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:Lcom/google/android/gms/games/a/au;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V
    .locals 1

    .prologue
    .line 25
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 26
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/i/m;->c:Lcom/google/android/gms/games/internal/dr;

    .line 27
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/i/m;->d:Lcom/google/android/gms/games/a/au;

    .line 28
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 10

    .prologue
    .line 37
    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v1

    .line 38
    iget-object v2, p0, Lcom/google/android/gms/games/service/a/i/m;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v6

    const-string v7, "game_category"

    invoke-virtual {p1, v7, v0, v6}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v7

    const-string v8, "xp_for_game"

    invoke-virtual {p1, v8, v0, v6}, Lcom/google/android/gms/common/data/DataHolder;->a(Ljava/lang/String;II)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v5, v7, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v0, "game_category_list"

    invoke-virtual {v5, v0, v4}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-interface {v2, v1, v5}, Lcom/google/android/gms/games/internal/dr;->d(ILandroid/os/Bundle;)V

    .line 40
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/i/m;->d:Lcom/google/android/gms/games/a/au;

    invoke-virtual {p2, v0}, Lcom/google/android/gms/games/a/t;->c(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
