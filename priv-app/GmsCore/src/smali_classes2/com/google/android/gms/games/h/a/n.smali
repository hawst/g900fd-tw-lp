.class public final Lcom/google/android/gms/games/h/a/n;
.super Lcom/google/android/gms/common/server/j;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/n;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/n;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/gms/common/server/j;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/google/android/gms/games/h/a/n;->a:Lcom/google/android/gms/common/server/n;

    .line 34
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/h/a/y;)Lcom/google/android/gms/games/h/a/cu;
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 120
    const-string v0, "players/%1$s/achievements"

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "state"

    invoke-static {p3}, Lcom/google/android/gms/games/h/a/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/games/h/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz p4, :cond_0

    const-string v0, "language"

    invoke-static {p4}, Lcom/google/android/gms/games/h/a/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/games/h/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz p5, :cond_1

    const-string v0, "pageToken"

    invoke-static {p5}, Lcom/google/android/gms/games/h/a/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/games/h/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 121
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/n;->a:Lcom/google/android/gms/common/server/n;

    const-class v5, Lcom/google/android/gms/games/h/a/cu;

    move-object v1, p1

    move-object v4, p6

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/cu;

    return-object v0
.end method
