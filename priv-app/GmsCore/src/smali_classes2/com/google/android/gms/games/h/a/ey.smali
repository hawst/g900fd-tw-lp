.class public final Lcom/google/android/gms/games/h/a/ey;
.super Lcom/google/android/gms/common/server/response/c;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 51
    sput-object v0, Lcom/google/android/gms/games/h/a/ey;->b:Ljava/util/HashMap;

    const-string v1, "leaderboardId"

    const-string v2, "leaderboardId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/google/android/gms/games/h/a/ey;->b:Ljava/util/HashMap;

    const-string v1, "score"

    const-string v2, "score"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->c(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/google/android/gms/games/h/a/ey;->b:Ljava/util/HashMap;

    const-string v1, "scoreTag"

    const-string v2, "scoreTag"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/google/android/gms/games/h/a/ey;->b:Ljava/util/HashMap;

    const-string v1, "signature"

    const-string v2, "signature"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 70
    if-eqz p1, :cond_0

    .line 71
    const-string v0, "leaderboardId"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/h/a/ey;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    :cond_0
    if-eqz p2, :cond_1

    .line 74
    const-string v0, "score"

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/games/h/a/ey;->a(Ljava/lang/String;J)V

    .line 76
    :cond_1
    if-eqz p3, :cond_2

    .line 77
    const-string v0, "scoreTag"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/games/h/a/ey;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    :cond_2
    if-eqz p4, :cond_3

    .line 80
    const-string v0, "signature"

    invoke-virtual {p0, v0, p4}, Lcom/google/android/gms/games/h/a/ey;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    :cond_3
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/google/android/gms/games/h/a/ey;->b:Ljava/util/HashMap;

    return-object v0
.end method
