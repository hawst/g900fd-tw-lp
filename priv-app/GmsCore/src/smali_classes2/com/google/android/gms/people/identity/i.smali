.class public final Lcom/google/android/gms/people/identity/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:Z

.field public final f:Lcom/google/android/gms/people/identity/j;

.field public final g:I

.field private final h:Ljava/util/List;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;ZLcom/google/android/gms/people/identity/j;)V
    .locals 1

    .prologue
    .line 197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 198
    iput-object p1, p0, Lcom/google/android/gms/people/identity/i;->a:Ljava/lang/String;

    .line 199
    iput-object p2, p0, Lcom/google/android/gms/people/identity/i;->b:Ljava/lang/String;

    .line 200
    iput-object p3, p0, Lcom/google/android/gms/people/identity/i;->c:Ljava/lang/String;

    .line 201
    iput-object p5, p0, Lcom/google/android/gms/people/identity/i;->h:Ljava/util/List;

    .line 202
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/people/identity/i;->d:Z

    .line 203
    iput-boolean p6, p0, Lcom/google/android/gms/people/identity/i;->e:Z

    .line 204
    iput-object p7, p0, Lcom/google/android/gms/people/identity/i;->f:Lcom/google/android/gms/people/identity/j;

    .line 205
    iput p4, p0, Lcom/google/android/gms/people/identity/i;->g:I

    .line 206
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/gms/people/identity/i;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/identity/i;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
