.class final Lcom/google/android/gms/people/internal/ad;
.super Lcom/google/android/gms/people/internal/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/people/internal/s;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/internal/s;)V
    .locals 0

    .prologue
    .line 1847
    invoke-direct {p0}, Lcom/google/android/gms/people/internal/a;-><init>()V

    .line 1848
    iput-object p1, p0, Lcom/google/android/gms/people/internal/ad;->a:Lcom/google/android/gms/people/internal/s;

    .line 1849
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1853
    monitor-enter p0

    const/4 v0, 0x3

    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1854
    const-string v0, "PeopleClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetById callback: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nresolution="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\ncontent="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1858
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/internal/ad;->a:Lcom/google/android/gms/people/internal/s;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/people/internal/s;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1859
    monitor-exit p0

    return-void

    .line 1853
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
