.class final Lcom/google/android/gms/car/qv;
.super Lcom/google/android/gms/car/qp;
.source "SourceFile"


# static fields
.field private static l:I

.field private static m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 807
    const/4 v0, 0x0

    sput v0, Lcom/google/android/gms/car/qv;->l:I

    .line 808
    const/16 v0, 0x4b

    sput v0, Lcom/google/android/gms/car/qv;->m:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 811
    sget v0, Lcom/google/android/gms/car/qv;->l:I

    sget v1, Lcom/google/android/gms/car/qv;->m:I

    add-int/2addr v0, v1

    int-to-long v0, v0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/car/qp;-><init>(JZ)V

    .line 812
    iget-object v0, p0, Lcom/google/android/gms/car/qv;->k:Lcom/google/android/gms/car/qq;

    iget-object v0, v0, Lcom/google/android/gms/car/qq;->a:[F

    invoke-static {v0, v2}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 813
    return-void
.end method


# virtual methods
.method protected final a(ZJ)Lcom/google/android/gms/car/qq;
    .locals 4

    .prologue
    .line 818
    iget-object v0, p0, Lcom/google/android/gms/car/qv;->k:Lcom/google/android/gms/car/qq;

    iput-wide p2, v0, Lcom/google/android/gms/car/qq;->c:J

    .line 819
    sget-object v0, Lcom/google/android/gms/car/qv;->c:Lcom/google/android/gms/car/pd;

    long-to-float v1, p2

    sget v2, Lcom/google/android/gms/car/qv;->l:I

    int-to-float v2, v2

    sget v3, Lcom/google/android/gms/car/qv;->m:I

    int-to-float v3, v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/car/pd;->a(FFF)F

    move-result v0

    .line 821
    iget-object v1, p0, Lcom/google/android/gms/car/qv;->k:Lcom/google/android/gms/car/qq;

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v0, v2, v0

    iput v0, v1, Lcom/google/android/gms/car/qq;->b:F

    .line 822
    iget-object v0, p0, Lcom/google/android/gms/car/qv;->k:Lcom/google/android/gms/car/qq;

    return-object v0
.end method
