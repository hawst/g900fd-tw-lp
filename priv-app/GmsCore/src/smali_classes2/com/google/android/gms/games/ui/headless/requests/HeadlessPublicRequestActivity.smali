.class public final Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestActivity;
.super Lcom/google/android/gms/games/ui/headless/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/b/a/s;
.implements Lcom/google/android/gms/games/ui/common/requests/c;
.implements Lcom/google/android/gms/games/ui/common/requests/m;


# static fields
.field private static final i:I

.field private static final j:I


# instance fields
.field private k:Lcom/google/android/gms/games/ui/headless/requests/a;

.field private l:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

.field private m:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    sget v0, Lcom/google/android/gms/l;->by:I

    sput v0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestActivity;->i:I

    .line 29
    sget v0, Lcom/google/android/gms/m;->m:I

    sput v0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestActivity;->j:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    sget v0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestActivity;->i:I

    sget v1, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestActivity;->j:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/headless/a;-><init>(II)V

    .line 38
    return-void
.end method


# virtual methods
.method public final O_()Lcom/google/android/gms/games/ui/common/requests/l;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestActivity;->k:Lcom/google/android/gms/games/ui/headless/requests/a;

    return-object v0
.end method

.method public final P_()Lcom/google/android/gms/games/ui/b/a/r;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestActivity;->k:Lcom/google/android/gms/games/ui/headless/requests/a;

    return-object v0
.end method

.method public final T()Lcom/google/android/gms/games/internal/request/GameRequestCluster;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestActivity;->l:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    return-object v0
.end method

.method public final U()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestActivity;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/headless/a;->onCreate(Landroid/os/Bundle;)V

    .line 43
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    :goto_0
    return-void

    .line 47
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.GAME_REQUEST_CLUSTER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestActivity;->l:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    .line 49
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestActivity;->m:Ljava/lang/String;

    .line 51
    new-instance v0, Lcom/google/android/gms/games/ui/headless/requests/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/headless/requests/a;-><init>(Lcom/google/android/gms/games/ui/headless/a;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestActivity;->k:Lcom/google/android/gms/games/ui/headless/requests/a;

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestActivity;->l:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->i()I

    move-result v0

    .line 54
    packed-switch v0, :pswitch_data_0

    .line 64
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid request type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 56
    :pswitch_0
    sget v0, Lcom/google/android/gms/p;->kO:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestActivity;->setTitle(I)V

    .line 66
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestActivity;->l:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->g()Lcom/google/android/gms/games/Player;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->v_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestActivity;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 60
    :pswitch_1
    sget v0, Lcom/google/android/gms/p;->kQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessPublicRequestActivity;->setTitle(I)V

    goto :goto_1

    .line 54
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
