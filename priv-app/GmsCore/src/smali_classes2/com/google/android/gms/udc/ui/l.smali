.class final Lcom/google/android/gms/udc/ui/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/av;


# instance fields
.field final synthetic a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)V
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, Lcom/google/android/gms/udc/ui/l;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/udc/ui/UdcConsentActivity;B)V
    .locals 0

    .prologue
    .line 256
    invoke-direct {p0, p1}, Lcom/google/android/gms/udc/ui/l;-><init>(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 4

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/l;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-static {v0}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->a(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/udc/util/j;

    move-result-object v0

    const v1, 0x1020002

    new-instance v2, Lcom/google/android/gms/udc/ui/u;

    invoke-direct {v2}, Lcom/google/android/gms/udc/ui/u;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/gms/udc/ui/u;->b()Lcom/google/android/gms/udc/ui/j;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/udc/util/j;->a(ILandroid/support/v4/app/Fragment;)V

    .line 264
    new-instance v0, Lcom/google/android/gms/udc/c/a;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/l;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/l;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-static {v2}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->b(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/common/api/v;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/udc/ui/l;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-static {v3}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->c(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/udc/e/d;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/udc/c/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/udc/e/d;)V

    .line 266
    const-wide/16 v2, 0xa

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/gms/udc/c/a;->a(JLjava/util/concurrent/TimeUnit;)V

    .line 267
    return-object v0
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 0

    .prologue
    .line 284
    return-void
.end method

.method public final synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 7

    .prologue
    const v6, 0x1020002

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 256
    check-cast p2, Lcom/google/android/gms/udc/f;

    invoke-interface {p2}, Lcom/google/android/gms/udc/f;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Lcom/google/android/gms/udc/f;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/udc/e/e;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/l;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-static {v1, v0}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->a(Lcom/google/android/gms/udc/ui/UdcConsentActivity;Lcom/google/android/gms/udc/e/e;)Lcom/google/android/gms/udc/e/e;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/l;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-static {v1}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->d(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Landroid/util/Pair;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/l;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-static {v1}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->e(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/analytics/bv;

    move-result-object v4

    new-instance v5, Lcom/google/android/gms/analytics/bi;

    invoke-direct {v5}, Lcom/google/android/gms/analytics/bi;-><init>()V

    iget-object v1, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, [I

    invoke-static {v1}, Lcom/google/android/gms/udc/util/a;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v2, v1}, Lcom/google/android/gms/analytics/bi;->a(ILjava/lang/String;)Lcom/google/android/gms/analytics/bh;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/analytics/bi;

    const/4 v5, 0x2

    iget-object v2, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, [I

    invoke-static {v2}, Lcom/google/android/gms/udc/util/a;->a([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v5, v2}, Lcom/google/android/gms/analytics/bi;->a(ILjava/lang/String;)Lcom/google/android/gms/analytics/bh;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/analytics/bi;

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/gms/udc/ui/l;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-static {v3}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->c(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/udc/e/d;

    move-result-object v3

    iget v3, v3, Lcom/google/android/gms/udc/e/d;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/analytics/bi;->a(ILjava/lang/String;)Lcom/google/android/gms/analytics/bh;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/analytics/bi;

    invoke-virtual {v1}, Lcom/google/android/gms/analytics/bi;->a()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/google/android/gms/analytics/bv;->a(Ljava/util/Map;)V

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/l;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-static {v1}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->a(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/udc/util/j;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/l;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-static {v2}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->f(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/udc/ui/l;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-static {v3}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->g(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/udc/ConsentFlowConfig;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/udc/ui/l;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-static {v4}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->c(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/udc/e/d;

    move-result-object v4

    iget v4, v4, Lcom/google/android/gms/udc/e/d;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/udc/ui/l;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-static {v5}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->h(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Z

    move-result v5

    invoke-static {v2, v0, v3, v4, v5}, Lcom/google/android/gms/udc/ui/n;->a(Ljava/lang/String;Lcom/google/android/gms/udc/e/e;Lcom/google/android/gms/udc/ConsentFlowConfig;Ljava/lang/Integer;Z)Lcom/google/android/gms/udc/ui/n;

    move-result-object v0

    invoke-virtual {v1, v6, v0}, Lcom/google/android/gms/udc/util/j;->a(ILandroid/support/v4/app/Fragment;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p2}, Lcom/google/android/gms/udc/f;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v3

    const-string v1, "UdcConsent"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Error reading the config data:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Status;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const-string v1, "UdcConsent"

    const-string v4, "Unknown statuscode:%d"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v0

    invoke-static {v4, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/l;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    sget v2, Lcom/google/android/gms/p;->yU:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/udc/ui/l;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-static {v2}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->e(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/analytics/bv;

    move-result-object v2

    const-string v4, "LoadOverviewConfig"

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v3

    invoke-static {v2, v4, v3}, Lcom/google/android/gms/udc/util/a;->a(Lcom/google/android/gms/analytics/bv;Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/l;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-static {v2}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->a(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/udc/util/j;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/udc/ui/g;

    invoke-direct {v3}, Lcom/google/android/gms/udc/ui/g;-><init>()V

    iget-object v4, p0, Lcom/google/android/gms/udc/ui/l;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    sget v5, Lcom/google/android/gms/p;->yR:I

    invoke-virtual {v4, v5}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/udc/ui/g;->a(Ljava/lang/CharSequence;)Lcom/google/android/gms/udc/ui/g;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/gms/udc/ui/g;->b(Ljava/lang/CharSequence;)Lcom/google/android/gms/udc/ui/g;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/udc/ui/g;->a(Z)Lcom/google/android/gms/udc/ui/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/udc/ui/g;->b()Lcom/google/android/gms/udc/ui/j;

    move-result-object v0

    invoke-virtual {v2, v6, v0}, Lcom/google/android/gms/udc/util/j;->a(ILandroid/support/v4/app/Fragment;)V

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/l;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    sget v1, Lcom/google/android/gms/p;->zd:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    move v0, v2

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/l;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    sget v1, Lcom/google/android/gms/p;->yY:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    move v0, v2

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/l;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    sget v1, Lcom/google/android/gms/p;->yO:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    move v0, v2

    goto :goto_1

    :pswitch_3
    iget-object v1, p0, Lcom/google/android/gms/udc/ui/l;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    sget v2, Lcom/google/android/gms/p;->yU:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1196
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
