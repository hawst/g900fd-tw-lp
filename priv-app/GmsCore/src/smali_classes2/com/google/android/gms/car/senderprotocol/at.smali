.class public final Lcom/google/android/gms/car/senderprotocol/at;
.super Lcom/google/android/gms/car/senderprotocol/az;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/car/senderprotocol/au;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/fu;Lcom/google/android/gms/car/gx;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/senderprotocol/az;-><init>(Lcom/google/android/gms/car/gw;Lcom/google/android/gms/car/gx;)V

    .line 85
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/at;->a:Lcom/google/android/gms/car/senderprotocol/au;

    .line 86
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 59
    new-instance v0, Lcom/google/android/c/b/bn;

    invoke-direct {v0}, Lcom/google/android/c/b/bn;-><init>()V

    .line 60
    iput p1, v0, Lcom/google/android/c/b/bn;->a:I

    .line 61
    iput p2, v0, Lcom/google/android/c/b/bn;->b:I

    .line 62
    const v1, 0x8005

    invoke-static {v0}, Lcom/google/android/c/b/bn;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/at;->a(I[B)V

    .line 64
    return-void
.end method

.method public final a(ILjava/lang/String;II[BI)V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lcom/google/android/c/b/bo;

    invoke-direct {v0}, Lcom/google/android/c/b/bo;-><init>()V

    .line 43
    invoke-virtual {v0, p1}, Lcom/google/android/c/b/bo;->b(I)Lcom/google/android/c/b/bo;

    .line 44
    if-nez p2, :cond_1

    .line 45
    const-string v1, ""

    iput-object v1, v0, Lcom/google/android/c/b/bo;->a:Ljava/lang/String;

    .line 49
    :goto_0
    invoke-virtual {v0, p3}, Lcom/google/android/c/b/bo;->d(I)Lcom/google/android/c/b/bo;

    .line 50
    invoke-virtual {v0, p4}, Lcom/google/android/c/b/bo;->c(I)Lcom/google/android/c/b/bo;

    .line 51
    invoke-virtual {v0, p6}, Lcom/google/android/c/b/bo;->a(I)Lcom/google/android/c/b/bo;

    .line 52
    if-eqz p5, :cond_0

    .line 53
    invoke-virtual {v0, p5}, Lcom/google/android/c/b/bo;->a([B)Lcom/google/android/c/b/bo;

    .line 55
    :cond_0
    const v1, 0x8004

    invoke-static {v0}, Lcom/google/android/c/b/bo;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/at;->a(I[B)V

    .line 56
    return-void

    .line 47
    :cond_1
    iput-object p2, v0, Lcom/google/android/c/b/bo;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final a(ILjava/nio/ByteBuffer;)V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/at;->a:Lcom/google/android/gms/car/senderprotocol/au;

    if-nez v0, :cond_0

    .line 76
    :goto_0
    return-void

    .line 71
    :cond_0
    const v0, 0x8001

    if-ne p1, v0, :cond_1

    .line 72
    new-instance v0, Lcom/google/android/c/b/bs;

    invoke-direct {v0}, Lcom/google/android/c/b/bs;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/at;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/at;->a:Lcom/google/android/gms/car/senderprotocol/au;

    invoke-interface {v0}, Lcom/google/android/gms/car/senderprotocol/au;->a()V

    goto :goto_0

    .line 74
    :cond_1
    const v0, 0x8002

    if-ne p1, v0, :cond_2

    .line 75
    new-instance v0, Lcom/google/android/c/b/bt;

    invoke-direct {v0}, Lcom/google/android/c/b/bt;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/at;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/at;->a:Lcom/google/android/gms/car/senderprotocol/au;

    invoke-interface {v0}, Lcom/google/android/gms/car/senderprotocol/au;->b()V

    goto :goto_0

    .line 78
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid message type;"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/c/b/bp;

    invoke-direct {v0}, Lcom/google/android/c/b/bp;-><init>()V

    .line 36
    iput p1, v0, Lcom/google/android/c/b/bp;->a:I

    .line 37
    const v1, 0x8003

    invoke-static {v0}, Lcom/google/android/c/b/bp;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/at;->a(I[B)V

    .line 38
    return-void
.end method
