.class final Lcom/google/android/gms/icing/impl/i;
.super Lcom/google/android/gms/icing/impl/j;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:I

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Z

.field final synthetic e:Lcom/google/android/gms/icing/impl/d;

.field final synthetic f:J

.field final synthetic g:Lcom/google/android/gms/icing/impl/e;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/impl/e;Ljava/lang/String;ILjava/lang/String;ZLcom/google/android/gms/icing/impl/d;J)V
    .locals 1

    .prologue
    .line 255
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/i;->g:Lcom/google/android/gms/icing/impl/e;

    iput-object p2, p0, Lcom/google/android/gms/icing/impl/i;->a:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/gms/icing/impl/i;->b:I

    iput-object p4, p0, Lcom/google/android/gms/icing/impl/i;->c:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/google/android/gms/icing/impl/i;->d:Z

    iput-object p6, p0, Lcom/google/android/gms/icing/impl/i;->e:Lcom/google/android/gms/icing/impl/d;

    iput-wide p7, p0, Lcom/google/android/gms/icing/impl/i;->f:J

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/j;-><init>(Lcom/google/android/gms/icing/impl/e;)V

    return-void
.end method

.method private c()Ljava/lang/Void;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 258
    .line 261
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/i;->g:Lcom/google/android/gms/icing/impl/e;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/i;->a:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/gms/icing/impl/i;->b:I

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/i;->c:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/gms/icing/impl/i;->d:Z

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/i;->e:Lcom/google/android/gms/icing/impl/d;

    iget-wide v6, p0, Lcom/google/android/gms/icing/impl/i;->f:J

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/icing/impl/e;->b(Ljava/lang/String;ILjava/lang/String;ZLcom/google/android/gms/icing/impl/d;J)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 263
    packed-switch v0, :pswitch_data_0

    :goto_0
    move v0, v8

    .line 272
    :goto_1
    if-nez v0, :cond_0

    .line 278
    const-string v0, "Indexing done %s"

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/i;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;Ljava/lang/Object;)I

    .line 279
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/i;->g:Lcom/google/android/gms/icing/impl/e;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/e;->a()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/i;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 281
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/i;->e:Lcom/google/android/gms/icing/impl/d;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/d;->d()V
    :try_end_1
    .catch Lcom/google/android/gms/icing/impl/bi; {:try_start_1 .. :try_end_1} :catch_0

    .line 288
    :cond_0
    :goto_2
    const/4 v0, 0x0

    return-object v0

    .line 266
    :pswitch_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/i;->g:Lcom/google/android/gms/icing/impl/e;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/i;->a:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/gms/icing/impl/i;->b:I

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/i;->c:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/gms/icing/impl/i;->d:Z

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/i;->e:Lcom/google/android/gms/icing/impl/d;

    iget-wide v6, p0, Lcom/google/android/gms/icing/impl/i;->f:J

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/icing/impl/e;->a(Ljava/lang/String;ILjava/lang/String;ZLcom/google/android/gms/icing/impl/d;J)V

    .line 268
    const/4 v0, 0x1

    .line 269
    goto :goto_1

    .line 271
    :pswitch_1
    const-string v0, "Aborting indexing of corpus %s"

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/i;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 277
    :catchall_0
    move-exception v0

    .line 278
    const-string v1, "Indexing done %s"

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/i;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;Ljava/lang/Object;)I

    .line 279
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/i;->g:Lcom/google/android/gms/icing/impl/e;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/e;->a()Ljava/util/Set;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/i;->c:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 281
    :try_start_3
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/i;->e:Lcom/google/android/gms/icing/impl/d;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/d;->d()V
    :try_end_3
    .catch Lcom/google/android/gms/icing/impl/bi; {:try_start_3 .. :try_end_3} :catch_1

    .line 285
    :goto_3
    throw v0

    .line 282
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/bi;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    const-string v1, "Cursor close call threw an exception"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 284
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/i;->g:Lcom/google/android/gms/icing/impl/e;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/e;->c:Lcom/google/android/gms/icing/impl/m;

    const-string v1, "cursor_close_exception"

    invoke-interface {v0, v1}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 282
    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/bi;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    const-string v2, "Cursor close call threw an exception"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 284
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/i;->g:Lcom/google/android/gms/icing/impl/e;

    iget-object v1, v1, Lcom/google/android/gms/icing/impl/e;->c:Lcom/google/android/gms/icing/impl/m;

    const-string v2, "cursor_close_exception"

    invoke-interface {v1, v2}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    goto :goto_3

    .line 263
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 255
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/i;->c()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
