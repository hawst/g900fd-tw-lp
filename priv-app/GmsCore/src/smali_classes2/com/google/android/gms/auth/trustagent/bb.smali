.class public final Lcom/google/android/gms/auth/trustagent/bb;
.super Lcom/google/android/gms/auth/trustagent/a;
.source "SourceFile"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private c:Landroid/widget/RelativeLayout;

.field private d:Landroid/util/DisplayMetrics;

.field private e:I

.field private f:Landroid/widget/Button;

.field private g:Landroid/preference/PreferenceScreen;

.field private h:Lcom/google/android/gms/auth/trustagent/am;

.field private i:Lcom/google/android/gms/auth/trustagent/trustlet/an;

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/a;-><init>()V

    .line 454
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/bb;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/bb;->d()V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 338
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->g:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 339
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->g:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    .line 340
    invoke-virtual {v1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 341
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->g:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 345
    :cond_0
    return-void

    .line 338
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 368
    iget-boolean v1, p0, Lcom/google/android/gms/auth/trustagent/a;->b:Z

    if-nez v1, :cond_2

    .line 369
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->j:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 370
    const-string v1, "Coffee - TrustedDevicesFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "A device was added to the trusted device list while another is pending, the old one "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/auth/trustagent/bb;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " will not be added."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/bb;->j:Ljava/lang/String;

    .line 430
    :cond_1
    :goto_0
    return v0

    .line 377
    :cond_2
    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/f;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 379
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "auth_trust_agent_pref_trusted_bluetooth_title"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 381
    const/4 v1, 0x0

    .line 382
    iget-object v5, p0, Lcom/google/android/gms/auth/trustagent/bb;->g:Landroid/preference/PreferenceScreen;

    invoke-virtual {v5, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    if-nez v5, :cond_1

    .line 387
    new-instance v5, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bb;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 388
    invoke-virtual {v5, v3}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 389
    invoke-virtual {v5, v0}, Landroid/preference/Preference;->setPersistent(Z)V

    .line 390
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v6

    invoke-virtual {v6}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v6

    const/16 v7, 0xc

    if-ne v6, v7, :cond_5

    .line 391
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    .line 393
    if-nez v1, :cond_3

    .line 394
    const-string v1, "Coffee - TrustedDevicesFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to add Bluetooth device as trusted device, invalid Bluetooth address specified: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 398
    :cond_3
    invoke-static {v1}, Lcom/google/android/gms/auth/trustagent/f;->a(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    move-result-object v0

    .line 399
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 404
    :goto_1
    invoke-virtual {v5, p1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 405
    invoke-static {v1}, Lcom/google/android/gms/auth/trustagent/f;->b(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 406
    sget v0, Lcom/google/android/gms/p;->cJ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/bb;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 426
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->g:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v5}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 427
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {v0, v3, v2}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Z)V

    .line 428
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {v0, v4, p1}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/bb;->g()V

    move v0, v2

    .line 430
    goto/16 :goto_0

    .line 409
    :cond_4
    sget v0, Lcom/google/android/gms/p;->cL:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/bb;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 413
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/am;->e:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 414
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 415
    const-string v7, "auth_trust_agent_pref_trusted_bluetooth_title"

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    const/16 v7, 0x2d

    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 417
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 418
    invoke-virtual {v5, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 422
    :goto_3
    sget v1, Lcom/google/android/gms/p;->cU:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/trustagent/bb;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    move-object p1, v0

    goto :goto_2

    :cond_7
    move-object v0, v1

    goto :goto_3

    :cond_8
    move-object p1, v0

    goto :goto_1
.end method

.method private d()V
    .locals 3

    .prologue
    .line 231
    sget-object v0, Lcom/google/android/gms/auth/b/a;->W:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/bb;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 233
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bb;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/auth/trustagent/TrustedDeviceSelectionActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 234
    const-string v1, "bluetooth_addresses_to_exclude"

    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/bb;->e()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 236
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bb;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x3e9

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 249
    :cond_0
    :goto_0
    return-void

    .line 238
    :cond_1
    sget-object v0, Lcom/google/android/gms/auth/b/a;->W:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 239
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bb;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/auth/trustagent/BluetoothDeviceSelectionActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 240
    const-string v1, "bluetooth_addresses_to_exclude"

    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/bb;->e()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 242
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bb;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x3ea

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 244
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/bb;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bb;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/auth/trustagent/NfcDeviceSelectionActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 246
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bb;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x3eb

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private e()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 252
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 253
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/bb;->g:Landroid/preference/PreferenceScreen;

    invoke-virtual {v2}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 254
    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/bb;->g:Landroid/preference/PreferenceScreen;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    .line 255
    const-string v3, "auth_trust_agent_pref_trusted_bluetooth_address"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 256
    const/16 v3, 0x2f

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 253
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 260
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method private f()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 264
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->g:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 266
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/am;->e:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 267
    if-nez v2, :cond_0

    .line 302
    :goto_0
    return-void

    .line 271
    :cond_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/gms/auth/b/a;->W:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    .line 272
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 273
    const-string v3, "auth_trust_agent_pref_trusted_bluetooth_address"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 274
    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 276
    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/trustagent/bb;->c(Ljava/lang/String;)Z

    goto :goto_2

    :cond_2
    move v0, v1

    .line 271
    goto :goto_1

    .line 281
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/bb;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 282
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->i:Lcom/google/android/gms/auth/trustagent/trustlet/an;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/an;->a:Lcom/google/android/gms/auth/trustagent/trustlet/af;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;

    .line 283
    new-instance v3, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bb;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 284
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "auth_trust_agent_pref_trusted_nfc_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 286
    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 287
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->g:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_3

    .line 291
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->g:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    if-eqz v0, :cond_5

    .line 292
    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bb;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 293
    const-string v2, "auth_trust_agent_pref_trusted_devices_add_trusted_device_key"

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 294
    sget v2, Lcom/google/android/gms/p;->cI:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/trustagent/bb;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 296
    sget v2, Lcom/google/android/gms/h;->a:I

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setIcon(I)V

    .line 297
    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setPersistent(Z)V

    .line 298
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->g:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 301
    :cond_5
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/bb;->g()V

    goto/16 :goto_0
.end method

.method private g()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 305
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->g:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 307
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->f:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 312
    :goto_0
    return-void

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 310
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->f:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method private h()Z
    .locals 1

    .prologue
    .line 445
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bb;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/auth/b/a;->X:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 315
    const-string v0, "auth_trust_agent_pref_trusted_bluetooth_address"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 316
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/trustagent/bb;->b(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->h:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/f;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/trustagent/am;->c(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->h:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/f;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/trustagent/am;->c(Ljava/lang/String;)V

    .line 320
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/bb;->f()V

    .line 321
    return-void

    .line 317
    :cond_1
    const-string v0, "auth_trust_agent_pref_trusted_nfc_"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 318
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/trustagent/bb;->b(Ljava/lang/String;)V

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->i:Lcom/google/android/gms/auth/trustagent/trustlet/an;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/an;->a(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 348
    const-string v1, "com.google.android.gms.auth.trustagent.ADD_DEVICE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 364
    :cond_0
    :goto_0
    return v0

    .line 351
    :cond_1
    const-string v1, "bluetooth_device_address"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 354
    if-nez v1, :cond_2

    .line 355
    const-string v1, "Coffee - TrustedDevicesFragment"

    const-string v2, "Invalid intent to add Bluetooth device as trusted device, no address specified."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 360
    :cond_2
    invoke-direct {p0, v1}, Lcom/google/android/gms/auth/trustagent/bb;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 361
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/bb;->g()V

    .line 362
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->h:Lcom/google/android/gms/auth/trustagent/am;

    .line 183
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->i:Lcom/google/android/gms/auth/trustagent/trustlet/an;

    if-nez v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a(Lcom/google/android/gms/auth/trustagent/am;)Lcom/google/android/gms/auth/trustagent/trustlet/af;

    move-result-object v0

    .line 186
    new-instance v1, Lcom/google/android/gms/auth/trustagent/trustlet/an;

    invoke-direct {v1, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/an;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/af;)V

    iput-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->i:Lcom/google/android/gms/auth/trustagent/trustlet/an;

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->j:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->j:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/trustagent/bb;->c(Ljava/lang/String;)Z

    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->j:Ljava/lang/String;

    .line 192
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/bb;->f()V

    .line 193
    return-void
.end method

.method public final b(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 434
    const-string v0, "SELECTED_NFC_DEVICE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;

    .line 435
    if-eqz v0, :cond_0

    .line 436
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->i:Lcom/google/android/gms/auth/trustagent/trustlet/an;

    iget-object v2, v1, Lcom/google/android/gms/auth/trustagent/trustlet/an;->a:Lcom/google/android/gms/auth/trustagent/trustlet/af;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/an;->a(Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;)Z

    .line 438
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/trustagent/a;->onCreate(Landroid/os/Bundle;)V

    .line 95
    sget v0, Lcom/google/android/gms/s;->g:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/bb;->addPreferencesFromResource(I)V

    .line 96
    const-string v0, "auth_trust_agent_pref_trusted_devices_list_key"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/bb;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->g:Landroid/preference/PreferenceScreen;

    .line 98
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bb;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->G:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setContentView(I)V

    .line 99
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bb;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 100
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->d:Landroid/util/DisplayMetrics;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bb;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->d:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->d:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->e:I

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bb;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->tn:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->c:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->c:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bb;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->to:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bb;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/google/android/gms/j;->tm:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget-object v2, Lcom/google/android/gms/auth/b/a;->W:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/google/android/gms/auth/b/a;->X:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    sget v2, Lcom/google/android/gms/p;->cS:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/trustagent/bb;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v0, Lcom/google/android/gms/p;->cR:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/bb;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    sget-object v2, Lcom/google/android/gms/auth/b/a;->W:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/google/android/gms/auth/b/a;->X:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    sget v2, Lcom/google/android/gms/p;->cZ:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/trustagent/bb;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v0, Lcom/google/android/gms/p;->cY:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/bb;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, -0x2

    const/4 v2, 0x0

    .line 138
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/auth/trustagent/a;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 140
    new-instance v1, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bb;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->f:Landroid/widget/Button;

    .line 141
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->f:Landroid/widget/Button;

    sget v3, Lcom/google/android/gms/p;->cr:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/trustagent/bb;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->f:Landroid/widget/Button;

    const/high16 v3, 0x41600000    # 14.0f

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setTextSize(F)V

    .line 143
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->f:Landroid/widget/Button;

    const/4 v3, -0x1

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setTextColor(I)V

    .line 144
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->f:Landroid/widget/Button;

    const v3, -0xff8e9a

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setBackgroundColor(I)V

    .line 146
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->d:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v1, v1

    iget-object v3, p0, Lcom/google/android/gms/auth/trustagent/bb;->d:Landroid/util/DisplayMetrics;

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v1, v3

    const/high16 v3, 0x44160000    # 600.0f

    cmpl-float v1, v1, v3

    if-ltz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_1

    .line 147
    iget v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->e:I

    mul-int/lit8 v1, v1, 0x14

    iget v3, p0, Lcom/google/android/gms/auth/trustagent/bb;->e:I

    mul-int/lit8 v3, v3, 0xf

    invoke-virtual {v0, v2, v2, v1, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 160
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->f:Landroid/widget/Button;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 162
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->f:Landroid/widget/Button;

    sget v2, Lcom/google/android/gms/p;->cs:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/trustagent/bb;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 164
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 165
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->f:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 168
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->f:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 170
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->f:Landroid/widget/Button;

    new-instance v2, Lcom/google/android/gms/auth/trustagent/bc;

    invoke-direct {v2, p0}, Lcom/google/android/gms/auth/trustagent/bc;-><init>(Lcom/google/android/gms/auth/trustagent/bb;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    return-object v0

    :cond_0
    move v1, v2

    .line 146
    goto :goto_0

    .line 150
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->d:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v1, v1

    iget-object v3, p0, Lcom/google/android/gms/auth/trustagent/bb;->d:Landroid/util/DisplayMetrics;

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v1, v3

    const/high16 v3, 0x43c80000    # 400.0f

    sub-float/2addr v1, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v1, v3

    float-to-int v1, v1

    iget v3, p0, Lcom/google/android/gms/auth/trustagent/bb;->e:I

    mul-int/2addr v3, v1

    .line 153
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bb;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v4, Lcom/google/android/gms/j;->tp:I

    invoke-virtual {v1, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 155
    invoke-virtual {v1, v3, v2, v3, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 156
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bb;->f:Landroid/widget/Button;

    iget v4, p0, Lcom/google/android/gms/auth/trustagent/bb;->e:I

    mul-int/lit8 v4, v4, 0x46

    add-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/gms/auth/trustagent/bb;->e:I

    mul-int/lit16 v4, v4, 0x96

    invoke-virtual {v1, v2, v2, v3, v4}, Landroid/widget/Button;->setPadding(IIII)V

    goto :goto_1
.end method

.method public final onPause()V
    .locals 3

    .prologue
    .line 197
    invoke-super {p0}, Lcom/google/android/gms/auth/trustagent/a;->onPause()V

    .line 198
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 199
    const-string v0, "Coffee - TrustedDevicesFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Pending device to add to trusted device, ignored. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/bb;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/bb;->j:Ljava/lang/String;

    .line 203
    :cond_0
    return-void
.end method

.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x0

    return v0
.end method

.method public final onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    .line 207
    const-string v0, "auth_trust_agent_pref_trusted_devices_add_trusted_device_key"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/bb;->d()V

    .line 214
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 210
    :cond_0
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/bd;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/trustagent/bd;

    move-result-object v0

    .line 212
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bb;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "Coffee - TrustedDevicesFragment"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/trustagent/bd;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method
