.class final Lcom/google/android/gms/common/account/d;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 174
    const v0, 0x1090003

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 175
    iput-object p2, p0, Lcom/google/android/gms/common/account/d;->b:Ljava/util/ArrayList;

    .line 176
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/gms/common/account/d;->a:Landroid/view/LayoutInflater;

    .line 178
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 184
    if-nez p2, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/common/account/d;->a:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->Z:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 187
    new-instance v1, Lcom/google/android/gms/common/account/f;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lcom/google/android/gms/common/account/f;-><init>(B)V

    .line 188
    sget v0, Lcom/google/android/gms/j;->h:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/android/gms/common/account/f;->b:Landroid/widget/TextView;

    .line 189
    sget v0, Lcom/google/android/gms/j;->g:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/android/gms/common/account/f;->a:Landroid/widget/ImageView;

    .line 190
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 195
    :goto_0
    iget-object v2, v1, Lcom/google/android/gms/common/account/f;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/common/account/d;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/account/e;

    iget-object v0, v0, Lcom/google/android/gms/common/account/e;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    iget-object v1, v1, Lcom/google/android/gms/common/account/f;->a:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/common/account/d;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/account/e;

    iget-object v0, v0, Lcom/google/android/gms/common/account/e;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 198
    return-object p2

    .line 192
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/account/f;

    move-object v1, v0

    goto :goto_0
.end method
