.class public final Lcom/google/android/gms/car/gn;
.super Lcom/google/android/gms/car/mv;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/gw;
.implements Lcom/google/android/gms/car/senderprotocol/bb;


# static fields
.field private static final m:Lcom/google/android/gms/car/CarSensorEvent;


# instance fields
.field private final a:Ljava/util/concurrent/locks/ReentrantLock;

.field private final b:Ljava/util/LinkedList;

.field private final c:Landroid/util/SparseArray;

.field private final d:Landroid/util/SparseArray;

.field private final e:Landroid/util/SparseArray;

.field private f:J

.field private g:Lcom/google/android/gms/car/senderprotocol/ba;

.field private h:[I

.field private i:[I

.field private final j:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private k:Lcom/google/android/gms/car/ij;

.field private final l:Lcom/google/android/gms/car/gx;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 72
    new-instance v0, Lcom/google/android/gms/car/CarSensorEvent;

    const/16 v1, 0xb

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/CarSensorEvent;-><init>(IJII)V

    .line 75
    sput-object v0, Lcom/google/android/gms/car/gn;->m:Lcom/google/android/gms/car/CarSensorEvent;

    iget-object v0, v0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    const/16 v1, 0x1f

    aput-byte v1, v0, v4

    .line 77
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/car/gx;)V
    .locals 2

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/gms/car/mv;-><init>()V

    .line 45
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    .line 47
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/gn;->b:Ljava/util/LinkedList;

    .line 50
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/gn;->c:Landroid/util/SparseArray;

    .line 53
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/gn;->d:Landroid/util/SparseArray;

    .line 56
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/gn;->e:Landroid/util/SparseArray;

    .line 58
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/gms/car/gn;->f:J

    .line 64
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/gms/car/gn;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 80
    iput-object p1, p0, Lcom/google/android/gms/car/gn;->l:Lcom/google/android/gms/car/gx;

    .line 81
    return-void
.end method

.method private a(Lcom/google/android/gms/car/na;)Lcom/google/android/gms/car/gp;
    .locals 4

    .prologue
    .line 612
    invoke-interface {p1}, Lcom/google/android/gms/car/na;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    .line 613
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/gp;

    .line 614
    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/gp;->a(Landroid/os/IBinder;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 618
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/car/gn;Lcom/google/android/gms/car/gp;)V
    .locals 5

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/car/gp;->b()[I

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    invoke-virtual {p1}, Lcom/google/android/gms/car/gp;->c()Lcom/google/android/gms/car/na;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gms/car/gn;->a(ILcom/google/android/gms/car/na;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private a(Lcom/google/android/gms/car/gs;II)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 314
    const-string v2, "CAR.SENSOR"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 315
    const-string v2, "CAR.SENSOR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "startSensor "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with rate "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/car/gn;->g:Lcom/google/android/gms/car/senderprotocol/ba;

    .line 318
    if-eqz v2, :cond_3

    .line 319
    invoke-virtual {v2}, Lcom/google/android/gms/car/senderprotocol/ba;->b()Z

    move-result v3

    if-nez v3, :cond_1

    .line 320
    const-string v1, "CAR.SENSOR"

    const-string v2, "Sensor channel not available."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    :goto_0
    return v0

    .line 323
    :cond_1
    iget-boolean v3, p1, Lcom/google/android/gms/car/gs;->b:Z

    if-eqz v3, :cond_2

    move v0, v1

    .line 324
    goto :goto_0

    .line 326
    :cond_2
    invoke-virtual {v2, p2}, Lcom/google/android/gms/car/senderprotocol/ba;->b(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 327
    iput-boolean v1, p1, Lcom/google/android/gms/car/gs;->b:Z

    move v0, v1

    .line 328
    goto :goto_0

    .line 331
    :cond_3
    const-string v1, "CAR.SENSOR"

    const-string v2, "requestSensorStart failed"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/car/CarSensorEvent;)V
    .locals 4

    .prologue
    const/4 v2, 0x4

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->d:Landroid/util/SparseArray;

    iget v1, p1, Lcom/google/android/gms/car/CarSensorEvent;->b:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/gs;

    .line 154
    if-eqz v0, :cond_2

    .line 155
    iput-object p1, v0, Lcom/google/android/gms/car/gs;->a:Lcom/google/android/gms/car/CarSensorEvent;

    .line 161
    :cond_0
    :goto_0
    iget v0, p1, Lcom/google/android/gms/car/CarSensorEvent;->b:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_3

    const-string v0, "CAR.SENSOR"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 163
    const-string v0, "CAR.SENSOR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "driving status change "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/car/CarSensorEvent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    :cond_1
    :goto_1
    iget v1, p1, Lcom/google/android/gms/car/CarSensorEvent;->b:I

    iget-object v0, p0, Lcom/google/android/gms/car/gn;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/gr;

    if-eqz v0, :cond_5

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/gr;->a(Lcom/google/android/gms/car/CarSensorEvent;)V

    .line 171
    :goto_2
    return-void

    .line 157
    :cond_2
    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    const-string v0, "CAR.SENSOR"

    const-string v1, "sensor data received without matching record"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 164
    :cond_3
    iget v0, p1, Lcom/google/android/gms/car/CarSensorEvent;->b:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_4

    const-string v0, "CAR.SENSOR"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 166
    const-string v0, "CAR.SENSOR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Day/night mode change: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/car/CarSensorEvent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 167
    :cond_4
    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 168
    const-string v0, "CAR.SENSOR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSensorData type:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Lcom/google/android/gms/car/CarSensorEvent;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 170
    :cond_5
    const-string v0, "CAR.SENSOR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sensor event while no listener, sensor:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private b(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 586
    iget-object v1, p0, Lcom/google/android/gms/car/gn;->h:[I

    if-eqz v1, :cond_0

    .line 587
    iget-object v2, p0, Lcom/google/android/gms/car/gn;->h:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    .line 588
    if-ne v4, p1, :cond_1

    .line 589
    const/4 v0, 0x1

    .line 593
    :cond_0
    return v0

    .line 587
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 137
    :cond_0
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 517
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 518
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotConnected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 520
    :cond_0
    return-void
.end method

.method private h()V
    .locals 10

    .prologue
    .line 523
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/32 v2, 0xea60

    sub-long v2, v0, v2

    .line 524
    iget-wide v0, p0, Lcom/google/android/gms/car/gn;->f:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 557
    :cond_0
    :goto_0
    return-void

    .line 528
    :cond_1
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/gms/car/gn;->f:J

    .line 529
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 530
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v5

    .line 531
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_3

    .line 532
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v6

    .line 533
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/go;

    .line 534
    iget-wide v8, v0, Lcom/google/android/gms/car/go;->b:J

    cmp-long v7, v8, v2

    if-gtz v7, :cond_2

    .line 535
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 531
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 537
    :cond_2
    iget-wide v6, p0, Lcom/google/android/gms/car/gn;->f:J

    iget-wide v8, v0, Lcom/google/android/gms/car/go;->b:J

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/gms/car/gn;->f:J

    goto :goto_2

    .line 542
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 543
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 544
    iget-object v1, p0, Lcom/google/android/gms/car/gn;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/go;

    iget-object v1, v1, Lcom/google/android/gms/car/go;->a:Lcom/google/android/gms/car/mx;

    .line 545
    iget-object v3, p0, Lcom/google/android/gms/car/gn;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->remove(I)V

    .line 547
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/mx;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 548
    :catch_0
    move-exception v0

    .line 549
    const-string v1, "CAR.SENSOR"

    const/4 v3, 0x2

    invoke-static {v1, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 550
    const-string v1, "CAR.SENSOR"

    const-string v3, ""

    invoke-static {v1, v3, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 555
    :cond_5
    invoke-direct {p0}, Lcom/google/android/gms/car/gn;->i()[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/gn;->i:[I

    goto/16 :goto_0
.end method

.method private i()[I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 560
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 561
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->h:[I

    if-nez v0, :cond_0

    move v0, v1

    .line 562
    :goto_0
    add-int v2, v3, v0

    new-array v4, v2, [I

    move v2, v1

    .line 564
    :goto_1
    if-ge v2, v3, :cond_1

    .line 565
    iget-object v5, p0, Lcom/google/android/gms/car/gn;->e:Landroid/util/SparseArray;

    invoke-virtual {v5, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v5

    .line 566
    aput v5, v4, v2

    .line 564
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 561
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->h:[I

    array-length v0, v0

    goto :goto_0

    .line 569
    :cond_1
    if-lez v0, :cond_2

    .line 570
    iget-object v2, p0, Lcom/google/android/gms/car/gn;->h:[I

    invoke-static {v2, v1, v4, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 574
    :cond_2
    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    :goto_2
    if-ltz v0, :cond_4

    .line 575
    aget v2, v4, v0

    .line 576
    iget-object v3, p0, Lcom/google/android/gms/car/gn;->d:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_3

    .line 577
    new-instance v3, Lcom/google/android/gms/car/gs;

    invoke-direct {v3, v1}, Lcom/google/android/gms/car/gs;-><init>(B)V

    .line 578
    iget-object v5, p0, Lcom/google/android/gms/car/gn;->d:Landroid/util/SparseArray;

    invoke-virtual {v5, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 574
    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 582
    :cond_4
    return-object v4
.end method


# virtual methods
.method public final a(I)Lcom/google/android/gms/car/CarSensorEvent;
    .locals 2

    .prologue
    .line 432
    invoke-direct {p0}, Lcom/google/android/gms/car/gn;->g()V

    .line 433
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 436
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/gs;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 438
    iget-object v1, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 440
    if-eqz v0, :cond_0

    .line 441
    iget-object v0, v0, Lcom/google/android/gms/car/gs;->a:Lcom/google/android/gms/car/CarSensorEvent;

    .line 443
    :goto_0
    return-object v0

    .line 438
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 443
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 85
    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    const-string v0, "CAR.SENSOR"

    const-string v1, "onSensorsConnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 91
    :try_start_0
    new-instance v0, Lcom/google/android/gms/car/gs;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/gs;-><init>(B)V

    .line 92
    sget-object v1, Lcom/google/android/gms/car/gn;->m:Lcom/google/android/gms/car/CarSensorEvent;

    iput-object v1, v0, Lcom/google/android/gms/car/gs;->a:Lcom/google/android/gms/car/CarSensorEvent;

    .line 93
    iget-object v1, p0, Lcom/google/android/gms/car/gn;->d:Landroid/util/SparseArray;

    const/16 v2, 0xb

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 96
    return-void

    .line 95
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(ILcom/google/android/gms/car/mx;)V
    .locals 3

    .prologue
    .line 476
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->l:Lcom/google/android/gms/car/gx;

    iget-object v0, v0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/car/of;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 478
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "this method can\'t be called from non-Google apps"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 480
    :cond_0
    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 481
    const-string v0, "CAR.SENSOR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unregisterInjectedSensorSource "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 485
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/go;

    iget-object v0, v0, Lcom/google/android/gms/car/go;->a:Lcom/google/android/gms/car/mx;

    invoke-interface {v0}, Lcom/google/android/gms/car/mx;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {p2}, Lcom/google/android/gms/car/mx;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    if-ne v0, v1, :cond_2

    .line 486
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 487
    invoke-direct {p0}, Lcom/google/android/gms/car/gn;->i()[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/gn;->i:[I

    .line 489
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/car/gn;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 491
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 492
    return-void

    .line 491
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(ILcom/google/android/gms/car/na;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x3

    .line 337
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    .line 339
    const-string v0, "CAR.SENSOR"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 340
    const-string v0, "CAR.SENSOR"

    const-string v1, "unregister while sensor not available"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    :cond_0
    :goto_0
    return-void

    .line 348
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 350
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/gs;

    .line 351
    if-nez v0, :cond_3

    .line 353
    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 354
    const-string v0, "CAR.SENSOR"

    const-string v1, "unregister for unsupported sensor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 399
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 358
    :cond_3
    :try_start_1
    invoke-direct {p0, p2}, Lcom/google/android/gms/car/gn;->a(Lcom/google/android/gms/car/na;)Lcom/google/android/gms/car/gp;

    move-result-object v2

    .line 359
    if-nez v2, :cond_5

    .line 361
    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 362
    const-string v0, "CAR.SENSOR"

    const-string v1, "unregister for not existing client"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 399
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 366
    :cond_5
    :try_start_2
    invoke-virtual {v2, p1}, Lcom/google/android/gms/car/gp;->b(I)V

    .line 367
    invoke-virtual {v2}, Lcom/google/android/gms/car/gp;->a()I

    move-result v1

    if-nez v1, :cond_6

    .line 368
    invoke-virtual {v2}, Lcom/google/android/gms/car/gp;->d()V

    .line 369
    iget-object v1, p0, Lcom/google/android/gms/car/gn;->b:Ljava/util/LinkedList;

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 371
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/car/gn;->c:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/gr;

    .line 372
    if-nez v1, :cond_8

    .line 374
    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 375
    const-string v0, "CAR.SENSOR"

    const-string v1, "unregister for non-active sensor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 399
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 379
    :cond_8
    :try_start_3
    invoke-virtual {v1, v2}, Lcom/google/android/gms/car/gr;->a(Lcom/google/android/gms/car/gp;)Lcom/google/android/gms/car/gq;

    move-result-object v2

    .line 381
    if-nez v2, :cond_a

    .line 382
    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 383
    const-string v0, "CAR.SENSOR"

    const-string v1, "unregister for not registered sensor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 399
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_0

    .line 387
    :cond_a
    :try_start_4
    iget-object v3, v1, Lcom/google/android/gms/car/gr;->a:Ljava/util/LinkedList;

    invoke-virtual {v3, v2}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 388
    iget-object v2, v1, Lcom/google/android/gms/car/gr;->a:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-nez v2, :cond_d

    .line 389
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/gn;->b(I)Z

    move-result v1

    .line 390
    iget-object v2, p0, Lcom/google/android/gms/car/gn;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->remove(I)V

    move v2, v4

    move v3, v1

    move v1, v4

    .line 395
    :goto_1
    const-string v6, "CAR.SENSOR"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 396
    const-string v6, "CAR.SENSOR"

    const-string v7, "unregister succeeded"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 399
    :cond_b
    iget-object v6, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v6}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 401
    if-eqz v3, :cond_12

    .line 402
    const-string v1, "CAR.SENSOR"

    invoke-static {v1, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_c

    const-string v1, "CAR.SENSOR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "stopSensor "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    iget-object v1, p0, Lcom/google/android/gms/car/gn;->g:Lcom/google/android/gms/car/senderprotocol/ba;

    invoke-virtual {v1}, Lcom/google/android/gms/car/senderprotocol/ba;->b()Z

    move-result v2

    if-nez v2, :cond_10

    const-string v0, "CAR.SENSOR"

    const-string v1, "Sensor channel not available."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 391
    :cond_d
    :try_start_5
    iget-object v2, v1, Lcom/google/android/gms/car/gr;->a:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v5

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/car/gq;

    iget v2, v2, Lcom/google/android/gms/car/gq;->b:I

    if-ge v2, v3, :cond_14

    :goto_3
    move v3, v2

    goto :goto_2

    :cond_e
    iget v2, v1, Lcom/google/android/gms/car/gr;->b:I

    if-eq v2, v3, :cond_f

    iput v3, v1, Lcom/google/android/gms/car/gr;->b:I

    const/4 v2, 0x1

    :goto_4
    if-eqz v2, :cond_13

    .line 392
    iget v1, v1, Lcom/google/android/gms/car/gr;->b:I

    .line 393
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/gn;->b(I)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v2

    move v3, v4

    goto :goto_1

    :cond_f
    move v2, v4

    .line 391
    goto :goto_4

    .line 399
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 402
    :cond_10
    iget-boolean v2, v0, Lcom/google/android/gms/car/gs;->b:Z

    if-eqz v2, :cond_0

    iput-boolean v4, v0, Lcom/google/android/gms/car/gs;->b:Z

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/gms/car/gs;->a:Lcom/google/android/gms/car/CarSensorEvent;

    const-string v0, "CAR.SENSOR"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_11

    const-string v0, "CAR.SENSOR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "stopSensor requestStop "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_11
    invoke-virtual {v1, p1}, Lcom/google/android/gms/car/senderprotocol/ba;->c(I)V

    goto/16 :goto_0

    .line 403
    :cond_12
    if-eqz v2, :cond_0

    .line 404
    invoke-direct {p0, v0, p1, v1}, Lcom/google/android/gms/car/gn;->a(Lcom/google/android/gms/car/gs;II)Z

    goto/16 :goto_0

    :cond_13
    move v1, v4

    move v2, v4

    move v3, v4

    goto/16 :goto_1

    :cond_14
    move v2, v3

    goto :goto_3
.end method

.method public final a(Lcom/google/android/gms/car/CarSensorEvent;)V
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 182
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/car/gn;->h()V

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->e:Landroid/util/SparseArray;

    iget v1, p1, Lcom/google/android/gms/car/CarSensorEvent;->b:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 185
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/gn;->b(Lcom/google/android/gms/car/CarSensorEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 189
    return-void

    .line 188
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/car/senderprotocol/az;)V
    .locals 2

    .prologue
    .line 636
    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 637
    const-string v0, "CAR.SENSOR"

    const-string v1, "onEndpointReady()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 639
    :cond_0
    check-cast p1, Lcom/google/android/gms/car/senderprotocol/ba;

    iput-object p1, p0, Lcom/google/android/gms/car/gn;->g:Lcom/google/android/gms/car/senderprotocol/ba;

    .line 640
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 641
    new-instance v0, Lcom/google/android/gms/car/ij;

    iget-object v1, p0, Lcom/google/android/gms/car/gn;->l:Lcom/google/android/gms/car/gx;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/car/ij;-><init>(Lcom/google/android/gms/car/gn;Lcom/google/android/gms/car/gx;)V

    iput-object v0, p0, Lcom/google/android/gms/car/gn;->k:Lcom/google/android/gms/car/ij;

    .line 643
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->k:Lcom/google/android/gms/car/ij;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ij;->a()V

    .line 644
    return-void
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 861
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "supported sensors:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/gn;->i:[I

    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 862
    const-string v0, "**last events for sensors**"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 863
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->d:Landroid/util/SparseArray;

    if-eqz v0, :cond_5

    .line 865
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->d:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    move v2, v1

    .line 866
    :goto_0
    if-ge v2, v3, :cond_2

    .line 867
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    .line 868
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/gs;

    .line 869
    if-eqz v0, :cond_0

    iget-object v5, v0, Lcom/google/android/gms/car/gs;->a:Lcom/google/android/gms/car/CarSensorEvent;

    if-eqz v5, :cond_0

    .line 870
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "sensor: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " active: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, v0, Lcom/google/android/gms/car/gs;->b:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 873
    const/16 v5, 0xa

    if-eq v4, v5, :cond_0

    const/16 v5, 0xf

    if-eq v4, v5, :cond_0

    const/16 v5, 0x11

    if-eq v4, v5, :cond_0

    .line 876
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/car/gs;->a:Lcom/google/android/gms/car/CarSensorEvent;

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarSensorEvent;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 879
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/gr;

    .line 880
    if-eqz v0, :cond_1

    .line 881
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, " rate: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v0, Lcom/google/android/gms/car/gr;->b:I

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 866
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 885
    :catch_0
    move-exception v0

    const-string v0, "concurrent modification happened"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 890
    :cond_2
    :goto_1
    const-string v0, "**clients**"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 892
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/gp;
    :try_end_1
    .catch Ljava/util/ConcurrentModificationException; {:try_start_1 .. :try_end_1} :catch_2

    .line 893
    if-eqz v0, :cond_6

    .line 895
    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "binder:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/car/gp;->a(Lcom/google/android/gms/car/gp;)Lcom/google/android/gms/car/na;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " active sensors:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/car/gp;->b()[I

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/util/ConcurrentModificationException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 898
    :catch_1
    move-exception v0

    :try_start_3
    const-string v0, "concurrent modification happened"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/util/ConcurrentModificationException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    .line 905
    :catch_2
    move-exception v0

    const-string v0, "concurrent modification happened"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 907
    :cond_3
    const-string v0, "**sensor listeners**"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 909
    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v2

    .line 910
    :goto_3
    if-ge v1, v2, :cond_7

    .line 911
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    .line 912
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/gr;

    .line 913
    if-eqz v0, :cond_4

    .line 914
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, " Sensor:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " num client:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/gms/car/gr;->a:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " rate:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v0, v0, Lcom/google/android/gms/car/gr;->b:I

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/util/ConcurrentModificationException; {:try_start_4 .. :try_end_4} :catch_3

    .line 910
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 888
    :cond_5
    const-string v0, "null records"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 901
    :cond_6
    :try_start_5
    const-string v0, "null client"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/util/ConcurrentModificationException; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_2

    .line 920
    :catch_3
    move-exception v0

    const-string v0, "concurrent modification happened"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 923
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->k:Lcom/google/android/gms/car/ij;

    .line 924
    if-eqz v0, :cond_8

    .line 925
    invoke-virtual {v0}, Lcom/google/android/gms/car/ij;->c()V

    .line 927
    :cond_8
    return-void
.end method

.method public final a([I)V
    .locals 3

    .prologue
    .line 140
    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    const-string v0, "CAR.SENSOR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sensors discovered "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/car/gn;->h:[I

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 146
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/car/gn;->i()[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/gn;->i:[I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 149
    return-void

    .line 148
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(IILcom/google/android/gms/car/mx;)Z
    .locals 4

    .prologue
    .line 449
    invoke-direct {p0}, Lcom/google/android/gms/car/gn;->g()V

    .line 450
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->l:Lcom/google/android/gms/car/gx;

    iget-object v0, v0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/car/of;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 452
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "this method can\'t be called from non-Google apps"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 454
    :cond_0
    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 455
    const-string v0, "CAR.SENSOR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "registerInjectedSensorSource "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    :cond_1
    const/4 v0, 0x0

    .line 458
    iget-object v1, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 460
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/car/gn;->h()V

    .line 461
    iget-object v1, p0, Lcom/google/android/gms/car/gn;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    .line 462
    new-instance v0, Lcom/google/android/gms/car/go;

    invoke-direct {v0, p3}, Lcom/google/android/gms/car/go;-><init>(Lcom/google/android/gms/car/mx;)V

    .line 463
    iget-object v1, p0, Lcom/google/android/gms/car/gn;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 464
    iget-wide v2, p0, Lcom/google/android/gms/car/gn;->f:J

    iget-wide v0, v0, Lcom/google/android/gms/car/go;->b:J

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/car/gn;->f:J

    .line 465
    invoke-direct {p0}, Lcom/google/android/gms/car/gn;->i()[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/gn;->i:[I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 466
    const/4 v0, 0x1

    .line 469
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 471
    return v0

    .line 469
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(IILcom/google/android/gms/car/na;)Z
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 201
    invoke-direct {p0}, Lcom/google/android/gms/car/gn;->g()V

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 207
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/car/gn;->h()V

    .line 208
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/gs;

    .line 209
    if-nez v0, :cond_1

    .line 210
    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    const-string v0, "CAR.SENSOR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Requested sensor "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    move v0, v3

    .line 284
    :goto_0
    return v0

    .line 215
    :cond_1
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    move-object v1, v4

    :goto_1
    if-eqz v1, :cond_2

    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/car/gn;->l:Lcom/google/android/gms/car/gx;

    iget-object v2, v2, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    invoke-static {v2, v1}, Lcom/google/android/gms/car/ie;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 216
    :cond_2
    const-string v1, "CAR.SENSOR"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 217
    const-string v1, "CAR.SENSOR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "registerOrUpdateSensorListener "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    :cond_3
    invoke-direct {p0, p3}, Lcom/google/android/gms/car/gn;->a(Lcom/google/android/gms/car/na;)Lcom/google/android/gms/car/gp;

    move-result-object v2

    .line 221
    iget-object v1, p0, Lcom/google/android/gms/car/gn;->c:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/gr;

    .line 222
    if-nez v2, :cond_b

    .line 223
    new-instance v2, Lcom/google/android/gms/car/gp;

    invoke-direct {v2, p0, p3}, Lcom/google/android/gms/car/gp;-><init>(Lcom/google/android/gms/car/gn;Lcom/google/android/gms/car/na;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225
    :try_start_2
    invoke-interface {p3}, Lcom/google/android/gms/car/na;->asBinder()Landroid/os/IBinder;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v2, v6}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 232
    :try_start_3
    iget-object v5, p0, Lcom/google/android/gms/car/gn;->b:Ljava/util/LinkedList;

    invoke-virtual {v5, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    move-object v5, v2

    .line 235
    :goto_2
    iget-object v2, p0, Lcom/google/android/gms/car/gn;->d:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/car/gs;

    .line 236
    if-eqz v2, :cond_5

    .line 238
    const/16 v6, 0xb

    if-ne p1, v6, :cond_4

    iget-object v6, v2, Lcom/google/android/gms/car/gs;->a:Lcom/google/android/gms/car/CarSensorEvent;

    if-nez v6, :cond_4

    .line 240
    sget-object v6, Lcom/google/android/gms/car/gn;->m:Lcom/google/android/gms/car/CarSensorEvent;

    iput-object v6, v2, Lcom/google/android/gms/car/gs;->a:Lcom/google/android/gms/car/CarSensorEvent;

    .line 241
    const-string v6, "CAR.SENSOR"

    const/4 v7, 0x4

    invoke-static {v6, v7}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 242
    const-string v6, "CAR.SENSOR"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "setting driving status to default value "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v2, Lcom/google/android/gms/car/gs;->a:Lcom/google/android/gms/car/CarSensorEvent;

    invoke-virtual {v8}, Lcom/google/android/gms/car/CarSensorEvent;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    :cond_4
    iget-object v6, v2, Lcom/google/android/gms/car/gs;->a:Lcom/google/android/gms/car/CarSensorEvent;

    if-eqz v6, :cond_5

    .line 247
    iget-object v2, v2, Lcom/google/android/gms/car/gs;->a:Lcom/google/android/gms/car/CarSensorEvent;

    invoke-virtual {v5, v2}, Lcom/google/android/gms/car/gp;->a(Lcom/google/android/gms/car/CarSensorEvent;)V

    .line 250
    :cond_5
    if-nez v1, :cond_8

    .line 251
    new-instance v1, Lcom/google/android/gms/car/gr;

    invoke-direct {v1, p2}, Lcom/google/android/gms/car/gr;-><init>(I)V

    .line 252
    iget-object v2, p0, Lcom/google/android/gms/car/gn;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 253
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/gn;->b(I)Z

    move-result v2

    move-object v9, v1

    move v1, v2

    move-object v2, v9

    .line 257
    :goto_3
    if-nez v4, :cond_9

    .line 258
    new-instance v4, Lcom/google/android/gms/car/gq;

    invoke-direct {v4, p0, v5, p2}, Lcom/google/android/gms/car/gq;-><init>(Lcom/google/android/gms/car/gn;Lcom/google/android/gms/car/gp;I)V

    .line 259
    iget-object v6, v2, Lcom/google/android/gms/car/gr;->a:Ljava/util/LinkedList;

    invoke-virtual {v6, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 263
    :goto_4
    iget v4, v2, Lcom/google/android/gms/car/gr;->b:I

    if-le v4, p2, :cond_6

    .line 264
    iput p2, v2, Lcom/google/android/gms/car/gr;->b:I

    .line 265
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/gn;->b(I)Z

    move-result v1

    .line 267
    :cond_6
    invoke-virtual {v5, p1}, Lcom/google/android/gms/car/gp;->a(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 269
    iget-object v2, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 272
    if-eqz v1, :cond_a

    .line 273
    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/car/gn;->a(Lcom/google/android/gms/car/gs;II)Z

    move-result v0

    if-nez v0, :cond_a

    .line 275
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 277
    :try_start_4
    invoke-virtual {v5, p1}, Lcom/google/android/gms/car/gp;->b(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 279
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    move v0, v3

    .line 281
    goto/16 :goto_0

    .line 215
    :pswitch_1
    :try_start_5
    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    goto/16 :goto_1

    :pswitch_2
    const-string v1, "com.google.android.gms.permission.CAR_SPEED"

    goto/16 :goto_1

    :pswitch_3
    const-string v1, "com.google.android.gms.permission.CAR_MILEAGE"

    goto/16 :goto_1

    :pswitch_4
    const-string v1, "com.google.android.gms.permission.CAR_FUEL"

    goto/16 :goto_1

    .line 227
    :catch_0
    move-exception v0

    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 228
    const-string v0, "CAR.SENSOR"

    const-string v1, "Adding listener failed."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 230
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    move v0, v3

    goto/16 :goto_0

    .line 255
    :cond_8
    :try_start_6
    invoke-virtual {v1, v5}, Lcom/google/android/gms/car/gr;->a(Lcom/google/android/gms/car/gp;)Lcom/google/android/gms/car/gq;

    move-result-object v2

    move-object v4, v2

    move-object v2, v1

    move v1, v3

    goto :goto_3

    .line 261
    :cond_9
    iput p2, v4, Lcom/google/android/gms/car/gq;->b:I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_4

    .line 269
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 279
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 284
    :cond_a
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_b
    move-object v5, v2

    goto/16 :goto_2

    .line 215
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/car/mx;Lcom/google/android/gms/car/CarSensorEvent;)Z
    .locals 3

    .prologue
    .line 497
    invoke-direct {p0}, Lcom/google/android/gms/car/gn;->g()V

    .line 498
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->l:Lcom/google/android/gms/car/gx;

    iget-object v0, v0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/car/of;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 500
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "this method can\'t be called from non-Google apps"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 502
    :cond_0
    const/4 v1, 0x0

    .line 503
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 505
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/car/gn;->h()V

    .line 506
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->e:Landroid/util/SparseArray;

    iget v2, p2, Lcom/google/android/gms/car/CarSensorEvent;->b:I

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/go;

    iget-object v0, v0, Lcom/google/android/gms/car/go;->a:Lcom/google/android/gms/car/mx;

    invoke-interface {v0}, Lcom/google/android/gms/car/mx;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/car/mx;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    if-ne v0, v2, :cond_1

    .line 507
    const/4 v0, 0x1

    .line 508
    invoke-direct {p0, p2}, Lcom/google/android/gms/car/gn;->b(Lcom/google/android/gms/car/CarSensorEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 511
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 513
    return v0

    .line 511
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 101
    iget-object v2, p0, Lcom/google/android/gms/car/gn;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 122
    :goto_0
    return-void

    .line 104
    :cond_0
    const-string v2, "CAR.SENSOR"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 105
    const-string v2, "CAR.SENSOR"

    const-string v3, "onSensorsDisconnected()"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/car/gn;->k:Lcom/google/android/gms/car/ij;

    if-eqz v2, :cond_2

    .line 108
    iget-object v2, p0, Lcom/google/android/gms/car/gn;->k:Lcom/google/android/gms/car/ij;

    invoke-virtual {v2}, Lcom/google/android/gms/car/ij;->b()V

    .line 109
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/gms/car/gn;->k:Lcom/google/android/gms/car/ij;

    .line 111
    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/car/gn;->a:Ljava/util/concurrent/locks/ReentrantLock;

    const-wide/16 v4, 0x3e8

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    :goto_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/car/gn;->c:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v4, v2

    :goto_2
    if-ltz v4, :cond_4

    .line 114
    iget-object v2, p0, Lcom/google/android/gms/car/gn;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/google/android/gms/car/gr;

    move-object v3, v0

    .line 115
    iget-object v2, v3, Lcom/google/android/gms/car/gr;->a:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/car/gq;

    iget-object v2, v2, Lcom/google/android/gms/car/gq;->a:Lcom/google/android/gms/car/gp;

    invoke-virtual {v2}, Lcom/google/android/gms/car/gp;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 121
    :catchall_0
    move-exception v2

    invoke-direct {p0}, Lcom/google/android/gms/car/gn;->f()V

    throw v2

    .line 115
    :cond_3
    :try_start_2
    iget-object v2, v3, Lcom/google/android/gms/car/gr;->a:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->clear()V

    .line 113
    add-int/lit8 v2, v4, -0x1

    move v4, v2

    goto :goto_2

    .line 117
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/car/gn;->c:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->clear()V

    .line 118
    iget-object v2, p0, Lcom/google/android/gms/car/gn;->d:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->clear()V

    .line 119
    iget-object v2, p0, Lcom/google/android/gms/car/gn;->b:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->clear()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 121
    invoke-direct {p0}, Lcom/google/android/gms/car/gn;->f()V

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method public final c()[I
    .locals 1

    .prologue
    .line 194
    invoke-direct {p0}, Lcom/google/android/gms/car/gn;->g()V

    .line 195
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->i:[I

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 648
    invoke-virtual {p0}, Lcom/google/android/gms/car/gn;->b()V

    .line 649
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/gn;->g:Lcom/google/android/gms/car/senderprotocol/ba;

    .line 650
    return-void
.end method

.method final e()Z
    .locals 1

    .prologue
    .line 653
    iget-object v0, p0, Lcom/google/android/gms/car/gn;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method
