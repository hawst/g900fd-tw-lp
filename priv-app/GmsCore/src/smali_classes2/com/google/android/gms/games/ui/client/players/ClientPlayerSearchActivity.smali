.class public final Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;
.super Lcom/google/android/gms/games/ui/client/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/common/players/b;
.implements Lcom/google/android/gms/games/ui/d/z;


# static fields
.field private static final n:I

.field private static final o:I


# instance fields
.field private p:Lcom/google/android/gms/games/ui/d/ac;

.field private q:Lcom/google/android/gms/games/ui/d/w;

.field private r:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    sget v0, Lcom/google/android/gms/l;->aE:I

    sput v0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->n:I

    .line 50
    sget v0, Lcom/google/android/gms/m;->n:I

    sput v0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->o:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 65
    sget v0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->n:I

    sget v1, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->o:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/client/a;-><init>(II)V

    .line 66
    return-void
.end method


# virtual methods
.method protected final N()I
    .locals 1

    .prologue
    .line 154
    const/16 v0, 0x8

    return v0
.end method

.method public final T()Z
    .locals 1

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/common/api/w;)V
    .locals 2

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->a(Lcom/google/android/gms/common/api/w;)V

    .line 95
    new-instance v0, Lcom/google/android/gms/people/ad;

    invoke-direct {v0}, Lcom/google/android/gms/people/ad;-><init>()V

    .line 96
    const/16 v1, 0x76

    iput v1, v0, Lcom/google/android/gms/people/ad;->a:I

    .line 97
    sget-object v1, Lcom/google/android/gms/people/x;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0}, Lcom/google/android/gms/people/ad;->a()Lcom/google/android/gms/people/ac;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    .line 98
    return-void
.end method

.method public final varargs a(Lcom/google/android/gms/games/Player;)V
    .locals 3

    .prologue
    .line 253
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "player_search_results"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->finish()V

    .line 254
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/Player;)V
    .locals 7

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->p:Lcom/google/android/gms/games/ui/d/ac;

    iget-object v1, v0, Lcom/google/android/gms/games/ui/d/ac;->b:Landroid/support/v7/widget/SearchView;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/games/ui/d/ac;->b:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->clearFocus()V

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->q:Lcom/google/android/gms/games/ui/d/w;

    if-eqz v0, :cond_1

    const-string v0, "CliPlayerSearchAct"

    const-string v1, "onManageCircles(): canceling the helper that was already running..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->q:Lcom/google/android/gms/games/ui/d/w;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/d/w;->b()V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->r:Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/games/ui/d/w;

    const/4 v6, 0x1

    move-object v1, p1

    move-object v2, p0

    move-object v3, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/ui/d/w;-><init>(Lcom/google/android/gms/games/Player;Lcom/google/android/gms/games/ui/d/z;Landroid/app/Activity;Lcom/google/android/gms/common/api/v;Ljava/lang/String;I)V

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/d/w;->a()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->q:Lcom/google/android/gms/games/ui/d/w;

    .line 267
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 114
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->b_(Landroid/os/Bundle;)V

    .line 117
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/v;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->r:Ljava/lang/String;

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->r:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 121
    const-string v0, "CliPlayerSearchAct"

    const-string v1, "onConnected: no current account! Bailing out..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->finish()V

    .line 125
    :cond_0
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 129
    if-ne p1, v0, :cond_2

    .line 139
    const/4 v1, -0x1

    if-ne p2, v1, :cond_2

    .line 140
    invoke-static {p3}, Lcom/google/android/gms/common/audience/a/i;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/audience/a/j;

    move-result-object v1

    .line 142
    invoke-interface {v1}, Lcom/google/android/gms/common/audience/a/j;->g()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Lcom/google/android/gms/common/audience/a/j;->h()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 144
    :cond_0
    :goto_0
    const-string v1, ""

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->r:Ljava/lang/String;

    const/4 v3, 0x3

    invoke-static {p0, v1, v2, v3, v0}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 150
    :goto_1
    return-void

    .line 142
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 149
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/client/a;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_1
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->getIntent()Landroid/content/Intent;

    .line 72
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->onCreate(Landroid/os/Bundle;)V

    .line 78
    sget v0, Lcom/google/android/gms/p;->kg:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->setTitle(I)V

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->l:Z

    .line 81
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->oU:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;

    .line 83
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 87
    new-instance v1, Lcom/google/android/gms/games/ui/d/ac;

    sget v2, Lcom/google/android/gms/p;->kV:I

    invoke-direct {v1, p0, v0, v2}, Lcom/google/android/gms/games/ui/d/ac;-><init>(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/ui/d/ag;I)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->p:Lcom/google/android/gms/games/ui/d/ac;

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->p:Lcom/google/android/gms/games/ui/d/ac;

    iget-object v1, v0, Lcom/google/android/gms/games/ui/d/ac;->a:Lcom/google/android/gms/games/ui/q;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/q;->setTitle(Ljava/lang/CharSequence;)V

    if-eqz p1, :cond_0

    const-string v1, "savedStatePreviousQuery"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/ui/d/ac;->d:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/android/gms/games/ui/d/ac;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/games/ui/d/ac;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/d/ac;->a(Ljava/lang/String;)V

    .line 90
    :cond_0
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 6

    .prologue
    const/16 v4, 0x10

    const/4 v5, 0x0

    .line 178
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    .line 181
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->p:Lcom/google/android/gms/games/ui/d/ac;

    new-instance v0, Landroid/support/v7/widget/SearchView;

    iget-object v3, v2, Lcom/google/android/gms/games/ui/d/ac;->a:Lcom/google/android/gms/games/ui/q;

    invoke-direct {v0, v3}, Landroid/support/v7/widget/SearchView;-><init>(Landroid/content/Context;)V

    iput-object v0, v2, Lcom/google/android/gms/games/ui/d/ac;->b:Landroid/support/v7/widget/SearchView;

    iget-object v0, v2, Lcom/google/android/gms/games/ui/d/ac;->a:Lcom/google/android/gms/games/ui/q;

    invoke-super {v0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/support/v7/app/a;->a(II)V

    iget-object v0, v2, Lcom/google/android/gms/games/ui/d/ac;->a:Lcom/google/android/gms/games/ui/q;

    invoke-super {v0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    iget-object v3, v2, Lcom/google/android/gms/games/ui/d/ac;->b:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0, v3}, Landroid/support/v7/app/a;->a(Landroid/view/View;)V

    iget-object v0, v2, Lcom/google/android/gms/games/ui/d/ac;->b:Landroid/support/v7/widget/SearchView;

    sget v3, Lcom/google/android/gms/j;->qS:I

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    if-eqz v0, :cond_0

    const/4 v3, -0x1

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setTextColor(I)V

    iget-object v3, v2, Lcom/google/android/gms/games/ui/d/ac;->a:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/q;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/gms/f;->ax:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setHintTextColor(I)V

    :cond_0
    iget-object v0, v2, Lcom/google/android/gms/games/ui/d/ac;->b:Landroid/support/v7/widget/SearchView;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    iget-object v0, v2, Lcom/google/android/gms/games/ui/d/ac;->b:Landroid/support/v7/widget/SearchView;

    if-nez v0, :cond_2

    const-string v0, "SearchHelper"

    const-string v2, "got passed a null searchView!"

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    :cond_1
    :goto_0
    return v1

    .line 181
    :cond_2
    new-instance v3, Lcom/google/android/gms/games/ui/d/ae;

    invoke-direct {v3, v2}, Lcom/google/android/gms/games/ui/d/ae;-><init>(Lcom/google/android/gms/games/ui/d/ac;)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/SearchView;->setOnQueryTextListener(Landroid/support/v7/widget/dn;)V

    new-instance v3, Lcom/google/android/gms/games/ui/d/af;

    invoke-direct {v3, v2, v0}, Lcom/google/android/gms/games/ui/d/af;-><init>(Lcom/google/android/gms/games/ui/d/ac;Landroid/support/v7/widget/SearchView;)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/SearchView;->setOnCloseListener(Landroid/support/v7/widget/dm;)V

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/SearchView;->setIconified(Z)V

    iget-object v3, v2, Lcom/google/android/gms/games/ui/d/ac;->a:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/q;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, v2, Lcom/google/android/gms/games/ui/d/ac;->c:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    iget-object v3, v2, Lcom/google/android/gms/games/ui/d/ac;->d:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, v2, Lcom/google/android/gms/games/ui/d/ac;->d:Ljava/lang/String;

    invoke-virtual {v0, v3, v5}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    :cond_3
    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->getImeOptions()I

    move-result v3

    const/high16 v4, 0x10000000

    or-int/2addr v3, v4

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/SearchView;->setImeOptions(I)V

    iget-boolean v0, v2, Lcom/google/android/gms/games/ui/d/ac;->h:Z

    if-eqz v0, :cond_1

    iget-object v0, v2, Lcom/google/android/gms/games/ui/d/ac;->a:Lcom/google/android/gms/games/ui/q;

    const-string v3, "search"

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/q;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    iget-object v3, v2, Lcom/google/android/gms/games/ui/d/ac;->a:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/q;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v0

    iget-object v2, v2, Lcom/google/android/gms/games/ui/d/ac;->b:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    goto :goto_0
.end method

.method protected final onNewIntent(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 166
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->setIntent(Landroid/content/Intent;)V

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->p:Lcom/google/android/gms/games/ui/d/ac;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.SEARCH"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "query"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/d/ac;->a(Ljava/lang/String;)V

    .line 168
    :goto_0
    return-void

    .line 167
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onNewIntent: Unexpected intent: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 172
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->p:Lcom/google/android/gms/games/ui/d/ac;

    const-string v1, "savedStatePreviousQuery"

    iget-object v0, v0, Lcom/google/android/gms/games/ui/d/ac;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    return-void
.end method

.method public final onSearchRequested()Z
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->p:Lcom/google/android/gms/games/ui/d/ac;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/d/ac;->c()Z

    move-result v0

    return v0
.end method

.method public final onStart()V
    .locals 2

    .prologue
    .line 108
    invoke-super {p0}, Lcom/google/android/gms/games/ui/client/a;->onStart()V

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->p:Lcom/google/android/gms/games/ui/d/ac;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/games/ui/d/ac;->f:Z

    .line 110
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 102
    invoke-super {p0}, Lcom/google/android/gms/games/ui/client/a;->onStop()V

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/ClientPlayerSearchActivity;->p:Lcom/google/android/gms/games/ui/d/ac;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/d/ac;->a()V

    .line 104
    return-void
.end method
