.class final Lcom/google/android/gms/panorama/a/i;
.super Lcom/google/android/gms/panorama/a/l;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/net/Uri;

.field final synthetic b:Landroid/os/Bundle;

.field final synthetic d:Lcom/google/android/gms/panorama/a/h;


# direct methods
.method constructor <init>(Lcom/google/android/gms/panorama/a/h;Lcom/google/android/gms/common/api/v;Landroid/net/Uri;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/gms/panorama/a/i;->d:Lcom/google/android/gms/panorama/a/h;

    iput-object p3, p0, Lcom/google/android/gms/panorama/a/i;->a:Landroid/net/Uri;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/panorama/a/i;->b:Landroid/os/Bundle;

    invoke-direct {p0, p2}, Lcom/google/android/gms/panorama/a/l;-><init>(Lcom/google/android/gms/common/api/v;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/ap;
    .locals 3

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/gms/panorama/a/a;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/panorama/a/a;-><init>(Lcom/google/android/gms/common/api/Status;Landroid/content/Intent;I)V

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Lcom/google/android/gms/panorama/a/e;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 35
    new-instance v0, Lcom/google/android/gms/panorama/a/k;

    invoke-direct {v0, p0}, Lcom/google/android/gms/panorama/a/k;-><init>(Lcom/google/android/gms/common/api/m;)V

    .line 36
    iget-object v1, p0, Lcom/google/android/gms/panorama/a/i;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/gms/panorama/a/i;->b:Landroid/os/Bundle;

    const-string v3, "com.google.android.gms"

    invoke-virtual {p1, v3, v1, v4}, Landroid/content/Context;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    new-instance v3, Lcom/google/android/gms/panorama/a/j;

    invoke-direct {v3, p1, v1, v0}, Lcom/google/android/gms/panorama/a/j;-><init>(Landroid/content/Context;Landroid/net/Uri;Lcom/google/android/gms/panorama/a/b;)V

    const/4 v0, 0x1

    :try_start_0
    invoke-interface {p2, v3, v1, v2, v0}, Lcom/google/android/gms/panorama/a/e;->a(Lcom/google/android/gms/panorama/a/b;Landroid/net/Uri;Landroid/os/Bundle;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p1, v1, v4}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    throw v0

    :catch_1
    move-exception v0

    invoke-virtual {p1, v1, v4}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    throw v0
.end method
