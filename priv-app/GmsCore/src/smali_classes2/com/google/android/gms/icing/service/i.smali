.class final Lcom/google/android/gms/icing/service/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:[Lcom/google/android/gms/appdatasearch/UsageInfo;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:J

.field final synthetic d:Ljava/util/List;

.field final synthetic e:Z

.field final synthetic f:Lcom/google/android/gms/appdatasearch/a/h;

.field final synthetic g:Lcom/google/android/gms/icing/service/g;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/service/g;[Lcom/google/android/gms/appdatasearch/UsageInfo;Ljava/lang/String;JLjava/util/List;ZLcom/google/android/gms/appdatasearch/a/h;)V
    .locals 0

    .prologue
    .line 370
    iput-object p1, p0, Lcom/google/android/gms/icing/service/i;->g:Lcom/google/android/gms/icing/service/g;

    iput-object p2, p0, Lcom/google/android/gms/icing/service/i;->a:[Lcom/google/android/gms/appdatasearch/UsageInfo;

    iput-object p3, p0, Lcom/google/android/gms/icing/service/i;->b:Ljava/lang/String;

    iput-wide p4, p0, Lcom/google/android/gms/icing/service/i;->c:J

    iput-object p6, p0, Lcom/google/android/gms/icing/service/i;->d:Ljava/util/List;

    iput-boolean p7, p0, Lcom/google/android/gms/icing/service/i;->e:Z

    iput-object p8, p0, Lcom/google/android/gms/icing/service/i;->f:Lcom/google/android/gms/appdatasearch/a/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 373
    iget-object v0, p0, Lcom/google/android/gms/icing/service/i;->g:Lcom/google/android/gms/icing/service/g;

    iget-object v0, p0, Lcom/google/android/gms/icing/service/i;->a:[Lcom/google/android/gms/appdatasearch/UsageInfo;

    invoke-static {v0}, Lcom/google/android/gms/icing/service/g;->a([Lcom/google/android/gms/appdatasearch/UsageInfo;)V

    .line 376
    iget-object v0, p0, Lcom/google/android/gms/icing/service/i;->g:Lcom/google/android/gms/icing/service/g;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/g;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 377
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Recording usage report from "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/icing/service/i;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;)I

    .line 380
    iget-object v0, p0, Lcom/google/android/gms/icing/service/i;->g:Lcom/google/android/gms/icing/service/g;

    iget-object v1, v0, Lcom/google/android/gms/icing/service/g;->a:Lcom/google/android/gms/icing/impl/e/h;

    iget-object v2, p0, Lcom/google/android/gms/icing/service/i;->a:[Lcom/google/android/gms/appdatasearch/UsageInfo;

    iget-object v3, p0, Lcom/google/android/gms/icing/service/i;->b:Ljava/lang/String;

    iget-wide v4, p0, Lcom/google/android/gms/icing/service/i;->c:J

    iget-object v6, p0, Lcom/google/android/gms/icing/service/i;->d:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/icing/service/i;->g:Lcom/google/android/gms/icing/service/g;

    iget-object v7, v0, Lcom/google/android/gms/icing/service/g;->d:Ljava/lang/Runnable;

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/gms/icing/impl/e/h;->a([Lcom/google/android/gms/appdatasearch/UsageInfo;Ljava/lang/String;JLjava/util/List;Ljava/lang/Runnable;)Z

    move-result v2

    .line 385
    iget-boolean v0, p0, Lcom/google/android/gms/icing/service/i;->e:Z

    if-eqz v0, :cond_9

    move v1, v8

    .line 386
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/icing/service/i;->a:[Lcom/google/android/gms/appdatasearch/UsageInfo;

    array-length v0, v0

    if-ge v1, v0, :cond_6

    .line 387
    iget-object v0, p0, Lcom/google/android/gms/icing/service/i;->a:[Lcom/google/android/gms/appdatasearch/UsageInfo;

    aget-object v0, v0, v1

    if-eqz v0, :cond_3

    .line 388
    iget-object v0, p0, Lcom/google/android/gms/icing/service/i;->a:[Lcom/google/android/gms/appdatasearch/UsageInfo;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/UsageInfo;->c()I

    move-result v0

    .line 389
    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    move v3, v9

    .line 390
    :goto_1
    const/4 v4, 0x3

    if-ne v0, v4, :cond_5

    move v0, v9

    .line 391
    :goto_2
    if-nez v3, :cond_0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/service/i;->a:[Lcom/google/android/gms/appdatasearch/UsageInfo;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/UsageInfo;->d()Lcom/google/android/gms/appdatasearch/DocumentContents;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 393
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/service/i;->g:Lcom/google/android/gms/icing/service/g;

    iget-object v3, v0, Lcom/google/android/gms/icing/service/g;->b:Lcom/google/android/gms/icing/impl/e/a;

    iget-object v0, p0, Lcom/google/android/gms/icing/service/i;->a:[Lcom/google/android/gms/appdatasearch/UsageInfo;

    aget-object v4, v0, v1

    invoke-virtual {v4}, Lcom/google/android/gms/appdatasearch/UsageInfo;->a()Lcom/google/android/gms/appdatasearch/DocumentId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/DocumentId;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v0, v3, Lcom/google/android/gms/icing/impl/e/a;->a:Lcom/google/android/gms/icing/impl/e/b;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/icing/impl/e/b;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    iget v6, v3, Lcom/google/android/gms/icing/impl/e/a;->b:I

    invoke-direct {v0, v6}, Ljava/util/ArrayList;-><init>(I)V

    :cond_1
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    iget v6, v3, Lcom/google/android/gms/icing/impl/e/a;->b:I

    if-le v4, v6, :cond_2

    invoke-interface {v0, v8}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_2
    iget-object v3, v3, Lcom/google/android/gms/icing/impl/e/a;->a:Lcom/google/android/gms/icing/impl/e/b;

    invoke-virtual {v3, v5, v0}, Lcom/google/android/gms/icing/impl/e/b;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 386
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_4
    move v3, v8

    .line 389
    goto :goto_1

    :cond_5
    move v0, v8

    .line 390
    goto :goto_2

    :cond_6
    move v0, v2

    .line 401
    :goto_3
    if-eqz v0, :cond_8

    move v0, v8

    .line 405
    :goto_4
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/icing/service/i;->f:Lcom/google/android/gms/appdatasearch/a/h;

    new-instance v2, Lcom/google/android/gms/common/api/Status;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/appdatasearch/a/h;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 409
    :goto_5
    return-void

    .line 398
    :cond_7
    const-string v0, "Ignoring usage report: reporting disabled."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;)I

    move v0, v9

    goto :goto_3

    .line 401
    :cond_8
    const/16 v0, 0x8

    goto :goto_4

    .line 406
    :catch_0
    move-exception v0

    const-string v1, "Client died during reportUsage"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_5

    :cond_9
    move v0, v2

    goto :goto_3
.end method
