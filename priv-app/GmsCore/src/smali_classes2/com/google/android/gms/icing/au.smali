.class public final Lcom/google/android/gms/icing/au;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile p:[Lcom/google/android/gms/icing/au;


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:D

.field public d:I

.field public e:Ljava/lang/String;

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 11120
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 11121
    iput v2, p0, Lcom/google/android/gms/icing/au;->a:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/au;->b:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/icing/au;->c:D

    iput v2, p0, Lcom/google/android/gms/icing/au;->d:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/au;->e:Ljava/lang/String;

    iput v2, p0, Lcom/google/android/gms/icing/au;->f:I

    iput v2, p0, Lcom/google/android/gms/icing/au;->g:I

    iput v2, p0, Lcom/google/android/gms/icing/au;->h:I

    iput v2, p0, Lcom/google/android/gms/icing/au;->i:I

    iput v2, p0, Lcom/google/android/gms/icing/au;->j:I

    iput v2, p0, Lcom/google/android/gms/icing/au;->k:I

    iput v2, p0, Lcom/google/android/gms/icing/au;->l:I

    iput v2, p0, Lcom/google/android/gms/icing/au;->m:I

    iput v2, p0, Lcom/google/android/gms/icing/au;->n:I

    iput v2, p0, Lcom/google/android/gms/icing/au;->o:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/au;->cachedSize:I

    .line 11122
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/au;
    .locals 2

    .prologue
    .line 11064
    sget-object v0, Lcom/google/android/gms/icing/au;->p:[Lcom/google/android/gms/icing/au;

    if-nez v0, :cond_1

    .line 11065
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 11067
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/au;->p:[Lcom/google/android/gms/icing/au;

    if-nez v0, :cond_0

    .line 11068
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/au;

    sput-object v0, Lcom/google/android/gms/icing/au;->p:[Lcom/google/android/gms/icing/au;

    .line 11070
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 11072
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/au;->p:[Lcom/google/android/gms/icing/au;

    return-object v0

    .line 11070
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 11292
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 11293
    iget v1, p0, Lcom/google/android/gms/icing/au;->a:I

    if-eqz v1, :cond_0

    .line 11294
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/icing/au;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11297
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/au;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 11298
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/au;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11301
    :cond_1
    iget-wide v2, p0, Lcom/google/android/gms/icing/au;->c:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 11303
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/icing/au;->c:D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 11306
    :cond_2
    iget v1, p0, Lcom/google/android/gms/icing/au;->d:I

    if-eqz v1, :cond_3

    .line 11307
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/icing/au;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11310
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/au;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 11311
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/icing/au;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11314
    :cond_4
    iget v1, p0, Lcom/google/android/gms/icing/au;->f:I

    if-eqz v1, :cond_5

    .line 11315
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/gms/icing/au;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11318
    :cond_5
    iget v1, p0, Lcom/google/android/gms/icing/au;->g:I

    if-eqz v1, :cond_6

    .line 11319
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/gms/icing/au;->g:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11322
    :cond_6
    iget v1, p0, Lcom/google/android/gms/icing/au;->h:I

    if-eqz v1, :cond_7

    .line 11323
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/gms/icing/au;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11326
    :cond_7
    iget v1, p0, Lcom/google/android/gms/icing/au;->i:I

    if-eqz v1, :cond_8

    .line 11327
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/gms/icing/au;->i:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11330
    :cond_8
    iget v1, p0, Lcom/google/android/gms/icing/au;->j:I

    if-eqz v1, :cond_9

    .line 11331
    const/16 v1, 0xa

    iget v2, p0, Lcom/google/android/gms/icing/au;->j:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11334
    :cond_9
    iget v1, p0, Lcom/google/android/gms/icing/au;->k:I

    if-eqz v1, :cond_a

    .line 11335
    const/16 v1, 0xb

    iget v2, p0, Lcom/google/android/gms/icing/au;->k:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11338
    :cond_a
    iget v1, p0, Lcom/google/android/gms/icing/au;->l:I

    if-eqz v1, :cond_b

    .line 11339
    const/16 v1, 0xc

    iget v2, p0, Lcom/google/android/gms/icing/au;->l:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11342
    :cond_b
    iget v1, p0, Lcom/google/android/gms/icing/au;->m:I

    if-eqz v1, :cond_c

    .line 11343
    const/16 v1, 0xd

    iget v2, p0, Lcom/google/android/gms/icing/au;->m:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11346
    :cond_c
    iget v1, p0, Lcom/google/android/gms/icing/au;->n:I

    if-eqz v1, :cond_d

    .line 11347
    const/16 v1, 0xe

    iget v2, p0, Lcom/google/android/gms/icing/au;->n:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11350
    :cond_d
    iget v1, p0, Lcom/google/android/gms/icing/au;->o:I

    if-eqz v1, :cond_e

    .line 11351
    const/16 v1, 0xf

    iget v2, p0, Lcom/google/android/gms/icing/au;->o:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11354
    :cond_e
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 11146
    if-ne p1, p0, :cond_1

    .line 11209
    :cond_0
    :goto_0
    return v0

    .line 11149
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/au;

    if-nez v2, :cond_2

    move v0, v1

    .line 11150
    goto :goto_0

    .line 11152
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/au;

    .line 11153
    iget v2, p0, Lcom/google/android/gms/icing/au;->a:I

    iget v3, p1, Lcom/google/android/gms/icing/au;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 11154
    goto :goto_0

    .line 11156
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/au;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 11157
    iget-object v2, p1, Lcom/google/android/gms/icing/au;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 11158
    goto :goto_0

    .line 11160
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/icing/au;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/au;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 11161
    goto :goto_0

    .line 11164
    :cond_5
    iget-wide v2, p0, Lcom/google/android/gms/icing/au;->c:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 11165
    iget-wide v4, p1, Lcom/google/android/gms/icing/au;->c:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    .line 11166
    goto :goto_0

    .line 11169
    :cond_6
    iget v2, p0, Lcom/google/android/gms/icing/au;->d:I

    iget v3, p1, Lcom/google/android/gms/icing/au;->d:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 11170
    goto :goto_0

    .line 11172
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/icing/au;->e:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 11173
    iget-object v2, p1, Lcom/google/android/gms/icing/au;->e:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    .line 11174
    goto :goto_0

    .line 11176
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/icing/au;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/au;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 11177
    goto :goto_0

    .line 11179
    :cond_9
    iget v2, p0, Lcom/google/android/gms/icing/au;->f:I

    iget v3, p1, Lcom/google/android/gms/icing/au;->f:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 11180
    goto :goto_0

    .line 11182
    :cond_a
    iget v2, p0, Lcom/google/android/gms/icing/au;->g:I

    iget v3, p1, Lcom/google/android/gms/icing/au;->g:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 11183
    goto :goto_0

    .line 11185
    :cond_b
    iget v2, p0, Lcom/google/android/gms/icing/au;->h:I

    iget v3, p1, Lcom/google/android/gms/icing/au;->h:I

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 11186
    goto :goto_0

    .line 11188
    :cond_c
    iget v2, p0, Lcom/google/android/gms/icing/au;->i:I

    iget v3, p1, Lcom/google/android/gms/icing/au;->i:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 11189
    goto :goto_0

    .line 11191
    :cond_d
    iget v2, p0, Lcom/google/android/gms/icing/au;->j:I

    iget v3, p1, Lcom/google/android/gms/icing/au;->j:I

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 11192
    goto :goto_0

    .line 11194
    :cond_e
    iget v2, p0, Lcom/google/android/gms/icing/au;->k:I

    iget v3, p1, Lcom/google/android/gms/icing/au;->k:I

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 11195
    goto/16 :goto_0

    .line 11197
    :cond_f
    iget v2, p0, Lcom/google/android/gms/icing/au;->l:I

    iget v3, p1, Lcom/google/android/gms/icing/au;->l:I

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 11198
    goto/16 :goto_0

    .line 11200
    :cond_10
    iget v2, p0, Lcom/google/android/gms/icing/au;->m:I

    iget v3, p1, Lcom/google/android/gms/icing/au;->m:I

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 11201
    goto/16 :goto_0

    .line 11203
    :cond_11
    iget v2, p0, Lcom/google/android/gms/icing/au;->n:I

    iget v3, p1, Lcom/google/android/gms/icing/au;->n:I

    if-eq v2, v3, :cond_12

    move v0, v1

    .line 11204
    goto/16 :goto_0

    .line 11206
    :cond_12
    iget v2, p0, Lcom/google/android/gms/icing/au;->o:I

    iget v3, p1, Lcom/google/android/gms/icing/au;->o:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 11207
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 11214
    iget v0, p0, Lcom/google/android/gms/icing/au;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 11216
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/au;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 11219
    iget-wide v2, p0, Lcom/google/android/gms/icing/au;->c:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 11220
    mul-int/lit8 v0, v0, 0x1f

    const/16 v4, 0x20

    ushr-long v4, v2, v4

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 11222
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/icing/au;->d:I

    add-int/2addr v0, v2

    .line 11223
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/au;->e:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 11225
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/au;->f:I

    add-int/2addr v0, v1

    .line 11226
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/au;->g:I

    add-int/2addr v0, v1

    .line 11227
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/au;->h:I

    add-int/2addr v0, v1

    .line 11228
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/au;->i:I

    add-int/2addr v0, v1

    .line 11229
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/au;->j:I

    add-int/2addr v0, v1

    .line 11230
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/au;->k:I

    add-int/2addr v0, v1

    .line 11231
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/au;->l:I

    add-int/2addr v0, v1

    .line 11232
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/au;->m:I

    add-int/2addr v0, v1

    .line 11233
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/au;->n:I

    add-int/2addr v0, v1

    .line 11234
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/au;->o:I

    add-int/2addr v0, v1

    .line 11235
    return v0

    .line 11216
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/au;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 11223
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/au;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 11058
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/au;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/au;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/au;->c:D

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/au;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/au;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/au;->f:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/au;->g:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/au;->h:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/au;->i:I

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/au;->j:I

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/au;->k:I

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/au;->l:I

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/au;->m:I

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/au;->n:I

    goto :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/au;->o:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x19 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 11241
    iget v0, p0, Lcom/google/android/gms/icing/au;->a:I

    if-eqz v0, :cond_0

    .line 11242
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/icing/au;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 11244
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/au;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 11245
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/au;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 11247
    :cond_1
    iget-wide v0, p0, Lcom/google/android/gms/icing/au;->c:D

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 11249
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/icing/au;->c:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 11251
    :cond_2
    iget v0, p0, Lcom/google/android/gms/icing/au;->d:I

    if-eqz v0, :cond_3

    .line 11252
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/icing/au;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 11254
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/au;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 11255
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/icing/au;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 11257
    :cond_4
    iget v0, p0, Lcom/google/android/gms/icing/au;->f:I

    if-eqz v0, :cond_5

    .line 11258
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/gms/icing/au;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 11260
    :cond_5
    iget v0, p0, Lcom/google/android/gms/icing/au;->g:I

    if-eqz v0, :cond_6

    .line 11261
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/android/gms/icing/au;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 11263
    :cond_6
    iget v0, p0, Lcom/google/android/gms/icing/au;->h:I

    if-eqz v0, :cond_7

    .line 11264
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/android/gms/icing/au;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 11266
    :cond_7
    iget v0, p0, Lcom/google/android/gms/icing/au;->i:I

    if-eqz v0, :cond_8

    .line 11267
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/android/gms/icing/au;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 11269
    :cond_8
    iget v0, p0, Lcom/google/android/gms/icing/au;->j:I

    if-eqz v0, :cond_9

    .line 11270
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/android/gms/icing/au;->j:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 11272
    :cond_9
    iget v0, p0, Lcom/google/android/gms/icing/au;->k:I

    if-eqz v0, :cond_a

    .line 11273
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/android/gms/icing/au;->k:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 11275
    :cond_a
    iget v0, p0, Lcom/google/android/gms/icing/au;->l:I

    if-eqz v0, :cond_b

    .line 11276
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/android/gms/icing/au;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 11278
    :cond_b
    iget v0, p0, Lcom/google/android/gms/icing/au;->m:I

    if-eqz v0, :cond_c

    .line 11279
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/android/gms/icing/au;->m:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 11281
    :cond_c
    iget v0, p0, Lcom/google/android/gms/icing/au;->n:I

    if-eqz v0, :cond_d

    .line 11282
    const/16 v0, 0xe

    iget v1, p0, Lcom/google/android/gms/icing/au;->n:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 11284
    :cond_d
    iget v0, p0, Lcom/google/android/gms/icing/au;->o:I

    if-eqz v0, :cond_e

    .line 11285
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/android/gms/icing/au;->o:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 11287
    :cond_e
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 11288
    return-void
.end method
