.class final Lcom/google/android/gms/maps/t;
.super Lcom/google/android/gms/b/a;
.source "SourceFile"


# instance fields
.field protected d:Lcom/google/android/gms/b/q;

.field private final e:Landroid/support/v4/app/Fragment;

.field private f:Landroid/app/Activity;

.field private final g:Ljava/util/List;


# direct methods
.method constructor <init>(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 228
    invoke-direct {p0}, Lcom/google/android/gms/b/a;-><init>()V

    .line 224
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/maps/t;->g:Ljava/util/List;

    .line 229
    iput-object p1, p0, Lcom/google/android/gms/maps/t;->e:Landroid/support/v4/app/Fragment;

    .line 230
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/maps/t;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/google/android/gms/maps/t;->f:Landroid/app/Activity;

    invoke-virtual {p0}, Lcom/google/android/gms/maps/t;->b()V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/b/q;)V
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/google/android/gms/maps/t;->d:Lcom/google/android/gms/b/q;

    .line 235
    invoke-virtual {p0}, Lcom/google/android/gms/maps/t;->b()V

    .line 236
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/gms/maps/t;->f:Landroid/app/Activity;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/maps/t;->d:Lcom/google/android/gms/b/q;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/b/a;->a:Lcom/google/android/gms/b/o;

    if-nez v0, :cond_1

    .line 248
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/maps/t;->f:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/gms/maps/m;->a(Landroid/content/Context;)I

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/maps/t;->f:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/gms/maps/internal/ct;->a(Landroid/content/Context;)Lcom/google/android/gms/maps/internal/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/maps/t;->f:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/h;->b(Lcom/google/android/gms/b/l;)Lcom/google/android/gms/maps/internal/w;

    move-result-object v0

    .line 251
    iget-object v1, p0, Lcom/google/android/gms/maps/t;->d:Lcom/google/android/gms/b/q;

    new-instance v2, Lcom/google/android/gms/maps/r;

    iget-object v3, p0, Lcom/google/android/gms/maps/t;->e:Landroid/support/v4/app/Fragment;

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/maps/r;-><init>(Landroid/support/v4/app/Fragment;Lcom/google/android/gms/maps/internal/w;)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/b/q;->a(Lcom/google/android/gms/b/o;)V

    .line 253
    iget-object v0, p0, Lcom/google/android/gms/maps/t;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/n;

    .line 254
    iget-object v1, p0, Lcom/google/android/gms/b/a;->a:Lcom/google/android/gms/b/o;

    check-cast v1, Lcom/google/android/gms/maps/r;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/common/eu; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    iget-object v3, v1, Lcom/google/android/gms/maps/r;->a:Lcom/google/android/gms/maps/internal/w;

    new-instance v4, Lcom/google/android/gms/maps/s;

    invoke-direct {v4, v1, v0}, Lcom/google/android/gms/maps/s;-><init>(Lcom/google/android/gms/maps/r;Lcom/google/android/gms/maps/n;)V

    invoke-interface {v3, v4}, Lcom/google/android/gms/maps/internal/w;->a(Lcom/google/android/gms/maps/internal/ay;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/gms/common/eu; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/gms/common/eu; {:try_start_2 .. :try_end_2} :catch_2

    .line 257
    :catch_1
    move-exception v0

    .line 258
    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1

    .line 255
    :cond_0
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/maps/t;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/google/android/gms/common/eu; {:try_start_3 .. :try_end_3} :catch_2

    .line 263
    :cond_1
    :goto_1
    return-void

    :catch_2
    move-exception v0

    goto :goto_1
.end method
