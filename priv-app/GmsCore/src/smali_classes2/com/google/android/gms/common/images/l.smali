.class final Lcom/google/android/gms/common/images/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 648
    iput-object p1, p0, Lcom/google/android/gms/common/images/l;->a:Landroid/net/Uri;

    .line 649
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 658
    instance-of v0, p1, Lcom/google/android/gms/common/images/l;

    if-nez v0, :cond_0

    .line 659
    const/4 v0, 0x0

    .line 666
    :goto_0
    return v0

    .line 661
    :cond_0
    if-ne p0, p1, :cond_1

    .line 662
    const/4 v0, 0x1

    goto :goto_0

    .line 665
    :cond_1
    check-cast p1, Lcom/google/android/gms/common/images/l;

    .line 666
    iget-object v0, p1, Lcom/google/android/gms/common/images/l;->a:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/gms/common/images/l;->a:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 653
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/common/images/l;->a:Landroid/net/Uri;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
