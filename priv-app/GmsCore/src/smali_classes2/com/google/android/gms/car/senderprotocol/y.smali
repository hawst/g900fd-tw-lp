.class public final Lcom/google/android/gms/car/senderprotocol/y;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/car/ii;


# instance fields
.field private final b:Lcom/google/android/gms/car/senderprotocol/ab;

.field private final c:Lcom/google/android/gms/car/senderprotocol/ad;

.field private d:Ljava/lang/Thread;

.field private e:Ljava/lang/Thread;

.field private final f:Ljava/io/InputStream;

.field private final g:Ljava/io/OutputStream;

.field private volatile h:Z

.field private volatile i:Z

.field private final j:Lcom/google/android/gms/car/senderprotocol/af;

.field private final k:Ljava/lang/Object;

.field private final l:Ljava/lang/Object;

.field private m:Lcom/google/android/gms/car/senderprotocol/SslWrapper;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lcom/google/android/gms/car/ii;

    const-string v1, "debug.car.framer_send_latency"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ii;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/senderprotocol/y;->a:Lcom/google/android/gms/car/ii;

    return-void
.end method

.method private constructor <init>(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/google/android/gms/car/senderprotocol/af;Lcom/google/android/gms/car/senderprotocol/z;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->k:Ljava/lang/Object;

    .line 128
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->l:Ljava/lang/Object;

    .line 149
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/y;->f:Ljava/io/InputStream;

    .line 156
    iput-object p2, p0, Lcom/google/android/gms/car/senderprotocol/y;->g:Ljava/io/OutputStream;

    .line 158
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/ad;

    const/16 v1, 0x3f00

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/car/senderprotocol/ad;-><init>(Lcom/google/android/gms/car/senderprotocol/y;I)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->c:Lcom/google/android/gms/car/senderprotocol/ad;

    .line 159
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/ab;

    invoke-direct {v0, p0, p0, p4}, Lcom/google/android/gms/car/senderprotocol/ab;-><init>(Lcom/google/android/gms/car/senderprotocol/y;Lcom/google/android/gms/car/senderprotocol/y;Lcom/google/android/gms/car/senderprotocol/z;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->b:Lcom/google/android/gms/car/senderprotocol/ab;

    .line 160
    iput-object p3, p0, Lcom/google/android/gms/car/senderprotocol/y;->j:Lcom/google/android/gms/car/senderprotocol/af;

    .line 161
    iput-boolean v2, p0, Lcom/google/android/gms/car/senderprotocol/y;->h:Z

    .line 162
    iput-boolean v2, p0, Lcom/google/android/gms/car/senderprotocol/y;->i:Z

    .line 163
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/google/android/gms/car/senderprotocol/af;Lcom/google/android/gms/car/senderprotocol/z;B)V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/car/senderprotocol/y;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/google/android/gms/car/senderprotocol/af;Lcom/google/android/gms/car/senderprotocol/z;)V

    .line 174
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/senderprotocol/y;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->k:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/car/senderprotocol/y;)Z
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->h:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/car/senderprotocol/y;)Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->d:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic d()Lcom/google/android/gms/car/ii;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/y;->a:Lcom/google/android/gms/car/ii;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/car/senderprotocol/y;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->h:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/car/senderprotocol/y;)Lcom/google/android/gms/car/senderprotocol/ab;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->b:Lcom/google/android/gms/car/senderprotocol/ab;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/car/senderprotocol/y;)Lcom/google/android/gms/car/senderprotocol/af;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->j:Lcom/google/android/gms/car/senderprotocol/af;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/car/senderprotocol/y;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->l:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/car/senderprotocol/y;)Z
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->i:Z

    return v0
.end method

.method static synthetic i(Lcom/google/android/gms/car/senderprotocol/y;)Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->e:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/gms/car/senderprotocol/y;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->i:Z

    return v0
.end method

.method static synthetic k(Lcom/google/android/gms/car/senderprotocol/y;)Lcom/google/android/gms/car/senderprotocol/ad;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->c:Lcom/google/android/gms/car/senderprotocol/ad;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/gms/car/senderprotocol/y;)Lcom/google/android/gms/car/senderprotocol/SslWrapper;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->m:Lcom/google/android/gms/car/senderprotocol/SslWrapper;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/gms/car/senderprotocol/y;)Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->g:Ljava/io/OutputStream;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/gms/car/senderprotocol/y;)Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->f:Ljava/io/InputStream;

    return-object v0
.end method

.method static synthetic o(Lcom/google/android/gms/car/senderprotocol/y;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->h:Z

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 194
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/y;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->e:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 195
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/y;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->d:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_1
    return-void

    .line 194
    :cond_0
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->i:Z

    new-instance v0, Lcom/google/android/gms/car/senderprotocol/ag;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/senderprotocol/ag;-><init>(Lcom/google/android/gms/car/senderprotocol/y;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->e:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->e:Ljava/lang/Thread;

    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Ljava/lang/Thread;->setPriority(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->e:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 195
    :cond_1
    const/4 v0, 0x1

    :try_start_3
    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->h:Z

    new-instance v0, Lcom/google/android/gms/car/senderprotocol/aa;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/senderprotocol/aa;-><init>(Lcom/google/android/gms/car/senderprotocol/y;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->d:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->d:Ljava/lang/Thread;

    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Ljava/lang/Thread;->setPriority(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->d:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(ILjava/nio/ByteBuffer;ZZ)V
    .locals 7

    .prologue
    .line 186
    if-nez p3, :cond_0

    const/4 v4, 0x1

    .line 187
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/y;->c:Lcom/google/android/gms/car/senderprotocol/ad;

    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    if-nez v4, :cond_1

    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    iget v2, v1, Lcom/google/android/gms/car/senderprotocol/ad;->a:I

    add-int/lit8 v2, v2, -0x4

    if-le v0, v2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Message is too long to send as one fragment"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 186
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 187
    :cond_1
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/ae;

    move v2, p1

    move-object v3, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/car/senderprotocol/ae;-><init>(Lcom/google/android/gms/car/senderprotocol/ad;ILjava/nio/ByteBuffer;ZZZ)V

    :try_start_0
    iget-object v1, v1, Lcom/google/android/gms/car/senderprotocol/ad;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1, v0}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-static {}, Ljava/lang/Thread;->yield()V

    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 188
    return-void

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/car/senderprotocol/SslWrapper;)V
    .locals 2

    .prologue
    .line 411
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/y;->m:Lcom/google/android/gms/car/senderprotocol/SslWrapper;

    .line 412
    const-string v0, "CAR.GAL"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413
    const-string v0, "CAR.GAL"

    const-string v1, "Ssl negotiation complete, turning on encryption!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 203
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/y;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->d:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->h:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->h:Z

    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->f:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->d:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->d:Ljava/lang/Thread;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Ljava/lang/Thread;->join(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->d:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CAR.GAL"

    const-string v2, "reader thread stuck? Trying SIGQUIT."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/os/Process;->sendSignal(II)V

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/os/Process;->sendSignal(II)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->d:Ljava/lang/Thread;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Ljava/lang/Thread;->join(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_1
    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->d:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->j:Lcom/google/android/gms/car/senderprotocol/af;

    invoke-interface {v0}, Lcom/google/android/gms/car/senderprotocol/af;->d()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->d:Ljava/lang/Thread;

    :cond_1
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 204
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/y;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_5
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->e:Ljava/lang/Thread;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->i:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->i:Z

    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->g:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->e:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/y;->e:Ljava/lang/Thread;

    :cond_2
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    return-void

    .line 203
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 204
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 406
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/y;->c:Lcom/google/android/gms/car/senderprotocol/ad;

    iget-object v0, v1, Lcom/google/android/gms/car/senderprotocol/ad;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/senderprotocol/ae;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_0

    .line 408
    return-void

    .line 406
    :cond_1
    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/senderprotocol/ad;->a(Lcom/google/android/gms/car/senderprotocol/ae;)V

    const/4 v0, 0x1

    goto :goto_0
.end method
