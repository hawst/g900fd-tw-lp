.class public final Lcom/google/android/gms/icing/impl/d/c;
.super Lcom/google/android/gms/icing/impl/d/b;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/playlog/c;


# instance fields
.field private final a:Lcom/google/android/gms/playlog/b;

.field private final b:Lcom/google/android/gms/playlog/b;

.field private final c:Ljava/lang/Object;

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/16 v2, 0xc

    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/d/b;-><init>(Landroid/content/Context;)V

    .line 21
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/d/c;->c:Ljava/lang/Object;

    .line 22
    iput-boolean v6, p0, Lcom/google/android/gms/icing/impl/d/c;->d:Z

    .line 23
    iput-boolean v6, p0, Lcom/google/android/gms/icing/impl/d/c;->e:Z

    .line 27
    new-instance v0, Lcom/google/android/gms/playlog/b;

    invoke-direct {v0, p1, v2, p0, v6}, Lcom/google/android/gms/playlog/b;-><init>(Landroid/content/Context;ILcom/google/android/gms/playlog/c;B)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/d/c;->a:Lcom/google/android/gms/playlog/b;

    .line 30
    new-instance v0, Lcom/google/android/gms/playlog/b;

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/playlog/b;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/playlog/c;Z)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/d/c;->b:Lcom/google/android/gms/playlog/b;

    .line 32
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 85
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/d/c;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 86
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/icing/impl/d/c;->d:Z

    if-eqz v0, :cond_1

    .line 89
    iget-boolean v0, p0, Lcom/google/android/gms/icing/impl/d/c;->e:Z

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d/c;->a:Lcom/google/android/gms/playlog/b;

    iget-object v0, v0, Lcom/google/android/gms/playlog/b;->a:Lcom/google/android/gms/playlog/internal/i;

    invoke-virtual {v0}, Lcom/google/android/gms/playlog/internal/i;->g()V

    .line 91
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/impl/d/c;->e:Z

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d/c;->b:Lcom/google/android/gms/playlog/b;

    iget-object v0, v0, Lcom/google/android/gms/playlog/b;->a:Lcom/google/android/gms/playlog/internal/i;

    invoke-virtual {v0}, Lcom/google/android/gms/playlog/internal/i;->g()V

    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/impl/d/c;->d:Z

    .line 96
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 72
    const-string v0, "Logging connection failed: %s"

    invoke-static {v0, p1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    .line 73
    return-void
.end method

.method protected final a(Ljava/lang/String;Lcom/google/k/f/an;Z)V
    .locals 5

    .prologue
    .line 45
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 47
    if-eqz p3, :cond_1

    .line 48
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d/c;->a:Lcom/google/android/gms/playlog/b;

    invoke-static {p2}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, p1, v1, v4}, Lcom/google/android/gms/playlog/b;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    .line 53
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/d/c;->c:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/gms/icing/impl/d/c;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d/c;->a:Lcom/google/android/gms/playlog/b;

    iget-object v0, v0, Lcom/google/android/gms/playlog/b;->a:Lcom/google/android/gms/playlog/internal/i;

    invoke-virtual {v0}, Lcom/google/android/gms/playlog/internal/i;->f()V

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d/c;->b:Lcom/google/android/gms/playlog/b;

    iget-object v0, v0, Lcom/google/android/gms/playlog/b;->a:Lcom/google/android/gms/playlog/internal/i;

    invoke-virtual {v0}, Lcom/google/android/gms/playlog/internal/i;->f()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/icing/impl/d/c;->d:Z

    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 55
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 56
    return-void

    .line 50
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d/c;->b:Lcom/google/android/gms/playlog/b;

    invoke-static {p2}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, p1, v1, v4}, Lcom/google/android/gms/playlog/b;->a(Ljava/lang/String;[B[Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 55
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0

    .line 53
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 61
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/d/c;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 62
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/icing/impl/d/c;->e:Z

    .line 63
    iget-boolean v0, p0, Lcom/google/android/gms/icing/impl/d/c;->d:Z

    if-nez v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d/c;->a:Lcom/google/android/gms/playlog/b;

    iget-object v0, v0, Lcom/google/android/gms/playlog/b;->a:Lcom/google/android/gms/playlog/internal/i;

    invoke-virtual {v0}, Lcom/google/android/gms/playlog/internal/i;->g()V

    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/impl/d/c;->e:Z

    .line 67
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 77
    const-string v0, "Logging connection failed"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 78
    return-void
.end method
