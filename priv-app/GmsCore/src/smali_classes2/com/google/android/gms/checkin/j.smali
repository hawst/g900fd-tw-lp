.class public final Lcom/google/android/gms/checkin/j;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/regex/Pattern;

.field private static final b:Ljava/util/HashMap;

.field private static final f:[C


# instance fields
.field private c:Ljava/lang/Object;

.field private d:[I

.field private e:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 116
    const-string v0, ",?([a-z_]+)(?:\\(([0-9]+)\\))?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/checkin/j;->a:Ljava/util/regex/Pattern;

    .line 117
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 441
    sput-object v0, Lcom/google/android/gms/checkin/j;->b:Ljava/util/HashMap;

    const-string v1, "count"

    new-instance v2, Lcom/google/android/gms/checkin/k;

    invoke-direct {v2}, Lcom/google/android/gms/checkin/k;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 452
    sget-object v0, Lcom/google/android/gms/checkin/j;->b:Ljava/util/HashMap;

    const-string v1, "sum"

    new-instance v2, Lcom/google/android/gms/checkin/m;

    invoke-direct {v2}, Lcom/google/android/gms/checkin/m;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 465
    sget-object v0, Lcom/google/android/gms/checkin/j;->b:Ljava/util/HashMap;

    const-string v1, "mean"

    new-instance v2, Lcom/google/android/gms/checkin/o;

    invoke-direct {v2}, Lcom/google/android/gms/checkin/o;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 482
    sget-object v0, Lcom/google/android/gms/checkin/j;->b:Ljava/util/HashMap;

    const-string v1, "min"

    new-instance v2, Lcom/google/android/gms/checkin/q;

    invoke-direct {v2}, Lcom/google/android/gms/checkin/q;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 495
    sget-object v0, Lcom/google/android/gms/checkin/j;->b:Ljava/util/HashMap;

    const-string v1, "max"

    new-instance v2, Lcom/google/android/gms/checkin/s;

    invoke-direct {v2}, Lcom/google/android/gms/checkin/s;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 513
    sget-object v0, Lcom/google/android/gms/checkin/j;->b:Ljava/util/HashMap;

    const-string v1, "group_by"

    new-instance v2, Lcom/google/android/gms/checkin/u;

    invoke-direct {v2}, Lcom/google/android/gms/checkin/u;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 532
    sget-object v0, Lcom/google/android/gms/checkin/j;->b:Ljava/util/HashMap;

    const-string v1, "packages"

    new-instance v2, Lcom/google/android/gms/checkin/w;

    invoke-direct {v2}, Lcom/google/android/gms/checkin/w;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 685
    const-string v0, "0123456789abcdef"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/checkin/j;->f:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    const/4 v0, 0x0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/gms/checkin/j;->d:[I

    .line 123
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/checkin/j;->e:Ljava/util/HashMap;

    .line 138
    return-void
.end method

.method private static a(Landroid/util/EventLog$Event;)J
    .locals 4

    .prologue
    .line 635
    invoke-virtual {p0}, Landroid/util/EventLog$Event;->getTimeNanos()J

    move-result-wide v0

    const-wide/32 v2, 0x7a120

    add-long/2addr v0, v2

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    return-wide v0
.end method

.method static synthetic a(Ljava/lang/Object;)J
    .locals 2

    .prologue
    .line 105
    instance-of v0, p0, Ljava/lang/Number;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "number expected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast p0, Ljava/lang/Number;

    invoke-virtual {p0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(Ljava/lang/String;)Lcom/google/android/gms/checkin/aa;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 413
    new-instance v1, Lcom/google/android/gms/checkin/aa;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lcom/google/android/gms/checkin/aa;-><init>(B)V

    .line 414
    sget-object v0, Lcom/google/android/gms/checkin/j;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 415
    :goto_0
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->regionStart()I

    move-result v0

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->regionEnd()I

    move-result v3

    if-ge v0, v3, :cond_6

    .line 418
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v0

    if-nez v0, :cond_0

    .line 419
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "bad spec: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->regionStart()I

    move-result v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 422
    :cond_0
    invoke-virtual {v2, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x2

    invoke-virtual {v2, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 423
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_3

    :cond_1
    const/4 v0, -0x1

    .line 424
    :goto_1
    const-string v4, "log"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 425
    iput-boolean v6, v1, Lcom/google/android/gms/checkin/aa;->b:Z

    .line 416
    :cond_2
    :goto_2
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->end()I

    move-result v0

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->regionEnd()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Ljava/util/regex/Matcher;->region(II)Ljava/util/regex/Matcher;

    goto :goto_0

    .line 423
    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1

    .line 427
    :cond_4
    sget-object v4, Lcom/google/android/gms/checkin/j;->b:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "bad function: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 428
    :cond_5
    iget-object v4, v1, Lcom/google/android/gms/checkin/aa;->d:Ljava/util/ArrayList;

    sget-object v5, Lcom/google/android/gms/checkin/j;->b:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 429
    iget-object v4, v1, Lcom/google/android/gms/checkin/aa;->e:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 430
    const-string v4, "group_by"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v1, Lcom/google/android/gms/checkin/aa;->c:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 434
    :cond_6
    return-object v1
.end method

.method static synthetic a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 105
    invoke-static {p0, p1}, Lcom/google/android/gms/checkin/j;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ljava/io/File;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    invoke-static {p0}, Lcom/google/android/gms/checkin/j;->b(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a([B)Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    invoke-static {p0}, Lcom/google/android/gms/checkin/j;->b([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(JJJLjava/util/List;Landroid/os/DropBoxManager;Landroid/content/Context;)V
    .locals 13

    .prologue
    .line 298
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 299
    const-string v2, "start="

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    const-string v2, "end="

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p3

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    cmp-long v2, p5, p1

    if-eqz v2, :cond_0

    .line 303
    const-string v2, "log_start="

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p5

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    :cond_0
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 307
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 310
    invoke-interface/range {p7 .. p7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/EventLog$Event;

    .line 311
    invoke-virtual {v2}, Landroid/util/EventLog$Event;->getTag()I

    move-result v11

    .line 312
    iget-object v3, p0, Lcom/google/android/gms/checkin/j;->e:Ljava/util/HashMap;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/checkin/aa;

    .line 313
    if-eqz v3, :cond_1

    iget-object v4, v3, Lcom/google/android/gms/checkin/aa;->d:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 315
    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 316
    iget-object v4, v3, Lcom/google/android/gms/checkin/aa;->a:Ljava/lang/String;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 317
    invoke-virtual {v2}, Landroid/util/EventLog$Event;->getData()Ljava/lang/Object;

    move-result-object v12

    .line 318
    iget-object v2, v3, Lcom/google/android/gms/checkin/aa;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v12, v2}, Lcom/google/android/gms/checkin/j;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 321
    :cond_2
    :try_start_0
    invoke-virtual {v9, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 322
    if-nez v2, :cond_4

    .line 323
    new-instance v5, Ljava/util/ArrayList;

    iget-object v2, v3, Lcom/google/android/gms/checkin/aa;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 324
    const/4 v2, 0x0

    move v6, v2

    :goto_2
    iget-object v2, v3, Lcom/google/android/gms/checkin/aa;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v6, v2, :cond_3

    .line 325
    iget-object v2, v3, Lcom/google/android/gms/checkin/aa;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/checkin/y;

    iget-object v4, v3, Lcom/google/android/gms/checkin/aa;->e:Ljava/util/ArrayList;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v2, v4}, Lcom/google/android/gms/checkin/y;->a(I)Lcom/google/android/gms/checkin/z;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 324
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_2

    .line 327
    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v9, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v5

    .line 330
    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/checkin/z;

    invoke-interface {v2, v12}, Lcom/google/android/gms/checkin/z;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 331
    :catch_0
    move-exception v2

    .line 332
    const-string v4, "EventLogAggregator"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Can\'t accumulate event: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v3, Lcom/google/android/gms/checkin/aa;->a:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 333
    iget-object v2, p0, Lcom/google/android/gms/checkin/j;->e:Ljava/util/HashMap;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 338
    :cond_5
    :try_start_1
    const-string v2, "\n"

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339
    invoke-virtual {v9}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 340
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v7}, Lcom/google/android/gms/common/internal/t;->a(Ljava/lang/String;Ljava/lang/Appendable;)V

    .line 341
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/checkin/z;

    .line 342
    const-string v5, ","

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 343
    move-object/from16 v0, p9

    invoke-interface {v2, v7, v0}, Lcom/google/android/gms/checkin/z;->a(Ljava/lang/StringBuilder;Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_5

    .line 347
    :catch_1
    move-exception v2

    .line 348
    const-string v3, "EventLogAggregator"

    const-string v4, "IOException writing StringBuilder"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 351
    :cond_6
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 353
    const-string v3, "event_data"

    move-object/from16 v0, p8

    invoke-virtual {v0, v3, v2}, Landroid/os/DropBoxManager;->addText(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    return-void

    .line 345
    :cond_7
    :try_start_2
    const-string v2, "\n"

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_4
.end method

.method private declared-synchronized a(JJLjava/util/List;Landroid/os/DropBoxManager;)V
    .locals 7

    .prologue
    .line 245
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 247
    cmp-long v0, p3, p1

    if-eqz v0, :cond_0

    .line 248
    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",0,0,event_log_start\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    :cond_0
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/EventLog$Event;

    .line 252
    invoke-virtual {v0}, Landroid/util/EventLog$Event;->getTag()I

    move-result v1

    .line 253
    iget-object v4, p0, Lcom/google/android/gms/checkin/j;->e:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/checkin/aa;

    .line 254
    if-eqz v1, :cond_1

    iget-boolean v4, v1, Lcom/google/android/gms/checkin/aa;->b:Z

    if-eqz v4, :cond_1

    .line 256
    invoke-static {v0}, Lcom/google/android/gms/checkin/j;->a(Landroid/util/EventLog$Event;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    invoke-virtual {v0}, Landroid/util/EventLog$Event;->getProcessId()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    invoke-virtual {v0}, Landroid/util/EventLog$Event;->getThreadId()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    iget-object v1, v1, Lcom/google/android/gms/checkin/aa;->a:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 262
    :try_start_1
    invoke-virtual {v0}, Landroid/util/EventLog$Event;->getData()Ljava/lang/Object;

    move-result-object v0

    .line 263
    instance-of v1, v0, [Ljava/lang/Object;

    if-eqz v1, :cond_2

    .line 264
    check-cast v0, [Ljava/lang/Object;

    .line 265
    const/4 v1, 0x0

    :goto_1
    array-length v4, v0

    if-ge v1, v4, :cond_3

    .line 266
    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/google/android/gms/common/internal/t;->a(Ljava/lang/String;Ljava/lang/Appendable;)V

    .line 265
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 269
    :cond_2
    if-eqz v0, :cond_3

    .line 270
    const-string v1, ","

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/t;->a(Ljava/lang/String;Ljava/lang/Appendable;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 277
    :cond_3
    :goto_2
    :try_start_2
    const-string v0, "\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 245
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 273
    :catch_0
    move-exception v0

    .line 274
    :try_start_3
    const-string v1, "EventLogAggregator"

    const-string v4, "IOException writing StringBuilder"

    invoke-static {v1, v4, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 280
    :cond_4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 281
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_5

    .line 283
    const-string v1, "event_log"

    invoke-virtual {p6, v1, v0}, Landroid/os/DropBoxManager;->addText(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 285
    :cond_5
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Landroid/content/ContentResolver;Z)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 363
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;)Ljava/lang/Object;

    move-result-object v3

    .line 364
    iget-object v0, p0, Lcom/google/android/gms/checkin/j;->c:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v3, v0, :cond_0

    .line 404
    :goto_0
    monitor-exit p0

    return-void

    .line 366
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/checkin/j;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 368
    const-string v0, "checkin_event_whitelist"

    invoke-static {p1, v0}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 369
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "event:"

    aput-object v5, v1, v4

    invoke-static {p1, v1}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;[Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 372
    const-string v4, "event:"

    invoke-static {v1, v4, p2, v0}, Lcom/google/android/gms/checkin/EventLogService;->a(Ljava/util/Map;Ljava/lang/String;ZLjava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 374
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 375
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v5, 0x6

    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 376
    invoke-static {v5}, Landroid/util/EventLog;->getTagCode(Ljava/lang/String;)I

    move-result v1

    .line 377
    if-gez v1, :cond_2

    .line 378
    const-string v0, "EventLogAggregator"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "Unknown tag: "

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 363
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 382
    :cond_2
    :try_start_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 383
    invoke-virtual {v0}, Ljava/lang/String;->length()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v6

    if-lez v6, :cond_1

    .line 386
    :try_start_3
    invoke-static {v0}, Lcom/google/android/gms/checkin/j;->a(Ljava/lang/String;)Lcom/google/android/gms/checkin/aa;

    move-result-object v6

    .line 387
    iget-boolean v7, v6, Lcom/google/android/gms/checkin/aa;->b:Z

    if-nez v7, :cond_3

    iget-object v7, v6, Lcom/google/android/gms/checkin/aa;->d:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_1

    .line 388
    :cond_3
    iput-object v5, v6, Lcom/google/android/gms/checkin/aa;->a:Ljava/lang/String;

    .line 389
    iget-object v7, p0, Lcom/google/android/gms/checkin/j;->e:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 390
    :catch_0
    move-exception v1

    .line 391
    :try_start_4
    const-string v6, "EventLogAggregator"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Bad spec: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 398
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/checkin/j;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/gms/checkin/j;->d:[I

    .line 399
    iget-object v0, p0, Lcom/google/android/gms/checkin/j;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v5, p0, Lcom/google/android/gms/checkin/j;->d:[I

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v5, v1

    move v1, v2

    goto :goto_2

    .line 400
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/checkin/j;->d:[I

    add-int/lit8 v2, v1, 0x1

    const v4, 0x11238

    aput v4, v0, v1

    .line 401
    iget-object v0, p0, Lcom/google/android/gms/checkin/j;->d:[I

    array-length v0, v0

    if-eq v2, v0, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 403
    :cond_6
    iput-object v3, p0, Lcom/google/android/gms/checkin/j;->c:Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method

.method static synthetic b(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 105
    instance-of v0, p0, Ljava/lang/Number;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "number expected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast p0, Ljava/lang/Number;

    invoke-virtual {p0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method

.method private static b(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 618
    if-gez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "parameter required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 619
    :cond_0
    instance-of v0, p0, [Ljava/lang/Object;

    if-eqz v0, :cond_4

    .line 620
    check-cast p0, [Ljava/lang/Object;

    .line 621
    if-ltz p1, :cond_1

    array-length v0, p0

    if-lt p1, v0, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bad index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 622
    :cond_2
    aget-object p0, p0, p1

    .line 626
    :cond_3
    return-object p0

    .line 623
    :cond_4
    if-lez p1, :cond_3

    .line 624
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "list expected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static b(Ljava/io/File;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 662
    const-string v0, "SHA-256"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 665
    const/4 v2, 0x0

    .line 667
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 671
    const/16 v2, 0x400

    :try_start_1
    new-array v2, v2, [B

    .line 672
    :goto_0
    invoke-virtual {v1, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_1

    .line 673
    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4, v3}, Ljava/security/MessageDigest;->update([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 676
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 677
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_0
    throw v0

    .line 676
    :cond_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 682
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/checkin/j;->b([B)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 676
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method private static b([B)Ljava/lang/String;
    .locals 6

    .prologue
    .line 694
    new-instance v1, Ljava/lang/StringBuilder;

    array-length v0, p0

    mul-int/lit8 v0, v0, 0x2

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 695
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-byte v3, p0, v0

    .line 696
    sget-object v4, Lcom/google/android/gms/checkin/j;->f:[C

    shr-int/lit8 v5, v3, 0x4

    and-int/lit8 v5, v5, 0xf

    aget-char v4, v4, v5

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/google/android/gms/checkin/j;->f:[C

    and-int/lit8 v3, v3, 0xf

    aget-char v3, v5, v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 695
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 698
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/content/Context;JJLandroid/os/DropBoxManager;Z)J
    .locals 20

    .prologue
    .line 161
    monitor-enter p0

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 164
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/checkin/j;->a(Landroid/content/ContentResolver;Z)V

    .line 168
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 169
    const v2, 0x11238

    invoke-static {v2, v14, v15}, Landroid/util/EventLog;->writeEvent(IJ)I

    .line 171
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 172
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/checkin/j;->d:[I

    invoke-static {v2, v11}, Landroid/util/EventLog;->readEvents([ILjava/util/Collection;)V

    .line 176
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 179
    const/4 v9, 0x0

    move-wide v4, v14

    move-wide v6, v14

    :goto_0
    if-ge v9, v10, :cond_a

    .line 180
    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/EventLog$Event;

    .line 181
    invoke-virtual {v2}, Landroid/util/EventLog$Event;->getTag()I

    move-result v16

    .line 182
    invoke-static {v2}, Lcom/google/android/gms/checkin/j;->a(Landroid/util/EventLog$Event;)J

    move-result-wide v12

    .line 186
    if-ne v8, v10, :cond_0

    cmp-long v17, v12, p2

    if-ltz v17, :cond_0

    .line 188
    cmp-long v8, v12, v6

    if-gez v8, :cond_9

    move-wide v6, v12

    move v8, v9

    .line 190
    :cond_0
    :goto_1
    if-ne v3, v10, :cond_1

    cmp-long v17, v12, p4

    if-ltz v17, :cond_1

    .line 192
    cmp-long v3, v12, v4

    if-gez v3, :cond_8

    move-wide v4, v12

    move v3, v9

    .line 196
    :cond_1
    :goto_2
    const v12, 0x11238

    move/from16 v0, v16

    if-ne v0, v12, :cond_7

    .line 197
    invoke-virtual {v2}, Landroid/util/EventLog$Event;->getData()Ljava/lang/Object;

    move-result-object v2

    .line 198
    instance-of v12, v2, Ljava/lang/Long;

    if-eqz v12, :cond_7

    .line 199
    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    .line 200
    cmp-long v2, v12, p2

    if-nez v2, :cond_2

    .line 201
    add-int/lit8 v8, v9, 0x1

    move-wide/from16 v6, p2

    .line 204
    :cond_2
    cmp-long v2, v12, p4

    if-nez v2, :cond_6

    .line 205
    add-int/lit8 v4, v9, 0x1

    move-wide/from16 v2, p4

    .line 208
    :goto_3
    cmp-long v5, v12, v14

    if-nez v5, :cond_5

    move-wide v12, v2

    move v10, v4

    move v2, v9

    .line 221
    :goto_4
    const-wide/16 v4, -0x1

    cmp-long v3, p2, v4

    if-lez v3, :cond_3

    if-eqz p6, :cond_3

    .line 222
    invoke-static {v8, v2}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-virtual {v11, v3, v2}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v8

    move-object/from16 v3, p0

    move-wide/from16 v4, p2

    move-object/from16 v9, p6

    .line 223
    invoke-direct/range {v3 .. v9}, Lcom/google/android/gms/checkin/j;->a(JJLjava/util/List;Landroid/os/DropBoxManager;)V

    .line 225
    :cond_3
    const-wide/16 v4, -0x1

    cmp-long v3, p4, v4

    if-lez v3, :cond_4

    if-eqz p6, :cond_4

    .line 226
    invoke-static {v10, v2}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-virtual {v11, v3, v2}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v10

    move-object/from16 v3, p0

    move-wide/from16 v4, p4

    move-wide v6, v14

    move-wide v8, v12

    move-object/from16 v11, p6

    move-object/from16 v12, p1

    .line 227
    invoke-direct/range {v3 .. v12}, Lcom/google/android/gms/checkin/j;->a(JJJLjava/util/List;Landroid/os/DropBoxManager;Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 231
    :cond_4
    monitor-exit p0

    return-wide v14

    :cond_5
    move-wide/from16 v18, v6

    move v6, v4

    move v7, v8

    move-wide/from16 v4, v18

    .line 179
    :goto_5
    add-int/lit8 v9, v9, 0x1

    move v8, v7

    move-wide/from16 v18, v4

    move-wide v4, v2

    move v3, v6

    move-wide/from16 v6, v18

    goto/16 :goto_0

    .line 161
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_6
    move-wide/from16 v18, v4

    move v4, v3

    move-wide/from16 v2, v18

    goto :goto_3

    :cond_7
    move-wide/from16 v18, v4

    move-wide v4, v6

    move v6, v3

    move v7, v8

    move-wide/from16 v2, v18

    goto :goto_5

    :cond_8
    move v3, v9

    goto/16 :goto_2

    :cond_9
    move v8, v9

    goto/16 :goto_1

    :cond_a
    move-wide v12, v4

    move v2, v10

    move v10, v3

    goto :goto_4
.end method
