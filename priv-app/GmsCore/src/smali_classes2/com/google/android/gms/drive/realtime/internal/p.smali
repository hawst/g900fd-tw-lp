.class public abstract Lcom/google/android/gms/drive/realtime/internal/p;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/realtime/internal/o;


# direct methods
.method public static a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/o;
    .locals 2

    .prologue
    .line 29
    if-nez p0, :cond_0

    .line 30
    const/4 v0, 0x0

    .line 36
    :goto_0
    return-object v0

    .line 32
    :cond_0
    const-string v0, "com.google.android.gms.drive.realtime.internal.IDataHolderEventCallback"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 33
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/drive/realtime/internal/o;

    if-eqz v1, :cond_1

    .line 34
    check-cast v0, Lcom/google/android/gms/drive/realtime/internal/o;

    goto :goto_0

    .line 36
    :cond_1
    new-instance v0, Lcom/google/android/gms/drive/realtime/internal/q;

    invoke-direct {v0, p0}, Lcom/google/android/gms/drive/realtime/internal/q;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 44
    sparse-switch p1, :sswitch_data_0

    .line 87
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 48
    :sswitch_0
    const-string v0, "com.google.android.gms.drive.realtime.internal.IDataHolderEventCallback"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v3

    .line 49
    goto :goto_0

    .line 53
    :sswitch_1
    const-string v0, "com.google.android.gms.drive.realtime.internal.IDataHolderEventCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 55
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    sget-object v0, Lcom/google/android/gms/common/data/DataHolder;->CREATOR:Lcom/google/android/gms/common/data/p;

    invoke-static {p2}, Lcom/google/android/gms/common/data/p;->a(Landroid/os/Parcel;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    move-object v1, v0

    .line 62
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    sget-object v0, Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;

    .line 68
    :goto_2
    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/drive/realtime/internal/p;->a(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    .line 69
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v3

    .line 70
    goto :goto_0

    :cond_0
    move-object v1, v2

    .line 59
    goto :goto_1

    :cond_1
    move-object v0, v2

    .line 66
    goto :goto_2

    .line 74
    :sswitch_2
    const-string v0, "com.google.android.gms.drive.realtime.internal.IDataHolderEventCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 76
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    .line 77
    sget-object v0, Lcom/google/android/gms/common/api/Status;->CREATOR:Lcom/google/android/gms/common/api/as;

    invoke-static {p2}, Lcom/google/android/gms/common/api/as;->a(Landroid/os/Parcel;)Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    .line 82
    :cond_2
    invoke-virtual {p0, v2}, Lcom/google/android/gms/drive/realtime/internal/p;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 83
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v3

    .line 84
    goto :goto_0

    .line 44
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
