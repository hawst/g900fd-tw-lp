.class public final Lcom/google/android/gms/games/internal/a/bx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/multiplayer/realtime/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/multiplayer/realtime/h;Ljava/lang/String;)Lcom/google/android/gms/games/multiplayer/realtime/Room;
    .locals 2

    .prologue
    .line 153
    invoke-interface {p1, p2}, Lcom/google/android/gms/common/api/v;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    .line 155
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v1

    invoke-virtual {v1, v0, p3}, Lcom/google/android/gms/games/internal/c;->e(Lcom/google/android/gms/common/api/aj;Ljava/lang/String;)Lcom/google/android/gms/games/multiplayer/realtime/Room;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 122
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lcom/google/android/gms/games/internal/c;->b(Ljava/lang/String;I)V

    .line 124
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 139
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p3, v1}, Lcom/google/android/gms/games/internal/c;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 141
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 128
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lcom/google/android/gms/games/internal/c;->a(Ljava/lang/String;I)V

    .line 130
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 146
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p3, v1}, Lcom/google/android/gms/games/internal/c;->b(Ljava/lang/String;Ljava/lang/String;I)V

    .line 148
    return-void
.end method

.method public final c(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 161
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/internal/c;->h(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
