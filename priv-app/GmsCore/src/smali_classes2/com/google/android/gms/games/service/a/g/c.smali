.class public final Lcom/google/android/gms/games/service/a/g/c;
.super Lcom/google/android/gms/games/service/a/b;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/games/internal/dr;

.field private final c:Lcom/google/android/gms/games/a/au;

.field private final d:Lcom/google/android/gms/games/e/g;

.field private final e:I

.field private final f:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/e/g;II)V
    .locals 2

    .prologue
    .line 28
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/service/a/b;-><init>(Lcom/google/android/gms/common/server/ClientContext;I)V

    .line 29
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/g/c;->b:Lcom/google/android/gms/games/internal/dr;

    .line 30
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/g/c;->c:Lcom/google/android/gms/games/a/au;

    .line 31
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/g/c;->d:Lcom/google/android/gms/games/e/g;

    .line 32
    iput p4, p0, Lcom/google/android/gms/games/service/a/g/c;->e:I

    .line 33
    iput p5, p0, Lcom/google/android/gms/games/service/a/g/c;->f:I

    .line 34
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/games/a/t;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 39
    if-nez p2, :cond_0

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/g/c;->c:Lcom/google/android/gms/games/a/au;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/g/c;->d:Lcom/google/android/gms/games/e/g;

    iget v2, p0, Lcom/google/android/gms/games/service/a/g/c;->e:I

    iget v3, p0, Lcom/google/android/gms/games/service/a/g/c;->f:I

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/e/g;II)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 44
    :goto_0
    return-object v0

    .line 42
    :cond_0
    if-ne p2, v2, :cond_1

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/g/c;->c:Lcom/google/android/gms/games/a/au;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/g/c;->d:Lcom/google/android/gms/games/e/g;

    invoke-virtual {v1}, Lcom/google/android/gms/games/e/g;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 46
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Incorrect number of data holders requested!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final a([Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 65
    array-length v0, p1

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/g/c;->b:Lcom/google/android/gms/games/internal/dr;

    aget-object v1, p1, v1

    aget-object v2, p1, v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/games/internal/dr;->a(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;)V

    .line 67
    return-void

    :cond_0
    move v0, v2

    .line 65
    goto :goto_0
.end method
