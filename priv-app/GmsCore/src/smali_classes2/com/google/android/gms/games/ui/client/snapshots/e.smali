.class final Lcom/google/android/gms/games/ui/client/snapshots/e;
.super Lcom/google/android/gms/games/ui/card/c;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/c;-><init>(Landroid/view/View;)V

    .line 56
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/games/ui/ac;ILjava/lang/Object;)V
    .locals 12

    .prologue
    .line 52
    check-cast p3, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/card/c;->a(Lcom/google/android/gms/games/ui/ac;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/e;->l:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p3}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    invoke-interface {p3}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->f()Landroid/net/Uri;

    move-result-object v0

    sget v2, Lcom/google/android/gms/h;->cg:I

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/games/ui/client/snapshots/e;->a(Landroid/net/Uri;I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/c;->k:Lcom/google/android/gms/games/ui/card/a;

    const/4 v2, 0x2

    const v3, 0x4003126f    # 2.048f

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/games/ui/card/a;->a(IF)V

    invoke-interface {p3}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->a()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->s_()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p3}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->k()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/e;->l:Landroid/content/Context;

    invoke-interface {p3}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->l()J

    move-result-wide v4

    invoke-static {v0, v4, v5}, Lcom/google/android/gms/games/ui/d/al;->b(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p3}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->m()J

    move-result-wide v6

    const-string v0, ""

    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    const-wide/32 v8, 0xea60

    cmp-long v0, v6, v8

    if-gez v0, :cond_1

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    long-to-int v0, v6

    sget v5, Lcom/google/android/gms/n;->f:I

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v1, v5, v0, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/client/snapshots/e;->e(Ljava/lang/String;)V

    sget v5, Lcom/google/android/gms/p;->ia:I

    invoke-virtual {p0, v5}, Lcom/google/android/gms/games/ui/client/snapshots/e;->d(I)V

    invoke-virtual {p0, v4}, Lcom/google/android/gms/games/ui/client/snapshots/e;->c(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/snapshots/e;->d(Ljava/lang/String;)V

    sget v5, Lcom/google/android/gms/p;->lH:I

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v6, Lcom/google/android/gms/p;->mf:I

    const/4 v7, 0x6

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v2, v7, v8

    const/4 v2, 0x1

    const-string v8, ""

    aput-object v8, v7, v2

    const/4 v2, 0x2

    aput-object v5, v7, v2

    const/4 v2, 0x3

    aput-object v4, v7, v2

    const/4 v2, 0x4

    aput-object v0, v7, v2

    const/4 v0, 0x5

    aput-object v3, v7, v0

    invoke-virtual {v1, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/snapshots/e;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/e;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/client/snapshots/d;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/snapshots/d;->a(Lcom/google/android/gms/games/ui/client/snapshots/d;)Lcom/google/android/gms/games/ui/common/b/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/b/b;->V()Z

    move-result v0

    if-eqz v0, :cond_5

    sget v0, Lcom/google/android/gms/m;->r:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/snapshots/e;->g(I)V

    :goto_1
    return-void

    :cond_1
    const-wide/32 v8, 0x36ee80

    cmp-long v0, v6, v8

    if-gez v0, :cond_2

    const-wide/32 v8, 0xea60

    div-long/2addr v6, v8

    long-to-int v0, v6

    sget v5, Lcom/google/android/gms/n;->d:I

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v1, v5, v0, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-wide/32 v8, 0x2255100

    cmp-long v0, v6, v8

    if-gez v0, :cond_4

    const-wide/32 v8, 0x36ee80

    div-long v8, v6, v8

    long-to-int v0, v8

    int-to-long v8, v0

    const-wide/32 v10, 0x36ee80

    mul-long/2addr v8, v10

    sub-long/2addr v6, v8

    const-wide/32 v8, 0xea60

    div-long/2addr v6, v8

    long-to-int v5, v6

    const/4 v6, 0x1

    if-ne v0, v6, :cond_3

    sget v0, Lcom/google/android/gms/n;->e:I

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v1, v0, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    sget v6, Lcom/google/android/gms/n;->c:I

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v8

    const/4 v0, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v0

    invoke-virtual {v1, v6, v5, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    const-wide/32 v8, 0x36ee80

    div-long/2addr v6, v8

    long-to-int v0, v6

    sget v5, Lcom/google/android/gms/p;->ii:I

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v7

    invoke-virtual {v1, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/snapshots/e;->g(I)V

    goto/16 :goto_1
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 115
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->gO:I

    if-ne v0, v1, :cond_0

    .line 116
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/e;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/client/snapshots/d;

    .line 117
    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/snapshots/d;->b(Lcom/google/android/gms/games/ui/client/snapshots/d;)Lcom/google/android/gms/games/ui/common/b/a;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/snapshots/e;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/common/b/a;->b(Lcom/google/android/gms/games/snapshot/SnapshotMetadata;)V

    .line 119
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final varargs u()V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/snapshots/e;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/client/snapshots/d;

    .line 110
    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/snapshots/d;->b(Lcom/google/android/gms/games/ui/client/snapshots/d;)Lcom/google/android/gms/games/ui/common/b/a;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/snapshots/e;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/common/b/a;->a(Lcom/google/android/gms/games/snapshot/SnapshotMetadata;)V

    .line 111
    return-void
.end method
