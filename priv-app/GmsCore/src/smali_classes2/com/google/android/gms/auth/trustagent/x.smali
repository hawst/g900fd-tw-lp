.class public final Lcom/google/android/gms/auth/trustagent/x;
.super Lcom/google/android/gms/auth/trustagent/a;
.source "SourceFile"


# instance fields
.field private c:Lcom/google/android/gms/auth/trustagent/ae;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/nfc/NfcAdapter;

.field private f:Landroid/os/Handler;

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/a;-><init>()V

    .line 36
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/x;->f:Landroid/os/Handler;

    .line 40
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/x;Landroid/nfc/Tag;)V
    .locals 3

    .prologue
    .line 30
    :try_start_0
    new-instance v0, Lcom/google/android/gms/auth/trustagent/z;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/trustagent/z;-><init>(Lcom/google/android/gms/auth/trustagent/x;)V

    invoke-static {p1, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;->a(Landroid/nfc/Tag;Lcom/google/android/gms/auth/trustagent/trustlet/bq;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Coffee - PairNfcDeviceFragment"

    const-string v2, "unable to communicate with tag"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/x;->d()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/x;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/x;->g:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/auth/trustagent/x;)Lcom/google/android/gms/auth/trustagent/ae;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/x;->c:Lcom/google/android/gms/auth/trustagent/ae;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/auth/trustagent/x;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/x;->d()V

    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 141
    sget v0, Lcom/google/android/gms/p;->cP:I

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/x;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/x;->g:Z

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/x;->f:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/auth/trustagent/aa;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/trustagent/aa;-><init>(Lcom/google/android/gms/auth/trustagent/x;)V

    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 142
    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/auth/trustagent/x;)V
    .locals 3

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/x;->d()V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/x;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->bK:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/x;->d:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->cq:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 162
    return-void
.end method

.method static synthetic e(Lcom/google/android/gms/auth/trustagent/x;)Z
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/x;->g:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/gms/auth/trustagent/x;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/x;->e()V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 5

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/x;->e:Landroid/nfc/NfcAdapter;

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/x;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/gms/p;->cx:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/google/android/gms/auth/trustagent/ab;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/trustagent/ab;-><init>(Lcom/google/android/gms/auth/trustagent/x;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    sget v1, Lcom/google/android/gms/p;->cu:I

    new-instance v2, Lcom/google/android/gms/auth/trustagent/ac;

    invoke-direct {v2, p0}, Lcom/google/android/gms/auth/trustagent/ac;-><init>(Lcom/google/android/gms/auth/trustagent/x;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    sget v1, Lcom/google/android/gms/p;->cV:I

    new-instance v2, Lcom/google/android/gms/auth/trustagent/ad;

    invoke-direct {v2, p0}, Lcom/google/android/gms/auth/trustagent/ad;-><init>(Lcom/google/android/gms/auth/trustagent/x;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 88
    :goto_0
    return-void

    .line 67
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/x;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->bu:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/x;->d:Landroid/widget/TextView;

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/x;->e:Landroid/nfc/NfcAdapter;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/x;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/auth/trustagent/y;

    invoke-direct {v2, p0}, Lcom/google/android/gms/auth/trustagent/y;-><init>(Lcom/google/android/gms/auth/trustagent/x;)V

    const/16 v3, 0x9f

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/nfc/NfcAdapter;->enableReaderMode(Landroid/app/Activity;Landroid/nfc/NfcAdapter$ReaderCallback;ILandroid/os/Bundle;)V

    .line 87
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/x;->e()V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 49
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/trustagent/a;->onCreate(Landroid/os/Bundle;)V

    .line 50
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/x;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/x;->e:Landroid/nfc/NfcAdapter;

    .line 51
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/x;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/trustagent/ae;

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/x;->c:Lcom/google/android/gms/auth/trustagent/ae;

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/x;->e:Landroid/nfc/NfcAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/x;->c:Lcom/google/android/gms/auth/trustagent/ae;

    if-nez v0, :cond_1

    .line 54
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/x;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->cQ:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/x;->c:Lcom/google/android/gms/auth/trustagent/ae;

    invoke-interface {v0, v3, v3}, Lcom/google/android/gms/auth/trustagent/ae;->a(Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;Ljava/lang/String;)V

    .line 57
    :cond_1
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 100
    sget v0, Lcom/google/android/gms/l;->u:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onPause()V
    .locals 2

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/a;->b:Z

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/x;->e:Landroid/nfc/NfcAdapter;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/x;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/nfc/NfcAdapter;->disableReaderMode(Landroid/app/Activity;)V

    .line 95
    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/auth/trustagent/a;->onPause()V

    .line 96
    return-void
.end method
