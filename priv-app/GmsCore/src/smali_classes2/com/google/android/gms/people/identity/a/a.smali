.class public final Lcom/google/android/gms/people/identity/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final f:[Ljava/lang/String;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:I

.field public e:I

.field private g:Ljava/util/ArrayList;

.field private h:Ljava/util/HashMap;

.field private i:Z

.field private final j:Z

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:I

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:I

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/util/List;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 47
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.provider.ALTERNATE_CONTACTS_STRUCTURE"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.provider.CONTACTS_STRUCTURE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/people/identity/a/a;->f:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/people/identity/a/a;-><init>(Landroid/content/Context;Ljava/lang/String;ZB)V

    .line 145
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;ZB)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object v1, p0, Lcom/google/android/gms/people/identity/a/a;->a:Ljava/lang/String;

    .line 97
    iput-object v1, p0, Lcom/google/android/gms/people/identity/a/a;->b:Ljava/lang/String;

    .line 107
    iput v0, p0, Lcom/google/android/gms/people/identity/a/a;->d:I

    .line 109
    iput v0, p0, Lcom/google/android/gms/people/identity/a/a;->e:I

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/identity/a/a;->g:Ljava/util/ArrayList;

    .line 119
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/identity/a/a;->h:Ljava/util/HashMap;

    .line 155
    iput-boolean p3, p0, Lcom/google/android/gms/people/identity/a/a;->j:Z

    .line 156
    iput-object p2, p0, Lcom/google/android/gms/people/identity/a/a;->c:Ljava/lang/String;

    .line 158
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    .line 160
    invoke-static {p1, p2}, Lcom/google/android/gms/people/identity/a/a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;

    move-result-object v1

    .line 163
    if-eqz v1, :cond_0

    .line 168
    :try_start_0
    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/people/identity/a/a;->a(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_0
    .catch Lcom/google/android/gms/people/identity/a/b; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    :cond_0
    if-eqz v1, :cond_1

    .line 184
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    .line 188
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/identity/a/a;->v:Ljava/util/List;

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/people/identity/a/a;->n:Ljava/lang/String;

    const-string v1, "inviteContactActionLabel"

    invoke-static {p1, v0, p2, v1}, Lcom/google/android/gms/people/identity/a/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/identity/a/a;->o:I

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/people/identity/a/a;->r:Ljava/lang/String;

    const-string v1, "viewGroupActionLabel"

    invoke-static {p1, v0, p2, v1}, Lcom/google/android/gms/people/identity/a/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/identity/a/a;->s:I

    .line 193
    iget-object v0, p0, Lcom/google/android/gms/people/identity/a/a;->w:Ljava/lang/String;

    const-string v1, "accountTypeLabel"

    invoke-static {p1, v0, p2, v1}, Lcom/google/android/gms/people/identity/a/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/identity/a/a;->d:I

    .line 195
    iget-object v0, p0, Lcom/google/android/gms/people/identity/a/a;->x:Ljava/lang/String;

    const-string v1, "accountTypeIcon"

    invoke-static {p1, v0, p2, v1}, Lcom/google/android/gms/people/identity/a/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/identity/a/a;->e:I

    .line 199
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/people/identity/a/a;->i:Z

    .line 200
    :cond_2
    :goto_0
    return-void

    .line 170
    :catch_0
    move-exception v0

    .line 171
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 172
    const-string v3, "Problem reading XML"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    if-eqz v1, :cond_3

    .line 174
    const-string v3, " in line "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->getLineNumber()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 177
    :cond_3
    const-string v3, " for external package "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    const-string v3, "ExAccountType"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 183
    if-eqz v1, :cond_2

    .line 184
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    goto :goto_0

    .line 183
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 184
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_4
    throw v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 518
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 538
    :goto_0
    return v0

    .line 521
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x40

    if-eq v1, v2, :cond_1

    .line 522
    const-string v1, "ExAccountType"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must be a resource name beginning with \'@\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 525
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 528
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 533
    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3, p2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 534
    if-nez v1, :cond_2

    .line 535
    const-string v1, "ExAccountType"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to load "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from package "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 530
    :catch_0
    move-exception v1

    const-string v1, "ExAccountType"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to load package "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    move v0, v1

    .line 538
    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v2, 0x0

    .line 213
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 214
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.content.SyncAdapter"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 215
    const/16 v1, 0x84

    invoke-virtual {v3, v0, v1}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 218
    if-eqz v0, :cond_3

    .line 219
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 220
    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    .line 221
    if-eqz v5, :cond_0

    .line 222
    sget-object v6, Lcom/google/android/gms/people/identity/a/a;->f:[Ljava/lang/String;

    array-length v7, v6

    move v1, v2

    :goto_0
    if-ge v1, v7, :cond_0

    aget-object v8, v6, v1

    .line 225
    invoke-virtual {v5, v3, v8}, Landroid/content/pm/ServiceInfo;->loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;

    move-result-object v0

    .line 227
    if-eqz v0, :cond_2

    .line 228
    const-string v1, "ExAccountType"

    invoke-static {v1, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 229
    const-string v1, "ExAccountType"

    const-string v3, "Metadata loaded from: %s, %s, %s"

    new-array v4, v9, [Ljava/lang/Object;

    iget-object v6, v5, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    aput-object v6, v4, v2

    const/4 v2, 0x1

    iget-object v5, v5, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    aput-object v5, v4, v2

    const/4 v2, 0x2

    aput-object v8, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    :cond_1
    :goto_1
    return-object v0

    .line 224
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 240
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 344
    invoke-static {p2}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v1

    .line 348
    :cond_0
    :try_start_0
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    if-eq v2, v9, :cond_1

    if-ne v2, v8, :cond_0

    .line 353
    :cond_1
    if-eq v2, v9, :cond_2

    .line 354
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No start tag found"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 459
    :catch_0
    move-exception v0

    .line 460
    new-instance v1, Lcom/google/android/gms/people/identity/a/b;

    const-string v2, "Problem reading XML"

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/people/identity/a/b;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 357
    :cond_2
    :try_start_1
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 358
    const-string v3, "ContactsAccountType"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "ContactsSource"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 360
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Top level element must be ContactsAccountType, not "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 461
    :catch_1
    move-exception v0

    .line 462
    new-instance v1, Lcom/google/android/gms/people/identity/a/b;

    const-string v2, "Problem reading XML"

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/people/identity/a/b;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 364
    :cond_3
    const/4 v2, 0x1

    :try_start_2
    iput-boolean v2, p0, Lcom/google/android/gms/people/identity/a/a;->z:Z

    .line 366
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v2

    .line 367
    :goto_0
    if-ge v0, v2, :cond_14

    .line 368
    invoke-interface {p2, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v3

    .line 369
    invoke-interface {p2, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v4

    .line 370
    const-string v5, "ExAccountType"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 371
    const-string v5, "ExAccountType"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    :cond_4
    const-string v5, "editContactActivity"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 374
    iput-object v4, p0, Lcom/google/android/gms/people/identity/a/a;->k:Ljava/lang/String;

    .line 367
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 375
    :cond_5
    const-string v5, "createContactActivity"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 376
    iput-object v4, p0, Lcom/google/android/gms/people/identity/a/a;->l:Ljava/lang/String;

    goto :goto_1

    .line 377
    :cond_6
    const-string v5, "inviteContactActivity"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 378
    iput-object v4, p0, Lcom/google/android/gms/people/identity/a/a;->m:Ljava/lang/String;

    goto :goto_1

    .line 379
    :cond_7
    const-string v5, "inviteContactActionLabel"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 380
    iput-object v4, p0, Lcom/google/android/gms/people/identity/a/a;->n:Ljava/lang/String;

    goto :goto_1

    .line 381
    :cond_8
    const-string v5, "viewContactNotifyService"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 382
    iput-object v4, p0, Lcom/google/android/gms/people/identity/a/a;->p:Ljava/lang/String;

    goto :goto_1

    .line 383
    :cond_9
    const-string v5, "viewGroupActivity"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 384
    iput-object v4, p0, Lcom/google/android/gms/people/identity/a/a;->q:Ljava/lang/String;

    goto :goto_1

    .line 385
    :cond_a
    const-string v5, "viewGroupActionLabel"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 386
    iput-object v4, p0, Lcom/google/android/gms/people/identity/a/a;->r:Ljava/lang/String;

    goto :goto_1

    .line 387
    :cond_b
    const-string v5, "viewStreamItemActivity"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 388
    iput-object v4, p0, Lcom/google/android/gms/people/identity/a/a;->t:Ljava/lang/String;

    goto :goto_1

    .line 389
    :cond_c
    const-string v5, "viewStreamItemPhotoActivity"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 390
    iput-object v4, p0, Lcom/google/android/gms/people/identity/a/a;->u:Ljava/lang/String;

    goto :goto_1

    .line 391
    :cond_d
    const-string v5, "dataSet"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 392
    iput-object v4, p0, Lcom/google/android/gms/people/identity/a/a;->b:Ljava/lang/String;

    goto :goto_1

    .line 393
    :cond_e
    const-string v5, "extensionPackageNames"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 394
    iget-object v3, p0, Lcom/google/android/gms/people/identity/a/a;->v:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 395
    :cond_f
    const-string v5, "accountType"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 396
    iput-object v4, p0, Lcom/google/android/gms/people/identity/a/a;->a:Ljava/lang/String;

    goto :goto_1

    .line 397
    :cond_10
    const-string v5, "accountTypeLabel"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 398
    iput-object v4, p0, Lcom/google/android/gms/people/identity/a/a;->w:Ljava/lang/String;

    goto/16 :goto_1

    .line 399
    :cond_11
    const-string v5, "accountTypeIcon"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 400
    iput-object v4, p0, Lcom/google/android/gms/people/identity/a/a;->x:Ljava/lang/String;

    goto/16 :goto_1

    .line 401
    :cond_12
    const-string v5, "readOnly"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 402
    iput-object v4, p0, Lcom/google/android/gms/people/identity/a/a;->y:Ljava/lang/String;

    goto/16 :goto_1

    .line 404
    :cond_13
    const-string v4, "ExAccountType"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unsupported attribute "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 409
    :cond_14
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v0

    .line 410
    :cond_15
    :goto_2
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    if-ne v2, v10, :cond_16

    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v3

    if-le v3, v0, :cond_1d

    :cond_16
    if-eq v2, v8, :cond_1d

    .line 414
    if-ne v2, v9, :cond_15

    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v2

    add-int/lit8 v3, v0, 0x1

    if-ne v2, v3, :cond_15

    .line 415
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 419
    const-string v3, "ContactsDataKind"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 423
    const/4 v2, 0x3

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    .line 428
    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 430
    if-nez v2, :cond_17

    .line 431
    const-string v2, "ExAccountType"

    const-string v3, "Failed to obtain ContactsDataKind styled attributes"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 434
    :cond_17
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 435
    if-nez v3, :cond_18

    .line 436
    const-string v2, "ExAccountType"

    const-string v3, "Failed to obtain mimeType from ContactsDataKind styled attributes"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 441
    :cond_18
    new-instance v4, Lcom/google/android/gms/people/identity/a/e;

    invoke-direct {v4}, Lcom/google/android/gms/people/identity/a/e;-><init>()V

    .line 442
    iput-object v3, v4, Lcom/google/android/gms/people/identity/a/e;->b:Ljava/lang/String;

    .line 444
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 445
    if-eqz v3, :cond_19

    .line 446
    iput-object v3, v4, Lcom/google/android/gms/people/identity/a/e;->c:Ljava/lang/String;

    .line 448
    :cond_19
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 449
    if-eqz v3, :cond_1a

    .line 450
    iput-object v3, v4, Lcom/google/android/gms/people/identity/a/e;->d:Ljava/lang/String;

    .line 452
    :cond_1a
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 454
    iget-object v2, v4, Lcom/google/android/gms/people/identity/a/e;->b:Ljava/lang/String;

    if-nez v2, :cond_1b

    new-instance v0, Lcom/google/android/gms/people/identity/a/b;

    const-string v1, "null is not a valid mime type"

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/identity/a/b;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1b
    iget-object v2, p0, Lcom/google/android/gms/people/identity/a/a;->h:Ljava/util/HashMap;

    iget-object v3, v4, Lcom/google/android/gms/people/identity/a/e;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1c

    new-instance v0, Lcom/google/android/gms/people/identity/a/b;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mime type \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v4, Lcom/google/android/gms/people/identity/a/e;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' is already registered"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/identity/a/b;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1c
    iget-object v2, p0, Lcom/google/android/gms/people/identity/a/a;->c:Ljava/lang/String;

    iput-object v2, v4, Lcom/google/android/gms/people/identity/a/e;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/identity/a/a;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/google/android/gms/people/identity/a/a;->h:Ljava/util/HashMap;

    iget-object v3, v4, Lcom/google/android/gms/people/identity/a/e;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_2

    .line 463
    :cond_1d
    return-void

    .line 423
    nop

    :array_0
    .array-data 4
        0x1010026
        0x10102a2
        0x10102a3
    .end array-data
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/gms/people/identity/a/e;
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/android/gms/people/identity/a/a;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/a/e;

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 248
    iget-boolean v0, p0, Lcom/google/android/gms/people/identity/a/a;->i:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 259
    iget-boolean v0, p0, Lcom/google/android/gms/people/identity/a/a;->z:Z

    return v0
.end method

.method public final c()Ljava/util/List;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/gms/people/identity/a/a;->v:Ljava/util/List;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 543
    const-string v0, "AccountType<accountType=%s, dataSet=%s, resourcePackgeName=%s>"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/people/identity/a/a;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/people/identity/a/a;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/people/identity/a/a;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
