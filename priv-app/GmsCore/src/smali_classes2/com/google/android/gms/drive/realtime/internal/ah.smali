.class public abstract Lcom/google/android/gms/drive/realtime/internal/ah;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/realtime/internal/ag;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 20
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/gms/drive/realtime/internal/ah;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 21
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x1

    .line 43
    sparse-switch p1, :sswitch_data_0

    .line 545
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 47
    :sswitch_0
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v6

    .line 48
    goto :goto_0

    .line 52
    :sswitch_1
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 56
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/drive/realtime/internal/ak;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/aj;

    move-result-object v1

    .line 57
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/aj;)V

    .line 58
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 59
    goto :goto_0

    .line 63
    :sswitch_2
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 65
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/realtime/internal/d;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/c;

    move-result-object v0

    .line 66
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Lcom/google/android/gms/drive/realtime/internal/c;)V

    .line 67
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 68
    goto :goto_0

    .line 72
    :sswitch_3
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/realtime/internal/an;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/am;

    move-result-object v0

    .line 75
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Lcom/google/android/gms/drive/realtime/internal/am;)V

    .line 76
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 77
    goto :goto_0

    .line 81
    :sswitch_4
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 83
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/realtime/internal/d;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/c;

    move-result-object v0

    .line 84
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/realtime/internal/ah;->d(Lcom/google/android/gms/drive/realtime/internal/c;)V

    .line 85
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 86
    goto :goto_0

    .line 90
    :sswitch_5
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 92
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/realtime/internal/an;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/am;

    move-result-object v0

    .line 93
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/realtime/internal/ah;->b(Lcom/google/android/gms/drive/realtime/internal/am;)V

    .line 94
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 95
    goto :goto_0

    .line 99
    :sswitch_6
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 101
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/realtime/internal/ae;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/ad;

    move-result-object v0

    .line 102
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Lcom/google/android/gms/drive/realtime/internal/ad;)V

    .line 103
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 104
    goto/16 :goto_0

    .line 108
    :sswitch_7
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 110
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 112
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 114
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/drive/realtime/internal/m;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/l;

    move-result-object v2

    .line 115
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/l;)V

    .line 116
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 117
    goto/16 :goto_0

    .line 121
    :sswitch_8
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 123
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 125
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/drive/realtime/internal/ae;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/ad;

    move-result-object v1

    .line 126
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/ad;)V

    .line 127
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 128
    goto/16 :goto_0

    .line 132
    :sswitch_9
    const-string v1, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 134
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 136
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    .line 137
    sget-object v0, Lcom/google/android/gms/common/data/DataHolder;->CREATOR:Lcom/google/android/gms/common/data/p;

    invoke-static {p2}, Lcom/google/android/gms/common/data/p;->a(Landroid/os/Parcel;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 143
    :cond_0
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/drive/realtime/internal/y;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/x;

    move-result-object v2

    .line 144
    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Ljava/lang/String;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/drive/realtime/internal/x;)V

    .line 145
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 146
    goto/16 :goto_0

    .line 150
    :sswitch_a
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 152
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 154
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/drive/realtime/internal/y;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/x;

    move-result-object v1

    .line 155
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/x;)V

    .line 156
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 157
    goto/16 :goto_0

    .line 161
    :sswitch_b
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 163
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 165
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/drive/realtime/internal/m;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/l;

    move-result-object v1

    .line 166
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/l;)V

    .line 167
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 168
    goto/16 :goto_0

    .line 172
    :sswitch_c
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 174
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 176
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 178
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/drive/realtime/internal/p;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/o;

    move-result-object v2

    .line 179
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/o;)V

    .line 180
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 181
    goto/16 :goto_0

    .line 185
    :sswitch_d
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 187
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 189
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/drive/realtime/internal/ae;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/ad;

    move-result-object v1

    .line 190
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/realtime/internal/ah;->b(Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/ad;)V

    .line 191
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 192
    goto/16 :goto_0

    .line 196
    :sswitch_e
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 198
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 200
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/drive/realtime/internal/ak;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/aj;

    move-result-object v1

    .line 201
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/realtime/internal/ah;->b(Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/aj;)V

    .line 202
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 203
    goto/16 :goto_0

    .line 207
    :sswitch_f
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 209
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 211
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 213
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 215
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/drive/realtime/internal/y;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/x;

    move-result-object v3

    .line 216
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/drive/realtime/internal/x;)V

    .line 217
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 218
    goto/16 :goto_0

    .line 222
    :sswitch_10
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 224
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 226
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 228
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 230
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/drive/realtime/internal/y;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/x;

    move-result-object v3

    .line 231
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Ljava/lang/String;IILcom/google/android/gms/drive/realtime/internal/x;)V

    .line 232
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 233
    goto/16 :goto_0

    .line 237
    :sswitch_11
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 239
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 241
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 243
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/drive/realtime/internal/y;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/x;

    move-result-object v2

    .line 244
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/x;)V

    .line 245
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 246
    goto/16 :goto_0

    .line 250
    :sswitch_12
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 252
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 254
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/drive/realtime/internal/m;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/l;

    move-result-object v1

    .line 255
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/realtime/internal/ah;->b(Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/l;)V

    .line 256
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 257
    goto/16 :goto_0

    .line 261
    :sswitch_13
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 263
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 265
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/drive/realtime/internal/ae;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/ad;

    move-result-object v1

    .line 266
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/realtime/internal/ah;->c(Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/ad;)V

    .line 267
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 268
    goto/16 :goto_0

    .line 272
    :sswitch_14
    const-string v1, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 274
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 276
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 278
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_1

    .line 279
    sget-object v0, Lcom/google/android/gms/common/data/DataHolder;->CREATOR:Lcom/google/android/gms/common/data/p;

    invoke-static {p2}, Lcom/google/android/gms/common/data/p;->a(Landroid/os/Parcel;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 285
    :cond_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/drive/realtime/internal/y;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/x;

    move-result-object v3

    .line 286
    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Ljava/lang/String;ILcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/drive/realtime/internal/x;)V

    .line 287
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 288
    goto/16 :goto_0

    .line 292
    :sswitch_15
    const-string v1, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 294
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 296
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 298
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_2

    .line 299
    sget-object v0, Lcom/google/android/gms/common/data/DataHolder;->CREATOR:Lcom/google/android/gms/common/data/p;

    invoke-static {p2}, Lcom/google/android/gms/common/data/p;->a(Landroid/os/Parcel;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 305
    :cond_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/drive/realtime/internal/p;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/o;

    move-result-object v3

    .line 306
    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Ljava/lang/String;ILcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/drive/realtime/internal/o;)V

    .line 307
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 308
    goto/16 :goto_0

    .line 312
    :sswitch_16
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 314
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 316
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 318
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 320
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/drive/realtime/internal/p;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/o;

    move-result-object v3

    .line 321
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Ljava/lang/String;IILcom/google/android/gms/drive/realtime/internal/o;)V

    .line 322
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 323
    goto/16 :goto_0

    .line 327
    :sswitch_17
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 329
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 331
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 333
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 335
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 337
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/realtime/internal/y;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/x;

    move-result-object v5

    move-object v0, p0

    .line 338
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Ljava/lang/String;ILjava/lang/String;ILcom/google/android/gms/drive/realtime/internal/x;)V

    .line 339
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 340
    goto/16 :goto_0

    .line 344
    :sswitch_18
    const-string v1, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 346
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_3

    .line 347
    sget-object v0, Lcom/google/android/gms/drive/realtime/internal/BeginCompoundOperationRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/realtime/internal/BeginCompoundOperationRequest;

    .line 353
    :cond_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/drive/realtime/internal/an;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/am;

    move-result-object v1

    .line 354
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Lcom/google/android/gms/drive/realtime/internal/BeginCompoundOperationRequest;Lcom/google/android/gms/drive/realtime/internal/am;)V

    .line 355
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 356
    goto/16 :goto_0

    .line 360
    :sswitch_19
    const-string v1, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 362
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_4

    .line 363
    sget-object v0, Lcom/google/android/gms/drive/realtime/internal/EndCompoundOperationRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/realtime/internal/EndCompoundOperationRequest;

    .line 369
    :cond_4
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/drive/realtime/internal/y;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/x;

    move-result-object v1

    .line 370
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Lcom/google/android/gms/drive/realtime/internal/EndCompoundOperationRequest;Lcom/google/android/gms/drive/realtime/internal/x;)V

    .line 371
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 372
    goto/16 :goto_0

    .line 376
    :sswitch_1a
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 378
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/realtime/internal/y;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/x;

    move-result-object v0

    .line 379
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Lcom/google/android/gms/drive/realtime/internal/x;)V

    .line 380
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 381
    goto/16 :goto_0

    .line 385
    :sswitch_1b
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 387
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/realtime/internal/y;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/x;

    move-result-object v0

    .line 388
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/realtime/internal/ah;->b(Lcom/google/android/gms/drive/realtime/internal/x;)V

    .line 389
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 390
    goto/16 :goto_0

    .line 394
    :sswitch_1c
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 396
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/realtime/internal/d;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/c;

    move-result-object v0

    .line 397
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/realtime/internal/ah;->b(Lcom/google/android/gms/drive/realtime/internal/c;)V

    .line 398
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 399
    goto/16 :goto_0

    .line 403
    :sswitch_1d
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 405
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/realtime/internal/d;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/c;

    move-result-object v0

    .line 406
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/realtime/internal/ah;->c(Lcom/google/android/gms/drive/realtime/internal/c;)V

    .line 407
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 408
    goto/16 :goto_0

    .line 412
    :sswitch_1e
    const-string v1, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 414
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_5

    .line 415
    sget-object v0, Lcom/google/android/gms/drive/realtime/internal/ParcelableIndexReference;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/realtime/internal/ParcelableIndexReference;

    .line 421
    :cond_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/drive/realtime/internal/ak;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/aj;

    move-result-object v1

    .line 422
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Lcom/google/android/gms/drive/realtime/internal/ParcelableIndexReference;Lcom/google/android/gms/drive/realtime/internal/aj;)V

    .line 423
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 424
    goto/16 :goto_0

    .line 428
    :sswitch_1f
    const-string v1, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 430
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 432
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    if-nez v2, :cond_6

    .line 433
    :goto_1
    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/aa;)V

    .line 434
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 435
    goto/16 :goto_0

    .line 432
    :cond_6
    const-string v0, "com.google.android.gms.drive.realtime.internal.IIndexReferenceCallback"

    invoke-interface {v2, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_7

    instance-of v3, v0, Lcom/google/android/gms/drive/realtime/internal/aa;

    if-eqz v3, :cond_7

    check-cast v0, Lcom/google/android/gms/drive/realtime/internal/aa;

    goto :goto_1

    :cond_7
    new-instance v0, Lcom/google/android/gms/drive/realtime/internal/ac;

    invoke-direct {v0, v2}, Lcom/google/android/gms/drive/realtime/internal/ac;-><init>(Landroid/os/IBinder;)V

    goto :goto_1

    .line 439
    :sswitch_20
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 441
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 443
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 445
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/drive/realtime/internal/an;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/am;

    move-result-object v2

    .line 446
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Ljava/lang/String;ILcom/google/android/gms/drive/realtime/internal/am;)V

    .line 447
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 448
    goto/16 :goto_0

    .line 452
    :sswitch_21
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 454
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/realtime/internal/ae;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/ad;

    move-result-object v0

    .line 455
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/realtime/internal/ah;->b(Lcom/google/android/gms/drive/realtime/internal/ad;)V

    .line 456
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 457
    goto/16 :goto_0

    .line 461
    :sswitch_22
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 463
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 465
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/drive/realtime/internal/y;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/x;

    move-result-object v1

    .line 466
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(ILcom/google/android/gms/drive/realtime/internal/x;)V

    .line 467
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 468
    goto/16 :goto_0

    .line 472
    :sswitch_23
    const-string v1, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 474
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    if-nez v1, :cond_8

    .line 475
    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Lcom/google/android/gms/drive/realtime/internal/i;)V

    .line 476
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 477
    goto/16 :goto_0

    .line 474
    :cond_8
    const-string v0, "com.google.android.gms.drive.realtime.internal.ICollaboratorsCallback"

    invoke-interface {v1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_9

    instance-of v2, v0, Lcom/google/android/gms/drive/realtime/internal/i;

    if-eqz v2, :cond_9

    check-cast v0, Lcom/google/android/gms/drive/realtime/internal/i;

    goto :goto_2

    :cond_9
    new-instance v0, Lcom/google/android/gms/drive/realtime/internal/k;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/realtime/internal/k;-><init>(Landroid/os/IBinder;)V

    goto :goto_2

    .line 481
    :sswitch_24
    const-string v1, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 483
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    if-nez v1, :cond_a

    .line 484
    :goto_3
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Lcom/google/android/gms/drive/realtime/internal/f;)V

    .line 485
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 486
    goto/16 :goto_0

    .line 483
    :cond_a
    const-string v0, "com.google.android.gms.drive.realtime.internal.ICollaboratorEventCallback"

    invoke-interface {v1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_b

    instance-of v2, v0, Lcom/google/android/gms/drive/realtime/internal/f;

    if-eqz v2, :cond_b

    check-cast v0, Lcom/google/android/gms/drive/realtime/internal/f;

    goto :goto_3

    :cond_b
    new-instance v0, Lcom/google/android/gms/drive/realtime/internal/h;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/realtime/internal/h;-><init>(Landroid/os/IBinder;)V

    goto :goto_3

    .line 490
    :sswitch_25
    const-string v1, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 492
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    if-nez v1, :cond_c

    .line 493
    :goto_4
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Lcom/google/android/gms/drive/realtime/internal/u;)V

    .line 494
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 495
    goto/16 :goto_0

    .line 492
    :cond_c
    const-string v0, "com.google.android.gms.drive.realtime.internal.IErrorCallback"

    invoke-interface {v1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_d

    instance-of v2, v0, Lcom/google/android/gms/drive/realtime/internal/u;

    if-eqz v2, :cond_d

    check-cast v0, Lcom/google/android/gms/drive/realtime/internal/u;

    goto :goto_4

    :cond_d
    new-instance v0, Lcom/google/android/gms/drive/realtime/internal/w;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/realtime/internal/w;-><init>(Landroid/os/IBinder;)V

    goto :goto_4

    .line 499
    :sswitch_26
    const-string v1, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 501
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    if-nez v1, :cond_e

    .line 502
    :goto_5
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Lcom/google/android/gms/drive/realtime/internal/r;)V

    .line 503
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 504
    goto/16 :goto_0

    .line 501
    :cond_e
    const-string v0, "com.google.android.gms.drive.realtime.internal.IDocumentSaveStateEventCallback"

    invoke-interface {v1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_f

    instance-of v2, v0, Lcom/google/android/gms/drive/realtime/internal/r;

    if-eqz v2, :cond_f

    check-cast v0, Lcom/google/android/gms/drive/realtime/internal/r;

    goto :goto_5

    :cond_f
    new-instance v0, Lcom/google/android/gms/drive/realtime/internal/t;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/realtime/internal/t;-><init>(Landroid/os/IBinder;)V

    goto :goto_5

    .line 508
    :sswitch_27
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 510
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 512
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/drive/realtime/internal/an;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/am;

    move-result-object v1

    .line 513
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/am;)V

    .line 514
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 515
    goto/16 :goto_0

    .line 519
    :sswitch_28
    const-string v0, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 521
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 523
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/drive/realtime/internal/an;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/am;

    move-result-object v1

    .line 524
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/realtime/internal/ah;->b(Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/am;)V

    .line 525
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 526
    goto/16 :goto_0

    .line 530
    :sswitch_29
    const-string v1, "com.google.android.gms.drive.realtime.internal.IRealtimeService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 532
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_10

    .line 533
    sget-object v0, Lcom/google/android/gms/drive/realtime/internal/EndCompoundOperationRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/realtime/internal/EndCompoundOperationRequest;

    .line 539
    :cond_10
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/drive/realtime/internal/an;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/realtime/internal/am;

    move-result-object v1

    .line 540
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/realtime/internal/ah;->a(Lcom/google/android/gms/drive/realtime/internal/EndCompoundOperationRequest;Lcom/google/android/gms/drive/realtime/internal/am;)V

    .line 541
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v6

    .line 542
    goto/16 :goto_0

    .line 43
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_7
        0x5 -> :sswitch_8
        0x6 -> :sswitch_9
        0x7 -> :sswitch_a
        0x8 -> :sswitch_d
        0x9 -> :sswitch_e
        0xa -> :sswitch_f
        0xb -> :sswitch_10
        0xc -> :sswitch_11
        0xd -> :sswitch_12
        0xe -> :sswitch_13
        0xf -> :sswitch_14
        0x10 -> :sswitch_15
        0x11 -> :sswitch_16
        0x12 -> :sswitch_18
        0x13 -> :sswitch_29
        0x14 -> :sswitch_b
        0x15 -> :sswitch_c
        0x16 -> :sswitch_1a
        0x17 -> :sswitch_1b
        0x18 -> :sswitch_1c
        0x19 -> :sswitch_1d
        0x1a -> :sswitch_1e
        0x1b -> :sswitch_1f
        0x1c -> :sswitch_20
        0x1d -> :sswitch_21
        0x1e -> :sswitch_22
        0x1f -> :sswitch_23
        0x20 -> :sswitch_24
        0x21 -> :sswitch_4
        0x22 -> :sswitch_25
        0x23 -> :sswitch_5
        0x24 -> :sswitch_26
        0x25 -> :sswitch_17
        0x26 -> :sswitch_27
        0x27 -> :sswitch_28
        0x28 -> :sswitch_6
        0x29 -> :sswitch_19
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
