.class final Lcom/google/android/gms/auth/trustagent/trustlet/ba;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/trustagent/trustlet/az;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/trustagent/trustlet/az;)V
    .locals 0

    .prologue
    .line 407
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ba;->a:Lcom/google/android/gms/auth/trustagent/trustlet/az;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v13, 0x0

    const/4 v12, 0x4

    .line 407
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ba;->a:Lcom/google/android/gms/auth/trustagent/trustlet/az;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->a(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "auth_trust_agent_pref_trusted_place_home_work_account"

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Coffee - PlaceTrustlet"

    invoke-static {v0, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Coffee - PlaceTrustlet"

    const-string v1, "Account name is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-object v13

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ba;->a:Lcom/google/android/gms/auth/trustagent/trustlet/az;

    const-string v2, "Home"

    invoke-static {v0, v5, v2}, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a(Lcom/google/android/gms/auth/trustagent/trustlet/az;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ba;->a:Lcom/google/android/gms/auth/trustagent/trustlet/az;

    const-string v2, "Work"

    invoke-static {v0, v5, v2}, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a(Lcom/google/android/gms/auth/trustagent/trustlet/az;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "Coffee - PlaceTrustlet"

    invoke-static {v0, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Coffee - PlaceTrustlet"

    const-string v1, "Home and work are empty, no need to disable them."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/android/gms/common/api/w;

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ba;->a:Lcom/google/android/gms/auth/trustagent/trustlet/az;

    iget-object v2, v2, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a:Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    iget-object v2, v2, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/google/android/gms/location/places/q;->b:Lcom/google/android/gms/common/api/c;

    sget-object v3, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->a:Lcom/google/android/gms/location/places/s;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    iput-object v5, v0, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->c()Lcom/google/android/gms/common/c;

    sget-object v2, Lcom/google/android/gms/location/places/q;->c:Lcom/google/android/gms/location/places/u;

    sget-object v3, Lcom/google/android/gms/location/places/UserDataType;->b:Lcom/google/android/gms/location/places/UserDataType;

    invoke-interface {v2, v0, v3}, Lcom/google/android/gms/location/places/u;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/places/UserDataType;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    const-wide/16 v2, 0xa

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v8}, Lcom/google/android/gms/common/api/am;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/personalized/d;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/personalized/d;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v2

    if-nez v2, :cond_3

    const-string v0, "Coffee - PlaceTrustlet"

    invoke-static {v0, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Coffee - PlaceTrustlet"

    const-string v1, "Fetch account name fail."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v2, "Coffee - PlaceTrustlet"

    invoke-static {v2, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "Coffee - PlaceTrustlet"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v8, "Fetch account name "

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " successfully."

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/gms/location/places/personalized/d;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v1

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/personalized/PlaceUserData;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/personalized/PlaceUserData;->c()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v3, v2

    move v2, v1

    :cond_5
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/location/places/personalized/PlaceAlias;

    const-string v10, "Home"

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/personalized/PlaceAlias;->c()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/personalized/PlaceUserData;->b()Ljava/lang/String;

    move-result-object v1

    const-string v3, "Coffee - PlaceTrustlet"

    invoke-static {v3, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "Coffee - PlaceTrustlet"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Old home place id: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v3, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string v3, "Coffee - PlaceTrustlet"

    invoke-static {v3, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_7

    const-string v3, "Coffee - PlaceTrustlet"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "New home place id: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ba;->a:Lcom/google/android/gms/auth/trustagent/trustlet/az;

    invoke-virtual {v1, v6, v5}, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    move v3, v4

    goto :goto_2

    :cond_9
    const-string v10, "Work"

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/personalized/PlaceAlias;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/personalized/PlaceUserData;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Coffee - PlaceTrustlet"

    invoke-static {v2, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v2, "Coffee - PlaceTrustlet"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Old work place id: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v2, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    const-string v2, "Coffee - PlaceTrustlet"

    invoke-static {v2, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v2, "Coffee - PlaceTrustlet"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "New work place id: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ba;->a:Lcom/google/android/gms/auth/trustagent/trustlet/az;

    invoke-virtual {v1, v7, v5}, Lcom/google/android/gms/auth/trustagent/trustlet/az;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    move v2, v4

    goto/16 :goto_2

    :cond_d
    move v1, v2

    move v2, v3

    goto/16 :goto_1

    :cond_e
    if-nez v2, :cond_f

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ba;->a:Lcom/google/android/gms/auth/trustagent/trustlet/az;

    invoke-virtual {v0, v6, v5}, Lcom/google/android/gms/auth/trustagent/trustlet/az;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ba;->a:Lcom/google/android/gms/auth/trustagent/trustlet/az;

    invoke-virtual {v0, v7, v5}, Lcom/google/android/gms/auth/trustagent/trustlet/az;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
