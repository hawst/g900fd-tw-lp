.class public final Lcom/google/android/gms/common/util/bc;
.super Landroid/support/v4/app/m;
.source "SourceFile"


# instance fields
.field private j:Landroid/content/DialogInterface$OnCancelListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 138
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    .line 142
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/util/bc;->j:Landroid/content/DialogInterface$OnCancelListener;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/common/util/bc;
    .locals 3

    .prologue
    .line 145
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 146
    const-string v1, "ProgressDialogMessage"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    new-instance v1, Lcom/google/android/gms/common/util/bc;

    invoke-direct {v1}, Lcom/google/android/gms/common/util/bc;-><init>()V

    .line 148
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/util/bc;->a(Z)V

    .line 149
    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/util/bc;->setArguments(Landroid/os/Bundle;)V

    .line 150
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/google/android/gms/common/util/bc;->j:Landroid/content/DialogInterface$OnCancelListener;

    .line 168
    return-void
.end method

.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/google/android/gms/common/util/bc;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 156
    const-string v1, "ProgressDialogMessage"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 157
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/google/android/gms/common/util/bc;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 158
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 159
    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 161
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 162
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 163
    return-object v1
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 172
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onCancel(Landroid/content/DialogInterface;)V

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/common/util/bc;->j:Landroid/content/DialogInterface$OnCancelListener;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/common/util/bc;->j:Landroid/content/DialogInterface$OnCancelListener;

    invoke-interface {v0, p1}, Landroid/content/DialogInterface$OnCancelListener;->onCancel(Landroid/content/DialogInterface;)V

    .line 176
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/util/bc;->setRetainInstance(Z)V

    .line 189
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onCreate(Landroid/os/Bundle;)V

    .line 190
    return-void
.end method

.method public final onDestroyView()V
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Landroid/support/v4/app/m;->f:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/util/bc;->getRetainInstance()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Landroid/support/v4/app/m;->f:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 183
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/m;->onDestroyView()V

    .line 184
    return-void
.end method
