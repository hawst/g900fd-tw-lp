.class public final Lcom/google/android/gms/games/service/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/service/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Z)Ljava/lang/String;
    .locals 6

    .prologue
    .line 34
    const/4 v1, 0x0

    .line 35
    new-instance v2, Lcom/google/android/gms/common/server/a/a;

    const/4 v0, 0x1

    invoke-direct {v2, p2, v0}, Lcom/google/android/gms/common/server/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;Z)V

    .line 38
    :try_start_0
    invoke-virtual {v2, p1}, Lcom/google/android/gms/common/server/a/a;->b(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 39
    if-eqz p3, :cond_0

    .line 40
    :try_start_1
    invoke-static {p1, v0}, Lcom/google/android/gms/auth/r;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 41
    invoke-virtual {v2, p1}, Lcom/google/android/gms/common/server/a/a;->b(Landroid/content/Context;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 48
    :cond_0
    :goto_0
    return-object v0

    .line 43
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    .line 46
    :goto_1
    const-string v2, "RoomServiceClient"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error when getting auth token: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 43
    :catch_1
    move-exception v1

    goto :goto_1
.end method
