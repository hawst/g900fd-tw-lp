.class public final Lcom/google/android/gms/car/senderprotocol/ax;
.super Lcom/google/android/gms/car/senderprotocol/az;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/car/senderprotocol/ay;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/ga;Lcom/google/android/gms/car/gx;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/senderprotocol/az;-><init>(Lcom/google/android/gms/car/gw;Lcom/google/android/gms/car/gx;)V

    .line 36
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/ax;->a:Lcom/google/android/gms/car/senderprotocol/ay;

    .line 37
    return-void
.end method


# virtual methods
.method protected final a(ILjava/nio/ByteBuffer;)V
    .locals 4

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ax;->a:Lcom/google/android/gms/car/senderprotocol/ay;

    if-nez v0, :cond_0

    .line 77
    :goto_0
    return-void

    .line 70
    :cond_0
    const v0, 0x8002

    if-ne p1, v0, :cond_1

    .line 71
    new-instance v0, Lcom/google/android/c/b/ca;

    invoke-direct {v0}, Lcom/google/android/c/b/ca;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/ax;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/c/b/ca;

    .line 73
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ax;->a:Lcom/google/android/gms/car/senderprotocol/ay;

    invoke-virtual {v0}, Lcom/google/android/c/b/ca;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/c/b/ca;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/c/b/ca;->a:Lcom/google/android/c/b/an;

    iget v0, v0, Lcom/google/android/c/b/an;->a:I

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/car/senderprotocol/ay;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 75
    :cond_1
    const-string v0, "CAR.INST"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid message type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/CarPhoneStatus;)V
    .locals 4

    .prologue
    .line 40
    new-instance v1, Lcom/google/android/c/b/by;

    invoke-direct {v1}, Lcom/google/android/c/b/by;-><init>()V

    .line 41
    iget-object v0, p1, Lcom/google/android/gms/car/CarPhoneStatus;->b:[Lcom/google/android/gms/car/CarPhoneStatus$CarCall;

    if-eqz v0, :cond_4

    .line 42
    iget-object v0, p1, Lcom/google/android/gms/car/CarPhoneStatus;->b:[Lcom/google/android/gms/car/CarPhoneStatus$CarCall;

    array-length v0, v0

    new-array v0, v0, [Lcom/google/android/c/b/bz;

    iput-object v0, v1, Lcom/google/android/c/b/by;->a:[Lcom/google/android/c/b/bz;

    .line 43
    const/4 v0, 0x0

    :goto_0
    iget-object v2, v1, Lcom/google/android/c/b/by;->a:[Lcom/google/android/c/b/bz;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 44
    iget-object v2, v1, Lcom/google/android/c/b/by;->a:[Lcom/google/android/c/b/bz;

    new-instance v3, Lcom/google/android/c/b/bz;

    invoke-direct {v3}, Lcom/google/android/c/b/bz;-><init>()V

    aput-object v3, v2, v0

    .line 45
    iget-object v2, v1, Lcom/google/android/c/b/by;->a:[Lcom/google/android/c/b/bz;

    aget-object v2, v2, v0

    iget-object v3, p1, Lcom/google/android/gms/car/CarPhoneStatus;->b:[Lcom/google/android/gms/car/CarPhoneStatus$CarCall;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->b:I

    iput v3, v2, Lcom/google/android/c/b/bz;->a:I

    .line 46
    iget-object v2, v1, Lcom/google/android/c/b/by;->a:[Lcom/google/android/c/b/bz;

    aget-object v2, v2, v0

    iget-object v3, p1, Lcom/google/android/gms/car/CarPhoneStatus;->b:[Lcom/google/android/gms/car/CarPhoneStatus$CarCall;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->c:I

    iput v3, v2, Lcom/google/android/c/b/bz;->b:I

    .line 47
    iget-object v2, p1, Lcom/google/android/gms/car/CarPhoneStatus;->b:[Lcom/google/android/gms/car/CarPhoneStatus$CarCall;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 48
    iget-object v2, v1, Lcom/google/android/c/b/by;->a:[Lcom/google/android/c/b/bz;

    aget-object v2, v2, v0

    iget-object v3, p1, Lcom/google/android/gms/car/CarPhoneStatus;->b:[Lcom/google/android/gms/car/CarPhoneStatus$CarCall;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/c/b/bz;->a(Ljava/lang/String;)Lcom/google/android/c/b/bz;

    .line 50
    :cond_0
    iget-object v2, p1, Lcom/google/android/gms/car/CarPhoneStatus;->b:[Lcom/google/android/gms/car/CarPhoneStatus$CarCall;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->e:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 51
    iget-object v2, v1, Lcom/google/android/c/b/by;->a:[Lcom/google/android/c/b/bz;

    aget-object v2, v2, v0

    iget-object v3, p1, Lcom/google/android/gms/car/CarPhoneStatus;->b:[Lcom/google/android/gms/car/CarPhoneStatus$CarCall;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/c/b/bz;->b(Ljava/lang/String;)Lcom/google/android/c/b/bz;

    .line 53
    :cond_1
    iget-object v2, p1, Lcom/google/android/gms/car/CarPhoneStatus;->b:[Lcom/google/android/gms/car/CarPhoneStatus$CarCall;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->f:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 54
    iget-object v2, v1, Lcom/google/android/c/b/by;->a:[Lcom/google/android/c/b/bz;

    aget-object v2, v2, v0

    iget-object v3, p1, Lcom/google/android/gms/car/CarPhoneStatus;->b:[Lcom/google/android/gms/car/CarPhoneStatus$CarCall;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/c/b/bz;->c(Ljava/lang/String;)Lcom/google/android/c/b/bz;

    .line 56
    :cond_2
    iget-object v2, p1, Lcom/google/android/gms/car/CarPhoneStatus;->b:[Lcom/google/android/gms/car/CarPhoneStatus$CarCall;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->g:[B

    if-eqz v2, :cond_3

    .line 57
    iget-object v2, v1, Lcom/google/android/c/b/by;->a:[Lcom/google/android/c/b/bz;

    aget-object v2, v2, v0

    iget-object v3, p1, Lcom/google/android/gms/car/CarPhoneStatus;->b:[Lcom/google/android/gms/car/CarPhoneStatus$CarCall;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->g:[B

    invoke-virtual {v2, v3}, Lcom/google/android/c/b/bz;->a([B)Lcom/google/android/c/b/bz;

    .line 43
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 61
    :cond_4
    iget v0, p1, Lcom/google/android/gms/car/CarPhoneStatus;->c:I

    invoke-virtual {v1, v0}, Lcom/google/android/c/b/by;->a(I)Lcom/google/android/c/b/by;

    .line 62
    const v0, 0x8001

    invoke-static {v1}, Lcom/google/android/c/b/by;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/car/senderprotocol/ax;->a(I[B)V

    .line 63
    return-void
.end method
