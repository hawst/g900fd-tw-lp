.class public final Lcom/google/android/gms/wallet/q;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final synthetic a:Lcom/google/android/gms/wallet/MaskedWalletRequest;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/wallet/MaskedWalletRequest;)V
    .locals 0

    .prologue
    .line 257
    iput-object p1, p0, Lcom/google/android/gms/wallet/q;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 258
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/wallet/MaskedWalletRequest;B)V
    .locals 0

    .prologue
    .line 254
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/q;-><init>(Lcom/google/android/gms/wallet/MaskedWalletRequest;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/identity/intents/model/CountrySpecification;)Lcom/google/android/gms/wallet/q;
    .locals 2

    .prologue
    .line 389
    iget-object v0, p0, Lcom/google/android/gms/wallet/q;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iget-object v0, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->n:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/google/android/gms/wallet/q;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->n:Ljava/util/ArrayList;

    .line 394
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/q;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iget-object v0, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 395
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/wallet/q;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/gms/wallet/q;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-object p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->a:Ljava/lang/String;

    .line 267
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/gms/wallet/q;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/gms/wallet/q;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->b:Z

    .line 277
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/wallet/q;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/gms/wallet/q;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-object p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->e:Ljava/lang/String;

    .line 309
    return-object p0
.end method

.method public final b(Z)Lcom/google/android/gms/wallet/q;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/gms/wallet/q;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c:Z

    .line 286
    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/gms/wallet/q;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/gms/wallet/q;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-object p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->f:Ljava/lang/String;

    .line 317
    return-object p0
.end method

.method public final c(Z)Lcom/google/android/gms/wallet/q;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/gms/wallet/q;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->d:Z

    .line 296
    return-object p0
.end method

.method public final d(Ljava/lang/String;)Lcom/google/android/gms/wallet/q;
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/gms/wallet/q;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-object p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->g:Ljava/lang/String;

    .line 327
    return-object p0
.end method

.method public final d(Z)Lcom/google/android/gms/wallet/q;
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/android/gms/wallet/q;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->i:Z

    .line 350
    return-object p0
.end method

.method public final e(Z)Lcom/google/android/gms/wallet/q;
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/android/gms/wallet/q;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->j:Z

    .line 360
    return-object p0
.end method

.method public final f(Z)Lcom/google/android/gms/wallet/q;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/gms/wallet/q;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->l:Z

    .line 369
    return-object p0
.end method

.method public final g(Z)Lcom/google/android/gms/wallet/q;
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/google/android/gms/wallet/q;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->m:Z

    .line 378
    return-object p0
.end method
