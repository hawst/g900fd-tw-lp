.class final Lcom/google/android/gms/games/a/r;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/a/q;

.field private final b:Landroid/content/Context;

.field private final c:Landroid/net/Uri;

.field private final d:Ljava/util/ArrayList;

.field private final e:Ljava/util/ArrayList;

.field private final f:Ljava/util/HashMap;

.field private final g:Ljava/util/HashMap;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/a/q;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 1

    .prologue
    .line 682
    iput-object p1, p0, Lcom/google/android/gms/games/a/r;->a:Lcom/google/android/gms/games/a/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 683
    iput-object p2, p0, Lcom/google/android/gms/games/a/r;->b:Landroid/content/Context;

    .line 684
    invoke-static {p3}, Lcom/google/android/gms/games/provider/ad;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/a/r;->c:Landroid/net/Uri;

    .line 685
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/r;->d:Ljava/util/ArrayList;

    .line 686
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/r;->e:Ljava/util/ArrayList;

    .line 687
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/r;->f:Ljava/util/HashMap;

    .line 688
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/r;->g:Ljava/util/HashMap;

    .line 689
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 725
    iget-object v0, p0, Lcom/google/android/gms/games/a/r;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/a/r;->e:Ljava/util/ArrayList;

    const-string v2, "AppContentAgent"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 728
    const/4 v0, 0x0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v0

    :goto_0
    if-ge v3, v5, :cond_0

    .line 730
    iget-object v0, p0, Lcom/google/android/gms/games/a/r;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 733
    iget-object v1, p0, Lcom/google/android/gms/games/a/r;->g:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 734
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Landroid/content/ContentValues;

    .line 735
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 738
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentProviderResult;

    iget-object v0, v0, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    .line 739
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 728
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 741
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/ContentValues;Ljava/lang/String;Lcom/google/android/gms/games/h/a/bp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 703
    if-eqz p3, :cond_1

    .line 704
    invoke-virtual {p3}, Lcom/google/android/gms/games/h/a/bp;->d()Ljava/lang/String;

    move-result-object v0

    .line 705
    iget-object v1, p0, Lcom/google/android/gms/games/a/r;->c:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/gms/games/a/r;->e:Ljava/util/ArrayList;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v0

    .line 706
    iget-object v1, p0, Lcom/google/android/gms/games/a/r;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 707
    iget-object v1, p0, Lcom/google/android/gms/games/a/r;->g:Ljava/util/HashMap;

    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, p1, p4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 708
    invoke-virtual {p3}, Lcom/google/android/gms/games/h/a/bp;->b()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, p5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 709
    invoke-virtual {p3}, Lcom/google/android/gms/games/h/a/bp;->e()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, p6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 711
    iget-object v0, p0, Lcom/google/android/gms/games/a/r;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 712
    if-nez v0, :cond_0

    .line 713
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 715
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 716
    iget-object v1, p0, Lcom/google/android/gms/games/a/r;->f:Ljava/util/HashMap;

    invoke-virtual {v1, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 718
    :cond_1
    return-void
.end method
