.class public final Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;
.super Lcom/google/android/gms/games/ui/client/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/bo;
.implements Lcom/google/android/gms/games/ui/common/matches/v;


# static fields
.field private static final n:I

.field private static final o:I


# instance fields
.field private p:I

.field private q:I

.field private r:Lcom/google/android/gms/games/ui/client/matches/a;

.field private s:Landroid/support/v4/app/Fragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    sget v0, Lcom/google/android/gms/l;->aJ:I

    sput v0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->n:I

    .line 52
    sget v0, Lcom/google/android/gms/m;->p:I

    sput v0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->o:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 73
    sget v0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->n:I

    sget v1, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->o:I

    invoke-direct {p0, v0, v1, v2, v2}, Lcom/google/android/gms/games/ui/client/a;-><init>(IIZZ)V

    .line 75
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    .line 168
    if-eqz p1, :cond_0

    .line 169
    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->q:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCurrentFragment: unexpected index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->q:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/r;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/common/matches/r;-><init>()V

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->s:Landroid/support/v4/app/Fragment;

    .line 170
    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 171
    sget v1, Lcom/google/android/gms/j;->dw:I

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->s:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 172
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 179
    :goto_1
    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->q:I

    packed-switch v0, :pswitch_data_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setCurrentTitle: unexpected index: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->q:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :pswitch_1
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/k;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/common/matches/k;-><init>()V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/q;->b(I)Lcom/google/android/gms/games/ui/common/matches/q;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/q;->b(I)Lcom/google/android/gms/games/ui/common/matches/q;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/q;->b(I)Lcom/google/android/gms/games/ui/common/matches/q;

    move-result-object v0

    goto :goto_0

    .line 174
    :cond_0
    sget v0, Lcom/google/android/gms/j;->dw:I

    invoke-virtual {v1, v0}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->s:Landroid/support/v4/app/Fragment;

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->s:Landroid/support/v4/app/Fragment;

    const-string v1, "Failed to find fragment during resume!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 179
    :pswitch_5
    sget v0, Lcom/google/android/gms/p;->jS:I

    :goto_2
    if-lez v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->setTitle(I)V

    .line 180
    :goto_3
    return-void

    .line 179
    :pswitch_6
    sget v0, Lcom/google/android/gms/p;->iN:I

    goto :goto_2

    :pswitch_7
    sget v0, Lcom/google/android/gms/p;->iO:I

    goto :goto_2

    :pswitch_8
    sget v0, Lcom/google/android/gms/p;->iR:I

    goto :goto_2

    :pswitch_9
    sget v0, Lcom/google/android/gms/p;->iM:I

    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 169
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 179
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method


# virtual methods
.method protected final N()I
    .locals 1

    .prologue
    .line 163
    const/16 v0, 0xa

    return v0
.end method

.method public final T()Lcom/google/android/gms/games/ui/common/matches/u;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->r:Lcom/google/android/gms/games/ui/client/matches/a;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/games/i;)V
    .locals 1

    .prologue
    .line 119
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->a(Lcom/google/android/gms/games/i;)V

    .line 120
    const-string v0, "copresence"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/i;->a(Ljava/lang/String;)Lcom/google/android/gms/games/i;

    .line 121
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 305
    const/4 v0, -0x1

    .line 306
    const-string v2, "invitationsButton"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 316
    :cond_0
    :goto_0
    if-ltz v0, :cond_1

    iget v2, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->q:I

    if-eq v0, v2, :cond_1

    .line 317
    iput v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->q:I

    .line 318
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->b(Z)V

    .line 320
    :cond_1
    return-void

    .line 308
    :cond_2
    const-string v2, "myTurnButton"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 309
    const/4 v0, 0x2

    goto :goto_0

    .line 310
    :cond_3
    const-string v2, "theirTurnButton"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 311
    const/4 v0, 0x3

    goto :goto_0

    .line 312
    :cond_4
    const-string v2, "completedMatchesButton"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 313
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 243
    const/4 v0, 0x1

    return v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 137
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_2

    const/16 v0, 0x384

    if-ne p2, v0, :cond_2

    .line 141
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->s:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/matches/r;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->s:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/r;

    const-string v1, "com.google.android.gms.games.INVITATION_CLUSTER"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    const-string v2, "com.google.android.gms.games.REMOVE_CLUSTER"

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    const-string v3, "com.google.android.gms.games.REMOVED_ID_LIST"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/common/matches/r;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;ZLjava/util/ArrayList;)V

    .line 159
    :goto_0
    return-void

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->s:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/matches/k;

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->s:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/k;

    const-string v1, "com.google.android.gms.games.INVITATION_CLUSTER"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    const-string v2, "com.google.android.gms.games.REMOVE_CLUSTER"

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    const-string v3, "com.google.android.gms.games.REMOVED_ID_LIST"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/common/matches/k;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;ZLjava/util/ArrayList;)V

    goto :goto_0

    .line 152
    :cond_1
    const-string v0, "ClientMultiInboxAct"

    const-string v1, "onActivityResult received coming from the Public Invitation UI but the current fragment cannot go to this UI. Something is really weird."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 157
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/client/a;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 254
    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->p:I

    packed-switch v0, :pswitch_data_0

    .line 266
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid entry mode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->p:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 257
    :pswitch_0
    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->q:I

    if-eqz v0, :cond_0

    move v0, v1

    .line 268
    :goto_0
    if-eqz v0, :cond_1

    .line 269
    iput v2, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->q:I

    .line 270
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->b(Z)V

    .line 274
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 257
    goto :goto_0

    :pswitch_1
    move v0, v2

    .line 263
    goto :goto_0

    .line 272
    :cond_1
    invoke-super {p0}, Lcom/google/android/gms/games/ui/client/a;->onBackPressed()V

    goto :goto_1

    .line 254
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 79
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->onCreate(Landroid/os/Bundle;)V

    .line 82
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->l:Z

    .line 84
    new-instance v0, Lcom/google/android/gms/games/ui/client/matches/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/client/matches/a;-><init>(Lcom/google/android/gms/games/ui/client/a;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->r:Lcom/google/android/gms/games/ui/client/matches/a;

    .line 86
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 87
    const-string v3, "com.google.android.gms.games.SHOW_MULTIPLAYER_INBOX"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 88
    iput v1, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->p:I

    .line 97
    :goto_0
    if-nez p1, :cond_2

    move v0, v1

    .line 98
    :goto_1
    if-eqz v0, :cond_3

    .line 99
    iget v3, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->p:I

    packed-switch v3, :pswitch_data_0

    .line 109
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid entry mode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->p:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_0
    const-string v3, "com.google.android.gms.games.SHOW_INVITATIONS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 90
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->p:I

    goto :goto_0

    .line 92
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Intent action is invalid: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    move v0, v2

    .line 97
    goto :goto_1

    .line 101
    :pswitch_0
    iput v2, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->q:I

    .line 114
    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->b(Z)V

    .line 115
    return-void

    .line 105
    :pswitch_1
    iput v1, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->q:I

    goto :goto_2

    .line 112
    :cond_3
    const-string v1, "savedStateCurrentFragmentIndex"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->q:I

    goto :goto_2

    .line 99
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 278
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    .line 281
    sget v0, Lcom/google/android/gms/j;->lS:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 282
    invoke-static {v2}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 283
    sget-object v0, Lcom/google/android/gms/games/ui/n;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 285
    return v1
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 291
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->lS:I

    if-ne v0, v1, :cond_0

    .line 292
    invoke-static {p0}, Lcom/google/android/gms/games/ui/d/al;->d(Landroid/content/Context;)V

    .line 293
    const/4 v0, 0x1

    .line 295
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 125
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 127
    const-string v0, "savedStateCurrentFragmentIndex"

    iget v1, p0, Lcom/google/android/gms/games/ui/client/matches/ClientMultiplayerInboxActivity;->q:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 128
    return-void
.end method

.method protected final u()I
    .locals 1

    .prologue
    .line 248
    sget v0, Lcom/google/android/gms/l;->aS:I

    return v0
.end method
