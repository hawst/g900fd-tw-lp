.class public final Lcom/google/android/gms/common/a/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lcom/google/android/gms/common/a/d;

.field public static b:Lcom/google/android/gms/common/a/d;

.field public static c:Lcom/google/android/gms/common/a/d;

.field public static d:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 29
    const-string v0, "gms:common:system_health_log_delay_after_install_millis"

    const-wide/32 v2, 0xea60

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/a/c;->a:Lcom/google/android/gms/common/a/d;

    .line 34
    const-string v0, "gms:common:system_health_log_delay_after_boot_millis"

    const-wide/32 v2, 0x1b7740

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/a/c;->b:Lcom/google/android/gms/common/a/d;

    .line 39
    const-string v0, "gms:playlog:service:monitoring_interval"

    const-wide/32 v2, 0x5265c00

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/a/c;->c:Lcom/google/android/gms/common/a/d;

    .line 44
    const-string v0, "gms:common:upload_internal_stats_interval"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/a/c;->d:Lcom/google/android/gms/common/a/d;

    return-void
.end method
