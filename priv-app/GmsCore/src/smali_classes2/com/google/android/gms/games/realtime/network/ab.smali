.class final Lcom/google/android/gms/games/realtime/network/ab;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/realtime/network/aa;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/games/realtime/network/aa;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/gms/games/realtime/network/ab;->a:Lcom/google/android/gms/games/realtime/network/aa;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/games/realtime/network/aa;B)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/realtime/network/ab;-><init>(Lcom/google/android/gms/games/realtime/network/aa;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 65
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/ab;->a:Lcom/google/android/gms/games/realtime/network/aa;

    monitor-enter v1

    .line 66
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/ab;->a:Lcom/google/android/gms/games/realtime/network/aa;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 67
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 69
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/ab;->a:Lcom/google/android/gms/games/realtime/network/aa;

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/ab;->a:Lcom/google/android/gms/games/realtime/network/aa;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/network/aa;->e:Landroid/net/LocalServerSocket;

    invoke-virtual {v1}, Landroid/net/LocalServerSocket;->accept()Landroid/net/LocalSocket;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/realtime/network/aa;->f:Landroid/net/LocalSocket;

    .line 70
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/ab;->a:Lcom/google/android/gms/games/realtime/network/aa;

    iget-object v0, v1, Lcom/google/android/gms/games/realtime/network/aa;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, v1, Lcom/google/android/gms/games/realtime/network/aa;->c:Ljava/lang/String;

    aput-object v3, v0, v2
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    iget-object v2, v1, Lcom/google/android/gms/games/realtime/network/aa;->f:Landroid/net/LocalSocket;

    invoke-virtual {v2}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    iget-object v3, v1, Lcom/google/android/gms/games/realtime/network/aa;->f:Landroid/net/LocalSocket;

    invoke-virtual {v3}, Landroid/net/LocalSocket;->getReceiveBufferSize()I

    move-result v3

    const/16 v4, 0x490

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    new-array v3, v3, [B

    :goto_0
    invoke-virtual {v2, v3}, Ljava/io/InputStream;->read([B)I

    move-result v4

    if-lez v4, :cond_0

    const-string v5, "SocketProxy"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Forwarding "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " bytes."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/gms/games/internal/ej;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-array v5, v4, [B

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v3, v6, v5, v7, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v4, v1, Lcom/google/android/gms/games/realtime/network/aa;->a:Lcom/google/android/gms/games/realtime/network/b;

    invoke-interface {v4, v5, v0}, Lcom/google/android/gms/games/realtime/network/b;->a([B[Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v2, v1, Lcom/google/android/gms/games/realtime/network/aa;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    invoke-virtual {v1}, Lcom/google/android/gms/games/realtime/network/aa;->a()V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 75
    :catch_0
    move-exception v0

    .line 72
    const-string v1, "SocketProxy"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "IOException in readLoop"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/ej;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/ab;->a:Lcom/google/android/gms/games/realtime/network/aa;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/aa;->a:Lcom/google/android/gms/games/realtime/network/b;

    new-instance v1, Lcom/google/android/gms/games/realtime/network/o;

    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/ab;->a:Lcom/google/android/gms/games/realtime/network/aa;

    iget-object v2, v2, Lcom/google/android/gms/games/realtime/network/aa;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/realtime/network/ab;->a:Lcom/google/android/gms/games/realtime/network/aa;

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/games/realtime/network/o;-><init>(Ljava/lang/String;Lcom/google/android/gms/games/realtime/network/aa;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/realtime/network/b;->a(Lcom/google/android/gms/games/realtime/network/o;)V

    .line 76
    :goto_1
    return-void

    .line 67
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 70
    :cond_0
    :try_start_4
    iget-object v0, v1, Lcom/google/android/gms/games/realtime/network/aa;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    invoke-virtual {v1}, Lcom/google/android/gms/games/realtime/network/aa;->a()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1
.end method
