.class final Lcom/google/android/gms/games/a/y;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/a/u;

.field private final b:Ljava/util/HashMap;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/games/a/u;)V
    .locals 1

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/android/gms/games/a/y;->a:Lcom/google/android/gms/games/a/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/y;->b:Ljava/util/HashMap;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/games/a/u;B)V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/y;-><init>(Lcom/google/android/gms/games/a/u;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/a/y;Lcom/google/android/gms/games/a/au;Z)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 105
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/a/y;->b:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->d()Lcom/google/android/gms/games/b/m;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/games/a/y;->a:Lcom/google/android/gms/games/a/u;

    invoke-static {}, Lcom/google/android/gms/games/a/u;->a()Lcom/google/android/gms/common/util/p;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v4, v6

    const-wide/32 v6, 0x36ee80

    cmp-long v0, v4, v6

    if-gtz v0, :cond_1

    move v0, v2

    :goto_0
    if-nez v0, :cond_2

    :cond_0
    move v0, v2

    :goto_1
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/a/au;)V
    .locals 4

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/gms/games/a/y;->b:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->d()Lcom/google/android/gms/games/b/m;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/a/y;->a:Lcom/google/android/gms/games/a/u;

    invoke-static {}, Lcom/google/android/gms/games/a/u;->a()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    return-void
.end method
