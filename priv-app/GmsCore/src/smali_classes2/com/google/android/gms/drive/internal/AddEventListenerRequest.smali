.class public Lcom/google/android/gms/drive/internal/AddEventListenerRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field final b:Lcom/google/android/gms/drive/DriveId;

.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/drive/internal/a;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/internal/AddEventListenerRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/drive/DriveId;I)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput p1, p0, Lcom/google/android/gms/drive/internal/AddEventListenerRequest;->a:I

    .line 57
    iput-object p2, p0, Lcom/google/android/gms/drive/internal/AddEventListenerRequest;->b:Lcom/google/android/gms/drive/DriveId;

    .line 58
    iput p3, p0, Lcom/google/android/gms/drive/internal/AddEventListenerRequest;->c:I

    .line 59
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/DriveId;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/AddEventListenerRequest;->b:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/google/android/gms/drive/internal/AddEventListenerRequest;->c:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 43
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/drive/internal/a;->a(Lcom/google/android/gms/drive/internal/AddEventListenerRequest;Landroid/os/Parcel;I)V

    .line 44
    return-void
.end method
