.class public final Lcom/google/android/gms/maps/model/internal/CameraUpdateParcelable;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/maps/model/internal/b;


# instance fields
.field private final a:I

.field private b:I

.field private c:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/gms/maps/model/internal/b;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/internal/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/maps/model/internal/CameraUpdateParcelable;->CREATOR:Lcom/google/android/gms/maps/model/internal/b;

    return-void
.end method

.method constructor <init>(IILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput p1, p0, Lcom/google/android/gms/maps/model/internal/CameraUpdateParcelable;->a:I

    .line 48
    iput p2, p0, Lcom/google/android/gms/maps/model/internal/CameraUpdateParcelable;->b:I

    .line 49
    iput-object p3, p0, Lcom/google/android/gms/maps/model/internal/CameraUpdateParcelable;->c:Landroid/os/Bundle;

    .line 50
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/google/android/gms/maps/model/internal/CameraUpdateParcelable;->a:I

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/google/android/gms/maps/model/internal/CameraUpdateParcelable;->b:I

    return v0
.end method

.method public final c()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/gms/maps/model/internal/CameraUpdateParcelable;->c:Landroid/os/Bundle;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 58
    invoke-static {p0, p1}, Lcom/google/android/gms/maps/model/internal/b;->a(Lcom/google/android/gms/maps/model/internal/CameraUpdateParcelable;Landroid/os/Parcel;)V

    .line 59
    return-void
.end method
