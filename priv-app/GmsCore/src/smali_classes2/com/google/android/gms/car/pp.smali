.class final Lcom/google/android/gms/car/pp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/po;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/po;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/gms/car/pp;->a:Lcom/google/android/gms/car/po;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 34
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->mN:I

    if-ne v0, v1, :cond_1

    .line 35
    iget-object v0, p0, Lcom/google/android/gms/car/pp;->a:Lcom/google/android/gms/car/po;

    invoke-virtual {v0}, Lcom/google/android/gms/car/po;->a()Lcom/google/android/car/fsm/FsmController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/car/fsm/FsmController;->c()Lcom/google/android/car/fsm/g;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/pg;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/pg;->a(Z)V

    .line 37
    iget-object v0, p0, Lcom/google/android/gms/car/pp;->a:Lcom/google/android/gms/car/po;

    invoke-virtual {v0}, Lcom/google/android/gms/car/po;->a()Lcom/google/android/car/fsm/FsmController;

    move-result-object v0

    const-string v1, "EVENT_CAR_CONNECTION_ALLOWED"

    invoke-virtual {v0, v1}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/String;)V

    .line 43
    :cond_0
    :goto_0
    return-void

    .line 38
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->fI:I

    if-ne v0, v1, :cond_0

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/car/pp;->a:Lcom/google/android/gms/car/po;

    invoke-virtual {v0}, Lcom/google/android/gms/car/po;->a()Lcom/google/android/car/fsm/FsmController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/car/fsm/FsmController;->c()Lcom/google/android/car/fsm/g;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/pg;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/pg;->a(Z)V

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/car/pp;->a:Lcom/google/android/gms/car/po;

    invoke-virtual {v0}, Lcom/google/android/gms/car/po;->a()Lcom/google/android/car/fsm/FsmController;

    move-result-object v0

    const-string v1, "EVENT_CAR_CONNECTION_DISALLOWED"

    invoke-virtual {v0, v1}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
