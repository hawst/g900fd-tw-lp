.class final Lcom/google/android/gms/car/ir;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/nr;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/ip;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/ip;)V
    .locals 0

    .prologue
    .line 408
    iput-object p1, p0, Lcom/google/android/gms/car/ir;->a:Lcom/google/android/gms/car/ip;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 450
    const-string v0, "CAR.VIDEO"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 451
    const-string v0, "CAR.VIDEO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "video encoder returned error "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    :cond_0
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V
    .locals 3

    .prologue
    .line 411
    const-string v0, "CAR.VIDEO"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412
    const-string v0, "CAR.VIDEO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "codec config "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 415
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 416
    iget-object v1, p0, Lcom/google/android/gms/car/ir;->a:Lcom/google/android/gms/car/ip;

    invoke-static {v1}, Lcom/google/android/gms/car/ip;->b(Lcom/google/android/gms/car/ip;)Lcom/google/android/gms/car/senderprotocol/be;

    move-result-object v1

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/senderprotocol/be;->a([B)V

    .line 417
    return-void
.end method

.method public final b(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V
    .locals 6

    .prologue
    const/16 v5, 0x1000

    const/4 v4, 0x2

    .line 421
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 422
    const-string v0, "CAR.VIDEO"

    invoke-static {v0, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    if-le v0, v5, :cond_0

    .line 425
    const-string v0, "CAR.VIDEO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "data ready "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/car/senderprotocol/am;->a(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 428
    iget-object v1, p0, Lcom/google/android/gms/car/ir;->a:Lcom/google/android/gms/car/ip;

    invoke-static {v1}, Lcom/google/android/gms/car/ip;->b(Lcom/google/android/gms/car/ip;)Lcom/google/android/gms/car/senderprotocol/be;

    move-result-object v1

    iget-wide v2, p2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/gms/car/senderprotocol/be;->a(JLjava/nio/ByteBuffer;)V

    .line 429
    const-string v0, "CAR.VIDEO"

    invoke-static {v0, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    if-le v0, v5, :cond_1

    .line 430
    const-string v0, "CAR.VIDEO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "data sent "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/ir;->a:Lcom/google/android/gms/car/ip;

    invoke-static {v0}, Lcom/google/android/gms/car/ip;->c(Lcom/google/android/gms/car/ip;)Lcom/google/android/gms/car/iv;

    move-result-object v0

    .line 436
    if-eqz v0, :cond_2

    .line 437
    invoke-interface {v0}, Lcom/google/android/gms/car/iv;->a()V

    .line 439
    :cond_2
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 440
    return-void
.end method
