.class public final Lcom/google/android/gms/games/h/a/es;
.super Lcom/google/android/gms/common/server/response/c;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 67
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 69
    sput-object v0, Lcom/google/android/gms/games/h/a/es;->b:Ljava/util/HashMap;

    const-string v1, "connectionSetupLatencyMillis"

    const-string v2, "connectionSetupLatencyMillis"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sget-object v0, Lcom/google/android/gms/games/h/a/es;->b:Ljava/util/HashMap;

    const-string v1, "error"

    const-string v2, "error"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lcom/google/android/gms/games/h/a/es;->b:Ljava/util/HashMap;

    const-string v1, "error_reason"

    const-string v2, "error_reason"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcom/google/android/gms/games/h/a/es;->b:Ljava/util/HashMap;

    const-string v1, "participantId"

    const-string v2, "participantId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/google/android/gms/games/h/a/es;->b:Ljava/util/HashMap;

    const-string v1, "status"

    const-string v2, "status"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/google/android/gms/games/h/a/es;->b:Ljava/util/HashMap;

    const-string v1, "unreliableRoundtripLatencyMillis"

    const-string v2, "unreliableRoundtripLatencyMillis"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 2

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 92
    if-eqz p1, :cond_0

    .line 93
    const-string v0, "connectionSetupLatencyMillis"

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/h/a/es;->a(Ljava/lang/String;I)V

    .line 95
    :cond_0
    if-eqz p2, :cond_1

    .line 96
    const-string v0, "error"

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/games/h/a/es;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :cond_1
    if-eqz p3, :cond_2

    .line 102
    const-string v0, "participantId"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/games/h/a/es;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :cond_2
    if-eqz p4, :cond_3

    .line 105
    const-string v0, "status"

    invoke-virtual {p0, v0, p4}, Lcom/google/android/gms/games/h/a/es;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    :cond_3
    if-eqz p5, :cond_4

    .line 108
    const-string v0, "unreliableRoundtripLatencyMillis"

    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/h/a/es;->a(Ljava/lang/String;I)V

    .line 110
    :cond_4
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/google/android/gms/games/h/a/es;->b:Ljava/util/HashMap;

    return-object v0
.end method
