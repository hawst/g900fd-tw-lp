.class final Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;
.super Lcom/google/android/gms/games/service/statemachine/roomclient/r;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field private e:Lcom/google/android/gms/games/service/statemachine/roomclient/al;

.field private j:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V
    .locals 1

    .prologue
    .line 44
    const/16 v0, 0xa

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/r;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;I)V

    .line 45
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;)Lcom/google/android/gms/games/service/statemachine/roomclient/al;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->e:Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/google/android/gms/games/service/statemachine/roomclient/al;Lcom/google/android/gms/games/service/statemachine/roomclient/ce;)V
    .locals 1

    .prologue
    .line 52
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->e:Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    .line 53
    iput-object p3, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    .line 54
    iput-object p1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->k:Ljava/lang/String;

    .line 55
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->c()V

    .line 56
    return-void
.end method

.method public final b(Landroid/os/Message;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 60
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 131
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->g:Z

    :goto_0
    return v0

    .line 63
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 64
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->f:Z

    goto :goto_0

    .line 66
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bj;

    .line 68
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bj;->b:[B

    array-length v1, v1

    if-eqz v1, :cond_0

    .line 69
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bj;->b:[B

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    invoke-static {v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/cf;->a([B)Lcom/google/android/gms/games/g/ah;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/games/g/ag;->F:Lcom/google/android/gms/games/g/ah;

    .line 71
    :cond_0
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bj;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 73
    new-instance v1, Lcom/google/android/gms/games/h/a/ep;

    invoke-direct {v1}, Lcom/google/android/gms/games/h/a/ep;-><init>()V

    .line 75
    :try_start_0
    new-instance v2, Lcom/google/android/gms/common/server/response/d;

    invoke-direct {v2}, Lcom/google/android/gms/common/server/response/d;-><init>()V

    .line 77
    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bj;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/common/server/response/d;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    :try_end_0
    .catch Lcom/google/android/gms/common/server/response/m; {:try_start_0 .. :try_end_0} :catch_0

    move-object v6, v1

    .line 84
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->e:Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/al;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/u;

    iget-object v4, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/u;->c:Lcom/google/android/gms/games/internal/dr;

    .line 85
    iget-object v5, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->e:Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->c:Lcom/google/android/gms/games/service/statemachine/roomclient/bz;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->e:Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/al;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/u;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/u;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->e:Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/al;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/u;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/u;->b:Lcom/google/android/gms/common/server/ClientContext;

    new-instance v3, Lcom/google/android/gms/games/service/statemachine/roomclient/cl;

    invoke-direct {v3, p0, v4, v5}, Lcom/google/android/gms/games/service/statemachine/roomclient/cl;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/service/statemachine/roomclient/al;)V

    iget-object v4, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->e:Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    iget-object v4, v4, Lcom/google/android/gms/games/service/statemachine/roomclient/al;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/u;

    iget-object v4, v4, Lcom/google/android/gms/games/service/statemachine/roomclient/u;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->k:Ljava/lang/String;

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/games/service/statemachine/roomclient/bz;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/service/h;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/h/a/ep;)V

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->c()V

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->m:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitForHttpLeaveState;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitForHttpLeaveState;->a(Lcom/google/android/gms/games/service/statemachine/roomclient/ce;)V

    .line 97
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->f:Z

    goto :goto_0

    .line 78
    :catch_0
    move-exception v1

    .line 80
    const-string v2, "RoomServiceClientStateMachine"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception when parsing leave diagnostics: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bj;->a:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 100
    :sswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/br;

    .line 101
    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/br;->a:Lcom/google/android/gms/games/service/statemachine/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/g;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->c:Lcom/google/android/gms/games/service/statemachine/roomclient/bz;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->e:Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/al;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/u;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/u;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->e:Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/al;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/u;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/u;->b:Lcom/google/android/gms/common/server/ClientContext;

    new-instance v3, Lcom/google/android/gms/games/service/statemachine/roomclient/cm;

    invoke-direct {v3, p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/cm;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;)V

    iget-object v4, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->e:Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    iget-object v4, v4, Lcom/google/android/gms/games/service/statemachine/roomclient/al;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/u;

    iget-object v4, v4, Lcom/google/android/gms/games/service/statemachine/roomclient/u;->d:Ljava/lang/String;

    const-string v5, "REALTIME_ROOM_SERVICE_CRASHED"

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/games/service/statemachine/roomclient/bz;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/service/h;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/h/a/ep;)V

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;->b()V

    .line 123
    :cond_2
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->f:Z

    goto/16 :goto_0

    .line 128
    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 129
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->f:Z

    goto/16 :goto_0

    .line 60
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_1
        0xd -> :sswitch_2
        0x10 -> :sswitch_3
        0x1a -> :sswitch_3
        0x1d -> :sswitch_3
        0x27 -> :sswitch_0
        0x29 -> :sswitch_0
    .end sparse-switch
.end method
