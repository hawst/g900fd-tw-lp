.class final Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;
.super Lcom/google/android/gms/games/service/statemachine/roomclient/ca;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    .line 20
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 1

    .prologue
    .line 41
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 46
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;->g:Z

    :goto_0
    return v0

    .line 43
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;->b()V

    .line 44
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;->f:Z

    goto :goto_0

    .line 41
    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_0
    .end packed-switch
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;->c()V

    .line 27
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->f:Lcom/google/android/gms/games/service/statemachine/roomclient/cj;

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    const-string v1, "Unbinding from service"

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->f:Lcom/google/android/gms/games/service/statemachine/roomclient/cj;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/cj;->a()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->f:Lcom/google/android/gms/games/service/statemachine/roomclient/cj;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 34
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->f:Lcom/google/android/gms/games/service/statemachine/roomclient/cj;

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/af;

    invoke-direct {v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/af;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 37
    return-void
.end method
