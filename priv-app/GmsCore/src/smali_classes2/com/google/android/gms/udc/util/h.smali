.class public final Lcom/google/android/gms/udc/util/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/16 v0, 0x14

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/udc/util/h;->a:[B

    return-void

    :array_0
    .array-data 1
        -0x11t
        -0x53t
        0x18t
        -0x42t
        -0x34t
        0x37t
        0x1dt
        -0x9t
        -0x2dt
        0x56t
        -0x1t
        -0x23t
        -0x5dt
        0x5bt
        -0x2ft
        0x35t
        0x35t
        -0x78t
        -0x5ft
        0x17t
    .end array-data
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/udc/e/a;
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 124
    new-instance v2, Lcom/google/android/gms/udc/e/a;

    invoke-direct {v2}, Lcom/google/android/gms/udc/e/a;-><init>()V

    .line 125
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/android/gms/udc/util/b;->a()Lcom/google/android/gms/udc/util/b;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/udc/util/b;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/udc/util/b;->a()Lcom/google/android/gms/udc/util/b;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/udc/util/b;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    iput-object v0, v2, Lcom/google/android/gms/udc/e/a;->a:Ljava/lang/String;

    .line 126
    iget-object v0, v2, Lcom/google/android/gms/udc/e/a;->a:Ljava/lang/String;

    if-nez v0, :cond_2

    :goto_1
    return-object v1

    .line 125
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/udc/util/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v3, Lcom/google/android/gms/udc/util/i;

    invoke-direct {v3, v0}, Lcom/google/android/gms/udc/util/i;-><init>(Ljava/lang/String;)V

    const-string v0, "UdcConsistencyTokenPrefs"

    invoke-virtual {p0, v0, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v4, v3, Lcom/google/android/gms/udc/util/i;->a:Ljava/lang/String;

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, v3, Lcom/google/android/gms/udc/util/i;->c:Ljava/lang/String;

    const-wide/16 v6, 0x0

    invoke-interface {v0, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    iget-object v3, v3, Lcom/google/android/gms/udc/util/i;->b:Ljava/lang/String;

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v1, v2

    .line 126
    goto :goto_1
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 192
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 215
    :goto_0
    return-object v0

    .line 195
    :cond_0
    const-string v1, "MD5"

    invoke-static {v1}, Lcom/google/android/gms/common/util/e;->b(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 196
    if-nez v1, :cond_1

    .line 197
    const-string v1, "UdcUtils"

    const-string v2, "couldn\'t get a digest"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 200
    :cond_1
    sget-object v2, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 201
    sget-object v2, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/security/MessageDigest;->update([B)V

    .line 203
    :cond_2
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 204
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/security/MessageDigest;->update([B)V

    .line 208
    :cond_3
    sget-object v2, Lcom/google/android/gms/udc/util/h;->a:[B

    invoke-virtual {v1, v2}, Ljava/security/MessageDigest;->update([B)V

    .line 210
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    .line 211
    if-nez v1, :cond_4

    .line 212
    const-string v1, "UdcUtils"

    const-string v2, "couldn\'t digest account-id"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 215
    :cond_4
    const/4 v0, 0x0

    invoke-static {v1, v0}, Lcom/google/android/gms/common/util/e;->a([BZ)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 143
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    const-string v0, "UdcConsistencyTokenPrefs"

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v4, ":"

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v4, v0

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    aget-object v4, v0, v2

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    aget-object v0, v0, v2

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 145
    :cond_1
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v4

    array-length v5, v4

    move v1, v2

    :goto_1
    if-ge v1, v5, :cond_3

    aget-object v0, v4, v1

    .line 148
    :try_start_0
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/gms/auth/r;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/udc/util/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 150
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 151
    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    :cond_2
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 153
    :catch_0
    move-exception v0

    .line 154
    :goto_3
    const-string v6, "UdcUtils"

    const-string v7, "Could not retrieve accountId"

    invoke-static {v6, v7, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 158
    :cond_3
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 159
    if-eqz v0, :cond_4

    new-instance v3, Lcom/google/android/gms/udc/util/i;

    invoke-direct {v3, v0}, Lcom/google/android/gms/udc/util/i;-><init>(Ljava/lang/String;)V

    const-string v0, "UdcConsistencyTokenPrefs"

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v4, v3, Lcom/google/android/gms/udc/util/i;->a:Ljava/lang/String;

    invoke-interface {v0, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    iget-object v4, v3, Lcom/google/android/gms/udc/util/i;->b:Ljava/lang/String;

    invoke-interface {v0, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    iget-object v3, v3, Lcom/google/android/gms/udc/util/i;->c:Ljava/lang/String;

    invoke-interface {v0, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_4

    .line 161
    :cond_5
    return-void

    .line 153
    :catch_1
    move-exception v0

    goto :goto_3
.end method

.method public static a(Lcom/google/android/gms/udc/e/b;Landroid/content/Context;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 131
    const-string v0, "AccountId must not be empty"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 132
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/udc/e/b;->a:Lcom/google/c/d/a/a/c;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/udc/e/b;->a:Lcom/google/c/d/a/a/c;

    iget-object v0, v0, Lcom/google/c/d/a/a/c;->a:Ljava/lang/String;

    .line 134
    iget-object v1, p0, Lcom/google/android/gms/udc/e/b;->a:Lcom/google/c/d/a/a/c;

    iget v1, v1, Lcom/google/c/d/a/a/c;->b:I

    .line 135
    if-eqz v0, :cond_0

    .line 136
    invoke-static {p2}, Lcom/google/android/gms/udc/util/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v3, Lcom/google/android/gms/udc/util/i;

    invoke-direct {v3, v2}, Lcom/google/android/gms/udc/util/i;-><init>(Ljava/lang/String;)V

    const-string v2, "UdcConsistencyTokenPrefs"

    invoke-virtual {p1, v2, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    if-gtz v1, :cond_2

    iget-object v4, v3, Lcom/google/android/gms/udc/util/i;->a:Ljava/lang/String;

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v4, v3, Lcom/google/android/gms/udc/util/i;->b:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    iget-object v3, v3, Lcom/google/android/gms/udc/util/i;->c:Ljava/lang/String;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    if-nez v1, :cond_1

    invoke-static {}, Lcom/google/android/gms/udc/util/b;->a()Lcom/google/android/gms/udc/util/b;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/udc/util/b;->a(Ljava/lang/String;)V

    :goto_0
    invoke-static {v2}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 139
    :cond_0
    return-void

    .line 136
    :cond_1
    invoke-static {}, Lcom/google/android/gms/udc/util/b;->a()Lcom/google/android/gms/udc/util/b;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/udc/util/b;->a:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    iget-object v4, v3, Lcom/google/android/gms/udc/util/i;->a:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v4, v3, Lcom/google/android/gms/udc/util/i;->b:Ljava/lang/String;

    invoke-interface {v2, v4, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    iget-object v0, v3, Lcom/google/android/gms/udc/util/i;->c:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v6, v1

    add-long/2addr v4, v6

    invoke-interface {v2, v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-static {}, Lcom/google/android/gms/udc/util/b;->a()Lcom/google/android/gms/udc/util/b;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/udc/util/b;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
