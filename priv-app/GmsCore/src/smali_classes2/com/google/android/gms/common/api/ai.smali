.class public abstract Lcom/google/android/gms/common/api/ai;
.super Landroid/support/v4/a/a;
.source "SourceFile"


# instance fields
.field private final f:Lcom/google/android/gms/common/api/v;

.field private g:Lcom/google/android/gms/common/api/ap;

.field private h:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/api/v;)V
    .locals 2

    .prologue
    .line 77
    invoke-direct {p0, p1}, Landroid/support/v4/a/a;-><init>(Landroid/content/Context;)V

    .line 63
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/common/api/ai;->h:J

    .line 78
    iput-object p2, p0, Lcom/google/android/gms/common/api/ai;->f:Lcom/google/android/gms/common/api/v;

    .line 79
    return-void
.end method

.method private a(Lcom/google/android/gms/common/api/ap;)V
    .locals 1

    .prologue
    .line 181
    iput-object p1, p0, Lcom/google/android/gms/common/api/ai;->g:Lcom/google/android/gms/common/api/ap;

    .line 183
    iget-boolean v0, p0, Landroid/support/v4/a/j;->p:Z

    if-eqz v0, :cond_0

    .line 184
    invoke-super {p0, p1}, Landroid/support/v4/a/a;->b(Ljava/lang/Object;)V

    .line 186
    :cond_0
    return-void
.end method


# virtual methods
.method protected a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/ai;->k()Lcom/google/android/gms/common/api/l;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/ap;
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/ai;->k()Lcom/google/android/gms/common/api/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/api/l;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    return-object v0
.end method

.method public final a(JLjava/util/concurrent/TimeUnit;)V
    .locals 3

    .prologue
    .line 109
    iget-boolean v0, p0, Landroid/support/v4/a/j;->r:Z

    const-string v1, "Can only setTimeout while loader is reset"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 110
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/common/api/ai;->h:J

    .line 111
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 169
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v4/a/a;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 170
    iget-wide v0, p0, Lcom/google/android/gms/common/api/ai;->h:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 171
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 172
    const-string v0, "mTimeout="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 173
    iget-wide v0, p0, Lcom/google/android/gms/common/api/ai;->h:J

    invoke-virtual {p3, v0, v1}, Ljava/io/PrintWriter;->print(J)V

    .line 174
    const-string v0, "ms"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 176
    :cond_0
    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 56
    check-cast p1, Lcom/google/android/gms/common/api/ap;

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/ai;->a(Lcom/google/android/gms/common/api/ap;)V

    return-void
.end method

.method public final synthetic d()Ljava/lang/Object;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 56
    iget-wide v0, p0, Lcom/google/android/gms/common/api/ai;->h:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/ai;->f:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->c()Lcom/google/android/gms/common/c;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/gms/common/api/Status;->e:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/ai;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/ai;->f:Lcom/google/android/gms/common/api/v;

    iget-wide v2, p0, Lcom/google/android/gms/common/api/ai;->h:J

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/gms/common/api/v;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/c;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/ai;->f:Lcom/google/android/gms/common/api/v;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/ai;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/common/api/ai;->h:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_2

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-wide v2, p0, Lcom/google/android/gms/common/api/ai;->h:J

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/gms/common/api/am;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    goto :goto_1
.end method

.method protected final e()V
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/gms/common/api/ai;->g:Lcom/google/android/gms/common/api/ap;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/google/android/gms/common/api/ai;->g:Lcom/google/android/gms/common/api/ap;

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/ai;->a(Lcom/google/android/gms/common/api/ap;)V

    .line 200
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/ai;->i()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/api/ai;->g:Lcom/google/android/gms/common/api/ap;

    if-nez v0, :cond_2

    .line 201
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/a/j;->a()V

    .line 203
    :cond_2
    return-void
.end method

.method protected final f()V
    .locals 0

    .prologue
    .line 208
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/ai;->b()Z

    .line 209
    return-void
.end method

.method protected final g()V
    .locals 1

    .prologue
    .line 213
    invoke-super {p0}, Landroid/support/v4/a/a;->g()V

    .line 216
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/ai;->b()Z

    .line 218
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/ai;->g:Lcom/google/android/gms/common/api/ap;

    .line 219
    return-void
.end method

.method protected abstract k()Lcom/google/android/gms/common/api/l;
.end method
