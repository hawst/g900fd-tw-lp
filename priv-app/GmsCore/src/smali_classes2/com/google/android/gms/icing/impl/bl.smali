.class public final Lcom/google/android/gms/icing/impl/bl;
.super Lcom/google/android/gms/icing/b/h;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/icing/impl/u;

.field private final b:Z

.field private final c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/impl/u;ZZ)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/b/h;-><init>(I)V

    .line 39
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/bl;->a:Lcom/google/android/gms/icing/impl/u;

    .line 40
    iput-boolean p2, p0, Lcom/google/android/gms/icing/impl/bl;->b:Z

    .line 41
    iput-boolean p3, p0, Lcom/google/android/gms/icing/impl/bl;->c:Z

    .line 42
    return-void
.end method

.method private c()Ljava/lang/Void;
    .locals 25

    .prologue
    .line 47
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/bl;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/u;->C()Lcom/google/android/gms/icing/impl/NativeIndex;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->r()Z

    .line 49
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/bl;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/u;->j()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v10

    .line 50
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 52
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 53
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/icing/impl/bl;->a:Lcom/google/android/gms/icing/impl/u;

    iget-object v5, v5, Lcom/google/android/gms/icing/impl/u;->h:Lcom/google/android/gms/icing/impl/e/i;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/icing/impl/bl;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v6}, Lcom/google/android/gms/icing/impl/u;->z()Landroid/content/Context;

    move-result-object v6

    new-instance v7, Lcom/google/android/gms/icing/impl/e/j;

    invoke-direct {v7, v6}, Lcom/google/android/gms/icing/impl/e/j;-><init>(Landroid/content/Context;)V

    const-string v6, "getFileDescriptor can not be called on main thread"

    invoke-static {v6}, Lcom/google/android/gms/common/internal/bx;->c(Ljava/lang/String;)V

    iget-object v6, v7, Lcom/google/android/gms/icing/impl/e/j;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    const/4 v6, 0x0

    iput-object v6, v7, Lcom/google/android/gms/icing/impl/e/j;->d:Landroid/os/ParcelFileDescriptor;

    iget-object v6, v7, Lcom/google/android/gms/icing/impl/e/j;->b:Landroid/os/ConditionVariable;

    invoke-virtual {v6}, Landroid/os/ConditionVariable;->close()V

    iget-object v6, v7, Lcom/google/android/gms/icing/impl/e/j;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v6}, Lcom/google/android/gms/common/api/v;->b()V

    iget-object v6, v7, Lcom/google/android/gms/icing/impl/e/j;->b:Landroid/os/ConditionVariable;

    const-wide/16 v8, 0x7d0

    invoke-virtual {v6, v8, v9}, Landroid/os/ConditionVariable;->block(J)Z

    move-result v6

    iget-object v8, v7, Lcom/google/android/gms/icing/impl/e/j;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    if-nez v6, :cond_0

    const-string v6, "Usage reports not received in time."

    invoke-static {v6}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;)I

    :cond_0
    iget-object v6, v7, Lcom/google/android/gms/icing/impl/e/j;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v6}, Lcom/google/android/gms/common/api/v;->d()V

    iget-object v6, v7, Lcom/google/android/gms/icing/impl/e/j;->d:Landroid/os/ParcelFileDescriptor;

    invoke-static {v6}, Lcom/google/android/gms/icing/impl/e/h;->a(Landroid/os/ParcelFileDescriptor;)Ljava/util/Iterator;

    move-result-object v7

    new-instance v12, Lcom/google/android/gms/icing/impl/e/m;

    iget-object v5, v5, Lcom/google/android/gms/icing/impl/e/i;->a:Lcom/google/android/gms/icing/impl/e/k;

    invoke-direct {v12, v7, v6, v5}, Lcom/google/android/gms/icing/impl/e/m;-><init>(Ljava/util/Iterator;Landroid/os/ParcelFileDescriptor;Lcom/google/android/gms/icing/impl/e/k;)V

    .line 57
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/gms/icing/impl/bl;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v5, :cond_f

    .line 60
    invoke-virtual {v12}, Lcom/google/android/gms/icing/impl/e/m;->b()V

    const/4 v2, 0x0

    .line 147
    :goto_0
    return-object v2

    .line 77
    :catch_0
    move-exception v2

    .line 78
    :try_start_1
    const-string v3, "Implicit corpus from usage report failed: %s"

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/b/a;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    .line 62
    :goto_1
    invoke-virtual {v12}, Lcom/google/android/gms/icing/impl/e/m;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 63
    invoke-virtual {v12}, Lcom/google/android/gms/icing/impl/e/m;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/google/android/gms/icing/impl/e/l;

    move-object v3, v0

    .line 64
    const/4 v9, 0x0

    .line 66
    const/4 v8, 0x0

    .line 67
    iget-object v2, v3, Lcom/google/android/gms/icing/impl/e/l;->b:Lcom/google/android/gms/icing/g;

    if-nez v2, :cond_3

    const/4 v2, 0x1

    :goto_2
    if-nez v2, :cond_4

    .line 71
    iget-object v4, v3, Lcom/google/android/gms/icing/impl/e/l;->b:Lcom/google/android/gms/icing/g;

    move/from16 v24, v8

    move-object v8, v9

    move-object v9, v4

    move v4, v5

    move v5, v6

    move/from16 v6, v24

    .line 107
    :goto_3
    invoke-virtual {v10, v9}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/g;)Ljava/lang/String;

    move-result-object v13

    .line 108
    invoke-interface {v11, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/icing/impl/az;

    .line 109
    if-nez v2, :cond_1

    .line 110
    invoke-virtual {v10, v13}, Lcom/google/android/gms/icing/impl/a/j;->f(Ljava/lang/String;)Lcom/google/android/gms/icing/j;

    move-result-object v14

    .line 111
    new-instance v2, Lcom/google/android/gms/icing/impl/az;

    iget-object v14, v14, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    const-wide/16 v16, 0x0

    move-wide/from16 v0, v16

    invoke-direct {v2, v14, v0, v1}, Lcom/google/android/gms/icing/impl/az;-><init>([Lcom/google/android/gms/icing/k;J)V

    .line 112
    invoke-interface {v11, v13, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    :cond_1
    if-eqz v8, :cond_2

    .line 115
    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "push index "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/google/android/gms/icing/impl/az;->a(Ljava/lang/String;)V

    .line 118
    :cond_2
    if-eqz v6, :cond_a

    .line 121
    const-string v3, "push index rejected rate limit"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/impl/az;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v6, v5

    move v5, v4

    goto :goto_1

    .line 67
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 75
    :cond_4
    :try_start_2
    iget-object v2, v3, Lcom/google/android/gms/icing/impl/e/l;->c:Lcom/google/android/gms/icing/impl/a/aa;

    iget-object v4, v3, Lcom/google/android/gms/icing/impl/e/l;->a:Lcom/google/android/gms/icing/bf;

    iget-object v4, v4, Lcom/google/android/gms/icing/bf;->h:Lcom/google/android/gms/icing/o;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/gms/icing/impl/bl;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v13}, Lcom/google/android/gms/icing/impl/u;->A()Lcom/google/android/gms/icing/impl/a/f;

    move-result-object v13

    invoke-virtual {v13, v2}, Lcom/google/android/gms/icing/impl/a/f;->a(Lcom/google/android/gms/icing/impl/a/aa;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v13

    invoke-virtual {v13, v14, v15}, Lcom/google/android/gms/icing/impl/a/e;->a(J)Z
    :try_end_2
    .catch Lcom/google/android/gms/icing/impl/b/a; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/google/android/gms/icing/impl/b/d; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v16

    if-nez v16, :cond_7

    const/4 v2, 0x0

    move-object v4, v2

    .line 90
    :goto_4
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/bl;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/u;->j()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v13

    invoke-virtual {v13, v4}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/g;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/google/android/gms/icing/impl/a/j;->f(Ljava/lang/String;)Lcom/google/android/gms/icing/j;

    move-result-object v15

    if-eqz v15, :cond_8

    invoke-static {v4}, Lcom/google/android/gms/icing/impl/a/j;->f(Lcom/google/android/gms/icing/g;)Z

    move-result v2

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/bl;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/u;->C()Lcom/google/android/gms/icing/impl/NativeIndex;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->k()Lcom/google/android/gms/icing/r;

    move-result-object v2

    iget-object v0, v2, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v16, v0

    iget v0, v4, Lcom/google/android/gms/icing/g;->a:I

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    if-le v0, v1, :cond_8

    iget-object v2, v2, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    iget v0, v4, Lcom/google/android/gms/icing/g;->a:I

    move/from16 v16, v0

    aget-object v2, v2, v16

    iget-wide v0, v2, Lcom/google/android/gms/icing/s;->c:J

    move-wide/from16 v16, v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    iget-object v2, v15, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;

    if-eqz v2, :cond_5

    iget-object v2, v15, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;

    iget-wide v0, v2, Lcom/google/android/gms/icing/l;->a:J

    move-wide/from16 v20, v0

    sub-long v20, v18, v20

    sget-object v2, Lcom/google/android/gms/icing/a/a;->y:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    cmp-long v2, v20, v22

    if-lez v2, :cond_6

    :cond_5
    new-instance v2, Lcom/google/android/gms/icing/l;

    invoke-direct {v2}, Lcom/google/android/gms/icing/l;-><init>()V

    iput-object v2, v15, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;

    iget-object v2, v15, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;

    move-wide/from16 v0, v18

    iput-wide v0, v2, Lcom/google/android/gms/icing/l;->a:J

    iget-object v2, v15, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;

    move-wide/from16 v0, v16

    iput-wide v0, v2, Lcom/google/android/gms/icing/l;->b:J

    const/4 v2, 0x0

    const/16 v18, 0x0

    iget-object v0, v15, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v13, v14, v2, v0, v1}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;[Lcom/google/android/gms/icing/k;Ljava/lang/Integer;Lcom/google/android/gms/icing/l;)V

    :cond_6
    iget-object v2, v15, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;

    iget-wide v14, v2, Lcom/google/android/gms/icing/l;->b:J

    sub-long v14, v16, v14

    sget-object v2, Lcom/google/android/gms/icing/a/a;->z:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    cmp-long v2, v14, v16

    if-ltz v2, :cond_8

    const-string v2, "Too many pushes from %s"

    iget-object v13, v4, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-static {v2, v13}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;Ljava/lang/Object;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v2, 0x0

    :goto_5
    if-nez v2, :cond_9

    .line 92
    const/4 v2, 0x1

    .line 93
    add-int/lit8 v5, v5, 0x1

    move-object v8, v9

    move-object v9, v4

    move v4, v5

    move v5, v6

    move v6, v2

    goto/16 :goto_3

    .line 75
    :cond_7
    :try_start_4
    iget-object v0, v2, Lcom/google/android/gms/icing/impl/a/aa;->e:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v0, v4}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Lcom/google/android/gms/icing/o;)Lcom/google/android/gms/icing/g;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/bl;->a:Lcom/google/android/gms/icing/impl/u;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v4, v14, v15}, Lcom/google/android/gms/icing/impl/a/ac;->a(Ljava/lang/Object;J)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v4

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v2, v13, v1, v4}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/impl/a/aa;Lcom/google/android/gms/icing/impl/a/e;ZLcom/google/android/gms/icing/impl/a/ac;)Lcom/google/android/gms/icing/g;
    :try_end_4
    .catch Lcom/google/android/gms/icing/impl/b/a; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lcom/google/android/gms/icing/impl/b/d; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v2

    move-object v4, v2

    goto/16 :goto_4

    .line 81
    :catch_1
    move-exception v2

    .line 82
    :try_start_5
    const-string v3, "Implicit corpus from usage report failed: %s"

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/b/d;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 129
    :catchall_0
    move-exception v2

    invoke-virtual {v12}, Lcom/google/android/gms/icing/impl/e/m;->b()V

    throw v2

    .line 85
    :catch_2
    move-exception v2

    .line 86
    :try_start_6
    const-string v3, "Implicit corpus from usage report failed: %s"

    invoke-virtual {v2}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto/16 :goto_1

    .line 90
    :cond_8
    const/4 v2, 0x1

    goto :goto_5

    .line 95
    :cond_9
    iget-object v2, v3, Lcom/google/android/gms/icing/impl/e/l;->a:Lcom/google/android/gms/icing/bf;

    iget-object v2, v2, Lcom/google/android/gms/icing/bf;->h:Lcom/google/android/gms/icing/o;

    .line 97
    iget v9, v4, Lcom/google/android/gms/icing/g;->a:I

    iput v9, v2, Lcom/google/android/gms/icing/o;->a:I

    .line 99
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/bl;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/u;->C()Lcom/google/android/gms/icing/impl/NativeIndex;

    move-result-object v2

    const-wide/16 v14, 0x0

    iget-object v9, v3, Lcom/google/android/gms/icing/impl/e/l;->a:Lcom/google/android/gms/icing/bf;

    iget-object v9, v9, Lcom/google/android/gms/icing/bf;->h:Lcom/google/android/gms/icing/o;

    invoke-virtual {v2, v14, v15, v9}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(JLcom/google/android/gms/icing/o;)I

    move-result v2

    .line 102
    invoke-static {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->e(I)Ljava/lang/String;

    move-result-object v2

    .line 103
    add-int/lit8 v6, v6, 0x1

    move-object v9, v4

    move v4, v5

    move v5, v6

    move v6, v8

    move-object v8, v2

    goto/16 :goto_3

    .line 123
    :cond_a
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/icing/impl/bl;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v6}, Lcom/google/android/gms/icing/impl/u;->C()Lcom/google/android/gms/icing/impl/NativeIndex;

    move-result-object v6

    iget-object v3, v3, Lcom/google/android/gms/icing/impl/e/l;->a:Lcom/google/android/gms/icing/bf;

    invoke-static {v9, v3}, Lcom/google/android/gms/icing/impl/e/i;->a(Lcom/google/android/gms/icing/g;Lcom/google/android/gms/icing/bf;)Lcom/google/android/gms/icing/ag;

    move-result-object v3

    invoke-virtual {v6, v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(Lcom/google/android/gms/icing/ag;)V

    .line 124
    const-string v3, "usage report"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/impl/az;->a(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 125
    add-int/lit8 v2, v7, 0x1

    move v6, v5

    move v7, v2

    move v5, v4

    .line 127
    goto/16 :goto_1

    .line 129
    :cond_b
    invoke-virtual {v12}, Lcom/google/android/gms/icing/impl/e/m;->b()V

    .line 132
    if-lez v7, :cond_d

    .line 133
    invoke-interface {v11}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 134
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/icing/impl/az;

    .line 135
    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/az;->a()V

    .line 136
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, v3, Lcom/google/android/gms/icing/impl/az;->a:[Lcom/google/android/gms/icing/k;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v10, v2, v3, v8, v9}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;[Lcom/google/android/gms/icing/k;Ljava/lang/Integer;Lcom/google/android/gms/icing/l;)V

    goto :goto_6

    .line 139
    :cond_c
    const-string v2, "Usage reports %d indexed %d rejected %d imm upload %b"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v4

    const/4 v4, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v4

    const/4 v4, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/gms/icing/impl/bl;->c:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 141
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/gms/icing/impl/bl;->c:Z

    if-eqz v2, :cond_e

    .line 142
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/bl;->a:Lcom/google/android/gms/icing/impl/u;

    new-instance v3, Lcom/google/android/gms/icing/impl/b;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/icing/impl/bl;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-direct {v3, v4}, Lcom/google/android/gms/icing/impl/b;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    .line 147
    :cond_d
    :goto_7
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 144
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/bl;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-static {v2}, Lcom/google/android/gms/icing/impl/b;->a(Lcom/google/android/gms/icing/impl/u;)V

    goto :goto_7

    :cond_f
    move v5, v2

    move v6, v3

    move v7, v4

    goto/16 :goto_1
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/bl;->c()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
