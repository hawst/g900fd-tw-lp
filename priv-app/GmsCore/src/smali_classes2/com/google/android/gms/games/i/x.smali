.class final Lcom/google/android/gms/games/i/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final a:Landroid/content/Context;

.field final synthetic b:Lcom/google/android/gms/games/i/b;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/i/b;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 386
    iput-object p1, p0, Lcom/google/android/gms/games/i/x;->b:Lcom/google/android/gms/games/i/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387
    iput-object p2, p0, Lcom/google/android/gms/games/i/x;->a:Landroid/content/Context;

    .line 388
    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 395
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesSignInService"

    invoke-interface {p2}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 398
    iget-object v0, p0, Lcom/google/android/gms/games/i/x;->b:Lcom/google/android/gms/games/i/b;

    invoke-static {p2}, Lcom/google/android/gms/games/internal/eb;->a(Landroid/os/IBinder;)Lcom/google/android/gms/games/internal/ea;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/i/b;->f:Lcom/google/android/gms/games/internal/ea;

    .line 399
    monitor-enter p0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 400
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/i/x;->b:Lcom/google/android/gms/games/i/b;

    iget-object v0, v0, Lcom/google/android/gms/games/i/b;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 401
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 402
    iget-object v0, p0, Lcom/google/android/gms/games/i/x;->b:Lcom/google/android/gms/games/i/b;

    iget-object v0, v0, Lcom/google/android/gms/games/i/b;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/i/q;

    invoke-interface {v0}, Lcom/google/android/gms/games/i/q;->a()V

    .line 401
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 404
    :cond_0
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 415
    :goto_1
    return-void

    .line 404
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0

    throw v0
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 408
    :catch_0
    move-exception v0

    const-string v0, "SignInClient"

    const-string v1, "Unable to connect to sign-in service"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/i/x;->a:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 413
    iget-object v0, p0, Lcom/google/android/gms/games/i/x;->b:Lcom/google/android/gms/games/i/b;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/games/i/b;->g:Lcom/google/android/gms/games/i/x;

    .line 414
    iget-object v0, p0, Lcom/google/android/gms/games/i/x;->b:Lcom/google/android/gms/games/i/b;

    iget-object v0, v0, Lcom/google/android/gms/games/i/b;->b:Lcom/google/android/gms/games/i/r;

    invoke-interface {v0}, Lcom/google/android/gms/games/i/r;->b()V

    goto :goto_1
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    .prologue
    .line 420
    monitor-enter p0

    .line 421
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/i/x;->b:Lcom/google/android/gms/games/i/b;

    iget-object v0, v0, Lcom/google/android/gms/games/i/b;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 422
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 423
    iget-object v2, p0, Lcom/google/android/gms/games/i/x;->b:Lcom/google/android/gms/games/i/b;

    iget-object v2, v2, Lcom/google/android/gms/games/i/b;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 422
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 425
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
