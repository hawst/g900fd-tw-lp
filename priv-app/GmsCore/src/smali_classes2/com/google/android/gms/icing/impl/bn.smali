.class public final Lcom/google/android/gms/icing/impl/bn;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:D

.field private static b:D

.field private static c:D


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Ljava/io/File;

.field private final f:J

.field private final g:J

.field private h:J

.field private i:J

.field private j:J

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const-wide v0, 0x3fa999999999999aL    # 0.05

    .line 37
    sput-wide v0, Lcom/google/android/gms/icing/impl/bn;->a:D

    .line 40
    sput-wide v0, Lcom/google/android/gms/icing/impl/bn;->b:D

    .line 44
    const-wide v0, 0x3fd3333333333333L    # 0.3

    sput-wide v0, Lcom/google/android/gms/icing/impl/bn;->c:D

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-wide v2, p0, Lcom/google/android/gms/icing/impl/bn;->h:J

    .line 55
    iput-wide v2, p0, Lcom/google/android/gms/icing/impl/bn;->i:J

    .line 56
    iput-wide v2, p0, Lcom/google/android/gms/icing/impl/bn;->j:J

    .line 57
    iput-boolean v4, p0, Lcom/google/android/gms/icing/impl/bn;->k:Z

    .line 61
    if-nez p2, :cond_0

    .line 62
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Index file directory must be set"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_0
    new-instance v0, Ljava/io/File;

    const-string v1, "AppDataSearch"

    invoke-direct {v0, p2, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 68
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 69
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_2

    .line 70
    new-instance v0, Ljava/io/IOException;

    const-string v1, "The index path could not be created"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_2

    .line 73
    new-instance v0, Ljava/io/IOException;

    const-string v1, "The index path is not a directory"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :cond_2
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/bn;->d:Landroid/content/Context;

    .line 76
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/gms/icing/impl/bn;->e:Ljava/io/File;

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bn;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_3

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bn;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_3

    .line 80
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot create directory "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/bn;->e:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bn;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getTotalSpace()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/impl/bn;->f:J

    .line 84
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/bn;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    .line 85
    const-string v0, "There is no storage capacity, icing will not index"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 87
    :cond_4
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/gms/icing/impl/bn;->g:J

    .line 89
    const-string v0, "Storage manager: low %s usage %s avail %s capacity %s"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/bn;->d()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/bn;->e:Ljava/io/File;

    invoke-static {v3}, Lcom/google/android/gms/icing/impl/bn;->a(Ljava/io/File;)J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/android/gms/icing/impl/bp;->a(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/bn;->e:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getUsableSpace()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/android/gms/icing/impl/bp;->a(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/android/gms/icing/impl/bn;->f:J

    invoke-static {v4, v5}, Lcom/google/android/gms/icing/impl/bp;->a(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 92
    return-void
.end method

.method private static a(Ljava/io/File;)J
    .locals 7

    .prologue
    const-wide/16 v0, 0x0

    .line 240
    if-nez p0, :cond_1

    .line 254
    :cond_0
    :goto_0
    return-wide v0

    .line 243
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 244
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 245
    if-eqz v3, :cond_0

    .line 249
    array-length v6, v3

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v6, :cond_0

    aget-object v4, v3, v2

    .line 250
    invoke-static {v4}, Lcom/google/android/gms/icing/impl/bn;->a(Ljava/io/File;)J

    move-result-wide v4

    add-long/2addr v4, v0

    .line 249
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-wide v0, v4

    goto :goto_1

    .line 254
    :cond_2
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v0

    goto :goto_0
.end method

.method static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 303
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/impl/bo;->a:[Ljava/lang/String;

    aget-object v0, v0, p0
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 305
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "unknown"

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/icing/bi;)Z
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 266
    move v0, v1

    move-wide v2, v4

    move-wide v6, v4

    .line 268
    :goto_0
    iget-object v8, p0, Lcom/google/android/gms/icing/bi;->a:[Lcom/google/android/gms/icing/bj;

    array-length v8, v8

    if-ge v0, v8, :cond_0

    .line 269
    iget-object v8, p0, Lcom/google/android/gms/icing/bi;->a:[Lcom/google/android/gms/icing/bj;

    aget-object v8, v8, v0

    .line 270
    iget-wide v10, v8, Lcom/google/android/gms/icing/bj;->d:J

    add-long/2addr v6, v10

    .line 271
    iget-wide v8, v8, Lcom/google/android/gms/icing/bj;->e:J

    add-long/2addr v2, v8

    .line 268
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 273
    :cond_0
    add-long/2addr v6, v2

    .line 274
    cmp-long v0, v6, v4

    if-nez v0, :cond_2

    .line 284
    :cond_1
    :goto_1
    return v1

    .line 277
    :cond_2
    long-to-double v2, v2

    long-to-double v4, v6

    div-double/2addr v2, v4

    .line 279
    sget-object v0, Lcom/google/android/gms/icing/a/a;->i:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 281
    long-to-double v4, v4

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    div-double/2addr v4, v6

    .line 282
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    .line 284
    cmpl-double v0, v2, v4

    if-ltz v0, :cond_1

    const/4 v1, 0x1

    goto :goto_1
.end method

.method private d()Z
    .locals 4

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bn;->d:Landroid/content/Context;

    const/4 v1, 0x0

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 128
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 129
    iget-wide v2, p0, Lcom/google/android/gms/icing/impl/bn;->h:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x2710

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    .line 130
    iput-wide v0, p0, Lcom/google/android/gms/icing/impl/bn;->h:J

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bn;->e:Ljava/io/File;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bn;->a(Ljava/io/File;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/impl/bn;->i:J

    .line 133
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/bn;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/impl/bn;->k:Z

    .line 135
    iget-boolean v0, p0, Lcom/google/android/gms/icing/impl/bn;->k:Z

    if-eqz v0, :cond_1

    .line 137
    iput-wide v8, p0, Lcom/google/android/gms/icing/impl/bn;->j:J

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/a/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v0, Lcom/google/android/gms/icing/a/a;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/bn;->e:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getUsableSpace()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/gms/icing/impl/bn;->i:J

    add-long/2addr v4, v6

    sub-long v2, v4, v2

    iget-wide v6, p0, Lcom/google/android/gms/icing/impl/bn;->f:J

    mul-long/2addr v0, v6

    const-wide/16 v6, 0x64

    div-long/2addr v0, v6

    sub-long v0, v4, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    const-wide/32 v2, 0x1400000

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x2

    div-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/gms/icing/impl/bn;->g:J

    invoke-static {v0, v1, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/impl/bn;->j:J

    goto :goto_0
.end method

.method private f()J
    .locals 2

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/bn;->e()V

    .line 181
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/bn;->i:J

    return-wide v0
.end method

.method private g()J
    .locals 2

    .prologue
    .line 190
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/bn;->e()V

    .line 191
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/bn;->j:J

    return-wide v0
.end method


# virtual methods
.method final a(D)I
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 310
    sget-wide v2, Lcom/google/android/gms/icing/impl/bn;->a:D

    cmpg-double v2, p1, v2

    if-gtz v2, :cond_1

    .line 311
    const-string v0, "Design limits for indexing reached"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;)I

    .line 312
    const/4 v0, 0x3

    .line 320
    :cond_0
    :goto_0
    return v0

    .line 313
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/bn;->e()V

    iget-wide v2, p0, Lcom/google/android/gms/icing/impl/bn;->i:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/impl/bn;->j:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    move v2, v1

    :goto_1
    if-nez v2, :cond_0

    .line 315
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/bn;->e()V

    iget-boolean v2, p0, Lcom/google/android/gms/icing/impl/bn;->k:Z

    if-nez v2, :cond_2

    move v0, v1

    :cond_2
    if-eqz v0, :cond_4

    .line 316
    const-string v0, "Not enough disk space for indexing trimmable"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;)I

    move v0, v1

    .line 317
    goto :goto_0

    :cond_3
    move v2, v0

    .line 313
    goto :goto_1

    .line 319
    :cond_4
    const-string v0, "Not enough disk space for indexing"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 320
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/icing/impl/m;Lcom/google/android/gms/icing/b;D)V
    .locals 11

    .prologue
    .line 325
    invoke-virtual {p0, p3, p4}, Lcom/google/android/gms/icing/impl/bn;->a(D)I

    move-result v1

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bn;->e:Ljava/io/File;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bn;->a(Ljava/io/File;)J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bn;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getUsableSpace()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/gms/icing/impl/bn;->f:J

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/bn;->d()Z

    move-result v8

    if-nez p2, :cond_0

    const/4 v9, 0x0

    :goto_0
    move-object v0, p1

    invoke-interface/range {v0 .. v9}, Lcom/google/android/gms/icing/impl/m;->a(IJJJZI)V

    .line 328
    return-void

    .line 325
    :cond_0
    iget v9, p2, Lcom/google/android/gms/icing/b;->a:I

    goto :goto_0
.end method

.method final a(Ljava/io/PrintWriter;D)V
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    .line 372
    const-string v0, "Storage state: %s\n"

    new-array v1, v8, [Ljava/lang/Object;

    invoke-virtual {p0, p2, p3}, Lcom/google/android/gms/icing/impl/bn;->a(D)I

    move-result v2

    invoke-static {v2}, Lcom/google/android/gms/icing/impl/bn;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-virtual {p1, v0, v1}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 373
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/bn;->f()J

    move-result-wide v0

    .line 374
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/bn;->g()J

    move-result-wide v2

    .line 375
    const-string v4, "Disk usage %s budget %s free frac %.3f%% index free frac %.3f%%\n"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/impl/bp;->a(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v2, v3}, Lcom/google/android/gms/icing/impl/bp;->a(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    const/4 v6, 0x2

    cmp-long v7, v0, v2

    if-lez v7, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v0, 0x3

    mul-double v2, p2, v10

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    aput-object v1, v5, v0

    invoke-virtual {p1, v4, v5}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 381
    return-void

    .line 375
    :cond_0
    long-to-double v8, v2

    long-to-double v0, v0

    sub-double v0, v8, v0

    long-to-double v2, v2

    div-double/2addr v0, v2

    mul-double/2addr v0, v10

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bn;->e:Ljava/io/File;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bp;->a(Ljava/io/File;)V

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bn;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    return v0
.end method

.method final b(D)D
    .locals 15

    .prologue
    .line 339
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/bn;->f()J

    move-result-wide v6

    .line 340
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/bn;->g()J

    move-result-wide v8

    .line 341
    cmp-long v0, v6, v8

    if-lez v0, :cond_1

    const-wide/16 v0, 0x0

    move-wide v4, v0

    .line 347
    :goto_0
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_2

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpg-double v0, p1, v0

    if-gez v0, :cond_2

    long-to-double v0, v6

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double v2, v2, p1

    div-double/2addr v0, v2

    double-to-long v0, v0

    .line 353
    :goto_1
    sget-wide v2, Lcom/google/android/gms/icing/impl/bn;->a:D

    cmpg-double v2, p1, v2

    if-lez v2, :cond_0

    sget-wide v2, Lcom/google/android/gms/icing/impl/bn;->b:D

    cmpg-double v2, v4, v2

    if-gtz v2, :cond_3

    :cond_0
    const/4 v2, 0x1

    .line 356
    :goto_2
    invoke-static {v8, v9, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-double v0, v0

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    sget-wide v12, Lcom/google/android/gms/icing/impl/bn;->c:D

    sub-double/2addr v10, v12

    mul-double/2addr v0, v10

    double-to-long v0, v0

    .line 360
    const-string v3, "Performing maintenance usage %s budget %s free %.3f%% index free %.3f%% purge? %s target %s"

    const/4 v10, 0x6

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v10, v11

    const/4 v8, 0x2

    const-wide/high16 v12, 0x4059000000000000L    # 100.0

    mul-double/2addr v4, v12

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v10, v8

    const/4 v4, 0x3

    const-wide/high16 v8, 0x4059000000000000L    # 100.0

    mul-double v8, v8, p1

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v10, v4

    const/4 v4, 0x4

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v10, v4

    const/4 v4, 0x5

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v10, v4

    invoke-static {v3, v10}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 366
    if-eqz v2, :cond_4

    cmp-long v2, v0, v6

    if-gez v2, :cond_4

    long-to-double v2, v6

    long-to-double v0, v0

    sub-double v0, v2, v0

    long-to-double v2, v6

    div-double/2addr v0, v2

    :goto_3
    return-wide v0

    .line 341
    :cond_1
    sub-long v0, v8, v6

    long-to-double v0, v0

    long-to-double v2, v8

    div-double/2addr v0, v2

    move-wide v4, v0

    goto :goto_0

    .line 347
    :cond_2
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_1

    .line 353
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 366
    :cond_4
    const-wide/16 v0, 0x0

    goto :goto_3
.end method

.method public final b()Ljava/io/File;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bn;->e:Ljava/io/File;

    return-object v0
.end method

.method public final c()D
    .locals 4

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/bn;->e()V

    .line 167
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/bn;->i:J

    iget-wide v2, p0, Lcom/google/android/gms/icing/impl/bn;->j:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 168
    const-wide/16 v0, 0x0

    .line 171
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/bn;->j:J

    iget-wide v2, p0, Lcom/google/android/gms/icing/impl/bn;->i:J

    sub-long/2addr v0, v2

    long-to-double v0, v0

    iget-wide v2, p0, Lcom/google/android/gms/icing/impl/bn;->j:J

    long-to-double v2, v2

    div-double/2addr v0, v2

    goto :goto_0
.end method
