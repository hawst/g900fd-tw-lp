.class public abstract Lcom/google/android/gms/car/senderprotocol/ap;
.super Lcom/google/android/gms/car/senderprotocol/az;
.source "SourceFile"


# instance fields
.field protected a:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/gw;Lcom/google/android/gms/car/gx;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/senderprotocol/az;-><init>(Lcom/google/android/gms/car/gw;Lcom/google/android/gms/car/gx;)V

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/senderprotocol/ap;->a:I

    .line 33
    return-void
.end method


# virtual methods
.method protected final a(ILjava/nio/ByteBuffer;)V
    .locals 3

    .prologue
    .line 37
    sparse-switch p1, :sswitch_data_0

    .line 51
    const-string v0, "CAR.MIC"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received message with invalid type header: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    .line 57
    invoke-static {p2}, Lcom/google/android/c/a/a;->a(Ljava/nio/ByteBuffer;)V

    .line 59
    :cond_1
    return-void

    .line 39
    :sswitch_0
    new-instance v0, Lcom/google/android/c/b/bk;

    invoke-direct {v0}, Lcom/google/android/c/b/bk;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/ap;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/c/b/bk;

    .line 41
    if-eqz v0, :cond_0

    .line 42
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/senderprotocol/ap;->a(Lcom/google/android/c/b/bk;)V

    goto :goto_0

    .line 47
    :sswitch_1
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->getLong()J

    invoke-virtual {p0, p2}, Lcom/google/android/gms/car/senderprotocol/ap;->b(Ljava/nio/ByteBuffer;)V

    goto :goto_0

    .line 37
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x8006 -> :sswitch_0
    .end sparse-switch
.end method

.method protected abstract a(Lcom/google/android/c/b/bk;)V
.end method

.method protected final b(I)V
    .locals 2

    .prologue
    .line 68
    new-instance v0, Lcom/google/android/c/b/e;

    invoke-direct {v0}, Lcom/google/android/c/b/e;-><init>()V

    .line 69
    iput p1, v0, Lcom/google/android/c/b/e;->a:I

    .line 70
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/c/b/e;->a(I)Lcom/google/android/c/b/e;

    .line 71
    const v1, 0x8004

    invoke-static {v0}, Lcom/google/android/c/b/e;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/ap;->a(I[B)V

    .line 72
    return-void
.end method

.method protected abstract b(Ljava/nio/ByteBuffer;)V
.end method
