.class public final Lcom/google/android/gms/icing/m;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:[Lcom/google/android/gms/icing/k;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2715
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2716
    iput v0, p0, Lcom/google/android/gms/icing/m;->a:I

    iput v0, p0, Lcom/google/android/gms/icing/m;->b:I

    invoke-static {}, Lcom/google/android/gms/icing/k;->a()[Lcom/google/android/gms/icing/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/m;->c:[Lcom/google/android/gms/icing/k;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/m;->cachedSize:I

    .line 2717
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 2781
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2782
    iget v1, p0, Lcom/google/android/gms/icing/m;->a:I

    if-eqz v1, :cond_0

    .line 2783
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/icing/m;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2786
    :cond_0
    iget v1, p0, Lcom/google/android/gms/icing/m;->b:I

    if-eqz v1, :cond_1

    .line 2787
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/icing/m;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2790
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/m;->c:[Lcom/google/android/gms/icing/k;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/icing/m;->c:[Lcom/google/android/gms/icing/k;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 2791
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/m;->c:[Lcom/google/android/gms/icing/k;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 2792
    iget-object v2, p0, Lcom/google/android/gms/icing/m;->c:[Lcom/google/android/gms/icing/k;

    aget-object v2, v2, v0

    .line 2793
    if-eqz v2, :cond_2

    .line 2794
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 2791
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2799
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2729
    if-ne p1, p0, :cond_1

    .line 2746
    :cond_0
    :goto_0
    return v0

    .line 2732
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/m;

    if-nez v2, :cond_2

    move v0, v1

    .line 2733
    goto :goto_0

    .line 2735
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/m;

    .line 2736
    iget v2, p0, Lcom/google/android/gms/icing/m;->a:I

    iget v3, p1, Lcom/google/android/gms/icing/m;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 2737
    goto :goto_0

    .line 2739
    :cond_3
    iget v2, p0, Lcom/google/android/gms/icing/m;->b:I

    iget v3, p1, Lcom/google/android/gms/icing/m;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 2740
    goto :goto_0

    .line 2742
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/icing/m;->c:[Lcom/google/android/gms/icing/k;

    iget-object v3, p1, Lcom/google/android/gms/icing/m;->c:[Lcom/google/android/gms/icing/k;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2744
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2751
    iget v0, p0, Lcom/google/android/gms/icing/m;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 2753
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/m;->b:I

    add-int/2addr v0, v1

    .line 2754
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/m;->c:[Lcom/google/android/gms/icing/k;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2756
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2689
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/m;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/m;->b:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/m;->c:[Lcom/google/android/gms/icing/k;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/k;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/m;->c:[Lcom/google/android/gms/icing/k;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/icing/k;

    invoke-direct {v3}, Lcom/google/android/gms/icing/k;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/m;->c:[Lcom/google/android/gms/icing/k;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/icing/k;

    invoke-direct {v3}, Lcom/google/android/gms/icing/k;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/m;->c:[Lcom/google/android/gms/icing/k;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 2762
    iget v0, p0, Lcom/google/android/gms/icing/m;->a:I

    if-eqz v0, :cond_0

    .line 2763
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/icing/m;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2765
    :cond_0
    iget v0, p0, Lcom/google/android/gms/icing/m;->b:I

    if-eqz v0, :cond_1

    .line 2766
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/icing/m;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2768
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/m;->c:[Lcom/google/android/gms/icing/k;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/icing/m;->c:[Lcom/google/android/gms/icing/k;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 2769
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/icing/m;->c:[Lcom/google/android/gms/icing/k;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 2770
    iget-object v1, p0, Lcom/google/android/gms/icing/m;->c:[Lcom/google/android/gms/icing/k;

    aget-object v1, v1, v0

    .line 2771
    if-eqz v1, :cond_2

    .line 2772
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2769
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2776
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2777
    return-void
.end method
