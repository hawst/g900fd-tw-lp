.class public final Lcom/google/android/gms/games/h/e/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/server/response/s;


# static fields
.field private static final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    sget v0, Lcom/google/android/gms/g;->G:I

    sput v0, Lcom/google/android/gms/games/h/e/a;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/server/response/FastJsonResponse;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 5

    .prologue
    .line 13
    check-cast p1, Lcom/google/android/gms/games/h/a/a;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    const-string v2, "is_revealed_icon_default"

    const-string v3, "revealed_icon_image_url"

    sget v4, Lcom/google/android/gms/games/h/e/a;->a:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/games/h/e/g;->a(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v2, "is_unlocked_icon_default"

    const-string v3, "unlocked_icon_image_url"

    sget v4, Lcom/google/android/gms/games/h/e/a;->a:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/games/h/e/g;->a(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    return-object p1
.end method
