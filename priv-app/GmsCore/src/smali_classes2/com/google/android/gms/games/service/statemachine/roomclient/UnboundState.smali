.class final Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;
.super Lcom/google/android/gms/games/service/statemachine/roomclient/ca;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    .line 26
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 5

    .prologue
    .line 37
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 44
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;->g:Z

    :goto_0
    return v0

    .line 39
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    .line 40
    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->d:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gms/games/service/RoomAndroidService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    new-instance v3, Lcom/google/android/gms/games/service/statemachine/roomclient/cj;

    invoke-direct {v3, p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/cj;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;Landroid/content/Context;)V

    iput-object v3, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->f:Lcom/google/android/gms/games/service/statemachine/roomclient/cj;

    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v3, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->f:Lcom/google/android/gms/games/service/statemachine/roomclient/cj;

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;->b()V

    .line 41
    :goto_1
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;->f:Z

    goto :goto_0

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->o:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceConnectState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceConnectState;->b()V

    goto :goto_1

    .line 37
    :pswitch_data_0
    .packed-switch 0x1d
        :pswitch_0
    .end packed-switch
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;->c()V

    .line 33
    return-void
.end method
