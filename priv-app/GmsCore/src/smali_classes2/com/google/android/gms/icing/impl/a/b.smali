.class public final Lcom/google/android/gms/icing/impl/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/pm/ApplicationInfo;

.field private final b:Landroid/content/Context;

.field private final c:Lorg/xmlpull/v1/XmlPullParser;

.field private final d:Landroid/util/TypedValue;


# direct methods
.method private constructor <init>(Landroid/content/pm/ApplicationInfo;Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 1

    .prologue
    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 137
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/a/b;->a:Landroid/content/pm/ApplicationInfo;

    .line 138
    iput-object p2, p0, Lcom/google/android/gms/icing/impl/a/b;->b:Landroid/content/Context;

    .line 139
    iput-object p3, p0, Lcom/google/android/gms/icing/impl/a/b;->c:Lorg/xmlpull/v1/XmlPullParser;

    .line 140
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/a/b;->d:Landroid/util/TypedValue;

    .line 141
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/os/Bundle;)D
    .locals 6

    .prologue
    .line 465
    const-string v0, "factor"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 466
    if-nez v0, :cond_0

    .line 467
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Section feature "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " needs parameter factor"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 472
    :cond_0
    :try_start_0
    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 477
    const-wide/16 v4, 0x0

    cmpg-double v1, v2, v4

    if-lez v1, :cond_1

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v1, v2, v4

    if-lez v1, :cond_2

    .line 478
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Factor not in range: Must be > 0 and <= 1 "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 474
    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Parameter factor="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " must be a number > 0 and <= 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 480
    :cond_2
    return-wide v2
.end method

.method private a(Landroid/util/AttributeSet;I)I
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 642
    invoke-interface {p1, p2, v1}, Landroid/util/AttributeSet;->getAttributeResourceValue(II)I

    move-result v0

    .line 643
    if-ne v0, v1, :cond_0

    .line 644
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1, p2}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " must be a resource reference."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 646
    :cond_0
    return v0
.end method

.method private a()Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/b;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v0}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v9

    .line 192
    if-nez v9, :cond_0

    .line 193
    const-string v0, "No attributes specified"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 197
    :cond_0
    const-string v3, "0"

    move v5, v6

    move-object v0, v7

    move v1, v8

    move-object v2, v7

    move-object v4, v7

    .line 202
    :goto_0
    invoke-interface {v9}, Landroid/util/AttributeSet;->getAttributeCount()I

    move-result v10

    if-ge v5, v10, :cond_7

    .line 203
    invoke-interface {v9, v5}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v10

    .line 204
    const-string v11, "corpusId"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 205
    invoke-direct {p0, v9, v5}, Lcom/google/android/gms/icing/impl/a/b;->c(Landroid/util/AttributeSet;I)Ljava/lang/String;

    move-result-object v4

    .line 202
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 206
    :cond_1
    const-string v11, "corpusVersion"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 207
    invoke-direct {p0, v9, v5}, Lcom/google/android/gms/icing/impl/a/b;->c(Landroid/util/AttributeSet;I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 208
    :cond_2
    const-string v11, "contentProviderUri"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 209
    invoke-direct {p0, v9, v5}, Lcom/google/android/gms/icing/impl/a/b;->c(Landroid/util/AttributeSet;I)Ljava/lang/String;

    move-result-object v2

    .line 210
    if-nez v2, :cond_3

    move-object v2, v7

    goto :goto_1

    :cond_3
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_1

    .line 211
    :cond_4
    const-string v11, "trimmable"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 212
    invoke-interface {v9, v5, v1}, Landroid/util/AttributeSet;->getAttributeBooleanValue(IZ)Z

    move-result v1

    goto :goto_1

    .line 213
    :cond_5
    const-string v0, "schemaOrgType"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 214
    invoke-direct {p0, v9, v5}, Lcom/google/android/gms/icing/impl/a/b;->c(Landroid/util/AttributeSet;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 216
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid attribute name "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 220
    :cond_7
    if-nez v4, :cond_8

    .line 221
    const-string v0, "No corpus ID specified."

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 223
    :cond_8
    if-nez v2, :cond_9

    .line 224
    const-string v0, "No content provider URI specified."

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 227
    :cond_9
    invoke-static {v4}, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->a(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/ai;

    move-result-object v4

    iput-object v3, v4, Lcom/google/android/gms/appdatasearch/ai;->a:Ljava/lang/String;

    iput-object v2, v4, Lcom/google/android/gms/appdatasearch/ai;->b:Landroid/net/Uri;

    iput-boolean v1, v4, Lcom/google/android/gms/appdatasearch/ai;->e:Z

    iput-object v0, v4, Lcom/google/android/gms/appdatasearch/ai;->h:Ljava/lang/String;

    .line 237
    new-instance v1, Lcom/google/android/gms/icing/impl/a/c;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/b;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-direct {v1, v0}, Lcom/google/android/gms/icing/impl/a/c;-><init>(Lorg/xmlpull/v1/XmlPullParser;)V

    move v0, v6

    .line 238
    :goto_2
    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/a/c;->a()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 240
    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/a/c;->b()Ljava/lang/String;

    move-result-object v2

    .line 241
    const-string v3, "Section"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 242
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/b;->b()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v2

    iget-object v3, v4, Lcom/google/android/gms/appdatasearch/ai;->c:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 243
    :cond_a
    const-string v3, "GlobalSearchCorpus"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 244
    if-eqz v0, :cond_b

    .line 245
    const-string v0, "Duplicate element GlobalSearchCorpus"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 248
    :cond_b
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/b;->d()Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/appdatasearch/ai;->d:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    move v0, v8

    goto :goto_2

    .line 249
    :cond_c
    const-string v3, "IMECorpus"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 250
    if-eqz v6, :cond_d

    .line 251
    const-string v0, "Duplicate element IMECorpus"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 254
    :cond_d
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/b;->c()Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

    move-result-object v2

    iput-object v2, v4, Lcom/google/android/gms/appdatasearch/ai;->g:Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

    move v6, v8

    goto :goto_2

    .line 256
    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid tag "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " inside Corpus; expected Section"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " or GlobalSearchCorpus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 259
    :cond_f
    invoke-virtual {v4}, Lcom/google/android/gms/appdatasearch/ai;->a()Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/pm/PackageManager;Landroid/content/Context;Landroid/content/pm/ApplicationInfo;)Lcom/google/android/gms/icing/impl/a/b;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 112
    iget-object v1, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 113
    const-string v1, "null packageName in ApplicationInfo, bailing"

    invoke-static {v1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 132
    :goto_0
    return-object v0

    .line 117
    :cond_0
    :try_start_0
    iget-object v1, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v2

    .line 119
    const-string v1, "com.google.android.gms.appdatasearch"

    invoke-virtual {p2, p0, v1}, Landroid/content/pm/ApplicationInfo;->loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;

    move-result-object v3

    .line 120
    if-nez v3, :cond_1

    .line 121
    const-string v1, "Failed to read %s meta data from %s; could not create XML parser"

    const-string v2, "com.google.android.gms.appdatasearch"

    iget-object v3, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 127
    :catch_0
    move-exception v1

    .line 128
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to get context for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0

    .line 126
    :cond_1
    :try_start_1
    new-instance v1, Lcom/google/android/gms/icing/impl/a/b;

    invoke-direct {v1, p2, v2, v3}, Lcom/google/android/gms/icing/impl/a/b;-><init>(Landroid/content/pm/ApplicationInfo;Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    goto :goto_0

    .line 129
    :catch_1
    move-exception v1

    .line 130
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to get context for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0
.end method

.method private a(Ljava/lang/String;IZ)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 620
    if-nez p2, :cond_0

    .line 632
    :goto_0
    :pswitch_0
    return-object v0

    .line 624
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/b;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/b;->d:Landroid/util/TypedValue;

    const/4 v3, 0x1

    invoke-virtual {v1, p2, v2, v3}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 625
    if-nez p3, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/b;->d:Landroid/util/TypedValue;

    iget v1, v1, Landroid/util/TypedValue;->changingConfigurations:I

    if-eqz v1, :cond_1

    .line 626
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " must not change between configurations"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 636
    :catch_0
    move-exception v0

    .line 637
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resource not found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/icing/impl/a/d;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/b;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/a/b;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-direct {v2, v3, v4, v1, v0}, Lcom/google/android/gms/icing/impl/a/d;-><init>(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 628
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/b;->d:Landroid/util/TypedValue;

    iget v1, v1, Landroid/util/TypedValue;->type:I

    packed-switch v1, :pswitch_data_0

    .line 634
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " does not refer to a string resource"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 632
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/b;->d:Landroid/util/TypedValue;

    iget-object v0, v0, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0

    .line 628
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 485
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/b;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v0}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v3

    .line 486
    if-nez v3, :cond_0

    .line 487
    const-string v0, "No attributes specified"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 492
    :cond_0
    const/4 v0, 0x0

    move v2, v0

    move-object v0, v1

    :goto_0
    invoke-interface {v3}, Landroid/util/AttributeSet;->getAttributeCount()I

    move-result v4

    if-ge v2, v4, :cond_3

    .line 493
    invoke-interface {v3, v2}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v4

    .line 494
    const-string v5, "paramName"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 495
    invoke-interface {v3, v2}, Landroid/util/AttributeSet;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v1

    .line 492
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 496
    :cond_1
    const-string v0, "paramValue"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 497
    invoke-direct {p0, v3, v2}, Lcom/google/android/gms/icing/impl/a/b;->c(Landroid/util/AttributeSet;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 499
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid attribute name "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 502
    :cond_3
    if-eqz v1, :cond_4

    if-nez v0, :cond_5

    .line 503
    :cond_4
    const-string v0, "Both name and value must be specified"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 505
    :cond_5
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    return-void
.end method

.method private b(Landroid/util/AttributeSet;I)I
    .locals 3

    .prologue
    .line 651
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/icing/impl/a/b;->a(Landroid/util/AttributeSet;I)I

    move-result v0

    .line 653
    invoke-interface {p1, p2}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/gms/icing/impl/a/b;->a(Ljava/lang/String;IZ)Ljava/lang/String;

    .line 654
    return v0
.end method

.method private b(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/Feature;
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v2, -0x1

    .line 409
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/b;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v0}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v4

    .line 410
    if-nez v4, :cond_0

    .line 411
    const-string v0, "No attributes specified"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 414
    :cond_0
    const-string v1, ""

    .line 416
    const/4 v0, 0x0

    move-object v3, v1

    move v1, v2

    :goto_0
    invoke-interface {v4}, Landroid/util/AttributeSet;->getAttributeCount()I

    move-result v5

    if-ge v0, v5, :cond_2

    .line 417
    invoke-interface {v4, v0}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v3

    .line 418
    const-string v5, "featureType"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 419
    invoke-interface {v4, v0, v1}, Landroid/util/AttributeSet;->getAttributeIntValue(II)I

    move-result v1

    .line 416
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 422
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid attribute name "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 427
    :cond_2
    new-instance v0, Lcom/google/android/gms/icing/impl/a/c;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/a/b;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-direct {v0, v4}, Lcom/google/android/gms/icing/impl/a/c;-><init>(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 428
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 429
    :goto_1
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/c;->a()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 430
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/c;->b()Ljava/lang/String;

    move-result-object v5

    .line 431
    const-string v6, "FeatureParam"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 432
    invoke-direct {p0, v4}, Lcom/google/android/gms/icing/impl/a/b;->a(Landroid/os/Bundle;)V

    goto :goto_1

    .line 434
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid tag "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " inside SectionFeature; expected FeatureParam"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 437
    :cond_4
    if-ne v1, v2, :cond_5

    .line 440
    const-string v0, "No type specified."

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 441
    :cond_5
    const/4 v0, 0x1

    if-ne v1, v0, :cond_7

    .line 443
    invoke-virtual {v4}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 444
    const-string v0, "Section feature match_global_nicknames does not take set"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 446
    :cond_6
    invoke-static {}, Lcom/google/android/gms/appdatasearch/aw;->a()Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    .line 457
    :goto_2
    return-object v0

    .line 447
    :cond_7
    if-ne v1, v7, :cond_8

    .line 448
    const-string v0, "demote_common_words"

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/icing/impl/a/b;->a(Ljava/lang/String;Landroid/os/Bundle;)D

    move-result-wide v0

    new-instance v2, Lcom/google/android/gms/appdatasearch/Feature;

    invoke-direct {v2, v7}, Lcom/google/android/gms/appdatasearch/Feature;-><init>(I)V

    const-string v3, "factor"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/appdatasearch/Feature;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    goto :goto_2

    .line 449
    :cond_8
    if-ne v1, v8, :cond_9

    const-string v0, "rfc822"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 451
    const-string v0, "demote_rfc822_hostnames"

    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/icing/impl/a/b;->a(Ljava/lang/String;Landroid/os/Bundle;)D

    move-result-wide v0

    new-instance v2, Lcom/google/android/gms/appdatasearch/Feature;

    invoke-direct {v2, v8}, Lcom/google/android/gms/appdatasearch/Feature;-><init>(I)V

    const-string v3, "factor"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/appdatasearch/Feature;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    goto :goto_2

    .line 453
    :cond_9
    const/4 v0, 0x4

    if-ne v1, v0, :cond_a

    const-string v0, "url"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 455
    invoke-static {}, Lcom/google/android/gms/appdatasearch/aw;->b()Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    goto :goto_2

    .line 456
    :cond_a
    const/4 v0, 0x5

    if-ne v1, v0, :cond_b

    .line 457
    invoke-static {}, Lcom/google/android/gms/appdatasearch/aw;->c()Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    goto :goto_2

    .line 459
    :cond_b
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid section feature of type "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " inside section with format "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0
.end method

.method private b()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;
    .locals 12

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x0

    .line 266
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/b;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v9

    .line 267
    if-nez v9, :cond_0

    .line 268
    const-string v0, "No attributes specified"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 272
    :cond_0
    const-string v5, "plain"

    .line 274
    const/4 v3, 0x1

    move v7, v8

    move-object v1, v0

    move v2, v8

    move v4, v8

    move-object v6, v0

    .line 279
    :goto_0
    invoke-interface {v9}, Landroid/util/AttributeSet;->getAttributeCount()I

    move-result v10

    if-ge v7, v10, :cond_8

    .line 280
    invoke-interface {v9, v7}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v10

    .line 281
    const-string v11, "sectionId"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 282
    invoke-direct {p0, v9, v7}, Lcom/google/android/gms/icing/impl/a/b;->c(Landroid/util/AttributeSet;I)Ljava/lang/String;

    move-result-object v6

    .line 279
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 283
    :cond_1
    const-string v11, "sectionFormat"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 284
    invoke-interface {v9, v7, v8}, Landroid/util/AttributeSet;->getAttributeIntValue(II)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    const-string v0, "Invalid section format"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    :pswitch_0
    const-string v5, "plain"

    goto :goto_1

    :pswitch_1
    const-string v5, "html"

    goto :goto_1

    :pswitch_2
    const-string v5, "rfc822"

    goto :goto_1

    :pswitch_3
    const-string v5, "url"

    goto :goto_1

    .line 285
    :cond_2
    const-string v11, "noIndex"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 286
    invoke-interface {v9, v7, v4}, Landroid/util/AttributeSet;->getAttributeBooleanValue(IZ)Z

    move-result v4

    goto :goto_1

    .line 287
    :cond_3
    const-string v11, "sectionWeight"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 288
    invoke-interface {v9, v7, v3}, Landroid/util/AttributeSet;->getAttributeIntValue(II)I

    move-result v3

    goto :goto_1

    .line 289
    :cond_4
    const-string v11, "indexPrefixes"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 290
    invoke-interface {v9, v7, v2}, Landroid/util/AttributeSet;->getAttributeBooleanValue(IZ)Z

    move-result v2

    goto :goto_1

    .line 291
    :cond_5
    const-string v11, "subsectionSeparator"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 292
    invoke-direct {p0, v9, v7}, Lcom/google/android/gms/icing/impl/a/b;->c(Landroid/util/AttributeSet;I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 293
    :cond_6
    const-string v0, "schemaOrgProperty"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 294
    invoke-direct {p0, v9, v7}, Lcom/google/android/gms/icing/impl/a/b;->c(Landroid/util/AttributeSet;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 296
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid attribute name "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 299
    :cond_8
    if-nez v6, :cond_9

    .line 300
    const-string v0, "No section ID specified."

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 303
    :cond_9
    invoke-static {v6}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->a(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/ak;

    move-result-object v6

    iput-object v5, v6, Lcom/google/android/gms/appdatasearch/ak;->a:Ljava/lang/String;

    iput-boolean v4, v6, Lcom/google/android/gms/appdatasearch/ak;->b:Z

    iput v3, v6, Lcom/google/android/gms/appdatasearch/ak;->c:I

    iput-boolean v2, v6, Lcom/google/android/gms/appdatasearch/ak;->d:Z

    iput-object v1, v6, Lcom/google/android/gms/appdatasearch/ak;->e:Ljava/lang/String;

    iput-object v0, v6, Lcom/google/android/gms/appdatasearch/ak;->f:Ljava/lang/String;

    .line 311
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 314
    new-instance v1, Lcom/google/android/gms/icing/impl/a/c;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/b;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-direct {v1, v2}, Lcom/google/android/gms/icing/impl/a/c;-><init>(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 315
    :goto_2
    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/a/c;->a()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 316
    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/a/c;->b()Ljava/lang/String;

    move-result-object v2

    .line 317
    const-string v3, "SectionFeature"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 318
    invoke-direct {p0, v5}, Lcom/google/android/gms/icing/impl/a/b;->b(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v2

    .line 319
    iget v3, v2, Lcom/google/android/gms/appdatasearch/Feature;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 320
    const-string v0, "Duplicate feature defined for section"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 322
    :cond_a
    invoke-virtual {v6, v2}, Lcom/google/android/gms/appdatasearch/ak;->a(Lcom/google/android/gms/appdatasearch/Feature;)Lcom/google/android/gms/appdatasearch/ak;

    .line 323
    iget v2, v2, Lcom/google/android/gms/appdatasearch/Feature;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 325
    :cond_b
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid tag "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " inside Section; expected SectionFeature"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 328
    :cond_c
    invoke-virtual {v6}, Lcom/google/android/gms/appdatasearch/ak;->a()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v0

    return-object v0

    .line 284
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private c(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;
    .locals 13

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x0

    .line 574
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/b;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v9

    .line 575
    if-nez v9, :cond_0

    .line 576
    const-string v0, "No attributes specified"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 579
    :cond_0
    const/4 v1, 0x1

    .line 581
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/b;->a:Landroid/content/pm/ApplicationInfo;

    iget v4, v2, Landroid/content/pm/ApplicationInfo;->icon:I

    move-object v7, v8

    move-object v6, v8

    move-object v5, v8

    move v3, v0

    move v2, v0

    move v12, v0

    move v0, v1

    move v1, v12

    .line 586
    :goto_0
    invoke-interface {v9}, Landroid/util/AttributeSet;->getAttributeCount()I

    move-result v10

    if-ge v1, v10, :cond_8

    .line 587
    invoke-interface {v9, v1}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v10

    .line 588
    const-string v11, "searchEnabled"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 589
    invoke-interface {v9, v1, v0}, Landroid/util/AttributeSet;->getAttributeBooleanValue(IZ)Z

    move-result v0

    .line 586
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 590
    :cond_1
    const-string v11, "searchLabel"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 591
    invoke-direct {p0, v9, v1}, Lcom/google/android/gms/icing/impl/a/b;->b(Landroid/util/AttributeSet;I)I

    move-result v2

    goto :goto_1

    .line 592
    :cond_2
    const-string v11, "settingsDescription"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 593
    invoke-direct {p0, v9, v1}, Lcom/google/android/gms/icing/impl/a/b;->b(Landroid/util/AttributeSet;I)I

    move-result v3

    goto :goto_1

    .line 594
    :cond_3
    const-string v11, "defaultIntentAction"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 595
    invoke-direct {p0, v9, v1}, Lcom/google/android/gms/icing/impl/a/b;->c(Landroid/util/AttributeSet;I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 596
    :cond_4
    const-string v11, "defaultIntentData"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 597
    invoke-direct {p0, v9, v1}, Lcom/google/android/gms/icing/impl/a/b;->c(Landroid/util/AttributeSet;I)Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 598
    :cond_5
    const-string v11, "defaultIntentActivity"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 599
    invoke-direct {p0, v9, v1}, Lcom/google/android/gms/icing/impl/a/b;->c(Landroid/util/AttributeSet;I)Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 600
    :cond_6
    const-string v4, "globalSearchIcon"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 601
    invoke-direct {p0, v9, v1}, Lcom/google/android/gms/icing/impl/a/b;->a(Landroid/util/AttributeSet;I)I

    move-result v4

    goto :goto_1

    .line 603
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid attribute name "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 606
    :cond_8
    if-nez v2, :cond_9

    .line 607
    const-string v0, "No label specified"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 610
    :cond_9
    if-eqz v0, :cond_a

    .line 611
    new-instance v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;-><init>(Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    :goto_2
    return-object v0

    :cond_a
    move-object v0, v8

    goto :goto_2
.end method

.method private c()Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;
    .locals 13

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 335
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/b;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v0}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v8

    .line 336
    const/4 v4, 0x1

    .line 337
    const/4 v3, -0x1

    .line 339
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 342
    if-eqz v8, :cond_c

    move v5, v6

    move-object v0, v7

    move-object v1, v7

    move-object v2, v7

    .line 343
    :goto_0
    invoke-interface {v8}, Landroid/util/AttributeSet;->getAttributeCount()I

    move-result v10

    if-ge v5, v10, :cond_6

    .line 344
    invoke-interface {v8, v5}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v10

    .line 345
    const-string v11, "inputEnabled"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 346
    invoke-interface {v8, v5, v4}, Landroid/util/AttributeSet;->getAttributeBooleanValue(IZ)Z

    move-result v4

    .line 343
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 347
    :cond_0
    const-string v11, "sourceClass"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 348
    invoke-interface {v8, v5, v6}, Landroid/util/AttributeSet;->getAttributeIntValue(II)I

    move-result v3

    goto :goto_1

    .line 349
    :cond_1
    const-string v11, "userInputTag"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 350
    invoke-interface {v8, v5}, Landroid/util/AttributeSet;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 351
    :cond_2
    const-string v11, "userInputSection"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 352
    invoke-interface {v8, v5}, Landroid/util/AttributeSet;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 353
    :cond_3
    const-string v11, "userInputValue"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 354
    invoke-interface {v8, v5}, Landroid/util/AttributeSet;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 355
    :cond_4
    const-string v0, "toAddressesSection"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 356
    invoke-interface {v8, v5}, Landroid/util/AttributeSet;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 358
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid attribute name "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    :cond_6
    move-object v6, v0

    move-object v4, v2

    move-object v12, v1

    move v1, v3

    move-object v3, v12

    .line 363
    :goto_2
    if-gez v1, :cond_7

    .line 364
    const-string v0, "Missing source class information."

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 367
    :cond_7
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    if-nez v4, :cond_8

    .line 368
    const-string v0, "userInputValue is specified but missing userInputSection"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 373
    :cond_8
    new-instance v0, Lcom/google/android/gms/icing/impl/a/c;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/b;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-direct {v0, v2}, Lcom/google/android/gms/icing/impl/a/c;-><init>(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 374
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 375
    :goto_3
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/c;->a()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 376
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/c;->b()Ljava/lang/String;

    move-result-object v5

    .line 377
    const-string v8, "IMESection"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 378
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/b;->b()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v5

    iget-object v5, v5, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->b:Ljava/lang/String;

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 380
    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid tag "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " inside IMECorpus; expected IMESection"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 383
    :cond_a
    new-instance v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v2, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_b

    move-object v5, v7

    :goto_4
    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;-><init>(I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_b
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v9, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    goto :goto_4

    :cond_c
    move-object v6, v7

    move-object v4, v7

    move v1, v3

    move-object v3, v7

    goto/16 :goto_2
.end method

.method private c(Landroid/util/AttributeSet;I)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 659
    invoke-interface {p1, p2, v1}, Landroid/util/AttributeSet;->getAttributeResourceValue(II)I

    move-result v0

    .line 660
    if-eq v0, v1, :cond_0

    .line 661
    invoke-interface {p1, p2}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/gms/icing/impl/a/b;->a(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v0

    .line 663
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1, p2}, Landroid/util/AttributeSet;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private d()Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;
    .locals 12

    .prologue
    const/4 v6, -0x1

    const/4 v1, 0x0

    .line 510
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/b;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v0}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v4

    .line 511
    const/4 v0, 0x1

    .line 513
    if-eqz v4, :cond_2

    move v2, v1

    move v3, v0

    move v0, v1

    .line 514
    :goto_0
    invoke-interface {v4}, Landroid/util/AttributeSet;->getAttributeCount()I

    move-result v5

    if-ge v0, v5, :cond_3

    .line 515
    invoke-interface {v4, v0}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v5

    .line 516
    const-string v7, "searchEnabled"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 517
    invoke-interface {v4, v0, v3}, Landroid/util/AttributeSet;->getAttributeBooleanValue(IZ)Z

    move-result v3

    .line 514
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 518
    :cond_0
    const-string v7, "allowShortcuts"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 519
    invoke-interface {v4, v0, v2}, Landroid/util/AttributeSet;->getAttributeBooleanValue(IZ)Z

    move-result v2

    goto :goto_1

    .line 521
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid attribute name "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    :cond_2
    move v2, v1

    move v3, v0

    .line 527
    :cond_3
    invoke-static {}, Lcom/google/android/gms/appdatasearch/y;->a()I

    move-result v0

    new-array v7, v0, [I

    .line 529
    new-instance v8, Lcom/google/android/gms/icing/impl/a/c;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/b;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-direct {v8, v0}, Lcom/google/android/gms/icing/impl/a/c;-><init>(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 530
    :goto_2
    invoke-virtual {v8}, Lcom/google/android/gms/icing/impl/a/c;->a()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 532
    invoke-virtual {v8}, Lcom/google/android/gms/icing/impl/a/c;->b()Ljava/lang/String;

    move-result-object v0

    .line 533
    const-string v4, "GlobalSearchSection"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 534
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid tag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " inside GlobalSearchCorpus; expected GlobalSearchSection"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 537
    :cond_4
    iget-object v0, v8, Lcom/google/android/gms/icing/impl/a/c;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-static {v0}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v9

    .line 538
    if-nez v9, :cond_5

    .line 539
    const-string v0, "No attributes specified"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    :cond_5
    move v0, v1

    move v4, v1

    move v5, v6

    .line 543
    :goto_3
    invoke-interface {v9}, Landroid/util/AttributeSet;->getAttributeCount()I

    move-result v10

    if-ge v0, v10, :cond_8

    .line 544
    invoke-interface {v9, v0}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v10

    .line 545
    const-string v11, "sectionType"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 546
    invoke-interface {v9, v0, v5}, Landroid/util/AttributeSet;->getAttributeIntValue(II)I

    move-result v5

    .line 543
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 547
    :cond_6
    const-string v4, "sectionContent"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 548
    invoke-direct {p0, v9, v0}, Lcom/google/android/gms/icing/impl/a/b;->b(Landroid/util/AttributeSet;I)I

    move-result v4

    goto :goto_4

    .line 550
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid attribute name "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 553
    :cond_8
    if-ne v5, v6, :cond_9

    .line 554
    const-string v0, "No sectionId specified"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 555
    :cond_9
    if-ltz v5, :cond_a

    invoke-static {}, Lcom/google/android/gms/appdatasearch/y;->a()I

    move-result v0

    if-le v5, v0, :cond_b

    .line 556
    :cond_a
    const-string v0, "Section ID out of range; badly formed XML?"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 558
    :cond_b
    aput v4, v7, v5

    goto/16 :goto_2

    .line 560
    :cond_c
    if-eqz v3, :cond_e

    .line 561
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 562
    if-eqz v2, :cond_d

    .line 563
    invoke-static {}, Lcom/google/android/gms/appdatasearch/w;->a()Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 565
    :cond_d
    new-instance v1, Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/gms/appdatasearch/Feature;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/appdatasearch/Feature;

    invoke-direct {v1, v7, v0}, Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;-><init>([I[Lcom/google/android/gms/appdatasearch/Feature;)V

    move-object v0, v1

    .line 568
    :goto_5
    return-object v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;
    .locals 3

    .prologue
    .line 668
    new-instance v0, Lcom/google/android/gms/icing/impl/a/d;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/b;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/b;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/icing/impl/a/d;-><init>(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/a;
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 147
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/b;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .line 148
    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v3, :cond_0

    .line 150
    :cond_1
    if-eq v0, v3, :cond_2

    .line 151
    const-string v0, "No start tag found!"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 180
    :catch_0
    move-exception v0

    .line 181
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to read search meta data from package "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/b;->a:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    move-object v0, v1

    .line 185
    :goto_0
    return-object v0

    .line 154
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/b;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "AppDataSearch"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Invalid root tag "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/b;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; expected AppDataSearch"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 183
    :catch_1
    move-exception v0

    .line 184
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to read search meta data from package "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/b;->a:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    move-object v0, v1

    .line 185
    goto :goto_0

    .line 160
    :cond_3
    :try_start_2
    new-instance v3, Lcom/google/android/gms/icing/impl/a/c;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/b;->c:Lorg/xmlpull/v1/XmlPullParser;

    invoke-direct {v3, v0}, Lcom/google/android/gms/icing/impl/a/c;-><init>(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 162
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object v2, v1

    .line 164
    :goto_1
    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/a/c;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 165
    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/a/c;->b()Ljava/lang/String;

    move-result-object v0

    .line 166
    const-string v5, "Corpus"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 167
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/b;->a()Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 168
    :cond_4
    const-string v5, "GlobalSearch"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 169
    if-eqz v2, :cond_5

    .line 170
    const-string v0, "Duplicate element GlobalSearch"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 172
    :cond_5
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/a/b;->c(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    move-result-object v0

    move-object v2, v0

    goto :goto_1

    .line 174
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid tag "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " inside AppDataSearch; expected Corpus"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " or GlobalSearch"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/b;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/d;

    move-result-object v0

    throw v0

    .line 177
    :cond_7
    new-instance v0, Lcom/google/android/gms/icing/impl/a/a;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/b;->a:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-direct {v0, v3, v4, v2}, Lcom/google/android/gms/icing/impl/a/a;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)V
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0
.end method
