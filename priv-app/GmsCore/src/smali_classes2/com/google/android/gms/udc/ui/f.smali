.class public final Lcom/google/android/gms/udc/ui/f;
.super Lcom/google/android/gms/udc/ui/j;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private k:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/gms/udc/ui/j;-><init>()V

    .line 111
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 41
    sget v0, Lcom/google/android/gms/l;->fy:I

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 42
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/f;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 44
    sget v2, Lcom/google/android/gms/j;->jw:I

    const-string v3, "UdcDialogTitle"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/udc/ui/f;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 45
    sget v2, Lcom/google/android/gms/j;->sy:I

    const-string v3, "UdcDialogMessage"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/udc/ui/f;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 46
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/CharSequence;

    const-string v3, "UdcDialogTitle"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    const-string v4, " "

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "UdcDialogMessage"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/udc/ui/f;->k:Ljava/lang/CharSequence;

    .line 49
    const-string v2, "UdcDialogShowRetry"

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    sget v0, Lcom/google/android/gms/j;->ty:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 51
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 52
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    :cond_0
    return-object v1
.end method

.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/f;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 62
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v2, "UdcDialogTitle"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "UdcDialogMessage"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 66
    const-string v2, "UdcDialogPositive"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 67
    const-string v2, "UdcDialogPositive"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 70
    :cond_0
    const-string v2, "UdcDialogNegative"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 71
    const-string v2, "UdcDialogNegative"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 74
    :cond_1
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 80
    instance-of v1, v0, Lcom/google/android/gms/udc/ui/h;

    if-eqz v1, :cond_0

    .line 81
    check-cast v0, Lcom/google/android/gms/udc/ui/h;

    invoke-interface {v0, p2}, Lcom/google/android/gms/udc/ui/h;->a(I)V

    .line 83
    :cond_0
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 88
    instance-of v1, v0, Lcom/google/android/gms/udc/ui/i;

    if-eqz v1, :cond_0

    .line 89
    check-cast v0, Lcom/google/android/gms/udc/ui/i;

    invoke-interface {v0}, Lcom/google/android/gms/udc/ui/i;->a()V

    .line 91
    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 2

    .prologue
    .line 95
    invoke-super {p0}, Lcom/google/android/gms/udc/ui/j;->onResume()V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/f;->getView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/f;->k:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 97
    return-void
.end method
