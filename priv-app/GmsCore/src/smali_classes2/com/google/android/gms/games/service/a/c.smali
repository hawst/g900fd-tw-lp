.class public abstract Lcom/google/android/gms/games/service/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/service/e;


# instance fields
.field protected final a:Lcom/google/android/gms/common/server/ClientContext;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/c;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 41
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 80
    return-void
.end method

.method protected abstract a(I)V
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)V
    .locals 3

    .prologue
    .line 47
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/games/service/a/c;->b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)I
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/games/h/c/a; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    .line 71
    :cond_0
    :goto_0
    :try_start_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/service/a/c;->a(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_3

    .line 77
    :goto_1
    return-void

    .line 48
    :catch_0
    move-exception v0

    .line 49
    const-string v1, "StatusOperation"

    const-string v2, "Auth error while performing operation, requesting reconnect"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 50
    const/4 v0, 0x2

    .line 68
    goto :goto_0

    .line 51
    :catch_1
    move-exception v1

    .line 52
    const-string v0, "StatusOperation"

    invoke-virtual {v1}, Lcom/google/android/gms/games/h/c/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 53
    invoke-virtual {v1}, Lcom/google/android/gms/games/h/c/a;->b()I

    move-result v0

    .line 57
    invoke-virtual {v1}, Lcom/google/android/gms/games/h/c/a;->a()I

    move-result v1

    .line 58
    const/16 v2, 0x5dc

    if-ne v1, v2, :cond_1

    .line 59
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/c;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v1, p2}, Lcom/google/android/gms/games/k/a;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/a/t;)V

    .line 60
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/c;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/gms/games/i/a;->e(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 61
    :cond_1
    const/16 v2, 0x3eb

    if-ne v1, v2, :cond_0

    .line 62
    invoke-virtual {p2, p1}, Lcom/google/android/gms/games/a/t;->c(Landroid/content/Context;)V

    goto :goto_0

    .line 64
    :catch_2
    move-exception v0

    .line 65
    const-string v1, "StatusOperation"

    const-string v2, "Killing (on development devices) due to RuntimeException"

    invoke-static {p1, v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 67
    const/4 v0, 0x1

    goto :goto_0

    .line 72
    :catch_3
    move-exception v0

    .line 75
    const-string v1, "StatusOperation"

    const-string v2, "When providing result "

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method protected abstract b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)I
.end method
