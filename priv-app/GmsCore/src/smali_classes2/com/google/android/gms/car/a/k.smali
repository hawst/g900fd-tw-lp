.class public final Lcom/google/android/gms/car/a/k;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/bluetooth/BluetoothAdapter;

.field c:Ljava/lang/String;

.field public d:Landroid/bluetooth/BluetoothDevice;

.field final e:Lcom/google/android/gms/car/a/p;

.field public final f:Lcom/google/android/gms/car/a/n;

.field public final g:Lcom/google/android/gms/car/a/s;

.field public final h:Lcom/google/android/gms/car/a/q;

.field public final i:Lcom/google/android/gms/car/a/r;

.field public final j:Lcom/google/android/gms/car/a/m;

.field public volatile k:I

.field public l:Lcom/google/android/gms/car/a/h;

.field public m:I

.field public n:I

.field public o:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/car/a/p;)V
    .locals 5

    .prologue
    const/4 v2, -0x3

    const/high16 v1, -0x80000000

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object v3, p0, Lcom/google/android/gms/car/a/k;->c:Ljava/lang/String;

    .line 79
    new-instance v0, Lcom/google/android/gms/car/a/n;

    invoke-direct {v0, p0, v4}, Lcom/google/android/gms/car/a/n;-><init>(Lcom/google/android/gms/car/a/k;B)V

    iput-object v0, p0, Lcom/google/android/gms/car/a/k;->f:Lcom/google/android/gms/car/a/n;

    .line 81
    new-instance v0, Lcom/google/android/gms/car/a/s;

    invoke-direct {v0, p0, v4}, Lcom/google/android/gms/car/a/s;-><init>(Lcom/google/android/gms/car/a/k;B)V

    iput-object v0, p0, Lcom/google/android/gms/car/a/k;->g:Lcom/google/android/gms/car/a/s;

    .line 82
    new-instance v0, Lcom/google/android/gms/car/a/q;

    invoke-direct {v0, p0, v4}, Lcom/google/android/gms/car/a/q;-><init>(Lcom/google/android/gms/car/a/k;B)V

    iput-object v0, p0, Lcom/google/android/gms/car/a/k;->h:Lcom/google/android/gms/car/a/q;

    .line 83
    new-instance v0, Lcom/google/android/gms/car/a/r;

    invoke-direct {v0, p0, v4}, Lcom/google/android/gms/car/a/r;-><init>(Lcom/google/android/gms/car/a/k;B)V

    iput-object v0, p0, Lcom/google/android/gms/car/a/k;->i:Lcom/google/android/gms/car/a/r;

    .line 85
    new-instance v0, Lcom/google/android/gms/car/a/m;

    invoke-direct {v0, p0, v4}, Lcom/google/android/gms/car/a/m;-><init>(Lcom/google/android/gms/car/a/k;B)V

    iput-object v0, p0, Lcom/google/android/gms/car/a/k;->j:Lcom/google/android/gms/car/a/m;

    .line 88
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/a/k;->k:I

    .line 92
    iput v1, p0, Lcom/google/android/gms/car/a/k;->m:I

    .line 93
    iput v1, p0, Lcom/google/android/gms/car/a/k;->n:I

    .line 99
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    const-string v0, "CAR.BT"

    const-string v1, "BluetoothUtil"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/car/a/k;->a:Landroid/content/Context;

    .line 104
    iput-object p3, p0, Lcom/google/android/gms/car/a/k;->e:Lcom/google/android/gms/car/a/p;

    .line 107
    if-eqz p2, :cond_1

    invoke-static {p2}, Landroid/bluetooth/BluetoothAdapter;->checkBluetoothAddress(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 108
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid peer Bluetooth address: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    const/4 v0, -0x2

    iput v0, p0, Lcom/google/android/gms/car/a/k;->k:I

    .line 110
    iput-object v3, p0, Lcom/google/android/gms/car/a/k;->b:Landroid/bluetooth/BluetoothAdapter;

    .line 111
    iput-object v3, p0, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    .line 170
    :goto_0
    return-void

    .line 116
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/a/k;->a:Landroid/content/Context;

    const-string v1, "bluetooth"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothManager;

    .line 118
    if-nez v0, :cond_2

    .line 119
    const-string v0, "CAR.BT"

    const-string v1, "Cannot get BluetoothManager"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    iput v2, p0, Lcom/google/android/gms/car/a/k;->k:I

    .line 121
    iput-object v3, p0, Lcom/google/android/gms/car/a/k;->b:Landroid/bluetooth/BluetoothAdapter;

    .line 122
    iput-object v3, p0, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    goto :goto_0

    .line 125
    :cond_2
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothManager;->getAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/a/k;->b:Landroid/bluetooth/BluetoothAdapter;

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/car/a/k;->b:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_3

    .line 127
    const-string v0, "CAR.BT"

    const-string v1, "Cannot get BluetoothAdapter"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    iput v2, p0, Lcom/google/android/gms/car/a/k;->k:I

    .line 129
    iput-object v3, p0, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    goto :goto_0

    .line 134
    :cond_3
    iput-object p2, p0, Lcom/google/android/gms/car/a/k;->c:Ljava/lang/String;

    .line 135
    if-eqz p2, :cond_4

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/car/a/k;->b:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, p2}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    .line 143
    :goto_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 144
    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 145
    iget-object v1, p0, Lcom/google/android/gms/car/a/k;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/car/a/k;->f:Lcom/google/android/gms/car/a/n;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 147
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 148
    const-string v1, "android.bluetooth.device.action.PAIRING_REQUEST"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 150
    const/16 v1, 0x3e7

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->setPriority(I)V

    .line 151
    iget-object v1, p0, Lcom/google/android/gms/car/a/k;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/car/a/k;->g:Lcom/google/android/gms/car/a/s;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 153
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 154
    const-string v1, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 155
    iget-object v1, p0, Lcom/google/android/gms/car/a/k;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/car/a/k;->h:Lcom/google/android/gms/car/a/q;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 157
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 158
    const-string v1, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 160
    iget-object v1, p0, Lcom/google/android/gms/car/a/k;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/car/a/k;->i:Lcom/google/android/gms/car/a/r;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 163
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 164
    const-string v1, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 166
    iget-object v1, p0, Lcom/google/android/gms/car/a/k;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/car/a/k;->j:Lcom/google/android/gms/car/a/m;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 169
    iput v4, p0, Lcom/google/android/gms/car/a/k;->k:I

    goto/16 :goto_0

    .line 138
    :cond_4
    iput-object v3, p0, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    goto :goto_1
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 247
    const-string v0, "CAR.BT"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    const-string v0, "CAR.BT"

    const-string v1, "isEnabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    :cond_0
    iget v0, p0, Lcom/google/android/gms/car/a/k;->k:I

    if-eqz v0, :cond_2

    .line 251
    const-string v0, "CAR.BT"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 252
    const-string v0, "CAR.BT"

    const-string v1, "isEnabled: This object wasn\'t initialized successfully."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    :cond_1
    const/4 v0, 0x0

    .line 256
    :goto_0
    return v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/a/k;->b:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v0, 0x0

    .line 265
    const-string v1, "CAR.BT"

    invoke-static {v1, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 266
    const-string v1, "CAR.BT"

    const-string v2, "enable"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    :cond_0
    iget v1, p0, Lcom/google/android/gms/car/a/k;->k:I

    if-eqz v1, :cond_2

    .line 269
    const-string v1, "CAR.BT"

    invoke-static {v1, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 270
    const-string v1, "CAR.BT"

    const-string v2, "enable: This object wasn\'t initialized successfully."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    :cond_1
    :goto_0
    return v0

    .line 276
    :cond_2
    :try_start_0
    const-class v0, Landroid/bluetooth/BluetoothAdapter;

    const-string v1, "enableNoAutoConnect"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 278
    if-nez v0, :cond_3

    .line 279
    const-string v0, "CAR.BT"

    const-string v1, "BluetoothAdapter.enableNoAutoConnect is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    iget-object v0, p0, Lcom/google/android/gms/car/a/k;->b:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    move-result v0

    goto :goto_0

    .line 283
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/car/a/k;->b:Landroid/bluetooth/BluetoothAdapter;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    goto :goto_0

    .line 285
    :catch_0
    move-exception v0

    const-string v0, "CAR.BT"

    const-string v1, "Cannot find BluetoothAdapter#enableNoAutoConnect"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    iget-object v0, p0, Lcom/google/android/gms/car/a/k;->b:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    move-result v0

    goto :goto_0

    .line 288
    :catch_1
    move-exception v0

    const-string v0, "CAR.BT"

    const-string v1, "IllegalAccessException from BluetoothAdapter#enableNoAutoConnect"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    iget-object v0, p0, Lcom/google/android/gms/car/a/k;->b:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    move-result v0

    goto :goto_0

    .line 291
    :catch_2
    move-exception v0

    const-string v0, "CAR.BT"

    const-string v1, "InvocationTargetException from BluetoothAdapter#enableNoAutoConnect"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    iget-object v0, p0, Lcom/google/android/gms/car/a/k;->b:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    move-result v0

    goto :goto_0
.end method

.method public final c()Z
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v0, 0x0

    .line 302
    const-string v1, "CAR.BT"

    invoke-static {v1, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 303
    const-string v1, "CAR.BT"

    const-string v2, "isPairing"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    :cond_0
    iget v1, p0, Lcom/google/android/gms/car/a/k;->k:I

    if-eqz v1, :cond_2

    .line 306
    const-string v1, "CAR.BT"

    invoke-static {v1, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 307
    const-string v1, "CAR.BT"

    const-string v2, "isPairing: This object wasn\'t initialized successfully."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v1

    const/16 v2, 0xb

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final d()Z
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v0, 0x0

    .line 320
    const-string v1, "CAR.BT"

    invoke-static {v1, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 321
    const-string v1, "CAR.BT"

    const-string v2, "isPaired"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    :cond_0
    iget v1, p0, Lcom/google/android/gms/car/a/k;->k:I

    if-eqz v1, :cond_2

    .line 324
    const-string v1, "CAR.BT"

    invoke-static {v1, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 325
    const-string v1, "CAR.BT"

    const-string v2, "isPaired: This object wasn\'t initialized successfully."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v1

    const/16 v2, 0xc

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final e()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/high16 v2, -0x80000000

    .line 367
    const-string v0, "CAR.BT"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 368
    const-string v0, "CAR.BT"

    const-string v1, "invalidateAuthenticationData"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    :cond_0
    iget v0, p0, Lcom/google/android/gms/car/a/k;->k:I

    if-eqz v0, :cond_2

    .line 371
    const-string v0, "CAR.BT"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 372
    const-string v0, "CAR.BT"

    const-string v1, "invalidateAuthenticationData: This object wasn\'t initialized successfully"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    :cond_1
    :goto_0
    return-void

    .line 377
    :cond_2
    iput v2, p0, Lcom/google/android/gms/car/a/k;->m:I

    .line 378
    iput v2, p0, Lcom/google/android/gms/car/a/k;->n:I

    .line 379
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/a/k;->o:Ljava/lang/String;

    goto :goto_0
.end method

.method public final f()Z
    .locals 5

    .prologue
    const/4 v3, 0x3

    const/4 v1, 0x0

    .line 413
    const-string v0, "CAR.BT"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 414
    const-string v0, "CAR.BT"

    const-string v2, "unpair"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    :cond_0
    iget v0, p0, Lcom/google/android/gms/car/a/k;->k:I

    if-eqz v0, :cond_2

    .line 417
    const-string v0, "CAR.BT"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 418
    const-string v0, "CAR.BT"

    const-string v2, "unpair: This object wasn\'t initialized successfully."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    move v0, v1

    .line 473
    :goto_0
    return v0

    .line 423
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v0

    .line 425
    const/16 v2, 0xb

    if-ne v0, v2, :cond_3

    .line 429
    :try_start_0
    const-class v2, Landroid/bluetooth/BluetoothDevice;

    const-string v3, "cancelBondProcess"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 431
    if-nez v2, :cond_4

    .line 432
    const-string v2, "CAR.BT"

    const-string v3, "BluetoothDevice.cancelBondProcess is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    :cond_3
    :goto_1
    const/16 v2, 0xa

    if-ne v0, v2, :cond_5

    move v0, v1

    .line 452
    goto :goto_0

    .line 436
    :cond_4
    iget-object v3, p0, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_1

    .line 440
    :catch_0
    move-exception v2

    const-string v2, "CAR.BT"

    const-string v3, "Cannot find BluetoothDevice#cancelBondProcess"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 443
    :catch_1
    move-exception v2

    const-string v2, "CAR.BT"

    const-string v3, "IllegalAccessException from BluetoothDevice#cancelBondProcess"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 446
    :catch_2
    move-exception v2

    const-string v2, "CAR.BT"

    const-string v3, "InvocationTargetException from BluetoothDevice#cancelBondProcess"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 458
    :cond_5
    :try_start_1
    const-class v0, Landroid/bluetooth/BluetoothDevice;

    const-string v2, "removeBond"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 459
    if-nez v0, :cond_6

    .line 460
    const-string v0, "CAR.BT"

    const-string v2, "BluetoothDevice.removeBond is null"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 461
    goto :goto_0

    .line 464
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_5

    move-result v0

    goto :goto_0

    .line 466
    :catch_3
    move-exception v0

    const-string v0, "CAR.BT"

    const-string v2, "Cannot find BluetoothDevice#removeBond"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 467
    goto :goto_0

    .line 469
    :catch_4
    move-exception v0

    const-string v0, "CAR.BT"

    const-string v2, "IllegalAccessException from BluetoothDevice#removeBond"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 470
    goto/16 :goto_0

    .line 472
    :catch_5
    move-exception v0

    const-string v0, "CAR.BT"

    const-string v2, "InvocationTargetException from BluetoothDevice#removeBond"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 473
    goto/16 :goto_0
.end method

.method public final g()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    const/4 v5, 0x3

    .line 523
    iget v1, p0, Lcom/google/android/gms/car/a/k;->m:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/car/a/k;->o:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 524
    :cond_0
    const-string v0, "CAR.BT"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 525
    const-string v0, "CAR.BT"

    const-string v1, "authenticateIfReady not ready yet"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    :cond_1
    :goto_0
    return-void

    .line 530
    :cond_2
    const-string v1, "CAR.BT"

    invoke-static {v1, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 531
    const-string v1, "CAR.BT"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "authenticateIfReady: requested pairing method="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/car/a/k;->m:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", requested pairing key="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/car/a/k;->n:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", pairing key from client="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/car/a/k;->o:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    :cond_3
    const-string v1, "CAR.BT"

    invoke-static {v1, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 540
    const-string v1, "CAR.BT"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Pairing Bluetooth using method "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/car/a/k;->m:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    :cond_4
    iget v1, p0, Lcom/google/android/gms/car/a/k;->m:I

    packed-switch v1, :pswitch_data_0

    .line 575
    :pswitch_0
    const-string v1, "CAR.BT"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid Bluetooth pairing method: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/car/a/k;->m:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    :cond_5
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/car/a/k;->e()V

    .line 581
    if-nez v0, :cond_1

    .line 582
    const-string v0, "CAR.BT"

    const-string v1, "Authentication failed. Unpairing"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 583
    invoke-virtual {p0}, Lcom/google/android/gms/car/a/k;->f()Z

    goto :goto_0

    .line 546
    :pswitch_1
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/car/a/k;->o:Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 552
    iget-object v1, p0, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1, v0}, Landroid/bluetooth/BluetoothDevice;->setPin([B)Z

    move-result v0

    .line 553
    const-string v1, "CAR.BT"

    invoke-static {v1, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 554
    const-string v1, "CAR.BT"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setPin returned "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 548
    :catch_0
    move-exception v1

    const-string v1, "CAR.BT"

    const-string v2, "Cannot encode the authentication data from the car"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 559
    :pswitch_2
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%06d"

    new-array v3, v6, [Ljava/lang/Object;

    iget v4, p0, Lcom/google/android/gms/car/a/k;->n:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 560
    iget-object v2, p0, Lcom/google/android/gms/car/a/k;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 561
    const-string v1, "CAR.BT"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bluetooth pairing authentication data mismatch. Pairing passkey from intent="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/car/a/k;->n:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", auth data from client="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/car/a/k;->o:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    iget-object v1, p0, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1, v0}, Landroid/bluetooth/BluetoothDevice;->setPairingConfirmation(Z)Z

    goto/16 :goto_1

    .line 568
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v6}, Landroid/bluetooth/BluetoothDevice;->setPairingConfirmation(Z)Z

    move-result v0

    .line 569
    const-string v1, "CAR.BT"

    invoke-static {v1, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 570
    const-string v1, "CAR.BT"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setPairingConfirmation returned "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 542
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
