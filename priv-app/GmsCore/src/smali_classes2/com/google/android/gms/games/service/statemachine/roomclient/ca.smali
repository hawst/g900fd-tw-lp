.class abstract Lcom/google/android/gms/games/service/statemachine/roomclient/ca;
.super Lcom/google/android/gms/games/service/statemachine/m;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

.field protected final b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

.field protected final c:Lcom/google/android/gms/games/service/statemachine/d;

.field protected final d:Lcom/google/android/gms/games/service/statemachine/roomclient/cb;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->f()Lcom/google/android/gms/games/service/statemachine/d;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/statemachine/m;-><init>(Lcom/google/android/gms/games/service/statemachine/d;)V

    .line 26
    iput-object p1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/cb;

    .line 27
    invoke-virtual {p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    .line 28
    invoke-virtual {p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->f()Lcom/google/android/gms/games/service/statemachine/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;->c:Lcom/google/android/gms/games/service/statemachine/d;

    .line 29
    invoke-virtual {p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    .line 30
    return-void
.end method

.method protected static a(Ljava/lang/String;)Landroid/os/RemoteException;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xf
    .end annotation

    .prologue
    .line 50
    const/16 v0, 0xf

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0, p0}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    .line 53
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/os/RemoteException;)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/cb;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->b(Landroid/os/RemoteException;)V

    .line 46
    return-void
.end method

.method protected final g()V
    .locals 2

    .prologue
    .line 34
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->b:Lcom/google/android/gms/games/internal/ed;

    const-string v1, "Libjingle did not disconnect"

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/internal/ed;->a(Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->g:Lcom/google/android/gms/games/service/statemachine/f;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/f;->a()Lcom/google/android/gms/games/service/statemachine/g;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    :goto_0
    return-void

    .line 39
    :catch_0
    move-exception v0

    .line 40
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;->a(Landroid/os/RemoteException;)V

    goto :goto_0
.end method
