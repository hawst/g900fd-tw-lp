.class public final Lcom/google/android/gms/games/service/a/e/f;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:Lcom/google/android/gms/games/a/au;

.field private final e:Ljava/lang/String;

.field private final f:I

.field private final g:Z

.field private final h:Z

.field private final i:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZZZ)V
    .locals 1

    .prologue
    .line 27
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 28
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/e/f;->c:Lcom/google/android/gms/games/internal/dr;

    .line 29
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/e/f;->d:Lcom/google/android/gms/games/a/au;

    .line 30
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/e/f;->e:Ljava/lang/String;

    .line 31
    iput p4, p0, Lcom/google/android/gms/games/service/a/e/f;->f:I

    .line 32
    iput-boolean p5, p0, Lcom/google/android/gms/games/service/a/e/f;->g:Z

    .line 33
    iput-boolean p6, p0, Lcom/google/android/gms/games/service/a/e/f;->h:Z

    .line 34
    iput-boolean p7, p0, Lcom/google/android/gms/games/service/a/e/f;->i:Z

    .line 35
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/e/f;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->h(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 47
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    .prologue
    .line 40
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/e/f;->d:Lcom/google/android/gms/games/a/au;

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/e/f;->e:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/games/service/a/e/f;->f:I

    iget-boolean v4, p0, Lcom/google/android/gms/games/service/a/e/f;->g:Z

    iget-boolean v5, p0, Lcom/google/android/gms/games/service/a/e/f;->h:Z

    iget-boolean v6, p0, Lcom/google/android/gms/games/service/a/e/f;->i:Z

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZZZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
