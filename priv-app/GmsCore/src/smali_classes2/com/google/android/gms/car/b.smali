.class final Lcom/google/android/gms/car/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Lcom/google/android/gms/car/d;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/content/pm/PackageInstaller;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x1

    .line 46
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/gms/car/d;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/gms/car/d;

    const-string v3, "com.google.android.projection.gearhead"

    const-wide/16 v4, -0x1

    const-string v6, "gearhead_minimum_version"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/android/gms/car/d;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/gms/car/d;

    const-string v3, "com.google.android.apps.maps"

    const-string v4, "gmm_minimum_version"

    invoke-direct {v2, v3, v8, v9, v4}, Lcom/google/android/gms/car/d;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/google/android/gms/car/d;

    const-string v3, "com.google.android.googlequicksearchbox"

    const-string v4, "gsa_minimum_version"

    invoke-direct {v2, v3, v8, v9, v4}, Lcom/google/android/gms/car/d;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/google/android/gms/car/d;

    const-string v3, "com.google.android.music"

    const-string v4, "music_minimum_version"

    invoke-direct {v2, v3, v8, v9, v4}, Lcom/google/android/gms/car/d;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/car/b;->a:[Lcom/google/android/gms/car/d;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/b;->d:I

    .line 62
    iput-object p1, p0, Lcom/google/android/gms/car/b;->b:Landroid/content/Context;

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/b;->c:Landroid/content/pm/PackageInstaller;

    .line 64
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/content/pm/PackageInstaller$SessionInfo;
    .locals 3

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/gms/car/b;->c:Landroid/content/pm/PackageInstaller;

    invoke-virtual {v0}, Landroid/content/pm/PackageInstaller;->getAllSessions()Ljava/util/List;

    move-result-object v0

    .line 102
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInstaller$SessionInfo;

    .line 103
    invoke-virtual {v0}, Landroid/content/pm/PackageInstaller$SessionInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 107
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/car/b;)Landroid/content/pm/PackageInstaller;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/car/b;->c:Landroid/content/pm/PackageInstaller;

    return-object v0
.end method

.method static a(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 114
    sget-object v2, Lcom/google/android/gms/car/b;->a:[Lcom/google/android/gms/car/d;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 115
    invoke-virtual {v4, p0}, Lcom/google/android/gms/car/d;->a(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 119
    :goto_1
    return v0

    .line 114
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 119
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method final a()Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 71
    :cond_0
    iget v0, p0, Lcom/google/android/gms/car/b;->d:I

    sget-object v1, Lcom/google/android/gms/car/b;->a:[Lcom/google/android/gms/car/d;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 72
    sget-object v0, Lcom/google/android/gms/car/b;->a:[Lcom/google/android/gms/car/d;

    iget v1, p0, Lcom/google/android/gms/car/b;->d:I

    aget-object v1, v0, v1

    .line 73
    iget v0, p0, Lcom/google/android/gms/car/b;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/b;->d:I

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/car/b;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/d;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v1, Lcom/google/android/gms/car/d;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/b;->a(Ljava/lang/String;)Landroid/content/pm/PackageInstaller$SessionInfo;

    move-result-object v0

    if-nez v0, :cond_0

    .line 75
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.vending.billing.PURCHASE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "com.android.vending"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "backend"

    const/4 v3, 0x3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "document_type"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "full_docid"

    iget-object v3, v1, Lcom/google/android/gms/car/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "backend_docid"

    iget-object v1, v1, Lcom/google/android/gms/car/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "offer_type"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 78
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final a(Lcom/google/android/car/fsm/FsmController;)V
    .locals 7

    .prologue
    .line 87
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 89
    sget-object v2, Lcom/google/android/gms/car/b;->a:[Lcom/google/android/gms/car/d;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 90
    iget-object v5, v4, Lcom/google/android/gms/car/d;->a:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/google/android/gms/car/b;->a(Ljava/lang/String;)Landroid/content/pm/PackageInstaller$SessionInfo;

    move-result-object v5

    .line 91
    iget-object v6, p0, Lcom/google/android/gms/car/b;->b:Landroid/content/Context;

    invoke-virtual {v4, v6}, Lcom/google/android/gms/car/d;->a(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz v5, :cond_0

    .line 92
    invoke-virtual {v5}, Landroid/content/pm/PackageInstaller$SessionInfo;->getSessionId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 89
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/b;->c:Landroid/content/pm/PackageInstaller;

    new-instance v2, Lcom/google/android/gms/car/c;

    invoke-direct {v2, p0, v1, p1}, Lcom/google/android/gms/car/c;-><init>(Lcom/google/android/gms/car/b;Ljava/util/Set;Lcom/google/android/car/fsm/FsmController;)V

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageInstaller;->registerSessionCallback(Landroid/content/pm/PackageInstaller$SessionCallback;)V

    .line 98
    return-void
.end method
