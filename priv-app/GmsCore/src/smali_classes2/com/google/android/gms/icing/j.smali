.class public final Lcom/google/android/gms/icing/j;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:J

.field public c:[Lcom/google/android/gms/icing/k;

.field public d:I

.field public e:Z

.field public f:I

.field public g:Lcom/google/android/gms/icing/l;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 2460
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2461
    iput-wide v2, p0, Lcom/google/android/gms/icing/j;->a:J

    iput-wide v2, p0, Lcom/google/android/gms/icing/j;->b:J

    invoke-static {}, Lcom/google/android/gms/icing/k;->a()[Lcom/google/android/gms/icing/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    iput v1, p0, Lcom/google/android/gms/icing/j;->d:I

    iput-boolean v1, p0, Lcom/google/android/gms/icing/j;->e:Z

    iput v1, p0, Lcom/google/android/gms/icing/j;->f:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/j;->cachedSize:I

    .line 2462
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 2567
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2568
    iget-wide v2, p0, Lcom/google/android/gms/icing/j;->a:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 2569
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/icing/j;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2572
    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/icing/j;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 2573
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/icing/j;->b:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2576
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 2577
    const/4 v1, 0x0

    move v6, v1

    move v1, v0

    move v0, v6

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 2578
    iget-object v2, p0, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    aget-object v2, v2, v0

    .line 2579
    if-eqz v2, :cond_2

    .line 2580
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 2577
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2585
    :cond_4
    iget v1, p0, Lcom/google/android/gms/icing/j;->d:I

    if-eqz v1, :cond_5

    .line 2586
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/icing/j;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2589
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/gms/icing/j;->e:Z

    if-eqz v1, :cond_6

    .line 2590
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/gms/icing/j;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2593
    :cond_6
    iget v1, p0, Lcom/google/android/gms/icing/j;->f:I

    if-eqz v1, :cond_7

    .line 2594
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/gms/icing/j;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2597
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;

    if-eqz v1, :cond_8

    .line 2598
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2601
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2478
    if-ne p1, p0, :cond_1

    .line 2513
    :cond_0
    :goto_0
    return v0

    .line 2481
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/j;

    if-nez v2, :cond_2

    move v0, v1

    .line 2482
    goto :goto_0

    .line 2484
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/j;

    .line 2485
    iget-wide v2, p0, Lcom/google/android/gms/icing/j;->a:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/j;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 2486
    goto :goto_0

    .line 2488
    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/icing/j;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/j;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    .line 2489
    goto :goto_0

    .line 2491
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    iget-object v3, p1, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 2493
    goto :goto_0

    .line 2495
    :cond_5
    iget v2, p0, Lcom/google/android/gms/icing/j;->d:I

    iget v3, p1, Lcom/google/android/gms/icing/j;->d:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 2496
    goto :goto_0

    .line 2498
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/gms/icing/j;->e:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/j;->e:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2499
    goto :goto_0

    .line 2501
    :cond_7
    iget v2, p0, Lcom/google/android/gms/icing/j;->f:I

    iget v3, p1, Lcom/google/android/gms/icing/j;->f:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 2502
    goto :goto_0

    .line 2504
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;

    if-nez v2, :cond_9

    .line 2505
    iget-object v2, p1, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;

    if-eqz v2, :cond_0

    move v0, v1

    .line 2506
    goto :goto_0

    .line 2509
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;

    iget-object v3, p1, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/l;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2510
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 2518
    iget-wide v0, p0, Lcom/google/android/gms/icing/j;->a:J

    iget-wide v2, p0, Lcom/google/android/gms/icing/j;->a:J

    ushr-long/2addr v2, v6

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    .line 2521
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/j;->b:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/j;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 2523
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2525
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/j;->d:I

    add-int/2addr v0, v1

    .line 2526
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/icing/j;->e:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v1

    .line 2527
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/j;->f:I

    add-int/2addr v0, v1

    .line 2528
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    add-int/2addr v0, v1

    .line 2530
    return v0

    .line 2526
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0

    .line 2528
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/l;->hashCode()I

    move-result v0

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2160
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/icing/j;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/icing/j;->b:J

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/k;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/icing/k;

    invoke-direct {v3}, Lcom/google/android/gms/icing/k;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/icing/k;

    invoke-direct {v3}, Lcom/google/android/gms/icing/k;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/icing/j;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/j;->e:Z

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/j;->f:I

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/gms/icing/l;

    invoke-direct {v0}, Lcom/google/android/gms/icing/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 2536
    iget-wide v0, p0, Lcom/google/android/gms/icing/j;->a:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 2537
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/icing/j;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2539
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/icing/j;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 2540
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/icing/j;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2542
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 2543
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 2544
    iget-object v1, p0, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    aget-object v1, v1, v0

    .line 2545
    if-eqz v1, :cond_2

    .line 2546
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2543
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2550
    :cond_3
    iget v0, p0, Lcom/google/android/gms/icing/j;->d:I

    if-eqz v0, :cond_4

    .line 2551
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/icing/j;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2553
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/icing/j;->e:Z

    if-eqz v0, :cond_5

    .line 2554
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/gms/icing/j;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2556
    :cond_5
    iget v0, p0, Lcom/google/android/gms/icing/j;->f:I

    if-eqz v0, :cond_6

    .line 2557
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/gms/icing/j;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2559
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;

    if-eqz v0, :cond_7

    .line 2560
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2562
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2563
    return-void
.end method
