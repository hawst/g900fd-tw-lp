.class public final Lcom/google/android/gms/car/pc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/io/InputStream;

.field final b:Ljava/io/OutputStream;

.field final c:Lcom/google/android/gms/car/gx;

.field final d:Lcom/google/android/gms/car/senderprotocol/k;

.field final e:[Lcom/google/android/gms/car/senderprotocol/a;

.field f:Lcom/google/android/gms/car/senderprotocol/ah;

.field g:Lcom/google/android/gms/car/senderprotocol/ba;

.field h:Lcom/google/android/gms/car/senderprotocol/be;

.field i:Lcom/google/android/gms/car/senderprotocol/ar;

.field j:Lcom/google/android/gms/car/senderprotocol/c;

.field k:Lcom/google/android/gms/car/senderprotocol/at;

.field l:Lcom/google/android/gms/car/senderprotocol/an;

.field m:Lcom/google/android/gms/car/senderprotocol/ak;

.field n:Lcom/google/android/gms/car/senderprotocol/ax;

.field o:Lcom/google/android/gms/car/senderprotocol/av;

.field volatile p:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/senderprotocol/o;Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/google/android/gms/car/gx;Landroid/content/Context;Lcom/google/android/gms/car/senderprotocol/w;Lcom/google/android/gms/car/senderprotocol/v;Lcom/google/android/gms/car/senderprotocol/u;Lcom/google/android/gms/car/senderprotocol/t;Lcom/google/android/gms/car/eb;)V
    .locals 9

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/gms/car/senderprotocol/a;

    iput-object v0, p0, Lcom/google/android/gms/car/pc;->e:[Lcom/google/android/gms/car/senderprotocol/a;

    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/pc;->p:I

    .line 71
    iput-object p4, p0, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    .line 73
    iput-object p2, p0, Lcom/google/android/gms/car/pc;->a:Ljava/io/InputStream;

    .line 74
    iput-object p3, p0, Lcom/google/android/gms/car/pc;->b:Ljava/io/OutputStream;

    .line 75
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v2, p0, Lcom/google/android/gms/car/pc;->a:Ljava/io/InputStream;

    iget-object v3, p0, Lcom/google/android/gms/car/pc;->b:Ljava/io/OutputStream;

    move-object v1, p1

    move-object v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/car/senderprotocol/k;-><init>(Lcom/google/android/gms/car/senderprotocol/o;Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/google/android/gms/car/senderprotocol/w;Lcom/google/android/gms/car/senderprotocol/v;Lcom/google/android/gms/car/senderprotocol/u;Lcom/google/android/gms/car/senderprotocol/t;Lcom/google/android/gms/car/eb;)V

    iput-object v0, p0, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    .line 80
    iget-object v3, p0, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    new-instance v0, Lcom/google/android/gms/car/senderprotocol/e;

    const/4 v1, 0x0

    const/16 v2, -0x80

    iget-object v4, v3, Lcom/google/android/gms/car/senderprotocol/k;->t:Lcom/google/android/gms/car/senderprotocol/q;

    iget-object v5, v3, Lcom/google/android/gms/car/senderprotocol/k;->u:Lcom/google/android/gms/car/senderprotocol/n;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/senderprotocol/e;-><init>(IILcom/google/android/gms/car/senderprotocol/k;Lcom/google/android/gms/car/senderprotocol/f;Landroid/os/Handler;)V

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/e;->b()V

    iget-object v1, v3, Lcom/google/android/gms/car/senderprotocol/k;->t:Lcom/google/android/gms/car/senderprotocol/q;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/senderprotocol/q;->a(Lcom/google/android/gms/car/senderprotocol/e;)V

    iget-object v1, v3, Lcom/google/android/gms/car/senderprotocol/k;->t:Lcom/google/android/gms/car/senderprotocol/q;

    invoke-virtual {v1}, Lcom/google/android/gms/car/senderprotocol/q;->a()V

    iget-object v1, v3, Lcom/google/android/gms/car/senderprotocol/k;->c:[Lcom/google/android/gms/car/senderprotocol/e;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    .line 81
    invoke-virtual {p5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 82
    iget-object v1, p0, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v1, v1, Lcom/google/android/gms/car/senderprotocol/k;->t:Lcom/google/android/gms/car/senderprotocol/q;

    .line 83
    sget v1, Lcom/google/android/gms/o;->b:I

    invoke-static {v0, v1}, Lcom/google/android/gms/car/pc;->a(Landroid/content/res/Resources;I)[B

    move-result-object v1

    sput-object v1, Lcom/google/android/gms/car/senderprotocol/q;->b:[B

    .line 84
    sget v1, Lcom/google/android/gms/o;->c:I

    invoke-static {v0, v1}, Lcom/google/android/gms/car/pc;->a(Landroid/content/res/Resources;I)[B

    move-result-object v1

    sput-object v1, Lcom/google/android/gms/car/senderprotocol/q;->c:[B

    .line 85
    sget v1, Lcom/google/android/gms/o;->a:I

    invoke-static {v0, v1}, Lcom/google/android/gms/car/pc;->a(Landroid/content/res/Resources;I)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/car/senderprotocol/q;->d:[B

    .line 86
    const-string v0, "Android"

    sput-object v0, Lcom/google/android/gms/car/senderprotocol/q;->a:Ljava/lang/String;

    .line 87
    return-void
.end method

.method private static a(Landroid/content/res/Resources;I)[B
    .locals 6

    .prologue
    .line 341
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 342
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    .line 343
    const/16 v0, 0x400

    new-array v3, v0, [B

    .line 344
    const/4 v0, 0x0

    .line 345
    :cond_0
    :goto_0
    const/4 v4, -0x1

    if-eq v0, v4, :cond_1

    .line 347
    :try_start_0
    invoke-virtual {v2, v3}, Ljava/io/InputStream;->read([B)I

    move-result v0

    .line 348
    if-lez v0, :cond_0

    .line 349
    invoke-virtual {v1, v3}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 352
    :catch_0
    move-exception v4

    const-string v4, "CAR.GAL"

    const-string v5, "Error reading raw resource."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 355
    :cond_1
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/k;->t:Lcom/google/android/gms/car/senderprotocol/q;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/q;->b()V

    .line 123
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/ig;)V
    .locals 4

    .prologue
    .line 240
    invoke-virtual {p1}, Lcom/google/android/gms/car/ig;->a()Ljava/lang/String;

    move-result-object v1

    .line 241
    new-instance v2, Lcom/google/android/gms/car/senderprotocol/bc;

    iget-object v0, p0, Lcom/google/android/gms/car/pc;->c:Lcom/google/android/gms/car/gx;

    invoke-direct {v2, p1, v0}, Lcom/google/android/gms/car/senderprotocol/bc;-><init>(Lcom/google/android/gms/car/ig;Lcom/google/android/gms/car/gx;)V

    .line 244
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v0, p0, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/k;->s:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/senderprotocol/p;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Vendor extension service: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not available."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 255
    :catch_0
    move-exception v0

    .line 254
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 244
    :cond_0
    :try_start_1
    iget v0, v0, Lcom/google/android/gms/car/senderprotocol/p;->b:I

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v2, v1}, Lcom/google/android/gms/car/senderprotocol/k;->a(ILcom/google/android/gms/car/senderprotocol/az;I)Lcom/google/android/gms/car/senderprotocol/e;

    .line 247
    monitor-enter v2
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 248
    :goto_0
    :try_start_2
    iget-boolean v0, v2, Lcom/google/android/gms/car/senderprotocol/bc;->a:Z

    if-nez v0, :cond_1

    .line 249
    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 251
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2

    throw v0
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_1
    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/gms/car/pc;->d:Lcom/google/android/gms/car/senderprotocol/k;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/k;->a()V

    .line 264
    iget-object v0, p0, Lcom/google/android/gms/car/pc;->h:Lcom/google/android/gms/car/senderprotocol/be;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/google/android/gms/car/pc;->h:Lcom/google/android/gms/car/senderprotocol/be;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/be;->c()V

    .line 267
    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_2

    .line 268
    iget-object v1, p0, Lcom/google/android/gms/car/pc;->e:[Lcom/google/android/gms/car/senderprotocol/a;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    .line 269
    iget-object v1, p0, Lcom/google/android/gms/car/pc;->e:[Lcom/google/android/gms/car/senderprotocol/a;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/gms/car/senderprotocol/a;->c()V

    .line 267
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 272
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/pc;->f:Lcom/google/android/gms/car/senderprotocol/ah;

    if-eqz v0, :cond_3

    .line 273
    iget-object v0, p0, Lcom/google/android/gms/car/pc;->f:Lcom/google/android/gms/car/senderprotocol/ah;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/ah;->c()V

    .line 275
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/pc;->i:Lcom/google/android/gms/car/senderprotocol/ar;

    if-eqz v0, :cond_4

    .line 276
    iget-object v0, p0, Lcom/google/android/gms/car/pc;->i:Lcom/google/android/gms/car/senderprotocol/ar;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/ar;->c()V

    .line 278
    :cond_4
    return-void
.end method
