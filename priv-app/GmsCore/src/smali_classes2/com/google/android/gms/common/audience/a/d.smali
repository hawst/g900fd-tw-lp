.class public final Lcom/google/android/gms/common/audience/a/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/audience/a/b;
.implements Lcom/google/android/gms/common/audience/a/c;
.implements Lcom/google/android/gms/common/audience/a/j;
.implements Lcom/google/android/gms/common/audience/a/k;
.implements Lcom/google/android/gms/common/audience/a/p;
.implements Lcom/google/android/gms/common/audience/a/q;


# instance fields
.field public final a:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    .line 98
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 90
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/audience/a/d;-><init>(Landroid/content/Intent;)V

    .line 91
    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/google/android/gms/common/people/data/AudienceMember;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 831
    const-string v1, "com.google.android.gms.common.acl.EXTRA_UPDATE_PERSON"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 832
    const-string v1, "com.google.android.gms.common.acl.EXTRA_UPDATE_PERSON_ID"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 839
    :goto_0
    return-object v0

    .line 835
    :cond_0
    const-string v1, "com.google.android.gms.common.acl.EXTRA_UPDATE_PERSON_ID"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0, v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    goto :goto_0

    .line 839
    :cond_1
    const-string v0, "com.google.android.gms.common.acl.EXTRA_UPDATE_PERSON"

    sget-object v1, Lcom/google/android/gms/common/people/data/AudienceMember;->CREATOR:Lcom/google/android/gms/common/people/data/d;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/common/internal/safeparcel/d;->a(Landroid/content/Intent;Ljava/lang/String;Landroid/os/Parcelable$Creator;)Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    goto :goto_0
.end method

.method public static b(Landroid/content/Intent;)Ljava/util/List;
    .locals 1

    .prologue
    .line 984
    const-string v0, "com.google.android.gms.common.acl.EXTRA_INITIAL_AUDIENCE"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 985
    const-string v0, "com.google.android.gms.common.acl.EXTRA_INITIAL_AUDIENCE"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 987
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Landroid/content/Intent;)Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 1017
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1018
    invoke-static {p0}, Lcom/google/android/gms/common/audience/a/d;->b(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v1

    .line 1019
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1020
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1022
    :cond_0
    const-string v1, "com.google.android.gms.common.acl.EXTRA_REMOVED_AUDIENCE"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1023
    if-eqz v1, :cond_1

    .line 1024
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 1026
    :cond_1
    const-string v1, "com.google.android.gms.common.acl.EXTRA_ADDED_AUDIENCE"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1027
    if-eqz v1, :cond_2

    .line 1028
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1030
    :cond_2
    return-object v0
.end method

.method private static f(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 1041
    instance-of v0, p0, Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1042
    check-cast p0, Ljava/util/ArrayList;

    .line 1045
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object p0, v0

    goto :goto_0
.end method

.method private q(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/d;
    .locals 2

    .prologue
    .line 516
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "DESCRIPTION_TEXT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 517
    return-object p0
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/gms/common/people/data/Audience;)Lcom/google/android/gms/common/audience/a/b;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/audience/a/d;->c(Ljava/util/List;)Lcom/google/android/gms/common/audience/a/d;

    return-object p0
.end method

.method public final synthetic a(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/b;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/audience/a/d;->e(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/util/List;)Lcom/google/android/gms/common/audience/a/b;
    .locals 0

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/audience/a/d;->c(Ljava/util/List;)Lcom/google/android/gms/common/audience/a/d;

    return-object p0
.end method

.method public final a(Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/audience/a/d;
    .locals 2

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_UPDATE_PERSON"

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/common/internal/safeparcel/d;->a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;Landroid/content/Intent;Ljava/lang/String;)V

    .line 197
    return-object p0
.end method

.method public final a(Ljava/util/ArrayList;)Lcom/google/android/gms/common/audience/a/d;
    .locals 2

    .prologue
    .line 697
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_ADDED_AUDIENCE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 698
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/gms/common/audience/a/d;
    .locals 2

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "ALL_CIRCLES_CHECKED"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 249
    return-object p0
.end method

.method public final a(I)Lcom/google/android/gms/common/audience/a/k;
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_HEADER_TEXT_COLOR"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 153
    return-object p0
.end method

.method public final synthetic b()Lcom/google/android/gms/common/audience/a/b;
    .locals 3

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "SHOW_CANCEL_VISIBLE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object p0
.end method

.method public final synthetic b(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/b;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/audience/a/d;->g(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/util/List;)Lcom/google/android/gms/common/audience/a/b;
    .locals 3

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "INITIAL_ACL"

    invoke-static {p1}, Lcom/google/android/gms/common/audience/a/d;->f(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    return-object p0
.end method

.method public final b(Ljava/util/ArrayList;)Lcom/google/android/gms/common/audience/a/d;
    .locals 2

    .prologue
    .line 708
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_REMOVED_AUDIENCE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 709
    return-object p0
.end method

.method public final b(Z)Lcom/google/android/gms/common/audience/a/d;
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "ALL_CONTACTS_CHECKED"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 264
    return-object p0
.end method

.method public final b(I)Lcom/google/android/gms/common/audience/a/k;
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_HEADER_BACKGROUND_COLOR"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 164
    return-object p0
.end method

.method public final synthetic b(Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/audience/a/k;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/audience/a/d;->a(Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/b;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/audience/a/d;->h(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final c(I)Lcom/google/android/gms/common/audience/a/d;
    .locals 2

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_DOMAIN_RESTRICTED"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 220
    return-object p0
.end method

.method public final c(Ljava/util/List;)Lcom/google/android/gms/common/audience/a/d;
    .locals 3

    .prologue
    .line 672
    if-nez p1, :cond_0

    .line 673
    sget-object p1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    .line 676
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_INITIAL_AUDIENCE"

    invoke-static {p1}, Lcom/google/android/gms/common/audience/a/d;->f(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 677
    return-object p0
.end method

.method public final synthetic c(Z)Lcom/google/android/gms/common/audience/a/p;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/audience/a/d;->b(Z)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    invoke-static {v0}, Lcom/google/android/gms/common/audience/a/d;->c(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/b;
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/audience/a/d;->q(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d(Ljava/util/List;)Lcom/google/android/gms/common/audience/a/k;
    .locals 0

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/audience/a/d;->c(Ljava/util/List;)Lcom/google/android/gms/common/audience/a/d;

    return-object p0
.end method

.method public final synthetic d(Z)Lcom/google/android/gms/common/audience/a/p;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/audience/a/d;->a(Z)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "ALL_CIRCLES_CHECKED"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final e(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/d;
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_ACCOUNT_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 120
    return-object p0
.end method

.method public final synthetic e(Ljava/util/List;)Lcom/google/android/gms/common/audience/a/p;
    .locals 0

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/audience/a/d;->c(Ljava/util/List;)Lcom/google/android/gms/common/audience/a/d;

    return-object p0
.end method

.method public final synthetic e(Z)Lcom/google/android/gms/common/audience/a/p;
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "SHOW_ALL_CONTACTS_CHECKBOX"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object p0
.end method

.method public final e()Z
    .locals 3

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "ALL_CONTACTS_CHECKED"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final f(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/d;
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_PLUS_PAGE_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 132
    return-object p0
.end method

.method public final synthetic f(Z)Lcom/google/android/gms/common/audience/a/p;
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "SHOW_ALL_CIRCLES_CHECKBOX"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object p0
.end method

.method public final f()Lcom/google/android/gms/common/people/data/AudienceMember;
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    invoke-static {v0}, Lcom/google/android/gms/common/audience/a/d;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    return-object v0
.end method

.method public final g(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/d;
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_CLIENT_APPLICATION_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 142
    return-object p0
.end method

.method public final synthetic g(Z)Lcom/google/android/gms/common/audience/a/p;
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "HAS_SHOW_CIRCLES"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object p0
.end method

.method public final g()Ljava/util/ArrayList;
    .locals 2

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_ADDED_AUDIENCE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public final h(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/d;
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_TITLE_TEXT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 208
    return-object p0
.end method

.method public final h()Ljava/util/ArrayList;
    .locals 2

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_REMOVED_AUDIENCE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/k;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/audience/a/d;->g(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    invoke-static {v0}, Lcom/google/android/gms/common/audience/a/d;->c(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/k;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 24
    const-string v0, "People qualified ID"

    invoke-static {p1, v0}, Lcom/google/android/gms/people/internal/at;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1, v1, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v2, "com.google.android.gms.common.acl.EXTRA_UPDATE_PERSON"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/internal/safeparcel/d;->a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;Landroid/content/Intent;Ljava/lang/String;)V

    return-object p0
.end method

.method public final synthetic k(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/k;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/audience/a/d;->h(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic l(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/k;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/audience/a/d;->f(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic m(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/k;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/audience/a/d;->e(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic n(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/p;
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/audience/a/d;->q(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic o(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/p;
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v1, "TITLE_LOGO"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public final synthetic p(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/p;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/audience/a/d;->e(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    return-object v0
.end method
