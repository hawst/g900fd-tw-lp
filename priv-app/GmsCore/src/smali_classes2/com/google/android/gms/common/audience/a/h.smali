.class public final Lcom/google/android/gms/common/audience/a/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/common/people/data/AudienceMember;

.field public final b:I

.field public final c:I

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILcom/google/android/gms/common/people/data/AudienceMember;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput p1, p0, Lcom/google/android/gms/common/audience/a/h;->b:I

    .line 81
    iput-object p2, p0, Lcom/google/android/gms/common/audience/a/h;->a:Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 82
    iput p3, p0, Lcom/google/android/gms/common/audience/a/h;->c:I

    .line 83
    iput-object p4, p0, Lcom/google/android/gms/common/audience/a/h;->d:Ljava/lang/String;

    .line 84
    invoke-direct {p0}, Lcom/google/android/gms/common/audience/a/h;->b()V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    const-string v0, "com.google.android.gms.plus.audience.EXTRA_CREATE_CIRCLE_STATUS_CODE"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/audience/a/h;->b:I

    .line 72
    const-string v0, "com.google.android.gms.plus.audience.EXTRA_CIRCLE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    iput-object v0, p0, Lcom/google/android/gms/common/audience/a/h;->a:Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 73
    const-string v0, "com.google.android.gms.plus.audience.EXTRA_ADD_PERSON_STATUS_CODE"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/audience/a/h;->c:I

    .line 74
    const-string v0, "com.google.android.gms.plus.audience.EXTRA_QUALIFIED_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/audience/a/h;->d:Ljava/lang/String;

    .line 75
    invoke-direct {p0}, Lcom/google/android/gms/common/audience/a/h;->b()V

    .line 76
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 88
    iget v0, p0, Lcom/google/android/gms/common/audience/a/h;->b:I

    const-string v1, "Invalid create circle status code."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ILjava/lang/Object;)I

    .line 89
    iget v0, p0, Lcom/google/android/gms/common/audience/a/h;->c:I

    const-string v1, "Invalid add person status code."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ILjava/lang/Object;)I

    .line 91
    iget v0, p0, Lcom/google/android/gms/common/audience/a/h;->b:I

    if-ne v0, v2, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/h;->a:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Must provide a circle with circle id."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/h;->a:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Must provide a circle with display name."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    :cond_0
    iget v0, p0, Lcom/google/android/gms/common/audience/a/h;->c:I

    if-ne v0, v2, :cond_1

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/h;->d:Ljava/lang/String;

    const-string v1, "Must provide qualified id."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 104
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 105
    const-string v1, "com.google.android.gms.plus.audience.EXTRA_CREATE_CIRCLE_STATUS_CODE"

    iget v2, p0, Lcom/google/android/gms/common/audience/a/h;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 106
    const-string v1, "com.google.android.gms.plus.audience.EXTRA_CIRCLE"

    iget-object v2, p0, Lcom/google/android/gms/common/audience/a/h;->a:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 107
    const-string v1, "com.google.android.gms.plus.audience.EXTRA_ADD_PERSON_STATUS_CODE"

    iget v2, p0, Lcom/google/android/gms/common/audience/a/h;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 108
    const-string v1, "com.google.android.gms.plus.audience.EXTRA_QUALIFIED_ID"

    iget-object v2, p0, Lcom/google/android/gms/common/audience/a/h;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 109
    return-object v0
.end method
