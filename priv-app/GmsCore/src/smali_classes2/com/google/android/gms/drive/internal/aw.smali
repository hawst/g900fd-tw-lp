.class final Lcom/google/android/gms/drive/internal/aw;
.super Lcom/google/android/gms/drive/internal/bb;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/am;

.field final synthetic b:I

.field final synthetic d:I

.field final synthetic e:Lcom/google/android/gms/drive/ad;

.field final synthetic f:Lcom/google/android/gms/drive/internal/av;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/internal/av;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/am;IILcom/google/android/gms/drive/ad;)V
    .locals 1

    .prologue
    .line 184
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/aw;->f:Lcom/google/android/gms/drive/internal/av;

    iput-object p3, p0, Lcom/google/android/gms/drive/internal/aw;->a:Lcom/google/android/gms/drive/am;

    iput p4, p0, Lcom/google/android/gms/drive/internal/aw;->b:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/drive/internal/aw;->d:I

    iput-object p6, p0, Lcom/google/android/gms/drive/internal/aw;->e:Lcom/google/android/gms/drive/ad;

    invoke-direct {p0, p2}, Lcom/google/android/gms/drive/internal/bb;-><init>(Lcom/google/android/gms/common/api/v;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Lcom/google/android/gms/common/api/h;)V
    .locals 6

    .prologue
    .line 184
    check-cast p1, Lcom/google/android/gms/drive/internal/aj;

    iget-object v0, p0, Lcom/google/android/gms/drive/internal/aw;->a:Lcom/google/android/gms/drive/am;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/am;->e()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/aj;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/gms/drive/internal/CreateFileRequest;

    iget-object v1, p0, Lcom/google/android/gms/drive/internal/aw;->f:Lcom/google/android/gms/drive/internal/av;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/av;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/internal/aw;->a:Lcom/google/android/gms/drive/am;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/am;->e()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/drive/internal/aw;->b:I

    iget v4, p0, Lcom/google/android/gms/drive/internal/aw;->d:I

    iget-object v5, p0, Lcom/google/android/gms/drive/internal/aw;->e:Lcom/google/android/gms/drive/ad;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/internal/CreateFileRequest;-><init>(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;IILcom/google/android/gms/drive/ad;)V

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/aj;->f()Lcom/google/android/gms/drive/internal/bx;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/drive/internal/ay;

    invoke-direct {v2, p0}, Lcom/google/android/gms/drive/internal/ay;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/drive/internal/bx;->a(Lcom/google/android/gms/drive/internal/CreateFileRequest;Lcom/google/android/gms/drive/internal/ca;)V

    return-void
.end method
