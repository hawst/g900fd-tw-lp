.class public abstract Lcom/google/android/gms/people/identity/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/android/gms/people/identity/p;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/identity/p;)V
    .locals 0

    .prologue
    .line 334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 335
    iput-object p1, p0, Lcom/google/android/gms/people/identity/k;->a:Lcom/google/android/gms/people/identity/p;

    .line 336
    return-void
.end method

.method public static a(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;I)Lcom/google/android/gms/people/identity/k;
    .locals 8

    .prologue
    .line 372
    move/from16 v0, p9

    invoke-static {p0, v0}, Lcom/google/android/gms/people/identity/k;->a(Lcom/google/android/gms/common/data/DataHolder;I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/people/identity/k;->a(Ljava/util/ArrayList;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/people/identity/p;

    .line 373
    if-eqz v2, :cond_0

    .line 374
    new-instance v1, Lcom/google/android/gms/people/identity/r;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v6, p8

    move/from16 v7, p9

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/people/identity/r;-><init>(Lcom/google/android/gms/people/identity/p;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;I)V

    .line 384
    :goto_0
    return-object v1

    .line 377
    :cond_0
    move/from16 v0, p9

    invoke-static {p4, v0}, Lcom/google/android/gms/people/identity/k;->a(Lcom/google/android/gms/common/data/DataHolder;I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/people/identity/k;->a(Ljava/util/ArrayList;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/people/identity/p;

    .line 378
    if-eqz v2, :cond_1

    .line 379
    new-instance v1, Lcom/google/android/gms/people/identity/q;

    move-object v3, p5

    move-object v4, p6

    move-object v5, p7

    move/from16 v6, p9

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/people/identity/q;-><init>(Lcom/google/android/gms/people/identity/p;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;I)V

    goto :goto_0

    .line 384
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/util/ArrayList;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 418
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 419
    :cond_0
    const/4 v0, 0x0

    .line 423
    :goto_0
    return-object v0

    .line 422
    :cond_1
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v0, :cond_2

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 423
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 422
    goto :goto_1
.end method

.method protected static a(Lcom/google/android/gms/common/data/DataHolder;I)Ljava/util/ArrayList;
    .locals 4

    .prologue
    .line 392
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 394
    if-eqz p0, :cond_2

    .line 395
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 397
    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v2

    .line 399
    const-string v3, "ordinal"

    invoke-virtual {p0, v3, v0, v2}, Lcom/google/android/gms/common/data/DataHolder;->b(Ljava/lang/String;II)I

    move-result v2

    if-ne p1, v2, :cond_1

    .line 401
    new-instance v2, Lcom/google/android/gms/people/identity/p;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/people/identity/p;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 405
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 402
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 410
    :cond_2
    return-object v1
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method protected final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 339
    iget-object v0, p0, Lcom/google/android/gms/people/identity/k;->a:Lcom/google/android/gms/people/identity/p;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/people/identity/p;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final b(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/gms/people/identity/k;->a:Lcom/google/android/gms/people/identity/p;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/people/identity/p;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public abstract c()Z
.end method

.method protected final c(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/android/gms/people/identity/k;->a:Lcom/google/android/gms/people/identity/p;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/people/identity/p;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method public abstract e()I
.end method

.method public abstract f()Ljava/lang/String;
.end method

.method public abstract g()Ljava/util/List;
.end method

.method public abstract h()Ljava/util/List;
.end method

.method public abstract i()Ljava/util/List;
.end method

.method public abstract j()Ljava/util/List;
.end method
