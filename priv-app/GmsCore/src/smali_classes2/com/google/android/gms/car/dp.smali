.class final Lcom/google/android/gms/car/dp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/do;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/do;)V
    .locals 0

    .prologue
    .line 570
    iput-object p1, p0, Lcom/google/android/gms/car/dp;->a:Lcom/google/android/gms/car/do;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4

    .prologue
    .line 573
    iget-object v0, p0, Lcom/google/android/gms/car/dp;->a:Lcom/google/android/gms/car/do;

    check-cast p2, Lcom/google/android/gms/car/nj;

    invoke-static {v0, p2}, Lcom/google/android/gms/car/do;->a(Lcom/google/android/gms/car/do;Lcom/google/android/gms/car/nj;)Lcom/google/android/gms/car/nj;

    .line 574
    iget-object v0, p0, Lcom/google/android/gms/car/dp;->a:Lcom/google/android/gms/car/do;

    invoke-static {v0}, Lcom/google/android/gms/car/do;->a(Lcom/google/android/gms/car/do;)Lcom/google/android/gms/car/nj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/nj;->a()Lcom/google/android/gms/car/InCallServiceImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/InCallServiceImpl;->getPhone()Landroid/telecom/Phone;

    move-result-object v1

    .line 575
    const-string v0, "CAR.TEL.PhoneAdapter"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 576
    const-string v0, "CAR.TEL.PhoneAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onServiceConnected InCallService with phone: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 578
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/dp;->a:Lcom/google/android/gms/car/do;

    invoke-static {v0}, Lcom/google/android/gms/car/do;->b(Lcom/google/android/gms/car/do;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 579
    iget-object v0, p0, Lcom/google/android/gms/car/dp;->a:Lcom/google/android/gms/car/do;

    invoke-static {v0}, Lcom/google/android/gms/car/do;->b(Lcom/google/android/gms/car/do;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/dq;

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/dq;->a(Landroid/telecom/Phone;)V

    .line 581
    :cond_1
    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 585
    const-string v0, "CAR.TEL.PhoneAdapter"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586
    const-string v0, "CAR.TEL.PhoneAdapter"

    const-string v1, "onServiceDisconnected from InCallService"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 588
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/dp;->a:Lcom/google/android/gms/car/do;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/car/do;->a(Lcom/google/android/gms/car/do;Lcom/google/android/gms/car/nj;)Lcom/google/android/gms/car/nj;

    .line 589
    return-void
.end method
