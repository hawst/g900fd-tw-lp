.class public final Lcom/google/android/gms/plus/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/api/j;

.field static final b:Lcom/google/android/gms/common/api/i;

.field public static final c:Lcom/google/android/gms/common/api/c;

.field public static final d:Lcom/google/android/gms/common/api/Scope;

.field public static final e:Lcom/google/android/gms/common/api/Scope;

.field public static final f:Lcom/google/android/gms/plus/d;

.field public static final g:Lcom/google/android/gms/plus/e;

.field public static final h:Lcom/google/android/gms/plus/a;

.field public static final i:Lcom/google/android/gms/plus/c;

.field public static final j:Lcom/google/android/gms/plus/b;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 109
    new-instance v0, Lcom/google/android/gms/common/api/j;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/f;->a:Lcom/google/android/gms/common/api/j;

    .line 116
    new-instance v0, Lcom/google/android/gms/plus/g;

    invoke-direct {v0}, Lcom/google/android/gms/plus/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/f;->b:Lcom/google/android/gms/common/api/i;

    .line 157
    new-instance v0, Lcom/google/android/gms/common/api/c;

    sget-object v1, Lcom/google/android/gms/plus/f;->b:Lcom/google/android/gms/common/api/i;

    sget-object v2, Lcom/google/android/gms/plus/f;->a:Lcom/google/android/gms/common/api/j;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/google/android/gms/common/api/Scope;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/c;-><init>(Lcom/google/android/gms/common/api/i;Lcom/google/android/gms/common/api/j;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/plus/f;->c:Lcom/google/android/gms/common/api/c;

    .line 172
    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string v1, "https://www.googleapis.com/auth/plus.login"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/plus/f;->d:Lcom/google/android/gms/common/api/Scope;

    .line 177
    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string v1, "https://www.googleapis.com/auth/plus.me"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/plus/f;->e:Lcom/google/android/gms/common/api/Scope;

    .line 180
    new-instance v0, Lcom/google/android/gms/plus/internal/a/f;

    invoke-direct {v0}, Lcom/google/android/gms/plus/internal/a/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/f;->f:Lcom/google/android/gms/plus/d;

    .line 183
    new-instance v0, Lcom/google/android/gms/plus/internal/a/g;

    invoke-direct {v0}, Lcom/google/android/gms/plus/internal/a/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/f;->g:Lcom/google/android/gms/plus/e;

    .line 186
    new-instance v0, Lcom/google/android/gms/plus/internal/a/a;

    invoke-direct {v0}, Lcom/google/android/gms/plus/internal/a/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/f;->h:Lcom/google/android/gms/plus/a;

    .line 193
    new-instance v0, Lcom/google/android/gms/plus/internal/a/e;

    invoke-direct {v0}, Lcom/google/android/gms/plus/internal/a/e;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/f;->i:Lcom/google/android/gms/plus/c;

    .line 199
    new-instance v0, Lcom/google/android/gms/plus/internal/a/b;

    invoke-direct {v0}, Lcom/google/android/gms/plus/internal/a/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/f;->j:Lcom/google/android/gms/plus/b;

    return-void
.end method
