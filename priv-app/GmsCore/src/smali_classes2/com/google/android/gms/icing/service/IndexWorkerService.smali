.class public Lcom/google/android/gms/icing/service/IndexWorkerService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/icing/service/p;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 284
    return-void
.end method

.method private static a(JJ)J
    .locals 10

    .prologue
    .line 356
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 357
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 360
    const/16 v0, 0xb

    const-wide/32 v6, 0x36ee80

    div-long v6, p0, v6

    const-wide/16 v8, 0x18

    rem-long/2addr v6, v8

    long-to-int v1, v6

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 361
    const/16 v0, 0xc

    const-wide/32 v6, 0xea60

    div-long v6, p0, v6

    const-wide/16 v8, 0x3c

    rem-long/2addr v6, v8

    long-to-int v1, v6

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 362
    const/16 v0, 0xd

    const-wide/16 v6, 0x3e8

    div-long v6, p0, v6

    const-wide/16 v8, 0x3c

    rem-long/2addr v6, v8

    long-to-int v1, v6

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 363
    const/16 v0, 0xe

    const-wide/16 v6, 0x3e8

    rem-long v6, p0, v6

    long-to-int v1, v6

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 364
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    .line 368
    :goto_0
    add-long v6, v4, p2

    cmp-long v3, v0, v6

    if-gez v3, :cond_0

    .line 369
    const/4 v0, 0x7

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->add(II)V

    .line 370
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    goto :goto_0

    .line 372
    :cond_0
    const-string v3, "%s: First maintenance at %s"

    const-string v4, "main"

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 373
    return-wide v0
.end method

.method static synthetic a(Landroid/content/Context;IJ)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 71
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "icing"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "index-corpora"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.icing.INDEX_CONTENT_PROVIDER"

    const-class v3, Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-direct {v1, v2, v0, p0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "delay"

    invoke-virtual {v1, v0, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const/4 v0, 0x0

    invoke-static {p0, v0, v1, p1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 256
    new-instance v0, Landroid/content/Intent;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-direct {v0, p1, v1, p0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v3, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final a()Lcom/google/android/gms/icing/impl/u;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/gms/icing/service/IndexWorkerService;->a:Lcom/google/android/gms/icing/service/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/p;->c()Lcom/google/android/gms/icing/impl/u;

    move-result-object v0

    return-object v0
.end method

.method final a(Z)V
    .locals 4

    .prologue
    .line 319
    new-instance v1, Landroid/content/ComponentName;

    const-class v0, Lcom/google/android/gms/icing/service/PowerConnectedReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 321
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 324
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/service/IndexWorkerService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v0, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 325
    return-void

    .line 321
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method final b()V
    .locals 6

    .prologue
    .line 312
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/service/IndexWorkerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 313
    const/4 v1, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x36ee80

    add-long/2addr v2, v4

    const-string v4, "com.google.android.gms.icing.INDEX_ONETIME_MAINTENANCE"

    invoke-direct {p0, v4}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 316
    return-void
.end method

.method protected final b(Z)V
    .locals 18

    .prologue
    .line 377
    sget-object v2, Lcom/google/android/gms/icing/a/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 378
    sget-object v2, Lcom/google/android/gms/icing/a/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    .line 379
    sget-object v2, Lcom/google/android/gms/icing/a/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    .line 382
    const-string v2, "alarm"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/service/IndexWorkerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AlarmManager;

    .line 387
    if-eqz p1, :cond_0

    const-wide/32 v4, 0x927c0

    .line 388
    :goto_0
    const/4 v3, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/icing/service/IndexWorkerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "android_id"

    invoke-static {v8, v9}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_3

    const-string v9, "MD5"

    invoke-static {v9}, Lcom/google/android/gms/icing/impl/bp;->b(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v9

    if-eqz v9, :cond_3

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v3

    move-object v10, v3

    :goto_1
    if-eqz v10, :cond_2

    array-length v3, v10

    const/16 v8, 0x8

    if-lt v3, v8, :cond_2

    const/4 v3, 0x7

    aget-byte v3, v10, v3

    and-int/lit8 v3, v3, 0x7f

    int-to-long v8, v3

    const/4 v3, 0x6

    :goto_2
    if-ltz v3, :cond_1

    const/16 v11, 0x8

    shl-long/2addr v8, v11

    aget-byte v11, v10, v3

    and-int/lit16 v11, v11, 0xff

    int-to-long v0, v11

    move-wide/from16 v16, v0

    add-long v8, v8, v16

    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    .line 387
    :cond_0
    const-wide/16 v4, 0x0

    goto :goto_0

    .line 388
    :cond_1
    rem-long/2addr v8, v14

    :goto_3
    add-long/2addr v8, v12

    .line 390
    const/4 v3, 0x0

    invoke-static {v8, v9, v4, v5}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a(JJ)J

    move-result-wide v4

    const-string v8, "com.google.android.gms.icing.INDEX_RECURRING_MAINTENANCE"

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v8

    invoke-virtual/range {v2 .. v8}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 394
    return-void

    .line 388
    :cond_2
    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    long-to-int v8, v14

    invoke-virtual {v3, v8}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    int-to-long v8, v3

    goto :goto_3

    :cond_3
    move-object v10, v3

    goto :goto_1
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/gms/icing/service/IndexWorkerService;->a:Lcom/google/android/gms/icing/service/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/p;->c()Lcom/google/android/gms/icing/impl/u;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/icing/impl/u;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 253
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 236
    const-string v0, "%s: Binding with intent %s"

    const-string v1, "main"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 237
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 110
    const-string v0, "%s: IndexWorkerService onCreate"

    const-string v1, "main"

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 111
    const-string v0, "main"

    invoke-static {v0, p0}, Lcom/google/android/gms/icing/service/p;->a(Ljava/lang/String;Landroid/app/Service;)Lcom/google/android/gms/icing/service/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/service/IndexWorkerService;->a:Lcom/google/android/gms/icing/service/p;

    .line 112
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 113
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 118
    const-string v0, "%s: IndexWorkerService onDestroy"

    const-string v1, "main"

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/icing/service/IndexWorkerService;->a:Lcom/google/android/gms/icing/service/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/p;->a()V

    .line 120
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 121
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4

    .prologue
    .line 125
    const-string v0, "%s: IndexWorkerService: onStartCommand with %s"

    const-string v1, "main"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 127
    if-eqz p1, :cond_0

    const-string v0, "com.google.android.gms.icing.START_STICKY"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    const/4 v0, 0x1

    .line 231
    :goto_0
    return v0

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/service/IndexWorkerService;->a:Lcom/google/android/gms/icing/service/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/p;->b()Lcom/google/android/gms/icing/b/a;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/icing/service/d;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/icing/service/d;-><init>(Lcom/google/android/gms/icing/service/IndexWorkerService;Landroid/content/Intent;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    .line 231
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 242
    const-string v0, "%s: Unbind"

    const-string v1, "main"

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 243
    const/4 v0, 0x0

    return v0
.end method
