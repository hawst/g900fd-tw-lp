.class final Lcom/google/android/gms/people/internal/ap;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/people/p;


# instance fields
.field private final a:Lcom/google/android/gms/common/api/Status;

.field private final b:Landroid/os/ParcelFileDescriptor;

.field private final c:Z

.field private final d:I

.field private final e:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;ZII)V
    .locals 0

    .prologue
    .line 890
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 891
    iput-object p1, p0, Lcom/google/android/gms/people/internal/ap;->a:Lcom/google/android/gms/common/api/Status;

    .line 892
    iput-object p2, p0, Lcom/google/android/gms/people/internal/ap;->b:Landroid/os/ParcelFileDescriptor;

    .line 893
    iput-boolean p3, p0, Lcom/google/android/gms/people/internal/ap;->c:Z

    .line 894
    iput p4, p0, Lcom/google/android/gms/people/internal/ap;->d:I

    .line 895
    iput p5, p0, Lcom/google/android/gms/people/internal/ap;->e:I

    .line 896
    return-void
.end method


# virtual methods
.method public final C_()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 900
    iget-object v0, p0, Lcom/google/android/gms/people/internal/ap;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final c()Landroid/os/ParcelFileDescriptor;
    .locals 1

    .prologue
    .line 905
    iget-object v0, p0, Lcom/google/android/gms/people/internal/ap;->b:Landroid/os/ParcelFileDescriptor;

    return-object v0
.end method

.method public final w_()V
    .locals 1

    .prologue
    .line 925
    iget-object v0, p0, Lcom/google/android/gms/people/internal/ap;->b:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_0

    .line 926
    iget-object v0, p0, Lcom/google/android/gms/people/internal/ap;->b:Landroid/os/ParcelFileDescriptor;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    .line 928
    :cond_0
    return-void
.end method
