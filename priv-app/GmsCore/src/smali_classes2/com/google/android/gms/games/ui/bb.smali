.class final Lcom/google/android/gms/games/ui/bb;
.super Lcom/google/android/gms/games/ui/ay;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/aj;

.field private final g:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/aj;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 744
    iput-object p1, p0, Lcom/google/android/gms/games/ui/bb;->a:Lcom/google/android/gms/games/ui/aj;

    .line 745
    invoke-static {p2}, Lcom/google/android/gms/games/ui/aj;->b(I)I

    move-result v0

    invoke-direct {p0, p1, v0, v1, v1}, Lcom/google/android/gms/games/ui/ay;-><init>(Lcom/google/android/gms/games/ui/aj;IIZ)V

    .line 746
    iput p2, p0, Lcom/google/android/gms/games/ui/bb;->g:I

    .line 747
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 751
    sget v0, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 752
    sget v1, Lcom/google/android/gms/j;->sl:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 755
    iget v2, p0, Lcom/google/android/gms/games/ui/bb;->g:I

    packed-switch v2, :pswitch_data_0

    .line 769
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown channel type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/bb;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 757
    :pswitch_0
    sget v2, Lcom/google/android/gms/p;->ip:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 758
    sget v0, Lcom/google/android/gms/p;->iq:I

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 772
    :goto_0
    sget v0, Lcom/google/android/gms/j;->ui:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 773
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 775
    sget v0, Lcom/google/android/gms/j;->cE:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 776
    iget v1, p0, Lcom/google/android/gms/games/ui/bb;->g:I

    invoke-static {v1}, Lcom/google/android/gms/games/internal/b/e;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 777
    iget-object v2, p0, Lcom/google/android/gms/games/ui/bb;->a:Lcom/google/android/gms/games/ui/aj;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/aj;->c(Lcom/google/android/gms/games/ui/aj;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 778
    return-void

    .line 761
    :pswitch_1
    sget v2, Lcom/google/android/gms/p;->ix:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 762
    sget v0, Lcom/google/android/gms/p;->iy:I

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 765
    :pswitch_2
    sget v2, Lcom/google/android/gms/p;->iB:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 766
    sget v0, Lcom/google/android/gms/p;->iC:I

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 755
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
