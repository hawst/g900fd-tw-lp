.class final Lcom/google/android/gms/car/ca;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/gms/car/br;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/br;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lcom/google/android/gms/car/ca;->b:Lcom/google/android/gms/car/br;

    iput-object p2, p0, Lcom/google/android/gms/car/ca;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 253
    iget-object v0, p0, Lcom/google/android/gms/car/ca;->b:Lcom/google/android/gms/car/br;

    invoke-static {v0}, Lcom/google/android/gms/car/br;->d(Lcom/google/android/gms/car/br;)Lcom/google/android/gms/car/a/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/ca;->a:Ljava/lang/String;

    const-string v2, "CAR.BT"

    invoke-static {v2, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "CAR.BT"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "setAuthenticationData: auth data="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v2, v0, Lcom/google/android/gms/car/a/k;->k:I

    if-eqz v2, :cond_2

    const-string v0, "CAR.BT"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "CAR.BT"

    const-string v1, "setAuthenticationData: This object wasn\'t initialized successfully"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    :cond_1
    :goto_0
    return-void

    .line 253
    :cond_2
    iput-object v1, v0, Lcom/google/android/gms/car/a/k;->o:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/car/a/k;->g()V

    goto :goto_0
.end method
