.class public final Lcom/google/android/gms/icing/ao;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/android/gms/icing/ao;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4998
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 4999
    iput v0, p0, Lcom/google/android/gms/icing/ao;->a:I

    iput v0, p0, Lcom/google/android/gms/icing/ao;->b:I

    iput v0, p0, Lcom/google/android/gms/icing/ao;->c:I

    iput v0, p0, Lcom/google/android/gms/icing/ao;->d:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/ao;->cachedSize:I

    .line 5000
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/ao;
    .locals 2

    .prologue
    .line 4975
    sget-object v0, Lcom/google/android/gms/icing/ao;->e:[Lcom/google/android/gms/icing/ao;

    if-nez v0, :cond_1

    .line 4976
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 4978
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/ao;->e:[Lcom/google/android/gms/icing/ao;

    if-nez v0, :cond_0

    .line 4979
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/ao;

    sput-object v0, Lcom/google/android/gms/icing/ao;->e:[Lcom/google/android/gms/icing/ao;

    .line 4981
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4983
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/ao;->e:[Lcom/google/android/gms/icing/ao;

    return-object v0

    .line 4981
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 5065
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 5066
    iget v1, p0, Lcom/google/android/gms/icing/ao;->a:I

    if-eqz v1, :cond_0

    .line 5067
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/icing/ao;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5070
    :cond_0
    iget v1, p0, Lcom/google/android/gms/icing/ao;->b:I

    if-eqz v1, :cond_1

    .line 5071
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/icing/ao;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5074
    :cond_1
    iget v1, p0, Lcom/google/android/gms/icing/ao;->c:I

    if-eqz v1, :cond_2

    .line 5075
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/icing/ao;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5078
    :cond_2
    iget v1, p0, Lcom/google/android/gms/icing/ao;->d:I

    if-eqz v1, :cond_3

    .line 5079
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/icing/ao;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5082
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5013
    if-ne p1, p0, :cond_1

    .line 5032
    :cond_0
    :goto_0
    return v0

    .line 5016
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/ao;

    if-nez v2, :cond_2

    move v0, v1

    .line 5017
    goto :goto_0

    .line 5019
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/ao;

    .line 5020
    iget v2, p0, Lcom/google/android/gms/icing/ao;->a:I

    iget v3, p1, Lcom/google/android/gms/icing/ao;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 5021
    goto :goto_0

    .line 5023
    :cond_3
    iget v2, p0, Lcom/google/android/gms/icing/ao;->b:I

    iget v3, p1, Lcom/google/android/gms/icing/ao;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 5024
    goto :goto_0

    .line 5026
    :cond_4
    iget v2, p0, Lcom/google/android/gms/icing/ao;->c:I

    iget v3, p1, Lcom/google/android/gms/icing/ao;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 5027
    goto :goto_0

    .line 5029
    :cond_5
    iget v2, p0, Lcom/google/android/gms/icing/ao;->d:I

    iget v3, p1, Lcom/google/android/gms/icing/ao;->d:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 5030
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 5037
    iget v0, p0, Lcom/google/android/gms/icing/ao;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 5039
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/ao;->b:I

    add-int/2addr v0, v1

    .line 5040
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/ao;->c:I

    add-int/2addr v0, v1

    .line 5041
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/ao;->d:I

    add-int/2addr v0, v1

    .line 5042
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 4969
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/ao;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/ao;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/ao;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/icing/ao;->d:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 5048
    iget v0, p0, Lcom/google/android/gms/icing/ao;->a:I

    if-eqz v0, :cond_0

    .line 5049
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/icing/ao;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 5051
    :cond_0
    iget v0, p0, Lcom/google/android/gms/icing/ao;->b:I

    if-eqz v0, :cond_1

    .line 5052
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/icing/ao;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 5054
    :cond_1
    iget v0, p0, Lcom/google/android/gms/icing/ao;->c:I

    if-eqz v0, :cond_2

    .line 5055
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/icing/ao;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 5057
    :cond_2
    iget v0, p0, Lcom/google/android/gms/icing/ao;->d:I

    if-eqz v0, :cond_3

    .line 5058
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/icing/ao;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5060
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5061
    return-void
.end method
