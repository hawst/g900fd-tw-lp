.class final Lcom/google/android/gms/car/a/r;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/a/k;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/car/a/k;)V
    .locals 0

    .prologue
    .line 752
    iput-object p1, p0, Lcom/google/android/gms/car/a/r;->a:Lcom/google/android/gms/car/a/k;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/car/a/k;B)V
    .locals 0

    .prologue
    .line 752
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/a/r;-><init>(Lcom/google/android/gms/car/a/k;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x3

    .line 755
    const-string v0, "CAR.BT"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 756
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HeadsetConnectionStateChangeReceiver#onReceive. intent="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 759
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/a/r;->a:Lcom/google/android/gms/car/a/k;

    iget v0, v0, Lcom/google/android/gms/car/a/k;->k:I

    if-eqz v0, :cond_2

    .line 760
    const-string v0, "CAR.BT"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 761
    const-string v0, "CAR.BT"

    const-string v1, "BluetoothUtil not initialized or being cleaned up"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 812
    :cond_1
    :goto_0
    return-void

    .line 765
    :cond_2
    const-string v0, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 766
    const-string v0, "CAR.BT"

    const-string v1, "HeadsetConnectionStateChangeReceiver: Wrong intent. This shouldn\'t happen"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 771
    :cond_3
    const-string v0, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 772
    iget-object v1, p0, Lcom/google/android/gms/car/a/r;->a:Lcom/google/android/gms/car/a/k;

    iget-object v1, v1, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 773
    const-string v1, "CAR.BT"

    invoke-static {v1, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 774
    const-string v1, "CAR.BT"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HFP connection state changed, but not our device: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 780
    :cond_4
    const-string v0, "android.bluetooth.profile.extra.STATE"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 781
    const-string v0, "CAR.BT"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 782
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 783
    if-nez v1, :cond_8

    .line 784
    const-string v0, "DISCONNECTED"

    .line 792
    :cond_5
    :goto_1
    const-string v2, "CAR.BT"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "HeadsetConnectionStateChangeReceiver#onReceive. new state="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 794
    :cond_6
    if-ne v1, v6, :cond_c

    .line 795
    const-string v0, "CAR.BT"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 796
    const-string v0, "CAR.BT"

    const-string v1, "Connected to HFP"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 799
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/car/a/r;->a:Lcom/google/android/gms/car/a/k;

    iget-object v0, v0, Lcom/google/android/gms/car/a/k;->c:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 800
    iget-object v0, p0, Lcom/google/android/gms/car/a/r;->a:Lcom/google/android/gms/car/a/k;

    iget-object v1, p0, Lcom/google/android/gms/car/a/r;->a:Lcom/google/android/gms/car/a/k;

    iget-object v1, v1, Lcom/google/android/gms/car/a/k;->b:Landroid/bluetooth/BluetoothAdapter;

    iget-object v2, p0, Lcom/google/android/gms/car/a/r;->a:Lcom/google/android/gms/car/a/k;

    iget-object v2, v2, Lcom/google/android/gms/car/a/k;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    .line 804
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/car/a/r;->a:Lcom/google/android/gms/car/a/k;

    iget-object v0, v0, Lcom/google/android/gms/car/a/k;->l:Lcom/google/android/gms/car/a/h;

    iget-object v1, p0, Lcom/google/android/gms/car/a/r;->a:Lcom/google/android/gms/car/a/k;

    iget-object v1, v1, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    iput-object v1, v0, Lcom/google/android/gms/car/a/h;->a:Landroid/bluetooth/BluetoothDevice;

    .line 805
    iget-object v0, p0, Lcom/google/android/gms/car/a/r;->a:Lcom/google/android/gms/car/a/k;

    iget-object v0, v0, Lcom/google/android/gms/car/a/k;->e:Lcom/google/android/gms/car/a/p;

    invoke-interface {v0}, Lcom/google/android/gms/car/a/p;->e()V

    goto/16 :goto_0

    .line 785
    :cond_8
    const/4 v2, 0x1

    if-ne v1, v2, :cond_9

    .line 786
    const-string v0, "CONNECTING"

    goto :goto_1

    .line 787
    :cond_9
    if-ne v1, v6, :cond_a

    .line 788
    const-string v0, "CONNECTED"

    goto :goto_1

    .line 789
    :cond_a
    if-ne v1, v5, :cond_5

    .line 790
    const-string v0, "DISCONNECTING"

    goto :goto_1

    .line 802
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/car/a/r;->a:Lcom/google/android/gms/car/a/k;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    goto :goto_2

    .line 806
    :cond_c
    if-nez v1, :cond_1

    .line 807
    const-string v0, "CAR.BT"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 808
    const-string v0, "CAR.BT"

    const-string v1, "Disconnected HFP"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 810
    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/car/a/r;->a:Lcom/google/android/gms/car/a/k;

    iget-object v0, v0, Lcom/google/android/gms/car/a/k;->e:Lcom/google/android/gms/car/a/p;

    invoke-interface {v0}, Lcom/google/android/gms/car/a/p;->f()V

    goto/16 :goto_0
.end method
