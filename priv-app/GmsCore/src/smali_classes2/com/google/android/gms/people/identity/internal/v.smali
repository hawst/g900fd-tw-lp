.class public final Lcom/google/android/gms/people/identity/internal/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/people/identity/a;


# static fields
.field private static final a:Lcom/google/android/gms/people/identity/g;

.field private static final b:Lcom/google/android/gms/people/identity/t;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lcom/google/android/gms/people/identity/internal/q;

    invoke-direct {v0}, Lcom/google/android/gms/people/identity/internal/q;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/identity/internal/v;->a:Lcom/google/android/gms/people/identity/g;

    .line 60
    new-instance v0, Lcom/google/android/gms/people/identity/internal/t;

    invoke-direct {v0}, Lcom/google/android/gms/people/identity/internal/t;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/identity/internal/v;->b:Lcom/google/android/gms/people/identity/t;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 515
    return-void
.end method

.method static a(Lcom/google/android/gms/people/identity/internal/c;Landroid/content/Context;Ljava/util/Map;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 496
    new-instance v0, Lcom/google/android/gms/people/identity/internal/b;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/people/identity/internal/b;-><init>(Lcom/google/android/gms/people/identity/internal/c;Landroid/content/Context;Ljava/util/Map;Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Lcom/google/android/gms/people/identity/internal/b;->a([Ljava/lang/String;)V

    .line 497
    return-void
.end method


# virtual methods
.method public final varargs a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/people/identity/e;Lcom/google/android/gms/people/identity/g;[Ljava/lang/String;)Lcom/google/android/gms/common/api/am;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 68
    array-length v3, p4

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, p4, v2

    .line 69
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 68
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 69
    goto :goto_1

    .line 72
    :cond_1
    new-instance v0, Lcom/google/android/gms/people/identity/internal/w;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/identity/internal/w;-><init>(Lcom/google/android/gms/people/identity/internal/v;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/people/identity/e;Lcom/google/android/gms/people/identity/g;[Ljava/lang/String;)V

    .line 75
    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method
