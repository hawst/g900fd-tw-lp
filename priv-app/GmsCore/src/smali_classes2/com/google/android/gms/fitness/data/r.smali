.class public final Lcom/google/android/gms/fitness/data/r;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:J

.field b:J

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:I

.field public g:Lcom/google/android/gms/fitness/data/Application;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 203
    iput-wide v0, p0, Lcom/google/android/gms/fitness/data/r;->a:J

    .line 204
    iput-wide v0, p0, Lcom/google/android/gms/fitness/data/r;->b:J

    .line 205
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/fitness/data/r;->c:Ljava/lang/String;

    .line 208
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/fitness/data/r;->f:I

    .line 212
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/fitness/data/Session;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 391
    iget-wide v4, p0, Lcom/google/android/gms/fitness/data/r;->a:J

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "Start time should be specified."

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 392
    iget-wide v4, p0, Lcom/google/android/gms/fitness/data/r;->b:J

    cmp-long v0, v4, v6

    if-eqz v0, :cond_0

    iget-wide v4, p0, Lcom/google/android/gms/fitness/data/r;->b:J

    iget-wide v6, p0, Lcom/google/android/gms/fitness/data/r;->a:J

    cmp-long v0, v4, v6

    if-lez v0, :cond_3

    :cond_0
    :goto_1
    const-string v0, "End time should be later than start time."

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 394
    iget-object v0, p0, Lcom/google/android/gms/fitness/data/r;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 395
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/fitness/data/r;->c:Ljava/lang/String;

    if-nez v0, :cond_4

    const-string v0, ""

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v4, p0, Lcom/google/android/gms/fitness/data/r;->a:J

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/data/r;->d:Ljava/lang/String;

    .line 397
    :cond_1
    new-instance v0, Lcom/google/android/gms/fitness/data/Session;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/fitness/data/Session;-><init>(Lcom/google/android/gms/fitness/data/r;B)V

    return-object v0

    :cond_2
    move v0, v2

    .line 391
    goto :goto_0

    :cond_3
    move v1, v2

    .line 392
    goto :goto_1

    .line 395
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/fitness/data/r;->c:Ljava/lang/String;

    goto :goto_2
.end method

.method public final a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/data/r;
    .locals 3

    .prologue
    .line 248
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "End time should be positive."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 249
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/fitness/data/r;->b:J

    .line 250
    return-object p0

    .line 248
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/Session;)Lcom/google/android/gms/fitness/data/r;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 344
    iget-object v0, p0, Lcom/google/android/gms/fitness/data/r;->d:Ljava/lang/String;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/gms/fitness/data/r;->d:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/gms/fitness/data/Session;->e(Lcom/google/android/gms/fitness/data/Session;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    move v0, v1

    .line 345
    :goto_0
    iget-wide v4, p0, Lcom/google/android/gms/fitness/data/r;->a:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    if-nez v0, :cond_0

    iget-wide v4, p0, Lcom/google/android/gms/fitness/data/r;->a:J

    invoke-static {p1}, Lcom/google/android/gms/fitness/data/Session;->b(Lcom/google/android/gms/fitness/data/Session;)J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_e

    :cond_0
    move v3, v1

    :goto_1
    const-string v4, "Session start times differ: %d vs %d"

    new-array v5, v8, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/google/android/gms/fitness/data/r;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {p1}, Lcom/google/android/gms/fitness/data/Session;->b(Lcom/google/android/gms/fitness/data/Session;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 350
    iget-object v3, p0, Lcom/google/android/gms/fitness/data/r;->d:Ljava/lang/String;

    if-eqz v3, :cond_1

    invoke-static {p1}, Lcom/google/android/gms/fitness/data/Session;->e(Lcom/google/android/gms/fitness/data/Session;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    if-eqz v0, :cond_f

    :cond_1
    move v3, v1

    :goto_2
    const-string v4, "Identifiers differ: %s vs %s"

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/gms/fitness/data/r;->d:Ljava/lang/String;

    aput-object v6, v5, v2

    invoke-static {p1}, Lcom/google/android/gms/fitness/data/Session;->e(Lcom/google/android/gms/fitness/data/Session;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 354
    iget-object v3, p0, Lcom/google/android/gms/fitness/data/r;->g:Lcom/google/android/gms/fitness/data/Application;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/fitness/data/r;->g:Lcom/google/android/gms/fitness/data/Application;

    invoke-static {p1}, Lcom/google/android/gms/fitness/data/Session;->h(Lcom/google/android/gms/fitness/data/Session;)Lcom/google/android/gms/fitness/data/Application;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/fitness/data/Application;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    :cond_2
    move v3, v1

    :goto_3
    const-string v4, "Applications differ: %s vs %s"

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/gms/fitness/data/r;->g:Lcom/google/android/gms/fitness/data/Application;

    aput-object v6, v5, v2

    invoke-static {p1}, Lcom/google/android/gms/fitness/data/Session;->h(Lcom/google/android/gms/fitness/data/Session;)Lcom/google/android/gms/fitness/data/Application;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 358
    invoke-static {p1}, Lcom/google/android/gms/fitness/data/Session;->b(Lcom/google/android/gms/fitness/data/Session;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/fitness/data/r;->a:J

    .line 359
    invoke-static {p1}, Lcom/google/android/gms/fitness/data/Session;->c(Lcom/google/android/gms/fitness/data/Session;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/fitness/data/r;->b:J

    .line 361
    invoke-static {p1}, Lcom/google/android/gms/fitness/data/Session;->d(Lcom/google/android/gms/fitness/data/Session;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    if-eqz v0, :cond_4

    .line 362
    :cond_3
    invoke-static {p1}, Lcom/google/android/gms/fitness/data/Session;->d(Lcom/google/android/gms/fitness/data/Session;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/fitness/data/r;->c:Ljava/lang/String;

    .line 364
    :cond_4
    invoke-static {p1}, Lcom/google/android/gms/fitness/data/Session;->e(Lcom/google/android/gms/fitness/data/Session;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    if-eqz v0, :cond_6

    .line 365
    :cond_5
    invoke-static {p1}, Lcom/google/android/gms/fitness/data/Session;->e(Lcom/google/android/gms/fitness/data/Session;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/fitness/data/r;->d:Ljava/lang/String;

    .line 367
    :cond_6
    invoke-static {p1}, Lcom/google/android/gms/fitness/data/Session;->f(Lcom/google/android/gms/fitness/data/Session;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_7

    if-eqz v0, :cond_8

    .line 368
    :cond_7
    invoke-static {p1}, Lcom/google/android/gms/fitness/data/Session;->f(Lcom/google/android/gms/fitness/data/Session;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/fitness/data/r;->e:Ljava/lang/String;

    .line 370
    :cond_8
    invoke-static {p1}, Lcom/google/android/gms/fitness/data/Session;->g(Lcom/google/android/gms/fitness/data/Session;)I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_9

    if-eqz v0, :cond_a

    .line 371
    :cond_9
    invoke-static {p1}, Lcom/google/android/gms/fitness/data/Session;->g(Lcom/google/android/gms/fitness/data/Session;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/fitness/data/r;->f:I

    .line 373
    :cond_a
    invoke-static {p1}, Lcom/google/android/gms/fitness/data/Session;->h(Lcom/google/android/gms/fitness/data/Session;)Lcom/google/android/gms/fitness/data/Application;

    move-result-object v1

    if-nez v1, :cond_b

    if-eqz v0, :cond_c

    .line 374
    :cond_b
    invoke-static {p1}, Lcom/google/android/gms/fitness/data/Session;->h(Lcom/google/android/gms/fitness/data/Session;)Lcom/google/android/gms/fitness/data/Application;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/data/r;->g:Lcom/google/android/gms/fitness/data/Application;

    .line 376
    :cond_c
    return-object p0

    :cond_d
    move v0, v2

    .line 344
    goto/16 :goto_0

    :cond_e
    move v3, v2

    .line 345
    goto/16 :goto_1

    :cond_f
    move v3, v2

    .line 350
    goto/16 :goto_2

    :cond_10
    move v3, v2

    .line 354
    goto :goto_3
.end method
