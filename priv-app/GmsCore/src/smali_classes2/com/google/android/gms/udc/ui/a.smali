.class final Lcom/google/android/gms/udc/ui/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/av;


# instance fields
.field final synthetic a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)V
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lcom/google/android/gms/udc/ui/a;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;B)V
    .locals 0

    .prologue
    .line 225
    invoke-direct {p0, p1}, Lcom/google/android/gms/udc/ui/a;-><init>(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 4

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/a;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    invoke-static {v0}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->e(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 230
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/a;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    invoke-static {v0}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->f(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)Lcom/google/android/gms/udc/util/j;

    move-result-object v0

    const v1, 0x1020002

    new-instance v2, Lcom/google/android/gms/udc/ui/u;

    invoke-direct {v2}, Lcom/google/android/gms/udc/ui/u;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/gms/udc/ui/u;->b()Lcom/google/android/gms/udc/ui/j;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/udc/util/j;->a(ILandroid/support/v4/app/Fragment;)V

    .line 232
    new-instance v0, Lcom/google/android/gms/udc/ui/c;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/a;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/a;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    invoke-static {v2}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->a(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)Lcom/google/android/gms/common/api/v;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/udc/ui/a;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    invoke-static {v3}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->b(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/udc/ui/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 0

    .prologue
    .line 265
    return-void
.end method

.method public final synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 225
    check-cast p2, Lcom/google/android/gms/lockbox/f;

    invoke-interface {p2}, Lcom/google/android/gms/lockbox/f;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/udc/ui/a;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    invoke-static {v0}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->d(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)Lcom/google/android/gms/analytics/bv;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/analytics/bi;

    invoke-direct {v2}, Lcom/google/android/gms/analytics/bi;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/gms/analytics/bi;->a()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/analytics/bv;->a(Ljava/util/Map;)V

    iget-object v0, p0, Lcom/google/android/gms/udc/ui/a;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    invoke-static {v0}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->c(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/udc/ui/a;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    invoke-static {v2}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->g(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)Lcom/google/android/gms/common/widget/SwitchBar;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/a;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    invoke-static {v2}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->g(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)Lcom/google/android/gms/common/widget/SwitchBar;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/gms/common/widget/SwitchBar;->setEnabled(Z)V

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/a;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    invoke-static {v2}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->g(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)Lcom/google/android/gms/common/widget/SwitchBar;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/widget/SwitchBar;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/udc/ui/a;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    invoke-static {v0}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->g(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)Lcom/google/android/gms/common/widget/SwitchBar;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/a;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/widget/SwitchBar;->a(Lcom/google/android/gms/common/widget/h;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/a;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    invoke-static {v0}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->e(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/udc/ui/a;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    invoke-static {v0}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->f(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)Lcom/google/android/gms/udc/util/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/udc/util/j;->a()V

    :goto_1
    return-void

    :pswitch_0
    invoke-interface {p2}, Lcom/google/android/gms/lockbox/f;->b()Z

    move-result v0

    goto :goto_0

    :pswitch_1
    invoke-interface {p2}, Lcom/google/android/gms/lockbox/f;->c()Z

    move-result v0

    goto :goto_0

    :cond_1
    invoke-interface {p2}, Lcom/google/android/gms/lockbox/f;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    const-string v1, "DeviceUsage"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error loading data:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/a;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    sget v2, Lcom/google/android/gms/p;->yR:I

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->a(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;Lcom/google/android/gms/common/api/Status;I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
