.class public final Lcom/google/android/gms/common/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lcom/google/android/gms/common/a/d;

.field public static b:Lcom/google/android/gms/common/a/d;

.field public static c:Lcom/google/android/gms/common/a/d;

.field public static d:Lcom/google/android/gms/common/a/d;

.field public static final e:Lcom/google/android/gms/common/a/d;

.field public static f:Lcom/google/android/gms/common/a/d;

.field public static g:Lcom/google/android/gms/common/a/d;

.field public static h:Lcom/google/android/gms/common/a/d;

.field public static i:Lcom/google/android/gms/common/a/d;

.field public static j:Lcom/google/android/gms/common/a/d;

.field public static k:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 19
    const-string v0, "gms:common:allow_pii_logging"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/a/b;->a:Lcom/google/android/gms/common/a/d;

    .line 22
    const-string v0, "enable_dumpsys"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/a/b;->b:Lcom/google/android/gms/common/a/d;

    .line 25
    const-string v0, "gms:common:collect_gms_stats"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/a/b;->c:Lcom/google/android/gms/common/a/d;

    .line 56
    const-string v0, "gms:common:ignored_packages_for_logging"

    const-string v1, "com.google.android.gms"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/a/b;->d:Lcom/google/android/gms/common/a/d;

    .line 60
    const-string v0, "gms:app:google_settings_help_url"

    const-string v1, "https://support.google.com/?p=google_settings"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/a/b;->e:Lcom/google/android/gms/common/a/d;

    .line 64
    const-string v0, "gms:common:show_people_settings"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/a/b;->f:Lcom/google/android/gms/common/a/d;

    .line 67
    const-string v0, "gms:common:show_download_settings"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/a/b;->g:Lcom/google/android/gms/common/a/d;

    .line 70
    const-string v0, "gms:common:enable_auth_proxy_version"

    const-wide/32 v2, 0x4dd1e0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/a/b;->h:Lcom/google/android/gms/common/a/d;

    .line 75
    const-string v0, "gms:common:lso_server_url"

    const-string v1, "https://www.googleapis.com"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/a/b;->i:Lcom/google/android/gms/common/a/d;

    .line 79
    const-string v0, "gms:common:lso_server_api_path"

    const-string v1, "/oauth2/v3/"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/a/b;->j:Lcom/google/android/gms/common/a/d;

    .line 83
    const-string v0, "gms:common:lso_backend_override"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/a/b;->k:Lcom/google/android/gms/common/a/d;

    return-void
.end method
