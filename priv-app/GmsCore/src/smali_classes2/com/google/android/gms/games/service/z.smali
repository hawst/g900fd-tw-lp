.class final Lcom/google/android/gms/games/service/z;
.super Lcom/google/android/gms/games/service/s;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lcom/google/android/gms/games/internal/dx;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 251
    invoke-direct {p0}, Lcom/google/android/gms/games/service/s;-><init>()V

    .line 252
    iput-object p1, p0, Lcom/google/android/gms/games/service/z;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 253
    iput-object p2, p0, Lcom/google/android/gms/games/service/z;->b:Lcom/google/android/gms/games/internal/dx;

    .line 254
    iput-object p3, p0, Lcom/google/android/gms/games/service/z;->c:Ljava/lang/String;

    .line 255
    iput-object p4, p0, Lcom/google/android/gms/games/service/z;->d:Ljava/lang/String;

    .line 256
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 272
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/z;->b:Lcom/google/android/gms/games/internal/dx;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dx;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 276
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)I
    .locals 3

    .prologue
    .line 261
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/z;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lcom/google/android/gms/games/service/z;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/service/z;->c:Ljava/lang/String;

    invoke-virtual {p2, p1, v0, v1, v2}, Lcom/google/android/gms/games/a/t;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 264
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x1

    goto :goto_0
.end method
