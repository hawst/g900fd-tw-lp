.class final Lcom/google/android/gms/common/api/ag;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/api/z;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/z;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 382
    iput-object p1, p0, Lcom/google/android/gms/common/api/ag;->a:Lcom/google/android/gms/common/api/z;

    .line 383
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 384
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 388
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 394
    const-string v0, "GoogleApiClientImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown message id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    :goto_0
    return-void

    .line 390
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/ag;->a:Lcom/google/android/gms/common/api/z;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/z;->h()V

    goto :goto_0

    .line 393
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/gms/common/api/ag;->a:Lcom/google/android/gms/common/api/z;

    iget-object v0, v1, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-boolean v0, v1, Lcom/google/android/gms/common/api/z;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/z;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-object v0, v1, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, v1, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 388
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
