.class public final Lcom/google/android/gms/car/senderprotocol/av;
.super Lcom/google/android/gms/car/senderprotocol/az;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/car/senderprotocol/aw;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/fx;Lcom/google/android/gms/car/gx;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/senderprotocol/az;-><init>(Lcom/google/android/gms/car/gw;Lcom/google/android/gms/car/gx;)V

    .line 52
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/av;->a:Lcom/google/android/gms/car/senderprotocol/aw;

    .line 53
    return-void
.end method


# virtual methods
.method protected final a(ILjava/nio/ByteBuffer;)V
    .locals 4

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/av;->a:Lcom/google/android/gms/car/senderprotocol/aw;

    if-nez v0, :cond_1

    .line 102
    const-string v0, "CAR.NOTIFICATION"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    const-string v0, "CAR.NOTIFICATION"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring message of type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 108
    :cond_1
    const v0, 0x8003

    if-ne p1, v0, :cond_2

    .line 109
    new-instance v0, Lcom/google/android/c/b/ac;

    invoke-direct {v0}, Lcom/google/android/c/b/ac;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/av;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/c/b/ac;

    .line 111
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/av;->a:Lcom/google/android/gms/car/senderprotocol/aw;

    invoke-virtual {v0}, Lcom/google/android/c/b/ac;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/c/b/ac;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/c/b/ac;->c()[B

    invoke-interface {v1, v2, v3}, Lcom/google/android/gms/car/senderprotocol/aw;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 113
    :cond_2
    const v0, 0x8004

    if-ne p1, v0, :cond_3

    .line 114
    new-instance v0, Lcom/google/android/c/b/ab;

    invoke-direct {v0}, Lcom/google/android/c/b/ab;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/av;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/c/b/ab;

    .line 116
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/av;->a:Lcom/google/android/gms/car/senderprotocol/aw;

    invoke-virtual {v0}, Lcom/google/android/c/b/ab;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/c/b/ab;->b()Z

    move-result v0

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/car/senderprotocol/aw;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 118
    :cond_3
    const-string v0, "CAR.NOTIFICATION"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid message type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 59
    new-instance v0, Lcom/google/android/c/b/ae;

    invoke-direct {v0}, Lcom/google/android/c/b/ae;-><init>()V

    .line 60
    const v1, 0x8001

    invoke-static {v0}, Lcom/google/android/c/b/ae;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/av;->a(I[B)V

    .line 61
    return-void
.end method
