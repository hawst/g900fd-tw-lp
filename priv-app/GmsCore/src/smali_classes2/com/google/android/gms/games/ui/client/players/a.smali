.class public final Lcom/google/android/gms/games/ui/client/players/a;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private final a:Lcom/google/android/gms/games/ui/p;

.field private final b:Landroid/view/LayoutInflater;

.field private c:Ljava/util/ArrayList;

.field private d:Z

.field private e:I

.field private f:Landroid/view/View;

.field private g:Landroid/widget/TextView;

.field private h:Ljava/lang/String;

.field private i:I

.field private j:Lcom/google/android/gms/games/ui/client/players/f;

.field private k:Lcom/google/android/gms/games/ui/client/players/d;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/p;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 257
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 81
    iput-boolean v5, p0, Lcom/google/android/gms/games/ui/client/players/a;->d:Z

    .line 121
    new-instance v0, Lcom/google/android/gms/games/ui/client/players/d;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/client/players/d;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->k:Lcom/google/android/gms/games/ui/client/players/d;

    .line 258
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/players/a;->a:Lcom/google/android/gms/games/ui/p;

    .line 259
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->a:Lcom/google/android/gms/games/ui/p;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/p;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->b:Landroid/view/LayoutInflater;

    .line 262
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->e:I

    .line 263
    sget v1, Lcom/google/android/gms/k;->r:I

    .line 265
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->a:Lcom/google/android/gms/games/ui/p;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/p;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 266
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->e:I
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 272
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    .line 274
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->d:Z

    if-eqz v0, :cond_0

    .line 276
    new-instance v0, Lcom/google/android/gms/games/ui/client/players/d;

    invoke-direct {v0, v5}, Lcom/google/android/gms/games/ui/client/players/d;-><init>(I)V

    .line 278
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->a:Lcom/google/android/gms/games/ui/p;

    sget v2, Lcom/google/android/gms/p;->kh:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/p;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/ui/client/players/d;->d:Ljava/lang/String;

    .line 281
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 283
    :cond_0
    return-void

    .line 267
    :catch_0
    move-exception v0

    .line 268
    const-string v2, "ChipsGridAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to find numColumns resource: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 269
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Unable to find numColumns resource: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/client/players/a;)Landroid/view/View;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->f:Landroid/view/View;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/games/ui/client/players/d;)V
    .locals 3

    .prologue
    .line 343
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->d:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->i:I

    .line 346
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->k:Lcom/google/android/gms/games/ui/client/players/d;

    if-eqz v1, :cond_0

    .line 347
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/a;->k:Lcom/google/android/gms/games/ui/client/players/d;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 351
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 354
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->k:Lcom/google/android/gms/games/ui/client/players/d;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gt v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->e:I

    rem-int/2addr v0, v1

    if-eqz v0, :cond_1

    .line 356
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->k:Lcom/google/android/gms/games/ui/client/players/d;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 358
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 359
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->g:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 368
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/a;->notifyDataSetChanged()V

    .line 370
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->j:Lcom/google/android/gms/games/ui/client/players/f;

    if-eqz v0, :cond_3

    .line 371
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->j:Lcom/google/android/gms/games/ui/client/players/f;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/client/players/f;->a()V

    .line 373
    :cond_3
    return-void

    .line 343
    :cond_4
    iget v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->i:I

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/client/players/a;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method private b(Lcom/google/android/gms/games/ui/client/players/d;)V
    .locals 3

    .prologue
    .line 406
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 407
    if-nez v0, :cond_0

    .line 408
    const-string v0, "ChipsGridAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "removeChipInternal: chip "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->f:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 420
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->k:Lcom/google/android/gms/games/ui/client/players/d;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 422
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->e:I

    rem-int/2addr v0, v1

    if-eqz v0, :cond_1

    .line 423
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->k:Lcom/google/android/gms/games/ui/client/players/d;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 427
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/a;->notifyDataSetChanged()V

    .line 429
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->j:Lcom/google/android/gms/games/ui/client/players/f;

    if-eqz v0, :cond_2

    .line 430
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->j:Lcom/google/android/gms/games/ui/client/players/f;

    .line 432
    :cond_2
    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/client/players/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/client/players/a;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->g:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 322
    new-instance v0, Lcom/google/android/gms/games/ui/client/players/d;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/client/players/d;-><init>(I)V

    .line 324
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->a:Lcom/google/android/gms/games/ui/p;

    sget v2, Lcom/google/android/gms/p;->la:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/p;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/ui/client/players/d;->d:Ljava/lang/String;

    .line 327
    sget v1, Lcom/google/android/gms/h;->X:I

    iput v1, v0, Lcom/google/android/gms/games/ui/client/players/d;->f:I

    .line 329
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/players/a;->a(Lcom/google/android/gms/games/ui/client/players/d;)V

    .line 330
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 294
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->d:Z

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 297
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/client/players/d;

    .line 298
    iget v3, v0, Lcom/google/android/gms/games/ui/client/players/d;->a:I

    if-nez v3, :cond_2

    :goto_1
    invoke-static {v1}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 300
    iput-object p1, v0, Lcom/google/android/gms/games/ui/client/players/d;->e:Landroid/net/Uri;

    .line 301
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/a;->notifyDataSetChanged()V

    .line 303
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 296
    goto :goto_0

    :cond_2
    move v1, v2

    .line 298
    goto :goto_1
.end method

.method final a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 865
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->f:Landroid/view/View;

    if-nez v0, :cond_1

    .line 866
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/players/a;->f:Landroid/view/View;

    .line 867
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->f:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->gf:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->g:Landroid/widget/TextView;

    .line 868
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->g:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 869
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 870
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->k:Lcom/google/android/gms/games/ui/client/players/d;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 872
    :cond_0
    iput p2, p0, Lcom/google/android/gms/games/ui/client/players/a;->i:I

    .line 873
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->a:Lcom/google/android/gms/games/ui/p;

    sget v1, Lcom/google/android/gms/p;->le:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/p;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->h:Ljava/lang/String;

    .line 875
    :cond_1
    return-void
.end method

.method final a(Lcom/google/android/gms/games/ui/client/players/f;)V
    .locals 0

    .prologue
    .line 853
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/players/a;->j:Lcom/google/android/gms/games/ui/client/players/f;

    .line 854
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 384
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 388
    new-instance v0, Lcom/google/android/gms/games/ui/client/players/d;

    invoke-direct {v0, p1, v1, v1}, Lcom/google/android/gms/games/ui/client/players/d;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 389
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/players/a;->b(Lcom/google/android/gms/games/ui/client/players/d;)V

    .line 390
    return-void

    .line 384
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 310
    new-instance v0, Lcom/google/android/gms/games/ui/client/players/d;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/games/ui/client/players/d;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 311
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 312
    const-string v1, "ChipsGridAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "addPlayer: chip "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is already present"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :goto_0
    return-void

    .line 315
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/players/a;->a(Lcom/google/android/gms/games/ui/client/players/d;)V

    goto :goto_0
.end method

.method final a(Z)V
    .locals 2

    .prologue
    .line 878
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 879
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->g:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 881
    :cond_0
    return-void

    .line 879
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 537
    const/4 v0, 0x0

    return v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 401
    new-instance v0, Lcom/google/android/gms/games/ui/client/players/d;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/client/players/d;-><init>(I)V

    .line 402
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/players/a;->b(Lcom/google/android/gms/games/ui/client/players/d;)V

    .line 403
    return-void
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 472
    new-instance v0, Lcom/google/android/gms/games/ui/client/players/d;

    invoke-direct {v0, p1, v1, v1}, Lcom/google/android/gms/games/ui/client/players/d;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 473
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 439
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->d:Z

    if-eqz v0, :cond_4

    .line 440
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 443
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/client/players/d;

    .line 444
    iget v3, v0, Lcom/google/android/gms/games/ui/client/players/d;->a:I

    if-nez v3, :cond_3

    :goto_1
    invoke-static {v1}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 445
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 446
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 451
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 453
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->e:I

    rem-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 454
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->k:Lcom/google/android/gms/games/ui/client/players/d;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/a;->notifyDataSetChanged()V

    .line 460
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->j:Lcom/google/android/gms/games/ui/client/players/f;

    if-eqz v0, :cond_1

    .line 461
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->j:Lcom/google/android/gms/games/ui/client/players/f;

    .line 463
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 440
    goto :goto_0

    :cond_3
    move v1, v2

    .line 444
    goto :goto_1

    .line 448
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_2
.end method

.method public final d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 843
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 844
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/client/players/d;

    iput-boolean v2, v0, Lcom/google/android/gms/games/ui/client/players/d;->b:Z

    .line 843
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 846
    :cond_0
    return-void
.end method

.method public final getCount()I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 487
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 488
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->d:Z

    if-eqz v0, :cond_0

    .line 489
    if-lez v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 494
    :cond_0
    if-lez v3, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    add-int/lit8 v4, v3, -0x1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/client/players/d;

    iget v0, v0, Lcom/google/android/gms/games/ui/client/players/d;->a:I

    const/4 v4, 0x3

    if-ne v0, v4, :cond_3

    .line 495
    add-int/lit8 v0, v3, -0x1

    .line 497
    :goto_1
    iget v3, p0, Lcom/google/android/gms/games/ui/client/players/a;->e:I

    add-int/2addr v0, v3

    iget v3, p0, Lcom/google/android/gms/games/ui/client/players/a;->e:I

    div-int/2addr v0, v3

    .line 498
    if-lez v0, :cond_2

    :goto_2
    invoke-static {v1}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 499
    return v0

    :cond_1
    move v0, v2

    .line 489
    goto :goto_0

    :cond_2
    move v1, v2

    .line 498
    goto :goto_2

    :cond_3
    move v0, v3

    goto :goto_1
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 505
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 510
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 520
    const/4 v0, 0x0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v7, 0x4

    const/4 v3, 0x0

    .line 543
    if-nez p2, :cond_2

    .line 544
    invoke-static {p3}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->b:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->aD:I

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v5, Lcom/google/android/gms/games/ui/client/players/h;

    invoke-direct {v5, v3}, Lcom/google/android/gms/games/ui/client/players/h;-><init>(B)V

    sget v0, Lcom/google/android/gms/j;->rX:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, v5, Lcom/google/android/gms/games/ui/client/players/h;->b:Landroid/widget/FrameLayout;

    move v1, v3

    :goto_0
    if-ge v1, v7, :cond_1

    new-instance v6, Lcom/google/android/gms/games/ui/client/players/e;

    invoke-direct {v6, v3}, Lcom/google/android/gms/games/ui/client/players/e;-><init>(B)V

    sget-object v0, Lcom/google/android/gms/games/ui/client/players/h;->c:[I

    aget v0, v0, v1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iget v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->e:I

    if-le v0, v1, :cond_0

    iput-object v8, v6, Lcom/google/android/gms/games/ui/client/players/e;->a:Landroid/view/View;

    sget v0, Lcom/google/android/gms/j;->qR:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, v6, Lcom/google/android/gms/games/ui/client/players/e;->b:Landroid/widget/FrameLayout;

    sget v0, Lcom/google/android/gms/j;->mD:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/gms/games/ui/client/players/e;->c:Landroid/view/View;

    sget v0, Lcom/google/android/gms/j;->ri:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/gms/games/ui/client/players/e;->d:Landroid/view/View;

    sget v0, Lcom/google/android/gms/j;->mm:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v6, Lcom/google/android/gms/games/ui/client/players/e;->e:Landroid/widget/TextView;

    sget v0, Lcom/google/android/gms/j;->mn:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v6, Lcom/google/android/gms/games/ui/client/players/e;->f:Landroid/widget/TextView;

    sget v0, Lcom/google/android/gms/j;->ka:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, v6, Lcom/google/android/gms/games/ui/client/players/e;->g:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iget-object v0, v6, Lcom/google/android/gms/games/ui/client/players/e;->g:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->c()V

    sget v0, Lcom/google/android/gms/j;->uk:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/gms/games/ui/client/players/e;->h:Landroid/view/View;

    iget-object v0, v6, Lcom/google/android/gms/games/ui/client/players/e;->a:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, v6, Lcom/google/android/gms/games/ui/client/players/e;->a:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, v6, Lcom/google/android/gms/games/ui/client/players/e;->h:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v8, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v5, Lcom/google/android/gms/games/ui/client/players/h;->a:[Lcom/google/android/gms/games/ui/client/players/e;

    aput-object v6, v0, v1

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v5, Lcom/google/android/gms/games/ui/client/players/h;->a:[Lcom/google/android/gms/games/ui/client/players/e;

    aput-object v4, v0, v1

    goto :goto_1

    :cond_1
    iget v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->e:I

    if-gt v0, v7, :cond_3

    move v0, v2

    :goto_2
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    invoke-virtual {p2, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 548
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/client/players/h;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/h;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v7}, Landroid/widget/FrameLayout;->setVisibility(I)V

    move v8, v3

    move v1, v3

    :goto_3
    if-ge v8, v7, :cond_d

    iget v5, p0, Lcom/google/android/gms/games/ui/client/players/a;->e:I

    if-ge v8, v5, :cond_d

    iget v5, p0, Lcom/google/android/gms/games/ui/client/players/a;->e:I

    mul-int/2addr v5, p1

    add-int/2addr v5, v8

    iget-object v6, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v5, v6, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/ui/client/players/d;

    move v5, v2

    :goto_4
    iget-object v6, v0, Lcom/google/android/gms/games/ui/client/players/h;->a:[Lcom/google/android/gms/games/ui/client/players/e;

    aget-object v9, v6, v8

    invoke-static {v9}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    if-eqz v1, :cond_c

    iget-object v6, v9, Lcom/google/android/gms/games/ui/client/players/e;->e:Landroid/widget/TextView;

    iget-object v10, v1, Lcom/google/android/gms/games/ui/client/players/d;->d:Ljava/lang/String;

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, v9, Lcom/google/android/gms/games/ui/client/players/e;->f:Landroid/widget/TextView;

    iget-object v10, v1, Lcom/google/android/gms/games/ui/client/players/d;->d:Ljava/lang/String;

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v6, v1, Lcom/google/android/gms/games/ui/client/players/d;->f:I

    if-eqz v6, :cond_5

    iget-object v6, v9, Lcom/google/android/gms/games/ui/client/players/e;->g:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iget v10, v1, Lcom/google/android/gms/games/ui/client/players/d;->f:I

    invoke-virtual {v6, v4, v10}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    :goto_5
    iget-object v6, v9, Lcom/google/android/gms/games/ui/client/players/e;->a:Landroid/view/View;

    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v10, v9, Lcom/google/android/gms/games/ui/client/players/e;->c:Landroid/view/View;

    iget-boolean v6, v1, Lcom/google/android/gms/games/ui/client/players/d;->b:Z

    if-eqz v6, :cond_6

    move v6, v7

    :goto_6
    invoke-virtual {v10, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v10, v9, Lcom/google/android/gms/games/ui/client/players/e;->d:Landroid/view/View;

    iget-boolean v6, v1, Lcom/google/android/gms/games/ui/client/players/d;->b:Z

    if-eqz v6, :cond_7

    move v6, v3

    :goto_7
    invoke-virtual {v10, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v6, v9, Lcom/google/android/gms/games/ui/client/players/e;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v6, v7}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v6, v9, Lcom/google/android/gms/games/ui/client/players/e;->a:Landroid/view/View;

    invoke-virtual {v6, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget v6, v1, Lcom/google/android/gms/games/ui/client/players/d;->a:I

    const/4 v10, 0x2

    if-ne v6, v10, :cond_8

    iget-object v6, v9, Lcom/google/android/gms/games/ui/client/players/e;->h:Landroid/view/View;

    iget-object v10, v1, Lcom/google/android/gms/games/ui/client/players/d;->c:Ljava/lang/String;

    invoke-virtual {v6, v10}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v6, v9, Lcom/google/android/gms/games/ui/client/players/e;->a:Landroid/view/View;

    invoke-virtual {v6, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_8
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    move v1, v5

    goto :goto_3

    :cond_3
    move v0, v3

    .line 544
    goto/16 :goto_2

    :cond_4
    move v5, v1

    move-object v1, v4

    .line 548
    goto :goto_4

    :cond_5
    iget-object v6, v9, Lcom/google/android/gms/games/ui/client/players/e;->g:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iget-object v10, v1, Lcom/google/android/gms/games/ui/client/players/d;->e:Landroid/net/Uri;

    sget v11, Lcom/google/android/gms/h;->ag:I

    invoke-virtual {v6, v10, v11}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    goto :goto_5

    :cond_6
    move v6, v3

    goto :goto_6

    :cond_7
    move v6, v7

    goto :goto_7

    :cond_8
    iget v6, v1, Lcom/google/android/gms/games/ui/client/players/d;->a:I

    if-nez v6, :cond_9

    iget-object v1, v9, Lcom/google/android/gms/games/ui/client/players/e;->a:Landroid/view/View;

    const-string v6, "current_player_chip"

    invoke-virtual {v1, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, v9, Lcom/google/android/gms/games/ui/client/players/e;->h:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_8

    :cond_9
    iget v6, v1, Lcom/google/android/gms/games/ui/client/players/d;->a:I

    const/4 v10, 0x3

    if-ne v6, v10, :cond_a

    iget-object v1, v9, Lcom/google/android/gms/games/ui/client/players/e;->c:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v9, Lcom/google/android/gms/games/ui/client/players/e;->d:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v9, Lcom/google/android/gms/games/ui/client/players/e;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v1, v9, Lcom/google/android/gms/games/ui/client/players/e;->a:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, v9, Lcom/google/android/gms/games/ui/client/players/e;->h:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, v9, Lcom/google/android/gms/games/ui/client/players/e;->b:Landroid/widget/FrameLayout;

    new-instance v6, Lcom/google/android/gms/games/ui/client/players/c;

    invoke-direct {v6, p0, v9}, Lcom/google/android/gms/games/ui/client/players/c;-><init>(Lcom/google/android/gms/games/ui/client/players/a;Lcom/google/android/gms/games/ui/client/players/e;)V

    invoke-virtual {v1, v6}, Landroid/widget/FrameLayout;->post(Ljava/lang/Runnable;)Z

    goto :goto_8

    :cond_a
    iget v6, v1, Lcom/google/android/gms/games/ui/client/players/d;->a:I

    if-ne v6, v2, :cond_b

    move v6, v2

    :goto_9
    invoke-static {v6}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    iget-object v6, v9, Lcom/google/android/gms/games/ui/client/players/e;->a:Landroid/view/View;

    invoke-virtual {v6, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, v9, Lcom/google/android/gms/games/ui/client/players/e;->h:Landroid/view/View;

    const-string v6, "auto_match_chip_x"

    invoke-virtual {v1, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_8

    :cond_b
    move v6, v3

    goto :goto_9

    :cond_c
    iget-object v1, v9, Lcom/google/android/gms/games/ui/client/players/e;->a:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v9, Lcom/google/android/gms/games/ui/client/players/e;->a:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, v9, Lcom/google/android/gms/games/ui/client/players/e;->h:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_8

    :cond_d
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/a;->f:Landroid/view/View;

    if-eqz v2, :cond_e

    if-nez v1, :cond_e

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/h;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/h;->b:Landroid/widget/FrameLayout;

    new-instance v2, Lcom/google/android/gms/games/ui/client/players/b;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/games/ui/client/players/b;-><init>(Lcom/google/android/gms/games/ui/client/players/a;Lcom/google/android/gms/games/ui/client/players/h;)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->post(Ljava/lang/Runnable;)Z

    .line 549
    :cond_e
    return-object p2
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 525
    const/4 v0, 0x1

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 515
    const/4 v0, 0x0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 532
    const/4 v0, 0x0

    return v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 736
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 739
    if-nez v0, :cond_0

    .line 742
    const-string v0, "ChipsGridAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onClick(): null tag for view "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    :goto_0
    return-void

    .line 745
    :cond_0
    const-string v1, "current_player_chip"

    if-ne v0, v1, :cond_1

    .line 747
    const-string v0, "ChipsGridAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onClick(): unexpected click on current player chip: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 756
    :cond_1
    const-string v1, "auto_match_chip_x"

    if-ne v0, v1, :cond_3

    .line 766
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->a:Lcom/google/android/gms/games/ui/p;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/client/players/g;

    if-eqz v0, :cond_2

    .line 767
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/a;->a:Lcom/google/android/gms/games/ui/p;

    check-cast v0, Lcom/google/android/gms/games/ui/client/players/g;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/client/players/g;->I_()V

    .line 773
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/a;->d()V

    .line 812
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/a;->notifyDataSetChanged()V

    goto :goto_0

    .line 769
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Parent "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->a:Lcom/google/android/gms/games/ui/p;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isn\'t an OnRemovePlayerListener"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->b(Ljava/lang/Object;)V

    goto :goto_1

    .line 775
    :cond_3
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 777
    check-cast v0, Ljava/lang/String;

    .line 785
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->a:Lcom/google/android/gms/games/ui/p;

    instance-of v1, v1, Lcom/google/android/gms/games/ui/client/players/g;

    if-eqz v1, :cond_4

    .line 786
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->a:Lcom/google/android/gms/games/ui/p;

    check-cast v1, Lcom/google/android/gms/games/ui/client/players/g;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/client/players/g;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 788
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Parent "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/a;->a:Lcom/google/android/gms/games/ui/p;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isn\'t an OnRemovePlayerListener"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->b(Ljava/lang/Object;)V

    goto :goto_2

    .line 791
    :cond_5
    instance-of v1, v0, Lcom/google/android/gms/games/ui/client/players/d;

    if-eqz v1, :cond_7

    .line 793
    check-cast v0, Lcom/google/android/gms/games/ui/client/players/d;

    .line 799
    iget-boolean v1, v0, Lcom/google/android/gms/games/ui/client/players/d;->b:Z

    if-eqz v1, :cond_6

    .line 800
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/games/ui/client/players/d;->b:Z

    goto :goto_2

    .line 804
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/a;->d()V

    .line 805
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/games/ui/client/players/d;->b:Z

    goto :goto_2

    .line 808
    :cond_7
    const-string v1, "ChipsGridAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onClick(): unexpected tag: view "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", tag "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 817
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 819
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "current_player_chip"

    if-ne v0, v1, :cond_0

    .line 824
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/a;->d()V

    .line 825
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/a;->notifyDataSetChanged()V

    .line 829
    const/4 v0, 0x1

    .line 832
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
