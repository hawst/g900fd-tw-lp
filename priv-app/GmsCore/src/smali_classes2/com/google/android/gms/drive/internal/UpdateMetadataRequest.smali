.class public Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field final b:Lcom/google/android/gms/drive/DriveId;

.field final c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/drive/internal/dj;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/dj;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput p1, p0, Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;->a:I

    .line 55
    iput-object p2, p0, Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;->b:Lcom/google/android/gms/drive/DriveId;

    .line 56
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;->c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    .line 57
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/DriveId;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;->b:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;->c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 41
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/drive/internal/dj;->a(Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;Landroid/os/Parcel;I)V

    .line 42
    return-void
.end method
