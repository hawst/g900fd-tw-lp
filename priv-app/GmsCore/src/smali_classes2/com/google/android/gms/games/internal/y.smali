.class final Lcom/google/android/gms/games/internal/y;
.super Lcom/google/android/gms/games/internal/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/aj;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/aj;)V
    .locals 0

    .prologue
    .line 753
    invoke-direct {p0}, Lcom/google/android/gms/games/internal/a;-><init>()V

    .line 754
    iput-object p1, p0, Lcom/google/android/gms/games/internal/y;->a:Lcom/google/android/gms/common/api/aj;

    .line 755
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 778
    iget-object v0, p0, Lcom/google/android/gms/games/internal/y;->a:Lcom/google/android/gms/common/api/aj;

    new-instance v1, Lcom/google/android/gms/games/internal/aa;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/aa;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 779
    return-void
.end method

.method public final l(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 3

    .prologue
    .line 760
    new-instance v1, Lcom/google/android/gms/games/multiplayer/a;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/multiplayer/a;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 761
    const/4 v0, 0x0

    .line 763
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/a;->c()I

    move-result v2

    if-lez v2, :cond_0

    .line 764
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/multiplayer/a;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 767
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/a;->w_()V

    .line 771
    if-eqz v0, :cond_1

    .line 772
    iget-object v1, p0, Lcom/google/android/gms/games/internal/y;->a:Lcom/google/android/gms/common/api/aj;

    new-instance v2, Lcom/google/android/gms/games/internal/z;

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/internal/z;-><init>(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 774
    :cond_1
    return-void

    .line 767
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/a;->w_()V

    throw v0
.end method
