.class final Lcom/google/android/gms/drive/internal/bl;
.super Lcom/google/android/gms/drive/internal/c;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 0

    .prologue
    .line 181
    invoke-direct {p0}, Lcom/google/android/gms/drive/internal/c;-><init>()V

    .line 182
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/bl;->a:Lcom/google/android/gms/common/api/m;

    .line 183
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/bl;->a:Lcom/google/android/gms/common/api/m;

    new-instance v1, Lcom/google/android/gms/drive/internal/bm;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/google/android/gms/drive/internal/bm;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/drive/aj;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 196
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/OnMetadataResponse;)V
    .locals 5

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/bl;->a:Lcom/google/android/gms/common/api/m;

    new-instance v1, Lcom/google/android/gms/drive/internal/bm;

    sget-object v2, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    new-instance v3, Lcom/google/android/gms/drive/internal/l;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/OnMetadataResponse;->a()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/gms/drive/internal/l;-><init>(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/drive/internal/bm;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/drive/aj;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 190
    return-void
.end method
