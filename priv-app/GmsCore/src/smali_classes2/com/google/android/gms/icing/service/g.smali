.class final Lcom/google/android/gms/icing/service/g;
.super Lcom/google/android/gms/appdatasearch/a/f;
.source "SourceFile"


# static fields
.field private static final e:Ljava/lang/Object;


# instance fields
.field final a:Lcom/google/android/gms/icing/impl/e/h;

.field final b:Lcom/google/android/gms/icing/impl/e/a;

.field final c:Lcom/google/android/gms/appdatasearch/c;

.field final d:Ljava/lang/Runnable;

.field private final f:Landroid/content/Context;

.field private final g:Ljava/lang/String;

.field private final h:Z

.field private final i:Landroid/app/ActivityManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 179
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/icing/service/g;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/icing/impl/e/h;Lcom/google/android/gms/icing/impl/e/a;Z)V
    .locals 2

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/google/android/gms/appdatasearch/a/f;-><init>()V

    .line 189
    new-instance v0, Lcom/google/android/gms/icing/service/h;

    invoke-direct {v0, p0}, Lcom/google/android/gms/icing/service/h;-><init>(Lcom/google/android/gms/icing/service/g;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/service/g;->d:Ljava/lang/Runnable;

    .line 202
    iput-object p1, p0, Lcom/google/android/gms/icing/service/g;->f:Landroid/content/Context;

    .line 203
    iput-object p2, p0, Lcom/google/android/gms/icing/service/g;->g:Ljava/lang/String;

    .line 204
    iput-object p3, p0, Lcom/google/android/gms/icing/service/g;->a:Lcom/google/android/gms/icing/impl/e/h;

    .line 205
    iput-object p4, p0, Lcom/google/android/gms/icing/service/g;->b:Lcom/google/android/gms/icing/impl/e/a;

    .line 206
    iput-boolean p5, p0, Lcom/google/android/gms/icing/service/g;->h:Z

    .line 207
    new-instance v0, Lcom/google/android/gms/appdatasearch/c;

    iget-object v1, p0, Lcom/google/android/gms/icing/service/g;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/appdatasearch/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/service/g;->c:Lcom/google/android/gms/appdatasearch/c;

    .line 208
    iget-object v0, p0, Lcom/google/android/gms/icing/service/g;->f:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/google/android/gms/icing/service/g;->i:Landroid/app/ActivityManager;

    .line 210
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/icing/service/g;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/gms/icing/service/g;->f:Landroid/content/Context;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/util/List;[Lcom/google/android/gms/appdatasearch/UsageInfo;)Ljava/util/List;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 219
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 220
    :goto_0
    array-length v2, p3

    if-ge v0, v2, :cond_2

    .line 222
    aget-object v2, p3, v0

    if-eqz v2, :cond_1

    .line 226
    aget-object v2, p3, v0

    invoke-virtual {v2}, Lcom/google/android/gms/appdatasearch/UsageInfo;->d()Lcom/google/android/gms/appdatasearch/DocumentContents;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 228
    aget-object v2, p3, v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v2, p1, v6, v7, p2}, Lcom/google/android/gms/icing/impl/e/h;->a(Lcom/google/android/gms/appdatasearch/UsageInfo;Ljava/lang/String;JLjava/util/List;)Lcom/google/android/gms/icing/bf;

    move-result-object v6

    .line 231
    const/4 v4, 0x0

    .line 233
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/icing/service/g;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/16 v3, 0x40

    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 235
    iget v3, v2, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    :try_start_1
    invoke-static {v2}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/pm/PackageInfo;)Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 240
    :goto_1
    if-nez v2, :cond_0

    const-string v2, ""

    .line 241
    :cond_0
    iget-object v4, v6, Lcom/google/android/gms/icing/bf;->h:Lcom/google/android/gms/icing/o;

    invoke-static {p1, v4}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Lcom/google/android/gms/icing/o;)Lcom/google/android/gms/icing/g;

    move-result-object v4

    .line 243
    iget-object v7, v6, Lcom/google/android/gms/icing/bf;->h:Lcom/google/android/gms/icing/o;

    invoke-static {v4, v6}, Lcom/google/android/gms/icing/impl/e/i;->a(Lcom/google/android/gms/icing/g;Lcom/google/android/gms/icing/bf;)Lcom/google/android/gms/icing/ag;

    move-result-object v6

    invoke-static {v3, v2, v4, v7, v6}, Lcom/google/android/gms/icing/impl/a/j;->a(ILjava/lang/String;Lcom/google/android/gms/icing/g;Lcom/google/android/gms/icing/o;Lcom/google/android/gms/icing/ag;)Lcom/google/android/gms/icing/c/a/b;

    move-result-object v2

    .line 246
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 237
    :catch_0
    move-exception v2

    move v3, v1

    :goto_2
    const-string v7, "Information for package not found: %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    aput-object p1, v8, v1

    invoke-static {v2, v7, v8}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v2, v4

    goto :goto_1

    .line 248
    :cond_2
    return-object v5

    .line 237
    :catch_1
    move-exception v2

    goto :goto_2
.end method

.method private a(Ljava/util/List;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 266
    iget-boolean v0, p0, Lcom/google/android/gms/icing/service/g;->h:Z

    if-eqz v0, :cond_4

    .line 267
    sget-object v4, Lcom/google/android/gms/icing/service/g;->e:Ljava/lang/Object;

    monitor-enter v4

    .line 269
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/service/g;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    .line 270
    new-instance v1, Ljava/io/File;

    const-string v2, "icing_app_history_debug"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 271
    invoke-virtual {v1}, Ljava/io/File;->exists()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_3

    .line 272
    const/4 v2, 0x0

    .line 274
    :try_start_1
    new-instance v1, Ljava/io/BufferedOutputStream;

    iget-object v0, p0, Lcom/google/android/gms/icing/service/g;->f:Landroid/content/Context;

    const-string v5, "icing_app_history_debug"

    const v6, 0x8000

    invoke-virtual {v0, v5, v6}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 276
    const/16 v0, 0x2000

    :try_start_2
    new-array v5, v0, [B

    .line 277
    const/4 v0, 0x0

    array-length v2, v5

    invoke-static {v5, v0, v2}, Lcom/google/protobuf/nano/b;->a([BII)Lcom/google/protobuf/nano/b;

    move-result-object v6

    .line 279
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_8
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v7

    move v2, v3

    :goto_0
    :try_start_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/c/a/b;

    .line 280
    invoke-virtual {v0}, Lcom/google/android/gms/icing/c/a/b;->getSerializedSize()I

    move-result v3

    invoke-virtual {v6, v3}, Lcom/google/protobuf/nano/b;->d(I)V

    .line 281
    invoke-virtual {v0, v6}, Lcom/google/android/gms/icing/c/a/b;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 282
    add-int/lit8 v2, v2, 0x1

    .line 283
    goto :goto_0

    .line 284
    :cond_0
    const/4 v0, 0x0

    array-length v3, v5

    iget-object v6, v6, Lcom/google/protobuf/nano/b;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v6

    sub-int/2addr v3, v6

    invoke-virtual {v1, v5, v0, v3}, Ljava/io/BufferedOutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_9
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 290
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 298
    :cond_1
    :goto_1
    :try_start_5
    const-string v0, "Dumped %d app history events to debug file."

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 302
    :goto_2
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 306
    :goto_3
    return-void

    .line 285
    :catch_0
    move-exception v0

    move-object v1, v2

    move v2, v3

    :goto_4
    :try_start_6
    const-string v3, "Unable to create app history debug file."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0, v3, v5}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 290
    if-eqz v1, :cond_1

    .line 292
    :try_start_7
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_1

    .line 295
    :catch_1
    move-exception v0

    goto :goto_1

    .line 287
    :catch_2
    move-exception v0

    move-object v1, v2

    move v2, v3

    :goto_5
    :try_start_8
    const-string v3, "Failed to write to app history debug file."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0, v3, v5}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 290
    if-eqz v1, :cond_1

    .line 292
    :try_start_9
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_1

    .line 295
    :catch_3
    move-exception v0

    goto :goto_1

    .line 290
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_6
    if-eqz v1, :cond_2

    .line 292
    :try_start_a
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 295
    :cond_2
    :goto_7
    :try_start_b
    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 302
    :catchall_1
    move-exception v0

    monitor-exit v4

    throw v0

    .line 300
    :cond_3
    :try_start_c
    const-string v0, "App history debug file doesn\'t exist."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;)I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto :goto_2

    .line 304
    :cond_4
    const-string v0, "App history debug disabled."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;)I

    goto :goto_3

    .line 295
    :catch_4
    move-exception v0

    goto :goto_1

    :catch_5
    move-exception v1

    goto :goto_7

    .line 290
    :catchall_2
    move-exception v0

    goto :goto_6

    .line 287
    :catch_6
    move-exception v0

    move v2, v3

    goto :goto_5

    :catch_7
    move-exception v0

    goto :goto_5

    .line 285
    :catch_8
    move-exception v0

    move v2, v3

    goto :goto_4

    :catch_9
    move-exception v0

    goto :goto_4
.end method

.method static a([Lcom/google/android/gms/appdatasearch/UsageInfo;)V
    .locals 4

    .prologue
    .line 252
    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 253
    aget-object v1, p0, v0

    if-eqz v1, :cond_0

    .line 254
    aget-object v1, p0, v0

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/bb;->a(Lcom/google/android/gms/appdatasearch/UsageInfo;)Ljava/lang/String;

    move-result-object v1

    .line 255
    if-eqz v1, :cond_0

    .line 257
    const-string v2, "AppIndexApi"

    const-string v3, "Received an invalid action."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Ignoring usage report, got bad usage info: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;)I

    .line 260
    const/4 v1, 0x0

    aput-object v1, p0, v0

    .line 252
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 263
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/appdatasearch/GetRecentContextCall$Request;Lcom/google/android/gms/appdatasearch/a/h;)V
    .locals 2

    .prologue
    .line 444
    iget-object v0, p0, Lcom/google/android/gms/icing/service/g;->f:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/icing/service/g;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 445
    iget-object v0, p0, Lcom/google/android/gms/icing/service/g;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/service/g;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    .line 447
    if-eqz v0, :cond_0

    const-string v0, "com.google.android.googlequicksearchbox"

    iget-object v1, p0, Lcom/google/android/gms/icing/service/g;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 449
    :goto_0
    if-nez v0, :cond_1

    .line 450
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 447
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 452
    :cond_1
    new-instance v0, Lcom/google/android/gms/icing/service/k;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/icing/service/k;-><init>(Lcom/google/android/gms/icing/service/g;Lcom/google/android/gms/appdatasearch/GetRecentContextCall$Request;Lcom/google/android/gms/appdatasearch/a/h;)V

    .line 466
    iget-object v1, p0, Lcom/google/android/gms/icing/service/g;->f:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/service/LightweightIndexService$LightweightWorkerService;->a(Landroid/content/Context;Ljava/lang/Runnable;)V

    .line 467
    return-void
.end method

.method public final a(Lcom/google/android/gms/appdatasearch/a/h;)V
    .locals 2

    .prologue
    .line 416
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 417
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 420
    :cond_0
    new-instance v0, Lcom/google/android/gms/icing/service/j;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/icing/service/j;-><init>(Lcom/google/android/gms/icing/service/g;Lcom/google/android/gms/appdatasearch/a/h;)V

    .line 438
    iget-object v1, p0, Lcom/google/android/gms/icing/service/g;->f:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/service/LightweightIndexService$LightweightWorkerService;->a(Landroid/content/Context;Ljava/lang/Runnable;)V

    .line 439
    return-void
.end method

.method public final a(Lcom/google/android/gms/appdatasearch/a/h;Ljava/lang/String;[Lcom/google/android/gms/appdatasearch/UsageInfo;)V
    .locals 11

    .prologue
    .line 311
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "packageName empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null usageInfo"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/a/a;->A:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/icing/service/g;->i:Landroid/app/ActivityManager;

    invoke-virtual {v1, v0}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v6

    :goto_0
    const-string v0, "AppIndexApi"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0, p2, v6, p3}, Lcom/google/android/gms/icing/service/g;->a(Ljava/lang/String;Ljava/util/List;[Lcom/google/android/gms/appdatasearch/UsageInfo;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/c/a/b;

    const-string v1, "AppIndexApi"

    const-string v3, "Action:"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "AppIndexApi"

    const-string v3, "  pkg:%s ts:%d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v7, v0, Lcom/google/android/gms/icing/c/a/b;->b:Ljava/lang/String;

    aput-object v7, v4, v5

    const/4 v5, 0x1

    iget-wide v8, v0, Lcom/google/android/gms/icing/c/a/b;->a:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "AppIndexApi"

    const-string v3, "  title:[%s]"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v7, v0, Lcom/google/android/gms/icing/c/a/b;->i:Ljava/lang/String;

    aput-object v7, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "AppIndexApi"

    const-string v3, "  appUri:%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v7, v0, Lcom/google/android/gms/icing/c/a/b;->j:Ljava/lang/String;

    aput-object v7, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "AppIndexApi"

    const-string v3, "  webUrl:%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v7, v0, Lcom/google/android/gms/icing/c/a/b;->k:Ljava/lang/String;

    aput-object v7, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "AppIndexApi"

    const-string v3, "  actionType:%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v7, v0, Lcom/google/android/gms/icing/c/a/b;->g:Ljava/lang/String;

    aput-object v7, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "  top:%s @%d"

    if-eqz v6, :cond_4

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x0

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v1, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    :goto_1
    iget v4, v0, Lcom/google/android/gms/icing/c/a/b;->p:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v1, v4}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    iget-object v1, v0, Lcom/google/android/gms/icing/c/a/b;->n:Lcom/google/android/gms/icing/c/a/d;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/gms/icing/c/a/b;->n:Lcom/google/android/gms/icing/c/a/d;

    iget-object v1, v1, Lcom/google/android/gms/icing/c/a/d;->a:[Lcom/google/android/gms/icing/c/a/e;

    array-length v1, v1

    if-lez v1, :cond_2

    const-string v1, "AppIndexApi"

    const-string v3, "  outLinks:"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v0, Lcom/google/android/gms/icing/c/a/b;->n:Lcom/google/android/gms/icing/c/a/d;

    iget-object v1, v0, Lcom/google/android/gms/icing/c/a/d;->a:[Lcom/google/android/gms/icing/c/a/e;

    array-length v3, v1

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v3, :cond_2

    aget-object v4, v1, v0

    const-string v5, "AppIndexApi"

    const-string v7, "    appUri:%s webUrl:%s viewId:%d"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, v4, Lcom/google/android/gms/icing/c/a/e;->a:Ljava/lang/String;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, v4, Lcom/google/android/gms/icing/c/a/e;->b:Ljava/lang/String;

    aput-object v10, v8, v9

    const/4 v9, 0x2

    iget v4, v4, Lcom/google/android/gms/icing/c/a/e;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    const/4 v6, 0x0

    goto/16 :goto_0

    :cond_4
    const-string v1, "none"

    goto :goto_1

    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v0, p0, Lcom/google/android/gms/icing/service/g;->f:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/icing/service/g;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v7

    iget-boolean v0, p0, Lcom/google/android/gms/icing/service/g;->h:Z

    if-eqz v0, :cond_6

    const-string v0, "Not reporting usage and writing to app history debug file instead."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;)I

    invoke-static {p3}, Lcom/google/android/gms/icing/service/g;->a([Lcom/google/android/gms/appdatasearch/UsageInfo;)V

    invoke-direct {p0, p2, v6, p3}, Lcom/google/android/gms/icing/service/g;->a(Ljava/lang/String;Ljava/util/List;[Lcom/google/android/gms/appdatasearch/UsageInfo;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/service/g;->a(Ljava/util/List;)V

    .line 312
    :goto_3
    return-void

    .line 311
    :cond_6
    new-instance v0, Lcom/google/android/gms/icing/service/i;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p2

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/icing/service/i;-><init>(Lcom/google/android/gms/icing/service/g;[Lcom/google/android/gms/appdatasearch/UsageInfo;Ljava/lang/String;JLjava/util/List;ZLcom/google/android/gms/appdatasearch/a/h;)V

    iget-object v1, p0, Lcom/google/android/gms/icing/service/g;->f:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/service/LightweightIndexService$LightweightWorkerService;->a(Landroid/content/Context;Ljava/lang/Runnable;)V

    goto :goto_3
.end method

.method public final a(Lcom/google/android/gms/appdatasearch/a/h;Z)V
    .locals 2

    .prologue
    .line 490
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 491
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 493
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/service/g;->f:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/icing/service/m;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/icing/service/m;-><init>(Lcom/google/android/gms/icing/service/g;ZLcom/google/android/gms/appdatasearch/a/h;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/service/LightweightIndexService$LightweightWorkerService;->a(Landroid/content/Context;Ljava/lang/Runnable;)V

    .line 506
    return-void
.end method

.method final a()Z
    .locals 3

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/gms/icing/service/g;->f:Landroid/content/Context;

    const-string v1, "lightweight-appdatasearch"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "usage_reporting_enabled"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/gms/appdatasearch/a/h;)V
    .locals 2

    .prologue
    .line 471
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 472
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 474
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/service/g;->f:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/icing/service/l;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/icing/service/l;-><init>(Lcom/google/android/gms/icing/service/g;Lcom/google/android/gms/appdatasearch/a/h;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/service/LightweightIndexService$LightweightWorkerService;->a(Landroid/content/Context;Ljava/lang/Runnable;)V

    .line 485
    return-void
.end method
