.class final Lcom/google/android/gms/backup/z;
.super Landroid/app/backup/BackupTransport;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/backup/BackupTransportService;

.field private b:Lcom/google/android/gms/backup/p;

.field private c:Ljava/lang/String;

.field private d:Lcom/google/android/gms/backup/ao;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/util/HashMap;

.field private h:Ljava/util/HashMap;

.field private i:Lcom/google/ae/a/s;

.field private j:Landroid/os/ParcelFileDescriptor;

.field private k:Lcom/google/android/gms/backup/af;

.field private l:Ljava/lang/Object;

.field private m:Ljava/util/concurrent/atomic/AtomicInteger;

.field private n:Lcom/google/ae/a/z;

.field private o:Ljava/lang/Boolean;

.field private p:Ljava/io/InputStream;

.field private q:Ljavax/net/ssl/HttpsURLConnection;

.field private r:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/gms/backup/BackupTransportService;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 192
    iput-object p1, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-direct {p0}, Landroid/app/backup/BackupTransport;-><init>()V

    .line 194
    iput-object v1, p0, Lcom/google/android/gms/backup/z;->b:Lcom/google/android/gms/backup/p;

    .line 195
    iput-object v1, p0, Lcom/google/android/gms/backup/z;->c:Ljava/lang/String;

    .line 198
    iput-object v1, p0, Lcom/google/android/gms/backup/z;->d:Lcom/google/android/gms/backup/ao;

    .line 199
    iput-object v1, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    .line 200
    iput-object v1, p0, Lcom/google/android/gms/backup/z;->f:Ljava/lang/String;

    .line 203
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->g:Ljava/util/HashMap;

    .line 205
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->h:Ljava/util/HashMap;

    .line 207
    iput-object v1, p0, Lcom/google/android/gms/backup/z;->i:Lcom/google/ae/a/s;

    .line 208
    iput-object v1, p0, Lcom/google/android/gms/backup/z;->j:Landroid/os/ParcelFileDescriptor;

    .line 209
    iput-object v1, p0, Lcom/google/android/gms/backup/z;->k:Lcom/google/android/gms/backup/af;

    .line 210
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->l:Ljava/lang/Object;

    .line 212
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 213
    iput-object v1, p0, Lcom/google/android/gms/backup/z;->n:Lcom/google/ae/a/z;

    .line 214
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->o:Ljava/lang/Boolean;

    .line 219
    iput-object v1, p0, Lcom/google/android/gms/backup/z;->p:Ljava/io/InputStream;

    .line 220
    iput-object v1, p0, Lcom/google/android/gms/backup/z;->q:Ljavax/net/ssl/HttpsURLConnection;

    .line 221
    iput-object v1, p0, Lcom/google/android/gms/backup/z;->r:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/backup/z;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 918
    iget-object v1, p0, Lcom/google/android/gms/backup/z;->l:Ljava/lang/Object;

    monitor-enter v1

    .line 919
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 922
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->l:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 925
    :catch_0
    move-exception v0

    goto :goto_0

    .line 927
    :cond_0
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 928
    iput-object v2, p0, Lcom/google/android/gms/backup/z;->k:Lcom/google/android/gms/backup/af;

    .line 929
    iput-object v2, p0, Lcom/google/android/gms/backup/z;->i:Lcom/google/ae/a/s;

    .line 930
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->j:Landroid/os/ParcelFileDescriptor;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    .line 931
    iput-object v2, p0, Lcom/google/android/gms/backup/z;->j:Landroid/os/ParcelFileDescriptor;

    .line 932
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->o:Ljava/lang/Boolean;

    .line 933
    return-void

    .line 927
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/backup/z;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 192
    invoke-direct {p0, p1}, Lcom/google/android/gms/backup/z;->b(Ljava/lang/String;)V

    return-void
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 939
    sget-object v0, Lcom/google/android/gms/backup/a/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 940
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    .line 950
    :goto_0
    return v0

    .line 942
    :cond_0
    const-string v3, "ALL"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v0, v2

    .line 943
    goto :goto_0

    .line 945
    :cond_1
    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_3

    aget-object v5, v3, v0

    .line 946
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v0, v2

    .line 947
    goto :goto_0

    .line 945
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 950
    goto :goto_0
.end method

.method private varargs a(Z[Landroid/content/pm/PackageInfo;)[Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 989
    iget-object v1, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-virtual {v1}, Lcom/google/android/gms/backup/BackupTransportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 991
    const-string v2, "backup_restore_blacklist"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 992
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/k/c/em;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v2

    .line 994
    const-string v3, "full_backup_restore_whitelist"

    const-string v4, "ALL"

    invoke-static {v1, v3, v4}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 996
    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/k/c/em;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    .line 998
    const-string v5, "backup_restore_full_minTarget"

    invoke-static {v1, v5, v0}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    .line 1001
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1003
    array-length v7, p2

    move v1, v0

    :goto_0
    if-ge v1, v7, :cond_4

    aget-object v8, p2, v1

    .line 1004
    iget-object v9, v8, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 1007
    iget-object v10, v8, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v2, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 1008
    if-eqz p1, :cond_0

    const-string v10, "ALL"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    invoke-interface {v4, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1014
    :cond_0
    if-eqz v5, :cond_1

    iget-object v10, v8, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v10, v10, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    if-ge v10, v5, :cond_1

    .line 1020
    iget-object v10, v8, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v10, v10, Landroid/content/pm/ApplicationInfo;->backupAgentName:Ljava/lang/String;

    if-eqz v10, :cond_2

    .line 1021
    iget-object v8, v8, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v8, v8, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v10, 0x4000000

    and-int/2addr v8, v10

    if-nez v8, :cond_2

    .line 1025
    :cond_1
    const-string v8, "com.android.providers.settings"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1035
    const/4 v0, 0x1

    .line 1003
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1037
    :cond_3
    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1041
    :cond_4
    if-eqz v0, :cond_5

    .line 1042
    const-string v0, "com.android.providers.settings"

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1044
    :cond_5
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v6, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/backup/z;)Lcom/google/android/gms/backup/af;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->k:Lcom/google/android/gms/backup/af;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1062
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->p:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 1063
    iput-object v1, p0, Lcom/google/android/gms/backup/z;->p:Ljava/io/InputStream;

    .line 1064
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->q:Ljavax/net/ssl/HttpsURLConnection;

    if-eqz v0, :cond_0

    .line 1065
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->q:Ljavax/net/ssl/HttpsURLConnection;

    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    .line 1066
    iput-object v1, p0, Lcom/google/android/gms/backup/z;->q:Ljavax/net/ssl/HttpsURLConnection;

    .line 1068
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1146
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 1147
    iget-object v1, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v1}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;)Landroid/accounts/Account;

    move-result-object v1

    .line 1148
    if-nez v1, :cond_0

    .line 1149
    new-instance v0, Landroid/accounts/AccountsException;

    const-string v1, "Missing backup account"

    invoke-direct {v0, v1}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1152
    :cond_0
    :try_start_0
    iget-object v1, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1155
    return-void

    .line 1154
    :catch_0
    move-exception v0

    new-instance v0, Landroid/accounts/AccountsException;

    const-string v1, "Fail to invalid auth token."

    invoke-direct {v0, v1}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic c(Lcom/google/android/gms/backup/z;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->l:Ljava/lang/Object;

    return-object v0
.end method

.method private c()Ljavax/net/ssl/HttpsURLConnection;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1072
    invoke-static {}, Lcom/google/android/gms/backup/BackupTransportService;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GmsBackupTransport"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Create http connection for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1074
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->h:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1075
    if-nez v0, :cond_1

    .line 1076
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 1079
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/backup/z;->r:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 1080
    invoke-direct {p0}, Lcom/google/android/gms/backup/z;->d()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/backup/z;->r:Ljava/lang/String;

    .line 1083
    :cond_2
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 1084
    invoke-virtual {v2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    .line 1085
    const-string v2, "GET"

    invoke-virtual {v0, v2}, Ljavax/net/ssl/HttpsURLConnection;->setRequestMethod(Ljava/lang/String;)V

    move v2, v1

    .line 1090
    :goto_0
    if-nez v2, :cond_3

    const/4 v3, 0x3

    if-gt v1, v3, :cond_3

    .line 1091
    add-int/lit8 v1, v1, 0x1

    .line 1092
    const-string v3, "Authorization"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Bearer "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/gms/backup/z;->r:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljavax/net/ssl/HttpsURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1093
    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->connect()V

    .line 1094
    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v3

    .line 1095
    const-string v4, "GmsBackupTransport"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Http response status : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1096
    sparse-switch v3, :sswitch_data_0

    .line 1112
    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    .line 1113
    new-instance v0, Ljava/io/IOException;

    const-string v1, "HTTP Connection ERROR."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1099
    :sswitch_0
    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    .line 1101
    const-wide/16 v4, 0x2710

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1105
    iget-object v3, p0, Lcom/google/android/gms/backup/z;->r:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/google/android/gms/backup/z;->b(Ljava/lang/String;)V

    .line 1106
    invoke-direct {p0}, Lcom/google/android/gms/backup/z;->d()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gms/backup/z;->r:Ljava/lang/String;

    goto :goto_0

    .line 1102
    :catch_0
    move-exception v0

    .line 1103
    new-instance v1, Landroid/accounts/AccountsException;

    invoke-direct {v1, v0}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1109
    :sswitch_1
    const/4 v2, 0x1

    .line 1110
    goto :goto_0

    .line 1115
    :cond_3
    if-nez v2, :cond_4

    .line 1118
    new-instance v0, Landroid/accounts/AccountsException;

    const-string v1, "Cannot get valid auth token."

    invoke-direct {v0, v1}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1121
    :cond_4
    return-object v0

    .line 1096
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_1
        0x191 -> :sswitch_0
    .end sparse-switch
.end method

.method private d()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1125
    invoke-static {}, Lcom/google/android/gms/backup/BackupTransportService;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GmsBackupTransport"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Get drive auth token for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1127
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 1128
    iget-object v1, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v1}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;)Landroid/accounts/Account;

    move-result-object v1

    .line 1129
    if-nez v1, :cond_1

    .line 1130
    new-instance v0, Landroid/accounts/AccountsException;

    const-string v1, "Missing backup account"

    invoke-direct {v0, v1}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1133
    :cond_1
    :try_start_0
    const-string v2, "oauth2:https://www.googleapis.com/auth/drive"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 1134
    if-nez v0, :cond_2

    .line 1135
    new-instance v0, Landroid/accounts/AccountsException;

    const-string v1, "No auth token available"

    invoke-direct {v0, v1}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1141
    :catch_0
    move-exception v0

    new-instance v0, Landroid/accounts/AccountsException;

    const-string v1, "Get auth token error"

    invoke-direct {v0, v1}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1138
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/google/android/gms/backup/BackupTransportService;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "GmsBackupTransport"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Got auth token : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1139
    :cond_3
    return-object v0
.end method

.method private e()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1163
    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "lock not held"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1164
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/backup/z;->b:Lcom/google/android/gms/backup/p;

    if-eqz v1, :cond_1

    .line 1176
    :goto_0
    return v0

    .line 1166
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1167
    iget-object v1, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v1}, Lcom/google/android/gms/backup/BackupTransportService;->b(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v1

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v4, v5}, Lcom/android/a/a;->a(J)V

    .line 1170
    iget-object v1, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v1}, Lcom/google/android/gms/backup/BackupTransportService;->b(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v4}, Lcom/google/android/gms/backup/BackupTransportService;->c(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/b;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/android/a/a;->a(Lcom/android/a/b;)J

    move-result-wide v4

    cmp-long v1, v4, v2

    if-lez v1, :cond_2

    .line 1171
    const-string v0, "GmsBackupTransport"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not ready for backup request right now: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v2}, Lcom/google/android/gms/backup/BackupTransportService;->b(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1172
    const/4 v0, 0x0

    goto :goto_0

    .line 1174
    :cond_2
    invoke-static {}, Lcom/google/android/gms/backup/BackupTransportService;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "GmsBackupTransport"

    const-string v2, "starting new backup session"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1175
    :cond_3
    new-instance v1, Lcom/google/android/gms/backup/p;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/backup/p;-><init>(J)V

    iput-object v1, p0, Lcom/google/android/gms/backup/z;->b:Lcom/google/android/gms/backup/p;

    goto :goto_0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 1187
    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "lock not held"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1188
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->d:Lcom/google/android/gms/backup/ao;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "restore was never started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1189
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1190
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->d:Lcom/google/android/gms/backup/ao;

    iget-object v1, p0, Lcom/google/android/gms/backup/z;->g:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/google/android/gms/backup/z;->h:Ljava/util/HashMap;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/backup/ao;->a(Ljava/util/Map;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    .line 1191
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 1192
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->d:Lcom/google/android/gms/backup/ao;

    iget-object v0, v0, Lcom/google/android/gms/backup/ao;->a:Lcom/google/android/gms/backup/i;

    .line 1193
    if-nez v0, :cond_4

    .line 1200
    :cond_3
    return-void

    .line 1195
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v1}, Lcom/google/android/gms/backup/BackupTransportService;->h(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a;->a(J)V

    .line 1196
    iget-object v1, p0, Lcom/google/android/gms/backup/z;->d:Lcom/google/android/gms/backup/ao;

    iget-object v2, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    iget-object v3, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v3}, Lcom/google/android/gms/backup/BackupTransportService;->h(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v3

    invoke-static {v2, v0, v3}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;Lcom/google/android/gms/backup/i;Lcom/android/a/a;)Lcom/google/android/gms/backup/u;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/backup/ao;->a(Lcom/google/android/gms/backup/u;)V

    .line 1197
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->d:Lcom/google/android/gms/backup/ao;

    iget-object v1, p0, Lcom/google/android/gms/backup/z;->g:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/google/android/gms/backup/z;->h:Ljava/util/HashMap;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/backup/ao;->a(Ljava/util/Map;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    .line 1198
    invoke-static {}, Lcom/google/android/gms/backup/BackupTransportService;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "GmsBackupTransport"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/backup/z;->g:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " keys"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public final abortFullRestore()I
    .locals 1

    .prologue
    .line 908
    invoke-direct {p0}, Lcom/google/android/gms/backup/z;->b()V

    .line 909
    const/4 v0, 0x0

    return v0
.end method

.method public final cancelFullBackup()V
    .locals 2

    .prologue
    .line 840
    invoke-static {}, Lcom/google/android/gms/backup/BackupTransportService;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 841
    const-string v0, "GmsBackupTransport"

    const-string v1, "Manually throw IOException to scotty client to cancel http transfer."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 842
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->k:Lcom/google/android/gms/backup/af;

    invoke-virtual {v0}, Lcom/google/android/gms/backup/af;->c()V

    .line 843
    invoke-direct {p0}, Lcom/google/android/gms/backup/z;->a()V

    .line 844
    return-void
.end method

.method public final declared-synchronized clearBackupData(Landroid/content/pm/PackageInfo;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 428
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/backup/z;->e()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    const/16 v0, -0x3e8

    .line 445
    :goto_0
    monitor-exit p0

    return v0

    .line 429
    :cond_0
    const/4 v1, 0x0

    .line 430
    :try_start_1
    iget-object v3, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 432
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-virtual {v2}, Lcom/google/android/gms/backup/BackupTransportService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 434
    iget-object v4, v2, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v4, :cond_1

    iget-object v4, v2, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v5, "com.google.android.backup.api_key"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 435
    :cond_1
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    .line 437
    :goto_1
    if-nez v2, :cond_3

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_3

    .line 438
    :cond_2
    const-string v2, "GmsBackupTransport"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "IGNORING WIPE without API key: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 442
    :catch_0
    move-exception v2

    :try_start_3
    const-string v2, "GmsBackupTransport"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown package in wipe request: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/backup/z;->b:Lcom/google/android/gms/backup/p;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/backup/p;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/backup/q;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/google/android/gms/backup/q;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 428
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    move v2, v0

    .line 435
    goto :goto_1
.end method

.method public final configurationIntent()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 231
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    const-class v2, Lcom/google/android/gms/backup/SetBackupAccountActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public final currentDestinationString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;)Landroid/accounts/Account;

    move-result-object v0

    .line 237
    invoke-static {}, Lcom/google/android/gms/backup/BackupTransportService;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GmsBackupTransport"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "currentDestinationString: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    :cond_0
    if-eqz v0, :cond_1

    .line 239
    iget-object v1, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-virtual {v1}, Lcom/google/android/gms/backup/BackupTransportService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->xG:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 242
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-virtual {v0}, Lcom/google/android/gms/backup/BackupTransportService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->rm:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final dataManagementIntent()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;)Landroid/accounts/Account;

    move-result-object v0

    .line 250
    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-virtual {v0}, Lcom/google/android/gms/backup/BackupTransportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "backup_restore_manage_data_url"

    invoke-static {v0, v1}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 253
    if-eqz v1, :cond_0

    .line 254
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 255
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 259
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dataManagementLabel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-virtual {v0}, Lcom/google/android/gms/backup/BackupTransportService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->qX:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized finishBackup()I
    .locals 12

    .prologue
    const/16 v4, -0x3e8

    const/4 v3, 0x0

    const/4 v1, -0x1

    .line 450
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->f(Lcom/google/android/gms/backup/BackupTransportService;)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 451
    invoke-static {}, Landroid/net/TrafficStats;->setThreadStatsTagBackup()V

    .line 452
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->f(Lcom/google/android/gms/backup/BackupTransportService;)I

    move-result v0

    invoke-static {v0}, Landroid/net/TrafficStats;->setThreadStatsUid(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 456
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->o:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 457
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->k:Lcom/google/android/gms/backup/af;

    invoke-virtual {v0}, Lcom/google/android/gms/backup/af;->a()I

    .line 458
    invoke-direct {p0}, Lcom/google/android/gms/backup/z;->a()V

    .line 460
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
    :try_end_1
    .catch Lcom/google/android/gms/backup/t; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_1

    .line 461
    :try_start_2
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 504
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    .line 506
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;I)I

    .line 507
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->b:Lcom/google/android/gms/backup/p;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move v0, v3

    :goto_0
    monitor-exit p0

    return v0

    .line 465
    :cond_1
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    div-int/lit8 v0, v0, 0x64
    :try_end_3
    .catch Lcom/google/android/gms/backup/t; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 466
    :try_start_4
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 504
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    .line 506
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;I)I

    .line 507
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->b:Lcom/google/android/gms/backup/p;

    move v0, v4

    goto :goto_0

    .line 470
    :cond_2
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 504
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    .line 506
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;I)I

    .line 507
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->b:Lcom/google/android/gms/backup/p;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    const/16 v0, -0x3ea

    goto :goto_0

    .line 474
    :cond_3
    :try_start_5
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->b:Lcom/google/android/gms/backup/p;

    if-eqz v0, :cond_9

    .line 475
    iget-object v5, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    iget-object v6, p0, Lcom/google/android/gms/backup/z;->b:Lcom/google/android/gms/backup/p;

    new-instance v7, Lcom/google/android/gms/backup/i;

    invoke-direct {v7}, Lcom/google/android/gms/backup/i;-><init>()V

    const/4 v0, 0x2

    invoke-virtual {v7, v0}, Lcom/google/android/gms/backup/i;->b(I)Lcom/google/android/gms/backup/i;

    iget-boolean v0, v6, Lcom/google/android/gms/backup/p;->c:Z

    if-eqz v0, :cond_4

    new-instance v0, Lcom/google/android/gms/backup/n;

    invoke-direct {v0}, Lcom/google/android/gms/backup/n;-><init>()V

    invoke-virtual {v7, v0}, Lcom/google/android/gms/backup/i;->a(Lcom/google/android/gms/backup/n;)Lcom/google/android/gms/backup/i;

    const/4 v0, 0x0

    iput-boolean v0, v6, Lcom/google/android/gms/backup/p;->c:Z

    :cond_4
    iget-object v0, v6, Lcom/google/android/gms/backup/p;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_5
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/backup/q;

    iget-object v2, v1, Lcom/google/android/gms/backup/q;->a:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, v1, Lcom/google/android/gms/backup/q;->b:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, v1, Lcom/google/android/gms/backup/q;->c:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    :cond_6
    new-instance v9, Lcom/google/android/gms/backup/j;

    invoke-direct {v9}, Lcom/google/android/gms/backup/j;-><init>()V

    invoke-virtual {v7, v9}, Lcom/google/android/gms/backup/i;->a(Lcom/google/android/gms/backup/j;)Lcom/google/android/gms/backup/i;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v9, v0}, Lcom/google/android/gms/backup/j;->a(Ljava/lang/String;)Lcom/google/android/gms/backup/j;

    iget-object v0, v1, Lcom/google/android/gms/backup/q;->d:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, v1, Lcom/google/android/gms/backup/q;->d:Ljava/lang/String;

    invoke-virtual {v9, v0}, Lcom/google/android/gms/backup/j;->b(Ljava/lang/String;)Lcom/google/android/gms/backup/j;

    :cond_7
    iget-object v0, v1, Lcom/google/android/gms/backup/q;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v11, Lcom/google/android/gms/backup/x;

    invoke-direct {v11}, Lcom/google/android/gms/backup/x;-><init>()V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v11, v2}, Lcom/google/android/gms/backup/x;->a(Ljava/lang/String;)Lcom/google/android/gms/backup/x;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0}, Lcom/google/protobuf/a/a;->a([B)Lcom/google/protobuf/a/a;

    move-result-object v0

    invoke-virtual {v11, v0}, Lcom/google/android/gms/backup/x;->a(Lcom/google/protobuf/a/a;)Lcom/google/android/gms/backup/x;

    :cond_8
    invoke-virtual {v9, v11}, Lcom/google/android/gms/backup/j;->a(Lcom/google/android/gms/backup/x;)Lcom/google/android/gms/backup/j;
    :try_end_5
    .catch Lcom/google/android/gms/backup/s; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lcom/google/android/gms/backup/t; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 477
    :catch_0
    move-exception v0

    .line 478
    :try_start_6
    const-string v1, "GmsBackupTransport"

    const-string v2, "Server policy rejection: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 480
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->g(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v0

    const-wide/16 v6, 0x0

    invoke-virtual {v0, v6, v7}, Lcom/android/a/a;->a(J)V

    .line 481
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    iget-object v1, p0, Lcom/google/android/gms/backup/z;->b:Lcom/google/android/gms/backup/p;

    invoke-virtual {v1}, Lcom/google/android/gms/backup/p;->c()Lcom/google/android/gms/backup/i;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v2}, Lcom/google/android/gms/backup/BackupTransportService;->g(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;Lcom/google/android/gms/backup/i;Lcom/android/a/a;)Lcom/google/android/gms/backup/u;

    .line 484
    :cond_9
    :goto_3
    const-string v0, "GmsBackupTransport"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "backup finished for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/backup/z;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/backup/z;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 487
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sget-object v0, Lcom/google/android/gms/backup/a/a;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_10

    new-instance v1, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {v1, v8, v9}, Ljava/util/Random;-><init>(J)V

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    move v1, v0

    :goto_4
    sget-object v0, Lcom/google/android/gms/backup/a/a;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v8, v0

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    int-to-long v0, v1

    const-wide/32 v10, 0x36ee80

    mul-long/2addr v0, v10

    add-long/2addr v0, v8

    add-long/2addr v0, v6

    .line 490
    iget-object v2, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v2}, Lcom/google/android/gms/backup/BackupTransportService;->e(Lcom/google/android/gms/backup/BackupTransportService;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/gms/backup/z;->c:Ljava/lang/String;

    invoke-interface {v2, v5, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_6
    .catch Lcom/google/android/gms/backup/t; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 494
    :cond_a
    :try_start_7
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 504
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    .line 506
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;I)I

    .line 507
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->b:Lcom/google/android/gms/backup/p;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move v0, v3

    goto/16 :goto_0

    .line 475
    :cond_b
    :try_start_8
    iget-object v0, v1, Lcom/google/android/gms/backup/q;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, v1, Lcom/google/android/gms/backup/q;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v10, Lcom/google/android/gms/backup/k;

    invoke-direct {v10}, Lcom/google/android/gms/backup/k;-><init>()V

    const-string v11, ""

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_c

    invoke-virtual {v10, v0}, Lcom/google/android/gms/backup/k;->a(Ljava/lang/String;)Lcom/google/android/gms/backup/k;

    :cond_c
    invoke-virtual {v9, v10}, Lcom/google/android/gms/backup/j;->a(Lcom/google/android/gms/backup/k;)Lcom/google/android/gms/backup/j;
    :try_end_8
    .catch Lcom/google/android/gms/backup/s; {:try_start_8 .. :try_end_8} :catch_0
    .catch Lcom/google/android/gms/backup/t; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_5

    .line 496
    :catch_1
    move-exception v0

    .line 497
    :try_start_9
    const-string v1, "GmsBackupTransport"

    const-string v2, "Backup server requires initialization: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 498
    :try_start_a
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 504
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    .line 506
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;I)I

    .line 507
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->b:Lcom/google/android/gms/backup/p;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    const/16 v0, -0x3e9

    goto/16 :goto_0

    .line 475
    :cond_d
    :try_start_b
    iget-object v0, v1, Lcom/google/android/gms/backup/q;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v6, Lcom/google/android/gms/backup/p;->a:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Lcom/google/android/gms/backup/m;

    invoke-direct {v11}, Lcom/google/android/gms/backup/m;-><init>()V

    invoke-virtual {v11, v10}, Lcom/google/android/gms/backup/m;->a(Ljava/lang/String;)Lcom/google/android/gms/backup/m;

    invoke-virtual {v11, v0}, Lcom/google/android/gms/backup/m;->b(Ljava/lang/String;)Lcom/google/android/gms/backup/m;

    invoke-virtual {v9, v11}, Lcom/google/android/gms/backup/j;->a(Lcom/google/android/gms/backup/m;)Lcom/google/android/gms/backup/j;
    :try_end_b
    .catch Lcom/google/android/gms/backup/s; {:try_start_b .. :try_end_b} :catch_0
    .catch Lcom/google/android/gms/backup/t; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_6

    .line 499
    :catch_2
    move-exception v0

    .line 500
    :try_start_c
    const-string v1, "GmsBackupTransport"

    const-string v2, "Error sending final backup to server: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 501
    :try_start_d
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 504
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    .line 506
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;I)I

    .line 507
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->b:Lcom/google/android/gms/backup/p;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    move v0, v4

    goto/16 :goto_0

    .line 475
    :cond_e
    :try_start_e
    iget-object v0, v1, Lcom/google/android/gms/backup/q;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V
    :try_end_e
    .catch Lcom/google/android/gms/backup/s; {:try_start_e .. :try_end_e} :catch_0
    .catch Lcom/google/android/gms/backup/t; {:try_start_e .. :try_end_e} :catch_1
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto/16 :goto_1

    .line 503
    :catchall_0
    move-exception v0

    :try_start_f
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 504
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    .line 506
    iget-object v1, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    const/4 v2, -0x1

    invoke-static {v1, v2}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;I)I

    .line 507
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/backup/z;->b:Lcom/google/android/gms/backup/p;

    throw v0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 450
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 475
    :cond_f
    :try_start_10
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->b(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v0

    invoke-static {v5, v7, v0}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;Lcom/google/android/gms/backup/i;Lcom/android/a/a;)Lcom/google/android/gms/backup/u;
    :try_end_10
    .catch Lcom/google/android/gms/backup/s; {:try_start_10 .. :try_end_10} :catch_0
    .catch Lcom/google/android/gms/backup/t; {:try_start_10 .. :try_end_10} :catch_1
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_2
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto/16 :goto_3

    :cond_10
    move v1, v3

    .line 487
    goto/16 :goto_4
.end method

.method public final declared-synchronized finishRestore()V
    .locals 2

    .prologue
    .line 627
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/gms/backup/z;->d:Lcom/google/android/gms/backup/ao;

    .line 628
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    .line 629
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 630
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 631
    invoke-static {}, Lcom/google/android/gms/backup/BackupTransportService;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GmsBackupTransport"

    const-string v1, "restore finished"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 632
    :cond_0
    monitor-exit p0

    return-void

    .line 627
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized getAvailableRestoreSets()[Landroid/app/backup/RestoreSet;
    .locals 10

    .prologue
    .line 514
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->h(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/android/a/a;->a(J)V

    .line 515
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    new-instance v1, Lcom/google/android/gms/backup/i;

    invoke-direct {v1}, Lcom/google/android/gms/backup/i;-><init>()V

    new-instance v2, Lcom/google/android/gms/backup/o;

    invoke-direct {v2}, Lcom/google/android/gms/backup/o;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/backup/i;->a(Lcom/google/android/gms/backup/o;)Lcom/google/android/gms/backup/i;

    iget-object v2, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v2}, Lcom/google/android/gms/backup/BackupTransportService;->h(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;Lcom/google/android/gms/backup/i;Lcom/android/a/a;)Lcom/google/android/gms/backup/u;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/backup/u;->c()I

    move-result v0

    new-array v1, v0, [Landroid/app/backup/RestoreSet;

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    invoke-virtual {v4}, Lcom/google/android/gms/backup/u;->c()I

    move-result v0

    if-ge v3, v0, :cond_1

    iget-object v0, v4, Lcom/google/android/gms/backup/u;->d:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/w;

    new-instance v5, Landroid/app/backup/RestoreSet;

    iget-boolean v2, v0, Lcom/google/android/gms/backup/w;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/android/gms/backup/w;->c:Ljava/lang/String;

    :goto_1
    const-string v6, "device"

    iget-wide v8, v0, Lcom/google/android/gms/backup/w;->a:J

    invoke-direct {v5, v2, v6, v8, v9}, Landroid/app/backup/RestoreSet;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    aput-object v5, v1, v3

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    const-string v2, "generic"

    goto :goto_1

    .line 517
    :cond_1
    invoke-static {}, Lcom/google/android/gms/backup/BackupTransportService;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "GmsBackupTransport"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "got "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " devices from server"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    move-object v0, v1

    .line 521
    :goto_2
    monitor-exit p0

    return-object v0

    .line 519
    :catch_0
    move-exception v0

    .line 520
    :try_start_1
    const-string v1, "GmsBackupTransport"

    const-string v2, "Error getting device list from server: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 521
    const/4 v0, 0x0

    goto :goto_2

    .line 514
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized getCurrentRestoreSet()J
    .locals 4

    .prologue
    .line 527
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-virtual {v0}, Lcom/google/android/gms/backup/BackupTransportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final getNextFullRestoreDataChunk(Landroid/os/ParcelFileDescriptor;)I
    .locals 6

    .prologue
    const/4 v0, -0x1

    .line 849
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/backup/BackupTransportService;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GmsBackupTransport"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Read next chunk for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 851
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/backup/z;->q:Ljavax/net/ssl/HttpsURLConnection;
    :try_end_0
    .catch Landroid/accounts/AccountsException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 853
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/gms/backup/z;->c()Ljavax/net/ssl/HttpsURLConnection;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/backup/z;->q:Ljavax/net/ssl/HttpsURLConnection;

    .line 854
    iget-object v1, p0, Lcom/google/android/gms/backup/z;->q:Ljavax/net/ssl/HttpsURLConnection;

    invoke-virtual {v1}, Ljavax/net/ssl/HttpsURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/backup/z;->p:Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 862
    :cond_1
    const v1, 0x8000

    :try_start_2
    new-array v2, v1, [B
    :try_end_2
    .catch Landroid/accounts/AccountsException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 865
    :try_start_3
    iget-object v1, p0, Lcom/google/android/gms/backup/z;->p:Ljava/io/InputStream;

    invoke-virtual {v1, v2}, Ljava/io/InputStream;->read([B)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Landroid/accounts/AccountsException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v1

    .line 871
    :try_start_4
    invoke-static {}, Lcom/google/android/gms/backup/BackupTransportService;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "GmsBackupTransport"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Read "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Bytes"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 873
    :cond_2
    if-ne v1, v0, :cond_3

    .line 875
    invoke-direct {p0}, Lcom/google/android/gms/backup/z;->b()V

    .line 876
    const-string v1, "GmsBackupTransport"

    const-string v2, "Reach end of http content -- NO MORE DATA"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Landroid/accounts/AccountsException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 877
    invoke-static {p1}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    .line 899
    :goto_0
    return v0

    .line 855
    :catch_0
    move-exception v0

    .line 856
    :try_start_5
    const-string v1, "GmsBackupTransport"

    const-string v2, "Http connection error."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 857
    throw v0
    :try_end_5
    .catch Landroid/accounts/AccountsException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 893
    :catch_1
    move-exception v0

    .line 894
    :try_start_6
    invoke-direct {p0}, Lcom/google/android/gms/backup/z;->b()V

    .line 895
    const-string v1, "GmsBackupTransport"

    invoke-virtual {v0}, Landroid/accounts/AccountsException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 896
    invoke-static {p1}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    const/16 v0, -0x3e8

    goto :goto_0

    .line 866
    :catch_2
    move-exception v0

    .line 867
    :try_start_7
    const-string v1, "GmsBackupTransport"

    const-string v2, "HTTP reading error."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 868
    throw v0
    :try_end_7
    .catch Landroid/accounts/AccountsException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 898
    :catch_3
    move-exception v0

    :try_start_8
    invoke-direct {p0}, Lcom/google/android/gms/backup/z;->b()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 899
    invoke-static {p1}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    const/16 v0, -0x3ea

    goto :goto_0

    .line 880
    :cond_3
    :try_start_9
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_9
    .catch Landroid/accounts/AccountsException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 884
    const/4 v3, 0x0

    :try_start_a
    invoke-virtual {v0, v2, v3, v1}, Ljava/io/FileOutputStream;->write([BII)V

    .line 885
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->flush()V

    .line 886
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4
    .catch Landroid/accounts/AccountsException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 892
    invoke-static {p1}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    move v0, v1

    goto :goto_0

    .line 887
    :catch_4
    move-exception v0

    .line 888
    :try_start_b
    const-string v1, "GmsBackupTransport"

    const-string v2, "Fail to write to socket."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 889
    throw v0
    :try_end_b
    .catch Landroid/accounts/AccountsException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 899
    :catchall_0
    move-exception v0

    invoke-static {p1}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    throw v0
.end method

.method public final declared-synchronized getRestoreData(Landroid/os/ParcelFileDescriptor;)I
    .locals 4

    .prologue
    const/16 v0, -0x3e8

    .line 604
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "no package to restore"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 607
    :cond_0
    :try_start_1
    new-instance v1, Landroid/app/backup/BackupDataOutput;

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/backup/BackupDataOutput;-><init>(Ljava/io/FileDescriptor;)V

    .line 608
    new-instance v2, Lcom/google/android/gms/backup/g;

    invoke-direct {v2, v1}, Lcom/google/android/gms/backup/g;-><init>(Landroid/app/backup/BackupDataOutput;)V

    .line 609
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/backup/z;->f:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 610
    iget-object v1, p0, Lcom/google/android/gms/backup/z;->g:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/backup/g;->a(Ljava/util/Map;)V

    .line 611
    invoke-direct {p0}, Lcom/google/android/gms/backup/z;->f()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/gms/backup/h; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 614
    :catch_0
    move-exception v1

    .line 615
    :try_start_2
    const-string v2, "GmsBackupTransport"

    const-string v3, "Error getting restore data from server: "

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 616
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/backup/z;->d:Lcom/google/android/gms/backup/ao;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 621
    :goto_1
    monitor-exit p0

    return v0

    .line 613
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 618
    :catch_1
    move-exception v1

    .line 619
    :try_start_3
    const-string v2, "GmsBackupTransport"

    const-string v3, "Error in restore data from server"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 620
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/backup/z;->d:Lcom/google/android/gms/backup/ao;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final declared-synchronized initializeDevice()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/16 v1, -0x3e8

    .line 315
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/backup/z;->e()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 332
    :goto_0
    monitor-exit p0

    return v0

    .line 316
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/google/android/gms/backup/BackupTransportService;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GmsBackupTransport"

    const-string v3, "*** initializing device ***"

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/backup/z;->b:Lcom/google/android/gms/backup/p;

    iget-object v0, v3, Lcom/google/android/gms/backup/p;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/q;

    iget-object v5, v0, Lcom/google/android/gms/backup/q;->a:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->clear()V

    iget-object v5, v0, Lcom/google/android/gms/backup/q;->b:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->clear()V

    iget-object v0, v0, Lcom/google/android/gms/backup/q;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 315
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 317
    :cond_2
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, v3, Lcom/google/android/gms/backup/p;->c:Z

    .line 319
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->d(Lcom/google/android/gms/backup/BackupTransportService;)Landroid/app/backup/BackupManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/backup/BackupManager;->isBackupEnabled()Z

    move-result v0

    .line 320
    if-nez v0, :cond_3

    move v0, v2

    .line 321
    goto :goto_0

    .line 325
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;)Landroid/accounts/Account;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_4

    move v0, v2

    .line 326
    goto :goto_0

    :cond_4
    move v0, v1

    .line 332
    goto :goto_0
.end method

.method public final name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    const-string v0, "com.google.android.gms/.backup.BackupTransportService"

    return-object v0
.end method

.method public final declared-synchronized nextRestorePackage()Landroid/app/backup/RestoreDescription;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 554
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->d:Lcom/google/android/gms/backup/ao;

    if-nez v0, :cond_0

    .line 555
    const-string v0, "GmsBackupTransport"

    const-string v2, "Restore processing aborted, no more packages"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v1

    .line 598
    :goto_0
    monitor-exit p0

    return-object v0

    .line 560
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-virtual {v0}, Lcom/google/android/gms/backup/BackupTransportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "enable_full_restore"

    const/4 v4, 0x1

    invoke-static {v0, v3, v4}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v3

    .line 565
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/backup/z;->f:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 566
    invoke-direct {p0}, Lcom/google/android/gms/backup/z;->f()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 595
    :catch_0
    move-exception v0

    .line 596
    :try_start_2
    const-string v2, "GmsBackupTransport"

    const-string v3, "Error getting restore data from server: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 597
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->d:Lcom/google/android/gms/backup/ao;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v0, v1

    .line 598
    goto :goto_0

    .line 568
    :cond_1
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 569
    sget-object v0, Landroid/app/backup/RestoreDescription;->NO_MORE_PACKAGES:Landroid/app/backup/RestoreDescription;

    goto :goto_0

    .line 571
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->f:Ljava/lang/String;

    .line 572
    const-string v0, "GmsBackupTransport"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Current restore app : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->h:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 575
    const-string v4, "GmsBackupTransport"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v0, "A full restore : "

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/backup/z;->h:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    if-eqz v3, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-virtual {v4}, Lcom/google/android/gms/backup/BackupTransportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "full_backup_restore_whitelist"

    const-string v6, "ALL"

    invoke-static {v4, v5, v6}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "ALL"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v0, v2

    :goto_2
    if-eqz v0, :cond_4

    .line 578
    new-instance v0, Landroid/app/backup/RestoreDescription;

    iget-object v2, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    const/4 v3, 0x2

    invoke-direct {v0, v2, v3}, Landroid/app/backup/RestoreDescription;-><init>(Ljava/lang/String;I)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 554
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 577
    :cond_3
    :try_start_4
    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/k/c/em;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    invoke-interface {v4, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_2

    .line 579
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 582
    const-string v0, "GmsBackupTransport"

    const-string v4, "Full restore feature is disabled by gservice and no key/value restore."

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 592
    :cond_5
    const-string v0, "GmsBackupTransport"

    const-string v2, "A key/value pairs restore"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 593
    new-instance v0, Landroid/app/backup/RestoreDescription;

    iget-object v2, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-direct {v0, v2, v3}, Landroid/app/backup/RestoreDescription;-><init>(Ljava/lang/String;I)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method

.method public final declared-synchronized performBackup(Landroid/content/pm/PackageInfo;Landroid/os/ParcelFileDescriptor;)I
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/16 v1, -0x3e8

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 337
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/gms/backup/a/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->b(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-object v0, Lcom/google/android/gms/backup/a/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v6, v0

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    add-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Lcom/android/a/a;->b(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move v0, v1

    .line 422
    :goto_0
    monitor-exit p0

    return v0

    .line 343
    :cond_0
    :try_start_1
    iget-object v6, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 344
    iput-object v6, p0, Lcom/google/android/gms/backup/z;->c:Ljava/lang/String;

    .line 346
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->e(Lcom/google/android/gms/backup/BackupTransportService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-wide/16 v8, 0x0

    invoke-interface {v0, v6, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 347
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 348
    invoke-static {v6}, Lcom/google/android/gms/backup/z;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    cmp-long v0, v10, v8

    if-gez v0, :cond_4

    .line 349
    const-string v0, "GmsBackupTransport"

    const-string v1, "K/V backup for %s aborted by rate limiter. next=%d, current=%d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v6, v3, v7

    const/4 v6, 0x1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v3, v6

    const/4 v6, 0x2

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v3, v6

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    const/16 v0, 0x16

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 356
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->b(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v6

    sget-object v0, Lcom/google/android/gms/backup/a/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "ALL"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->e(Lcom/google/android/gms/backup/BackupTransportService;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    move-object v3, v0

    :goto_1
    array-length v7, v3

    move-wide v0, v4

    :goto_2
    if-ge v2, v7, :cond_2

    aget-object v4, v3, v2

    iget-object v5, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v5}, Lcom/google/android/gms/backup/BackupTransportService;->e(Lcom/google/android/gms/backup/BackupTransportService;)Landroid/content/SharedPreferences;

    move-result-object v5

    const-wide/16 v8, 0x0

    invoke-interface {v5, v4, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    goto :goto_1

    :cond_2
    invoke-virtual {v6, v0, v1}, Lcom/android/a/a;->b(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 362
    :cond_3
    const/16 v0, -0x3ea

    goto/16 :goto_0

    .line 365
    :cond_4
    const/4 v0, 0x0

    .line 368
    :try_start_2
    iget-object v4, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-virtual {v4}, Lcom/google/android/gms/backup/BackupTransportService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/16 v5, 0x80

    invoke-virtual {v4, v6, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    .line 371
    iget-object v5, v4, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v5, :cond_5

    iget-object v5, v4, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v7, "com.google.android.backup.api_key"

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 372
    :cond_5
    iget v4, v4, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_7

    .line 374
    :goto_3
    if-nez v3, :cond_8

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_8

    .line 375
    :cond_6
    const-string v3, "GmsBackupTransport"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "IGNORING BACKUP DATA without API key: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move v0, v2

    .line 376
    goto/16 :goto_0

    :cond_7
    move v3, v2

    .line 372
    goto :goto_3

    :cond_8
    move-object v3, v0

    .line 385
    :goto_4
    :try_start_3
    iget-object v4, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    iget-object v0, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v0, :cond_9

    iget-object v0, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    :goto_5
    invoke-static {v4, v0}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;I)I

    .line 388
    invoke-static {}, Landroid/net/TrafficStats;->setThreadStatsTagBackup()V

    .line 389
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->f(Lcom/google/android/gms/backup/BackupTransportService;)I

    move-result v0

    invoke-static {v0}, Landroid/net/TrafficStats;->setThreadStatsUid(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 392
    :try_start_4
    invoke-direct {p0}, Lcom/google/android/gms/backup/z;->e()Z
    :try_end_4
    .catch Lcom/google/android/gms/backup/t; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v0

    if-nez v0, :cond_a

    .line 421
    :try_start_5
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 422
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    move v0, v1

    goto/16 :goto_0

    .line 379
    :catch_0
    move-exception v3

    const-string v3, "GmsBackupTransport"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown package in backup request: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-object v3, v0

    goto :goto_4

    .line 385
    :cond_9
    const/16 v0, 0x3e8

    goto :goto_5

    .line 393
    :cond_a
    :try_start_6
    const-string v0, "GmsBackupTransport"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "starting performBackup for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    new-instance v0, Lcom/google/android/gms/backup/f;

    new-instance v4, Landroid/app/backup/BackupDataInput;

    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/app/backup/BackupDataInput;-><init>(Ljava/io/FileDescriptor;)V

    iget-object v5, p0, Lcom/google/android/gms/backup/z;->b:Lcom/google/android/gms/backup/p;

    invoke-virtual {v5, v6, v3}, Lcom/google/android/gms/backup/p;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/backup/q;

    move-result-object v3

    invoke-direct {v0, v4, v3}, Lcom/google/android/gms/backup/f;-><init>(Landroid/app/backup/BackupDataInput;Lcom/google/android/gms/backup/q;)V
    :try_end_6
    .catch Lcom/google/android/gms/backup/t; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 399
    :cond_b
    :goto_6
    :try_start_7
    invoke-virtual {v0}, Lcom/google/android/gms/backup/f;->a()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 400
    iget-object v3, p0, Lcom/google/android/gms/backup/z;->b:Lcom/google/android/gms/backup/p;

    invoke-virtual {v3}, Lcom/google/android/gms/backup/p;->a()I

    move-result v3

    const v4, 0x7d000

    if-lt v3, v4, :cond_b

    .line 401
    iget-object v3, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    iget-object v4, p0, Lcom/google/android/gms/backup/z;->b:Lcom/google/android/gms/backup/p;

    invoke-virtual {v4}, Lcom/google/android/gms/backup/p;->b()Lcom/google/android/gms/backup/i;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v5}, Lcom/google/android/gms/backup/BackupTransportService;->b(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;Lcom/google/android/gms/backup/i;Lcom/android/a/a;)Lcom/google/android/gms/backup/u;

    .line 402
    iget-object v3, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v3}, Lcom/google/android/gms/backup/BackupTransportService;->b(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/android/a/a;->a(J)V
    :try_end_7
    .catch Lcom/google/android/gms/backup/s; {:try_start_7 .. :try_end_7} :catch_1
    .catch Lcom/google/android/gms/backup/t; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_6

    .line 405
    :catch_1
    move-exception v0

    .line 406
    :try_start_8
    const-string v3, "GmsBackupTransport"

    const-string v4, "Server policy rejection: "

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 408
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->g(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v0

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v4, v5}, Lcom/android/a/a;->a(J)V

    .line 409
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    iget-object v3, p0, Lcom/google/android/gms/backup/z;->b:Lcom/google/android/gms/backup/p;

    invoke-virtual {v3}, Lcom/google/android/gms/backup/p;->c()Lcom/google/android/gms/backup/i;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v4}, Lcom/google/android/gms/backup/BackupTransportService;->g(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;Lcom/google/android/gms/backup/i;Lcom/android/a/a;)Lcom/google/android/gms/backup/u;

    .line 412
    :cond_c
    const-string v0, "GmsBackupTransport"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "performBackup done for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Lcom/google/android/gms/backup/t; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 413
    :try_start_9
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 422
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move v0, v2

    goto/16 :goto_0

    .line 414
    :catch_2
    move-exception v0

    .line 415
    :try_start_a
    const-string v1, "GmsBackupTransport"

    const-string v2, "Uninitialized device: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 416
    :try_start_b
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 422
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    const/16 v0, -0x3e9

    goto/16 :goto_0

    .line 417
    :catch_3
    move-exception v0

    .line 418
    :try_start_c
    const-string v2, "GmsBackupTransport"

    const-string v3, "Error sending partial backup to server: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 419
    :try_start_d
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 422
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    move v0, v1

    goto/16 :goto_0

    .line 421
    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 422
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    throw v0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 337
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized performFullBackup(Landroid/content/pm/PackageInfo;Landroid/os/ParcelFileDescriptor;)I
    .locals 13

    .prologue
    const/16 v7, -0x3e8

    const/16 v0, -0x3ea

    const/4 v8, 0x0

    .line 653
    monitor-enter p0

    :try_start_0
    iget-object v9, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 654
    const-string v1, "GmsBackupTransport"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Attempt to do full backup on "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 656
    iget-object v1, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-virtual {v1}, Lcom/google/android/gms/backup/BackupTransportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "enable_full_backup"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    .line 658
    if-nez v1, :cond_1

    .line 659
    const-string v0, "GmsBackupTransport"

    const-string v1, "Full backup feature is disabled by gservice."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v7

    .line 808
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 663
    :cond_1
    const/4 v1, 0x1

    const/4 v2, 0x1

    :try_start_1
    new-array v2, v2, [Landroid/content/pm/PackageInfo;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/backup/z;->a(Z[Landroid/content/pm/PackageInfo;)[Ljava/lang/String;

    move-result-object v1

    .line 664
    array-length v1, v1

    if-eqz v1, :cond_0

    .line 668
    iget-object v1, p0, Lcom/google/android/gms/backup/z;->j:Landroid/os/ParcelFileDescriptor;

    if-eqz v1, :cond_2

    .line 669
    const-string v1, "GmsBackupTransport"

    const-string v2, "Attempt to initiate full backup while one is in progress"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 653
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 673
    :cond_2
    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    iget-object v0, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v0, :cond_7

    iget-object v0, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    :goto_1
    invoke-static {v1, v0}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;I)I

    .line 676
    invoke-static {}, Landroid/net/TrafficStats;->setThreadStatsTagBackup()V

    .line 677
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->f(Lcom/google/android/gms/backup/BackupTransportService;)I

    move-result v0

    invoke-static {v0}, Landroid/net/TrafficStats;->setThreadStatsUid(I)V

    .line 679
    invoke-static {}, Lcom/google/android/gms/backup/BackupTransportService;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "GmsBackupTransport"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "performFullBackup : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    :cond_3
    iput-object p2, p0, Lcom/google/android/gms/backup/z;->j:Landroid/os/ParcelFileDescriptor;

    .line 683
    const-string v0, "SHA-1"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->b(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 684
    if-nez v1, :cond_4

    .line 685
    const-string v0, "GmsBackupTransport"

    const-string v2, "No such message digest algorithm found : SHA-1"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    :cond_4
    new-instance v10, Lcom/google/android/gms/backup/b/e;

    new-instance v0, Ljava/io/FileInputStream;

    iget-object v2, p0, Lcom/google/android/gms/backup/z;->j:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    new-instance v2, Lcom/google/android/gms/drive/b/a/j;

    sget-object v3, Lcom/google/android/gms/backup/b/b;->a:Lcom/google/android/gms/drive/b/a/n;

    const/16 v4, 0x400

    invoke-interface {v3, v4}, Lcom/google/android/gms/drive/b/a/n;->a(I)Lcom/google/android/gms/drive/b/a/m;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/gms/drive/b/a/j;-><init>(Lcom/google/android/gms/drive/b/a/m;)V

    invoke-direct {v10, v0, v2, v1}, Lcom/google/android/gms/backup/b/e;-><init>(Ljava/io/InputStream;Lcom/google/android/gms/drive/b/a/j;Ljava/security/MessageDigest;)V

    .line 693
    new-instance v0, Lcom/google/android/gms/backup/af;

    invoke-direct {v0, v10}, Lcom/google/android/gms/backup/af;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->k:Lcom/google/android/gms/backup/af;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 696
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    iget-object v2, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v2}, Lcom/google/android/gms/backup/BackupTransportService;->b(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v2

    invoke-static {v0, p1, v2}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;Landroid/content/pm/PackageInfo;Lcom/android/a/a;)Lcom/google/android/gms/backup/i;

    move-result-object v5

    .line 699
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->n:Lcom/google/ae/a/z;

    if-nez v0, :cond_5

    .line 700
    new-instance v0, Lcom/google/ae/a/e;

    invoke-direct {v0}, Lcom/google/ae/a/e;-><init>()V

    .line 701
    new-instance v2, Lcom/google/ae/a/r;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/google/ae/a/r;-><init>(Lcom/google/ae/a/b;B)V

    new-instance v0, Lcom/google/ae/a/q;

    iget-object v2, v2, Lcom/google/ae/a/r;->a:Lcom/google/ae/a/b;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/google/ae/a/q;-><init>(Lcom/google/ae/a/b;B)V

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->n:Lcom/google/ae/a/z;

    .line 704
    :cond_5
    new-instance v3, Lcom/google/ae/a/c;

    invoke-direct {v3}, Lcom/google/ae/a/c;-><init>()V

    .line 705
    iget-object v11, v5, Lcom/google/android/gms/backup/i;->d:Ljava/lang/String;

    .line 706
    const-string v0, "authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "GoogleLogin auth="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Lcom/google/ae/a/c;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/ae/a/c;

    .line 707
    const-string v0, "Content-Type"

    const-string v2, "application/octet-stream"

    invoke-virtual {v3, v0, v2}, Lcom/google/ae/a/c;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/ae/a/c;

    .line 709
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0, v9}, Lcom/google/android/gms/backup/b/b;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v2

    .line 711
    iget-object v4, p0, Lcom/google/android/gms/backup/z;->k:Lcom/google/android/gms/backup/af;

    .line 712
    if-eqz v2, :cond_6

    .line 713
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/drive/b/a/i;

    new-instance v4, Lcom/google/android/gms/backup/b/a;

    iget-object v6, p0, Lcom/google/android/gms/backup/z;->k:Lcom/google/android/gms/backup/af;

    invoke-direct {v4, v6}, Lcom/google/android/gms/backup/b/a;-><init>(Lcom/google/ae/a/a;)V

    invoke-static {v0, v4}, Lcom/google/android/gms/backup/b/b;->a(Lcom/google/android/gms/drive/b/a/i;Ljava/io/InputStream;)Lcom/google/ae/a/a;

    move-result-object v4

    .line 716
    const-string v6, "X-goog-diff-content-encoding"

    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v6, v0}, Lcom/google/ae/a/c;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/ae/a/c;

    .line 719
    :cond_6
    new-instance v0, Lcom/google/ae/a/y;

    invoke-direct {v0}, Lcom/google/ae/a/y;-><init>()V

    iput-object v1, v0, Lcom/google/ae/a/y;->c:Ljava/security/MessageDigest;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/ae/a/y;->d:Z

    new-instance v6, Lcom/google/ae/a/x;

    const/4 v1, 0x0

    invoke-direct {v6, v0, v1}, Lcom/google/ae/a/x;-><init>(Lcom/google/ae/a/y;B)V

    .line 721
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->n:Lcom/google/ae/a/z;

    iget-object v1, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-virtual {v1}, Lcom/google/android/gms/backup/BackupTransportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "scotty_server_upload_url"

    const-string v12, "https://android.clients.google.com/backup/upload"

    invoke-static {v1, v2, v12}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "PUT"

    invoke-virtual {v5}, Lcom/google/android/gms/backup/i;->g()[B

    move-result-object v5

    const/4 v12, 0x2

    invoke-static {v5, v12}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v5

    invoke-interface/range {v0 .. v6}, Lcom/google/ae/a/z;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/ae/a/c;Lcom/google/ae/a/a;Ljava/lang/String;Lcom/google/ae/a/x;)Lcom/google/ae/a/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->i:Lcom/google/ae/a/s;

    .line 729
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->i:Lcom/google/ae/a/s;

    new-instance v1, Lcom/google/android/gms/backup/aa;

    invoke-direct {v1, p0, v11, v10, v9}, Lcom/google/android/gms/backup/aa;-><init>(Lcom/google/android/gms/backup/z;Ljava/lang/String;Lcom/google/android/gms/backup/b/e;Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/google/ae/a/s;->a(Lcom/google/ae/a/w;I)V

    .line 790
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->i:Lcom/google/ae/a/s;

    invoke-interface {v0}, Lcom/google/ae/a/s;->a()Ljava/util/concurrent/Future;

    .line 792
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->o:Ljava/lang/Boolean;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 793
    :try_start_4
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 808
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v0, v8

    goto/16 :goto_0

    .line 673
    :cond_7
    const/16 v0, 0x3e8

    goto/16 :goto_1

    .line 795
    :catch_0
    move-exception v0

    .line 796
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 797
    const-string v1, "GmsBackupTransport"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 799
    :cond_8
    const-string v0, "GmsBackupTransport"

    const-string v1, "Exception when generating full backup request."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 800
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->k:Lcom/google/android/gms/backup/af;

    .line 801
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/backup/z;->j:Landroid/os/ParcelFileDescriptor;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 805
    :try_start_6
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 808
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    move v0, v7

    goto/16 :goto_0

    .line 807
    :catchall_1
    move-exception v0

    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 808
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method public final declared-synchronized requestBackupTime()J
    .locals 8

    .prologue
    const-wide/32 v0, 0x240c8400

    const-wide/16 v6, 0x0

    .line 293
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v2}, Lcom/google/android/gms/backup/BackupTransportService;->b(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v2

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/android/a/a;->a(J)V

    .line 297
    iget-object v2, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v2}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;)Landroid/accounts/Account;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 298
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->b(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v1}, Lcom/google/android/gms/backup/BackupTransportService;->c(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/a/a;->a(Lcom/android/a/b;)J

    move-result-wide v0

    .line 301
    const-wide/32 v2, 0x240c8400

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v0, v4

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 303
    cmp-long v2, v0, v6

    if-lez v2, :cond_0

    .line 304
    const-string v2, "GmsBackupTransport"

    const-string v3, "Next backup will happen in %d millis."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    :cond_0
    const-wide/16 v2, 0x0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 309
    :cond_1
    monitor-exit p0

    return-wide v0

    .line 293
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized requestFullBackupTime()J
    .locals 6

    .prologue
    .line 637
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->b(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/android/a/a;->a(J)V

    .line 641
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/BackupTransportService;)Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 642
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v0}, Lcom/google/android/gms/backup/BackupTransportService;->b(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/backup/z;->a:Lcom/google/android/gms/backup/BackupTransportService;

    invoke-static {v1}, Lcom/google/android/gms/backup/BackupTransportService;->c(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/a/a;->a(Lcom/android/a/b;)J

    move-result-wide v0

    .line 643
    const-wide/16 v2, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v0, v4

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 646
    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    const-wide/32 v0, 0x240c8400

    goto :goto_0

    .line 637
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized sendBackupData(I)I
    .locals 1

    .prologue
    .line 814
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/backup/z;->k:Lcom/google/android/gms/backup/af;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/backup/z;->k:Lcom/google/android/gms/backup/af;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/backup/af;->a(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 816
    const/4 v0, 0x0

    .line 818
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/16 v0, -0x3ea

    goto :goto_0

    .line 814
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized startRestore(J[Landroid/content/pm/PackageInfo;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 532
    monitor-enter p0

    const/4 v1, 0x0

    :try_start_0
    iput-object v1, p0, Lcom/google/android/gms/backup/z;->d:Lcom/google/android/gms/backup/ao;

    .line 533
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/backup/z;->e:Ljava/lang/String;

    .line 534
    iget-object v1, p0, Lcom/google/android/gms/backup/z;->g:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 535
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/backup/z;->f:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 538
    const/4 v1, 0x0

    :try_start_1
    invoke-direct {p0, v1, p3}, Lcom/google/android/gms/backup/z;->a(Z[Landroid/content/pm/PackageInfo;)[Ljava/lang/String;

    move-result-object v1

    .line 540
    new-instance v2, Lcom/google/android/gms/backup/ao;

    invoke-direct {v2, p1, p2, v1}, Lcom/google/android/gms/backup/ao;-><init>(J[Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/gms/backup/z;->d:Lcom/google/android/gms/backup/ao;

    .line 542
    invoke-static {}, Lcom/google/android/gms/backup/BackupTransportService;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 543
    const-string v2, "GmsBackupTransport"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "new restore session, "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v1, v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " apps"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/backup/z;->f()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 548
    :goto_0
    monitor-exit p0

    return v0

    .line 546
    :catch_0
    move-exception v0

    .line 547
    :try_start_2
    const-string v1, "GmsBackupTransport"

    const-string v2, "Error getting restore data from server: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 548
    const/16 v0, -0x3e8

    goto :goto_0

    .line 532
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final transportDirName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 270
    const-string v0, "com.google.android.gms.backup.BackupTransportService"

    return-object v0
.end method
