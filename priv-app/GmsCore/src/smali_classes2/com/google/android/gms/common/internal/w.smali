.class public final Lcom/google/android/gms/common/internal/w;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:I


# instance fields
.field private final b:Ljava/lang/Object;

.field private final c:Lcom/google/android/gms/common/internal/a/b;

.field private final d:J

.field private final e:J

.field private f:Ljava/util/HashMap;

.field private g:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const/4 v0, -0x1

    sput v0, Lcom/google/android/gms/common/internal/w;->a:I

    return-void
.end method

.method public constructor <init>(JLjava/util/concurrent/TimeUnit;)V
    .locals 5

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/w;->b:Ljava/lang/Object;

    .line 36
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/common/internal/w;->d:J

    .line 37
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3, p3}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/common/internal/w;->e:J

    .line 38
    invoke-direct {p0}, Lcom/google/android/gms/common/internal/w;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/w;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "ExpirableLruCache has both access and write expiration negative"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 40
    new-instance v0, Lcom/google/android/gms/common/internal/x;

    const/16 v1, 0x2710

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/common/internal/x;-><init>(Lcom/google/android/gms/common/internal/w;I)V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/w;->c:Lcom/google/android/gms/common/internal/a/b;

    .line 64
    invoke-direct {p0}, Lcom/google/android/gms/common/internal/w;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/w;->f:Ljava/util/HashMap;

    .line 67
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/common/internal/w;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/w;->g:Ljava/util/HashMap;

    .line 70
    :cond_2
    return-void

    .line 38
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/common/internal/w;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lcom/google/android/gms/common/internal/w;->b:Ljava/lang/Object;

    return-object v0
.end method

.method private a()Z
    .locals 4

    .prologue
    .line 181
    iget-wide v0, p0, Lcom/google/android/gms/common/internal/w;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Z
    .locals 4

    .prologue
    .line 185
    iget-wide v0, p0, Lcom/google/android/gms/common/internal/w;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/common/internal/w;)Z
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/google/android/gms/common/internal/w;->a()Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/common/internal/w;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lcom/google/android/gms/common/internal/w;->f:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/common/internal/w;)Z
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/google/android/gms/common/internal/w;->b()Z

    move-result v0

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/common/internal/w;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lcom/google/android/gms/common/internal/w;->g:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 143
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/w;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/common/internal/w;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/common/internal/w;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long v4, v2, v4

    iget-wide v6, p0, Lcom/google/android/gms/common/internal/w;->d:J

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/common/internal/w;->c:Lcom/google/android/gms/common/internal/a/b;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/a/b;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/w;->c:Lcom/google/android/gms/common/internal/a/b;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 147
    if-eqz v0, :cond_1

    iget-wide v2, p0, Lcom/google/android/gms/common/internal/w;->d:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 148
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    .line 149
    iget-object v1, p0, Lcom/google/android/gms/common/internal/w;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 150
    :try_start_0
    iget-object v4, p0, Lcom/google/android/gms/common/internal/w;->f:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v4, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    :cond_1
    return-object v0

    .line 143
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/common/internal/w;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/common/internal/w;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/common/internal/w;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    iget-wide v4, p0, Lcom/google/android/gms/common/internal/w;->e:J

    cmp-long v0, v2, v4

    if-lez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 151
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/google/android/gms/common/internal/w;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 129
    iget-object v2, p0, Lcom/google/android/gms/common/internal/w;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 130
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/common/internal/w;->g:Ljava/util/HashMap;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/w;->c:Lcom/google/android/gms/common/internal/a/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/common/internal/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method
