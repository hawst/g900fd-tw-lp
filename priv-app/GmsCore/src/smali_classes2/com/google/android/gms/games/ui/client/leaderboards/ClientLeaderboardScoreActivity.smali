.class public final Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreActivity;
.super Lcom/google/android/gms/games/ui/client/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/common/leaderboards/j;


# static fields
.field private static final n:I

.field private static final o:I


# instance fields
.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    sget v0, Lcom/google/android/gms/l;->bh:I

    sput v0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreActivity;->n:I

    .line 37
    sget v0, Lcom/google/android/gms/m;->m:I

    sput v0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreActivity;->o:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 44
    sget v0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreActivity;->n:I

    sget v1, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreActivity;->o:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/client/a;-><init>(II)V

    .line 45
    return-void
.end method


# virtual methods
.method protected final N()I
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x3

    return v0
.end method

.method protected final O()Z
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x1

    return v0
.end method

.method public final T()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreActivity;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final U()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreActivity;->Q()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final V()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreActivity;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final W()I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreActivity;->r:I

    return v0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 61
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->b_(Landroid/os/Bundle;)V

    .line 62
    sget-object v0, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/k;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/k;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/Game;

    move-result-object v0

    .line 63
    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->s_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreActivity;->q:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 49
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->onCreate(Landroid/os/Bundle;)V

    .line 53
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreActivity;->l:Z

    .line 56
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.LEADERBOARD_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreActivity;->p:Ljava/lang/String;

    const-string v1, "com.google.android.gms.games.LEADERBOARD_TIME_SPAN"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreActivity;->r:I

    iget v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreActivity;->r:I

    if-eq v0, v3, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreActivity;->r:I

    invoke-static {v0}, Lcom/google/android/gms/games/internal/b/i;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ClientLeaderboardScore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid timespan "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreActivity;->r:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    iput v3, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreActivity;->r:I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreActivity;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "ClientLeaderboardScore"

    const-string v1, "EXTRA_LEADERBOARD_ID extra missing; bailing out..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreActivity;->finish()V

    .line 57
    :cond_1
    return-void
.end method
