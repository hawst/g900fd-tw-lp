.class final Lcom/google/android/gms/games/internal/em;
.super Lcom/google/android/gms/games/internal/ek;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xc
.end annotation


# instance fields
.field private c:Ljava/lang/ref/WeakReference;

.field private d:Z


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/games/internal/c;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 88
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/internal/ek;-><init>(Lcom/google/android/gms/games/internal/c;IB)V

    .line 85
    iput-boolean v0, p0, Lcom/google/android/gms/games/internal/em;->d:Z

    .line 89
    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v0, -0x1

    const/4 v6, 0x0

    .line 194
    .line 195
    const/16 v1, 0x11

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 196
    invoke-virtual {p1}, Landroid/view/View;->getDisplay()Landroid/view/Display;

    move-result-object v1

    .line 197
    if-eqz v1, :cond_0

    .line 198
    invoke-virtual {v1}, Landroid/view/Display;->getDisplayId()I

    move-result v0

    .line 203
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    .line 204
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 205
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationInWindow([I)V

    .line 206
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 207
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    .line 209
    iget-object v5, p0, Lcom/google/android/gms/games/internal/em;->b:Lcom/google/android/gms/games/internal/el;

    iput v0, v5, Lcom/google/android/gms/games/internal/el;->c:I

    .line 210
    iget-object v0, p0, Lcom/google/android/gms/games/internal/em;->b:Lcom/google/android/gms/games/internal/el;

    iput-object v1, v0, Lcom/google/android/gms/games/internal/el;->a:Landroid/os/IBinder;

    .line 211
    iget-object v0, p0, Lcom/google/android/gms/games/internal/em;->b:Lcom/google/android/gms/games/internal/el;

    aget v1, v2, v6

    iput v1, v0, Lcom/google/android/gms/games/internal/el;->d:I

    .line 212
    iget-object v0, p0, Lcom/google/android/gms/games/internal/em;->b:Lcom/google/android/gms/games/internal/el;

    aget v1, v2, v7

    iput v1, v0, Lcom/google/android/gms/games/internal/el;->e:I

    .line 213
    iget-object v0, p0, Lcom/google/android/gms/games/internal/em;->b:Lcom/google/android/gms/games/internal/el;

    aget v1, v2, v6

    add-int/2addr v1, v3

    iput v1, v0, Lcom/google/android/gms/games/internal/el;->f:I

    .line 214
    iget-object v0, p0, Lcom/google/android/gms/games/internal/em;->b:Lcom/google/android/gms/games/internal/el;

    aget v1, v2, v7

    add-int/2addr v1, v4

    iput v1, v0, Lcom/google/android/gms/games/internal/el;->g:I

    .line 216
    iget-boolean v0, p0, Lcom/google/android/gms/games/internal/em;->d:Z

    if-eqz v0, :cond_1

    .line 217
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/em;->a()V

    .line 218
    iput-boolean v6, p0, Lcom/google/android/gms/games/internal/em;->d:Z

    .line 220
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/gms/games/internal/em;->b:Lcom/google/android/gms/games/internal/el;

    iget-object v0, v0, Lcom/google/android/gms/games/internal/el;->a:Landroid/os/IBinder;

    if-eqz v0, :cond_0

    .line 157
    invoke-super {p0}, Lcom/google/android/gms/games/internal/ek;->a()V

    .line 165
    :goto_0
    return-void

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/internal/em;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/gms/games/internal/em;->d:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected final a(I)V
    .locals 3

    .prologue
    .line 95
    new-instance v0, Lcom/google/android/gms/games/internal/el;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/games/internal/el;-><init>(ILandroid/os/IBinder;B)V

    iput-object v0, p0, Lcom/google/android/gms/games/internal/em;->b:Lcom/google/android/gms/games/internal/el;

    .line 96
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/gms/games/internal/em;->a:Lcom/google/android/gms/games/internal/c;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->z()V

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/games/internal/em;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/internal/em;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 107
    iget-object v1, p0, Lcom/google/android/gms/games/internal/em;->a:Lcom/google/android/gms/games/internal/c;

    invoke-virtual {v1}, Lcom/google/android/gms/games/internal/c;->h()Landroid/content/Context;

    move-result-object v1

    .line 108
    if-nez v0, :cond_0

    instance-of v2, v1, Landroid/app/Activity;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 109
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 112
    :cond_0
    if-eqz v0, :cond_1

    .line 113
    invoke-virtual {v0, p0}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 114
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 115
    const/16 v1, 0x10

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 116
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 122
    :cond_1
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/internal/em;->c:Ljava/lang/ref/WeakReference;

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/games/internal/em;->a:Lcom/google/android/gms/games/internal/c;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->h()Landroid/content/Context;

    move-result-object v1

    .line 125
    if-nez p1, :cond_3

    instance-of v0, v1, Landroid/app/Activity;

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 126
    check-cast v0, Landroid/app/Activity;

    .line 128
    const v2, 0x1020002

    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 129
    if-nez v0, :cond_2

    .line 130
    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 133
    :cond_2
    const-string v1, "PopupManager"

    const-string v2, "You have not specified a View to use as content view for popups. Falling back to the Activity content view which may not work properly in future versions of the API. Use setViewForPopups() to set your content view."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v0

    .line 139
    :cond_3
    if-eqz p1, :cond_5

    .line 140
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/internal/em;->b(Landroid/view/View;)V

    .line 142
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/games/internal/em;->c:Ljava/lang/ref/WeakReference;

    .line 143
    invoke-virtual {p1, p0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 144
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 145
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 151
    :goto_1
    return-void

    .line 118
    :cond_4
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    .line 147
    :cond_5
    const-string v0, "PopupManager"

    const-string v1, "No content view usable to display popups. Popups will not be displayed in response to this client\'s calls. Use setViewForPopups() to set your content view."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final onGlobalLayout()V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/gms/games/internal/em;->c:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/internal/em;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 185
    if-eqz v0, :cond_0

    .line 189
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/internal/em;->b(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 169
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/internal/em;->b(Landroid/view/View;)V

    .line 170
    return-void
.end method

.method public final onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/gms/games/internal/em;->a:Lcom/google/android/gms/games/internal/c;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->z()V

    .line 175
    invoke-virtual {p1, p0}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 176
    return-void
.end method
