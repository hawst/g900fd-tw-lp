.class public final Lcom/google/android/gms/games/ui/signin/a;
.super Lcom/google/android/gms/games/ui/signin/g;
.source "SourceFile"


# static fields
.field private static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 20
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "com.google"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/games/ui/signin/a;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/g;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x2

    return v0
.end method

.method public final a(Lcom/google/android/gms/games/i/b;)V
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v8, 0x0

    const/4 v0, 0x0

    .line 59
    sget-object v2, Lcom/google/android/gms/games/ui/signin/a;->b:[Ljava/lang/String;

    move-object v1, v0

    move-object v4, v0

    move-object v5, v0

    move-object v6, v0

    move-object v7, v0

    move v9, v8

    move v10, v3

    invoke-static/range {v0 .. v10}, Lcom/google/android/gms/common/a;->a(Landroid/accounts/Account;Ljava/util/ArrayList;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;ZII)Landroid/content/Intent;

    move-result-object v0

    .line 65
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 66
    const-string v1, "overrideTheme"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 69
    invoke-virtual {p0, v0, v8}, Lcom/google/android/gms/games/ui/signin/a;->startActivityForResult(Landroid/content/Intent;I)V

    .line 70
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x3

    return v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/signin/g;->onActivityResult(IILandroid/content/Intent;)V

    .line 33
    if-nez p1, :cond_1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 34
    const-string v0, "authAccount"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 36
    if-eqz v0, :cond_0

    .line 37
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/a;->a(Ljava/lang/String;)V

    .line 38
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/a;->b(I)V

    .line 45
    :goto_0
    return-void

    .line 40
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/signin/a;->d(I)V

    goto :goto_0

    .line 43
    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/signin/a;->d(I)V

    goto :goto_0
.end method

.method public final onResume()V
    .locals 0

    .prologue
    .line 26
    invoke-super {p0}, Lcom/google/android/gms/games/ui/signin/g;->onResume()V

    .line 27
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/a;->h()V

    .line 28
    return-void
.end method
