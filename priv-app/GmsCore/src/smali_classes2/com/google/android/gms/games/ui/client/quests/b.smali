.class public final Lcom/google/android/gms/games/ui/client/quests/b;
.super Lcom/google/android/gms/games/ui/common/quests/i;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/games/ui/client/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/client/a;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/quests/i;-><init>()V

    .line 38
    invoke-static {p1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 39
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/quests/b;->a:Lcom/google/android/gms/games/ui/client/a;

    .line 40
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/quest/Quest;)V
    .locals 4

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/quests/b;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/quests/b;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 47
    const-string v0, "ClientQuestHelper"

    const-string v1, "onPlayQuestClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    invoke-interface {p1}, Lcom/google/android/gms/games/quest/Quest;->m()I

    move-result v1

    .line 53
    packed-switch v1, :pswitch_data_0

    .line 76
    :pswitch_0
    const-string v0, "ClientQuestHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onPlayQuestClicked: unexpected quest state: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 55
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/quests/b;->a:Lcom/google/android/gms/games/ui/client/a;

    sget v2, Lcom/google/android/gms/p;->kt:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/client/a;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 57
    invoke-static {v1}, Lcom/google/android/gms/games/ui/b/f;->a(Ljava/lang/String;)Lcom/google/android/gms/games/ui/b/f;

    move-result-object v1

    .line 58
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/quests/b;->a:Lcom/google/android/gms/games/ui/client/a;

    const-string v3, "com.google.android.gms.games.ui.dialog.progressDialogAcceptingQuest"

    invoke-static {v2, v1, v3}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    .line 61
    sget-object v1, Lcom/google/android/gms/games/d;->q:Lcom/google/android/gms/games/quest/e;

    invoke-interface {p1}, Lcom/google/android/gms/games/quest/Quest;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/quest/e;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/client/quests/c;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/client/quests/c;-><init>(Lcom/google/android/gms/games/ui/client/quests/b;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0

    .line 70
    :pswitch_2
    const/4 v0, 0x3

    invoke-interface {p1}, Lcom/google/android/gms/games/quest/Quest;->j()Lcom/google/android/gms/games/quest/Milestone;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/quest/Milestone;->f()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 72
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/client/quests/b;->b(Lcom/google/android/gms/games/quest/Quest;)V

    goto :goto_0

    .line 53
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method final b(Lcom/google/android/gms/games/quest/Quest;)V
    .locals 3

    .prologue
    .line 102
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 103
    const-string v2, "quest"

    invoke-interface {p1}, Lcom/google/android/gms/games/quest/Quest;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/quests/b;->a:Lcom/google/android/gms/games/ui/client/a;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/games/ui/client/a;->setResult(ILandroid/content/Intent;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/quests/b;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/a;->finish()V

    .line 106
    return-void
.end method
