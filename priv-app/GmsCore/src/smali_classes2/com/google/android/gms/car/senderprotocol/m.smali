.class final Lcom/google/android/gms/car/senderprotocol/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/senderprotocol/af;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/senderprotocol/k;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/car/senderprotocol/k;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/m;->a:Lcom/google/android/gms/car/senderprotocol/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/car/senderprotocol/k;B)V
    .locals 0

    .prologue
    .line 118
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/senderprotocol/m;-><init>(Lcom/google/android/gms/car/senderprotocol/k;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/m;->a:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/k;->v:Lcom/google/android/gms/car/eb;

    invoke-interface {v0}, Lcom/google/android/gms/car/eb;->a()V

    .line 122
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/google/android/gms/car/senderprotocol/m;->a()V

    .line 128
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 133
    const-string v0, "CAR.GAL"

    const-string v1, "Framing Error encountered."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/m;->a:Lcom/google/android/gms/car/senderprotocol/k;

    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/c/a/a;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v3, v1, v3, v3}, Lcom/google/android/gms/car/senderprotocol/k;->a(ILjava/nio/ByteBuffer;ZZ)V

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/m;->a:Lcom/google/android/gms/car/senderprotocol/k;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/senderprotocol/k;->a(Z)V

    .line 138
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/m;->a:Lcom/google/android/gms/car/senderprotocol/k;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/k;->v:Lcom/google/android/gms/car/eb;

    invoke-interface {v0}, Lcom/google/android/gms/car/eb;->b()V

    .line 148
    return-void
.end method
