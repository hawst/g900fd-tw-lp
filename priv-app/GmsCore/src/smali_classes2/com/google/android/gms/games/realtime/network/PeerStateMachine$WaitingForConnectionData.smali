.class final Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;
.super Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

.field private final f:I

.field private final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;ILjava/lang/String;ILcom/google/android/gms/games/service/statemachine/roomclient/ch;)V
    .locals 6

    .prologue
    .line 471
    iput-object p1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move v4, p5

    move-object v5, p6

    .line 472
    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;-><init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/games/service/statemachine/roomclient/ch;)V

    .line 473
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;->f:I

    .line 474
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;->g:Ljava/lang/String;

    .line 475
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ch;->a(I)V

    .line 476
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 8

    .prologue
    .line 480
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 502
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 482
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/jingle/i;

    .line 483
    iget-boolean v1, v0, Lcom/google/android/gms/games/jingle/i;->b:Z

    if-eqz v1, :cond_0

    .line 484
    new-instance v5, Lcom/google/android/gms/games/realtime/a;

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;->b:Ljava/lang/String;

    iget v0, v0, Lcom/google/android/gms/games/jingle/i;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {v5, v1, v0, v2}, Lcom/google/android/gms/games/realtime/a;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 487
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->b:Lcom/google/android/gms/games/realtime/network/a;

    invoke-interface {v0, v5}, Lcom/google/android/gms/games/realtime/network/a;->b(Lcom/google/android/gms/games/realtime/a;)V

    .line 488
    iget-object v7, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    new-instance v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;->g:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;->c:I

    iget-object v6, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;-><init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/games/realtime/a;Lcom/google/android/gms/games/service/statemachine/roomclient/ch;)V

    iput-object v0, v7, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->d:Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

    .line 498
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 491
    :cond_0
    new-instance v1, Lcom/google/android/gms/games/realtime/a;

    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;->b:Ljava/lang/String;

    iget v0, v0, Lcom/google/android/gms/games/jingle/i;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v3, "P2P_FAILED"

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/gms/games/realtime/a;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 495
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->b:Lcom/google/android/gms/games/realtime/network/a;

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/realtime/network/a;->a(Lcom/google/android/gms/games/realtime/a;)V

    .line 496
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    new-instance v1, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;

    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v3, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;-><init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;Lcom/google/android/gms/games/service/statemachine/roomclient/ch;)V

    iput-object v1, v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->d:Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

    goto :goto_1

    .line 480
    nop

    :pswitch_data_0
    .packed-switch 0x12d
        :pswitch_0
    .end packed-switch
.end method
