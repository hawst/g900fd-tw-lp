.class final Lcom/google/android/gms/auth/trustagent/m;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/m;->a:Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 98
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 99
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 100
    const-string v0, "keyguard"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 102
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/m;->a:Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;

    invoke-static {v1}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->a(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "auth_trust_agent_pref_first_notification_shown_"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/m;->a:Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;

    invoke-static {v1}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->b(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;)Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->b:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;

    invoke-direct {v1, p1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 107
    const/high16 v0, 0x14000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/m;->a:Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->b(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;)Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->j:Ljava/util/Map;

    .line 111
    const-string v2, "Bluetooth"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 112
    const-string v2, "type"

    const-class v3, Lcom/google/android/gms/auth/trustagent/trustlet/d;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    const-string v2, "name"

    const-string v3, "Bluetooth"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 117
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/m;->a:Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->a(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "auth_trust_agent_pref_first_notification_shown_"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    const-string v2, "Place"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 122
    const-string v2, "type"

    const-class v3, Lcom/google/android/gms/auth/trustagent/trustlet/aw;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 124
    const-string v2, "name"

    const-string v3, "Place"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/m;->a:Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->a(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "auth_trust_agent_pref_first_notification_shown_"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 133
    :cond_2
    const-string v1, "android.intent.action.USER_BACKGROUND"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    const-string v0, "Coffee - GoogleTrustAgent"

    const-string v1, "User switched (ACTION_USER_BACKGROUND)"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/m;->a:Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->b(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;)Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    move-result-object v0

    iget-boolean v1, v0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->c:Z

    if-nez v1, :cond_0

    const-string v1, "Coffee - TrustletManager"

    const-string v2, "Revoking trust and requiring user authentication."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v4, v0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->c:Z

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->c()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
