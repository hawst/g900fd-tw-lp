.class public final Lcom/google/android/gms/car/ig;
.super Lcom/google/android/gms/car/ne;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/gw;
.implements Lcom/google/android/gms/car/senderprotocol/bd;


# instance fields
.field a:Ljava/lang/String;

.field b:[B

.field volatile c:Z

.field final d:Ljava/lang/Object;

.field e:Lcom/google/android/gms/car/ih;

.field private f:Lcom/google/android/gms/car/senderprotocol/bc;

.field private final g:Ljava/lang/Object;

.field private h:[Landroid/os/ParcelFileDescriptor;

.field private i:Ljava/io/InputStream;

.field private j:[B

.field private k:I

.field private final l:Ljava/lang/Object;

.field private m:[Landroid/os/ParcelFileDescriptor;

.field private n:Ljava/io/OutputStream;

.field private final o:Ljava/util/Queue;

.field private p:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Lcom/google/android/gms/car/ne;-><init>()V

    .line 33
    iput-boolean v1, p0, Lcom/google/android/gms/car/ig;->c:Z

    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ig;->d:Ljava/lang/Object;

    .line 41
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ig;->g:Ljava/lang/Object;

    .line 52
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ig;->l:Ljava/lang/Object;

    .line 57
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ig;->o:Ljava/util/Queue;

    .line 59
    iput v1, p0, Lcom/google/android/gms/car/ig;->p:I

    .line 433
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/ig;)V
    .locals 0

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/google/android/gms/car/ig;->b()V

    return-void
.end method

.method private a(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 400
    iget-object v1, p0, Lcom/google/android/gms/car/ig;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 401
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/car/ig;->e:Lcom/google/android/gms/car/ih;

    if-nez v2, :cond_1

    .line 402
    const-string v2, "CAR.VENDOR"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 403
    const-string v2, "CAR.VENDOR"

    const-string v3, "Data available but no client"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 414
    :goto_0
    return v0

    .line 408
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/car/ig;->e:Lcom/google/android/gms/car/ih;

    iget-object v2, v2, Lcom/google/android/gms/car/ih;->a:Lcom/google/android/gms/car/ng;

    invoke-interface {v2, p1}, Lcom/google/android/gms/car/ng;->a(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 413
    monitor-exit v1

    .line 414
    const/4 v0, 0x1

    goto :goto_0

    .line 410
    :catch_0
    move-exception v2

    :try_start_2
    const-string v2, "CAR.VENDOR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error in notifyDataToClient for vendor extension "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/car/ig;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 413
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Lcom/google/android/gms/car/ig;)V
    .locals 0

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/google/android/gms/car/ig;->c()V

    return-void
.end method

.method private g(Lcom/google/android/gms/car/ng;)V
    .locals 2

    .prologue
    .line 419
    iget-boolean v0, p0, Lcom/google/android/gms/car/ig;->c:Z

    if-nez v0, :cond_0

    .line 420
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotConnected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 421
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->f:Lcom/google/android/gms/car/senderprotocol/bc;

    if-nez v0, :cond_1

    .line 422
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "VendorExtensionEndpoint not set."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 425
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->e:Lcom/google/android/gms/car/ih;

    if-nez v0, :cond_2

    .line 426
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No client has been registered"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 428
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->e:Lcom/google/android/gms/car/ih;

    iget-object v0, v0, Lcom/google/android/gms/car/ih;->a:Lcom/google/android/gms/car/ng;

    invoke-interface {v0}, Lcom/google/android/gms/car/ng;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/car/ng;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    if-eq v0, v1, :cond_3

    .line 429
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This vendor extension service is already in use."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 431
    :cond_3
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/car/ng;I)V
    .locals 3

    .prologue
    .line 178
    const-string v0, "CAR.VENDOR"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    const-string v0, "CAR.VENDOR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "prepareSending. totalLength="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ig;->g(Lcom/google/android/gms/car/ng;)V

    .line 182
    iget-object v1, p0, Lcom/google/android/gms/car/ig;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 183
    :try_start_0
    new-array v0, p2, [B

    iput-object v0, p0, Lcom/google/android/gms/car/ig;->j:[B

    .line 184
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/ig;->k:I

    .line 185
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/car/senderprotocol/az;)V
    .locals 4

    .prologue
    .line 69
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/ig;->c:Z

    .line 70
    check-cast p1, Lcom/google/android/gms/car/senderprotocol/bc;

    iput-object p1, p0, Lcom/google/android/gms/car/ig;->f:Lcom/google/android/gms/car/senderprotocol/bc;

    .line 71
    iget-object v1, p0, Lcom/google/android/gms/car/ig;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 72
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->e:Lcom/google/android/gms/car/ih;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 74
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->e:Lcom/google/android/gms/car/ih;

    iget-object v0, v0, Lcom/google/android/gms/car/ih;->a:Lcom/google/android/gms/car/ng;

    invoke-interface {v0}, Lcom/google/android/gms/car/ng;->a()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v1

    return-void

    .line 76
    :catch_0
    move-exception v0

    const-string v0, "CAR.VENDOR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error in onEndPointReady for vendor extension "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/car/ig;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a([B)V
    .locals 3

    .prologue
    const/4 v1, 0x3

    .line 266
    const-string v0, "CAR.VENDOR"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    const-string v0, "CAR.VENDOR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onData. data length="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/ig;->l:Ljava/lang/Object;

    monitor-enter v1

    .line 270
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->o:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 271
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->o:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 272
    const-string v0, "CAR.VENDOR"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 273
    const-string v0, "CAR.VENDOR"

    const-string v2, "onData: No pending data. Notifying the client"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    :cond_1
    array-length v0, p1

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ig;->a(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 276
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->o:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 286
    :cond_2
    :goto_0
    monitor-exit v1

    return-void

    .line 282
    :cond_3
    const-string v0, "CAR.VENDOR"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 283
    const-string v0, "CAR.VENDOR"

    const-string v2, "onData: There are pending data"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 286
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/car/ng;)Z
    .locals 3

    .prologue
    .line 104
    const-string v0, "CAR.VENDOR"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    const-string v0, "CAR.VENDOR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "registerClient. client="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/gms/car/ng;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/ig;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 108
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->e:Lcom/google/android/gms/car/ih;

    if-eqz v0, :cond_1

    .line 109
    const/4 v0, 0x0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    :goto_0
    return v0

    .line 112
    :cond_1
    :try_start_1
    new-instance v0, Lcom/google/android/gms/car/ih;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/ih;-><init>(Lcom/google/android/gms/car/ig;Lcom/google/android/gms/car/ng;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ig;->e:Lcom/google/android/gms/car/ih;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 116
    const/4 v0, 0x1

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 113
    :catch_0
    move-exception v0

    .line 114
    :try_start_3
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method final b()V
    .locals 4

    .prologue
    .line 347
    iget-object v1, p0, Lcom/google/android/gms/car/ig;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 348
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->e:Lcom/google/android/gms/car/ih;

    if-nez v0, :cond_0

    .line 349
    const-string v0, "CAR.VENDOR"

    const-string v2, "removeClient: No registered client"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    monitor-exit v1

    .line 357
    :goto_0
    return-void

    .line 352
    :cond_0
    const-string v0, "CAR.VENDOR"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 353
    const-string v0, "CAR.VENDOR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "removeClient: Removing "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/car/ig;->e:Lcom/google/android/gms/car/ih;

    iget-object v3, v3, Lcom/google/android/gms/car/ih;->a:Lcom/google/android/gms/car/ng;

    invoke-interface {v3}, Lcom/google/android/gms/car/ng;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->e:Lcom/google/android/gms/car/ih;

    iget-object v0, v0, Lcom/google/android/gms/car/ih;->a:Lcom/google/android/gms/car/ng;

    invoke-interface {v0}, Lcom/google/android/gms/car/ng;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/car/ig;->e:Lcom/google/android/gms/car/ih;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 356
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/ig;->e:Lcom/google/android/gms/car/ih;

    .line 357
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Lcom/google/android/gms/car/ng;)V
    .locals 3

    .prologue
    .line 122
    const-string v0, "CAR.VENDOR"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    const-string v0, "CAR.VENDOR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unregisterClient. client="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/gms/car/ng;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ig;->g(Lcom/google/android/gms/car/ng;)V

    .line 126
    invoke-virtual {p0}, Lcom/google/android/gms/car/ig;->b()V

    .line 127
    invoke-virtual {p0}, Lcom/google/android/gms/car/ig;->c()V

    .line 128
    return-void
.end method

.method public final b(Lcom/google/android/gms/car/ng;I)V
    .locals 6

    .prologue
    const/4 v1, 0x3

    .line 190
    const-string v0, "CAR.VENDOR"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    const-string v0, "CAR.VENDOR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "notifyDataChunkWritten. chunkLength="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ig;->g(Lcom/google/android/gms/car/ng;)V

    .line 196
    iget-object v1, p0, Lcom/google/android/gms/car/ig;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 197
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/car/ig;->k:I

    add-int/2addr v0, p2

    iget-object v2, p0, Lcom/google/android/gms/car/ig;->j:[B

    array-length v2, v2

    if-le v0, v2, :cond_1

    .line 198
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    move v0, p2

    .line 202
    :goto_0
    if-lez v0, :cond_4

    .line 203
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/car/ig;->i:Ljava/io/InputStream;

    iget-object v3, p0, Lcom/google/android/gms/car/ig;->j:[B

    iget v4, p0, Lcom/google/android/gms/car/ig;->k:I

    invoke-virtual {v2, v3, v4, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 207
    const-string v3, "CAR.VENDOR"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 208
    const-string v3, "CAR.VENDOR"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "notifyDataChunkWritten read "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " bytes"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 213
    :cond_2
    const/4 v3, -0x1

    if-ne v2, v3, :cond_3

    .line 214
    :try_start_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Unexpected EOF"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 210
    :catch_0
    move-exception v0

    .line 211
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 216
    :cond_3
    sub-int/2addr v0, v2

    .line 217
    iget v3, p0, Lcom/google/android/gms/car/ig;->k:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/gms/car/ig;->k:I

    goto :goto_0

    .line 219
    :cond_4
    const-string v0, "CAR.VENDOR"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 220
    const-string v0, "CAR.VENDOR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "notifyDataChunkWritten finished reading the chunk of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bytes"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    :cond_5
    iget v0, p0, Lcom/google/android/gms/car/ig;->k:I

    iget-object v2, p0, Lcom/google/android/gms/car/ig;->j:[B

    array-length v2, v2

    if-ne v0, v2, :cond_7

    .line 225
    const-string v0, "CAR.VENDOR"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 226
    const-string v0, "CAR.VENDOR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "notifyDataChunkWritten read all the data of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/car/ig;->k:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bytes"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->f:Lcom/google/android/gms/car/senderprotocol/bc;

    iget-object v2, p0, Lcom/google/android/gms/car/ig;->j:[B

    iget-object v3, p0, Lcom/google/android/gms/car/ig;->j:[B

    array-length v3, v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/car/senderprotocol/bc;->a([BI)V

    .line 230
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/ig;->j:[B

    .line 232
    :cond_7
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void
.end method

.method public final c(Lcom/google/android/gms/car/ng;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ig;->g(Lcom/google/android/gms/car/ng;)V

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->a:Ljava/lang/String;

    return-object v0
.end method

.method final c()V
    .locals 3

    .prologue
    const/4 v1, 0x3

    .line 361
    const-string v0, "CAR.VENDOR"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    const-string v0, "CAR.VENDOR"

    const-string v1, "cleanupStreamsAndFds"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/ig;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 366
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->i:Ljava/io/InputStream;

    if-eqz v0, :cond_2

    .line 367
    const-string v0, "CAR.VENDOR"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 368
    const-string v0, "CAR.VENDOR"

    const-string v2, "Closing and resetting the stream for input from client"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 371
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->i:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 375
    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/google/android/gms/car/ig;->i:Ljava/io/InputStream;

    .line 377
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/ig;->h:[Landroid/os/ParcelFileDescriptor;

    .line 378
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 380
    iget-object v1, p0, Lcom/google/android/gms/car/ig;->l:Ljava/lang/Object;

    monitor-enter v1

    .line 381
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->n:Ljava/io/OutputStream;

    if-eqz v0, :cond_4

    .line 382
    const-string v0, "CAR.VENDOR"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 383
    const-string v0, "CAR.VENDOR"

    const-string v2, "Closing and resetting the stream for output to client"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 386
    :cond_3
    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->n:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 390
    :goto_1
    const/4 v0, 0x0

    :try_start_5
    iput-object v0, p0, Lcom/google/android/gms/car/ig;->n:Ljava/io/OutputStream;

    .line 392
    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/ig;->m:[Landroid/os/ParcelFileDescriptor;

    .line 393
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    return-void

    .line 373
    :catch_0
    move-exception v0

    :try_start_6
    const-string v0, "CAR.VENDOR"

    const-string v2, "Failed to close input stream"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 378
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 388
    :catch_1
    move-exception v0

    :try_start_7
    const-string v0, "CAR.VENDOR"

    const-string v2, "Failed to close output stream"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_1

    .line 393
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c(Lcom/google/android/gms/car/ng;I)V
    .locals 5

    .prologue
    const/4 v1, 0x3

    .line 291
    const-string v0, "CAR.VENDOR"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    const-string v0, "CAR.VENDOR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "notifyReadyToReadNextDataChunk. chunkLength="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ig;->g(Lcom/google/android/gms/car/ng;)V

    .line 297
    iget-object v1, p0, Lcom/google/android/gms/car/ig;->l:Ljava/lang/Object;

    monitor-enter v1

    .line 298
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->o:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 299
    if-nez v0, :cond_1

    .line 300
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "No data"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 343
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 303
    :cond_1
    :try_start_1
    iget v2, p0, Lcom/google/android/gms/car/ig;->p:I

    add-int/2addr v2, p2

    array-length v3, v0

    if-le v2, v3, :cond_2

    .line 304
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 308
    :cond_2
    :try_start_2
    const-string v2, "CAR.VENDOR"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 309
    const-string v2, "CAR.VENDOR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "notifyReadyToReadNextDataChunk writing. offset="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/google/android/gms/car/ig;->p:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " size="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/car/ig;->n:Ljava/io/OutputStream;

    iget v3, p0, Lcom/google/android/gms/car/ig;->p:I

    invoke-virtual {v2, v0, v3, p2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 316
    :try_start_3
    iget v2, p0, Lcom/google/android/gms/car/ig;->p:I

    add-int/2addr v2, p2

    iput v2, p0, Lcom/google/android/gms/car/ig;->p:I

    .line 318
    iget v2, p0, Lcom/google/android/gms/car/ig;->p:I

    array-length v0, v0

    if-ne v2, v0, :cond_6

    .line 319
    const-string v0, "CAR.VENDOR"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 320
    const-string v0, "CAR.VENDOR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "notifyReadyToReadNextDataChunk finished writing the data of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/car/ig;->p:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bytes"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->o:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    .line 325
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/ig;->p:I

    .line 327
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->o:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 328
    if-eqz v0, :cond_7

    .line 329
    const-string v2, "CAR.VENDOR"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 330
    const-string v2, "CAR.VENDOR"

    const-string v3, "notifyReadyToReadNextDataChunk: There are pending data. Notifying the client"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    :cond_5
    array-length v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ig;->a(I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 335
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->o:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 343
    :cond_6
    :goto_0
    monitor-exit v1

    return-void

    .line 313
    :catch_0
    move-exception v0

    .line 314
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 338
    :cond_7
    const-string v0, "CAR.VENDOR"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 339
    const-string v0, "CAR.VENDOR"

    const-string v2, "notifyReadyToReadNextDataChunk: No pending data"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/car/ng;)[B
    .locals 1

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ig;->g(Lcom/google/android/gms/car/ng;)V

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->b:[B

    return-object v0
.end method

.method public final e(Lcom/google/android/gms/car/ng;)Landroid/os/ParcelFileDescriptor;
    .locals 4

    .prologue
    const/4 v1, 0x3

    .line 149
    const-string v0, "CAR.VENDOR"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    const-string v0, "CAR.VENDOR"

    const-string v1, "getOutputFileDescriptor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ig;->g(Lcom/google/android/gms/car/ng;)V

    .line 155
    iget-object v1, p0, Lcom/google/android/gms/car/ig;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 156
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->h:[Landroid/os/ParcelFileDescriptor;

    if-nez v0, :cond_3

    .line 157
    const-string v0, "CAR.VENDOR"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    const-string v0, "CAR.VENDOR"

    const-string v2, "Creating new fds for input from client"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    :cond_1
    :try_start_1
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/ig;->h:[Landroid/os/ParcelFileDescriptor;

    .line 162
    new-instance v0, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;

    iget-object v2, p0, Lcom/google/android/gms/car/ig;->h:[Landroid/os/ParcelFileDescriptor;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-direct {v0, v2}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ig;->i:Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 172
    :cond_2
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->h:[Landroid/os/ParcelFileDescriptor;

    const/4 v2, 0x1

    aget-object v0, v0, v2

    monitor-exit v1

    return-object v0

    .line 165
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "cannot create pipe for input from client"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 173
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 168
    :cond_3
    :try_start_3
    const-string v0, "CAR.VENDOR"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 169
    const-string v0, "CAR.VENDOR"

    const-string v2, "Using the existing fds for input from client"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public final f(Lcom/google/android/gms/car/ng;)Landroid/os/ParcelFileDescriptor;
    .locals 4

    .prologue
    const/4 v1, 0x3

    .line 237
    const-string v0, "CAR.VENDOR"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    const-string v0, "CAR.VENDOR"

    const-string v1, "getInputFileDescriptor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ig;->g(Lcom/google/android/gms/car/ng;)V

    .line 243
    iget-object v1, p0, Lcom/google/android/gms/car/ig;->l:Ljava/lang/Object;

    monitor-enter v1

    .line 244
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->m:[Landroid/os/ParcelFileDescriptor;

    if-nez v0, :cond_3

    .line 245
    const-string v0, "CAR.VENDOR"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 246
    const-string v0, "CAR.VENDOR"

    const-string v2, "Creating new fds for output to client"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    :cond_1
    :try_start_1
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/ig;->m:[Landroid/os/ParcelFileDescriptor;

    .line 250
    new-instance v0, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    iget-object v2, p0, Lcom/google/android/gms/car/ig;->m:[Landroid/os/ParcelFileDescriptor;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-direct {v0, v2}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ig;->n:Ljava/io/OutputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 260
    :cond_2
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/car/ig;->m:[Landroid/os/ParcelFileDescriptor;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    monitor-exit v1

    return-object v0

    .line 253
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "cannot create pipe for output to client"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 261
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 256
    :cond_3
    :try_start_3
    const-string v0, "CAR.VENDOR"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 257
    const-string v0, "CAR.VENDOR"

    const-string v2, "Using the existing fds for output to client"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method
