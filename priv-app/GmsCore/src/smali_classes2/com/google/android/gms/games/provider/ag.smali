.class public final Lcom/google/android/gms/games/provider/ag;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/provider/BaseColumns;


# static fields
.field public static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1650
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "client_context_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "external_game_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "external_leaderboard_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "external_player_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "raw_score"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "achieved_timestamp"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "score_tag"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "signature"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/games/provider/ag;->a:[Ljava/lang/String;

    return-void
.end method

.method public static a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1628
    const-string v0, "leaderboard_pending_scores"

    invoke-static {p0, v0}, Lcom/google/android/gms/games/provider/ac;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 1637
    const-string v0, "leaderboard_pending_scores"

    invoke-static {p0, v0}, Lcom/google/android/gms/games/provider/ac;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
