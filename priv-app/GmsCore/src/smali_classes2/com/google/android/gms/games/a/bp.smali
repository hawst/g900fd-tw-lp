.class final Lcom/google/android/gms/games/a/bp;
.super Lcom/google/android/gms/games/a/bb;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/a/ax;


# static fields
.field private static final a:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;


# instance fields
.field private final d:Ljava/util/Random;

.field private e:J

.field private final f:Ljava/util/HashMap;

.field private final g:Lcom/google/android/gms/games/h/a/dx;

.field private final h:Lcom/google/android/gms/games/h/a/dw;

.field private final i:Lcom/google/android/gms/games/h/a/dr;

.field private final j:Lcom/google/android/gms/games/h/a/du;

.field private final k:Lcom/google/android/gms/games/a/aa;

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 87
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/bp;->a:Ljava/util/concurrent/locks/ReentrantLock;

    .line 101
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "quest_sync_token"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/gms/games/a/bp;->b:[Ljava/lang/String;

    .line 103
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "quest_sync_metadata_token"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/gms/games/a/bp;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/games/a/aa;)V
    .locals 4

    .prologue
    .line 155
    const-string v0, "QuestAgent"

    sget-object v1, Lcom/google/android/gms/games/a/bp;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    .line 114
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/games/a/bp;->e:J

    .line 150
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/a/bp;->l:Z

    .line 156
    new-instance v0, Lcom/google/android/gms/games/h/a/dw;

    invoke-direct {v0, p2}, Lcom/google/android/gms/games/h/a/dw;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bp;->h:Lcom/google/android/gms/games/h/a/dw;

    .line 157
    new-instance v0, Lcom/google/android/gms/games/h/a/dx;

    invoke-direct {v0, p3}, Lcom/google/android/gms/games/h/a/dx;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bp;->g:Lcom/google/android/gms/games/h/a/dx;

    .line 158
    new-instance v0, Lcom/google/android/gms/games/h/a/dr;

    invoke-direct {v0, p3}, Lcom/google/android/gms/games/h/a/dr;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bp;->i:Lcom/google/android/gms/games/h/a/dr;

    .line 159
    new-instance v0, Lcom/google/android/gms/games/h/a/du;

    invoke-direct {v0, p2}, Lcom/google/android/gms/games/h/a/du;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bp;->j:Lcom/google/android/gms/games/h/a/du;

    .line 160
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bp;->f:Ljava/util/HashMap;

    .line 161
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bp;->d:Ljava/util/Random;

    .line 162
    iput-object p4, p0, Lcom/google/android/gms/games/a/bp;->k:Lcom/google/android/gms/games/a/aa;

    .line 163
    return-void
.end method

.method private static a(Lcom/android/volley/ac;I)I
    .locals 3

    .prologue
    const/16 v0, 0x1f42

    .line 912
    .line 913
    const-string v1, "QuestAgent"

    invoke-static {p0, v1}, Lcom/google/android/gms/common/server/b/c;->b(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/a;

    move-result-object v1

    .line 914
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/b/a;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 932
    :cond_0
    :goto_0
    return p1

    .line 919
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/gms/common/server/b/a;->b()Ljava/lang/String;

    move-result-object v1

    .line 920
    const-string v2, "QuestMilestoneClaimed"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 921
    const/16 p1, 0x1f40

    goto :goto_0

    .line 922
    :cond_2
    const-string v2, "QuestMilestoneNotComplete"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 923
    const/16 p1, 0x1f41

    goto :goto_0

    .line 924
    :cond_3
    const-string v2, "QuestExpired"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    move p1, v0

    .line 925
    goto :goto_0

    .line 926
    :cond_4
    const-string v2, "QuestNotStarted"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 927
    const/16 p1, 0x1f43

    goto :goto_0

    .line 928
    :cond_5
    const-string v2, "QuestClosed"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    move p1, v0

    .line 929
    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/games/a/au;[II[Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    .prologue
    .line 991
    invoke-virtual {p0}, Lcom/google/android/gms/games/a/au;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 992
    new-instance v0, Lcom/google/android/gms/common/e/b;

    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/games/provider/as;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 997
    :goto_0
    array-length v2, p1

    .line 998
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1000
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_3

    .line 1001
    aget v4, p1, v1

    .line 1002
    packed-switch v4, :pswitch_data_0

    .line 1047
    sget-object v5, Lcom/google/android/gms/games/quest/Quest;->a:[I

    invoke-static {v5, v4}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1049
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "quest_state = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1000
    :cond_0
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 995
    :cond_1
    new-instance v0, Lcom/google/android/gms/common/e/b;

    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1}, Lcom/google/android/gms/games/provider/as;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    goto :goto_0

    .line 1007
    :pswitch_0
    const/4 v4, 0x4

    invoke-static {p1, v4}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1008
    const-string v4, "quest_state = 4 AND milestone_state = 3"

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1021
    :pswitch_1
    const/4 v4, 0x3

    invoke-static {p1, v4}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1022
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    const-wide/32 v6, 0x1b7740

    add-long/2addr v4, v6

    .line 1027
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "quest_state = 3 AND notification_ts <= "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND notification_ts <> -1"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1036
    :pswitch_2
    const/4 v4, 0x6

    invoke-static {p1, v4}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1037
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    const-wide/32 v6, 0x240c8400

    sub-long/2addr v4, v6

    .line 1042
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "quest_state = 6 AND quest_last_updated_ts >= "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1051
    :cond_2
    const-string v5, "QuestAgent"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Invalid quest selector used ("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "), see the selectors at the beginning of Quests for a valid list."

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1057
    :cond_3
    const-string v1, " OR "

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bt;->a(Ljava/lang/String;)Lcom/google/android/gms/common/internal/bt;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/gms/common/internal/bt;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/b;->c(Ljava/lang/String;)V

    .line 1059
    const/4 v1, 0x1

    if-ne p2, v1, :cond_5

    .line 1060
    const-string v1, "quest_end_ts DESC,milestones_sorting_rank ASC"

    .line 1066
    :goto_3
    if-eqz p3, :cond_4

    .line 1067
    const-string v2, "external_quest_id"

    invoke-virtual {v0, v2, p3}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1070
    :cond_4
    new-instance v2, Lcom/google/android/gms/games/a/o;

    invoke-direct {v2, p0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    iput-object v0, v2, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    iput-object v1, v2, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    iput p4, v2, Lcom/google/android/gms/games/a/o;->d:I

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0

    .line 1062
    :cond_5
    const-string v1, "quest_last_updated_ts DESC,milestones_sorting_rank ASC"

    goto :goto_3

    .line 1002
    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Landroid/net/Uri;J)Lcom/google/android/gms/common/e/b;
    .locals 5

    .prologue
    .line 943
    new-instance v0, Lcom/google/android/gms/common/e/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 945
    const-string v1, "muted"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 947
    const-string v1, "notified"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 949
    const-string v1, "notification_ts"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    const-string v3, "<=?"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 951
    const-string v1, "notification_ts"

    const-string v2, "-1"

    const-string v3, "<>?"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 953
    const-string v1, "quest_state"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 954
    return-object v0
.end method

.method private static a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/HashSet;)Ljava/util/ArrayList;
    .locals 4

    .prologue
    .line 1396
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1398
    invoke-virtual {p1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1399
    invoke-static {p0, v0}, Lcom/google/android/gms/games/provider/ar;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1400
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1404
    :cond_0
    return-object v1
.end method

.method private a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/a/bs;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;
    .locals 14

    .prologue
    .line 710
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/gms/games/a/bs;->b:Ljava/lang/String;

    .line 711
    if-eqz v2, :cond_0

    .line 712
    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v3}, Lcom/google/android/gms/games/provider/m;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v3

    .line 713
    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 718
    :cond_0
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    .line 724
    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->g()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 725
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 726
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->j()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 728
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->j()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/android/gms/games/provider/ar;->b(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v2

    move-object v9, v3

    move-object v3, v2

    .line 738
    :goto_0
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    new-instance v5, Lcom/google/android/gms/common/e/b;

    invoke-static {v4}, Lcom/google/android/gms/games/provider/ar;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v5, v4}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "quest_state in ("

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, ","

    sget-object v7, Lcom/google/android/gms/games/quest/Quest;->b:[Ljava/lang/String;

    invoke-static {v6, v7}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ")"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/google/android/gms/common/e/b;->c(Ljava/lang/String;)V

    const-string v4, "external_quest_id"

    invoke-static {v2, v5, v4}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Lcom/google/android/gms/common/e/b;Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v11

    .line 741
    if-eqz p6, :cond_2

    .line 742
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const-string v4, "external_quest_id"

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v2

    move-object v4, v2

    .line 749
    :goto_1
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 754
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v6, v2

    :goto_2
    if-ltz v6, :cond_6

    .line 755
    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/dl;

    .line 756
    if-eqz v2, :cond_3

    const/4 v5, 0x1

    :goto_3
    const-string v7, "We are trying to add a null quest"

    invoke-static {v5, v7}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 757
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/dl;->b()Ljava/lang/String;

    move-result-object v5

    .line 758
    invoke-virtual {v9, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 759
    iget-object v5, v2, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    const-string v7, "quest_state"

    invoke-virtual {v5, v7}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 761
    sparse-switch v5, :sswitch_data_0

    const/4 v5, 0x0

    :goto_4
    if-eqz v5, :cond_4

    .line 762
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/dl;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 763
    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 754
    :goto_5
    add-int/lit8 v2, v6, -0x1

    move v6, v2

    goto :goto_2

    .line 731
    :cond_1
    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const-string v4, "external_game_id"

    invoke-static {v3, v2, v4}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v3

    .line 733
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2}, Lcom/google/android/gms/games/provider/ar;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    move-object v9, v3

    move-object v3, v2

    goto/16 :goto_0

    .line 745
    :cond_2
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    move-object v4, v2

    goto :goto_1

    .line 756
    :cond_3
    const/4 v5, 0x0

    goto :goto_3

    .line 761
    :sswitch_0
    const/4 v5, 0x1

    goto :goto_4

    .line 765
    :cond_4
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/dl;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_5

    .line 768
    :cond_5
    const-string v2, "QuestAgent"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Attempting to add quest to an unknown application id: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_5

    .line 774
    :cond_6
    invoke-virtual {v4}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 775
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const-string v5, "external_quest_id"

    const-string v6, "external_game_id"

    invoke-static {v2, v3, v5, v6, v4}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)Ljava/util/HashMap;

    move-result-object v2

    .line 778
    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 779
    new-instance v6, Lcom/google/android/gms/games/a/br;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v7, 0x3

    invoke-direct {v6, v3, v2, v7}, Lcom/google/android/gms/games/a/br;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v12, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 785
    :cond_7
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2, v4}, Lcom/google/android/gms/games/a/bp;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/HashSet;)Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 789
    :cond_8
    const/4 v2, 0x0

    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    move-result v13

    move v10, v2

    :goto_7
    if-ge v10, v13, :cond_a

    .line 790
    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/games/h/a/dl;

    .line 791
    invoke-virtual {v5}, Lcom/google/android/gms/games/h/a/dl;->b()Ljava/lang/String;

    move-result-object v2

    .line 792
    iget-object v3, v5, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    const-string v4, "quest_state"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 793
    new-instance v4, Lcom/google/android/gms/games/a/br;

    invoke-virtual {v5}, Lcom/google/android/gms/games/h/a/dl;->c()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x2

    invoke-direct {v4, v2, v6, v7}, Lcom/google/android/gms/games/a/br;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 797
    const/4 v4, 0x4

    if-ne v4, v3, :cond_9

    invoke-virtual {v5}, Lcom/google/android/gms/games/h/a/dl;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 798
    new-instance v3, Lcom/google/android/gms/games/a/br;

    invoke-virtual {v5}, Lcom/google/android/gms/games/h/a/dl;->c()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x1

    invoke-direct {v3, v2, v4, v6}, Lcom/google/android/gms/games/a/br;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 801
    :cond_9
    invoke-virtual {v9, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object v3, p0

    move-object v4, p1

    move-object/from16 v8, p5

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/games/a/bp;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/dl;JLjava/util/ArrayList;)V

    .line 789
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto :goto_7

    .line 804
    :cond_a
    return-object v12

    .line 761
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x7 -> :sswitch_0
    .end sparse-switch
.end method

.method private a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/dl;JLjava/util/ArrayList;)V
    .locals 17

    .prologue
    .line 598
    if-nez p2, :cond_1

    .line 668
    :cond_0
    :goto_0
    return-void

    .line 601
    :cond_1
    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 605
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 606
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 607
    const-string v3, "game_id"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 608
    const-string v3, "external_game_id"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 609
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/a/au;->c()Lcom/google/android/gms/games/a/av;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/dl;->b()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gms/games/a/av;->e:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object v4

    .line 613
    iget-object v3, v4, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v3}, Lcom/google/android/gms/games/provider/ar;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 619
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/dl;->getMilestones()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/dl;->getMilestones()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 621
    :cond_2
    const-string v2, "QuestAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Milestones are missing for questId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/dl;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 625
    :cond_3
    const/4 v2, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/dl;->getMilestones()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v12

    move v10, v2

    :goto_1
    if-ge v10, v12, :cond_0

    .line 626
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/dl;->getMilestones()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/dt;

    .line 627
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/dt;->getCriteria()Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/dt;->getCriteria()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 629
    :cond_4
    const-string v3, "QuestAgent"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Criteria not present for milestoneId: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/dt;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 633
    :cond_5
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/dt;->getCriteria()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v9, v3

    check-cast v9, Lcom/google/android/gms/games/h/a/dn;

    .line 635
    const-wide/16 v6, -0x1

    .line 636
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/dt;->c()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v5, 0x2

    if-ne v3, v5, :cond_6

    .line 641
    invoke-virtual {v9}, Lcom/google/android/gms/games/h/a/dn;->getInitialPlayerProgress()Lcom/google/android/gms/games/h/a/dm;

    move-result-object v3

    .line 642
    invoke-virtual {v9}, Lcom/google/android/gms/games/h/a/dn;->getCurrentContribution()Lcom/google/android/gms/games/h/a/dm;

    move-result-object v5

    .line 645
    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/dm;->b()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v5}, Lcom/google/android/gms/games/h/a/dm;->b()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    add-long/2addr v6, v14

    .line 648
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/a/bp;->k:Lcom/google/android/gms/games/a/aa;

    invoke-virtual {v9}, Lcom/google/android/gms/games/h/a/dn;->b()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v8, p5

    invoke-interface/range {v3 .. v8}, Lcom/google/android/gms/games/a/aa;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;JLjava/util/ArrayList;)J

    move-result-wide v6

    .line 650
    const-wide/16 v14, 0x0

    cmp-long v3, v6, v14

    if-gez v3, :cond_7

    .line 651
    const-string v2, "QuestAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "EventInstance not present for externalEventId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/google/android/gms/games/h/a/dn;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", on milestone "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " of questId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/dl;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 657
    :cond_7
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 658
    iget-object v2, v2, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-virtual {v3, v2}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 659
    const-string v2, "event_instance_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 660
    const-string v2, "milestones_sorting_rank"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 662
    iget-object v2, v4, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2}, Lcom/google/android/gms/games/provider/am;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v5, "quest_id"

    invoke-virtual {v2, v5, v11}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 625
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto/16 :goto_1
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Lcom/google/android/gms/games/internal/el;)V
    .locals 4

    .prologue
    .line 381
    if-eqz p2, :cond_0

    .line 382
    invoke-static {p0, p1}, Lcom/google/android/gms/games/a/bp;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 383
    const-string v1, "com.google.android.gms.games.extra.state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 384
    packed-switch v1, :pswitch_data_0

    .line 391
    :goto_0
    const-string v0, "QuestAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Not showing popup for quest in state "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", because the state is not invalid."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    :cond_0
    return-void

    .line 387
    :pswitch_0
    invoke-static {p0, p2, v0}, Lcom/google/android/gms/games/ui/c/j;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Landroid/os/Bundle;)V

    goto :goto_0

    .line 384
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 1108
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v0

    .line 1109
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    .line 1110
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/a/am;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1127
    :goto_0
    return-void

    .line 1115
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2, p1}, Lcom/google/android/gms/games/provider/as;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1117
    new-instance v3, Lcom/google/android/gms/games/a/o;

    invoke-direct {v3, p0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-virtual {v3, v2}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    .line 1121
    if-eqz p2, :cond_1

    .line 1122
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v3, p1, v2}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/data/DataHolder;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1126
    :cond_1
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method public static a(Lcom/google/android/gms/games/a/au;J)Z
    .locals 5

    .prologue
    .line 491
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    add-long/2addr v0, v2

    .line 493
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2}, Lcom/google/android/gms/games/provider/ar;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/games/a/bp;->a(Landroid/net/Uri;J)Lcom/google/android/gms/common/e/b;

    move-result-object v0

    .line 495
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/android/gms/common/e/b;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/a/bs;Z)Z
    .locals 7

    .prologue
    .line 1246
    iget-object v4, p2, Lcom/google/android/gms/games/a/bs;->a:Ljava/util/ArrayList;

    .line 1247
    if-nez v4, :cond_0

    .line 1248
    const/4 v0, 0x1

    .line 1255
    :goto_0
    return v0

    .line 1252
    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1253
    const-string v3, "quest_sync_token"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/a/bp;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/a/bs;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v0

    .line 1255
    invoke-direct {p0, p1, v5, v0}, Lcom/google/android/gms/games/a/bp;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1142
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1143
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/a/bp;->a(Lcom/google/android/gms/games/a/au;)I

    move-result v0

    .line 1144
    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "QuestAgent"

    invoke-static {v3, p2, v4}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1146
    const-string v0, "QuestAgent"

    const-string v1, "Failed to store quests."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 1165
    :cond_0
    return v1

    .line 1155
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/a/bp;->a(Lcom/google/android/gms/games/a/au;)I

    move-result v3

    if-eq v0, v3, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/games/a/bp;->l:Z

    .line 1156
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_0

    .line 1157
    invoke-virtual {p3, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/a/br;

    .line 1158
    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->c()Lcom/google/android/gms/games/a/av;

    move-result-object v5

    iget-object v6, v0, Lcom/google/android/gms/games/a/br;->a:Ljava/lang/String;

    iput-object v6, v5, Lcom/google/android/gms/games/a/av;->e:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object p1

    .line 1161
    iget-object v5, v0, Lcom/google/android/gms/games/a/br;->b:Ljava/lang/String;

    iget v0, v0, Lcom/google/android/gms/games/a/br;->c:I

    if-ne v1, v0, :cond_3

    move v0, v1

    :goto_2
    invoke-static {p1, v5, v0}, Lcom/google/android/gms/games/a/bp;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Z)V

    .line 1163
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/bp;->f(Lcom/google/android/gms/games/a/au;)V

    .line 1156
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 1155
    goto :goto_0

    :cond_3
    move v0, v2

    .line 1161
    goto :goto_2
.end method

.method private a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Z)Z
    .locals 3

    .prologue
    .line 1268
    new-instance v0, Lcom/google/android/gms/games/a/bs;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p2, v1, v2}, Lcom/google/android/gms/games/a/bs;-><init>(Ljava/util/ArrayList;Ljava/lang/String;I)V

    .line 1270
    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/gms/games/a/bp;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/a/bs;Z)Z

    move-result v0

    return v0
.end method

.method private static b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 5

    .prologue
    .line 672
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/provider/ar;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 674
    new-instance v2, Lcom/google/android/gms/games/quest/b;

    new-instance v1, Lcom/google/android/gms/games/a/o;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/quest/b;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 677
    const/4 v0, 0x0

    .line 679
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/gms/games/quest/b;->c()I

    move-result v1

    if-lez v1, :cond_0

    .line 680
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/quest/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/quest/Quest;

    .line 681
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 682
    const-string v3, "com.google.android.gms.games.extra.name"

    invoke-interface {v0}, Lcom/google/android/gms/games/quest/Quest;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    const-string v3, "com.google.android.gms.games.extra.imageUri"

    invoke-interface {v0}, Lcom/google/android/gms/games/quest/Quest;->h()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 685
    const-string v3, "com.google.android.gms.games.extra.state"

    invoke-interface {v0}, Lcom/google/android/gms/games/quest/Quest;->m()I

    move-result v0

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v1

    .line 688
    :cond_0
    invoke-virtual {v2}, Lcom/google/android/gms/games/quest/b;->w_()V

    .line 691
    return-object v0

    .line 688
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/games/quest/b;->w_()V

    throw v0
.end method

.method public static b(Lcom/google/android/gms/games/a/au;)V
    .locals 11

    .prologue
    const/4 v4, 0x2

    const/4 v10, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x3

    .line 240
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/games/provider/as;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 242
    new-instance v2, Lcom/google/android/gms/common/e/b;

    invoke-direct {v2, v1}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 243
    const-string v1, "quest_state"

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    const-string v1, "milestone_state"

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    new-instance v1, Lcom/google/android/gms/games/a/o;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    iput-object v2, v1, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    sget-object v2, Lcom/google/android/gms/games/a/bq;->a:[Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v1

    .line 249
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 250
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 252
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 254
    const/4 v4, 0x3

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 255
    const/4 v6, 0x4

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 258
    const/4 v8, 0x5

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 260
    sub-long v4, v6, v4

    cmp-long v4, v4, v8

    if-ltz v4, :cond_0

    .line 262
    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 263
    iget-object v6, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v6, v4, v5}, Lcom/google/android/gms/games/provider/ar;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v4

    .line 264
    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "quest_state"

    const/4 v6, 0x4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "quest_last_updated_ts"

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 268
    const/4 v4, 0x1

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 269
    iget-object v6, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v6, v4, v5}, Lcom/google/android/gms/games/provider/am;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v4

    .line 271
    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "milestone_state"

    const/4 v6, 0x3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 274
    const/4 v4, 0x2

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 278
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 281
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 283
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v4, "QuestAgent"

    invoke-static {v1, v2, v4}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 287
    :cond_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_3

    .line 288
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v0, v10}, Lcom/google/android/gms/games/a/bp;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Z)V

    .line 287
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 290
    :cond_3
    return-void
.end method

.method private static b(I)Z
    .locals 1

    .prologue
    .line 1176
    if-eqz p0, :cond_0

    const/16 v0, 0x1f4

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/quest/b;
    .locals 9

    .prologue
    .line 452
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 453
    iget-object v3, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 455
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v0

    const-wide/32 v4, 0x1b7740

    add-long/2addr v0, v4

    .line 456
    invoke-static {v3}, Lcom/google/android/gms/games/provider/as;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4, v0, v1}, Lcom/google/android/gms/games/a/bp;->a(Landroid/net/Uri;J)Lcom/google/android/gms/common/e/b;

    move-result-object v0

    .line 458
    new-instance v4, Lcom/google/android/gms/games/quest/b;

    new-instance v1, Lcom/google/android/gms/games/a/o;

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    iput-object v0, v1, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    const-string v0, "quest_last_updated_ts DESC,milestones_sorting_rank ASC"

    iput-object v0, v1, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/android/gms/games/quest/b;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 462
    invoke-virtual {v4}, Lcom/google/android/gms/games/quest/b;->c()I

    move-result v5

    .line 464
    invoke-virtual {v4}, Lcom/google/android/gms/games/quest/b;->c()I

    move-result v0

    if-lez v0, :cond_1

    .line 465
    new-instance v6, Ljava/util/ArrayList;

    invoke-virtual {v4}, Lcom/google/android/gms/games/quest/b;->c()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 466
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    .line 467
    invoke-virtual {v4, v1}, Lcom/google/android/gms/games/quest/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/quest/Quest;

    invoke-interface {v0}, Lcom/google/android/gms/games/quest/Quest;->a()Ljava/lang/String;

    move-result-object v0

    .line 470
    invoke-static {v3, v0}, Lcom/google/android/gms/games/provider/ar;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v7, "notified"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 466
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 475
    :cond_0
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "QuestAgent"

    invoke-static {v0, v6, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 478
    :cond_1
    return-object v4
.end method

.method private static c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 816
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 817
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1}, Lcom/google/android/gms/games/provider/m;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 821
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1}, Lcom/google/android/gms/games/provider/ar;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    .line 822
    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 823
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "QuestAgent"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 824
    return-void
.end method

.method private d(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/games/a/bs;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1287
    :goto_0
    :try_start_0
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    .line 1289
    iget-object v0, p0, Lcom/google/android/gms/games/a/bp;->i:Lcom/google/android/gms/games/h/a/dr;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "questMetadata/sync"

    if-eqz v2, :cond_0

    const-string v4, "language"

    invoke-static {v2}, Lcom/google/android/gms/games/h/a/dr;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v2}, Lcom/google/android/gms/games/h/a/dr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz p2, :cond_1

    const-string v2, "syncToken"

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/dr;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v2, v4}, Lcom/google/android/gms/games/h/a/dr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    iget-object v0, v0, Lcom/google/android/gms/games/h/a/dr;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/ds;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/ds;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 1308
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ds;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    .line 1309
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ds;->c()Ljava/lang/String;

    move-result-object v2

    .line 1310
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ds;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1311
    invoke-static {p2, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    const-string v3, "Server claims to have more data, yet sync tokens match!"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 1313
    invoke-direct {p0, p1, v2}, Lcom/google/android/gms/games/a/bp;->d(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/games/a/bs;

    move-result-object v0

    .line 1315
    iget v6, v0, Lcom/google/android/gms/games/a/bs;->c:I

    .line 1316
    if-nez v6, :cond_2

    .line 1317
    iget-object v2, v0, Lcom/google/android/gms/games/a/bs;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1318
    iget-object v0, v0, Lcom/google/android/gms/games/a/bs;->b:Ljava/lang/String;

    move-object v2, v0

    .line 1322
    :cond_2
    if-nez v1, :cond_6

    .line 1324
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1326
    :goto_2
    new-instance v1, Lcom/google/android/gms/games/a/bs;

    invoke-direct {v1, v0, v2, v6}, Lcom/google/android/gms/games/a/bs;-><init>(Ljava/util/ArrayList;Ljava/lang/String;I)V

    move-object v0, v1

    :goto_3
    return-object v0

    .line 1291
    :catch_0
    move-exception v0

    .line 1292
    const/16 v1, 0x19a

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1295
    const-string v0, "quest_sync_metadata_token"

    invoke-static {p1, v0}, Lcom/google/android/gms/games/a/bp;->c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)V

    .line 1296
    const-string v0, "QuestAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Token "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is invalid. Retrying with no token."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object p2, v7

    .line 1297
    goto/16 :goto_0

    .line 1300
    :cond_3
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1301
    const-string v1, "QuestAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 1303
    :cond_4
    new-instance v0, Lcom/google/android/gms/games/a/bs;

    invoke-direct {v0}, Lcom/google/android/gms/games/a/bs;-><init>()V

    goto :goto_3

    :cond_5
    move v0, v6

    .line 1311
    goto :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_2
.end method

.method private e(Lcom/google/android/gms/games/a/au;)I
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 837
    .line 839
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->g:Z

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/google/android/gms/games/provider/ar;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/games/a/bp;->f:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-eqz v0, :cond_6

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v4, v6

    const-wide/32 v6, 0x36ee80

    cmp-long v0, v4, v6

    if-gtz v0, :cond_6

    move v0, v3

    :goto_0
    if-nez v0, :cond_a

    .line 844
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/bp;->k:Lcom/google/android/gms/games/a/aa;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/a/aa;->c(Lcom/google/android/gms/games/a/au;)I

    move-result v6

    .line 845
    if-nez v6, :cond_5

    .line 846
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    move-object v0, v2

    :cond_1
    iget-boolean v1, p1, Lcom/google/android/gms/games/a/au;->h:Z

    if-eqz v1, :cond_7

    iget-object v2, p0, Lcom/google/android/gms/games/a/bp;->g:Lcom/google/android/gms/games/h/a/dx;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    const-string v5, "applications/%1$s/players/%2$s/quests"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v3}, Lcom/google/android/gms/games/h/a/dx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v9, v10

    const/4 v3, 0x1

    invoke-static {v4}, Lcom/google/android/gms/games/h/a/dx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v3

    invoke-static {v5, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v8, :cond_2

    const-string v4, "language"

    invoke-static {v8}, Lcom/google/android/gms/games/h/a/dx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/games/h/a/dx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    if-eqz v0, :cond_3

    const-string v4, "pageToken"

    invoke-static {v0}, Lcom/google/android/gms/games/h/a/dx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/games/h/a/dx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_3
    iget-object v0, v2, Lcom/google/android/gms/games/h/a/dx;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/do;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/do;

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/do;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/do;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    move-object v11, v0

    move-object v0, v1

    move-object v1, v11

    :goto_1
    if-eqz v1, :cond_4

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_4
    if-nez v0, :cond_1

    .line 847
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/bp;->f(Lcom/google/android/gms/games/a/au;)V

    .line 848
    const/4 v0, 0x1

    invoke-direct {p0, p1, v7, v0}, Lcom/google/android/gms/games/a/bp;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Z)Z

    :cond_5
    move v0, v6

    .line 856
    :goto_2
    return v0

    :cond_6
    move v0, v1

    .line 839
    goto :goto_0

    .line 846
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/games/a/bp;->h:Lcom/google/android/gms/games/h/a/dw;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    const-string v4, "players/%1$s/quests"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v3}, Lcom/google/android/gms/games/h/a/dw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v8, :cond_8

    const-string v4, "language"

    invoke-static {v8}, Lcom/google/android/gms/games/h/a/dw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/games/h/a/dw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_8
    if-eqz v0, :cond_9

    const-string v4, "pageToken"

    invoke-static {v0}, Lcom/google/android/gms/games/h/a/dw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/games/h/a/dw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_9
    iget-object v0, v2, Lcom/google/android/gms/games/h/a/dw;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/dp;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/dp;

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/dp;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/dp;->getItems()Ljava/util/ArrayList;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v11, v0

    move-object v0, v1

    move-object v1, v11

    goto :goto_1

    .line 850
    :catch_0
    move-exception v0

    .line 851
    const-string v1, "QuestAgent"

    const-string v2, "Unable to retrieve quest list"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 853
    const/16 v0, 0x1f4

    goto :goto_2

    :cond_a
    move v0, v1

    goto :goto_2
.end method

.method private e(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/games/a/bs;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1344
    :goto_0
    :try_start_0
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    .line 1346
    iget-object v0, p0, Lcom/google/android/gms/games/a/bp;->g:Lcom/google/android/gms/games/h/a/dx;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "quests/sync"

    if-eqz v2, :cond_0

    const-string v4, "language"

    invoke-static {v2}, Lcom/google/android/gms/games/h/a/dx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v2}, Lcom/google/android/gms/games/h/a/dx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz p2, :cond_1

    const-string v2, "syncToken"

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/dx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v2, v4}, Lcom/google/android/gms/games/h/a/dx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    iget-object v0, v0, Lcom/google/android/gms/games/h/a/dx;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/dv;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/dv;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 1365
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/dv;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    .line 1366
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/dv;->c()Ljava/lang/String;

    move-result-object v2

    .line 1367
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/dv;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1368
    invoke-static {p2, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    const-string v3, "Server claims to have more data, yet sync tokens match!"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 1370
    invoke-direct {p0, p1, v2}, Lcom/google/android/gms/games/a/bp;->e(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/games/a/bs;

    move-result-object v0

    .line 1372
    iget v6, v0, Lcom/google/android/gms/games/a/bs;->c:I

    .line 1373
    if-nez v6, :cond_2

    .line 1374
    iget-object v2, v0, Lcom/google/android/gms/games/a/bs;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1375
    iget-object v0, v0, Lcom/google/android/gms/games/a/bs;->b:Ljava/lang/String;

    move-object v2, v0

    .line 1379
    :cond_2
    if-nez v1, :cond_6

    .line 1381
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1383
    :goto_2
    new-instance v1, Lcom/google/android/gms/games/a/bs;

    invoke-direct {v1, v0, v2, v6}, Lcom/google/android/gms/games/a/bs;-><init>(Ljava/util/ArrayList;Ljava/lang/String;I)V

    move-object v0, v1

    :goto_3
    return-object v0

    .line 1348
    :catch_0
    move-exception v0

    .line 1349
    const/16 v1, 0x19a

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1352
    const-string v0, "quest_sync_token"

    invoke-static {p1, v0}, Lcom/google/android/gms/games/a/bp;->c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)V

    .line 1353
    const-string v0, "QuestAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Token "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is invalid. Retrying with no token."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object p2, v7

    .line 1354
    goto/16 :goto_0

    .line 1357
    :cond_3
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1358
    const-string v1, "QuestAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 1360
    :cond_4
    new-instance v0, Lcom/google/android/gms/games/a/bs;

    invoke-direct {v0}, Lcom/google/android/gms/games/a/bs;-><init>()V

    goto :goto_3

    :cond_5
    move v0, v6

    .line 1368
    goto :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_2
.end method

.method private f(Lcom/google/android/gms/games/a/au;)V
    .locals 4

    .prologue
    .line 1409
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/provider/ar;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1411
    iget-object v1, p0, Lcom/google/android/gms/games/a/bp;->f:Ljava/util/HashMap;

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1412
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/a/au;)I
    .locals 3

    .prologue
    .line 406
    new-instance v0, Lcom/google/android/gms/common/e/b;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1}, Lcom/google/android/gms/games/provider/ar;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 407
    const-string v1, "quest_state"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Lcom/google/android/gms/common/e/b;)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 205
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/provider/ar;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/ar;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 208
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const-string v2, "accepted_ts"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/a/l;->e(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    .line 213
    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 215
    :try_start_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 218
    iget-object v0, p0, Lcom/google/android/gms/games/a/bp;->h:Lcom/google/android/gms/games/h/a/dw;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "quests/%1$s/accept"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/dw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_0

    const-string v4, "language"

    invoke-static {v2}, Lcom/google/android/gms/games/h/a/dw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v2}, Lcom/google/android/gms/games/h/a/dw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/games/h/a/dw;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/dl;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/dl;

    .line 220
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 221
    const/4 v0, 0x0

    invoke-direct {p0, p1, v7, v0}, Lcom/google/android/gms/games/a/bp;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Z)Z
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v6

    .line 230
    :goto_0
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1, p2}, Lcom/google/android/gms/games/provider/as;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 232
    new-instance v2, Lcom/google/android/gms/games/a/o;

    invoke-direct {v2, p1}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v1

    const-string v2, "quest_last_updated_ts DESC,milestones_sorting_rank ASC"

    iput-object v2, v1, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    iput v0, v1, Lcom/google/android/gms/games/a/o;->d:I

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0

    .line 222
    :catch_0
    move-exception v0

    .line 223
    const-string v1, "QuestAgent"

    const-string v2, "Unable to accept quest"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 224
    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/games/a/bp;->a(Lcom/android/volley/ac;I)I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v6

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 308
    .line 312
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/games/a/bp;->j:Lcom/google/android/gms/games/h/a/du;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v4, p0, Lcom/google/android/gms/games/a/bp;->d:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const-string v5, "quests/%1$s/milestones/%2$s/claim"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/du;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {p3}, Lcom/google/android/gms/games/h/a/du;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "requestId"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v6, v4}, Lcom/google/android/gms/games/h/a/du;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v2, v2, Lcom/google/android/gms/games/h/a/du;->a:Lcom/google/android/gms/common/server/n;

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v5, v4, v6}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 322
    :goto_0
    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, p3}, Lcom/google/android/gms/games/provider/am;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 326
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 327
    const-string v3, "milestone_state"

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 328
    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v0, v2, v9, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 333
    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/as;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 335
    new-instance v2, Lcom/google/android/gms/games/a/o;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    const-string v2, "milestones_sorting_rank ASC"

    iput-object v2, v0, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    iput v1, v0, Lcom/google/android/gms/games/a/o;->d:I

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0

    .line 314
    :catch_0
    move-exception v2

    .line 315
    const-string v3, "QuestAgent"

    const-string v4, "Unable to claim milestone."

    invoke-static {v3, v4, v2}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 316
    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/gms/games/a/bp;->a(Lcom/android/volley/ac;I)I

    move-result v2

    .line 319
    const/16 v3, 0x1f40

    if-ne v2, v3, :cond_1

    :goto_1
    move v1, v2

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/a/au;[II[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    .prologue
    .line 359
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->h:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Attempting to access a 3P API using a 1P Context"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 360
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/bp;->e(Lcom/google/android/gms/games/a/au;)I

    move-result v0

    .line 361
    invoke-static {v0}, Lcom/google/android/gms/games/a/bp;->b(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 362
    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 364
    :goto_1
    return-object v0

    .line 359
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 364
    :cond_1
    invoke-static {p1, p2, p3, p4, v0}, Lcom/google/android/gms/games/a/bp;->a(Lcom/google/android/gms/games/a/au;[II[Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_1
.end method

.method public final a()Lcom/google/android/gms/games/a/bb;
    .locals 0

    .prologue
    .line 179
    return-object p0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 167
    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_0

    .line 168
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/a/bp;->l:Z

    .line 170
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/a/au;[II[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 429
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->h:Z

    const-string v2, "Attempting to access a 1P API using a 3P Context"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 431
    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/bp;->e(Lcom/google/android/gms/games/a/au;)I

    move-result v0

    .line 433
    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/games/a/bp;->b(I)Z

    move-result v1

    if-nez v1, :cond_4

    .line 434
    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 436
    :goto_1
    return-object v0

    .line 431
    :cond_0
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->g:Z

    if-nez v0, :cond_2

    iget-wide v4, p0, Lcom/google/android/gms/games/a/bp;->e:J

    sub-long/2addr v2, v4

    sget-object v0, Lcom/google/android/gms/games/c/a;->v:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gtz v0, :cond_2

    const-string v0, "QuestAgent"

    const-string v2, "Returning cached entities for quest"

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_2
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    sget-object v3, Lcom/google/android/gms/games/a/bp;->b:[Ljava/lang/String;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/a/bp;->e(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/games/a/bs;

    move-result-object v0

    const-string v2, "QuestAgent"

    const-string v3, "Received %s quest entities during sync"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, v0, Lcom/google/android/gms/games/a/bs;->a:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v2, v0, Lcom/google/android/gms/games/a/bs;->c:I

    if-eqz v2, :cond_3

    iget v0, v0, Lcom/google/android/gms/games/a/bs;->c:I

    goto :goto_0

    :cond_3
    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/games/a/bp;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/a/bs;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/games/a/bp;->e:J

    goto :goto_2

    .line 436
    :cond_4
    invoke-static {p1, p2, p3, p4, v0}, Lcom/google/android/gms/games/a/bp;->a(Lcom/google/android/gms/games/a/au;[II[Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    const-string v0, "inbox_quests_count"

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 184
    iget-boolean v0, p0, Lcom/google/android/gms/games/a/bp;->l:Z

    return v0
.end method

.method public final d(Lcom/google/android/gms/games/a/au;)I
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 511
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v0

    .line 512
    iget-wide v2, p0, Lcom/google/android/gms/games/a/bp;->e:J

    sub-long v2, v0, v2

    sget-object v0, Lcom/google/android/gms/games/c/a;->v:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-gtz v0, :cond_1

    .line 513
    const-string v0, "QuestAgent"

    const-string v1, "Returning cached entities for quest metadata"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    :cond_0
    :goto_0
    return v6

    .line 518
    :cond_1
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    sget-object v2, Lcom/google/android/gms/games/a/bp;->c:[Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 520
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/a/bp;->d(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/games/a/bs;

    move-result-object v2

    .line 522
    const-string v0, "QuestAgent"

    const-string v1, "Received %s quest metadata entities during sync"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, v2, Lcom/google/android/gms/games/a/bs;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    iget v0, v2, Lcom/google/android/gms/games/a/bs;->c:I

    if-eqz v0, :cond_2

    .line 525
    iget v6, v2, Lcom/google/android/gms/games/a/bs;->c:I

    goto :goto_0

    .line 529
    :cond_2
    iget-object v3, v2, Lcom/google/android/gms/games/a/bs;->a:Ljava/util/ArrayList;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v6

    :goto_1
    if-ge v1, v5, :cond_5

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/dq;

    const-string v8, "QUEST_METADATA"

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/dq;->c()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/dq;->getQuest()Lcom/google/android/gms/games/h/a/dl;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    const-string v8, "APPLICATION_ID"

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/dq;->c()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/dq;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    const-string v8, "QuestAgent"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Invalid QuestMetadata type: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/dq;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const-string v3, "quest_sync_metadata_token"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/a/bp;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/a/bs;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v6

    :goto_3
    if-ge v1, v3, :cond_6

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v4, v0}, Lcom/google/android/gms/games/provider/ar;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v4

    invoke-virtual {v0, v4}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_6
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/provider/ar;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const-string v3, "external_game_id"

    const-string v4, "external_quest_id"

    invoke-static {v1, v0, v3, v4, v7}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v4, Lcom/google/android/gms/games/a/br;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v7, 0x3

    invoke-direct {v4, v1, v0, v7}, Lcom/google/android/gms/games/a/br;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_7
    invoke-direct {p0, p1, v5, v2}, Lcom/google/android/gms/games/a/bp;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v0

    .line 530
    if-eqz v0, :cond_0

    .line 531
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/a/bp;->e:J

    goto/16 :goto_0
.end method
