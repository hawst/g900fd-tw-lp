.class public final Lcom/google/android/gms/udc/ui/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/av;


# instance fields
.field final synthetic a:Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;)V
    .locals 0

    .prologue
    .line 241
    iput-object p1, p0, Lcom/google/android/gms/udc/ui/y;->a:Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 4

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/y;->a:Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;

    invoke-static {v0}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->a(Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;)Lcom/google/android/gms/udc/util/j;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->gq:I

    new-instance v2, Lcom/google/android/gms/udc/ui/u;

    invoke-direct {v2}, Lcom/google/android/gms/udc/ui/u;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/gms/udc/ui/u;->b()Lcom/google/android/gms/udc/ui/j;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/udc/util/j;->a(ILandroid/support/v4/app/Fragment;)V

    .line 250
    new-instance v0, Lcom/google/android/gms/udc/e/f;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/f;-><init>()V

    .line 251
    iget-object v1, p0, Lcom/google/android/gms/udc/ui/y;->a:Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/udc/c/w;->a(Landroid/content/Context;ILjava/lang/String;)Lcom/google/android/gms/udc/e/u;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/udc/e/f;->c:Lcom/google/android/gms/udc/e/u;

    .line 254
    new-instance v1, Lcom/google/android/gms/udc/c/i;

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/y;->a:Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;

    iget-object v3, p0, Lcom/google/android/gms/udc/ui/y;->a:Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;

    invoke-static {v3}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->b(Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;)Lcom/google/android/gms/common/api/v;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/gms/udc/c/i;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/udc/e/f;)V

    .line 256
    const-wide/16 v2, 0xa

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/gms/udc/c/i;->a(JLjava/util/concurrent/TimeUnit;)V

    .line 258
    return-object v1
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 0

    .prologue
    .line 275
    return-void
.end method

.method public final synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 241
    check-cast p2, Lcom/google/android/gms/udc/f;

    invoke-interface {p2}, Lcom/google/android/gms/udc/f;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p2}, Lcom/google/android/gms/udc/f;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/udc/e/g;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/y;->a:Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;

    invoke-static {v1}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->c(Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;)Lcom/google/android/gms/analytics/bv;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/analytics/bi;

    invoke-direct {v2}, Lcom/google/android/gms/analytics/bi;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/gms/analytics/bi;->a()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/analytics/bv;->a(Ljava/util/Map;)V

    iget-object v1, v0, Lcom/google/android/gms/udc/e/g;->b:[Lcom/google/android/gms/udc/e/m;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/udc/e/g;->b:[Lcom/google/android/gms/udc/e/m;

    array-length v1, v1

    if-nez v1, :cond_1

    :cond_0
    new-instance v0, Lcom/google/android/gms/udc/ui/g;

    invoke-direct {v0}, Lcom/google/android/gms/udc/ui/g;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/y;->a:Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;

    sget v2, Lcom/google/android/gms/p;->za:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/ui/g;->a(Ljava/lang/CharSequence;)Lcom/google/android/gms/udc/ui/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/udc/ui/g;->b()Lcom/google/android/gms/udc/ui/j;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/udc/ui/y;->a:Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;

    invoke-static {v1}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->a(Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;)Lcom/google/android/gms/udc/util/j;

    move-result-object v1

    sget v2, Lcom/google/android/gms/j;->gq:I

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/udc/util/j;->a(ILandroid/support/v4/app/Fragment;)V

    :goto_1
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/udc/ui/y;->a:Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;

    invoke-static {v1}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->d(Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/udc/ui/z;->a(Ljava/lang/String;Lcom/google/android/gms/udc/e/g;)Lcom/google/android/gms/udc/ui/z;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-interface {p2}, Lcom/google/android/gms/udc/f;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    const-string v3, "UdcSettingsListActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Error reading the config data:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    const-string v3, "UdcSettingsListActivity"

    const-string v4, "Unknown statuscode:%d"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v0, v1

    invoke-static {v4, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/udc/ui/y;->a:Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;

    sget v3, Lcom/google/android/gms/p;->yU:I

    invoke-virtual {v0, v3}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move v7, v1

    move-object v1, v0

    move v0, v7

    :goto_2
    iget-object v3, p0, Lcom/google/android/gms/udc/ui/y;->a:Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;

    invoke-static {v3}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->c(Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;)Lcom/google/android/gms/analytics/bv;

    move-result-object v3

    const-string v4, "LoadOverviewConfig"

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v2

    invoke-static {v3, v4, v2}, Lcom/google/android/gms/udc/util/a;->a(Lcom/google/android/gms/analytics/bv;Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/y;->a:Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;

    invoke-static {v2}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->a(Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;)Lcom/google/android/gms/udc/util/j;

    move-result-object v2

    sget v3, Lcom/google/android/gms/j;->gq:I

    new-instance v4, Lcom/google/android/gms/udc/ui/g;

    invoke-direct {v4}, Lcom/google/android/gms/udc/ui/g;-><init>()V

    iget-object v5, p0, Lcom/google/android/gms/udc/ui/y;->a:Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;

    sget v6, Lcom/google/android/gms/p;->yS:I

    invoke-virtual {v5, v6}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/udc/ui/g;->a(Ljava/lang/CharSequence;)Lcom/google/android/gms/udc/ui/g;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/google/android/gms/udc/ui/g;->b(Ljava/lang/CharSequence;)Lcom/google/android/gms/udc/ui/g;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/udc/ui/g;->a(Z)Lcom/google/android/gms/udc/ui/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/udc/ui/g;->b()Lcom/google/android/gms/udc/ui/j;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/udc/util/j;->a(ILandroid/support/v4/app/Fragment;)V

    goto/16 :goto_1

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/gms/udc/ui/y;->a:Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;

    sget v3, Lcom/google/android/gms/p;->zd:I

    invoke-virtual {v1, v3}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/gms/udc/ui/y;->a:Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;

    sget v3, Lcom/google/android/gms/p;->yY:I

    invoke-virtual {v1, v3}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/gms/udc/ui/y;->a:Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;

    sget v3, Lcom/google/android/gms/p;->yO:I

    invoke-virtual {v1, v3}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/y;->a:Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;

    sget v3, Lcom/google/android/gms/p;->yU:I

    invoke-virtual {v0, v3}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move v7, v1

    move-object v1, v0

    move v0, v7

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1196
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
