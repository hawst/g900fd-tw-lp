.class final Lcom/google/android/gms/ads/a;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/ads/AdRequestBrokerService;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/ads/AdRequestBrokerService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/gms/ads/a;->a:Lcom/google/android/gms/ads/AdRequestBrokerService;

    .line 43
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 44
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 53
    :try_start_0
    iget-object v8, p0, Lcom/google/android/gms/ads/a;->a:Lcom/google/android/gms/ads/AdRequestBrokerService;

    new-instance v0, Lcom/google/android/gms/ads/internal/d/a;

    sget-object v1, Lcom/google/android/gms/ads/a/e;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/ads/a/e;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/ads/a/c;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    sget-object v4, Lcom/google/android/gms/ads/a/g;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    sget-object v5, Lcom/google/android/gms/ads/a/d;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v5}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    sget-object v6, Lcom/google/android/gms/ads/a/d;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v6}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/ads/settings/c/f;->c()I

    move-result v7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/ads/internal/d/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V

    iget-object v1, p0, Lcom/google/android/gms/ads/a;->a:Lcom/google/android/gms/ads/AdRequestBrokerService;

    const-string v1, "Location will be disabled for ad requests."

    invoke-static {v1}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/gms/ads/internal/j/b;

    invoke-direct {v1}, Lcom/google/android/gms/ads/internal/j/b;-><init>()V

    new-instance v2, Lcom/google/android/gms/ads/social/c;

    iget-object v3, p0, Lcom/google/android/gms/ads/a;->a:Lcom/google/android/gms/ads/AdRequestBrokerService;

    invoke-direct {v2, v3}, Lcom/google/android/gms/ads/social/c;-><init>(Landroid/content/Context;)V

    invoke-static {v8, v0, v1, v2}, Lcom/google/android/gms/ads/internal/request/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/d/a;Lcom/google/android/gms/ads/internal/j/a;Lcom/google/android/gms/ads/internal/n/a;)Lcom/google/android/gms/ads/internal/request/a/a;

    move-result-object v0

    .line 58
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {p1, v1, v0, v2}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :goto_0
    return-void

    .line 59
    :catch_0
    move-exception v0

    .line 60
    const-string v1, "Client died while brokering the ad request service."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
