.class final Lcom/google/android/gms/games/ui/client/players/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/api/r;

.field final synthetic b:Lcom/google/android/gms/common/api/r;

.field final synthetic c:Lcom/google/android/gms/common/api/r;

.field final synthetic d:Lcom/google/android/gms/common/api/r;

.field final synthetic e:Lcom/google/android/gms/common/api/r;

.field final synthetic f:Lcom/google/android/gms/games/ui/client/players/q;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/client/players/q;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;)V
    .locals 0

    .prologue
    .line 365
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    iput-object p2, p0, Lcom/google/android/gms/games/ui/client/players/s;->a:Lcom/google/android/gms/common/api/r;

    iput-object p3, p0, Lcom/google/android/gms/games/ui/client/players/s;->b:Lcom/google/android/gms/common/api/r;

    iput-object p4, p0, Lcom/google/android/gms/games/ui/client/players/s;->c:Lcom/google/android/gms/common/api/r;

    iput-object p5, p0, Lcom/google/android/gms/games/ui/client/players/s;->d:Lcom/google/android/gms/common/api/r;

    iput-object p6, p0, Lcom/google/android/gms/games/ui/client/players/s;->e:Lcom/google/android/gms/common/api/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 13

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 365
    check-cast p1, Lcom/google/android/gms/common/api/q;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/q;->b(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->B()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/q;->c(Lcom/google/android/gms/games/ui/client/players/q;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/q;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/q;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/games/ui/d/al;->b(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/q;->e()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/q;->d(Lcom/google/android/gms/games/ui/client/players/q;)Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/q;->isDetached()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/q;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/q;->e(Lcom/google/android/gms/games/ui/client/players/q;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/s;->a:Lcom/google/android/gms/common/api/r;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/s;->a:Lcom/google/android/gms/common/api/r;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/z;

    invoke-interface {v0}, Lcom/google/android/gms/games/z;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v1

    move-object v3, v0

    move v4, v1

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/s;->b:Lcom/google/android/gms/common/api/r;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/z;

    invoke-interface {v0}, Lcom/google/android/gms/games/z;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v9

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/s;->c:Lcom/google/android/gms/common/api/r;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/ap;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/z;

    invoke-interface {v1}, Lcom/google/android/gms/games/z;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v10

    iget-object v7, p0, Lcom/google/android/gms/games/ui/client/players/s;->d:Lcom/google/android/gms/common/api/r;

    if-eqz v7, :cond_18

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->d:Lcom/google/android/gms/common/api/r;

    invoke-virtual {p1, v2}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/ap;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/z;

    invoke-interface {v2}, Lcom/google/android/gms/games/z;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v7

    move-object v8, v2

    move v2, v7

    :goto_2
    :try_start_0
    iget-object v7, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v7}, Lcom/google/android/gms/games/ui/client/players/q;->f(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/q;

    move-result-object v7

    invoke-virtual {v7, v4}, Lcom/google/android/gms/games/ui/q;->b(I)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/client/players/q;->g(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/q;

    move-result-object v4

    invoke-virtual {v4, v9}, Lcom/google/android/gms/games/ui/q;->b(I)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/client/players/q;->h(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/q;

    move-result-object v4

    invoke-virtual {v4, v10}, Lcom/google/android/gms/games/ui/q;->b(I)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/client/players/q;->i(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/q;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/gms/games/ui/q;->b(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    if-eqz v3, :cond_5

    invoke-interface {v3}, Lcom/google/android/gms/games/z;->c()Lcom/google/android/gms/games/t;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/t;->w_()V

    :cond_5
    invoke-interface {v0}, Lcom/google/android/gms/games/z;->c()Lcom/google/android/gms/games/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/t;->w_()V

    invoke-interface {v1}, Lcom/google/android/gms/games/z;->c()Lcom/google/android/gms/games/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/t;->w_()V

    if-eqz v8, :cond_2

    invoke-interface {v8}, Lcom/google/android/gms/games/z;->c()Lcom/google/android/gms/games/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/t;->w_()V

    goto/16 :goto_0

    :cond_6
    :try_start_1
    invoke-static {v10}, Lcom/google/android/gms/games/ui/d/al;->a(I)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->a(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/client/players/i;->e()V

    :cond_7
    if-eqz v8, :cond_8

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-interface {v8}, Lcom/google/android/gms/games/z;->c()Lcom/google/android/gms/games/t;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/gms/games/ui/client/players/q;->a(Lcom/google/android/gms/games/ui/client/players/q;Lcom/google/android/gms/common/data/d;)Lcom/google/android/gms/common/data/d;

    :goto_3
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->j(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/google/android/gms/games/ui/client/players/i;->c(Z)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->j(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/google/android/gms/games/ui/client/players/i;->d(Z)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->j(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/client/players/q;->k(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/common/data/d;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/games/ui/client/players/i;->a(Lcom/google/android/gms/common/data/d;)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->j(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/google/android/gms/games/ui/client/players/i;->a(Z)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->k(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/common/data/d;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v2

    if-lez v2, :cond_b

    move v2, v6

    :goto_4
    or-int/lit8 v7, v2, 0x0

    if-eqz v3, :cond_c

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-interface {v3}, Lcom/google/android/gms/games/z;->c()Lcom/google/android/gms/games/t;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/gms/games/ui/client/players/q;->b(Lcom/google/android/gms/games/ui/client/players/q;Lcom/google/android/gms/common/data/d;)Lcom/google/android/gms/common/data/d;

    :goto_5
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->l(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/google/android/gms/games/ui/client/players/i;->c(Z)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->m(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/common/data/d;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v2

    if-lez v2, :cond_17

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->l(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/client/players/q;->n(Lcom/google/android/gms/games/ui/client/players/q;)Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/games/ui/client/players/i;->c(Z)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->l(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/client/players/q;->o(Lcom/google/android/gms/games/ui/client/players/q;)Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/games/ui/client/players/i;->d(Z)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/client/players/q;->l(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/gms/games/ui/client/players/q;->a(Lcom/google/android/gms/games/ui/client/players/q;Lcom/google/android/gms/games/ui/client/players/i;)Lcom/google/android/gms/games/ui/client/players/i;

    move v4, v6

    :goto_6
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->l(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;

    move-result-object v2

    iget-object v9, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v9}, Lcom/google/android/gms/games/ui/client/players/q;->m(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/common/data/d;

    move-result-object v9

    invoke-virtual {v2, v9}, Lcom/google/android/gms/games/ui/client/players/i;->a(Lcom/google/android/gms/common/data/d;)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->m(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/common/data/d;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v2

    if-lez v2, :cond_d

    move v2, v6

    :goto_7
    or-int v9, v7, v2

    invoke-interface {v0}, Lcom/google/android/gms/games/z;->c()Lcom/google/android/gms/games/t;

    move-result-object v2

    iget-object v7, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    new-instance v11, Lcom/google/android/gms/common/data/s;

    const-string v12, "external_player_id"

    invoke-direct {v11, v2, v12}, Lcom/google/android/gms/common/data/s;-><init>(Lcom/google/android/gms/common/data/a;Ljava/lang/String;)V

    invoke-static {v7, v11}, Lcom/google/android/gms/games/ui/client/players/q;->a(Lcom/google/android/gms/games/ui/client/players/q;Lcom/google/android/gms/common/data/s;)Lcom/google/android/gms/common/data/s;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->m(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/common/data/d;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v11

    move v7, v5

    :goto_8
    if-ge v7, v11, :cond_e

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->m(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/common/data/d;

    move-result-object v2

    invoke-interface {v2, v7}, Lcom/google/android/gms/common/data/d;->a(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/Player;

    iget-object v12, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v12}, Lcom/google/android/gms/games/ui/client/players/q;->p(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/common/data/s;

    move-result-object v12

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Lcom/google/android/gms/common/data/s;->a(Ljava/lang/String;)V

    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_8

    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    new-instance v4, Lcom/google/android/gms/common/data/w;

    invoke-direct {v4}, Lcom/google/android/gms/common/data/w;-><init>()V

    invoke-static {v2, v4}, Lcom/google/android/gms/games/ui/client/players/q;->a(Lcom/google/android/gms/games/ui/client/players/q;Lcom/google/android/gms/common/data/d;)Lcom/google/android/gms/common/data/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_3

    :catchall_0
    move-exception v2

    if-eqz v3, :cond_9

    invoke-interface {v3}, Lcom/google/android/gms/games/z;->c()Lcom/google/android/gms/games/t;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/games/t;->w_()V

    :cond_9
    invoke-interface {v0}, Lcom/google/android/gms/games/z;->c()Lcom/google/android/gms/games/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/t;->w_()V

    invoke-interface {v1}, Lcom/google/android/gms/games/z;->c()Lcom/google/android/gms/games/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/t;->w_()V

    if-eqz v8, :cond_a

    invoke-interface {v8}, Lcom/google/android/gms/games/z;->c()Lcom/google/android/gms/games/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/t;->w_()V

    :cond_a
    throw v2

    :cond_b
    move v2, v5

    goto/16 :goto_4

    :cond_c
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    new-instance v4, Lcom/google/android/gms/common/data/w;

    invoke-direct {v4}, Lcom/google/android/gms/common/data/w;-><init>()V

    invoke-static {v2, v4}, Lcom/google/android/gms/games/ui/client/players/q;->b(Lcom/google/android/gms/games/ui/client/players/q;Lcom/google/android/gms/common/data/d;)Lcom/google/android/gms/common/data/d;

    goto/16 :goto_5

    :cond_d
    move v2, v5

    goto :goto_7

    :cond_e
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->q(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;

    move-result-object v2

    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Lcom/google/android/gms/games/ui/client/players/i;->c(Z)V

    if-nez v4, :cond_f

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->p(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/common/data/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/s;->c()I

    move-result v2

    if-lez v2, :cond_f

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->q(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/client/players/q;->n(Lcom/google/android/gms/games/ui/client/players/q;)Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/games/ui/client/players/i;->c(Z)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->q(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/client/players/q;->o(Lcom/google/android/gms/games/ui/client/players/q;)Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/games/ui/client/players/i;->d(Z)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/client/players/q;->q(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/gms/games/ui/client/players/q;->a(Lcom/google/android/gms/games/ui/client/players/q;Lcom/google/android/gms/games/ui/client/players/i;)Lcom/google/android/gms/games/ui/client/players/i;

    move v4, v6

    :cond_f
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->q(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;

    move-result-object v2

    iget-object v7, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v7}, Lcom/google/android/gms/games/ui/client/players/q;->p(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/common/data/s;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/google/android/gms/games/ui/client/players/i;->a(Lcom/google/android/gms/common/data/d;)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->p(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/common/data/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/s;->c()I

    move-result v2

    if-lez v2, :cond_11

    move v2, v6

    :goto_9
    or-int v7, v9, v2

    invoke-interface {v1}, Lcom/google/android/gms/games/z;->c()Lcom/google/android/gms/games/t;

    move-result-object v2

    iget-object v9, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    new-instance v11, Lcom/google/android/gms/common/data/ah;

    const-string v12, "profile_name"

    invoke-direct {v11, v2, v12}, Lcom/google/android/gms/common/data/ah;-><init>(Lcom/google/android/gms/common/data/a;Ljava/lang/String;)V

    invoke-static {v9, v11}, Lcom/google/android/gms/games/ui/client/players/q;->a(Lcom/google/android/gms/games/ui/client/players/q;Lcom/google/android/gms/common/data/ah;)Lcom/google/android/gms/common/data/ah;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->a(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;

    move-result-object v2

    const/4 v9, 0x0

    invoke-virtual {v2, v9}, Lcom/google/android/gms/games/ui/client/players/i;->c(Z)V

    if-nez v4, :cond_10

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->a(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/client/players/q;->n(Lcom/google/android/gms/games/ui/client/players/q;)Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/games/ui/client/players/i;->c(Z)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->a(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/client/players/q;->o(Lcom/google/android/gms/games/ui/client/players/q;)Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/games/ui/client/players/i;->d(Z)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/client/players/q;->a(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/gms/games/ui/client/players/q;->a(Lcom/google/android/gms/games/ui/client/players/q;Lcom/google/android/gms/games/ui/client/players/i;)Lcom/google/android/gms/games/ui/client/players/i;

    :cond_10
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->a(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/client/players/q;->r(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/common/data/ah;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/games/ui/client/players/i;->a(Lcom/google/android/gms/common/data/d;)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->r(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/common/data/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/ah;->c()I

    move-result v2

    if-lez v2, :cond_12

    move v2, v6

    :goto_a
    or-int v4, v7, v2

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->e:Lcom/google/android/gms/common/api/r;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->e:Lcom/google/android/gms/common/api/r;

    invoke-virtual {p1, v2}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/ap;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/z;

    invoke-interface {v2}, Lcom/google/android/gms/games/z;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v6

    if-nez v6, :cond_14

    invoke-interface {v2}, Lcom/google/android/gms/games/z;->c()Lcom/google/android/gms/games/t;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v6

    :try_start_3
    invoke-virtual {v6}, Lcom/google/android/gms/games/t;->c()I

    move-result v7

    :goto_b
    if-ge v5, v7, :cond_13

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->b(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    move-result-object v9

    invoke-virtual {v6, v5}, Lcom/google/android/gms/games/t;->b(I)Lcom/google/android/gms/games/Player;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/Player;

    const/4 v11, 0x1

    invoke-virtual {v9, v2, v11}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->b(Lcom/google/android/gms/games/Player;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    add-int/lit8 v5, v5, 0x1

    goto :goto_b

    :cond_11
    move v2, v5

    goto/16 :goto_9

    :cond_12
    move v2, v5

    goto :goto_a

    :cond_13
    :try_start_4
    invoke-virtual {v6}, Lcom/google/android/gms/games/t;->w_()V

    :cond_14
    if-nez v4, :cond_15

    if-nez v10, :cond_16

    :cond_15
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->s(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/d/t;

    move-result-object v2

    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Lcom/google/android/gms/games/ui/d/t;->a(I)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/client/players/q;->k()V

    :goto_c
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->b(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->y()V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->b(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->C()V

    goto/16 :goto_0

    :catchall_1
    move-exception v2

    invoke-virtual {v6}, Lcom/google/android/gms/games/t;->w_()V

    throw v2

    :cond_16
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/s;->f:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/client/players/q;->t(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/d/t;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/google/android/gms/games/ui/d/t;->c(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_c

    :cond_17
    move v4, v5

    goto/16 :goto_6

    :cond_18
    move-object v8, v2

    move v2, v5

    goto/16 :goto_2

    :cond_19
    move-object v3, v2

    move v4, v5

    goto/16 :goto_1
.end method
