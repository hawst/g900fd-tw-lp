.class public final Lcom/google/android/gms/games/ui/signin/SignInActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/i/q;
.implements Lcom/google/android/gms/games/i/r;


# static fields
.field static final a:Ljava/util/HashMap;

.field static final b:Ljava/util/ArrayList;


# instance fields
.field private c:Lcom/google/android/gms/games/i/b;

.field private d:Landroid/view/View;

.field private e:Z

.field private f:Z

.field private g:I

.field private h:I

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:[Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Z

.field private p:Ljava/lang/String;

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:I

.field private u:I

.field private v:Lcom/google/android/gms/common/api/Status;

.field private w:I

.field private x:J

.field private y:J


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0xa

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 163
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a:Ljava/util/HashMap;

    .line 170
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Integer;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Integer;[Ljava/lang/Integer;)V

    .line 171
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/Integer;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    const/4 v2, 0x3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Integer;[Ljava/lang/Integer;)V

    .line 173
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Integer;[Ljava/lang/Integer;)V

    .line 175
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Integer;[Ljava/lang/Integer;)V

    .line 177
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    const/4 v2, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Integer;[Ljava/lang/Integer;)V

    .line 179
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/Integer;

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    const/4 v2, 0x3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Integer;[Ljava/lang/Integer;)V

    .line 181
    const/4 v0, 0x6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/Integer;

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    const/4 v2, 0x3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Integer;[Ljava/lang/Integer;)V

    .line 183
    const/4 v0, 0x7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Integer;

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    const/4 v2, 0x3

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Integer;[Ljava/lang/Integer;)V

    .line 185
    const/16 v0, 0x8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Integer;

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Integer;[Ljava/lang/Integer;)V

    .line 186
    const/16 v0, 0x9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Integer;[Ljava/lang/Integer;)V

    .line 187
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Integer;[Ljava/lang/Integer;)V

    .line 191
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 193
    sput-object v0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b:Ljava/util/ArrayList;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    sget-object v0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b:Ljava/util/ArrayList;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 195
    sget-object v0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b:Ljava/util/ArrayList;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 196
    sget-object v0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b:Ljava/util/ArrayList;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 197
    sget-object v0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b:Ljava/util/ArrayList;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    sget-object v0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b:Ljava/util/ArrayList;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 199
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 132
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    .line 248
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->o:Z

    .line 255
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->q:Z

    .line 258
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->r:Z

    .line 262
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->s:Z

    .line 265
    const/16 v0, 0x11

    iput v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->t:I

    .line 269
    const/16 v0, 0x2712

    iput v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->u:I

    .line 276
    iput v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->w:I

    return-void
.end method

.method private a(Ljava/lang/Class;)V
    .locals 4

    .prologue
    .line 772
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    .line 775
    sget v0, Lcom/google/android/gms/j;->gr:I

    invoke-virtual {v1, v0}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 776
    if-eqz v0, :cond_0

    instance-of v2, v0, Lcom/google/android/gms/games/ui/signin/g;

    if-eqz v2, :cond_0

    .line 777
    check-cast v0, Lcom/google/android/gms/games/ui/signin/g;

    .line 778
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/g;->a()I

    move-result v2

    iget v3, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->g:I

    if-ne v2, v3, :cond_0

    .line 779
    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->c:Lcom/google/android/gms/games/i/b;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/signin/g;->b(Lcom/google/android/gms/games/i/b;)V

    .line 797
    :goto_0
    return-void

    .line 785
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->o()V

    .line 788
    :try_start_0
    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    .line 789
    sget v2, Lcom/google/android/gms/j;->gr:I

    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 790
    invoke-virtual {v1}, Landroid/support/v4/app/aj;->a()I
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 791
    :catch_0
    move-exception v0

    .line 793
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 794
    :catch_1
    move-exception v0

    .line 796
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static varargs a(Ljava/lang/Integer;[Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 167
    sget-object v0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a:Ljava/util/HashMap;

    new-instance v1, Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    return-void
.end method

.method private q()Z
    .locals 2

    .prologue
    .line 385
    const-string v0, "com.google.android.play.games"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private r()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 566
    const-string v0, "SignInActivity"

    const-string v3, "onSignInFailed()..."

    invoke-static {v0, v3}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    const-string v0, "SignInActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "==> Returning non-OK result: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->u:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    const/16 v0, 0xe

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(II)V

    .line 571
    invoke-static {}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->b()Ljava/lang/String;

    move-result-object v0

    .line 572
    invoke-static {}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->a()Z

    move-result v3

    if-nez v3, :cond_0

    .line 573
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to connect to server ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 577
    :cond_0
    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->u:I

    const/16 v3, 0x4e20

    if-ne v0, v3, :cond_1

    sget v0, Lcom/google/android/gms/p;->hH:I

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const-string v0, "SignInActivity"

    const-string v3, "No account on this device can access the Games APIs"

    invoke-static {v0, v3}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x2712

    :cond_1
    iput v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->u:I

    .line 578
    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->u:I

    const/4 v3, -0x1

    if-eq v0, v3, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 579
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->v:Lcom/google/android/gms/common/api/Status;

    if-nez v0, :cond_3

    .line 580
    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->u:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->setResult(I)V

    .line 586
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->finish()V

    .line 587
    return-void

    :cond_2
    move v0, v2

    .line 578
    goto :goto_0

    .line 582
    :cond_3
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 583
    const-string v1, "status"

    iget-object v2, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->v:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 584
    iget v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->u:I

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_1
.end method

.method private s()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 701
    const-string v0, "SignInActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Transition from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->h:I

    iput v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->g:I

    .line 705
    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->g:I

    packed-switch v0, :pswitch_data_0

    .line 765
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown state to be transitioning to: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 707
    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->r:Z

    if-nez v0, :cond_0

    .line 708
    invoke-virtual {p0, v4}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b(I)V

    .line 768
    :goto_0
    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->i:Z

    .line 769
    return-void

    .line 710
    :cond_0
    iput-boolean v4, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->e:Z

    .line 711
    const-class v0, Lcom/google/android/gms/games/ui/signin/e;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Class;)V

    goto :goto_0

    .line 716
    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->q:Z

    if-eqz v0, :cond_3

    .line 717
    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->q:Z

    .line 718
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->n:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 720
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b(I)V

    goto :goto_0

    .line 721
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->s:Z

    if-eqz v0, :cond_2

    .line 724
    invoke-virtual {p0, v5}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b(I)V

    goto :goto_0

    .line 726
    :cond_2
    const-class v0, Lcom/google/android/gms/games/ui/signin/b;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Class;)V

    goto :goto_0

    .line 728
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->o:Z

    if-eqz v0, :cond_4

    .line 732
    const/4 v0, 0x0

    invoke-virtual {p0, v3, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(ILcom/google/android/gms/common/api/Status;)V

    goto :goto_0

    .line 734
    :cond_4
    invoke-virtual {p0, v5}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b(I)V

    goto :goto_0

    .line 738
    :pswitch_2
    const-class v0, Lcom/google/android/gms/games/ui/signin/a;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Class;)V

    goto :goto_0

    .line 741
    :pswitch_3
    const-class v0, Lcom/google/android/gms/games/ui/signin/k;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Class;)V

    goto :goto_0

    .line 744
    :pswitch_4
    const-class v0, Lcom/google/android/gms/games/ui/signin/i;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Class;)V

    goto :goto_0

    .line 747
    :pswitch_5
    const-class v0, Lcom/google/android/gms/games/ui/signin/c;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Class;)V

    goto :goto_0

    .line 750
    :pswitch_6
    const-class v0, Lcom/google/android/gms/games/ui/signin/d;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Class;)V

    goto :goto_0

    .line 753
    :pswitch_7
    const-class v0, Lcom/google/android/gms/games/ui/signin/h;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Class;)V

    goto :goto_0

    .line 756
    :pswitch_8
    const-class v0, Lcom/google/android/gms/games/ui/signin/j;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/Class;)V

    goto :goto_0

    .line 759
    :pswitch_9
    const/16 v0, 0xd

    invoke-virtual {p0, v0, v3}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(II)V

    const-string v0, "games.oob_state"

    invoke-virtual {p0, v0, v3}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "oob_complete"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->finish()V

    goto :goto_0

    .line 762
    :pswitch_a
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->r()V

    goto/16 :goto_0

    .line 705
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 611
    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->h:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->f:Z

    if-eqz v0, :cond_0

    .line 612
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->s()V

    .line 614
    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 10

    .prologue
    .line 986
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->y:J

    sub-long v8, v0, v2

    .line 987
    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->n:Ljava/lang/String;

    iget-wide v4, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->x:J

    move-object v0, p0

    move v6, p1

    move v7, p2

    invoke-static/range {v0 .. v9}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JIIJ)V

    .line 989
    return-void
.end method

.method public final a(ILcom/google/android/gms/common/api/Status;)V
    .locals 1

    .prologue
    .line 695
    iput p1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->u:I

    .line 696
    iput-object p2, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->v:Lcom/google/android/gms/common/api/Status;

    .line 697
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b(I)V

    .line 698
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 867
    iput-object p1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->n:Ljava/lang/String;

    .line 868
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 886
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->o:Z

    .line 887
    return-void
.end method

.method public final a(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 634
    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->c:Lcom/google/android/gms/games/i/b;

    invoke-virtual {v1}, Lcom/google/android/gms/games/i/b;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 639
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->g:I

    if-ne v1, p1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->i:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 621
    const-string v0, "SignInActivity"

    const-string v1, "Failed to connect to sign-in service"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->r()V

    .line 623
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 655
    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a:Ljava/util/HashMap;

    iget v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid transition: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput p1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->h:I

    .line 656
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->i:Z

    .line 661
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->c:Lcom/google/android/gms/games/i/b;

    if-nez v0, :cond_2

    .line 673
    :cond_1
    :goto_0
    return-void

    .line 666
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->c:Lcom/google/android/gms/games/i/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/i/b;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 667
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->c:Lcom/google/android/gms/games/i/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/i/b;->a()V

    goto :goto_0

    .line 672
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->s()V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 902
    iput-object p1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->p:Ljava/lang/String;

    .line 903
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 972
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(II)V

    .line 973
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 394
    const-string v0, "593950602418"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lcom/google/android/gms/games/i/b;
    .locals 2

    .prologue
    .line 515
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->c:Lcom/google/android/gms/games/i/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/i/b;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 516
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Sign-in client not connected!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 518
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->c:Lcom/google/android/gms/games/i/b;

    return-object v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 684
    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->w:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->w:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 691
    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->w:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 833
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 843
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final i()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 851
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->m:[Ljava/lang/String;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 858
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 875
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->o:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 893
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 909
    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->t:I

    return v0
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 917
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->d:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 918
    return-void
.end method

.method public final o()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 925
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->e:Z

    if-eqz v0, :cond_0

    .line 926
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->e:Z

    .line 930
    :goto_0
    return-void

    .line 928
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 993
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    .line 994
    instance-of v0, p1, Lcom/google/android/gms/games/ui/b/b;

    if-eqz v0, :cond_0

    .line 996
    check-cast p1, Lcom/google/android/gms/games/ui/b/b;

    invoke-virtual {p1, p0}, Lcom/google/android/gms/games/ui/b/b;->a(Landroid/content/Context;)V

    .line 998
    :cond_0
    return-void
.end method

.method public final onBackPressed()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 802
    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->g:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->g:I

    if-ne v0, v1, :cond_2

    .line 804
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/q;->onBackPressed()V

    .line 809
    :cond_1
    :goto_0
    return-void

    .line 805
    :cond_2
    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->g:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_1
    if-nez v0, :cond_1

    .line 806
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b(I)V

    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 805
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/16 v3, 0x9

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 286
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 287
    sget v0, Lcom/google/android/gms/l;->bL:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->setContentView(I)V

    .line 288
    sget v0, Lcom/google/android/gms/j;->ll:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->d:Landroid/view/View;

    .line 290
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    .line 295
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->k:Ljava/lang/String;

    .line 296
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->k:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 297
    const-string v0, "SignInActivity"

    const-string v1, "SignInActivity must be started with startActivityForResult"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->finish()V

    .line 362
    :goto_0
    return-void

    .line 305
    :cond_0
    const-string v0, "com.google.android.gms.games.GAME_ID"

    invoke-virtual {v5, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j:Ljava/lang/String;

    .line 307
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 308
    const-string v0, "SignInActivity"

    const-string v1, "You must pass a game ID in the GamesIntents.EXTRA_GAME_ID extra."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->finish()V

    goto :goto_0

    .line 317
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->getRequestedOrientation()I

    move-result v6

    sget-object v0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b:Ljava/util/ArrayList;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v4, Landroid/util/DisplayMetrics;

    invoke-direct {v4}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {v0, v4}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v7

    iget v8, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v9, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    const/16 v0, 0x8

    invoke-static {v3}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v4

    if-nez v4, :cond_14

    move v0, v1

    move v3, v2

    :goto_1
    if-eqz v7, :cond_2

    const/4 v4, 0x2

    if-ne v7, v4, :cond_9

    :cond_2
    move v4, v1

    :goto_2
    if-eqz v4, :cond_3

    if-gt v8, v9, :cond_4

    :cond_3
    if-nez v4, :cond_a

    if-le v9, v8, :cond_a

    :cond_4
    move v4, v1

    :goto_3
    packed-switch v7, :pswitch_data_0

    const/4 v0, 0x5

    :cond_5
    :goto_4
    if-eq v6, v0, :cond_6

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->setRequestedOrientation(I)V

    .line 319
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_10

    .line 320
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->k:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    const-string v0, "593950602418"

    iget-object v3, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "232243143311"

    iget-object v3, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    :cond_7
    move v0, v1

    :goto_5
    if-nez v0, :cond_8

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->q()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_8
    :goto_6
    if-nez v1, :cond_f

    .line 322
    const-string v0, "SignInActivity"

    const-string v1, "Invalid (empty) game ID found in the EXTRA_GAME_ID extra."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->finish()V

    goto/16 :goto_0

    :cond_9
    move v4, v2

    .line 317
    goto :goto_2

    :cond_a
    move v4, v2

    goto :goto_3

    :pswitch_0
    if-eqz v4, :cond_b

    move v0, v1

    goto :goto_4

    :cond_b
    move v0, v2

    goto :goto_4

    :pswitch_1
    if-eqz v4, :cond_5

    move v0, v2

    goto :goto_4

    :pswitch_2
    if-nez v4, :cond_5

    move v0, v3

    goto :goto_4

    :pswitch_3
    if-eqz v4, :cond_c

    :goto_7
    move v0, v3

    goto :goto_4

    :cond_c
    move v3, v1

    goto :goto_7

    :cond_d
    move v0, v2

    .line 320
    goto :goto_5

    :cond_e
    move v1, v2

    goto :goto_6

    .line 326
    :cond_f
    const-string v0, "com.google.android.gms"

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->k:Ljava/lang/String;

    .line 330
    :cond_10
    const-string v0, "com.google.android.gms.games.USE_DESIRED_ACCOUNT_NAME"

    invoke-virtual {v5, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->o:Z

    .line 333
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->i:Z

    .line 334
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->g:I

    .line 335
    iput v2, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->h:I

    .line 336
    if-eqz p1, :cond_11

    .line 337
    const-string v0, "account_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->n:Ljava/lang/String;

    .line 338
    const-string v0, "player_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->p:Ljava/lang/String;

    .line 339
    const-string v0, "desired_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->h:I

    .line 340
    const-string v0, "account_selector_bypassed"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->q:Z

    .line 341
    const-string v0, "failure_result_code"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->u:I

    .line 344
    :cond_11
    const-string v0, "com.google.android.gms.games.SCOPES"

    invoke-virtual {v5, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->m:[Ljava/lang/String;

    .line 345
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->m:[Ljava/lang/String;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->m:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_13

    .line 346
    :cond_12
    const-string v0, "SignInActivity"

    const-string v1, "Must specify at least one scope in order to sign in!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->finish()V

    goto/16 :goto_0

    .line 351
    :cond_13
    const-string v0, "com.google.android.gms.games.SHOW_CONNECTING_POPUP"

    invoke-virtual {v5, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->r:Z

    .line 353
    const-string v0, "com.google.android.gms.games.CONNECTING_POPUP_GRAVITY"

    const/16 v1, 0x11

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->t:I

    .line 355
    const-string v0, "com.google.android.gms.games.RETRYING_SIGN_IN"

    invoke-virtual {v5, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->s:Z

    .line 358
    new-instance v0, Lcom/google/android/gms/games/i/b;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->k:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->m:[Ljava/lang/String;

    move-object v1, p0

    move-object v4, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/i/b;-><init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/gms/games/i/q;Lcom/google/android/gms/games/i/r;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->c:Lcom/google/android/gms/games/i/b;

    .line 360
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->x:J

    .line 361
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->y:J

    goto/16 :goto_0

    :cond_14
    move v10, v3

    move v3, v0

    move v0, v10

    goto/16 :goto_1

    .line 317
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected final onPause()V
    .locals 1

    .prologue
    .line 485
    invoke-super {p0}, Landroid/support/v4/app/q;->onPause()V

    .line 486
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->f:Z

    .line 487
    return-void
.end method

.method protected final onResume()V
    .locals 1

    .prologue
    .line 473
    invoke-super {p0}, Landroid/support/v4/app/q;->onResume()V

    .line 474
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->f:Z

    .line 477
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->c:Lcom/google/android/gms/games/i/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->c:Lcom/google/android/gms/games/i/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/i/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->h:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->i:Z

    if-eqz v0, :cond_0

    .line 479
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->s()V

    .line 481
    :cond_0
    return-void
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 501
    const-string v0, "account_name"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    const-string v0, "player_id"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    const-string v0, "desired_state"

    iget v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->h:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 504
    const-string v0, "account_selector_bypassed"

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->q:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 505
    const-string v0, "failure_result_code"

    iget v1, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->u:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 506
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 507
    return-void
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 463
    invoke-super {p0}, Landroid/support/v4/app/q;->onStart()V

    .line 466
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->c:Lcom/google/android/gms/games/i/b;

    if-eqz v0, :cond_0

    .line 467
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->c:Lcom/google/android/gms/games/i/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/i/b;->a()V

    .line 469
    :cond_0
    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 491
    invoke-super {p0}, Landroid/support/v4/app/q;->onStop()V

    .line 494
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->c:Lcom/google/android/gms/games/i/b;

    if-eqz v0, :cond_0

    .line 495
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->c:Lcom/google/android/gms/games/i/b;

    iput-object v3, v0, Lcom/google/android/gms/games/i/b;->f:Lcom/google/android/gms/games/internal/ea;

    iget-object v1, v0, Lcom/google/android/gms/games/i/b;->g:Lcom/google/android/gms/games/i/x;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/games/i/b;->a:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/android/gms/games/i/b;->g:Lcom/google/android/gms/games/i/x;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    iput-object v3, v0, Lcom/google/android/gms/games/i/b;->g:Lcom/google/android/gms/games/i/x;

    .line 497
    :cond_0
    return-void
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 938
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/SignInActivity;->e:Z

    .line 939
    return-void
.end method
