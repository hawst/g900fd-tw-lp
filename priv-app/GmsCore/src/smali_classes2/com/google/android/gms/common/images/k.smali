.class public abstract Lcom/google/android/gms/common/images/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/common/images/l;

.field protected b:I

.field protected c:I

.field protected d:Z

.field protected e:Lcom/google/android/gms/common/images/g;

.field public f:Z

.field public g:Z

.field public h:Z

.field protected i:I


# direct methods
.method public constructor <init>(Landroid/net/Uri;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput v1, p0, Lcom/google/android/gms/common/images/k;->b:I

    .line 52
    iput v1, p0, Lcom/google/android/gms/common/images/k;->c:I

    .line 53
    iput-boolean v1, p0, Lcom/google/android/gms/common/images/k;->d:Z

    .line 57
    iput-boolean v0, p0, Lcom/google/android/gms/common/images/k;->f:Z

    .line 58
    iput-boolean v1, p0, Lcom/google/android/gms/common/images/k;->g:Z

    .line 59
    iput-boolean v0, p0, Lcom/google/android/gms/common/images/k;->h:Z

    .line 64
    new-instance v0, Lcom/google/android/gms/common/images/l;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/images/l;-><init>(Landroid/net/Uri;)V

    iput-object v0, p0, Lcom/google/android/gms/common/images/k;->a:Lcom/google/android/gms/common/images/l;

    .line 65
    iput v1, p0, Lcom/google/android/gms/common/images/k;->c:I

    .line 66
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/images/internal/h;I)Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    .line 194
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 195
    iget v0, p0, Lcom/google/android/gms/common/images/k;->i:I

    if-lez v0, :cond_2

    .line 197
    new-instance v2, Lcom/google/android/gms/common/images/internal/i;

    iget v0, p0, Lcom/google/android/gms/common/images/k;->i:I

    invoke-direct {v2, p3, v0}, Lcom/google/android/gms/common/images/internal/i;-><init>(II)V

    .line 198
    invoke-virtual {p2, v2}, Lcom/google/android/gms/common/images/internal/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 200
    if-nez v0, :cond_1

    .line 204
    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 207
    iget v3, p0, Lcom/google/android/gms/common/images/k;->i:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    .line 208
    invoke-static {v1, v0}, Lcom/google/android/gms/common/images/internal/f;->a(Landroid/content/res/Resources;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 212
    :cond_0
    invoke-virtual {p2, v2, v0}, Lcom/google/android/gms/common/images/internal/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 95
    iput p1, p0, Lcom/google/android/gms/common/images/k;->c:I

    .line 96
    return-void
.end method

.method final a(Landroid/content/Context;Landroid/graphics/Bitmap;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 149
    invoke-static {p2}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 150
    iget v0, p0, Lcom/google/android/gms/common/images/k;->i:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 151
    invoke-static {p2}, Lcom/google/android/gms/common/images/internal/f;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p2

    .line 153
    :cond_0
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 155
    iget-object v1, p0, Lcom/google/android/gms/common/images/k;->e:Lcom/google/android/gms/common/images/g;

    if-eqz v1, :cond_1

    .line 156
    iget-object v1, p0, Lcom/google/android/gms/common/images/k;->e:Lcom/google/android/gms/common/images/g;

    iget-object v2, p0, Lcom/google/android/gms/common/images/k;->a:Lcom/google/android/gms/common/images/l;

    iget-object v2, v2, Lcom/google/android/gms/common/images/l;->a:Landroid/net/Uri;

    invoke-interface {v1, v3}, Lcom/google/android/gms/common/images/g;->a(Z)V

    .line 160
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v0, p3, v1, v3}, Lcom/google/android/gms/common/images/k;->a(Landroid/graphics/drawable/Drawable;ZZZ)V

    .line 162
    return-void
.end method

.method final a(Landroid/content/Context;Lcom/google/android/gms/common/images/internal/h;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 165
    iget-boolean v0, p0, Lcom/google/android/gms/common/images/k;->h:Z

    if-eqz v0, :cond_1

    .line 166
    const/4 v0, 0x0

    .line 167
    iget v1, p0, Lcom/google/android/gms/common/images/k;->b:I

    if-eqz v1, :cond_0

    .line 168
    iget v0, p0, Lcom/google/android/gms/common/images/k;->b:I

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/common/images/k;->a(Landroid/content/Context;Lcom/google/android/gms/common/images/internal/h;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 171
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v2, v1, v2}, Lcom/google/android/gms/common/images/k;->a(Landroid/graphics/drawable/Drawable;ZZZ)V

    .line 174
    :cond_1
    return-void
.end method

.method final a(Landroid/content/Context;Lcom/google/android/gms/common/images/internal/h;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 178
    const/4 v0, 0x0

    .line 179
    iget v1, p0, Lcom/google/android/gms/common/images/k;->c:I

    if-eqz v1, :cond_0

    .line 180
    iget v0, p0, Lcom/google/android/gms/common/images/k;->c:I

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/common/images/k;->a(Landroid/content/Context;Lcom/google/android/gms/common/images/internal/h;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 183
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/common/images/k;->e:Lcom/google/android/gms/common/images/g;

    if-eqz v1, :cond_1

    .line 184
    iget-object v1, p0, Lcom/google/android/gms/common/images/k;->e:Lcom/google/android/gms/common/images/g;

    iget-object v2, p0, Lcom/google/android/gms/common/images/k;->a:Lcom/google/android/gms/common/images/l;

    iget-object v2, v2, Lcom/google/android/gms/common/images/l;->a:Landroid/net/Uri;

    invoke-interface {v1, v3}, Lcom/google/android/gms/common/images/g;->a(Z)V

    .line 188
    :cond_1
    invoke-virtual {p0, v0, p3, v3, v3}, Lcom/google/android/gms/common/images/k;->a(Landroid/graphics/drawable/Drawable;ZZZ)V

    .line 190
    return-void
.end method

.method protected abstract a(Landroid/graphics/drawable/Drawable;ZZZ)V
.end method

.method public final a(Lcom/google/android/gms/common/images/g;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/gms/common/images/k;->e:Lcom/google/android/gms/common/images/g;

    .line 85
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/images/k;->d:Z

    .line 146
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 135
    iput p1, p0, Lcom/google/android/gms/common/images/k;->i:I

    .line 136
    return-void
.end method
