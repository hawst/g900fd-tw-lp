.class public final Lcom/google/android/gms/games/service/a/i/l;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:Lcom/google/android/gms/games/a/au;

.field private final e:I

.field private final f:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V
    .locals 1

    .prologue
    .line 23
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 24
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/i/l;->c:Lcom/google/android/gms/games/internal/dr;

    .line 25
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/i/l;->d:Lcom/google/android/gms/games/a/au;

    .line 26
    iput p3, p0, Lcom/google/android/gms/games/service/a/i/l;->e:I

    .line 27
    iput-boolean p4, p0, Lcom/google/android/gms/games/service/a/i/l;->f:Z

    .line 28
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/i/l;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->e(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 39
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/i/l;->d:Lcom/google/android/gms/games/a/au;

    iget v1, p0, Lcom/google/android/gms/games/service/a/i/l;->e:I

    iget-boolean v2, p0, Lcom/google/android/gms/games/service/a/i/l;->f:Z

    invoke-virtual {p2, v0, v1, v2}, Lcom/google/android/gms/games/a/t;->d(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
