.class final Lcom/google/android/gms/drive/al;
.super Lcom/google/android/gms/drive/aj;
.source "SourceFile"


# instance fields
.field final a:I

.field private final b:Lcom/google/android/gms/common/data/DataHolder;

.field private final c:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;I)V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/google/android/gms/drive/aj;-><init>()V

    .line 92
    iput-object p1, p0, Lcom/google/android/gms/drive/al;->b:Lcom/google/android/gms/common/data/DataHolder;

    .line 93
    iput p2, p0, Lcom/google/android/gms/drive/al;->a:I

    .line 94
    invoke-virtual {p1, p2}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/al;->c:I

    .line 95
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/gms/drive/al;->b:Lcom/google/android/gms/common/data/DataHolder;

    iget v1, p0, Lcom/google/android/gms/drive/al;->a:I

    iget v2, p0, Lcom/google/android/gms/drive/al;->c:I

    invoke-interface {p1, v0, v1, v2}, Lcom/google/android/gms/drive/metadata/f;->a(Lcom/google/android/gms/common/data/DataHolder;II)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 86
    invoke-static {}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/metadata/internal/f;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/f;

    instance-of v3, v0, Lcom/google/android/gms/drive/metadata/b;

    if-nez v3, :cond_0

    sget-object v3, Lcom/google/android/gms/drive/metadata/internal/a/a;->z:Lcom/google/android/gms/drive/metadata/f;

    if-eq v0, v3, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/drive/al;->b:Lcom/google/android/gms/common/data/DataHolder;

    iget v4, p0, Lcom/google/android/gms/drive/al;->a:I

    iget v5, p0, Lcom/google/android/gms/drive/al;->c:I

    invoke-interface {v0, v3, v1, v4, v5}, Lcom/google/android/gms/drive/metadata/f;->a(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;II)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/gms/drive/internal/l;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/internal/l;-><init>(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V

    return-object v0
.end method

.method public final z_()Z
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/gms/drive/al;->b:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->i()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
