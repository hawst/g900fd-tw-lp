.class public abstract Lcom/google/android/gms/games/service/statemachine/roomclient/ag;
.super Lcom/google/android/gms/games/service/statemachine/e;
.source "SourceFile"


# instance fields
.field public final d:Landroid/content/Context;

.field public final e:Lcom/google/android/gms/common/server/ClientContext;

.field public final f:Ljava/lang/String;

.field public final g:Lcom/google/android/gms/games/internal/dr;

.field public final h:Landroid/os/IBinder;

.field public final i:Z

.field public final j:Lcom/google/android/gms/games/service/statemachine/roomclient/v;

.field public final k:J

.field public final l:Lcom/google/android/gms/games/service/statemachine/roomclient/x;

.field public final m:Lcom/google/android/gms/games/service/statemachine/roomclient/am;

.field public final n:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;


# direct methods
.method protected constructor <init>(ILandroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/games/internal/dr;Landroid/os/IBinder;ZJLjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 387
    const/16 v0, 0x1d

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/statemachine/e;-><init>(I)V

    .line 344
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomclient/x;

    invoke-direct {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/x;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->l:Lcom/google/android/gms/games/service/statemachine/roomclient/x;

    .line 345
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomclient/am;

    invoke-direct {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/am;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->m:Lcom/google/android/gms/games/service/statemachine/roomclient/am;

    .line 388
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    move-object v1, p2

    move-object v2, p3

    move v3, p1

    move-object v4, p4

    move v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    .line 390
    iput-object p3, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->e:Lcom/google/android/gms/common/server/ClientContext;

    .line 391
    iput-object p2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->d:Landroid/content/Context;

    .line 392
    iput-object p4, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->f:Ljava/lang/String;

    .line 393
    iput-object p5, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->g:Lcom/google/android/gms/games/internal/dr;

    .line 394
    iput-object p6, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->h:Landroid/os/IBinder;

    .line 395
    iput-boolean p7, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->i:Z

    .line 396
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomclient/v;

    move-object v1, p2

    move-object v2, p4

    move-object/from16 v3, p11

    move-object v4, p3

    move-object/from16 v5, p10

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/statemachine/roomclient/v;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/v;

    .line 398
    iput-wide p8, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->k:J

    .line 399
    return-void
.end method


# virtual methods
.method public abstract a(Lcom/google/android/gms/games/service/statemachine/roomclient/bz;Lcom/google/android/gms/games/service/g;Lcom/google/android/gms/games/internal/ConnectionInfo;)Ljava/lang/Runnable;
.end method

.method public abstract b()Lcom/google/android/gms/games/service/statemachine/roomclient/ad;
.end method
