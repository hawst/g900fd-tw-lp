.class public final Lcom/google/android/gms/games/ui/GamesSettingsActivity;
.super Lcom/google/android/gms/games/ui/q;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# static fields
.field private static final i:I


# instance fields
.field private j:Ljava/lang/String;

.field private k:[Ljava/lang/String;

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/String;

.field private o:Ljava/util/ArrayList;

.field private p:Z

.field private final q:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    sget v0, Lcom/google/android/gms/l;->bG:I

    sput v0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->i:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    const/4 v0, 0x2

    invoke-direct {p0, v0, v1, v1, v1}, Lcom/google/android/gms/games/ui/q;-><init>(IIII)V

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->l:Z

    .line 60
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->m:Z

    .line 65
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->q:Landroid/os/Bundle;

    .line 71
    return-void
.end method


# virtual methods
.method public final K()V
    .locals 6

    .prologue
    .line 120
    invoke-super {p0}, Lcom/google/android/gms/games/ui/q;->K()V

    .line 122
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v3

    .line 123
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/support/v7/app/a;->a(Z)V

    .line 125
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->l:Z

    if-eqz v0, :cond_0

    .line 126
    new-instance v0, Lcom/google/android/gms/common/account/h;

    invoke-direct {v0, v3}, Lcom/google/android/gms/common/account/h;-><init>(Landroid/support/v7/app/a;)V

    sget v1, Lcom/google/android/gms/p;->lx:I

    iput v1, v0, Lcom/google/android/gms/common/account/h;->a:I

    iput-object p0, v0, Lcom/google/android/gms/common/account/h;->b:Landroid/widget/AdapterView$OnItemSelectedListener;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/common/account/h;->c:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/common/account/h;->a()Lcom/google/android/gms/common/account/g;

    .line 146
    :goto_0
    return-void

    .line 135
    :cond_0
    sget v0, Lcom/google/android/gms/p;->lx:I

    invoke-virtual {v3, v0}, Landroid/support/v7/app/a;->c(I)V

    .line 136
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->lx:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x15

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    sget v0, Lcom/google/android/gms/p;->hI:I

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_1
    sget-object v0, Lcom/google/android/gms/games/ui/q;->f:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    sget v2, Lcom/google/android/gms/h;->bL:I

    sget-object v0, Lcom/google/android/gms/games/ui/n;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    sget v0, Lcom/google/android/gms/h;->aZ:I

    :goto_2
    invoke-static {v4, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/q;->f:Landroid/graphics/Bitmap;

    :cond_2
    new-instance v0, Landroid/app/ActivityManager$TaskDescription;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/ui/q;->f:Landroid/graphics/Bitmap;

    sget v5, Lcom/google/android/gms/f;->W:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v0, v1, v2, v4}, Landroid/app/ActivityManager$TaskDescription;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/q;->setTaskDescription(Landroid/app/ActivityManager$TaskDescription;)V

    .line 139
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/f;->W:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 140
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v3, v1}, Landroid/support/v7/app/a;->b(Landroid/graphics/drawable/Drawable;)V

    .line 143
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/h;->aY:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/support/v7/app/a;->c(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    move-object v1, v0

    goto :goto_1
.end method

.method public final N()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->o:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final O()Z
    .locals 1

    .prologue
    .line 294
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->p:Z

    return v0
.end method

.method public final P()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->q:Landroid/os/Bundle;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/game/ExtendedGame;)V
    .locals 2

    .prologue
    .line 310
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->gq:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/gms/games/ui/aj;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/google/android/gms/games/ui/aj;

    .line 311
    :goto_0
    if-eqz v0, :cond_0

    .line 312
    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/aj;->a(Lcom/google/android/gms/games/internal/game/ExtendedGame;)V

    .line 314
    :cond_0
    return-void

    .line 310
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->o:Ljava/util/ArrayList;

    .line 284
    return-void
.end method

.method public final a(ZLandroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 302
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->p:Z

    .line 303
    invoke-virtual {p2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 304
    iget-object v2, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->q:Landroid/os/Bundle;

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 307
    :cond_0
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 269
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->m:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    sget v0, Lcom/google/android/gms/j;->gq:I

    invoke-virtual {v1, v0}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v2, v0, Lcom/google/android/gms/games/ui/aj;

    if-eqz v2, :cond_1

    check-cast v0, Lcom/google/android/gms/games/ui/aj;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/aj;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GamesSettings"

    const-string v1, "Not adding duplicate fragment"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    :cond_0
    :goto_0
    return-void

    .line 269
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->n:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/gms/games/ui/aj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/ui/aj;

    move-result-object v0

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    sget v2, Lcom/google/android/gms/j;->gq:I

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x1

    return v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 77
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j:Ljava/lang/String;

    .line 80
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.DEST_APP_VERSION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->n:Ljava/lang/String;

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 83
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->l:Z

    .line 86
    sget v0, Lcom/google/android/gms/q;->e:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->setTheme(I)V

    .line 103
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/common/util/a;->f(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_3

    move v1, v2

    :goto_1
    new-array v5, v1, [Ljava/lang/String;

    move v3, v2

    :goto_2
    if-ge v3, v1, :cond_4

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v0, v5, v3

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 89
    :cond_1
    if-eqz p1, :cond_2

    .line 90
    const-string v0, "selected_account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j:Ljava/lang/String;

    .line 94
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 96
    const-string v0, "settingsDefaultAccount"

    const-string v1, "gcore.sharedPrefs"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j:Ljava/lang/String;

    goto :goto_0

    .line 103
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    goto :goto_1

    :cond_4
    iput-object v5, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->k:[Ljava/lang/String;

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->k:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->k:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_6

    .line 105
    :cond_5
    const-string v0, "GamesSettings"

    const-string v1, "No accounts found when creating settings activity. Bailing out."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->finish()V

    .line 116
    :goto_3
    return-void

    .line 110
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->k:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 112
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->k:[Ljava/lang/String;

    aget-object v0, v0, v2

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j:Ljava/lang/String;

    .line 115
    :cond_8
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/q;->onCreate(Landroid/os/Bundle;)V

    goto :goto_3
.end method

.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->k:[Ljava/lang/String;

    aget-object v0, v0, p3

    iget-object v1, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    :goto_0
    return-void

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->k:[Ljava/lang/String;

    aget-object v0, v0, p3

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j:Ljava/lang/String;

    .line 248
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->v()V

    .line 249
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    goto :goto_0
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 254
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 258
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 264
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/q;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 261
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->finish()V

    .line 262
    const/4 v0, 0x1

    goto :goto_0

    .line 258
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public final onPause()V
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->m:Z

    .line 189
    invoke-super {p0}, Lcom/google/android/gms/games/ui/q;->onPause()V

    .line 190
    return-void
.end method

.method public final onResume()V
    .locals 1

    .prologue
    .line 182
    invoke-super {p0}, Lcom/google/android/gms/games/ui/q;->onResume()V

    .line 183
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->m:Z

    .line 184
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 194
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/q;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 195
    const-string v0, "selected_account"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    .line 200
    invoke-super {p0}, Lcom/google/android/gms/games/ui/q;->onStop()V

    .line 201
    const-string v0, "settingsDefaultAccount"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j:Ljava/lang/String;

    const-string v2, "gcore.sharedPrefs"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-static {v2}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 203
    return-void
.end method

.method protected final r()Lcom/google/android/gms/common/api/v;
    .locals 3

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    .line 161
    new-instance v1, Lcom/google/android/gms/games/ui/af;

    invoke-direct {v1}, Lcom/google/android/gms/games/ui/af;-><init>()V

    .line 162
    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v2, Lcom/google/android/gms/j;->gq:I

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 164
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 170
    :cond_0
    invoke-static {}, Lcom/google/android/gms/games/h;->a()Lcom/google/android/gms/games/i;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/games/i;->a:Z

    invoke-virtual {v0}, Lcom/google/android/gms/games/i;->a()Lcom/google/android/gms/games/h;

    move-result-object v0

    .line 173
    new-instance v1, Lcom/google/android/gms/common/api/w;

    invoke-direct {v1, p0, p0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V

    sget-object v2, Lcom/google/android/gms/games/d;->e:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    const-string v1, "com.google.android.gms"

    iput-object v1, v0, Lcom/google/android/gms/common/api/w;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    return-object v0
.end method

.method protected final u()I
    .locals 1

    .prologue
    .line 155
    sget v0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->i:I

    return v0
.end method
