.class final Lcom/google/android/gms/drive/internal/da;
.super Lcom/google/android/gms/drive/internal/c;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/m;

.field private final b:Lcom/google/android/gms/drive/o;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/m;Lcom/google/android/gms/drive/o;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/gms/drive/internal/c;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/da;->a:Lcom/google/android/gms/common/api/m;

    .line 28
    iput-object p2, p0, Lcom/google/android/gms/drive/internal/da;->b:Lcom/google/android/gms/drive/o;

    .line 29
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/da;->a:Lcom/google/android/gms/common/api/m;

    new-instance v1, Lcom/google/android/gms/drive/internal/v;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/google/android/gms/drive/internal/v;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/drive/m;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 50
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/OnContentsResponse;)V
    .locals 5

    .prologue
    .line 33
    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/OnContentsResponse;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    .line 35
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/drive/internal/da;->a:Lcom/google/android/gms/common/api/m;

    new-instance v2, Lcom/google/android/gms/drive/internal/v;

    new-instance v3, Lcom/google/android/gms/drive/internal/an;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/OnContentsResponse;->a()Lcom/google/android/gms/drive/Contents;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/gms/drive/internal/an;-><init>(Lcom/google/android/gms/drive/Contents;)V

    invoke-direct {v2, v0, v3}, Lcom/google/android/gms/drive/internal/v;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/drive/m;)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 37
    return-void

    .line 33
    :cond_0
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/drive/internal/OnDownloadProgressResponse;)V
    .locals 6

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/da;->b:Lcom/google/android/gms/drive/o;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/da;->b:Lcom/google/android/gms/drive/o;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/OnDownloadProgressResponse;->a()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/OnDownloadProgressResponse;->b()J

    move-result-wide v4

    invoke-interface {v0, v2, v3, v4, v5}, Lcom/google/android/gms/drive/o;->a(JJ)V

    .line 45
    :cond_0
    return-void
.end method
