.class final Lcom/google/android/gms/games/a/ay;
.super Lcom/google/android/gms/games/a/bb;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/a/ce;


# static fields
.field private static final a:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final b:[Ljava/lang/String;

.field private static final c:Lcom/google/android/gms/games/provider/a;


# instance fields
.field private final d:Lcom/google/android/gms/games/h/a/cc;

.field private final e:Lcom/google/android/gms/games/h/a/cd;

.field private final f:Lcom/google/android/gms/games/h/a/ez;

.field private final g:Lcom/google/android/gms/games/h/a/fa;

.field private final h:Lcom/google/android/gms/games/b/o;

.field private final i:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 91
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/ay;->a:Ljava/util/concurrent/locks/ReentrantLock;

    .line 137
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "score_order"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/games/a/ay;->b:[Ljava/lang/String;

    .line 143
    invoke-static {}, Lcom/google/android/gms/games/provider/a;->a()Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "instance_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->d:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "page_type"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "default_display_image_uri"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "default_display_image_url"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "default_display_name"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "rank"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->d:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "display_rank"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "raw_score"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->d:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "display_score"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "achieved_timestamp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->d:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "score_tag"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/k/a;->e:Lcom/google/android/gms/games/provider/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/c;->a(Lcom/google/android/gms/games/provider/a;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/c;->a()Lcom/google/android/gms/games/provider/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/a/ay;->c:Lcom/google/android/gms/games/provider/a;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;)V
    .locals 2

    .prologue
    .line 213
    const-string v0, "LeaderboardAgent"

    sget-object v1, Lcom/google/android/gms/games/a/ay;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    .line 214
    new-instance v0, Lcom/google/android/gms/games/h/a/cc;

    invoke-direct {v0, p2}, Lcom/google/android/gms/games/h/a/cc;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ay;->d:Lcom/google/android/gms/games/h/a/cc;

    .line 215
    new-instance v0, Lcom/google/android/gms/games/h/a/cd;

    invoke-direct {v0, p3}, Lcom/google/android/gms/games/h/a/cd;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ay;->e:Lcom/google/android/gms/games/h/a/cd;

    .line 216
    new-instance v0, Lcom/google/android/gms/games/h/a/ez;

    invoke-direct {v0, p2}, Lcom/google/android/gms/games/h/a/ez;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ay;->f:Lcom/google/android/gms/games/h/a/ez;

    .line 217
    new-instance v0, Lcom/google/android/gms/games/h/a/fa;

    invoke-direct {v0, p3}, Lcom/google/android/gms/games/h/a/fa;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ay;->g:Lcom/google/android/gms/games/h/a/fa;

    .line 218
    new-instance v0, Lcom/google/android/gms/games/b/o;

    sget-object v1, Lcom/google/android/gms/games/a/ay;->c:Lcom/google/android/gms/games/provider/a;

    iget-object v1, v1, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/b/o;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ay;->h:Lcom/google/android/gms/games/b/o;

    .line 219
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/ay;->i:Ljava/util/HashMap;

    .line 220
    return-void
.end method

.method private static a(Landroid/net/Uri;III)Landroid/content/ContentProviderOperation;
    .locals 3

    .prologue
    .line 1445
    invoke-static {p0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "leaderboard_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "timespan"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "collection"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/games/h/a/cb;)Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 1288
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1289
    invoke-virtual {p0}, Lcom/google/android/gms/games/h/a/cb;->getPlayerScore()Lcom/google/android/gms/games/h/a/bx;

    move-result-object v1

    .line 1290
    if-eqz v1, :cond_0

    .line 1291
    iget-object v1, v1, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    .line 1292
    const-string v2, "display_score"

    const-string v3, "player_display_score"

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 1294
    const-string v2, "raw_score"

    const-string v3, "player_raw_score"

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 1296
    const-string v2, "display_rank"

    const-string v3, "player_display_rank"

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 1298
    const-string v2, "rank"

    const-string v3, "player_rank"

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 1300
    const-string v2, "score_tag"

    const-string v3, "player_score_tag"

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 1312
    :goto_0
    const-string v1, "total_scores"

    invoke-virtual {p0}, Lcom/google/android/gms/games/h/a/cb;->c()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1313
    return-object v0

    .line 1306
    :cond_0
    const-string v1, "player_display_score"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1307
    const-string v1, "player_raw_score"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1308
    const-string v1, "player_display_rank"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1309
    const-string v1, "player_rank"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1310
    const-string v1, "player_score_tag"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    .prologue
    .line 467
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/provider/af;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 469
    new-instance v1, Lcom/google/android/gms/games/a/o;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    const-string v1, "sorting_rank,name,external_leaderboard_id,timespan DESC,collection"

    iput-object v1, v0, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    iput p2, v0, Lcom/google/android/gms/games/a/o;->d:I

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IIII)Lcom/google/android/gms/common/data/DataHolder;
    .locals 16

    .prologue
    .line 880
    invoke-direct/range {p0 .. p4}, Lcom/google/android/gms/games/a/ay;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;II)J

    move-result-wide v12

    .line 882
    const-wide/16 v2, -0x1

    cmp-long v2, v12, v2

    if-nez v2, :cond_0

    .line 883
    const-string v2, "LeaderboardAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "No instance found for leaderboard "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static/range {p4 .. p4}, Lcom/google/android/gms/games/internal/b/c;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/games/internal/b/i;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 886
    const/4 v2, 0x4

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    .line 914
    :goto_0
    return-object v2

    .line 890
    :cond_0
    invoke-direct/range {p0 .. p2}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Z

    .line 891
    new-instance v2, Lcom/google/android/gms/games/b/t;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    move-object/from16 v0, p2

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/games/b/t;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    .line 893
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/a/ay;->h:Lcom/google/android/gms/games/b/o;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/games/b/o;->a(Ljava/lang/Object;)V

    .line 894
    new-instance v14, Lcom/google/android/gms/games/b/s;

    move/from16 v0, p6

    invoke-direct {v14, v12, v13, v0}, Lcom/google/android/gms/games/b/s;-><init>(JI)V

    .line 895
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/gms/games/a/au;->g:Z

    if-eqz v2, :cond_1

    .line 896
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ay;->h:Lcom/google/android/gms/games/b/o;

    invoke-virtual {v2, v14}, Lcom/google/android/gms/games/b/o;->c(Ljava/lang/Object;)V

    .line 900
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/ay;->h:Lcom/google/android/gms/games/b/o;

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    invoke-virtual {v2, v14, v4, v5}, Lcom/google/android/gms/games/b/o;->a(Ljava/lang/Object;J)Z

    move-result v2

    if-nez v2, :cond_4

    .line 901
    const/4 v11, 0x0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    const/4 v9, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    invoke-direct/range {v2 .. v10}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/h/a/cb;

    move-result-object v7

    if-eqz v7, :cond_2

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    move-wide v4, v12

    move/from16 v6, p6

    move-object v8, v15

    invoke-static/range {v3 .. v8}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/common/server/ClientContext;JILcom/google/android/gms/games/h/a/cb;Ljava/util/ArrayList;)V

    :cond_2
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_3

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "LeaderboardAgent"

    invoke-static {v2, v15, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    :cond_3
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2, v12, v13}, Lcom/google/android/gms/games/provider/ah;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/games/a/o;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    const-string v4, "page_type=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static/range {p6 .. p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/games/a/o;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/games/a/ay;->c:Lcom/google/android/gms/games/provider/a;

    iget-object v3, v3, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    const-string v3, "rank ASC"

    iput-object v3, v2, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v9

    if-nez v7, :cond_6

    :try_start_0
    invoke-virtual {v9}, Landroid/database/AbstractWindowedCursor;->getCount()I

    move-result v2

    if-nez v2, :cond_5

    const/4 v2, 0x4

    :goto_1
    move v5, v2

    :goto_2
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    move-object/from16 v2, p0

    move-wide v6, v12

    move/from16 v8, p6

    invoke-direct/range {v2 .. v9}, Lcom/google/android/gms/games/a/ay;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;IJILandroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v9}, Landroid/database/AbstractWindowedCursor;->close()V

    .line 907
    :cond_4
    invoke-static {}, Lcom/google/android/gms/games/e/g;->a()Lcom/google/android/gms/games/e/h;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/e/h;->a(Ljava/lang/String;)Lcom/google/android/gms/games/e/h;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/e/h;->b(Ljava/lang/String;)Lcom/google/android/gms/games/e/h;

    move-result-object v2

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/e/h;->a(I)Lcom/google/android/gms/games/e/h;

    move-result-object v2

    move/from16 v0, p4

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/e/h;->b(I)Lcom/google/android/gms/games/e/h;

    move-result-object v2

    move/from16 v0, p6

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/e/h;->c(I)Lcom/google/android/gms/games/e/h;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/e/h;->a()Lcom/google/android/gms/games/e/g;

    move-result-object v2

    .line 914
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/a/ay;->h:Lcom/google/android/gms/games/b/o;

    move/from16 v0, p5

    invoke-virtual {v3, v14, v2, v0}, Lcom/google/android/gms/games/b/o;->a(Lcom/google/android/gms/games/b/s;Lcom/google/android/gms/games/e/g;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto/16 :goto_0

    .line 901
    :cond_5
    const/4 v2, 0x3

    goto :goto_1

    :catchall_0
    move-exception v2

    invoke-virtual {v9}, Landroid/database/AbstractWindowedCursor;->close()V

    throw v2

    :cond_6
    move v5, v11

    goto :goto_2
.end method

.method private a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/h/a/cb;
    .locals 1

    .prologue
    .line 1042
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->h:Z

    if-eqz v0, :cond_0

    .line 1043
    invoke-direct/range {p0 .. p8}, Lcom/google/android/gms/games/a/ay;->c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/h/a/cb;

    move-result-object v0

    .line 1046
    :goto_0
    return-object v0

    :cond_0
    invoke-direct/range {p0 .. p8}, Lcom/google/android/gms/games/a/ay;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/h/a/cb;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/h/a/dg;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x1

    .line 813
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 814
    new-instance v1, Lcom/google/android/gms/games/h/a/ey;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-direct {v1, p2, v3, p6, p7}, Lcom/google/android/gms/games/h/a/ey;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 815
    new-instance v4, Lcom/google/android/gms/games/h/a/dh;

    invoke-direct {v4, v0}, Lcom/google/android/gms/games/h/a/dh;-><init>(Ljava/util/ArrayList;)V

    .line 816
    iget-object v0, p0, Lcom/google/android/gms/games/a/ay;->f:Lcom/google/android/gms/games/h/a/ez;

    const-string v3, "leaderboards/scores"

    if-eqz p5, :cond_0

    const-string v1, "language"

    invoke-static {p5}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v1, v5}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/games/h/a/ez;->a:Lcom/google/android/gms/common/server/n;

    const-class v5, Lcom/google/android/gms/games/h/a/df;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/df;

    .line 818
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/df;->getSubmittedScores()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v2, :cond_1

    :goto_0
    const-string v1, "Should be one score response"

    invoke-static {v2, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 820
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/df;->getSubmittedScores()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/dg;

    return-object v0

    :cond_1
    move v2, v6

    .line 818
    goto :goto_0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;IJILandroid/database/Cursor;)V
    .locals 14

    .prologue
    .line 1115
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1116
    :goto_0
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1117
    sget-object v4, Lcom/google/android/gms/games/a/ay;->c:Lcom/google/android/gms/games/provider/a;

    move-object/from16 v0, p7

    invoke-virtual {v4, v0}, Lcom/google/android/gms/games/provider/a;->a(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1121
    :cond_0
    const/4 v9, 0x0

    .line 1122
    const/4 v10, 0x0

    .line 1123
    move-object/from16 v0, p2

    move-wide/from16 v1, p4

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/provider/af;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v4

    .line 1124
    new-instance v5, Lcom/google/android/gms/games/a/o;

    invoke-direct {v5, p1}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v4}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v4

    sget-object v5, Lcom/google/android/gms/games/a/az;->a:[Ljava/lang/String;

    iput-object v5, v4, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    invoke-virtual {v4}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v5

    .line 1129
    :try_start_0
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1130
    packed-switch p6, :pswitch_data_0

    .line 1139
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Unknown page type "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1143
    :catchall_0
    move-exception v4

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v4

    .line 1132
    :pswitch_0
    const/4 v4, 0x2

    :try_start_1
    invoke-interface {v5, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1133
    const/4 v4, 0x1

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v10

    .line 1143
    :cond_1
    :goto_1
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 1147
    new-instance v6, Lcom/google/android/gms/games/b/s;

    move-wide/from16 v0, p4

    move/from16 v2, p6

    invoke-direct {v6, v0, v1, v2}, Lcom/google/android/gms/games/b/s;-><init>(JI)V

    .line 1148
    iget-object v4, p0, Lcom/google/android/gms/games/a/ay;->h:Lcom/google/android/gms/games/b/o;

    invoke-virtual {v4, v6}, Lcom/google/android/gms/games/b/o;->c(Ljava/lang/Object;)V

    .line 1149
    iget-object v5, p0, Lcom/google/android/gms/games/a/ay;->h:Lcom/google/android/gms/games/b/o;

    const/4 v11, -0x1

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v12

    move/from16 v8, p3

    invoke-virtual/range {v5 .. v13}, Lcom/google/android/gms/games/b/o;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJ)V

    .line 1151
    return-void

    .line 1136
    :pswitch_1
    const/4 v4, 0x0

    :try_start_2
    invoke-interface {v5, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v10

    goto :goto_1

    .line 1130
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;JILcom/google/android/gms/games/h/a/cb;I)V
    .locals 21

    .prologue
    .line 1156
    invoke-virtual/range {p6 .. p6}, Lcom/google/android/gms/games/h/a/cb;->getItems()Ljava/util/ArrayList;

    move-result-object v8

    .line 1157
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v6, v4

    .line 1159
    :goto_0
    invoke-virtual/range {p6 .. p6}, Lcom/google/android/gms/games/h/a/cb;->b()Ljava/lang/String;

    move-result-object v10

    .line 1160
    invoke-virtual/range {p6 .. p6}, Lcom/google/android/gms/games/h/a/cb;->d()Ljava/lang/String;

    move-result-object v9

    .line 1161
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v12

    .line 1164
    invoke-static/range {p6 .. p6}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/h/a/cb;)Landroid/content/ContentValues;

    move-result-object v4

    .line 1165
    invoke-static/range {p2 .. p4}, Lcom/google/android/gms/games/provider/af;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v5

    .line 1166
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const/4 v11, 0x0

    const/4 v14, 0x0

    invoke-virtual {v7, v5, v4, v11, v14}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1170
    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/games/provider/ad;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v11

    .line 1171
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 1172
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 1173
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 1174
    new-instance v16, Ljava/util/ArrayList;

    move-object/from16 v0, v16

    invoke-direct {v0, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 1176
    const/4 v4, 0x0

    move v5, v4

    :goto_1
    if-ge v5, v6, :cond_1

    .line 1177
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/games/h/a/bx;

    .line 1178
    iget-object v0, v4, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    move-object/from16 v17, v0

    .line 1179
    const-string v18, "instance_id"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1180
    const-string v18, "page_type"

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1181
    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/bx;->getPlayer()Lcom/google/android/gms/games/h/a/cs;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 1182
    const-string v4, "last_updated"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1183
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/content/ContentValues;)V

    .line 1188
    const-string v4, "profile_icon_image_url"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1189
    move-object/from16 v0, v16

    invoke-static {v11, v4, v0}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v14, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1190
    const-string v4, "profile_hi_res_image_url"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1191
    move-object/from16 v0, v16

    invoke-static {v11, v4, v0}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1192
    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1176
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1

    .line 1157
    :cond_0
    const/4 v4, 0x0

    move v6, v4

    goto/16 :goto_0

    .line 1194
    :cond_1
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne v4, v6, :cond_2

    const/4 v4, 0x1

    :goto_2
    invoke-static {v4}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 1195
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne v4, v6, :cond_3

    const/4 v4, 0x1

    :goto_3
    invoke-static {v4}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 1200
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "LeaderboardAgent"

    move-object/from16 v0, v16

    invoke-static {v4, v0, v5}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v11

    .line 1202
    const/4 v4, 0x0

    move v8, v4

    :goto_4
    if-ge v8, v6, :cond_4

    .line 1203
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/ContentValues;

    .line 1204
    const-string v16, "profile_icon_image_url"

    const-string v17, "profile_icon_image_uri"

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v4, v0, v1, v11, v5}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 1206
    const-string v16, "default_display_image_url"

    const-string v17, "default_display_image_uri"

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v4, v0, v1, v11, v5}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 1208
    const-string v16, "profile_hi_res_image_url"

    const-string v17, "profile_hi_res_image_uri"

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v4, v0, v1, v11, v5}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 1202
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_4

    .line 1194
    :cond_2
    const/4 v4, 0x0

    goto :goto_2

    .line 1195
    :cond_3
    const/4 v4, 0x0

    goto :goto_3

    .line 1213
    :cond_4
    new-instance v6, Lcom/google/android/gms/games/b/s;

    move-wide/from16 v0, p3

    move/from16 v2, p5

    invoke-direct {v6, v0, v1, v2}, Lcom/google/android/gms/games/b/s;-><init>(JI)V

    .line 1214
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/a/ay;->h:Lcom/google/android/gms/games/b/o;

    const/4 v8, 0x0

    move/from16 v11, p7

    invoke-virtual/range {v5 .. v13}, Lcom/google/android/gms/games/b/o;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJ)V

    .line 1216
    return-void
.end method

.method private static a(Lcom/google/android/gms/common/server/ClientContext;JILcom/google/android/gms/games/h/a/cb;Ljava/util/ArrayList;)V
    .locals 13

    .prologue
    const/4 v11, 0x1

    const/4 v4, 0x0

    .line 1320
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/games/h/a/cb;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    .line 1321
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v3, v2

    .line 1322
    :goto_0
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/games/h/a/cb;->b()Ljava/lang/String;

    move-result-object v2

    .line 1323
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/games/h/a/cb;->d()Ljava/lang/String;

    move-result-object v6

    .line 1324
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v8

    .line 1327
    invoke-static/range {p4 .. p4}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/h/a/cb;)Landroid/content/ContentValues;

    move-result-object v7

    .line 1331
    if-nez p3, :cond_2

    .line 1332
    const-string v6, "top_page_token_next"

    invoke-virtual {v7, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1337
    :cond_0
    :goto_1
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/games/provider/af;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v2

    .line 1338
    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v11}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1344
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/games/provider/ah;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v2

    .line 1345
    new-array v6, v11, [Ljava/lang/String;

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    .line 1346
    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v7, "page_type=?"

    invoke-virtual {v2, v7, v6}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1352
    :goto_2
    if-ge v4, v3, :cond_3

    .line 1354
    invoke-static {p0}, Lcom/google/android/gms/games/provider/ah;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    .line 1356
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/bx;

    .line 1357
    iget-object v7, v2, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    .line 1358
    const-string v10, "instance_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1359
    const-string v10, "page_type"

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1362
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/bx;->getPlayer()Lcom/google/android/gms/games/h/a/cs;

    move-result-object v2

    .line 1363
    const-string v10, "player_id"

    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    move-result v11

    invoke-virtual {v6, v10, v11}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 1364
    iget-object v2, v2, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-static {p0, v2, v8, v9}, Lcom/google/android/gms/games/a/l;->a(Lcom/google/android/gms/common/server/ClientContext;Landroid/content/ContentValues;J)Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1367
    invoke-virtual {v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1352
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_1
    move v3, v4

    .line 1321
    goto/16 :goto_0

    .line 1333
    :cond_2
    move/from16 v0, p3

    if-ne v0, v11, :cond_0

    .line 1334
    const-string v10, "window_page_token_next"

    invoke-virtual {v7, v10, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1335
    const-string v2, "window_page_token_prev"

    invoke-virtual {v7, v2, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1369
    :cond_3
    return-void
.end method

.method private a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 825
    iget-object v0, p0, Lcom/google/android/gms/games/a/ay;->h:Lcom/google/android/gms/games/b/o;

    new-instance v1, Lcom/google/android/gms/games/b/t;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/games/b/t;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/b/o;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 826
    iget-object v0, p0, Lcom/google/android/gms/games/a/ay;->h:Lcom/google/android/gms/games/b/o;

    iget-object v0, v0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0}, Landroid/support/v4/g/h;->b()V

    .line 828
    :cond_0
    return-void
.end method

.method private static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/bw;ILjava/util/ArrayList;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1414
    if-nez p1, :cond_0

    .line 1441
    :goto_0
    return-void

    .line 1420
    :cond_0
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1421
    iget-object v1, p1, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    .line 1422
    const-string v2, "game_id"

    invoke-virtual {p0}, Lcom/google/android/gms/games/a/au;->j()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1423
    if-ltz p2, :cond_1

    .line 1424
    const-string v2, "sorting_rank"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1426
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2}, Lcom/google/android/gms/games/provider/ai;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    .line 1431
    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1434
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1}, Lcom/google/android/gms/games/provider/af;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    .line 1435
    invoke-static {v1, v0, v8, v7}, Lcom/google/android/gms/games/a/ay;->a(Landroid/net/Uri;III)Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1436
    invoke-static {v1, v0, v8, v6}, Lcom/google/android/gms/games/a/ay;->a(Landroid/net/Uri;III)Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1437
    invoke-static {v1, v0, v6, v7}, Lcom/google/android/gms/games/a/ay;->a(Landroid/net/Uri;III)Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1438
    invoke-static {v1, v0, v6, v6}, Lcom/google/android/gms/games/a/ay;->a(Landroid/net/Uri;III)Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1439
    invoke-static {v1, v0, v7, v7}, Lcom/google/android/gms/games/a/ay;->a(Landroid/net/Uri;III)Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1440
    invoke-static {v1, v0, v7, v6}, Lcom/google/android/gms/games/a/ay;->a(Landroid/net/Uri;III)Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V
    .locals 14

    .prologue
    .line 608
    iget-object v12, p0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    .line 610
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2}, Lcom/google/android/gms/games/provider/ag;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v3

    .line 611
    new-instance v6, Lcom/google/android/gms/common/e/b;

    invoke-direct {v6, v3}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 612
    const-string v2, "external_game_id"

    iget-object v4, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v6, v2, v4}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    const-string v2, "external_leaderboard_id"

    invoke-virtual {v6, v2, p1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    const-string v2, "external_player_id"

    invoke-virtual {v6, v2, v12}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    const-wide/16 v10, -0x1

    .line 617
    const-wide/16 v8, -0x1

    .line 618
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v4, Lcom/google/android/gms/games/a/ba;->a:[Ljava/lang/String;

    invoke-virtual {v6}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v6, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 622
    :try_start_0
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 623
    const/4 v2, 0x0

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 624
    const/4 v2, 0x2

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v6

    move-wide v10, v4

    .line 627
    :goto_0
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 631
    const-wide/16 v4, 0x0

    cmp-long v2, v10, v4

    if-lez v2, :cond_1

    .line 632
    iget-object v3, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    move-object v5, p1

    move-wide/from16 v8, p2

    invoke-static/range {v3 .. v9}, Lcom/google/android/gms/games/a/ay;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;JJ)Z

    move-result v2

    if-nez v2, :cond_0

    .line 664
    :goto_1
    return-void

    .line 627
    :catchall_0
    move-exception v2

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    throw v2

    .line 638
    :cond_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 639
    const-string v3, "raw_score"

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 640
    const-string v3, "score_tag"

    move-object/from16 v0, p6

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    const-string v3, "signature"

    move-object/from16 v0, p7

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    iget-object v3, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v3, v10, v11}, Lcom/google/android/gms/games/provider/ag;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v3

    .line 644
    iget-object v4, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v3, v2, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1

    .line 649
    :cond_1
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 650
    const-string v4, "client_context_id"

    iget-object v5, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v5, v6}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 652
    const-string v4, "external_game_id"

    iget-object v5, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    const-string v4, "external_leaderboard_id"

    invoke-virtual {v2, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    const-string v4, "external_player_id"

    invoke-virtual {v2, v4, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    const-string v4, "raw_score"

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 657
    const-string v4, "achieved_timestamp"

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 658
    const-string v4, "score_tag"

    move-object/from16 v0, p6

    invoke-virtual {v2, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    const-string v4, "signature"

    move-object/from16 v0, p7

    invoke-virtual {v2, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 660
    iget-object v4, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 663
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2, v3}, Lcom/google/android/gms/games/service/GamesUploadService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    goto/16 :goto_1

    :cond_2
    move-wide v6, v8

    goto/16 :goto_0
.end method

.method private a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Z)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1373
    if-nez p2, :cond_1

    .line 1410
    :cond_0
    :goto_0
    return-void

    .line 1379
    :cond_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1380
    new-instance v4, Ljava/util/ArrayList;

    add-int/lit8 v0, v3, 0x1

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1384
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v3}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v1

    .line 1385
    :goto_1
    if-ge v2, v3, :cond_3

    .line 1386
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/bw;

    .line 1387
    if-eqz v0, :cond_2

    .line 1388
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bw;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1389
    invoke-static {p1, v0, v2, v4}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/bw;ILjava/util/ArrayList;)V

    .line 1385
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1393
    :cond_3
    if-eqz p3, :cond_4

    .line 1396
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v2, 0x1f4

    if-gt v0, v2, :cond_5

    const/4 v0, 0x1

    :goto_2
    const-string v1, "Attempting to preserve too many leaderboards!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 1398
    const-string v0, "external_leaderboard_id NOT IN "

    invoke-static {v0, v5}, Lcom/google/android/gms/common/e/d;->a(Ljava/lang/String;Ljava/util/Collection;)Lcom/google/android/gms/common/e/d;

    move-result-object v0

    .line 1399
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->j()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/games/provider/ai;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v1

    .line 1401
    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/d;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/gms/common/e/d;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1407
    :cond_4
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1408
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "LeaderboardAgent"

    invoke-static {v0, v4, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    goto :goto_0

    :cond_5
    move v0, v1

    .line 1396
    goto :goto_2
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;JJ)Z
    .locals 5

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 668
    .line 669
    invoke-static {p1, p2}, Lcom/google/android/gms/games/provider/ai;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 670
    new-instance v4, Lcom/google/android/gms/games/a/o;

    invoke-direct {v4, p0}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v0}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    sget-object v4, Lcom/google/android/gms/games/a/ay;->b:[Ljava/lang/String;

    iput-object v4, v0, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v4

    .line 675
    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 676
    const/4 v0, 0x0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 679
    :goto_0
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 683
    if-ne v0, v1, :cond_0

    move v0, v2

    .line 691
    :goto_1
    return v0

    .line 679
    :catchall_0
    move-exception v0

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v0

    .line 687
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 693
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown score order "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 689
    :pswitch_0
    cmp-long v0, p5, p3

    if-gez v0, :cond_1

    move v0, v2

    goto :goto_1

    :cond_1
    move v0, v3

    goto :goto_1

    .line 691
    :pswitch_1
    cmp-long v0, p5, p3

    if-lez v0, :cond_2

    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v3

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0

    .line 687
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Z
    .locals 18

    .prologue
    .line 729
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 730
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 731
    const/4 v11, 0x1

    .line 732
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 733
    invoke-static {v13}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 734
    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v15

    .line 735
    invoke-static {v2}, Lcom/google/android/gms/games/provider/ag;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    .line 739
    if-nez p2, :cond_1

    .line 740
    new-instance v3, Lcom/google/android/gms/games/a/o;

    invoke-direct {v3, v13}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    const-string v4, "account_name=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v15, v5, v6

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/games/a/o;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/games/a/ba;->a:[Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v2

    move-object v12, v2

    .line 752
    :goto_0
    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 755
    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 756
    const/4 v2, 0x1

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 758
    const/4 v2, 0x2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 759
    const/4 v2, 0x3

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 760
    const/4 v2, 0x5

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 761
    const/4 v3, 0x6

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 762
    invoke-static {v3, v2, v15}, Lcom/google/android/gms/games/a/l;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v4

    .line 764
    const/4 v2, 0x4

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    move-object/from16 v3, p0

    .line 769
    :try_start_1
    invoke-direct/range {v3 .. v10}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/h/a/dg;

    .line 772
    const/4 v2, 0x1

    .line 776
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v3, v11

    .line 791
    :goto_1
    if-eqz v2, :cond_0

    .line 792
    :try_start_2
    move-wide/from16 v0, v16

    invoke-static {v4, v0, v1}, Lcom/google/android/gms/games/provider/ag;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v2

    .line 794
    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v4

    invoke-virtual {v2, v4}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    move v11, v3

    .line 798
    goto :goto_0

    .line 745
    :cond_1
    new-instance v3, Lcom/google/android/gms/games/a/o;

    invoke-direct {v3, v13}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    const-string v4, "account_name=? AND external_leaderboard_id=?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v15, v5, v6

    const/4 v6, 0x1

    aput-object p2, v5, v6

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/games/a/o;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/games/a/ba;->a:[Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v2

    move-object v12, v2

    goto :goto_0

    .line 777
    :catch_0
    move-exception v2

    .line 778
    const/4 v3, 0x0

    .line 779
    :try_start_3
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 780
    const-string v5, "LeaderboardAgent"

    invoke-static {v2, v5}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 782
    :cond_2
    invoke-static {v2}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 783
    const-string v2, "LeaderboardAgent"

    const-string v5, "Could not submit score, will try again later"

    invoke-static {v2, v5}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 784
    const/4 v2, 0x0

    goto :goto_1

    .line 786
    :cond_3
    const/4 v2, 0x1

    goto :goto_1

    .line 800
    :cond_4
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 804
    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "LeaderboardAgent"

    invoke-static {v2, v14, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 805
    return v11

    .line 800
    :catchall_0
    move-exception v2

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v2
.end method

.method private b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J
    .locals 5

    .prologue
    const-wide/16 v0, -0x1

    .line 1249
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/games/a/ay;->d:Lcom/google/android/gms/games/h/a/cc;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, p2, v4}, Lcom/google/android/gms/games/h/a/cc;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/h/a/bw;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1257
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1258
    const/4 v4, -0x1

    invoke-static {p1, v2, v4, v3}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/bw;ILjava/util/ArrayList;)V

    .line 1259
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 1272
    :cond_0
    :goto_0
    return-wide v0

    .line 1252
    :catch_0
    move-exception v2

    const-string v2, "LeaderboardAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to retrieve leaderboard "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1265
    :cond_1
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v4, "LeaderboardAgent"

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1267
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 1270
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentProviderResult;

    iget-object v0, v0, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    .line 1271
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 1272
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;II)J
    .locals 4

    .prologue
    const-wide/16 v0, -0x1

    .line 1223
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v3, p2}, Lcom/google/android/gms/games/provider/ai;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;Landroid/net/Uri;)J

    move-result-wide v2

    .line 1226
    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    .line 1227
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/a/ay;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J

    move-result-wide v2

    .line 1228
    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    .line 1241
    :goto_0
    return-wide v0

    .line 1236
    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/af;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1238
    new-instance v1, Lcom/google/android/gms/common/e/b;

    invoke-direct {v1, v0}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 1239
    const-string v2, "timespan"

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1240
    const-string v2, "collection"

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1241
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v1, v1, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    invoke-static {v2, v0, v3, v1}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/h/a/cb;
    .locals 12

    .prologue
    .line 1054
    const/4 v7, 0x0

    .line 1057
    const/4 v1, 0x1

    move/from16 v0, p6

    if-ne v0, v1, :cond_4

    .line 1058
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/a/ay;->f:Lcom/google/android/gms/games/h/a/ez;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static/range {p4 .. p4}, Lcom/google/android/gms/games/internal/b/c;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p3}, Lcom/google/android/gms/games/internal/b/i;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const-string v8, "leaderboards/%1$s/window/%2$s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v3}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v8, "timeSpan"

    invoke-static {v4}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v8, v4}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz p8, :cond_0

    const-string v3, "language"

    invoke-static/range {p8 .. p8}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v3, v8}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_0
    if-eqz v5, :cond_1

    const-string v3, "maxResults"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v3, v5}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_1
    if-eqz p7, :cond_2

    const-string v3, "pageToken"

    invoke-static/range {p7 .. p7}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v3, v5}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_2
    if-eqz v6, :cond_3

    const-string v3, "returnTopIfAbsent"

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v3, v5}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_3
    iget-object v1, v1, Lcom/google/android/gms/games/h/a/ez;->a:Lcom/google/android/gms/common/server/n;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const-class v6, Lcom/google/android/gms/games/h/a/cb;

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/h/a/cb;

    .line 1077
    :goto_0
    return-object v1

    .line 1063
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/games/a/ay;->f:Lcom/google/android/gms/games/h/a/ez;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static/range {p4 .. p4}, Lcom/google/android/gms/games/internal/b/c;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p3}, Lcom/google/android/gms/games/internal/b/i;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, "leaderboards/%1$s/scores/%2$s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v3}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v8, v9

    invoke-static {v6, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "timeSpan"

    invoke-static {v4}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v6, v4}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz p8, :cond_5

    const-string v3, "language"

    invoke-static/range {p8 .. p8}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v3, v6}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_5
    if-eqz v5, :cond_6

    const-string v3, "maxResults"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v3, v5}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_6
    if-eqz p7, :cond_7

    const-string v3, "pageToken"

    invoke-static/range {p7 .. p7}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v3, v5}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_7
    iget-object v1, v1, Lcom/google/android/gms/games/h/a/ez;->a:Lcom/google/android/gms/common/server/n;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const-class v6, Lcom/google/android/gms/games/h/a/cb;

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/h/a/cb;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1068
    :catch_0
    move-exception v1

    .line 1069
    const-string v2, "LeaderboardAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to retrieve leaderboard scores for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p3}, Lcom/google/android/gms/games/internal/b/i;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/google/android/gms/games/internal/dq;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1073
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1074
    const-string v2, "LeaderboardAgent"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    :cond_8
    move-object v1, v7

    goto/16 :goto_0
.end method

.method private c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/h/a/cb;
    .locals 12

    .prologue
    .line 1084
    const/4 v7, 0x0

    .line 1087
    const/4 v1, 0x1

    move/from16 v0, p6

    if-ne v0, v1, :cond_4

    .line 1090
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/a/ay;->g:Lcom/google/android/gms/games/h/a/fa;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static/range {p4 .. p4}, Lcom/google/android/gms/games/internal/b/c;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p3}, Lcom/google/android/gms/games/internal/b/i;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const-string v8, "leaderboards/%1$s/window/%2$s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/fa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v3}, Lcom/google/android/gms/games/h/a/fa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v8, "timeSpan"

    invoke-static {v4}, Lcom/google/android/gms/games/h/a/fa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v8, v4}, Lcom/google/android/gms/games/h/a/fa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz p8, :cond_0

    const-string v3, "language"

    invoke-static/range {p8 .. p8}, Lcom/google/android/gms/games/h/a/fa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v3, v8}, Lcom/google/android/gms/games/h/a/fa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_0
    if-eqz v5, :cond_1

    const-string v3, "maxResults"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v3, v5}, Lcom/google/android/gms/games/h/a/fa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_1
    if-eqz p7, :cond_2

    const-string v3, "pageToken"

    invoke-static/range {p7 .. p7}, Lcom/google/android/gms/games/h/a/fa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v3, v5}, Lcom/google/android/gms/games/h/a/fa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_2
    if-eqz v6, :cond_3

    const-string v3, "returnTopIfAbsent"

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v3, v5}, Lcom/google/android/gms/games/h/a/fa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_3
    iget-object v1, v1, Lcom/google/android/gms/games/h/a/fa;->a:Lcom/google/android/gms/common/server/n;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const-class v6, Lcom/google/android/gms/games/h/a/cb;

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/h/a/cb;

    .line 1109
    :goto_0
    return-object v1

    .line 1096
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/games/a/ay;->g:Lcom/google/android/gms/games/h/a/fa;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static/range {p4 .. p4}, Lcom/google/android/gms/games/internal/b/c;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p3}, Lcom/google/android/gms/games/internal/b/i;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, "leaderboards/%1$s/scores/%2$s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/fa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v3}, Lcom/google/android/gms/games/h/a/fa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v8, v9

    invoke-static {v6, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "timeSpan"

    invoke-static {v4}, Lcom/google/android/gms/games/h/a/fa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v6, v4}, Lcom/google/android/gms/games/h/a/fa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz p8, :cond_5

    const-string v3, "language"

    invoke-static/range {p8 .. p8}, Lcom/google/android/gms/games/h/a/fa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v3, v6}, Lcom/google/android/gms/games/h/a/fa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_5
    if-eqz v5, :cond_6

    const-string v3, "maxResults"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v3, v5}, Lcom/google/android/gms/games/h/a/fa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_6
    if-eqz p7, :cond_7

    const-string v3, "pageToken"

    invoke-static/range {p7 .. p7}, Lcom/google/android/gms/games/h/a/fa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v3, v5}, Lcom/google/android/gms/games/h/a/fa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_7
    iget-object v1, v1, Lcom/google/android/gms/games/h/a/fa;->a:Lcom/google/android/gms/common/server/n;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const-class v6, Lcom/google/android/gms/games/h/a/cb;

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/h/a/cb;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1100
    :catch_0
    move-exception v1

    .line 1101
    const-string v2, "LeaderboardAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to retrieve leaderboard scores for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p3}, Lcom/google/android/gms/games/internal/b/i;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/google/android/gms/games/internal/dq;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1105
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1106
    const-string v2, "LeaderboardAgent"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    :cond_8
    move-object v1, v7

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 236
    .line 237
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->g:Z

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/gms/games/provider/ai;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/games/a/ay;->i:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long/2addr v4, v8

    const-wide/32 v8, 0x36ee80

    cmp-long v0, v4, v8

    if-gtz v0, :cond_3

    move v0, v2

    :goto_0
    if-nez v0, :cond_8

    .line 240
    :cond_0
    :try_start_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    move-object v0, v1

    move v1, v2

    :goto_1
    if-nez v0, :cond_1

    if-eqz v1, :cond_7

    :cond_1
    iget-boolean v1, p1, Lcom/google/android/gms/games/a/au;->h:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/games/a/ay;->e:Lcom/google/android/gms/games/h/a/cd;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v8, v0}, Lcom/google/android/gms/games/h/a/cd;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/h/a/by;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/by;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/by;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    move-object v10, v0

    move-object v0, v1

    move-object v1, v10

    :goto_2
    if-eqz v1, :cond_2

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_2
    move v1, v6

    goto :goto_1

    :cond_3
    move v0, v6

    .line 237
    goto :goto_0

    .line 240
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/a/ay;->d:Lcom/google/android/gms/games/h/a/cc;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    const-string v3, "leaderboards"

    if-eqz v8, :cond_5

    const-string v4, "language"

    invoke-static {v8}, Lcom/google/android/gms/games/h/a/cc;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/games/h/a/cc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_5
    if-eqz v0, :cond_6

    const-string v4, "pageToken"

    invoke-static {v0}, Lcom/google/android/gms/games/h/a/cc;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/games/h/a/cc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_6
    iget-object v0, v2, Lcom/google/android/gms/games/h/a/cc;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/bz;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/bz;

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bz;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bz;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    move-object v10, v0

    move-object v0, v1

    move-object v1, v10

    goto :goto_2

    .line 241
    :cond_7
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/provider/ai;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/a/ay;->i:Ljava/util/HashMap;

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    const/4 v0, 0x1

    invoke-direct {p0, p1, v7, v0}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Z)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v6

    .line 254
    :goto_3
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/games/provider/af;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 256
    new-instance v2, Lcom/google/android/gms/games/a/o;

    invoke-direct {v2, p1}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v1

    const-string v2, "sorting_rank,name,external_leaderboard_id,timespan DESC,collection"

    iput-object v2, v1, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    iput v0, v1, Lcom/google/android/gms/games/a/o;->d:I

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0

    .line 243
    :catch_0
    move-exception v0

    .line 244
    const-string v1, "LeaderboardAgent"

    const-string v2, "Unable to retrieve leaderboard list"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 246
    const/4 v0, 0x3

    goto :goto_3

    :cond_8
    move v0, v6

    goto :goto_3
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;II)Lcom/google/android/gms/common/data/DataHolder;
    .locals 11

    .prologue
    .line 282
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v6

    .line 283
    iget-object v8, p1, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    .line 284
    const-string v0, "Cannot lookup score for a null player ID!"

    invoke-static {v8, v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 286
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/ay;->f:Lcom/google/android/gms/games/h/a/ez;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p3}, Lcom/google/android/gms/games/internal/b/i;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p4}, Lcom/google/android/gms/games/internal/b/c;->a(I)Ljava/lang/String;

    move-result-object v4

    const-string v3, "players/%1$s/leaderboards/%2$s/scores/%3$s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v8}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v5, v9

    const/4 v9, 0x1

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v5, v9

    const/4 v9, 0x2

    invoke-static {v2}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v9

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v4, :cond_0

    const-string v2, "includeRankType"

    invoke-static {v4}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v2, v4}, Lcom/google/android/gms/games/h/a/ez;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/games/h/a/ez;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/da;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/da;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/da;->getPlayer()Lcom/google/android/gms/games/h/a/cs;

    move-result-object v2

    .line 299
    invoke-static {v2}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 302
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/da;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 303
    if-nez v1, :cond_2

    .line 304
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 365
    :goto_0
    return-object v0

    .line 290
    :catch_0
    move-exception v0

    .line 291
    const-string v1, "LeaderboardAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error getting scores for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 293
    const-string v1, "LeaderboardAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 295
    :cond_1
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 306
    :cond_2
    const/4 v3, 0x2

    if-ge v1, v3, :cond_3

    const/4 v1, 0x1

    :goto_1
    const-string v3, "Found multiple entries for player (%s), in leaderboard (%s)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v8, v4, v5

    const/4 v5, 0x1

    aput-object p2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 311
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/da;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/cz;

    .line 312
    packed-switch p4, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 306
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 312
    :pswitch_0
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/cz;->getPublicRank()Lcom/google/android/gms/games/h/a/ca;

    move-result-object v1

    .line 318
    :goto_2
    iget-object v0, v0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    .line 319
    iget-object v3, v2, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 322
    if-eqz v1, :cond_6

    .line 323
    const-string v3, "rank"

    invoke-virtual {v1}, Lcom/google/android/gms/games/h/a/ca;->c()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 324
    const-string v3, "display_rank"

    invoke-virtual {v1}, Lcom/google/android/gms/games/h/a/ca;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    :goto_3
    iget-object v1, v2, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 337
    const-string v1, "last_updated"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 340
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/content/ContentValues;)V

    .line 342
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1}, Lcom/google/android/gms/games/provider/ad;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    .line 344
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 345
    const-string v3, "profile_icon_image_url"

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v2}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v3

    .line 347
    const-string v4, "profile_hi_res_image_url"

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4, v2}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v1

    .line 352
    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "LeaderboardAgent"

    invoke-static {v4, v2, v5}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 354
    if-eqz v3, :cond_4

    .line 355
    const-string v4, "profile_icon_image_url"

    const-string v5, "profile_icon_image_uri"

    invoke-static {v0, v4, v5, v2, v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 357
    const-string v4, "default_display_image_url"

    const-string v5, "default_display_image_uri"

    invoke-static {v0, v4, v5, v2, v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 360
    :cond_4
    if-eqz v1, :cond_5

    .line 361
    const-string v3, "profile_hi_res_image_url"

    const-string v4, "profile_hi_res_image_uri"

    invoke-static {v0, v3, v4, v2, v1}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 365
    :cond_5
    sget-object v1, Lcom/google/android/gms/games/a/ay;->c:Lcom/google/android/gms/games/provider/a;

    iget-object v1, v1, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lcom/google/android/gms/common/data/m;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/data/m;->a(Landroid/content/ContentValues;)Lcom/google/android/gms/common/data/m;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/data/m;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto/16 :goto_0

    .line 312
    :pswitch_1
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/cz;->getSocialRank()Lcom/google/android/gms/games/h/a/ca;

    move-result-object v1

    goto/16 :goto_2

    .line 327
    :cond_6
    const-string v1, "rank"

    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 328
    const-string v1, "display_rank"

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/gms/p;->jC:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 312
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;III)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    .prologue
    .line 849
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IIII)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IIIII)Lcom/google/android/gms/common/data/DataHolder;
    .locals 14

    .prologue
    .line 982
    invoke-direct/range {p0 .. p4}, Lcom/google/android/gms/games/a/ay;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;II)J

    move-result-wide v12

    .line 984
    const-wide/16 v2, -0x1

    cmp-long v2, v12, v2

    if-nez v2, :cond_0

    .line 985
    const-string v2, "LeaderboardAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "No instance found for leaderboard "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static/range {p4 .. p4}, Lcom/google/android/gms/games/internal/b/c;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/games/internal/b/i;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 988
    const/4 v2, 0x4

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    .line 1034
    :goto_0
    return-object v2

    .line 995
    :cond_0
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    .line 996
    new-instance v11, Lcom/google/android/gms/games/b/s;

    move/from16 v0, p6

    invoke-direct {v11, v12, v13, v0}, Lcom/google/android/gms/games/b/s;-><init>(JI)V

    .line 997
    iget-object v2, p0, Lcom/google/android/gms/games/a/ay;->h:Lcom/google/android/gms/games/b/o;

    invoke-virtual {v2, v11, v4, v5}, Lcom/google/android/gms/games/b/o;->a(Ljava/lang/Object;J)Z

    move-result v2

    if-nez v2, :cond_1

    .line 999
    iget-boolean v2, p1, Lcom/google/android/gms/games/a/au;->g:Z

    if-nez v2, :cond_5

    .line 1000
    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->c()Lcom/google/android/gms/games/a/av;

    move-result-object v2

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/android/gms/games/a/av;->g:Z

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object v3

    :goto_1
    move-object v2, p0

    move-object/from16 v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    .line 1004
    invoke-direct/range {v2 .. v8}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IIII)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto :goto_0

    .line 1010
    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/games/a/ay;->h:Lcom/google/android/gms/games/b/o;

    packed-switch p7, :pswitch_data_0

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown page direction "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p7

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_0
    invoke-virtual {v3, v11, v4, v5}, Lcom/google/android/gms/games/b/u;->b(Ljava/lang/Object;J)Ljava/lang/String;

    move-result-object v9

    .line 1011
    :goto_2
    if-eqz v9, :cond_2

    .line 1012
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    invoke-direct/range {v2 .. v10}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/h/a/cb;

    move-result-object v9

    .line 1016
    if-eqz v9, :cond_4

    .line 1017
    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v5, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    move-object v3, p0

    move-wide v6, v12

    move/from16 v8, p6

    move/from16 v10, p7

    invoke-direct/range {v3 .. v10}, Lcom/google/android/gms/games/a/ay;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;JILcom/google/android/gms/games/h/a/cb;I)V

    .line 1027
    :cond_2
    :goto_3
    invoke-static {}, Lcom/google/android/gms/games/e/g;->a()Lcom/google/android/gms/games/e/h;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/e/h;->a(Ljava/lang/String;)Lcom/google/android/gms/games/e/h;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/e/h;->b(Ljava/lang/String;)Lcom/google/android/gms/games/e/h;

    move-result-object v2

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/e/h;->a(I)Lcom/google/android/gms/games/e/h;

    move-result-object v2

    move/from16 v0, p4

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/e/h;->b(I)Lcom/google/android/gms/games/e/h;

    move-result-object v2

    move/from16 v0, p6

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/e/h;->c(I)Lcom/google/android/gms/games/e/h;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/e/h;->a()Lcom/google/android/gms/games/e/g;

    move-result-object v2

    .line 1034
    iget-object v3, p0, Lcom/google/android/gms/games/a/ay;->h:Lcom/google/android/gms/games/b/o;

    iget-object v2, v2, Lcom/google/android/gms/games/e/g;->a:Landroid/os/Bundle;

    const/4 v4, -0x1

    invoke-virtual {v3, v11, v2, v4}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto/16 :goto_0

    .line 1010
    :pswitch_1
    iget-object v2, v3, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v2, v11}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/b/v;

    if-eqz v2, :cond_3

    invoke-virtual {v3, v11, v4, v5}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;J)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v2, v2, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    iget-object v9, v2, Lcom/google/android/gms/common/data/ai;->a:Ljava/lang/String;

    goto :goto_2

    :cond_3
    const/4 v9, 0x0

    goto :goto_2

    .line 1021
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/a/ay;->h:Lcom/google/android/gms/games/b/o;

    const/4 v3, 0x3

    invoke-virtual {v2, v11, v3}, Lcom/google/android/gms/games/b/o;->a(Ljava/lang/Object;I)V

    goto :goto_3

    :cond_5
    move-object v3, p1

    goto/16 :goto_1

    .line 1010
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 435
    const/4 v2, 0x0

    .line 437
    if-nez p3, :cond_0

    .line 439
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/ay;->d:Lcom/google/android/gms/games/h/a/cc;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, p2, v4}, Lcom/google/android/gms/games/h/a/cc;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/h/a/bw;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v2, v0

    move v0, v1

    .line 449
    :goto_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 450
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 451
    invoke-direct {p0, p1, v3, v1}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Z)V

    .line 452
    invoke-static {p1, p2, v0}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0

    .line 442
    :catch_0
    move-exception v0

    const-string v0, "LeaderboardAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to retrieve leaderboard "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    const/4 v0, 0x3

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;JJLjava/lang/String;Z)Lcom/google/android/gms/games/e/q;
    .locals 21

    .prologue
    .line 531
    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    .line 532
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-wide/from16 v2, p3

    move-object/from16 v4, p7

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/games/a/aw;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 536
    if-nez p8, :cond_0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-wide/from16 v8, p3

    move-wide/from16 v10, p5

    move-object/from16 v12, p7

    .line 537
    invoke-static/range {v6 .. v13}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V

    .line 539
    new-instance v6, Lcom/google/android/gms/games/e/q;

    const/4 v7, 0x5

    move-object/from16 v0, p2

    invoke-direct {v6, v7, v0, v15}, Lcom/google/android/gms/games/e/q;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 591
    :goto_0
    return-object v6

    .line 546
    :cond_0
    :try_start_0
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v6, p0

    move-object/from16 v8, p2

    move-wide/from16 v9, p3

    move-object/from16 v12, p7

    invoke-direct/range {v6 .. v13}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/h/a/dg;

    move-result-object v14

    .line 552
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v6, v1}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 573
    new-instance v16, Ljava/util/HashMap;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashMap;-><init>()V

    .line 574
    invoke-virtual {v14}, Lcom/google/android/gms/games/h/a/dg;->getUnbeatenScores()Ljava/util/ArrayList;

    move-result-object v17

    .line 575
    if-eqz v17, :cond_3

    .line 576
    const/4 v6, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v18

    move v13, v6

    :goto_1
    move/from16 v0, v18

    if-ge v13, v0, :cond_3

    .line 577
    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/games/h/a/de;

    .line 578
    invoke-virtual {v6}, Lcom/google/android/gms/games/h/a/de;->e()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/gms/games/internal/b/i;->a(Ljava/lang/String;)I

    move-result v7

    .line 579
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    new-instance v7, Lcom/google/android/gms/games/e/r;

    invoke-virtual {v6}, Lcom/google/android/gms/games/h/a/de;->c()Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v6}, Lcom/google/android/gms/games/h/a/de;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6}, Lcom/google/android/gms/games/h/a/de;->d()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-direct/range {v7 .. v12}, Lcom/google/android/gms/games/e/r;-><init>(JLjava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 576
    add-int/lit8 v6, v13, 0x1

    move v13, v6

    goto :goto_1

    .line 553
    :catch_0
    move-exception v6

    move-object v7, v6

    .line 554
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 555
    const-string v6, "LeaderboardAgent"

    invoke-static {v7, v6}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 559
    :cond_1
    const/4 v6, 0x6

    .line 560
    invoke-static {v7}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 561
    const-string v6, "LeaderboardAgent"

    const-string v7, "Could not submit score. Deferring for later"

    invoke-static {v6, v7}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    const/4 v14, 0x5

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-wide/from16 v8, p3

    move-wide/from16 v10, p5

    move-object/from16 v12, p7

    .line 563
    invoke-static/range {v6 .. v13}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V

    move v6, v14

    .line 569
    :goto_2
    new-instance v7, Lcom/google/android/gms/games/e/q;

    move-object/from16 v0, p2

    invoke-direct {v7, v6, v0, v15}, Lcom/google/android/gms/games/e/q;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    move-object v6, v7

    goto/16 :goto_0

    .line 566
    :cond_2
    const-string v7, "LeaderboardAgent"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Encountered a hard error while submitting score for leaderboard "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " and player "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 583
    :cond_3
    invoke-virtual {v14}, Lcom/google/android/gms/games/h/a/dg;->b()Ljava/util/ArrayList;

    move-result-object v17

    .line 584
    if-eqz v17, :cond_4

    .line 585
    const/4 v6, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v18

    move v13, v6

    :goto_3
    move/from16 v0, v18

    if-ge v13, v0, :cond_4

    .line 586
    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/gms/games/internal/b/i;->a(Ljava/lang/String;)I

    move-result v6

    .line 587
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    new-instance v7, Lcom/google/android/gms/games/e/r;

    invoke-virtual {v14}, Lcom/google/android/gms/games/h/a/dg;->c()Ljava/lang/String;

    move-result-object v10

    const/4 v12, 0x1

    move-wide/from16 v8, p3

    move-object/from16 v11, p7

    invoke-direct/range {v7 .. v12}, Lcom/google/android/gms/games/e/r;-><init>(JLjava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 585
    add-int/lit8 v6, v13, 0x1

    move v13, v6

    goto :goto_3

    .line 591
    :cond_4
    new-instance v6, Lcom/google/android/gms/games/e/q;

    const/4 v7, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-direct {v6, v7, v0, v15, v1}, Lcom/google/android/gms/games/e/q;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V

    goto/16 :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/gms/games/a/ay;->h:Lcom/google/android/gms/games/b/o;

    iget-object v0, v0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0}, Landroid/support/v4/g/h;->b()V

    .line 231
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 489
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 490
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 492
    const/4 v2, 0x0

    move v3, v0

    .line 494
    :goto_0
    if-nez v2, :cond_0

    if-eqz v3, :cond_2

    .line 496
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/games/a/ay;->e:Lcom/google/android/gms/games/h/a/cd;

    iget-object v6, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v7, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v3, v6, v7, v5, v2}, Lcom/google/android/gms/games/h/a/cd;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/h/a/by;

    move-result-object v3

    .line 499
    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/by;->b()Ljava/lang/String;

    move-result-object v2

    .line 500
    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/by;->getItems()Ljava/util/ArrayList;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 501
    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/by;->getItems()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    move v3, v1

    .line 503
    goto :goto_0

    .line 505
    :catch_0
    move-exception v0

    const-string v0, "LeaderboardAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to sync leaderboards for game "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    iget-object v0, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    move v0, v1

    .line 511
    :goto_1
    return v0

    .line 510
    :cond_2
    invoke-direct {p0, p1, v4, v0}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Z)V

    goto :goto_1
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;III)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    .prologue
    .line 872
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IIII)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)V
    .locals 6

    .prologue
    .line 711
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 712
    iget-object v0, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    .line 714
    :cond_0
    return-void
.end method
