.class public final Lcom/google/android/gms/plus/internal/cn;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:[Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

.field private final g:Ljava/util/ArrayList;

.field private h:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/cn;->g:Ljava/util/ArrayList;

    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/cn;->c:Ljava/lang/String;

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/cn;->b:Ljava/lang/String;

    .line 39
    new-instance v0, Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-direct {v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/cn;->f:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/cn;->g:Ljava/util/ArrayList;

    const-string v1, "https://www.googleapis.com/auth/plus.login"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/plus/internal/cn;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/cn;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 78
    return-object p0
.end method

.method public final varargs a([Ljava/lang/String;)Lcom/google/android/gms/plus/internal/cn;
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/cn;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/cn;->g:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 69
    return-object p0
.end method

.method public final b()Lcom/google/android/gms/plus/internal/PlusSession;
    .locals 9

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/cn;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 155
    const-string v0, "<<default account>>"

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/cn;->a:Ljava/lang/String;

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/cn;->g:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/cn;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 159
    new-instance v0, Lcom/google/android/gms/plus/internal/PlusSession;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/cn;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/internal/cn;->h:[Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/cn;->d:[Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/cn;->b:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/plus/internal/cn;->c:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/plus/internal/cn;->e:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/gms/plus/internal/cn;->f:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/plus/internal/PlusSession;-><init>(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/PlusCommonExtras;)V

    return-object v0
.end method
