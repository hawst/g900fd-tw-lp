.class final Lcom/google/android/gms/car/ab;
.super Lcom/google/android/gms/car/pf;
.source "SourceFile"


# static fields
.field private static final a:Landroid/util/SparseArray;


# instance fields
.field private final c:Z

.field private final d:Lcom/google/android/gms/car/ad;

.field private final e:Lcom/google/android/gms/car/ad;

.field private final f:Ljava/util/Queue;

.field private final g:Ljava/lang/Object;

.field private final h:Ljava/util/concurrent/Semaphore;

.field private final i:Ljava/lang/String;

.field private final j:Landroid/content/ComponentName;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 985
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 987
    sput-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/4 v1, 0x0

    const-string v2, "SETUP"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 988
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/4 v1, 0x1

    const-string v2, "SETUP_COMPLETE"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 989
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/4 v1, 0x2

    const-string v2, "START"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 990
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/4 v1, 0x3

    const-string v2, "START_COMPLETE"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 991
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/4 v1, 0x4

    const-string v2, "STOP"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 992
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/4 v1, 0x5

    const-string v2, "STOP_COMPLETE"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 993
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/4 v1, 0x6

    const-string v2, "RESUME"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 994
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/4 v1, 0x7

    const-string v2, "RESUME_COMPLETE"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 995
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/16 v1, 0x8

    const-string v2, "VIDEO_CONFIG_CHANGE"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 996
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/16 v1, 0x9

    const-string v2, "VIDEO_CONFIG_CHANGE_COMPLETE"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 997
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/16 v1, 0xa

    const-string v2, "PAUSE"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 998
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/16 v1, 0xb

    const-string v2, "PAUSE_COMPLETE"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 999
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/16 v1, 0xc

    const-string v2, "ON_NEW_INTENT"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1000
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/16 v1, 0xd

    const-string v2, "ON_NEW_INTENT_COMPLETE"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1001
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/16 v1, 0xe

    const-string v2, "PRE_FINISH"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1002
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/16 v1, 0xf

    const-string v2, "FINISH"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1003
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/16 v1, 0x10

    const-string v2, "ON_CRASH"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1004
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/16 v1, 0x11

    const-string v2, "RELINQUISH_VIDEO_FOCUS"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1005
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/16 v1, 0x12

    const-string v2, "REQUEST_VIDEO_FOCUS"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1006
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/16 v1, 0x13

    const-string v2, "INPUT_FOCUS_CHANGE"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1007
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/16 v1, 0x14

    const-string v2, "INPUT_FOCUS_CHANGE_COMPLETE"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1008
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/16 v1, 0x15

    const-string v2, "START_INPUT"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1009
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/16 v1, 0x16

    const-string v2, "STOP_INPUT"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1010
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/16 v1, 0x17

    const-string v2, "ON_STOP_IME_INPUT"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1011
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/16 v1, 0x18

    const-string v2, "SET_CRASH_REPORTING_ENABLED"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1012
    sget-object v0, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    const/16 v1, 0x19

    const-string v2, "ON_WINDOW_REMOVED"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1013
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/car/y;Landroid/os/Looper;ZLandroid/content/ComponentName;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1074
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/pf;-><init>(Ljava/lang/Object;Landroid/os/Looper;)V

    .line 1048
    new-instance v0, Lcom/google/android/gms/car/ad;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/car/ad;-><init>(Lcom/google/android/gms/car/ab;B)V

    iput-object v0, p0, Lcom/google/android/gms/car/ab;->d:Lcom/google/android/gms/car/ad;

    .line 1054
    new-instance v0, Lcom/google/android/gms/car/ad;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/car/ad;-><init>(Lcom/google/android/gms/car/ab;B)V

    iput-object v0, p0, Lcom/google/android/gms/car/ab;->e:Lcom/google/android/gms/car/ad;

    .line 1060
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ab;->f:Ljava/util/Queue;

    .line 1062
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ab;->g:Ljava/lang/Object;

    .line 1069
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/ab;->h:Ljava/util/concurrent/Semaphore;

    .line 1075
    iput-boolean p3, p0, Lcom/google/android/gms/car/ab;->c:Z

    .line 1076
    iput-object p4, p0, Lcom/google/android/gms/car/ab;->j:Landroid/content/ComponentName;

    .line 1077
    invoke-virtual {p4}, Landroid/content/ComponentName;->getShortClassName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/ab;->i:Ljava/lang/String;

    .line 1078
    return-void
.end method

.method private B()V
    .locals 1

    .prologue
    .line 1344
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->d:Lcom/google/android/gms/car/ad;

    invoke-static {p0, v0}, Lcom/google/android/gms/car/qh;->b(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1345
    return-void
.end method

.method private C()V
    .locals 2

    .prologue
    .line 1530
    iget-object v0, p0, Lcom/google/android/gms/car/pf;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    .line 1531
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/gms/car/y;->c(Lcom/google/android/gms/car/y;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1536
    new-instance v1, Lcom/google/android/gms/car/ac;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/ac;-><init>(Lcom/google/android/gms/car/ab;)V

    .line 1550
    invoke-static {v0}, Lcom/google/android/gms/car/y;->c(Lcom/google/android/gms/car/y;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1552
    :cond_0
    return-void
.end method

.method private D()V
    .locals 1

    .prologue
    .line 1605
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->d:Lcom/google/android/gms/car/ad;

    invoke-static {p0, v0}, Lcom/google/android/gms/car/qh;->b(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1606
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->e:Lcom/google/android/gms/car/ad;

    invoke-static {p0, v0}, Lcom/google/android/gms/car/qh;->b(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1607
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/ab;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1608
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/ab;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 954
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->g:Ljava/lang/Object;

    return-object v0
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 1339
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->d:Lcom/google/android/gms/car/ad;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/ad;->a(I)V

    .line 1340
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->d:Lcom/google/android/gms/car/ad;

    invoke-static {p0, v0}, Lcom/google/android/gms/car/qh;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1341
    return-void
.end method

.method private a(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 1580
    iget-object v1, p0, Lcom/google/android/gms/car/ab;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 1581
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->f:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1582
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/car/ab;I)V
    .locals 0

    .prologue
    .line 954
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ab;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/ab;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 954
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ab;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1561
    const-string v0, "CAR.CAM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1562
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/ab;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; client crash for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1565
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/pf;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    .line 1566
    if-eqz v0, :cond_1

    .line 1567
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/y;->a(Ljava/lang/Throwable;)V

    .line 1569
    :cond_1
    return-void
.end method

.method private a(ZLjava/lang/Throwable;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1113
    if-eqz p1, :cond_1

    .line 1122
    :cond_0
    :goto_0
    return-void

    .line 1116
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->B()V

    .line 1117
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->h:Ljava/util/concurrent/Semaphore;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    .line 1118
    if-eqz p2, :cond_0

    .line 1119
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/ab;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; Exception calling "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1120
    invoke-direct {p0, p2}, Lcom/google/android/gms/car/ab;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/car/ab;)Ljava/util/Queue;
    .locals 1

    .prologue
    .line 954
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->f:Ljava/util/Queue;

    return-object v0
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 1513
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/ab;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; ANR waiting for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/gms/car/y;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to complete"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1515
    iget-boolean v0, p0, Lcom/google/android/gms/car/ab;->c:Z

    if-eqz v0, :cond_0

    .line 1516
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/ab;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; Ignoring ANR because ANR monitoring is disabled."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1527
    :goto_0
    return-void

    .line 1521
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/ab;->a()V

    .line 1522
    iget-object v0, p0, Lcom/google/android/gms/car/pf;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    .line 1523
    if-eqz v0, :cond_1

    .line 1524
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->S()V

    .line 1526
    :cond_1
    new-instance v0, Ljava/lang/Throwable;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ANR in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/ab;->j:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ab;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method final a()V
    .locals 2

    .prologue
    .line 1085
    iget-object v1, p0, Lcom/google/android/gms/car/ab;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 1086
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 1087
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1432
    const/16 v0, 0xc

    invoke-static {p0, v0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1433
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ab;->a(Landroid/os/Message;)V

    .line 1434
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    .line 1435
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;I)V
    .locals 2

    .prologue
    .line 1407
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-static {p0, v0, p2, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    .line 1409
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1410
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ab;->a(Landroid/os/Message;)V

    .line 1411
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    .line 1412
    return-void
.end method

.method public final a(Landroid/view/KeyEvent;Z)V
    .locals 3

    .prologue
    .line 1494
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->e:Lcom/google/android/gms/car/ad;

    invoke-static {p0, v0}, Lcom/google/android/gms/car/qh;->b(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1495
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1496
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/ab;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; KeyEvent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " handled = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1499
    :cond_0
    if-nez p2, :cond_1

    .line 1500
    iget-object v0, p0, Lcom/google/android/gms/car/pf;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    .line 1501
    if-eqz v0, :cond_1

    .line 1502
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/y;->b(Landroid/view/KeyEvent;)V

    .line 1505
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/ExceptionParcel;)V
    .locals 1

    .prologue
    .line 1458
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->D()V

    .line 1459
    invoke-virtual {p0}, Lcom/google/android/gms/car/ab;->a()V

    .line 1461
    const/16 v0, 0x10

    invoke-static {p0, v0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1462
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/ab;->sendMessage(Landroid/os/Message;)Z

    .line 1463
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/b/g;Landroid/view/inputmethod/EditorInfo;)V
    .locals 2

    .prologue
    .line 1573
    const/16 v0, 0x15

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1574
    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1575
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ab;->a(Landroid/os/Message;)V

    .line 1576
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    .line 1577
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1354
    iget-object v1, p0, Lcom/google/android/gms/car/ab;->e:Lcom/google/android/gms/car/ad;

    if-eqz p1, :cond_0

    const/16 v0, 0x3ea

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/ad;->a(I)V

    .line 1355
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->e:Lcom/google/android/gms/car/ad;

    invoke-static {p0, v0}, Lcom/google/android/gms/car/qh;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1356
    return-void

    .line 1354
    :cond_0
    const/16 v0, 0x3e9

    goto :goto_0
.end method

.method final b()V
    .locals 1

    .prologue
    .line 1091
    invoke-virtual {p0}, Lcom/google/android/gms/car/ab;->a()V

    .line 1092
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->D()V

    .line 1093
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->h:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->drainPermits()I

    .line 1094
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->h:Ljava/util/concurrent/Semaphore;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    .line 1095
    return-void
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1478
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 1479
    :goto_0
    const/16 v2, 0x13

    invoke-static {p0, v2, v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    .line 1480
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ab;->a(Landroid/os/Message;)V

    .line 1481
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    .line 1482
    return-void

    :cond_0
    move v0, v1

    .line 1478
    goto :goto_0
.end method

.method final c()V
    .locals 4

    .prologue
    .line 1099
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->h:Ljava/util/concurrent/Semaphore;

    const-wide/16 v2, 0x1388

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;J)Z

    move-result v0

    .line 1100
    if-nez v0, :cond_0

    .line 1101
    const/16 v0, 0x3e8

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ab;->b(I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1106
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 1598
    const/16 v0, 0x18

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 1599
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, v1, Landroid/os/Message;->arg1:I

    .line 1600
    invoke-direct {p0, v1}, Lcom/google/android/gms/car/ab;->a(Landroid/os/Message;)V

    .line 1601
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    .line 1602
    return-void

    .line 1599
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final d()V
    .locals 1

    .prologue
    .line 1109
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->h:Ljava/util/concurrent/Semaphore;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    .line 1110
    return-void
.end method

.method final e()V
    .locals 1

    .prologue
    .line 1359
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->e:Lcom/google/android/gms/car/ad;

    invoke-static {p0, v0}, Lcom/google/android/gms/car/qh;->b(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1360
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1363
    const/4 v0, 0x0

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1364
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ab;->a(Landroid/os/Message;)V

    .line 1365
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    .line 1366
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 1369
    const/4 v0, 0x1

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1370
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/ab;->sendMessage(Landroid/os/Message;)Z

    .line 1371
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 1374
    const/4 v0, 0x2

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1375
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ab;->a(Landroid/os/Message;)V

    .line 1376
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    .line 1377
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1126
    iget-object v0, p0, Lcom/google/android/gms/car/pf;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    .line 1127
    if-nez v0, :cond_1

    .line 1333
    :cond_0
    :goto_0
    return-void

    .line 1132
    :cond_1
    const/4 v2, 0x0

    .line 1134
    const-string v1, "CAR.CAM"

    const/4 v5, 0x3

    invoke-static {v1, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1135
    sget-object v1, Lcom/google/android/gms/car/ab;->a:Landroid/util/SparseArray;

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1136
    if-nez v1, :cond_2

    .line 1137
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "UNKNOWN ("

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ")"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1139
    :cond_2
    const-string v5, "CAR.CAM"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "; component: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v6, p0, Lcom/google/android/gms/car/ab;->i:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1142
    :cond_3
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 1329
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/ab;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; Unknown msg.what "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1145
    :pswitch_0
    :try_start_0
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/ab;->a(I)V

    .line 1146
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->x()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    move v1, v0

    move-object v0, v2

    .line 1150
    :goto_1
    const-string v2, "setup"

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/gms/car/ab;->a(ZLjava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1147
    :catch_0
    move-exception v0

    move v1, v3

    goto :goto_1

    .line 1153
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->B()V

    .line 1154
    invoke-static {}, Lcom/google/android/gms/car/y;->y()V

    .line 1155
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->h:Ljava/util/concurrent/Semaphore;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    .line 1156
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    goto/16 :goto_0

    .line 1160
    :pswitch_2
    :try_start_1
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/ab;->a(I)V

    .line 1161
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->z()Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_9

    move-result v3

    .line 1165
    :goto_2
    const-string v0, "start"

    invoke-direct {p0, v3, v2, v0}, Lcom/google/android/gms/car/ab;->a(ZLjava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1168
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->B()V

    .line 1169
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->A()V

    .line 1170
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->h:Ljava/util/concurrent/Semaphore;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    .line 1171
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    goto/16 :goto_0

    .line 1175
    :pswitch_4
    :try_start_2
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/ab;->a(I)V

    .line 1176
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->B()Z
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_8

    move-result v3

    .line 1180
    :goto_3
    const-string v0, "stop"

    invoke-direct {p0, v3, v2, v0}, Lcom/google/android/gms/car/ab;->a(ZLjava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1183
    :pswitch_5
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->D()V

    .line 1184
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->C()V

    .line 1185
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->h:Ljava/util/concurrent/Semaphore;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_0

    .line 1189
    :pswitch_6
    :try_start_3
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/ab;->a(I)V

    .line 1190
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->F()Z
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_7

    move-result v3

    .line 1194
    :goto_4
    const-string v0, "resume"

    invoke-direct {p0, v3, v2, v0}, Lcom/google/android/gms/car/ab;->a(ZLjava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1197
    :pswitch_7
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->B()V

    .line 1198
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->G()V

    .line 1199
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->h:Ljava/util/concurrent/Semaphore;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    .line 1200
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    goto/16 :goto_0

    .line 1204
    :pswitch_8
    :try_start_4
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/ab;->a(I)V

    .line 1205
    iget v4, p1, Landroid/os/Message;->arg1:I

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/content/res/Configuration;

    invoke-virtual {v0, v4, v1}, Lcom/google/android/gms/car/y;->a(ILandroid/content/res/Configuration;)Z
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_6

    move-result v3

    .line 1211
    :goto_5
    const-string v0, "video config change"

    invoke-direct {p0, v3, v2, v0}, Lcom/google/android/gms/car/ab;->a(ZLjava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1214
    :pswitch_9
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->B()V

    .line 1215
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->H()V

    .line 1216
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->h:Ljava/util/concurrent/Semaphore;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    .line 1217
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    goto/16 :goto_0

    .line 1221
    :pswitch_a
    :try_start_5
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/ab;->a(I)V

    .line 1222
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->D()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_5

    move-result v3

    .line 1226
    :goto_6
    const-string v0, "pause"

    invoke-direct {p0, v3, v2, v0}, Lcom/google/android/gms/car/ab;->a(ZLjava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1229
    :pswitch_b
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->B()V

    .line 1230
    invoke-static {}, Lcom/google/android/gms/car/y;->E()V

    .line 1231
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->h:Ljava/util/concurrent/Semaphore;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    .line 1232
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    goto/16 :goto_0

    .line 1236
    :pswitch_c
    :try_start_6
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/ab;->a(I)V

    .line 1237
    iget v1, p1, Landroid/os/Message;->arg1:I

    if-ne v1, v3, :cond_4

    move v4, v3

    :cond_4
    invoke-virtual {v0, v4}, Lcom/google/android/gms/car/y;->b(Z)Z
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_4

    move-result v3

    .line 1241
    :goto_7
    const-string v0, "input focus changed"

    invoke-direct {p0, v3, v2, v0}, Lcom/google/android/gms/car/ab;->a(ZLjava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1244
    :pswitch_d
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->B()V

    .line 1245
    invoke-static {}, Lcom/google/android/gms/car/y;->I()V

    .line 1246
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->h:Ljava/util/concurrent/Semaphore;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    .line 1247
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    goto/16 :goto_0

    .line 1251
    :pswitch_e
    :try_start_7
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/ab;->a(I)V

    .line 1252
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/y;->b(Landroid/content/Intent;)Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_3

    move-result v3

    .line 1256
    :goto_8
    const-string v0, "new intent"

    invoke-direct {p0, v3, v2, v0}, Lcom/google/android/gms/car/ab;->a(ZLjava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1259
    :pswitch_f
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->B()V

    .line 1260
    invoke-static {}, Lcom/google/android/gms/car/y;->J()V

    .line 1261
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->h:Ljava/util/concurrent/Semaphore;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    .line 1262
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    goto/16 :goto_0

    .line 1265
    :pswitch_10
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->K()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1266
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->D()V

    .line 1268
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->h:Ljava/util/concurrent/Semaphore;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    .line 1269
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    goto/16 :goto_0

    .line 1272
    :pswitch_11
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->K()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1273
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->M()V

    .line 1275
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->h:Ljava/util/concurrent/Semaphore;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_0

    .line 1278
    :pswitch_12
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/car/ExceptionParcel;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ExceptionParcel;->a()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ab;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 1281
    :pswitch_13
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->O()V

    .line 1282
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->h:Ljava/util/concurrent/Semaphore;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_0

    .line 1285
    :pswitch_14
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->N()V

    .line 1286
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->h:Ljava/util/concurrent/Semaphore;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_0

    .line 1290
    :pswitch_15
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/util/Pair;

    .line 1292
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/gms/car/b/g;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Landroid/view/inputmethod/EditorInfo;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/car/y;->a(Lcom/google/android/gms/car/b/g;Landroid/view/inputmethod/EditorInfo;)V

    .line 1294
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->h:Ljava/util/concurrent/Semaphore;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_0

    .line 1297
    :pswitch_16
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->Q()V

    .line 1298
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->h:Ljava/util/concurrent/Semaphore;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_0

    .line 1304
    :pswitch_17
    :try_start_8
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->R()Z
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_2

    .line 1308
    :goto_9
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->h:Ljava/util/concurrent/Semaphore;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    .line 1309
    if-eqz v2, :cond_0

    .line 1310
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "component: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/car/ab;->i:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "; Exception calling stop ime"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1312
    invoke-direct {p0, v2}, Lcom/google/android/gms/car/ab;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 1316
    :pswitch_18
    iget v1, p1, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_7

    :goto_a
    invoke-static {v0, v3}, Lcom/google/android/gms/car/y;->a(Lcom/google/android/gms/car/y;Z)V

    .line 1317
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->h:Ljava/util/concurrent/Semaphore;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    goto/16 :goto_0

    :cond_7
    move v3, v4

    .line 1316
    goto :goto_a

    .line 1321
    :pswitch_19
    :try_start_9
    invoke-static {v0}, Lcom/google/android/gms/car/y;->b(Lcom/google/android/gms/car/y;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_1

    goto/16 :goto_0

    .line 1327
    :catch_1
    move-exception v0

    goto/16 :goto_0

    .line 1305
    :catch_2
    move-exception v2

    goto :goto_9

    .line 1253
    :catch_3
    move-exception v2

    goto/16 :goto_8

    .line 1238
    :catch_4
    move-exception v2

    goto/16 :goto_7

    .line 1223
    :catch_5
    move-exception v2

    goto/16 :goto_6

    .line 1208
    :catch_6
    move-exception v2

    goto/16 :goto_5

    .line 1191
    :catch_7
    move-exception v2

    goto/16 :goto_4

    .line 1177
    :catch_8
    move-exception v2

    goto/16 :goto_3

    .line 1162
    :catch_9
    move-exception v2

    goto/16 :goto_2

    .line 1142
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_c
        :pswitch_d
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 1380
    const/4 v0, 0x3

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1381
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/ab;->sendMessage(Landroid/os/Message;)Z

    .line 1382
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 1385
    const/4 v0, 0x4

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1386
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ab;->a(Landroid/os/Message;)V

    .line 1387
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    .line 1388
    return-void
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 1391
    const/4 v0, 0x5

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1392
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/ab;->sendMessage(Landroid/os/Message;)Z

    .line 1393
    return-void
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 1396
    const/4 v0, 0x6

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1397
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ab;->a(Landroid/os/Message;)V

    .line 1398
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    .line 1399
    return-void
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 1402
    const/4 v0, 0x7

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1403
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/ab;->sendMessage(Landroid/os/Message;)Z

    .line 1404
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 1415
    const/16 v0, 0x9

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1417
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/ab;->sendMessage(Landroid/os/Message;)Z

    .line 1418
    return-void
.end method

.method public final o()V
    .locals 1

    .prologue
    .line 1421
    const/16 v0, 0xa

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1422
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ab;->a(Landroid/os/Message;)V

    .line 1423
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    .line 1424
    return-void
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 1427
    const/16 v0, 0xb

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1428
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/ab;->sendMessage(Landroid/os/Message;)Z

    .line 1429
    return-void
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 1438
    const/16 v0, 0xd

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1439
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/ab;->sendMessage(Landroid/os/Message;)Z

    .line 1440
    return-void
.end method

.method public final r()V
    .locals 1

    .prologue
    .line 1443
    const/16 v0, 0xe

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1444
    invoke-virtual {p0}, Lcom/google/android/gms/car/ab;->b()V

    .line 1445
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ab;->a(Landroid/os/Message;)V

    .line 1446
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    .line 1447
    return-void
.end method

.method public final s()V
    .locals 1

    .prologue
    .line 1450
    const/16 v0, 0xf

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1451
    invoke-virtual {p0}, Lcom/google/android/gms/car/ab;->b()V

    .line 1452
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ab;->a(Landroid/os/Message;)V

    .line 1453
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    .line 1454
    return-void
.end method

.method public final t()V
    .locals 1

    .prologue
    .line 1466
    const/16 v0, 0x11

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1467
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ab;->a(Landroid/os/Message;)V

    .line 1468
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    .line 1469
    return-void
.end method

.method public final u()V
    .locals 1

    .prologue
    .line 1472
    const/16 v0, 0x12

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1473
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ab;->a(Landroid/os/Message;)V

    .line 1474
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    .line 1475
    return-void
.end method

.method public final v()V
    .locals 1

    .prologue
    .line 1485
    const/16 v0, 0x14

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1486
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/ab;->sendMessage(Landroid/os/Message;)Z

    .line 1487
    return-void
.end method

.method public final w()V
    .locals 1

    .prologue
    .line 1490
    iget-object v0, p0, Lcom/google/android/gms/car/ab;->e:Lcom/google/android/gms/car/ad;

    invoke-static {p0, v0}, Lcom/google/android/gms/car/qh;->b(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1491
    return-void
.end method

.method public final x()V
    .locals 1

    .prologue
    .line 1508
    const/16 v0, 0x19

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1509
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/ab;->sendMessage(Landroid/os/Message;)Z

    .line 1510
    return-void
.end method

.method public final y()V
    .locals 1

    .prologue
    .line 1586
    const/16 v0, 0x16

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1587
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ab;->a(Landroid/os/Message;)V

    .line 1588
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    .line 1589
    return-void
.end method

.method public final z()V
    .locals 1

    .prologue
    .line 1592
    const/16 v0, 0x17

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1593
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ab;->a(Landroid/os/Message;)V

    .line 1594
    invoke-direct {p0}, Lcom/google/android/gms/car/ab;->C()V

    .line 1595
    return-void
.end method
