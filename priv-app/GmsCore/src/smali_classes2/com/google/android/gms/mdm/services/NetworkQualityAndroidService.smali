.class public final Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;
.super Landroid/app/Service;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/es;
.implements Lcom/google/android/gms/common/et;


# instance fields
.field private a:Lcom/google/android/gms/location/reporting/i;

.field private b:Z

.field private c:Z

.field private d:Landroid/accounts/AccountManager;

.field private e:Lcom/google/android/gms/common/api/v;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 98
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;Z)Z
    .locals 0

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->b:Z

    return p1
.end method

.method private c()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 141
    iget-boolean v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->b:Z

    if-nez v0, :cond_1

    .line 167
    :cond_0
    :goto_0
    return v1

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->d:Landroid/accounts/AccountManager;

    const-string v2, "com.google"

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 149
    array-length v0, v2

    new-array v3, v0, [Lcom/google/android/gms/location/reporting/ReportingState;

    move v0, v1

    .line 151
    :goto_1
    array-length v4, v2

    if-ge v0, v4, :cond_2

    .line 153
    :try_start_0
    iget-object v4, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->a:Lcom/google/android/gms/location/reporting/i;

    aget-object v5, v2, v0

    invoke-virtual {v4, v5}, Lcom/google/android/gms/location/reporting/i;->a(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;

    move-result-object v4

    aput-object v4, v3, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 161
    :catch_0
    move-exception v0

    const-string v0, "Herrevad"

    const-string v2, "ULR reporting client disconnected between connection check and use - won\'t upload location"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 167
    :cond_2
    array-length v0, v3

    if-eqz v0, :cond_0

    array-length v2, v3

    move v0, v1

    :goto_2
    if-ge v0, v2, :cond_3

    aget-object v4, v3, v0

    invoke-virtual {v4}, Lcom/google/android/gms/location/reporting/ReportingState;->f()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Lcom/google/android/gms/location/reporting/ReportingState;->d()Z

    move-result v4

    if-eqz v4, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    const/4 v1, 0x1

    goto :goto_0

    .line 155
    :catch_1
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final T_()V
    .locals 1

    .prologue
    .line 197
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->c:Z

    .line 198
    return-void
.end method

.method public final W_()V
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->c:Z

    .line 204
    return-void
.end method

.method public final a()Landroid/location/Location;
    .locals 2

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    sget-object v0, Lcom/google/android/gms/location/p;->b:Lcom/google/android/gms/location/d;

    iget-object v1, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->e:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1}, Lcom/google/android/gms/location/d;->a(Lcom/google/android/gms/common/api/v;)Landroid/location/Location;

    move-result-object v0

    .line 127
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->c:Z

    .line 209
    return-void
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 91
    const-string v0, "com.google.android.gms.mdm.services.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    new-instance v0, Lcom/google/android/gms/mdm/services/f;

    invoke-direct {v0, p0, p0}, Lcom/google/android/gms/mdm/services/f;-><init>(Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/gms/mdm/services/f;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 94
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreate()V
    .locals 2

    .prologue
    .line 53
    const-string v0, "account"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountManager;

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->d:Landroid/accounts/AccountManager;

    .line 57
    new-instance v0, Lcom/google/android/gms/location/reporting/i;

    invoke-direct {v0, p0, p0, p0}, Lcom/google/android/gms/location/reporting/i;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;)V

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->a:Lcom/google/android/gms/location/reporting/i;

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->a:Lcom/google/android/gms/location/reporting/i;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/i;->a()V

    .line 60
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/location/p;->a:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/mdm/services/e;

    invoke-direct {v1, p0}, Lcom/google/android/gms/mdm/services/e;-><init>(Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/x;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/mdm/services/d;

    invoke-direct {v1, p0}, Lcom/google/android/gms/mdm/services/d;-><init>(Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->e:Lcom/google/android/gms/common/api/v;

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->e:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 81
    return-void
.end method

.method public final onDestroy()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->a:Lcom/google/android/gms/location/reporting/i;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/i;->b()V

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->e:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 87
    return-void
.end method
