.class public final Lcom/google/android/gms/games/h/e/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/server/response/s;


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    sget v0, Lcom/google/android/gms/g;->M:I

    sput v0, Lcom/google/android/gms/games/h/e/f;->a:I

    .line 21
    sget v0, Lcom/google/android/gms/g;->K:I

    sput v0, Lcom/google/android/gms/games/h/e/f;->b:I

    .line 22
    sget v0, Lcom/google/android/gms/g;->J:I

    sput v0, Lcom/google/android/gms/games/h/e/f;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/server/response/FastJsonResponse;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 6

    .prologue
    .line 19
    check-cast p1, Lcom/google/android/gms/games/h/a/cs;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    const-string v2, "profile_icon_image_url"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    new-instance v3, Lcom/google/android/gms/common/internal/bs;

    invoke-direct {v3, v2}, Lcom/google/android/gms/common/internal/bs;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/gms/games/h/e/f;->a:I

    invoke-virtual {v3, v0, v2}, Lcom/google/android/gms/common/internal/bs;->a(Landroid/content/Context;I)Lcom/google/android/gms/common/internal/br;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/internal/br;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "profile_hi_res_image_url"

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/cs;->getExperienceInfo()Lcom/google/android/gms/games/h/a/cy;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/cy;->c()Ljava/lang/Long;

    move-result-object v3

    if-eqz v3, :cond_0

    const-string v4, "last_level_up_timestamp"

    invoke-virtual {v1, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_0
    const-string v3, "current_xp_total"

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/cy;->b()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/cy;->getCurrentLevel()Lcom/google/android/gms/games/h/a/db;

    move-result-object v3

    const-string v4, "current_level"

    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/db;->b()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "current_level_min_xp"

    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/db;->d()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "current_level_max_xp"

    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/db;->c()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/cy;->getNextLevel()Lcom/google/android/gms/games/h/a/db;

    move-result-object v2

    const-string v3, "next_level"

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/db;->b()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "next_level_max_xp"

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/db;->c()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/cs;->getLastPlayedApp()Lcom/google/android/gms/games/h/a/cr;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, v2, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    const-string v2, "most_recent_game_icon_url"

    sget v3, Lcom/google/android/gms/games/h/e/f;->b:I

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/games/h/e/g;->a(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;I)V

    const-string v2, "most_recent_game_icon_url"

    const-string v3, "most_recent_game_hi_res_url"

    sget v4, Lcom/google/android/gms/games/h/e/f;->c:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/games/h/e/g;->b(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v2, "most_recent_game_featured_url"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-static {v0, v3}, Lcom/google/android/gms/games/h/e/g;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/cs;->getProfileSettings()Lcom/google/android/gms/games/h/a/dk;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v2, "is_profile_visible"

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/dk;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    :cond_3
    const-string v0, "last_updated"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    return-object p1

    :cond_4
    const-string v2, "profile_hi_res_image_url"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
