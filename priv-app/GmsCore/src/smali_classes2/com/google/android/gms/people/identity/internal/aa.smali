.class final Lcom/google/android/gms/people/identity/internal/aa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/data/d;


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/api/l;

.field final synthetic b:Lcom/google/android/gms/people/identity/internal/w;


# direct methods
.method constructor <init>(Lcom/google/android/gms/people/identity/internal/w;Lcom/google/android/gms/common/api/l;)V
    .locals 0

    .prologue
    .line 282
    iput-object p1, p0, Lcom/google/android/gms/people/identity/internal/aa;->b:Lcom/google/android/gms/people/identity/internal/w;

    iput-object p2, p0, Lcom/google/android/gms/people/identity/internal/aa;->a:Lcom/google/android/gms/common/api/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 290
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/aa;->b:Lcom/google/android/gms/people/identity/internal/w;

    iget-object v0, v0, Lcom/google/android/gms/people/identity/internal/w;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    move-object v10, v1

    .line 293
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/aa;->b:Lcom/google/android/gms/people/identity/internal/w;

    iget-object v0, v0, Lcom/google/android/gms/people/identity/internal/w;->p:Ljava/util/List;

    if-nez v0, :cond_1

    move-object v11, v1

    .line 296
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/aa;->b:Lcom/google/android/gms/people/identity/internal/w;

    iget-object v0, v0, Lcom/google/android/gms/people/identity/internal/w;->g:Lcom/google/android/gms/common/data/DataHolder;

    if-nez v0, :cond_2

    move-object v5, v1

    .line 301
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/aa;->b:Lcom/google/android/gms/people/identity/internal/w;

    iget-object v0, v0, Lcom/google/android/gms/people/identity/internal/w;->a:Lcom/google/android/gms/people/identity/g;

    iget-object v1, p0, Lcom/google/android/gms/people/identity/internal/aa;->b:Lcom/google/android/gms/people/identity/internal/w;

    iget-object v1, v1, Lcom/google/android/gms/people/identity/internal/w;->e:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/people/identity/internal/aa;->b:Lcom/google/android/gms/people/identity/internal/w;

    iget-object v2, v2, Lcom/google/android/gms/people/identity/internal/w;->b:[Ljava/lang/Object;

    aget-object v2, v2, p1

    move-object v3, v10

    move-object v4, v11

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/people/identity/g;->a(Landroid/content/Context;Ljava/lang/Object;Lcom/google/android/gms/people/identity/s;Lcom/google/android/gms/people/identity/h;Lcom/google/android/gms/people/identity/k;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/aa;->b:Lcom/google/android/gms/people/identity/internal/w;

    iget-object v0, v0, Lcom/google/android/gms/people/identity/internal/w;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    invoke-static {v0}, Lcom/google/android/gms/people/identity/s;->a(Landroid/os/Bundle;)Lcom/google/android/gms/people/identity/s;

    move-result-object v0

    move-object v10, v0

    goto :goto_0

    .line 293
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/aa;->b:Lcom/google/android/gms/people/identity/internal/w;

    iget-object v0, v0, Lcom/google/android/gms/people/identity/internal/w;->p:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/h;

    move-object v11, v0

    goto :goto_1

    .line 296
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/aa;->b:Lcom/google/android/gms/people/identity/internal/w;

    iget-object v0, v0, Lcom/google/android/gms/people/identity/internal/w;->g:Lcom/google/android/gms/common/data/DataHolder;

    iget-object v1, p0, Lcom/google/android/gms/people/identity/internal/aa;->b:Lcom/google/android/gms/people/identity/internal/w;

    iget-object v1, v1, Lcom/google/android/gms/people/identity/internal/w;->h:Lcom/google/android/gms/common/data/DataHolder;

    iget-object v2, p0, Lcom/google/android/gms/people/identity/internal/aa;->b:Lcom/google/android/gms/people/identity/internal/w;

    iget-object v2, v2, Lcom/google/android/gms/people/identity/internal/w;->i:Lcom/google/android/gms/common/data/DataHolder;

    iget-object v3, p0, Lcom/google/android/gms/people/identity/internal/aa;->b:Lcom/google/android/gms/people/identity/internal/w;

    iget-object v3, v3, Lcom/google/android/gms/people/identity/internal/w;->j:Lcom/google/android/gms/common/data/DataHolder;

    iget-object v4, p0, Lcom/google/android/gms/people/identity/internal/aa;->b:Lcom/google/android/gms/people/identity/internal/w;

    iget-object v4, v4, Lcom/google/android/gms/people/identity/internal/w;->k:Lcom/google/android/gms/common/data/DataHolder;

    iget-object v5, p0, Lcom/google/android/gms/people/identity/internal/aa;->b:Lcom/google/android/gms/people/identity/internal/w;

    iget-object v5, v5, Lcom/google/android/gms/people/identity/internal/w;->l:Lcom/google/android/gms/common/data/DataHolder;

    iget-object v6, p0, Lcom/google/android/gms/people/identity/internal/aa;->b:Lcom/google/android/gms/people/identity/internal/w;

    iget-object v6, v6, Lcom/google/android/gms/people/identity/internal/w;->m:Lcom/google/android/gms/common/data/DataHolder;

    iget-object v7, p0, Lcom/google/android/gms/people/identity/internal/aa;->b:Lcom/google/android/gms/people/identity/internal/w;

    iget-object v7, v7, Lcom/google/android/gms/people/identity/internal/w;->n:Lcom/google/android/gms/common/data/DataHolder;

    iget-object v8, p0, Lcom/google/android/gms/people/identity/internal/aa;->b:Lcom/google/android/gms/people/identity/internal/w;

    iget-object v8, v8, Lcom/google/android/gms/people/identity/internal/w;->o:Lcom/google/android/gms/common/data/DataHolder;

    move v9, p1

    invoke-static/range {v0 .. v9}, Lcom/google/android/gms/people/identity/k;->a(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;I)Lcom/google/android/gms/people/identity/k;

    move-result-object v5

    goto :goto_2
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/aa;->b:Lcom/google/android/gms/people/identity/internal/w;

    iget-object v0, v0, Lcom/google/android/gms/people/identity/internal/w;->b:[Ljava/lang/Object;

    array-length v0, v0

    return v0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 316
    invoke-virtual {p0}, Lcom/google/android/gms/people/identity/internal/aa;->w_()V

    .line 317
    return-void
.end method

.method public final e()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 311
    const/4 v0, 0x0

    return-object v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 326
    new-instance v0, Lcom/google/android/gms/common/data/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/data/e;-><init>(Lcom/google/android/gms/common/data/d;)V

    return-object v0
.end method

.method public final w_()V
    .locals 2

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/aa;->a:Lcom/google/android/gms/common/api/l;

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/aa;->a:Lcom/google/android/gms/common/api/l;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/l;->b()V

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/aa;->b:Lcom/google/android/gms/people/identity/internal/w;

    iget-object v0, v0, Lcom/google/android/gms/people/identity/internal/w;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    .line 349
    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    .line 351
    :cond_1
    return-void
.end method
