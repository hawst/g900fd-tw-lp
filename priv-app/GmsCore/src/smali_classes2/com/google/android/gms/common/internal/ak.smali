.class final Lcom/google/android/gms/common/internal/ak;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/internal/aj;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/internal/aj;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/android/gms/common/internal/ak;->a:Lcom/google/android/gms/common/internal/aj;

    .line 98
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 99
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x4

    const/4 v2, 0x1

    .line 106
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/internal/ak;->a:Lcom/google/android/gms/common/internal/aj;

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/aj;->n_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/common/internal/al;

    .line 108
    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/al;->a()V

    .line 109
    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/al;->c()V

    .line 145
    :goto_0
    return-void

    .line 113
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 114
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ak;->a:Lcom/google/android/gms/common/internal/aj;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/aj;->a(Lcom/google/android/gms/common/internal/aj;)Lcom/google/android/gms/common/internal/ar;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/common/c;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v2, v0, v5}, Lcom/google/android/gms/common/c;-><init>(ILandroid/app/PendingIntent;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/internal/ar;->a(Lcom/google/android/gms/common/c;)V

    goto :goto_0

    .line 118
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v3, :cond_2

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ak;->a:Lcom/google/android/gms/common/internal/aj;

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/aj;->a(Lcom/google/android/gms/common/internal/aj;I)V

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ak;->a:Lcom/google/android/gms/common/internal/aj;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/aj;->a(Lcom/google/android/gms/common/internal/aj;)Lcom/google/android/gms/common/internal/ar;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/internal/ar;->a(I)V

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ak;->a:Lcom/google/android/gms/common/internal/aj;

    invoke-static {v0, v3, v2, v5}, Lcom/google/android/gms/common/internal/aj;->a(Lcom/google/android/gms/common/internal/aj;IILandroid/os/IInterface;)Z

    goto :goto_0

    .line 129
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v4, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/common/internal/ak;->a:Lcom/google/android/gms/common/internal/aj;

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/aj;->c_()Z

    move-result v0

    if-nez v0, :cond_3

    .line 130
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/common/internal/al;

    .line 131
    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/al;->a()V

    .line 132
    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/al;->c()V

    goto :goto_0

    .line 137
    :cond_3
    iget v0, p1, Landroid/os/Message;->what:I

    if-eq v0, v4, :cond_4

    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v2, :cond_5

    .line 138
    :cond_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/common/internal/al;

    .line 139
    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/al;->b()V

    goto :goto_0

    .line 144
    :cond_5
    const-string v0, "GmsClient"

    const-string v1, "Don\'t know how to handle this message."

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
