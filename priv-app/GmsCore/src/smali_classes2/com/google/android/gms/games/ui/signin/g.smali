.class public abstract Lcom/google/android/gms/games/ui/signin/g;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method protected static c(I)I
    .locals 1

    .prologue
    .line 262
    sparse-switch p0, :sswitch_data_0

    .line 271
    const/16 v0, 0x2712

    :goto_0
    return v0

    .line 264
    :sswitch_0
    const/16 v0, 0x4e20

    goto :goto_0

    .line 267
    :sswitch_1
    const/16 v0, 0x2716

    goto :goto_0

    .line 269
    :sswitch_2
    const/16 v0, 0x2714

    goto :goto_0

    .line 262
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x4 -> :sswitch_1
        0x9 -> :sswitch_2
        0x3eb -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public abstract a()I
.end method

.method protected final a(II)V
    .locals 2

    .prologue
    .line 293
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/g;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-static {p2}, Lcom/google/android/gms/games/o;->a(I)Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(ILcom/google/android/gms/common/api/Status;)V

    .line 296
    :cond_0
    return-void
.end method

.method protected abstract a(Lcom/google/android/gms/games/i/b;)V
.end method

.method protected final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/g;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Ljava/lang/String;)V

    .line 141
    :cond_0
    return-void
.end method

.method public abstract b()I
.end method

.method protected final b(I)V
    .locals 1

    .prologue
    .line 228
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/g;->a()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 229
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/g;->b:Z

    .line 231
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/g;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 232
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b(I)V

    .line 234
    :cond_1
    return-void
.end method

.method public final b(II)V
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(II)V

    .line 329
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/i/b;)V
    .locals 2

    .prologue
    .line 344
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/g;->b:Z

    if-nez v0, :cond_0

    .line 345
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/g;->b:Z

    .line 346
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/signin/g;->a(Lcom/google/android/gms/games/i/b;)V

    .line 350
    :goto_0
    return-void

    .line 348
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Attempting to transition multiple times."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final d(I)V
    .locals 2

    .prologue
    .line 281
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/g;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(ILcom/google/android/gms/common/api/Status;)V

    .line 284
    :cond_0
    return-void
.end method

.method public final e(I)V
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->c(I)V

    .line 312
    :cond_0
    return-void
.end method

.method protected final g()Z
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/g;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(I)Z

    move-result v0

    return v0
.end method

.method protected final h()V
    .locals 1

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/g;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->n()V

    .line 210
    :cond_0
    return-void
.end method

.method final i()Z
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/g;->isDetached()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/g;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/g;->c:Z

    .line 78
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 37
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 38
    if-eqz p1, :cond_0

    .line 39
    const-string v0, "activity_launched"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/g;->c:Z

    .line 41
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 46
    const-string v0, "activity_launched"

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/signin/g;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 47
    return-void
.end method

.method public final onStart()V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 52
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/signin/SignInActivity;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    .line 53
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/g;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/g;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->c(I)V

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->d()Lcom/google/android/gms/games/i/b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/g;->b(Lcom/google/android/gms/games/i/b;)V

    .line 57
    :cond_0
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 61
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not supported for sign-in fragments; use startActivityForResult."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/g;->c:Z

    if-nez v0, :cond_0

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/signin/g;->c:Z

    .line 69
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 73
    :goto_0
    return-void

    .line 71
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Attempting to launch more than one activity."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
