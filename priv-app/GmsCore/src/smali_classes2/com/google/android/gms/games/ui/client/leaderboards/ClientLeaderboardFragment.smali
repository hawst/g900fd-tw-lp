.class public final Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardFragment;
.super Lcom/google/android/gms/games/ui/common/leaderboards/g;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/g;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/games/e/a;)V
    .locals 5

    .prologue
    .line 25
    invoke-interface {p1}, Lcom/google/android/gms/games/e/a;->a()Ljava/lang/String;

    move-result-object v0

    .line 26
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardFragment;->b()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    .line 27
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-static {v1, v2}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 28
    const-string v0, "ClientLeaderboard"

    const-string v1, "onClick: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    :goto_0
    return-void

    .line 31
    :cond_0
    sget-object v2, Lcom/google/android/gms/games/d;->j:Lcom/google/android/gms/games/e/n;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/q;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v1, v0, v3}, Lcom/google/android/gms/games/e/n;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 34
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardFragment;->d:Lcom/google/android/gms/games/ui/q;

    const/16 v2, 0x384

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/games/ui/q;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method
