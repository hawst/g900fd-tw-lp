.class public final Lcom/google/android/gms/icing/t;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:I

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8487
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 8488
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/t;->a:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/icing/t;->b:I

    iput v1, p0, Lcom/google/android/gms/icing/t;->c:I

    iput v1, p0, Lcom/google/android/gms/icing/t;->d:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/t;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/t;->f:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/t;->g:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/t;->cachedSize:I

    .line 8489
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 8598
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 8599
    iget-object v1, p0, Lcom/google/android/gms/icing/t;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 8600
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/t;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8603
    :cond_0
    iget v1, p0, Lcom/google/android/gms/icing/t;->b:I

    if-eqz v1, :cond_1

    .line 8604
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/icing/t;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8607
    :cond_1
    iget v1, p0, Lcom/google/android/gms/icing/t;->c:I

    if-eqz v1, :cond_2

    .line 8608
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/icing/t;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8611
    :cond_2
    iget v1, p0, Lcom/google/android/gms/icing/t;->d:I

    if-eqz v1, :cond_3

    .line 8612
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/icing/t;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8615
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/t;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 8616
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/icing/t;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8619
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/t;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 8620
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/icing/t;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8623
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/icing/t;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 8624
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/icing/t;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8627
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8505
    if-ne p1, p0, :cond_1

    .line 8549
    :cond_0
    :goto_0
    return v0

    .line 8508
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/t;

    if-nez v2, :cond_2

    move v0, v1

    .line 8509
    goto :goto_0

    .line 8511
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/t;

    .line 8512
    iget-object v2, p0, Lcom/google/android/gms/icing/t;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 8513
    iget-object v2, p1, Lcom/google/android/gms/icing/t;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 8514
    goto :goto_0

    .line 8516
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/t;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/t;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 8517
    goto :goto_0

    .line 8519
    :cond_4
    iget v2, p0, Lcom/google/android/gms/icing/t;->b:I

    iget v3, p1, Lcom/google/android/gms/icing/t;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 8520
    goto :goto_0

    .line 8522
    :cond_5
    iget v2, p0, Lcom/google/android/gms/icing/t;->c:I

    iget v3, p1, Lcom/google/android/gms/icing/t;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 8523
    goto :goto_0

    .line 8525
    :cond_6
    iget v2, p0, Lcom/google/android/gms/icing/t;->d:I

    iget v3, p1, Lcom/google/android/gms/icing/t;->d:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 8526
    goto :goto_0

    .line 8528
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/icing/t;->e:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 8529
    iget-object v2, p1, Lcom/google/android/gms/icing/t;->e:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    .line 8530
    goto :goto_0

    .line 8532
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/icing/t;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/t;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 8533
    goto :goto_0

    .line 8535
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/icing/t;->f:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 8536
    iget-object v2, p1, Lcom/google/android/gms/icing/t;->f:Ljava/lang/String;

    if-eqz v2, :cond_b

    move v0, v1

    .line 8537
    goto :goto_0

    .line 8539
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/icing/t;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/t;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 8540
    goto :goto_0

    .line 8542
    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/icing/t;->g:Ljava/lang/String;

    if-nez v2, :cond_c

    .line 8543
    iget-object v2, p1, Lcom/google/android/gms/icing/t;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 8544
    goto :goto_0

    .line 8546
    :cond_c
    iget-object v2, p0, Lcom/google/android/gms/icing/t;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/t;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 8547
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 8554
    iget-object v0, p0, Lcom/google/android/gms/icing/t;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 8557
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/icing/t;->b:I

    add-int/2addr v0, v2

    .line 8558
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/icing/t;->c:I

    add-int/2addr v0, v2

    .line 8559
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/icing/t;->d:I

    add-int/2addr v0, v2

    .line 8560
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/t;->e:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 8562
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/t;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 8564
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/t;->g:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 8566
    return v0

    .line 8554
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/t;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 8560
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/t;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 8562
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/t;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 8564
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/t;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8449
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/t;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/t;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/t;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/t;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/t;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/t;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/t;->g:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 8572
    iget-object v0, p0, Lcom/google/android/gms/icing/t;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8573
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/t;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 8575
    :cond_0
    iget v0, p0, Lcom/google/android/gms/icing/t;->b:I

    if-eqz v0, :cond_1

    .line 8576
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/icing/t;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8578
    :cond_1
    iget v0, p0, Lcom/google/android/gms/icing/t;->c:I

    if-eqz v0, :cond_2

    .line 8579
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/icing/t;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8581
    :cond_2
    iget v0, p0, Lcom/google/android/gms/icing/t;->d:I

    if-eqz v0, :cond_3

    .line 8582
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/icing/t;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8584
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/t;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 8585
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/icing/t;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 8587
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/icing/t;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 8588
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/gms/icing/t;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 8590
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/t;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 8591
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/gms/icing/t;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 8593
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 8594
    return-void
.end method
