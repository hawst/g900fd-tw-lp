.class public final Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/String;

.field final d:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/gms/wallet/shared/h;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/shared/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput p1, p0, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->a:I

    .line 42
    iput-object p2, p0, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->b:Ljava/lang/String;

    .line 43
    iput-object p3, p0, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->c:Ljava/lang/String;

    .line 44
    if-eqz p4, :cond_0

    :goto_0
    iput-object p4, p0, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->d:[Ljava/lang/String;

    .line 45
    return-void

    .line 44
    :cond_0
    const/4 v0, 0x0

    new-array p4, v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;-><init>(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 50
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->d:[Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 77
    invoke-static {p0, p1}, Lcom/google/android/gms/wallet/shared/h;->a(Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;Landroid/os/Parcel;)V

    .line 78
    return-void
.end method
