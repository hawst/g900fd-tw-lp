.class final Lcom/google/android/gms/car/or;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/on;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/car/on;)V
    .locals 0

    .prologue
    .line 1677
    iput-object p1, p0, Lcom/google/android/gms/car/or;->a:Lcom/google/android/gms/car/on;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/car/on;B)V
    .locals 0

    .prologue
    .line 1677
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/or;-><init>(Lcom/google/android/gms/car/on;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 1681
    const/4 v0, -0x8

    :try_start_0
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1683
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/or;->a:Lcom/google/android/gms/car/on;

    invoke-static {v0}, Lcom/google/android/gms/car/on;->b(Lcom/google/android/gms/car/on;)V
    :try_end_1
    .catch Lcom/google/android/gms/car/ow; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1690
    :try_start_2
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 1691
    iget-object v0, p0, Lcom/google/android/gms/car/or;->a:Lcom/google/android/gms/car/on;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/car/on;->a(Lcom/google/android/gms/car/on;Landroid/os/Looper;)Landroid/os/Looper;

    .line 1692
    iget-object v0, p0, Lcom/google/android/gms/car/or;->a:Lcom/google/android/gms/car/on;

    new-instance v1, Lcom/google/android/gms/car/oq;

    iget-object v2, p0, Lcom/google/android/gms/car/or;->a:Lcom/google/android/gms/car/on;

    iget-object v3, p0, Lcom/google/android/gms/car/or;->a:Lcom/google/android/gms/car/on;

    invoke-static {v3}, Lcom/google/android/gms/car/on;->e(Lcom/google/android/gms/car/on;)Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/car/oq;-><init>(Lcom/google/android/gms/car/on;Landroid/os/Looper;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/car/on;->a(Lcom/google/android/gms/car/on;Lcom/google/android/gms/car/oq;)Lcom/google/android/gms/car/oq;

    .line 1694
    iget-object v0, p0, Lcom/google/android/gms/car/or;->a:Lcom/google/android/gms/car/on;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/car/on;->a(Lcom/google/android/gms/car/on;Z)Z

    .line 1695
    iget-object v0, p0, Lcom/google/android/gms/car/or;->a:Lcom/google/android/gms/car/on;

    invoke-static {v0}, Lcom/google/android/gms/car/on;->d(Lcom/google/android/gms/car/on;)Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1696
    invoke-static {}, Landroid/os/Looper;->loop()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1702
    const-string v0, "CAR.WM"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1703
    const-string v0, "CAR.WM"

    const-string v1, "CompositionThread exiting"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1705
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/or;->a:Lcom/google/android/gms/car/on;

    invoke-static {v0}, Lcom/google/android/gms/car/on;->c(Lcom/google/android/gms/car/on;)V

    .line 1706
    :goto_0
    return-void

    .line 1685
    :catch_0
    move-exception v0

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/car/or;->a:Lcom/google/android/gms/car/on;

    invoke-static {v0}, Lcom/google/android/gms/car/on;->c(Lcom/google/android/gms/car/on;)V

    .line 1686
    iget-object v0, p0, Lcom/google/android/gms/car/or;->a:Lcom/google/android/gms/car/on;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/car/on;->a(Lcom/google/android/gms/car/on;Z)Z

    .line 1687
    iget-object v0, p0, Lcom/google/android/gms/car/or;->a:Lcom/google/android/gms/car/on;

    invoke-static {v0}, Lcom/google/android/gms/car/on;->d(Lcom/google/android/gms/car/on;)Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1702
    const-string v0, "CAR.WM"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1703
    const-string v0, "CAR.WM"

    const-string v1, "CompositionThread exiting"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1705
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/or;->a:Lcom/google/android/gms/car/on;

    invoke-static {v0}, Lcom/google/android/gms/car/on;->c(Lcom/google/android/gms/car/on;)V

    goto :goto_0

    .line 1697
    :catch_1
    move-exception v0

    .line 1698
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1699
    iget-object v1, p0, Lcom/google/android/gms/car/or;->a:Lcom/google/android/gms/car/on;

    invoke-static {v1}, Lcom/google/android/gms/car/on;->f(Lcom/google/android/gms/car/on;)Lcom/google/android/gms/car/gx;

    move-result-object v1

    const/16 v2, 0xb

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "OpenGl error:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/car/gx;->a(ILjava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1702
    const-string v0, "CAR.WM"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1703
    const-string v0, "CAR.WM"

    const-string v1, "CompositionThread exiting"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1705
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/or;->a:Lcom/google/android/gms/car/on;

    invoke-static {v0}, Lcom/google/android/gms/car/on;->c(Lcom/google/android/gms/car/on;)V

    goto :goto_0

    .line 1702
    :catchall_0
    move-exception v0

    const-string v1, "CAR.WM"

    invoke-static {v1, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1703
    const-string v1, "CAR.WM"

    const-string v2, "CompositionThread exiting"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1705
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/car/or;->a:Lcom/google/android/gms/car/on;

    invoke-static {v1}, Lcom/google/android/gms/car/on;->c(Lcom/google/android/gms/car/on;)V

    throw v0
.end method
