.class final Lcom/google/android/gms/games/internal/bo;
.super Lcom/google/android/gms/games/internal/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/aj;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/aj;)V
    .locals 0

    .prologue
    .line 860
    invoke-direct {p0}, Lcom/google/android/gms/games/internal/a;-><init>()V

    .line 861
    iput-object p1, p0, Lcom/google/android/gms/games/internal/bo;->a:Lcom/google/android/gms/common/api/aj;

    .line 862
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 885
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bo;->a:Lcom/google/android/gms/common/api/aj;

    new-instance v1, Lcom/google/android/gms/games/internal/bq;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bq;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 886
    return-void
.end method

.method public final m(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 3

    .prologue
    .line 867
    new-instance v1, Lcom/google/android/gms/games/request/a;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/request/a;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 868
    const/4 v0, 0x0

    .line 870
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/request/a;->c()I

    move-result v2

    if-lez v2, :cond_0

    .line 871
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/request/a;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 874
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/request/a;->w_()V

    .line 878
    if-eqz v0, :cond_1

    .line 879
    iget-object v1, p0, Lcom/google/android/gms/games/internal/bo;->a:Lcom/google/android/gms/common/api/aj;

    new-instance v2, Lcom/google/android/gms/games/internal/bp;

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/internal/bp;-><init>(Lcom/google/android/gms/games/request/GameRequest;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 881
    :cond_1
    return-void

    .line 874
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/request/a;->w_()V

    throw v0
.end method
