.class public final Lcom/google/android/gms/udc/util/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/analytics/bv;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 36
    invoke-static {p0}, Lcom/google/android/gms/analytics/ax;->a(Landroid/content/Context;)Lcom/google/android/gms/analytics/ax;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/udc/b/a;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/analytics/ax;->a(Ljava/lang/String;)Lcom/google/android/gms/analytics/bv;

    move-result-object v0

    .line 38
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 39
    const-string v1, "&cd"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/analytics/bv;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    :cond_0
    const-string v1, "&aip"

    invoke-static {v4}, Lcom/google/android/gms/analytics/bz;->a(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/analytics/bv;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    const-string v1, "&sf"

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    invoke-static {v2, v3}, Ljava/lang/Double;->toHexString(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/analytics/bv;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    iget-boolean v1, v0, Lcom/google/android/gms/analytics/bv;->c:Z

    if-eq v1, v4, :cond_1

    iput-boolean v4, v0, Lcom/google/android/gms/analytics/bv;->c:Z

    new-instance v1, Lcom/google/android/gms/analytics/z;

    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/analytics/bv;->a:Landroid/content/Context;

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/gms/analytics/z;-><init>(Lcom/google/android/gms/analytics/bv;Ljava/lang/Thread$UncaughtExceptionHandler;Landroid/content/Context;)V

    iput-object v1, v0, Lcom/google/android/gms/analytics/bv;->f:Lcom/google/android/gms/analytics/z;

    iget-object v1, v0, Lcom/google/android/gms/analytics/bv;->f:Lcom/google/android/gms/analytics/z;

    invoke-static {v1}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    const-string v1, "Uncaught exceptions will be reported to Google Analytics."

    invoke-static {v1}, Lcom/google/android/gms/analytics/bl;->c(Ljava/lang/String;)V

    .line 44
    :cond_1
    const-string v1, "Udc"

    const-string v2, "&an"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/analytics/bv;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    const-string v1, "6777000"

    const-string v2, "&av"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/analytics/bv;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    return-object v0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    packed-switch p0, :pswitch_data_0

    .line 164
    const-string v0, "Unknown"

    :goto_0
    return-object v0

    .line 158
    :pswitch_0
    const-string v0, "Unset"

    goto :goto_0

    .line 160
    :pswitch_1
    const-string v0, "Enable"

    goto :goto_0

    .line 162
    :pswitch_2
    const-string v0, "Disable"

    goto :goto_0

    .line 156
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a([I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 143
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    if-eqz p0, :cond_1

    .line 145
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 146
    if-lez v0, :cond_0

    .line 147
    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    :cond_0
    aget v2, p0, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 145
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 152
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/analytics/bv;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 123
    new-instance v0, Lcom/google/android/gms/analytics/bf;

    invoke-direct {v0}, Lcom/google/android/gms/analytics/bf;-><init>()V

    const-string v1, "ApiError"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/bf;->a(Ljava/lang/String;)Lcom/google/android/gms/analytics/bf;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/analytics/bf;->b(Ljava/lang/String;)Lcom/google/android/gms/analytics/bf;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/bf;->c(Ljava/lang/String;)Lcom/google/android/gms/analytics/bf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/bf;->a()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/bv;->a(Ljava/util/Map;)V

    .line 128
    return-void
.end method

.method public static a(Lcom/google/android/gms/analytics/bv;Ljava/lang/String;ILjava/lang/Integer;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 58
    new-array v4, v3, [I

    aput p2, v4, v2

    if-nez p3, :cond_1

    new-array v0, v2, [I

    move-object v1, v0

    :goto_0
    array-length v0, v1

    if-eqz v0, :cond_0

    array-length v0, v4

    array-length v5, v1

    if-ne v0, v5, :cond_2

    :cond_0
    move v0, v3

    :goto_1
    const-string v2, "Number of settingIds and number of settingValuesCurrent must be equal"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/android/gms/analytics/bf;

    invoke-direct {v0}, Lcom/google/android/gms/analytics/bf;-><init>()V

    const-string v2, "UX"

    invoke-virtual {v0, v2}, Lcom/google/android/gms/analytics/bf;->a(Ljava/lang/String;)Lcom/google/android/gms/analytics/bf;

    move-result-object v0

    const-string v2, "Click"

    invoke-virtual {v0, v2}, Lcom/google/android/gms/analytics/bf;->b(Ljava/lang/String;)Lcom/google/android/gms/analytics/bf;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/analytics/bf;->c(Ljava/lang/String;)Lcom/google/android/gms/analytics/bf;

    move-result-object v2

    array-length v0, v4

    if-nez v0, :cond_3

    const-string v0, "-"

    :goto_2
    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/analytics/bf;->a(ILjava/lang/String;)Lcom/google/android/gms/analytics/bh;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/analytics/bf;

    const/4 v2, 0x2

    array-length v3, v1

    if-nez v3, :cond_4

    const-string v1, "-"

    :goto_3
    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/analytics/bf;->a(ILjava/lang/String;)Lcom/google/android/gms/analytics/bh;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/analytics/bf;

    const/4 v1, 0x3

    const-string v2, "-"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/analytics/bf;->a(ILjava/lang/String;)Lcom/google/android/gms/analytics/bh;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/analytics/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/bf;->a()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/bv;->a(Ljava/util/Map;)V

    .line 63
    return-void

    .line 58
    :cond_1
    new-array v0, v3, [I

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aput v1, v0, v2

    move-object v1, v0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    invoke-static {v4}, Lcom/google/android/gms/udc/util/a;->a([I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_4
    invoke-static {v1}, Lcom/google/android/gms/udc/util/a;->a([I)Ljava/lang/String;

    move-result-object v1

    goto :goto_3
.end method

.method public static a(Lcom/google/android/gms/analytics/bv;Ljava/lang/String;[II)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 104
    new-instance v1, Lcom/google/android/gms/analytics/bf;

    invoke-direct {v1}, Lcom/google/android/gms/analytics/bf;-><init>()V

    const-string v2, "Consent"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/analytics/bf;->a(Ljava/lang/String;)Lcom/google/android/gms/analytics/bf;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/analytics/bf;->b(Ljava/lang/String;)Lcom/google/android/gms/analytics/bf;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/gms/udc/util/a;->a([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/analytics/bf;->c(Ljava/lang/String;)Lcom/google/android/gms/analytics/bf;

    move-result-object v1

    packed-switch p3, :pswitch_data_0

    :goto_0
    :pswitch_0
    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/analytics/bf;->a(J)Lcom/google/android/gms/analytics/bf;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {p3}, Lcom/google/android/gms/udc/util/a;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/analytics/bf;->a(ILjava/lang/String;)Lcom/google/android/gms/analytics/bh;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/analytics/bf;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/bf;->a()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/bv;->a(Ljava/util/Map;)V

    .line 112
    return-void

    .line 104
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v0, -0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
