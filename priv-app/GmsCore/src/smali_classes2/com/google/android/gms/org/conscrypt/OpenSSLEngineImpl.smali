.class public Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;
.super Ljavax/net/ssl/SSLEngine;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/org/conscrypt/NativeCrypto$SSLHandshakeCallbacks;
.implements Lcom/google/android/gms/org/conscrypt/SSLParametersImpl$AliasChooser;
.implements Lcom/google/android/gms/org/conscrypt/SSLParametersImpl$PSKCallbacks;


# static fields
.field private static nullSource:Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSource;


# instance fields
.field channelIdPrivateKey:Lcom/google/android/gms/org/conscrypt/OpenSSLKey;

.field private engineState:Lcom/google/android/gms/org/conscrypt/m;

.field private handshakeSession:Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;

.field private handshakeSink:Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;

.field private final localToRemoteSink:Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;

.field private sslNativePointer:J

.field private final sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

.field private sslSession:Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;

.field private final stateLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSource;->wrap(Ljava/nio/ByteBuffer;)Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSource;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->nullSource:Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSource;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;)V
    .locals 1

    .prologue
    .line 122
    invoke-direct {p0}, Ljavax/net/ssl/SSLEngine;-><init>()V

    .line 48
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/google/android/gms/org/conscrypt/m;->a:Lcom/google/android/gms/org/conscrypt/m;

    iput-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    .line 108
    invoke-static {}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;->create()Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->localToRemoteSink:Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;

    .line 123
    iput-object p1, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    .line 124
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILcom/google/android/gms/org/conscrypt/SSLParametersImpl;)V
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0, p1, p2}, Ljavax/net/ssl/SSLEngine;-><init>(Ljava/lang/String;I)V

    .line 48
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/google/android/gms/org/conscrypt/m;->a:Lcom/google/android/gms/org/conscrypt/m;

    iput-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    .line 108
    invoke-static {}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;->create()Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->localToRemoteSink:Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;

    .line 128
    iput-object p3, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    .line 129
    return-void
.end method

.method private static checkIndex(III)V
    .locals 2

    .prologue
    .line 376
    if-gez p1, :cond_0

    .line 377
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "offset < 0"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 378
    :cond_0
    if-gez p2, :cond_1

    .line 379
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "count < 0"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 380
    :cond_1
    if-le p1, p0, :cond_2

    .line 381
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "offset > length"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 382
    :cond_2
    sub-int v0, p0, p2

    if-le p1, v0, :cond_3

    .line 383
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "offset + count > length"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 385
    :cond_3
    return-void
.end method

.method private free()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 709
    iget-wide v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 714
    :goto_0
    return-void

    .line 712
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    invoke-static {v0, v1}, Lcom/google/android/gms/org/conscrypt/NativeCrypto;->SSL_free(J)V

    .line 713
    iput-wide v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    goto :goto_0
.end method

.method private getNextAvailableByteBuffer([Ljava/nio/ByteBuffer;II)Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 505
    :goto_0
    if-ge p2, p3, :cond_1

    .line 506
    aget-object v0, p1, p2

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    if-lez v0, :cond_0

    .line 507
    aget-object v0, p1, p2

    .line 510
    :goto_1
    return-object v0

    .line 505
    :cond_0
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 510
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private shutdown()V
    .locals 7

    .prologue
    .line 690
    :try_start_0
    iget-wide v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    sget-object v2, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->nullSource:Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSource;

    invoke-virtual {v2}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSource;->getContext()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->localToRemoteSink:Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;

    invoke-virtual {v4}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;->getContext()J

    move-result-wide v4

    move-object v6, p0

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/org/conscrypt/NativeCrypto;->SSL_shutdown_BIO(JJJLcom/google/android/gms/org/conscrypt/NativeCrypto$SSLHandshakeCallbacks;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 698
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private shutdownAndFreeSslNative()V
    .locals 1

    .prologue
    .line 702
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->shutdown()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 704
    invoke-direct {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->free()V

    .line 705
    return-void

    .line 704
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->free()V

    throw v0
.end method

.method private static writeSinkToByteBuffer(Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;Ljava/nio/ByteBuffer;)I
    .locals 4

    .prologue
    .line 607
    invoke-virtual {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;->available()I

    move-result v0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 608
    invoke-virtual {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;->toByteArray()[B

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;->position()I

    move-result v2

    invoke-virtual {p1, v1, v2, v0}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 609
    int-to-long v2, v0

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;->skip(J)J

    .line 610
    return v0
.end method


# virtual methods
.method public beginHandshake()V
    .locals 9

    .prologue
    .line 133
    iget-object v1, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 134
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v2, Lcom/google/android/gms/org/conscrypt/m;->j:Lcom/google/android/gms/org/conscrypt/m;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v2, Lcom/google/android/gms/org/conscrypt/m;->i:Lcom/google/android/gms/org/conscrypt/m;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v2, Lcom/google/android/gms/org/conscrypt/m;->h:Lcom/google/android/gms/org/conscrypt/m;

    if-ne v0, v2, :cond_1

    .line 136
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Engine has already been closed"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 138
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v2, Lcom/google/android/gms/org/conscrypt/m;->d:Lcom/google/android/gms/org/conscrypt/m;

    if-ne v0, v2, :cond_2

    .line 139
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Handshake has already been started"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 141
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v2, Lcom/google/android/gms/org/conscrypt/m;->b:Lcom/google/android/gms/org/conscrypt/m;

    if-eq v0, v2, :cond_3

    .line 142
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Client/server mode must be set before handshake"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 144
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getUseClientMode()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 145
    sget-object v0, Lcom/google/android/gms/org/conscrypt/m;->c:Lcom/google/android/gms/org/conscrypt/m;

    iput-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    .line 149
    :goto_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->getSessionContext()Lcom/google/android/gms/org/conscrypt/a;

    move-result-object v0

    .line 154
    iget-wide v2, v0, Lcom/google/android/gms/org/conscrypt/a;->sslCtxNativePointer:J

    .line 155
    invoke-static {v2, v3}, Lcom/google/android/gms/org/conscrypt/NativeCrypto;->SSL_new(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    iget-wide v4, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    invoke-virtual {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getPeerHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getPeerPort()I

    move-result v6

    invoke-virtual {v0, v4, v5, v1, v6}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->getSessionToReuse(JLjava/lang/String;I)Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;

    .line 158
    iget-object v1, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    iget-wide v4, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    invoke-virtual {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getPeerHost()Ljava/lang/String;

    move-result-object v8

    move-object v6, p0

    move-object v7, p0

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->setSSLParameters(JJLcom/google/android/gms/org/conscrypt/SSLParametersImpl$AliasChooser;Lcom/google/android/gms/org/conscrypt/SSLParametersImpl$PSKCallbacks;Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    iget-wide v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->setCertificateValidation(J)V

    .line 161
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    iget-wide v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    iget-object v1, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->channelIdPrivateKey:Lcom/google/android/gms/org/conscrypt/OpenSSLKey;

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->setTlsChannelId(JLcom/google/android/gms/org/conscrypt/OpenSSLKey;)V

    .line 162
    invoke-virtual {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getUseClientMode()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 163
    iget-wide v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    invoke-static {v0, v1}, Lcom/google/android/gms/org/conscrypt/NativeCrypto;->SSL_set_connect_state(J)V

    .line 167
    :goto_1
    invoke-static {}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;->create()Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->handshakeSink:Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 168
    return-void

    .line 147
    :cond_4
    :try_start_3
    sget-object v0, Lcom/google/android/gms/org/conscrypt/m;->d:Lcom/google/android/gms/org/conscrypt/m;

    iput-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 165
    :cond_5
    :try_start_4
    iget-wide v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    invoke-static {v0, v1}, Lcom/google/android/gms/org/conscrypt/NativeCrypto;->SSL_set_accept_state(J)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    .line 180
    :catch_0
    move-exception v0

    .line 171
    :try_start_5
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 173
    const-string v2, "unexpected CCS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 174
    const-string v1, "ssl_unexpected_ccs: host=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getPeerHost()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 175
    invoke-static {v1}, Lcom/google/android/gms/org/conscrypt/Platform;->logEvent(Ljava/lang/String;)V

    .line 177
    :cond_6
    new-instance v1, Ljavax/net/ssl/SSLException;

    invoke-direct {v1, v0}, Ljavax/net/ssl/SSLException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 179
    :catchall_1
    move-exception v0

    .line 180
    iget-object v1, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 181
    :try_start_6
    sget-object v2, Lcom/google/android/gms/org/conscrypt/m;->j:Lcom/google/android/gms/org/conscrypt/m;

    iput-object v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    .line 182
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 183
    invoke-direct {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->shutdownAndFreeSslNative()V

    throw v0

    .line 182
    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public chooseClientAlias(Ljavax/net/ssl/X509KeyManager;[Ljavax/security/auth/x500/X500Principal;[Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 738
    instance-of v0, p1, Ljavax/net/ssl/X509ExtendedKeyManager;

    if-eqz v0, :cond_0

    .line 739
    check-cast p1, Ljavax/net/ssl/X509ExtendedKeyManager;

    .line 740
    invoke-virtual {p1, p3, p2, p0}, Ljavax/net/ssl/X509ExtendedKeyManager;->chooseEngineClientAlias([Ljava/lang/String;[Ljava/security/Principal;Ljavax/net/ssl/SSLEngine;)Ljava/lang/String;

    move-result-object v0

    .line 742
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-interface {p1, p3, p2, v0}, Ljavax/net/ssl/X509KeyManager;->chooseClientAlias([Ljava/lang/String;[Ljava/security/Principal;Ljava/net/Socket;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public chooseClientPSKIdentity(Lcom/google/android/gms/org/conscrypt/PSKKeyManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 753
    invoke-interface {p1, p2, p0}, Lcom/google/android/gms/org/conscrypt/PSKKeyManager;->chooseClientKeyIdentity(Ljava/lang/String;Ljavax/net/ssl/SSLEngine;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public chooseServerAlias(Ljavax/net/ssl/X509KeyManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 727
    instance-of v0, p1, Ljavax/net/ssl/X509ExtendedKeyManager;

    if-eqz v0, :cond_0

    .line 728
    check-cast p1, Ljavax/net/ssl/X509ExtendedKeyManager;

    .line 729
    invoke-virtual {p1, p2, v1, p0}, Ljavax/net/ssl/X509ExtendedKeyManager;->chooseEngineServerAlias(Ljava/lang/String;[Ljava/security/Principal;Ljavax/net/ssl/SSLEngine;)Ljava/lang/String;

    move-result-object v0

    .line 731
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1, p2, v1, v1}, Ljavax/net/ssl/X509KeyManager;->chooseServerAlias(Ljava/lang/String;[Ljava/security/Principal;Ljava/net/Socket;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public chooseServerPSKIdentityHint(Lcom/google/android/gms/org/conscrypt/PSKKeyManager;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 748
    invoke-interface {p1, p0}, Lcom/google/android/gms/org/conscrypt/PSKKeyManager;->chooseServerKeyIdentityHint(Ljavax/net/ssl/SSLEngine;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public clientCertificateRequested([B[[B)V
    .locals 7

    .prologue
    .line 684
    iget-object v1, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    iget-wide v4, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    move-object v2, p1

    move-object v3, p2

    move-object v6, p0

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->chooseClientCertificate([B[[BJLcom/google/android/gms/org/conscrypt/SSLParametersImpl$AliasChooser;)V

    .line 686
    return-void
.end method

.method public clientPSKKeyRequested(Ljava/lang/String;[B[B)I
    .locals 1

    .prologue
    .line 615
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0, p1, p2, p3, p0}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->clientPSKKeyRequested(Ljava/lang/String;[B[BLcom/google/android/gms/org/conscrypt/SSLParametersImpl$PSKCallbacks;)I

    move-result v0

    return v0
.end method

.method public closeInbound()V
    .locals 3

    .prologue
    .line 190
    iget-object v1, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 191
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v2, Lcom/google/android/gms/org/conscrypt/m;->j:Lcom/google/android/gms/org/conscrypt/m;

    if-ne v0, v2, :cond_0

    .line 192
    monitor-exit v1

    .line 199
    :goto_0
    return-void

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v2, Lcom/google/android/gms/org/conscrypt/m;->i:Lcom/google/android/gms/org/conscrypt/m;

    if-ne v0, v2, :cond_1

    .line 195
    sget-object v0, Lcom/google/android/gms/org/conscrypt/m;->j:Lcom/google/android/gms/org/conscrypt/m;

    iput-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    .line 199
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 197
    :cond_1
    :try_start_1
    sget-object v0, Lcom/google/android/gms/org/conscrypt/m;->h:Lcom/google/android/gms/org/conscrypt/m;

    iput-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public closeOutbound()V
    .locals 3

    .prologue
    .line 205
    iget-object v1, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 206
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v2, Lcom/google/android/gms/org/conscrypt/m;->j:Lcom/google/android/gms/org/conscrypt/m;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v2, Lcom/google/android/gms/org/conscrypt/m;->i:Lcom/google/android/gms/org/conscrypt/m;

    if-ne v0, v2, :cond_1

    .line 207
    :cond_0
    monitor-exit v1

    .line 219
    :goto_0
    return-void

    .line 209
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v2, Lcom/google/android/gms/org/conscrypt/m;->b:Lcom/google/android/gms/org/conscrypt/m;

    if-eq v0, v2, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v2, Lcom/google/android/gms/org/conscrypt/m;->a:Lcom/google/android/gms/org/conscrypt/m;

    if-eq v0, v2, :cond_2

    .line 210
    invoke-direct {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->shutdownAndFreeSslNative()V

    .line 212
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v2, Lcom/google/android/gms/org/conscrypt/m;->h:Lcom/google/android/gms/org/conscrypt/m;

    if-ne v0, v2, :cond_3

    .line 213
    sget-object v0, Lcom/google/android/gms/org/conscrypt/m;->j:Lcom/google/android/gms/org/conscrypt/m;

    iput-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    .line 217
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    invoke-direct {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->shutdown()V

    goto :goto_0

    .line 215
    :cond_3
    :try_start_1
    sget-object v0, Lcom/google/android/gms/org/conscrypt/m;->i:Lcom/google/android/gms/org/conscrypt/m;

    iput-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 217
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected finalize()V
    .locals 1

    .prologue
    .line 719
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->free()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 721
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 722
    return-void

    .line 721
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public getDelegatedTask()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x0

    return-object v0
.end method

.method public getEnableSessionCreation()Z
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->getEnableSessionCreation()Z

    move-result v0

    return v0
.end method

.method public getEnabledCipherSuites()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->getEnabledCipherSuites()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEnabledProtocols()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->getEnabledProtocols()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;
    .locals 4

    .prologue
    .line 244
    iget-object v1, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 245
    :try_start_0
    sget-object v0, Lcom/google/android/gms/org/conscrypt/l;->a:[I

    iget-object v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    invoke-virtual {v2}, Lcom/google/android/gms/org/conscrypt/m;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 277
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected engine state: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 278
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 247
    :pswitch_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getUseClientMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    sget-object v0, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_WRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    monitor-exit v1

    .line 273
    :goto_0
    return-object v0

    .line 250
    :cond_0
    sget-object v0, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_UNWRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    monitor-exit v1

    goto :goto_0

    .line 253
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->handshakeSink:Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;

    invoke-virtual {v0}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;->available()I

    move-result v0

    if-lez v0, :cond_1

    .line 254
    sget-object v0, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_WRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    monitor-exit v1

    goto :goto_0

    .line 256
    :cond_1
    sget-object v0, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_UNWRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    monitor-exit v1

    goto :goto_0

    .line 259
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->handshakeSink:Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;

    invoke-virtual {v0}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;->available()I

    move-result v0

    if-nez v0, :cond_2

    .line 260
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->handshakeSink:Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;

    .line 261
    sget-object v0, Lcom/google/android/gms/org/conscrypt/m;->g:Lcom/google/android/gms/org/conscrypt/m;

    iput-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    .line 262
    sget-object v0, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->FINISHED:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    monitor-exit v1

    goto :goto_0

    .line 264
    :cond_2
    sget-object v0, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_WRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    monitor-exit v1

    goto :goto_0

    .line 273
    :pswitch_3
    sget-object v0, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NOT_HANDSHAKING:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 245
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public getNeedClientAuth()Z
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->getNeedClientAuth()Z

    move-result v0

    return v0
.end method

.method public getPSKKey(Lcom/google/android/gms/org/conscrypt/PSKKeyManager;Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/SecretKey;
    .locals 1

    .prologue
    .line 758
    invoke-interface {p1, p2, p3, p0}, Lcom/google/android/gms/org/conscrypt/PSKKeyManager;->getKey(Ljava/lang/String;Ljava/lang/String;Ljavax/net/ssl/SSLEngine;)Ljavax/crypto/SecretKey;

    move-result-object v0

    return-object v0
.end method

.method public getSession()Ljavax/net/ssl/SSLSession;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;

    if-nez v0, :cond_0

    .line 289
    invoke-static {}, Lcom/google/android/gms/org/conscrypt/SSLNullSession;->getNullSession()Ljavax/net/ssl/SSLSession;

    move-result-object v0

    .line 291
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;

    goto :goto_0
.end method

.method public getSupportedCipherSuites()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 296
    invoke-static {}, Lcom/google/android/gms/org/conscrypt/NativeCrypto;->getSupportedCipherSuites()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSupportedProtocols()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 301
    invoke-static {}, Lcom/google/android/gms/org/conscrypt/NativeCrypto;->getSupportedProtocols()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUseClientMode()Z
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->getUseClientMode()Z

    move-result v0

    return v0
.end method

.method public getWantClientAuth()Z
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->getWantClientAuth()Z

    move-result v0

    return v0
.end method

.method public isInboundDone()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 316
    iget-wide v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    .line 317
    iget-object v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v2

    .line 318
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v4, Lcom/google/android/gms/org/conscrypt/m;->j:Lcom/google/android/gms/org/conscrypt/m;

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v4, Lcom/google/android/gms/org/conscrypt/m;->h:Lcom/google/android/gms/org/conscrypt/m;

    if-ne v3, v4, :cond_2

    :cond_0
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 322
    :cond_1
    :goto_1
    return v0

    :cond_2
    move v0, v1

    .line 318
    goto :goto_0

    .line 320
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 322
    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    invoke-static {v2, v3}, Lcom/google/android/gms/org/conscrypt/NativeCrypto;->SSL_get_shutdown(J)I

    move-result v2

    and-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_1
.end method

.method public isOutboundDone()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 328
    iget-wide v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    .line 329
    iget-object v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v2

    .line 330
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v4, Lcom/google/android/gms/org/conscrypt/m;->j:Lcom/google/android/gms/org/conscrypt/m;

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v4, Lcom/google/android/gms/org/conscrypt/m;->i:Lcom/google/android/gms/org/conscrypt/m;

    if-ne v3, v4, :cond_2

    :cond_0
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 334
    :cond_1
    :goto_1
    return v0

    :cond_2
    move v0, v1

    .line 330
    goto :goto_0

    .line 332
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 334
    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    invoke-static {v2, v3}, Lcom/google/android/gms/org/conscrypt/NativeCrypto;->SSL_get_shutdown(J)I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_1
.end method

.method public onSSLStateChange(JII)V
    .locals 4

    .prologue
    .line 625
    iget-object v1, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 626
    sparse-switch p3, :sswitch_data_0

    .line 641
    :goto_0
    :try_start_0
    monitor-exit v1

    return-void

    .line 628
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v2, Lcom/google/android/gms/org/conscrypt/m;->d:Lcom/google/android/gms/org/conscrypt/m;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v2, Lcom/google/android/gms/org/conscrypt/m;->f:Lcom/google/android/gms/org/conscrypt/m;

    if-eq v0, v2, :cond_0

    .line 630
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Completed handshake while in mode "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 641
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 633
    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/android/gms/org/conscrypt/m;->e:Lcom/google/android/gms/org/conscrypt/m;

    iput-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    goto :goto_0

    .line 638
    :sswitch_1
    sget-object v0, Lcom/google/android/gms/org/conscrypt/m;->d:Lcom/google/android/gms/org/conscrypt/m;

    iput-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 626
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x20 -> :sswitch_0
    .end sparse-switch
.end method

.method public serverPSKKeyRequested(Ljava/lang/String;Ljava/lang/String;[B)I
    .locals 1

    .prologue
    .line 620
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0, p1, p2, p3, p0}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->serverPSKKeyRequested(Ljava/lang/String;Ljava/lang/String;[BLcom/google/android/gms/org/conscrypt/SSLParametersImpl$PSKCallbacks;)I

    move-result v0

    return v0
.end method

.method public setEnableSessionCreation(Z)V
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->setEnableSessionCreation(Z)V

    .line 351
    return-void
.end method

.method public setEnabledCipherSuites([Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->setEnabledCipherSuites([Ljava/lang/String;)V

    .line 341
    return-void
.end method

.method public setEnabledProtocols([Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->setEnabledProtocols([Ljava/lang/String;)V

    .line 346
    return-void
.end method

.method public setNeedClientAuth(Z)V
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->setNeedClientAuth(Z)V

    .line 356
    return-void
.end method

.method public setUseClientMode(Z)V
    .locals 4

    .prologue
    .line 360
    iget-object v1, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 361
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v2, Lcom/google/android/gms/org/conscrypt/m;->b:Lcom/google/android/gms/org/conscrypt/m;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v2, Lcom/google/android/gms/org/conscrypt/m;->a:Lcom/google/android/gms/org/conscrypt/m;

    if-eq v0, v2, :cond_0

    .line 362
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can not change mode after handshake: engineState == "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 366
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 365
    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/android/gms/org/conscrypt/m;->b:Lcom/google/android/gms/org/conscrypt/m;

    iput-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    .line 366
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 367
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->setUseClientMode(Z)V

    .line 368
    return-void
.end method

.method public setWantClientAuth(Z)V
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->setWantClientAuth(Z)V

    .line 373
    return-void
.end method

.method public unwrap(Ljava/nio/ByteBuffer;[Ljava/nio/ByteBuffer;II)Ljavax/net/ssl/SSLEngineResult;
    .locals 21

    .prologue
    .line 390
    if-nez p1, :cond_0

    .line 391
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "src == null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 392
    :cond_0
    if-nez p2, :cond_1

    .line 393
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "dsts == null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 395
    :cond_1
    move-object/from16 v0, p2

    array-length v4, v0

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-static {v4, v0, v1}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->checkIndex(III)V

    .line 396
    const/4 v5, 0x0

    .line 397
    const/4 v4, 0x0

    :goto_0
    move-object/from16 v0, p2

    array-length v6, v0

    if-ge v4, v6, :cond_5

    .line 398
    aget-object v6, p2, v4

    .line 399
    if-nez v6, :cond_2

    .line 400
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "one of the dst == null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 401
    :cond_2
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->isReadOnly()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 402
    new-instance v4, Ljava/nio/ReadOnlyBufferException;

    invoke-direct {v4}, Ljava/nio/ReadOnlyBufferException;-><init>()V

    throw v4

    .line 404
    :cond_3
    move/from16 v0, p3

    if-lt v4, v0, :cond_4

    add-int v7, p3, p4

    if-ge v4, v7, :cond_4

    .line 405
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v6

    add-int/2addr v5, v6

    .line 397
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 409
    :cond_5
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v6

    .line 411
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v7, Lcom/google/android/gms/org/conscrypt/m;->j:Lcom/google/android/gms/org/conscrypt/m;

    if-eq v4, v7, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v7, Lcom/google/android/gms/org/conscrypt/m;->h:Lcom/google/android/gms/org/conscrypt/m;

    if-ne v4, v7, :cond_7

    .line 412
    :cond_6
    new-instance v4, Ljavax/net/ssl/SSLEngineResult;

    sget-object v5, Ljavax/net/ssl/SSLEngineResult$Status;->CLOSED:Ljavax/net/ssl/SSLEngineResult$Status;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v4, v5, v7, v8, v9}, Ljavax/net/ssl/SSLEngineResult;-><init>(Ljavax/net/ssl/SSLEngineResult$Status;Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;II)V

    monitor-exit v6

    .line 499
    :goto_1
    return-object v4

    .line 414
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v7, Lcom/google/android/gms/org/conscrypt/m;->a:Lcom/google/android/gms/org/conscrypt/m;

    if-eq v4, v7, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v7, Lcom/google/android/gms/org/conscrypt/m;->b:Lcom/google/android/gms/org/conscrypt/m;

    if-ne v4, v7, :cond_9

    .line 415
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->beginHandshake()V

    .line 417
    :cond_9
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 420
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v6

    .line 421
    sget-object v4, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_UNWRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    if-ne v6, v4, :cond_f

    .line 422
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v16

    .line 423
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSource;->wrap(Ljava/nio/ByteBuffer;)Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSource;

    move-result-object v17

    .line 424
    const-wide/16 v14, 0x0

    .line 426
    :try_start_1
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSource;->getContext()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->handshakeSink:Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;

    invoke-virtual {v8}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;->getContext()J

    move-result-wide v8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getUseClientMode()Z

    move-result v11

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    iget-object v12, v10, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->npnProtocols:[B

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    iget-object v13, v10, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->alpnProtocols:[B

    move-object/from16 v10, p0

    invoke-static/range {v4 .. v13}, Lcom/google/android/gms/org/conscrypt/NativeCrypto;->SSL_do_handshake_bio(JJJLcom/google/android/gms/org/conscrypt/NativeCrypto$SSLHandshakeCallbacks;Z[B[B)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-wide v6

    .line 429
    const-wide/16 v4, 0x0

    cmp-long v4, v6, v4

    if-eqz v4, :cond_b

    .line 430
    :try_start_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;

    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v5, Lcom/google/android/gms/org/conscrypt/m;->d:Lcom/google/android/gms/org/conscrypt/m;

    if-ne v4, v5, :cond_a

    .line 431
    sget-object v4, Lcom/google/android/gms/org/conscrypt/m;->f:Lcom/google/android/gms/org/conscrypt/m;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    .line 433
    :cond_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getPeerHost()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getPeerPort()I

    move-result v12

    const/4 v13, 0x1

    invoke-virtual/range {v5 .. v13}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->setupSession(JJLcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;Ljava/lang/String;IZ)Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;

    .line 436
    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->handshakeSink:Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;

    invoke-virtual {v4}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;->position()I

    move-result v8

    .line 437
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    sub-int v9, v4, v16

    .line 438
    new-instance v4, Ljavax/net/ssl/SSLEngineResult;

    if-lez v9, :cond_d

    sget-object v5, Ljavax/net/ssl/SSLEngineResult$Status;->OK:Ljavax/net/ssl/SSLEngineResult$Status;

    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v10

    invoke-direct {v4, v5, v10, v9, v8}, Ljavax/net/ssl/SSLEngineResult;-><init>(Ljavax/net/ssl/SSLEngineResult$Status;Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;II)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 444
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;

    if-nez v5, :cond_c

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_c

    .line 445
    invoke-static {v6, v7}, Lcom/google/android/gms/org/conscrypt/NativeCrypto;->SSL_SESSION_free(J)V

    .line 447
    :cond_c
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSource;->release()V

    goto/16 :goto_1

    .line 417
    :catchall_0
    move-exception v4

    monitor-exit v6

    throw v4

    .line 438
    :cond_d
    :try_start_3
    sget-object v5, Ljavax/net/ssl/SSLEngineResult$Status;->BUFFER_UNDERFLOW:Ljavax/net/ssl/SSLEngineResult$Status;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    .line 440
    :catch_0
    move-exception v4

    move-wide v6, v14

    .line 441
    :goto_3
    :try_start_4
    new-instance v5, Ljavax/net/ssl/SSLHandshakeException;

    const-string v8, "Handshake failed"

    invoke-direct {v5, v8}, Ljavax/net/ssl/SSLHandshakeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljavax/net/ssl/SSLHandshakeException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v4

    check-cast v4, Ljavax/net/ssl/SSLHandshakeException;

    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 444
    :catchall_1
    move-exception v4

    :goto_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;

    if-nez v5, :cond_e

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_e

    .line 445
    invoke-static {v6, v7}, Lcom/google/android/gms/org/conscrypt/NativeCrypto;->SSL_SESSION_free(J)V

    .line 447
    :cond_e
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSource;->release()V

    throw v4

    .line 449
    :cond_f
    sget-object v4, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NOT_HANDSHAKING:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    if-eq v6, v4, :cond_10

    .line 450
    new-instance v4, Ljavax/net/ssl/SSLEngineResult;

    sget-object v5, Ljavax/net/ssl/SSLEngineResult$Status;->OK:Ljavax/net/ssl/SSLEngineResult$Status;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v4, v5, v6, v7, v8}, Ljavax/net/ssl/SSLEngineResult;-><init>(Ljavax/net/ssl/SSLEngineResult$Status;Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;II)V

    goto/16 :goto_1

    .line 453
    :cond_10
    if-nez v5, :cond_11

    .line 454
    new-instance v4, Ljavax/net/ssl/SSLEngineResult;

    sget-object v5, Ljavax/net/ssl/SSLEngineResult$Status;->BUFFER_OVERFLOW:Ljavax/net/ssl/SSLEngineResult$Status;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v4, v5, v6, v7, v8}, Ljavax/net/ssl/SSLEngineResult;-><init>(Ljavax/net/ssl/SSLEngineResult$Status;Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;II)V

    goto/16 :goto_1

    .line 457
    :cond_11
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v18

    .line 458
    invoke-static/range {v18 .. v18}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSource;->wrap(Ljava/nio/ByteBuffer;)Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSource;

    move-result-object v19

    .line 460
    :try_start_5
    invoke-virtual/range {v18 .. v18}, Ljava/nio/ByteBuffer;->position()I

    move-result v20

    .line 461
    const/4 v4, 0x0

    .line 462
    const/4 v5, 0x0

    move/from16 v16, v4

    move/from16 v17, v5

    .line 464
    :goto_5
    if-nez v17, :cond_15

    .line 465
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getNextAvailableByteBuffer([Ljava/nio/ByteBuffer;II)Ljava/nio/ByteBuffer;

    move-result-object v15

    .line 466
    if-nez v15, :cond_12

    .line 467
    const/4 v4, 0x1

    move/from16 v17, v4

    .line 468
    goto :goto_5

    .line 471
    :cond_12
    invoke-virtual {v15}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v4

    if-eqz v4, :cond_17

    .line 472
    invoke-virtual {v15}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 475
    :goto_6
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v5

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->position()I

    move-result v6

    add-int v8, v5, v6

    .line 477
    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v7

    invoke-virtual {v15}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v9

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSource;->getContext()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->localToRemoteSink:Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;

    invoke-virtual {v12}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;->getContext()J

    move-result-wide v12

    move-object/from16 v14, p0

    invoke-static/range {v5 .. v14}, Lcom/google/android/gms/org/conscrypt/NativeCrypto;->SSL_read_BIO(J[BIIJJLcom/google/android/gms/org/conscrypt/NativeCrypto$SSLHandshakeCallbacks;)I

    move-result v5

    .line 480
    if-gtz v5, :cond_13

    .line 481
    const/4 v4, 0x1

    move/from16 v17, v4

    .line 482
    goto :goto_5

    .line 484
    :cond_13
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->position()I

    move-result v6

    add-int/2addr v6, v5

    invoke-virtual {v4, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 485
    add-int v5, v5, v16

    .line 486
    if-eq v15, v4, :cond_14

    .line 487
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 488
    invoke-virtual {v15, v4}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    :cond_14
    move/from16 v16, v5

    .line 490
    goto :goto_5

    .line 492
    :cond_15
    invoke-virtual/range {v18 .. v18}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    sub-int v6, v4, v20

    .line 493
    invoke-virtual/range {v18 .. v18}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 494
    new-instance v4, Ljavax/net/ssl/SSLEngineResult;

    if-lez v6, :cond_16

    sget-object v5, Ljavax/net/ssl/SSLEngineResult$Status;->OK:Ljavax/net/ssl/SSLEngineResult$Status;

    :goto_7
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v7

    move/from16 v0, v16

    invoke-direct {v4, v5, v7, v6, v0}, Ljavax/net/ssl/SSLEngineResult;-><init>(Ljavax/net/ssl/SSLEngineResult$Status;Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;II)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 499
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSource;->release()V

    goto/16 :goto_1

    .line 494
    :cond_16
    :try_start_6
    sget-object v5, Ljavax/net/ssl/SSLEngineResult$Status;->BUFFER_UNDERFLOW:Ljavax/net/ssl/SSLEngineResult$Status;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto :goto_7

    .line 496
    :catch_1
    move-exception v4

    .line 497
    :try_start_7
    new-instance v5, Ljavax/net/ssl/SSLException;

    invoke-direct {v5, v4}, Ljavax/net/ssl/SSLException;-><init>(Ljava/lang/Throwable;)V

    throw v5
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 499
    :catchall_2
    move-exception v4

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSource;->release()V

    throw v4

    .line 444
    :catchall_3
    move-exception v4

    move-wide v6, v14

    goto/16 :goto_4

    .line 440
    :catch_2
    move-exception v4

    goto/16 :goto_3

    :cond_17
    move-object v4, v15

    goto :goto_6
.end method

.method public verifyCertificateChain(J[JLjava/lang/String;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v10, 0x0

    .line 648
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v1}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->getX509TrustManager()Ljavax/net/ssl/X509TrustManager;

    move-result-object v9

    .line 649
    if-nez v9, :cond_0

    .line 650
    new-instance v0, Ljava/security/cert/CertificateException;

    const-string v1, "No X.509 TrustManager"

    invoke-direct {v0, v1}, Ljava/security/cert/CertificateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 671
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 677
    :catchall_0
    move-exception v0

    iput-object v10, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->handshakeSession:Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;

    throw v0

    .line 652
    :cond_0
    if-eqz p3, :cond_1

    :try_start_2
    array-length v1, p3

    if-nez v1, :cond_2

    .line 653
    :cond_1
    new-instance v0, Ljavax/net/ssl/SSLException;

    const-string v1, "Peer sent no certificate"

    invoke-direct {v0, v1}, Ljavax/net/ssl/SSLException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/security/cert/CertificateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 673
    :catch_1
    move-exception v0

    .line 674
    :try_start_3
    new-instance v1, Ljava/security/cert/CertificateException;

    invoke-direct {v1, v0}, Ljava/security/cert/CertificateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 655
    :cond_2
    :try_start_4
    array-length v1, p3

    new-array v5, v1, [Lcom/google/android/gms/org/conscrypt/OpenSSLX509Certificate;

    .line 656
    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_3

    .line 657
    new-instance v1, Lcom/google/android/gms/org/conscrypt/OpenSSLX509Certificate;

    aget-wide v2, p3, v0

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/org/conscrypt/OpenSSLX509Certificate;-><init>(J)V

    aput-object v1, v5, v0

    .line 656
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 661
    :cond_3
    new-instance v1, Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getPeerHost()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getPeerPort()I

    move-result v7

    const/4 v8, 0x0

    move-wide v2, p1

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;-><init>(J[Ljava/security/cert/X509Certificate;[Ljava/security/cert/X509Certificate;Ljava/lang/String;ILcom/google/android/gms/org/conscrypt/a;)V

    iput-object v1, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->handshakeSession:Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;

    .line 664
    iget-object v0, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->getUseClientMode()Z

    move-result v0

    .line 665
    if-eqz v0, :cond_4

    .line 666
    invoke-virtual {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getPeerHost()Ljava/lang/String;

    move-result-object v0

    invoke-static {v9, v5, p4, v0}, Lcom/google/android/gms/org/conscrypt/Platform;->checkServerTrusted(Ljavax/net/ssl/X509TrustManager;[Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/security/cert/CertificateException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 677
    :goto_1
    iput-object v10, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->handshakeSession:Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;

    .line 678
    return-void

    .line 668
    :cond_4
    const/4 v0, 0x0

    :try_start_5
    aget-object v0, v5, v0

    invoke-virtual {v0}, Lcom/google/android/gms/org/conscrypt/OpenSSLX509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v0

    invoke-interface {v0}, Ljava/security/PublicKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v0

    .line 669
    invoke-interface {v9, v5, v0}, Ljavax/net/ssl/X509TrustManager;->checkClientTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/security/cert/CertificateException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method

.method public wrap([Ljava/nio/ByteBuffer;IILjava/nio/ByteBuffer;)Ljavax/net/ssl/SSLEngineResult;
    .locals 14

    .prologue
    .line 516
    if-nez p1, :cond_0

    .line 517
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "srcs == null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 518
    :cond_0
    if-nez p4, :cond_1

    .line 519
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "dst == null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 520
    :cond_1
    invoke-virtual/range {p4 .. p4}, Ljava/nio/ByteBuffer;->isReadOnly()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 521
    new-instance v2, Ljava/nio/ReadOnlyBufferException;

    invoke-direct {v2}, Ljava/nio/ReadOnlyBufferException;-><init>()V

    throw v2

    .line 523
    :cond_2
    array-length v3, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_4

    aget-object v4, p1, v2

    .line 524
    if-nez v4, :cond_3

    .line 525
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "one of the src == null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 523
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 528
    :cond_4
    array-length v2, p1

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->checkIndex(III)V

    .line 530
    invoke-virtual/range {p4 .. p4}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    const/16 v3, 0x4145

    if-ge v2, v3, :cond_5

    .line 531
    new-instance v2, Ljavax/net/ssl/SSLEngineResult;

    sget-object v3, Ljavax/net/ssl/SSLEngineResult$Status;->BUFFER_OVERFLOW:Ljavax/net/ssl/SSLEngineResult$Status;

    invoke-virtual {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Ljavax/net/ssl/SSLEngineResult;-><init>(Ljavax/net/ssl/SSLEngineResult$Status;Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;II)V

    .line 598
    :goto_1
    return-object v2

    .line 534
    :cond_5
    iget-object v3, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v3

    .line 536
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v4, Lcom/google/android/gms/org/conscrypt/m;->j:Lcom/google/android/gms/org/conscrypt/m;

    if-eq v2, v4, :cond_6

    iget-object v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v4, Lcom/google/android/gms/org/conscrypt/m;->i:Lcom/google/android/gms/org/conscrypt/m;

    if-ne v2, v4, :cond_7

    .line 537
    :cond_6
    new-instance v2, Ljavax/net/ssl/SSLEngineResult;

    sget-object v4, Ljavax/net/ssl/SSLEngineResult$Status;->CLOSED:Ljavax/net/ssl/SSLEngineResult$Status;

    invoke-virtual {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v2, v4, v5, v6, v7}, Ljavax/net/ssl/SSLEngineResult;-><init>(Ljavax/net/ssl/SSLEngineResult$Status;Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;II)V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 542
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 539
    :cond_7
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v4, Lcom/google/android/gms/org/conscrypt/m;->a:Lcom/google/android/gms/org/conscrypt/m;

    if-eq v2, v4, :cond_8

    iget-object v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v4, Lcom/google/android/gms/org/conscrypt/m;->b:Lcom/google/android/gms/org/conscrypt/m;

    if-ne v2, v4, :cond_9

    .line 540
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->beginHandshake()V

    .line 542
    :cond_9
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 545
    invoke-virtual {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v3

    .line 546
    sget-object v2, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_WRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    if-ne v3, v2, :cond_e

    .line 547
    iget-object v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->handshakeSink:Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;

    invoke-virtual {v2}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;->available()I

    move-result v2

    if-nez v2, :cond_c

    .line 548
    const-wide/16 v12, 0x0

    .line 550
    :try_start_2
    iget-wide v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    sget-object v4, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->nullSource:Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSource;

    invoke-virtual {v4}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSource;->getContext()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->handshakeSink:Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;

    invoke-virtual {v6}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;->getContext()J

    move-result-wide v6

    invoke-virtual {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getUseClientMode()Z

    move-result v9

    iget-object v8, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    iget-object v10, v8, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->npnProtocols:[B

    iget-object v8, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    iget-object v11, v8, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->alpnProtocols:[B

    move-object v8, p0

    invoke-static/range {v2 .. v11}, Lcom/google/android/gms/org/conscrypt/NativeCrypto;->SSL_do_handshake_bio(JJJLcom/google/android/gms/org/conscrypt/NativeCrypto$SSLHandshakeCallbacks;Z[B[B)J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-wide v4

    .line 554
    const-wide/16 v2, 0x0

    cmp-long v2, v4, v2

    if-eqz v2, :cond_b

    .line 555
    :try_start_3
    iget-object v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    sget-object v3, Lcom/google/android/gms/org/conscrypt/m;->d:Lcom/google/android/gms/org/conscrypt/m;

    if-ne v2, v3, :cond_a

    .line 556
    sget-object v2, Lcom/google/android/gms/org/conscrypt/m;->f:Lcom/google/android/gms/org/conscrypt/m;

    iput-object v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/google/android/gms/org/conscrypt/m;

    .line 558
    :cond_a
    iget-object v3, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;

    iget-wide v6, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    iget-object v8, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;

    invoke-virtual {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getPeerHost()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getPeerPort()I

    move-result v10

    const/4 v11, 0x1

    invoke-virtual/range {v3 .. v11}, Lcom/google/android/gms/org/conscrypt/SSLParametersImpl;->setupSession(JJLcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;Ljava/lang/String;IZ)Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 565
    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;

    if-nez v2, :cond_c

    const-wide/16 v2, 0x0

    cmp-long v2, v4, v2

    if-eqz v2, :cond_c

    .line 566
    invoke-static {v4, v5}, Lcom/google/android/gms/org/conscrypt/NativeCrypto;->SSL_SESSION_free(J)V

    .line 570
    :cond_c
    iget-object v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->handshakeSink:Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;

    move-object/from16 v0, p4

    invoke-static {v2, v0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->writeSinkToByteBuffer(Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;Ljava/nio/ByteBuffer;)I

    move-result v3

    .line 571
    new-instance v2, Ljavax/net/ssl/SSLEngineResult;

    sget-object v4, Ljavax/net/ssl/SSLEngineResult$Status;->OK:Ljavax/net/ssl/SSLEngineResult$Status;

    invoke-virtual {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v2, v4, v5, v6, v3}, Ljavax/net/ssl/SSLEngineResult;-><init>(Ljavax/net/ssl/SSLEngineResult$Status;Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;II)V

    goto/16 :goto_1

    .line 561
    :catch_0
    move-exception v2

    move-wide v4, v12

    .line 562
    :goto_2
    :try_start_4
    new-instance v3, Ljavax/net/ssl/SSLHandshakeException;

    const-string v6, "Handshake failed"

    invoke-direct {v3, v6}, Ljavax/net/ssl/SSLHandshakeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljavax/net/ssl/SSLHandshakeException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v2

    check-cast v2, Ljavax/net/ssl/SSLHandshakeException;

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 565
    :catchall_1
    move-exception v2

    :goto_3
    iget-object v3, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/google/android/gms/org/conscrypt/OpenSSLSessionImpl;

    if-nez v3, :cond_d

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_d

    .line 566
    invoke-static {v4, v5}, Lcom/google/android/gms/org/conscrypt/NativeCrypto;->SSL_SESSION_free(J)V

    :cond_d
    throw v2

    .line 572
    :cond_e
    sget-object v2, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NOT_HANDSHAKING:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    if-eq v3, v2, :cond_f

    .line 573
    new-instance v2, Ljavax/net/ssl/SSLEngineResult;

    sget-object v4, Ljavax/net/ssl/SSLEngineResult$Status;->OK:Ljavax/net/ssl/SSLEngineResult$Status;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v2, v4, v3, v5, v6}, Ljavax/net/ssl/SSLEngineResult;-><init>(Ljavax/net/ssl/SSLEngineResult$Status;Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;II)V

    goto/16 :goto_1

    .line 577
    :cond_f
    const/4 v9, 0x0

    .line 578
    const/4 v4, 0x0

    .line 580
    :try_start_5
    array-length v11, p1

    const/4 v2, 0x0

    move v10, v2

    :goto_4
    if-ge v10, v11, :cond_12

    aget-object v12, p1, v10

    .line 581
    invoke-virtual {v12}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v5

    .line 582
    if-eqz v4, :cond_10

    array-length v2, v4

    if-le v5, v2, :cond_11

    .line 583
    :cond_10
    new-array v4, v5, [B

    .line 589
    :cond_11
    invoke-virtual {v12}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v4, v3, v5}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 590
    iget-wide v2, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    iget-object v6, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->localToRemoteSink:Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;

    invoke-virtual {v6}, Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;->getContext()J

    move-result-wide v6

    move-object v8, p0

    invoke-static/range {v2 .. v8}, Lcom/google/android/gms/org/conscrypt/NativeCrypto;->SSL_write_BIO(J[BIJLcom/google/android/gms/org/conscrypt/NativeCrypto$SSLHandshakeCallbacks;)I

    move-result v2

    .line 592
    if-lez v2, :cond_13

    .line 593
    invoke-virtual {v12}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    add-int/2addr v3, v2

    invoke-virtual {v12, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 594
    add-int/2addr v2, v9

    .line 580
    :goto_5
    add-int/lit8 v3, v10, 0x1

    move v10, v3

    move v9, v2

    goto :goto_4

    .line 598
    :cond_12
    new-instance v2, Ljavax/net/ssl/SSLEngineResult;

    sget-object v3, Ljavax/net/ssl/SSLEngineResult$Status;->OK:Ljavax/net/ssl/SSLEngineResult$Status;

    invoke-virtual {p0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->localToRemoteSink:Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;

    move-object/from16 v0, p4

    invoke-static {v5, v0}, Lcom/google/android/gms/org/conscrypt/OpenSSLEngineImpl;->writeSinkToByteBuffer(Lcom/google/android/gms/org/conscrypt/OpenSSLBIOSink;Ljava/nio/ByteBuffer;)I

    move-result v5

    invoke-direct {v2, v3, v4, v9, v5}, Ljavax/net/ssl/SSLEngineResult;-><init>(Ljavax/net/ssl/SSLEngineResult$Status;Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;II)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_1

    .line 600
    :catch_1
    move-exception v2

    .line 601
    new-instance v3, Ljavax/net/ssl/SSLException;

    invoke-direct {v3, v2}, Ljavax/net/ssl/SSLException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 565
    :catchall_2
    move-exception v2

    move-wide v4, v12

    goto :goto_3

    .line 561
    :catch_2
    move-exception v2

    goto/16 :goto_2

    :cond_13
    move v2, v9

    goto :goto_5
.end method
