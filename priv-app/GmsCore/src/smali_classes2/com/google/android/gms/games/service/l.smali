.class public final Lcom/google/android/gms/games/service/l;
.super Lcom/google/android/gms/games/internal/dv;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/games/service/p;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/common/server/ClientContext;

.field private final d:I

.field private final e:Ljava/lang/String;

.field private f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final g:Ljava/util/HashMap;

.field private final h:Ljava/lang/Object;

.field private final i:Z

.field private j:Landroid/content/ContentValues;

.field private k:Landroid/content/ContentValues;

.field private l:Lcom/google/android/gms/games/service/b;

.field private final m:Ljava/util/Map;

.field private final n:Lcom/google/android/gms/games/internal/d/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 375
    invoke-direct {p0}, Lcom/google/android/gms/games/internal/dv;-><init>()V

    .line 331
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/l;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 336
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/l;->g:Ljava/util/HashMap;

    .line 341
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/l;->h:Ljava/lang/Object;

    .line 348
    iput-object v1, p0, Lcom/google/android/gms/games/service/l;->j:Landroid/content/ContentValues;

    .line 350
    iput-object v1, p0, Lcom/google/android/gms/games/service/l;->k:Landroid/content/ContentValues;

    .line 356
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/l;->m:Ljava/util/Map;

    .line 377
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/a/d;->a(Landroid/content/Context;)V

    .line 379
    iput-object p1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    .line 380
    iput-object p2, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    .line 381
    iput p3, p0, Lcom/google/android/gms/games/service/l;->d:I

    .line 382
    iput-object p4, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    .line 383
    iput-boolean p5, p0, Lcom/google/android/gms/games/service/l;->i:Z

    .line 384
    new-instance v0, Lcom/google/android/gms/games/service/m;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/service/m;-><init>(Lcom/google/android/gms/games/service/l;)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/l;->n:Lcom/google/android/gms/games/internal/d/d;

    .line 391
    new-instance v0, Lcom/google/android/gms/games/service/p;

    invoke-direct {v0, p1, p2, p0}, Lcom/google/android/gms/games/service/p;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/service/l;)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/l;->a:Lcom/google/android/gms/games/service/p;

    .line 392
    return-void
.end method

.method static synthetic I()V
    .locals 0

    .prologue
    .line 305
    invoke-static {}, Lcom/google/android/gms/games/service/l;->J()V

    return-void
.end method

.method private static J()V
    .locals 2

    .prologue
    .line 495
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 496
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Self permission check failed"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 498
    :cond_0
    return-void
.end method

.method private K()V
    .locals 2

    .prologue
    .line 509
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 511
    return-void
.end method

.method private L()V
    .locals 2

    .prologue
    .line 520
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 521
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->K()V

    .line 523
    :cond_0
    return-void
.end method

.method private M()V
    .locals 2

    .prologue
    .line 532
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->K()V

    .line 533
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    const-string v1, "https://www.googleapis.com/auth/games.firstparty"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    const-string v1, "https://www.googleapis.com/auth/games"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 535
    :cond_0
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Incorrect scope configuration - 1P access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 537
    :cond_1
    return-void
.end method

.method private N()V
    .locals 1

    .prologue
    .line 545
    iget-boolean v0, p0, Lcom/google/android/gms/games/service/l;->i:Z

    if-nez v0, :cond_0

    .line 546
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 548
    :cond_0
    return-void
.end method

.method private O()V
    .locals 2

    .prologue
    .line 556
    iget-boolean v0, p0, Lcom/google/android/gms/games/service/l;->i:Z

    if-eqz v0, :cond_0

    .line 557
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t be headless when checking if signed in"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 559
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/i/a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 561
    if-nez v0, :cond_1

    .line 562
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Not signed in when calling API"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 564
    :cond_1
    return-void
.end method

.method private P()Lcom/google/android/gms/games/service/ai;
    .locals 2

    .prologue
    .line 592
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 593
    invoke-static {}, Lcom/google/android/gms/games/service/ak;->a()Lcom/google/android/gms/games/service/ai;

    move-result-object v0

    return-object v0
.end method

.method private Q()V
    .locals 2

    .prologue
    .line 3290
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->k:Landroid/content/ContentValues;

    if-nez v0, :cond_0

    .line 3301
    :goto_0
    return-void

    .line 3298
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->k:Landroid/content/ContentValues;

    const-string v1, "snapshots_enabled"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 3299
    const-string v1, "Cannot use snapshots without enabling the \'Saved Game\' feature in the Play console"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/games/service/l;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    return-object v0
.end method

.method private varargs a(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;
    .locals 3

    .prologue
    .line 3668
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    .line 3669
    array-length v0, p4

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 3670
    aget-object v2, p4, v0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    .line 3669
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 3674
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/a/av;

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/games/a/av;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/games/a/av;->c:Ljava/lang/String;

    iput-boolean p3, v0, Lcom/google/android/gms/games/a/av;->g:Z

    iput-object p2, v0, Lcom/google/android/gms/games/a/av;->f:Ljava/lang/String;

    .line 3680
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3681
    iput-object p1, v0, Lcom/google/android/gms/games/a/av;->e:Ljava/lang/String;

    .line 3683
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/games/service/l;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 305
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    new-array v2, v3, [Ljava/lang/String;

    invoke-direct {p0, v1, p1, v3, v2}, Lcom/google/android/gms/games/service/l;->a(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 3243
    if-eqz p1, :cond_0

    .line 3244
    invoke-virtual {p1}, Lcom/google/android/gms/games/snapshot/d;->c()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 3245
    if-eqz v1, :cond_0

    .line 3247
    invoke-static {v1}, Lcom/google/android/gms/common/util/ac;->a(Landroid/graphics/Bitmap;)I

    move-result v2

    .line 3248
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->x()I

    move-result v3

    .line 3249
    if-le v2, v3, :cond_0

    .line 3250
    const-string v4, "GamesServiceBroker"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Snapshot cover image is too large. Currently at "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " bytes; max is "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ". Image will be scaled"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3252
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 3253
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 3256
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v5

    div-int/2addr v5, v4

    .line 3257
    int-to-float v6, v4

    int-to-float v7, v2

    div-float/2addr v6, v7

    .line 3258
    int-to-float v5, v5

    mul-float/2addr v5, v6

    .line 3259
    int-to-float v3, v3

    div-float/2addr v3, v5

    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-int v3, v6

    .line 3260
    int-to-float v5, v3

    int-to-float v2, v2

    div-float v2, v5, v2

    .line 3261
    int-to-float v4, v4

    mul-float/2addr v2, v4

    float-to-int v2, v2

    .line 3264
    invoke-static {v1, v2, v3, v0}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 3266
    new-instance v2, Lcom/google/android/gms/common/data/BitmapTeleporter;

    invoke-direct {v2, v1}, Lcom/google/android/gms/common/data/BitmapTeleporter;-><init>(Landroid/graphics/Bitmap;)V

    .line 3267
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/data/BitmapTeleporter;->a(Ljava/io/File;)V

    .line 3268
    invoke-virtual {p1, v2}, Lcom/google/android/gms/games/snapshot/d;->a(Lcom/google/android/gms/common/data/BitmapTeleporter;)V

    .line 3274
    :cond_0
    if-eqz p2, :cond_2

    .line 3276
    invoke-interface {p2}, Lcom/google/android/gms/drive/m;->b()I

    move-result v1

    .line 3277
    const/high16 v2, 0x20000000

    if-eq v1, v2, :cond_1

    :goto_0
    const-string v1, "Provided an unreadable contents object!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 3279
    invoke-interface {p2}, Lcom/google/android/gms/drive/m;->c()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->getStatSize()J

    move-result-wide v0

    .line 3280
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->w()I

    move-result v2

    .line 3281
    int-to-long v4, v2

    cmp-long v3, v0, v4

    if-lez v3, :cond_2

    .line 3282
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Contents are too large - provided "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " bytes; max is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 3277
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 3286
    :cond_2
    return-void
.end method

.method private a(Lcom/google/android/gms/common/api/c;)Z
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    if-nez v0, :cond_0

    .line 598
    const/4 v0, 0x0

    .line 601
    :goto_0
    return v0

    .line 600
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/b;->c()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 601
    invoke-interface {v0, p1}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/c;)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/games/service/l;)Lcom/google/android/gms/games/a/au;
    .locals 1

    .prologue
    .line 305
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    return-object v0
.end method

.method private varargs b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 3708
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    .line 3709
    const-string v0, "593950602418"

    iget-object v3, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 3710
    sget-object v0, Lcom/google/android/gms/games/c/a;->R:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 3711
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    .line 3714
    if-nez v4, :cond_0

    if-eqz v3, :cond_0

    .line 3715
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    .line 3716
    const/4 v0, 0x0

    .line 3721
    :cond_0
    array-length v5, p4

    if-lez v5, :cond_2

    .line 3722
    if-eqz v4, :cond_1

    if-eqz v3, :cond_1

    .line 3723
    invoke-static {v1}, Lcom/google/android/gms/games/a/l;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    .line 3728
    :goto_0
    array-length v4, p4

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_2

    .line 3729
    aget-object v5, p4, v3

    invoke-virtual {v1, v5}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    .line 3728
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 3725
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1}, Lcom/google/android/gms/games/a/l;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    goto :goto_0

    .line 3733
    :cond_2
    new-instance v3, Lcom/google/android/gms/games/a/av;

    iget-object v4, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-direct {v3, v4, v1}, Lcom/google/android/gms/games/a/av;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/google/android/gms/games/a/av;->c:Ljava/lang/String;

    iput-boolean p3, v3, Lcom/google/android/gms/games/a/av;->g:Z

    iput-object p1, v3, Lcom/google/android/gms/games/a/av;->e:Ljava/lang/String;

    iput-object p2, v3, Lcom/google/android/gms/games/a/av;->f:Ljava/lang/String;

    .line 3740
    iget-boolean v1, p0, Lcom/google/android/gms/games/service/l;->i:Z

    if-eqz v1, :cond_3

    .line 3741
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->e()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_2
    const-string v2, "Current player should be null in the headless UI"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3743
    iput-object p2, v3, Lcom/google/android/gms/games/a/av;->c:Ljava/lang/String;

    .line 3746
    :cond_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 3747
    iput-object v0, v3, Lcom/google/android/gms/games/a/av;->d:Ljava/lang/String;

    .line 3750
    :cond_4
    invoke-virtual {v3}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object v0

    return-object v0

    :cond_5
    move v1, v2

    .line 3741
    goto :goto_2
.end method

.method private static b(I)Ljava/util/ArrayList;
    .locals 2

    .prologue
    .line 2703
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2704
    and-int/lit8 v1, p0, 0x1

    if-eqz v1, :cond_0

    .line 2705
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2707
    :cond_0
    and-int/lit8 v1, p0, 0x2

    if-eqz v1, :cond_1

    .line 2708
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2710
    :cond_1
    return-object v0
.end method

.method private static b([I)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3615
    array-length v0, p0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Must provide at least one turn status!"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    move v0, v2

    .line 3617
    :goto_1
    array-length v3, p0

    if-ge v0, v3, :cond_2

    .line 3618
    aget v4, p0, v0

    .line 3619
    if-ltz v4, :cond_1

    const/4 v3, 0x4

    if-ge v4, v3, :cond_1

    move v3, v1

    :goto_2
    const-string v5, "Not a valid turn status: %s"

    new-array v6, v1, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v2

    invoke-static {v3, v5, v6}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 3617
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    .line 3615
    goto :goto_0

    :cond_1
    move v3, v2

    .line 3619
    goto :goto_2

    .line 3623
    :cond_2
    return-void
.end method

.method private static b([Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2755
    if-eqz p0, :cond_0

    array-length v0, p0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Must provide at least one ID!"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    move v0, v2

    .line 2757
    :goto_1
    array-length v3, p0

    if-ge v0, v3, :cond_2

    .line 2758
    aget-object v3, p0, v0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    move v3, v1

    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Not a valid external ID: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v5, p0, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2757
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    .line 2755
    goto :goto_0

    :cond_1
    move v3, v2

    .line 2758
    goto :goto_2

    .line 2761
    :cond_2
    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/games/service/l;)Lcom/google/android/gms/common/server/ClientContext;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    return-object v0
.end method

.method private d(Z)Lcom/google/android/gms/games/a/au;
    .locals 4

    .prologue
    .line 3635
    iget-boolean v0, p0, Lcom/google/android/gms/games/service/l;->i:Z

    if-eqz v0, :cond_0

    .line 3636
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t be headless with the default games context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3643
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3, p1}, Lcom/google/android/gms/games/a/au;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    return-object v0
.end method

.method private j(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 932
    sget-object v0, Lcom/google/android/gms/location/copresence/f;->b:Lcom/google/android/gms/common/api/c;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->a(Lcom/google/android/gms/common/api/c;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 933
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 934
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/r;

    .line 935
    if-eqz v0, :cond_0

    .line 939
    invoke-static {}, Lcom/google/android/gms/location/copresence/f;->a()Lcom/google/android/gms/location/copresence/h;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/google/android/gms/location/copresence/h;->a(Ljava/lang/String;)Lcom/google/android/gms/location/copresence/h;

    move-result-object v2

    .line 940
    invoke-interface {v2, v0}, Lcom/google/android/gms/location/copresence/h;->a(Lcom/google/android/gms/location/copresence/r;)Lcom/google/android/gms/location/copresence/h;

    .line 941
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/b;->c()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/google/android/gms/location/copresence/h;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    .line 943
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 945
    :cond_1
    return-void

    .line 943
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private k(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2467
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2468
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2469
    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2471
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2472
    return-object v0
.end method

.method private l(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3690
    const/4 v0, 0x0

    new-array v1, v2, [Ljava/lang/String;

    invoke-direct {p0, p1, v0, v2, v1}, Lcom/google/android/gms/games/service/l;->a(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    return-object v0
.end method

.method private m(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3757
    const/4 v0, 0x0

    new-array v1, v2, [Ljava/lang/String;

    invoke-direct {p0, p1, v0, v2, v1}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final A()Z
    .locals 1

    .prologue
    .line 409
    iget-boolean v0, p0, Lcom/google/android/gms/games/service/l;->i:Z

    return v0
.end method

.method public final B()V
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    if-eqz v0, :cond_0

    .line 429
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/b;->b()V

    .line 430
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    .line 432
    :cond_0
    return-void
.end method

.method public final C()Ljava/util/Map;
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->m:Ljava/util/Map;

    return-object v0
.end method

.method public final D()Lcom/google/android/gms/games/service/b;
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    return-object v0
.end method

.method public final E()V
    .locals 2

    .prologue
    .line 482
    sget-object v0, Lcom/google/android/gms/drive/b;->f:Lcom/google/android/gms/common/api/c;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->a(Lcom/google/android/gms/common/api/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 483
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/b;->a()V

    .line 484
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/b;)V

    .line 487
    :cond_0
    return-void
.end method

.method final F()V
    .locals 2

    .prologue
    .line 571
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v0

    .line 572
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;)V

    .line 573
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->a:Lcom/google/android/gms/games/service/p;

    monitor-enter v1

    .line 574
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->a:Lcom/google/android/gms/games/service/p;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/p;->a()V

    .line 575
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final G()V
    .locals 6

    .prologue
    .line 873
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->a:Lcom/google/android/gms/games/service/p;

    monitor-enter v1

    .line 874
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->a:Lcom/google/android/gms/games/service/p;

    iget-object v0, v2, Lcom/google/android/gms/games/service/p;->f:Lcom/google/android/gms/games/service/l;

    iget-boolean v0, v0, Lcom/google/android/gms/games/service/l;->i:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v3, "We don\'t allow headless UIs to start a session."

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    iget v0, v2, Lcom/google/android/gms/games/service/p;->e:I

    if-nez v0, :cond_0

    iget-object v0, v2, Lcom/google/android/gms/games/service/p;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/android/gms/games/service/p;->h:J

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/games/service/p;->g:Ljava/lang/String;

    iget-object v0, v2, Lcom/google/android/gms/games/service/p;->b:Landroid/content/Context;

    iget-object v3, v2, Lcom/google/android/gms/games/service/p;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v3}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    iget-object v0, v2, Lcom/google/android/gms/games/service/p;->d:Lcom/google/android/gms/games/service/r;

    iget-object v3, v2, Lcom/google/android/gms/games/service/p;->b:Landroid/content/Context;

    invoke-interface {v0, v3}, Lcom/google/android/gms/games/service/r;->a(Landroid/content/Context;)Lcom/google/android/gms/ads/identifier/c;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/games/service/p;->i:Lcom/google/android/gms/ads/identifier/c;

    :cond_0
    iget v0, v2, Lcom/google/android/gms/games/service/p;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/google/android/gms/games/service/p;->e:I

    .line 875
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 874
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 875
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final H()V
    .locals 3

    .prologue
    .line 952
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 953
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 954
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->j(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 956
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;[BLjava/lang/String;Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2883
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2884
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2885
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Room ID must not be null or empty"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2887
    invoke-static {p4}, Lcom/google/android/gms/games/multiplayer/k;->a(Ljava/lang/String;)Z

    move-result v0

    const-string v3, "Invalid participant ID %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2889
    const-string v0, "Cannot send a null message"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2891
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->P()Lcom/google/android/gms/games/service/ai;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/bs;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/google/android/gms/games/service/statemachine/roomclient/bs;-><init>(Lcom/google/android/gms/games/internal/dr;[BLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/ai;->a(Lcom/google/android/gms/games/service/statemachine/roomclient/bs;)I

    move-result v0

    return v0

    :cond_0
    move v0, v2

    .line 2885
    goto :goto_0
.end method

.method public final a([BLjava/lang/String;[Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2910
    const-string v0, "Cannot send a null message"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2911
    array-length v0, p1

    const/16 v3, 0x490

    if-le v0, v3, :cond_0

    .line 2912
    const/4 v0, -0x1

    .line 2925
    :goto_0
    return v0

    .line 2914
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2915
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "Room ID must not be null or empty"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2917
    if-eqz p3, :cond_2

    move v0, v2

    .line 2918
    :goto_2
    array-length v3, p3

    if-ge v0, v3, :cond_2

    .line 2919
    aget-object v3, p3, v0

    .line 2920
    invoke-static {v3}, Lcom/google/android/gms/games/multiplayer/k;->a(Ljava/lang/String;)Z

    move-result v4

    const-string v5, "Invalid participant ID %s"

    new-array v6, v1, [Ljava/lang/Object;

    aput-object v3, v6, v2

    invoke-static {v4, v5, v6}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2918
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    move v0, v2

    .line 2915
    goto :goto_1

    .line 2925
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->P()Lcom/google/android/gms/games/service/ai;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/bt;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/gms/games/service/statemachine/roomclient/bt;-><init>([BLjava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/ai;->a(Lcom/google/android/gms/games/service/statemachine/roomclient/bt;)I

    move-result v0

    goto :goto_0
.end method

.method public final a(IIZ)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 3324
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3328
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SELECT_OPPONENTS_TURN_BASED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3329
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 3330
    const-string v1, "com.google.android.gms.games.MIN_SELECTIONS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3331
    const-string v1, "com.google.android.gms.games.MAX_SELECTIONS"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3332
    const-string v1, "com.google.android.gms.games.SHOW_AUTOMATCH"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3333
    const-string v1, "com.google.android.gms.games.MULTIPLAYER_TYPE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3335
    iget v1, p0, Lcom/google/android/gms/games/service/l;->d:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(I[BILjava/lang/String;)Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2482
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2484
    if-eq p1, v2, :cond_0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Must provide a valid type."

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2486
    const-string v0, "Must provide a non-null payload."

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2487
    array-length v0, p2

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->s()I

    move-result v3

    if-gt v0, v3, :cond_4

    move v0, v2

    :goto_1
    const-string v3, "Payload is too big"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2489
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->t()I

    move-result v0

    if-gt p3, v0, :cond_5

    if-gtz p3, :cond_1

    const/4 v0, -0x1

    if-ne p3, v0, :cond_5

    :cond_1
    move v0, v2

    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Request lifetime days must be <= "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->t()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", greater than zero, or -1 for server default."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2495
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    :goto_3
    const-string v0, "Must provide a valid item description"

    invoke-static {v2, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2500
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SEND_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2501
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2502
    const-string v1, "com.google.android.gms.games.REQUEST_TYPE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2503
    const-string v1, "com.google.android.gms.games.PAYLOAD"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 2504
    const-string v1, "com.google.android.gms.games.REQUEST_LIFETIME"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2505
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2506
    const-string v1, "com.google.android.gms.games.REQUEST_ITEM_NAME"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2508
    :cond_2
    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2510
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2511
    iget v1, p0, Lcom/google/android/gms/games/service/l;->d:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v1

    .line 2484
    goto/16 :goto_0

    :cond_4
    move v0, v1

    .line 2487
    goto :goto_1

    :cond_5
    move v0, v1

    .line 2489
    goto :goto_2

    :cond_6
    move v2, v1

    .line 2495
    goto :goto_3
.end method

.method public final a(Lcom/google/android/gms/games/achievement/AchievementEntity;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1251
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->L()V

    .line 1252
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 1254
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.VIEW_ACHIEVEMENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1255
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1256
    const-string v1, "com.google.android.gms.games.ACHIEVEMENT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1257
    iget v1, p0, Lcom/google/android/gms/games/service/l;->d:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1321
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->L()V

    .line 1322
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 1327
    iget-boolean v0, p0, Lcom/google/android/gms/games/service/l;->i:Z

    if-eqz v0, :cond_0

    .line 1328
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_PUBLIC_INVITATIONS_INTERNAL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1334
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1335
    const-string v1, "com.google.android.gms.games.INVITATION_CLUSTER"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1336
    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1337
    const-string v1, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1338
    iget v1, p0, Lcom/google/android/gms/games/service/l;->d:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 1330
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_PUBLIC_INVITATIONS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1331
    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2531
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->L()V

    .line 2532
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 2537
    iget-boolean v0, p0, Lcom/google/android/gms/games/service/l;->i:Z

    if-eqz v0, :cond_0

    .line 2538
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_PUBLIC_REQUESTS_INTERNAL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2544
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2545
    const-string v1, "com.google.android.gms.games.GAME_REQUEST_CLUSTER"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2546
    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2547
    iget v1, p0, Lcom/google/android/gms/games/service/l;->d:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 2540
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_PUBLIC_REQUESTS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2541
    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/realtime/RoomEntity;I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2787
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2788
    const-string v0, "Room parameter must not be null"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2789
    if-ltz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "minParticipantsToStart must be >= 0"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2794
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_REAL_TIME_WAITING_ROOM"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2795
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2796
    const-string v1, "room"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2797
    const-string v1, "com.google.android.gms.games.MIN_PARTICIPANTS_TO_START"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2798
    iget v1, p0, Lcom/google/android/gms/games/service/l;->d:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 2789
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;ZZI)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3055
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3057
    sget-object v0, Lcom/google/android/gms/drive/b;->f:Lcom/google/android/gms/common/api/c;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->a(Lcom/google/android/gms/common/api/c;)Z

    move-result v0

    const-string v3, "Must include Drive.SCOPE_APPFOLDER to use snapshots!"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3059
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->Q()V

    .line 3060
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "Must provide a valid title"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3061
    const/4 v0, -0x1

    if-eq p4, v0, :cond_0

    if-lez p4, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "maxSnapshots must be greater than 0 or equal to -1"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3068
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_SELECT_SNAPSHOT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3069
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 3070
    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3072
    const-string v1, "com.google.android.gms.games.TITLE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3073
    const-string v1, "com.google.android.gms.games.ALLOW_CREATE_SNAPSHOT"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3074
    const-string v1, "com.google.android.gms.games.ALLOW_DELETE_SNAPSHOT"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3075
    const-string v1, "com.google.android.gms.games.MAX_SNAPSHOTS"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3076
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3077
    iget v1, p0, Lcom/google/android/gms/games/service/l;->d:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v2

    .line 3060
    goto :goto_0
.end method

.method public final a([I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2293
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2295
    array-length v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide at least one quest state"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2297
    const-string v0, "Must provide a non-null array of quest states"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2299
    invoke-static {p1}, Lcom/google/android/gms/games/internal/b/f;->a([I)Z

    move-result v0

    const-string v1, "Invalid quest state provided."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2301
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->n:Lcom/google/android/gms/games/internal/d/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/d/d;->b()V

    .line 2303
    const-string v0, "com.google.android.gms.games.SHOW_QUESTS"

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->k(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2304
    const-string v1, "com.google.android.gms.games.QUEST_STATES"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 2305
    iget v1, p0, Lcom/google/android/gms/games/service/l;->d:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 2295
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a([Lcom/google/android/gms/games/multiplayer/ParticipantEntity;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 7

    .prologue
    .line 1227
    iget-object v6, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/games/service/l;->a([Lcom/google/android/gms/games/multiplayer/ParticipantEntity;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a([Lcom/google/android/gms/games/multiplayer/ParticipantEntity;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1235
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->L()V

    .line 1236
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 1238
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.VIEW_PARTICIPANTS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1239
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1240
    const-string v1, "com.google.android.gms.games.PARTICIPANTS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1241
    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1242
    const-string v1, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1243
    const-string v1, "com.google.android.gms.games.FEATURED_URI"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1244
    const-string v1, "com.google.android.gms.games.ICON_URI"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1245
    const-string v1, "com.google.android.gms.games.GAME_ID"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1246
    iget v1, p0, Lcom/google/android/gms/games/service/l;->d:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/net/Uri;)Landroid/os/ParcelFileDescriptor;
    .locals 4

    .prologue
    .line 3763
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3764
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 3765
    const-string v0, "Uri cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3770
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 3772
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/images/a;->a(Landroid/content/Context;)Lcom/google/android/gms/common/images/a;

    move-result-object v0

    .line 3775
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/common/images/a;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    .line 3776
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 3778
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 846
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 631
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->L()V

    .line 632
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 634
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Game package name must not be empty"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 637
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 634
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 1741
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1742
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/d/ar;->a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v0

    .line 1743
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    invoke-static {v1, v0, v2, p1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1745
    return-void
.end method

.method public final a(J)V
    .locals 5

    .prologue
    .line 885
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->n:Lcom/google/android/gms/games/internal/d/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/d/d;->b()V

    .line 887
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/am;->a(J)V

    .line 888
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 890
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->P()Lcom/google/android/gms/games/service/ai;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/google/android/gms/games/service/ai;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;J)V

    .line 895
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->a:Lcom/google/android/gms/games/service/p;

    monitor-enter v1

    .line 896
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->a:Lcom/google/android/gms/games/service/p;

    iget-object v0, v2, Lcom/google/android/gms/games/service/p;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget v0, v2, Lcom/google/android/gms/games/service/p;->e:I

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    const-string v3, "We should never be decrementing the session reference counter, unless it is greater than 0."

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    iget v0, v2, Lcom/google/android/gms/games/service/p;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, v2, Lcom/google/android/gms/games/service/p;->e:I

    iget v0, v2, Lcom/google/android/gms/games/service/p;->e:I

    if-nez v0, :cond_1

    invoke-virtual {v2}, Lcom/google/android/gms/games/service/p;->a()V

    .line 897
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 896
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 897
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 1438
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1441
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide a valid Game ID, or null for \'all games\'"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1444
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v0

    .line 1445
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3, p1, p2}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;J)V

    .line 1447
    return-void

    .line 1441
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/ContentValues;)V
    .locals 1

    .prologue
    .line 416
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    iput-object v0, p0, Lcom/google/android/gms/games/service/l;->j:Landroid/content/ContentValues;

    .line 417
    return-void
.end method

.method public final a(Landroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2261
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2263
    new-instance v1, Lcom/google/android/gms/games/internal/el;

    invoke-direct {v1, p2, p1}, Lcom/google/android/gms/games/internal/el;-><init>(Landroid/os/Bundle;Landroid/os/IBinder;)V

    .line 2265
    const/4 v0, 0x0

    .line 2266
    new-instance v2, Lcom/google/android/gms/games/t;

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->f()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/gms/games/t;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 2268
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/gms/games/t;->c()I

    move-result v3

    if-lez v3, :cond_0

    .line 2269
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/t;->b(I)Lcom/google/android/gms/games/Player;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/PlayerEntity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2272
    :cond_0
    invoke-virtual {v2}, Lcom/google/android/gms/games/t;->w_()V

    .line 2275
    if-eqz v0, :cond_1

    .line 2276
    invoke-direct {p0, v4}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v2

    invoke-static {v2, v1, v0}, Lcom/google/android/gms/games/ui/c/k;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Lcom/google/android/gms/games/Player;)V

    .line 2278
    :cond_1
    return-void

    .line 2272
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/games/t;->w_()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)V
    .locals 2

    .prologue
    .line 459
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    .line 460
    new-instance v1, Lcom/google/android/gms/games/service/b;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/service/b;-><init>(Lcom/google/android/gms/common/api/v;)V

    iput-object v1, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    .line 463
    if-eqz v0, :cond_0

    .line 464
    invoke-virtual {v0}, Lcom/google/android/gms/games/service/b;->b()V

    .line 466
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/Contents;)V
    .locals 3

    .prologue
    .line 3159
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3160
    new-instance v0, Lcom/google/android/gms/drive/internal/an;

    invoke-direct {v0, p1}, Lcom/google/android/gms/drive/internal/an;-><init>(Lcom/google/android/gms/drive/Contents;)V

    .line 3162
    sget-object v1, Lcom/google/android/gms/drive/b;->f:Lcom/google/android/gms/common/api/c;

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/service/l;->a(Lcom/google/android/gms/common/api/c;)Z

    move-result v1

    const-string v2, "Must include Drive.SCOPE_APPFOLDER to use snapshots!"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3164
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->Q()V

    .line 3165
    const-string v1, "Must provide non-null contents"

    invoke-static {p1, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3166
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    invoke-virtual {v1}, Lcom/google/android/gms/games/service/b;->c()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/m;->a(Lcom/google/android/gms/common/api/v;)V

    .line 3167
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;)V
    .locals 5

    .prologue
    .line 851
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 852
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->n:Lcom/google/android/gms/games/internal/d/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/d/d;->b()V

    .line 855
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v0

    .line 856
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;)V

    .line 857
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 858
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->P()Lcom/google/android/gms/games/service/ai;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/service/ai;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    .line 861
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;)V

    .line 862
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v3}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x6

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 865
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1287
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1288
    const-string v2, "Must provide a valid callback object"

    invoke-static {p1, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1289
    if-eqz p2, :cond_0

    if-ne p2, v0, :cond_1

    :cond_0
    :goto_0
    const-string v2, "Invalid invitation sort order!"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1293
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1294
    invoke-static {v0, p1, p2, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->d(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V

    .line 1296
    return-void

    :cond_1
    move v0, v1

    .line 1289
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;III)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2645
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2647
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2648
    if-eqz p4, :cond_0

    if-ne p4, v2, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Invalid request sort order!"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2651
    if-eqz p2, :cond_1

    if-ne p2, v2, :cond_3

    :cond_1
    move v0, v2

    :goto_1
    const-string v3, "Invalid request direction!"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2654
    if-eqz p3, :cond_4

    :goto_2
    const-string v0, "Must provide at least one request type"

    invoke-static {v2, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2656
    invoke-static {p3}, Lcom/google/android/gms/games/service/l;->b(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 2657
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v1

    .line 2658
    invoke-static {v1, p1, p2, p4, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IILjava/util/ArrayList;)V

    .line 2660
    return-void

    :cond_2
    move v0, v1

    .line 2648
    goto :goto_0

    :cond_3
    move v0, v1

    .line 2651
    goto :goto_1

    :cond_4
    move v2, v1

    .line 2654
    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;IIZZ)V
    .locals 2

    .prologue
    .line 1117
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1118
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 1120
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1121
    packed-switch p3, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    const-string v1, "Invalid game collection type"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1123
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->m(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1124
    invoke-static {v0, p1, p2, p3, p4}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IIZ)V

    .line 1126
    return-void

    .line 1121
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;II[Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3368
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3369
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3370
    const-string v0, "Invited players must not be null"

    invoke-static {p4, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3371
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 3372
    if-lez p2, :cond_2

    move v0, v1

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Variant must be a positive integer if provided! Input was "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3375
    :cond_0
    if-eqz p5, :cond_1

    .line 3376
    const-string v0, "min_automatch_players"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 3377
    const-string v0, "max_automatch_players"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 3378
    if-ltz v3, :cond_3

    move v0, v1

    :goto_1
    const-string v5, "Min players must be a positive integer"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3380
    if-gt v3, v4, :cond_4

    :goto_2
    const-string v0, "Min players must be less than or equal to max players"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3383
    :cond_1
    invoke-direct {p0, v2}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 3384
    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;II[Ljava/lang/String;Landroid/os/Bundle;)V

    .line 3386
    return-void

    :cond_2
    move v0, v2

    .line 3372
    goto :goto_0

    :cond_3
    move v0, v2

    .line 3378
    goto :goto_1

    :cond_4
    move v1, v2

    .line 3380
    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;ILjava/lang/String;[Ljava/lang/String;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 804
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->K()V

    .line 805
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 807
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 809
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-direct {p0, v1, v1, p5, v0}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 811
    new-instance v1, Lcom/google/android/gms/games/a/s;

    invoke-direct {v1, p3, p2, p4}, Lcom/google/android/gms/games/a/s;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    .line 813
    invoke-static {v0, p1, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/a/s;)V

    .line 814
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;IZZ)V
    .locals 2

    .prologue
    .line 1981
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1982
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1983
    if-lez p2, :cond_0

    const/16 v0, 0x19

    if-gt p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Page size must be between 1 and 25"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1985
    invoke-direct {p0, p4}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V

    .line 1987
    return-void

    .line 1983
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;I[I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3493
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3495
    const-string v2, "Must provide a valid callback object"

    invoke-static {p1, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3496
    if-eqz p2, :cond_0

    if-ne p2, v0, :cond_1

    :cond_0
    :goto_0
    const-string v2, "Invalid match sort order!"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3499
    invoke-static {p3}, Lcom/google/android/gms/games/service/l;->b([I)V

    .line 3501
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 3502
    invoke-static {v0, p1, p2, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;I[I)V

    .line 3504
    return-void

    :cond_1
    move v0, v1

    .line 3496
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;J)V
    .locals 8

    .prologue
    .line 1262
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1263
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1265
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v1

    .line 1266
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    move-wide v4, p2

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/games/internal/dr;)V

    .line 1268
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;JLjava/lang/String;)V
    .locals 8

    .prologue
    .line 1425
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1426
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 1427
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1428
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide a valid Game ID, or null for \'all games\'"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1431
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v1

    .line 1432
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    move-object v3, p4

    move-wide v4, p2

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/games/internal/dr;)V

    .line 1434
    return-void

    .line 1428
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Landroid/os/Bundle;II)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1589
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1590
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1591
    const-string v0, "Must provide a non-null bundle!"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1592
    packed-switch p4, :pswitch_data_0

    move v0, v2

    :goto_0
    const-string v3, "Unrecognized page direction %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1594
    sget-object v0, Lcom/google/android/gms/games/c/a;->n:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v3

    .line 1595
    if-lez p3, :cond_1

    if-gt p3, v3, :cond_1

    move v0, v1

    :goto_1
    const-string v4, "Max results must be between 1 and %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v4, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1597
    new-instance v1, Lcom/google/android/gms/games/e/g;

    invoke-direct {v1, p2}, Lcom/google/android/gms/games/e/g;-><init>(Landroid/os/Bundle;)V

    .line 1602
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/gms/games/e/g;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1604
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/e/g;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->m(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1608
    :goto_2
    invoke-static {v0, p1, v1, p3, p4}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/e/g;II)V

    .line 1610
    return-void

    :pswitch_0
    move v0, v1

    .line 1592
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1595
    goto :goto_1

    .line 1606
    :cond_2
    invoke-direct {p0, v2}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    goto :goto_2

    .line 1592
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Landroid/os/IBinder;I[Ljava/lang/String;Landroid/os/Bundle;ZJ)V
    .locals 17

    .prologue
    .line 2806
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2807
    const-string v2, "Must provide a valid callback object"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2808
    const-string v2, "Invited players must not be null"

    move-object/from16 v0, p4

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2809
    const/4 v2, -0x1

    move/from16 v0, p3

    if-eq v0, v2, :cond_0

    .line 2810
    if-lez p3, :cond_2

    const/4 v2, 0x1

    :goto_0
    const-string v3, "Variant must be a positive integer if provided! Input was %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2813
    :cond_0
    if-eqz p5, :cond_1

    .line 2814
    const-string v2, "min_automatch_players"

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 2815
    const-string v2, "max_automatch_players"

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 2816
    if-ltz v3, :cond_3

    const/4 v2, 0x1

    :goto_1
    const-string v5, "Min players must be a positive integer"

    invoke-static {v2, v5}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2818
    if-gt v3, v4, :cond_4

    const/4 v2, 0x1

    :goto_2
    const-string v3, "Min players must be less than or equal to max players"

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2823
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v3}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/i/a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 2825
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/games/service/l;->P()Lcom/google/android/gms/games/service/ai;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/games/service/l;->e()Ljava/lang/String;

    move-result-object v15

    new-instance v2, Lcom/google/android/gms/games/service/statemachine/roomclient/z;

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move/from16 v11, p6

    move-wide/from16 v12, p7

    invoke-direct/range {v2 .. v15}, Lcom/google/android/gms/games/service/statemachine/roomclient/z;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/games/internal/dr;Landroid/os/IBinder;I[Ljava/lang/String;Landroid/os/Bundle;ZJLjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/service/ai;->a(Lcom/google/android/gms/games/service/statemachine/roomclient/ag;)V

    .line 2840
    return-void

    .line 2810
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 2816
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 2818
    :cond_4
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Landroid/os/IBinder;Ljava/lang/String;ZJ)V
    .locals 13

    .prologue
    .line 2847
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2848
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2849
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Room ID must not be null or empty"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2853
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/i/a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 2855
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->P()Lcom/google/android/gms/games/service/ai;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v4, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->e()Ljava/lang/String;

    move-result-object v11

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ai;

    move-object v5, p1

    move-object v6, p2

    move/from16 v7, p4

    move-wide/from16 v8, p5

    move-object/from16 v12, p3

    invoke-direct/range {v1 .. v12}, Lcom/google/android/gms/games/service/statemachine/roomclient/ai;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/games/internal/dr;Landroid/os/IBinder;ZJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/ai;->a(Lcom/google/android/gms/games/service/statemachine/roomclient/ag;)V

    .line 2868
    return-void

    .line 2849
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1939
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/games/service/l;->f(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Z)V

    .line 1940
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2716
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 2717
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 2719
    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Must provide at least one request type"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2720
    const/4 v0, 0x0

    new-array v2, v1, [Ljava/lang/String;

    invoke-direct {p0, v0, p2, v1, v2}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2722
    invoke-virtual {v0}, Lcom/google/android/gms/games/a/au;->h()Z

    move-result v1

    const-string v2, "Attempting to load requests for another player"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2724
    invoke-static {v0, p1, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->d(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;I)V

    .line 2725
    return-void

    :cond_0
    move v0, v1

    .line 2719
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IIIZ)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1550
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1551
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1552
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Leaderboard ID must not be null or empty"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1554
    invoke-static {p3}, Lcom/google/android/gms/games/internal/b/i;->b(I)Z

    move-result v0

    const-string v3, "Unrecognized time span %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1556
    invoke-static {p4}, Lcom/google/android/gms/games/internal/b/c;->b(I)Z

    move-result v0

    const-string v3, "Unrecognized leaderboard collection %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1558
    sget-object v0, Lcom/google/android/gms/games/c/a;->n:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v3

    .line 1559
    if-lez p5, :cond_1

    if-gt p5, v3, :cond_1

    move v0, v1

    :goto_1
    const-string v4, "Max results must be between 1 and %d"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v4, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1562
    invoke-direct {p0, p6}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;III)V

    .line 1564
    return-void

    :cond_0
    move v0, v2

    .line 1552
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1559
    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 713
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 715
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Achievement ID must not be null or empty"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 717
    if-lez p3, :cond_1

    :goto_1
    const-string v0, "Number of steps must be greater than 0"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 719
    new-instance v0, Lcom/google/android/gms/games/internal/el;

    invoke-direct {v0, p5, p4}, Lcom/google/android/gms/games/internal/el;-><init>(Landroid/os/Bundle;Landroid/os/IBinder;)V

    .line 720
    invoke-direct {p0, v2}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v1

    .line 721
    invoke-static {v1, p1, p2, p3, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;ILcom/google/android/gms/games/internal/el;)V

    .line 723
    return-void

    :cond_0
    move v0, v2

    .line 715
    goto :goto_0

    :cond_1
    move v1, v2

    .line 717
    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZ)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2166
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 2167
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2169
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Game ID must not be empty"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2171
    invoke-static {p2}, Lcom/google/android/gms/common/util/am;->b(Ljava/lang/String;)Z

    move-result v0

    const-string v3, "Game ID must be numeric!"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2173
    packed-switch p3, :pswitch_data_0

    move v0, v2

    :goto_1
    const-string v3, "Unrecognized ROP level %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2178
    const/4 v0, 0x0

    new-array v1, v1, [Ljava/lang/String;

    const-string v3, "https://www.googleapis.com/auth/plus.circles.members"

    aput-object v3, v1, v2

    invoke-direct {p0, p2, v0, p4, v1}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2180
    invoke-static {v0, p1, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;I)V

    .line 2181
    return-void

    :cond_0
    move v0, v2

    .line 2169
    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 2173
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZZ)V
    .locals 6

    .prologue
    const/16 v5, 0x32

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1157
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1158
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1160
    if-gt p3, v5, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "We don\'t handle loading more than %d games simultaneously"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1165
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->e()Ljava/lang/String;

    move-result-object p2

    .line 1170
    :cond_0
    const/4 v0, 0x0

    new-array v1, v1, [Ljava/lang/String;

    const-string v3, "https://www.googleapis.com/auth/drive.appdata"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, p2, p5, v1}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1172
    invoke-static {v0, p1, p3, p4}, Lcom/google/android/gms/games/service/GamesIntentService;->e(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V

    .line 1174
    return-void

    :cond_1
    move v0, v2

    .line 1160
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZZZZ)V
    .locals 7

    .prologue
    .line 1132
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1133
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1135
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1136
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->m(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p7

    .line 1137
    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZZZ)V

    .line 1139
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;I[I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 3569
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 3570
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 3572
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3575
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Must provide a valid Game ID, or null for \'all games\'"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3577
    if-eqz p3, :cond_1

    if-ne p3, v2, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    const-string v0, "Invalid match sort order!"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3580
    invoke-static {p4}, Lcom/google/android/gms/games/service/l;->b([I)V

    .line 3582
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/service/l;->m(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 3583
    invoke-static {v0, p1, p3, p4}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;I[I)V

    .line 3585
    return-void

    :cond_3
    move v0, v1

    .line 3575
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;J)V
    .locals 7

    .prologue
    .line 1496
    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/games/service/l;->a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;JLjava/lang/String;)V

    .line 1497
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;JLjava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1502
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1504
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Leaderboard ID must not be null or empty"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1506
    if-eqz p5, :cond_0

    .line 1507
    invoke-static {}, Lcom/google/android/gms/games/service/k;->d()Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Score tags must be no more than 64 URI safe characters. Input was "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1512
    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-static/range {v1 .. v6}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;JLjava/lang/String;)V

    .line 1514
    return-void

    :cond_1
    move v0, v1

    .line 1504
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 687
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 689
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Achievement ID must not be null or empty"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 691
    new-instance v0, Lcom/google/android/gms/games/internal/el;

    invoke-direct {v0, p4, p3}, Lcom/google/android/gms/games/internal/el;-><init>(Landroid/os/Bundle;Landroid/os/IBinder;)V

    .line 692
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v1

    .line 693
    invoke-static {v1, p1, p2, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Lcom/google/android/gms/games/internal/el;)V

    .line 695
    return-void

    :cond_0
    move v0, v1

    .line 689
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/SnapshotMetadataChangeEntity;Lcom/google/android/gms/drive/Contents;)V
    .locals 6

    .prologue
    .line 3139
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3140
    new-instance v5, Lcom/google/android/gms/drive/internal/an;

    invoke-direct {v5, p4}, Lcom/google/android/gms/drive/internal/an;-><init>(Lcom/google/android/gms/drive/Contents;)V

    .line 3142
    sget-object v0, Lcom/google/android/gms/drive/b;->f:Lcom/google/android/gms/common/api/c;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->a(Lcom/google/android/gms/common/api/c;)Z

    move-result v0

    const-string v1, "Must include Drive.SCOPE_APPFOLDER to use snapshots!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3144
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->Q()V

    .line 3145
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3146
    const-string v0, "Must provide a non-empty snapshot ID"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 3147
    const-string v0, "Must provide a non-null metadata change"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3148
    const-string v0, "Must provide non-null contents"

    invoke-static {p4, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3149
    invoke-direct {p0, p3, v5}, Lcom/google/android/gms/games/service/l;->a(Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)V

    .line 3152
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/b;->a()V

    .line 3153
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/service/b;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)V

    .line 3155
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1663
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/gms/games/service/l;->b(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1665
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1616
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1617
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1618
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v2, "External leaderboard ID must not be empty or null"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1620
    invoke-static {p4}, Lcom/google/android/gms/games/internal/b/i;->b(I)Z

    move-result v0

    const-string v2, "Must provide a valid time span"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 1622
    invoke-static {p5}, Lcom/google/android/gms/games/internal/b/c;->b(I)Z

    move-result v0

    const-string v2, "Must provide a valid leaderboard collection"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 1626
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1627
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->e()Ljava/lang/String;

    move-result-object p2

    .line 1629
    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/au;->c()Lcom/google/android/gms/games/a/av;

    move-result-object v0

    iput-object p2, v0, Lcom/google/android/gms/games/a/av;->f:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1634
    invoke-static {v0, p1, p3, p4, p5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;II)V

    .line 1636
    return-void

    :cond_1
    move v0, v1

    .line 1618
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2665
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 2666
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 2668
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Must provide a valid Game ID, or null for \'all games\'"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2673
    if-eqz p6, :cond_1

    if-ne p6, v2, :cond_4

    :cond_1
    move v0, v2

    :goto_1
    const-string v3, "Invalid request sort order!"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2676
    if-eqz p4, :cond_2

    if-ne p4, v2, :cond_5

    :cond_2
    move v0, v2

    :goto_2
    const-string v3, "Invalid request direction!"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2679
    if-eqz p5, :cond_6

    move v0, v2

    :goto_3
    const-string v3, "Must provide at least one request type"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2681
    invoke-static {p5}, Lcom/google/android/gms/games/service/l;->b(I)Ljava/util/ArrayList;

    move-result-object v3

    .line 2682
    new-array v0, v1, [Ljava/lang/String;

    invoke-direct {p0, p2, p3, v1, v0}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v4

    .line 2687
    const-string v5, "GamesServiceBroker"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v0, "Input player null: "

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p3, :cond_7

    move v0, v2

    :goto_4
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2688
    const-string v0, "GamesServiceBroker"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "IsHeadless: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v6, p0, Lcom/google/android/gms/games/service/l;->i:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2689
    iget-object v0, v4, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    .line 2690
    iget-object v5, v4, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    .line 2691
    const-string v6, "GamesServiceBroker"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Null target: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v0, :cond_8

    move v0, v2

    :goto_5
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2692
    const-string v0, "GamesServiceBroker"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Null current: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v5, :cond_9

    :goto_6
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2693
    const-string v0, "GamesServiceBroker"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Targeting current: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/google/android/gms/games/a/au;->h()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2696
    invoke-virtual {v4}, Lcom/google/android/gms/games/a/au;->h()Z

    move-result v0

    const-string v1, "Attempting to load requests for another player"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2698
    invoke-static {v4, p1, p4, p6, v3}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IILjava/util/ArrayList;)V

    .line 2700
    return-void

    :cond_3
    move v0, v1

    .line 2671
    goto/16 :goto_0

    :cond_4
    move v0, v1

    .line 2673
    goto/16 :goto_1

    :cond_5
    move v0, v1

    .line 2676
    goto/16 :goto_2

    :cond_6
    move v0, v1

    .line 2679
    goto/16 :goto_3

    :cond_7
    move v0, v1

    .line 2687
    goto/16 :goto_4

    :cond_8
    move v0, v1

    .line 2691
    goto :goto_5

    :cond_9
    move v2, v1

    .line 2692
    goto :goto_6
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;IIIZ)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1687
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1688
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1690
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1691
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Game ID must not be empty"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1693
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "Leaderboard ID must not be null or empty"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1695
    invoke-static {p4}, Lcom/google/android/gms/games/internal/b/i;->b(I)Z

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unrecognized time span "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1697
    invoke-static {p5}, Lcom/google/android/gms/games/internal/b/c;->b(I)Z

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unrecognized leaderboard collection "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1699
    sget-object v0, Lcom/google/android/gms/games/c/a;->n:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v0

    .line 1700
    if-lez p6, :cond_2

    if-gt p6, v0, :cond_2

    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Max results must be between 1 and "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1703
    const/4 v0, 0x0

    new-array v1, v2, [Ljava/lang/String;

    invoke-direct {p0, p2, v0, p7, v1}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    move-object v1, p1

    move-object v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    .line 1705
    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;III)V

    .line 1707
    return-void

    :cond_0
    move v0, v2

    .line 1691
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1693
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1700
    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 4

    .prologue
    .line 2067
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->L()V

    .line 2068
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2070
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2073
    const/4 v0, 0x0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "https://www.googleapis.com/auth/plus.circles.read"

    aput-object v3, v1, v2

    invoke-direct {p0, p3, v0, p6, v1}, Lcom/google/android/gms/games/service/l;->a(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2075
    invoke-static {v0, p1, p2, p4, p5}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZ)V

    .line 2077
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/SnapshotMetadataChangeEntity;Lcom/google/android/gms/drive/Contents;)V
    .locals 7

    .prologue
    .line 3186
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3187
    new-instance v6, Lcom/google/android/gms/drive/internal/an;

    invoke-direct {v6, p5}, Lcom/google/android/gms/drive/internal/an;-><init>(Lcom/google/android/gms/drive/Contents;)V

    .line 3189
    sget-object v0, Lcom/google/android/gms/drive/b;->f:Lcom/google/android/gms/common/api/c;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->a(Lcom/google/android/gms/common/api/c;)Z

    move-result v0

    const-string v1, "Must include Drive.SCOPE_APPFOLDER to use snapshots!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3191
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->Q()V

    .line 3192
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3193
    const-string v0, "Must provide a non-empty conflict ID"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 3194
    const-string v0, "Must provide a non-empty snapshot ID"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 3195
    const-string v0, "Must provide a non-null metadata change"

    invoke-static {p4, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3196
    const-string v0, "Must provide non-null contents"

    invoke-static {p5, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3197
    invoke-direct {p0, p4, v6}, Lcom/google/android/gms/games/service/l;->a(Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)V

    .line 3200
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/b;->a()V

    .line 3201
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/service/b;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)V

    .line 3204
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 754
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 755
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 758
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 759
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Game ID must not be empty"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 761
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Player ID must not be empty"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 763
    new-array v0, v2, [Ljava/lang/String;

    invoke-direct {p0, p3, p2, p4, v0}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 765
    invoke-static {v0, p1}, Lcom/google/android/gms/games/service/GamesIntentService;->f(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V

    .line 766
    return-void

    :cond_0
    move v0, v2

    .line 759
    goto :goto_0

    :cond_1
    move v1, v2

    .line 761
    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;[IIZ)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2376
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 2377
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 2378
    const-string v2, "Must provide a valid callback object"

    invoke-static {p1, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2379
    invoke-static {p4}, Lcom/google/android/gms/games/internal/b/f;->a([I)Z

    move-result v2

    const-string v3, "Invalid quest state provided."

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2381
    if-eq p5, v0, :cond_0

    if-nez p5, :cond_1

    :cond_0
    :goto_0
    const-string v2, "Must provide a valid quest sort order."

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2384
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->n:Lcom/google/android/gms/games/internal/d/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/d/d;->b()V

    .line 2385
    new-array v0, v1, [Ljava/lang/String;

    invoke-direct {p0, p2, p3, p6, v0}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2387
    invoke-static {v0, p1, p4, p5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[II)V

    .line 2388
    return-void

    :cond_1
    move v0, v1

    .line 2381
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2627
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 2628
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 2630
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2631
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Must provide a valid game ID"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2633
    invoke-static {p4}, Lcom/google/android/gms/games/service/l;->b([Ljava/lang/String;)V

    .line 2635
    new-array v0, v1, [Ljava/lang/String;

    invoke-direct {p0, p2, p3, v1, v0}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2637
    invoke-virtual {v0}, Lcom/google/android/gms/games/a/au;->h()Z

    move-result v1

    const-string v2, "Attempting to load requests for another player"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2639
    invoke-static {v0, p1, p4}, Lcom/google/android/gms/games/service/GamesIntentService;->d(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V

    .line 2640
    return-void

    :cond_0
    move v0, v1

    .line 2631
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2393
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2394
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 2395
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2396
    const-string v0, "Must provide a valid quest id"

    invoke-static {p4, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2397
    array-length v0, p4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Must provide a valid quest id"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2398
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->n:Lcom/google/android/gms/games/internal/d/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/d/d;->b()V

    .line 2399
    new-array v0, v1, [Ljava/lang/String;

    invoke-direct {p0, p2, p3, p5, v0}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2401
    invoke-static {v0, p1, p4}, Lcom/google/android/gms/games/service/GamesIntentService;->e(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V

    .line 2402
    return-void

    :cond_0
    move v0, v1

    .line 2397
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 1753
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->L()V

    .line 1754
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 1756
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1757
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Game ID must not be empty"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1760
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/service/l;->l(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1761
    invoke-static {v0, p1, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->c(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Z)V

    .line 1762
    return-void

    .line 1757
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;ZI)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 3105
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3107
    sget-object v0, Lcom/google/android/gms/drive/b;->f:Lcom/google/android/gms/common/api/c;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->a(Lcom/google/android/gms/common/api/c;)Z

    move-result v0

    const-string v2, "Must include Drive.SCOPE_APPFOLDER to use snapshots!"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3109
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->Q()V

    .line 3110
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3111
    const-string v0, "Must provide a non empty fileName!"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 3112
    invoke-static {}, Lcom/google/android/gms/games/service/k;->e()Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    const-string v2, "Must provide a valid file name!"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3114
    packed-switch p4, :pswitch_data_0

    :pswitch_0
    move v0, v1

    :goto_0
    const-string v2, "Must provide a valid conflict policy!"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3118
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/b;->a()V

    .line 3119
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/service/b;Ljava/lang/String;ZI)V

    .line 3121
    return-void

    .line 3114
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;[BLjava/lang/String;[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3414
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3415
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3416
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "Match ID must not be empty"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3417
    if-eqz p4, :cond_0

    .line 3418
    invoke-static {p4}, Lcom/google/android/gms/games/multiplayer/k;->a(Ljava/lang/String;)Z

    move-result v0

    .line 3419
    const-string v3, "Invalid participant ID %s"

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p4, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 3421
    :cond_0
    if-eqz p3, :cond_1

    .line 3422
    array-length v0, p3

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->i()I

    move-result v3

    if-gt v0, v3, :cond_3

    move v0, v1

    :goto_1
    const-string v3, "Match data is too large (%d bytes). The maximum is %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    array-length v5, p3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->i()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 3426
    :cond_1
    invoke-direct {p0, v2}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p3

    move-object v5, p5

    .line 3427
    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V

    .line 3429
    return-void

    :cond_2
    move v0, v2

    .line 3416
    goto :goto_0

    :cond_3
    move v0, v2

    .line 3422
    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3460
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3461
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3462
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Match ID must not be empty"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3463
    if-eqz p3, :cond_0

    .line 3464
    array-length v0, p3

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->i()I

    move-result v3

    if-gt v0, v3, :cond_2

    move v0, v1

    :goto_1
    const-string v3, "Match data is too large (%d bytes). The maximum is %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    array-length v5, p3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->i()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 3468
    :cond_0
    invoke-direct {p0, v2}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 3469
    invoke-static {v0, p1, p2, p3, p4}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V

    .line 3471
    return-void

    :cond_1
    move v0, v2

    .line 3462
    goto :goto_0

    :cond_2
    move v0, v2

    .line 3464
    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;[I)V
    .locals 1

    .prologue
    .line 3562
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/google/android/gms/games/service/l;->a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;I[I)V

    .line 3564
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;[Ljava/lang/String;I[BI)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2583
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2585
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->L()V

    .line 2587
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2588
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Must provide a valid game ID"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2590
    invoke-static {p3}, Lcom/google/android/gms/games/service/l;->b([Ljava/lang/String;)V

    .line 2591
    packed-switch p4, :pswitch_data_0

    move v0, v2

    :goto_1
    const-string v3, "Must provide a valid request type"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2593
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->t()I

    move-result v0

    if-gt p6, v0, :cond_2

    if-gtz p6, :cond_0

    const/4 v0, -0x1

    if-ne p6, v0, :cond_2

    :cond_0
    move v0, v1

    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Request lifetime days must be <= "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->t()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and greater than zero. Use -1 for server default."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2599
    if-eqz p5, :cond_3

    array-length v0, p5

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->s()I

    move-result v3

    if-gt v0, v3, :cond_3

    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Payload must be non null and at most "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->s()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2603
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/service/l;->l(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    move-object v1, p1

    move v2, p4

    move v3, p6

    move-object v4, p5

    move-object v5, p3

    .line 2604
    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;II[B[Ljava/lang/String;)V

    .line 2606
    return-void

    :cond_1
    move v0, v2

    .line 2588
    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 2591
    goto :goto_1

    :cond_2
    move v0, v2

    .line 2593
    goto :goto_2

    :cond_3
    move v1, v2

    .line 2599
    goto :goto_3

    .line 2591
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Z)V
    .locals 1

    .prologue
    .line 678
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 679
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 680
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 681
    invoke-static {v0, p1}, Lcom/google/android/gms/games/service/GamesIntentService;->e(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V

    .line 682
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;ZLandroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1874
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->K()V

    .line 1875
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 1877
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1878
    invoke-virtual {p3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1880
    invoke-static {v0}, Lcom/google/android/gms/games/internal/b/e;->a(Ljava/lang/String;)I

    move-result v1

    .line 1881
    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    const/4 v1, 0x1

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown channel "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 1886
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    .line 1887
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-static {v1, v0, p1, p2, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;ZLandroid/os/Bundle;)V

    .line 1889
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;Z[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1049
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1050
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1051
    const-string v0, "eventIds must not be null"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1052
    array-length v0, p3

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "must specify at least one event id"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1053
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->n:Lcom/google/android/gms/games/internal/d/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/d/d;->b()V

    .line 1054
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1055
    invoke-static {v0, p1, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V

    .line 1056
    return-void

    .line 1052
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;[I)V
    .locals 1

    .prologue
    .line 3486
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/gms/games/service/l;->a(Lcom/google/android/gms/games/internal/dr;I[I)V

    .line 3488
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;[IIZ)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2347
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2348
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2349
    array-length v0, p2

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "Must provide at least one quest state"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2351
    invoke-static {p2}, Lcom/google/android/gms/games/internal/b/f;->a([I)Z

    move-result v0

    const-string v3, "Invalid quest state provided."

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2353
    if-eq p3, v1, :cond_0

    if-nez p3, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    const-string v0, "Must provide a valid quest sort order."

    invoke-static {v2, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2356
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->n:Lcom/google/android/gms/games/internal/d/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/d/d;->b()V

    .line 2357
    invoke-direct {p0, p4}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2358
    invoke-static {v0, p1, p2, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[II)V

    .line 2359
    return-void

    :cond_2
    move v0, v2

    .line 2349
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2610
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2611
    invoke-static {p2}, Lcom/google/android/gms/games/service/l;->b([Ljava/lang/String;)V

    .line 2612
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->l(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2613
    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->c(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V

    .line 2614
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 2364
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2365
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2366
    const-string v0, "Must provide a valid quest id"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2367
    array-length v0, p2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide a valid quest id"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2368
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->n:Lcom/google/android/gms/games/internal/d/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/d/d;->b()V

    .line 2369
    invoke-direct {p0, p3}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2370
    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->e(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V

    .line 2371
    return-void

    .line 2367
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final a(Ljava/io/PrintWriter;)V
    .locals 2

    .prologue
    .line 585
    const-string v0, "  GamesServiceInstance:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 586
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "    Game ID: "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 588
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "    Package name: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 589
    return-void

    .line 586
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1300
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1301
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Invitation ID must not be null or empty"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1303
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1304
    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;I)V

    .line 1306
    return-void

    :cond_0
    move v0, v1

    .line 1301
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2417
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2418
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Must provide a valid quest ID"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2420
    new-instance v0, Lcom/google/android/gms/games/internal/el;

    invoke-direct {v0, p3, p2}, Lcom/google/android/gms/games/internal/el;-><init>(Landroid/os/Bundle;Landroid/os/IBinder;)V

    .line 2421
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v1

    .line 2422
    invoke-static {v1, p1, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Lcom/google/android/gms/games/internal/el;)V

    .line 2424
    return-void

    :cond_0
    move v0, v1

    .line 2418
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 643
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->L()V

    .line 644
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 646
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Game package name must not be empty"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 648
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Account name must not be empty"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 651
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/common/util/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 652
    return-void

    :cond_0
    move v0, v2

    .line 646
    goto :goto_0

    :cond_1
    move v1, v2

    .line 648
    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1344
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1345
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 1347
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Must decline invite on behalf of a valid game"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1349
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Invitation ID must not be null or empty"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1352
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/l;->m(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1353
    invoke-static {v0, p2, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;I)V

    .line 1355
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Z)V

    .line 1357
    return-void

    :cond_0
    move v0, v2

    .line 1347
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1349
    goto :goto_1
.end method

.method public final a(Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 442
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1825
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1826
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 1827
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Z)V

    .line 1828
    return-void
.end method

.method public final a([Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 818
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->K()V

    .line 819
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 820
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)V

    .line 821
    return-void
.end method

.method public final b(IIZ)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2770
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2774
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SELECT_OPPONENTS_REAL_TIME"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2775
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2776
    const-string v1, "com.google.android.gms.games.MIN_SELECTIONS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2777
    const-string v1, "com.google.android.gms.games.MAX_SELECTIONS"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2778
    const-string v1, "com.google.android.gms.games.SHOW_AUTOMATCH"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2779
    const-string v1, "com.google.android.gms.games.MULTIPLAYER_TYPE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2781
    iget v1, p0, Lcom/google/android/gms/games/service/l;->d:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final b()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 829
    sget-object v1, Lcom/google/android/gms/games/service/k;->e:Ljava/util/HashMap;

    monitor-enter v1

    .line 831
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/service/k;->e:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 832
    if-eqz v0, :cond_0

    const-string v2, "com.google.android.gms.games.ACCOUNT_KEY"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 835
    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    .line 836
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.google.android.gms.games.ACCOUNT_KEY"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 837
    const/4 v0, 0x0

    .line 840
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 841
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2931
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2932
    invoke-static {p1}, Lcom/google/android/gms/games/multiplayer/k;->a(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "Invalid participant ID %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2934
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->P()Lcom/google/android/gms/games/service/ai;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ac;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ac;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/ai;->a(Lcom/google/android/gms/games/service/statemachine/roomclient/ac;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(J)V
    .locals 3

    .prologue
    .line 1274
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v0

    .line 1275
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;J)V

    .line 1277
    return-void
.end method

.method public final b(JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 3603
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 3606
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide a valid Game ID, or null for \'all games\'"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3609
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v0

    .line 3610
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3, p1, p2}, Lcom/google/android/gms/games/a/am;->b(Ljava/lang/String;Ljava/lang/String;J)V

    .line 3612
    return-void

    .line 3606
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/content/ContentValues;)V
    .locals 0

    .prologue
    .line 424
    iput-object p1, p0, Lcom/google/android/gms/games/service/l;->k:Landroid/content/ContentValues;

    .line 425
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/internal/dr;)V
    .locals 1

    .prologue
    .line 1519
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/games/service/l;->b(Lcom/google/android/gms/games/internal/dr;Z)V

    .line 1520
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/internal/dr;IZZ)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2015
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 2016
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2018
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2019
    if-lez p2, :cond_0

    const/16 v0, 0x19

    if-gt p2, v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Page size must be between 1 and 25"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2024
    new-array v0, v1, [Ljava/lang/String;

    const-string v1, "https://www.googleapis.com/auth/plus.circles.read"

    aput-object v1, v0, v2

    invoke-direct {p0, v4, v4, p4, v0}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2026
    invoke-static {v0, p1, p2, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V

    .line 2028
    return-void

    :cond_0
    move v0, v2

    .line 2019
    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/games/internal/dr;J)V
    .locals 8

    .prologue
    .line 3340
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3341
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3343
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v1

    .line 3344
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    move-wide v4, p2

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/games/a/am;->b(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/games/internal/dr;)V

    .line 3346
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/internal/dr;JLjava/lang/String;)V
    .locals 8

    .prologue
    .line 3590
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 3591
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 3592
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3593
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide a valid Game ID, or null for \'all games\'"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3596
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v1

    .line 3597
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    move-object v3, p4

    move-wide v4, p2

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/games/a/am;->b(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/games/internal/dr;)V

    .line 3599
    return-void

    .line 3593
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1533
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/games/service/l;->c(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Z)V

    .line 1534
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2199
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 2200
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2201
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2202
    if-lez p3, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Must load at least 1 XP event"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2205
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->e()Ljava/lang/String;

    move-result-object p2

    .line 2208
    :cond_0
    const/4 v0, 0x0

    new-array v2, v1, [Ljava/lang/String;

    invoke-direct {p0, v0, p2, v1, v2}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2210
    invoke-static {v0, p1, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->c(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;I)V

    .line 2211
    return-void

    :cond_1
    move v0, v1

    .line 2202
    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IIIZ)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1570
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1571
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1572
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Leaderboard ID must not be null or empty"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1574
    invoke-static {p3}, Lcom/google/android/gms/games/internal/b/i;->b(I)Z

    move-result v0

    const-string v3, "Unrecognized time span %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1576
    invoke-static {p4}, Lcom/google/android/gms/games/internal/b/c;->b(I)Z

    move-result v0

    const-string v3, "Unrecognized leaderboard collection %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1578
    sget-object v0, Lcom/google/android/gms/games/c/a;->n:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v3

    .line 1579
    if-lez p5, :cond_1

    if-gt p5, v3, :cond_1

    move v0, v1

    :goto_1
    const-string v4, "Max results must be between 1 and %d"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v4, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1582
    invoke-direct {p0, p6}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;III)V

    .line 1584
    return-void

    :cond_0
    move v0, v2

    .line 1572
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1579
    goto :goto_1
.end method

.method public final b(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 728
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 730
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Achievement ID must not be null or empty"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 732
    if-lez p3, :cond_1

    :goto_1
    const-string v0, "Number of steps must be greater than 0"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 734
    new-instance v0, Lcom/google/android/gms/games/internal/el;

    invoke-direct {v0, p5, p4}, Lcom/google/android/gms/games/internal/el;-><init>(Landroid/os/Bundle;Landroid/os/IBinder;)V

    .line 735
    invoke-direct {p0, v2}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v1

    .line 736
    invoke-static {v1, p1, p2, p3, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;ILcom/google/android/gms/games/internal/el;)V

    .line 738
    return-void

    :cond_0
    move v0, v2

    .line 730
    goto :goto_0

    :cond_1
    move v1, v2

    .line 732
    goto :goto_1
.end method

.method public final b(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZ)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1391
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1392
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 1394
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1395
    if-eqz p3, :cond_0

    if-ne p3, v2, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Invalid invitation sort order!"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1401
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    const-string v0, "Must provide a valid Game ID, or null for \'all games\'"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1404
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/service/l;->m(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1405
    invoke-static {v0, p1, p3, p4}, Lcom/google/android/gms/games/service/GamesIntentService;->d(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V

    .line 1407
    return-void

    :cond_3
    move v0, v1

    .line 1395
    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZZ)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3012
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->L()V

    .line 3013
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3015
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Query must not be empty"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3018
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    const/4 v3, 0x0

    new-array v1, v1, [Ljava/lang/String;

    const-string v4, "https://www.googleapis.com/auth/plus.circles.read"

    aput-object v4, v1, v2

    invoke-direct {p0, v0, v3, p5, v1}, Lcom/google/android/gms/games/service/l;->a(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 3020
    invoke-static {v0, p1, p2, p3, p4}, Lcom/google/android/gms/games/service/GamesIntentService;->d(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZ)V

    .line 3022
    return-void

    :cond_0
    move v0, v2

    .line 3015
    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 700
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 702
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Achievement ID must not be null or empty"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 704
    new-instance v0, Lcom/google/android/gms/games/internal/el;

    invoke-direct {v0, p4, p3}, Lcom/google/android/gms/games/internal/el;-><init>(Landroid/os/Bundle;Landroid/os/IBinder;)V

    .line 705
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v1

    .line 706
    invoke-static {v1, p1, p2, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Lcom/google/android/gms/games/internal/el;)V

    .line 708
    return-void

    :cond_0
    move v0, v1

    .line 702
    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 743
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 744
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->e()Ljava/lang/String;

    move-result-object p2

    .line 747
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/gms/games/service/l;->a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 749
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;IIIZ)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1713
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1714
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1716
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1717
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Game ID must not be empty"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1719
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "Leaderboard ID must not be null or empty"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1721
    invoke-static {p4}, Lcom/google/android/gms/games/internal/b/i;->b(I)Z

    move-result v0

    const-string v3, "Unrecognized time span %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1723
    invoke-static {p5}, Lcom/google/android/gms/games/internal/b/c;->b(I)Z

    move-result v0

    const-string v3, "Unrecognized leaderboard collection %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1725
    sget-object v0, Lcom/google/android/gms/games/c/a;->n:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v3

    .line 1726
    if-lez p6, :cond_2

    if-gt p6, v3, :cond_2

    move v0, v1

    :goto_2
    const-string v4, "Max results must be between 1 and %d"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v4, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1729
    const/4 v0, 0x0

    new-array v1, v2, [Ljava/lang/String;

    invoke-direct {p0, p2, v0, p7, v1}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    move-object v1, p1

    move-object v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    .line 1731
    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;III)V

    .line 1733
    return-void

    :cond_0
    move v0, v2

    .line 1717
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1719
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1726
    goto :goto_2
.end method

.method public final b(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 2

    .prologue
    .line 2050
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 2051
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2053
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2054
    const/4 v0, 0x0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {p0, p3, v0, p6, v1}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2056
    invoke-static {v0, p1, p2, p4, p5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZ)V

    .line 2058
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1670
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1671
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1673
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1674
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Game ID must not be empty"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1676
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Leaderboard ID must not be null or empty"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1678
    const/4 v0, 0x0

    new-array v1, v2, [Ljava/lang/String;

    invoke-direct {p0, p2, v0, p4, v1}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1680
    invoke-static {v0, p1, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    .line 1681
    return-void

    :cond_0
    move v0, v2

    .line 1674
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1676
    goto :goto_1
.end method

.method public final b(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 1384
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/google/android/gms/games/service/l;->b(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZ)V

    .line 1386
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/internal/dr;Z)V
    .locals 2

    .prologue
    .line 1524
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1525
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    .line 1528
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2618
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2619
    invoke-static {p2}, Lcom/google/android/gms/games/service/l;->b([Ljava/lang/String;)V

    .line 2620
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->l(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2621
    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->d(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V

    .line 2622
    return-void
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1310
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1311
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Invitation ID must not be null or empty"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1313
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1314
    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;I)V

    .line 1316
    return-void

    :cond_0
    move v0, v1

    .line 1311
    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3547
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 3548
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 3550
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Must dismiss match on behalf of a valid game"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3552
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Match ID must not be empty"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3554
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/l;->m(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 3555
    invoke-static {v0, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)V

    .line 3556
    return-void

    :cond_0
    move v0, v2

    .line 3550
    goto :goto_0

    :cond_1
    move v1, v2

    .line 3552
    goto :goto_1
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1362
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1363
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 1365
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Must dismiss invitation on behalf of a valid game"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1367
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Invitation ID must not be null or empty"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1369
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/l;->m(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1370
    invoke-static {v0, p2, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;I)V

    .line 1372
    return-void

    :cond_0
    move v0, v2

    .line 1365
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1367
    goto :goto_1
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 1839
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1840
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 1841
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/service/i;->b(Landroid/content/Context;Z)V

    .line 1843
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2284
    invoke-static {}, Lcom/google/android/gms/games/ui/c/b;->i()V

    .line 2285
    return-void
.end method

.method public final c(J)V
    .locals 3

    .prologue
    .line 3352
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v0

    .line 3353
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/google/android/gms/games/a/am;->b(Ljava/lang/String;Ljava/lang/String;J)V

    .line 3355
    return-void
.end method

.method public final c(JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 2743
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 2746
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide a valid Game ID, or null for \'all games\'"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2749
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v0

    .line 2750
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3, p1, p2}, Lcom/google/android/gms/games/a/am;->d(Ljava/lang/String;Ljava/lang/String;J)V

    .line 2752
    return-void

    .line 2746
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/games/internal/dr;)V
    .locals 1

    .prologue
    .line 673
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/games/service/l;->a(Lcom/google/android/gms/games/internal/dr;Z)V

    .line 674
    return-void
.end method

.method public final c(Lcom/google/android/gms/games/internal/dr;IZZ)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2096
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 2097
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2099
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2103
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "https://www.googleapis.com/auth/plus.circles.read"

    aput-object v2, v0, v1

    invoke-direct {p0, v3, v3, p4, v0}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2105
    invoke-static {v0, p1, p2, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V

    .line 2107
    return-void
.end method

.method public final c(Lcom/google/android/gms/games/internal/dr;J)V
    .locals 8

    .prologue
    .line 2552
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2553
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2555
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v1

    .line 2556
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    move-wide v4, p2

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/games/a/am;->d(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/games/internal/dr;)V

    .line 2558
    return-void
.end method

.method public final c(Lcom/google/android/gms/games/internal/dr;JLjava/lang/String;)V
    .locals 8

    .prologue
    .line 2730
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 2731
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 2732
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2733
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide a valid Game ID, or null for \'all games\'"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2736
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v1

    .line 2737
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    move-object v3, p4

    move-wide v4, p2

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/games/a/am;->d(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/games/internal/dr;)V

    .line 2739
    return-void

    .line 2733
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2872
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2873
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Room ID must not be null or empty"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2876
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->P()Lcom/google/android/gms/games/service/ai;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/u;

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, v3, p1, p2}, Lcom/google/android/gms/games/service/statemachine/roomclient/u;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/ai;->a(Lcom/google/android/gms/games/service/statemachine/roomclient/u;)V

    .line 2878
    return-void

    .line 2873
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2216
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 2217
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2218
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219
    if-lez p3, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Must load at least 1 XP event"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2222
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->e()Ljava/lang/String;

    move-result-object p2

    .line 2225
    :cond_0
    const/4 v0, 0x0

    new-array v2, v1, [Ljava/lang/String;

    invoke-direct {p0, v0, p2, v1, v2}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2227
    invoke-static {v0, p1, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;I)V

    .line 2228
    return-void

    :cond_1
    move v0, v1

    .line 2219
    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZZ)V
    .locals 2

    .prologue
    .line 3027
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 3028
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3030
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Query must not be empty"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3033
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->m(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 3034
    invoke-static {v0, p1, p2, p3, p4}, Lcom/google/android/gms/games/service/GamesIntentService;->e(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZ)V

    .line 3036
    return-void

    .line 3030
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3444
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3445
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3446
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Match ID must not be empty"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3447
    if-eqz p3, :cond_0

    .line 3448
    invoke-static {p3}, Lcom/google/android/gms/games/multiplayer/k;->a(Ljava/lang/String;)Z

    move-result v0

    .line 3449
    const-string v3, "Invalid participant ID %s"

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p3, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 3452
    :cond_0
    invoke-direct {p0, v2}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 3453
    invoke-static {v0, p1, p2, v1, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;ZLjava/lang/String;)V

    .line 3455
    return-void

    :cond_1
    move v0, v2

    .line 3446
    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3219
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 3220
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3222
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3223
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "Must be loading snapshots on behalf of a valid game"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3225
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    :cond_0
    move v0, v1

    :goto_1
    const-string v3, "Player ID must be either null or length > 0"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3227
    if-nez p2, :cond_1

    .line 3228
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->e()Ljava/lang/String;

    move-result-object p2

    .line 3236
    :cond_1
    new-array v0, v1, [Ljava/lang/String;

    const-string v1, "https://www.googleapis.com/auth/drive.appdata"

    aput-object v1, v0, v2

    invoke-direct {p0, p3, p2, p4, v0}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 3238
    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/service/b;)V

    .line 3239
    return-void

    :cond_2
    move v0, v2

    .line 3223
    goto :goto_0

    :cond_3
    move v0, v2

    .line 3225
    goto :goto_1
.end method

.method public final c(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 1539
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1540
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1541
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Leaderboard ID must not be null or empty"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1543
    invoke-direct {p0, p3}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    .line 1545
    return-void

    .line 1541
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/games/internal/dr;Z)V
    .locals 2

    .prologue
    .line 2003
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2004
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2008
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;I)V

    .line 2010
    return-void
.end method

.method public final c(Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1970
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1971
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1972
    if-eqz p2, :cond_0

    array-length v0, p2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Must provide at least 1 external player ID."

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1974
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V

    .line 1976
    return-void

    :cond_0
    move v0, v1

    .line 1972
    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2150
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 2151
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2153
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Player ID must not be empty"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2158
    const/4 v0, 0x0

    new-array v1, v1, [Ljava/lang/String;

    const-string v3, "https://www.googleapis.com/auth/plus.circles.write"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, p1, v2, v1}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2160
    invoke-static {v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;)V

    .line 2161
    return-void

    :cond_0
    move v0, v2

    .line 2153
    goto :goto_0
.end method

.method public final c(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 1792
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1793
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 1798
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide a valid Game ID, or null for \'all games\'"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1801
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/d/ar;->a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v0

    .line 1802
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-static {v1, v0, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1804
    return-void

    .line 1798
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 15

    .prologue
    .line 910
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->L()V

    .line 911
    sget-object v0, Lcom/google/android/gms/location/copresence/f;->b:Lcom/google/android/gms/common/api/c;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->a(Lcom/google/android/gms/common/api/c;)Z

    move-result v0

    const-string v1, "Must enable a Copresence client before calling!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 914
    if-eqz p1, :cond_7

    .line 915
    const-string v12, "nearby_players"

    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    sget-object v0, Lcom/google/android/gms/location/copresence/f;->b:Lcom/google/android/gms/common/api/c;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->a(Lcom/google/android/gms/common/api/c;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/android/gms/location/copresence/f;->a()Lcom/google/android/gms/location/copresence/h;

    move-result-object v13

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    new-instance v14, Lcom/google/android/gms/location/copresence/Message;

    invoke-direct {v14, v12, v0}, Lcom/google/android/gms/location/copresence/Message;-><init>(Ljava/lang/String;[B)V

    new-instance v0, Lcom/google/android/gms/location/copresence/c;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/c;-><init>()V

    invoke-static {}, Lcom/google/android/gms/location/copresence/AccessPolicy;->o()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/copresence/c;->a(Lcom/google/android/gms/common/people/data/Audience;)Lcom/google/android/gms/location/copresence/c;

    move-result-object v10

    iget v0, v10, Lcom/google/android/gms/location/copresence/c;->a:I

    and-int/lit8 v0, v0, 0x19

    add-int/lit8 v1, v0, -0x1

    and-int/2addr v0, v1

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can\'t set more than one of name, audience, or aclResourceId"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    iget v0, v10, Lcom/google/android/gms/location/copresence/c;->a:I

    and-int/lit8 v0, v0, 0x19

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/common/people/data/a;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v10, v0}, Lcom/google/android/gms/location/copresence/c;->a(Lcom/google/android/gms/common/people/data/Audience;)Lcom/google/android/gms/location/copresence/c;

    :cond_0
    new-instance v1, Lcom/google/android/gms/location/copresence/AccessPolicy;

    iget v2, v10, Lcom/google/android/gms/location/copresence/c;->a:I

    iget-object v3, v10, Lcom/google/android/gms/location/copresence/c;->e:Ljava/lang/String;

    iget-wide v4, v10, Lcom/google/android/gms/location/copresence/c;->b:J

    iget-object v6, v10, Lcom/google/android/gms/location/copresence/c;->c:Lcom/google/android/gms/location/copresence/AccessLock;

    iget-object v7, v10, Lcom/google/android/gms/location/copresence/c;->d:Lcom/google/android/gms/common/people/data/Audience;

    iget v8, v10, Lcom/google/android/gms/location/copresence/c;->f:I

    iget v9, v10, Lcom/google/android/gms/location/copresence/c;->g:I

    iget-object v10, v10, Lcom/google/android/gms/location/copresence/c;->h:Lcom/google/android/gms/location/copresence/AclResourceId;

    const/4 v11, 0x0

    invoke-direct/range {v1 .. v11}, Lcom/google/android/gms/location/copresence/AccessPolicy;-><init>(ILjava/lang/String;JLcom/google/android/gms/location/copresence/AccessLock;Lcom/google/android/gms/common/people/data/Audience;IILcom/google/android/gms/location/copresence/AclResourceId;B)V

    new-instance v2, Lcom/google/android/gms/location/copresence/v;

    invoke-direct {v2}, Lcom/google/android/gms/location/copresence/v;-><init>()V

    iget-boolean v0, v2, Lcom/google/android/gms/location/copresence/v;->b:Z

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    const-string v3, "Cannot call setNoOptInRequired() in conjunction with setWakeUpOthers()."

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, v2, Lcom/google/android/gms/location/copresence/v;->c:Z

    invoke-virtual {v2}, Lcom/google/android/gms/location/copresence/v;->a()Lcom/google/android/gms/location/copresence/u;

    move-result-object v8

    invoke-interface {v13, v12, v8, v14, v1}, Lcom/google/android/gms/location/copresence/h;->a(Ljava/lang/String;Lcom/google/android/gms/location/copresence/u;Lcom/google/android/gms/location/copresence/Message;Lcom/google/android/gms/location/copresence/AccessPolicy;)Lcom/google/android/gms/location/copresence/h;

    sget-object v0, Lcom/google/android/gms/games/c/a;->P:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    new-instance v9, Lcom/google/android/gms/location/copresence/o;

    invoke-direct {v9}, Lcom/google/android/gms/location/copresence/o;-><init>()V

    sget-object v6, Lcom/google/android/gms/location/copresence/People;->b:Lcom/google/android/gms/location/copresence/People;

    const-wide/16 v0, -0x1

    cmp-long v0, v4, v0

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-lez v0, :cond_6

    const-wide v0, 0x757b12c00L

    cmp-long v0, v4, v0

    if-gez v0, :cond_6

    :cond_1
    const/4 v0, 0x1

    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "timeToLiveMillis value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not between 1 and 31536000000 or INFINITE_TIME_TO_LIVE_MILLIS"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    const-string v0, "senders cannot be null."

    invoke-static {v6, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v10, v9, Lcom/google/android/gms/location/copresence/o;->a:Ljava/util/List;

    new-instance v0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x0

    move-object v2, v12

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/AccessKey;JLcom/google/android/gms/location/copresence/People;B)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/location/copresence/MessageFilter;

    iget-object v1, v9, Lcom/google/android/gms/location/copresence/o;->a:Ljava/util/List;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/location/copresence/MessageFilter;-><init>(Ljava/util/List;B)V

    new-instance v1, Lcom/google/android/gms/games/service/n;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/service/n;-><init>(Lcom/google/android/gms/games/service/l;)V

    invoke-interface {v13, v8, v0, v1}, Lcom/google/android/gms/location/copresence/h;->a(Lcom/google/android/gms/location/copresence/u;Lcom/google/android/gms/location/copresence/MessageFilter;Lcom/google/android/gms/location/copresence/r;)Lcom/google/android/gms/location/copresence/h;

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->h:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->g:Ljava/util/HashMap;

    invoke-virtual {v0, v12}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/r;

    if-eqz v0, :cond_2

    invoke-interface {v13, v0}, Lcom/google/android/gms/location/copresence/h;->a(Lcom/google/android/gms/location/copresence/r;)Lcom/google/android/gms/location/copresence/h;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->g:Ljava/util/HashMap;

    invoke-virtual {v0, v12, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/b;->c()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    invoke-interface {v13, v0}, Lcom/google/android/gms/location/copresence/h;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    .line 919
    :cond_3
    :goto_3
    return-void

    .line 915
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 917
    :cond_7
    const-string v0, "nearby_players"

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->j(Ljava/lang/String;)V

    goto :goto_3
.end method

.method public final d(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 2976
    invoke-static {}, Lcom/google/android/gms/games/service/l;->J()V

    .line 2979
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->P()Lcom/google/android/gms/games/service/ai;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/bw;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/bw;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/ai;->a(Lcom/google/android/gms/games/service/statemachine/roomclient/bw;)I

    move-result v0

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 4

    .prologue
    .line 610
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 615
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 616
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 617
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    .line 618
    const-string v2, "android.permission.GET_ACCOUNTS"

    invoke-virtual {v0, v2, v1}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 619
    if-eqz v0, :cond_0

    .line 620
    new-instance v0, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Package "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is missing permission android.permission.GET_ACCOUNTS"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 626
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/i/a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(J)V
    .locals 3

    .prologue
    .line 2564
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v0

    .line 2565
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/google/android/gms/games/a/am;->d(Ljava/lang/String;Ljava/lang/String;J)V

    .line 2567
    return-void
.end method

.method public final d(JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 2452
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 2455
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "The externalGameId must be null or a non-empty string."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2458
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v0

    .line 2459
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3, p1, p2}, Lcom/google/android/gms/games/a/am;->c(Ljava/lang/String;Ljava/lang/String;J)V

    .line 2461
    return-void

    .line 2455
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/games/internal/dr;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1096
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1097
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1098
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1099
    invoke-static {v0, p1, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Z)V

    .line 1100
    return-void
.end method

.method public final d(Lcom/google/android/gms/games/internal/dr;IZZ)V
    .locals 7

    .prologue
    .line 2041
    const-string v2, "circled"

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/games/service/l;->b(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 2044
    return-void
.end method

.method public final d(Lcom/google/android/gms/games/internal/dr;J)V
    .locals 8

    .prologue
    .line 2406
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2407
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2409
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v1

    .line 2410
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    move-wide v4, p2

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/games/a/am;->c(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/games/internal/dr;)V

    .line 2412
    return-void
.end method

.method public final d(Lcom/google/android/gms/games/internal/dr;JLjava/lang/String;)V
    .locals 8

    .prologue
    .line 2438
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 2439
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 2440
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2441
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "The externalGameId must be null or a non-empty string."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2444
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v1

    .line 2445
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    move-object v3, p4

    move-wide v4, p2

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/games/a/am;->c(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/games/internal/dr;)V

    .line 2447
    return-void

    .line 2441
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1641
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/games/service/l;->d(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Z)V

    .line 1642
    return-void
.end method

.method public final d(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZZ)V
    .locals 2

    .prologue
    .line 1993
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1994
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1995
    if-lez p3, :cond_0

    const/16 v0, 0x19

    if-gt p3, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Page size must be between 1 and 25"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1997
    invoke-direct {p0, p5}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    invoke-static {v0, p1, p2, p3, p4}, Lcom/google/android/gms/games/service/GamesIntentService;->c(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZ)V

    .line 1999
    return-void

    .line 1995
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3518
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 3519
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 3521
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3522
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Must rematch on behalf of a valid game"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3524
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Match ID must not be empty"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3526
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/service/l;->m(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 3527
    invoke-static {v0, p1, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->c(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    .line 3528
    return-void

    :cond_0
    move v0, v2

    .line 3522
    goto :goto_0

    :cond_1
    move v1, v2

    .line 3524
    goto :goto_1
.end method

.method public final d(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1647
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1648
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1650
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1651
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Game ID must not be empty"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1653
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->e()Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {p0, p2, v0, p3, v1}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1655
    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    .line 1657
    return-void

    :cond_0
    move v0, v1

    .line 1651
    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/games/internal/dr;Z)V
    .locals 2

    .prologue
    .line 3082
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3084
    sget-object v0, Lcom/google/android/gms/drive/b;->f:Lcom/google/android/gms/common/api/c;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->a(Lcom/google/android/gms/common/api/c;)Z

    move-result v0

    const-string v1, "Must include Drive.SCOPE_APPFOLDER to use snapshots!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3086
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->Q()V

    .line 3087
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3090
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/b;->a()V

    .line 3091
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    invoke-static {v0, p1, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/service/b;)V

    .line 3093
    return-void
.end method

.method public final d(Ljava/lang/String;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2134
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 2135
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2137
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Player ID must not be empty"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2139
    packed-switch p2, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    .line 2137
    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 2139
    :goto_1
    const-string v3, "Can only record positive actions with this API"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2143
    const/4 v0, 0x0

    new-array v1, v1, [Ljava/lang/String;

    const-string v3, "https://www.googleapis.com/auth/plus.circles.write"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, p1, v2, v1}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2145
    invoke-static {v0, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;I)V

    .line 2146
    return-void

    :pswitch_1
    move v0, v2

    .line 2139
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final e(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 3810
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->K()V

    .line 3811
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3813
    if-eqz p1, :cond_0

    .line 3814
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 3817
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1912
    iget-boolean v0, p0, Lcom/google/android/gms/games/service/l;->i:Z

    if-eqz v0, :cond_0

    .line 1913
    const/4 v0, 0x0

    .line 1919
    :goto_0
    return-object v0

    .line 1917
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1918
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->j:Landroid/content/ContentValues;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->j:Landroid/content/ContentValues;

    const-string v1, "external_player_id"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final e(J)V
    .locals 3

    .prologue
    .line 2430
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v0

    .line 2431
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/google/android/gms/games/a/am;->c(Ljava/lang/String;Ljava/lang/String;J)V

    .line 2433
    return-void
.end method

.method public final e(Lcom/google/android/gms/games/internal/dr;)V
    .locals 1

    .prologue
    .line 1282
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/games/service/l;->a(Lcom/google/android/gms/games/internal/dr;I)V

    .line 1283
    return-void
.end method

.method public final e(Lcom/google/android/gms/games/internal/dr;IZZ)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2119
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 2120
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2122
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2126
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "https://www.googleapis.com/auth/plus.circles.read"

    aput-object v2, v0, v1

    invoke-direct {p0, v3, v3, p4, v0}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2128
    invoke-static {v0, p1, p2, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->c(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V

    .line 2130
    return-void
.end method

.method public final e(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1143
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1144
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 1146
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1147
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "Game ID must not be empty"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1149
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/service/l;->m(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1150
    invoke-static {v0, p1, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Z)V

    .line 1151
    return-void

    .line 1147
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZZ)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1179
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1180
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1182
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1183
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Player ID must not be empty or null."

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1185
    const/4 v0, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {p0, v0, p2, p5, v1}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1187
    invoke-static {v0, p1, p3, p4}, Lcom/google/android/gms/games/service/GamesIntentService;->f(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V

    .line 1188
    return-void

    :cond_0
    move v0, v1

    .line 1183
    goto :goto_0
.end method

.method public final e(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3533
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 3534
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 3536
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3537
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Must accept turn-based invite on behalf of a valid game"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3539
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Match ID must not be empty"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3541
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/service/l;->m(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 3542
    invoke-static {v0, p1, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->d(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    .line 3543
    return-void

    :cond_0
    move v0, v2

    .line 3537
    goto :goto_0

    :cond_1
    move v1, v2

    .line 3539
    goto :goto_1
.end method

.method public final e(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 3098
    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/gms/games/service/l;->a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;ZI)V

    .line 3100
    return-void
.end method

.method public final e(Lcom/google/android/gms/games/internal/dr;Z)V
    .locals 2

    .prologue
    .line 1860
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->K()V

    .line 1861
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 1863
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1865
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    .line 1866
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-static {v1, v0, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;Z)V

    .line 1868
    return-void
.end method

.method public final e(Ljava/lang/String;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1060
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1061
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Event ID must not be null or empty"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1063
    if-ltz p2, :cond_1

    :goto_1
    const-string v0, "Number of steps must be >= 0"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1064
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->n:Lcom/google/android/gms/games/internal/d/d;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/internal/d/d;->a(Ljava/lang/String;I)V

    .line 1065
    return-void

    :cond_0
    move v0, v2

    .line 1061
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1063
    goto :goto_1
.end method

.method public final f(Ljava/lang/String;I)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 1474
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1475
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 1476
    invoke-static {p2}, Lcom/google/android/gms/games/internal/b/i;->b(I)Z

    move-result v0

    const-string v1, "Unrecognized time span %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1482
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.VIEW_LEADERBOARD_SCORES"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1483
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1484
    const-string v1, "com.google.android.gms.games.LEADERBOARD_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1485
    const-string v1, "com.google.android.gms.games.LEADERBOARD_TIME_SPAN"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1486
    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1488
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1489
    iget v1, p0, Lcom/google/android/gms/games/service/l;->d:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    .prologue
    .line 1924
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1925
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->j:Landroid/content/ContentValues;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930
    sget-object v0, Lcom/google/android/gms/games/provider/aq;->a:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lcom/google/android/gms/common/data/m;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->j:Landroid/content/ContentValues;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/data/m;->a(Landroid/content/ContentValues;)Lcom/google/android/gms/common/data/m;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/data/m;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 1933
    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->a()V

    .line 1934
    return-object v0
.end method

.method public final f(Lcom/google/android/gms/games/internal/dr;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2033
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0, v1, v1}, Lcom/google/android/gms/games/service/l;->d(Lcom/google/android/gms/games/internal/dr;IZZ)V

    .line 2035
    return-void
.end method

.method public final f(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1104
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1105
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1107
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Game ID must not be empty"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1110
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/service/l;->m(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1111
    invoke-static {v0, p1}, Lcom/google/android/gms/games/service/GamesIntentService;->i(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V

    .line 1112
    return-void

    .line 1108
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZZ)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1194
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1195
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1197
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1198
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Player ID must not be empty or null."

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1200
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->e()Ljava/lang/String;

    move-result-object v2

    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {p0, v0, v2, p5, v1}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1202
    invoke-static {v0, p1, p3, p4}, Lcom/google/android/gms/games/service/GamesIntentService;->g(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V

    .line 1204
    return-void

    :cond_0
    move v0, v1

    .line 1198
    goto :goto_0
.end method

.method public final f(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2334
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2335
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2336
    const-string v0, "Must provide a valid milestone id"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 2337
    const-string v0, "Must provide a valid quest id"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 2338
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->n:Lcom/google/android/gms/games/internal/d/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/d/d;->b()V

    .line 2339
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2340
    invoke-static {v0, p1, p2, p3}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;)V

    .line 2342
    return-void
.end method

.method public final f(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 1945
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1946
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1948
    if-nez p2, :cond_0

    .line 1949
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->e()Ljava/lang/String;

    move-result-object p2

    .line 1952
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1954
    const/4 v0, 0x0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {p0, v0, p2, p3, v1}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1965
    :cond_1
    :goto_0
    invoke-static {v0, p1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V

    .line 1966
    return-void

    .line 1957
    :cond_2
    invoke-direct {p0, p3}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1959
    iget-object v1, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1960
    invoke-virtual {v0}, Lcom/google/android/gms/games/a/au;->c()Lcom/google/android/gms/games/a/av;

    move-result-object v0

    iput-object p2, v0, Lcom/google/android/gms/games/a/av;->f:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object v0

    goto :goto_0
.end method

.method public final f(Lcom/google/android/gms/games/internal/dr;Z)V
    .locals 1

    .prologue
    .line 1039
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1040
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1041
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->n:Lcom/google/android/gms/games/internal/d/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/d/d;->b()V

    .line 1042
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1043
    invoke-static {v0, p1}, Lcom/google/android/gms/games/service/GamesIntentService;->g(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V

    .line 1044
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 3359
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3360
    const-string v0, "Match ID must not be null"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3361
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 3362
    invoke-static {v0, p1}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)V

    .line 3363
    return-void
.end method

.method public final g(Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 1469
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/games/service/l;->f(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final g(Lcom/google/android/gms/games/internal/dr;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2112
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v1, v1}, Lcom/google/android/gms/games/service/l;->e(Lcom/google/android/gms/games/internal/dr;IZZ)V

    .line 2114
    return-void
.end method

.method public final g(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1377
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/games/service/l;->b(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Z)V

    .line 1378
    return-void
.end method

.method public final g(Lcom/google/android/gms/games/internal/dr;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2233
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->K()V

    .line 2234
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 2236
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2237
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-direct {p0, v1, v1, p2, v0}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2239
    invoke-static {v0, p1}, Lcom/google/android/gms/games/service/GamesIntentService;->c(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V

    .line 2240
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1816
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1817
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 1820
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final h(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 4

    .prologue
    .line 2941
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2942
    invoke-static {p1}, Lcom/google/android/gms/games/multiplayer/k;->a(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "Invalid participant ID %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2944
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->P()Lcom/google/android/gms/games/service/ai;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/y;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/y;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/ai;->a(Lcom/google/android/gms/games/service/statemachine/roomclient/y;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1073
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1078
    sget-object v0, Lcom/google/android/gms/games/k/a;->a:Lcom/google/android/gms/games/provider/a;

    iget-object v0, v0, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lcom/google/android/gms/common/data/m;

    move-result-object v0

    .line 1083
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->k:Landroid/content/ContentValues;

    if-nez v1, :cond_0

    .line 1084
    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/data/m;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 1090
    :goto_0
    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->a()V

    .line 1091
    return-object v0

    .line 1087
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->k:Landroid/content/ContentValues;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/data/m;->a(Landroid/content/ContentValues;)Lcom/google/android/gms/common/data/m;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/data/m;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method

.method public final h(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)Lcom/google/android/gms/games/multiplayer/realtime/RoomEntity;
    .locals 3

    .prologue
    .line 2954
    invoke-static {}, Lcom/google/android/gms/games/service/l;->J()V

    .line 2955
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2957
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->P()Lcom/google/android/gms/games/service/ai;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/bp;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/games/service/statemachine/roomclient/bp;-><init>(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/ai;->a(Lcom/google/android/gms/games/service/statemachine/roomclient/bp;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 2959
    new-instance v1, Lcom/google/android/gms/games/multiplayer/realtime/c;

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/multiplayer/realtime/c;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 2960
    const/4 v0, 0x0

    .line 2962
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/realtime/c;->c()I

    move-result v2

    if-lez v2, :cond_0

    .line 2963
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/multiplayer/realtime/c;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/realtime/RoomEntity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2966
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/realtime/c;->w_()V

    .line 2968
    return-object v0

    .line 2966
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/realtime/c;->w_()V

    throw v0
.end method

.method public final h(Lcom/google/android/gms/games/internal/dr;)V
    .locals 2

    .prologue
    .line 775
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->K()V

    .line 776
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 778
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 780
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    .line 781
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-static {v1, v0, p1}, Lcom/google/android/gms/games/service/GamesIntentService;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;)V

    .line 782
    return-void
.end method

.method public final h(Lcom/google/android/gms/games/internal/dr;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2246
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->K()V

    .line 2247
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 2249
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2250
    new-array v0, v1, [Ljava/lang/String;

    invoke-direct {p0, v2, v2, v1, v0}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2252
    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Z)V

    .line 2253
    return-void
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 3408
    sget-object v0, Lcom/google/android/gms/games/c/a;->w:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v0

    return v0
.end method

.method public final i(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2310
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2312
    const-string v0, "Must provide a valid quest ID"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2315
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->n:Lcom/google/android/gms/games/internal/d/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/d/d;->b()V

    .line 2316
    const-string v0, "com.google.android.gms.games.SHOW_QUEST"

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->k(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2317
    const-string v1, "com.google.android.gms.games.QUEST_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2318
    iget v1, p0, Lcom/google/android/gms/games/service/l;->d:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final i(Lcom/google/android/gms/games/internal/dr;)V
    .locals 2

    .prologue
    .line 1848
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->K()V

    .line 1849
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 1851
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1853
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    .line 1854
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-static {v1, v0, p1}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;)V

    .line 1855
    return-void
.end method

.method public final i(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1769
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->L()V

    .line 1770
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 1772
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1773
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Game ID must not be empty"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1776
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/service/l;->l(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1777
    invoke-static {v0, p1}, Lcom/google/android/gms/games/service/GamesIntentService;->h(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V

    .line 1778
    return-void

    .line 1773
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 1808
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1809
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 1811
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    .line 1812
    return-void
.end method

.method public final j(Lcom/google/android/gms/games/internal/dr;)V
    .locals 1

    .prologue
    .line 2081
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 2088
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2090
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/games/internal/dr;)V

    .line 2091
    return-void
.end method

.method public final j(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 787
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->K()V

    .line 788
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 790
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 791
    const-string v0, "ACL data must not be null"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 793
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    .line 794
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-static {v1, v0, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    .line 795
    return-void
.end method

.method public final k()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1455
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1459
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.VIEW_LEADERBOARDS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1460
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1461
    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1463
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1464
    iget v1, p0, Lcom/google/android/gms/games/service/l;->d:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final k(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1411
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1412
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 1414
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1415
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide a valid Invitation ID"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1418
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->m(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1419
    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    .line 1420
    return-void

    .line 1415
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 660
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 664
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.VIEW_ACHIEVEMENTS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 665
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 666
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 667
    iget v1, p0, Lcom/google/android/gms/games/service/l;->d:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final l(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3390
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3391
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3392
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Match ID must not be empty"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3393
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 3394
    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->c(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    .line 3395
    return-void

    :cond_0
    move v0, v1

    .line 3392
    goto :goto_0
.end method

.method public final m()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 3309
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3313
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_MULTIPLAYER_INBOX"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3314
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 3315
    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3317
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3318
    iget v1, p0, Lcom/google/android/gms/games/service/l;->d:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final m(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3399
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3400
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3401
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Match ID must not be empty"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3402
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 3403
    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->d(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    .line 3404
    return-void

    :cond_0
    move v0, v1

    .line 3401
    goto :goto_0
.end method

.method public final n()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1212
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1216
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_INVITATIONS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1217
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1218
    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1220
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1221
    iget v1, p0, Lcom/google/android/gms/games/service/l;->d:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final n(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3475
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3477
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Match ID must not be empty"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3479
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 3480
    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->e(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    .line 3481
    return-void

    :cond_0
    move v0, v1

    .line 3477
    goto :goto_0
.end method

.method public final o()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2990
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2994
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.PLAYER_SEARCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2995
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2996
    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2998
    iget v1, p0, Lcom/google/android/gms/games/service/l;->d:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final o(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3433
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3434
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3435
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Match ID must not be empty"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3436
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 3437
    const/4 v2, 0x0

    invoke-static {v0, p1, p2, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;ZLjava/lang/String;)V

    .line 3439
    return-void

    :cond_0
    move v0, v1

    .line 3435
    goto :goto_0
.end method

.method public final p()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 3795
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3799
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3800
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 3801
    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3803
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3804
    iget v1, p0, Lcom/google/android/gms/games/service/l;->d:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final p(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3508
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3509
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3510
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Match ID must not be empty"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3511
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 3512
    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->f(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    .line 3513
    return-void

    :cond_0
    move v0, v1

    .line 3510
    goto :goto_0
.end method

.method public final q()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 3784
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3788
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.PARCEL_COMPAT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3789
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 3790
    iget v1, p0, Lcom/google/android/gms/games/service/l;->d:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final q(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 3040
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 3041
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3043
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Query must not be empty"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3044
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    .line 3046
    return-void

    .line 3043
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 869
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/h/b;->e(Lcom/google/android/gms/common/server/ClientContext;)I

    move-result v0

    return v0
.end method

.method public final r(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 3171
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 3173
    sget-object v0, Lcom/google/android/gms/drive/b;->f:Lcom/google/android/gms/common/api/c;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->a(Lcom/google/android/gms/common/api/c;)Z

    move-result v0

    const-string v1, "Must include Drive.SCOPE_APPFOLDER to use snapshots!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 3175
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->Q()V

    .line 3176
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3177
    const-string v0, "Must provide a non-empty snapshot ID"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 3178
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/b;->a()V

    .line 3179
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->l:Lcom/google/android/gms/games/service/b;

    invoke-static {v0, p1, v1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/service/b;Ljava/lang/String;)V

    .line 3181
    return-void
.end method

.method public final s()I
    .locals 1

    .prologue
    .line 2571
    sget-object v0, Lcom/google/android/gms/games/c/a;->x:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v0

    return v0
.end method

.method public final s(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2186
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 2187
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2188
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Player ID must not be null or empty"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 2191
    const/4 v0, 0x0

    new-array v2, v1, [Ljava/lang/String;

    invoke-direct {p0, v0, p2, v1, v2}, Lcom/google/android/gms/games/service/l;->b(Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2193
    invoke-static {v0, p1}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V

    .line 2194
    return-void

    :cond_0
    move v0, v1

    .line 2188
    goto :goto_0
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 2576
    sget-object v0, Lcom/google/android/gms/games/c/a;->y:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v0

    return v0
.end method

.method public final t(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1894
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1895
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 1900
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Game ID filtering not supported!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 1901
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/service/l;->m(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1902
    invoke-static {v0, p1}, Lcom/google/android/gms/games/service/GamesIntentService;->d(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V

    .line 1903
    return-void

    .line 1900
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final u()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2516
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2520
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_REQUEST_INBOX"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2521
    iget-object v1, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2522
    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Lcom/google/android/gms/games/service/l;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2524
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2525
    iget v1, p0, Lcom/google/android/gms/games/service/l;->d:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Intent;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final u(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2323
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 2324
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2325
    const-string v0, "Must provide a valid quest id"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 2326
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->n:Lcom/google/android/gms/games/internal/d/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/d/d;->b()V

    .line 2327
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->d(Z)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2328
    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->g(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    .line 2329
    return-void
.end method

.method public final v()V
    .locals 1

    .prologue
    .line 1783
    invoke-static {}, Lcom/google/android/gms/games/service/l;->J()V

    .line 1784
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->O()V

    .line 1786
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/l;->l(Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1787
    invoke-static {v0}, Lcom/google/android/gms/games/service/GamesIntentService;->b(Lcom/google/android/gms/games/a/au;)V

    .line 1788
    return-void
.end method

.method public final w()I
    .locals 1

    .prologue
    .line 3213
    sget-object v0, Lcom/google/android/gms/games/c/a;->K:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v0

    return v0
.end method

.method public final x()I
    .locals 1

    .prologue
    .line 3208
    sget-object v0, Lcom/google/android/gms/games/c/a;->L:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v0

    return v0
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 1832
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->M()V

    .line 1833
    invoke-direct {p0}, Lcom/google/android/gms/games/service/l;->N()V

    .line 1834
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/games/service/i;->b(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final z()Ljava/lang/String;
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Lcom/google/android/gms/games/service/l;->e:Ljava/lang/String;

    return-object v0
.end method
