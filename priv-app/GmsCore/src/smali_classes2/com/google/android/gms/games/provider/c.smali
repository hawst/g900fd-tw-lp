.class public final Lcom/google/android/gms/games/provider/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/HashMap;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/c;->a:Ljava/util/HashMap;

    .line 126
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/google/android/gms/games/provider/c;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/games/provider/a;
    .locals 2

    .prologue
    .line 147
    new-instance v0, Lcom/google/android/gms/games/provider/a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/games/provider/a;-><init>(Lcom/google/android/gms/games/provider/c;B)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/games/provider/a;)Lcom/google/android/gms/games/provider/c;
    .locals 3

    .prologue
    .line 140
    iget-object v0, p1, Lcom/google/android/gms/games/provider/a;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 141
    iget-object v1, p1, Lcom/google/android/gms/games/provider/a;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/provider/d;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    goto :goto_0

    .line 143
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/gms/games/provider/c;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/games/provider/c;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/provider/d;

    .line 131
    if-ne v0, p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cannot add the same column with different types!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/provider/c;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    return-object p0

    .line 131
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
