.class public final Lcom/google/android/gms/icing/impl/e/g;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:Lcom/google/k/f/ap;

.field private c:J

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 24
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "query"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "query_universal"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "suggest"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/icing/impl/e/g;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Lcom/google/k/f/ap;

    invoke-direct {v0}, Lcom/google/k/f/ap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    .line 37
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    iput p1, v0, Lcom/google/k/f/ap;->a:I

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    iput p2, v0, Lcom/google/k/f/ap;->c:I

    .line 39
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/impl/e/g;->c:J

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/impl/e/g;->d:I

    .line 41
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;IIII)Lcom/google/k/f/ap;
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 69
    iget v0, p0, Lcom/google/android/gms/icing/impl/e/g;->d:I

    if-ne v0, v8, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 70
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    iput p5, v0, Lcom/google/k/f/ap;->b:I

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    iget v3, v0, Lcom/google/k/f/ap;->i:I

    iget-wide v6, p0, Lcom/google/android/gms/icing/impl/e/g;->c:J

    sub-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v3, v4

    iput v3, v0, Lcom/google/k/f/ap;->i:I

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    iput v3, v0, Lcom/google/k/f/ap;->d:I

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    iput p2, v0, Lcom/google/k/f/ap;->e:I

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    iput p3, v0, Lcom/google/k/f/ap;->f:I

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    iput p4, v0, Lcom/google/k/f/ap;->g:I

    .line 77
    iput v9, p0, Lcom/google/android/gms/icing/impl/e/g;->d:I

    .line 80
    const-string v0, "%s [%s] req/res/sco %d/%d/%d in %d+%d+%d = %d ms"

    const/16 v3, 0x9

    new-array v3, v3, [Ljava/lang/Object;

    sget-object v4, Lcom/google/android/gms/icing/impl/e/g;->a:[Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    iget v5, v5, Lcom/google/k/f/ap;->a:I

    aget-object v4, v4, v5

    aput-object v4, v3, v2

    aput-object p1, v3, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    iget v2, v2, Lcom/google/k/f/ap;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    iget v1, v1, Lcom/google/k/f/ap;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v8

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    iget v1, v1, Lcom/google/k/f/ap;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v9

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    iget v2, v2, Lcom/google/k/f/ap;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    iget v2, v2, Lcom/google/k/f/ap;->i:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    iget v2, v2, Lcom/google/k/f/ap;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    iget v2, v2, Lcom/google/k/f/ap;->h:I

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    iget v4, v4, Lcom/google/k/f/ap;->i:I

    add-int/2addr v2, v4

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    iget v4, v4, Lcom/google/k/f/ap;->j:I

    add-int/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-static {v0, v3}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    return-object v0

    :cond_0
    move v0, v2

    .line 69
    goto/16 :goto_0
.end method

.method public final a()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 44
    iget v0, p0, Lcom/google/android/gms/icing/impl/e/g;->d:I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 45
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    iget-wide v4, p0, Lcom/google/android/gms/icing/impl/e/g;->c:J

    sub-long v4, v2, v4

    long-to-int v4, v4

    iput v4, v0, Lcom/google/k/f/ap;->h:I

    .line 47
    iput-wide v2, p0, Lcom/google/android/gms/icing/impl/e/g;->c:J

    .line 48
    iput v1, p0, Lcom/google/android/gms/icing/impl/e/g;->d:I

    .line 49
    return-void

    .line 44
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 52
    iget v1, p0, Lcom/google/android/gms/icing/impl/e/g;->d:I

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 53
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 54
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    iget-wide v4, p0, Lcom/google/android/gms/icing/impl/e/g;->c:J

    sub-long v4, v0, v4

    long-to-int v3, v4

    iput v3, v2, Lcom/google/k/f/ap;->i:I

    .line 55
    iput-wide v0, p0, Lcom/google/android/gms/icing/impl/e/g;->c:J

    .line 56
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/icing/impl/e/g;->d:I

    .line 57
    return-void

    .line 52
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 60
    iget v0, p0, Lcom/google/android/gms/icing/impl/e/g;->d:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 61
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 62
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/e/g;->b:Lcom/google/k/f/ap;

    iget-wide v4, p0, Lcom/google/android/gms/icing/impl/e/g;->c:J

    sub-long v4, v0, v4

    long-to-int v3, v4

    iput v3, v2, Lcom/google/k/f/ap;->j:I

    .line 63
    iput-wide v0, p0, Lcom/google/android/gms/icing/impl/e/g;->c:J

    .line 64
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/icing/impl/e/g;->d:I

    .line 65
    return-void

    .line 60
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
