.class public Lcom/google/android/gms/car/SetupFsm$WaitingForCarConnectionState;
.super Lcom/google/android/car/fsm/j;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/car/fsm/l;
    a = {
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_ERROR"
            b = Lcom/google/android/gms/car/SetupFsm$ErrorState;
            c = Lcom/google/android/gms/car/SetupFsm$WaitingForCarConnectionState;
        .end subannotation,
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_CAR_DISCONNECTED"
            b = Lcom/google/android/gms/car/SetupFsm$SetupFailedState;
            c = Lcom/google/android/gms/car/SetupFsm$WaitingForCarConnectionState;
        .end subannotation,
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_CONNECTED_TO_CAR"
            b = Lcom/google/android/gms/car/SetupFsm$CheckCountryWhitelistState;
            c = Lcom/google/android/gms/car/SetupFsm$WaitingForCarConnectionState;
        .end subannotation
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 170
    invoke-direct {p0}, Lcom/google/android/car/fsm/j;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 175
    const-string v0, "EVENT_ERROR"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "EVENT_CAR_DISCONNECTED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "EVENT_CONNECTED_TO_CAR"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 179
    :cond_0
    const/4 v0, 0x0

    .line 183
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
