.class public final Lcom/google/android/gms/lockbox/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final d:Lcom/google/android/gms/lockbox/i;


# instance fields
.field final a:I

.field b:I

.field c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/google/android/gms/lockbox/i;

    invoke-direct {v0}, Lcom/google/android/gms/lockbox/i;-><init>()V

    sput-object v0, Lcom/google/android/gms/lockbox/g;->d:Lcom/google/android/gms/lockbox/i;

    return-void
.end method

.method constructor <init>(III)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput p1, p0, Lcom/google/android/gms/lockbox/g;->a:I

    .line 42
    iput p2, p0, Lcom/google/android/gms/lockbox/g;->b:I

    .line 43
    iput p3, p0, Lcom/google/android/gms/lockbox/g;->c:I

    .line 44
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/android/gms/lockbox/g;->b:I

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/google/android/gms/lockbox/g;->c:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 65
    invoke-static {p0, p1}, Lcom/google/android/gms/lockbox/i;->a(Lcom/google/android/gms/lockbox/g;Landroid/os/Parcel;)V

    .line 66
    return-void
.end method
