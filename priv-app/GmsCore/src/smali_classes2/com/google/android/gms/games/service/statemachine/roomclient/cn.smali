.class final Lcom/google/android/gms/games/service/statemachine/roomclient/cn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/service/statemachine/g;

.field final synthetic b:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;Lcom/google/android/gms/games/service/statemachine/g;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cn;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;

    iput-object p2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cn;->a:Lcom/google/android/gms/games/service/statemachine/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final binderDied()V
    .locals 3

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cn;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/br;

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cn;->a:Lcom/google/android/gms/games/service/statemachine/g;

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/br;-><init>(Lcom/google/android/gms/games/service/statemachine/g;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cn;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cn;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;

    const-string v1, "RoomAndroidService has died"

    invoke-static {v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;->a(Ljava/lang/String;)Landroid/os/RemoteException;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;->a(Landroid/os/RemoteException;)V

    .line 46
    return-void
.end method
