.class public final Lcom/google/android/gms/games/internal/b/g;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 31
    packed-switch p0, :pswitch_data_0

    .line 37
    const-string v0, "RequestType"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown request type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    const-string v0, "UNKNOWN_TYPE"

    :goto_0
    return-object v0

    .line 33
    :pswitch_0
    const-string v0, "GIFT"

    goto :goto_0

    .line 35
    :pswitch_1
    const-string v0, "WISH"

    goto :goto_0

    .line 31
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
