.class public interface abstract Lcom/google/android/gms/games/Player;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/gms/common/data/u;


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public abstract a(Landroid/database/CharArrayBuffer;)V
.end method

.method public abstract e()Landroid/net/Uri;
.end method

.method public abstract f()Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract g()Landroid/net/Uri;
.end method

.method public abstract h()Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract i()J
.end method

.method public abstract j()J
.end method

.method public abstract k()I
.end method

.method public abstract l()Z
.end method

.method public abstract m()Ljava/lang/String;
.end method

.method public abstract n()Lcom/google/android/gms/games/PlayerLevelInfo;
.end method

.method public abstract o()Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;
.end method

.method public abstract v_()Ljava/lang/String;
.end method
