.class public abstract Lcom/google/android/gms/games/internal/d/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicReference;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/internal/d/d;->a:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method


# virtual methods
.method protected abstract a()Lcom/google/android/gms/games/internal/d/a;
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/games/internal/d/d;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/d/a;

    .line 38
    if-nez v0, :cond_2

    .line 39
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/d/d;->a()Lcom/google/android/gms/games/internal/d/a;

    move-result-object v0

    .line 40
    iget-object v1, p0, Lcom/google/android/gms/games/internal/d/d;->a:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/games/internal/d/d;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/d/a;

    move-object v1, v0

    .line 45
    :goto_0
    iget-object v2, v1, Lcom/google/android/gms/games/internal/d/a;->b:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-boolean v0, v1, Lcom/google/android/gms/games/internal/d/a;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/android/gms/games/internal/d/a;->d:Z

    iget-object v0, v1, Lcom/google/android/gms/games/internal/d/a;->c:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/gms/games/internal/d/b;

    invoke-direct {v3, v1}, Lcom/google/android/gms/games/internal/d/b;-><init>(Lcom/google/android/gms/games/internal/d/a;)V

    iget v4, v1, Lcom/google/android/gms/games/internal/d/a;->f:I

    int-to-long v4, v4

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    iget-object v0, v1, Lcom/google/android/gms/games/internal/d/a;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicInteger;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iget-object v1, v1, Lcom/google/android/gms/games/internal/d/a;->e:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-virtual {v0, p2}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/games/internal/d/d;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/d/a;

    .line 25
    if-eqz v0, :cond_0

    .line 26
    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/d/a;->a()V

    .line 28
    :cond_0
    return-void
.end method
