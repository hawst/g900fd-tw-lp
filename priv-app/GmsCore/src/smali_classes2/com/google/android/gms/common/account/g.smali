.class public final Lcom/google/android/gms/common/account/g;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:Landroid/content/Context;

.field private final e:Landroid/view/LayoutInflater;

.field private f:[Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;I[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 32
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/account/g;->a:Ljava/lang/Object;

    .line 41
    iput-object p1, p0, Lcom/google/android/gms/common/account/g;->d:Landroid/content/Context;

    .line 42
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/account/g;->e:Landroid/view/LayoutInflater;

    .line 43
    iput-object p2, p0, Lcom/google/android/gms/common/account/g;->b:Ljava/lang/String;

    .line 44
    iput p3, p0, Lcom/google/android/gms/common/account/g;->c:I

    .line 45
    iput-object p4, p0, Lcom/google/android/gms/common/account/g;->f:[Ljava/lang/String;

    .line 46
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Ljava/lang/String;I[Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/common/account/g;-><init>(Landroid/content/Context;Ljava/lang/String;I[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/common/account/g;->f:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/gms/common/account/g;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/common/account/g;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/a;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/a;->a(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/gms/common/account/g;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/common/account/g;->f:[Ljava/lang/String;

    invoke-static {v1, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iput-object v1, p0, Lcom/google/android/gms/common/account/g;->f:[Ljava/lang/String;

    const/4 v0, 0x1

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/g;->notifyDataSetChanged()V

    .line 200
    :cond_1
    return-void

    .line 198
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a(Landroid/support/v7/app/a;Landroid/widget/AdapterView$OnItemSelectedListener;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 176
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/common/account/g;->f:[Ljava/lang/String;

    move v0, v1

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    aget-object v3, v2, v0

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v2, v0

    :goto_1
    sget v0, Lcom/google/android/gms/l;->aa:I

    invoke-virtual {p1, v0}, Landroid/support/v7/app/a;->a(I)V

    const/16 v0, 0x10

    const/16 v3, 0x18

    invoke-virtual {p1, v0, v3}, Landroid/support/v7/app/a;->a(II)V

    invoke-virtual {p1}, Landroid/support/v7/app/a;->b()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/google/android/gms/j;->w:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-virtual {v0, p2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setSelection(I)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 177
    return-void

    .line 176
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    move v2, v0

    goto :goto_1
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/common/account/g;->f:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 131
    if-nez p2, :cond_0

    .line 132
    iget-object v1, p0, Lcom/google/android/gms/common/account/g;->e:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/gms/l;->fq:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 136
    :goto_0
    :try_start_0
    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v1, v0

    iget-object v3, p0, Lcom/google/android/gms/common/account/g;->f:[Ljava/lang/String;

    aget-object v3, v3, p1

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 141
    :goto_1
    return-object v2

    .line 138
    :catch_0
    move-exception v1

    const-string v1, "GoogleAccountSpinnerAdapter"

    const-string v3, "drop down view didn\'t cast to TextView"

    invoke-static {v1, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_0
    move-object v2, p2

    goto :goto_0
.end method

.method public final bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gms/common/account/g;->f:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/common/account/g;->f:[Ljava/lang/String;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 92
    if-nez p2, :cond_0

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/common/account/g;->e:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->ab:I

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 98
    :cond_0
    iget v0, p0, Lcom/google/android/gms/common/account/g;->c:I

    if-eqz v0, :cond_1

    .line 100
    :try_start_0
    sget v0, Lcom/google/android/gms/j;->y:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 101
    iget-object v1, p0, Lcom/google/android/gms/common/account/g;->d:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/q;->i:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 103
    iget v1, p0, Lcom/google/android/gms/common/account/g;->c:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    :cond_1
    :goto_0
    :try_start_1
    sget v0, Lcom/google/android/gms/j;->x:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 111
    iget-object v1, p0, Lcom/google/android/gms/common/account/g;->d:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/q;->h:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 113
    iget-object v1, p0, Lcom/google/android/gms/common/account/g;->f:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_1

    .line 119
    :goto_1
    return-object p2

    .line 105
    :catch_0
    move-exception v0

    const-string v0, "GoogleAccountSpinnerAdapter"

    const-string v1, "title field didn\'t cast to TextView"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 116
    :catch_1
    move-exception v0

    const-string v0, "GoogleAccountSpinnerAdapter"

    const-string v1, "subtitle field didn\'t cast to TextView"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x1

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x1

    return v0
.end method
