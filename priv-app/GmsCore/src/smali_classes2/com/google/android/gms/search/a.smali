.class public abstract Lcom/google/android/gms/search/a;
.super Lcom/google/android/gms/icing/b/h;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/android/gms/icing/impl/u;

.field protected final b:Ljava/lang/Object;

.field protected final c:Lcom/google/android/gms/icing/impl/a/h;


# direct methods
.method public constructor <init>(IILcom/google/android/gms/icing/impl/u;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 87
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/b/h;-><init>(I)V

    .line 88
    iput-object p3, p0, Lcom/google/android/gms/search/a;->a:Lcom/google/android/gms/icing/impl/u;

    .line 89
    iput-object p5, p0, Lcom/google/android/gms/search/a;->b:Ljava/lang/Object;

    .line 90
    invoke-virtual {p3, p4}, Lcom/google/android/gms/icing/impl/u;->l(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/search/a;->c:Lcom/google/android/gms/icing/impl/a/h;

    .line 92
    iget-object v3, p0, Lcom/google/android/gms/search/a;->c:Lcom/google/android/gms/icing/impl/a/h;

    sget v0, Lcom/google/android/gms/search/b;->b:I

    xor-int/lit8 v0, v0, -0x1

    and-int/2addr v0, p2

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    const-string v4, "Invalid permissions: %d"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v0, v4, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-boolean v0, v3, Lcom/google/android/gms/icing/impl/a/h;->b:Z

    if-eqz v0, :cond_0

    const/16 v1, 0xf

    :cond_0
    iget-boolean v0, v3, Lcom/google/android/gms/icing/impl/a/h;->c:Z

    if-eqz v0, :cond_1

    or-int/lit8 v1, v1, 0x2

    :cond_1
    xor-int/lit8 v0, v1, -0x1

    and-int/2addr v0, p2

    if-eqz v0, :cond_3

    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Access denied. Not authorized for: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/search/b;->a(I)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    move v0, v2

    goto :goto_0

    .line 93
    :cond_3
    return-void
.end method


# virtual methods
.method protected final a(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 98
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/search/a;->b(Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    :goto_0
    return-void

    .line 99
    :catch_0
    move-exception v0

    const-string v1, "Task %s failed to deliver result for request %s."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/search/a;->c:Lcom/google/android/gms/icing/impl/a/h;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method protected b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 115
    return-void
.end method
