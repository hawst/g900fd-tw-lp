.class public final Lcom/google/android/gms/games/h/a/cs;
.super Lcom/google/android/gms/common/server/response/a;
.source "SourceFile"


# static fields
.field private static final b:Lcom/google/android/gms/games/h/e/f;

.field private static final c:Ljava/util/HashMap;


# instance fields
.field private final d:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 104
    new-instance v0, Lcom/google/android/gms/games/h/e/f;

    invoke-direct {v0}, Lcom/google/android/gms/games/h/e/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/h/a/cs;->b:Lcom/google/android/gms/games/h/e/f;

    .line 107
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 109
    sput-object v0, Lcom/google/android/gms/games/h/a/cs;->c:Ljava/util/HashMap;

    const-string v1, "avatarImageUrl"

    const-string v2, "profile_icon_image_url"

    const-class v3, Lcom/google/android/gms/games/h/b/m;

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;Ljava/lang/Class;Z)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lcom/google/android/gms/games/h/a/cs;->c:Ljava/util/HashMap;

    const-string v1, "displayName"

    const-string v2, "profile_name"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v0, Lcom/google/android/gms/games/h/a/cs;->c:Ljava/util/HashMap;

    const-string v1, "experienceInfo"

    const-string v2, "experienceInfo"

    const-class v3, Lcom/google/android/gms/games/h/a/cy;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v0, Lcom/google/android/gms/games/h/a/cs;->c:Ljava/util/HashMap;

    const-string v1, "hasAllPublicAcls"

    const-string v2, "has_all_public_acls"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lcom/google/android/gms/games/h/a/cs;->c:Ljava/util/HashMap;

    const-string v1, "isCircled"

    const-string v2, "is_in_circles"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v0, Lcom/google/android/gms/games/h/a/cs;->c:Ljava/util/HashMap;

    const-string v1, "lastPlayedApp"

    const-string v2, "lastPlayedApp"

    const-class v3, Lcom/google/android/gms/games/h/a/cr;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lcom/google/android/gms/games/h/a/cs;->c:Ljava/util/HashMap;

    const-string v1, "lastPlayedWith"

    const-string v2, "lastPlayedWith"

    const-class v3, Lcom/google/android/gms/games/h/a/cq;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    sget-object v0, Lcom/google/android/gms/games/h/a/cs;->c:Ljava/util/HashMap;

    const-string v1, "playerId"

    const-string v2, "external_player_id"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v0, Lcom/google/android/gms/games/h/a/cs;->c:Ljava/util/HashMap;

    const-string v1, "profileSettings"

    const-string v2, "profileSettings"

    const-class v3, Lcom/google/android/gms/games/h/a/dk;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v0, Lcom/google/android/gms/games/h/a/cs;->c:Ljava/util/HashMap;

    const-string v1, "title"

    const-string v2, "player_title"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/a;-><init>()V

    .line 182
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/h/a/cs;->d:Ljava/util/HashMap;

    .line 137
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 134
    sget-object v0, Lcom/google/android/gms/games/h/a/cs;->c:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/cs;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    return-void
.end method

.method public final bridge synthetic ag_()Lcom/google/android/gms/common/server/response/s;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/gms/games/h/a/cs;->b:Lcom/google/android/gms/games/h/e/f;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    const-string v1, "profile_name"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    const-string v1, "is_in_circles"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method protected final d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/cs;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final getExperienceInfo()Lcom/google/android/gms/games/h/a/cy;
    .locals 2
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/cs;->d:Ljava/util/HashMap;

    const-string v1, "experienceInfo"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/cy;

    return-object v0
.end method

.method public final getLastPlayedApp()Lcom/google/android/gms/games/h/a/cr;
    .locals 2
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/cs;->d:Ljava/util/HashMap;

    const-string v1, "lastPlayedApp"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/cr;

    return-object v0
.end method

.method public final getLastPlayedWith()Lcom/google/android/gms/games/h/a/cq;
    .locals 2
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/cs;->d:Ljava/util/HashMap;

    const-string v1, "lastPlayedWith"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/cq;

    return-object v0
.end method

.method public final getProfileSettings()Lcom/google/android/gms/games/h/a/dk;
    .locals 2
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/cs;->d:Ljava/util/HashMap;

    const-string v1, "profileSettings"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/dk;

    return-object v0
.end method
