.class public final Lcom/google/android/gms/games/provider/bg;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/Map;

.field private c:J

.field private final d:Ljava/io/File;

.field private final e:Lcom/google/android/gms/games/provider/az;

.field private f:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/games/provider/bg;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Lcom/google/android/gms/games/provider/az;)V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/games/provider/bg;->c:J

    .line 60
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide an existing directory!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 62
    iput-object p1, p0, Lcom/google/android/gms/games/provider/bg;->d:Ljava/io/File;

    .line 63
    iput-object p2, p0, Lcom/google/android/gms/games/provider/bg;->e:Lcom/google/android/gms/games/provider/az;

    .line 64
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/bg;->b:Ljava/util/Map;

    .line 65
    invoke-direct {p0}, Lcom/google/android/gms/games/provider/bg;->a()V

    .line 66
    return-void

    .line 60
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(JZ)I
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 351
    iget-object v0, p0, Lcom/google/android/gms/games/provider/bg;->b:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/provider/bh;

    .line 352
    if-eqz v0, :cond_0

    .line 353
    iget-wide v4, p0, Lcom/google/android/gms/games/provider/bg;->c:J

    iget-wide v6, v0, Lcom/google/android/gms/games/provider/bh;->b:J

    sub-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/gms/games/provider/bg;->c:J

    .line 354
    iget-object v3, p0, Lcom/google/android/gms/games/provider/bg;->b:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    :cond_0
    if-eqz p3, :cond_1

    .line 357
    iget-object v0, p0, Lcom/google/android/gms/games/provider/bg;->f:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "images"

    const-string v4, "_id=?"

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-virtual {v0, v3, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 359
    :goto_0
    return v0

    :cond_1
    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public static a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .prologue
    .line 76
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "images/games/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 77
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 78
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_0

    .line 79
    const/4 v0, 0x0

    .line 82
    :cond_0
    return-object v0
.end method

.method private a()V
    .locals 8

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/gms/games/provider/bg;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 122
    if-nez v1, :cond_0

    .line 137
    :goto_0
    return-void

    .line 125
    :cond_0
    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 127
    :try_start_0
    new-instance v4, Lcom/google/android/gms/games/provider/bh;

    invoke-direct {v4, v3}, Lcom/google/android/gms/games/provider/bh;-><init>(Ljava/io/File;)V

    .line 128
    iget-wide v6, v4, Lcom/google/android/gms/games/provider/bh;->a:J

    invoke-direct {p0, v6, v7, v4}, Lcom/google/android/gms/games/provider/bg;->a(JLcom/google/android/gms/games/provider/bh;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 131
    :catch_0
    move-exception v4

    invoke-static {v3}, Lcom/google/android/gms/games/provider/bg;->a(Ljava/io/File;)V

    goto :goto_2

    .line 136
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/provider/bg;->e:Lcom/google/android/gms/games/provider/az;

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/az;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/provider/bg;->f:Landroid/database/sqlite/SQLiteDatabase;

    goto :goto_0
.end method

.method private a(JLcom/google/android/gms/games/provider/bh;)V
    .locals 7

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/gms/games/provider/bg;->b:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 335
    iget-wide v0, p0, Lcom/google/android/gms/games/provider/bg;->c:J

    iget-wide v2, p3, Lcom/google/android/gms/games/provider/bh;->b:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/games/provider/bg;->c:J

    .line 340
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/provider/bg;->b:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 341
    return-void

    .line 337
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/provider/bg;->b:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/provider/bh;

    .line 338
    iget-wide v2, p0, Lcom/google/android/gms/games/provider/bg;->c:J

    iget-wide v4, p3, Lcom/google/android/gms/games/provider/bh;->b:J

    iget-wide v0, v0, Lcom/google/android/gms/games/provider/bh;->b:J

    sub-long v0, v4, v0

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/games/provider/bg;->c:J

    goto :goto_0
.end method

.method private static a(Ljava/io/File;)V
    .locals 3

    .prologue
    .line 292
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v0

    .line 293
    if-nez v0, :cond_0

    .line 294
    const-string v0, "ImageStore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not clean up file "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    :cond_0
    return-void
.end method

.method private c(J)Ljava/io/File;
    .locals 3

    .prologue
    .line 325
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/gms/games/provider/bg;->d:Ljava/io/File;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a([BLjava/lang/String;J)J
    .locals 11

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 188
    if-eqz p1, :cond_2

    .line 194
    :try_start_0
    const-string v1, "img"

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gms/games/provider/bg;->d:Ljava/io/File;

    invoke-static {v1, v4, v5}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 195
    :try_start_1
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 196
    invoke-virtual {v0, p1}, Ljava/io/FileOutputStream;->write([B)V

    .line 197
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    .line 200
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 201
    const-string v1, "url"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    const-string v1, "local"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 203
    const-string v1, "filesize"

    array-length v5, p1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 204
    const-string v1, "download_timestamp"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 205
    iget-object v1, p0, Lcom/google/android/gms/games/provider/bg;->f:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "images"

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 206
    cmp-long v5, v0, v2

    if-eqz v5, :cond_0

    .line 208
    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/bg;->c(J)Ljava/io/File;

    move-result-object v5

    .line 209
    invoke-virtual {v4, v5}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 210
    new-instance v6, Lcom/google/android/gms/games/provider/bh;

    invoke-direct {v6, v5}, Lcom/google/android/gms/games/provider/bh;-><init>(Ljava/io/File;)V

    .line 211
    iget-wide v8, v6, Lcom/google/android/gms/games/provider/bh;->a:J

    invoke-direct {p0, v8, v9, v6}, Lcom/google/android/gms/games/provider/bg;->a(JLcom/google/android/gms/games/provider/bh;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 236
    :goto_0
    return-wide v0

    :catch_0
    move-exception v1

    :goto_1
    move-object v4, v0

    .line 220
    :cond_0
    if-eqz v4, :cond_1

    .line 221
    invoke-static {v4}, Lcom/google/android/gms/games/provider/bg;->a(Ljava/io/File;)V

    :cond_1
    move-wide v0, v2

    .line 236
    goto :goto_0

    .line 225
    :cond_2
    if-nez p2, :cond_3

    .line 226
    const-string v0, "ImageStore"

    const-string v1, "Attempted to insert image with no bytes and no URL!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v0, v2

    .line 227
    goto :goto_0

    .line 231
    :cond_3
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 232
    const-string v2, "url"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    const-string v2, "local"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 234
    iget-object v2, p0, Lcom/google/android/gms/games/provider/bg;->f:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "images"

    invoke-virtual {v2, v3, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v0, v4

    goto :goto_1
.end method

.method public final a(J)Lcom/google/android/gms/games/provider/bh;
    .locals 3

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/gms/games/provider/bg;->b:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/provider/bh;

    return-object v0
.end method

.method public final a(Ljava/util/Set;)Ljava/util/Set;
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 151
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 152
    iget-object v1, p0, Lcom/google/android/gms/games/provider/bg;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 153
    invoke-interface {v0, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 154
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 155
    const-string v1, "ImageStore"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "cleanup removing "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " files"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 160
    invoke-direct {p0, v4, v5}, Lcom/google/android/gms/games/provider/bg;->c(J)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/games/provider/bg;->a(Ljava/io/File;)V

    invoke-direct {p0, v4, v5, v6}, Lcom/google/android/gms/games/provider/bg;->a(JZ)I

    goto :goto_0

    .line 164
    :cond_0
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 165
    invoke-interface {v8, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/games/provider/bg;->f:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "images"

    sget-object v2, Lcom/google/android/gms/games/provider/bg;->a:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 168
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 169
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 172
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 174
    return-object v8
.end method

.method public final a(J[BJ)Z
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 251
    .line 254
    :try_start_0
    const-string v3, "img"

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gms/games/provider/bg;->d:Ljava/io/File;

    invoke-static {v3, v4, v5}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 255
    :try_start_1
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 256
    invoke-virtual {v3, p3}, Ljava/io/FileOutputStream;->write([B)V

    .line 257
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 262
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 263
    const-string v4, "local"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 264
    const-string v4, "filesize"

    array-length v5, p3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 265
    const-string v4, "download_timestamp"

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 266
    iget-object v4, p0, Lcom/google/android/gms/games/provider/bg;->f:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "images"

    const-string v6, "_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    int-to-long v4, v3

    .line 268
    const-wide/16 v6, 0x1

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 270
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/provider/bg;->c(J)Ljava/io/File;

    move-result-object v3

    .line 271
    invoke-virtual {v2, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 273
    new-instance v4, Lcom/google/android/gms/games/provider/bh;

    invoke-direct {v4, v3}, Lcom/google/android/gms/games/provider/bh;-><init>(Ljava/io/File;)V

    .line 274
    invoke-direct {p0, p1, p2, v4}, Lcom/google/android/gms/games/provider/bg;->a(JLcom/google/android/gms/games/provider/bh;)V

    .line 288
    :goto_0
    return v0

    .line 278
    :cond_0
    const-string v0, "ImageStore"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " rows updated, expected 1"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 285
    :cond_1
    :goto_1
    if-eqz v2, :cond_2

    .line 286
    invoke-static {v2}, Lcom/google/android/gms/games/provider/bg;->a(Ljava/io/File;)V

    :cond_2
    move v0, v1

    .line 288
    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v2

    :goto_2
    move-object v2, v0

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v0, v2

    goto :goto_2
.end method

.method public final b(J)I
    .locals 1

    .prologue
    .line 304
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/provider/bg;->c(J)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/games/provider/bg;->a(Ljava/io/File;)V

    .line 305
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/provider/bg;->a(JZ)I

    move-result v0

    return v0
.end method
