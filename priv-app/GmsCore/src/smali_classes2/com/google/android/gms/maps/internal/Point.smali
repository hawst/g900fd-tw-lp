.class public Lcom/google/android/gms/maps/internal/Point;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/maps/internal/cu;


# instance fields
.field private final a:I

.field private final b:Landroid/graphics/Point;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/gms/maps/internal/cu;

    invoke-direct {v0}, Lcom/google/android/gms/maps/internal/cu;-><init>()V

    sput-object v0, Lcom/google/android/gms/maps/internal/Point;->CREATOR:Lcom/google/android/gms/maps/internal/cu;

    return-void
.end method

.method public constructor <init>(ILandroid/graphics/Point;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput p1, p0, Lcom/google/android/gms/maps/internal/Point;->a:I

    .line 32
    iput-object p2, p0, Lcom/google/android/gms/maps/internal/Point;->b:Landroid/graphics/Point;

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Point;)V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/maps/internal/Point;-><init>(ILandroid/graphics/Point;)V

    .line 37
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/google/android/gms/maps/internal/Point;->a:I

    return v0
.end method

.method public final b()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/maps/internal/Point;->b:Landroid/graphics/Point;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 49
    if-ne p0, p1, :cond_0

    .line 50
    const/4 v0, 0x1

    .line 57
    :goto_0
    return v0

    .line 52
    :cond_0
    instance-of v0, p1, Lcom/google/android/gms/maps/internal/Point;

    if-nez v0, :cond_1

    .line 53
    const/4 v0, 0x0

    goto :goto_0

    .line 56
    :cond_1
    check-cast p1, Lcom/google/android/gms/maps/internal/Point;

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/maps/internal/Point;->b:Landroid/graphics/Point;

    iget-object v1, p1, Lcom/google/android/gms/maps/internal/Point;->b:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/maps/internal/Point;->b:Landroid/graphics/Point;

    invoke-virtual {v0}, Landroid/graphics/Point;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/maps/internal/Point;->b:Landroid/graphics/Point;

    invoke-virtual {v0}, Landroid/graphics/Point;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 77
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/maps/internal/cu;->a(Lcom/google/android/gms/maps/internal/Point;Landroid/os/Parcel;I)V

    .line 78
    return-void
.end method
