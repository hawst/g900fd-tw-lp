.class public final Lcom/google/android/gms/games/service/a/e/b;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:I

.field private final e:I

.field private final f:Z

.field private final g:Lcom/google/android/gms/games/a/au;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IIZ)V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 25
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/e/b;->c:Lcom/google/android/gms/games/internal/dr;

    .line 26
    iput p3, p0, Lcom/google/android/gms/games/service/a/e/b;->d:I

    .line 27
    iput p4, p0, Lcom/google/android/gms/games/service/a/e/b;->e:I

    .line 28
    iput-boolean p5, p0, Lcom/google/android/gms/games/service/a/e/b;->f:Z

    .line 29
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/e/b;->g:Lcom/google/android/gms/games/a/au;

    .line 30
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/e/b;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->h(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 42
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/e/b;->g:Lcom/google/android/gms/games/a/au;

    iget v1, p0, Lcom/google/android/gms/games/service/a/e/b;->d:I

    iget v2, p0, Lcom/google/android/gms/games/service/a/e/b;->e:I

    iget-boolean v3, p0, Lcom/google/android/gms/games/service/a/e/b;->f:Z

    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;IIZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
