.class public final Lcom/google/android/gms/games/service/af;
.super Lcom/google/android/gms/games/internal/ee;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/games/realtime/network/d;

.field private final c:Z

.field private d:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/google/android/gms/games/internal/ee;-><init>()V

    .line 115
    iput-object p1, p0, Lcom/google/android/gms/games/service/af;->a:Landroid/content/Context;

    .line 116
    new-instance v0, Lcom/google/android/gms/games/service/ag;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/service/ag;-><init>(Lcom/google/android/gms/games/service/af;)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/af;->b:Lcom/google/android/gms/games/realtime/network/d;

    .line 125
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/service/af;->c:Z

    .line 126
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RoomAndroidService: kill "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 188
    if-nez p1, :cond_0

    .line 189
    const-string v1, "RoomAndroidService"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    :goto_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    .line 194
    return-void

    .line 191
    :cond_0
    const-string v1, "RoomAndroidService"

    invoke-static {v1, v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/gms/games/service/af;->d:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomservice/k;

    iget-object v2, p0, Lcom/google/android/gms/games/service/af;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomservice/k;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 174
    return-void
.end method

.method public final a(Landroid/os/IBinder;Lcom/google/android/gms/games/internal/eg;)V
    .locals 3

    .prologue
    .line 147
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    :try_start_0
    new-instance v0, Lcom/google/android/gms/games/service/ah;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/service/ah;-><init>(Lcom/google/android/gms/games/service/af;)V

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/af;->d:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    if-nez v0, :cond_0

    .line 160
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    iget-object v1, p0, Lcom/google/android/gms/games/service/af;->b:Lcom/google/android/gms/games/realtime/network/d;

    iget-boolean v2, p0, Lcom/google/android/gms/games/service/af;->c:Z

    invoke-direct {v0, p2, v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;-><init>(Lcom/google/android/gms/games/internal/eg;Lcom/google/android/gms/games/realtime/network/d;Z)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/af;->d:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/games/service/af;->d:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->d()V

    .line 165
    :cond_0
    :try_start_1
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    invoke-interface {p2, v0}, Lcom/google/android/gms/games/internal/eg;->a(Landroid/os/IBinder;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 169
    :goto_1
    return-void

    .line 156
    :catch_0
    move-exception v0

    .line 157
    const-string v1, "Linking to death"

    invoke-static {v1, v0}, Lcom/google/android/gms/games/service/af;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 166
    :catch_1
    move-exception v0

    .line 167
    const-string v1, "During setup"

    invoke-static {v1, v0}, Lcom/google/android/gms/games/service/af;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/data/DataHolder;Z)V
    .locals 3

    .prologue
    .line 217
    const/4 v0, 0x0

    .line 218
    new-instance v1, Lcom/google/android/gms/games/multiplayer/realtime/c;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/multiplayer/realtime/c;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 220
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/realtime/c;->c()I

    move-result v2

    if-lez v2, :cond_0

    .line 221
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/multiplayer/realtime/c;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/realtime/Room;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/realtime/c;->w_()V

    .line 227
    if-eqz v0, :cond_1

    .line 228
    iget-object v1, p0, Lcom/google/android/gms/games/service/af;->d:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    new-instance v2, Lcom/google/android/gms/games/service/statemachine/roomservice/i;

    invoke-direct {v2, v0, p2}, Lcom/google/android/gms/games/service/statemachine/roomservice/i;-><init>(Lcom/google/android/gms/games/multiplayer/realtime/Room;Z)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 230
    :cond_1
    return-void

    .line 224
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/realtime/c;->w_()V

    throw v0
.end method

.method public final a(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 293
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 295
    iget-object v0, p0, Lcom/google/android/gms/games/service/af;->d:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/google/android/gms/games/service/af;->d:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 298
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/android/gms/games/service/af;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 184
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/gms/games/service/af;->d:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomservice/c;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/games/service/statemachine/roomservice/c;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 287
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/gms/games/service/af;->d:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomservice/b;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/gms/games/service/statemachine/roomservice/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 207
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/gms/games/service/af;->d:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomservice/h;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/service/statemachine/roomservice/h;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 240
    return-void
.end method

.method public final a([BLjava/lang/String;I)V
    .locals 2

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/gms/games/service/af;->d:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomservice/q;

    invoke-direct {v1, p3, p1, p2}, Lcom/google/android/gms/games/service/statemachine/roomservice/q;-><init>(I[BLjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 252
    return-void
.end method

.method public final a([B[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/gms/games/service/af;->d:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomservice/r;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/games/service/statemachine/roomservice/r;-><init>([B[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 275
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/gms/games/service/af;->d:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomservice/t;

    iget-object v2, p0, Lcom/google/android/gms/games/service/af;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomservice/t;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 179
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 257
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/games/service/RoomAndroidService;->a(Ljava/lang/String;)Lcom/google/android/gms/games/h/a/ev;

    move-result-object v0

    .line 258
    iget-object v1, p0, Lcom/google/android/gms/games/service/af;->d:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    new-instance v2, Lcom/google/android/gms/games/service/statemachine/roomservice/s;

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/s;-><init>(Lcom/google/android/gms/games/h/a/ev;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V
    :try_end_0
    .catch Lcom/google/android/gms/common/server/response/m; {:try_start_0 .. :try_end_0} :catch_0

    .line 262
    :goto_0
    return-void

    .line 260
    :catch_0
    move-exception v0

    const-string v0, "RoomAndroidService"

    const-string v1, "Room status could not be parsed."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/android/gms/games/service/af;->d:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomservice/d;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/games/service/statemachine/roomservice/d;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 281
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/gms/games/service/af;->d:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomservice/g;

    invoke-direct {v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/g;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 212
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/gms/games/service/af;->d:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomservice/j;

    invoke-direct {v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/j;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 235
    return-void
.end method
