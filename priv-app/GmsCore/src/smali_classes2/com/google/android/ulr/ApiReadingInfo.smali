.class public final Lcom/google/android/ulr/ApiReadingInfo;
.super Lcom/google/android/gms/common/server/response/c;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# static fields
.field private static final b:Ljava/util/HashMap;


# instance fields
.field private final c:Ljava/util/HashMap;

.field private final d:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 32
    sput-object v0, Lcom/google/android/ulr/ApiReadingInfo;->b:Ljava/util/HashMap;

    const-string v1, "batteryCharging"

    const-string v2, "batteryCharging"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object v0, Lcom/google/android/ulr/ApiReadingInfo;->b:Ljava/util/HashMap;

    const-string v1, "batteryCondition"

    const-string v2, "batteryCondition"

    const-class v3, Lcom/google/android/ulr/ApiBatteryCondition;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v0, Lcom/google/android/ulr/ApiReadingInfo;->b:Ljava/util/HashMap;

    const-string v1, "batteryLevel"

    const-string v2, "batteryLevel"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v0, Lcom/google/android/ulr/ApiReadingInfo;->b:Ljava/util/HashMap;

    const-string v1, "source"

    const-string v2, "source"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    sget-object v0, Lcom/google/android/ulr/ApiReadingInfo;->b:Ljava/util/HashMap;

    const-string v1, "wifiScans"

    const-string v2, "wifiScans"

    const-class v3, Lcom/google/android/ulr/WifiStrengthProto;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 73
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/ulr/ApiReadingInfo;->c:Ljava/util/HashMap;

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/ulr/ApiReadingInfo;->d:Ljava/util/HashMap;

    .line 47
    return-void
.end method

.method public constructor <init>(Lcom/google/android/ulr/ApiBatteryCondition;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 73
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/ulr/ApiReadingInfo;->c:Ljava/util/HashMap;

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/ulr/ApiReadingInfo;->d:Ljava/util/HashMap;

    .line 56
    if-eqz p1, :cond_0

    .line 60
    const-string v0, "batteryCondition"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/ulr/ApiReadingInfo;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    .line 62
    :cond_0
    if-eqz p2, :cond_1

    .line 66
    const-string v0, "source"

    invoke-virtual {p0, v0, p2}, Lcom/google/android/ulr/ApiReadingInfo;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :cond_1
    if-eqz p3, :cond_2

    .line 69
    const-string v0, "wifiScans"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/ulr/ApiReadingInfo;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 71
    :cond_2
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/google/android/ulr/ApiReadingInfo;->b:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/ulr/ApiReadingInfo;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/ulr/ApiReadingInfo;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    return-void
.end method

.method protected final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/ulr/ApiReadingInfo;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/ulr/ApiReadingInfo;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final getBatteryCondition()Lcom/google/android/ulr/ApiBatteryCondition;
    .locals 2
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/ulr/ApiReadingInfo;->c:Ljava/util/HashMap;

    const-string v1, "batteryCondition"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ulr/ApiBatteryCondition;

    return-object v0
.end method

.method public final getWifiScans()Ljava/util/ArrayList;
    .locals 2
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/ulr/ApiReadingInfo;->d:Ljava/util/HashMap;

    const-string v1, "wifiScans"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method
