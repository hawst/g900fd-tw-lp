.class public final Lcom/google/android/libraries/social/autobackup/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field private static final d:Ljava/lang/String;

.field private static final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    sget-object v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/g/a/d;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/autobackup/ab;->a:Ljava/lang/String;

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "upload_account_id = -1 AND media_url NOT IN ( SELECT media_url FROM "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/libraries/social/autobackup/ab;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE upload_account_id = ? )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND (bucket_id IS null OR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "bucket_id NOT IN ( SELECT bucket_id FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "exclude_bucket ))"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/autobackup/ab;->b:Ljava/lang/String;

    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SELECT count(*) FROM "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/g/a/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE upload_account_id = ? AND upload_state = 100"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/autobackup/ab;->c:Ljava/lang/String;

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SELECT COUNT(*) FROM "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/libraries/social/autobackup/ab;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE upload_account_id = ? AND ( upload_state = 100 OR upload_state = 200 ) AND upload_reason = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/autobackup/ab;->d:Ljava/lang/String;

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "upload_account_id = -1 AND bucket_id = ? AND media_url NOT IN ( SELECT media_url FROM "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/libraries/social/autobackup/ab;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE upload_account_id = ? )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/autobackup/ab;->e:Ljava/lang/String;

    return-void
.end method

.method static a(Lcom/google/android/libraries/social/autobackup/au;)I
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v10, 0x0

    const/4 v5, 0x0

    .line 550
    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/au;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/social/autobackup/ab;->a:Ljava/lang/String;

    new-array v3, v1, [Ljava/lang/String;

    const-string v4, "COUNT(*)"

    aput-object v4, v3, v10

    const-string v4, "upload_account_id = -1"

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    move-object v9, v5

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 559
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 561
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return v0

    :cond_0
    move v0, v10

    .line 559
    goto :goto_0

    .line 561
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;ILjava/lang/String;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 353
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 378
    :goto_0
    return-void

    .line 357
    :cond_0
    sget-object v1, Lcom/google/android/libraries/social/autobackup/ab;->a:Ljava/lang/String;

    sget-object v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/g/a/d;->b()[Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/social/autobackup/ab;->e:Ljava/lang/String;

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p2, v4, v0

    const/4 v0, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 362
    :cond_1
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 363
    invoke-static {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(Landroid/database/Cursor;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    .line 364
    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->id:J

    .line 365
    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->e(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 366
    const/16 v2, 0x1e

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 367
    const/16 v2, 0x64

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 369
    sget-object v2, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-virtual {v2, p0, v0}, Lcom/google/android/libraries/social/g/a/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/libraries/social/g/a/a;)J

    .line 371
    const-string v2, "iu.UploadsManager"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 372
    const-string v2, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ADD; upload media id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "; iu: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 377
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 385
    sget-object v0, Lcom/google/android/libraries/social/autobackup/ab;->a:Ljava/lang/String;

    const-string v1, "upload_account_id != -1 AND bucket_id = ? AND upload_state != 400"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 387
    return-void
.end method

.method public static a(Lcom/google/android/libraries/social/autobackup/au;I)V
    .locals 4

    .prologue
    .line 485
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 493
    :goto_0
    return-void

    .line 488
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 492
    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/au;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, Lcom/google/android/libraries/social/autobackup/ab;->a:Ljava/lang/String;

    const-string v3, "upload_account_id = ? AND upload_state = 100"

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method static a(Lcom/google/android/libraries/social/autobackup/au;II)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 459
    const/16 v0, 0x28

    if-eq p2, v0, :cond_0

    const/16 v0, 0x1e

    if-eq p2, v0, :cond_0

    .line 461
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "only REASON_UPLOAD_ALL and REASON_INSTANT_UPLOAD supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 465
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/au;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/social/autobackup/ab;->a:Ljava/lang/String;

    sget-object v2, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/g/a/d;->b()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "upload_account_id = ? AND ( upload_state = 100 OR upload_state = 200 ) AND upload_reason = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 471
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 472
    invoke-static {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(Landroid/database/Cursor;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    .line 473
    sget-object v2, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/au;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    iget-wide v4, v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->id:J

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/libraries/social/g/a/d;->a(Landroid/database/sqlite/SQLiteDatabase;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 477
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 478
    return-void
.end method

.method static a(Landroid/content/ContentResolver;Landroid/net/Uri;)Z
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v1, 0x0

    .line 721
    const-string v0, "_data"

    invoke-static {p0, p1, v0}, Lcom/google/android/libraries/social/autobackup/util/b;->b(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 725
    if-eqz v2, :cond_5

    .line 727
    const/16 v0, 0x2e

    invoke-virtual {v2, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 728
    if-ltz v0, :cond_0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 729
    :goto_0
    const-string v3, "jpg"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "jpeg"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 759
    :goto_1
    return v0

    .line 728
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 734
    :cond_1
    :try_start_0
    new-instance v0, Lcom/google/android/libraries/social/e/c;

    invoke-direct {v0}, Lcom/google/android/libraries/social/e/c;-><init>()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 736
    :try_start_1
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/e/c;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 744
    :try_start_2
    sget v3, Lcom/google/android/libraries/social/e/c;->s:I

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/e/c;->b(I)Ljava/lang/String;

    move-result-object v0

    .line 746
    if-eqz v0, :cond_5

    const-string v3, "Google"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-ltz v3, :cond_5

    .line 747
    const-string v3, "iu.UploadsManager"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 748
    const-string v3, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "*** Found Google EXIF tag; value: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 750
    :cond_2
    const/4 v0, 0x1

    goto :goto_1

    .line 738
    :catch_0
    move-exception v0

    const-string v0, "iu.UploadsManager"

    const/4 v3, 0x4

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 739
    const-string v0, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "INFO: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " does not contain any EXIF data"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_3
    move v0, v1

    .line 741
    goto :goto_1

    .line 752
    :catch_1
    move-exception v0

    .line 753
    const-string v3, "iu.UploadsManager"

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 754
    const-string v3, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "INFO: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " error getting EXIF; "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    move v0, v1

    .line 756
    goto/16 :goto_1

    :cond_5
    move v0, v1

    .line 759
    goto/16 :goto_1
.end method

.method static a(Landroid/content/Context;)Z
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 441
    const-class v0, Lcom/google/android/libraries/social/autobackup/au;

    invoke-static {p0, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/au;

    .line 443
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/au;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/libraries/social/autobackup/ab;->a:Ljava/lang/String;

    sget-object v3, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/g/a/d;->b()[Ljava/lang/String;

    move-result-object v3

    const-string v4, "upload_reason = 30 AND upload_state = 400"

    const-string v9, "1"

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 448
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 450
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return v1

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static a(Landroid/content/Context;I)Z
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 398
    const-class v0, Lcom/google/android/libraries/social/autobackup/au;

    invoke-static {p0, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/au;

    .line 402
    const/4 v1, -0x1

    if-ne p1, v1, :cond_2

    .line 403
    const-string v2, "upload_account_id != -1 AND upload_state = 200"

    .line 405
    const/4 v1, 0x0

    .line 415
    :goto_0
    const-string v5, "iu.UploadsManager"

    invoke-static {v5, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 421
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SELECT COUNT(*) FROM "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, Lcom/google/android/libraries/social/autobackup/ab;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " WHERE "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 423
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/au;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    invoke-static {v6, v5, v1}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v6

    .line 425
    const-string v5, "iu.UploadsManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "num queued entries: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    :cond_0
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 429
    const-string v6, "upload_state"

    const/16 v7, 0x64

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 430
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/au;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v6, Lcom/google/android/libraries/social/autobackup/ab;->a:Ljava/lang/String;

    invoke-virtual {v0, v6, v5, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 433
    const-string v1, "iu.UploadsManager"

    invoke-static {v1, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 434
    const-string v1, "iu.UploadsManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "num updated entries: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    :cond_1
    if-lez v0, :cond_3

    move v0, v3

    :goto_1
    return v0

    .line 407
    :cond_2
    const-string v2, "upload_account_id = ? AND upload_state = 200"

    .line 413
    new-array v1, v3, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    goto/16 :goto_0

    :cond_3
    move v0, v4

    .line 437
    goto :goto_1
.end method

.method private static a(Landroid/content/Context;ILandroid/content/ContentResolver;Lcom/google/android/libraries/social/autobackup/ao;Landroid/net/Uri;Z)Z
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    const/4 v8, 0x4

    const/4 v1, 0x0

    .line 625
    const-string v0, "MMM dd, yyyy h:mmaa"

    .line 628
    const-string v2, "datetaken"

    invoke-static {p2, p4, v2}, Lcom/google/android/libraries/social/autobackup/util/b;->a(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v2

    .line 630
    cmp-long v4, v2, v6

    if-lez v4, :cond_2

    invoke-interface {p3}, Lcom/google/android/libraries/social/autobackup/ao;->i()J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-gtz v4, :cond_0

    invoke-interface {p3}, Lcom/google/android/libraries/social/autobackup/ao;->h()J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-gez v4, :cond_2

    .line 632
    :cond_0
    const-string v4, "iu.UploadsManager"

    invoke-static {v4, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 633
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-static {v0, v4}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 635
    const-string v2, "iu.UploadsManager"

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 636
    const-string v2, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FAIL: bad taken time; taken: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    move v0, v1

    .line 709
    :goto_0
    return v0

    .line 643
    :cond_2
    const-string v2, "_data"

    invoke-static {p2, p4, v2}, Lcom/google/android/libraries/social/autobackup/util/b;->b(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 645
    if-eqz v2, :cond_5

    .line 646
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 647
    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    .line 648
    cmp-long v3, v4, v6

    if-lez v3, :cond_5

    invoke-interface {p3}, Lcom/google/android/libraries/social/autobackup/ao;->i()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-gtz v3, :cond_3

    invoke-interface {p3}, Lcom/google/android/libraries/social/autobackup/ao;->h()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-gez v3, :cond_5

    .line 650
    :cond_3
    const-string v2, "iu.UploadsManager"

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 651
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-static {v0, v2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 653
    const-string v2, "iu.UploadsManager"

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 654
    const-string v2, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FAIL: bad modify time; modified: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    move v0, v1

    .line 657
    goto :goto_0

    .line 663
    :cond_5
    if-eqz v2, :cond_6

    const-string v0, "cache/com.google.android.googlephotos"

    invoke-virtual {v2, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_6

    .line 665
    const-string v0, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FAIL: file from cache directory; path: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 666
    goto :goto_0

    .line 671
    :cond_6
    if-eqz p5, :cond_c

    if-eqz v2, :cond_c

    .line 673
    const/16 v0, 0x2e

    invoke-virtual {v2, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 674
    if-ltz v0, :cond_8

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 675
    :goto_1
    const-string v3, "jpg"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string v3, "jpeg"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 676
    const-string v0, "iu.UploadsManager"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 677
    const-string v0, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FAIL: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not a jpeg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    move v0, v1

    .line 679
    goto/16 :goto_0

    .line 674
    :cond_8
    const-string v0, ""

    goto :goto_1

    .line 683
    :cond_9
    :try_start_0
    new-instance v0, Lcom/google/android/libraries/social/e/c;

    invoke-direct {v0}, Lcom/google/android/libraries/social/e/c;-><init>()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 685
    :try_start_1
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/e/c;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 693
    :try_start_2
    sget v3, Lcom/google/android/libraries/social/e/c;->s:I

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/e/c;->b(I)Ljava/lang/String;

    move-result-object v0

    .line 696
    if-eqz v0, :cond_c

    const-string v3, "Google"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_c

    .line 697
    const-string v0, "iu.UploadsManager"

    const/4 v3, 0x4

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 698
    const-string v0, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FAIL: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " has GOOGLE_EXIF_TAG set"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    move v0, v1

    .line 700
    goto/16 :goto_0

    .line 686
    :catch_0
    move-exception v0

    .line 687
    const-string v3, "iu.UploadsManager"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 688
    const-string v3, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "FAIL: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " does not contain any EXIF data"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_b
    move v0, v1

    .line 690
    goto/16 :goto_0

    .line 703
    :catch_1
    move-exception v0

    const-string v0, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FAIL: could get EXIF for file: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 704
    goto/16 :goto_0

    .line 708
    :cond_c
    const-class v0, Lcom/google/android/libraries/social/experiments/c;

    invoke-static {p0, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/experiments/c;

    .line 709
    sget-object v2, Lcom/google/android/libraries/social/autobackup/c;->a:Lcom/google/android/libraries/social/experiments/a;

    invoke-interface {v0, v2, p1}, Lcom/google/android/libraries/social/experiments/c;->a(Lcom/google/android/libraries/social/experiments/a;I)Z

    move-result v2

    if-eqz v2, :cond_e

    if-nez p5, :cond_d

    sget-object v2, Lcom/google/android/libraries/social/autobackup/c;->b:Lcom/google/android/libraries/social/experiments/a;

    invoke-interface {v0, v2, p1}, Lcom/google/android/libraries/social/experiments/c;->a(Lcom/google/android/libraries/social/experiments/a;I)Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_d
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_e
    move v0, v1

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/content/ContentResolver;Lcom/google/android/libraries/social/autobackup/ao;Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;JLandroid/net/Uri;ZZZ)Z
    .locals 10

    .prologue
    .line 199
    const-class v2, Lcom/google/android/libraries/social/autobackup/o;

    invoke-static {p0, v2}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/social/autobackup/o;

    .line 202
    invoke-virtual/range {p8 .. p8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    .line 205
    invoke-static {p3, v4}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v3

    .line 206
    if-eqz v3, :cond_1

    .line 207
    const-string v2, "iu.UploadsManager"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 208
    const-string v2, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SKIP; duplicate entry: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    :cond_0
    const/4 v2, 0x0

    .line 268
    :goto_0
    return v2

    .line 215
    :cond_1
    invoke-virtual {p4}, Landroid/content/ContentValues;->clear()V

    .line 216
    const-string v3, "album_id"

    invoke-virtual {p4, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 217
    const-string v3, "event_id"

    invoke-virtual {p4, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 218
    const-string v3, "upload_account_id"

    const/4 v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 219
    const-string v3, "bucket_id"

    invoke-virtual {p4, v3, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const-string v3, "is_image"

    invoke-static/range {p9 .. p9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {p4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 221
    const-string v3, "media_id"

    invoke-static/range {p6 .. p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {p4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 222
    const-string v3, "media_time"

    move-object/from16 v0, p8

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->b(Landroid/content/ContentResolver;Landroid/net/Uri;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {p4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 224
    const-string v3, "_data"

    move-object/from16 v0, p8

    invoke-static {p1, v0, v3}, Lcom/google/android/libraries/social/autobackup/util/b;->b(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 225
    if-nez v3, :cond_2

    move-object v3, v4

    .line 228
    :cond_2
    const-string v5, "media_hash"

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p4, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 229
    const-string v3, "media_url"

    invoke-virtual {p4, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    const-string v3, "upload_reason"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p4, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 231
    const-string v3, "upload_state"

    const/16 v4, 0x1f4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p4, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 232
    sget-object v3, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-static {p4}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(Landroid/content/ContentValues;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v4

    invoke-virtual {v3, p3, v4}, Lcom/google/android/libraries/social/g/a/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/libraries/social/g/a/a;)J

    .line 235
    if-eqz p10, :cond_3

    move-object/from16 v0, p8

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/autobackup/ab;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 236
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 239
    :cond_4
    invoke-interface {p2}, Lcom/google/android/libraries/social/autobackup/ao;->g()I

    move-result v4

    .line 240
    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/o;->d()I

    move-result v3

    .line 242
    const-string v2, "event_id"

    invoke-virtual {p4, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 244
    const/4 v2, 0x0

    .line 245
    if-eqz p11, :cond_7

    if-ne v3, v4, :cond_7

    .line 246
    invoke-interface {p2}, Lcom/google/android/libraries/social/autobackup/ao;->a()V

    invoke-interface {p2}, Lcom/google/android/libraries/social/autobackup/ao;->f()Ljava/lang/String;

    move-result-object v8

    const-string v2, "title"

    move-object/from16 v0, p8

    invoke-static {p1, v0, v2}, Lcom/google/android/libraries/social/autobackup/util/b;->b(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-eqz v8, :cond_a

    const/4 v2, -0x1

    if-eq v3, v2, :cond_a

    move-object v2, p0

    move-object v4, p1

    move-object v5, p2

    move-object/from16 v6, p8

    move/from16 v7, p9

    invoke-static/range {v2 .. v7}, Lcom/google/android/libraries/social/autobackup/ab;->a(Landroid/content/Context;ILandroid/content/ContentResolver;Lcom/google/android/libraries/social/autobackup/ao;Landroid/net/Uri;Z)Z

    move-result v2

    if-eqz v2, :cond_a

    if-eqz v9, :cond_5

    const-string v2, ":nopm:"

    invoke-virtual {v9, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_5
    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_b

    const-string v2, "event_id"

    invoke-virtual {p4, v2, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "upload_account_id"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p4, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "upload_reason"

    const/16 v4, 0x14

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p4, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "upload_state"

    const/16 v4, 0x64

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p4, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v2, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-static {p4}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(Landroid/content/ContentValues;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v4

    invoke-virtual {v2, p3, v4}, Lcom/google/android/libraries/social/g/a/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/libraries/social/g/a/a;)J

    const-string v2, "iu.UploadsManager"

    const/4 v4, 0x4

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "NEW; upload media id: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p6

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; is: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    const/4 v2, 0x1

    .line 250
    :cond_7
    :goto_2
    if-nez v2, :cond_8

    invoke-static {p0}, Lcom/google/android/libraries/social/autobackup/s;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 251
    const-string v2, "upload_account_id"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p4, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 252
    const-string v2, "upload_reason"

    const/16 v4, 0x1e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p4, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 254
    const-string v2, "upload_state"

    const/16 v4, 0x64

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p4, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 256
    sget-object v2, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-static {p4}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(Landroid/content/ContentValues;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v4

    invoke-virtual {v2, p3, v4}, Lcom/google/android/libraries/social/g/a/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/libraries/social/g/a/a;)J

    .line 258
    const-string v2, "iu.UploadsManager"

    const/4 v4, 0x4

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 259
    const-string v2, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "NEW; upload media id: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p6

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; iu: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    :cond_8
    const-string v2, "iu.UploadsManager"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 265
    const-string v2, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "NEW; add media id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p6

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    :cond_9
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 246
    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_b
    const/4 v2, 0x0

    goto :goto_2
.end method

.method static b(Lcom/google/android/libraries/social/autobackup/au;II)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 536
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    .line 540
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/au;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, Lcom/google/android/libraries/social/autobackup/ab;->d:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v1, v2, v3}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0
.end method

.method public static b(Lcom/google/android/libraries/social/autobackup/au;I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 503
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 504
    const-string v3, "upload_account_id != -1 AND upload_state = 100"

    move-object v4, v5

    .line 513
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/au;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/social/autobackup/ab;->a:Ljava/lang/String;

    sget-object v2, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/g/a/d;->b()[Ljava/lang/String;

    move-result-object v2

    const-string v7, "upload_reason ASC, upload_state ASC, upload_status ASC, is_image DESC, retry_end_time ASC LIMIT 1"

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 517
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 518
    invoke-static {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(Landroid/database/Cursor;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 521
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 524
    :goto_1
    return-object v5

    .line 507
    :cond_0
    const-string v3, "upload_account_id = ? AND upload_state = 100"

    .line 508
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    goto :goto_0

    .line 521
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static c(Lcom/google/android/libraries/social/autobackup/au;I)I
    .locals 5

    .prologue
    .line 528
    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/au;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/social/autobackup/ab;->c:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method static d(Lcom/google/android/libraries/social/autobackup/au;I)Landroid/database/Cursor;
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 569
    sget-object v4, Lcom/google/android/libraries/social/autobackup/ab;->b:Ljava/lang/String;

    .line 570
    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/au;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/social/autobackup/ab;->a:Ljava/lang/String;

    sget-object v3, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/g/a/d;->b()[Ljava/lang/String;

    move-result-object v3

    new-array v5, v1, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    move-object v7, v6

    move-object v8, v6

    move-object v9, v6

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method static e(Lcom/google/android/libraries/social/autobackup/au;I)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 580
    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/au;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    .line 586
    invoke-static {p0, p1}, Lcom/google/android/libraries/social/autobackup/ab;->d(Lcom/google/android/libraries/social/autobackup/au;I)Landroid/database/Cursor;

    move-result-object v2

    .line 588
    :goto_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589
    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(Landroid/database/Cursor;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    .line 591
    const-wide/16 v4, 0x0

    iput-wide v4, v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->id:J

    .line 592
    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->e(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 593
    const/16 v3, 0x28

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 594
    const/16 v3, 0x64

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 595
    sget-object v3, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-virtual {v3, v10, v0}, Lcom/google/android/libraries/social/g/a/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/libraries/social/g/a/a;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 598
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 602
    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/au;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/social/autobackup/ab;->a:Ljava/lang/String;

    sget-object v3, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/g/a/d;->b()[Ljava/lang/String;

    move-result-object v3

    const-string v4, "upload_account_id = ? AND upload_state = 300"

    new-array v5, v1, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    move-object v7, v6

    move-object v8, v6

    move-object v9, v6

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 607
    :goto_1
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 608
    invoke-static {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(Landroid/database/Cursor;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    .line 609
    const/16 v2, 0x64

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 610
    sget-object v2, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-virtual {v2, v10, v0}, Lcom/google/android/libraries/social/g/a/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/libraries/social/g/a/a;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 613
    :catchall_1
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 614
    return-void
.end method
