.class public abstract Lcom/google/android/libraries/social/rpc/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/rpc/p;


# static fields
.field private static final a:Lcom/google/android/libraries/social/f/e;

.field private static final b:Lcom/google/android/libraries/social/f/e;

.field private static final c:Lcom/google/android/libraries/social/f/a;

.field private static final d:Lcom/google/android/libraries/social/f/e;

.field private static final e:Lcom/google/android/libraries/social/f/e;

.field private static final f:Lcom/google/android/libraries/social/f/e;

.field private static final g:Lcom/google/android/libraries/social/f/e;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/libraries/social/f/e;

    const-string v1, "debug.plus.apiary_token"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/social/f/e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/social/rpc/c;->a:Lcom/google/android/libraries/social/f/e;

    .line 28
    new-instance v0, Lcom/google/android/libraries/social/f/e;

    const-string v1, "debug.plus.backend.url"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/social/f/e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/social/rpc/c;->b:Lcom/google/android/libraries/social/f/e;

    .line 34
    new-instance v0, Lcom/google/android/libraries/social/f/a;

    const-string v1, "debug.plus.tracing_enabled"

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/f/a;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/social/rpc/c;->c:Lcom/google/android/libraries/social/f/a;

    .line 41
    new-instance v0, Lcom/google/android/libraries/social/f/e;

    const-string v1, "debug.plus.tracing_token"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/social/f/e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/social/rpc/c;->d:Lcom/google/android/libraries/social/f/e;

    .line 47
    new-instance v0, Lcom/google/android/libraries/social/f/e;

    const-string v1, "debug.plus.tracing_path"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/social/f/e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/social/rpc/c;->e:Lcom/google/android/libraries/social/f/e;

    .line 55
    new-instance v0, Lcom/google/android/libraries/social/f/e;

    const-string v1, "debug.plus.tracing_level"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/social/f/e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/social/rpc/c;->f:Lcom/google/android/libraries/social/f/e;

    .line 61
    new-instance v0, Lcom/google/android/libraries/social/f/e;

    const-string v1, "debug.plus.experiment_override"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/social/f/e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/social/rpc/c;->g:Lcom/google/android/libraries/social/f/e;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/google/android/libraries/social/rpc/c;->a:Lcom/google/android/libraries/social/f/e;

    iget-object v0, v0, Lcom/google/android/libraries/social/f/e;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/google/android/libraries/social/rpc/c;->c:Lcom/google/android/libraries/social/f/a;

    const/4 v0, 0x0

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    sget-object v0, Lcom/google/android/libraries/social/rpc/c;->g:Lcom/google/android/libraries/social/f/e;

    iget-object v0, v0, Lcom/google/android/libraries/social/f/e;->a:Ljava/lang/String;

    return-object v0
.end method
