.class Lcom/google/android/libraries/social/account/impl/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/account/f;


# instance fields
.field protected final b:Landroid/content/SharedPreferences$Editor;

.field protected final c:Ljava/lang/String;

.field protected d:Ljava/util/Map;

.field final synthetic e:Lcom/google/android/libraries/social/account/impl/a;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/social/account/impl/a;Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 519
    iput-object p1, p0, Lcom/google/android/libraries/social/account/impl/g;->e:Lcom/google/android/libraries/social/account/impl/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 517
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/account/impl/g;->d:Ljava/util/Map;

    .line 520
    iput-object p2, p0, Lcom/google/android/libraries/social/account/impl/g;->b:Landroid/content/SharedPreferences$Editor;

    .line 521
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/account/impl/g;->c:Ljava/lang/String;

    .line 522
    return-void
.end method

.method private e(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 650
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 636
    const-string v0, "account_status"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/account/impl/g;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->e:Lcom/google/android/libraries/social/account/impl/a;

    invoke-static {v1}, Lcom/google/android/libraries/social/account/impl/a;->b(Lcom/google/android/libraries/social/account/impl/a;)Landroid/content/SharedPreferences;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 580
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/account/impl/g;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 581
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->e:Lcom/google/android/libraries/social/account/impl/a;

    invoke-static {v1}, Lcom/google/android/libraries/social/account/impl/a;->b(Lcom/google/android/libraries/social/account/impl/a;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)Lcom/google/android/libraries/social/account/f;
    .locals 1

    .prologue
    .line 574
    const-string v0, "account_status"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/libraries/social/account/impl/g;->b(Ljava/lang/String;I)Lcom/google/android/libraries/social/account/f;

    .line 575
    return-object p0
.end method

.method public b(Ljava/lang/String;F)Lcom/google/android/libraries/social/account/f;
    .locals 3

    .prologue
    .line 558
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/account/impl/g;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 559
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->d:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 560
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->b:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1, v0, p2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 561
    return-object p0
.end method

.method public b(Ljava/lang/String;I)Lcom/google/android/libraries/social/account/f;
    .locals 3

    .prologue
    .line 542
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/account/impl/g;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 543
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->d:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->b:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1, v0, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 545
    return-object p0
.end method

.method public b(Ljava/lang/String;J)Lcom/google/android/libraries/social/account/f;
    .locals 4

    .prologue
    .line 550
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/account/impl/g;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 551
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->d:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 552
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->b:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1, v0, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 553
    return-object p0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/account/f;
    .locals 2

    .prologue
    .line 526
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/account/impl/g;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 527
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->d:Ljava/util/Map;

    invoke-interface {v1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 528
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->b:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1, v0, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 529
    return-object p0
.end method

.method public b(Ljava/lang/String;Z)Lcom/google/android/libraries/social/account/f;
    .locals 3

    .prologue
    .line 534
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/account/impl/g;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 535
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->d:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->b:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1, v0, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 537
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 594
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/account/impl/g;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->e:Lcom/google/android/libraries/social/account/impl/a;

    invoke-static {v1}, Lcom/google/android/libraries/social/account/impl/a;->b(Lcom/google/android/libraries/social/account/impl/a;)Landroid/content/SharedPreferences;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 641
    invoke-virtual {p0}, Lcom/google/android/libraries/social/account/impl/g;->a()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 607
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/account/impl/g;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->e:Lcom/google/android/libraries/social/account/impl/a;

    invoke-static {v1}, Lcom/google/android/libraries/social/account/impl/a;->b(Lcom/google/android/libraries/social/account/impl/a;)Landroid/content/SharedPreferences;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public synthetic d(Ljava/lang/String;)Lcom/google/android/libraries/social/account/c;
    .locals 1

    .prologue
    .line 514
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/account/impl/g;->h(Ljava/lang/String;)Lcom/google/android/libraries/social/account/f;

    move-result-object v0

    return-object v0
.end method

.method public g(Ljava/lang/String;)Lcom/google/android/libraries/social/account/f;
    .locals 2

    .prologue
    .line 566
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/account/impl/g;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 567
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 568
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->b:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 569
    return-object p0
.end method

.method public h(Ljava/lang/String;)Lcom/google/android/libraries/social/account/f;
    .locals 5

    .prologue
    .line 646
    new-instance v0, Lcom/google/android/libraries/social/account/impl/g;

    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/g;->e:Lcom/google/android/libraries/social/account/impl/a;

    iget-object v2, p0, Lcom/google/android/libraries/social/account/impl/g;->b:Landroid/content/SharedPreferences$Editor;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/libraries/social/account/impl/g;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/libraries/social/account/impl/g;-><init>(Lcom/google/android/libraries/social/account/impl/a;Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    return-object v0
.end method
