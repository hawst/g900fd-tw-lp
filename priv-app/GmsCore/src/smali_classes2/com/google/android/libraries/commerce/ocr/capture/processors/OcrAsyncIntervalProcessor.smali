.class public Lcom/google/android/libraries/commerce/ocr/capture/processors/OcrAsyncIntervalProcessor;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;


# static fields
.field private static final TAG:Ljava/lang/String; = "OcrAsyncIntervalProcessor"


# instance fields
.field protected final ocrHandler:Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;

.field private final ocrIntervalPolicy:Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;

.field protected final ocrRecognizer:Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer;Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/OcrAsyncIntervalProcessor;->ocrIntervalPolicy:Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;

    .line 31
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/OcrAsyncIntervalProcessor;->ocrRecognizer:Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer;

    .line 32
    iput-object p3, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/OcrAsyncIntervalProcessor;->ocrHandler:Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;

    .line 33
    return-void
.end method


# virtual methods
.method public discard(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)V
    .locals 0

    .prologue
    .line 53
    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->recycle()V

    .line 54
    return-void
.end method

.method public bridge synthetic discard(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/OcrAsyncIntervalProcessor;->discard(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)V

    return-void
.end method

.method public isProcessingNeeded(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)Z
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/OcrAsyncIntervalProcessor;->ocrIntervalPolicy:Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;->isOverInterval()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic isProcessingNeeded(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 17
    check-cast p1, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/OcrAsyncIntervalProcessor;->isProcessingNeeded(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic process(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    check-cast p1, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/OcrAsyncIntervalProcessor;->process(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public process(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)Ljava/lang/Void;
    .locals 3

    .prologue
    .line 42
    const-string v0, "OcrAsyncIntervalProcessor"

    const-string v1, "Performing OCR recognition"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    :try_start_0
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/OcrAsyncIntervalProcessor;->ocrRecognizer:Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer;

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/OcrAsyncIntervalProcessor;->ocrHandler:Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;

    invoke-interface {v1, v0, v2}, Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer;->performOcr(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 46
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/OcrAsyncIntervalProcessor;->discard(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)V

    .line 48
    const/4 v0, 0x0

    return-object v0

    .line 46
    :catchall_0
    move-exception v0

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/OcrAsyncIntervalProcessor;->discard(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)V

    throw v0
.end method

.method public shutdown()V
    .locals 0

    .prologue
    .line 59
    return-void
.end method
