.class public Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/libraries/commerce/ocr/capture/EdgeChangeListener;
.implements Lcom/google/android/libraries/commerce/ocr/ui/PreviewOverlayView;


# static fields
.field private static final BOUNDING_BOX_WIDTH_IN_PX:I = 0x19

.field private static final CAP_PADDING_IN_PX:I = 0x1

.field private static final CORNER_LINE_LENGTH_TO_BOUNDING_BOX_HEIGHT_RATIO:F = 0.25f

.field private static final DEBUGGING_LINE_WIDTH_IN_PX:I = 0x1

.field private static final MESASSAGE_TO_BOUNDING_BOX_MARGIN_IN_PX:I = 0xf


# instance fields
.field private final boundingBoxView:Landroid/view/View;

.field private final cardAreaPaint:Landroid/graphics/Paint;

.field private cardBoundaries:Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;

.field private final context:Landroid/content/Context;

.field private final creditCardBitmapPaint:Landroid/graphics/Paint;

.field private final debuggingLinePaint:Landroid/graphics/Paint;

.field private final edgeLinePaint:Landroid/graphics/Paint;

.field private final exitHandler:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

.field private final messageTextView:Landroid/widget/TextView;

.field private outerBoundingBoxArea:Landroid/graphics/Rect;

.field private final presenter:Lcom/google/android/libraries/commerce/ocr/ui/PreviewOverlayView$Presenter;

.field private final previewOverlayView:Landroid/view/View;

.field private final roiProvider:Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;

.field private final uiHandler:Landroid/os/Handler;

.field private final uiOption:I

.field private visiblePreviewArea:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/Button;Lcom/google/android/libraries/commerce/ocr/ui/PreviewOverlayView$Presenter;Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;ILandroid/os/Handler;)V
    .locals 5

    .prologue
    const/16 v4, 0xff

    const/16 v3, 0x9b

    const/4 v2, -0x1

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->previewOverlayView:Landroid/view/View;

    .line 72
    iput-object p5, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->presenter:Lcom/google/android/libraries/commerce/ocr/ui/PreviewOverlayView$Presenter;

    .line 73
    iput-object p6, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->exitHandler:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

    .line 74
    iput-object p7, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->roiProvider:Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;

    .line 75
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->presenter:Lcom/google/android/libraries/commerce/ocr/ui/PreviewOverlayView$Presenter;

    invoke-interface {v0, p0}, Lcom/google/android/libraries/commerce/ocr/ui/PreviewOverlayView$Presenter;->setView(Lcom/google/android/libraries/commerce/ocr/ui/PreviewOverlayView;)V

    .line 76
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->context:Landroid/content/Context;

    .line 77
    iput p8, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->uiOption:I

    .line 78
    iput-object p9, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->uiHandler:Landroid/os/Handler;

    .line 79
    iput-object p3, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->messageTextView:Landroid/widget/TextView;

    .line 80
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->cardAreaPaint:Landroid/graphics/Paint;

    .line 81
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->cardAreaPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 82
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->edgeLinePaint:Landroid/graphics/Paint;

    .line 83
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->edgeLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 84
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->edgeLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 85
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->edgeLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 86
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->edgeLinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41c80000    # 25.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 87
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->debuggingLinePaint:Landroid/graphics/Paint;

    .line 88
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->debuggingLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 89
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->debuggingLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 90
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->debuggingLinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 92
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;

    invoke-direct {v0, p0, p1}, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;-><init>(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->boundingBoxView:Landroid/view/View;

    .line 94
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->creditCardBitmapPaint:Landroid/graphics/Paint;

    .line 95
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->creditCardBitmapPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 97
    invoke-virtual {p4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->uiOption:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->roiProvider:Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->visiblePreviewArea:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->cardAreaPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->edgeLinePaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->cardBoundaries:Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;

    return-object v0
.end method

.method private isLayoutFinalized()Z
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->visiblePreviewArea:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->outerBoundingBoxArea:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private layoutOverlayElementsRelativeToBoundingBox()V
    .locals 5

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->isLayoutFinalized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    const/high16 v0, 0x40000000    # 2.0f

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->messageTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->messageTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->messageTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 146
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->outerBoundingBoxArea:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v0, v1, v0

    add-int/lit8 v1, v0, -0xf

    .line 149
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->messageTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 151
    iget v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iget v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 153
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->messageTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 155
    :cond_0
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 102
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->exitHandler:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

    const/16 v1, 0x2717

    invoke-virtual {v0, v1, v2, v2}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;->exit(ILcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;Landroid/os/Bundle;)V

    .line 103
    return-void
.end method

.method public onEdgeChange(Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;)V
    .locals 2

    .prologue
    .line 132
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->uiOption:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 133
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->cardBoundaries:Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;

    .line 134
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->boundingBoxView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    .line 136
    :cond_0
    return-void
.end method

.method public renderInvalidCardMessage()V
    .locals 2

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->uiHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/libraries/commerce/ocr/credit/ui/d;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/d;-><init>(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 318
    return-void
.end method

.method public showBoundingBox(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 112
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->visiblePreviewArea:Landroid/graphics/Rect;

    .line 113
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->roiProvider:Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->getBoundingBoxRect()Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->outerBoundingBoxArea:Landroid/graphics/Rect;

    .line 115
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->boundingBoxView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->previewOverlayView:Landroid/view/View;

    sget v1, Lcom/google/android/libraries/commerce/ocr/R$id;->ocrBoundingBoxRoot:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->boundingBoxView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 119
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->uiOption:I

    packed-switch v0, :pswitch_data_0

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 122
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->layoutOverlayElementsRelativeToBoundingBox()V

    goto :goto_0

    .line 119
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public toView()Landroid/view/View;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->previewOverlayView:Landroid/view/View;

    return-object v0
.end method
