.class public interface abstract Lcom/google/android/libraries/commerce/ocr/ui/PreviewOverlayView;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final BOUNDING_BOX_AREA_FILL_ALPHA:I = 0x9b

.field public static final EDGE_DETECTION_AREA_COLOR:I = -0x1

.field public static final EDGE_DETECTION_FILL_ALPHA:I = 0xff


# virtual methods
.method public abstract renderInvalidCardMessage()V
.end method

.method public abstract showBoundingBox(Landroid/graphics/Rect;)V
.end method

.method public abstract toView()Landroid/view/View;
.end method
