.class public Lcom/google/android/libraries/commerce/ocr/pub/PrerequisiteChecker;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final allowedCpuAbis:Ljava/util/Set;

.field private final defaultCreditCardOcrModelBlacklist:Ljava/util/Set;

.field private final featurePrerequisiteCheckerUtil:Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;


# direct methods
.method protected constructor <init>(ILjava/lang/String;Ljava/util/Set;Lcom/google/android/libraries/commerce/ocr/util/Function;Ljava/util/Set;Landroid/content/pm/PackageManager;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p3, p0, Lcom/google/android/libraries/commerce/ocr/pub/PrerequisiteChecker;->allowedCpuAbis:Ljava/util/Set;

    .line 46
    iput-object p5, p0, Lcom/google/android/libraries/commerce/ocr/pub/PrerequisiteChecker;->defaultCreditCardOcrModelBlacklist:Ljava/util/Set;

    .line 47
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;

    invoke-direct {v0, p1, p2, p4, p6}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;-><init>(ILjava/lang/String;Lcom/google/android/libraries/commerce/ocr/util/Function;Landroid/content/pm/PackageManager;)V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/PrerequisiteChecker;->featurePrerequisiteCheckerUtil:Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/pm/PackageManager;)V
    .locals 7

    .prologue
    .line 34
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sget-object v3, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->NATIVE_CPU_ABIS:Ljava/util/HashSet;

    invoke-static {}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->getCameraInfoFunction()Lcom/google/android/libraries/commerce/ocr/util/Function;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOcrPrerequisiteChecker;->DEFAULT_CREDIT_CARD_OCR_MODEL_BLACKLIST:Ljava/util/HashSet;

    move-object v0, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/libraries/commerce/ocr/pub/PrerequisiteChecker;-><init>(ILjava/lang/String;Ljava/util/Set;Lcom/google/android/libraries/commerce/ocr/util/Function;Ljava/util/Set;Landroid/content/pm/PackageManager;)V

    .line 38
    return-void
.end method


# virtual methods
.method public creditCardPrerequisitesOK(Ljava/lang/String;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 57
    invoke-static {p1}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->split(Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/commerce/ocr/pub/PrerequisiteChecker;->creditCardPrerequisitesOK(Ljava/util/Set;)Z

    move-result v0

    return v0
.end method

.method public creditCardPrerequisitesOK(Ljava/util/Set;)Z
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 66
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/pub/PrerequisiteChecker;->defaultCreditCardOcrModelBlacklist:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 67
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 68
    new-instance v1, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOcrPrerequisiteChecker;

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/pub/PrerequisiteChecker;->featurePrerequisiteCheckerUtil:Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;

    new-instance v3, Ljava/util/HashSet;

    iget-object v4, p0, Lcom/google/android/libraries/commerce/ocr/pub/PrerequisiteChecker;->allowedCpuAbis:Ljava/util/Set;

    invoke-direct {v3, v4}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOcrPrerequisiteChecker;-><init>(Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;Ljava/util/HashSet;Ljava/util/HashSet;)V

    .line 70
    invoke-virtual {v1}, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOcrPrerequisiteChecker;->check()Z

    move-result v0

    return v0
.end method

.method public loyaltyCardPrerequisitesOK(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 80
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/ConnectivityManager;

    .line 82
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    .line 84
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/pub/PrerequisiteChecker;->featurePrerequisiteCheckerUtil:Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;

    new-instance v3, Ljava/util/HashSet;

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/pub/PrerequisiteChecker;->allowedCpuAbis:Ljava/util/Set;

    invoke-direct {v3, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    move-object v2, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;-><init>(Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;Ljava/lang/String;Ljava/util/HashSet;Landroid/net/ConnectivityManager;Landroid/telephony/TelephonyManager;Ljava/lang/String;)V

    .line 91
    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;->check()Z

    move-result v0

    return v0
.end method
