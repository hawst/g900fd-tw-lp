.class public Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils$MediaTimeSnapshot;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public a:Landroid/net/Uri;

.field public b:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 484
    new-instance v0, Lcom/google/android/libraries/social/autobackup/util/e;

    invoke-direct {v0}, Lcom/google/android/libraries/social/autobackup/util/e;-><init>()V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils$MediaTimeSnapshot;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 503
    const-class v0, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils$MediaConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils$MediaTimeSnapshot;->a:Landroid/net/Uri;

    .line 504
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils$MediaTimeSnapshot;->b:J

    .line 505
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 509
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 514
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils$MediaTimeSnapshot;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 515
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils$MediaTimeSnapshot;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 516
    return-void
.end method
