.class public final Lcom/google/android/libraries/rocket/impressions/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/Object;

.field public final b:Lcom/google/android/libraries/rocket/impressions/f;

.field public final c:Lcom/google/android/libraries/rocket/impressions/o;

.field public final d:Lcom/google/android/libraries/rocket/impressions/Session;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/rocket/impressions/f;Lcom/google/android/libraries/rocket/impressions/o;Lcom/google/android/libraries/rocket/impressions/Session;)V
    .locals 2

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/rocket/impressions/h;->a:Ljava/lang/Object;

    .line 73
    iput-object p1, p0, Lcom/google/android/libraries/rocket/impressions/h;->b:Lcom/google/android/libraries/rocket/impressions/f;

    .line 75
    if-nez p2, :cond_0

    .line 76
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "transport must be non-null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :cond_0
    iput-object p2, p0, Lcom/google/android/libraries/rocket/impressions/h;->c:Lcom/google/android/libraries/rocket/impressions/o;

    .line 80
    if-nez p3, :cond_1

    .line 81
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "session must be non-null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_1
    iput-object p3, p0, Lcom/google/android/libraries/rocket/impressions/h;->d:Lcom/google/android/libraries/rocket/impressions/Session;

    .line 84
    return-void
.end method

.method private b(Lcom/google/android/libraries/rocket/impressions/g;)V
    .locals 4

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/h;->d:Lcom/google/android/libraries/rocket/impressions/Session;

    iget-object v1, p1, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v1, v1, Lcom/google/c/b/b/a/a/e;->h:Lcom/google/c/b/b/a/a/a;

    iget-object v1, v1, Lcom/google/c/b/b/a/a/a;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "not an instant timing impression; instead: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v2, v2, Lcom/google/c/b/b/a/a/e;->h:Lcom/google/c/b/b/a/a/a;

    iget-object v2, v2, Lcom/google/c/b/b/a/a/a;->c:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p1, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v1, v1, Lcom/google/c/b/b/a/a/e;->h:Lcom/google/c/b/b/a/a/a;

    iget-object v1, v1, Lcom/google/c/b/b/a/a/a;->a:Lcom/google/c/b/b/a/a/c;

    iget-object v1, v1, Lcom/google/c/b/b/a/a/c;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/rocket/impressions/Session;->a(J)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/rocket/impressions/g;->b(J)Lcom/google/android/libraries/rocket/impressions/g;

    .line 205
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/rocket/impressions/h;->a(Lcom/google/android/libraries/rocket/impressions/g;)V

    .line 206
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 98
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/h;->b:Lcom/google/android/libraries/rocket/impressions/f;

    const/16 v1, 0x2cc

    invoke-static {v0, v1, v2, v2}, Lcom/google/android/libraries/rocket/impressions/g;->a(Lcom/google/android/libraries/rocket/impressions/f;ILjava/lang/Long;Ljava/lang/Long;)Lcom/google/android/libraries/rocket/impressions/g;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/rocket/impressions/h;->b(Lcom/google/android/libraries/rocket/impressions/g;)V

    .line 99
    return-void
.end method

.method public final a(Lcom/google/android/libraries/rocket/impressions/g;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 209
    iget-object v0, p1, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/e;->b:Ljava/lang/Integer;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "must call setImpressionCode() before build()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p1, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/e;->c:Ljava/lang/Long;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "must call setSequenceNumber() before build()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p1, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/e;->h:Lcom/google/c/b/b/a/a/a;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/e;->h:Lcom/google/c/b/b/a/a/a;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/a;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot build an impression without any timing information: code "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v2, v2, Lcom/google/c/b/b/a/a/e;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p1, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/e;->h:Lcom/google/c/b/b/a/a/a;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/a;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown timing type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v2, v2, Lcom/google/c/b/b/a/a/e;->h:Lcom/google/c/b/b/a/a/a;

    iget-object v2, v2, Lcom/google/c/b/b/a/a/a;->c:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p1, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/e;->h:Lcom/google/c/b/b/a/a/a;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/a;->a:Lcom/google/c/b/b/a/a/c;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/c;->a:Ljava/lang/Long;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "instant timing without timestamp"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    iget-object v0, p1, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/e;->h:Lcom/google/c/b/b/a/a/a;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/a;->b:Lcom/google/c/b/b/a/a/b;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/b;->a:Ljava/lang/Long;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "elapsed timing without start timestamp"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-object v0, p1, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/e;->h:Lcom/google/c/b/b/a/a/a;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/a;->b:Lcom/google/c/b/b/a/a/b;

    iget-object v0, v0, Lcom/google/c/b/b/a/a/b;->b:Ljava/lang/Long;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "elapsed timing without end timestamp"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    iget-object v0, p1, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    .line 210
    const-string v1, "Adding impression: code %s,  seq_num %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, v0, Lcom/google/c/b/b/a/a/e;->b:Ljava/lang/Integer;

    aput-object v3, v2, v4

    iget-object v3, v0, Lcom/google/c/b/b/a/a/e;->c:Ljava/lang/Long;

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lcom/google/android/libraries/rocket/impressions/j;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 212
    iget-object v1, p0, Lcom/google/android/libraries/rocket/impressions/h;->d:Lcom/google/android/libraries/rocket/impressions/Session;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/rocket/impressions/Session;->a(Lcom/google/c/b/b/a/a/e;)V

    .line 215
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/h;->d:Lcom/google/android/libraries/rocket/impressions/Session;

    invoke-virtual {v0}, Lcom/google/android/libraries/rocket/impressions/Session;->d()Lcom/google/c/b/b/a/a/f;

    move-result-object v0

    const-string v1, "Flushing batch to transport; first seq num: %s"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, v0, Lcom/google/c/b/b/a/a/f;->a:[Lcom/google/c/b/b/a/a/e;

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/google/c/b/b/a/a/e;->c:Ljava/lang/Long;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/libraries/rocket/impressions/j;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/libraries/rocket/impressions/h;->c:Lcom/google/android/libraries/rocket/impressions/o;

    invoke-interface {v1, v0}, Lcom/google/android/libraries/rocket/impressions/o;->b(Lcom/google/c/b/b/a/a/f;)V

    .line 216
    return-void

    .line 209
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/android/libraries/rocket/impressions/l;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/h;->d:Lcom/google/android/libraries/rocket/impressions/Session;

    invoke-virtual {v0}, Lcom/google/android/libraries/rocket/impressions/Session;->a()Lcom/google/android/libraries/rocket/impressions/l;

    move-result-object v0

    if-eq v0, p1, :cond_0

    .line 198
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in a "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/rocket/impressions/h;->d:Lcom/google/android/libraries/rocket/impressions/Session;

    invoke-virtual {v2}, Lcom/google/android/libraries/rocket/impressions/Session;->a()Lcom/google/android/libraries/rocket/impressions/l;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " session"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 201
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 106
    iget-object v1, p0, Lcom/google/android/libraries/rocket/impressions/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 107
    :try_start_0
    sget-object v0, Lcom/google/android/libraries/rocket/impressions/l;->b:Lcom/google/android/libraries/rocket/impressions/l;

    const-string v2, "resumeSession"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/libraries/rocket/impressions/h;->a(Lcom/google/android/libraries/rocket/impressions/l;Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/h;->c:Lcom/google/android/libraries/rocket/impressions/o;

    invoke-interface {v0}, Lcom/google/android/libraries/rocket/impressions/o;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Cannot resume a paused session instance; recreate a new ImpressionLogger instance and call resumeSession() on that instead"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 113
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/libraries/rocket/impressions/h;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 114
    monitor-exit v1

    .line 122
    :goto_0
    return-void

    .line 118
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/h;->b:Lcom/google/android/libraries/rocket/impressions/f;

    iget-object v2, p0, Lcom/google/android/libraries/rocket/impressions/h;->d:Lcom/google/android/libraries/rocket/impressions/Session;

    invoke-virtual {v2}, Lcom/google/android/libraries/rocket/impressions/Session;->b()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/libraries/rocket/impressions/h;->d:Lcom/google/android/libraries/rocket/impressions/Session;

    invoke-virtual {v4}, Lcom/google/android/libraries/rocket/impressions/Session;->c()J

    move-result-wide v4

    invoke-static {v0, v2, v3, v4, v5}, Lcom/google/android/libraries/rocket/impressions/g;->a(Lcom/google/android/libraries/rocket/impressions/f;JJ)Lcom/google/android/libraries/rocket/impressions/g;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/rocket/impressions/h;->b(Lcom/google/android/libraries/rocket/impressions/g;)V

    .line 121
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/h;->d:Lcom/google/android/libraries/rocket/impressions/Session;

    sget-object v2, Lcom/google/android/libraries/rocket/impressions/l;->c:Lcom/google/android/libraries/rocket/impressions/l;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/rocket/impressions/Session;->a(Lcom/google/android/libraries/rocket/impressions/l;)V

    .line 122
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final c()Landroid/os/Parcelable;
    .locals 6

    .prologue
    .line 135
    iget-object v1, p0, Lcom/google/android/libraries/rocket/impressions/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 136
    :try_start_0
    sget-object v0, Lcom/google/android/libraries/rocket/impressions/l;->c:Lcom/google/android/libraries/rocket/impressions/l;

    const-string v2, "pauseSession"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/libraries/rocket/impressions/h;->a(Lcom/google/android/libraries/rocket/impressions/l;Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/h;->b:Lcom/google/android/libraries/rocket/impressions/f;

    iget-object v2, p0, Lcom/google/android/libraries/rocket/impressions/h;->d:Lcom/google/android/libraries/rocket/impressions/Session;

    invoke-virtual {v2}, Lcom/google/android/libraries/rocket/impressions/Session;->b()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/libraries/rocket/impressions/h;->d:Lcom/google/android/libraries/rocket/impressions/Session;

    invoke-virtual {v4}, Lcom/google/android/libraries/rocket/impressions/Session;->c()J

    move-result-wide v4

    invoke-static {v0, v2, v3, v4, v5}, Lcom/google/android/libraries/rocket/impressions/g;->a(Lcom/google/android/libraries/rocket/impressions/f;JJ)Lcom/google/android/libraries/rocket/impressions/g;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/rocket/impressions/h;->b(Lcom/google/android/libraries/rocket/impressions/g;)V

    .line 141
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/h;->d:Lcom/google/android/libraries/rocket/impressions/Session;

    sget-object v2, Lcom/google/android/libraries/rocket/impressions/l;->b:Lcom/google/android/libraries/rocket/impressions/l;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/rocket/impressions/Session;->a(Lcom/google/android/libraries/rocket/impressions/l;)V

    .line 142
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/h;->c:Lcom/google/android/libraries/rocket/impressions/o;

    invoke-interface {v0}, Lcom/google/android/libraries/rocket/impressions/o;->b()V

    .line 144
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/h;->d:Lcom/google/android/libraries/rocket/impressions/Session;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 145
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d()V
    .locals 6

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/h;->b:Lcom/google/android/libraries/rocket/impressions/f;

    iget-object v1, p0, Lcom/google/android/libraries/rocket/impressions/h;->d:Lcom/google/android/libraries/rocket/impressions/Session;

    invoke-virtual {v1}, Lcom/google/android/libraries/rocket/impressions/Session;->b()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/libraries/rocket/impressions/h;->d:Lcom/google/android/libraries/rocket/impressions/Session;

    invoke-virtual {v1}, Lcom/google/android/libraries/rocket/impressions/Session;->c()J

    move-result-wide v4

    const/16 v1, 0x2b3

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/libraries/rocket/impressions/g;->a(Lcom/google/android/libraries/rocket/impressions/f;ILjava/lang/Long;Ljava/lang/Long;)Lcom/google/android/libraries/rocket/impressions/g;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/rocket/impressions/h;->b(Lcom/google/android/libraries/rocket/impressions/g;)V

    .line 181
    return-void
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/h;->d:Lcom/google/android/libraries/rocket/impressions/Session;

    iget-object v1, p0, Lcom/google/android/libraries/rocket/impressions/h;->b:Lcom/google/android/libraries/rocket/impressions/f;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/rocket/impressions/Session;->b(Lcom/google/android/libraries/rocket/impressions/f;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 231
    const/4 v0, 0x0

    .line 238
    :goto_0
    return v0

    .line 234
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/rocket/impressions/h;->d()V

    .line 235
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/h;->d:Lcom/google/android/libraries/rocket/impressions/Session;

    iget-object v1, p0, Lcom/google/android/libraries/rocket/impressions/h;->b:Lcom/google/android/libraries/rocket/impressions/f;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/rocket/impressions/Session;->a(Lcom/google/android/libraries/rocket/impressions/f;)V

    .line 236
    invoke-virtual {p0}, Lcom/google/android/libraries/rocket/impressions/h;->a()V

    .line 237
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/h;->d:Lcom/google/android/libraries/rocket/impressions/Session;

    sget-object v1, Lcom/google/android/libraries/rocket/impressions/l;->c:Lcom/google/android/libraries/rocket/impressions/l;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/rocket/impressions/Session;->a(Lcom/google/android/libraries/rocket/impressions/l;)V

    .line 238
    const/4 v0, 0x1

    goto :goto_0
.end method
