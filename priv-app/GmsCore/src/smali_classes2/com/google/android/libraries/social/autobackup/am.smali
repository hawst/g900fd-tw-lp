.class final Lcom/google/android/libraries/social/autobackup/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/autobackup/f;


# instance fields
.field final a:I

.field final b:I

.field private final c:Lcom/google/android/libraries/social/networkcapability/a;

.field private volatile d:Z

.field private e:Lcom/google/android/libraries/social/autobackup/d;

.field private final f:Lcom/google/android/libraries/social/autobackup/au;

.field private final g:Lcom/google/android/libraries/social/autobackup/ao;

.field private final h:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

.field private final i:Landroid/content/Context;

.field private final j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

.field private final k:Ljava/lang/String;

.field private final l:Lcom/google/android/libraries/social/autobackup/ax;

.field private final m:Lcom/google/android/libraries/social/account/b;

.field private final n:Lcom/google/android/libraries/social/autobackup/a;

.field private final o:Lcom/google/android/libraries/social/autobackup/al;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-boolean v2, p0, Lcom/google/android/libraries/social/autobackup/am;->d:Z

    .line 110
    invoke-virtual {p2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v0, v1

    goto :goto_0

    .line 111
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/autobackup/am;->a:I

    .line 112
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    .line 113
    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/ax;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->l:Lcom/google/android/libraries/social/autobackup/ax;

    .line 114
    iput-object p2, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 115
    const-class v0, Lcom/google/android/libraries/social/autobackup/au;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/au;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->f:Lcom/google/android/libraries/social/autobackup/au;

    .line 116
    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/ap;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->g:Lcom/google/android/libraries/social/autobackup/ao;

    .line 117
    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->h:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    .line 118
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    const-class v3, Lcom/google/android/libraries/social/networkcapability/a;

    invoke-static {v0, v3}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/networkcapability/a;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->c:Lcom/google/android/libraries/social/networkcapability/a;

    .line 119
    invoke-virtual {p2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->g()I

    move-result v0

    .line 120
    invoke-virtual {p2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c()Z

    move-result v3

    .line 121
    shl-int/lit8 v4, v0, 0x1

    if-eqz v3, :cond_2

    move v0, v1

    :goto_1
    or-int/2addr v0, v4

    iput v0, p0, Lcom/google/android/libraries/social/autobackup/am;->b:I

    .line 122
    invoke-virtual {p2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->w()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->k:Ljava/lang/String;

    .line 123
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/account/b;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->m:Lcom/google/android/libraries/social/account/b;

    .line 124
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/autobackup/a;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/a;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->n:Lcom/google/android/libraries/social/autobackup/a;

    .line 125
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/autobackup/al;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/al;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->o:Lcom/google/android/libraries/social/autobackup/al;

    .line 126
    return-void

    :cond_2
    move v0, v2

    .line 121
    goto :goto_1
.end method

.method private a(JJ)J
    .locals 5

    .prologue
    .line 1027
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->q()J

    move-result-wide v0

    .line 1029
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 1030
    add-long v0, p1, p3

    .line 1031
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->e(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 1033
    :cond_0
    const-string v2, "iu.SyncTask"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1034
    const-string v2, "iu.SyncTask"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "+++ RETRY until "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; task: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1036
    :cond_1
    return-wide v0
.end method

.method private a(IJLjava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1042
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/am;->l:Lcom/google/android/libraries/social/autobackup/ax;

    monitor-enter v1

    .line 1043
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->d(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(Ljava/lang/Throwable;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 1044
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->h()I

    move-result v0

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/google/android/libraries/social/autobackup/am;->b(IJLjava/lang/Throwable;)V

    .line 1046
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->l:Lcom/google/android/libraries/social/autobackup/ax;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/ax;->b()V

    .line 1047
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(ILjava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1051
    const-wide/32 v0, 0x240c8400

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/google/android/libraries/social/autobackup/am;->a(IJLjava/lang/Throwable;)V

    .line 1052
    return-void
.end method

.method private a(Landroid/content/SyncStats;I)V
    .locals 4

    .prologue
    .line 642
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const/16 v1, 0x258

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->d(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 645
    iget-wide v0, p1, Landroid/content/SyncStats;->numSkippedEntries:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p1, Landroid/content/SyncStats;->numSkippedEntries:J

    .line 646
    return-void
.end method

.method private a(Landroid/content/SyncStats;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 475
    iget-wide v0, p1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p1, Landroid/content/SyncStats;->numIoExceptions:J

    .line 476
    const/4 v0, 0x3

    invoke-direct {p0, v0, p2}, Lcom/google/android/libraries/social/autobackup/am;->a(ILjava/lang/Throwable;)V

    .line 477
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/autobackup/am;->b(Z)V

    .line 478
    return-void
.end method

.method private a(Lcom/google/android/libraries/social/autobackup/d;)V
    .locals 2

    .prologue
    .line 464
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/am;->l:Lcom/google/android/libraries/social/autobackup/ax;

    monitor-enter v1

    .line 465
    :try_start_0
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/am;->e:Lcom/google/android/libraries/social/autobackup/d;

    .line 466
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Landroid/content/Context;Landroid/net/Uri;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 621
    const/4 v1, 0x0

    .line 623
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 624
    invoke-virtual {v1}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-gez v2, :cond_1

    .line 625
    if-eqz v1, :cond_0

    .line 632
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 638
    :cond_0
    :goto_0
    return v0

    .line 630
    :cond_1
    if-eqz v1, :cond_2

    .line 632
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 638
    :cond_2
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 628
    :catch_0
    move-exception v2

    if-eqz v1, :cond_0

    .line 632
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    .line 630
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    .line 632
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 635
    :cond_3
    :goto_2
    throw v0

    :catch_2
    move-exception v1

    goto :goto_0

    :catch_3
    move-exception v0

    goto :goto_1

    :catch_4
    move-exception v1

    goto :goto_2
.end method

.method private a(Landroid/content/SyncStats;)Z
    .locals 10

    .prologue
    .line 487
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/am;->l:Lcom/google/android/libraries/social/autobackup/ax;

    monitor-enter v1

    .line 488
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/am;->d:Z

    if-nez v0, :cond_0

    .line 489
    const/4 v0, 0x0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 610
    :goto_0
    return v0

    .line 491
    :cond_0
    monitor-exit v1

    .line 493
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/experiments/c;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/experiments/c;

    .line 494
    sget-object v1, Lcom/google/android/libraries/social/autobackup/c;->a:Lcom/google/android/libraries/social/experiments/a;

    iget v2, p0, Lcom/google/android/libraries/social/autobackup/am;->a:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/libraries/social/experiments/c;->a(Lcom/google/android/libraries/social/experiments/a;I)Z

    move-result v0

    .line 496
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 497
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 498
    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->g()I

    move-result v3

    .line 501
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 502
    iget-object v6, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v6}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->q()J

    move-result-wide v6

    .line 503
    const-wide/16 v8, 0x0

    cmp-long v8, v6, v8

    if-eqz v8, :cond_2

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    .line 504
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 505
    const-string v0, "iu.SyncTask"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "+++ SKIP task "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; exceed retry time; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    :cond_1
    const/16 v0, 0x28

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/autobackup/am;->a(Landroid/content/SyncStats;I)V

    .line 509
    const/4 v0, 0x0

    goto :goto_0

    .line 491
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 513
    :cond_2
    const/16 v4, 0x14

    if-ne v3, v4, :cond_4

    if-nez v0, :cond_4

    .line 514
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 515
    const-string v0, "iu.SyncTask"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "+++ SKIP record; instant share disabled; "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    :cond_3
    const/16 v0, 0x24

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/autobackup/am;->a(Landroid/content/SyncStats;I)V

    .line 518
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 522
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/autobackup/ab;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 524
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 525
    const-string v0, "iu.SyncTask"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "+++ SKIP record; has google exif; "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    :cond_5
    const/16 v0, 0x25

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/autobackup/am;->a(Landroid/content/SyncStats;I)V

    .line 528
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 532
    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/autobackup/am;->a(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 533
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 534
    const-string v0, "iu.SyncTask"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "+++ SKIP record; media removed; "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    :cond_7
    const/16 v0, 0x29

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/autobackup/am;->a(Landroid/content/SyncStats;I)V

    .line 537
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 540
    :cond_8
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->l()Ljava/lang/String;

    move-result-object v6

    .line 541
    if-nez v6, :cond_a

    .line 542
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/b/a;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/b/a;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/autobackup/b/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 543
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(Ljava/lang/String;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 546
    if-nez v6, :cond_a

    .line 547
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 548
    const-string v0, "iu.SyncTask"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "+++ QUEUE task "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; fingerprint not available; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    :cond_9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v2, 0x240c8400

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/libraries/social/autobackup/am;->a(JJ)J

    .line 552
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/autobackup/am;->a(Landroid/content/SyncStats;Ljava/lang/Throwable;)V

    .line 553
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 557
    :cond_a
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v1

    .line 558
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->m:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v0

    .line 559
    const-string v2, "is_plus_page"

    invoke-interface {v0, v2}, Lcom/google/android/libraries/social/account/c;->c(Ljava/lang/String;)Z

    move-result v2

    .line 560
    const-string v4, "gaia_id"

    invoke-interface {v0, v4}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 562
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->g()I

    move-result v0

    const/16 v5, 0xa

    if-ne v0, v5, :cond_c

    const/4 v0, 0x1

    .line 564
    :goto_1
    if-nez v2, :cond_d

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->o:Lcom/google/android/libraries/social/autobackup/al;

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->f:Lcom/google/android/libraries/social/autobackup/au;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/au;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v0, v4, v6}, Lcom/google/android/libraries/social/autobackup/al;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 567
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 568
    const-string v0, "iu.SyncTask"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "+++ SKIP record; duplicate upload; "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 570
    :cond_b
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const/16 v1, 0x190

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->d(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    const/16 v1, 0x22

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 573
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 562
    :cond_c
    const/4 v0, 0x0

    goto :goto_1

    .line 577
    :cond_d
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 578
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 579
    new-instance v2, Lcom/google/android/libraries/social/r/a;

    iget-object v5, p0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    invoke-direct {v2, v5, v1, v4, v0}, Lcom/google/android/libraries/social/r/a;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;)V

    .line 581
    invoke-virtual {v2}, Lcom/google/android/libraries/social/r/a;->a()V

    .line 582
    invoke-virtual {v2, v6}, Lcom/google/android/libraries/social/r/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 583
    invoke-virtual {v2, v6}, Lcom/google/android/libraries/social/r/a;->b(Ljava/lang/String;)J

    move-result-wide v2

    .line 584
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 585
    const-string v0, "iu.SyncTask"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "+++ SKIP record; duplicate upload; "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 587
    :cond_e
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/al;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/al;

    move-result-object v1

    const/4 v5, 0x0

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/libraries/social/autobackup/al;->a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const/16 v1, 0x190

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->d(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    const/16 v1, 0x22

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 592
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 596
    :cond_f
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_12

    const/4 v0, 0x1

    .line 597
    :goto_2
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->d()Ljava/lang/String;

    move-result-object v1

    .line 598
    if-nez v1, :cond_10

    if-nez v0, :cond_10

    .line 599
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-string v2, "instant"

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(Ljava/lang/String;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 603
    :cond_10
    const/16 v0, 0x14

    if-ne v3, v0, :cond_11

    if-nez v1, :cond_11

    .line 604
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-string v1, "instant"

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(Ljava/lang/String;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 608
    :cond_11
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 609
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->d(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 610
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 596
    :cond_12
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private a(Z)Z
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 860
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->l:Lcom/google/android/libraries/social/autobackup/ax;

    monitor-enter v2

    .line 861
    :try_start_0
    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->h()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 899
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->h()I

    move-result v0

    if-eq v0, v4, :cond_5

    .line 901
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 902
    const-string v0, "iu.SyncTask"

    const-string v1, "--- STOP wrong state after upload; task: "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 906
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "--- STOP wrong state;  task: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->h()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 914
    :goto_0
    const/4 v1, 0x5

    invoke-direct {p0, v1, v0}, Lcom/google/android/libraries/social/autobackup/am;->a(ILjava/lang/Throwable;)V

    .line 917
    :goto_1
    const/4 v0, 0x0

    monitor-exit v2

    :goto_2
    return v0

    .line 864
    :pswitch_1
    const-string v1, "iu.SyncTask"

    const/4 v3, 0x4

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 865
    const-string v1, "iu.SyncTask"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "--- QUEUE stalled "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/am;->k:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " task: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 868
    :cond_1
    const/4 v1, 0x3

    const/4 v3, 0x0

    invoke-direct {p0, v1, v3}, Lcom/google/android/libraries/social/autobackup/am;->a(ILjava/lang/Throwable;)V

    .line 869
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/autobackup/am;->b(Z)V

    .line 870
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 918
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 873
    :pswitch_2
    :try_start_1
    const-string v1, "iu.SyncTask"

    const/4 v3, 0x4

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 874
    const-string v1, "iu.SyncTask"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "--- QUEUE unauthorized "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/am;->k:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " task: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 877
    :cond_2
    const/4 v1, 0x3

    const/4 v3, 0x0

    invoke-direct {p0, v1, v3}, Lcom/google/android/libraries/social/autobackup/am;->a(ILjava/lang/Throwable;)V

    .line 878
    const/16 v1, 0xa

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/autobackup/am;->a(I)V

    .line 879
    monitor-exit v2

    goto :goto_2

    .line 882
    :pswitch_3
    const-string v1, "iu.SyncTask"

    const/4 v3, 0x4

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 883
    const-string v1, "iu.SyncTask"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "--- QUEUE quota exceeded "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/am;->k:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " task: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 886
    :cond_3
    const/4 v1, 0x3

    const/4 v3, 0x0

    invoke-direct {p0, v1, v3}, Lcom/google/android/libraries/social/autobackup/am;->a(ILjava/lang/Throwable;)V

    .line 887
    const/16 v1, 0x9

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/autobackup/am;->a(I)V

    .line 888
    monitor-exit v2

    goto/16 :goto_2

    .line 891
    :pswitch_4
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 892
    const-string v0, "iu.SyncTask"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "--- STOP cancelled "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/am;->k:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " task: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 895
    :cond_4
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/social/autobackup/am;->a(ILjava/lang/Throwable;)V

    goto/16 :goto_1

    .line 909
    :cond_5
    const-string v0, "iu.SyncTask"

    const/4 v3, 0x4

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 910
    const-string v0, "iu.SyncTask"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "--- STOP failed "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/am;->k:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " task: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_6
    move-object v0, v1

    goto/16 :goto_0

    .line 861
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b(I)V
    .locals 4

    .prologue
    .line 445
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/am;->l:Lcom/google/android/libraries/social/autobackup/ax;

    monitor-enter v1

    .line 446
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 447
    const-string v0, "iu.SyncTask"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 448
    const-string v0, "iu.SyncTask"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "stopCurrentTask: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/autobackup/am;->a(ILjava/lang/Throwable;)V

    .line 451
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->l:Lcom/google/android/libraries/social/autobackup/ax;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 453
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->e:Lcom/google/android/libraries/social/autobackup/d;

    if-eqz v0, :cond_1

    .line 454
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->e:Lcom/google/android/libraries/social/autobackup/d;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/d;->a()V

    .line 457
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private b(IJLjava/lang/Throwable;)V
    .locals 14

    .prologue
    const/16 v5, 0x28

    const/16 v6, 0x190

    const/4 v4, 0x0

    const/16 v2, 0x64

    const/16 v3, 0x12c

    .line 1062
    packed-switch p1, :pswitch_data_0

    .line 1170
    :goto_0
    :pswitch_0
    return-void

    .line 1065
    :pswitch_1
    const/4 v4, 0x1

    move v12, v4

    move v4, v2

    move v2, v12

    .line 1151
    :goto_1
    iget-object v5, p0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    .line 1152
    iget-object v5, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v5}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v5

    .line 1155
    if-ne v4, v3, :cond_5

    .line 1156
    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/am;->n:Lcom/google/android/libraries/social/autobackup/a;

    invoke-interface {v3, v5}, Lcom/google/android/libraries/social/autobackup/a;->a(I)V

    .line 1161
    :cond_0
    :goto_2
    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(Ljava/lang/Throwable;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 1166
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->f:Lcom/google/android/libraries/social/autobackup/au;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/au;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1167
    sget-object v3, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3, v2, v4}, Lcom/google/android/libraries/social/g/a/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/libraries/social/g/a/a;)J

    .line 1169
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-direct {p0, v2}, Lcom/google/android/libraries/social/autobackup/am;->b(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)V

    goto :goto_0

    .line 1073
    :pswitch_2
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v2, v8, v9}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->f(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move v2, v4

    move v4, v6

    .line 1074
    goto :goto_1

    .line 1078
    :pswitch_3
    const/16 v2, 0x22

    move v4, v6

    .line 1079
    goto :goto_1

    .line 1082
    :pswitch_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 1083
    move-wide/from16 v0, p2

    invoke-direct {p0, v8, v9, v0, v1}, Lcom/google/android/libraries/social/autobackup/am;->a(JJ)J

    move-result-wide v10

    cmp-long v2, v10, v8

    if-gez v2, :cond_1

    move v2, v5

    move v4, v3

    .line 1085
    goto :goto_1

    .line 1087
    :cond_1
    const/16 v2, 0xc8

    move v12, v4

    move v4, v2

    move v2, v12

    .line 1090
    goto :goto_1

    .line 1094
    :pswitch_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 1095
    move-wide/from16 v0, p2

    invoke-direct {p0, v8, v9, v0, v1}, Lcom/google/android/libraries/social/autobackup/am;->a(JJ)J

    move-result-wide v10

    cmp-long v4, v10, v8

    if-gez v4, :cond_2

    move v2, v5

    move v4, v3

    .line 1097
    goto :goto_1

    .line 1100
    :cond_2
    const/4 v4, 0x2

    move v12, v4

    move v4, v2

    move v2, v12

    .line 1102
    goto :goto_1

    .line 1106
    :pswitch_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1107
    move-wide/from16 v0, p2

    invoke-direct {p0, v4, v5, v0, v1}, Lcom/google/android/libraries/social/autobackup/am;->a(JJ)J

    move-result-wide v8

    cmp-long v4, v8, v4

    if-gez v4, :cond_3

    move v2, v3

    .line 1112
    :cond_3
    const/16 v4, 0x1f

    move v12, v4

    move v4, v2

    move v2, v12

    .line 1113
    goto/16 :goto_1

    .line 1117
    :pswitch_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1118
    move-wide/from16 v0, p2

    invoke-direct {p0, v4, v5, v0, v1}, Lcom/google/android/libraries/social/autobackup/am;->a(JJ)J

    move-result-wide v8

    cmp-long v4, v8, v4

    if-gez v4, :cond_4

    move v2, v3

    .line 1123
    :cond_4
    const/16 v4, 0x1e

    move v12, v4

    move v4, v2

    move v2, v12

    .line 1124
    goto/16 :goto_1

    :pswitch_8
    move v2, v4

    move v4, v3

    .line 1131
    goto/16 :goto_1

    .line 1136
    :pswitch_9
    const/16 v2, 0x27

    move v4, v3

    .line 1137
    goto/16 :goto_1

    .line 1142
    :pswitch_a
    const/16 v2, 0x26

    move v4, v3

    .line 1143
    goto/16 :goto_1

    .line 1157
    :cond_5
    if-ne v4, v6, :cond_0

    .line 1158
    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/am;->n:Lcom/google/android/libraries/social/autobackup/a;

    invoke-interface {v3, v5}, Lcom/google/android/libraries/social/autobackup/a;->b(I)V

    goto/16 :goto_2

    .line 1062
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_8
        :pswitch_5
        :pswitch_0
        :pswitch_9
        :pswitch_6
        :pswitch_7
        :pswitch_a
        :pswitch_3
    .end packed-switch
.end method

.method private b(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)V
    .locals 3

    .prologue
    .line 150
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 151
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    const-string v0, "iu.SyncTask"

    const-string v1, "Do not send broadcast because current sync thread is canceled"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.libraries.social.autobackup.upload_progress"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 157
    const-string v1, "upload_account_id"

    invoke-virtual {p1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 158
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/a/m;->a(Landroid/content/Context;)Landroid/support/v4/a/m;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/a/m;->a(Landroid/content/Intent;)Z

    goto :goto_0
.end method

.method private b(Z)V
    .locals 1

    .prologue
    .line 950
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->c:Lcom/google/android/libraries/social/networkcapability/a;

    invoke-interface {v0}, Lcom/google/android/libraries/social/networkcapability/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xf

    .line 955
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/autobackup/am;->a(I)V

    .line 956
    return-void

    .line 950
    :cond_0
    const/16 v0, 0xe

    goto :goto_0

    :cond_1
    const/16 v0, 0xd

    goto :goto_0
.end method

.method private c(I)V
    .locals 3

    .prologue
    .line 959
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 960
    const-string v0, "iu.SyncTask"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "REJECT "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " due to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/aj;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 963
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/autobackup/am;->a(I)V

    .line 964
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->g()I

    move-result v0

    const/16 v1, 0x28

    if-ne v0, v1, :cond_1

    .line 965
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->l:Lcom/google/android/libraries/social/autobackup/ax;

    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/ax;->b(I)V

    .line 967
    :cond_1
    return-void
.end method

.method private static c(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)Z
    .locals 4

    .prologue
    .line 715
    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->n()J

    move-result-wide v0

    .line 716
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->m()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 922
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/am;->l:Lcom/google/android/libraries/social/autobackup/ax;

    monitor-enter v1

    .line 923
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/am;->d:Z

    if-nez v0, :cond_0

    .line 924
    monitor-exit v1

    .line 936
    :goto_0
    return-void

    .line 928
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->l:Lcom/google/android/libraries/social/autobackup/ax;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/ax;->b()V

    .line 929
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->g()I

    move-result v0

    .line 930
    const/16 v2, 0x28

    if-ne v0, v2, :cond_1

    .line 931
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->l:Lcom/google/android/libraries/social/autobackup/ax;

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/autobackup/ax;->b(I)V

    .line 933
    :cond_1
    const-string v0, "iu.SyncTask"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 934
    const-string v0, "iu.SyncTask"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "   task done: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 936
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private e()Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 990
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->m()J

    move-result-wide v4

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->g:Lcom/google/android/libraries/social/autobackup/ao;

    invoke-interface {v0}, Lcom/google/android/libraries/social/autobackup/ao;->k()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    move v0, v1

    .line 992
    :goto_0
    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->g()I

    move-result v3

    const/16 v4, 0x14

    if-ne v3, v4, :cond_3

    move v3, v2

    .line 999
    :goto_1
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->g()I

    move-result v0

    const/16 v4, 0xa

    if-eq v0, v4, :cond_1

    if-nez v3, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->g:Lcom/google/android/libraries/social/autobackup/ao;

    invoke-interface {v0}, Lcom/google/android/libraries/social/autobackup/ao;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    return v2

    :cond_2
    move v0, v2

    .line 990
    goto :goto_0

    .line 992
    :cond_3
    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/am;->g:Lcom/google/android/libraries/social/autobackup/ao;

    invoke-interface {v3}, Lcom/google/android/libraries/social/autobackup/ao;->b()Z

    move-result v3

    goto :goto_1
.end method

.method private f()Z
    .locals 2

    .prologue
    .line 1006
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->g()I

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->g()I

    move-result v0

    const/16 v1, 0x14

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->g:Lcom/google/android/libraries/social/autobackup/ao;

    invoke-interface {v0}, Lcom/google/android/libraries/social/autobackup/ao;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Z
    .locals 2

    .prologue
    .line 1013
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->g()I

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->g()I

    move-result v0

    const/16 v1, 0x14

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->g:Lcom/google/android/libraries/social/autobackup/ao;

    invoke-interface {v0}, Lcom/google/android/libraries/social/autobackup/ao;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()Z
    .locals 2

    .prologue
    .line 1019
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->g()I

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 393
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->g()I

    move-result v0

    .line 395
    sparse-switch v0, :sswitch_data_0

    .line 409
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unknown upload reason: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 397
    :sswitch_0
    const-string v0, "instant_upload_state"

    .line 411
    :goto_0
    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 412
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 413
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/aj;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2, v1, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 415
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->g()I

    move-result v0

    const/16 v1, 0x28

    if-ne v0, v1, :cond_0

    .line 416
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->l:Lcom/google/android/libraries/social/autobackup/ax;

    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/ax;->b(I)V

    .line 418
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->l:Lcom/google/android/libraries/social/autobackup/ax;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/ax;->b()V

    .line 419
    return-void

    .line 400
    :sswitch_1
    const-string v0, "instant_share_state"

    goto :goto_0

    .line 403
    :sswitch_2
    const-string v0, "upload_all_state"

    goto :goto_0

    .line 406
    :sswitch_3
    const-string v0, "manual_upload_state"

    goto :goto_0

    .line 395
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_3
        0x14 -> :sswitch_1
        0x1e -> :sswitch_0
        0x28 -> :sswitch_2
    .end sparse-switch
.end method

.method protected final a(Landroid/content/SyncResult;)V
    .locals 17

    .prologue
    .line 311
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/autobackup/am;->l:Lcom/google/android/libraries/social/autobackup/ax;

    monitor-enter v3

    .line 312
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/autobackup/am;->d:Z

    if-nez v2, :cond_0

    .line 313
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 352
    :goto_0
    return-void

    .line 315
    :cond_0
    monitor-exit v3

    .line 317
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/libraries/social/mediaupload/t;->a(Ljava/lang/String;)I

    move-result v11

    .line 319
    :try_start_1
    const-string v2, "iu.SyncTask"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 320
    const-string v2, "iu.SyncTask"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "--- START syncing "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/autobackup/am;->k:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; account: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/libraries/social/autobackup/am;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    :cond_1
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/autobackup/am;->a(I)V

    .line 325
    const-string v2, "iu.SyncTask"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 326
    const-string v2, "iu.SyncTask"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "+++ START; upload started; "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    :cond_2
    move-object/from16 v0, p1

    iget-object v12, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/android/libraries/social/autobackup/am;->a(Landroid/content/SyncStats;)Z

    move-result v2

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    const-class v3, Lcom/google/android/libraries/social/autobackup/o;

    invoke-static {v2, v3}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/social/autobackup/o;

    const-string v3, "iu.SyncTask"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "iu.SyncTask"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "+++ START "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/autobackup/am;->k:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", task: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/autobackup/am;->g:Lcom/google/android/libraries/social/autobackup/ao;

    invoke-interface {v3}, Lcom/google/android/libraries/social/autobackup/ao;->a()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->g()I

    move-result v3

    const/16 v4, 0xa

    if-ne v3, v4, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/autobackup/o;->c(I)Z

    move-result v2

    if-eqz v2, :cond_c

    :cond_4
    const/4 v2, 0x1

    move v3, v2

    :goto_1
    if-eqz v3, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->g:Lcom/google/android/libraries/social/autobackup/ao;

    invoke-interface {v2}, Lcom/google/android/libraries/social/autobackup/ao;->j()Z

    move-result v2

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->g:Lcom/google/android/libraries/social/autobackup/ao;

    invoke-interface {v2}, Lcom/google/android/libraries/social/autobackup/ao;->l()Lcom/google/android/libraries/social/mediaupload/w;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/social/mediaupload/w;->a()Z

    move-result v2

    if-eqz v2, :cond_d

    const/4 v2, 0x1

    :goto_2
    if-nez v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->k()Z

    move-result v3

    if-eqz v3, :cond_e

    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/autobackup/am;->g:Lcom/google/android/libraries/social/autobackup/ao;

    invoke-interface {v3}, Lcom/google/android/libraries/social/autobackup/ao;->j()Z

    move-result v3

    if-eqz v3, :cond_e

    if-nez v2, :cond_e

    const/4 v8, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->v()Z

    move-result v2

    if-nez v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    :cond_6
    iget-wide v14, v12, Landroid/content/SyncStats;->numIoExceptions:J

    monitor-enter p0
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->t()Z

    move-result v2

    if-nez v2, :cond_f

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_7
    :goto_4
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/am;->c(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)Z

    move-result v2

    if-nez v2, :cond_2c

    iget-wide v2, v12, Landroid/content/SyncStats;->numIoExceptions:J

    cmp-long v2, v2, v14

    if-lez v2, :cond_2b

    const/4 v2, 0x1

    :goto_5
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/libraries/social/autobackup/am;->a(Z)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x240c8400

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/libraries/social/autobackup/am;->a(JJ)J

    .line 330
    :cond_8
    :goto_6
    const-string v2, "iu.SyncTask"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 331
    const-string v2, "iu.SyncTask"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "--- DONE syncing "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/autobackup/am;->k:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; account: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/libraries/social/autobackup/am;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 342
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->f:Lcom/google/android/libraries/social/autobackup/au;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/au;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 343
    sget-object v3, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3, v2, v4}, Lcom/google/android/libraries/social/g/a/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/libraries/social/g/a/a;)J

    .line 345
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->h()I

    move-result v2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->h()I

    move-result v2

    const/16 v3, 0xb

    if-ne v2, v3, :cond_b

    .line 347
    :cond_a
    invoke-direct/range {p0 .. p0}, Lcom/google/android/libraries/social/autobackup/am;->d()V

    .line 351
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    const-string v3, "iu.upload"

    invoke-static {v2, v11, v3}, Lcom/google/android/libraries/social/mediaupload/t;->a(Landroid/content/Context;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 315
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 328
    :cond_c
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_1

    :cond_d
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_e
    const/4 v8, 0x0

    goto/16 :goto_3

    :cond_f
    const/4 v2, 0x1

    const/4 v3, 0x0

    :try_start_4
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/libraries/social/autobackup/am;->a(ILjava/lang/Throwable;)V

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    new-instance v16, Lcom/google/android/libraries/social/autobackup/d;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/libraries/social/autobackup/am;->a:I

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/libraries/social/autobackup/d;-><init>(Landroid/content/Context;ILcom/google/android/libraries/social/autobackup/f;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->f:Lcom/google/android/libraries/social/autobackup/au;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/libraries/social/autobackup/am;->a:I

    invoke-static {v2, v3}, Lcom/google/android/libraries/social/autobackup/ab;->c(Lcom/google/android/libraries/social/autobackup/au;I)I
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result v2

    add-int/lit8 v9, v2, -0x1

    :try_start_6
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/autobackup/am;->a(Lcom/google/android/libraries/social/autobackup/d;)V

    move-object/from16 v0, v16

    iput-object v13, v0, Lcom/google/android/libraries/social/autobackup/d;->e:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-string v2, "iu.UploadsManager"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_10

    const-string v3, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v8, :cond_19

    const-string v2, ""

    :goto_7
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "upload full size; task: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v13}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", remaining: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_10
    invoke-virtual {v13}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->g()I

    move-result v3

    const/16 v4, 0xa

    if-eq v3, v4, :cond_1a

    const/4 v10, 0x1

    :goto_8
    invoke-virtual {v13}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v13}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->s()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_11

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/android/libraries/social/autobackup/d;->a:Landroid/content/Context;

    invoke-static {v4, v3}, Lcom/google/android/libraries/social/mediaupload/ae;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v13, v4}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->e(Ljava/lang/String;)V

    :cond_11
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/android/libraries/social/autobackup/d;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/libraries/b/a/b;->b(Landroid/net/Uri;)Z

    move-result v5

    if-nez v5, :cond_1b

    const/4 v4, 0x0

    :goto_9
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1c

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/d;->c:Lcom/google/android/libraries/social/mediaupload/c;

    invoke-virtual {v13}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->s()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v13}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v13}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v2 .. v10}, Lcom/google/android/libraries/social/mediaupload/c;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZ)Lcom/google/android/libraries/social/mediaupload/k;

    move-result-object v2

    move-object v3, v2

    :goto_a
    iget-object v2, v3, Lcom/google/android/libraries/social/mediaupload/k;->a:Lcom/google/android/libraries/social/mediaupload/w;

    if-eqz v2, :cond_12

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/android/libraries/social/autobackup/d;->a:Landroid/content/Context;

    move-object/from16 v0, v16

    iget v5, v0, Lcom/google/android/libraries/social/autobackup/d;->b:I

    invoke-static {v4, v5, v2}, Lcom/google/android/libraries/social/autobackup/AutoBackupProvider;->a(Landroid/content/Context;ILcom/google/android/libraries/social/mediaupload/w;)V

    :cond_12
    const/4 v2, 0x0

    iget-object v4, v3, Lcom/google/android/libraries/social/mediaupload/k;->b:Ljava/lang/String;

    if-eqz v4, :cond_13

    iget-object v2, v3, Lcom/google/android/libraries/social/mediaupload/k;->b:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/libraries/social/l/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_13
    iget-wide v4, v3, Lcom/google/android/libraries/social/mediaupload/k;->e:J

    invoke-virtual {v13, v4, v5}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v4

    iget-wide v6, v3, Lcom/google/android/libraries/social/mediaupload/k;->e:J

    invoke-virtual {v4, v6, v7}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v4

    iget-object v5, v3, Lcom/google/android/libraries/social/mediaupload/k;->d:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->d(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(Ljava/lang/String;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v2

    iget-wide v4, v3, Lcom/google/android/libraries/social/mediaupload/k;->c:J

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v2

    const/16 v3, 0x190

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    :try_end_6
    .catch Lcom/google/android/libraries/social/mediaupload/h; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lcom/google/android/libraries/social/mediaupload/e; {:try_start_6 .. :try_end_6} :catch_2
    .catch Lcom/google/android/libraries/social/mediaupload/i; {:try_start_6 .. :try_end_6} :catch_3
    .catch Lcom/google/android/libraries/social/mediaupload/l; {:try_start_6 .. :try_end_6} :catch_4
    .catch Lcom/google/android/libraries/social/mediaupload/n; {:try_start_6 .. :try_end_6} :catch_5
    .catch Lcom/google/android/libraries/social/mediaupload/g; {:try_start_6 .. :try_end_6} :catch_6
    .catch Lcom/google/android/libraries/social/mediaupload/o; {:try_start_6 .. :try_end_6} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_8
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    const/4 v2, 0x0

    :try_start_7
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/libraries/social/autobackup/am;->a(Lcom/google/android/libraries/social/autobackup/d;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    goto/16 :goto_4

    .line 333
    :catch_0
    move-exception v2

    .line 334
    :try_start_8
    const-string v3, "iu.SyncTask"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 335
    const-string v3, "iu.SyncTask"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "+++ SKIP task "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/autobackup/am;->k:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; task: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 338
    :cond_14
    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v3, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v3, Landroid/content/SyncStats;->numIoExceptions:J

    .line 339
    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v3, Landroid/content/SyncStats;->numSkippedEntries:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v3, Landroid/content/SyncStats;->numSkippedEntries:J

    const/16 v3, 0xb

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2}, Lcom/google/android/libraries/social/autobackup/am;->a(ILjava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 342
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->f:Lcom/google/android/libraries/social/autobackup/au;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/au;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 343
    sget-object v3, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3, v2, v4}, Lcom/google/android/libraries/social/g/a/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/libraries/social/g/a/a;)J

    .line 345
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->h()I

    move-result v2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->h()I

    move-result v2

    const/16 v3, 0xb

    if-ne v2, v3, :cond_16

    .line 347
    :cond_15
    invoke-direct/range {p0 .. p0}, Lcom/google/android/libraries/social/autobackup/am;->d()V

    .line 351
    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    const-string v3, "iu.upload"

    invoke-static {v2, v11, v3}, Lcom/google/android/libraries/social/mediaupload/t;->a(Landroid/content/Context;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 328
    :catchall_1
    move-exception v2

    :try_start_9
    monitor-exit p0

    throw v2
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 342
    :catchall_2
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/autobackup/am;->f:Lcom/google/android/libraries/social/autobackup/au;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/autobackup/au;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 343
    sget-object v4, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v4, v3, v5}, Lcom/google/android/libraries/social/g/a/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/libraries/social/g/a/a;)J

    .line 345
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->h()I

    move-result v3

    const/4 v4, 0x4

    if-eq v3, v4, :cond_17

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->h()I

    move-result v3

    const/16 v4, 0xb

    if-ne v3, v4, :cond_18

    .line 347
    :cond_17
    invoke-direct/range {p0 .. p0}, Lcom/google/android/libraries/social/autobackup/am;->d()V

    .line 351
    :cond_18
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    const-string v4, "iu.upload"

    invoke-static {v3, v11, v4}, Lcom/google/android/libraries/social/mediaupload/t;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 352
    throw v2

    .line 328
    :cond_19
    :try_start_a
    const-string v2, "Don\'t "

    goto/16 :goto_7

    :cond_1a
    const/4 v10, 0x0

    goto/16 :goto_8

    :cond_1b
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "_camera_roll_"

    invoke-static {v4, v3, v5}, Lcom/google/android/libraries/b/a/b;->a(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_9

    :cond_1c
    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/android/libraries/social/autobackup/d;->c:Lcom/google/android/libraries/social/mediaupload/c;

    invoke-virtual {v5, v3, v4, v2}, Lcom/google/android/libraries/social/mediaupload/c;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/mediaupload/k;
    :try_end_a
    .catch Lcom/google/android/libraries/social/mediaupload/h; {:try_start_a .. :try_end_a} :catch_1
    .catch Lcom/google/android/libraries/social/mediaupload/e; {:try_start_a .. :try_end_a} :catch_2
    .catch Lcom/google/android/libraries/social/mediaupload/i; {:try_start_a .. :try_end_a} :catch_3
    .catch Lcom/google/android/libraries/social/mediaupload/l; {:try_start_a .. :try_end_a} :catch_4
    .catch Lcom/google/android/libraries/social/mediaupload/n; {:try_start_a .. :try_end_a} :catch_5
    .catch Lcom/google/android/libraries/social/mediaupload/g; {:try_start_a .. :try_end_a} :catch_6
    .catch Lcom/google/android/libraries/social/mediaupload/o; {:try_start_a .. :try_end_a} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_8
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    move-result-object v2

    move-object v3, v2

    goto/16 :goto_a

    :catch_1
    move-exception v2

    :try_start_b
    const-string v3, "iu.SyncTask"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1d

    const-string v3, "iu.SyncTask"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "PAUSE task; media changed: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->d(Ljava/lang/String;)V

    const/4 v3, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2}, Lcom/google/android/libraries/social/autobackup/am;->a(ILjava/lang/Throwable;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    const/4 v2, 0x0

    :try_start_c
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/libraries/social/autobackup/am;->a(Lcom/google/android/libraries/social/autobackup/d;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/am;->c(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    goto/16 :goto_4

    :catch_2
    move-exception v2

    :try_start_d
    invoke-virtual {v2}, Lcom/google/android/libraries/social/mediaupload/e;->a()Z

    move-result v3

    if-eqz v3, :cond_1f

    const-string v3, "iu.SyncTask"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1e

    const-string v3, "iu.SyncTask"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "+++ SKIP record; invalid MIME type: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/libraries/social/mediaupload/e;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const/16 v4, 0x12c

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v3

    const/16 v4, 0x21

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(Ljava/lang/Throwable;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    :goto_b
    const/4 v2, 0x0

    :try_start_e
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/libraries/social/autobackup/am;->a(Lcom/google/android/libraries/social/autobackup/d;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/am;->c(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_0
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    goto/16 :goto_4

    :cond_1f
    :try_start_f
    const-string v3, "iu.SyncTask"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_20

    const-string v3, "iu.SyncTask"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "+++ QUEUE task "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/autobackup/am;->k:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; placeholder MIME type; "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_20
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/32 v6, 0xea60

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/google/android/libraries/social/autobackup/am;->a(JJ)J

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2}, Lcom/google/android/libraries/social/autobackup/am;->a(Landroid/content/SyncStats;Ljava/lang/Throwable;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    goto :goto_b

    :catchall_3
    move-exception v2

    const/4 v3, 0x0

    :try_start_10
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/libraries/social/autobackup/am;->a(Lcom/google/android/libraries/social/autobackup/d;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v3}, Lcom/google/android/libraries/social/autobackup/am;->c(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)Z

    move-result v3

    if-eqz v3, :cond_21

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    :cond_21
    throw v2
    :try_end_10
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_0
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    :catch_3
    move-exception v2

    :try_start_11
    const-string v3, "iu.SyncTask"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_22

    const-string v3, "iu.SyncTask"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "PAUSE task; media unavailable: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_22
    const-wide/16 v4, 0x5

    move-object/from16 v0, p1

    iput-wide v4, v0, Landroid/content/SyncResult;->delayUntil:J

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v3, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v3, Landroid/content/SyncStats;->numIoExceptions:J

    const/4 v3, 0x6

    const-wide/32 v4, 0x493e0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v2}, Lcom/google/android/libraries/social/autobackup/am;->a(IJLjava/lang/Throwable;)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    const/4 v2, 0x0

    :try_start_12
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/libraries/social/autobackup/am;->a(Lcom/google/android/libraries/social/autobackup/d;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/am;->c(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    :try_end_12
    .catch Ljava/lang/Throwable; {:try_start_12 .. :try_end_12} :catch_0
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    goto/16 :goto_4

    :catch_4
    move-exception v2

    :try_start_13
    const-string v3, "iu.SyncTask"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_23

    const-string v3, "iu.SyncTask"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "PAUSE task; transient error: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_23
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/mediaupload/l;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->d(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/libraries/social/mediaupload/l;->b()Z

    move-result v3

    if-nez v3, :cond_24

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v3, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v3, Landroid/content/SyncStats;->numIoExceptions:J

    :goto_c
    const/4 v3, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2}, Lcom/google/android/libraries/social/autobackup/am;->a(ILjava/lang/Throwable;)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_3

    const/4 v2, 0x0

    :try_start_14
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/libraries/social/autobackup/am;->a(Lcom/google/android/libraries/social/autobackup/d;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/am;->c(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    :try_end_14
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_14} :catch_0
    .catchall {:try_start_14 .. :try_end_14} :catchall_2

    goto/16 :goto_4

    :cond_24
    const-wide/16 v4, 0x5460

    :try_start_15
    move-object/from16 v0, p1

    iput-wide v4, v0, Landroid/content/SyncResult;->delayUntil:J

    goto :goto_c

    :catch_5
    move-exception v2

    const-string v3, "iu.SyncTask"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_25

    const-string v3, "iu.SyncTask"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "PAUSE task; unauthorized: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_25
    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v3, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v3, Landroid/content/SyncStats;->numAuthExceptions:J

    const/16 v3, 0x9

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2}, Lcom/google/android/libraries/social/autobackup/am;->a(ILjava/lang/Throwable;)V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_3

    const/4 v2, 0x0

    :try_start_16
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/libraries/social/autobackup/am;->a(Lcom/google/android/libraries/social/autobackup/d;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/am;->c(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    :try_end_16
    .catch Ljava/lang/Throwable; {:try_start_16 .. :try_end_16} :catch_0
    .catchall {:try_start_16 .. :try_end_16} :catchall_2

    goto/16 :goto_4

    :catch_6
    move-exception v2

    :try_start_17
    invoke-static {}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a()Z

    move-result v3

    if-eqz v3, :cond_27

    const-string v3, "iu.SyncTask"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_26

    const-string v3, "iu.SyncTask"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "FAIL task: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_26
    const/4 v3, 0x5

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2}, Lcom/google/android/libraries/social/autobackup/am;->a(ILjava/lang/Throwable;)V

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numSkippedEntries:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v2, Landroid/content/SyncStats;->numSkippedEntries:J
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_3

    :goto_d
    const/4 v2, 0x0

    :try_start_18
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/libraries/social/autobackup/am;->a(Lcom/google/android/libraries/social/autobackup/d;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/am;->c(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    :try_end_18
    .catch Ljava/lang/Throwable; {:try_start_18 .. :try_end_18} :catch_0
    .catchall {:try_start_18 .. :try_end_18} :catchall_2

    goto/16 :goto_4

    :cond_27
    :try_start_19
    const-string v3, "iu.SyncTask"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_28

    const-string v3, "iu.SyncTask"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "PAUSE task; media unmounted: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_28
    const/4 v3, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2}, Lcom/google/android/libraries/social/autobackup/am;->a(ILjava/lang/Throwable;)V

    goto :goto_d

    :catch_7
    move-exception v2

    const-string v3, "iu.SyncTask"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_29

    const-string v3, "iu.SyncTask"

    const-string v4, "FAIL task: permanent failure: "

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_29
    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v3, Landroid/content/SyncStats;->numSkippedEntries:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v3, Landroid/content/SyncStats;->numSkippedEntries:J

    const/4 v3, 0x5

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2}, Lcom/google/android/libraries/social/autobackup/am;->a(ILjava/lang/Throwable;)V
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_3

    const/4 v2, 0x0

    :try_start_1a
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/libraries/social/autobackup/am;->a(Lcom/google/android/libraries/social/autobackup/d;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/am;->c(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    :try_end_1a
    .catch Ljava/lang/Throwable; {:try_start_1a .. :try_end_1a} :catch_0
    .catchall {:try_start_1a .. :try_end_1a} :catchall_2

    goto/16 :goto_4

    :catch_8
    move-exception v2

    :try_start_1b
    const-string v3, "iu.SyncTask"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2a

    const-string v3, "iu.SyncTask"

    const-string v4, "FAIL task: permanent failure: "

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2a
    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v3, Landroid/content/SyncStats;->numSkippedEntries:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v3, Landroid/content/SyncStats;->numSkippedEntries:J

    const/4 v3, 0x5

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2}, Lcom/google/android/libraries/social/autobackup/am;->a(ILjava/lang/Throwable;)V
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_3

    const/4 v2, 0x0

    :try_start_1c
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/libraries/social/autobackup/am;->a(Lcom/google/android/libraries/social/autobackup/d;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/am;->c(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    goto/16 :goto_4

    :cond_2b
    const/4 v2, 0x0

    goto/16 :goto_5

    :cond_2c
    const/4 v2, 0x4

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/libraries/social/autobackup/am;->a(ILjava/lang/Throwable;)V

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numEntries:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v2, Landroid/content/SyncStats;->numEntries:J

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numInserts:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v2, Landroid/content/SyncStats;->numInserts:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_30

    const-string v2, "iu.SyncTask"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2d

    const-string v2, "iu.SyncTask"

    const-string v3, "no user owns the photo"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2d
    :goto_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->i()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_2e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    :cond_2e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->r()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_2f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->f(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    :cond_2f
    const-string v2, "iu.SyncTask"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "iu.SyncTask"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "+++ DONE; upload finished; "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    :cond_30
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->o()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/autobackup/am;->m:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v3, v2}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v2

    const-string v3, "gaia_id"

    invoke-interface {v2, v3}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->p()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->l()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/al;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/al;

    move-result-object v3

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/libraries/social/autobackup/al;->a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1c
    .catch Ljava/lang/Throwable; {:try_start_1c .. :try_end_1c} :catch_0
    .catchall {:try_start_1c .. :try_end_1c} :catchall_2

    goto/16 :goto_e
.end method

.method public final a(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)V
    .locals 5

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    if-ne p1, v0, :cond_3

    .line 131
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/am;->l:Lcom/google/android/libraries/social/autobackup/ax;

    monitor-enter v1

    .line 132
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/am;->d:Z

    if-eqz v0, :cond_2

    .line 133
    const-string v0, "iu.SyncTask"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    const-string v0, "iu.SyncTask"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  progress: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->t()Z

    move-result v0

    if-nez v0, :cond_1

    .line 137
    monitor-exit v1

    .line 147
    :goto_0
    return-void

    .line 139
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->h()I

    move-result v0

    const-wide/32 v2, 0x240c8400

    const/4 v4, 0x0

    invoke-direct {p0, v0, v2, v3, v4}, Lcom/google/android/libraries/social/autobackup/am;->b(IJLjava/lang/Throwable;)V

    .line 141
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->l:Lcom/google/android/libraries/social/autobackup/ax;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/ax;->b()V

    .line 143
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/autobackup/am;->b(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)V

    goto :goto_0

    .line 143
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final a()Z
    .locals 8

    .prologue
    const/4 v7, 0x6

    const/4 v6, 0x4

    const/4 v1, 0x0

    const/4 v5, 0x3

    .line 213
    new-instance v0, Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->m:Lcom/google/android/libraries/social/account/b;

    iget v3, p0, Lcom/google/android/libraries/social/autobackup/am;->a:I

    invoke-interface {v2, v3}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v2

    const-string v3, "account_name"

    invoke-interface {v2, v3}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.google"

    invoke-direct {v0, v2, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/aj;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v2

    .line 216
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->i:Landroid/content/Context;

    const-class v3, Lcom/google/android/libraries/social/networkcapability/a;

    invoke-static {v0, v3}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/networkcapability/a;

    .line 219
    const-string v3, "iu.SyncTask"

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 220
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "-- isAccepted state -- isBackgroundSync: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/libraries/social/autobackup/am;->h()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " getMasterSync: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " doAutoSync: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isConnected: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Lcom/google/android/libraries/social/networkcapability/a;->a()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " backgroundDataAllowed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/am;->h:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    iget-boolean v4, v4, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->d:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isPlugged: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/am;->h:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    iget-boolean v4, v4, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isSyncOnBattery: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/libraries/social/autobackup/am;->g()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isNetworkMetered: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/am;->h:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    iget-boolean v4, v4, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->b:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isMobileNetwork: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Lcom/google/android/libraries/social/networkcapability/a;->f()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isSyncOnWifiOnly "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/libraries/social/autobackup/am;->e()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isRoaming "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/am;->h:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    iget-boolean v4, v4, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->c:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isSyncOnRoaming "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/libraries/social/autobackup/am;->f()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 245
    const-string v4, "iu.SyncTask"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/autobackup/am;->h()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 251
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v3

    if-nez v3, :cond_2

    .line 252
    const-string v0, "iu.SyncTask"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 253
    const-string v0, "iu.SyncTask"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reject "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " because master auto sync is off"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    :cond_1
    invoke-direct {p0, v7}, Lcom/google/android/libraries/social/autobackup/am;->c(I)V

    move v0, v1

    .line 305
    :goto_0
    return v0

    .line 259
    :cond_2
    if-nez v2, :cond_4

    .line 260
    const-string v0, "iu.SyncTask"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 261
    const-string v0, "iu.SyncTask"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reject "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " because auto sync is off"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    :cond_3
    invoke-direct {p0, v7}, Lcom/google/android/libraries/social/autobackup/am;->c(I)V

    move v0, v1

    .line 264
    goto :goto_0

    .line 268
    :cond_4
    invoke-interface {v0}, Lcom/google/android/libraries/social/networkcapability/a;->a()Z

    move-result v2

    if-nez v2, :cond_6

    .line 269
    const-string v0, "iu.SyncTask"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 270
    const-string v0, "iu.SyncTask"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reject "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " on no network"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    :cond_5
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/autobackup/am;->c(I)V

    move v0, v1

    .line 273
    goto :goto_0

    .line 275
    :cond_6
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->h:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    iget-boolean v2, v2, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->d:Z

    if-nez v2, :cond_8

    invoke-direct {p0}, Lcom/google/android/libraries/social/autobackup/am;->h()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 276
    const-string v0, "iu.SyncTask"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 277
    const-string v0, "iu.SyncTask"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reject "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for disabled background data"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    :cond_7
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/autobackup/am;->c(I)V

    move v0, v1

    .line 280
    goto/16 :goto_0

    .line 282
    :cond_8
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->h:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    iget-boolean v2, v2, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a:Z

    if-nez v2, :cond_a

    invoke-direct {p0}, Lcom/google/android/libraries/social/autobackup/am;->g()Z

    move-result v2

    if-nez v2, :cond_a

    .line 283
    const-string v0, "iu.SyncTask"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 284
    const-string v0, "iu.SyncTask"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reject "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " on battery"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    :cond_9
    invoke-direct {p0, v6}, Lcom/google/android/libraries/social/autobackup/am;->c(I)V

    move v0, v1

    .line 287
    goto/16 :goto_0

    .line 289
    :cond_a
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->h:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    iget-boolean v2, v2, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->b:Z

    if-nez v2, :cond_b

    invoke-interface {v0}, Lcom/google/android/libraries/social/networkcapability/a;->f()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 290
    :cond_b
    invoke-direct {p0}, Lcom/google/android/libraries/social/autobackup/am;->e()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 291
    const-string v0, "iu.SyncTask"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 292
    const-string v0, "iu.SyncTask"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reject "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for non-wifi connection"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    :cond_c
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/autobackup/am;->c(I)V

    move v0, v1

    .line 295
    goto/16 :goto_0

    .line 296
    :cond_d
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/am;->h:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    iget-boolean v0, v0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->c:Z

    if-eqz v0, :cond_f

    invoke-direct {p0}, Lcom/google/android/libraries/social/autobackup/am;->f()Z

    move-result v0

    if-nez v0, :cond_f

    .line 297
    const-string v0, "iu.SyncTask"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 298
    const-string v0, "iu.SyncTask"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reject "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for roaming"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    :cond_e
    invoke-direct {p0, v5}, Lcom/google/android/libraries/social/autobackup/am;->c(I)V

    move v0, v1

    .line 301
    goto/16 :goto_0

    .line 305
    :cond_f
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method protected final a(J)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 426
    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/am;->l:Lcom/google/android/libraries/social/autobackup/ax;

    monitor-enter v3

    .line 428
    :try_start_0
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    iget-wide v4, v2, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->id:J

    cmp-long v2, p1, v4

    if-nez v2, :cond_1

    move v2, v0

    .line 429
    :goto_0
    iget-boolean v4, p0, Lcom/google/android/libraries/social/autobackup/am;->d:Z

    if-eqz v4, :cond_2

    if-nez v2, :cond_2

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/am;->d:Z

    .line 430
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 431
    if-eqz v2, :cond_0

    .line 432
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/autobackup/am;->b(I)V

    .line 434
    :cond_0
    return v2

    :cond_1
    move v2, v1

    .line 428
    goto :goto_0

    :cond_2
    move v0, v1

    .line 429
    goto :goto_1

    .line 430
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method protected final b()V
    .locals 3

    .prologue
    .line 365
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366
    const-string v0, "iu.SyncTask"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "--- CANCEL sync "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; task: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/am;->l:Lcom/google/android/libraries/social/autobackup/ax;

    monitor-enter v1

    .line 369
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/am;->d:Z

    .line 370
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 371
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/autobackup/am;->b(I)V

    .line 372
    return-void

    .line 370
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final c()V
    .locals 3

    .prologue
    .line 379
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 380
    const-string v0, "iu.SyncTask"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "--- STOP sync "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; task: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/am;->l:Lcom/google/android/libraries/social/autobackup/ax;

    monitor-enter v1

    .line 383
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/am;->d:Z

    .line 384
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/autobackup/am;->b(I)V

    .line 386
    return-void

    .line 384
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 164
    const-string v0, "[%s; reason: %s, id: %d, accountId: %d]"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/am;->k:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/am;->j:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    iget-wide v4, v3, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lcom/google/android/libraries/social/autobackup/am;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
