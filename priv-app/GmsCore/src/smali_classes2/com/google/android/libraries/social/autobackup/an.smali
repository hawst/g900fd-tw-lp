.class public Lcom/google/android/libraries/social/autobackup/an;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;IZ)V
    .locals 2

    .prologue
    .line 33
    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p0, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    .line 34
    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/account/b;->b(I)Lcom/google/android/libraries/social/account/d;

    move-result-object v0

    .line 35
    const-string v1, "has_synced_photo_uploads"

    invoke-interface {v0, v1, p2}, Lcom/google/android/libraries/social/account/d;->a(Ljava/lang/String;Z)Lcom/google/android/libraries/social/account/d;

    .line 36
    invoke-interface {v0}, Lcom/google/android/libraries/social/account/d;->c()I

    .line 37
    return-void
.end method

.method public static a(Landroid/content/Context;I)Z
    .locals 2

    .prologue
    .line 24
    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p0, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    .line 25
    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v0

    .line 26
    const-string v1, "has_synced_photo_uploads"

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/c;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
