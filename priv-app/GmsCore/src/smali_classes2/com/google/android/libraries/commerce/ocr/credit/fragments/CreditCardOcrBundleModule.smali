.class public final Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrBundleModule;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final DEFAULT_UI_OPTION:I = 0x1

.field private static final TARGET_COLLECTION_PREVIEW_HEIGHT:I = 0x2d0

.field private static final TARGET_COLLECTION_PREVIEW_WIDTH:I = 0x500

.field private static final TARGET_PREVIEW_HEIGHT:I = 0x2d0

.field private static final TARGET_PREVIEW_WIDTH:I = 0x500


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method

.method public static provideDebugContinuousSave(Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 59
    const-string v0, "DEBUG_CONTINUOUS_SAVE"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static provideDebugValidateResult(Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 35
    const-string v0, "DEBUG_VALIDATE_RESULT"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static provideEdgeFinderWidthToOuterBoundingBoxEdgeLengthRatio(Landroid/os/Bundle;)F
    .locals 2

    .prologue
    .line 26
    const-string v0, "EDGE_FINDER_WIDTH_TO_OUTER_BOUNDING_BOX_EDGE_LENGTH_RATIO"

    const v1, 0x3da3d70a    # 0.08f

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public static provideImageCaptureMode(Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 31
    const-string v0, "DEBUG_IMAGE_CAPTURE_MODE"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static provideMinPerformCreditCardOcrIntervalInMs(Landroid/os/Bundle;)I
    .locals 2

    .prologue
    .line 67
    const-string v0, "MIN_PERFORM_LOYALTY_CARD_OCR_INTERVAL_IN_MS"

    const/16 v1, 0x5a

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static provideOrientationOffset(Landroid/os/Bundle;)I
    .locals 2

    .prologue
    .line 80
    const-string v0, "ORIENTATION_OFFSET"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static provideRecognizeExpirationDate(Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 47
    const-string v0, "RECOGNIZE_EXPIRATION_DATE"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static provideTargetPreviewSize(Landroid/os/Bundle;)Landroid/graphics/Point;
    .locals 4

    .prologue
    const/16 v3, 0x500

    const/16 v2, 0x2d0

    .line 71
    const-string v0, "DEBUG_IMAGE_CAPTURE_MODE"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v3, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 74
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v3, v2}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_0
.end method

.method public static provideThresholdExpirationDateMinConfidence4Digit(Landroid/os/Bundle;)F
    .locals 2

    .prologue
    .line 51
    const-string v0, "THRESHOLD_EXPIRATION_DATE_MIN_CONFIDENCE_4_DIGIT"

    const v1, 0x404b851f    # 3.18f

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public static provideThresholdExpirationDateMinConfidence6Digit(Landroid/os/Bundle;)F
    .locals 2

    .prologue
    .line 55
    const-string v0, "THRESHOLD_EXPIRATION_DATE_MIN_CONFIDENCE_6_DIGIT"

    const v1, 0x40933333    # 4.6f

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public static provideThresholdMeanDigitConfidence(Landroid/os/Bundle;)F
    .locals 2

    .prologue
    .line 43
    const-string v0, "THRESHOLD_MEAN_DIGIT_CONFIDENCE"

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public static provideThresholdMinDigitConfidence(Landroid/os/Bundle;)F
    .locals 2

    .prologue
    .line 39
    const-string v0, "THRESHOLD_MIN_DIGIT_CONFIDENCE"

    const v1, 0x3f333333    # 0.7f

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public static provideUiOption(Landroid/os/Bundle;)I
    .locals 2

    .prologue
    .line 63
    const-string v0, "UI_OPTION"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method
