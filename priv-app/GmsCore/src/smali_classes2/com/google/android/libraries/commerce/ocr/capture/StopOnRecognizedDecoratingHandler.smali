.class public Lcom/google/android/libraries/commerce/ocr/capture/StopOnRecognizedDecoratingHandler;
.super Lcom/google/android/libraries/commerce/ocr/capture/DecoratingOcrHandler;
.source "SourceFile"


# static fields
.field private static final TAG:Ljava/lang/String; = "StopOnRecognizedDecoratingHandler"


# instance fields
.field private pipeline:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/DecoratingOcrHandler;-><init>(Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;)V

    .line 22
    return-void
.end method

.method public static create(Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;)Lcom/google/android/libraries/commerce/ocr/capture/StopOnRecognizedDecoratingHandler;
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/capture/StopOnRecognizedDecoratingHandler;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/commerce/ocr/capture/StopOnRecognizedDecoratingHandler;-><init>(Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;)V

    return-object v0
.end method

.method private stopProcessing()V
    .locals 3

    .prologue
    .line 41
    const-string v0, "StopOnRecognizedDecoratingHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Stopping frame processor: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/capture/StopOnRecognizedDecoratingHandler;->pipeline:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/StopOnRecognizedDecoratingHandler;->pipeline:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/StopOnRecognizedDecoratingHandler;->pipeline:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->shutdown()V

    .line 45
    :cond_0
    return-void
.end method


# virtual methods
.method public onRecognized(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/libraries/commerce/ocr/capture/StopOnRecognizedDecoratingHandler;->stopProcessing()V

    .line 31
    invoke-super {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/DecoratingOcrHandler;->onRecognized(Ljava/lang/Object;)V

    .line 32
    return-void
.end method

.method public onRecognized(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/libraries/commerce/ocr/capture/StopOnRecognizedDecoratingHandler;->stopProcessing()V

    .line 37
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/commerce/ocr/capture/DecoratingOcrHandler;->onRecognized(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 38
    return-void
.end method

.method public setPipeline(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/capture/StopOnRecognizedDecoratingHandler;->pipeline:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    .line 26
    return-void
.end method
