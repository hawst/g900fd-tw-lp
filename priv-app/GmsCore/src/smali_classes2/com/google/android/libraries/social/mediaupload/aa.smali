.class final Lcom/google/android/libraries/social/mediaupload/aa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/nio/channels/ReadableByteChannel;


# instance fields
.field private final a:Ljava/nio/channels/ReadableByteChannel;

.field private final b:Lcom/google/android/libraries/social/mediaupload/ad;

.field private final c:J

.field private d:J

.field private volatile e:Z

.field private final f:Ljava/lang/Runnable;


# direct methods
.method private constructor <init>(Ljava/nio/channels/ReadableByteChannel;Lcom/google/android/libraries/social/mediaupload/ad;J)V
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/mediaupload/aa;->e:Z

    .line 113
    new-instance v0, Lcom/google/android/libraries/social/mediaupload/ab;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/social/mediaupload/ab;-><init>(Lcom/google/android/libraries/social/mediaupload/aa;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/mediaupload/aa;->f:Ljava/lang/Runnable;

    .line 124
    iput-object p1, p0, Lcom/google/android/libraries/social/mediaupload/aa;->a:Ljava/nio/channels/ReadableByteChannel;

    .line 125
    iput-object p2, p0, Lcom/google/android/libraries/social/mediaupload/aa;->b:Lcom/google/android/libraries/social/mediaupload/ad;

    .line 126
    iput-wide p3, p0, Lcom/google/android/libraries/social/mediaupload/aa;->c:J

    .line 127
    return-void
.end method

.method synthetic constructor <init>(Ljava/nio/channels/ReadableByteChannel;Lcom/google/android/libraries/social/mediaupload/ad;JB)V
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/libraries/social/mediaupload/aa;-><init>(Ljava/nio/channels/ReadableByteChannel;Lcom/google/android/libraries/social/mediaupload/ad;J)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/libraries/social/mediaupload/aa;)J
    .locals 2

    .prologue
    .line 107
    iget-wide v0, p0, Lcom/google/android/libraries/social/mediaupload/aa;->d:J

    return-wide v0
.end method

.method static synthetic a(Lcom/google/android/libraries/social/mediaupload/aa;Z)Z
    .locals 0

    .prologue
    .line 107
    iput-boolean p1, p0, Lcom/google/android/libraries/social/mediaupload/aa;->e:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/libraries/social/mediaupload/aa;)J
    .locals 2

    .prologue
    .line 107
    iget-wide v0, p0, Lcom/google/android/libraries/social/mediaupload/aa;->c:J

    return-wide v0
.end method

.method static synthetic c(Lcom/google/android/libraries/social/mediaupload/aa;)Lcom/google/android/libraries/social/mediaupload/ad;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/aa;->b:Lcom/google/android/libraries/social/mediaupload/ad;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/libraries/social/mediaupload/aa;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/aa;->f:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/aa;->a:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/ReadableByteChannel;->close()V

    .line 159
    return-void
.end method

.method public final isOpen()Z
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/aa;->a:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/ReadableByteChannel;->isOpen()Z

    move-result v0

    return v0
.end method

.method public final read(Ljava/nio/ByteBuffer;)I
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/aa;->a:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v0, p1}, Ljava/nio/channels/ReadableByteChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 133
    iget-wide v2, p0, Lcom/google/android/libraries/social/mediaupload/aa;->d:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/libraries/social/mediaupload/aa;->d:J

    .line 135
    iget-wide v2, p0, Lcom/google/android/libraries/social/mediaupload/aa;->d:J

    iget-wide v4, p0, Lcom/google/android/libraries/social/mediaupload/aa;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/libraries/social/mediaupload/aa;->e:Z

    if-nez v1, :cond_1

    .line 136
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_2

    .line 137
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lcom/google/android/libraries/social/mediaupload/aa;->f:Ljava/lang/Runnable;

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 148
    :cond_1
    :goto_0
    return v0

    .line 139
    :cond_2
    new-instance v1, Lcom/google/android/libraries/social/mediaupload/ac;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/social/mediaupload/ac;-><init>(Lcom/google/android/libraries/social/mediaupload/aa;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Void;

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/mediaupload/ac;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
