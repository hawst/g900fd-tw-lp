.class public Lcom/google/android/libraries/social/experiments/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/libraries/social/experiments/e;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/google/android/libraries/social/f/e;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/social/experiments/e;)V
    .locals 3

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x1f

    if-le v0, v1, :cond_0

    .line 41
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\".length() > 31"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_0
    iput-object p1, p0, Lcom/google/android/libraries/social/experiments/a;->a:Ljava/lang/String;

    .line 44
    iput-object p2, p0, Lcom/google/android/libraries/social/experiments/a;->b:Ljava/lang/String;

    .line 45
    iput-object p3, p0, Lcom/google/android/libraries/social/experiments/a;->d:Ljava/lang/String;

    .line 46
    iput-object p4, p0, Lcom/google/android/libraries/social/experiments/a;->c:Lcom/google/android/libraries/social/experiments/e;

    .line 48
    new-instance v0, Lcom/google/android/libraries/social/f/e;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/android/libraries/social/f/e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/experiments/a;->e:Lcom/google/android/libraries/social/f/e;

    .line 49
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/social/experiments/e;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/libraries/social/experiments/a;->c:Lcom/google/android/libraries/social/experiments/e;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/libraries/social/experiments/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/libraries/social/experiments/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/social/f/e;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/libraries/social/experiments/a;->e:Lcom/google/android/libraries/social/f/e;

    return-object v0
.end method
