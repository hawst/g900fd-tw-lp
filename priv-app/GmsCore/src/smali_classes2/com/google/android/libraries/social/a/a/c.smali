.class public Lcom/google/android/libraries/social/a/a/c;
.super Lcom/google/android/libraries/social/i/an;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/a/d;


# instance fields
.field protected final a:Lcom/google/android/libraries/social/a/a;

.field private c:Lcom/google/android/libraries/social/i/ab;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/libraries/social/i/an;-><init>()V

    .line 22
    new-instance v0, Lcom/google/android/libraries/social/a/a;

    invoke-direct {v0}, Lcom/google/android/libraries/social/a/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/a/a/c;->a:Lcom/google/android/libraries/social/a/a;

    return-void
.end method

.method static synthetic a(Lcom/google/android/libraries/social/a/a/c;)Lcom/google/android/libraries/social/i/i;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a/c;->b:Lcom/google/android/libraries/social/i/i;

    return-object v0
.end method


# virtual methods
.method public final U_()Lcom/google/android/libraries/social/a/a;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a/c;->a:Lcom/google/android/libraries/social/a/a;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a/c;->a:Lcom/google/android/libraries/social/a/a;

    new-instance v1, Lcom/google/android/libraries/social/a/a/a/c;

    iget-object v2, p0, Lcom/google/android/libraries/social/a/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-direct {v1, p0, v2}, Lcom/google/android/libraries/social/a/a/a/c;-><init>(Landroid/app/Activity;Lcom/google/android/libraries/social/i/w;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Lcom/google/android/libraries/social/a/f;)Lcom/google/android/libraries/social/a/a;

    .line 61
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/google/android/libraries/social/a/a/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/social/a/a;->b(Landroid/content/Context;)Lcom/google/android/libraries/social/a/a;

    move-result-object v0

    .line 33
    iget-object v1, p0, Lcom/google/android/libraries/social/a/a/c;->a:Lcom/google/android/libraries/social/a/a;

    invoke-virtual {v1, p0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;)V

    .line 34
    iget-object v1, p0, Lcom/google/android/libraries/social/a/a/c;->a:Lcom/google/android/libraries/social/a/a;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/a/a;->a(Lcom/google/android/libraries/social/a/a;)V

    .line 36
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/a/a/c;->a(Landroid/os/Bundle;)V

    .line 37
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a/c;->a:Lcom/google/android/libraries/social/a/a;

    const-class v1, Lcom/google/android/libraries/social/a/a/b/a;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/a/a;->c(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/libraries/social/a/a/c;->b:Lcom/google/android/libraries/social/i/i;

    iget-object v1, p0, Lcom/google/android/libraries/social/a/a/c;->a:Lcom/google/android/libraries/social/a/a;

    goto :goto_0

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a/c;->a:Lcom/google/android/libraries/social/a/a;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/a/a;->a()V

    .line 40
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a/c;->b:Lcom/google/android/libraries/social/i/i;

    new-instance v1, Lcom/google/android/libraries/social/a/a/d;

    invoke-direct {v1, p0, p1}, Lcom/google/android/libraries/social/a/a/d;-><init>(Lcom/google/android/libraries/social/a/a/c;Landroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/i/i;->a(Lcom/google/android/libraries/social/i/ab;)Lcom/google/android/libraries/social/i/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/a/a/c;->c:Lcom/google/android/libraries/social/i/ab;

    .line 50
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/i/an;->onCreate(Landroid/os/Bundle;)V

    .line 51
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a/c;->b:Lcom/google/android/libraries/social/i/i;

    iget-object v1, p0, Lcom/google/android/libraries/social/a/a/c;->c:Lcom/google/android/libraries/social/i/ab;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/i/i;->b(Lcom/google/android/libraries/social/i/ab;)V

    .line 56
    invoke-super {p0}, Lcom/google/android/libraries/social/i/an;->onDestroy()V

    .line 57
    return-void
.end method
