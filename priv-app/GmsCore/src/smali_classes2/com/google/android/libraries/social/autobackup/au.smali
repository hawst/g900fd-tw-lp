.class public Lcom/google/android/libraries/social/autobackup/au;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 26
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "iu.upload.db"

    const/4 v2, 0x0

    const/16 v3, 0x21

    invoke-direct {p0, v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 28
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/au;->a:Landroid/content/Context;

    .line 29
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 82
    const-string v0, "UploadsDatabaseHelper"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    const-string v0, "UploadsDatabaseHelper"

    const-string v1, "Resetting database and upload preferences"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/au;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 86
    sget-object v1, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/g/a/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 87
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/au;->a:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/autobackup/ac;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/ac;

    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/au;->a:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/autobackup/ac;->a(Landroid/content/Context;)V

    .line 88
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/au;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/r;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/r;->b()V

    .line 89
    return-void
.end method

.method public declared-synchronized getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    .prologue
    .line 55
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 58
    :goto_0
    monitor-exit p0

    return-object v0

    .line 57
    :catch_0
    move-exception v0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/au;->a:Landroid/content/Context;

    const-string v1, "iu.upload.db"

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z

    .line 58
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    .prologue
    .line 45
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 48
    :goto_0
    monitor-exit p0

    return-object v0

    .line 47
    :catch_0
    move-exception v0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/au;->a:Landroid/content/Context;

    const-string v1, "iu.upload.db"

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z

    .line 48
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 33
    const-string v0, "UploadsDatabaseHelper"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    const-string v0, "UploadsDatabaseHelper"

    const-string v1, "Creating database tables at version 33"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    :cond_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/g/a/d;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 37
    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/al;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 38
    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/b/a;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 39
    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/ad;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 40
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 4

    .prologue
    .line 64
    const-string v0, "UploadsDatabaseHelper"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const-string v0, "UploadsDatabaseHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Upgrade database: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " --> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    :cond_0
    :try_start_0
    new-instance v0, Lcom/google/android/libraries/social/autobackup/av;

    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/au;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/google/android/libraries/social/autobackup/av;-><init>(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-virtual {v0, p2, p3}, Lcom/google/android/libraries/social/autobackup/av;->a(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    return-void

    .line 71
    :catch_0
    move-exception v0

    .line 72
    const-string v1, "UploadsDatabaseHelper"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 73
    const-string v1, "UploadsDatabaseHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to upgrade database: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " --> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 75
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
