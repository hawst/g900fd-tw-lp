.class public Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final YEAR_2K:I = 0x7d0


# instance fields
.field private final calendarProvider:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator$CalendarProvider;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator$CalendarProvider;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator;->calendarProvider:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator$CalendarProvider;

    .line 19
    return-void
.end method

.method static from2DigitTo4DigitYear(I)I
    .locals 1

    .prologue
    .line 81
    add-int/lit16 v0, p0, 0x7d0

    return v0
.end method


# virtual methods
.method public validateExpDate(II)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 69
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    move v0, v1

    .line 76
    :cond_1
    :goto_0
    return v0

    .line 72
    :cond_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 73
    invoke-static {p1}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator;->from2DigitTo4DigitYear(I)I

    move-result v3

    invoke-virtual {v2, v0, v3}, Ljava/util/Calendar;->set(II)V

    .line 74
    const/4 v3, 0x2

    invoke-virtual {v2, v3, p2}, Ljava/util/Calendar;->set(II)V

    .line 75
    const/4 v3, 0x5

    invoke-virtual {v2, v3, v0}, Ljava/util/Calendar;->set(II)V

    .line 76
    iget-object v3, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator;->calendarProvider:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator$CalendarProvider;

    invoke-virtual {v3}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator$CalendarProvider;->getFirstDayOfCurrentMonth()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public validatePAN(Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 33
    if-nez p1, :cond_1

    .line 55
    :cond_0
    :goto_0
    return v1

    .line 36
    :cond_1
    const-string v0, "\\s"

    const-string v2, ""

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    .line 38
    const/16 v3, 0xe

    if-lt v2, v3, :cond_0

    const/16 v3, 0x10

    if-gt v2, v3, :cond_0

    .line 43
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->reverse()Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    move v0, v1

    move v2, v1

    move v3, v1

    .line 44
    :goto_1
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v0, v5, :cond_4

    .line 45
    invoke-virtual {v4, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0xa

    invoke-static {v5, v6}, Ljava/lang/Character;->digit(CI)I

    move-result v5

    .line 46
    rem-int/lit8 v6, v0, 0x2

    if-nez v6, :cond_3

    .line 47
    add-int/2addr v3, v5

    .line 44
    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 49
    :cond_3
    mul-int/lit8 v6, v5, 0x2

    add-int/2addr v2, v6

    .line 50
    const/4 v6, 0x5

    if-lt v5, v6, :cond_2

    .line 51
    add-int/lit8 v2, v2, -0x9

    goto :goto_2

    .line 55
    :cond_4
    add-int v0, v3, v2

    rem-int/lit8 v0, v0, 0xa

    if-nez v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method
