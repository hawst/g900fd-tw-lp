.class public final Lcom/google/android/libraries/social/account/refresh/a/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/account/refresh/a/a;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/libraries/social/account/refresh/a/a/a;->a:Landroid/content/Context;

    .line 23
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 28
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/account/refresh/a/a/a;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/auth/r;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 34
    :goto_0
    return-object v0

    .line 29
    :catch_0
    move-exception v0

    .line 30
    :goto_1
    const-string v1, "StableAccountIdProvider"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 31
    const-string v1, "StableAccountIdProvider"

    const-string v2, "Error when acquiring stable account id"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 34
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 29
    :catch_1
    move-exception v0

    goto :goto_1
.end method
