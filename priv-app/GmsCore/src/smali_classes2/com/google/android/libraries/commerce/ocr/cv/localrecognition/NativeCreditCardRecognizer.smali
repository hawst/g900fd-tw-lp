.class public Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final TAG:Ljava/lang/String; = "NativeCreditCardRecognizer"


# instance fields
.field private final assetManager:Landroid/content/res/AssetManager;

.field private final responseHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer;->responseHandler:Landroid/os/Handler;

    .line 134
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer;->assetManager:Landroid/content/res/AssetManager;

    .line 135
    return-void
.end method

.method private invokeSuccess(Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$Callback;Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;)V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer;->responseHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/a;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/a;-><init>(Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer;Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$Callback;Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 188
    return-void
.end method


# virtual methods
.method public native getCreditCardNumbers([BLandroid/content/res/AssetManager;)Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$NativeCreditCardResult;
.end method

.method public native getExpirationDate([BLandroid/content/res/AssetManager;)Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$ExpDateResult;
.end method

.method protected getExpirationMonth(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 191
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method protected getExpirationYear(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 195
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 198
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 196
    :pswitch_1
    const/4 v0, 0x3

    const/4 v1, 0x5

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 197
    :pswitch_2
    const/4 v0, 0x6

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 195
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public recognizeCard([B[BZLcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$Callback;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 142
    const-string v0, "NativeCreditCardRecognizer"

    const-string v1, "making local request for OCR"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer;->assetManager:Landroid/content/res/AssetManager;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer;->getCreditCardNumbers([BLandroid/content/res/AssetManager;)Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$NativeCreditCardResult;

    move-result-object v8

    .line 144
    new-instance v1, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$ExpDateResult;

    const-string v2, ""

    const-wide/16 v4, 0x0

    new-array v6, v3, [I

    new-array v7, v3, [I

    invoke-direct/range {v1 .. v7}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$ExpDateResult;-><init>(Ljava/lang/String;ID[I[I)V

    .line 145
    if-eqz p3, :cond_0

    .line 146
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer;->assetManager:Landroid/content/res/AssetManager;

    invoke-virtual {p0, p2, v0}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer;->getExpirationDate([BLandroid/content/res/AssetManager;)Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$ExpDateResult;

    move-result-object v1

    .line 148
    :cond_0
    invoke-virtual {v8}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$NativeCreditCardResult;->getNumbers()[I

    move-result-object v2

    .line 149
    new-instance v4, Ljava/lang/StringBuilder;

    array-length v0, v2

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    move v0, v3

    .line 150
    :goto_0
    array-length v5, v2

    if-ge v0, v5, :cond_1

    .line 151
    aget v5, v2, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 150
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 153
    :cond_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 154
    const-string v0, "NativeCreditCardRecognizer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "Number received on Java side is "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " digits long."

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    if-eqz p3, :cond_6

    invoke-virtual {v1}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$ExpDateResult;->getPredictionStatus()I

    move-result v0

    if-lez v0, :cond_6

    .line 158
    invoke-virtual {v1}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$ExpDateResult;->getExpirationDate()Ljava/lang/String;

    move-result-object v0

    .line 159
    const-string v2, "NativeCreditCardRecognizer"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Got expiration date: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer;->getExpirationMonth(Ljava/lang/String;)I

    move-result v2

    .line 161
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer;->getExpirationYear(Ljava/lang/String;)I

    move-result v0

    .line 162
    if-lez v2, :cond_2

    if-lez v0, :cond_2

    const/4 v3, 0x1

    .line 164
    :cond_2
    :goto_1
    new-instance v5, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;

    invoke-direct {v5}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;-><init>()V

    .line 165
    iput-object v4, v5, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->cardNumber:Ljava/lang/String;

    .line 166
    invoke-virtual {v8}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$NativeCreditCardResult;->getMinDigitConfidence()D

    move-result-wide v6

    iput-wide v6, v5, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->minDigitConfidence:D

    .line 167
    invoke-virtual {v8}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$NativeCreditCardResult;->getMeanDigitConfidence()D

    move-result-wide v6

    iput-wide v6, v5, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->meanDigitConfidence:D

    .line 169
    const-string v6, "NativeCreditCardRecognizer"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v4, "Has expiration date = "

    invoke-direct {v7, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v3, :cond_4

    const-string v4, "true"

    :goto_2
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    if-eqz v3, :cond_3

    .line 171
    iput v2, v5, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->expirationMonth:I

    .line 172
    iput v0, v5, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->expirationYear:I

    .line 173
    invoke-virtual {v1}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$ExpDateResult;->getExpirationDate()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v2, 0x5

    if-ne v0, v2, :cond_5

    const/4 v0, 0x4

    :goto_3
    iput v0, v5, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->expirationDateNumDigits:I

    .line 175
    invoke-virtual {v1}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$ExpDateResult;->getPredictionStatus()I

    move-result v0

    iput v0, v5, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->expirationDatePredictionStatus:I

    .line 176
    invoke-virtual {v1}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$ExpDateResult;->getConfidence()D

    move-result-wide v0

    iput-wide v0, v5, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->expirationDateConfidence:D

    .line 178
    :cond_3
    invoke-direct {p0, p4, v5}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer;->invokeSuccess(Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$Callback;Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;)V

    .line 179
    return-void

    .line 169
    :cond_4
    const-string v4, "false"

    goto :goto_2

    .line 173
    :cond_5
    const/4 v0, 0x6

    goto :goto_3

    :cond_6
    move v0, v3

    move v2, v3

    goto :goto_1
.end method
