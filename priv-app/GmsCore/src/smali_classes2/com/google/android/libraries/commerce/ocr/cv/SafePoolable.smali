.class public Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private isValid:Z

.field private final poolable:Ljava/lang/Object;

.field private final resourcePool:Lcom/google/android/libraries/commerce/ocr/cv/RecyclablePool;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/cv/RecyclablePool;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->resourcePool:Lcom/google/android/libraries/commerce/ocr/cv/RecyclablePool;

    .line 31
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->poolable:Ljava/lang/Object;

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->isValid:Z

    .line 33
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p1, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->resourcePool:Lcom/google/android/libraries/commerce/ocr/cv/RecyclablePool;

    iget-object v1, p1, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->poolable:Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;-><init>(Lcom/google/android/libraries/commerce/ocr/cv/RecyclablePool;Ljava/lang/Object;)V

    .line 27
    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->isValid:Z

    const-string v1, "value already recycled and is invalid"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 45
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->poolable:Ljava/lang/Object;

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->isValid:Z

    return v0
.end method

.method public recycle()V
    .locals 2

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->isValid:Z

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->resourcePool:Lcom/google/android/libraries/commerce/ocr/cv/RecyclablePool;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->poolable:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/commerce/ocr/cv/RecyclablePool;->recycle(Ljava/lang/Object;)Z

    .line 56
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->isValid:Z

    .line 57
    return-void
.end method
