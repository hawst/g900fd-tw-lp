.class public Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final DESTROYED_MSG:Ljava/lang/String; = "OcrImage Destroyed"


# instance fields
.field private data:[B

.field private format:I

.field private orientation:I

.field private resolution:Landroid/graphics/Point;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>([BIIII)V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, p3, p4}, Landroid/graphics/Point;-><init>(II)V

    invoke-direct {p0, p1, p2, v0, p5}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;-><init>([BILandroid/graphics/Point;I)V

    .line 30
    return-void
.end method

.method public constructor <init>([BILandroid/graphics/Point;I)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-string v0, "must have data for image"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    const-string v0, "must have resolution for image"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    const/16 v0, 0x100

    if-eq p2, v0, :cond_0

    const/16 v0, 0x10

    if-eq p2, v0, :cond_0

    const/16 v0, 0x11

    if-eq p2, v0, :cond_0

    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    if-eqz p2, :cond_0

    const/16 v0, 0x14

    if-eq p2, v0, :cond_0

    const v0, 0x32315659

    if-ne p2, v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Invalid format %s"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v1

    invoke-static {v0, v3, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 45
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->data:[B

    .line 46
    iput p2, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->format:I

    .line 47
    iput-object p3, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->resolution:Landroid/graphics/Point;

    .line 48
    iput p4, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->orientation:I

    .line 49
    return-void

    :cond_1
    move v0, v1

    .line 35
    goto :goto_0
.end method


# virtual methods
.method public copyFrom(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;
    .locals 4

    .prologue
    .line 59
    iget-object v0, p1, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->data:[B

    iget v1, p1, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->format:I

    iget-object v2, p1, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->resolution:Landroid/graphics/Point;

    iget v3, p1, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->orientation:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->set([BILandroid/graphics/Point;I)Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    move-result-object v0

    return-object v0
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/high16 v0, -0x80000000

    .line 113
    iput-object v1, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->data:[B

    .line 114
    iput-object v1, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->resolution:Landroid/graphics/Point;

    .line 115
    iput v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->format:I

    .line 116
    iput v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->orientation:I

    .line 117
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 131
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 141
    :cond_0
    :goto_0
    return v0

    .line 135
    :cond_1
    instance-of v2, p1, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    if-eqz v2, :cond_3

    .line 136
    check-cast p1, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    .line 137
    iget v2, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->format:I

    iget v3, p1, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->format:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->resolution:Landroid/graphics/Point;

    iget-object v3, p1, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->resolution:Landroid/graphics/Point;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->data:[B

    iget-object v3, p1, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->data:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 141
    goto :goto_0
.end method

.method public getData()[B
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->data:[B

    const-string v1, "OcrImage Destroyed"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->data:[B

    return-object v0
.end method

.method public getFormat()I
    .locals 2

    .prologue
    .line 79
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->format:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "OcrImage Destroyed"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 80
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->format:I

    return v0

    .line 79
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHeight()I
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->resolution:Landroid/graphics/Point;

    const-string v1, "OcrImage Destroyed"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->resolution:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    return v0
.end method

.method public getOrientation()I
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->resolution:Landroid/graphics/Point;

    const-string v1, "OcrImage Destroyed"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->orientation:I

    return v0
.end method

.method public getResolution()Landroid/graphics/Point;
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->resolution:Landroid/graphics/Point;

    const-string v1, "OcrImage Destroyed"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->resolution:Landroid/graphics/Point;

    return-object v0
.end method

.method public getWidth()I
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->resolution:Landroid/graphics/Point;

    const-string v1, "OcrImage Destroyed"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->resolution:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    return v0
.end method

.method public init(I)Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->data:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->data:[B

    array-length v0, v0

    if-eq v0, p1, :cond_1

    .line 53
    :cond_0
    new-array v0, p1, [B

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->data:[B

    .line 55
    :cond_1
    return-object p0
.end method

.method public set([BILandroid/graphics/Point;I)Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->data:[B

    const-string v2, "Initialization not complete"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    array-length v0, p1

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->data:[B

    array-length v2, v2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Data length should be the same"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 66
    iput p2, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->format:I

    .line 67
    iput-object p3, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->resolution:Landroid/graphics/Point;

    .line 68
    iput p4, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->orientation:I

    .line 69
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->data:[B

    array-length v2, p1

    invoke-static {p1, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 70
    return-object p0

    :cond_0
    move v0, v1

    .line 64
    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 121
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "data"

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->data:[B

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "format"

    iget v2, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->format:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "resolution"

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->resolution:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "orientiation"

    iget v2, p0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->orientation:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/bv;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
