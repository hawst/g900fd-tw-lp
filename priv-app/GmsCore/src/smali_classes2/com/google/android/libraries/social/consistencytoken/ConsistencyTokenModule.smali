.class public final Lcom/google/android/libraries/social/consistencytoken/ConsistencyTokenModule;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/a/f;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/libraries/social/a/a;)V
    .locals 2

    .prologue
    .line 20
    const-class v0, Lcom/google/android/libraries/social/consistencytoken/a;

    if-ne p2, v0, :cond_1

    .line 23
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-eq v0, p1, :cond_0

    .line 24
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ConsistencyTokenManager should always be instantiated from the application context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 27
    :cond_0
    const-class v0, Lcom/google/android/libraries/social/consistencytoken/a;

    new-instance v1, Lcom/google/android/libraries/social/consistencytoken/a;

    invoke-direct {v1}, Lcom/google/android/libraries/social/consistencytoken/a;-><init>()V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    .line 29
    :cond_1
    return-void
.end method
