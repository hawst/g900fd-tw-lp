.class public Lcom/google/android/libraries/social/autobackup/AutoBackupModule;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/a/f;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/libraries/social/a/a;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 28
    const-class v0, Lcom/google/android/libraries/social/experiments/a;

    if-ne p2, v0, :cond_1

    .line 29
    const-class v0, Lcom/google/android/libraries/social/experiments/a;

    const/4 v1, 0x7

    new-array v1, v1, [Lcom/google/android/libraries/social/experiments/a;

    sget-object v2, Lcom/google/android/libraries/social/autobackup/c;->a:Lcom/google/android/libraries/social/experiments/a;

    aput-object v2, v1, v3

    sget-object v2, Lcom/google/android/libraries/social/autobackup/c;->b:Lcom/google/android/libraries/social/experiments/a;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/libraries/social/autobackup/c;->c:Lcom/google/android/libraries/social/experiments/a;

    aput-object v2, v1, v5

    const/4 v2, 0x3

    sget-object v3, Lcom/google/android/libraries/social/autobackup/c;->d:Lcom/google/android/libraries/social/experiments/a;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget-object v3, Lcom/google/android/libraries/social/autobackup/c;->e:Lcom/google/android/libraries/social/experiments/a;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    sget-object v3, Lcom/google/android/libraries/social/autobackup/c;->i:Lcom/google/android/libraries/social/experiments/a;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/google/android/libraries/social/autobackup/c;->f:Lcom/google/android/libraries/social/experiments/a;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/util/Collection;)V

    .line 52
    :cond_0
    :goto_0
    return-void

    .line 30
    :cond_1
    const-class v0, Lcom/google/android/libraries/social/account/h;

    if-ne p2, v0, :cond_2

    .line 31
    const-class v1, Lcom/google/android/libraries/social/account/h;

    new-array v2, v5, [Lcom/google/android/libraries/social/account/h;

    const-class v0, Lcom/google/android/libraries/social/autobackup/o;

    invoke-virtual {p3, v0}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/h;

    aput-object v0, v2, v3

    new-instance v0, Lcom/google/android/libraries/social/autobackup/bc;

    invoke-direct {v0}, Lcom/google/android/libraries/social/autobackup/bc;-><init>()V

    aput-object v0, v2, v4

    invoke-virtual {p3, v1, v2}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_0

    .line 34
    :cond_2
    const-class v0, Lcom/google/android/libraries/social/autobackup/o;

    if-ne p2, v0, :cond_3

    .line 35
    const-class v0, Lcom/google/android/libraries/social/autobackup/o;

    new-instance v1, Lcom/google/android/libraries/social/autobackup/o;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/social/autobackup/o;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto :goto_0

    .line 37
    :cond_3
    const-class v0, Lcom/google/android/libraries/social/autobackup/util/a;

    if-ne p2, v0, :cond_4

    .line 38
    const-class v0, Lcom/google/android/libraries/social/autobackup/util/a;

    new-instance v1, Lcom/google/android/libraries/social/autobackup/util/a;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/libraries/social/autobackup/util/a;-><init>(Landroid/content/ContentResolver;)V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto :goto_0

    .line 40
    :cond_4
    const-class v0, Lcom/google/android/libraries/social/autobackup/t;

    if-ne p2, v0, :cond_5

    .line 41
    const-class v0, Lcom/google/android/libraries/social/autobackup/t;

    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/t;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/t;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto :goto_0

    .line 42
    :cond_5
    const-class v0, Lcom/google/android/libraries/social/autobackup/au;

    if-ne p2, v0, :cond_6

    .line 43
    const-class v0, Lcom/google/android/libraries/social/autobackup/au;

    new-instance v1, Lcom/google/android/libraries/social/autobackup/au;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/social/autobackup/au;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto :goto_0

    .line 44
    :cond_6
    const-class v0, Lcom/google/android/libraries/social/autobackup/al;

    if-ne p2, v0, :cond_7

    .line 45
    const-class v0, Lcom/google/android/libraries/social/autobackup/al;

    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/al;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/al;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto :goto_0

    .line 46
    :cond_7
    const-class v0, Lcom/google/android/libraries/social/autobackup/an;

    if-ne p2, v0, :cond_8

    .line 47
    const-class v0, Lcom/google/android/libraries/social/autobackup/an;

    new-instance v1, Lcom/google/android/libraries/social/autobackup/an;

    invoke-direct {v1}, Lcom/google/android/libraries/social/autobackup/an;-><init>()V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto :goto_0

    .line 48
    :cond_8
    const-class v0, Lcom/google/android/libraries/social/autobackup/ac;

    if-ne p2, v0, :cond_9

    .line 49
    const-class v0, Lcom/google/android/libraries/social/autobackup/ac;

    new-instance v1, Lcom/google/android/libraries/social/autobackup/ad;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/social/autobackup/ad;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto/16 :goto_0

    .line 50
    :cond_9
    const-class v0, Lcom/google/android/libraries/social/d/a/a;

    if-ne p2, v0, :cond_0

    .line 51
    sget-object v0, Lcom/google/android/libraries/social/d/a/a;->a:Lcom/google/android/libraries/social/f/a;

    goto/16 :goto_0
.end method
