.class final Lcom/google/android/libraries/commerce/ocr/capture/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# instance fields
.field final synthetic a:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$ImageCallback;

.field final synthetic b:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$ImageCallback;)V
    .locals 0

    .prologue
    .line 406
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/capture/b;->b:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;

    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/capture/b;->a:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$ImageCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 5

    .prologue
    .line 409
    invoke-virtual {p2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    .line 410
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v2

    iget v2, v2, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v3

    iget v3, v3, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 412
    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/capture/b;->a:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$ImageCallback;

    new-instance v3, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPictureFormat()I

    move-result v0

    iget-object v4, p0, Lcom/google/android/libraries/commerce/ocr/capture/b;->b:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;

    invoke-virtual {v4}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->getOrientation()I

    move-result v4

    invoke-direct {v3, p1, v0, v1, v4}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;-><init>([BILandroid/graphics/Point;I)V

    invoke-interface {v2, v3}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$ImageCallback;->onImage(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)V

    .line 414
    return-void
.end method
