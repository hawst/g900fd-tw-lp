.class final Lcom/google/android/libraries/social/e/q;
.super Ljava/io/FilterOutputStream;
.source "SourceFile"


# instance fields
.field private final a:Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 29
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/e/q;->a:Ljava/nio/ByteBuffer;

    .line 33
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/libraries/social/e/q;
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/libraries/social/e/q;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 49
    iget-object v0, p0, Lcom/google/android/libraries/social/e/q;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 50
    iget-object v0, p0, Lcom/google/android/libraries/social/e/q;->out:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/google/android/libraries/social/e/q;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 51
    return-object p0
.end method

.method public final a(Ljava/nio/ByteOrder;)Lcom/google/android/libraries/social/e/q;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/libraries/social/e/q;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 37
    return-object p0
.end method

.method public final a(S)Lcom/google/android/libraries/social/e/q;
    .locals 4

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/libraries/social/e/q;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/social/e/q;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 43
    iget-object v0, p0, Lcom/google/android/libraries/social/e/q;->out:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/google/android/libraries/social/e/q;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 44
    return-object p0
.end method
