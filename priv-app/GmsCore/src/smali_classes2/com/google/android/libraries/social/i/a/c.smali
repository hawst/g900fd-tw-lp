.class public Lcom/google/android/libraries/social/i/a/c;
.super Landroid/support/v4/app/q;
.source "SourceFile"


# instance fields
.field private a:I

.field protected final b:Lcom/google/android/libraries/social/i/i;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    .line 20
    new-instance v0, Lcom/google/android/libraries/social/i/i;

    invoke-direct {v0}, Lcom/google/android/libraries/social/i/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 158
    iget v0, p0, Lcom/google/android/libraries/social/i/a/c;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/social/i/a/c;->a:I

    .line 159
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 152
    iget v0, p0, Lcom/google/android/libraries/social/i/a/c;->a:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/libraries/social/i/a/c;->a:I

    if-nez v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->o()V

    .line 155
    :cond_0
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    const/4 v0, 0x1

    .line 198
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public finish()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->g()V

    .line 176
    invoke-super {p0}, Landroid/support/v4/app/q;->finish()V

    .line 177
    return-void
.end method

.method public onActionModeFinished(Landroid/view/ActionMode;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->i()V

    .line 190
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onActionModeFinished(Landroid/view/ActionMode;)V

    .line 191
    return-void
.end method

.method public onActionModeStarted(Landroid/view/ActionMode;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->h()V

    .line 183
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onActionModeStarted(Landroid/view/ActionMode;)V

    .line 184
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->s()V

    .line 98
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/q;->onActivityResult(IILandroid/content/Intent;)V

    .line 99
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->n()Z

    move-result v0

    if-nez v0, :cond_0

    .line 228
    invoke-super {p0}, Landroid/support/v4/app/q;->onBackPressed()V

    .line 230
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/i/i;->c(Landroid/os/Bundle;)V

    .line 30
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 31
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    const/4 v0, 0x1

    .line 206
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->c()V

    .line 86
    invoke-super {p0}, Landroid/support/v4/app/q;->onDestroy()V

    .line 87
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->e()V

    .line 164
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onNewIntent(Landroid/content/Intent;)V

    .line 165
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    const/4 v0, 0x1

    .line 222
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->b()V

    .line 68
    invoke-super {p0}, Landroid/support/v4/app/q;->onPause()V

    .line 69
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/i/i;->a(Landroid/os/Bundle;)V

    .line 36
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onPostCreate(Landroid/os/Bundle;)V

    .line 37
    return-void
.end method

.method protected onPostResume()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->a()V

    .line 62
    invoke-super {p0}, Landroid/support/v4/app/q;->onPostResume()V

    .line 63
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    const/4 v0, 0x1

    .line 214
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/i/i;->b(Landroid/os/Bundle;)V

    .line 49
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 50
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/google/android/libraries/social/i/a/c;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/social/i/a/d;->a(Landroid/support/v4/app/v;)V

    .line 55
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->q()V

    .line 56
    invoke-super {p0}, Landroid/support/v4/app/q;->onResume()V

    .line 57
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/i/i;->d(Landroid/os/Bundle;)V

    .line 80
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 81
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/google/android/libraries/social/i/a/c;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/social/i/a/d;->a(Landroid/support/v4/app/v;)V

    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->p()V

    .line 43
    invoke-super {p0}, Landroid/support/v4/app/q;->onStart()V

    .line 44
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->r()V

    .line 74
    invoke-super {p0}, Landroid/support/v4/app/q;->onStop()V

    .line 75
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->f()V

    .line 170
    invoke-super {p0}, Landroid/support/v4/app/q;->onUserLeaveHint()V

    .line 171
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/c;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->d()V

    .line 92
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onWindowFocusChanged(Z)V

    .line 93
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/i/a/c;->a(Landroid/content/Intent;)V

    .line 104
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->startActivity(Landroid/content/Intent;)V

    .line 105
    invoke-direct {p0}, Lcom/google/android/libraries/social/i/a/c;->a()V

    .line 106
    return-void
.end method

.method public startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/i/a/c;->a(Landroid/content/Intent;)V

    .line 112
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/q;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 113
    invoke-direct {p0}, Lcom/google/android/libraries/social/i/a/c;->a()V

    .line 114
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/i/a/c;->a(Landroid/content/Intent;)V

    .line 135
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/q;->startActivityForResult(Landroid/content/Intent;I)V

    .line 136
    invoke-direct {p0}, Lcom/google/android/libraries/social/i/a/c;->a()V

    .line 137
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/i/a/c;->a(Landroid/content/Intent;)V

    .line 143
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/q;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 144
    invoke-direct {p0}, Lcom/google/android/libraries/social/i/a/c;->a()V

    .line 145
    return-void
.end method

.method public startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 127
    invoke-direct {p0, p2}, Lcom/google/android/libraries/social/i/a/c;->a(Landroid/content/Intent;)V

    .line 128
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v4/app/q;->startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 129
    invoke-direct {p0}, Lcom/google/android/libraries/social/i/a/c;->a()V

    .line 130
    return-void
.end method

.method public startActivityFromFragment(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 118
    invoke-direct {p0, p2}, Lcom/google/android/libraries/social/i/a/c;->a(Landroid/content/Intent;)V

    .line 119
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/q;->startActivityFromFragment(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    .line 120
    invoke-direct {p0}, Lcom/google/android/libraries/social/i/a/c;->a()V

    .line 121
    return-void
.end method
