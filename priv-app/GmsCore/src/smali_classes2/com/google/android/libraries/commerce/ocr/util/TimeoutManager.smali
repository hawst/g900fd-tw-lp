.class public Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final TAG:Ljava/lang/String; = "TimeoutManager"


# instance fields
.field private final defaultTimeoutInMs:J

.field private final timer:Ljava/util/Timer;


# direct methods
.method public constructor <init>(J)V
    .locals 3

    .prologue
    .line 21
    new-instance v0, Ljava/util/Timer;

    const-class v1, Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Ljava/util/Timer;-><init>(Ljava/lang/String;Z)V

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;-><init>(Ljava/util/Timer;J)V

    .line 22
    return-void
.end method

.method protected constructor <init>(Ljava/util/Timer;J)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;->timer:Ljava/util/Timer;

    .line 27
    iput-wide p2, p0, Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;->defaultTimeoutInMs:J

    .line 28
    return-void
.end method


# virtual methods
.method public startTimeout(Ljava/util/TimerTask;)Z
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;->defaultTimeoutInMs:J

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;->startTimeout(Ljava/util/TimerTask;J)Z

    move-result v0

    return v0
.end method

.method public startTimeout(Ljava/util/TimerTask;J)Z
    .locals 2

    .prologue
    .line 52
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;->timer:Ljava/util/Timer;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 54
    :catch_0
    move-exception v0

    const-string v0, "TimeoutManager"

    const-string v1, "timer already cancelled or task already scheduled."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stopTimeout()V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 70
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 71
    return-void
.end method

.method public stopTimeout(Ljava/util/TimerTask;)V
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p1}, Ljava/util/TimerTask;->cancel()Z

    .line 62
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 63
    return-void
.end method
