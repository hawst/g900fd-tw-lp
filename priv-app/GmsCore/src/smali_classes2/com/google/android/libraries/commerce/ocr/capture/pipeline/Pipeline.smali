.class public Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    return-void
.end method

.method public static forAsync(Ljava/util/concurrent/ExecutorService;Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    invoke-direct {v0, p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;-><init>(Ljava/util/concurrent/ExecutorService;Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;)V

    return-object v0
.end method

.method public static forSharedReferenceSync(Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/c;
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/c;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/c;-><init>(Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;)V

    return-object v0
.end method

.method public static forSync(Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;-><init>(Ljava/util/concurrent/ExecutorService;Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;)V

    return-object v0
.end method

.method public static wire(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;-><init>(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;Lcom/google/android/libraries/commerce/ocr/capture/pipeline/a;)V

    return-object v0
.end method
