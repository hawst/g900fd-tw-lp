.class public final Lcom/google/android/libraries/social/rpc/b/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 23
    const/16 v0, 0xe

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "getmobileexperiments"

    aput-object v0, v2, v1

    const-string v0, "registerdevice"

    aput-object v0, v2, v4

    const-string v0, "fetchnotifications"

    aput-object v0, v2, v5

    const/4 v0, 0x3

    const-string v3, "fetchnotificationscount"

    aput-object v3, v2, v0

    const/4 v0, 0x4

    const-string v3, "getphotossettings"

    aput-object v3, v2, v0

    const/4 v0, 0x5

    const-string v3, "getsimpleprofile"

    aput-object v3, v2, v0

    const/4 v0, 0x6

    const-string v3, "getmobilesettings"

    aput-object v3, v2, v0

    const/4 v0, 0x7

    const-string v3, "loadblockedpeople"

    aput-object v3, v2, v0

    const/16 v0, 0x8

    const-string v3, "loadpeople"

    aput-object v3, v2, v0

    const/16 v0, 0x9

    const-string v3, "loadsocialnetwork"

    aput-object v3, v2, v0

    const/16 v0, 0xa

    const-string v3, "getactivities"

    aput-object v3, v2, v0

    const/16 v0, 0xb

    const-string v3, "getvolumecontrols"

    aput-object v3, v2, v0

    const/16 v0, 0xc

    const-string v3, "readcollectionsbyid"

    aput-object v3, v2, v0

    const/16 v0, 0xd

    const-string v3, "syncuserhighlights"

    aput-object v3, v2, v0

    .line 41
    new-array v3, v5, [Ljava/lang/String;

    const-string v0, "getappupgradestatus"

    aput-object v0, v3, v1

    const-string v0, "postclientlogs"

    aput-object v0, v3, v4

    .line 46
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    move v0, v1

    .line 47
    :goto_0
    array-length v5, v2

    if-ge v0, v5, :cond_0

    .line 48
    aget-object v5, v2, v0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v7, v2, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "background"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 50
    :cond_0
    :goto_1
    array-length v0, v3

    if-ge v1, v0, :cond_1

    .line 51
    aget-object v0, v3, v1

    aget-object v2, v3, v1

    invoke-interface {v4, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 53
    :cond_1
    invoke-static {v4}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/rpc/b/b;->a:Ljava/util/Map;

    .line 54
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 57
    sget-object v0, Lcom/google/android/libraries/social/rpc/b/b;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 59
    if-nez v0, :cond_1

    .line 60
    const-string v0, "BackgroundEndpoints"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    const-string v0, "BackgroundEndpoints"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Calling operation ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] from sync loop with no sync endpoint"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    :cond_0
    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method
