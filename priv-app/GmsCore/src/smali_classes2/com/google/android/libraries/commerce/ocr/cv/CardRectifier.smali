.class public Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private native rectify(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;Landroid/graphics/Rect;FFIZ)Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;
.end method


# virtual methods
.method public rectify(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;Landroid/graphics/Rect;FFZ)Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 61
    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getOrientation()I

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier;->rectify(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;Landroid/graphics/Rect;FFIZ)Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;

    move-result-object v4

    .line 66
    invoke-virtual {v4}, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;->getBoundaries()Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;

    move-result-object v5

    .line 67
    invoke-virtual {v4}, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;->getCardImage()Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    move-result-object v0

    if-nez v0, :cond_0

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->allEdgesDetected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    .line 69
    new-instance v8, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;

    if-nez v6, :cond_1

    move-object v3, v7

    :goto_0
    const/4 v0, 0x1

    if-ne v6, v0, :cond_2

    move-object v2, v7

    :goto_1
    const/4 v0, 0x2

    if-ne v6, v0, :cond_3

    move-object v1, v7

    :goto_2
    const/4 v0, 0x3

    if-ne v6, v0, :cond_4

    move-object v0, v7

    :goto_3
    invoke-direct {v8, v3, v2, v1, v0}, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;-><init>(Lcom/google/android/libraries/commerce/ocr/cv/Edge;Lcom/google/android/libraries/commerce/ocr/cv/Edge;Lcom/google/android/libraries/commerce/ocr/cv/Edge;Lcom/google/android/libraries/commerce/ocr/cv/Edge;)V

    invoke-virtual {v4, v8}, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;->setBoundaries(Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;)V

    .line 74
    :cond_0
    return-object v4

    .line 69
    :cond_1
    invoke-virtual {v5}, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->getTop()Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    move-result-object v0

    move-object v3, v0

    goto :goto_0

    :cond_2
    invoke-virtual {v5}, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->getLeft()Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    move-result-object v0

    move-object v2, v0

    goto :goto_1

    :cond_3
    invoke-virtual {v5}, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->getRight()Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    :cond_4
    invoke-virtual {v5}, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->getBottom()Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    move-result-object v0

    goto :goto_3
.end method
