.class public final Lcom/google/android/libraries/social/h/b/b;
.super Landroid/os/HandlerThread;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lcom/google/android/libraries/social/h/b/a;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/ArrayList;

.field private final c:Ljava/lang/Object;

.field private d:Lcom/google/android/gms/common/api/v;

.field private e:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 96
    const-string v0, "PanoramaClient"

    const/16 v1, 0xa

    invoke-direct {p0, v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/h/b/b;->b:Ljava/util/ArrayList;

    .line 45
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/h/b/b;->c:Ljava/lang/Object;

    .line 97
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/h/b/b;->a:Landroid/content/Context;

    .line 98
    return-void
.end method

.method static synthetic a(Lcom/google/android/libraries/social/h/b/b;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/libraries/social/h/b/b;->e:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    packed-switch p0, :pswitch_data_0

    const-string v0, "none"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "360 horizontal"

    goto :goto_0

    :pswitch_1
    const-string v0, "partial"

    goto :goto_0

    :pswitch_2
    const-string v0, "full"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/libraries/social/h/b/b;->d:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/h/b/b;->d:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Lcom/google/android/libraries/social/h/b/d;)V
    .locals 3

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/libraries/social/h/b/b;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/ew;->a(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    invoke-interface {p2}, Lcom/google/android/libraries/social/h/b/d;->a()V

    .line 119
    :goto_0
    return-void

    .line 108
    :cond_0
    new-instance v0, Lcom/google/android/libraries/social/h/b/c;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/android/libraries/social/h/b/c;-><init>(Lcom/google/android/libraries/social/h/b/b;Lcom/google/android/libraries/social/h/b/d;Landroid/net/Uri;)V

    .line 110
    iget-object v1, p0, Lcom/google/android/libraries/social/h/b/b;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 111
    :try_start_0
    iget-object v2, p0, Lcom/google/android/libraries/social/h/b/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    iget-object v0, p0, Lcom/google/android/libraries/social/h/b/b;->e:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 114
    invoke-virtual {p0}, Lcom/google/android/libraries/social/h/b/b;->start()V

    .line 115
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/h/b/b;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/h/b/b;->e:Landroid/os/Handler;

    .line 118
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/h/b/b;->e:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 119
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 124
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 164
    :goto_0
    return v0

    .line 127
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/libraries/social/h/b/b;->c:Ljava/lang/Object;

    monitor-enter v2

    .line 129
    :try_start_0
    iget-object v3, p0, Lcom/google/android/libraries/social/h/b/b;->e:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 130
    new-instance v4, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/libraries/social/h/b/b;->b:Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 131
    iget-object v3, p0, Lcom/google/android/libraries/social/h/b/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 132
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v0

    .line 134
    :goto_1
    if-ge v3, v5, :cond_5

    .line 135
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/h/b/c;

    .line 136
    invoke-direct {p0}, Lcom/google/android/libraries/social/h/b/b;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v1

    :goto_2
    if-eqz v2, :cond_4

    .line 137
    const-string v2, "PanoramaClient"

    invoke-static {v2, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 138
    const-string v2, "PanoramaClient"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Detecting if the image is a panorama: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v0, Lcom/google/android/libraries/social/h/b/c;->b:Landroid/net/Uri;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/social/h/b/b;->e:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 142
    sget-object v2, Lcom/google/android/gms/panorama/c;->d:Lcom/google/android/gms/panorama/e;

    iget-object v6, p0, Lcom/google/android/libraries/social/h/b/b;->d:Lcom/google/android/gms/common/api/v;

    iget-object v7, v0, Lcom/google/android/libraries/social/h/b/c;->b:Landroid/net/Uri;

    invoke-interface {v2, v6, v7}, Lcom/google/android/gms/panorama/e;->a(Lcom/google/android/gms/common/api/v;Landroid/net/Uri;)Lcom/google/android/gms/common/api/am;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 134
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 136
    :cond_1
    iget-object v2, p0, Lcom/google/android/libraries/social/h/b/b;->d:Lcom/google/android/gms/common/api/v;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/libraries/social/h/b/b;->d:Lcom/google/android/gms/common/api/v;

    invoke-interface {v2}, Lcom/google/android/gms/common/api/v;->d()V

    :cond_2
    new-instance v2, Lcom/google/android/gms/common/api/w;

    iget-object v6, p0, Lcom/google/android/libraries/social/h/b/b;->a:Landroid/content/Context;

    invoke-direct {v2, v6}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v6, Lcom/google/android/gms/panorama/c;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v2, v6}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/libraries/social/h/b/b;->d:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/libraries/social/h/b/b;->d:Lcom/google/android/gms/common/api/v;

    const-wide/16 v6, 0x1e

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v6, v7, v8}, Lcom/google/android/gms/common/api/v;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/c;

    move-result-object v2

    const-string v6, "PanoramaClient"

    invoke-static {v6, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "PanoramaClient"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "ConnectionResult: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-virtual {v2}, Lcom/google/android/gms/common/c;->b()Z

    move-result v2

    goto :goto_2

    .line 146
    :cond_4
    iget-object v0, v0, Lcom/google/android/libraries/social/h/b/c;->a:Lcom/google/android/libraries/social/h/b/d;

    invoke-interface {v0}, Lcom/google/android/libraries/social/h/b/d;->a()V

    goto :goto_3

    :cond_5
    move v0, v1

    .line 149
    goto/16 :goto_0

    .line 153
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/libraries/social/h/b/b;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 154
    const-string v0, "PanoramaClient"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 155
    const-string v0, "PanoramaClient"

    const-string v2, "Disconnecting from GooglePlayServices"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/h/b/b;->d:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 158
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/h/b/b;->d:Lcom/google/android/gms/common/api/v;

    :cond_7
    move v0, v1

    .line 160
    goto/16 :goto_0

    .line 124
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
