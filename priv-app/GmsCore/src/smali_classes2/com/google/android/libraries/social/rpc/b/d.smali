.class public abstract Lcom/google/android/libraries/social/rpc/b/d;
.super Lcom/google/android/libraries/social/rpc/a/f;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/social/rpc/q;Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)V
    .locals 8

    .prologue
    .line 54
    const-string v6, "plusi"

    const-string v7, "oauth2:https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.stream.read https://www.googleapis.com/auth/plus.stream.write https://www.googleapis.com/auth/plus.circles.write https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/plus.photos.readwrite https://www.googleapis.com/auth/plus.native"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/libraries/social/rpc/a/f;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/rpc/q;Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    return-void
.end method


# virtual methods
.method public final a([BLjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 89
    .line 91
    :try_start_0
    new-instance v2, Lcom/google/android/libraries/social/rpc/b/a;

    invoke-direct {v2, p1}, Lcom/google/android/libraries/social/rpc/b/a;-><init>([B)V

    .line 92
    invoke-virtual {v2}, Lcom/google/android/libraries/social/rpc/b/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 93
    new-instance v0, Lcom/google/android/libraries/social/rpc/b/c;

    iget-object v3, p0, Lcom/google/android/libraries/social/rpc/b/d;->c:Lcom/google/android/libraries/social/rpc/q;

    iget-object v3, v3, Lcom/google/android/libraries/social/rpc/q;->a:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lcom/google/android/libraries/social/rpc/b/c;-><init>(Lcom/google/android/libraries/social/rpc/b/a;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :try_start_1
    const-string v3, "APP_UPGRADE_REQUIRED"

    invoke-static {v0, v3}, Lcom/google/android/libraries/social/rpc/b/c;->a(Ljava/lang/Exception;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 97
    iget-object v3, p0, Lcom/google/android/libraries/social/rpc/b/d;->b:Landroid/content/Context;

    const-class v4, Lcom/google/android/libraries/social/rpc/b;

    invoke-static {v3, v4}, Lcom/google/android/libraries/social/a/a;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    .line 99
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Apiary error response: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/libraries/social/rpc/a/e;->g:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "   domain: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/libraries/social/rpc/b/a;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "   reason: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/libraries/social/rpc/b/a;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "   message: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/libraries/social/rpc/b/a;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/rpc/b/a;->d()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v4, "\\n"

    const-string v5, "\n"

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "\\t"

    const-string v5, "\t"

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "   debugInfo: \n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v2, 0x6

    const-string v4, "PlusiProtoOperation"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v4, v3}, Lcom/google/android/libraries/b/a/d;->a(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 110
    :goto_0
    if-eqz v0, :cond_2

    .line 111
    invoke-virtual {v0}, Lcom/google/android/libraries/social/rpc/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/rpc/b/d;->c(Ljava/lang/String;)V

    .line 112
    throw v0

    .line 114
    :cond_2
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/rpc/b/d;->c(Ljava/lang/String;)V

    .line 115
    return-void

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v2

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method protected final a(Ljava/lang/Exception;)Z
    .locals 1

    .prologue
    .line 135
    const-string v0, "INVALID_CREDENTIALS"

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/rpc/b/c;->a(Ljava/lang/Exception;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    const/4 v0, 0x1

    .line 138
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/rpc/a/f;->a(Ljava/lang/Exception;)Z

    move-result v0

    goto :goto_0
.end method

.method protected final b([B)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 82
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/rpc/a/f;->b([B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    .line 83
    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/b/d;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/libraries/social/rpc/b/e;->a(Landroid/content/Context;Lcom/google/protobuf/nano/j;)V

    .line 84
    return-object v0
.end method

.method protected final c(Lcom/google/protobuf/nano/j;)V
    .locals 4

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/b/d;->c:Lcom/google/android/libraries/social/rpc/q;

    iget-object v0, v0, Lcom/google/android/libraries/social/rpc/q;->c:Lcom/google/android/libraries/social/rpc/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 67
    :goto_0
    iget-object v2, p0, Lcom/google/android/libraries/social/rpc/b/d;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/b/d;->c:Lcom/google/android/libraries/social/rpc/q;

    iget-object v3, v1, Lcom/google/android/libraries/social/rpc/q;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/b/d;->c:Lcom/google/android/libraries/social/rpc/q;

    iget-boolean v1, v1, Lcom/google/android/libraries/social/rpc/q;->d:Z

    if-eqz v1, :cond_1

    const/16 v1, 0x32

    :goto_1
    invoke-static {v2, p1, v3, v0, v1}, Lcom/google/android/libraries/social/rpc/b/e;->a(Landroid/content/Context;Lcom/google/protobuf/nano/j;Ljava/lang/String;ZI)V

    .line 69
    return-void

    .line 66
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 67
    :cond_1
    const/16 v1, 0x64

    goto :goto_1
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 58
    invoke-super {p0}, Lcom/google/android/libraries/social/rpc/a/f;->h()Ljava/lang/String;

    move-result-object v0

    .line 59
    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/b/d;->c:Lcom/google/android/libraries/social/rpc/q;

    iget-boolean v1, v1, Lcom/google/android/libraries/social/rpc/q;->d:Z

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/google/android/libraries/social/rpc/b/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method
