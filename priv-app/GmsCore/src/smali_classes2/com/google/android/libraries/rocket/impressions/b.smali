.class public final Lcom/google/android/libraries/rocket/impressions/b;
.super Lcom/google/android/libraries/rocket/impressions/a;
.source "SourceFile"


# static fields
.field private static final c:J


# instance fields
.field private final a:Lcom/google/android/gms/common/api/v;

.field private final b:Lcom/google/android/gms/clearcut/a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 77
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/libraries/rocket/impressions/b;->c:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/libraries/rocket/impressions/a;-><init>()V

    .line 40
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/clearcut/a;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    .line 42
    if-eqz p2, :cond_0

    .line 43
    iput-object p2, v0, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    .line 45
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/rocket/impressions/b;->a:Lcom/google/android/gms/common/api/v;

    .line 46
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/b;->a:Lcom/google/android/gms/common/api/v;

    new-instance v1, Lcom/google/android/libraries/rocket/impressions/c;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/rocket/impressions/c;-><init>(Lcom/google/android/libraries/rocket/impressions/b;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/x;)V

    .line 58
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/b;->a:Lcom/google/android/gms/common/api/v;

    new-instance v1, Lcom/google/android/libraries/rocket/impressions/d;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/rocket/impressions/d;-><init>(Lcom/google/android/libraries/rocket/impressions/b;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/y;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/b;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 71
    const-string v0, ""

    .line 72
    new-instance v1, Lcom/google/android/gms/clearcut/a;

    const/16 v2, 0x2c

    invoke-direct {v1, p1, v2, p2, v0}, Lcom/google/android/gms/clearcut/a;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/libraries/rocket/impressions/b;->b:Lcom/google/android/gms/clearcut/a;

    .line 73
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 5

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/b;->b:Lcom/google/android/gms/clearcut/a;

    iget-object v1, p0, Lcom/google/android/libraries/rocket/impressions/b;->a:Lcom/google/android/gms/common/api/v;

    sget-wide v2, Lcom/google/android/libraries/rocket/impressions/b;->c:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/gms/clearcut/a;->a(Lcom/google/android/gms/common/api/v;JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    .line 82
    const-string v1, "ClearcutLogger flush result: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    const-string v0, "successful"

    :goto_0
    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/libraries/rocket/impressions/j;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/b;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 84
    return-void

    .line 82
    :cond_0
    const-string v0, "failed"

    goto :goto_0
.end method

.method protected final a(Lcom/google/c/b/b/a/a/f;)V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/b;->b:Lcom/google/android/gms/clearcut/a;

    invoke-static {p1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/clearcut/a;->a([B)Lcom/google/android/gms/clearcut/c;

    move-result-object v0

    .line 93
    iget-object v1, p0, Lcom/google/android/libraries/rocket/impressions/b;->a:Lcom/google/android/gms/common/api/v;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/clearcut/c;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/libraries/rocket/impressions/e;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/rocket/impressions/e;-><init>(Lcom/google/android/libraries/rocket/impressions/b;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 99
    return-void
.end method
