.class public final Lcom/google/android/libraries/social/autobackup/c/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:J

.field private static final b:J

.field private static final c:Lcom/google/android/libraries/social/f/b;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 44
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/libraries/social/autobackup/c/b;->a:J

    .line 45
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/libraries/social/autobackup/c/b;->b:J

    .line 56
    new-instance v0, Lcom/google/android/libraries/social/f/b;

    const-string v1, "debug.plus.ab_setup_notif"

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/f/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/c/b;->c:Lcom/google/android/libraries/social/f/b;

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 129
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 131
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":notifications:setup_backup"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/gms/j;->mG:I

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 133
    return-void
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 71
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 73
    invoke-static {p0, p1}, Lcom/google/android/libraries/social/autobackup/c/b;->c(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/gms/j;->mF:I

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 75
    return-void
.end method

.method public static a(Landroid/content/Context;IIIZ)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 90
    sub-int v0, p3, p2

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 91
    invoke-static {p0, v0}, Lcom/google/android/libraries/social/autobackup/c/b;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 92
    if-eqz p4, :cond_0

    sget v0, Lcom/google/android/gms/p;->hv:I

    move v1, v0

    .line 94
    :goto_0
    sget v3, Lcom/google/android/gms/h;->dj:I

    .line 96
    const-class v0, Lcom/google/android/libraries/social/autobackup/c/a;

    invoke-static {p0, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/c/a;

    invoke-interface {v0}, Lcom/google/android/libraries/social/autobackup/c/a;->a()Landroid/content/Intent;

    move-result-object v0

    .line 98
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    const/high16 v4, 0x14000000

    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 100
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    long-to-int v4, v4

    invoke-static {p0, v4, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 102
    new-instance v4, Landroid/support/v4/app/bk;

    invoke-direct {v4, p0}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    .line 103
    invoke-virtual {v4, v3}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    .line 104
    invoke-virtual {v4}, Landroid/support/v4/app/bk;->a()Landroid/support/v4/app/bk;

    .line 105
    sget v3, Lcom/google/android/gms/p;->pB:I

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    .line 106
    new-array v3, v7, [Ljava/lang/Object;

    aput-object v2, v3, v6

    invoke-virtual {p0, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/support/v4/app/bk;->e(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    .line 107
    new-array v3, v7, [Ljava/lang/Object;

    aput-object v2, v3, v6

    invoke-virtual {p0, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    .line 108
    iput-object v0, v4, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    .line 110
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 113
    invoke-static {p0, p1}, Lcom/google/android/libraries/social/autobackup/c/b;->c(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/gms/j;->mF:I

    invoke-static {}, Landroid/support/v4/app/bf;->a()Landroid/support/v4/app/bm;

    move-result-object v3

    invoke-interface {v3, v4}, Landroid/support/v4/app/bm;->a(Landroid/support/v4/app/bk;)Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 115
    return-void

    .line 92
    :cond_0
    sget v0, Lcom/google/android/gms/p;->ht:I

    move v1, v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;JZ)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 225
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 226
    invoke-static {p0, v6, p1, p2}, Lcom/google/android/libraries/social/autobackup/AutoBackupStalledReceiver;->a(Landroid/content/Context;ZJ)Landroid/app/PendingIntent;

    move-result-object v0

    .line 230
    if-nez v0, :cond_0

    .line 231
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 233
    const/4 v1, 0x1

    invoke-static {p0, v1, p1, p2}, Lcom/google/android/libraries/social/autobackup/AutoBackupStalledReceiver;->a(Landroid/content/Context;ZJ)Landroid/app/PendingIntent;

    move-result-object v1

    .line 237
    if-eqz p3, :cond_1

    sget-wide v2, Lcom/google/android/libraries/social/autobackup/c/b;->b:J

    :goto_0
    add-long/2addr v2, v4

    invoke-virtual {v0, v6, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 243
    :cond_0
    return-void

    .line 237
    :cond_1
    sget-wide v2, Lcom/google/android/libraries/social/autobackup/c/b;->a:J

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 300
    const-class v0, Lcom/google/android/libraries/social/autobackup/c/a;

    invoke-static {p0, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/c/a;

    invoke-interface {v0, p0}, Lcom/google/android/libraries/social/autobackup/c/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 302
    invoke-static {p0}, Landroid/support/v4/app/cv;->a(Landroid/content/Context;)Landroid/support/v4/app/cv;

    move-result-object v1

    .line 303
    invoke-virtual {v1, v0}, Landroid/support/v4/app/cv;->b(Landroid/content/Intent;)Landroid/support/v4/app/cv;

    .line 305
    new-instance v2, Landroid/support/v4/app/bk;

    invoke-direct {v2, p0}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    .line 306
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    long-to-int v0, v4

    invoke-virtual {v1, v0}, Landroid/support/v4/app/cv;->a(I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 308
    sget v1, Lcom/google/android/gms/p;->rq:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 310
    sget v3, Lcom/google/android/gms/p;->rr:I

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 312
    invoke-virtual {v2, v3}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v1

    sget v3, Lcom/google/android/gms/h;->ca:I

    invoke-virtual {v1, v3}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    move-result-object v1

    iput-object v0, v1, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    sget v0, Lcom/google/android/gms/h;->cZ:I

    sget v3, Lcom/google/android/gms/p;->ro:I

    invoke-virtual {p0, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {p0, p2}, Lcom/google/android/libraries/social/autobackup/FolderAutoBackupReceiver;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v1, v0, v3, v4}, Landroid/support/v4/app/bk;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/bk;

    move-result-object v0

    sget v1, Lcom/google/android/gms/h;->da:I

    sget v3, Lcom/google/android/gms/p;->rp:I

    invoke-virtual {p0, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {p0, p2}, Lcom/google/android/libraries/social/autobackup/FolderAutoBackupReceiver;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v0, v1, v3, v4}, Landroid/support/v4/app/bk;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bk;->a()Landroid/support/v4/app/bk;

    .line 324
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 327
    sget v1, Lcom/google/android/gms/j;->mE:I

    invoke-virtual {v2}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 329
    return-void
.end method

.method public static b(Landroid/content/Context;I)Ljava/lang/String;
    .locals 10

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 185
    const/16 v0, 0x384

    if-ge p1, v0, :cond_0

    .line 186
    sget v0, Lcom/google/android/gms/p;->hu:I

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 193
    :goto_0
    return-object v0

    .line 187
    :cond_0
    const v0, 0xe1000

    if-ge p1, v0, :cond_1

    .line 189
    sget v0, Lcom/google/android/gms/p;->hs:I

    new-array v1, v1, [Ljava/lang/Object;

    int-to-double v2, p1

    const-wide/high16 v4, 0x4090000000000000L    # 1024.0

    div-double/2addr v2, v4

    invoke-static {v2, v3, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 193
    :cond_1
    sget v0, Lcom/google/android/gms/p;->hw:I

    new-array v1, v1, [Ljava/lang/Object;

    int-to-double v2, p1

    const-wide/high16 v4, 0x4130000000000000L    # 1048576.0

    div-double/2addr v2, v4

    invoke-static {v2, v3, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 203
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-static {p0, v0, v2, v3}, Lcom/google/android/libraries/social/autobackup/AutoBackupStalledReceiver;->a(Landroid/content/Context;ZJ)Landroid/app/PendingIntent;

    move-result-object v1

    .line 207
    if-eqz v1, :cond_0

    .line 208
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 210
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 212
    :cond_0
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":notifications:backup_stalled"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/gms/j;->mG:I

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 213
    return-void
.end method

.method private static c(Landroid/content/Context;I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 336
    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p0, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 339
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":notifications:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 352
    const-class v0, Lcom/google/android/libraries/social/autobackup/o;

    invoke-static {p0, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/o;

    .line 354
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/o;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 380
    :cond_0
    :goto_0
    return-void

    .line 358
    :cond_1
    invoke-static {p0}, Lcom/google/android/libraries/social/autobackup/s;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/libraries/social/autobackup/c/b;->c:Lcom/google/android/libraries/social/f/b;

    goto :goto_0

    .line 363
    :cond_2
    invoke-static {p0}, Lcom/google/android/libraries/social/autobackup/s;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 368
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 369
    invoke-static {p0}, Lcom/google/android/libraries/social/autobackup/c/b;->e(Landroid/content/Context;)J

    move-result-wide v2

    .line 370
    sub-long/2addr v0, v2

    sget-object v2, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x5a

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 376
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    array-length v0, v1

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    array-length v3, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_4

    aget-object v4, v1, v0

    iget-object v5, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "@youtube.com"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 379
    const-class v0, Lcom/google/android/libraries/social/autobackup/c/a;

    invoke-static {p0, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 386
    invoke-static {p0}, Lcom/google/android/libraries/social/autobackup/s;->f(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 387
    invoke-static {p0}, Lcom/google/android/libraries/social/autobackup/c/b;->b(Landroid/content/Context;)V

    .line 389
    :cond_0
    return-void
.end method

.method private static e(Landroid/content/Context;)J
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 249
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 250
    const-string v0, "iu.auto_backup_notification_timestamp"

    invoke-interface {v2, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 254
    const-string v0, "accounts"

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 256
    const-string v1, "ab_notification_timestamp"

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 258
    cmp-long v3, v0, v4

    if-lez v3, :cond_0

    .line 259
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v2, "iu.auto_backup_notification_timestamp"

    invoke-interface {v3, v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    const-string v4, "iu.auto_backup_notification_version_code"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 263
    :goto_1
    return-wide v0

    .line 259
    :catch_0
    move-exception v2

    const-string v4, "ABNotifications"

    const-string v5, "Cannot get package version"

    invoke-static {v4, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 263
    :cond_0
    const-string v0, "iu.auto_backup_notification_timestamp"

    invoke-interface {v2, v0, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_1
.end method
