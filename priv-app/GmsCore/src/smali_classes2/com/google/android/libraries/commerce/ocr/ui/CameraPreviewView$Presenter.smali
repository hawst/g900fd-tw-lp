.class public interface abstract Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# virtual methods
.method public abstract enableImageProcessing()V
.end method

.method public abstract onPreviewLayoutFinalized(Landroid/graphics/Rect;Landroid/graphics/Point;)V
.end method

.method public abstract setOnErrorCallback(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$OnErrorCallback;)V
.end method

.method public abstract setOnPreviewLayoutFinalizedCallback(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$OnPreviewLayoutFinalizedCallback;)V
.end method

.method public abstract setOnPreviewReadyCallback(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;)V
.end method

.method public abstract setView(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;)V
.end method
