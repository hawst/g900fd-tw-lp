.class public interface abstract Lcom/google/android/libraries/commerce/ocr/ui/OcrView$Presenter;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract attachView()V
.end method

.method public abstract onPreviewLayoutFinalized(Landroid/graphics/Rect;Landroid/graphics/Point;)V
.end method

.method public abstract onPreviewReady()V
.end method

.method public abstract setOnPreviewReadyCallback(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;)V
.end method

.method public abstract showErrorMessage(I)V
.end method
