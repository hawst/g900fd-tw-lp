.class public Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final root:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;


# direct methods
.method private constructor <init>(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;->root:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    .line 61
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;Lcom/google/android/libraries/commerce/ocr/capture/pipeline/a;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;-><init>(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;)V

    return-void
.end method


# virtual methods
.method public getPipelineNode()Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;->root:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    return-object v0
.end method

.method public pipeTo(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;->root:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->pipeTo(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;)V

    .line 80
    invoke-static {p1}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline;->wire(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;

    move-result-object v0

    return-object v0
.end method

.method public to(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;->root:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->pipeTo(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;)V

    .line 70
    return-object p0
.end method
