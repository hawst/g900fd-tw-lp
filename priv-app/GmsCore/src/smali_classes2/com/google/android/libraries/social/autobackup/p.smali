.class final Lcom/google/android/libraries/social/autobackup/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/account/j;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    const-string v0, "AutobackupAccountSettingsManager-AutoBackupAsAccountSettingMigration"

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/libraries/social/account/f;Lcom/google/android/libraries/social/account/c;)V
    .locals 3

    .prologue
    .line 195
    const-class v0, Lcom/google/android/libraries/social/autobackup/t;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/t;

    .line 196
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/t;->c()V

    .line 197
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/t;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/t;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {p3, v1}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 199
    :goto_0
    const-string v1, "com.google.android.libraries.social.autobackup.AutobackupAccountSettingsManager"

    invoke-interface {p2, v1}, Lcom/google/android/libraries/social/account/f;->h(Ljava/lang/String;)Lcom/google/android/libraries/social/account/f;

    move-result-object v1

    .line 203
    const-string v2, "auto_backup_enabled"

    invoke-interface {v1, v2}, Lcom/google/android/libraries/social/account/f;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 204
    const-string v2, "auto_backup_enabled"

    invoke-interface {v1, v2, v0}, Lcom/google/android/libraries/social/account/f;->b(Ljava/lang/String;Z)Lcom/google/android/libraries/social/account/f;

    .line 206
    :cond_0
    return-void

    .line 197
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
