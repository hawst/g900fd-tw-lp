.class public final Lcom/google/android/libraries/social/autobackup/ad;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/autobackup/ac;


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;

.field private static final e:[Ljava/lang/String;


# instance fields
.field private final f:Landroid/content/Context;

.field private final g:Ljava/util/Map;

.field private final h:Lcom/google/android/libraries/social/autobackup/au;

.field private i:Z

.field private final j:Ljava/util/Set;

.field private final k:Landroid/os/Handler;

.field private final l:Ljava/util/Set;

.field private final m:Ljava/lang/Object;

.field private final n:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 59
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "bucket_id"

    aput-object v1, v0, v5

    const-string v1, "_data"

    aput-object v1, v0, v6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "date_added"

    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/z;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "date_modified"

    invoke-static {v3}, Lcom/google/android/libraries/social/autobackup/z;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/libraries/social/autobackup/z;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " as corrected_added_modified"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    const-string v1, "mime_type"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "0 as orientation"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "bucket_display_name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/libraries/social/autobackup/ad;->a:[Ljava/lang/String;

    .line 71
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "bucket_id"

    aput-object v1, v0, v5

    const-string v1, "_data"

    aput-object v1, v0, v6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "date_added"

    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/z;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "date_modified"

    invoke-static {v3}, Lcom/google/android/libraries/social/autobackup/z;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/libraries/social/autobackup/z;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " as corrected_added_modified"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    const-string v1, "mime_type"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "orientation"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "bucket_display_name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/libraries/social/autobackup/ad;->b:[Ljava/lang/String;

    .line 113
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "bucket_id"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/libraries/social/autobackup/ad;->c:[Ljava/lang/String;

    .line 137
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "media_type"

    aput-object v1, v0, v4

    const-string v1, "volume_name"

    aput-object v1, v0, v5

    const-string v1, "last_media_id"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/libraries/social/autobackup/ad;->d:[Ljava/lang/String;

    .line 156
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "bucket_id"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/libraries/social/autobackup/ad;->e:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->g:Ljava/util/Map;

    .line 175
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->j:Ljava/util/Set;

    .line 179
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->l:Ljava/util/Set;

    .line 184
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->m:Ljava/lang/Object;

    .line 186
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->n:Ljava/lang/Object;

    .line 189
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/ad;->f:Landroid/content/Context;

    .line 190
    const-class v0, Lcom/google/android/libraries/social/autobackup/au;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/au;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->h:Lcom/google/android/libraries/social/autobackup/au;

    .line 191
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "MediaTracker bucket changes"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/libraries/social/autobackup/ad;->k:Landroid/os/Handler;

    .line 192
    invoke-direct {p0}, Lcom/google/android/libraries/social/autobackup/ad;->g()V

    .line 193
    return-void
.end method

.method static synthetic a(Lcom/google/android/libraries/social/autobackup/ad;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->n:Ljava/lang/Object;

    return-object v0
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 97
    const-string v0, "CREATE TABLE media_tracker (_id INTEGER PRIMARY KEY, volume_name TEXT NOT NULL, media_type TEXT NOT NULL,last_media_id INTEGER NOT NULL DEFAULT(0))"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 98
    const-string v0, "CREATE TABLE exclude_bucket (_id INTEGER PRIMARY KEY, bucket_id TEXT UNIQUE NOT NULL)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 99
    const-string v0, "CREATE TABLE local_folders (bucket_id TEXT UNIQUE NOT NULL)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method static synthetic b(Lcom/google/android/libraries/social/autobackup/ad;)Lcom/google/android/libraries/social/autobackup/au;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->h:Lcom/google/android/libraries/social/autobackup/au;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/libraries/social/autobackup/ad;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->f:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/google/android/libraries/social/autobackup/ad;->b:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/google/android/libraries/social/autobackup/ad;->a:[Ljava/lang/String;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 606
    new-instance v1, Lcom/google/android/libraries/social/autobackup/ai;

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->f:Landroid/content/Context;

    const-string v2, "AUTO_BACKUP_MEDIA_TRACKER_INCLUDED_BUCKET_IDS"

    invoke-direct {v1, v0, v2}, Lcom/google/android/libraries/social/autobackup/ai;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 607
    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/ai;->a()Ljava/util/Set;

    move-result-object v0

    .line 608
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 609
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/autobackup/ad;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 611
    :cond_0
    iget-object v0, v1, Lcom/google/android/libraries/social/autobackup/ai;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/libraries/social/autobackup/ai;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 612
    return-void
.end method

.method private f()V
    .locals 6

    .prologue
    .line 618
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/ag;

    .line 619
    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/ad;->h:Lcom/google/android/libraries/social/autobackup/au;

    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/ad;->g:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Lcom/google/android/libraries/social/autobackup/ag;->a(Lcom/google/android/libraries/social/autobackup/au;J)V

    goto :goto_0

    .line 621
    :cond_0
    return-void
.end method

.method private g()V
    .locals 12

    .prologue
    const-wide/16 v6, 0x0

    const/4 v3, 0x0

    .line 627
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 630
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->g:Ljava/util/Map;

    new-instance v1, Lcom/google/android/libraries/social/autobackup/ag;

    const-string v2, "photo"

    const-string v4, "external"

    invoke-direct {v1, v2, v4}, Lcom/google/android/libraries/social/autobackup/ag;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 631
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->g:Ljava/util/Map;

    new-instance v1, Lcom/google/android/libraries/social/autobackup/ag;

    const-string v2, "photo"

    const-string v4, "phoneStorage"

    invoke-direct {v1, v2, v4}, Lcom/google/android/libraries/social/autobackup/ag;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 632
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->g:Ljava/util/Map;

    new-instance v1, Lcom/google/android/libraries/social/autobackup/ag;

    const-string v2, "video"

    const-string v4, "external"

    invoke-direct {v1, v2, v4}, Lcom/google/android/libraries/social/autobackup/ag;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 633
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->g:Ljava/util/Map;

    new-instance v1, Lcom/google/android/libraries/social/autobackup/ag;

    const-string v2, "video"

    const-string v4, "phoneStorage"

    invoke-direct {v1, v2, v4}, Lcom/google/android/libraries/social/autobackup/ag;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 636
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->h:Lcom/google/android/libraries/social/autobackup/au;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/au;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 637
    const-string v1, "media_tracker"

    sget-object v2, Lcom/google/android/libraries/social/autobackup/ad;->d:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 641
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 642
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 643
    const/4 v4, 0x1

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 644
    const/4 v5, 0x2

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 645
    iget-object v5, p0, Lcom/google/android/libraries/social/autobackup/ad;->g:Ljava/util/Map;

    new-instance v8, Lcom/google/android/libraries/social/autobackup/ag;

    invoke-direct {v8, v2, v4}, Lcom/google/android/libraries/social/autobackup/ag;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v5, v8, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 648
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 651
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/ad;->m:Ljava/lang/Object;

    monitor-enter v1

    .line 652
    :try_start_1
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/ad;->j:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 654
    const-string v5, "exclude_bucket"

    sget-object v6, Lcom/google/android/libraries/social/autobackup/ad;->c:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v4, v0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v2

    .line 658
    :goto_1
    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 659
    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 660
    iget-object v5, p0, Lcom/google/android/libraries/social/autobackup/ad;->j:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    .line 663
    :catchall_1
    move-exception v0

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 665
    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0

    .line 663
    :cond_1
    :try_start_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 665
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 667
    const-string v1, "local_folders"

    sget-object v2, Lcom/google/android/libraries/social/autobackup/ad;->e:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 671
    :goto_2
    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 672
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 673
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/ad;->l:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    goto :goto_2

    .line 676
    :catchall_3
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 677
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 28

    .prologue
    .line 293
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/libraries/social/autobackup/ad;->n:Ljava/lang/Object;

    move-object/from16 v18, v0

    monitor-enter v18

    .line 295
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/autobackup/ad;->m:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 297
    :try_start_1
    new-instance v19, Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/ad;->j:Ljava/util/Set;

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 298
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 300
    :try_start_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/autobackup/ad;->i:Z

    if-eqz v2, :cond_0

    .line 301
    const/4 v2, 0x0

    monitor-exit v18

    .line 453
    :goto_0
    return v2

    .line 298
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 454
    :catchall_1
    move-exception v2

    monitor-exit v18

    throw v2

    .line 304
    :cond_0
    :try_start_3
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 305
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/ad;->f:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/ap;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/ap;

    move-result-object v4

    .line 306
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/ad;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 307
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/ad;->h:Lcom/google/android/libraries/social/autobackup/au;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/au;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 308
    const/4 v7, 0x0

    .line 309
    const/4 v15, 0x0

    .line 310
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 312
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/ad;->f:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v22

    .line 314
    const-string v2, "exclusion_scanner.has_run"

    const/4 v8, 0x0

    move-object/from16 v0, v22

    invoke-interface {v0, v2, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v23

    .line 319
    if-nez v23, :cond_2

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v17, v2

    .line 321
    :goto_1
    const-string v2, "iu.UploadsManager"

    const/4 v8, 0x3

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 322
    const-string v2, "iu.UploadsManager"

    const-string v8, "Start processing new media"

    invoke-static {v2, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    :cond_1
    if-nez v23, :cond_3

    .line 326
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/ad;->g:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/social/autobackup/ag;

    .line 327
    invoke-static {v2, v3}, Lcom/google/android/libraries/social/autobackup/ag;->a(Lcom/google/android/libraries/social/autobackup/ag;Landroid/content/ContentResolver;)Lcom/google/android/libraries/social/autobackup/ah;

    move-result-object v9

    iget-wide v10, v9, Lcom/google/android/libraries/social/autobackup/ah;->a:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    move-object/from16 v0, v17

    invoke-interface {v0, v2, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 319
    :cond_2
    const/4 v2, 0x0

    move-object/from16 v17, v2

    goto :goto_1

    .line 331
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/ad;->g:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :cond_4
    :goto_3
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/google/android/libraries/social/autobackup/ag;

    move-object v14, v0

    .line 332
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/autobackup/ad;->i:Z

    if-nez v2, :cond_e

    .line 333
    invoke-virtual {v14, v3}, Lcom/google/android/libraries/social/autobackup/ag;->a(Landroid/content/ContentResolver;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 337
    const-string v2, "iu.UploadsManager"

    const/4 v8, 0x3

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 338
    const-string v2, "iu.UploadsManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "SKIP; "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, v14, Lcom/google/android/libraries/social/autobackup/ag;->b:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v14, Lcom/google/android/libraries/social/autobackup/ag;->a:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 343
    :cond_5
    const-string v2, "iu.UploadsManager"

    const/4 v8, 0x3

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 344
    const-string v2, "iu.UploadsManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "START; "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, v14, Lcom/google/android/libraries/social/autobackup/ag;->b:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v14, Lcom/google/android/libraries/social/autobackup/ag;->a:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    move/from16 v16, v7

    .line 348
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/ad;->g:Ljava/util/Map;

    invoke-interface {v2, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v14, v3, v8, v9}, Lcom/google/android/libraries/social/autobackup/ag;->a(Lcom/google/android/libraries/social/autobackup/ag;Landroid/content/ContentResolver;J)Lcom/google/android/libraries/social/autobackup/ah;

    move-result-object v12

    .line 350
    iget-wide v8, v12, Lcom/google/android/libraries/social/autobackup/ah;->a:J

    .line 351
    iget-object v7, v12, Lcom/google/android/libraries/social/autobackup/ah;->b:Ljava/lang/String;

    .line 352
    const-wide/16 v10, -0x1

    cmp-long v2, v8, v10

    if-eqz v2, :cond_16

    .line 353
    if-eqz v7, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/ad;->l:Ljava/util/Set;

    invoke-interface {v2, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 357
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/ad;->l:Ljava/util/Set;

    invoke-interface {v2, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 358
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/ad;->h:Lcom/google/android/libraries/social/autobackup/au;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/au;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    new-instance v10, Landroid/content/ContentValues;

    const/4 v11, 0x1

    invoke-direct {v10, v11}, Landroid/content/ContentValues;-><init>(I)V

    const-string v11, "bucket_id"

    invoke-virtual {v10, v11, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v11, "local_folders"

    const/4 v13, 0x0

    invoke-virtual {v2, v11, v13, v10}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 359
    move-object/from16 v0, v19

    invoke-interface {v0, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 363
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/google/android/libraries/social/autobackup/ad;->b(Ljava/lang/String;)V

    .line 364
    if-eqz v23, :cond_7

    .line 365
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/ad;->f:Landroid/content/Context;

    const-class v10, Lcom/google/android/libraries/social/autobackup/o;

    invoke-static {v2, v10}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/social/autobackup/o;

    .line 367
    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/o;->e()Ljava/util/List;

    move-result-object v10

    .line 368
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-lez v10, :cond_7

    .line 369
    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/o;->e()Ljava/util/List;

    move-result-object v2

    const/4 v10, 0x0

    invoke-interface {v2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 371
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/libraries/social/autobackup/ad;->f:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    iget-object v2, v12, Lcom/google/android/libraries/social/autobackup/ah;->c:Ljava/lang/String;

    invoke-static {v10, v2, v7}, Lcom/google/android/libraries/social/autobackup/c/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/ad;->g:Ljava/util/Map;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v2, v14, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    invoke-direct/range {p0 .. p0}, Lcom/google/android/libraries/social/autobackup/ad;->f()V

    .line 380
    iget-object v2, v14, Lcom/google/android/libraries/social/autobackup/ag;->c:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v10

    .line 382
    const-string v2, "photo"

    invoke-virtual {v14, v2}, Lcom/google/android/libraries/social/autobackup/ag;->a(Ljava/lang/String;)Z

    move-result v11

    .line 390
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    .line 392
    iget-object v2, v12, Lcom/google/android/libraries/social/autobackup/ah;->d:Ljava/lang/String;

    .line 393
    if-nez v23, :cond_8

    move-object/from16 v0, v17

    invoke-interface {v0, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v26

    cmp-long v2, v26, v8

    if-gez v2, :cond_c

    :cond_8
    if-nez v13, :cond_9

    move-object/from16 v0, v19

    invoke-interface {v0, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_9
    const/4 v12, 0x1

    .line 395
    :goto_5
    if-nez v23, :cond_a

    move-object/from16 v0, v17

    invoke-interface {v0, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v26

    cmp-long v2, v26, v8

    if-gez v2, :cond_d

    :cond_a
    if-eqz v13, :cond_d

    const/4 v13, 0x1

    .line 398
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/ad;->f:Landroid/content/Context;

    invoke-static/range {v2 .. v13}, Lcom/google/android/libraries/social/autobackup/ab;->a(Landroid/content/Context;Landroid/content/ContentResolver;Lcom/google/android/libraries/social/autobackup/ao;Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;JLandroid/net/Uri;ZZZ)Z

    move-result v2

    .line 402
    add-int/lit8 v7, v16, 0x1

    .line 403
    if-eqz v2, :cond_15

    .line 404
    add-int/lit8 v2, v15, 0x1

    .line 406
    :goto_7
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/google/android/libraries/social/autobackup/ad;->i:Z

    if-eqz v8, :cond_14

    .line 408
    :goto_8
    const-string v8, "iu.UploadsManager"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 409
    const-string v8, "iu.UploadsManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "DONE; no more media; "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, v14, Lcom/google/android/libraries/social/autobackup/ag;->b:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v14, Lcom/google/android/libraries/social/autobackup/ag;->a:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    move v15, v2

    .line 412
    goto/16 :goto_3

    .line 393
    :cond_c
    const/4 v12, 0x0

    goto :goto_5

    .line 395
    :cond_d
    const/4 v13, 0x0

    goto :goto_6

    .line 414
    :cond_e
    const-string v2, "iu.UploadsManager"

    const/4 v4, 0x4

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 415
    const-string v2, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "End new media; added: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", uploading: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", time: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v20

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    :cond_f
    invoke-interface/range {v22 .. v22}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v4, "exclusion_scanner.has_run"

    const/4 v5, 0x1

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 424
    invoke-direct/range {p0 .. p0}, Lcom/google/android/libraries/social/autobackup/ad;->e()V

    .line 426
    if-lez v7, :cond_10

    .line 427
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/ad;->f:Landroid/content/Context;

    const-class v4, Lcom/google/android/libraries/social/experiments/c;

    invoke-static {v2, v4}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/social/experiments/c;

    .line 428
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/autobackup/ad;->f:Landroid/content/Context;

    const-class v5, Lcom/google/android/libraries/social/account/b;

    invoke-static {v4, v5}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/social/account/b;

    const/4 v5, 0x1

    new-array v5, v5, [I

    const/4 v6, 0x0

    const/4 v8, 0x4

    aput v8, v5, v6

    invoke-interface {v4, v5}, Lcom/google/android/libraries/social/account/b;->a([I)Ljava/util/List;

    move-result-object v4

    .line 431
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_12

    .line 434
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/ad;->f:Landroid/content/Context;

    const-class v4, Lcom/google/android/libraries/social/autobackup/util/a;

    invoke-static {v2, v4}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/social/autobackup/util/a;

    .line 436
    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/util/a;->a()Z

    move-result v2

    .line 442
    :goto_9
    if-eqz v2, :cond_10

    .line 443
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/ad;->f:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/c/b;->c(Landroid/content/Context;)V

    .line 447
    :cond_10
    if-lez v15, :cond_11

    .line 448
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/ad;->f:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/aj;->a(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 449
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/ad;->f:Landroid/content/Context;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const/4 v3, 0x0

    invoke-static {v2, v4, v5, v3}, Lcom/google/android/libraries/social/autobackup/c/b;->a(Landroid/content/Context;JZ)V

    .line 453
    :cond_11
    monitor-exit v18

    move v2, v7

    goto/16 :goto_0

    .line 439
    :cond_12
    sget-object v5, Lcom/google/android/libraries/social/autobackup/c;->i:Lcom/google/android/libraries/social/experiments/a;

    const/4 v6, 0x0

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v2, v5, v4}, Lcom/google/android/libraries/social/experiments/c;->a(Lcom/google/android/libraries/social/experiments/a;I)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v2

    if-nez v2, :cond_13

    const/4 v2, 0x1

    goto :goto_9

    :cond_13
    const/4 v2, 0x0

    goto :goto_9

    :cond_14
    move v15, v2

    move/from16 v16, v7

    goto/16 :goto_4

    :cond_15
    move v2, v15

    goto/16 :goto_7

    :cond_16
    move v2, v15

    move/from16 v7, v16

    goto/16 :goto_8
.end method

.method public final a(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 575
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/ad;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 576
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->i:Z

    .line 577
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->h:Lcom/google/android/libraries/social/autobackup/au;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/au;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 578
    const-string v2, "media_tracker"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 579
    const-string v2, "local_folders"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 580
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "exclusion_scanner.has_run"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 583
    invoke-direct {p0}, Lcom/google/android/libraries/social/autobackup/ad;->g()V

    .line 584
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->i:Z

    .line 585
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 204
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/ad;->m:Ljava/lang/Object;

    monitor-enter v1

    .line 207
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->l:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    new-instance v0, Lcom/google/android/libraries/social/autobackup/ai;

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/ad;->f:Landroid/content/Context;

    const-string v3, "AUTO_BACKUP_MEDIA_TRACKER_INCLUDED_BUCKET_IDS"

    invoke-direct {v0, v2, v3}, Lcom/google/android/libraries/social/autobackup/ai;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 209
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/ai;->a()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v2}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/ai;->a:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/libraries/social/autobackup/ai;->b:Ljava/lang/String;

    invoke-virtual {v3}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->j:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 214
    monitor-exit v1

    .line 251
    :goto_0
    return-void

    .line 218
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 219
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/ad;->f:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/aj;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 221
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->k:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/libraries/social/autobackup/ae;

    invoke-direct {v2, p0, p1}, Lcom/google/android/libraries/social/autobackup/ae;-><init>(Lcom/google/android/libraries/social/autobackup/ad;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 251
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 256
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/ad;->m:Ljava/lang/Object;

    monitor-enter v1

    .line 258
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->j:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 259
    monitor-exit v1

    .line 288
    :goto_0
    return-void

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 264
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/ad;->f:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/aj;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 266
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->k:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/libraries/social/autobackup/af;

    invoke-direct {v2, p0, p1}, Lcom/google/android/libraries/social/autobackup/af;-><init>(Lcom/google/android/libraries/social/autobackup/ad;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 288
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 544
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->f:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 546
    const-string v1, "exclusion_scanner.has_run"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 197
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/ad;->m:Ljava/lang/Object;

    monitor-enter v1

    .line 198
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->j:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 595
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 596
    const-string v0, "MediaTracker:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 597
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ad;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/ag;

    .line 598
    const-string v3, ";"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/ag;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/ad;->g:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 601
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
