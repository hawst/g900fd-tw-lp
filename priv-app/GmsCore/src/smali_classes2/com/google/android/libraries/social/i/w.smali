.class public Lcom/google/android/libraries/social/i/w;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final d:Landroid/os/Bundle;


# instance fields
.field a:Ljava/util/List;

.field b:Ljava/util/List;

.field private c:Ljava/util/HashSet;

.field private e:Lcom/google/android/libraries/social/i/ab;

.field private f:Lcom/google/android/libraries/social/i/ab;

.field private g:Lcom/google/android/libraries/social/i/ab;

.field private h:Lcom/google/android/libraries/social/i/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sput-object v0, Lcom/google/android/libraries/social/i/w;->d:Landroid/os/Bundle;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/i/w;->a:Ljava/util/List;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/i/w;->b:Ljava/util/List;

    .line 40
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/i/w;->c:Ljava/util/HashSet;

    .line 75
    return-void
.end method

.method public static a(Lcom/google/android/libraries/social/i/ak;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    .line 107
    if-eqz p1, :cond_0

    .line 108
    invoke-static {p0}, Lcom/google/android/libraries/social/i/w;->c(Lcom/google/android/libraries/social/i/ak;)Ljava/lang/String;

    move-result-object v0

    .line 109
    if-eqz v0, :cond_1

    .line 110
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 115
    :cond_0
    :goto_0
    return-object v0

    .line 112
    :cond_1
    sget-object v0, Lcom/google/android/libraries/social/i/w;->d:Landroid/os/Bundle;

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/libraries/social/i/ak;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    invoke-static {p0}, Lcom/google/android/libraries/social/i/w;->c(Lcom/google/android/libraries/social/i/ak;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static c(Lcom/google/android/libraries/social/i/ak;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 208
    const/4 v0, 0x0

    .line 209
    instance-of v1, p0, Lcom/google/android/libraries/social/i/ah;

    if-eqz v1, :cond_0

    .line 210
    instance-of v0, p0, Lcom/google/android/libraries/social/i/al;

    if-eqz v0, :cond_1

    .line 211
    check-cast p0, Lcom/google/android/libraries/social/i/al;

    invoke-interface {p0}, Lcom/google/android/libraries/social/i/al;->a()Ljava/lang/String;

    move-result-object v0

    .line 216
    :cond_0
    :goto_0
    return-object v0

    .line 213
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/libraries/social/i/ab;)Lcom/google/android/libraries/social/i/ab;
    .locals 2

    .prologue
    .line 84
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/i/w;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/libraries/social/i/w;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/i/ak;

    .line 86
    invoke-interface {p1, v0}, Lcom/google/android/libraries/social/i/ab;->a(Lcom/google/android/libraries/social/i/ak;)V

    .line 84
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/i/w;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    return-object p1
.end method

.method public final a(Lcom/google/android/libraries/social/i/ak;)Lcom/google/android/libraries/social/i/ak;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-static {p1}, Lcom/google/android/libraries/social/i/w;->c(Lcom/google/android/libraries/social/i/ak;)Ljava/lang/String;

    move-result-object v1

    .line 55
    if-eqz v1, :cond_1

    .line 56
    iget-object v2, p0, Lcom/google/android/libraries/social/i/w;->c:Ljava/util/HashSet;

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 57
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Duplicate observer tag: \'%s\'. Implement LifecycleObserverTag to provide unique tags."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 61
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/social/i/w;->c:Ljava/util/HashSet;

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 64
    :cond_1
    iget-object v1, p0, Lcom/google/android/libraries/social/i/w;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v0

    .line 65
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/i/w;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 66
    iget-object v0, p0, Lcom/google/android/libraries/social/i/w;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/i/ab;

    .line 67
    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/i/ab;->a(Lcom/google/android/libraries/social/i/ak;)V

    .line 65
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 69
    :cond_2
    return-object p1
.end method

.method public b()V
    .locals 3

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/libraries/social/i/w;->g:Lcom/google/android/libraries/social/i/ab;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/i/w;->b(Lcom/google/android/libraries/social/i/ab;)V

    .line 154
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/i/w;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 155
    iget-object v0, p0, Lcom/google/android/libraries/social/i/w;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/i/ak;

    .line 156
    instance-of v2, v0, Lcom/google/android/libraries/social/i/af;

    if-eqz v2, :cond_0

    .line 157
    check-cast v0, Lcom/google/android/libraries/social/i/af;

    invoke-interface {v0}, Lcom/google/android/libraries/social/i/af;->a()V

    .line 154
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 160
    :cond_1
    return-void
.end method

.method public final b(Lcom/google/android/libraries/social/i/ab;)V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/libraries/social/i/w;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 97
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/libraries/social/i/w;->h:Lcom/google/android/libraries/social/i/ab;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/i/w;->b(Lcom/google/android/libraries/social/i/ab;)V

    .line 189
    iget-object v0, p0, Lcom/google/android/libraries/social/i/w;->e:Lcom/google/android/libraries/social/i/ab;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/i/w;->b(Lcom/google/android/libraries/social/i/ab;)V

    .line 190
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/i/w;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 191
    iget-object v0, p0, Lcom/google/android/libraries/social/i/w;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/i/ak;

    .line 192
    instance-of v2, v0, Lcom/google/android/libraries/social/i/ae;

    if-eqz v2, :cond_0

    .line 193
    check-cast v0, Lcom/google/android/libraries/social/i/ae;

    invoke-interface {v0}, Lcom/google/android/libraries/social/i/ae;->a()V

    .line 190
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 196
    :cond_1
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 119
    new-instance v0, Lcom/google/android/libraries/social/i/x;

    invoke-direct {v0, p0, p1}, Lcom/google/android/libraries/social/i/x;-><init>(Lcom/google/android/libraries/social/i/w;Landroid/os/Bundle;)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/i/w;->a(Lcom/google/android/libraries/social/i/ab;)Lcom/google/android/libraries/social/i/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/i/w;->e:Lcom/google/android/libraries/social/i/ab;

    .line 128
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 173
    new-instance v0, Lcom/google/android/libraries/social/i/aa;

    invoke-direct {v0, p0, p1}, Lcom/google/android/libraries/social/i/aa;-><init>(Lcom/google/android/libraries/social/i/w;Landroid/os/Bundle;)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/i/w;->a(Lcom/google/android/libraries/social/i/ab;)Lcom/google/android/libraries/social/i/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/i/w;->h:Lcom/google/android/libraries/social/i/ab;

    .line 185
    return-void
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 131
    new-instance v0, Lcom/google/android/libraries/social/i/y;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/social/i/y;-><init>(Lcom/google/android/libraries/social/i/w;)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/i/w;->a(Lcom/google/android/libraries/social/i/ab;)Lcom/google/android/libraries/social/i/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/i/w;->f:Lcom/google/android/libraries/social/i/ab;

    .line 139
    return-void
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 142
    new-instance v0, Lcom/google/android/libraries/social/i/z;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/social/i/z;-><init>(Lcom/google/android/libraries/social/i/w;)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/i/w;->a(Lcom/google/android/libraries/social/i/ab;)Lcom/google/android/libraries/social/i/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/i/w;->g:Lcom/google/android/libraries/social/i/ab;

    .line 150
    return-void
.end method

.method public final r()V
    .locals 3

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/libraries/social/i/w;->f:Lcom/google/android/libraries/social/i/ab;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/i/w;->b(Lcom/google/android/libraries/social/i/ab;)V

    .line 164
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/i/w;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 165
    iget-object v0, p0, Lcom/google/android/libraries/social/i/w;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/i/ak;

    .line 166
    instance-of v2, v0, Lcom/google/android/libraries/social/i/aj;

    if-eqz v2, :cond_0

    .line 167
    check-cast v0, Lcom/google/android/libraries/social/i/aj;

    invoke-interface {v0}, Lcom/google/android/libraries/social/i/aj;->b()V

    .line 164
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 170
    :cond_1
    return-void
.end method

.method public final s()V
    .locals 2

    .prologue
    .line 199
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/libraries/social/i/w;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 200
    iget-object v1, p0, Lcom/google/android/libraries/social/i/w;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 201
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 205
    :cond_0
    return-void
.end method
