.class final Lcom/google/android/libraries/commerce/ocr/ui/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;


# instance fields
.field final synthetic a:Lcom/google/android/libraries/commerce/ocr/ui/a;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/commerce/ocr/ui/a;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/ui/b;->a:Lcom/google/android/libraries/commerce/ocr/ui/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFinish()V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/b;->a:Lcom/google/android/libraries/commerce/ocr/ui/a;

    iget-object v0, v0, Lcom/google/android/libraries/commerce/ocr/ui/a;->a:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;

    # getter for: Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->onPreviewReadyCallback:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->access$200(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;)Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/b;->a:Lcom/google/android/libraries/commerce/ocr/ui/a;

    iget-object v0, v0, Lcom/google/android/libraries/commerce/ocr/ui/a;->a:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;

    # getter for: Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->onPreviewReadyCallback:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->access$200(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;)Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;->onFinish()V

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/b;->a:Lcom/google/android/libraries/commerce/ocr/ui/a;

    iget-object v0, v0, Lcom/google/android/libraries/commerce/ocr/ui/a;->a:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;

    # getter for: Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->access$000(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;)Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    move-result-object v0

    new-instance v1, Lcom/google/android/libraries/commerce/ocr/ui/c;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/commerce/ocr/ui/c;-><init>(Lcom/google/android/libraries/commerce/ocr/ui/b;)V

    invoke-interface {v0, v1}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;->addContinuousPreviewImageCallback(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$ImageCallback;)V

    .line 78
    return-void
.end method
