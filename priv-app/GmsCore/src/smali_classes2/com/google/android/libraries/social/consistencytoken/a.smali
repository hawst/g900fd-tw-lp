.class public Lcom/google/android/libraries/social/consistencytoken/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private volatile a:Lcom/google/android/libraries/social/consistencytoken/b;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 33
    iget-object v1, p0, Lcom/google/android/libraries/social/consistencytoken/a;->a:Lcom/google/android/libraries/social/consistencytoken/b;

    .line 34
    if-nez v1, :cond_1

    .line 45
    :cond_0
    :goto_0
    return-object v0

    .line 38
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, v1, Lcom/google/android/libraries/social/consistencytoken/b;->b:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 45
    iget-object v0, v1, Lcom/google/android/libraries/social/consistencytoken/b;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Lcom/google/c/d/a/a/b;)V
    .locals 6

    .prologue
    .line 53
    iget-object v0, p1, Lcom/google/c/d/a/a/b;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gtz v0, :cond_0

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/consistencytoken/a;->a:Lcom/google/android/libraries/social/consistencytoken/b;

    .line 61
    :goto_0
    return-void

    .line 58
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p1, Lcom/google/c/d/a/a/b;->b:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 60
    new-instance v2, Lcom/google/android/libraries/social/consistencytoken/b;

    iget-object v3, p1, Lcom/google/c/d/a/a/b;->a:Ljava/lang/String;

    invoke-direct {v2, v3, v0, v1}, Lcom/google/android/libraries/social/consistencytoken/b;-><init>(Ljava/lang/String;J)V

    iput-object v2, p0, Lcom/google/android/libraries/social/consistencytoken/a;->a:Lcom/google/android/libraries/social/consistencytoken/b;

    goto :goto_0
.end method
