.class Lcom/google/android/libraries/social/e/b;
.super Ljava/io/FilterInputStream;
.source "SourceFile"


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:I

.field private final c:[B

.field private final d:Ljava/nio/ByteBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/libraries/social/e/b;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/social/e/b;->b:I

    .line 35
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/libraries/social/e/b;->c:[B

    .line 36
    iget-object v0, p0, Lcom/google/android/libraries/social/e/b;->c:[B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/e/b;->d:Ljava/nio/ByteBuffer;

    .line 40
    return-void
.end method

.method private a([BI)V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/libraries/social/e/b;->read([BII)I

    move-result v0

    .line 89
    if-eq v0, p2, :cond_0

    .line 90
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 92
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/google/android/libraries/social/e/b;->b:I

    return v0
.end method

.method public final a(ILjava/nio/charset/Charset;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 139
    new-array v0, p1, [B

    .line 140
    array-length v1, v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/social/e/b;->a([BI)V

    .line 141
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0, p2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    return-object v1
.end method

.method public final a(J)V
    .locals 5

    .prologue
    .line 81
    iget v0, p0, Lcom/google/android/libraries/social/e/b;->b:I

    int-to-long v0, v0

    .line 82
    sub-long v0, p1, v0

    .line 83
    sget-boolean v2, Lcom/google/android/libraries/social/e/b;->a:Z

    if-nez v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 84
    :cond_0
    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/e/b;->skip(J)J

    move-result-wide v2

    cmp-long v0, v2, v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 85
    :cond_1
    return-void
.end method

.method public final a(Ljava/nio/ByteOrder;)V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/libraries/social/e/b;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 100
    return-void
.end method

.method public final b()Ljava/nio/ByteOrder;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/libraries/social/e/b;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;

    move-result-object v0

    return-object v0
.end method

.method public final c()S
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/libraries/social/e/b;->c:[B

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/social/e/b;->a([BI)V

    .line 108
    iget-object v0, p0, Lcom/google/android/libraries/social/e/b;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 109
    iget-object v0, p0, Lcom/google/android/libraries/social/e/b;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/libraries/social/e/b;->c()S

    move-result v0

    const v1, 0xffff

    and-int/2addr v0, v1

    return v0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/e/b;->c:[B

    const/4 v1, 0x4

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/social/e/b;->a([BI)V

    .line 118
    iget-object v0, p0, Lcom/google/android/libraries/social/e/b;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 119
    iget-object v0, p0, Lcom/google/android/libraries/social/e/b;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    return v0
.end method

.method public final f()J
    .locals 4

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/libraries/social/e/b;->e()I

    move-result v0

    int-to-long v0, v0

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    return-wide v0
.end method

.method public read()I
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/libraries/social/e/b;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 63
    iget v2, p0, Lcom/google/android/libraries/social/e/b;->b:I

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/libraries/social/e/b;->b:I

    .line 64
    return v1

    .line 63
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public read([B)I
    .locals 3

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/libraries/social/e/b;->in:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .line 49
    iget v2, p0, Lcom/google/android/libraries/social/e/b;->b:I

    if-ltz v1, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/libraries/social/e/b;->b:I

    .line 50
    return v1

    .line 49
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public read([BII)I
    .locals 3

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/libraries/social/e/b;->in:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 56
    iget v2, p0, Lcom/google/android/libraries/social/e/b;->b:I

    if-ltz v1, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/libraries/social/e/b;->b:I

    .line 57
    return v1

    .line 56
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public skip(J)J
    .locals 5

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/libraries/social/e/b;->in:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    .line 70
    iget v2, p0, Lcom/google/android/libraries/social/e/b;->b:I

    int-to-long v2, v2

    add-long/2addr v2, v0

    long-to-int v2, v2

    iput v2, p0, Lcom/google/android/libraries/social/e/b;->b:I

    .line 71
    return-wide v0
.end method
