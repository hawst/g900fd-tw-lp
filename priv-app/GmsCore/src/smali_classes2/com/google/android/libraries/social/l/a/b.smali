.class final Lcom/google/android/libraries/social/l/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[C

.field private static final b:[C

.field private static final c:[C

.field private static final d:[C

.field private static final e:[C

.field private static final f:[C

.field private static final g:[C

.field private static final h:[C

.field private static final i:[C

.field private static final j:[C

.field private static final k:[C

.field private static final l:[C

.field private static final m:[C

.field private static final n:[C

.field private static final o:[C

.field private static final p:[[C


# instance fields
.field private A:Z

.field private B:I

.field private C:I

.field private D:I

.field private E:[I

.field private F:[I

.field private q:[C

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 218
    const-string v0, "http://"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/l/a/b;->a:[C

    .line 219
    const-string v0, "https://"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/l/a/b;->b:[C

    .line 220
    const-string v0, "lh"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/l/a/b;->c:[C

    .line 221
    const-string v0, "sp"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/l/a/b;->d:[C

    .line 222
    const-string v0, "bp"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/l/a/b;->e:[C

    .line 223
    const-string v0, ".googleusercontent.com/"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/l/a/b;->f:[C

    .line 225
    const-string v0, "www.google.com/visualsearch/lh/"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/l/a/b;->g:[C

    .line 227
    const-string v0, ".google.com/"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/l/a/b;->h:[C

    .line 228
    const-string v0, ".blogger.com/"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/l/a/b;->i:[C

    .line 229
    const-string v0, ".ggpht.com/"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/l/a/b;->j:[C

    .line 230
    const-string v0, "public"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/l/a/b;->k:[C

    .line 231
    const-string v0, "proxy"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/l/a/b;->l:[C

    .line 232
    const-string v0, "image"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/l/a/b;->m:[C

    .line 233
    const-string v0, "%3D"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/l/a/b;->n:[C

    .line 234
    const-string v0, "%3d"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/l/a/b;->o:[C

    .line 235
    const/4 v0, 0x5

    new-array v0, v0, [[C

    new-array v1, v5, [C

    const/16 v2, 0x4f

    aput-char v2, v1, v4

    aput-object v1, v0, v4

    new-array v1, v5, [C

    const/16 v2, 0x4a

    aput-char v2, v1, v4

    aput-object v1, v0, v5

    new-array v1, v3, [C

    fill-array-data v1, :array_0

    aput-object v1, v0, v3

    const/4 v1, 0x3

    new-array v2, v5, [C

    const/16 v3, 0x55

    aput-char v3, v2, v4

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v5, [C

    const/16 v3, 0x49

    aput-char v3, v2, v4

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/libraries/social/l/a/b;->p:[[C

    return-void

    :array_0
    .array-data 2
        0x55s
        0x74s
    .end array-data
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 241
    const/16 v0, 0x7d0

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    .line 251
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/l/a/b;->A:Z

    .line 255
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->E:[I

    .line 256
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->F:[I

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 213
    invoke-direct {p0}, Lcom/google/android/libraries/social/l/a/b;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/StringBuilder;)V
    .locals 12

    .prologue
    const/16 v11, 0x2d

    const/4 v1, 0x0

    .line 404
    iget v5, p0, Lcom/google/android/libraries/social/l/a/b;->u:I

    .line 405
    iget v0, p0, Lcom/google/android/libraries/social/l/a/b;->u:I

    iget v2, p0, Lcom/google/android/libraries/social/l/a/b;->B:I

    add-int v7, v0, v2

    .line 406
    :goto_0
    if-ge v5, v7, :cond_5

    move v0, v1

    .line 407
    :goto_1
    sget-object v2, Lcom/google/android/libraries/social/l/a/b;->p:[[C

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 408
    const/4 v6, 0x1

    .line 410
    sget-object v2, Lcom/google/android/libraries/social/l/a/b;->p:[[C

    aget-object v8, v2, v0

    move v2, v1

    move v4, v5

    .line 412
    :goto_2
    array-length v3, v8

    if-ge v2, v3, :cond_7

    if-ge v4, v7, :cond_7

    .line 413
    add-int/lit8 v3, v2, 0x1

    aget-char v9, v8, v2

    iget-object v10, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    add-int/lit8 v2, v4, 0x1

    aget-char v4, v10, v4

    if-eq v9, v4, :cond_6

    move v4, v2

    move v2, v1

    .line 415
    :goto_3
    if-eqz v2, :cond_1

    array-length v6, v8

    if-ne v3, v6, :cond_0

    if-eq v4, v7, :cond_1

    iget-object v3, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    aget-char v3, v3, v4

    if-eq v3, v11, :cond_1

    :cond_0
    move v2, v1

    .line 422
    :cond_1
    if-eqz v2, :cond_3

    .line 423
    sget-object v2, Lcom/google/android/libraries/social/l/a/b;->p:[[C

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 427
    :cond_2
    :goto_4
    if-ge v5, v7, :cond_4

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    aget-char v0, v0, v5

    if-eq v0, v11, :cond_4

    .line 428
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 407
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 430
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 432
    :cond_5
    return-void

    :cond_6
    move v4, v2

    move v2, v3

    goto :goto_2

    :cond_7
    move v3, v2

    move v2, v6

    goto :goto_3
.end method

.method private a(I[C)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 693
    array-length v2, p2

    .line 694
    add-int v1, p1, v2

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->r:I

    if-le v1, v3, :cond_1

    .line 702
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v1, v0

    .line 697
    :goto_1
    if-ge v1, v2, :cond_2

    .line 698
    iget-object v3, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    add-int v4, p1, v1

    aget-char v3, v3, v4

    aget-char v4, p2, v1

    if-ne v3, v4, :cond_0

    .line 697
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 702
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a([C)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 673
    iget v1, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    .line 674
    array-length v4, p1

    .line 675
    add-int v2, v1, v4

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->r:I

    if-le v2, v3, :cond_1

    .line 686
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v2, v1

    move v1, v0

    .line 679
    :goto_1
    if-ge v1, v4, :cond_2

    .line 680
    iget-object v5, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    add-int/lit8 v3, v2, 0x1

    aget-char v5, v5, v2

    add-int/lit8 v2, v1, 0x1

    aget-char v1, p1, v1

    if-ne v5, v1, :cond_0

    move v1, v2

    move v2, v3

    goto :goto_1

    .line 685
    :cond_2
    iput v2, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    .line 686
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(I[C)I
    .locals 6

    .prologue
    .line 715
    array-length v2, p2

    .line 716
    iget v0, p0, Lcom/google/android/libraries/social/l/a/b;->r:I

    sub-int v3, v0, v2

    move v0, p1

    .line 718
    :goto_0
    if-gt v0, v3, :cond_1

    .line 719
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_2

    .line 720
    iget-object v4, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    add-int v5, v0, v1

    aget-char v4, v4, v5

    aget-char v5, p2, v1

    if-ne v4, v5, :cond_0

    .line 721
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 718
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 726
    :cond_1
    const/4 v0, -0x1

    :cond_2
    return v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x5

    const/4 v5, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 453
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/l/a/b;->r:I

    .line 454
    iget v0, p0, Lcom/google/android/libraries/social/l/a/b;->r:I

    const/16 v3, 0x7d0

    if-lt v0, v3, :cond_0

    .line 455
    iput-boolean v1, p0, Lcom/google/android/libraries/social/l/a/b;->A:Z

    .line 509
    :goto_0
    return-void

    .line 459
    :cond_0
    iget v0, p0, Lcom/google/android/libraries/social/l/a/b;->r:I

    iget-object v3, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    invoke-virtual {p1, v1, v0, v3, v1}, Ljava/lang/String;->getChars(II[CI)V

    .line 460
    iput v1, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    .line 461
    iput-boolean v1, p0, Lcom/google/android/libraries/social/l/a/b;->x:Z

    .line 462
    iput-boolean v1, p0, Lcom/google/android/libraries/social/l/a/b;->y:Z

    .line 463
    iput-boolean v1, p0, Lcom/google/android/libraries/social/l/a/b;->z:Z

    .line 465
    sget-object v0, Lcom/google/android/libraries/social/l/a/b;->a:[C

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/l/a/b;->a([C)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/libraries/social/l/a/b;->b:[C

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/l/a/b;->a([C)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/libraries/social/l/a/b;->w:Z

    .line 466
    iget-boolean v0, p0, Lcom/google/android/libraries/social/l/a/b;->w:Z

    if-nez v0, :cond_e

    .line 467
    iput-boolean v1, p0, Lcom/google/android/libraries/social/l/a/b;->A:Z

    goto :goto_0

    .line 465
    :cond_1
    iget v0, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    iput v0, p0, Lcom/google/android/libraries/social/l/a/b;->t:I

    sget-object v0, Lcom/google/android/libraries/social/l/a/b;->c:[C

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/l/a/b;->a([C)Z

    move-result v0

    if-eqz v0, :cond_9

    iput-boolean v2, p0, Lcom/google/android/libraries/social/l/a/b;->x:Z

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    aget-char v0, v0, v3

    const/16 v3, 0x33

    if-lt v0, v3, :cond_8

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    aget-char v0, v0, v3

    const/16 v3, 0x36

    if-gt v0, v3, :cond_8

    iget v0, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    aget-char v0, v0, v3

    const/16 v3, 0x2d

    if-ne v0, v3, :cond_5

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    add-int/lit8 v3, v3, 0x1

    aget-char v0, v0, v3

    const/16 v3, 0x64

    if-ne v0, v3, :cond_5

    iget v0, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    aget-char v0, v0, v3

    const/16 v3, 0x61

    if-lt v0, v3, :cond_2

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    aget-char v0, v0, v3

    const/16 v3, 0x67

    if-le v0, v3, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    aget-char v0, v0, v3

    const/16 v3, 0x7a

    if-ne v0, v3, :cond_4

    :cond_3
    iget v0, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    sget-object v0, Lcom/google/android/libraries/social/l/a/b;->f:[C

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/l/a/b;->a([C)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    sget-object v0, Lcom/google/android/libraries/social/l/a/b;->h:[C

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/l/a/b;->a([C)Z

    move-result v0

    if-eqz v0, :cond_7

    iput-boolean v2, p0, Lcom/google/android/libraries/social/l/a/b;->y:Z

    :cond_6
    iget v0, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/social/l/a/b;->C:I

    move v0, v2

    goto/16 :goto_1

    :cond_7
    sget-object v0, Lcom/google/android/libraries/social/l/a/b;->f:[C

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/l/a/b;->a([C)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/google/android/libraries/social/l/a/b;->j:[C

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/l/a/b;->a([C)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    goto/16 :goto_1

    :cond_8
    move v0, v1

    goto/16 :goto_1

    :cond_9
    sget-object v0, Lcom/google/android/libraries/social/l/a/b;->d:[C

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/l/a/b;->a([C)Z

    move-result v0

    if-eqz v0, :cond_b

    iput-boolean v2, p0, Lcom/google/android/libraries/social/l/a/b;->x:Z

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    aget-char v0, v0, v3

    const/16 v3, 0x31

    if-lt v0, v3, :cond_a

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    aget-char v0, v0, v3

    const/16 v3, 0x33

    if-gt v0, v3, :cond_a

    iget v0, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    sget-object v0, Lcom/google/android/libraries/social/l/a/b;->f:[C

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/l/a/b;->a([C)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/google/android/libraries/social/l/a/b;->j:[C

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/l/a/b;->a([C)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    goto/16 :goto_1

    :cond_a
    move v0, v1

    goto/16 :goto_1

    :cond_b
    iput-boolean v1, p0, Lcom/google/android/libraries/social/l/a/b;->x:Z

    sget-object v0, Lcom/google/android/libraries/social/l/a/b;->e:[C

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/l/a/b;->a([C)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    aget-char v0, v0, v3

    const/16 v3, 0x30

    if-lt v0, v3, :cond_c

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    aget-char v0, v0, v3

    const/16 v3, 0x33

    if-gt v0, v3, :cond_c

    iget v0, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    sget-object v0, Lcom/google/android/libraries/social/l/a/b;->i:[C

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/l/a/b;->a([C)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    goto/16 :goto_1

    :cond_c
    move v0, v1

    goto/16 :goto_1

    :cond_d
    sget-object v0, Lcom/google/android/libraries/social/l/a/b;->g:[C

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/l/a/b;->a([C)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    goto/16 :goto_1

    .line 472
    :cond_e
    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    .line 473
    iget v0, p0, Lcom/google/android/libraries/social/l/a/b;->s:I

    .line 474
    iput v1, p0, Lcom/google/android/libraries/social/l/a/b;->D:I

    .line 476
    :goto_2
    iget v4, p0, Lcom/google/android/libraries/social/l/a/b;->r:I

    if-eq v3, v4, :cond_f

    iget-object v4, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    aget-char v4, v4, v3

    const/16 v6, 0x2f

    if-ne v4, v6, :cond_10

    .line 477
    :cond_f
    if-ne v3, v0, :cond_11

    iget v4, p0, Lcom/google/android/libraries/social/l/a/b;->r:I

    if-eq v3, v4, :cond_11

    .line 479
    add-int/lit8 v0, v3, 0x1

    .line 493
    :cond_10
    :goto_3
    iget v4, p0, Lcom/google/android/libraries/social/l/a/b;->r:I

    if-eq v3, v4, :cond_13

    .line 494
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 481
    :cond_11
    iget v4, p0, Lcom/google/android/libraries/social/l/a/b;->D:I

    const/16 v6, 0x8

    if-lt v4, v6, :cond_12

    .line 482
    iput-boolean v1, p0, Lcom/google/android/libraries/social/l/a/b;->A:Z

    goto/16 :goto_0

    .line 486
    :cond_12
    iget-object v4, p0, Lcom/google/android/libraries/social/l/a/b;->E:[I

    iget v6, p0, Lcom/google/android/libraries/social/l/a/b;->D:I

    aput v0, v4, v6

    .line 487
    iget-object v4, p0, Lcom/google/android/libraries/social/l/a/b;->F:[I

    iget v6, p0, Lcom/google/android/libraries/social/l/a/b;->D:I

    sub-int v0, v3, v0

    aput v0, v4, v6

    .line 488
    add-int/lit8 v0, v3, 0x1

    .line 489
    iget v4, p0, Lcom/google/android/libraries/social/l/a/b;->D:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/android/libraries/social/l/a/b;->D:I

    goto :goto_3

    .line 500
    :cond_13
    iget v0, p0, Lcom/google/android/libraries/social/l/a/b;->D:I

    if-le v0, v2, :cond_25

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->F:[I

    aget v0, v0, v1

    sget-object v3, Lcom/google/android/libraries/social/l/a/b;->m:[C

    array-length v3, v3

    if-ne v0, v3, :cond_25

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->E:[I

    aget v0, v0, v1

    sget-object v3, Lcom/google/android/libraries/social/l/a/b;->m:[C

    invoke-direct {p0, v0, v3}, Lcom/google/android/libraries/social/l/a/b;->a(I[C)Z

    move-result v0

    if-eqz v0, :cond_25

    move v0, v2

    :goto_4
    if-nez v0, :cond_14

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->D:I

    if-ne v3, v7, :cond_14

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->E:[I

    aget v0, v0, v8

    iput v0, p0, Lcom/google/android/libraries/social/l/a/b;->u:I

    iput v1, p0, Lcom/google/android/libraries/social/l/a/b;->B:I

    :goto_5
    move v0, v2

    :goto_6
    if-eqz v0, :cond_18

    .line 501
    iput-boolean v2, p0, Lcom/google/android/libraries/social/l/a/b;->z:Z

    .line 502
    iput-boolean v2, p0, Lcom/google/android/libraries/social/l/a/b;->A:Z

    goto/16 :goto_0

    .line 500
    :cond_14
    if-eqz v0, :cond_15

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->D:I

    const/4 v4, 0x6

    if-ne v3, v4, :cond_15

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->E:[I

    aget v0, v0, v7

    iput v0, p0, Lcom/google/android/libraries/social/l/a/b;->u:I

    iput v1, p0, Lcom/google/android/libraries/social/l/a/b;->B:I

    goto :goto_5

    :cond_15
    if-nez v0, :cond_16

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->D:I

    const/4 v4, 0x6

    if-ne v3, v4, :cond_16

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->E:[I

    aget v0, v0, v8

    iput v0, p0, Lcom/google/android/libraries/social/l/a/b;->u:I

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->F:[I

    aget v0, v0, v8

    iput v0, p0, Lcom/google/android/libraries/social/l/a/b;->B:I

    goto :goto_5

    :cond_16
    if-eqz v0, :cond_17

    iget v0, p0, Lcom/google/android/libraries/social/l/a/b;->D:I

    const/4 v3, 0x7

    if-ne v0, v3, :cond_17

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->E:[I

    aget v0, v0, v7

    iput v0, p0, Lcom/google/android/libraries/social/l/a/b;->u:I

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->F:[I

    aget v0, v0, v7

    iput v0, p0, Lcom/google/android/libraries/social/l/a/b;->B:I

    goto :goto_5

    :cond_17
    move v0, v1

    goto :goto_6

    .line 503
    :cond_18
    iget v0, p0, Lcom/google/android/libraries/social/l/a/b;->D:I

    if-le v0, v2, :cond_24

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->F:[I

    aget v0, v0, v1

    sget-object v3, Lcom/google/android/libraries/social/l/a/b;->k:[C

    array-length v3, v3

    if-ne v0, v3, :cond_19

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->E:[I

    aget v0, v0, v1

    sget-object v3, Lcom/google/android/libraries/social/l/a/b;->k:[C

    invoke-direct {p0, v0, v3}, Lcom/google/android/libraries/social/l/a/b;->a(I[C)Z

    move-result v0

    if-nez v0, :cond_1a

    :cond_19
    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->F:[I

    aget v0, v0, v1

    sget-object v3, Lcom/google/android/libraries/social/l/a/b;->l:[C

    array-length v3, v3

    if-ne v0, v3, :cond_24

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->E:[I

    aget v0, v0, v1

    sget-object v3, Lcom/google/android/libraries/social/l/a/b;->l:[C

    invoke-direct {p0, v0, v3}, Lcom/google/android/libraries/social/l/a/b;->a(I[C)Z

    move-result v0

    if-eqz v0, :cond_24

    :cond_1a
    move v0, v2

    :goto_7
    if-nez v0, :cond_1c

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->D:I

    if-ne v3, v2, :cond_1c

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->E:[I

    aget v3, v0, v1

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->F:[I

    aget v0, v0, v1

    :goto_8
    move v4, v3

    :goto_9
    iget v6, p0, Lcom/google/android/libraries/social/l/a/b;->r:I

    if-ge v4, v6, :cond_1f

    iget-object v6, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    aget-char v6, v6, v4

    const/16 v7, 0x3d

    if-ne v6, v7, :cond_1e

    :goto_a
    iput v4, p0, Lcom/google/android/libraries/social/l/a/b;->v:I

    iget v4, p0, Lcom/google/android/libraries/social/l/a/b;->v:I

    if-eq v4, v5, :cond_20

    iget v4, p0, Lcom/google/android/libraries/social/l/a/b;->v:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/android/libraries/social/l/a/b;->u:I

    :cond_1b
    :goto_b
    iget v4, p0, Lcom/google/android/libraries/social/l/a/b;->v:I

    if-eq v4, v5, :cond_22

    iget v4, p0, Lcom/google/android/libraries/social/l/a/b;->u:I

    sub-int v3, v4, v3

    sub-int/2addr v0, v3

    iput v0, p0, Lcom/google/android/libraries/social/l/a/b;->B:I

    :goto_c
    move v0, v2

    :goto_d
    if-eqz v0, :cond_23

    .line 504
    iput-boolean v1, p0, Lcom/google/android/libraries/social/l/a/b;->z:Z

    .line 505
    iput-boolean v2, p0, Lcom/google/android/libraries/social/l/a/b;->A:Z

    goto/16 :goto_0

    .line 503
    :cond_1c
    if-eqz v0, :cond_1d

    iget v0, p0, Lcom/google/android/libraries/social/l/a/b;->D:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1d

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->E:[I

    aget v3, v0, v2

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->F:[I

    aget v0, v0, v2

    goto :goto_8

    :cond_1d
    move v0, v1

    goto :goto_d

    :cond_1e
    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    :cond_1f
    move v4, v5

    goto :goto_a

    :cond_20
    sget-object v4, Lcom/google/android/libraries/social/l/a/b;->n:[C

    invoke-direct {p0, v3, v4}, Lcom/google/android/libraries/social/l/a/b;->b(I[C)I

    move-result v4

    iput v4, p0, Lcom/google/android/libraries/social/l/a/b;->v:I

    iget v4, p0, Lcom/google/android/libraries/social/l/a/b;->v:I

    if-ne v4, v5, :cond_21

    sget-object v4, Lcom/google/android/libraries/social/l/a/b;->o:[C

    invoke-direct {p0, v3, v4}, Lcom/google/android/libraries/social/l/a/b;->b(I[C)I

    move-result v4

    iput v4, p0, Lcom/google/android/libraries/social/l/a/b;->v:I

    :cond_21
    iget v4, p0, Lcom/google/android/libraries/social/l/a/b;->v:I

    if-eq v4, v5, :cond_1b

    iget v4, p0, Lcom/google/android/libraries/social/l/a/b;->v:I

    add-int/lit8 v4, v4, 0x3

    iput v4, p0, Lcom/google/android/libraries/social/l/a/b;->u:I

    goto :goto_b

    :cond_22
    add-int/2addr v0, v3

    iput v0, p0, Lcom/google/android/libraries/social/l/a/b;->v:I

    iget v0, p0, Lcom/google/android/libraries/social/l/a/b;->v:I

    iput v0, p0, Lcom/google/android/libraries/social/l/a/b;->u:I

    iput v1, p0, Lcom/google/android/libraries/social/l/a/b;->B:I

    goto :goto_c

    .line 507
    :cond_23
    iput-boolean v1, p0, Lcom/google/android/libraries/social/l/a/b;->A:Z

    goto/16 :goto_0

    :cond_24
    move v0, v1

    goto :goto_7

    :cond_25
    move v0, v1

    goto/16 :goto_4
.end method

.method private b(Ljava/lang/StringBuilder;)V
    .locals 5

    .prologue
    const/16 v4, 0x2d

    .line 435
    iget v1, p0, Lcom/google/android/libraries/social/l/a/b;->u:I

    .line 436
    iget v0, p0, Lcom/google/android/libraries/social/l/a/b;->u:I

    iget v2, p0, Lcom/google/android/libraries/social/l/a/b;->B:I

    add-int/2addr v2, v0

    .line 437
    :goto_0
    if-ge v1, v2, :cond_4

    .line 438
    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    aget-char v0, v0, v1

    const/16 v3, 0x66

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    .line 439
    :goto_1
    if-ge v1, v2, :cond_2

    iget-object v3, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    aget-char v3, v3, v1

    if-eq v3, v4, :cond_2

    .line 440
    if-eqz v0, :cond_0

    .line 441
    iget-object v3, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    aget-char v3, v3, v1

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 443
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 438
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 445
    :cond_2
    if-eqz v0, :cond_3

    .line 446
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 448
    :cond_3
    add-int/lit8 v1, v1, 0x1

    .line 449
    goto :goto_0

    .line 450
    :cond_4
    return-void
.end method


# virtual methods
.method final declared-synchronized a(Ljava/lang/String;IIIILandroid/graphics/RectF;I)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 270
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/l/a/b;->b(Ljava/lang/String;)V

    .line 272
    iget-boolean v1, p0, Lcom/google/android/libraries/social/l/a/b;->A:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 273
    const/4 v0, 0x0

    .line 276
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/google/android/libraries/social/l/a/b;->r:I

    add-int/lit8 v2, v2, 0x32

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    if-eqz p7, :cond_1

    iget-boolean v2, p0, Lcom/google/android/libraries/social/l/a/b;->x:Z

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->t:I

    invoke-virtual {v1, v0, v2, v3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lcom/google/android/libraries/social/l/a/b;->y:Z

    sget-object v0, Lcom/google/android/libraries/social/l/a/b;->c:[C

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    rem-int/lit8 v0, p7, 0x4

    add-int/lit8 v0, v0, 0x33

    int-to-char v0, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/google/android/libraries/social/l/a/b;->t:I

    add-int/lit8 v0, v0, 0x3

    iget-object v2, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->C:I

    sub-int/2addr v3, v0

    invoke-virtual {v1, v2, v0, v3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/google/android/libraries/social/l/a/b;->C:I

    :cond_1
    iget-boolean v2, p0, Lcom/google/android/libraries/social/l/a/b;->z:Z

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->u:I

    sub-int/2addr v3, v0

    invoke-virtual {v1, v2, v0, v3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/16 v2, 0x73

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, "d-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v2, Lcom/google/android/libraries/social/l/a/a;->a:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "l"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/google/android/libraries/social/l/a/a;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-direct {p0, v1}, Lcom/google/android/libraries/social/l/a/b;->a(Ljava/lang/StringBuilder;)V

    invoke-direct {p0, v1}, Lcom/google/android/libraries/social/l/a/b;->b(Ljava/lang/StringBuilder;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-le v2, v0, :cond_3

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/libraries/social/l/a/b;->z:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/google/android/libraries/social/l/a/b;->B:I

    if-nez v0, :cond_4

    const/16 v0, 0x2f

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    iget v2, p0, Lcom/google/android/libraries/social/l/a/b;->u:I

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->B:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->r:I

    iget v4, p0, Lcom/google/android/libraries/social/l/a/b;->u:I

    iget v5, p0, Lcom/google/android/libraries/social/l/a/b;->B:I

    add-int/2addr v4, v5

    sub-int/2addr v3, v4

    invoke-virtual {v1, v0, v2, v3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    iget-object v2, p0, Lcom/google/android/libraries/social/l/a/b;->q:[C

    iget v3, p0, Lcom/google/android/libraries/social/l/a/b;->v:I

    sub-int/2addr v3, v0

    invoke-virtual {v1, v2, v0, v3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    const/16 v0, 0x3d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 270
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 263
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/l/a/b;->b(Ljava/lang/String;)V

    .line 264
    iget-boolean v0, p0, Lcom/google/android/libraries/social/l/a/b;->w:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 263
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
