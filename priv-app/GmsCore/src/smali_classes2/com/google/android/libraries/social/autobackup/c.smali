.class public final Lcom/google/android/libraries/social/autobackup/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/libraries/social/experiments/a;

.field public static final b:Lcom/google/android/libraries/social/experiments/a;

.field public static final c:Lcom/google/android/libraries/social/experiments/a;

.field public static final d:Lcom/google/android/libraries/social/experiments/a;

.field public static final e:Lcom/google/android/libraries/social/experiments/a;

.field public static final f:Lcom/google/android/libraries/social/experiments/a;

.field public static final g:Lcom/google/android/libraries/social/experiments/a;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final h:Lcom/google/android/libraries/social/experiments/a;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final i:Lcom/google/android/libraries/social/experiments/a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 18
    new-instance v0, Lcom/google/android/libraries/social/experiments/a;

    const-string v1, "debug.plus.instant_share"

    const-string v2, "true"

    const-string v3, "63a6dd90"

    sget-object v4, Lcom/google/android/libraries/social/experiments/e;->a:Lcom/google/android/libraries/social/experiments/e;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/social/experiments/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/social/experiments/e;)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/c;->a:Lcom/google/android/libraries/social/experiments/a;

    .line 27
    new-instance v0, Lcom/google/android/libraries/social/experiments/a;

    const-string v1, "debug.plus.instant_share_video"

    const-string v2, "true"

    const-string v3, "2848510c"

    sget-object v4, Lcom/google/android/libraries/social/experiments/e;->a:Lcom/google/android/libraries/social/experiments/e;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/social/experiments/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/social/experiments/e;)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/c;->b:Lcom/google/android/libraries/social/experiments/a;

    .line 36
    new-instance v0, Lcom/google/android/libraries/social/experiments/a;

    const-string v1, "debug.plus.upload_media_bg"

    const-string v2, "false"

    const-string v3, "3949ac2b"

    sget-object v4, Lcom/google/android/libraries/social/experiments/e;->a:Lcom/google/android/libraries/social/experiments/e;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/social/experiments/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/social/experiments/e;)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/c;->c:Lcom/google/android/libraries/social/experiments/a;

    .line 44
    new-instance v0, Lcom/google/android/libraries/social/experiments/a;

    const-string v1, "debug.photos.allow_device_mgmt"

    const-string v2, "false"

    const-string v3, "bd4bcefc"

    sget-object v4, Lcom/google/android/libraries/social/experiments/e;->b:Lcom/google/android/libraries/social/experiments/e;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/social/experiments/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/social/experiments/e;)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/c;->d:Lcom/google/android/libraries/social/experiments/a;

    .line 53
    new-instance v0, Lcom/google/android/libraries/social/experiments/a;

    const-string v1, "debug.photos.max_video_upload"

    const-wide/32 v2, 0x9600000

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    const-string v3, "c20dbbc8"

    sget-object v4, Lcom/google/android/libraries/social/experiments/e;->b:Lcom/google/android/libraries/social/experiments/e;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/social/experiments/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/social/experiments/e;)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/c;->e:Lcom/google/android/libraries/social/experiments/a;

    .line 63
    new-instance v0, Lcom/google/android/libraries/social/experiments/a;

    const-string v1, "debug.photos.gms_ab_kill"

    const-string v2, "false"

    const-string v3, "cf8ab61a"

    sget-object v4, Lcom/google/android/libraries/social/experiments/e;->b:Lcom/google/android/libraries/social/experiments/e;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/social/experiments/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/social/experiments/e;)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/c;->f:Lcom/google/android/libraries/social/experiments/a;

    .line 72
    new-instance v0, Lcom/google/android/libraries/social/experiments/a;

    const-string v1, "debug.photos.view_photos_of_me"

    const-string v2, "false"

    const-string v3, "1fc8d78d"

    sget-object v4, Lcom/google/android/libraries/social/experiments/e;->b:Lcom/google/android/libraries/social/experiments/e;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/social/experiments/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/social/experiments/e;)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/c;->g:Lcom/google/android/libraries/social/experiments/a;

    .line 85
    new-instance v0, Lcom/google/android/libraries/social/experiments/a;

    const-string v1, "debug.photos.close_to_quota"

    const-wide/32 v2, 0x40000000

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    const-string v3, "5ec52a1"

    sget-object v4, Lcom/google/android/libraries/social/experiments/e;->b:Lcom/google/android/libraries/social/experiments/e;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/social/experiments/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/social/experiments/e;)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/c;->h:Lcom/google/android/libraries/social/experiments/a;

    .line 95
    new-instance v0, Lcom/google/android/libraries/social/experiments/a;

    const-string v1, "debug.plus.turn_off_ab_notif"

    const-string v2, "false"

    const-string v3, "a1f420d7"

    sget-object v4, Lcom/google/android/libraries/social/experiments/e;->b:Lcom/google/android/libraries/social/experiments/e;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/social/experiments/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/social/experiments/e;)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/c;->i:Lcom/google/android/libraries/social/experiments/a;

    return-void
.end method
