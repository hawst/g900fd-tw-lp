.class public Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;


# static fields
.field private static final TAG:Ljava/lang/String; = "CameraPreviewPresenterImpl"


# instance fields
.field private final cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

.field private final gatingProcessor:Lcom/google/android/libraries/commerce/ocr/capture/processors/GatingProcessor;

.field private final killFrameProcessorsOnSurfaceDestroy:Z

.field private final ocrImagePool:Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;

.field private onErrorCallback:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$OnErrorCallback;

.field private onPreviewLayoutFinalizedCallback:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$OnPreviewLayoutFinalizedCallback;

.field private onPreviewReadyCallback:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;

.field private final pipelineProcessor:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

.field private previewing:Z

.field private view:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;Lcom/google/android/libraries/commerce/ocr/capture/processors/GatingProcessor;Z)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->previewing:Z

    .line 41
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    .line 42
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->ocrImagePool:Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;

    .line 43
    iput-object p3, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->pipelineProcessor:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    .line 44
    iput-object p4, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->gatingProcessor:Lcom/google/android/libraries/commerce/ocr/capture/processors/GatingProcessor;

    .line 45
    invoke-virtual {p4}, Lcom/google/android/libraries/commerce/ocr/capture/processors/GatingProcessor;->block()V

    .line 47
    iput-boolean p5, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->killFrameProcessorsOnSurfaceDestroy:Z

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;)Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;)Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->view:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;)Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->onPreviewReadyCallback:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->pipelineProcessor:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    return-object v0
.end method

.method private handleCameraException(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 91
    const-string v0, "CameraPreviewPresenterImpl"

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->onErrorCallback:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$OnErrorCallback;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->onErrorCallback:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$OnErrorCallback;

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$OnErrorCallback;->onError()V

    .line 95
    :cond_0
    return-void
.end method


# virtual methods
.method public enableImageProcessing()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->gatingProcessor:Lcom/google/android/libraries/commerce/ocr/capture/processors/GatingProcessor;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/capture/processors/GatingProcessor;->allow()V

    .line 100
    return-void
.end method

.method public onPreviewLayoutFinalized(Landroid/graphics/Rect;Landroid/graphics/Point;)V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->onPreviewLayoutFinalizedCallback:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$OnPreviewLayoutFinalizedCallback;

    invoke-interface {v0, p1, p2}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$OnPreviewLayoutFinalizedCallback;->onFinalized(Landroid/graphics/Rect;Landroid/graphics/Point;)V

    .line 122
    return-void
.end method

.method public setOnErrorCallback(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$OnErrorCallback;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->onErrorCallback:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$OnErrorCallback;

    .line 138
    return-void
.end method

.method public setOnPreviewLayoutFinalizedCallback(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$OnPreviewLayoutFinalizedCallback;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->onPreviewLayoutFinalizedCallback:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$OnPreviewLayoutFinalizedCallback;

    .line 128
    return-void
.end method

.method public setOnPreviewReadyCallback(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->onPreviewReadyCallback:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;

    .line 133
    return-void
.end method

.method public setView(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->view:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;

    .line 117
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 3

    .prologue
    .line 54
    const-string v0, "CameraPreviewPresenterImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Camera surface changed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 4

    .prologue
    .line 59
    const-string v0, "CameraPreviewPresenterImpl"

    const-string v1, "Camera surface created"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    iget-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->previewing:Z

    if-nez v0, :cond_0

    .line 62
    :try_start_0
    new-instance v0, Landroid/graphics/Point;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->view:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;

    invoke-interface {v1}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;->toView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->view:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;

    invoke-interface {v2}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;->toView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 63
    const-string v1, "CameraPreviewPresenterImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Camera container size: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    new-instance v2, Lcom/google/android/libraries/commerce/ocr/ui/a;

    invoke-direct {v2, p0}, Lcom/google/android/libraries/commerce/ocr/ui/a;-><init>(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;)V

    invoke-interface {v1, v0, p1, v2}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;->openDriver(Landroid/graphics/Point;Landroid/view/SurfaceHolder;Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;)V

    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->previewing:Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 83
    :catch_0
    move-exception v0

    .line 84
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 85
    invoke-direct {p0, v0}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->handleCameraException(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;->stopPreview()V

    .line 105
    iget-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->killFrameProcessorsOnSurfaceDestroy:Z

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->pipelineProcessor:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->shutdown()V

    .line 108
    :cond_0
    const-string v0, "CameraPreviewPresenterImpl"

    const-string v1, "Camera surface destroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;->closeDriver()V

    .line 110
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->ocrImagePool:Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->close()V

    .line 111
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->previewing:Z

    .line 112
    return-void
.end method
