.class public Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$ExpDateResult;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final confidence:D

.field private final digitPosX:[I

.field private final digitPosY:[I

.field private final expDate:Ljava/lang/String;

.field private final predictionStatus:I


# direct methods
.method public constructor <init>(Ljava/lang/String;ID[I[I)V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$ExpDateResult;->expDate:Ljava/lang/String;

    .line 99
    iput p2, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$ExpDateResult;->predictionStatus:I

    .line 100
    iput-wide p3, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$ExpDateResult;->confidence:D

    .line 101
    iput-object p5, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$ExpDateResult;->digitPosX:[I

    .line 102
    iput-object p6, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$ExpDateResult;->digitPosY:[I

    .line 103
    return-void
.end method


# virtual methods
.method public getConfidence()D
    .locals 2

    .prologue
    .line 114
    iget-wide v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$ExpDateResult;->confidence:D

    return-wide v0
.end method

.method public getDigitPosX()[I
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$ExpDateResult;->digitPosX:[I

    return-object v0
.end method

.method public getDigitPosY()[I
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$ExpDateResult;->digitPosY:[I

    return-object v0
.end method

.method public getExpirationDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$ExpDateResult;->expDate:Ljava/lang/String;

    return-object v0
.end method

.method public getPredictionStatus()I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$ExpDateResult;->predictionStatus:I

    return v0
.end method
