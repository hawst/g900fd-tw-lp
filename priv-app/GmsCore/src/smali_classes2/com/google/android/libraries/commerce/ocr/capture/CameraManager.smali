.class public interface abstract Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final IMAGE_QUALITY_FULL_PICTURE:B = 0x2t

.field public static final IMAGE_QUALITY_PREVIEW:B = 0x1t


# virtual methods
.method public abstract addContinuousPreviewImageCallback(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$ImageCallback;)V
.end method

.method public abstract closeDriver()V
.end method

.method public abstract getOrientation()I
.end method

.method public abstract getPictureSize()Landroid/graphics/Point;
.end method

.method public abstract getPreviewFormat()I
.end method

.method public abstract getPreviewSize()Landroid/graphics/Point;
.end method

.method public abstract getScreenFillingPreviewSize()Landroid/graphics/Point;
.end method

.method public abstract isFocusing()Z
.end method

.method public abstract isInContinuousPictureFocusMode()Z
.end method

.method public abstract openDriver(Landroid/graphics/Point;Landroid/view/SurfaceHolder;Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;)V
.end method

.method public abstract requestAutoFocus()V
.end method

.method public abstract requestAutoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
.end method

.method public abstract requestImage(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$ImageCallback;B)V
.end method

.method public abstract restartPreview()V
.end method

.method public abstract setOrientation()V
.end method

.method public abstract startPreview(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;)V
.end method

.method public abstract stopPreview()V
.end method

.method public abstract supportsAutoFocus()Z
.end method
