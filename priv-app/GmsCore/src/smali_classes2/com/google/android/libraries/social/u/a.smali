.class public final Lcom/google/android/libraries/social/u/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/lang/Integer;

.field private static b:Ljava/lang/Integer;

.field private static c:Ljava/lang/Integer;

.field private static d:Ljava/lang/Integer;

.field private static e:Ljava/lang/String;


# direct methods
.method public static declared-synchronized a(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 37
    const-class v1, Lcom/google/android/libraries/social/u/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/libraries/social/u/a;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 38
    invoke-static {p0}, Lcom/google/android/libraries/social/u/a;->e(Landroid/content/Context;)V

    .line 40
    :cond_0
    sget-object v0, Lcom/google/android/libraries/social/u/a;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized b(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 44
    const-class v1, Lcom/google/android/libraries/social/u/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/libraries/social/u/a;->b:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 45
    invoke-static {p0}, Lcom/google/android/libraries/social/u/a;->e(Landroid/content/Context;)V

    .line 47
    :cond_0
    sget-object v0, Lcom/google/android/libraries/social/u/a;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    .line 44
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized c(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 51
    const-class v1, Lcom/google/android/libraries/social/u/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/libraries/social/u/a;->c:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 52
    invoke-static {p0}, Lcom/google/android/libraries/social/u/a;->e(Landroid/content/Context;)V

    .line 54
    :cond_0
    sget-object v0, Lcom/google/android/libraries/social/u/a;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized d(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 58
    const-class v1, Lcom/google/android/libraries/social/u/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/libraries/social/u/a;->d:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 59
    invoke-static {p0}, Lcom/google/android/libraries/social/u/a;->e(Landroid/content/Context;)V

    .line 61
    :cond_0
    sget-object v0, Lcom/google/android/libraries/social/u/a;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static e(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 72
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/u/a;->a:Ljava/lang/Integer;

    .line 73
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/u/a;->b:Ljava/lang/Integer;

    .line 74
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/u/a;->c:Ljava/lang/Integer;

    .line 75
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/u/a;->d:Ljava/lang/Integer;

    .line 76
    const-string v0, ""

    sput-object v0, Lcom/google/android/libraries/social/u/a;->e:Ljava/lang/String;

    .line 78
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 79
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 80
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 81
    iget v1, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/google/android/libraries/social/u/a;->a:Ljava/lang/Integer;

    .line 83
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 84
    if-eqz v0, :cond_0

    const-string v1, "DEVELOPMENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 85
    :cond_0
    const-string v0, "dev"

    sput-object v0, Lcom/google/android/libraries/social/u/a;->e:Ljava/lang/String;

    .line 86
    const v0, 0x3b9ac9ff

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/u/a;->b:Ljava/lang/Integer;

    .line 87
    const v0, 0x3b9ac9ff

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/u/a;->c:Ljava/lang/Integer;

    .line 88
    const v0, 0x3b9ac9ff

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/u/a;->d:Ljava/lang/Integer;

    .line 107
    :cond_1
    :goto_0
    return-void

    .line 90
    :cond_2
    invoke-static {p0}, Lcom/google/android/libraries/social/u/a;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/libraries/social/u/a;->e:Ljava/lang/String;

    .line 91
    const-string v1, "\\."

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 92
    array-length v1, v0

    if-lez v1, :cond_3

    .line 93
    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/google/android/libraries/social/u/a;->b:Ljava/lang/Integer;

    .line 95
    :cond_3
    array-length v1, v0

    if-le v1, v3, :cond_4

    .line 96
    const/4 v1, 0x1

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/google/android/libraries/social/u/a;->c:Ljava/lang/Integer;

    .line 98
    :cond_4
    array-length v1, v0

    if-le v1, v4, :cond_1

    .line 99
    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/u/a;->d:Ljava/lang/Integer;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 106
    :catch_0
    move-exception v0

    goto :goto_0

    .line 107
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private static f(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 111
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "buildtype"

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 112
    invoke-static {v0}, Lcom/google/android/libraries/b/a/h;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    .line 113
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 115
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, ""

    goto :goto_0
.end method
