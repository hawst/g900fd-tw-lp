.class public final Lcom/google/android/libraries/social/rpc/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/b/a/a/a/a/c;


# direct methods
.method public constructor <init>([B)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Lcom/google/b/a/a/a/a/b;

    invoke-direct {v0}, Lcom/google/b/a/a/a/a/b;-><init>()V

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/a/a/a/b;

    iget-object v0, v0, Lcom/google/b/a/a/a/a/b;->a:Lcom/google/b/a/a/a/a/c;

    iput-object v0, p0, Lcom/google/android/libraries/social/rpc/b/a;->a:Lcom/google/b/a/a/a/a/c;

    .line 21
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/b/a;->a:Lcom/google/b/a/a/a/a/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/b/a;->a:Lcom/google/b/a/a/a/a/c;

    iget-object v0, v0, Lcom/google/b/a/a/a/a/c;->c:[Lcom/google/b/a/a/a/a/d;

    if-eqz v0, :cond_1

    .line 28
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/b/a;->a:Lcom/google/b/a/a/a/a/c;

    iget-object v1, v1, Lcom/google/b/a/a/a/a/c;->c:[Lcom/google/b/a/a/a/a/d;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 29
    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/b/a;->a:Lcom/google/b/a/a/a/a/c;

    iget-object v1, v1, Lcom/google/b/a/a/a/a/c;->c:[Lcom/google/b/a/a/a/a/d;

    aget-object v1, v1, v0

    .line 30
    iget-object v2, v1, Lcom/google/b/a/a/a/a/d;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 31
    iget-object v0, v1, Lcom/google/b/a/a/a/a/d;->c:Ljava/lang/String;

    .line 35
    :goto_1
    return-object v0

    .line 28
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 35
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/b/a;->a:Lcom/google/b/a/a/a/a/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/b/a;->a:Lcom/google/b/a/a/a/a/c;

    iget-object v0, v0, Lcom/google/b/a/a/a/a/c;->c:[Lcom/google/b/a/a/a/a/d;

    if-eqz v0, :cond_1

    .line 43
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/b/a;->a:Lcom/google/b/a/a/a/a/c;

    iget-object v1, v1, Lcom/google/b/a/a/a/a/c;->c:[Lcom/google/b/a/a/a/a/d;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 44
    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/b/a;->a:Lcom/google/b/a/a/a/a/c;

    iget-object v1, v1, Lcom/google/b/a/a/a/a/c;->c:[Lcom/google/b/a/a/a/a/d;

    aget-object v1, v1, v0

    .line 45
    iget-object v2, v1, Lcom/google/b/a/a/a/a/d;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 46
    iget-object v0, v1, Lcom/google/b/a/a/a/a/d;->b:Ljava/lang/String;

    .line 50
    :goto_1
    return-object v0

    .line 43
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 50
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/b/a;->a:Lcom/google/b/a/a/a/a/c;

    if-eqz v0, :cond_2

    .line 58
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/b/a;->a:Lcom/google/b/a/a/a/a/c;

    iget-object v0, v0, Lcom/google/b/a/a/a/a/c;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/b/a;->a:Lcom/google/b/a/a/a/a/c;

    iget-object v0, v0, Lcom/google/b/a/a/a/a/c;->b:Ljava/lang/String;

    .line 70
    :goto_0
    return-object v0

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/b/a;->a:Lcom/google/b/a/a/a/a/c;

    iget-object v0, v0, Lcom/google/b/a/a/a/a/c;->c:[Lcom/google/b/a/a/a/a/d;

    if-eqz v0, :cond_2

    .line 62
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/b/a;->a:Lcom/google/b/a/a/a/a/c;

    iget-object v1, v1, Lcom/google/b/a/a/a/a/c;->c:[Lcom/google/b/a/a/a/a/d;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 63
    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/b/a;->a:Lcom/google/b/a/a/a/a/c;

    iget-object v1, v1, Lcom/google/b/a/a/a/a/c;->c:[Lcom/google/b/a/a/a/a/d;

    aget-object v1, v1, v0

    .line 64
    iget-object v2, v1, Lcom/google/b/a/a/a/a/d;->a:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 65
    iget-object v0, v1, Lcom/google/b/a/a/a/a/d;->a:Ljava/lang/String;

    goto :goto_0

    .line 62
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 70
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/b/a;->a:Lcom/google/b/a/a/a/a/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/b/a;->a:Lcom/google/b/a/a/a/a/c;

    iget-object v0, v0, Lcom/google/b/a/a/a/a/c;->c:[Lcom/google/b/a/a/a/a/d;

    if-eqz v0, :cond_1

    .line 79
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/b/a;->a:Lcom/google/b/a/a/a/a/c;

    iget-object v1, v1, Lcom/google/b/a/a/a/a/c;->c:[Lcom/google/b/a/a/a/a/d;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 80
    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/b/a;->a:Lcom/google/b/a/a/a/a/c;

    iget-object v1, v1, Lcom/google/b/a/a/a/a/c;->c:[Lcom/google/b/a/a/a/a/d;

    aget-object v1, v1, v0

    .line 81
    iget-object v2, v1, Lcom/google/b/a/a/a/a/d;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 82
    iget-object v0, v1, Lcom/google/b/a/a/a/a/d;->g:Ljava/lang/String;

    .line 86
    :goto_1
    return-object v0

    .line 79
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 86
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
