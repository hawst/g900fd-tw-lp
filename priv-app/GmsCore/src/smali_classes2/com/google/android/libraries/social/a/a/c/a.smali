.class public Lcom/google/android/libraries/social/a/a/c/a;
.super Lcom/google/android/libraries/social/i/a/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/a/d;


# instance fields
.field protected final j:Lcom/google/android/libraries/social/a/e;

.field protected final k:Lcom/google/android/libraries/social/a/a;

.field private m:Lcom/google/android/libraries/social/i/ab;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/libraries/social/i/a/a;-><init>()V

    .line 25
    new-instance v0, Lcom/google/android/libraries/social/a/e;

    invoke-direct {v0}, Lcom/google/android/libraries/social/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/a/a/c/a;->j:Lcom/google/android/libraries/social/a/e;

    .line 26
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a/c/a;->j:Lcom/google/android/libraries/social/a/e;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/a/e;->U_()Lcom/google/android/libraries/social/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/a/a/c/a;->k:Lcom/google/android/libraries/social/a/a;

    return-void
.end method

.method static synthetic a(Lcom/google/android/libraries/social/a/a/c/a;)Lcom/google/android/libraries/social/i/r;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a/c/a;->l:Lcom/google/android/libraries/social/i/r;

    return-object v0
.end method


# virtual methods
.method public final U_()Lcom/google/android/libraries/social/a/a;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a/c/a;->k:Lcom/google/android/libraries/social/a/a;

    return-object v0
.end method

.method public getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 70
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/i/a/a;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    .line 71
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a/c/a;->j:Lcom/google/android/libraries/social/a/e;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    return-object v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/google/android/libraries/social/a/a/c/a;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Landroid/support/v4/app/Fragment;)Lcom/google/android/libraries/social/a/a;

    move-result-object v0

    .line 37
    iget-object v1, p0, Lcom/google/android/libraries/social/a/a/c/a;->j:Lcom/google/android/libraries/social/a/e;

    invoke-virtual {v1, p1}, Lcom/google/android/libraries/social/a/e;->a(Landroid/content/Context;)V

    .line 38
    iget-object v1, p0, Lcom/google/android/libraries/social/a/a/c/a;->j:Lcom/google/android/libraries/social/a/e;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/a/e;->a(Lcom/google/android/libraries/social/a/a;)V

    .line 39
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a/c/a;->k:Lcom/google/android/libraries/social/a/a;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/String;)V

    .line 40
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/i/a/a;->onAttach(Landroid/app/Activity;)V

    .line 41
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a/c/a;->k:Lcom/google/android/libraries/social/a/a;

    new-instance v1, Lcom/google/android/libraries/social/a/a/c/a/c;

    iget-object v2, p0, Lcom/google/android/libraries/social/a/a/c/a;->l:Lcom/google/android/libraries/social/i/r;

    invoke-direct {v1, p0, v2}, Lcom/google/android/libraries/social/a/a/c/a/c;-><init>(Landroid/support/v4/app/Fragment;Lcom/google/android/libraries/social/i/w;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Lcom/google/android/libraries/social/a/f;)Lcom/google/android/libraries/social/a/a;

    .line 46
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a/c/a;->k:Lcom/google/android/libraries/social/a/a;

    const-class v1, Lcom/google/android/libraries/social/a/a/c/b/a;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/a/a;->c(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/libraries/social/a/a/c/a;->l:Lcom/google/android/libraries/social/i/r;

    iget-object v1, p0, Lcom/google/android/libraries/social/a/a/c/a;->k:Lcom/google/android/libraries/social/a/a;

    goto :goto_0

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a/c/a;->k:Lcom/google/android/libraries/social/a/a;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/a/a;->a()V

    .line 49
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a/c/a;->l:Lcom/google/android/libraries/social/i/r;

    new-instance v1, Lcom/google/android/libraries/social/a/a/c/b;

    invoke-direct {v1, p0, p1}, Lcom/google/android/libraries/social/a/a/c/b;-><init>(Lcom/google/android/libraries/social/a/a/c/a;Landroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/i/r;->a(Lcom/google/android/libraries/social/i/ab;)Lcom/google/android/libraries/social/i/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/a/a/c/a;->m:Lcom/google/android/libraries/social/i/ab;

    .line 59
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/i/a/a;->onCreate(Landroid/os/Bundle;)V

    .line 60
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a/c/a;->l:Lcom/google/android/libraries/social/i/r;

    iget-object v1, p0, Lcom/google/android/libraries/social/a/a/c/a;->m:Lcom/google/android/libraries/social/i/ab;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/i/r;->b(Lcom/google/android/libraries/social/i/ab;)V

    .line 65
    invoke-super {p0}, Lcom/google/android/libraries/social/i/a/a;->onDestroy()V

    .line 66
    return-void
.end method
