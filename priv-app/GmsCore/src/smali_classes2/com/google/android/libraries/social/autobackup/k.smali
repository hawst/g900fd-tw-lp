.class final Lcom/google/android/libraries/social/autobackup/k;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/libraries/social/autobackup/i;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/social/autobackup/i;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/k;->a:Lcom/google/android/libraries/social/autobackup/i;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 218
    :try_start_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown message: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catch Lcom/google/android/libraries/social/account/e; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    :catch_0
    move-exception v0

    .line 220
    const-string v1, "iu.SyncManager"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 221
    const-string v1, "iu.SyncManager"

    const-string v2, "Account removed during sync"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 218
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/k;->a:Lcom/google/android/libraries/social/autobackup/i;

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/i;->b(Lcom/google/android/libraries/social/autobackup/i;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/k;->a:Lcom/google/android/libraries/social/autobackup/i;

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/i;->c(Lcom/google/android/libraries/social/autobackup/i;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/k;->a:Lcom/google/android/libraries/social/autobackup/i;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/android/libraries/social/autobackup/i;->a(Lcom/google/android/libraries/social/autobackup/i;J)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/k;->a:Lcom/google/android/libraries/social/autobackup/i;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/google/android/libraries/social/autobackup/i;->a(Lcom/google/android/libraries/social/autobackup/i;I)V

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/k;->a:Lcom/google/android/libraries/social/autobackup/i;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/google/android/libraries/social/autobackup/i;->b(Lcom/google/android/libraries/social/autobackup/i;I)V
    :try_end_1
    .catch Lcom/google/android/libraries/social/account/e; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method
