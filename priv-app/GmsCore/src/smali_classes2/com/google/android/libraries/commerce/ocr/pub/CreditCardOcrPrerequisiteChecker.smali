.class public Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOcrPrerequisiteChecker;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteChecker;


# static fields
.field public static final DEFAULT_CREDIT_CARD_OCR_MODEL_BLACKLIST:Ljava/util/HashSet;

.field private static final TAG:Ljava/lang/String; = "CreditCardPrerequisiteChecker"


# instance fields
.field private final allowedCpus:Ljava/util/HashSet;

.field private final checkerUtil:Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;

.field private final modelBlacklist:Ljava/util/HashSet;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 20
    const/16 v0, 0x1d

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "SCH-S738C"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "ALCATEL-ONE-TOUCH-Fierce"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "LG-VM696"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "GT-P3113"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "SM-T217S"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "LG-LS840"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "SPH-M840"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Huawei-Y301A1"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "SCH-R740C"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "C5155"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "Event"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "SPH-M830"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "ALCATEL-ONE-TOUCH-5020N"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "LGMS500"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "C5215"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "Z992"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "GT-N8013"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "SGH-M919N"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "LG-VS410PP"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "ALCATEL-ONE-TOUCH-5020T"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "HTCEVOV4G"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "N9510"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "XT897"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "LG-MS870"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "N8000-USA-Cricket"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "H866C"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "LG-MS695"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "N9810"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "C6522N"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOcrPrerequisiteChecker;->DEFAULT_CREDIT_CARD_OCR_MODEL_BLACKLIST:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;Ljava/util/HashSet;Ljava/util/HashSet;)V
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOcrPrerequisiteChecker;->DEFAULT_CREDIT_CARD_OCR_MODEL_BLACKLIST:Ljava/util/HashSet;

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOcrPrerequisiteChecker;-><init>(Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;Ljava/util/HashSet;Ljava/util/HashSet;Ljava/util/HashSet;)V

    .line 62
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;Ljava/util/HashSet;Ljava/util/HashSet;Ljava/util/HashSet;)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOcrPrerequisiteChecker;->checkerUtil:Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;

    .line 69
    iput-object p4, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOcrPrerequisiteChecker;->allowedCpus:Ljava/util/HashSet;

    .line 70
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOcrPrerequisiteChecker;->modelBlacklist:Ljava/util/HashSet;

    .line 71
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOcrPrerequisiteChecker;->modelBlacklist:Ljava/util/HashSet;

    invoke-virtual {v0, p3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 72
    return-void
.end method


# virtual methods
.method public check()Z
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOcrPrerequisiteChecker;->checkerUtil:Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->buildVersionOK()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOcrPrerequisiteChecker;->checkerUtil:Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOcrPrerequisiteChecker;->modelBlacklist:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->modelOK(Ljava/util/HashSet;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOcrPrerequisiteChecker;->checkerUtil:Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOcrPrerequisiteChecker;->allowedCpus:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->cpuAbiOK(Ljava/util/HashSet;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOcrPrerequisiteChecker;->checkerUtil:Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->cameraOK()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOcrPrerequisiteChecker;->checkerUtil:Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->orientationOk()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    const-string v0, "CreditCardPrerequisiteChecker"

    const-string v1, "All prerequisites OK."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    const/4 v0, 0x1

    .line 84
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
