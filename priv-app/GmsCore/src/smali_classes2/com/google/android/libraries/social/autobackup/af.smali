.class final Lcom/google/android/libraries/social/autobackup/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/libraries/social/autobackup/ad;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/social/autobackup/ad;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/af;->b:Lcom/google/android/libraries/social/autobackup/ad;

    iput-object p2, p0, Lcom/google/android/libraries/social/autobackup/af;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/af;->b:Lcom/google/android/libraries/social/autobackup/ad;

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/ad;->a(Lcom/google/android/libraries/social/autobackup/ad;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 271
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/af;->b:Lcom/google/android/libraries/social/autobackup/ad;

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/ad;->b(Lcom/google/android/libraries/social/autobackup/ad;)Lcom/google/android/libraries/social/autobackup/au;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/au;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 272
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 274
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    const/4 v3, 0x1

    invoke-direct {v0, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 275
    const-string v3, "bucket_id"

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/af;->a:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    const-string v3, "exclude_bucket"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 279
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/af;->a:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/google/android/libraries/social/autobackup/ab;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 281
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 283
    :try_start_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 285
    monitor-exit v1

    return-void

    .line 283
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 285
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method
