.class public final Lcom/google/android/libraries/social/l/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:I

.field private static b:I

.field private static c:Lcom/google/android/libraries/social/l/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    const/4 v0, -0x1

    sput v0, Lcom/google/android/libraries/social/l/a/a;->a:I

    .line 41
    new-instance v0, Lcom/google/android/libraries/social/l/a/b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/l/a/b;-><init>(B)V

    sput-object v0, Lcom/google/android/libraries/social/l/a/a;->c:Lcom/google/android/libraries/social/l/a/b;

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 172
    if-nez p0, :cond_0

    .line 180
    :goto_0
    return-object v6

    .line 175
    :cond_0
    if-nez p0, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 176
    sget v7, Lcom/google/android/libraries/social/l/a/a;->b:I

    sget-object v0, Lcom/google/android/libraries/social/l/a/a;->c:Lcom/google/android/libraries/social/l/a/b;

    const/4 v2, 0x2

    move-object v1, p0

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/libraries/social/l/a/b;->a(Ljava/lang/String;IIIILandroid/graphics/RectF;I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/google/android/libraries/social/l/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    move-object v6, v0

    goto :goto_0

    .line 175
    :cond_2
    sget-object v0, Lcom/google/android/libraries/social/l/a/a;->c:Lcom/google/android/libraries/social/l/a/b;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/l/a/b;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 177
    :cond_3
    const-string v0, "https"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "http"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 178
    :cond_4
    invoke-static {p0}, Lcom/google/android/libraries/social/l/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    :cond_5
    move-object v6, p0

    .line 180
    goto :goto_0
.end method
