.class public Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final blurDetector:Lcom/google/android/libraries/commerce/ocr/cv/BlurDetector;

.field private final cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

.field private final executorService:Lcom/google/android/libraries/commerce/ocr/util/Provider;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;Lcom/google/android/libraries/commerce/ocr/cv/BlurDetector;Lcom/google/android/libraries/commerce/ocr/util/Provider;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    .line 28
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;->blurDetector:Lcom/google/android/libraries/commerce/ocr/cv/BlurDetector;

    .line 29
    iput-object p3, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;->executorService:Lcom/google/android/libraries/commerce/ocr/util/Provider;

    .line 30
    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;Lcom/google/android/libraries/commerce/ocr/util/Provider;)V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;-><init>(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;Lcom/google/android/libraries/commerce/ocr/cv/BlurDetector;Lcom/google/android/libraries/commerce/ocr/util/Provider;)V

    .line 23
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;)Lcom/google/android/libraries/commerce/ocr/util/Provider;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;->executorService:Lcom/google/android/libraries/commerce/ocr/util/Provider;

    return-object v0
.end method


# virtual methods
.method public provideBlurDetector()Lcom/google/android/libraries/commerce/ocr/cv/BlurDetector;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;->blurDetector:Lcom/google/android/libraries/commerce/ocr/cv/BlurDetector;

    return-object v0
.end method

.method public provideCameraManager()Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    return-object v0
.end method

.method public provideExecutoryFactory()Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory;
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/a;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/a;-><init>(Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;)V

    return-object v0
.end method
