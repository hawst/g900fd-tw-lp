.class public Lcom/google/android/libraries/social/mediamonitor/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:J


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/libraries/social/mediamonitor/e;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 26
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/libraries/social/mediamonitor/c;->a:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/libraries/social/mediamonitor/d;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/mediamonitor/d;-><init>(B)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/mediamonitor/c;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/mediamonitor/e;)V

    .line 36
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/social/mediamonitor/e;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/libraries/social/mediamonitor/c;->b:Landroid/content/Context;

    .line 40
    iput-object p2, p0, Lcom/google/android/libraries/social/mediamonitor/c;->c:Lcom/google/android/libraries/social/mediamonitor/e;

    .line 41
    return-void
.end method


# virtual methods
.method final a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 44
    const-string v0, "com.google.android.libraries.social.mediamonitor.FORCE_RESCAN"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 45
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/mediamonitor/c;->a(Z)V

    .line 46
    return-void
.end method

.method final declared-synchronized a(Z)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 49
    monitor-enter p0

    :try_start_0
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/libraries/social/mediamonitor/c;->b:Landroid/content/Context;

    const-class v4, Lcom/google/android/libraries/social/mediamonitor/MediaMonitorIntentService;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 50
    iget-object v3, p0, Lcom/google/android/libraries/social/mediamonitor/c;->b:Landroid/content/Context;

    const/4 v4, 0x1

    const/high16 v5, 0x20000000

    invoke-static {v3, v4, v2, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 53
    if-eqz v3, :cond_0

    move v1, v0

    .line 55
    :cond_0
    if-eqz v1, :cond_5

    .line 56
    const-string v0, "MediaMonitor"

    const/4 v3, 0x2

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    const-string v0, "MediaMonitor"

    const-string v3, "Have service call pending"

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    :cond_1
    :goto_0
    if-nez v1, :cond_2

    .line 64
    iget-object v0, p0, Lcom/google/android/libraries/social/mediamonitor/c;->b:Landroid/content/Context;

    const-string v3, "alarm"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 65
    iget-object v3, p0, Lcom/google/android/libraries/social/mediamonitor/c;->b:Landroid/content/Context;

    const/4 v4, 0x1

    const/high16 v5, 0x10000000

    invoke-static {v3, v4, v2, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 67
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/libraries/social/mediamonitor/c;->c:Lcom/google/android/libraries/social/mediamonitor/e;

    invoke-interface {v5}, Lcom/google/android/libraries/social/mediamonitor/e;->a()J

    move-result-wide v6

    sget-wide v8, Lcom/google/android/libraries/social/mediamonitor/c;->a:J

    add-long/2addr v6, v8

    invoke-virtual {v0, v4, v6, v7, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 71
    :cond_2
    if-eqz v1, :cond_3

    if-eqz p1, :cond_4

    .line 72
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/mediamonitor/c;->b:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/google/android/libraries/social/mediamonitor/MediaMonitor;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 75
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/mediamonitor/c;->b:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/mediamonitor/b;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    monitor-exit p0

    return-void

    .line 59
    :cond_5
    :try_start_1
    const-string v0, "MediaMonitor"

    const/4 v3, 0x2

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    const-string v0, "MediaMonitor"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Scheduling MediaTrackerIntentService in "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-wide v4, Lcom/google/android/libraries/social/mediamonitor/c;->a:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " millis"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
