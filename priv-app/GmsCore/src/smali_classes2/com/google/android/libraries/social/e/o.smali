.class final Lcom/google/android/libraries/social/e/o;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final d:[I


# instance fields
.field private final a:I

.field private final b:Ljava/util/Map;

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/libraries/social/e/o;->d:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x4
    .end array-data
.end method

.method constructor <init>(I)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/e/o;->b:Ljava/util/Map;

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/social/e/o;->c:I

    .line 51
    iput p1, p0, Lcom/google/android/libraries/social/e/o;->a:I

    .line 52
    return-void
.end method

.method protected static a()[I
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/google/android/libraries/social/e/o;->d:[I

    return-object v0
.end method


# virtual methods
.method protected final a(Lcom/google/android/libraries/social/e/n;)Lcom/google/android/libraries/social/e/n;
    .locals 2

    .prologue
    .line 90
    iget v0, p0, Lcom/google/android/libraries/social/e/o;->a:I

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/e/n;->b(I)V

    .line 91
    iget-object v0, p0, Lcom/google/android/libraries/social/e/o;->b:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->b()S

    move-result v1

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/e/n;

    return-object v0
.end method

.method protected final a(S)Lcom/google/android/libraries/social/e/n;
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/libraries/social/e/o;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/e/n;

    return-object v0
.end method

.method protected final a(I)V
    .locals 0

    .prologue
    .line 116
    iput p1, p0, Lcom/google/android/libraries/social/e/o;->c:I

    .line 117
    return-void
.end method

.method protected final b(S)V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/libraries/social/e/o;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    return-void
.end method

.method protected final b()[Lcom/google/android/libraries/social/e/n;
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/libraries/social/e/o;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/e/o;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/libraries/social/e/n;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/libraries/social/e/n;

    return-object v0
.end method

.method protected final c()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/google/android/libraries/social/e/o;->a:I

    return v0
.end method

.method protected final d()I
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/libraries/social/e/o;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method protected final e()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lcom/google/android/libraries/social/e/o;->c:I

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 132
    if-ne p0, p1, :cond_0

    move v0, v1

    .line 154
    :goto_0
    return v0

    .line 135
    :cond_0
    if-nez p1, :cond_1

    move v0, v2

    .line 136
    goto :goto_0

    .line 138
    :cond_1
    instance-of v0, p1, Lcom/google/android/libraries/social/e/o;

    if-eqz v0, :cond_4

    .line 139
    check-cast p1, Lcom/google/android/libraries/social/e/o;

    .line 140
    iget v0, p1, Lcom/google/android/libraries/social/e/o;->a:I

    iget v3, p0, Lcom/google/android/libraries/social/e/o;->a:I

    if-ne v0, v3, :cond_4

    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/o;->d()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/e/o;->d()I

    move-result v3

    if-ne v0, v3, :cond_4

    .line 141
    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/o;->b()[Lcom/google/android/libraries/social/e/n;

    move-result-object v4

    .line 142
    array-length v5, v4

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    .line 143
    invoke-virtual {v6}, Lcom/google/android/libraries/social/e/n;->b()S

    move-result v0

    invoke-static {v0}, Lcom/google/android/libraries/social/e/c;->a(S)Z

    move-result v0

    if-nez v0, :cond_2

    .line 144
    iget-object v0, p0, Lcom/google/android/libraries/social/e/o;->b:Ljava/util/Map;

    invoke-virtual {v6}, Lcom/google/android/libraries/social/e/n;->b()S

    move-result v7

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/e/n;

    .line 147
    invoke-virtual {v6, v0}, Lcom/google/android/libraries/social/e/n;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 148
    goto :goto_0

    .line 142
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_3
    move v0, v1

    .line 151
    goto :goto_0

    :cond_4
    move v0, v2

    .line 154
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 161
    iget v0, p0, Lcom/google/android/libraries/social/e/o;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 163
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/libraries/social/e/o;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 164
    return v0
.end method
