.class public Lcom/google/android/libraries/commerce/ocr/credit/capture/processors/ProcessorModule;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final IDLE_KEEP_ALIVE_TIME_IN_MS:I = 0x2710

.field public static final MIN_BLUR_DETECTION_INTERVAL_IN_MS:I = 0xc8


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public provideCreditPipeline(Landroid/os/Bundle;Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory;Lcom/google/android/libraries/commerce/ocr/capture/InFocusFrameCheck;Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;
    .locals 5

    .prologue
    .line 46
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/capture/processors/CopyProcessor;

    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/credit/capture/processors/ProcessorModule;->provideFocusIntervalPolicy()Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/google/android/libraries/commerce/ocr/capture/processors/CopyProcessor;-><init>(Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;)V

    .line 47
    new-instance v1, Lcom/google/android/libraries/commerce/ocr/capture/processors/FocusProcessor;

    invoke-direct {v1, p5, p4}, Lcom/google/android/libraries/commerce/ocr/capture/processors/FocusProcessor;-><init>(Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;Lcom/google/android/libraries/commerce/ocr/capture/InFocusFrameCheck;)V

    .line 49
    new-instance v2, Lcom/google/android/libraries/commerce/ocr/capture/processors/OcrAsyncIntervalProcessor;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/credit/capture/processors/ProcessorModule;->provideOcrIntervalPolicy(Landroid/os/Bundle;)Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;

    move-result-object v3

    invoke-direct {v2, v3, p6, p7}, Lcom/google/android/libraries/commerce/ocr/capture/processors/OcrAsyncIntervalProcessor;-><init>(Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer;Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;)V

    .line 54
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline;->forSync(Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    move-result-object v0

    .line 55
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline;->wire(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;

    move-result-object v3

    invoke-virtual {p0, p3}, Lcom/google/android/libraries/commerce/ocr/credit/capture/processors/ProcessorModule;->provideFocusExecutor(Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline;->forAsync(Ljava/util/concurrent/ExecutorService;Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;->pipeTo(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;

    move-result-object v1

    invoke-virtual {p0, p3}, Lcom/google/android/libraries/commerce/ocr/credit/capture/processors/ProcessorModule;->provideOcrExecutor(Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline;->forAsync(Ljava/util/concurrent/ExecutorService;Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;->pipeTo(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;

    .line 58
    return-object v0
.end method

.method public provideFocusExecutor(Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory;)Ljava/util/concurrent/ExecutorService;
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 75
    const-wide/16 v4, 0x2710

    move-object v1, p1

    move v3, v2

    move v6, v2

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory;->create(IIJI)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method

.method public provideFocusIntervalPolicy()Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;
    .locals 2

    .prologue
    .line 66
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;-><init>(I)V

    return-object v0
.end method

.method public provideOcrExecutor(Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory;)Ljava/util/concurrent/ExecutorService;
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 82
    const-wide/16 v4, 0x2710

    move-object v1, p1

    move v3, v2

    move v6, v2

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory;->create(IIJI)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method

.method public provideOcrIntervalPolicy(Landroid/os/Bundle;)Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;
    .locals 2

    .prologue
    .line 70
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;

    invoke-static {p1}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrBundleModule;->provideMinPerformCreditCardOcrIntervalInMs(Landroid/os/Bundle;)I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;-><init>(I)V

    return-object v0
.end method
