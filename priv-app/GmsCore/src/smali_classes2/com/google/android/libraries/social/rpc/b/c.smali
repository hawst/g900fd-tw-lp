.class public final Lcom/google/android/libraries/social/rpc/b/c;
.super Lcom/google/android/libraries/social/rpc/o;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/rpc/b/a;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/libraries/social/rpc/b/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/libraries/social/rpc/b/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/rpc/o;-><init>(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p1}, Lcom/google/android/libraries/social/rpc/b/a;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/rpc/b/c;->b:Ljava/lang/String;

    .line 52
    iput-object p2, p0, Lcom/google/android/libraries/social/rpc/b/c;->a:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public static a(Ljava/lang/Exception;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 66
    instance-of v0, p0, Lcom/google/android/libraries/social/rpc/b/c;

    if-eqz v0, :cond_0

    .line 67
    check-cast p0, Lcom/google/android/libraries/social/rpc/b/c;

    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/b/c;->b:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    .line 71
    :goto_0
    return v0

    .line 68
    :cond_0
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/libraries/social/rpc/b/c;

    if-eqz v0, :cond_1

    .line 69
    invoke-virtual {p0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/rpc/b/c;

    iget-object v0, v0, Lcom/google/android/libraries/social/rpc/b/c;->b:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0

    .line 71
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
