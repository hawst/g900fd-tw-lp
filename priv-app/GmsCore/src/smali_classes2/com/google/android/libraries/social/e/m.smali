.class final Lcom/google/android/libraries/social/e/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/libraries/social/e/c;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/social/e/c;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/libraries/social/e/m;->a:Lcom/google/android/libraries/social/e/c;

    .line 38
    return-void
.end method


# virtual methods
.method protected final a(Ljava/io/InputStream;)Lcom/google/android/libraries/social/e/d;
    .locals 5

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/libraries/social/e/m;->a:Lcom/google/android/libraries/social/e/c;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/e/i;->a(Ljava/io/InputStream;Lcom/google/android/libraries/social/e/c;)Lcom/google/android/libraries/social/e/i;

    move-result-object v1

    .line 50
    new-instance v2, Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/i;->j()Ljava/nio/ByteOrder;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/libraries/social/e/d;-><init>(Ljava/nio/ByteOrder;)V

    .line 51
    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/i;->a()I

    move-result v0

    .line 54
    :goto_0
    const/4 v3, 0x5

    if-eq v0, v3, :cond_4

    .line 55
    packed-switch v0, :pswitch_data_0

    .line 91
    :goto_1
    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/i;->a()I

    move-result v0

    goto :goto_0

    .line 57
    :pswitch_0
    new-instance v0, Lcom/google/android/libraries/social/e/o;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/i;->d()I

    move-result v3

    invoke-direct {v0, v3}, Lcom/google/android/libraries/social/e/o;-><init>(I)V

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/social/e/d;->a(Lcom/google/android/libraries/social/e/o;)V

    goto :goto_1

    .line 60
    :pswitch_1
    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/i;->c()Lcom/google/android/libraries/social/e/n;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/n;->f()Z

    move-result v3

    if-nez v3, :cond_0

    .line 62
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/e/i;->a(Lcom/google/android/libraries/social/e/n;)V

    goto :goto_1

    .line 64
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/n;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/libraries/social/e/o;->a(Lcom/google/android/libraries/social/e/n;)Lcom/google/android/libraries/social/e/n;

    goto :goto_1

    .line 68
    :pswitch_2
    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/i;->c()Lcom/google/android/libraries/social/e/n;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/n;->c()S

    move-result v3

    const/4 v4, 0x7

    if-ne v3, v4, :cond_1

    .line 70
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/e/i;->b(Lcom/google/android/libraries/social/e/n;)V

    .line 72
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/n;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/libraries/social/e/o;->a(Lcom/google/android/libraries/social/e/n;)Lcom/google/android/libraries/social/e/n;

    goto :goto_1

    .line 75
    :pswitch_3
    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/i;->g()I

    move-result v0

    new-array v0, v0, [B

    .line 76
    array-length v3, v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/e/i;->a([B)I

    move-result v4

    if-ne v3, v4, :cond_2

    .line 77
    invoke-virtual {v2, v0}, Lcom/google/android/libraries/social/e/d;->a([B)V

    goto :goto_1

    .line 79
    :cond_2
    const-string v0, "ExifReader"

    const-string v3, "Failed to read the compressed thumbnail"

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 83
    :pswitch_4
    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/i;->f()I

    move-result v0

    new-array v0, v0, [B

    .line 84
    array-length v3, v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/e/i;->a([B)I

    move-result v4

    if-ne v3, v4, :cond_3

    .line 85
    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/i;->e()I

    move-result v3

    invoke-virtual {v2, v3, v0}, Lcom/google/android/libraries/social/e/d;->a(I[B)V

    goto :goto_1

    .line 87
    :cond_3
    const-string v0, "ExifReader"

    const-string v3, "Failed to read the strip bytes"

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 93
    :cond_4
    return-object v2

    .line 55
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
