.class public Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field private static a:Landroid/os/PowerManager$WakeLock;


# instance fields
.field private b:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    const-string v0, "Fingerprint Scanner"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 25
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 34
    invoke-static {p0}, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->c(Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 35
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 36
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 37
    return-void
.end method

.method private static declared-synchronized b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 46
    const-class v1, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->c(Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 48
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    :cond_0
    monitor-exit v1

    return-void

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized c(Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;
    .locals 4

    .prologue
    .line 53
    const-class v1, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->a:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    .line 54
    const-string v0, "power"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 55
    const/4 v2, 0x1

    const-string v3, "fingerprint_scanner_static"

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->a:Landroid/os/PowerManager$WakeLock;

    .line 57
    :cond_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->a:Landroid/os/PowerManager$WakeLock;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onCreate()V
    .locals 3

    .prologue
    .line 62
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 63
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 64
    const/4 v1, 0x1

    const-string v2, "fingerprint_scanner_local"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->b:Landroid/os/PowerManager$WakeLock;

    .line 65
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 77
    .line 80
    :try_start_0
    invoke-static {p0}, Lcom/google/android/libraries/social/autobackup/r;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/r;->a()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 84
    return-void

    .line 83
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 70
    invoke-super {p0, p1, p2}, Landroid/app/IntentService;->onStart(Landroid/content/Intent;I)V

    .line 71
    invoke-static {p0}, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->b(Landroid/content/Context;)V

    .line 72
    return-void
.end method
