.class public final Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Landroid/net/Uri;

.field public static final b:Landroid/net/Uri;

.field public static final c:[Landroid/net/Uri;

.field public static final d:[Ljava/lang/String;

.field private static final e:[Landroid/net/Uri;

.field private static final f:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 49
    const-string v0, "phoneStorage"

    invoke-static {v0}, Landroid/provider/MediaStore$Images$Media;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->a:Landroid/net/Uri;

    .line 51
    const-string v0, "phoneStorage"

    invoke-static {v0}, Landroid/provider/MediaStore$Video$Media;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->b:Landroid/net/Uri;

    .line 55
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->a:Landroid/net/Uri;

    aput-object v1, v0, v3

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->b:Landroid/net/Uri;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->c:[Landroid/net/Uri;

    .line 71
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->d:[Ljava/lang/String;

    .line 78
    new-array v0, v5, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v2

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->a:Landroid/net/Uri;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->e:[Landroid/net/Uri;

    .line 159
    const-string v0, "(\\d+)[xX](\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->f:Ljava/util/regex/Pattern;

    return-void
.end method

.method private static a(J)J
    .locals 2

    .prologue
    .line 210
    const-wide v0, 0x174876e800L

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    .line 213
    :goto_0
    return-wide p0

    :cond_0
    const-wide/16 v0, 0x3e8

    mul-long/2addr p0, v0

    goto :goto_0
.end method

.method public static a(Landroid/content/ContentResolver;Landroid/net/Uri;)J
    .locals 9

    .prologue
    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    .line 186
    invoke-static {p1}, Lcom/google/android/libraries/b/a/b;->b(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_1

    move-wide v0, v6

    .line 202
    :cond_0
    :goto_0
    return-wide v0

    .line 192
    :cond_1
    :try_start_0
    sget-object v2, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->d:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 194
    if-eqz v2, :cond_2

    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 195
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v0

    .line 198
    if-eqz v2, :cond_0

    .line 199
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 198
    :cond_2
    if-eqz v2, :cond_3

    .line 199
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_3
    move-wide v0, v6

    .line 202
    goto :goto_0

    .line 198
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_1
    if-eqz v1, :cond_4

    .line 199
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 198
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;IJ)Ljava/lang/String;
    .locals 2

    .prologue
    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "~"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "~"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "~"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 254
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "image/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "video/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/ContentResolver;Landroid/net/Uri;)J
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 225
    const-string v0, "datetaken"

    invoke-static {p0, p1, v0}, Lcom/google/android/libraries/social/autobackup/util/b;->a(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    .line 227
    cmp-long v2, v0, v4

    if-lez v2, :cond_0

    .line 228
    invoke-static {v0, v1}, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->a(J)J

    move-result-wide v0

    .line 243
    :goto_0
    return-wide v0

    .line 231
    :cond_0
    const-string v0, "date_added"

    invoke-static {p0, p1, v0}, Lcom/google/android/libraries/social/autobackup/util/b;->a(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    .line 233
    cmp-long v2, v0, v4

    if-lez v2, :cond_1

    .line 234
    invoke-static {v0, v1}, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->a(J)J

    move-result-wide v0

    goto :goto_0

    .line 237
    :cond_1
    const-string v0, "date_modified"

    invoke-static {p0, p1, v0}, Lcom/google/android/libraries/social/autobackup/util/b;->a(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    .line 239
    cmp-long v2, v0, v4

    if-lez v2, :cond_2

    .line 240
    invoke-static {v0, v1}, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->a(J)J

    move-result-wide v0

    goto :goto_0

    .line 243
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->a(J)J

    move-result-wide v0

    goto :goto_0
.end method
