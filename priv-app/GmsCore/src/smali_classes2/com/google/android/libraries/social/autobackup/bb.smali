.class final Lcom/google/android/libraries/social/autobackup/bb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/account/j;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    const-string v0, "com.google.android.libraries.social.autobackup.VideoReuploader-MigrateToAccountSpecificUploadWindow"

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/libraries/social/account/f;Lcom/google/android/libraries/social/account/c;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 129
    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/AutoBackupProvider;->c(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 130
    const-string v1, "video_upload_redo_window:start_time_seconds"

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 131
    const-string v1, "video_upload_redo_window:end_time_seconds"

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 132
    const-string v4, "com.google.android.libraries.social.autobackup.VideoReuploader"

    invoke-interface {p2, v4}, Lcom/google/android/libraries/social/account/f;->h(Ljava/lang/String;)Lcom/google/android/libraries/social/account/f;

    move-result-object v4

    .line 133
    const-string v5, "window_start_time_seconds"

    invoke-interface {v4, v5, v2, v3}, Lcom/google/android/libraries/social/account/f;->b(Ljava/lang/String;J)Lcom/google/android/libraries/social/account/f;

    .line 134
    const-string v2, "window_end_time_seconds"

    invoke-interface {v4, v2, v0, v1}, Lcom/google/android/libraries/social/account/f;->b(Ljava/lang/String;J)Lcom/google/android/libraries/social/account/f;

    .line 135
    return-void
.end method
