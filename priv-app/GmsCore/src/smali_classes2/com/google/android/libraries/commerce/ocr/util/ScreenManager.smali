.class public Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final display:Landroid/view/Display;

.field private final sysConfig:Landroid/content/res/Configuration;


# direct methods
.method public constructor <init>(Landroid/content/res/Configuration;Landroid/view/Display;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;->sysConfig:Landroid/content/res/Configuration;

    .line 18
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;->display:Landroid/view/Display;

    .line 19
    return-void
.end method


# virtual methods
.method public getDisplayRotation()I
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;->display:Landroid/view/Display;

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    return v0
.end method

.method public getScreenOrientation()I
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;->sysConfig:Landroid/content/res/Configuration;

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 37
    packed-switch v0, :pswitch_data_0

    .line 45
    :goto_0
    :pswitch_0
    return v0

    .line 40
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;->display:Landroid/view/Display;

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;->display:Landroid/view/Display;

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 41
    const/4 v0, 0x1

    goto :goto_0

    .line 43
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 37
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getSystemOrientation()I
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;->sysConfig:Landroid/content/res/Configuration;

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    return v0
.end method

.method public isInPortraitMode()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 56
    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;->getScreenOrientation()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
