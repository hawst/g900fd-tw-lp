.class public final Lcom/google/android/libraries/social/e/r;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:J

.field final b:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-wide p1, p0, Lcom/google/android/libraries/social/e/r;->a:J

    .line 39
    iput-wide p3, p0, Lcom/google/android/libraries/social/e/r;->b:J

    .line 40
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 74
    if-nez p1, :cond_1

    .line 84
    :cond_0
    :goto_0
    return v0

    .line 77
    :cond_1
    if-ne p0, p1, :cond_2

    move v0, v1

    .line 78
    goto :goto_0

    .line 80
    :cond_2
    instance-of v2, p1, Lcom/google/android/libraries/social/e/r;

    if-eqz v2, :cond_0

    .line 81
    check-cast p1, Lcom/google/android/libraries/social/e/r;

    .line 82
    iget-wide v2, p0, Lcom/google/android/libraries/social/e/r;->a:J

    iget-wide v4, p1, Lcom/google/android/libraries/social/e/r;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/libraries/social/e/r;->b:J

    iget-wide v4, p1, Lcom/google/android/libraries/social/e/r;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 91
    iget-wide v0, p0, Lcom/google/android/libraries/social/e/r;->a:J

    iget-wide v2, p0, Lcom/google/android/libraries/social/e/r;->a:J

    ushr-long/2addr v2, v6

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    .line 93
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/libraries/social/e/r;->b:J

    iget-wide v4, p0, Lcom/google/android/libraries/social/e/r;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 94
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p0, Lcom/google/android/libraries/social/e/r;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/libraries/social/e/r;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
