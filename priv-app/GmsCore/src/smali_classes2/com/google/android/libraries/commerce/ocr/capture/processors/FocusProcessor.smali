.class public Lcom/google/android/libraries/commerce/ocr/capture/processors/FocusProcessor;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;


# static fields
.field private static final TAG:Ljava/lang/String; = "FocusProcessor"


# instance fields
.field private final inFocusFrameCheck:Lcom/google/android/libraries/commerce/ocr/capture/InFocusFrameCheck;

.field private final roiProvider:Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;Lcom/google/android/libraries/commerce/ocr/capture/InFocusFrameCheck;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/FocusProcessor;->roiProvider:Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;

    .line 29
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/FocusProcessor;->inFocusFrameCheck:Lcom/google/android/libraries/commerce/ocr/capture/InFocusFrameCheck;

    .line 30
    return-void
.end method


# virtual methods
.method public discard(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)V
    .locals 0

    .prologue
    .line 49
    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->recycle()V

    .line 50
    return-void
.end method

.method public bridge synthetic discard(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/FocusProcessor;->discard(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)V

    return-void
.end method

.method public isProcessingNeeded(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)Z
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic isProcessingNeeded(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 19
    check-cast p1, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/FocusProcessor;->isProcessingNeeded(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)Z

    move-result v0

    return v0
.end method

.method public process(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;
    .locals 3

    .prologue
    .line 39
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/FocusProcessor;->inFocusFrameCheck:Lcom/google/android/libraries/commerce/ocr/capture/InFocusFrameCheck;

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/FocusProcessor;->roiProvider:Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/libraries/commerce/ocr/capture/InFocusFrameCheck;->isGoodAndTriggerAutofocus(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/FocusProcessor;->discard(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)V

    .line 42
    const/4 p1, 0x0

    .line 44
    :cond_0
    return-object p1
.end method

.method public bridge synthetic process(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    check-cast p1, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/FocusProcessor;->process(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    move-result-object v0

    return-object v0
.end method

.method public shutdown()V
    .locals 2

    .prologue
    .line 54
    const-string v0, "FocusProcessor"

    const-string v1, "Stop trying to focus"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    return-void
.end method
