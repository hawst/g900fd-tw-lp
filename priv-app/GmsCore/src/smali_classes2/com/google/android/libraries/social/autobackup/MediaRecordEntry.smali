.class public Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
.super Lcom/google/android/libraries/social/g/a/a;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/libraries/social/g/a/c;
    a = "media_record"
.end annotation


# static fields
.field public static final a:Lcom/google/android/libraries/social/g/a/d;


# instance fields
.field private c:Ljava/lang/Throwable;

.field private mAlbumId:Ljava/lang/String;
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "album_id"
    .end annotation
.end field

.field private mAllowFullRes:Z
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "allow_full_res"
        d = false
        e = "1"
    .end annotation
.end field

.field private mBucketId:Ljava/lang/String;
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "bucket_id"
    .end annotation
.end field

.field private mBytesTotal:J
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "bytes_total"
        d = false
        e = "-1"
    .end annotation
.end field

.field private mBytesUploaded:J
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "bytes_uploaded"
    .end annotation
.end field

.field private mEventId:Ljava/lang/String;
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "event_id"
    .end annotation
.end field

.field private mFingerprint:Ljava/lang/String;
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "fingerprint"
    .end annotation
.end field

.field private mFromCamera:Z
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "from_camera"
        d = false
        e = "0"
    .end annotation
.end field

.field private mIsImage:Z
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "is_image"
        d = false
        e = "1"
    .end annotation
.end field

.field private mMediaHash:J
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "media_hash"
        d = false
    .end annotation
.end field

.field private mMediaId:J
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "media_id"
        b = true
        d = false
    .end annotation
.end field

.field private mMediaTime:J
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "media_time"
        d = false
    .end annotation
.end field

.field private mMediaUrl:Ljava/lang/String;
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "media_url"
        d = false
    .end annotation
.end field

.field private mMimeType:Ljava/lang/String;
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "mime_type"
    .end annotation
.end field

.field private mRawComponentName:Ljava/lang/String;
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "component_name"
    .end annotation
.end field

.field private mResumeToken:Ljava/lang/String;
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "resume_token"
    .end annotation
.end field

.field private mRetryEndTime:J
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "retry_end_time"
        d = false
        e = "0"
    .end annotation
.end field

.field private mUploadAccountId:I
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "upload_account_id"
        d = false
        e = "-1"
    .end annotation
.end field

.field private mUploadError:Ljava/lang/String;
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "upload_error"
    .end annotation
.end field

.field private mUploadFinishTime:J
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "upload_finish_time"
        d = false
        e = "0"
    .end annotation
.end field

.field private mUploadId:J
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "upload_id"
    .end annotation
.end field

.field private mUploadReason:I
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "upload_reason"
        d = false
        e = "0"
    .end annotation
.end field

.field private mUploadState:I
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "upload_state"
        d = false
        e = "500"
    .end annotation
.end field

.field private mUploadStatus:I
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "upload_status"
        d = false
        e = "0"
    .end annotation
.end field

.field private mUploadTaskState:I
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "upload_task_state"
    .end annotation
.end field

.field private mUploadTime:J
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "upload_time"
    .end annotation
.end field

.field private mUploadUrl:Ljava/lang/String;
    .annotation runtime Lcom/google/android/libraries/social/g/a/b;
        a = "upload_url"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lcom/google/android/libraries/social/g/a/d;

    const-class v1, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/g/a/d;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 358
    invoke-direct {p0}, Lcom/google/android/libraries/social/g/a/a;-><init>()V

    .line 309
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadAccountId:I

    .line 358
    return-void
.end method

.method static a(Landroid/content/ContentValues;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 2

    .prologue
    .line 263
    sget-object v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    new-instance v1, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-direct {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;-><init>()V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/libraries/social/g/a/d;->a(Landroid/content/ContentValues;Lcom/google/android/libraries/social/g/a/a;)Lcom/google/android/libraries/social/g/a/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    return-object v0
.end method

.method public static a(Landroid/database/Cursor;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 2

    .prologue
    .line 232
    sget-object v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    new-instance v1, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-direct {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;-><init>()V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/libraries/social/g/a/d;->a(Landroid/database/Cursor;Lcom/google/android/libraries/social/g/a/a;)Lcom/google/android/libraries/social/g/a/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    return-object v0
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 3

    .prologue
    .line 240
    new-instance v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-direct {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;-><init>()V

    .line 241
    sget-object v1, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-virtual {v1, p0, p1, p2, v0}, Lcom/google/android/libraries/social/g/a/d;->a(Landroid/database/sqlite/SQLiteDatabase;JLcom/google/android/libraries/social/g/a/a;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 249
    sget-object v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/g/a/d;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/g/a/d;->b()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "media_url = ? AND upload_account_id = -1"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 252
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    sget-object v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    new-instance v2, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-direct {v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/g/a/d;->a(Landroid/database/Cursor;Lcom/google/android/libraries/social/g/a/a;)Lcom/google/android/libraries/social/g/a/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 257
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v0

    .line 255
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v5

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 361
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mMediaId:J

    return-wide v0
.end method

.method public final a(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 0

    .prologue
    .line 491
    iput p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadReason:I

    .line 492
    return-object p0
.end method

.method public final a(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 1

    .prologue
    .line 516
    iput-wide p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadTime:J

    .line 517
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 0

    .prologue
    .line 481
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mAlbumId:Ljava/lang/String;

    .line 482
    return-object p0
.end method

.method public final a(Ljava/lang/Throwable;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 0

    .prologue
    .line 506
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c:Ljava/lang/Throwable;

    .line 507
    return-object p0
.end method

.method public final b(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 0

    .prologue
    .line 496
    iput p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadState:I

    .line 497
    return-object p0
.end method

.method public final b(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 1

    .prologue
    .line 536
    iput-wide p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mBytesTotal:J

    .line 537
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 0

    .prologue
    .line 531
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mFingerprint:Ljava/lang/String;

    .line 532
    return-object p0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mMediaUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Landroid/content/ContentValues;)V
    .locals 1

    .prologue
    .line 616
    sget-object v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-virtual {v0, p1, p0}, Lcom/google/android/libraries/social/g/a/d;->a(Landroid/content/ContentValues;Lcom/google/android/libraries/social/g/a/a;)Lcom/google/android/libraries/social/g/a/a;

    .line 617
    return-void
.end method

.method public final c(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 0

    .prologue
    .line 501
    iput p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadStatus:I

    .line 502
    return-object p0
.end method

.method public final c(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 1

    .prologue
    .line 541
    iput-wide p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mBytesUploaded:J

    .line 542
    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 0

    .prologue
    .line 551
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadUrl:Ljava/lang/String;

    .line 552
    return-object p0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 377
    iget-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mIsImage:Z

    return v0
.end method

.method public final d(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 0

    .prologue
    .line 511
    iput p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadTaskState:I

    .line 512
    return-object p0
.end method

.method public final d(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 1

    .prologue
    .line 546
    iput-wide p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadId:J

    .line 547
    return-object p0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mAlbumId:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 566
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mResumeToken:Ljava/lang/String;

    .line 567
    return-void
.end method

.method public final e(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 0

    .prologue
    .line 526
    iput p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadAccountId:I

    .line 527
    return-object p0
.end method

.method public final e(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 1

    .prologue
    .line 556
    iput-wide p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mRetryEndTime:J

    .line 557
    return-object p0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mResumeToken:Ljava/lang/String;

    return-object v0
.end method

.method final e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 570
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mMimeType:Ljava/lang/String;

    .line 571
    return-void
.end method

.method public final f(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 1

    .prologue
    .line 561
    iput-wide p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadFinishTime:J

    .line 562
    return-object p0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mEventId:Ljava/lang/String;

    return-object v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 574
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mMediaUrl:Ljava/lang/String;

    .line 575
    return-void
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 405
    iget v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadReason:I

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 417
    iget v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadTaskState:I

    return v0
.end method

.method public final i()J
    .locals 2

    .prologue
    .line 421
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadTime:J

    return-wide v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 429
    iget v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadAccountId:I

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 433
    iget-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mAllowFullRes:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 437
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mFingerprint:Ljava/lang/String;

    return-object v0
.end method

.method public final m()J
    .locals 2

    .prologue
    .line 441
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mBytesTotal:J

    return-wide v0
.end method

.method public final n()J
    .locals 2

    .prologue
    .line 445
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mBytesUploaded:J

    return-wide v0
.end method

.method public final o()J
    .locals 2

    .prologue
    .line 449
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadId:J

    return-wide v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 453
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final q()J
    .locals 2

    .prologue
    .line 457
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mRetryEndTime:J

    return-wide v0
.end method

.method public final r()J
    .locals 2

    .prologue
    .line 461
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadFinishTime:J

    return-wide v0
.end method

.method final s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method public final t()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 582
    iget v1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadTaskState:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadTaskState:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    .line 621
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "media_id"

    aput-object v4, v3, v1

    const/4 v4, 0x1

    const-string v5, "album_id"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "event_id"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "upload_account_id"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "upload_url"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "bytes_total"

    aput-object v5, v3, v4

    invoke-virtual {v2, p0, v3}, Lcom/google/android/libraries/social/g/a/d;->a(Lcom/google/android/libraries/social/g/a/a;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v0, " {"

    iget v3, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadReason:I

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->w()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadState:I

    sparse-switch v0, :sswitch_data_0

    const-string v0, "Unknown"

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadStatus:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-string v0, "Unknown"

    :goto_1
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget v3, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadTaskState:I

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadTaskState:I

    packed-switch v0, :pswitch_data_1

    :pswitch_1
    const-string v0, "Unknown"

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "}"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mBytesTotal:J

    cmp-long v0, v4, v6

    if-eqz v0, :cond_2

    iget-wide v4, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mBytesUploaded:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_3

    :cond_2
    move v0, v1

    :goto_3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :sswitch_0
    const-string v0, "queued"

    goto :goto_0

    :sswitch_1
    const-string v0, "pending"

    goto/16 :goto_0

    :sswitch_2
    const-string v0, "failed"

    goto/16 :goto_0

    :sswitch_3
    const-string v0, "done"

    goto/16 :goto_0

    :sswitch_4
    const-string v0, "don\'t upload"

    goto/16 :goto_0

    :pswitch_2
    const-string v0, "ok"

    goto :goto_1

    :pswitch_3
    const-string v0, "in progress"

    goto/16 :goto_1

    :pswitch_4
    const-string v0, "stalled"

    goto/16 :goto_1

    :pswitch_5
    const-string v0, "no wifi"

    goto/16 :goto_1

    :pswitch_6
    const-string v0, "roaming"

    goto/16 :goto_1

    :pswitch_7
    const-string v0, "no power"

    goto/16 :goto_1

    :pswitch_8
    const-string v0, "upsync disabled"

    goto/16 :goto_1

    :pswitch_9
    const-string v0, "downsync disabled"

    goto/16 :goto_1

    :pswitch_a
    const-string v0, "background disabled"

    goto/16 :goto_1

    :pswitch_b
    const-string v0, "yielded"

    goto/16 :goto_1

    :pswitch_c
    const-string v0, "user auth"

    goto/16 :goto_1

    :pswitch_d
    const-string v0, "no storage"

    goto/16 :goto_1

    :pswitch_e
    const-string v0, "no network"

    goto/16 :goto_1

    :pswitch_f
    const-string v0, "network exception"

    goto/16 :goto_1

    :pswitch_10
    const-string v0, "FAIL quota"

    goto/16 :goto_1

    :pswitch_11
    const-string v0, "FAIL user auth"

    goto/16 :goto_1

    :pswitch_12
    const-string v0, "FAIL no storage"

    goto/16 :goto_1

    :pswitch_13
    const-string v0, "FAIL invalid metadata"

    goto/16 :goto_1

    :pswitch_14
    const-string v0, "FAIL duplicate"

    goto/16 :goto_1

    :pswitch_15
    const-string v0, "FAIL no fingerprint"

    goto/16 :goto_1

    :pswitch_16
    const-string v0, "FAIL disabled"

    goto/16 :goto_1

    :pswitch_17
    const-string v0, "FAIL google exif"

    goto/16 :goto_1

    :pswitch_18
    const-string v0, "FAIL skipped"

    goto/16 :goto_1

    :pswitch_19
    const-string v0, "FAIL cancelled"

    goto/16 :goto_1

    :pswitch_1a
    const-string v0, "FAIL exceed retry time"

    goto/16 :goto_1

    :pswitch_1b
    const-string v0, "FAIL media gone"

    goto/16 :goto_1

    :pswitch_1c
    const-string v0, "uploading"

    goto/16 :goto_2

    :pswitch_1d
    const-string v0, "queued"

    goto/16 :goto_2

    :pswitch_1e
    const-string v0, "completed"

    goto/16 :goto_2

    :pswitch_1f
    const-string v0, "failed"

    goto/16 :goto_2

    :pswitch_20
    const-string v0, "being stalled"

    goto/16 :goto_2

    :pswitch_21
    const-string v0, "being cancelled"

    goto/16 :goto_2

    :pswitch_22
    const-string v0, "cancelled"

    goto/16 :goto_2

    :pswitch_23
    const-string v0, "unauthorized"

    goto/16 :goto_2

    :pswitch_24
    const-string v0, "quota exceeded"

    goto/16 :goto_2

    :pswitch_25
    const-string v0, "skipped"

    goto/16 :goto_2

    :pswitch_26
    const-string v0, "duplicate"

    goto/16 :goto_2

    :cond_3
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mBytesUploaded:J

    long-to-float v0, v0

    iget-wide v4, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mBytesTotal:J

    long-to-float v1, v4

    div-float/2addr v0, v1

    float-to-double v0, v0

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    const/16 v1, 0x64

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto/16 :goto_3

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1c
        :pswitch_1
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
    .end packed-switch
.end method

.method public final u()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 587
    iget v1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadTaskState:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadTaskState:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final v()Z
    .locals 4

    .prologue
    .line 596
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mBytesUploaded:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final w()Ljava/lang/String;
    .locals 1

    .prologue
    .line 643
    iget v0, p0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->mUploadReason:I

    sparse-switch v0, :sswitch_data_0

    .line 653
    const-string v0, "Unknown"

    :goto_0
    return-object v0

    .line 645
    :sswitch_0
    const-string v0, "InstantUpload"

    goto :goto_0

    .line 647
    :sswitch_1
    const-string v0, "InstantShare"

    goto :goto_0

    .line 649
    :sswitch_2
    const-string v0, "UploadAll"

    goto :goto_0

    .line 651
    :sswitch_3
    const-string v0, "Manual"

    goto :goto_0

    .line 643
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_3
        0x14 -> :sswitch_1
        0x1e -> :sswitch_0
        0x28 -> :sswitch_2
    .end sparse-switch
.end method
