.class public final Lcom/google/android/libraries/social/autobackup/av;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final c:Lcom/google/android/libraries/social/f/a;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 379
    new-instance v0, Lcom/google/android/libraries/social/f/a;

    const-string v1, "botched_gallery3_refactoring"

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/f/a;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/av;->c:Lcom/google/android/libraries/social/f/a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/av;->a:Landroid/content/Context;

    .line 39
    iput-object p2, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    .line 40
    return-void
.end method

.method private final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 366
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->a:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/google/android/libraries/social/autobackup/u;->a(Landroid/content/Context;I)[Lcom/google/android/libraries/social/autobackup/w;

    move-result-object v1

    .line 369
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 370
    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v1, v0

    .line 371
    iget-object v5, v4, Lcom/google/android/libraries/social/autobackup/w;->a:Ljava/lang/Integer;

    if-eqz v5, :cond_0

    .line 372
    const-string v5, "bucket_id"

    iget-object v4, v4, Lcom/google/android/libraries/social/autobackup/w;->a:Ljava/lang/Integer;

    invoke-virtual {v2, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 373
    const-string v4, "local_folders"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 370
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 376
    :cond_1
    return-void
.end method

.method private b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 449
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/libraries/social/account/b;

    invoke-static {v0, v2}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    .line 451
    const-string v2, "ALTER TABLE media_record ADD COLUMN upload_account_id INTEGER NOT NULL DEFAULT -1"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 454
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 457
    :try_start_0
    const-string v3, "SELECT DISTINCT upload_account, plus_page_id FROM media_record"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 459
    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 460
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 461
    new-instance v3, Lcom/google/android/libraries/social/autobackup/aw;

    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v3, p0, v4, v5, v6}, Lcom/google/android/libraries/social/autobackup/aw;-><init>(Lcom/google/android/libraries/social/autobackup/av;Ljava/lang/String;Ljava/lang/String;B)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 466
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 467
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 466
    :cond_2
    if-eqz v1, :cond_3

    .line 467
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 471
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/social/autobackup/aw;

    .line 472
    iget-object v3, v1, Lcom/google/android/libraries/social/autobackup/aw;->b:Ljava/lang/String;

    .line 473
    iget-object v1, v1, Lcom/google/android/libraries/social/autobackup/aw;->a:Ljava/lang/String;

    .line 474
    invoke-interface {v0, v1, v3}, Lcom/google/android/libraries/social/account/b;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 476
    const/4 v5, -0x1

    if-ne v4, v5, :cond_6

    .line 477
    const-string v4, "UploadsDatabaseUpgrader"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 478
    const-string v4, "UploadsDatabaseUpgrader"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Could not locate account id when upgrading accountName="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/libraries/social/g/a/f;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", plusPageId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Lcom/google/android/libraries/social/g/a/f;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    :cond_4
    if-nez v3, :cond_5

    .line 483
    const-string v3, "DELETE FROM media_record WHERE upload_account = ? AND plus_page_id IS NULL"

    new-array v4, v8, [Ljava/lang/Object;

    aput-object v1, v4, v7

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 487
    :cond_5
    const-string v4, "DELETE FROM media_record WHERE upload_account = ? AND plus_page_id = ?"

    new-array v5, v9, [Ljava/lang/Object;

    aput-object v1, v5, v7

    aput-object v3, v5, v8

    invoke-virtual {p1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 492
    :cond_6
    if-nez v3, :cond_7

    .line 493
    const-string v3, "UPDATE media_record SET upload_account_id = ? WHERE upload_account = ?"

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v7

    aput-object v1, v5, v8

    invoke-virtual {p1, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 496
    :cond_7
    const-string v5, "UPDATE media_record SET upload_account_id = ? WHERE upload_account = ? AND plus_page_id = ?"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v7

    aput-object v1, v6, v8

    aput-object v3, v6, v9

    invoke-virtual {p1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 502
    :cond_8
    return-void
.end method

.method private c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x3

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 509
    const-string v2, "local_folder_auto_backup"

    .line 511
    const-string v3, "UploadsDatabaseUpgrader"

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 512
    const-string v3, "UploadsDatabaseUpgrader"

    const-string v4, "upgrading to version 33"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 515
    :cond_0
    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/av;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/libraries/social/autobackup/aj;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v3

    .line 517
    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/av;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v3, v2}, Lcom/google/android/libraries/social/autobackup/util/b;->a(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    move v0, v1

    .line 520
    :cond_1
    if-eqz v0, :cond_5

    .line 524
    const-string v0, "SELECT local_folders.bucket_id, exclude_bucket.bucket_id FROM local_folders LEFT JOIN exclude_bucket ON local_folders.bucket_id = exclude_bucket.bucket_id"

    .line 528
    invoke-virtual {p1, v0, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 531
    if-eqz v2, :cond_5

    .line 534
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 535
    :cond_2
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 536
    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    .line 537
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 541
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 543
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 544
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3, v1}, Landroid/content/ContentValues;-><init>(I)V

    .line 545
    const-string v4, "bucket_id"

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    const-string v4, "exclude_bucket"

    invoke-virtual {p1, v4, v7, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 547
    const-string v3, "UploadsDatabaseUpgrader"

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 548
    const-string v3, "UploadsDatabaseUpgrader"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "inserted new excluded bucket "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 553
    :cond_5
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 12

    .prologue
    .line 43
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/AutoBackupProvider;->a(Landroid/content/Context;)V

    .line 47
    :cond_0
    const/4 v0, 0x6

    if-ge p1, v0, :cond_1

    .line 50
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "DROP TABLE media_map"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "DROP TABLE upload_records"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    :cond_1
    :goto_0
    const/4 v0, 0x7

    if-ge p1, v0, :cond_2

    .line 58
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 60
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "media_scanner.external.photo.last_media_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "media_scanner.phoneStorage.photo.last_media_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "media_scanner.external.video.last_media_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "media_scanner.phoneStorage.video.last_media_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 67
    :cond_2
    const/16 v0, 0xd

    if-ge p1, v0, :cond_17

    .line 68
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE media_tracker (_id INTEGER PRIMARY KEY, volume_name TEXT NOT NULL, media_type TEXT NOT NULL,bucket_id TEXT, bucket_name TEXT, last_media_id INTEGER)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/av;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "media_scanner.external.photo.last_media_id"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    const-string v4, "media_scanner.phoneStorage.photo.last_media_id"

    const-wide/16 v6, 0x0

    invoke-interface {v1, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    const-string v6, "media_scanner.external.video.last_media_id"

    const-wide/16 v8, 0x0

    invoke-interface {v1, v6, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    const-string v8, "media_scanner.phoneStorage.video.last_media_id"

    const-wide/16 v10, 0x0

    invoke-interface {v1, v8, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    new-instance v10, Landroid/content/ContentValues;

    const/4 v11, 0x5

    invoke-direct {v10, v11}, Landroid/content/ContentValues;-><init>(I)V

    const-string v11, "bucket_id"

    invoke-virtual {v10, v11}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v11, "bucket_name"

    invoke-virtual {v10, v11}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v11, "last_media_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v10, v11, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "volume_name"

    const-string v3, "external"

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "media_type"

    const-string v3, "photo"

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "media_tracker"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v10}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const-string v2, "last_media_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "volume_name"

    const-string v3, "phoneStorage"

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "media_type"

    const-string v3, "photo"

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "media_tracker"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v10}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const-string v2, "last_media_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "volume_name"

    const-string v3, "external"

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "media_type"

    const-string v3, "video"

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "media_tracker"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v10}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const-string v2, "last_media_id"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "volume_name"

    const-string v3, "phoneStorage"

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "media_type"

    const-string v3, "video"

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "media_tracker"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v10}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "media_scanner.external.photo.last_media_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "media_scanner.phoneStorage.photo.last_media_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "media_scanner.external.video.last_media_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "media_scanner.phoneStorage.video.last_media_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 69
    const/16 v0, 0xd

    .line 72
    :goto_1
    const/16 v1, 0xe

    if-ge v0, v1, :cond_3

    .line 73
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_1
    const-string v0, "ALTER TABLE media_tracker RENAME TO tmp_table"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE media_tracker (_id INTEGER PRIMARY KEY, volume_name TEXT NOT NULL, media_type TEXT NOT NULL,bucket_id TEXT, last_media_id INTEGER NOT NULL DEFAULT(0))"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "INSERT INTO media_tracker(_id, volume_name, media_type, bucket_id) SELECT _id, volume_name, media_type, bucket_id FROM tmp_table;"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE tmp_table;"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 74
    const/16 v0, 0xe

    .line 77
    :cond_3
    const/16 v1, 0xf

    if-ge v0, v1, :cond_4

    .line 78
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    .line 79
    const/16 v0, 0xf

    .line 82
    :cond_4
    const/16 v1, 0x10

    if-ge v0, v1, :cond_5

    .line 83
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/AutoBackupProvider;->b(Landroid/content/Context;)V

    .line 84
    const/16 v0, 0x10

    .line 87
    :cond_5
    const/16 v1, 0x11

    if-ge v0, v1, :cond_6

    .line 88
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE fingerprints (_id INTEGER PRIMARY KEY AUTOINCREMENT,content_uri TEXT,fingerprint TEXT,image_url TEXT,owner_id TEXT,photo_id INTEGER NOT NULL DEFAULT 0)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE INDEX fingerprints_index_content_uri ON fingerprints (content_uri)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 89
    const/16 v0, 0x11

    .line 92
    :cond_6
    const/16 v1, 0x12

    if-ge v0, v1, :cond_7

    .line 93
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->a:Landroid/content/Context;

    const-string v1, "iu.picasa.db"

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z

    .line 94
    const/16 v0, 0x12

    .line 97
    :cond_7
    const/16 v1, 0x13

    if-ge v0, v1, :cond_8

    .line 98
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_2
    const-string v0, "DROP TABLE IF EXISTS upload_tasks"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS media_record_index_media_id"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE media_record RENAME TO tmp_table"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE media_record (_id INTEGER PRIMARY KEY AUTOINCREMENT,album_id TEXT,bucket_id TEXT,upload_url TEXT,upload_error TEXT,event_id TEXT,fingerprint TEXT,upload_account TEXT,component_name TEXT,plus_page_id TEXT,mime_type TEXT,media_url TEXT NOT NULL,media_time INTEGER NOT NULL,media_id INTEGER NOT NULL,media_hash INTEGER NOT NULL,bytes_total INTEGER NOT NULL DEFAULT -1,retry_end_time INTEGER NOT NULL DEFAULT 0,upload_time INTEGER,bytes_uploaded INTEGER,upload_finish_time INTEGER NOT NULL DEFAULT 0,upload_id INTEGER,upload_reason INTEGER NOT NULL DEFAULT 0,upload_state INTEGER NOT NULL DEFAULT 500,upload_task_state INTEGER,from_camera INTEGER NOT NULL DEFAULT 0,is_image INTEGER NOT NULL DEFAULT 1)"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX media_record_index_media_id ON media_record (media_id)"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "INSERT INTO media_record(_id,album_id,upload_error,event_id,upload_account,plus_page_id,media_url,media_time,media_id,media_hash,bytes_total,retry_end_time,upload_time,bytes_uploaded,upload_finish_time,upload_id,upload_reason,upload_state,from_camera,is_image) SELECT _id,album_id,upload_error,event_id,upload_account,plus_page_id,media_url,media_time,media_id,media_hash,bytes_total,retry_end_time,upload_time,bytes_uploaded,upload_finish_time,upload_id,upload_reason,upload_state,from_camera,is_image FROM tmp_table"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE tmp_table"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "UPDATE media_record SET album_id = \'instant\' WHERE album_id = \'camera-sync\'"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "UPDATE media_record SET album_id = NULL WHERE album_id = \'events\'"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 99
    const/16 v0, 0x13

    .line 102
    :cond_8
    const/16 v1, 0x14

    if-ge v0, v1, :cond_9

    .line 103
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ALTER TABLE media_tracker RENAME TO media_tracker_legacy"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE exclude_bucket (_id INTEGER PRIMARY KEY, bucket_id TEXT UNIQUE NOT NULL)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE media_tracker (_id INTEGER PRIMARY KEY, volume_name TEXT NOT NULL, media_type TEXT NOT NULL,last_media_id INTEGER NOT NULL DEFAULT(0))"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 104
    const/16 v0, 0x14

    .line 107
    :cond_9
    const/16 v1, 0x15

    if-ge v0, v1, :cond_a

    .line 108
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "DROP TABLE IF EXISTS fingerprints"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP INDEX IF EXISTS fingerprints_index_content_uri"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE local_fingerprints (content_uri TEXT PRIMARY KEY NOT NULL, fingerprint TEXT NOT NULL)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE server_fingerprints (_id INTEGER PRIMARY KEY, fingerprint TEXT NOT NULL, image_url TEXT, photo_id INT NOT NULL DEFAULT(0), owner_id TEXT NOT NULL, UNIQUE (owner_id, image_url), UNIQUE (owner_id, photo_id))"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE INDEX local_fingerprints_content_uri ON local_fingerprints(content_uri)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE INDEX local_fingerprints_fingerprint ON local_fingerprints(fingerprint)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE INDEX server_fingerprints_image_url ON server_fingerprints(image_url)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 109
    const/16 v0, 0x15

    .line 112
    :cond_a
    const/16 v1, 0x16

    if-ge v0, v1, :cond_b

    .line 113
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    .line 114
    const/16 v0, 0x16

    .line 117
    :cond_b
    const/16 v1, 0x17

    if-ge v0, v1, :cond_c

    .line 118
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ALTER TABLE media_record ADD COLUMN upload_status INT NOT NULL DEFAULT(0)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "UPDATE media_record SET upload_status = upload_state % 100"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "UPDATE media_record SET upload_state = (upload_state / 100) * 100"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 119
    const/16 v0, 0x17

    .line 122
    :cond_c
    const/16 v1, 0x18

    if-ge v0, v1, :cond_d

    .line 123
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ALTER TABLE media_record ADD COLUMN allow_full_res INT NOT NULL DEFAULT(1)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 124
    const/16 v0, 0x18

    .line 127
    :cond_d
    const/16 v1, 0x19

    if-ge v0, v1, :cond_e

    .line 128
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE local_folders (bucket_id TEXT UNIQUE NOT NULL)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 129
    const/16 v0, 0x19

    .line 132
    :cond_e
    const/16 v1, 0x1a

    if-ge v0, v1, :cond_f

    .line 133
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/autobackup/av;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 134
    const/16 v0, 0x1a

    .line 137
    :cond_f
    const/16 v1, 0x1b

    if-ge v0, v1, :cond_10

    .line 138
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    sget-object v0, Lcom/google/android/libraries/social/autobackup/av;->c:Lcom/google/android/libraries/social/f/a;

    .line 139
    const/16 v0, 0x1b

    .line 142
    :cond_10
    const/16 v1, 0x1c

    if-ge v0, v1, :cond_11

    .line 143
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "DELETE FROM local_fingerprints"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 144
    const/16 v0, 0x1c

    .line 147
    :cond_11
    const/16 v1, 0x1d

    if-ge v0, v1, :cond_12

    .line 148
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ALTER TABLE media_record ADD COLUMN resume_token TEXT"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 149
    const/16 v0, 0x1d

    .line 152
    :cond_12
    const/16 v1, 0x1e

    if-ge v0, v1, :cond_13

    .line 153
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    .line 154
    const/16 v0, 0x1e

    .line 157
    :cond_13
    const/16 v1, 0x1f

    if-ge v0, v1, :cond_14

    .line 158
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "DROP TABLE media_tracker_legacy"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 159
    const/16 v0, 0x1f

    .line 162
    :cond_14
    const/16 v1, 0x20

    if-ge v0, v1, :cond_15

    .line 163
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/autobackup/av;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 164
    const/16 v0, 0x20

    .line 167
    :cond_15
    const/16 v1, 0x21

    if-ge v0, v1, :cond_16

    .line 168
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/av;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/autobackup/av;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 169
    :cond_16
    return-void

    .line 73
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 98
    :catchall_1
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    :catch_0
    move-exception v0

    goto/16 :goto_0

    :cond_17
    move v0, p1

    goto/16 :goto_1
.end method
