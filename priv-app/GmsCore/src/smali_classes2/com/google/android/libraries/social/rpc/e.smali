.class public final Lcom/google/android/libraries/social/rpc/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/rpc/g;


# static fields
.field public static final a:Lcom/google/android/libraries/social/f/a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/google/android/libraries/social/f/a;

    const-string v1, "debug.social.rpc.debug_log"

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/f/a;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/social/rpc/e;->a:Lcom/google/android/libraries/social/f/a;

    return-void
.end method

.method private static a(Ljava/lang/String;I)Z
    .locals 3

    .prologue
    const/16 v2, 0x17

    const/4 v0, 0x0

    .line 56
    const-string v1, "HttpOperation"

    invoke-static {v1, p1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v2, :cond_0

    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_0
    invoke-static {p0, p1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method


# virtual methods
.method public final a(Lcom/google/android/libraries/social/rpc/i;)V
    .locals 0

    .prologue
    .line 50
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x3

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/rpc/e;->a(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/rpc/e;->a(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 33
    if-eqz p1, :cond_0

    .line 34
    const/4 v0, 0x3

    const-string v1, "HttpOperation"

    invoke-static {v0, v1, p1}, Lcom/google/android/libraries/b/a/d;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 36
    :cond_0
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 41
    if-eqz p1, :cond_0

    .line 42
    const/4 v0, 0x2

    const-string v1, "HttpOperation"

    invoke-static {v0, v1, p1}, Lcom/google/android/libraries/b/a/d;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 44
    :cond_0
    return-void
.end method
