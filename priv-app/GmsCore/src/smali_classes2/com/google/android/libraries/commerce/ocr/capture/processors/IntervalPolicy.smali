.class public Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final minIntervalInMs:I

.field private final stopwatch:Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 17
    invoke-static {}, Lcom/google/android/libraries/commerce/ocr/util/Ticker;->systemTicker()Lcom/google/android/libraries/commerce/ocr/util/Ticker;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;-><init>(ILcom/google/android/libraries/commerce/ocr/util/Ticker;)V

    .line 18
    return-void
.end method

.method constructor <init>(ILcom/google/android/libraries/commerce/ocr/util/Ticker;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput p1, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;->minIntervalInMs:I

    .line 23
    invoke-static {p2}, Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;->createStarted(Lcom/google/android/libraries/commerce/ocr/util/Ticker;)Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;->stopwatch:Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;

    .line 24
    return-void
.end method


# virtual methods
.method public isOverInterval()Z
    .locals 4

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;->stopwatch:Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;->elapsed(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    iget v2, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;->minIntervalInMs:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    .line 32
    :goto_0
    if-eqz v0, :cond_0

    .line 33
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;->stopwatch:Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;

    invoke-virtual {v1}, Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;->reset()Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;->start()Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;

    .line 35
    :cond_0
    return v0

    .line 30
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
