.class public final Lcom/google/android/libraries/social/rpc/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/HashMap;

.field b:Lcom/google/android/libraries/social/rpc/j;

.field c:J

.field d:J

.field e:J

.field f:J


# virtual methods
.method public final a()J
    .locals 5

    .prologue
    .line 182
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/i;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 183
    const-wide/16 v0, 0x0

    .line 184
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 185
    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/i;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/rpc/j;

    iget-wide v0, v0, Lcom/google/android/libraries/social/rpc/j;->e:J

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 186
    goto :goto_0

    .line 187
    :cond_0
    return-wide v2
.end method

.method public final b()J
    .locals 5

    .prologue
    .line 223
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/i;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 224
    const-wide/16 v0, 0x0

    .line 225
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 226
    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/i;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/rpc/j;

    iget-wide v0, v0, Lcom/google/android/libraries/social/rpc/j;->f:J

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 227
    goto :goto_0

    .line 228
    :cond_0
    return-wide v2
.end method
