.class final Lcom/google/android/libraries/social/h/b/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final a:Lcom/google/android/libraries/social/h/b/d;

.field final b:Landroid/net/Uri;

.field final synthetic c:Lcom/google/android/libraries/social/h/b/b;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/h/b/b;Lcom/google/android/libraries/social/h/b/d;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/android/libraries/social/h/b/c;->c:Lcom/google/android/libraries/social/h/b/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p2, p0, Lcom/google/android/libraries/social/h/b/c;->a:Lcom/google/android/libraries/social/h/b/d;

    .line 55
    iput-object p3, p0, Lcom/google/android/libraries/social/h/b/c;->b:Landroid/net/Uri;

    .line 56
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 6

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x1

    .line 49
    check-cast p1, Lcom/google/android/gms/panorama/f;

    invoke-interface {p1}, Lcom/google/android/gms/panorama/f;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/panorama/f;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/android/libraries/social/h/b/c;->a:Lcom/google/android/libraries/social/h/b/d;

    invoke-interface {p1}, Lcom/google/android/gms/panorama/f;->c()Landroid/content/Intent;

    invoke-interface {v3, v0}, Lcom/google/android/libraries/social/h/b/d;->a(I)V

    iget-object v3, p0, Lcom/google/android/libraries/social/h/b/c;->c:Lcom/google/android/libraries/social/h/b/b;

    invoke-static {v3}, Lcom/google/android/libraries/social/h/b/b;->a(Lcom/google/android/libraries/social/h/b/b;)Landroid/os/Handler;

    move-result-object v3

    const-wide/16 v4, 0xbb8

    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    const-string v1, "PanoramaClient"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "PanoramaClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onPanoramaTypeDetected: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/libraries/social/h/b/b;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void

    :pswitch_0
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_1
    move v0, v1

    goto :goto_0

    :pswitch_2
    move v0, v2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
