.class final Lcom/google/android/libraries/social/autobackup/ax;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/net/Uri;

.field private static b:Lcom/google/android/libraries/social/autobackup/ax;


# instance fields
.field private c:Lcom/google/android/libraries/social/account/b;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/libraries/social/autobackup/au;

.field private final f:Lcom/google/android/libraries/social/autobackup/ap;

.field private final g:Landroid/content/SharedPreferences;

.field private final h:Landroid/os/Handler;

.field private i:I

.field private volatile j:Z

.field private k:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const-string v0, "content://media/external/fs_id"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/autobackup/ax;->a:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x32

    const/4 v1, 0x0

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-boolean v1, p0, Lcom/google/android/libraries/social/autobackup/ax;->j:Z

    .line 99
    const-wide/16 v2, 0x3a98

    iput-wide v2, p0, Lcom/google/android/libraries/social/autobackup/ax;->k:J

    .line 102
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/ax;->d:Landroid/content/Context;

    .line 103
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->d:Landroid/content/Context;

    const-class v2, Lcom/google/android/libraries/social/account/b;

    invoke-static {v0, v2}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->c:Lcom/google/android/libraries/social/account/b;

    .line 104
    const-class v0, Lcom/google/android/libraries/social/autobackup/au;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/au;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->e:Lcom/google/android/libraries/social/autobackup/au;

    .line 105
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->g:Landroid/content/SharedPreferences;

    .line 107
    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/ap;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->f:Lcom/google/android/libraries/social/autobackup/ap;

    .line 109
    new-instance v0, Landroid/os/HandlerThread;

    const-string v2, "picasa-uploads-manager"

    const/16 v3, 0xa

    invoke-direct {v0, v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 111
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 112
    new-instance v2, Lcom/google/android/libraries/social/autobackup/az;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/libraries/social/autobackup/az;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/google/android/libraries/social/autobackup/ax;->h:Landroid/os/Handler;

    .line 114
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->h:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/ax;->h:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->g:Landroid/content/SharedPreferences;

    const-string v2, "external_storage_fsid"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->j:Z

    .line 118
    iget-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->j:Z

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->g:Landroid/content/SharedPreferences;

    const-string v2, "external_storage_fsid"

    const/4 v3, -0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->i:I

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->g:Landroid/content/SharedPreferences;

    const-string v2, "system_release"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/ax;->g:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "system_release"

    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const-string v2, "iu.UploadsManager"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "System changed from "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 125
    invoke-direct {p0}, Lcom/google/android/libraries/social/autobackup/ax;->d()V

    .line 135
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/social/autobackup/ax;->a:Landroid/net/Uri;

    new-instance v3, Lcom/google/android/libraries/social/autobackup/ay;

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/ax;->h:Landroid/os/Handler;

    invoke-direct {v3, p0, v4}, Lcom/google/android/libraries/social/autobackup/ay;-><init>(Lcom/google/android/libraries/social/autobackup/ax;Landroid/os/Handler;)V

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 142
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->h:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/ax;->h:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 143
    return-void

    :cond_3
    move v0, v1

    .line 124
    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/ax;
    .locals 2

    .prologue
    .line 80
    const-class v1, Lcom/google/android/libraries/social/autobackup/ax;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/ax;->b:Lcom/google/android/libraries/social/autobackup/ax;

    if-nez v0, :cond_0

    .line 81
    new-instance v0, Lcom/google/android/libraries/social/autobackup/ax;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/social/autobackup/ax;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/ax;->b:Lcom/google/android/libraries/social/autobackup/ax;

    .line 83
    :cond_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/ax;->b:Lcom/google/android/libraries/social/autobackup/ax;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/libraries/social/autobackup/ax;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/libraries/social/autobackup/ax;->e()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/libraries/social/autobackup/ax;I)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/autobackup/ax;->d(I)V

    return-void
.end method

.method private static b(Landroid/content/Context;)I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 234
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 235
    sget-object v1, Lcom/google/android/libraries/social/autobackup/ax;->a:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 237
    if-eqz v1, :cond_0

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 246
    invoke-static {v1}, Lcom/google/android/libraries/social/g/a/f;->a(Landroid/database/Cursor;)V

    :goto_0
    return v0

    .line 240
    :cond_0
    :try_start_1
    const-string v0, "iu.UploadsManager"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 241
    const-string v0, "iu.UploadsManager"

    const-string v2, "No FSID on this device!"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 243
    :cond_1
    invoke-static {v1}, Lcom/google/android/libraries/social/g/a/f;->a(Landroid/database/Cursor;)V

    const/4 v0, -0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcom/google/android/libraries/social/g/a/f;->a(Landroid/database/Cursor;)V

    throw v0
.end method

.method static synthetic b(Lcom/google/android/libraries/social/autobackup/ax;)Lcom/google/android/libraries/social/autobackup/ap;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->f:Lcom/google/android/libraries/social/autobackup/ap;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/libraries/social/autobackup/ax;I)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/autobackup/ax;->c(I)V

    return-void
.end method

.method static synthetic c()Lcom/google/android/libraries/social/autobackup/ax;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/libraries/social/autobackup/ax;->b:Lcom/google/android/libraries/social/autobackup/ax;

    return-object v0
.end method

.method private c(Ljava/lang/String;)Ljava/util/List;
    .locals 2

    .prologue
    .line 383
    sget-object v0, Lcom/google/android/libraries/social/autobackup/aj;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->d:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/autobackup/o;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/o;

    .line 387
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/o;->e()Ljava/util/List;

    move-result-object v0

    .line 394
    :goto_0
    return-object v0

    .line 388
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 389
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 390
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 392
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private declared-synchronized c(I)V
    .locals 5

    .prologue
    .line 310
    monitor-enter p0

    :try_start_0
    const-string v0, "iu.UploadsManager"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 311
    const-string v0, "iu.UploadsManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "enable existing photos upload for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->e:Lcom/google/android/libraries/social/autobackup/au;

    invoke-static {v0, p1}, Lcom/google/android/libraries/social/autobackup/ab;->e(Lcom/google/android/libraries/social/autobackup/au;I)V

    .line 315
    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/ax;->b()V

    .line 318
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->e:Lcom/google/android/libraries/social/autobackup/au;

    const/16 v1, 0x28

    invoke-static {v0, p1, v1}, Lcom/google/android/libraries/social/autobackup/ab;->b(Lcom/google/android/libraries/social/autobackup/au;II)I

    move-result v0

    .line 320
    if-nez v0, :cond_2

    .line 321
    new-instance v0, Landroid/content/ContentValues;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    .line 322
    const-string v1, "upload_all_state"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 324
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/ax;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/ax;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/aj;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 326
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/autobackup/ax;->d(I)V

    .line 328
    const-string v0, "iu.UploadsManager"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 329
    const-string v0, "iu.UploadsManager"

    const-string v1, "--- DONE upload all; no more photos"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 347
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 332
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 333
    new-instance v0, Landroid/content/ContentValues;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    .line 334
    const-string v1, "upload_all_state"

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 336
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/ax;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/ax;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/aj;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 338
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/autobackup/ax;->d(I)V

    .line 340
    const-string v0, "iu.UploadsManager"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 341
    const-string v0, "iu.UploadsManager"

    const-string v1, "--- DONE upload all; no storage"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 310
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 346
    :cond_3
    :try_start_2
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/i;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/i;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/social/autobackup/i;->b(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/libraries/social/autobackup/ax;)V
    .locals 6

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->d:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/autobackup/o;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/o;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/o;->e()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/ax;->e:Lcom/google/android/libraries/social/autobackup/au;

    const/16 v5, 0x28

    invoke-static {v4, v0, v5}, Lcom/google/android/libraries/social/autobackup/ab;->b(Lcom/google/android/libraries/social/autobackup/au;II)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    if-nez v1, :cond_1

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/autobackup/ax;->b(I)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method private declared-synchronized d()V
    .locals 4

    .prologue
    .line 146
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->d:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/autobackup/o;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/o;

    .line 148
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/o;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 149
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/autobackup/ax;->b(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 146
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 151
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->e:Lcom/google/android/libraries/social/autobackup/au;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/au;->a()V

    const-wide/16 v0, 0x3a98

    iput-wide v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->k:J
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152
    :goto_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->f:Lcom/google/android/libraries/social/autobackup/ap;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/ap;->a(Landroid/database/Cursor;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 153
    monitor-exit p0

    return-void

    .line 151
    :catch_0
    move-exception v0

    :try_start_3
    const-string v1, "iu.UploadsManager"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "iu.UploadsManager"

    const-string v2, "database not ready for reset; "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->h:Landroid/os/Handler;

    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/google/android/libraries/social/autobackup/ax;->k:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->k:J

    const-wide/16 v2, 0x2

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->k:J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private d(I)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 423
    .line 424
    const/4 v7, -0x1

    .line 426
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/ax;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/libraries/social/autobackup/aj;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "upload_all_state"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 429
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 430
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    .line 433
    :goto_0
    if-eqz v1, :cond_0

    .line 434
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 437
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/ax;->e:Lcom/google/android/libraries/social/autobackup/au;

    invoke-static {v1}, Lcom/google/android/libraries/social/autobackup/ab;->a(Lcom/google/android/libraries/social/autobackup/au;)I

    move-result v1

    .line 439
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/ax;->e:Lcom/google/android/libraries/social/autobackup/au;

    const/16 v3, 0x28

    invoke-static {v2, p1, v3}, Lcom/google/android/libraries/social/autobackup/ab;->b(Lcom/google/android/libraries/social/autobackup/au;II)I

    move-result v2

    .line 441
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.google.android.libraries.social.autobackup.upload_all_progress"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 442
    const-string v4, "upload_all_account_id"

    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 443
    const-string v4, "upload_all_progress"

    sub-int v2, v1, v2

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 445
    const-string v2, "upload_all_count"

    invoke-virtual {v3, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 446
    const-string v1, "upload_all_state"

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 447
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->d:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 448
    return-void

    .line 433
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_1

    .line 434
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 433
    :catchall_1
    move-exception v0

    goto :goto_1

    :cond_2
    move v0, v7

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/libraries/social/autobackup/ax;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/libraries/social/autobackup/ax;->d()V

    return-void
.end method

.method private declared-synchronized e()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 190
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 191
    const-string v0, "iu.UploadsManager"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    const-string v0, "iu.UploadsManager"

    const-string v1, "external storage not mounted"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 231
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 197
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/ax;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/libraries/social/autobackup/ax;->b(Landroid/content/Context;)I

    move-result v1

    .line 200
    const-string v2, "iu.UploadsManager"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 201
    const-string v2, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "storage changed; old: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/google/android/libraries/social/autobackup/ax;->i:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", new: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/libraries/social/autobackup/ax;->j:Z

    if-nez v2, :cond_5

    .line 206
    const-string v2, "iu.UploadsManager"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 207
    const-string v2, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "set fsid="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    :cond_3
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/libraries/social/autobackup/ax;->j:Z

    .line 210
    iput v1, p0, Lcom/google/android/libraries/social/autobackup/ax;->i:I

    .line 211
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/ax;->g:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "external_storage_fsid"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 228
    :cond_4
    :goto_1
    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/i;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/i;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/social/autobackup/i;->b(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 190
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 212
    :cond_5
    :try_start_2
    iget v2, p0, Lcom/google/android/libraries/social/autobackup/ax;->i:I

    if-ne v2, v1, :cond_6

    .line 214
    const/4 v2, -0x1

    if-ne v1, v2, :cond_4

    .line 215
    const/4 v0, 0x0

    goto :goto_1

    .line 219
    :cond_6
    const-string v2, "iu.UploadsManager"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 220
    const-string v2, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "fsid changed from "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/google/android/libraries/social/autobackup/ax;->i:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    :cond_7
    iput v1, p0, Lcom/google/android/libraries/social/autobackup/ax;->i:I

    .line 223
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/ax;->g:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "external_storage_fsid"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 224
    invoke-direct {p0}, Lcom/google/android/libraries/social/autobackup/ax;->d()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/ContentValues;)J
    .locals 6

    .prologue
    .line 256
    const-string v0, "media_url"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 257
    if-nez v1, :cond_0

    .line 258
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "must specify a media url"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->e:Lcom/google/android/libraries/social/autobackup/au;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/au;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 263
    const-string v0, "_id"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    .line 264
    const-string v0, "_id"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 266
    const/4 v0, 0x0

    .line 267
    if-eqz v3, :cond_1

    .line 268
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(Landroid/database/sqlite/SQLiteDatabase;J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    .line 271
    :cond_1
    if-nez v0, :cond_4

    .line 272
    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(Landroid/content/ContentValues;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    .line 278
    :goto_0
    invoke-static {v1}, Lcom/google/android/libraries/social/mediaupload/ae;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 279
    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/ax;->d:Landroid/content/Context;

    invoke-static {v3, v1}, Lcom/google/android/libraries/social/mediaupload/ae;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 280
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "file://"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->f(Ljava/lang/String;)V

    .line 281
    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/ax;->d:Landroid/content/Context;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/android/libraries/social/mediaupload/ae;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 282
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->s()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 283
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->e(Ljava/lang/String;)V

    .line 287
    :cond_2
    const-string v1, "upload_reason"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "upload_reason"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 291
    :goto_1
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 292
    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 293
    sget-object v1, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/libraries/social/g/a/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/libraries/social/g/a/a;)J

    move-result-wide v2

    .line 294
    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/ax;->b()V

    .line 296
    const-string v1, "iu.UploadsManager"

    const/4 v4, 0x4

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 297
    const-string v1, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "+++ ADD record; manual upload: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/i;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/i;

    move-result-object v0

    const-wide/16 v4, 0x1f4

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/social/autobackup/i;->b(J)V

    .line 302
    return-wide v2

    .line 274
    :cond_4
    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(Landroid/content/ContentValues;)V

    goto/16 :goto_0

    .line 287
    :cond_5
    const/16 v1, 0xa

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v1, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 350
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/autobackup/ax;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 351
    new-instance v3, Lcom/google/android/libraries/social/c/b;

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "upload_all_account_id"

    aput-object v5, v4, v2

    const-string v5, "upload_all_progress"

    aput-object v5, v4, v1

    const/4 v5, 0x2

    const-string v6, "upload_all_count"

    aput-object v6, v4, v5

    const-string v5, "upload_all_state"

    aput-object v5, v4, v10

    invoke-direct {v3, v4, v2}, Lcom/google/android/libraries/social/c/b;-><init>([Ljava/lang/String;B)V

    .line 357
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 358
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 359
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->e:Lcom/google/android/libraries/social/autobackup/au;

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/ab;->a(Lcom/google/android/libraries/social/autobackup/au;)I

    move-result v6

    .line 361
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->e:Lcom/google/android/libraries/social/autobackup/au;

    const/16 v7, 0x28

    invoke-static {v0, v5, v7}, Lcom/google/android/libraries/social/autobackup/ab;->b(Lcom/google/android/libraries/social/autobackup/au;II)I

    move-result v0

    .line 363
    sub-int v7, v6, v0

    .line 365
    const-string v0, "iu.UploadsManager"

    invoke-static {v0, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 367
    const-string v8, "iu.UploadsManager"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v9, "get upload-all status for "

    invoke-direct {v0, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/libraries/social/g/a/f;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, " allDone? "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    if-ne v6, v7, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, " current:"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, " total:"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, " state=0"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    :cond_0
    invoke-virtual {v3}, Lcom/google/android/libraries/social/c/b;->a()Lcom/google/android/libraries/social/c/c;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/social/c/c;->a(Ljava/lang/Object;)Lcom/google/android/libraries/social/c/c;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/social/c/c;->a(Ljava/lang/Object;)Lcom/google/android/libraries/social/c/c;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/social/c/c;->a(Ljava/lang/Object;)Lcom/google/android/libraries/social/c/c;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/social/c/c;->a(Ljava/lang/Object;)Lcom/google/android/libraries/social/c/c;

    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 367
    goto :goto_1

    .line 375
    :cond_2
    invoke-virtual {v3}, Lcom/google/android/libraries/social/c/b;->a()Lcom/google/android/libraries/social/c/c;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/libraries/social/c/c;->a(Ljava/lang/Object;)Lcom/google/android/libraries/social/c/c;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/libraries/social/c/c;->a(Ljava/lang/Object;)Lcom/google/android/libraries/social/c/c;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/libraries/social/c/c;->a(Ljava/lang/Object;)Lcom/google/android/libraries/social/c/c;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/c/c;->a(Ljava/lang/Object;)Lcom/google/android/libraries/social/c/c;

    .line 377
    :cond_3
    return-object v3
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->f:Lcom/google/android/libraries/social/autobackup/ap;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/ap;->m()Landroid/database/Cursor;

    move-result-object v0

    .line 186
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/ax;->h:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 187
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->h:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 307
    return-void
.end method

.method public final b(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 398
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/autobackup/ax;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 399
    new-instance v1, Lcom/google/android/libraries/social/c/b;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "iu_pending_count"

    aput-object v3, v2, v4

    invoke-direct {v1, v2, v4}, Lcom/google/android/libraries/social/c/b;-><init>([Ljava/lang/String;B)V

    .line 402
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 403
    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/ax;->e:Lcom/google/android/libraries/social/autobackup/au;

    const/16 v4, 0x1e

    invoke-static {v3, v0, v4}, Lcom/google/android/libraries/social/autobackup/ab;->b(Lcom/google/android/libraries/social/autobackup/au;II)I

    move-result v3

    .line 405
    invoke-virtual {v1}, Lcom/google/android/libraries/social/c/b;->a()Lcom/google/android/libraries/social/c/c;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/social/c/c;->a(Ljava/lang/Object;)Lcom/google/android/libraries/social/c/c;

    .line 406
    const-string v4, "iu.UploadsManager"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 407
    const-string v4, "iu.UploadsManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "get iu pending count for "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ":"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 412
    :cond_1
    return-object v1
.end method

.method final b()V
    .locals 3

    .prologue
    .line 417
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/social/autobackup/aj;->b:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 419
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/c/b;->d(Landroid/content/Context;)V

    .line 420
    return-void
.end method

.method final b(I)V
    .locals 3

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ax;->h:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 472
    return-void
.end method
