.class public Lcom/google/android/libraries/social/account/impl/AccountStoreModule;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/a/f;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/libraries/social/a/a;)V
    .locals 2

    .prologue
    .line 23
    const-class v0, Lcom/google/android/libraries/social/account/b;

    if-ne p2, v0, :cond_1

    .line 24
    const-class v0, Lcom/google/android/libraries/social/account/b;

    new-instance v1, Lcom/google/android/libraries/social/account/impl/a;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/social/account/impl/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    .line 29
    :cond_0
    :goto_0
    return-void

    .line 25
    :cond_1
    const-class v0, Lcom/google/android/libraries/social/account/k;

    if-ne p2, v0, :cond_2

    .line 26
    const-class v0, Lcom/google/android/libraries/social/account/k;

    new-instance v1, Lcom/google/android/libraries/social/account/impl/i;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/social/account/impl/i;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto :goto_0

    .line 27
    :cond_2
    const-class v0, Lcom/google/android/libraries/social/d/a/a;

    if-ne p2, v0, :cond_0

    .line 28
    sget-object v0, Lcom/google/android/libraries/social/d/a/a;->a:Lcom/google/android/libraries/social/f/a;

    goto :goto_0
.end method
