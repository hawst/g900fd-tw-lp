.class public Lcom/google/android/libraries/social/rpc/a/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/rpc/l;


# static fields
.field private static a:Lcom/google/android/libraries/social/rpc/a/a;

.field private static final b:Lcom/google/android/libraries/social/f/a;

.field private static f:Ljava/lang/String;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lcom/google/android/libraries/social/rpc/a/a;

    invoke-direct {v0}, Lcom/google/android/libraries/social/rpc/a/a;-><init>()V

    sput-object v0, Lcom/google/android/libraries/social/rpc/a/d;->a:Lcom/google/android/libraries/social/rpc/a/a;

    .line 36
    new-instance v0, Lcom/google/android/libraries/social/f/a;

    const-string v1, "debug.allowBackendOverride"

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/f/a;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/social/rpc/a/d;->b:Lcom/google/android/libraries/social/f/a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/libraries/social/rpc/a/d;->c:Landroid/content/Context;

    .line 46
    iput-object p2, p0, Lcom/google/android/libraries/social/rpc/a/d;->d:Ljava/lang/String;

    .line 47
    iput-object p3, p0, Lcom/google/android/libraries/social/rpc/a/d;->e:Ljava/lang/String;

    .line 48
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 111
    sget-object v0, Lcom/google/android/libraries/social/rpc/a/d;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Le/a/a/n;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (gzip)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/rpc/a/d;->f:Ljava/lang/String;

    .line 114
    :cond_0
    sget-object v0, Lcom/google/android/libraries/social/rpc/a/d;->f:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/util/Map;
    .locals 6

    .prologue
    .line 52
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 53
    const-string v1, "Accept-Encoding"

    const-string v2, "gzip"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    const-string v1, "Accept-Language"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    const-string v1, "User-Agent"

    iget-object v2, p0, Lcom/google/android/libraries/social/rpc/a/d;->c:Landroid/content/Context;

    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/rpc/a/d;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/a/d;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 64
    :try_start_0
    sget-object v1, Lcom/google/android/libraries/social/rpc/a/d;->a:Lcom/google/android/libraries/social/rpc/a/a;

    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/a/d;->e:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/libraries/social/rpc/a/a;->a(Ljava/lang/String;)Lcom/google/android/libraries/social/rpc/a/b;

    move-result-object v1

    .line 65
    iget-object v2, p0, Lcom/google/android/libraries/social/rpc/a/d;->c:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/libraries/social/rpc/a/d;->d:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/google/android/libraries/social/rpc/a/b;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 66
    iget-object v3, p0, Lcom/google/android/libraries/social/rpc/a/d;->c:Landroid/content/Context;

    invoke-interface {v1, v3, v2}, Lcom/google/android/libraries/social/rpc/a/b;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 71
    const-string v3, "Authorization"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Bearer "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    const-string v2, "X-Auth-Time"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    :cond_0
    sget-object v1, Lcom/google/android/libraries/social/rpc/a/d;->b:Lcom/google/android/libraries/social/f/a;

    .line 76
    return-object v0

    .line 67
    :catch_0
    move-exception v0

    .line 68
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Cannot obtain authentication token"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/a/d;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 95
    :try_start_0
    sget-object v0, Lcom/google/android/libraries/social/rpc/a/d;->a:Lcom/google/android/libraries/social/rpc/a/a;

    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/a/d;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/libraries/social/rpc/a/a;->a(Ljava/lang/String;)Lcom/google/android/libraries/social/rpc/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/a/d;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/libraries/social/rpc/a/d;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/libraries/social/rpc/a/b;->b(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :cond_0
    return-void

    .line 97
    :catch_0
    move-exception v0

    .line 98
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Cannot invalidate authentication token"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
