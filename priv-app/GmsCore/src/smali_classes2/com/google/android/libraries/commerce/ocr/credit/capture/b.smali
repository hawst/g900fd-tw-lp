.class final Lcom/google/android/libraries/commerce/ocr/credit/capture/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$Callback;


# instance fields
.field final synthetic a:Ljava/util/TimerTask;

.field final synthetic b:Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;

.field final synthetic c:Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;

.field final synthetic d:Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;

.field final synthetic e:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;Ljava/util/TimerTask;Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->e:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;

    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->a:Ljava/util/TimerTask;

    iput-object p3, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->b:Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;

    iput-object p4, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->c:Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;

    iput-object p5, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->d:Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSuccess(Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;)V
    .locals 7

    .prologue
    const/16 v6, 0x5a

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 142
    const-string v2, "CreditCardOcrRecognizer"

    const-string v3, "Credit card OCR operation complete"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->a:Ljava/util/TimerTask;

    invoke-virtual {v2}, Ljava/util/TimerTask;->scheduledExecutionTime()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 179
    :goto_0
    return-void

    .line 146
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->e:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->timeoutManager:Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;
    invoke-static {v2}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->access$000(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;)Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->a:Ljava/util/TimerTask;

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;->stopTimeout(Ljava/util/TimerTask;)V

    .line 149
    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->e:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->validateResult:Z
    invoke-static {v2}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->access$100(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 150
    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->e:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->validator:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator;
    invoke-static {v2}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->access$500(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;)Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->getCardNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator;->validatePAN(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->getMeanDigitConfidence()D

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->e:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->thresholdMeanDigitConfidence:F
    invoke-static {v4}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->access$600(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;)F

    move-result v4

    float-to-double v4, v4

    cmpg-double v2, v2, v4

    if-ltz v2, :cond_1

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->getMinDigitConfidence()D

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->e:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->thresholdMinDigitConfidence:F
    invoke-static {v4}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->access$700(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;)F

    move-result v4

    float-to-double v4, v4

    cmpg-double v2, v2, v4

    if-gez v2, :cond_2

    :cond_1
    move v2, v0

    :goto_1
    if-eqz v2, :cond_3

    .line 151
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->b:Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;->onUnrecognized(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    move v2, v1

    .line 150
    goto :goto_1

    .line 154
    :cond_3
    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->e:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->validator:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator;
    invoke-static {v2}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->access$500(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;)Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->getExpirationYear()I

    move-result v3

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->getExpirationMonth()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator;->validateExpDate(II)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    :goto_2
    if-eqz v0, :cond_5

    .line 155
    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->clearExpirationDate()V

    .line 159
    :cond_5
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 160
    const-string v1, "DEBUG_OCR_TIME"

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->e:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;

    iget-object v3, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->c:Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;

    # invokes: Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->stopAndGetTime(Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;)J
    invoke-static {v2, v3}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->access$200(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 161
    const-string v1, "DEBUG_RECTIFIED_OCR_IMAGE"

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->e:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->imageUtil:Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;
    invoke-static {v2}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->access$300(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;)Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->d:Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;

    invoke-virtual {v3}, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;->getRectifiedCard()Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getData()[B

    move-result-object v3

    invoke-virtual {v2, v3, v6}, Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;->nativeToJpeg([BI)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 164
    const-string v1, "DEBUG_RECTIFIED_EXPIRATION_DATE_IMAGE"

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->e:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->imageUtil:Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;
    invoke-static {v2}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->access$300(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;)Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->d:Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;

    invoke-virtual {v3}, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;->getExpirationDateImage()Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getData()[B

    move-result-object v3

    invoke-virtual {v2, v3, v6}, Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;->nativeToJpeg([BI)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 167
    const-string v1, "DEBUG_MIN_DIGIT_CONFIDENCE"

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->getMinDigitConfidence()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 168
    const-string v1, "DEBUG_MEAN_DIGIT_CONFIDENCE"

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->getMeanDigitConfidence()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 169
    const-string v1, "DEBUG_RECTIFICATION_TIME"

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->e:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->detectionAndRectificationTime:J
    invoke-static {v2}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->access$400(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 170
    const-string v1, "DEBUG_REQUEST_SIZE"

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->d:Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;

    invoke-virtual {v2}, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;->getRectifiedCard()Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getData()[B

    move-result-object v2

    array-length v2, v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 172
    const-string v1, "DEBUG_EXPIRATION_DATE_PREDICTION_STATUS"

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->getExpirationDatePredictionStatus()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 174
    const-string v1, "DEBUG_EXPIRATION_DATE_CONFIDENCE"

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->getExpirationDateConfidence()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 177
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->e:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->timeoutManager:Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;
    invoke-static {v1}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->access$000(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;)Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;->stopTimeout()V

    .line 178
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->b:Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;

    invoke-interface {v1, p1, v0}, Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;->onRecognized(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 154
    :cond_6
    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->getExpirationDateNumDigits()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    const-string v1, "CreditCardOcrRecognizer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid expiration date num digits: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->getExpirationDateNumDigits()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :pswitch_1
    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->getExpirationDateConfidence()D

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->e:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->thresholdExpirationDateMinConfidence4Digit:F
    invoke-static {v4}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->access$800(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;)F

    move-result v4

    float-to-double v4, v4

    cmpg-double v2, v2, v4

    if-ltz v2, :cond_4

    move v0, v1

    goto/16 :goto_2

    :pswitch_2
    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->getExpirationDateConfidence()D

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;->e:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->thresholdExpirationDateMinConfidence6Digit:F
    invoke-static {v4}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->access$900(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;)F

    move-result v4

    float-to-double v4, v4

    cmpg-double v2, v2, v4

    if-ltz v2, :cond_4

    move v0, v1

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
