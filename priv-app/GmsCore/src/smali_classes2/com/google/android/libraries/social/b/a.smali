.class public final Lcom/google/android/libraries/social/b/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/i/ae;
.implements Lcom/google/android/libraries/social/i/ak;


# instance fields
.field private a:Ljava/util/ArrayList;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/i/w;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/b/a;->a:Ljava/util/ArrayList;

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/b/a;->b:Z

    .line 43
    invoke-virtual {p1, p0}, Lcom/google/android/libraries/social/i/w;->a(Lcom/google/android/libraries/social/i/ak;)Lcom/google/android/libraries/social/i/ak;

    .line 44
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/Runnable;)Lcom/google/android/libraries/social/b/b;
    .locals 2

    .prologue
    .line 47
    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/libraries/social/b/a;->a(Ljava/lang/Runnable;J)Lcom/google/android/libraries/social/b/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Runnable;J)Lcom/google/android/libraries/social/b/b;
    .locals 2

    .prologue
    .line 51
    monitor-enter p0

    const/4 v0, 0x0

    .line 52
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/libraries/social/b/a;->b:Z

    if-eqz v1, :cond_0

    .line 53
    new-instance v0, Lcom/google/android/libraries/social/b/c;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/libraries/social/b/c;-><init>(Lcom/google/android/libraries/social/b/a;Ljava/lang/Runnable;J)V

    .line 54
    iget-object v1, p0, Lcom/google/android/libraries/social/b/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55
    invoke-interface {v0}, Lcom/google/android/libraries/social/b/b;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    :cond_0
    monitor-exit p0

    return-object v0

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 68
    monitor-enter p0

    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/libraries/social/b/a;->b:Z

    move v1, v0

    .line 69
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/b/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/google/android/libraries/social/b/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/b/b;

    .line 71
    invoke-interface {v0}, Lcom/google/android/libraries/social/b/b;->b()V

    .line 69
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/b/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    monitor-exit p0

    return-void

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/android/libraries/social/b/b;)V
    .locals 1

    .prologue
    .line 77
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/b/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    monitor-exit p0

    return-void

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
