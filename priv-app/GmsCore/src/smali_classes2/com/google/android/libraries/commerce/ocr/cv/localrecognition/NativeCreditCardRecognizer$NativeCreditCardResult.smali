.class public Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$NativeCreditCardResult;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final meanDigitConfidence:D

.field private final minDigitConfidence:D

.field private final numbers:[I


# direct methods
.method public constructor <init>([IDD)V
    .locals 0

    .prologue
    .line 211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 212
    iput-wide p2, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$NativeCreditCardResult;->minDigitConfidence:D

    .line 213
    iput-wide p4, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$NativeCreditCardResult;->meanDigitConfidence:D

    .line 214
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$NativeCreditCardResult;->numbers:[I

    .line 215
    return-void
.end method


# virtual methods
.method public getMeanDigitConfidence()D
    .locals 2

    .prologue
    .line 226
    iget-wide v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$NativeCreditCardResult;->meanDigitConfidence:D

    return-wide v0
.end method

.method public getMinDigitConfidence()D
    .locals 2

    .prologue
    .line 222
    iget-wide v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$NativeCreditCardResult;->minDigitConfidence:D

    return-wide v0
.end method

.method public getNumbers()[I
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$NativeCreditCardResult;->numbers:[I

    return-object v0
.end method
