.class public final Lcom/google/android/libraries/social/r/a;
.super Lcom/google/android/libraries/social/rpc/b/d;
.source "SourceFile"


# instance fields
.field private final i:Ljava/lang/String;

.field private final j:Ljava/util/List;

.field private final k:Z

.field private l:Ljava/util/Map;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;)V
    .locals 6

    .prologue
    .line 36
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/social/r/a;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;B)V

    .line 37
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;B)V
    .locals 6

    .prologue
    .line 41
    new-instance v2, Lcom/google/android/libraries/social/rpc/q;

    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, p2}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/libraries/social/rpc/q;-><init>(Ljava/lang/String;)V

    const-string v3, "checkphotosexistence"

    new-instance v4, Lcom/google/c/e/b/a/a/o;

    invoke-direct {v4}, Lcom/google/c/e/b/a/a/o;-><init>()V

    new-instance v5, Lcom/google/c/e/b/a/a/p;

    invoke-direct {v5}, Lcom/google/c/e/b/a/a/p;-><init>()V

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/social/rpc/b/d;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/rpc/q;Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)V

    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/r/a;->l:Ljava/util/Map;

    .line 44
    iput-object p3, p0, Lcom/google/android/libraries/social/r/a;->i:Ljava/lang/String;

    .line 45
    iput-object p4, p0, Lcom/google/android/libraries/social/r/a;->j:Ljava/util/List;

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/r/a;->k:Z

    .line 47
    return-void
.end method


# virtual methods
.method protected final synthetic a(Lcom/google/protobuf/nano/j;)V
    .locals 3

    .prologue
    .line 25
    check-cast p1, Lcom/google/c/e/b/a/a/o;

    new-instance v0, Lcom/google/c/e/b/d/a/j;

    invoke-direct {v0}, Lcom/google/c/e/b/d/a/j;-><init>()V

    iput-object v0, p1, Lcom/google/c/e/b/a/a/o;->a:Lcom/google/c/e/b/d/a/j;

    iget-object v1, p1, Lcom/google/c/e/b/a/a/o;->a:Lcom/google/c/e/b/d/a/j;

    iget-boolean v0, p0, Lcom/google/android/libraries/social/r/a;->k:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lcom/google/c/e/b/d/a/j;->d:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/libraries/social/r/a;->i:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/c/e/b/d/a/j;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/libraries/social/r/a;->j:Ljava/util/List;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v1, Lcom/google/c/e/b/d/a/j;->b:[Ljava/lang/String;

    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/libraries/social/r/a;->l:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/libraries/social/r/a;->l:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method protected final synthetic b(Lcom/google/protobuf/nano/j;)V
    .locals 6

    .prologue
    .line 25
    check-cast p1, Lcom/google/c/e/b/a/a/p;

    iget-object v2, p1, Lcom/google/c/e/b/a/a/p;->a:Lcom/google/c/e/b/d/a/a/b;

    iget-object v0, v2, Lcom/google/c/e/b/d/a/a/b;->a:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, v2, Lcom/google/c/e/b/d/a/a/b;->a:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, v2, Lcom/google/c/e/b/d/a/a/b;->a:[Ljava/lang/String;

    array-length v0, v0

    iget-object v1, p0, Lcom/google/android/libraries/social/r/a;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "returned array length doesn\'t match input"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/libraries/social/r/a;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, v2, Lcom/google/c/e/b/d/a/a/b;->a:[Ljava/lang/String;

    aget-object v4, v4, v1

    if-eqz v4, :cond_3

    :try_start_0
    iget-object v5, p0, Lcom/google/android/libraries/social/r/a;->l:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v5, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method
