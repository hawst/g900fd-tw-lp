.class public final Lcom/google/android/libraries/rocket/impressions/Session;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final a:J


# instance fields
.field private final b:Ljava/util/ArrayList;

.field private c:Lcom/google/android/libraries/rocket/impressions/l;

.field private final d:Lcom/google/c/b/b/a/a/a/a;

.field private final e:Lcom/google/c/b/b/a/a/i;

.field private final f:Lcom/google/c/b/b/a/a/g;

.field private g:J

.field private h:J

.field private i:J

.field private j:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 30
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xe

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/libraries/rocket/impressions/Session;->a:J

    .line 278
    new-instance v0, Lcom/google/android/libraries/rocket/impressions/k;

    invoke-direct {v0}, Lcom/google/android/libraries/rocket/impressions/k;-><init>()V

    sput-object v0, Lcom/google/android/libraries/rocket/impressions/Session;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->b:Ljava/util/ArrayList;

    .line 248
    :try_start_0
    new-instance v0, Lcom/google/c/b/b/a/a/a/a;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/a/a;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/c/b/b/a/a/a/a;

    iput-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->d:Lcom/google/c/b/b/a/a/a/a;

    .line 249
    new-instance v0, Lcom/google/c/b/b/a/a/i;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/i;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/c/b/b/a/a/i;

    iput-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->e:Lcom/google/c/b/b/a/a/i;

    .line 250
    new-instance v0, Lcom/google/c/b/b/a/a/g;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/g;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/c/b/b/a/a/g;

    iput-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->f:Lcom/google/c/b/b/a/a/g;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    .line 255
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->g:J

    .line 256
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->h:J

    .line 257
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->i:J

    .line 258
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->j:J

    .line 259
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/rocket/impressions/l;->valueOf(Ljava/lang/String;)Lcom/google/android/libraries/rocket/impressions/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->c:Lcom/google/android/libraries/rocket/impressions/l;

    .line 260
    return-void

    .line 251
    :catch_0
    move-exception v0

    .line 252
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method constructor <init>(Lcom/google/android/libraries/rocket/impressions/f;Lcom/google/android/libraries/rocket/impressions/m;)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->b:Ljava/util/ArrayList;

    .line 62
    new-instance v0, Lcom/google/c/b/b/a/a/a/a;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/a/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->d:Lcom/google/c/b/b/a/a/a/a;

    .line 63
    new-instance v0, Lcom/google/c/b/b/a/a/i;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->e:Lcom/google/c/b/b/a/a/i;

    .line 65
    iget-object v0, p2, Lcom/google/android/libraries/rocket/impressions/m;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/google/android/libraries/rocket/impressions/m;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_1

    .line 67
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "must set a valid SessionType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->e:Lcom/google/c/b/b/a/a/i;

    iget-object v1, p2, Lcom/google/android/libraries/rocket/impressions/m;->a:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/google/c/b/b/a/a/i;->f:Ljava/lang/Integer;

    .line 71
    new-instance v0, Lcom/google/c/b/b/a/a/g;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/g;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->f:Lcom/google/c/b/b/a/a/g;

    .line 72
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->f:Lcom/google/c/b/b/a/a/g;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/g;->a:Ljava/lang/Integer;

    .line 74
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/rocket/impressions/Session;->a(Lcom/google/android/libraries/rocket/impressions/f;)V

    .line 82
    return-void
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 163
    if-eq p0, p1, :cond_0

    if-eqz p0, :cond_1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final a(J)J
    .locals 5

    .prologue
    .line 219
    iget-wide v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->g:J

    iput-wide v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->h:J

    .line 220
    iput-wide p1, p0, Lcom/google/android/libraries/rocket/impressions/Session;->i:J

    .line 221
    iget-wide v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->g:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/google/android/libraries/rocket/impressions/Session;->g:J

    return-wide v0
.end method

.method final a()Lcom/google/android/libraries/rocket/impressions/l;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->c:Lcom/google/android/libraries/rocket/impressions/l;

    return-object v0
.end method

.method final a(Lcom/google/android/libraries/rocket/impressions/f;)V
    .locals 4

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->e:Lcom/google/c/b/b/a/a/i;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/i;->a:Ljava/lang/String;

    .line 94
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->e:Lcom/google/c/b/b/a/a/i;

    invoke-interface {p1}, Lcom/google/android/libraries/rocket/impressions/f;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/i;->b:Ljava/lang/Long;

    .line 95
    invoke-interface {p1}, Lcom/google/android/libraries/rocket/impressions/f;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->j:J

    .line 96
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->g:J

    .line 97
    sget-object v0, Lcom/google/android/libraries/rocket/impressions/l;->a:Lcom/google/android/libraries/rocket/impressions/l;

    iput-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->c:Lcom/google/android/libraries/rocket/impressions/l;

    .line 98
    return-void
.end method

.method public final a(Lcom/google/android/libraries/rocket/impressions/l;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/libraries/rocket/impressions/Session;->c:Lcom/google/android/libraries/rocket/impressions/l;

    .line 110
    return-void
.end method

.method final a(Lcom/google/c/b/b/a/a/e;)V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    return-void
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 113
    iget-wide v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->h:J

    return-wide v0
.end method

.method final b(Lcom/google/android/libraries/rocket/impressions/f;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 226
    invoke-interface {p1}, Lcom/google/android/libraries/rocket/impressions/f;->a()J

    move-result-wide v2

    .line 230
    iget-wide v4, p0, Lcom/google/android/libraries/rocket/impressions/Session;->j:J

    cmp-long v1, v4, v2

    if-lez v1, :cond_1

    .line 238
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v4, p0, Lcom/google/android/libraries/rocket/impressions/Session;->j:J

    sub-long/2addr v2, v4

    sget-wide v4, Lcom/google/android/libraries/rocket/impressions/Session;->a:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 117
    iget-wide v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->i:J

    return-wide v0
.end method

.method final d()Lcom/google/c/b/b/a/a/f;
    .locals 3

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 122
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "cannot flush empty impression queue"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_0
    new-instance v1, Lcom/google/c/b/b/a/a/f;

    invoke-direct {v1}, Lcom/google/c/b/b/a/a/f;-><init>()V

    .line 126
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->d:Lcom/google/c/b/b/a/a/a/a;

    iput-object v0, v1, Lcom/google/c/b/b/a/a/f;->c:Lcom/google/c/b/b/a/a/a/a;

    .line 127
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->e:Lcom/google/c/b/b/a/a/i;

    iput-object v0, v1, Lcom/google/c/b/b/a/a/f;->b:Lcom/google/c/b/b/a/a/i;

    .line 128
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->f:Lcom/google/c/b/b/a/a/g;

    iput-object v0, v1, Lcom/google/c/b/b/a/a/f;->d:Lcom/google/c/b/b/a/a/g;

    .line 129
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->b:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/libraries/rocket/impressions/Session;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/c/b/b/a/a/e;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/c/b/b/a/a/e;

    iput-object v0, v1, Lcom/google/c/b/b/a/a/f;->a:[Lcom/google/c/b/b/a/a/e;

    .line 130
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 132
    return-object v1
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 243
    const/4 v0, 0x0

    return v0
.end method

.method public final e()J
    .locals 4

    .prologue
    .line 215
    iget-wide v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->g:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/google/android/libraries/rocket/impressions/Session;->g:J

    return-wide v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 140
    if-nez p1, :cond_1

    .line 149
    :cond_0
    :goto_0
    return v0

    .line 144
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 148
    check-cast p1, Lcom/google/android/libraries/rocket/impressions/Session;

    .line 149
    iget-object v1, p0, Lcom/google/android/libraries/rocket/impressions/Session;->b:Ljava/util/ArrayList;

    iget-object v2, p1, Lcom/google/android/libraries/rocket/impressions/Session;->b:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Lcom/google/android/libraries/rocket/impressions/Session;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/rocket/impressions/Session;->c:Lcom/google/android/libraries/rocket/impressions/l;

    iget-object v2, p1, Lcom/google/android/libraries/rocket/impressions/Session;->c:Lcom/google/android/libraries/rocket/impressions/l;

    invoke-static {v1, v2}, Lcom/google/android/libraries/rocket/impressions/Session;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/rocket/impressions/Session;->d:Lcom/google/c/b/b/a/a/a/a;

    invoke-static {v1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/libraries/rocket/impressions/Session;->d:Lcom/google/c/b/b/a/a/a/a;

    invoke-static {v2}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/rocket/impressions/Session;->e:Lcom/google/c/b/b/a/a/i;

    invoke-static {v1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/libraries/rocket/impressions/Session;->e:Lcom/google/c/b/b/a/a/i;

    invoke-static {v2}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/rocket/impressions/Session;->f:Lcom/google/c/b/b/a/a/g;

    invoke-static {v1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/libraries/rocket/impressions/Session;->f:Lcom/google/c/b/b/a/a/g;

    invoke-static {v2}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/libraries/rocket/impressions/Session;->g:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-wide v2, p1, Lcom/google/android/libraries/rocket/impressions/Session;->g:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/libraries/rocket/impressions/Session;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/libraries/rocket/impressions/Session;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-wide v2, p1, Lcom/google/android/libraries/rocket/impressions/Session;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/libraries/rocket/impressions/Session;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/libraries/rocket/impressions/Session;->i:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-wide v2, p1, Lcom/google/android/libraries/rocket/impressions/Session;->i:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/libraries/rocket/impressions/Session;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/libraries/rocket/impressions/Session;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-wide v2, p1, Lcom/google/android/libraries/rocket/impressions/Session;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/libraries/rocket/impressions/Session;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 182
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/io/Serializable;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/libraries/rocket/impressions/Session;->b:Ljava/util/ArrayList;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/libraries/rocket/impressions/Session;->c:Lcom/google/android/libraries/rocket/impressions/l;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/libraries/rocket/impressions/Session;->d:Lcom/google/c/b/b/a/a/a/a;

    invoke-static {v2}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/libraries/rocket/impressions/Session;->e:Lcom/google/c/b/b/a/a/i;

    invoke-static {v2}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/libraries/rocket/impressions/Session;->f:Lcom/google/c/b/b/a/a/g;

    invoke-static {v2}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/android/libraries/rocket/impressions/Session;->g:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/google/android/libraries/rocket/impressions/Session;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/google/android/libraries/rocket/impressions/Session;->i:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-wide v2, p0, Lcom/google/android/libraries/rocket/impressions/Session;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 199
    const-string v0, "Session[queue=%s, status=%s, cilentInfo=%s, sessionInfo=%s, systemInfo=%s, currSeqNum=%s, lastHeartbeatSeqNum=%s, lastHeartbeatClientTimeUsec=%s, sessionStartTimeNsec=%s]"

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/libraries/rocket/impressions/Session;->b:Ljava/util/ArrayList;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/libraries/rocket/impressions/Session;->c:Lcom/google/android/libraries/rocket/impressions/l;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/libraries/rocket/impressions/Session;->d:Lcom/google/c/b/b/a/a/a/a;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/libraries/rocket/impressions/Session;->e:Lcom/google/c/b/b/a/a/i;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/libraries/rocket/impressions/Session;->f:Lcom/google/c/b/b/a/a/g;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-wide v4, p0, Lcom/google/android/libraries/rocket/impressions/Session;->g:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-wide v4, p0, Lcom/google/android/libraries/rocket/impressions/Session;->i:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-wide v4, p0, Lcom/google/android/libraries/rocket/impressions/Session;->i:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget-wide v4, p0, Lcom/google/android/libraries/rocket/impressions/Session;->j:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 265
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "cannot writeToParcel with impression to be flush"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->d:Lcom/google/c/b/b/a/a/a/a;

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 269
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->e:Lcom/google/c/b/b/a/a/i;

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 270
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->f:Lcom/google/c/b/b/a/a/g;

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 271
    iget-wide v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->g:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 272
    iget-wide v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->h:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 273
    iget-wide v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->i:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 274
    iget-wide v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->j:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 275
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/Session;->c:Lcom/google/android/libraries/rocket/impressions/l;

    invoke-virtual {v0}, Lcom/google/android/libraries/rocket/impressions/l;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 276
    return-void
.end method
