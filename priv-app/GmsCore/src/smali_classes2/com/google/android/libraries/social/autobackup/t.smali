.class public Lcom/google/android/libraries/social/autobackup/t;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static a:Lcom/google/android/libraries/social/autobackup/t;


# instance fields
.field private final b:Landroid/content/Context;

.field private c:Z

.field private d:Ljava/lang/String;

.field private final e:Lcom/google/android/libraries/social/account/b;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/t;->b:Landroid/content/Context;

    .line 36
    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/t;->e:Lcom/google/android/libraries/social/account/b;

    .line 37
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/t;
    .locals 2

    .prologue
    .line 40
    const-class v1, Lcom/google/android/libraries/social/autobackup/t;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/t;->a:Lcom/google/android/libraries/social/autobackup/t;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/google/android/libraries/social/autobackup/t;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/social/autobackup/t;-><init>(Landroid/content/Context;)V

    .line 42
    sput-object v0, Lcom/google/android/libraries/social/autobackup/t;->a:Lcom/google/android/libraries/social/autobackup/t;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/t;->c()V

    .line 44
    :cond_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/t;->a:Lcom/google/android/libraries/social/autobackup/t;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/t;->c:Z

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/t;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final c()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/t;->b:Landroid/content/Context;

    const-string v2, "auto_upload_enabled"

    invoke-static {v0, v2}, Lcom/google/android/libraries/social/autobackup/AutoBackupProvider;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 61
    :goto_0
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/t;->b:Landroid/content/Context;

    const-string v3, "auto_upload_account_id"

    invoke-static {v2, v3}, Lcom/google/android/libraries/social/autobackup/AutoBackupProvider;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 64
    const/4 v2, 0x0

    .line 65
    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/t;->e:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v4, v3}, Lcom/google/android/libraries/social/account/b;->c(I)Z

    move-result v4

    if-nez v4, :cond_4

    move-object v0, v2

    .line 72
    :goto_1
    const-string v2, "iu.LegacyUploadSettings"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 73
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 74
    const-string v2, "#reloadSettings()"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "; account: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/libraries/social/g/a/f;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v4, "; IU: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    if-eqz v1, :cond_5

    const-string v2, "enabled"

    :goto_2
    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 77
    const-string v2, "iu.LegacyUploadSettings"

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    :cond_0
    const-string v2, "iu.LegacyUploadSettings"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 81
    iget-boolean v2, p0, Lcom/google/android/libraries/social/autobackup/t;->c:Z

    if-eq v1, v2, :cond_1

    .line 82
    const-string v2, "iu.LegacyUploadSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "   auto upload changed to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :cond_1
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/t;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 85
    const-string v2, "iu.LegacyUploadSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "   account changed from: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/t;->d:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/libraries/social/g/a/f;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " --> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/libraries/social/g/a/f;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    :cond_2
    iput-boolean v1, p0, Lcom/google/android/libraries/social/autobackup/t;->c:Z

    .line 91
    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/t;->d:Ljava/lang/String;

    .line 92
    return-void

    :cond_3
    move v0, v1

    .line 59
    goto/16 :goto_0

    .line 69
    :cond_4
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/t;->e:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v1, v3}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v1

    const-string v2, "account_name"

    invoke-interface {v1, v2}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    goto/16 :goto_1

    .line 74
    :cond_5
    const-string v2, "disabled"

    goto :goto_2
.end method
