.class public Lcom/google/android/libraries/social/i/an;
.super Landroid/preference/PreferenceActivity;
.source "SourceFile"


# instance fields
.field private a:I

.field protected final b:Lcom/google/android/libraries/social/i/i;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 18
    new-instance v0, Lcom/google/android/libraries/social/i/i;

    invoke-direct {v0}, Lcom/google/android/libraries/social/i/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/google/android/libraries/social/i/an;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/social/i/an;->a:I

    .line 160
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 153
    iget v0, p0, Lcom/google/android/libraries/social/i/an;->a:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/libraries/social/i/an;->a:I

    if-nez v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->o()V

    .line 156
    :cond_0
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    const/4 v0, 0x1

    .line 199
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public finish()V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->g()V

    .line 177
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->finish()V

    .line 178
    return-void
.end method

.method public onActionModeFinished(Landroid/view/ActionMode;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->i()V

    .line 191
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onActionModeFinished(Landroid/view/ActionMode;)V

    .line 192
    return-void
.end method

.method public onActionModeStarted(Landroid/view/ActionMode;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->h()V

    .line 184
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onActionModeStarted(Landroid/view/ActionMode;)V

    .line 185
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->s()V

    .line 98
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 99
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->n()Z

    move-result v0

    if-nez v0, :cond_0

    .line 229
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onBackPressed()V

    .line 231
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/i/i;->c(Landroid/os/Bundle;)V

    .line 28
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    const/4 v0, 0x1

    .line 207
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->c()V

    .line 86
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 87
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->e()V

    .line 165
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 166
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    const/4 v0, 0x1

    .line 223
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->b()V

    .line 68
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 69
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/i/i;->a(Landroid/os/Bundle;)V

    .line 34
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 35
    return-void
.end method

.method protected onPostResume()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->a()V

    .line 62
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPostResume()V

    .line 63
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    const/4 v0, 0x1

    .line 215
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/i/i;->b(Landroid/os/Bundle;)V

    .line 50
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 51
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->q()V

    .line 56
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 57
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/i/i;->d(Landroid/os/Bundle;)V

    .line 80
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 81
    return-void
.end method

.method public onStart()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 40
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 41
    invoke-virtual {p0}, Lcom/google/android/libraries/social/i/an;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/social/i/am;->a:Ljava/lang/reflect/Method;

    if-nez v1, :cond_0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "noteStateNotSaved"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    sput-object v1, Lcom/google/android/libraries/social/i/am;->a:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    :try_start_1
    sget-object v1, Lcom/google/android/libraries/social/i/am;->a:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_2

    .line 43
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->p()V

    .line 44
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStart()V

    .line 45
    return-void

    .line 41
    :catch_0
    move-exception v1

    invoke-static {}, Lcom/google/android/libraries/social/i/am;->a()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/google/android/libraries/social/i/am;->a()V

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-static {}, Lcom/google/android/libraries/social/i/am;->a()V

    goto :goto_1
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->r()V

    .line 74
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStop()V

    .line 75
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->f()V

    .line 171
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onUserLeaveHint()V

    .line 172
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/libraries/social/i/an;->b:Lcom/google/android/libraries/social/i/i;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/i;->d()V

    .line 92
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onWindowFocusChanged(Z)V

    .line 93
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/i/an;->a(Landroid/content/Intent;)V

    .line 104
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->startActivity(Landroid/content/Intent;)V

    .line 105
    invoke-direct {p0}, Lcom/google/android/libraries/social/i/an;->a()V

    .line 106
    return-void
.end method

.method public startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/i/an;->a(Landroid/content/Intent;)V

    .line 112
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 113
    invoke-direct {p0}, Lcom/google/android/libraries/social/i/an;->a()V

    .line 114
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 135
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/i/an;->a(Landroid/content/Intent;)V

    .line 136
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 137
    invoke-direct {p0}, Lcom/google/android/libraries/social/i/an;->a()V

    .line 138
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 143
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/i/an;->a(Landroid/content/Intent;)V

    .line 144
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceActivity;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 145
    invoke-direct {p0}, Lcom/google/android/libraries/social/i/an;->a()V

    .line 146
    return-void
.end method

.method public startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;I)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 119
    invoke-direct {p0, p2}, Lcom/google/android/libraries/social/i/an;->a(Landroid/content/Intent;)V

    .line 120
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceActivity;->startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;I)V

    .line 121
    invoke-direct {p0}, Lcom/google/android/libraries/social/i/an;->a()V

    .line 122
    return-void
.end method

.method public startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 128
    invoke-direct {p0, p2}, Lcom/google/android/libraries/social/i/an;->a(Landroid/content/Intent;)V

    .line 129
    invoke-super {p0, p1, p2, p3, p4}, Landroid/preference/PreferenceActivity;->startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 130
    invoke-direct {p0}, Lcom/google/android/libraries/social/i/an;->a()V

    .line 131
    return-void
.end method
