.class public final Lcom/google/android/libraries/social/autobackup/ap;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/autobackup/ao;


# static fields
.field private static final a:[Ljava/lang/String;

.field private static b:Lcom/google/android/libraries/social/autobackup/ap;


# instance fields
.field private final c:Landroid/content/Context;

.field private d:Z

.field private e:I

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:J

.field private l:J

.field private m:Z

.field private n:Z

.field private o:J

.field private p:I

.field private q:Lcom/google/android/libraries/social/mediaupload/w;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 29
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "auto_upload_enabled"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "auto_upload_account_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "sync_on_wifi_only"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "video_upload_wifi_only"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "sync_on_roaming"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "sync_on_battery"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "instant_share_eventid"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "instant_share_account_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "instant_share_starttime"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "instant_share_endtime"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "upload_full_resolution"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "max_mobile_upload_size"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "quota_limit"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "quota_used"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "full_size_disabled"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "quota_unlimited"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/libraries/social/autobackup/ap;->a:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/ap;->c:Landroid/content/Context;

    .line 69
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/ap;
    .locals 3

    .prologue
    .line 72
    const-class v1, Lcom/google/android/libraries/social/autobackup/ap;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/ap;->b:Lcom/google/android/libraries/social/autobackup/ap;

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Lcom/google/android/libraries/social/autobackup/ap;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/social/autobackup/ap;-><init>(Landroid/content/Context;)V

    .line 74
    sput-object v0, Lcom/google/android/libraries/social/autobackup/ap;->b:Lcom/google/android/libraries/social/autobackup/ap;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/autobackup/ap;->a(Landroid/database/Cursor;)V

    .line 76
    :cond_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/ap;->b:Lcom/google/android/libraries/social/autobackup/ap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/autobackup/ap;->a(Landroid/database/Cursor;)V

    .line 84
    return-void
.end method

.method final a(Landroid/database/Cursor;)V
    .locals 29

    .prologue
    .line 175
    if-nez p1, :cond_0

    .line 176
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/autobackup/ap;->m()Landroid/database/Cursor;

    move-result-object p1

    .line 180
    :cond_0
    if-eqz p1, :cond_1

    :try_start_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_3

    .line 181
    :cond_1
    const-string v4, "iu.UploadsManager"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 182
    const-string v4, "iu.UploadsManager"

    const-string v5, "failed to query system settings"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 215
    :cond_2
    invoke-static/range {p1 .. p1}, Lcom/google/android/libraries/social/g/a/f;->a(Landroid/database/Cursor;)V

    .line 298
    :goto_0
    return-void

    .line 186
    :cond_3
    :try_start_1
    const-string v4, "auto_upload_enabled"

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_10

    const/4 v5, 0x1

    .line 188
    :goto_1
    const-string v4, "auto_upload_account_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 190
    const-string v4, "sync_on_wifi_only"

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_11

    const/4 v4, 0x1

    move/from16 v18, v4

    .line 192
    :goto_2
    const-string v4, "video_upload_wifi_only"

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_12

    const/4 v4, 0x1

    move/from16 v17, v4

    .line 194
    :goto_3
    const-string v4, "sync_on_roaming"

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_13

    const/4 v4, 0x1

    move/from16 v16, v4

    .line 195
    :goto_4
    const-string v4, "sync_on_battery"

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_14

    const/4 v4, 0x1

    move v15, v4

    .line 196
    :goto_5
    const-string v4, "instant_share_eventid"

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 197
    const-string v6, "instant_share_account_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 199
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v7

    if-eqz v7, :cond_15

    const/4 v6, -0x1

    move v14, v6

    .line 201
    :goto_6
    const-string v6, "instant_share_starttime"

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 203
    const-string v6, "instant_share_endtime"

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 204
    const-string v6, "upload_full_resolution"

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    if-eqz v6, :cond_16

    const/4 v6, 0x1

    move v13, v6

    .line 206
    :goto_7
    const-string v6, "max_mobile_upload_size"

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    .line 208
    const-string v6, "quota_limit"

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 209
    const-string v8, "quota_used"

    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 210
    const-string v10, "quota_unlimited"

    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    if-eqz v10, :cond_17

    const/4 v10, 0x1

    .line 212
    :goto_8
    const-string v11, "full_size_disabled"

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v11

    if-eqz v11, :cond_18

    const/4 v11, 0x1

    .line 215
    :goto_9
    invoke-static/range {p1 .. p1}, Lcom/google/android/libraries/social/g/a/f;->a(Landroid/database/Cursor;)V

    .line 218
    const/4 v12, -0x1

    move/from16 v0, v19

    if-ne v0, v12, :cond_4

    .line 220
    const/4 v5, 0x0

    .line 221
    const/4 v4, 0x0

    .line 224
    :cond_4
    const-string v12, "iu.UploadsManager"

    const/16 v26, 0x4

    move/from16 v0, v26

    invoke-static {v12, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 225
    new-instance v26, Ljava/lang/StringBuffer;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuffer;-><init>()V

    .line 226
    const-string v12, "#reloadSettings()"

    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "; account: "

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v27, "; IU: "

    move-object/from16 v0, v27

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v27

    if-eqz v5, :cond_19

    const-string v12, "enabled"

    :goto_a
    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v27, "; IS: "

    move-object/from16 v0, v27

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v27

    if-eqz v4, :cond_1a

    move-object v12, v4

    :goto_b
    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "; IS account: "

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 231
    if-eqz v4, :cond_5

    .line 232
    const-string v12, "MMM dd, yyyy h:mmaa"

    .line 233
    new-instance v27, Ljava/util/Date;

    move-object/from16 v0, v27

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v27

    invoke-static {v12, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    .line 235
    new-instance v28, Ljava/util/Date;

    move-object/from16 v0, v28

    move-wide/from16 v1, v22

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v28

    invoke-static {v12, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v12

    .line 237
    const-string v28, " (start: "

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuffer;

    move-result-object v27

    const-string v28, ", end: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v27, ")"

    move-object/from16 v0, v27

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 241
    :cond_5
    const-string v12, "; photoWiFi: "

    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v27, "; videoWiFi: "

    move-object/from16 v0, v27

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v27, "; roam: "

    move-object/from16 v0, v27

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    move/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v27, "; battery: "

    move-object/from16 v0, v27

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12, v15}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v27, "; size: "

    move-object/from16 v0, v27

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v27

    if-eqz v13, :cond_1b

    const-string v12, "FULL"

    :goto_c
    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v27, "; maxMobile: "

    move-object/from16 v0, v27

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    move-wide/from16 v0, v24

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 247
    const-string v12, "iu.UploadsManager"

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-static {v12, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    :cond_6
    const-string v12, "iu.UploadsManager"

    const/16 v26, 0x3

    move/from16 v0, v26

    invoke-static {v12, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_f

    .line 251
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/libraries/social/autobackup/ap;->d:Z

    if-eq v5, v12, :cond_7

    .line 252
    const-string v12, "iu.UploadsManager"

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "   auto upload changed to "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-static {v12, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    :cond_7
    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/libraries/social/autobackup/ap;->e:I

    move/from16 v0, v19

    if-eq v0, v12, :cond_8

    .line 255
    const-string v12, "iu.UploadsManager"

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "   account changed from: "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/libraries/social/autobackup/ap;->e:I

    move/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " --> "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-static {v12, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    :cond_8
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/libraries/social/autobackup/ap;->j:Ljava/lang/String;

    invoke-static {v4, v12}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_9

    .line 259
    const-string v26, "iu.UploadsManager"

    new-instance v27, Ljava/lang/StringBuilder;

    const-string v12, "   event ID changed from: "

    move-object/from16 v0, v27

    invoke-direct {v0, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/libraries/social/autobackup/ap;->j:Ljava/lang/String;

    if-nez v12, :cond_1c

    const-string v12, "<< NULL >>"

    :goto_d
    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v27, " --> "

    move-object/from16 v0, v27

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    if-nez v4, :cond_1d

    const-string v12, "<< NULL >>"

    :goto_e
    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, v26

    invoke-static {v0, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/libraries/social/autobackup/ap;->f:Z

    move/from16 v0, v18

    if-eq v0, v12, :cond_a

    .line 264
    const-string v12, "iu.UploadsManager"

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "   wifiOnlyPhoto changed to "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-static {v12, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/libraries/social/autobackup/ap;->g:Z

    move/from16 v0, v17

    if-eq v0, v12, :cond_b

    .line 267
    const-string v12, "iu.UploadsManager"

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "   wifiOnlyVideo changed to "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-static {v12, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/libraries/social/autobackup/ap;->h:Z

    move/from16 v0, v16

    if-eq v0, v12, :cond_c

    .line 270
    const-string v12, "iu.UploadsManager"

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "   syncOnRoaming changed to "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-static {v12, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/libraries/social/autobackup/ap;->i:Z

    if-eq v15, v12, :cond_d

    .line 273
    const-string v12, "iu.UploadsManager"

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "   syncOnBattery changed to "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-static {v12, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    :cond_d
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/libraries/social/autobackup/ap;->n:Z

    if-eq v13, v12, :cond_e

    .line 276
    const-string v12, "iu.UploadsManager"

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "   uploadFullRes changed to "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-static {v12, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    :cond_e
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/libraries/social/autobackup/ap;->o:J

    move-wide/from16 v26, v0

    cmp-long v12, v24, v26

    if-eqz v12, :cond_f

    .line 279
    const-string v12, "iu.UploadsManager"

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "   maxMobileUploadSize changed to "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-static {v12, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    :cond_f
    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/google/android/libraries/social/autobackup/ap;->d:Z

    .line 284
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/libraries/social/autobackup/ap;->e:I

    .line 285
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/libraries/social/autobackup/ap;->f:Z

    .line 286
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/libraries/social/autobackup/ap;->g:Z

    .line 287
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/libraries/social/autobackup/ap;->h:Z

    .line 288
    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/google/android/libraries/social/autobackup/ap;->i:Z

    .line 289
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/libraries/social/autobackup/ap;->j:Ljava/lang/String;

    .line 290
    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/libraries/social/autobackup/ap;->p:I

    .line 291
    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/libraries/social/autobackup/ap;->k:J

    .line 292
    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/libraries/social/autobackup/ap;->l:J

    .line 293
    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/google/android/libraries/social/autobackup/ap;->n:Z

    .line 294
    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/libraries/social/autobackup/ap;->o:J

    .line 295
    new-instance v5, Lcom/google/android/libraries/social/mediaupload/w;

    invoke-direct/range {v5 .. v11}, Lcom/google/android/libraries/social/mediaupload/w;-><init>(JJZZ)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/libraries/social/autobackup/ap;->q:Lcom/google/android/libraries/social/mediaupload/w;

    .line 297
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/libraries/social/autobackup/ap;->m:Z

    goto/16 :goto_0

    .line 186
    :cond_10
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 190
    :cond_11
    const/4 v4, 0x0

    move/from16 v18, v4

    goto/16 :goto_2

    .line 192
    :cond_12
    const/4 v4, 0x0

    move/from16 v17, v4

    goto/16 :goto_3

    .line 194
    :cond_13
    const/4 v4, 0x0

    move/from16 v16, v4

    goto/16 :goto_4

    .line 195
    :cond_14
    const/4 v4, 0x0

    move v15, v4

    goto/16 :goto_5

    .line 199
    :cond_15
    :try_start_2
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v6

    move v14, v6

    goto/16 :goto_6

    .line 204
    :cond_16
    const/4 v6, 0x0

    move v13, v6

    goto/16 :goto_7

    .line 210
    :cond_17
    const/4 v10, 0x0

    goto/16 :goto_8

    .line 212
    :cond_18
    const/4 v11, 0x0

    goto/16 :goto_9

    .line 215
    :catchall_0
    move-exception v4

    invoke-static/range {p1 .. p1}, Lcom/google/android/libraries/social/g/a/f;->a(Landroid/database/Cursor;)V

    throw v4

    .line 226
    :cond_19
    const-string v12, "disabled"

    goto/16 :goto_a

    :cond_1a
    const-string v12, "disabled"

    goto/16 :goto_b

    .line 241
    :cond_1b
    const-string v12, "STANDARD"

    goto/16 :goto_c

    .line 259
    :cond_1c
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/libraries/social/autobackup/ap;->j:Ljava/lang/String;

    goto/16 :goto_d

    :cond_1d
    move-object v12, v4

    goto/16 :goto_e
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/ap;->f:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/ap;->g:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/ap;->h:Z

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/ap;->i:Z

    return v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ap;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/google/android/libraries/social/autobackup/ap;->p:I

    return v0
.end method

.method public final h()J
    .locals 2

    .prologue
    .line 133
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/ap;->k:J

    return-wide v0
.end method

.method public final i()J
    .locals 2

    .prologue
    .line 138
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/ap;->l:J

    return-wide v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/ap;->n:Z

    return v0
.end method

.method public final k()J
    .locals 2

    .prologue
    .line 148
    iget-wide v0, p0, Lcom/google/android/libraries/social/autobackup/ap;->o:J

    return-wide v0
.end method

.method public final l()Lcom/google/android/libraries/social/mediaupload/w;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ap;->q:Lcom/google/android/libraries/social/mediaupload/w;

    return-object v0
.end method

.method final m()Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 301
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ap;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/ap;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/libraries/social/autobackup/aj;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/libraries/social/autobackup/ap;->a:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
