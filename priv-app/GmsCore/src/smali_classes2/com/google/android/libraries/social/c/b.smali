.class public final Lcom/google/android/libraries/social/c/b;
.super Landroid/database/AbstractCursor;
.source "SourceFile"


# instance fields
.field private final a:[Ljava/lang/String;

.field private b:[Ljava/lang/Object;

.field private c:I

.field private final d:I

.field private final e:Landroid/os/Bundle;


# direct methods
.method private constructor <init>([Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/social/c/b;->c:I

    .line 40
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/c/b;->e:Landroid/os/Bundle;

    .line 60
    iput-object p1, p0, Lcom/google/android/libraries/social/c/b;->a:[Ljava/lang/String;

    .line 61
    array-length v0, p1

    iput v0, p0, Lcom/google/android/libraries/social/c/b;->d:I

    .line 63
    iget v0, p0, Lcom/google/android/libraries/social/c/b;->d:I

    mul-int/lit8 v0, v0, 0x10

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/libraries/social/c/b;->b:[Ljava/lang/Object;

    .line 68
    return-void
.end method

.method public constructor <init>([Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/c/b;-><init>([Ljava/lang/String;)V

    .line 78
    return-void
.end method

.method private a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 84
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/google/android/libraries/social/c/b;->d:I

    if-lt p1, v0, :cond_1

    .line 85
    :cond_0
    new-instance v0, Landroid/database/CursorIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Requested column: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", # of columns: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/libraries/social/c/b;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/CursorIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88
    :cond_1
    iget v0, p0, Lcom/google/android/libraries/social/c/b;->mPos:I

    if-gez v0, :cond_2

    .line 89
    new-instance v0, Landroid/database/CursorIndexOutOfBoundsException;

    const-string v1, "Before first row."

    invoke-direct {v0, v1}, Landroid/database/CursorIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_2
    iget v0, p0, Lcom/google/android/libraries/social/c/b;->mPos:I

    iget v1, p0, Lcom/google/android/libraries/social/c/b;->c:I

    if-lt v0, v1, :cond_3

    .line 92
    new-instance v0, Landroid/database/CursorIndexOutOfBoundsException;

    const-string v1, "After last row."

    invoke-direct {v0, v1}, Landroid/database/CursorIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/c/b;->b:[Ljava/lang/Object;

    iget v1, p0, Lcom/google/android/libraries/social/c/b;->mPos:I

    iget v2, p0, Lcom/google/android/libraries/social/c/b;->d:I

    mul-int/2addr v1, v2

    add-int/2addr v1, p1

    aget-object v0, v0, v1

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/libraries/social/c/b;)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/libraries/social/c/b;->b:[Ljava/lang/Object;

    return-object v0
.end method

.method private b(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 190
    iget-object v0, p0, Lcom/google/android/libraries/social/c/b;->b:[Ljava/lang/Object;

    array-length v0, v0

    if-le p1, v0, :cond_0

    .line 191
    iget-object v1, p0, Lcom/google/android/libraries/social/c/b;->b:[Ljava/lang/Object;

    .line 192
    iget-object v0, p0, Lcom/google/android/libraries/social/c/b;->b:[Ljava/lang/Object;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    .line 193
    if-ge v0, p1, :cond_1

    .line 196
    :goto_0
    new-array v0, p1, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/libraries/social/c/b;->b:[Ljava/lang/Object;

    .line 197
    iget-object v0, p0, Lcom/google/android/libraries/social/c/b;->b:[Ljava/lang/Object;

    array-length v2, v1

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 199
    :cond_0
    return-void

    :cond_1
    move p1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/social/c/c;
    .locals 3

    .prologue
    .line 105
    iget v0, p0, Lcom/google/android/libraries/social/c/b;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/social/c/b;->c:I

    .line 106
    iget v0, p0, Lcom/google/android/libraries/social/c/b;->c:I

    iget v1, p0, Lcom/google/android/libraries/social/c/b;->d:I

    mul-int/2addr v0, v1

    .line 107
    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/c/b;->b(I)V

    .line 108
    iget v1, p0, Lcom/google/android/libraries/social/c/b;->d:I

    sub-int v1, v0, v1

    .line 109
    new-instance v2, Lcom/google/android/libraries/social/c/c;

    invoke-direct {v2, p0, v1, v0}, Lcom/google/android/libraries/social/c/c;-><init>(Lcom/google/android/libraries/social/c/b;II)V

    return-object v2
.end method

.method public final a([Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 122
    array-length v0, p1

    iget v1, p0, Lcom/google/android/libraries/social/c/b;->d:I

    if-eq v0, v1, :cond_0

    .line 123
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "columnNames.length = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/libraries/social/c/b;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", columnValues.length = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_0
    iget v0, p0, Lcom/google/android/libraries/social/c/b;->c:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/libraries/social/c/b;->c:I

    iget v1, p0, Lcom/google/android/libraries/social/c/b;->d:I

    mul-int/2addr v0, v1

    .line 129
    iget v1, p0, Lcom/google/android/libraries/social/c/b;->d:I

    add-int/2addr v1, v0

    invoke-direct {p0, v1}, Lcom/google/android/libraries/social/c/b;->b(I)V

    .line 130
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/libraries/social/c/b;->b:[Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/libraries/social/c/b;->d:I

    invoke-static {p1, v1, v2, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 131
    return-void
.end method

.method public final getBlob(I)[B
    .locals 1

    .prologue
    .line 295
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/c/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 296
    check-cast v0, [B

    return-object v0
.end method

.method public final getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/libraries/social/c/b;->a:[Ljava/lang/String;

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 238
    iget v0, p0, Lcom/google/android/libraries/social/c/b;->c:I

    return v0
.end method

.method public final getDouble(I)D
    .locals 2

    .prologue
    .line 287
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/c/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 288
    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    .line 290
    :goto_0
    return-wide v0

    .line 289
    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    goto :goto_0

    .line 290
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    goto :goto_0
.end method

.method public final getExtras()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/libraries/social/c/b;->e:Landroid/os/Bundle;

    return-object v0
.end method

.method public final getFloat(I)F
    .locals 2

    .prologue
    .line 279
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/c/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 280
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 282
    :goto_0
    return v0

    .line 281
    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    goto :goto_0

    .line 282
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    goto :goto_0
.end method

.method public final getInt(I)I
    .locals 2

    .prologue
    .line 263
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/c/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 264
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 266
    :goto_0
    return v0

    .line 265
    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    goto :goto_0

    .line 266
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final getLong(I)J
    .locals 2

    .prologue
    .line 271
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/c/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 272
    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    .line 274
    :goto_0
    return-wide v0

    .line 273
    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    goto :goto_0

    .line 274
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final getShort(I)S
    .locals 2

    .prologue
    .line 255
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/c/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 256
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 258
    :goto_0
    return v0

    .line 257
    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->shortValue()S

    move-result v0

    goto :goto_0

    .line 258
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    move-result v0

    goto :goto_0
.end method

.method public final getString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/c/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 249
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 250
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getType(I)I
    .locals 2

    .prologue
    .line 301
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/c/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 302
    if-nez v0, :cond_0

    .line 303
    const/4 v0, 0x0

    .line 311
    :goto_0
    return v0

    .line 304
    :cond_0
    instance-of v1, v0, [B

    if-eqz v1, :cond_1

    .line 305
    const/4 v0, 0x4

    goto :goto_0

    .line 306
    :cond_1
    instance-of v1, v0, Ljava/lang/Float;

    if-nez v1, :cond_2

    instance-of v1, v0, Ljava/lang/Double;

    if-eqz v1, :cond_3

    .line 307
    :cond_2
    const/4 v0, 0x2

    goto :goto_0

    .line 308
    :cond_3
    instance-of v1, v0, Ljava/lang/Long;

    if-nez v1, :cond_4

    instance-of v0, v0, Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 309
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 311
    :cond_5
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public final isNull(I)Z
    .locals 1

    .prologue
    .line 317
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/c/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
