.class public final Lcom/google/android/libraries/social/p/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/p/c;


# instance fields
.field private final a:Lcom/google/android/libraries/social/p/d;

.field private final b:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Lcom/google/android/libraries/social/p/d;

    invoke-direct {v0}, Lcom/google/android/libraries/social/p/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/p/a;->a:Lcom/google/android/libraries/social/p/d;

    .line 16
    iput-object p1, p0, Lcom/google/android/libraries/social/p/a;->b:Ljava/lang/Object;

    .line 17
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 39
    iget-object v1, p0, Lcom/google/android/libraries/social/p/a;->a:Lcom/google/android/libraries/social/p/d;

    iget-object v0, p0, Lcom/google/android/libraries/social/p/a;->b:Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, v1, Lcom/google/android/libraries/social/p/d;->a:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/p/e;

    iget-object v3, v1, Lcom/google/android/libraries/social/p/d;->a:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Lcom/google/android/libraries/social/p/e;->am_()V

    goto :goto_0

    .line 40
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/libraries/social/p/e;)V
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/libraries/social/p/a;->a:Lcom/google/android/libraries/social/p/d;

    iget-object v0, v0, Lcom/google/android/libraries/social/p/d;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 22
    return-void
.end method

.method public final b(Lcom/google/android/libraries/social/p/e;)V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/libraries/social/p/a;->a:Lcom/google/android/libraries/social/p/d;

    iget-object v0, v0, Lcom/google/android/libraries/social/p/d;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 30
    return-void
.end method
