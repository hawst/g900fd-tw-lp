.class public Lcom/google/android/libraries/commerce/ocr/capture/DecoratingOcrHandler;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;


# instance fields
.field protected final internalHandler:Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/capture/DecoratingOcrHandler;->internalHandler:Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;

    .line 19
    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/libraries/commerce/ocr/OcrException;)V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/DecoratingOcrHandler;->internalHandler:Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;->onError(Lcom/google/android/libraries/commerce/ocr/OcrException;)V

    .line 44
    return-void
.end method

.method public onOcrAttempt()Z
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/DecoratingOcrHandler;->internalHandler:Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;->onOcrAttempt()Z

    move-result v0

    return v0
.end method

.method public onRecognized(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/DecoratingOcrHandler;->internalHandler:Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;->onRecognized(Ljava/lang/Object;)V

    .line 24
    return-void
.end method

.method public onRecognized(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/DecoratingOcrHandler;->internalHandler:Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;

    invoke-interface {v0, p1, p2}, Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;->onRecognized(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 29
    return-void
.end method

.method public onUnrecognized()V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/DecoratingOcrHandler;->internalHandler:Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;->onUnrecognized()V

    .line 34
    return-void
.end method

.method public onUnrecognized(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/DecoratingOcrHandler;->internalHandler:Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;->onUnrecognized(Ljava/lang/Object;)V

    .line 39
    return-void
.end method
