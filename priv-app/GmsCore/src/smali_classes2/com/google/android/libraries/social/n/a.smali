.class public Lcom/google/android/libraries/social/n/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static volatile a:Le/a/a/h;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;ILjava/util/Map;Le/a/a/l;)Le/a/a/g;
    .locals 1

    .prologue
    .line 44
    invoke-static {p0}, Lcom/google/android/libraries/social/n/a;->a(Landroid/content/Context;)Le/a/a/h;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Le/a/a/h;->a(Ljava/lang/String;ILjava/util/Map;Le/a/a/l;)Le/a/a/g;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;ILjava/util/Map;Ljava/nio/channels/WritableByteChannel;Le/a/a/l;)Le/a/a/g;
    .locals 1

    .prologue
    .line 53
    invoke-static {p0}, Lcom/google/android/libraries/social/n/a;->a(Landroid/content/Context;)Le/a/a/h;

    move-result-object v0

    invoke-virtual {v0, p1, p3, p4, p5}, Le/a/a/h;->a(Ljava/lang/String;Ljava/util/Map;Ljava/nio/channels/WritableByteChannel;Le/a/a/l;)Le/a/a/g;

    move-result-object v0

    return-object v0
.end method

.method private static declared-synchronized a(Landroid/content/Context;)Le/a/a/h;
    .locals 3

    .prologue
    .line 27
    const-class v1, Lcom/google/android/libraries/social/n/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/libraries/social/n/a;->a:Le/a/a/h;

    if-nez v0, :cond_1

    .line 28
    const-class v2, Lcom/google/android/libraries/social/n/a;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 29
    :try_start_1
    sget-object v0, Lcom/google/android/libraries/social/n/a;->a:Le/a/a/h;

    if-nez v0, :cond_0

    .line 30
    const-class v0, Le/a/a/i;

    invoke-static {p0, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Le/a/a/i;

    invoke-static {p0, v0}, Le/a/a/h;->a(Landroid/content/Context;Le/a/a/i;)Le/a/a/h;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/n/a;->a:Le/a/a/h;

    .line 33
    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 35
    :cond_1
    :try_start_2
    sget-object v0, Lcom/google/android/libraries/social/n/a;->a:Le/a/a/h;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit v1

    return-object v0

    .line 33
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 27
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method
