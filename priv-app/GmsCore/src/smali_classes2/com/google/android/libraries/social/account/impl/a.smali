.class public final Lcom/google/android/libraries/social/account/impl/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/account/b;


# instance fields
.field private final a:Landroid/content/SharedPreferences;

.field private b:Ljava/util/List;

.field private final c:Ljava/util/List;

.field private final d:Ljava/util/List;

.field private e:Ljava/util/List;

.field private final f:Landroid/content/Context;

.field private g:Z

.field private h:Z

.field private final i:Landroid/util/SparseArray;

.field private final j:Landroid/util/SparseArray;

.field private final k:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 73
    const-string v0, "accounts"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/account/impl/a;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    .line 74
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->c:Ljava/util/List;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->d:Ljava/util/List;

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/account/impl/a;->g:Z

    .line 60
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->i:Landroid/util/SparseArray;

    .line 62
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->j:Landroid/util/SparseArray;

    .line 65
    new-instance v0, Lcom/google/android/libraries/social/account/impl/b;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/social/account/impl/b;-><init>(Lcom/google/android/libraries/social/account/impl/a;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->k:Ljava/lang/Runnable;

    .line 82
    iput-object p1, p0, Lcom/google/android/libraries/social/account/impl/a;->f:Landroid/content/Context;

    .line 83
    iput-object p2, p0, Lcom/google/android/libraries/social/account/impl/a;->a:Landroid/content/SharedPreferences;

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->b:Ljava/util/List;

    .line 85
    return-void
.end method

.method private static a(Landroid/util/SparseArray;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 123
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 124
    invoke-virtual {p0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/c;

    .line 125
    const-string v2, "account_name"

    invoke-interface {v0, v2}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "effective_gaia_id"

    invoke-interface {v0, v2}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    invoke-virtual {p0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    .line 131
    :goto_1
    return v0

    .line 123
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 131
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private a(ILcom/google/android/libraries/social/account/d;)V
    .locals 6

    .prologue
    .line 322
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 323
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 324
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 325
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 326
    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 327
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 328
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 330
    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 331
    check-cast v0, Ljava/lang/String;

    invoke-interface {p2, v1, v0}, Lcom/google/android/libraries/social/account/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/account/d;

    goto :goto_0

    .line 332
    :cond_1
    instance-of v4, v0, Ljava/lang/Boolean;

    if-eqz v4, :cond_2

    .line 333
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-interface {p2, v1, v0}, Lcom/google/android/libraries/social/account/d;->a(Ljava/lang/String;Z)Lcom/google/android/libraries/social/account/d;

    goto :goto_0

    .line 334
    :cond_2
    instance-of v4, v0, Ljava/lang/Integer;

    if-eqz v4, :cond_3

    .line 335
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p2, v1, v0}, Lcom/google/android/libraries/social/account/d;->a(Ljava/lang/String;I)Lcom/google/android/libraries/social/account/d;

    goto :goto_0

    .line 336
    :cond_3
    instance-of v4, v0, Ljava/lang/Long;

    if-eqz v4, :cond_4

    .line 337
    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {p2, v1, v4, v5}, Lcom/google/android/libraries/social/account/d;->a(Ljava/lang/String;J)Lcom/google/android/libraries/social/account/d;

    goto :goto_0

    .line 338
    :cond_4
    instance-of v4, v0, Ljava/lang/Float;

    if-eqz v4, :cond_0

    .line 339
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-interface {p2, v1, v0}, Lcom/google/android/libraries/social/account/d;->a(Ljava/lang/String;F)Lcom/google/android/libraries/social/account/d;

    goto :goto_0

    .line 343
    :cond_5
    return-void
.end method

.method static synthetic a(Lcom/google/android/libraries/social/account/impl/a;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/libraries/social/account/impl/a;->g()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/libraries/social/account/impl/a;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->a:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private b()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x1

    .line 90
    iget-boolean v0, p0, Lcom/google/android/libraries/social/account/impl/a;->h:Z

    if-nez v0, :cond_a

    .line 91
    iput-boolean v8, p0, Lcom/google/android/libraries/social/account/impl/a;->h:Z

    .line 93
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->f:Landroid/content/Context;

    const-class v2, Lcom/google/android/libraries/social/account/h;

    invoke-static {v0, v2}, Lcom/google/android/libraries/social/a/a;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->b:Ljava/util/List;

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->d:Ljava/util/List;

    new-instance v2, Lcom/google/android/libraries/social/account/impl/c;

    invoke-direct {v2, p0}, Lcom/google/android/libraries/social/account/impl/c;-><init>(Lcom/google/android/libraries/social/account/impl/a;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/libraries/social/account/impl/d;

    invoke-direct {v2, p0}, Lcom/google/android/libraries/social/account/impl/d;-><init>(Lcom/google/android/libraries/social/account/impl/a;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/h;

    .line 99
    iget-object v3, p0, Lcom/google/android/libraries/social/account/impl/a;->d:Ljava/util/List;

    invoke-interface {v0, v3}, Lcom/google/android/libraries/social/account/h;->a(Ljava/util/List;)V

    goto :goto_0

    .line 101
    :cond_1
    const-string v0, "AccountStore#upgradeAccountCreated"

    iget-object v2, p0, Lcom/google/android/libraries/social/account/impl/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v2, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/libraries/social/account/impl/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2, v0, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->a:Landroid/content/SharedPreferences;

    const-string v3, "count"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_3

    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/a;->a:Landroid/content/SharedPreferences;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".gaia_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ".created"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_4
    invoke-direct {p0}, Lcom/google/android/libraries/social/account/impl/a;->f()V

    invoke-virtual {p0}, Lcom/google/android/libraries/social/account/impl/a;->a()Ljava/util/List;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/j;

    invoke-interface {v0}, Lcom/google/android/libraries/social/account/j;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/account/impl/a;->b(I)Lcom/google/android/libraries/social/account/d;

    move-result-object v0

    invoke-interface {v0, v3, v8}, Lcom/google/android/libraries/social/account/d;->a(Ljava/lang/String;Z)Lcom/google/android/libraries/social/account/d;

    invoke-interface {v0}, Lcom/google/android/libraries/social/account/d;->c()I

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_2

    :cond_7
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/account/impl/a;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_9
    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/j;

    invoke-interface {v0}, Lcom/google/android/libraries/social/account/j;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Lcom/google/android/libraries/social/account/c;->a(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_9

    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/account/impl/a;->b(I)Lcom/google/android/libraries/social/account/d;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/libraries/social/account/impl/a;->f:Landroid/content/Context;

    invoke-interface {v0, v7, v6, v3}, Lcom/google/android/libraries/social/account/j;->a(Landroid/content/Context;Lcom/google/android/libraries/social/account/f;Lcom/google/android/libraries/social/account/c;)V

    invoke-interface {v6, v5, v8}, Lcom/google/android/libraries/social/account/d;->a(Ljava/lang/String;Z)Lcom/google/android/libraries/social/account/d;

    invoke-interface {v6}, Lcom/google/android/libraries/social/account/d;->c()I

    goto :goto_4

    .line 103
    :cond_a
    return-void
.end method

.method private declared-synchronized c(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 117
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/account/impl/a;->e()V

    .line 118
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->j:Landroid/util/SparseArray;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/google/android/libraries/social/account/impl/a;->a(Landroid/util/SparseArray;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c(Ljava/lang/String;)Lcom/google/android/libraries/social/account/d;
    .locals 7

    .prologue
    .line 242
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/account/impl/a;->b()V

    .line 244
    invoke-direct {p0}, Lcom/google/android/libraries/social/account/impl/a;->d()I

    move-result v3

    .line 245
    const-string v0, "AccountStore"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    const-string v0, "AccountStore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Creating account "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    :cond_0
    new-instance v0, Lcom/google/android/libraries/social/account/impl/h;

    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const/4 v5, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/libraries/social/account/impl/h;-><init>(Lcom/google/android/libraries/social/account/impl/a;Landroid/content/SharedPreferences$Editor;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    const-string v1, "created"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/libraries/social/account/d;->a(Ljava/lang/String;Z)Lcom/google/android/libraries/social/account/d;

    .line 252
    const-string v1, "account_name"

    invoke-interface {v0, v1, p1}, Lcom/google/android/libraries/social/account/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/account/d;

    .line 253
    const-string v1, "effective_gaia_id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/libraries/social/account/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/account/d;

    .line 254
    const-string v1, "is_managed_account"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/libraries/social/account/d;->a(Ljava/lang/String;Z)Lcom/google/android/libraries/social/account/d;

    .line 256
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 257
    iget-object v2, p0, Lcom/google/android/libraries/social/account/impl/a;->f:Landroid/content/Context;

    goto :goto_0

    .line 260
    :cond_1
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/google/android/libraries/social/account/impl/a;->c(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 261
    const/4 v1, -0x1

    if-eq v2, v1, :cond_3

    .line 262
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/a;->j:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 264
    invoke-direct {p0, v2, v0}, Lcom/google/android/libraries/social/account/impl/a;->a(ILcom/google/android/libraries/social/account/d;)V

    .line 265
    const-string v1, "tombstoned"

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/d;->e(Ljava/lang/String;)Lcom/google/android/libraries/social/account/d;

    .line 267
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/social/account/h;

    .line 268
    iget-object v4, p0, Lcom/google/android/libraries/social/account/impl/a;->f:Landroid/content/Context;

    invoke-interface {v1, v0}, Lcom/google/android/libraries/social/account/h;->a(Lcom/google/android/libraries/social/account/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 242
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 270
    :cond_2
    :try_start_1
    invoke-direct {p0, v2}, Lcom/google/android/libraries/social/account/impl/a;->g(I)V

    .line 274
    :cond_3
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/a;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/social/account/j;

    .line 275
    invoke-interface {v1}, Lcom/google/android/libraries/social/account/j;->a()Ljava/lang/String;

    move-result-object v1

    .line 276
    const/4 v3, 0x1

    invoke-interface {v0, v1, v3}, Lcom/google/android/libraries/social/account/d;->a(Ljava/lang/String;Z)Lcom/google/android/libraries/social/account/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 279
    :cond_4
    monitor-exit p0

    return-object v0
.end method

.method private declared-synchronized c()Ljava/util/List;
    .locals 2

    .prologue
    .line 381
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->e:Ljava/util/List;

    if-nez v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->f:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/account/a;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->e:Ljava/util/List;

    .line 384
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->e:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 381
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c(Lcom/google/android/libraries/social/account/impl/a;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/libraries/social/account/impl/a;->f()V

    invoke-direct {p0}, Lcom/google/android/libraries/social/account/impl/a;->g()V

    return-void
.end method

.method private declared-synchronized d()I
    .locals 4

    .prologue
    .line 479
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->a:Landroid/content/SharedPreferences;

    const-string v1, "count"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 480
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "count"

    add-int/lit8 v3, v0, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 481
    monitor-exit p0

    return v0

    .line 479
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic d(Lcom/google/android/libraries/social/account/impl/a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/libraries/social/account/impl/a;->c()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private e()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 485
    iget-boolean v0, p0, Lcom/google/android/libraries/social/account/impl/a;->g:Z

    if-nez v0, :cond_0

    .line 508
    :goto_0
    return-void

    .line 489
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/account/impl/a;->b()V

    .line 491
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->a:Landroid/content/SharedPreferences;

    const-string v2, "count"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 493
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->i:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    move v0, v1

    .line 494
    :goto_1
    if-ge v0, v2, :cond_2

    .line 495
    iget-object v3, p0, Lcom/google/android/libraries/social/account/impl/a;->a:Landroid/content/SharedPreferences;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".created"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 496
    iget-object v3, p0, Lcom/google/android/libraries/social/account/impl/a;->i:Landroid/util/SparseArray;

    new-instance v4, Lcom/google/android/libraries/social/account/impl/f;

    invoke-direct {v4, p0, v0}, Lcom/google/android/libraries/social/account/impl/f;-><init>(Lcom/google/android/libraries/social/account/impl/a;I)V

    invoke-virtual {v3, v0, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 494
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 500
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->j:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    move v0, v1

    .line 501
    :goto_2
    if-ge v0, v2, :cond_4

    .line 502
    iget-object v3, p0, Lcom/google/android/libraries/social/account/impl/a;->a:Landroid/content/SharedPreferences;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".tombstoned"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 503
    iget-object v3, p0, Lcom/google/android/libraries/social/account/impl/a;->j:Landroid/util/SparseArray;

    new-instance v4, Lcom/google/android/libraries/social/account/impl/f;

    invoke-direct {v4, p0, v0}, Lcom/google/android/libraries/social/account/impl/f;-><init>(Lcom/google/android/libraries/social/account/impl/a;I)V

    invoke-virtual {v3, v0, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 501
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 507
    :cond_4
    iput-boolean v1, p0, Lcom/google/android/libraries/social/account/impl/a;->g:Z

    goto :goto_0
.end method

.method private declared-synchronized f()V
    .locals 1

    .prologue
    .line 511
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/libraries/social/account/impl/a;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 512
    monitor-exit p0

    return-void

    .line 511
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private f(I)V
    .locals 5

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 203
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 204
    const-string v3, "key."

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 205
    iget-object v3, p0, Lcom/google/android/libraries/social/account/impl/a;->a:Landroid/content/SharedPreferences;

    const/4 v4, -0x1

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 206
    if-ne v3, p1, :cond_0

    .line 207
    invoke-interface {v1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 211
    :cond_1
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 212
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 885
    invoke-static {}, Lcom/google/android/libraries/b/a/k;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 886
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->k:Ljava/lang/Runnable;

    invoke-static {}, Lcom/google/android/libraries/b/a/k;->b()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-static {}, Lcom/google/android/libraries/b/a/k;->b()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 895
    :goto_0
    return-void

    .line 892
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 893
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/a;->c:Ljava/util/List;

    new-array v0, v0, [Lcom/google/android/libraries/social/account/i;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    goto :goto_0
.end method

.method private g(I)V
    .locals 5

    .prologue
    .line 346
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 347
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 348
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 349
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 350
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 351
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 352
    invoke-interface {v2, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 355
    :cond_1
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 356
    return-void
.end method

.method private h(I)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 462
    :try_start_0
    iget-object v2, p0, Lcom/google/android/libraries/social/account/impl/a;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    new-instance v3, Ljava/io/File;

    const-string v4, "account-blobs"

    invoke-direct {v3, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    const-string v4, "account-%d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    if-nez v1, :cond_2

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Could not create account blob dir: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 471
    :catch_0
    move-exception v0

    :goto_1
    return-void

    :cond_1
    move v1, v0

    .line 462
    goto :goto_0

    .line 463
    :cond_2
    invoke-virtual {v2}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 464
    :goto_2
    array-length v3, v1

    if-ge v0, v3, :cond_3

    .line 465
    new-instance v3, Ljava/io/File;

    aget-object v4, v1, v0

    invoke-direct {v3, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 464
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 467
    :cond_3
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 112
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/account/impl/a;->e()V

    .line 113
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->i:Landroid/util/SparseArray;

    invoke-static {v0, p1, p2}, Lcom/google/android/libraries/social/account/impl/a;->a(Landroid/util/SparseArray;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 112
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)Lcom/google/android/libraries/social/account/c;
    .locals 3

    .prologue
    .line 216
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/account/impl/a;->e()V

    .line 217
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/c;

    .line 218
    if-nez v0, :cond_0

    .line 219
    new-instance v0, Lcom/google/android/libraries/social/account/e;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No such account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/account/e;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 221
    :cond_0
    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)Lcom/google/android/libraries/social/account/d;
    .locals 1

    .prologue
    .line 237
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/account/impl/a;->c(Ljava/lang/String;)Lcom/google/android/libraries/social/account/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Ljava/util/List;
    .locals 3

    .prologue
    .line 823
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/account/impl/a;->e()V

    .line 824
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 825
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/libraries/social/account/impl/a;->i:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 826
    iget-object v2, p0, Lcom/google/android/libraries/social/account/impl/a;->i:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 825
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 828
    :cond_0
    monitor-exit p0

    return-object v1

    .line 823
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final varargs declared-synchronized a([I)Ljava/util/List;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 833
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/account/impl/a;->e()V

    .line 834
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v3, v2

    .line 835
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->i:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 836
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/c;

    move v1, v2

    .line 837
    :goto_1
    array-length v5, p1

    if-ge v1, v5, :cond_0

    .line 838
    invoke-interface {v0}, Lcom/google/android/libraries/social/account/c;->a()I

    move-result v5

    aget v6, p1, v1

    if-ne v5, v6, :cond_1

    .line 839
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 835
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 837
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 844
    :cond_2
    monitor-exit p0

    return-object v4

    .line 833
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 107
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/social/account/impl/a;->a(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(I)Lcom/google/android/libraries/social/account/d;
    .locals 3

    .prologue
    .line 360
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/account/impl/a;->c(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 361
    new-instance v0, Lcom/google/android/libraries/social/account/e;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No such account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/account/e;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 360
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 363
    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/libraries/social/account/impl/e;

    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, p1, v2}, Lcom/google/android/libraries/social/account/impl/e;-><init>(Lcom/google/android/libraries/social/account/impl/a;Landroid/content/SharedPreferences$Editor;ILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365
    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 152
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/account/impl/a;->e()V

    .line 153
    const-string v0, "AccountStore"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    const-string v0, "AccountStore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Renaming accounts from: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    :cond_0
    invoke-virtual {p0, p2}, Lcom/google/android/libraries/social/account/impl/a;->b(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 158
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Account already exists: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 161
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/account/impl/a;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 162
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/account/impl/a;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v2

    const-string v3, "account_name"

    invoke-interface {v2, v3}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 163
    invoke-static {p1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 164
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/account/impl/a;->b(I)Lcom/google/android/libraries/social/account/d;

    move-result-object v0

    const-string v2, "account_name"

    invoke-interface {v0, v2, p2}, Lcom/google/android/libraries/social/account/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/account/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/libraries/social/account/d;->c()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 169
    :cond_3
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized c(I)Z
    .locals 1

    .prologue
    .line 226
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/account/impl/a;->e()V

    .line 227
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 226
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d(I)V
    .locals 3

    .prologue
    .line 284
    invoke-direct {p0}, Lcom/google/android/libraries/social/account/impl/a;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/a;

    .line 285
    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/account/a;->b(I)V

    goto :goto_0

    .line 288
    :cond_0
    monitor-enter p0

    .line 289
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/account/impl/a;->a(I)Lcom/google/android/libraries/social/account/c;

    .line 290
    const-string v0, "AccountStore"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 291
    const-string v0, "AccountStore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Removing account "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 295
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/a;->f:Landroid/content/Context;

    goto :goto_1

    .line 297
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/account/impl/a;->b(I)Lcom/google/android/libraries/social/account/d;

    move-result-object v0

    .line 298
    const-string v1, "created"

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/d;->e(Ljava/lang/String;)Lcom/google/android/libraries/social/account/d;

    .line 299
    const-string v1, "tombstoned"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/libraries/social/account/d;->a(Ljava/lang/String;Z)Lcom/google/android/libraries/social/account/d;

    .line 300
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/d;->a(I)Lcom/google/android/libraries/social/account/d;

    .line 301
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/account/impl/a;->h(I)V

    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/account/impl/a;->f(I)V

    .line 303
    invoke-interface {v0}, Lcom/google/android/libraries/social/account/d;->c()I

    .line 304
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
