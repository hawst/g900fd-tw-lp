.class public Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final NULL:Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;


# instance fields
.field private final bottom:Lcom/google/android/libraries/commerce/ocr/cv/Edge;

.field private final left:Lcom/google/android/libraries/commerce/ocr/cv/Edge;

.field private final right:Lcom/google/android/libraries/commerce/ocr/cv/Edge;

.field private final top:Lcom/google/android/libraries/commerce/ocr/cv/Edge;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;

    invoke-direct {v0, v1, v1, v1, v1}, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;-><init>(Lcom/google/android/libraries/commerce/ocr/cv/Edge;Lcom/google/android/libraries/commerce/ocr/cv/Edge;Lcom/google/android/libraries/commerce/ocr/cv/Edge;Lcom/google/android/libraries/commerce/ocr/cv/Edge;)V

    sput-object v0, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->NULL:Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/cv/Edge;Lcom/google/android/libraries/commerce/ocr/cv/Edge;Lcom/google/android/libraries/commerce/ocr/cv/Edge;Lcom/google/android/libraries/commerce/ocr/cv/Edge;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->top:Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    .line 20
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->left:Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    .line 21
    iput-object p3, p0, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->right:Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    .line 22
    iput-object p4, p0, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->bottom:Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    .line 23
    return-void
.end method


# virtual methods
.method public allEdgesDetected()Z
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->top:Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->left:Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->bottom:Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->right:Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBottom()Lcom/google/android/libraries/commerce/ocr/cv/Edge;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->bottom:Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    return-object v0
.end method

.method public getLeft()Lcom/google/android/libraries/commerce/ocr/cv/Edge;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->left:Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    return-object v0
.end method

.method public getRight()Lcom/google/android/libraries/commerce/ocr/cv/Edge;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->right:Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    return-object v0
.end method

.method public getTop()Lcom/google/android/libraries/commerce/ocr/cv/Edge;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->top:Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 47
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "top"

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->top:Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "left"

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->left:Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "right"

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->right:Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "bottom"

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->bottom:Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/bv;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
