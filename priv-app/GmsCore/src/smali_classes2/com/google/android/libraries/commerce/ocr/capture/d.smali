.class final enum Lcom/google/android/libraries/commerce/ocr/capture/d;
.super Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;
.source "SourceFile"


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 628
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;-><init>(Ljava/lang/String;ILcom/google/android/libraries/commerce/ocr/capture/a;)V

    return-void
.end method


# virtual methods
.method public final getClosest(Ljava/util/List;Lcom/google/android/gms/common/util/ap;Landroid/graphics/Point;Landroid/graphics/Point;)Landroid/hardware/Camera$Size;
    .locals 16

    .prologue
    .line 632
    move-object/from16 v0, p3

    iget v2, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p3

    iget v3, v0, Landroid/graphics/Point;->y:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    int-to-double v2, v2

    move-object/from16 v0, p3

    iget v4, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p3

    iget v5, v0, Landroid/graphics/Point;->y:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    int-to-double v4, v4

    div-double v10, v2, v4

    .line 634
    const-string v2, "CameraManagerImpl"

    const-string v3, "Preview size to match: %s, ratio: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p3, v4, v5

    const/4 v5, 0x1

    invoke-static {v10, v11}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 637
    const/4 v3, 0x0

    .line 638
    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 639
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/Camera$Size;

    .line 640
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Lcom/google/android/gms/common/util/ap;->a(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 641
    iget v6, v2, Landroid/hardware/Camera$Size;->width:I

    int-to-double v6, v6

    iget v8, v2, Landroid/hardware/Camera$Size;->height:I

    int-to-double v12, v8

    div-double/2addr v6, v12

    .line 645
    sub-double/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    .line 647
    cmpg-double v8, v6, v4

    if-gez v8, :cond_2

    const/4 v8, 0x1

    .line 648
    :goto_1
    if-nez v8, :cond_1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v6, v7}, Lcom/google/android/libraries/commerce/ocr/capture/d;->isCloserToTargetSize(Landroid/hardware/Camera$Size;Landroid/hardware/Camera$Size;D)Z

    move-result v8

    if-eqz v8, :cond_4

    :cond_1
    move-object v4, v2

    move-wide v2, v6

    :goto_2
    move-wide v14, v2

    move-object v3, v4

    move-wide v4, v14

    .line 652
    goto :goto_0

    .line 647
    :cond_2
    const/4 v8, 0x0

    goto :goto_1

    .line 653
    :cond_3
    return-object v3

    :cond_4
    move-wide v14, v4

    move-object v4, v3

    move-wide v2, v14

    goto :goto_2
.end method
