.class public final Lcom/google/android/libraries/social/mediaupload/e;
.super Lcom/google/android/libraries/social/mediaupload/j;
.source "SourceFile"


# instance fields
.field private final a:Z

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 1100
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid content-type: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", permanent: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/mediaupload/j;-><init>(Ljava/lang/String;)V

    .line 1101
    iput-boolean p2, p0, Lcom/google/android/libraries/social/mediaupload/e;->a:Z

    .line 1102
    iput-object p1, p0, Lcom/google/android/libraries/social/mediaupload/e;->b:Ljava/lang/String;

    .line 1103
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1106
    iget-boolean v0, p0, Lcom/google/android/libraries/social/mediaupload/e;->a:Z

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1110
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/e;->b:Ljava/lang/String;

    return-object v0
.end method
