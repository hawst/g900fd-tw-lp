.class public Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;
.implements Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$OnErrorCallback;
.implements Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$OnPreviewLayoutFinalizedCallback;
.implements Lcom/google/android/libraries/commerce/ocr/ui/OcrView$Presenter;


# static fields
.field private static final TAG:Ljava/lang/String; = "CreditCardOcrViewPresenter"


# instance fields
.field private final cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

.field private final cameraPreviewPresenter:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;

.field private final exitHandler:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

.field private final fragment:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;

.field private onPreviewReadyCallback:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;

.field private final previewOverlayPresenter:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayPresenter;

.field private final roiProvider:Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;

.field private final view:Lcom/google/android/libraries/commerce/ocr/ui/OcrView;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/Fragment;Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;Lcom/google/android/libraries/commerce/ocr/ui/OcrView;Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayPresenter;Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    check-cast p1, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;

    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->fragment:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;

    .line 55
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->exitHandler:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

    .line 57
    iput-object p3, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->view:Lcom/google/android/libraries/commerce/ocr/ui/OcrView;

    .line 58
    iput-object p5, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->previewOverlayPresenter:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayPresenter;

    .line 60
    iput-object p4, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->cameraPreviewPresenter:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;

    .line 61
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->cameraPreviewPresenter:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;

    invoke-interface {p3}, Lcom/google/android/libraries/commerce/ocr/ui/OcrView;->getCameraPreviewView()Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;->setView(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;)V

    .line 62
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->cameraPreviewPresenter:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;

    invoke-interface {v0, p0}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;->setOnPreviewLayoutFinalizedCallback(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$OnPreviewLayoutFinalizedCallback;)V

    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->cameraPreviewPresenter:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;

    invoke-interface {v0, p0}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;->setOnPreviewReadyCallback(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->cameraPreviewPresenter:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;

    invoke-interface {v0, p0}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;->setOnErrorCallback(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$OnErrorCallback;)V

    .line 66
    iput-object p7, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    .line 67
    iput-object p6, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->roiProvider:Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;

    .line 68
    return-void
.end method

.method private adjustPreviewLayoutToMatchCamera()V
    .locals 4

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;->getScreenFillingPreviewSize()Landroid/graphics/Point;

    move-result-object v1

    .line 108
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->fragment:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/google/android/libraries/commerce/ocr/R$id;->ocrContainer:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 109
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 110
    iget v3, v1, Landroid/graphics/Point;->x:I

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 111
    iget v1, v1, Landroid/graphics/Point;->y:I

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 112
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->requestLayout()V

    .line 113
    const-string v0, "CreditCardOcrViewPresenter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Screen preview view size: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "x"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    return-void
.end method


# virtual methods
.method public attachView()V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->fragment:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->view:Lcom/google/android/libraries/commerce/ocr/ui/OcrView;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->fragment:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/libraries/commerce/ocr/R$id;->ocrPreview:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->fragment:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/libraries/commerce/ocr/R$id;->ocrPreview:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->view:Lcom/google/android/libraries/commerce/ocr/ui/OcrView;

    invoke-interface {v1}, Lcom/google/android/libraries/commerce/ocr/ui/OcrView;->getCameraPreviewView()Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;->toView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 92
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->fragment:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/libraries/commerce/ocr/R$id;->ocrOverlay:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->view:Lcom/google/android/libraries/commerce/ocr/ui/OcrView;

    invoke-interface {v1}, Lcom/google/android/libraries/commerce/ocr/ui/OcrView;->getPreviewOverlayView()Lcom/google/android/libraries/commerce/ocr/ui/PreviewOverlayView;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    invoke-virtual {v1}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->toView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 94
    return-void
.end method

.method public onError()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 82
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->exitHandler:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

    const/16 v1, 0x2713

    invoke-virtual {v0, v1, v2, v2}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;->exit(ILcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;Landroid/os/Bundle;)V

    .line 83
    return-void
.end method

.method public onFinalized(Landroid/graphics/Rect;Landroid/graphics/Point;)V
    .locals 0

    .prologue
    .line 72
    invoke-virtual {p0, p1, p2}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->onPreviewLayoutFinalized(Landroid/graphics/Rect;Landroid/graphics/Point;)V

    .line 73
    return-void
.end method

.method public onFinish()V
    .locals 0

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->onPreviewReady()V

    .line 78
    return-void
.end method

.method public onPreviewLayoutFinalized(Landroid/graphics/Rect;Landroid/graphics/Point;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 126
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p2, Landroid/graphics/Point;->x:I

    iget v2, p2, Landroid/graphics/Point;->y:I

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 128
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->roiProvider:Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;

    invoke-interface {v1, v0}, Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;->onImageAreaChange(Landroid/graphics/Rect;)V

    .line 129
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->previewOverlayPresenter:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayPresenter;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayPresenter;->onPreviewLayoutFinalized(Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 130
    return-void
.end method

.method public onPreviewReady()V
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->adjustPreviewLayoutToMatchCamera()V

    .line 99
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;->requestAutoFocus()V

    .line 100
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->onPreviewReadyCallback:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->onPreviewReadyCallback:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;->onFinish()V

    .line 103
    :cond_0
    return-void
.end method

.method public setOnPreviewReadyCallback(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->onPreviewReadyCallback:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;

    .line 120
    return-void
.end method

.method public showErrorMessage(I)V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->view:Lcom/google/android/libraries/commerce/ocr/ui/OcrView;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/commerce/ocr/ui/OcrView;->showErrorMessage(I)V

    .line 135
    return-void
.end method
