.class final Lcom/google/android/libraries/social/experiments/impl/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/experiments/impl/c;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/libraries/social/experiments/impl/a;

.field private final c:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/experiments/impl/d;->c:Ljava/util/ArrayList;

    .line 35
    iput-object p1, p0, Lcom/google/android/libraries/social/experiments/impl/d;->a:Landroid/content/Context;

    .line 36
    new-instance v0, Lcom/google/android/libraries/social/experiments/impl/a;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/social/experiments/impl/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/experiments/impl/d;->b:Lcom/google/android/libraries/social/experiments/impl/a;

    .line 37
    return-void
.end method

.method private static b(Ljava/lang/String;)[Lcom/google/c/e/b/b/a/a/c;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 158
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move-object v0, v1

    .line 175
    :cond_1
    :goto_0
    return-object v0

    .line 162
    :cond_2
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 164
    const/4 v3, 0x0

    array-length v4, v0

    invoke-static {v0, v3, v4}, Lcom/google/protobuf/nano/a;->a([BII)Lcom/google/protobuf/nano/a;

    move-result-object v3

    .line 165
    invoke-virtual {v3}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    .line 166
    new-array v0, v4, [Lcom/google/c/e/b/b/a/a/c;

    .line 167
    :goto_1
    if-ge v2, v4, :cond_1

    .line 168
    new-instance v5, Lcom/google/c/e/b/b/a/a/c;

    invoke-direct {v5}, Lcom/google/c/e/b/b/a/a/c;-><init>()V

    .line 169
    invoke-virtual {v3, v5}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    .line 170
    aput-object v5, v0, v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 173
    :catch_0
    move-exception v0

    .line 174
    const-string v2, "ExperimentLoader"

    const-string v3, "Unable to parse experiments"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 175
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/util/Map;
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 69
    .line 71
    iget-object v2, p0, Lcom/google/android/libraries/social/experiments/impl/d;->b:Lcom/google/android/libraries/social/experiments/impl/a;

    iget-object v2, v2, Lcom/google/android/libraries/social/experiments/impl/a;->a:Landroid/content/Context;

    const-string v3, "accounts"

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".flags"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 73
    if-eqz v2, :cond_9

    .line 74
    invoke-static {v2}, Lcom/google/android/libraries/social/experiments/impl/d;->b(Ljava/lang/String;)[Lcom/google/c/e/b/b/a/a/c;

    move-result-object v2

    move-object v4, v2

    .line 77
    :goto_0
    if-nez v4, :cond_0

    .line 108
    :goto_1
    return-object v1

    .line 81
    :cond_0
    array-length v5, v4

    .line 82
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2, v5}, Ljava/util/HashMap;-><init>(I)V

    move v3, v0

    .line 83
    :goto_2
    if-ge v3, v5, :cond_7

    .line 84
    aget-object v6, v4, v3

    .line 86
    iget-object v0, v6, Lcom/google/c/e/b/b/a/a/c;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 87
    const/high16 v7, -0x80000000

    if-eq v0, v7, :cond_1

    const/4 v7, 0x1

    if-ne v0, v7, :cond_3

    .line 90
    :cond_1
    const-string v0, "true"

    .line 103
    :goto_3
    if-eqz v0, :cond_2

    .line 104
    iget-object v6, v6, Lcom/google/c/e/b/b/a/a/c;->a:Ljava/lang/String;

    invoke-virtual {v2, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 91
    :cond_3
    const/4 v7, 0x4

    if-ne v0, v7, :cond_5

    .line 92
    iget-object v0, v6, Lcom/google/c/e/b/b/a/a/c;->c:Lcom/google/c/e/b/b/a/a/d;

    if-eqz v0, :cond_4

    iget-object v0, v6, Lcom/google/c/e/b/b/a/a/c;->c:Lcom/google/c/e/b/b/a/a/d;

    iget-object v0, v0, Lcom/google/c/e/b/b/a/a/d;->c:Ljava/lang/String;

    goto :goto_3

    :cond_4
    move-object v0, v1

    goto :goto_3

    .line 93
    :cond_5
    const/4 v7, 0x3

    if-ne v0, v7, :cond_6

    .line 94
    iget-object v0, v6, Lcom/google/c/e/b/b/a/a/c;->c:Lcom/google/c/e/b/b/a/a/d;

    if-eqz v0, :cond_8

    iget-object v0, v6, Lcom/google/c/e/b/b/a/a/c;->c:Lcom/google/c/e/b/b/a/a/d;

    iget-object v0, v0, Lcom/google/c/e/b/b/a/a/d;->b:Ljava/lang/Double;

    if-eqz v0, :cond_8

    .line 95
    iget-object v0, v6, Lcom/google/c/e/b/b/a/a/c;->c:Lcom/google/c/e/b/b/a/a/d;

    iget-object v0, v0, Lcom/google/c/e/b/b/a/a/d;->b:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 97
    :cond_6
    const/4 v7, 0x2

    if-ne v0, v7, :cond_8

    .line 98
    iget-object v0, v6, Lcom/google/c/e/b/b/a/a/c;->c:Lcom/google/c/e/b/b/a/a/d;

    if-eqz v0, :cond_8

    iget-object v0, v6, Lcom/google/c/e/b/b/a/a/c;->c:Lcom/google/c/e/b/b/a/a/d;

    iget-object v0, v0, Lcom/google/c/e/b/b/a/a/d;->a:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 99
    iget-object v0, v6, Lcom/google/c/e/b/b/a/a/c;->c:Lcom/google/c/e/b/b/a/a/d;

    iget-object v0, v0, Lcom/google/c/e/b/b/a/a/d;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_7
    move-object v1, v2

    .line 108
    goto :goto_1

    :cond_8
    move-object v0, v1

    goto :goto_3

    :cond_9
    move-object v4, v1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/libraries/social/experiments/b;)V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/libraries/social/experiments/impl/d;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    return-void
.end method
