.class public Lcom/google/android/libraries/social/i/a/a;
.super Landroid/support/v4/app/m;
.source "SourceFile"


# instance fields
.field protected final l:Lcom/google/android/libraries/social/i/r;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    .line 21
    new-instance v0, Lcom/google/android/libraries/social/i/r;

    invoke-direct {v0}, Lcom/google/android/libraries/social/i/r;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/i/a/a;->l:Lcom/google/android/libraries/social/i/r;

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/a;->l:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/r;->s()V

    .line 99
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/m;->onActivityResult(IILandroid/content/Intent;)V

    .line 100
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/a;->l:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/i/r;->a(Landroid/app/Activity;)V

    .line 49
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onAttach(Landroid/app/Activity;)V

    .line 50
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/a;->l:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/i/r;->c(Landroid/os/Bundle;)V

    .line 30
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onCreate(Landroid/os/Bundle;)V

    .line 31
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/a;->l:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0, p3}, Lcom/google/android/libraries/social/i/r;->b(Landroid/os/Bundle;)V

    .line 37
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/m;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/a;->l:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/r;->c()V

    .line 93
    invoke-super {p0}, Landroid/support/v4/app/m;->onDestroy()V

    .line 94
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/a;->l:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/r;->d()V

    .line 81
    invoke-super {p0}, Landroid/support/v4/app/m;->onDestroyView()V

    .line 82
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/a;->l:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/r;->e()V

    .line 111
    invoke-super {p0}, Landroid/support/v4/app/m;->onDetach()V

    .line 112
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/a;->l:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/r;->b()V

    .line 69
    invoke-super {p0}, Landroid/support/v4/app/m;->onPause()V

    .line 70
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/google/android/libraries/social/i/a/a;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/social/i/a/d;->a(Landroid/support/v4/app/v;)V

    .line 62
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/a;->l:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/r;->q()V

    .line 63
    invoke-super {p0}, Landroid/support/v4/app/m;->onResume()V

    .line 64
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/a;->l:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/i/r;->d(Landroid/os/Bundle;)V

    .line 87
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 88
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/google/android/libraries/social/i/a/a;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/social/i/a/d;->a(Landroid/support/v4/app/v;)V

    .line 55
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/a;->l:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/r;->p()V

    .line 56
    invoke-super {p0}, Landroid/support/v4/app/m;->onStart()V

    .line 57
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/a;->l:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/r;->r()V

    .line 75
    invoke-super {p0}, Landroid/support/v4/app/m;->onStop()V

    .line 76
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/a;->l:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/social/i/r;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 43
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/m;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 44
    return-void
.end method

.method public setMenuVisibility(Z)V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/a;->l:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/r;->a()V

    .line 105
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->setMenuVisibility(Z)V

    .line 106
    return-void
.end method
