.class public final enum Lcom/google/android/libraries/social/autobackup/at;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/libraries/social/autobackup/at;

.field public static final enum b:Lcom/google/android/libraries/social/autobackup/at;

.field public static final enum c:Lcom/google/android/libraries/social/autobackup/at;

.field public static final enum d:Lcom/google/android/libraries/social/autobackup/at;

.field public static final enum e:Lcom/google/android/libraries/social/autobackup/at;

.field public static final enum f:Lcom/google/android/libraries/social/autobackup/at;

.field private static final synthetic g:[Lcom/google/android/libraries/social/autobackup/at;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 127
    new-instance v0, Lcom/google/android/libraries/social/autobackup/at;

    const-string v1, "None"

    invoke-direct {v0, v1, v3}, Lcom/google/android/libraries/social/autobackup/at;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/at;->a:Lcom/google/android/libraries/social/autobackup/at;

    new-instance v0, Lcom/google/android/libraries/social/autobackup/at;

    const-string v1, "Pending"

    invoke-direct {v0, v1, v4}, Lcom/google/android/libraries/social/autobackup/at;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/at;->b:Lcom/google/android/libraries/social/autobackup/at;

    new-instance v0, Lcom/google/android/libraries/social/autobackup/at;

    const-string v1, "Uploading"

    invoke-direct {v0, v1, v5}, Lcom/google/android/libraries/social/autobackup/at;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/at;->c:Lcom/google/android/libraries/social/autobackup/at;

    new-instance v0, Lcom/google/android/libraries/social/autobackup/at;

    const-string v1, "RecentlyDone"

    invoke-direct {v0, v1, v6}, Lcom/google/android/libraries/social/autobackup/at;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/at;->d:Lcom/google/android/libraries/social/autobackup/at;

    new-instance v0, Lcom/google/android/libraries/social/autobackup/at;

    const-string v1, "Done"

    invoke-direct {v0, v1, v7}, Lcom/google/android/libraries/social/autobackup/at;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/at;->e:Lcom/google/android/libraries/social/autobackup/at;

    new-instance v0, Lcom/google/android/libraries/social/autobackup/at;

    const-string v1, "Failed"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/social/autobackup/at;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/at;->f:Lcom/google/android/libraries/social/autobackup/at;

    .line 126
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/libraries/social/autobackup/at;

    sget-object v1, Lcom/google/android/libraries/social/autobackup/at;->a:Lcom/google/android/libraries/social/autobackup/at;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/libraries/social/autobackup/at;->b:Lcom/google/android/libraries/social/autobackup/at;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/libraries/social/autobackup/at;->c:Lcom/google/android/libraries/social/autobackup/at;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/libraries/social/autobackup/at;->d:Lcom/google/android/libraries/social/autobackup/at;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/libraries/social/autobackup/at;->e:Lcom/google/android/libraries/social/autobackup/at;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/libraries/social/autobackup/at;->f:Lcom/google/android/libraries/social/autobackup/at;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/libraries/social/autobackup/at;->g:[Lcom/google/android/libraries/social/autobackup/at;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 126
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(IIJ)Lcom/google/android/libraries/social/autobackup/at;
    .locals 2

    .prologue
    .line 131
    sparse-switch p0, :sswitch_data_0

    .line 145
    sget-object v0, Lcom/google/android/libraries/social/autobackup/at;->a:Lcom/google/android/libraries/social/autobackup/at;

    :goto_0
    return-object v0

    .line 133
    :sswitch_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 134
    sget-object v0, Lcom/google/android/libraries/social/autobackup/at;->c:Lcom/google/android/libraries/social/autobackup/at;

    goto :goto_0

    .line 136
    :cond_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/at;->b:Lcom/google/android/libraries/social/autobackup/at;

    goto :goto_0

    .line 138
    :sswitch_1
    sget-object v0, Lcom/google/android/libraries/social/autobackup/at;->b:Lcom/google/android/libraries/social/autobackup/at;

    goto :goto_0

    .line 140
    :sswitch_2
    sget-object v0, Lcom/google/android/libraries/social/autobackup/at;->f:Lcom/google/android/libraries/social/autobackup/at;

    goto :goto_0

    .line 142
    :sswitch_3
    const-wide/16 v0, 0x7d0

    cmp-long v0, p2, v0

    if-lez v0, :cond_1

    sget-object v0, Lcom/google/android/libraries/social/autobackup/at;->e:Lcom/google/android/libraries/social/autobackup/at;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/libraries/social/autobackup/at;->d:Lcom/google/android/libraries/social/autobackup/at;

    goto :goto_0

    .line 131
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/libraries/social/autobackup/at;
    .locals 1

    .prologue
    .line 126
    const-class v0, Lcom/google/android/libraries/social/autobackup/at;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/at;

    return-object v0
.end method

.method public static values()[Lcom/google/android/libraries/social/autobackup/at;
    .locals 1

    .prologue
    .line 126
    sget-object v0, Lcom/google/android/libraries/social/autobackup/at;->g:[Lcom/google/android/libraries/social/autobackup/at;

    invoke-virtual {v0}, [Lcom/google/android/libraries/social/autobackup/at;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/libraries/social/autobackup/at;

    return-object v0
.end method
