.class public final Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrUiModule;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final providePreviewOverlayTextView(Landroid/view/View;)Landroid/widget/TextView;
    .locals 2

    .prologue
    .line 28
    sget v0, Lcom/google/android/libraries/commerce/ocr/R$id;->ocrMessage:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 29
    sget v1, Lcom/google/android/libraries/commerce/ocr/R$string;->ocr_cc_scan_card_details:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 30
    return-object v0
.end method

.method public static final providePreviewOverlayView(Landroid/support/v4/app/Fragment;)Landroid/view/View;
    .locals 4

    .prologue
    .line 22
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 23
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/google/android/libraries/commerce/ocr/R$id;->ocrOverlay:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 24
    sget v2, Lcom/google/android/libraries/commerce/ocr/R$layout;->ocr_preview_overlay:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static final provideSkipScanButton(Landroid/view/View;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 34
    sget v0, Lcom/google/android/libraries/commerce/ocr/R$id;->ocrSkipScanButton:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method
