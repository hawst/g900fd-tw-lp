.class public Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final target:Landroid/graphics/RectF;

.field final synthetic this$0:Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;


# direct methods
.method private constructor <init>(Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 106
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->this$0:Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    .line 108
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;Landroid/graphics/Rect;Lcom/google/android/libraries/commerce/ocr/util/b;)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;-><init>(Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;Landroid/graphics/Rect;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->this$0:Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    .line 112
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;Landroid/graphics/RectF;Lcom/google/android/libraries/commerce/ocr/util/b;)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;-><init>(Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;Landroid/graphics/RectF;)V

    return-void
.end method

.method private toFloat(D)F
    .locals 1

    .prologue
    .line 157
    double-to-float v0, p1

    return v0
.end method


# virtual methods
.method public getAsRect()Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 115
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method public rotate(IFF)Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;
    .locals 2

    .prologue
    .line 173
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 174
    int-to-float v1, p1

    invoke-virtual {v0, v1, p2, p3}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 175
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 176
    return-object p0
.end method

.method public scale(Landroid/graphics/Point;Landroid/graphics/Point;)Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;
    .locals 10

    .prologue
    .line 134
    iget v0, p2, Landroid/graphics/Point;->x:I

    int-to-double v0, v0

    iget v2, p1, Landroid/graphics/Point;->x:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    .line 135
    iget v2, p2, Landroid/graphics/Point;->y:I

    int-to-double v2, v2

    iget v4, p1, Landroid/graphics/Point;->y:I

    int-to-double v4, v4

    div-double/2addr v2, v4

    .line 137
    iget-object v4, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    float-to-double v6, v5

    mul-double/2addr v6, v0

    invoke-direct {p0, v6, v7}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->toFloat(D)F

    move-result v5

    iget-object v6, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    float-to-double v6, v6

    mul-double/2addr v6, v2

    invoke-direct {p0, v6, v7}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->toFloat(D)F

    move-result v6

    iget-object v7, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    float-to-double v8, v7

    mul-double/2addr v0, v8

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->toFloat(D)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    float-to-double v8, v1

    mul-double/2addr v2, v8

    invoke-direct {p0, v2, v3}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->toFloat(D)F

    move-result v1

    invoke-virtual {v4, v5, v6, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 139
    return-object p0
.end method

.method public scaleToHeight(F)Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;
    .locals 5

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    float-to-double v0, v0

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-double v2, v2

    div-double/2addr v0, v2

    .line 151
    float-to-double v2, p1

    div-double v0, v2, v0

    double-to-float v0, v0

    .line 152
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v4

    iget-object v4, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, p1

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 153
    return-object p0
.end method

.method public scaleToWidth(F)Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;
    .locals 6

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    float-to-double v0, v0

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-double v2, v2

    div-double/2addr v0, v2

    .line 144
    float-to-double v2, p1

    mul-double/2addr v0, v2

    double-to-float v0, v0

    .line 145
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    add-float/2addr v4, p1

    iget-object v5, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v5

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 146
    return-object p0
.end method

.method public swapDimensions()Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;
    .locals 5

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    iget-object v4, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 166
    return-object p0
.end method

.method public transpose(II)Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;
    .locals 6

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    int-to-float v2, p1

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    int-to-float v3, p2

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    int-to-float v4, p1

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->target:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    int-to-float v5, p2

    add-float/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 124
    return-object p0
.end method
