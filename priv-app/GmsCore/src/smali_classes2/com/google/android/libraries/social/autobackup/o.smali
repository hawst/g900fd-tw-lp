.class public Lcom/google/android/libraries/social/autobackup/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/account/h;
.implements Lcom/google/android/libraries/social/p/b;


# static fields
.field private static final a:Lcom/google/android/libraries/social/f/c;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/libraries/social/account/b;

.field private final d:Lcom/google/android/libraries/social/p/c;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Lcom/google/android/libraries/social/f/c;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/f/c;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/o;->a:Lcom/google/android/libraries/social/f/c;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Lcom/google/android/libraries/social/p/a;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/social/p/a;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/o;->d:Lcom/google/android/libraries/social/p/c;

    .line 47
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/o;->b:Landroid/content/Context;

    .line 48
    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/o;->c:Lcom/google/android/libraries/social/account/b;

    .line 49
    return-void
.end method

.method private a(IZ)V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/o;->c:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/account/b;->b(I)Lcom/google/android/libraries/social/account/d;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.autobackup.AutobackupAccountSettingsManager"

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/d;->f(Ljava/lang/String;)Lcom/google/android/libraries/social/account/d;

    move-result-object v0

    const-string v1, "auto_backup_enabled"

    invoke-interface {v0, v1, p2}, Lcom/google/android/libraries/social/account/d;->a(Ljava/lang/String;Z)Lcom/google/android/libraries/social/account/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/libraries/social/account/d;->c()I

    .line 81
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/o;->d:Lcom/google/android/libraries/social/p/c;

    invoke-interface {v0}, Lcom/google/android/libraries/social/p/c;->a()V

    .line 82
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/social/p/c;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/o;->d:Lcom/google/android/libraries/social/p/c;

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 63
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 64
    sget-object v0, Lcom/google/android/libraries/social/autobackup/o;->a:Lcom/google/android/libraries/social/f/c;

    .line 65
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Attempting to enable autobackup for INVALID_ID"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/autobackup/o;->a(IZ)V

    .line 70
    return-void
.end method

.method public final a(Lcom/google/android/libraries/social/account/f;)V
    .locals 3

    .prologue
    .line 169
    const-string v0, "com.google.android.libraries.social.autobackup.AutobackupAccountSettingsManager"

    invoke-interface {p1, v0}, Lcom/google/android/libraries/social/account/f;->h(Ljava/lang/String;)Lcom/google/android/libraries/social/account/f;

    move-result-object v0

    const-string v1, "auto_backup_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/libraries/social/account/f;->b(Ljava/lang/String;Z)Lcom/google/android/libraries/social/account/f;

    .line 170
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 179
    new-instance v0, Lcom/google/android/libraries/social/autobackup/p;

    invoke-direct {v0}, Lcom/google/android/libraries/social/autobackup/p;-><init>()V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    return-void
.end method

.method public final b()Ljava/util/List;
    .locals 5

    .prologue
    .line 88
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 90
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/o;->c:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0}, Lcom/google/android/libraries/social/account/b;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 91
    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/o;->c:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v3, v0}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v3

    .line 92
    invoke-interface {v3}, Lcom/google/android/libraries/social/account/c;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "is_plus_page"

    invoke-interface {v3, v4}, Lcom/google/android/libraries/social/account/c;->c(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "account_name"

    invoke-interface {v3, v4}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "@youtube.com"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 95
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 99
    :cond_1
    return-object v1
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/autobackup/o;->a(IZ)V

    .line 77
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/o;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(I)Z
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/o;->c:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.autobackup.AutobackupAccountSettingsManager"

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/c;->d(Ljava/lang/String;)Lcom/google/android/libraries/social/account/c;

    move-result-object v0

    const-string v1, "auto_backup_enabled"

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/c;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 3

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/o;->e()Ljava/util/List;

    move-result-object v0

    .line 115
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 116
    const-string v1, "AASM"

    const-string v2, "More than one appears enabled for Auto Backup! Choosing the first."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final e()Ljava/util/List;
    .locals 4

    .prologue
    .line 127
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 128
    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/o;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 129
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/autobackup/o;->c(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 130
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 133
    :cond_1
    return-object v1
.end method
