.class abstract Lcom/google/android/libraries/social/mediaupload/r;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final b:Landroid/os/ConditionVariable;

.field final c:Lcom/google/android/libraries/social/rpc/l;

.field d:I

.field e:J

.field f:[B

.field g:Ljava/io/IOException;

.field h:Z

.field protected final i:Le/a/a/l;


# direct methods
.method protected constructor <init>(Lcom/google/android/libraries/social/rpc/l;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/mediaupload/r;->b:Landroid/os/ConditionVariable;

    .line 32
    new-instance v0, Lcom/google/android/libraries/social/mediaupload/s;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/social/mediaupload/s;-><init>(Lcom/google/android/libraries/social/mediaupload/r;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/mediaupload/r;->i:Le/a/a/l;

    .line 29
    iput-object p1, p0, Lcom/google/android/libraries/social/mediaupload/r;->c:Lcom/google/android/libraries/social/rpc/l;

    .line 30
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method protected abstract a(Le/a/a/g;)V
.end method

.method protected abstract b()Le/a/a/g;
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/r;->b:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->close()V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediaupload/r;->b()Le/a/a/g;

    move-result-object v0

    invoke-interface {v0}, Le/a/a/g;->b()V

    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/r;->b:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 60
    return-void
.end method
