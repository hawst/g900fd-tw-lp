.class public Lcom/google/android/libraries/commerce/ocr/capture/InFocusFrameCheck;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final TAG:Ljava/lang/String; = "InFocusFrameCheck"


# instance fields
.field private final blurDetector:Lcom/google/android/libraries/commerce/ocr/cv/BlurDetector;

.field private final cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

.field private edgeChangeListener:Lcom/google/android/libraries/commerce/ocr/capture/EdgeChangeListener;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/cv/BlurDetector;Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/capture/InFocusFrameCheck;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    .line 22
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/capture/InFocusFrameCheck;->blurDetector:Lcom/google/android/libraries/commerce/ocr/cv/BlurDetector;

    .line 23
    return-void
.end method


# virtual methods
.method public isGoodAndTriggerAutofocus(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;)Z
    .locals 3

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/InFocusFrameCheck;->blurDetector:Lcom/google/android/libraries/commerce/ocr/cv/BlurDetector;

    invoke-interface {p2}, Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;->getBlurDetectorROI()Landroid/graphics/Rect;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/libraries/commerce/ocr/cv/BlurDetector;->isBlurred(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 31
    const-string v0, "InFocusFrameCheck"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Blur image, request auto-focus."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/InFocusFrameCheck;->edgeChangeListener:Lcom/google/android/libraries/commerce/ocr/capture/EdgeChangeListener;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/InFocusFrameCheck;->edgeChangeListener:Lcom/google/android/libraries/commerce/ocr/capture/EdgeChangeListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/libraries/commerce/ocr/capture/EdgeChangeListener;->onEdgeChange(Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;)V

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/InFocusFrameCheck;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;->requestAutoFocus()V

    .line 37
    const/4 v0, 0x0

    .line 39
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setEdgeChangeListener(Lcom/google/android/libraries/commerce/ocr/capture/EdgeChangeListener;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/capture/InFocusFrameCheck;->edgeChangeListener:Lcom/google/android/libraries/commerce/ocr/capture/EdgeChangeListener;

    .line 48
    return-void
.end method
