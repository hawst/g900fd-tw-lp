.class final Lcom/google/android/libraries/social/mediaupload/z;
.super Lcom/google/android/libraries/social/mediaupload/r;
.source "SourceFile"


# instance fields
.field a:Le/a/a/g;

.field private final j:Landroid/content/Context;

.field private final k:Ljava/lang/String;

.field private final l:Ljava/lang/String;

.field private final m:J

.field private final n:J

.field private final o:Ljava/io/InputStream;

.field private final p:Z

.field private final q:Lcom/google/android/libraries/social/mediaupload/ad;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/social/rpc/l;Ljava/lang/String;Ljava/lang/String;JJLjava/io/InputStream;ZLcom/google/android/libraries/social/mediaupload/ad;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0, p2}, Lcom/google/android/libraries/social/mediaupload/r;-><init>(Lcom/google/android/libraries/social/rpc/l;)V

    .line 62
    iput-object p1, p0, Lcom/google/android/libraries/social/mediaupload/z;->j:Landroid/content/Context;

    .line 63
    iput-object p3, p0, Lcom/google/android/libraries/social/mediaupload/z;->k:Ljava/lang/String;

    .line 64
    iput-object p4, p0, Lcom/google/android/libraries/social/mediaupload/z;->l:Ljava/lang/String;

    .line 65
    iput-wide p5, p0, Lcom/google/android/libraries/social/mediaupload/z;->m:J

    .line 66
    iput-wide p7, p0, Lcom/google/android/libraries/social/mediaupload/z;->n:J

    .line 67
    iput-object p9, p0, Lcom/google/android/libraries/social/mediaupload/z;->o:Ljava/io/InputStream;

    .line 68
    iput-boolean p10, p0, Lcom/google/android/libraries/social/mediaupload/z;->p:Z

    .line 69
    iput-object p11, p0, Lcom/google/android/libraries/social/mediaupload/z;->q:Lcom/google/android/libraries/social/mediaupload/ad;

    .line 70
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 10

    .prologue
    .line 74
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/google/android/libraries/social/mediaupload/r;->c:Lcom/google/android/libraries/social/rpc/l;

    iget-object v2, p0, Lcom/google/android/libraries/social/mediaupload/z;->k:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/libraries/social/rpc/l;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 77
    const-string v1, "Content-Range"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bytes "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/google/android/libraries/social/mediaupload/z;->m:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/libraries/social/mediaupload/z;->n:J

    const-wide/16 v6, 0x1

    sub-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/libraries/social/mediaupload/z;->n:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    iget-object v1, p0, Lcom/google/android/libraries/social/mediaupload/z;->j:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/libraries/social/mediaupload/z;->k:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/libraries/social/mediaupload/z;->p:Z

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/libraries/social/mediaupload/z;->i:Le/a/a/l;

    invoke-static {v1, v2, v3, v0, v4}, Lcom/google/android/libraries/social/n/a;->a(Landroid/content/Context;Ljava/lang/String;ILjava/util/Map;Le/a/a/l;)Le/a/a/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/mediaupload/z;->a:Le/a/a/g;

    .line 84
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/z;->a:Le/a/a/g;

    iget-object v7, p0, Lcom/google/android/libraries/social/mediaupload/z;->l:Ljava/lang/String;

    new-instance v1, Lcom/google/android/libraries/social/mediaupload/aa;

    iget-object v2, p0, Lcom/google/android/libraries/social/mediaupload/z;->o:Ljava/io/InputStream;

    invoke-static {v2}, Ljava/nio/channels/Channels;->newChannel(Ljava/io/InputStream;)Ljava/nio/channels/ReadableByteChannel;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/libraries/social/mediaupload/z;->q:Lcom/google/android/libraries/social/mediaupload/ad;

    iget-wide v4, p0, Lcom/google/android/libraries/social/mediaupload/z;->n:J

    iget-wide v8, p0, Lcom/google/android/libraries/social/mediaupload/z;->m:J

    sub-long/2addr v4, v8

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/libraries/social/mediaupload/aa;-><init>(Ljava/nio/channels/ReadableByteChannel;Lcom/google/android/libraries/social/mediaupload/ad;JB)V

    iget-wide v2, p0, Lcom/google/android/libraries/social/mediaupload/z;->n:J

    iget-wide v4, p0, Lcom/google/android/libraries/social/mediaupload/z;->m:J

    sub-long/2addr v2, v4

    invoke-interface {v0, v7, v1, v2, v3}, Le/a/a/g;->a(Ljava/lang/String;Ljava/nio/channels/ReadableByteChannel;J)V

    .line 86
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/z;->a:Le/a/a/g;

    const-string v1, "PUT"

    invoke-interface {v0, v1}, Le/a/a/g;->a(Ljava/lang/String;)V

    .line 88
    const-string v0, "MediaUploader"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    const-string v0, "MediaUploader"

    const-string v1, "UploadMediaRequest [payload]"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :cond_0
    return-void
.end method

.method protected final a(Le/a/a/g;)V
    .locals 0

    .prologue
    .line 101
    return-void
.end method

.method protected final b()Le/a/a/g;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/z;->a:Le/a/a/g;

    return-object v0
.end method
