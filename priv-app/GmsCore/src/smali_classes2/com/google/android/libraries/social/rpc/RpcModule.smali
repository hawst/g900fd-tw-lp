.class public Lcom/google/android/libraries/social/rpc/RpcModule;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/a/f;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/libraries/social/a/a;)V
    .locals 3

    .prologue
    .line 23
    const-class v0, Lcom/google/android/libraries/social/rpc/f;

    if-ne p2, v0, :cond_1

    .line 24
    const-class v0, Lcom/google/android/libraries/social/rpc/f;

    new-instance v1, Lcom/google/android/libraries/social/rpc/d;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/social/rpc/d;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    .line 33
    :cond_0
    :goto_0
    return-void

    .line 25
    :cond_1
    const-class v0, Lcom/google/android/libraries/social/rpc/g;

    if-ne p2, v0, :cond_2

    .line 26
    sget-object v0, Lcom/google/android/libraries/social/rpc/e;->a:Lcom/google/android/libraries/social/f/a;

    .line 27
    const-class v1, Lcom/google/android/libraries/social/rpc/g;

    new-instance v2, Lcom/google/android/libraries/social/rpc/n;

    const-class v0, Lcom/google/android/libraries/social/o/a;

    invoke-virtual {p3, v0}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/o/a;

    invoke-direct {v2, v0}, Lcom/google/android/libraries/social/rpc/n;-><init>(Lcom/google/android/libraries/social/o/a;)V

    invoke-virtual {p3, v1, v2}, Lcom/google/android/libraries/social/a/a;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 31
    :cond_2
    const-class v0, Lcom/google/android/libraries/social/d/a/a;

    if-ne p2, v0, :cond_0

    .line 32
    sget-object v0, Lcom/google/android/libraries/social/d/a/a;->a:Lcom/google/android/libraries/social/f/a;

    goto :goto_0
.end method
