.class public final Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrTimeoutModule;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static ocrTimeoutInMs:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 11
    const-wide/16 v0, 0x1770

    sput-wide v0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrTimeoutModule;->ocrTimeoutInMs:J

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideTimeoutManager()Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;
    .locals 4

    .prologue
    .line 16
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;

    sget-wide v2, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrTimeoutModule;->ocrTimeoutInMs:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;-><init>(J)V

    return-object v0
.end method
