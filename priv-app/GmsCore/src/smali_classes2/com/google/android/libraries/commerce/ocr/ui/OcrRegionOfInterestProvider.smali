.class public interface abstract Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract getBlurDetectorROI()Landroid/graphics/Rect;
.end method

.method public abstract getBoundingBoxRect()Landroid/graphics/Rect;
.end method

.method public abstract getBoundingBoxRectRelativeToCameraPreview()Landroid/graphics/Rect;
.end method

.method public abstract onImageAreaChange(Landroid/graphics/Rect;)V
.end method
