.class public Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment$BatteryReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 170
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 173
    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    move-result-object v0

    .line 174
    invoke-static {v0, p1, p2}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a(Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    .line 176
    if-eqz v1, :cond_0

    .line 178
    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/i;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/i;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/social/autobackup/i;->b(J)V

    .line 181
    iget-boolean v0, v0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a:Z

    if-eqz v0, :cond_0

    .line 182
    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/FingerprintScannerIntentService;->a(Landroid/content/Context;)V

    .line 185
    :cond_0
    return-void
.end method
