.class final Lcom/google/android/libraries/commerce/ocr/capture/pipeline/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory$RetirableRunnable;


# instance fields
.field final synthetic a:Ljava/lang/Object;

.field final synthetic b:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/b;->b:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/b;->a:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final retire()V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/b;->b:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    iget-object v0, v0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->processor:Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/b;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;->discard(Ljava/lang/Object;)V

    .line 114
    return-void
.end method

.method public final run()V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/b;->b:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/b;->a:Ljava/lang/Object;

    # invokes: Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->onProcess(Ljava/lang/Object;)V
    invoke-static {v0, v1}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->access$000(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;Ljava/lang/Object;)V

    .line 109
    return-void
.end method
