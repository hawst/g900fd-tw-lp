.class public interface abstract Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract discard(Ljava/lang/Object;)V
.end method

.method public abstract isProcessingNeeded(Ljava/lang/Object;)Z
.end method

.method public abstract process(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public abstract shutdown()V
.end method
