.class public abstract Lcom/google/android/libraries/social/c/a;
.super Landroid/support/v4/a/a;
.source "SourceFile"


# instance fields
.field protected f:Z

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Landroid/support/v4/a/a;-><init>(Landroid/content/Context;)V

    .line 26
    return-void
.end method

.method private m()V
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/google/android/libraries/social/c/a;->g:Z

    if-eqz v0, :cond_0

    .line 98
    invoke-virtual {p0}, Lcom/google/android/libraries/social/c/a;->l()Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/c/a;->g:Z

    .line 100
    :cond_0
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/android/libraries/social/c/a;->f:Z

    if-nez v0, :cond_0

    .line 60
    invoke-super {p0, p1}, Landroid/support/v4/a/a;->b(Ljava/lang/Object;)V

    .line 62
    :cond_0
    return-void
.end method

.method public final d()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/android/libraries/social/c/a;->f:Z

    if-nez v0, :cond_0

    .line 43
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/c/a;->n()Ljava/lang/Object;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 50
    :goto_0
    return-object v0

    .line 44
    :catch_0
    move-exception v0

    .line 45
    const-string v1, "EsAsyncTaskLoader"

    const-string v2, "loadInBackground failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/c/a;->f:Z

    .line 50
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final e()V
    .locals 1

    .prologue
    .line 72
    invoke-super {p0}, Landroid/support/v4/a/a;->e()V

    .line 73
    invoke-virtual {p0}, Landroid/support/v4/a/j;->a()V

    .line 74
    iget-boolean v0, p0, Lcom/google/android/libraries/social/c/a;->g:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/c/a;->k()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/c/a;->g:Z

    .line 75
    :cond_0
    return-void
.end method

.method protected final f()V
    .locals 0

    .prologue
    .line 66
    invoke-super {p0}, Landroid/support/v4/a/a;->f()V

    .line 67
    invoke-virtual {p0}, Lcom/google/android/libraries/social/c/a;->b()Z

    .line 68
    return-void
.end method

.method protected final g()V
    .locals 0

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/google/android/libraries/social/c/a;->b()Z

    .line 80
    invoke-super {p0}, Landroid/support/v4/a/a;->g()V

    .line 81
    invoke-direct {p0}, Lcom/google/android/libraries/social/c/a;->m()V

    .line 82
    return-void
.end method

.method protected final h()V
    .locals 0

    .prologue
    .line 86
    invoke-super {p0}, Landroid/support/v4/a/a;->h()V

    .line 87
    invoke-direct {p0}, Lcom/google/android/libraries/social/c/a;->m()V

    .line 88
    return-void
.end method

.method protected k()Z
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    return v0
.end method

.method protected l()Z
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x1

    return v0
.end method

.method public abstract n()Ljava/lang/Object;
.end method
