.class public Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# static fields
.field public static overridingModule:Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;


# instance fields
.field private exitHandler:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

.field private extraProcessorProvider:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment$ExtraProcessorProvider;

.field private final isCompleted:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private listener:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment$CreditCardOcrCaptureListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 83
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->isCompleted:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method private constructAndStart()V
    .locals 38

    .prologue
    .line 150
    new-instance v34, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;-><init>(Landroid/support/v4/app/Fragment;)V

    .line 151
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    if-nez v3, :cond_0

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    move-object/from16 v33, v3

    .line 154
    :goto_0
    invoke-virtual/range {v34 .. v34}, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;->provideHandler()Landroid/os/Handler;

    move-result-object v15

    .line 155
    new-instance v17, Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;

    invoke-direct/range {v17 .. v17}, Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;-><init>()V

    .line 156
    invoke-static {}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrTimeoutModule;->provideTimeoutManager()Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;

    move-result-object v22

    .line 157
    new-instance v6, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;

    invoke-direct {v6}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;-><init>()V

    .line 158
    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->getExecutorServiceFactory(Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;)Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory;

    move-result-object v35

    .line 159
    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move-object/from16 v2, v33

    invoke-direct {v0, v1, v2, v6}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->getCameraManager(Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;Landroid/os/Bundle;Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;)Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    move-result-object v5

    .line 160
    invoke-direct/range {p0 .. p0}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->getBlurDetector()Lcom/google/android/libraries/commerce/ocr/cv/BlurDetector;

    move-result-object v36

    .line 161
    new-instance v3, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v5}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;-><init>(Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->exitHandler:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

    .line 162
    new-instance v18, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer;

    invoke-virtual/range {v34 .. v34}, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;->provideContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer;-><init>(Landroid/content/Context;)V

    .line 164
    new-instance v3, Lcom/google/android/libraries/commerce/ocr/cv/CommonOcrCvModule;

    invoke-direct {v3}, Lcom/google/android/libraries/commerce/ocr/cv/CommonOcrCvModule;-><init>()V

    invoke-virtual {v3}, Lcom/google/android/libraries/commerce/ocr/cv/CommonOcrCvModule;->provideOcrImageResourcePool()Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;

    move-result-object v37

    .line 165
    new-instance v3, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;

    invoke-virtual/range {v34 .. v34}, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;->provideScreenManager()Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;

    move-result-object v4

    invoke-static/range {v33 .. v33}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrBundleModule;->provideEdgeFinderWidthToOuterBoundingBoxEdgeLengthRatio(Landroid/os/Bundle;)F

    move-result v7

    invoke-static/range {v33 .. v33}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrBundleModule;->provideImageCaptureMode(Landroid/os/Bundle;)Z

    move-result v8

    invoke-direct/range {v3 .. v8}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;-><init>(Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;FZ)V

    .line 173
    new-instance v11, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayPresenter;

    invoke-direct {v11}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayPresenter;-><init>()V

    .line 175
    new-instance v4, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->exitHandler:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

    invoke-virtual/range {v34 .. v34}, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;->provideVibrator()Landroid/os/Vibrator;

    move-result-object v7

    invoke-direct {v4, v5, v6, v11, v7}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;-><init>(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayPresenter;Landroid/os/Vibrator;)V

    .line 178
    invoke-static/range {p0 .. p0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrUiModule;->providePreviewOverlayView(Landroid/support/v4/app/Fragment;)Landroid/view/View;

    move-result-object v8

    .line 179
    new-instance v6, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    invoke-virtual/range {v34 .. v34}, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;->provideContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v8}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrUiModule;->providePreviewOverlayTextView(Landroid/view/View;)Landroid/widget/TextView;

    move-result-object v9

    invoke-static {v8}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrUiModule;->provideSkipScanButton(Landroid/view/View;)Landroid/widget/Button;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->exitHandler:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

    invoke-static/range {v33 .. v33}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrBundleModule;->provideUiOption(Landroid/os/Bundle;)I

    move-result v14

    move-object v13, v3

    invoke-direct/range {v6 .. v15}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/Button;Lcom/google/android/libraries/commerce/ocr/ui/PreviewOverlayView$Presenter;Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;ILandroid/os/Handler;)V

    .line 189
    new-instance v16, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->exitHandler:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

    move-object/from16 v20, v0

    new-instance v23, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier;

    invoke-direct/range {v23 .. v23}, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier;-><init>()V

    new-instance v24, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator;

    invoke-static {}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CalendarProviderModule;->provideCalendarProvider()Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator$CalendarProvider;

    move-result-object v7

    move-object/from16 v0, v24

    invoke-direct {v0, v7}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator;-><init>(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator$CalendarProvider;)V

    invoke-static/range {v33 .. v33}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrBundleModule;->provideDebugValidateResult(Landroid/os/Bundle;)Z

    move-result v25

    invoke-static/range {v33 .. v33}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrBundleModule;->provideEdgeFinderWidthToOuterBoundingBoxEdgeLengthRatio(Landroid/os/Bundle;)F

    move-result v26

    invoke-static/range {v33 .. v33}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrBundleModule;->provideThresholdMinDigitConfidence(Landroid/os/Bundle;)F

    move-result v27

    invoke-static/range {v33 .. v33}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrBundleModule;->provideThresholdMeanDigitConfidence(Landroid/os/Bundle;)F

    move-result v28

    invoke-static/range {v33 .. v33}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrBundleModule;->provideRecognizeExpirationDate(Landroid/os/Bundle;)Z

    move-result v29

    invoke-static/range {v33 .. v33}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrBundleModule;->provideThresholdExpirationDateMinConfidence4Digit(Landroid/os/Bundle;)F

    move-result v30

    invoke-static/range {v33 .. v33}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrBundleModule;->provideThresholdExpirationDateMinConfidence6Digit(Landroid/os/Bundle;)F

    move-result v31

    invoke-static/range {v33 .. v33}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrBundleModule;->provideImageCaptureMode(Landroid/os/Bundle;)Z

    move-result v32

    move-object/from16 v19, v6

    move-object/from16 v21, v3

    invoke-direct/range {v16 .. v32}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;-><init>(Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer;Lcom/google/android/libraries/commerce/ocr/capture/EdgeChangeListener;Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier;Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator;ZFFFZFFZ)V

    .line 207
    new-instance v28, Lcom/google/android/libraries/commerce/ocr/capture/processors/GatingProcessor;

    invoke-direct/range {v28 .. v28}, Lcom/google/android/libraries/commerce/ocr/capture/processors/GatingProcessor;-><init>()V

    move-object/from16 v18, p0

    move-object/from16 v19, v33

    move-object/from16 v20, v35

    move-object/from16 v21, v5

    move-object/from16 v22, v36

    move-object/from16 v23, v37

    move-object/from16 v24, v17

    move-object/from16 v25, v3

    move-object/from16 v26, v4

    move-object/from16 v27, v16

    .line 208
    invoke-direct/range {v18 .. v28}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->createPipelineProcessor(Landroid/os/Bundle;Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory;Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;Lcom/google/android/libraries/commerce/ocr/cv/BlurDetector;Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;Lcom/google/android/libraries/commerce/ocr/capture/processors/GatingProcessor;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    move-result-object v27

    .line 219
    new-instance v24, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;

    const/16 v29, 0x1

    move-object/from16 v25, v5

    move-object/from16 v26, v37

    invoke-direct/range {v24 .. v29}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;-><init>(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;Lcom/google/android/libraries/commerce/ocr/capture/processors/GatingProcessor;Z)V

    .line 222
    new-instance v4, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;

    invoke-virtual/range {v34 .. v34}, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;->provideContext()Landroid/content/Context;

    move-result-object v7

    move-object/from16 v0, v24

    invoke-direct {v4, v7, v0}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;-><init>(Landroid/content/Context;Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;)V

    .line 224
    new-instance v23, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewImpl;

    invoke-virtual/range {v34 .. v34}, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;->provideContext()Landroid/content/Context;

    move-result-object v7

    move-object/from16 v0, v23

    invoke-direct {v0, v7, v4, v6, v15}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewImpl;-><init>(Landroid/content/Context;Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;Landroid/os/Handler;)V

    .line 228
    new-instance v20, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->exitHandler:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

    move-object/from16 v22, v0

    move-object/from16 v21, p0

    move-object/from16 v25, v11

    move-object/from16 v26, v3

    move-object/from16 v27, v5

    invoke-direct/range {v20 .. v27}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;-><init>(Landroid/support/v4/app/Fragment;Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;Lcom/google/android/libraries/commerce/ocr/ui/OcrView;Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayPresenter;Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;)V

    .line 237
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewPresenter;->attachView()V

    .line 238
    invoke-interface/range {v24 .. v24}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;->enableImageProcessing()V

    .line 239
    return-void

    .line 151
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    move-object/from16 v33, v3

    goto/16 :goto_0
.end method

.method private createPipelineProcessor(Landroid/os/Bundle;Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory;Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;Lcom/google/android/libraries/commerce/ocr/cv/BlurDetector;Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;Lcom/google/android/libraries/commerce/ocr/capture/processors/GatingProcessor;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;
    .locals 8

    .prologue
    .line 251
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/credit/capture/processors/ProcessorModule;

    invoke-direct {v0}, Lcom/google/android/libraries/commerce/ocr/credit/capture/processors/ProcessorModule;-><init>()V

    .line 253
    new-instance v4, Lcom/google/android/libraries/commerce/ocr/capture/InFocusFrameCheck;

    invoke-direct {v4, p4, p3}, Lcom/google/android/libraries/commerce/ocr/capture/InFocusFrameCheck;-><init>(Lcom/google/android/libraries/commerce/ocr/cv/BlurDetector;Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;)V

    move-object v1, p1

    move-object v2, p5

    move-object v3, p2

    move-object v5, p7

    move-object/from16 v6, p9

    move-object/from16 v7, p8

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/libraries/commerce/ocr/credit/capture/processors/ProcessorModule;->provideCreditPipeline(Landroid/os/Bundle;Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory;Lcom/google/android/libraries/commerce/ocr/capture/InFocusFrameCheck;Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    move-result-object v0

    .line 261
    invoke-static/range {p10 .. p10}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline;->forSync(Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline;->wire(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;->to(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;

    move-result-object v6

    .line 264
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->extraProcessorProvider:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment$ExtraProcessorProvider;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->extraProcessorProvider:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment$ExtraProcessorProvider;

    move-object v1, p1

    move-object v2, p6

    move-object v3, p2

    move-object v4, p5

    move-object v5, p7

    invoke-interface/range {v0 .. v5}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment$ExtraProcessorProvider;->getExtraPipelineNode(Landroid/os/Bundle;Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory;Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    move-result-object v0

    .line 267
    if-eqz v0, :cond_0

    .line 268
    invoke-virtual {v6, v0}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;->to(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;)Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;

    .line 271
    :cond_0
    invoke-virtual {v6}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/Pipeline$PipelineBuilder;->getPipelineNode()Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    move-result-object v0

    return-object v0
.end method

.method private getBlurDetector()Lcom/google/android/libraries/commerce/ocr/cv/BlurDetector;
    .locals 1

    .prologue
    .line 281
    sget-object v0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->overridingModule:Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->overridingModule:Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;->provideBlurDetector()Lcom/google/android/libraries/commerce/ocr/cv/BlurDetector;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/cv/BlurDetectorImpl;

    invoke-direct {v0}, Lcom/google/android/libraries/commerce/ocr/cv/BlurDetectorImpl;-><init>()V

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->overridingModule:Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;->provideBlurDetector()Lcom/google/android/libraries/commerce/ocr/cv/BlurDetector;

    move-result-object v0

    goto :goto_0
.end method

.method private getCameraManager(Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;Landroid/os/Bundle;Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;)Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;
    .locals 8

    .prologue
    .line 287
    sget-object v0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->overridingModule:Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->overridingModule:Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;->provideCameraManager()Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    move-result-object v0

    if-nez v0, :cond_1

    .line 288
    :cond_0
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;->provideScreenManager()Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrBundleModule;->provideOrientationOffset(Landroid/os/Bundle;)I

    move-result v2

    invoke-static {p2}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrBundleModule;->provideTargetPreviewSize(Landroid/os/Bundle;)Landroid/graphics/Point;

    move-result-object v4

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;->RATIO:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;

    const/4 v7, 0x0

    move-object v3, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;-><init>(Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;ILcom/google/android/libraries/commerce/ocr/util/ShapeModifier;Landroid/graphics/Point;Landroid/graphics/Point;Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;Z)V

    .line 295
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->overridingModule:Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;->provideCameraManager()Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    move-result-object v0

    goto :goto_0
.end method

.method private getExecutorServiceFactory(Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;)Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory;
    .locals 1

    .prologue
    .line 275
    sget-object v0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->overridingModule:Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->overridingModule:Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;->provideExecutoryFactory()Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;->provideExecutorFactory()Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->overridingModule:Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/testing/CreditTestOverrideModule;->provideExecutoryFactory()Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public capturedResult(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->isCompleted:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->listener:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment$CreditCardOcrCaptureListener;

    invoke-interface {v0, p1, p2}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment$CreditCardOcrCaptureListener;->onCreditCardOcrCaptured(Landroid/content/Intent;I)V

    .line 144
    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 115
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 116
    invoke-direct {p0}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->constructAndStart()V

    .line 117
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 90
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 91
    instance-of v0, p1, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment$CreditCardOcrCaptureListener;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 92
    check-cast v0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment$CreditCardOcrCaptureListener;

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->listener:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment$CreditCardOcrCaptureListener;

    .line 94
    :cond_0
    instance-of v0, p1, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment$ExtraProcessorProvider;

    if-eqz v0, :cond_1

    .line 95
    check-cast p1, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment$ExtraProcessorProvider;

    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->extraProcessorProvider:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment$ExtraProcessorProvider;

    .line 98
    :cond_1
    invoke-static {}, Lcom/google/android/libraries/commerce/ocr/util/NativeLibraryLoader;->loadOcrClientLibrary()V

    .line 99
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 103
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 104
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 109
    sget v0, Lcom/google/android/libraries/commerce/ocr/R$layout;->ocr:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 110
    return-object v0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 136
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 137
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->listener:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment$CreditCardOcrCaptureListener;

    .line 138
    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    .line 128
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 129
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->isCompleted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->exitHandler:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;->exit(ILcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;Landroid/os/Bundle;)V

    .line 132
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 121
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 122
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->isCompleted:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 123
    return-void
.end method
