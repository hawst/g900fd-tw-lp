.class public Lcom/google/android/libraries/social/mediamonitor/g;
.super Landroid/database/ContentObserver;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 22
    iput-object p1, p0, Lcom/google/android/libraries/social/mediamonitor/g;->b:Landroid/content/Context;

    .line 23
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 26
    iget-boolean v0, p0, Lcom/google/android/libraries/social/mediamonitor/g;->a:Z

    if-eqz v0, :cond_1

    .line 35
    :cond_0
    return-void

    .line 29
    :cond_1
    iput-boolean v5, p0, Lcom/google/android/libraries/social/mediamonitor/g;->a:Z

    .line 31
    iget-object v0, p0, Lcom/google/android/libraries/social/mediamonitor/g;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 32
    sget-object v2, Lcom/google/android/libraries/social/mediamonitor/f;->c:[Landroid/net/Uri;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 33
    invoke-virtual {v1, v4, v5, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 32
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onChange(Z)V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/social/mediamonitor/g;->onChange(ZLandroid/net/Uri;)V

    .line 60
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 3

    .prologue
    .line 45
    if-eqz p2, :cond_1

    const-string v0, "blocking"

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 46
    const-string v0, "MediaObserver"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    const-string v0, "MediaObserver"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ignoring uri: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 53
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/mediamonitor/g;->b:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/mediamonitor/c;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/mediamonitor/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/mediamonitor/c;->a(Z)V

    goto :goto_0
.end method
