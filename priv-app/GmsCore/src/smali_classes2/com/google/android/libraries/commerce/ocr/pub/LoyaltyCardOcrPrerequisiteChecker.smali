.class public Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteChecker;


# static fields
.field private static final TAG:Ljava/lang/String; = "LoyaltyCardOcrPrerequisiteChecker"


# instance fields
.field private final allowedCpus:Ljava/util/HashSet;

.field private final blockedMobileNetworkTypes:Ljava/util/HashSet;

.field private final checkerUtil:Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;

.field private final connectivityManager:Landroid/net/ConnectivityManager;

.field private final modelBlacklist:Ljava/util/HashSet;

.field private final telephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;Ljava/lang/String;Ljava/util/HashSet;Landroid/net/ConnectivityManager;Landroid/telephony/TelephonyManager;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 35
    invoke-static {p2}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->split(Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v2

    invoke-static {p6}, Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;->convertBlockedMobileNetworkTypes(Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;-><init>(Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;Ljava/util/HashSet;Ljava/util/HashSet;Landroid/net/ConnectivityManager;Landroid/telephony/TelephonyManager;Ljava/util/HashSet;)V

    .line 41
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;Ljava/util/HashSet;Ljava/util/HashSet;Landroid/net/ConnectivityManager;Landroid/telephony/TelephonyManager;Ljava/util/HashSet;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;->checkerUtil:Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;

    .line 51
    iput-object p3, p0, Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;->allowedCpus:Ljava/util/HashSet;

    .line 52
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;->modelBlacklist:Ljava/util/HashSet;

    .line 53
    iput-object p4, p0, Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;->connectivityManager:Landroid/net/ConnectivityManager;

    .line 54
    iput-object p5, p0, Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 55
    iput-object p6, p0, Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;->blockedMobileNetworkTypes:Ljava/util/HashSet;

    .line 56
    return-void
.end method

.method private static convertBlockedMobileNetworkTypes(Ljava/lang/String;)Ljava/util/HashSet;
    .locals 3

    .prologue
    .line 73
    invoke-static {p0}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->split(Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v0

    .line 75
    new-instance v1, Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 76
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 77
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 79
    :cond_0
    return-object v1
.end method


# virtual methods
.method public check()Z
    .locals 4

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;->checkerUtil:Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->buildVersionOK()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;->checkerUtil:Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;->modelBlacklist:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->modelOK(Ljava/util/HashSet;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;->checkerUtil:Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;->allowedCpus:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->cpuAbiOK(Ljava/util/HashSet;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;->checkerUtil:Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->cameraOK()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;->checkerUtil:Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->orientationOk()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;->checkerUtil:Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;->blockedMobileNetworkTypes:Ljava/util/HashSet;

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;->connectivityManager:Landroid/net/ConnectivityManager;

    iget-object v3, p0, Lcom/google/android/libraries/commerce/ocr/pub/LoyaltyCardOcrPrerequisiteChecker;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->isConnectionOK(Ljava/util/HashSet;Landroid/net/ConnectivityManager;Landroid/telephony/TelephonyManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const-string v0, "LoyaltyCardOcrPrerequisiteChecker"

    const-string v1, "All prerequisites OK."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    const/4 v0, 0x1

    .line 68
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
