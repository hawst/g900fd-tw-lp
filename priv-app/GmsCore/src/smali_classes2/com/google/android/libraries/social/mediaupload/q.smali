.class final Lcom/google/android/libraries/social/mediaupload/q;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:J

.field private final f:Landroid/content/Context;

.field private final g:Lcom/google/android/libraries/social/m/a;

.field private final h:Ljava/lang/String;

.field private final i:Lcom/google/z/a/a/a/b;

.field private j:Z

.field private k:J

.field private l:Landroid/net/Uri;

.field private m:Landroid/net/Uri;

.field private n:Lcom/google/android/libraries/social/m/a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 149
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "datetaken"

    aput-object v1, v0, v2

    const-string v1, "date_added"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/libraries/social/mediaupload/q;->a:[Ljava/lang/String;

    .line 151
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "datetaken"

    aput-object v1, v0, v2

    const-string v1, "date_added"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/libraries/social/mediaupload/q;->b:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lcom/google/z/a/a/a/b;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    iput-object p1, p0, Lcom/google/android/libraries/social/mediaupload/q;->f:Landroid/content/Context;

    .line 177
    iput-object p2, p0, Lcom/google/android/libraries/social/mediaupload/q;->l:Landroid/net/Uri;

    .line 178
    iput-object p2, p0, Lcom/google/android/libraries/social/mediaupload/q;->m:Landroid/net/Uri;

    .line 179
    iput-object p3, p0, Lcom/google/android/libraries/social/mediaupload/q;->h:Ljava/lang/String;

    .line 180
    iput-object v3, p0, Lcom/google/android/libraries/social/mediaupload/q;->i:Lcom/google/z/a/a/a/b;

    .line 181
    if-eqz p4, :cond_0

    :goto_0
    iput-object p4, p0, Lcom/google/android/libraries/social/mediaupload/q;->d:Ljava/lang/String;

    .line 182
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->m:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/mediaupload/q;->a(Landroid/net/Uri;)Lcom/google/android/libraries/social/m/a;

    move-result-object v0

    .line 183
    iput-object v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->g:Lcom/google/android/libraries/social/m/a;

    .line 184
    iget-object v1, p0, Lcom/google/android/libraries/social/mediaupload/q;->g:Lcom/google/android/libraries/social/m/a;

    iput-object v1, p0, Lcom/google/android/libraries/social/mediaupload/q;->n:Lcom/google/android/libraries/social/m/a;

    .line 185
    invoke-virtual {v0}, Lcom/google/android/libraries/social/m/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->k:J

    .line 186
    iget-wide v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->k:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-gtz v0, :cond_1

    .line 187
    new-instance v0, Lcom/google/android/libraries/social/mediaupload/f;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Empty content at "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/libraries/social/mediaupload/q;->m:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/mediaupload/f;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :cond_0
    invoke-static {p1, p2}, Lcom/google/android/libraries/social/mediaupload/ae;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object p4

    goto :goto_0

    .line 189
    :cond_1
    invoke-static {p2}, Lcom/google/android/libraries/b/a/b;->b(Landroid/net/Uri;)Z

    move-result v0

    .line 191
    if-eqz v0, :cond_6

    .line 192
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 194
    iget-object v1, p0, Lcom/google/android/libraries/social/mediaupload/q;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/libraries/b/a/b;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 195
    sget-object v2, Lcom/google/android/libraries/social/mediaupload/q;->a:[Ljava/lang/String;

    move-object v1, p2

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 196
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->m:Landroid/net/Uri;

    invoke-static {v1, v0}, Lcom/google/android/libraries/social/mediaupload/c;->a(Landroid/database/Cursor;Landroid/net/Uri;)V

    .line 197
    const-string v0, "_data"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 198
    const-string v2, "datetaken"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/libraries/social/mediaupload/q;->e:J

    .line 211
    :goto_1
    if-nez v0, :cond_2

    .line 212
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 214
    :cond_2
    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 215
    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_3
    iput-object v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->c:Ljava/lang/String;

    .line 216
    return-void

    .line 199
    :cond_4
    iget-object v1, p0, Lcom/google/android/libraries/social/mediaupload/q;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/libraries/b/a/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 200
    sget-object v2, Lcom/google/android/libraries/social/mediaupload/q;->b:[Ljava/lang/String;

    move-object v1, p2

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 201
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->m:Landroid/net/Uri;

    invoke-static {v1, v0}, Lcom/google/android/libraries/social/mediaupload/c;->a(Landroid/database/Cursor;Landroid/net/Uri;)V

    .line 202
    const-string v0, "_data"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 203
    const-string v2, "datetaken"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/libraries/social/mediaupload/q;->e:J

    goto :goto_1

    .line 205
    :cond_5
    new-instance v0, Lcom/google/android/libraries/social/mediaupload/e;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid content at "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/social/mediaupload/e;-><init>(Ljava/lang/String;Z)V

    throw v0

    .line 208
    :cond_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->e:J

    move-object v0, v3

    goto :goto_1
.end method

.method private a(Landroid/net/Uri;)Lcom/google/android/libraries/social/m/a;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 341
    iget-object v1, p0, Lcom/google/android/libraries/social/mediaupload/q;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 343
    :try_start_0
    invoke-virtual {v1, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 344
    invoke-static {v1}, Lcom/google/android/libraries/social/m/a;->a(Ljava/io/InputStream;)Lcom/google/android/libraries/social/m/a;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 348
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0

    .line 346
    :catch_1
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public final a(J)Ljava/io/InputStream;
    .locals 5

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/mediaupload/q;->m:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 239
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 240
    const-wide/16 v2, 0x0

    cmp-long v0, p1, v2

    if-lez v0, :cond_0

    .line 241
    invoke-virtual {v1, p1, p2}, Ljava/io/BufferedInputStream;->skip(J)J

    .line 243
    :cond_0
    return-object v1
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->f:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/libraries/social/mediaupload/q;->m:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/mediaupload/ae;->c(Landroid/content/Context;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 225
    if-eqz v0, :cond_0

    .line 226
    iput-object v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->m:Landroid/net/Uri;

    .line 227
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->m:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/mediaupload/q;->a(Landroid/net/Uri;)Lcom/google/android/libraries/social/m/a;

    move-result-object v0

    .line 228
    iput-object v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->n:Lcom/google/android/libraries/social/m/a;

    .line 229
    invoke-virtual {v0}, Lcom/google/android/libraries/social/m/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->k:J

    .line 230
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->j:Z

    .line 232
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 250
    iget-boolean v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->j:Z

    return v0
.end method

.method public final c()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->m:Landroid/net/Uri;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 288
    iget-wide v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->e:J

    return-wide v0
.end method

.method public final g()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->l:Landroid/net/Uri;

    return-object v0
.end method

.method public final h()Lcom/google/android/libraries/social/m/a;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->g:Lcom/google/android/libraries/social/m/a;

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/social/m/a;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->n:Lcom/google/android/libraries/social/m/a;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final k()J
    .locals 2

    .prologue
    .line 329
    iget-wide v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->k:J

    return-wide v0
.end method

.method public final l()Lcom/google/z/a/a/a/b;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/q;->i:Lcom/google/z/a/a/a/b;

    return-object v0
.end method
