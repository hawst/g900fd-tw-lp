.class public abstract Lcom/google/android/libraries/social/a/a/a/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/a/a/b;
.implements Lcom/google/android/libraries/social/a/f;
.implements Lcom/google/android/libraries/social/i/ah;
.implements Lcom/google/android/libraries/social/i/ak;


# instance fields
.field protected final a:Lcom/google/android/libraries/social/i/w;

.field private b:Ljava/util/Set;

.field private final c:Ljava/lang/Class;

.field private d:Lcom/google/android/libraries/social/a/a/a/e;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/i/w;Ljava/lang/Class;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/a/a/a/f;->b:Ljava/util/Set;

    .line 38
    iput-object p1, p0, Lcom/google/android/libraries/social/a/a/a/f;->a:Lcom/google/android/libraries/social/i/w;

    .line 39
    iput-object p2, p0, Lcom/google/android/libraries/social/a/a/a/f;->c:Ljava/lang/Class;

    .line 40
    invoke-virtual {p1, p0}, Lcom/google/android/libraries/social/i/w;->a(Lcom/google/android/libraries/social/i/ak;)Lcom/google/android/libraries/social/i/ak;

    .line 41
    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/content/Context;)Lcom/google/android/libraries/social/a/a/a/e;
.end method

.method public final declared-synchronized a(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/libraries/social/a/a;)V
    .locals 3

    .prologue
    .line 47
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a/a/f;->c:Ljava/lang/Class;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne p2, v0, :cond_1

    .line 64
    :cond_0
    monitor-exit p0

    return-void

    .line 51
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a/a/f;->d:Lcom/google/android/libraries/social/a/a/a/e;

    if-nez v0, :cond_2

    .line 52
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/a/a/a/f;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/a/a/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/a/a/a/f;->d:Lcom/google/android/libraries/social/a/a/a/e;

    .line 55
    :cond_2
    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 56
    iget-object v1, p0, Lcom/google/android/libraries/social/a/a/a/f;->d:Lcom/google/android/libraries/social/a/a/a/e;

    invoke-virtual {v1, p2}, Lcom/google/android/libraries/social/a/a/a/e;->a(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v1

    .line 57
    if-eqz v1, :cond_0

    .line 58
    iget-object v2, p0, Lcom/google/android/libraries/social/a/a/a/f;->b:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 60
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/a/a/a/d;

    .line 61
    iget-object v2, p0, Lcom/google/android/libraries/social/a/a/a/f;->a:Lcom/google/android/libraries/social/i/w;

    invoke-virtual {p0, v0, v2, p3}, Lcom/google/android/libraries/social/a/a/a/f;->a(Lcom/google/android/libraries/social/a/a/a/d;Lcom/google/android/libraries/social/i/w;Lcom/google/android/libraries/social/a/a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract a(Lcom/google/android/libraries/social/a/a/a/d;Lcom/google/android/libraries/social/i/w;Lcom/google/android/libraries/social/a/a;)V
.end method

.method public final a(Lcom/google/android/libraries/social/a/a;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 71
    if-eqz p2, :cond_0

    .line 72
    const-string v0, "extra_auto_bound_objects"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 74
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 76
    :try_start_0
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 77
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 78
    :catch_0
    move-exception v0

    .line 79
    const-string v4, "BaseAutoBinderModule"

    const-string v5, "Autobound class not found upon reconstruction"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 83
    :cond_0
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a/a/f;->b:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/libraries/social/a/a/a/f;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 88
    const-string v1, "extra_auto_bound_objects"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 89
    return-void
.end method
