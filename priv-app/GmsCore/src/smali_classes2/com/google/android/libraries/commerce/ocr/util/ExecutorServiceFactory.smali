.class public Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    return-void
.end method


# virtual methods
.method public create(IIJI)Ljava/util/concurrent/ExecutorService;
    .locals 9

    .prologue
    .line 28
    new-instance v7, Lcom/google/android/libraries/commerce/ocr/util/a;

    invoke-direct {v7}, Lcom/google/android/libraries/commerce/ocr/util/a;-><init>()V

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-wide v4, p3

    move v6, p5

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory;->create(IIJILjava/util/concurrent/RejectedExecutionHandler;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method

.method protected create(IIJILjava/util/concurrent/RejectedExecutionHandler;)Ljava/util/concurrent/ExecutorService;
    .locals 9

    .prologue
    .line 37
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-direct {v7, p5}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    move v2, p1

    move v3, p2

    move-wide v4, p3

    move-object v8, p6

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/RejectedExecutionHandler;)V

    return-object v1
.end method
