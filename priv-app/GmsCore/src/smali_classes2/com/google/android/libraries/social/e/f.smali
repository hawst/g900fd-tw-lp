.class final Lcom/google/android/libraries/social/e/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/nio/ByteBuffer;

.field private final b:Lcom/google/android/libraries/social/e/d;

.field private final c:Ljava/util/List;

.field private final d:Lcom/google/android/libraries/social/e/c;

.field private e:I


# direct methods
.method protected constructor <init>(Ljava/nio/ByteBuffer;Lcom/google/android/libraries/social/e/c;)V
    .locals 4

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/e/f;->c:Ljava/util/List;

    .line 52
    iput-object p1, p0, Lcom/google/android/libraries/social/e/f;->a:Ljava/nio/ByteBuffer;

    .line 53
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/e/f;->e:I

    .line 54
    iput-object p2, p0, Lcom/google/android/libraries/social/e/f;->d:Lcom/google/android/libraries/social/e/c;

    .line 55
    const/4 v2, 0x0

    .line 57
    :try_start_0
    new-instance v1, Lcom/google/android/libraries/social/e/a;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/social/e/a;-><init>(Ljava/nio/ByteBuffer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/social/e/f;->d:Lcom/google/android/libraries/social/e/c;

    invoke-static {v1, v0}, Lcom/google/android/libraries/social/e/i;->a(Ljava/io/InputStream;Lcom/google/android/libraries/social/e/c;)Lcom/google/android/libraries/social/e/i;

    move-result-object v0

    .line 60
    new-instance v2, Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/i;->j()Ljava/nio/ByteOrder;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/libraries/social/e/d;-><init>(Ljava/nio/ByteOrder;)V

    iput-object v2, p0, Lcom/google/android/libraries/social/e/f;->b:Lcom/google/android/libraries/social/e/d;

    .line 61
    iget v2, p0, Lcom/google/android/libraries/social/e/f;->e:I

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/i;->i()I

    move-result v0

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/libraries/social/e/f;->e:I

    .line 62
    iget-object v0, p0, Lcom/google/android/libraries/social/e/f;->a:Ljava/nio/ByteBuffer;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 64
    invoke-static {v1}, Lcom/google/android/libraries/social/e/c;->a(Ljava/io/Closeable;)V

    .line 65
    return-void

    .line 64
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_0
    invoke-static {v1}, Lcom/google/android/libraries/social/e/c;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/libraries/social/e/n;)V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/libraries/social/e/f;->b:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/e/d;->a(Lcom/google/android/libraries/social/e/n;)Lcom/google/android/libraries/social/e/n;

    .line 203
    return-void
.end method

.method protected final a()Z
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 73
    const/4 v4, 0x0

    .line 75
    :try_start_0
    new-instance v3, Lcom/google/android/libraries/social/e/a;

    iget-object v0, p0, Lcom/google/android/libraries/social/e/f;->a:Ljava/nio/ByteBuffer;

    invoke-direct {v3, v0}, Lcom/google/android/libraries/social/e/a;-><init>(Ljava/nio/ByteBuffer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 77
    const/4 v0, 0x5

    :try_start_1
    new-array v5, v0, [Lcom/google/android/libraries/social/e/o;

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/google/android/libraries/social/e/f;->b:Lcom/google/android/libraries/social/e/d;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v4

    aput-object v4, v5, v0

    const/4 v0, 0x1

    iget-object v4, p0, Lcom/google/android/libraries/social/e/f;->b:Lcom/google/android/libraries/social/e/d;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v4

    aput-object v4, v5, v0

    const/4 v0, 0x2

    iget-object v4, p0, Lcom/google/android/libraries/social/e/f;->b:Lcom/google/android/libraries/social/e/d;

    const/4 v6, 0x2

    invoke-virtual {v4, v6}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v4

    aput-object v4, v5, v0

    const/4 v0, 0x3

    iget-object v4, p0, Lcom/google/android/libraries/social/e/f;->b:Lcom/google/android/libraries/social/e/d;

    const/4 v6, 0x3

    invoke-virtual {v4, v6}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v4

    aput-object v4, v5, v0

    const/4 v0, 0x4

    iget-object v4, p0, Lcom/google/android/libraries/social/e/f;->b:Lcom/google/android/libraries/social/e/d;

    const/4 v6, 0x4

    invoke-virtual {v4, v6}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v4

    aput-object v4, v5, v0

    .line 85
    const/4 v0, 0x0

    aget-object v0, v5, v0

    if-eqz v0, :cond_d

    move v0, v2

    .line 88
    :goto_0
    const/4 v4, 0x1

    aget-object v4, v5, v4

    if-eqz v4, :cond_0

    .line 89
    or-int/lit8 v0, v0, 0x2

    .line 91
    :cond_0
    const/4 v4, 0x2

    aget-object v4, v5, v4

    if-eqz v4, :cond_1

    .line 92
    or-int/lit8 v0, v0, 0x4

    .line 94
    :cond_1
    const/4 v4, 0x4

    aget-object v4, v5, v4

    if-eqz v4, :cond_2

    .line 95
    or-int/lit8 v0, v0, 0x8

    .line 97
    :cond_2
    const/4 v4, 0x3

    aget-object v4, v5, v4

    if-eqz v4, :cond_3

    .line 98
    or-int/lit8 v0, v0, 0x10

    .line 101
    :cond_3
    iget-object v4, p0, Lcom/google/android/libraries/social/e/f;->d:Lcom/google/android/libraries/social/e/c;

    invoke-static {v3, v0, v4}, Lcom/google/android/libraries/social/e/i;->a(Ljava/io/InputStream;ILcom/google/android/libraries/social/e/c;)Lcom/google/android/libraries/social/e/i;

    move-result-object v6

    .line 102
    invoke-virtual {v6}, Lcom/google/android/libraries/social/e/i;->a()I

    move-result v4

    .line 103
    const/4 v0, 0x0

    .line 104
    :goto_1
    const/4 v7, 0x5

    if-eq v4, v7, :cond_7

    .line 105
    packed-switch v4, :pswitch_data_0

    .line 129
    :cond_4
    :goto_2
    invoke-virtual {v6}, Lcom/google/android/libraries/social/e/i;->a()I

    move-result v4

    goto :goto_1

    .line 107
    :pswitch_0
    invoke-virtual {v6}, Lcom/google/android/libraries/social/e/i;->d()I

    move-result v0

    aget-object v0, v5, v0

    .line 108
    if-nez v0, :cond_4

    .line 109
    invoke-virtual {v6}, Lcom/google/android/libraries/social/e/i;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 138
    :catchall_0
    move-exception v0

    move-object v1, v3

    :goto_3
    invoke-static {v1}, Lcom/google/android/libraries/social/e/c;->a(Ljava/io/Closeable;)V

    throw v0

    .line 113
    :pswitch_1
    :try_start_2
    invoke-virtual {v6}, Lcom/google/android/libraries/social/e/i;->c()Lcom/google/android/libraries/social/e/n;

    move-result-object v4

    .line 114
    invoke-virtual {v4}, Lcom/google/android/libraries/social/e/n;->b()S

    move-result v7

    invoke-virtual {v0, v7}, Lcom/google/android/libraries/social/e/o;->a(S)Lcom/google/android/libraries/social/e/n;

    move-result-object v7

    .line 115
    if-eqz v7, :cond_4

    .line 116
    invoke-virtual {v7}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v8

    invoke-virtual {v4}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v9

    if-ne v8, v9, :cond_5

    invoke-virtual {v7}, Lcom/google/android/libraries/social/e/n;->c()S

    move-result v8

    invoke-virtual {v4}, Lcom/google/android/libraries/social/e/n;->c()S
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v9

    if-eq v8, v9, :cond_6

    .line 118
    :cond_5
    invoke-static {v3}, Lcom/google/android/libraries/social/e/c;->a(Ljava/io/Closeable;)V

    move v0, v1

    .line 140
    :goto_4
    return v0

    .line 120
    :cond_6
    :try_start_3
    iget-object v8, p0, Lcom/google/android/libraries/social/e/f;->c:Ljava/util/List;

    new-instance v9, Lcom/google/android/libraries/social/e/g;

    invoke-virtual {v4}, Lcom/google/android/libraries/social/e/n;->j()I

    move-result v10

    invoke-direct {v9, v7, v10}, Lcom/google/android/libraries/social/e/g;-><init>(Lcom/google/android/libraries/social/e/n;I)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    invoke-virtual {v4}, Lcom/google/android/libraries/social/e/n;->b()S

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/e/o;->b(S)V

    .line 122
    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/o;->d()I

    move-result v4

    if-nez v4, :cond_4

    .line 123
    invoke-virtual {v6}, Lcom/google/android/libraries/social/e/i;->b()V

    goto :goto_2

    .line 131
    :cond_7
    array-length v4, v5

    move v0, v1

    :goto_5
    if-ge v0, v4, :cond_9

    aget-object v6, v5, v0

    .line 132
    if-eqz v6, :cond_8

    invoke-virtual {v6}, Lcom/google/android/libraries/social/e/o;->d()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v6

    if-lez v6, :cond_8

    .line 133
    invoke-static {v3}, Lcom/google/android/libraries/social/e/c;->a(Ljava/io/Closeable;)V

    move v0, v1

    goto :goto_4

    .line 131
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 136
    :cond_9
    :try_start_4
    iget-object v0, p0, Lcom/google/android/libraries/social/e/f;->a:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/libraries/social/e/f;->b:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v4}, Lcom/google/android/libraries/social/e/d;->e()Ljava/nio/ByteOrder;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/google/android/libraries/social/e/f;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_a
    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/e/g;

    iget-object v5, v0, Lcom/google/android/libraries/social/e/g;->b:Lcom/google/android/libraries/social/e/n;

    iget v0, v0, Lcom/google/android/libraries/social/e/g;->a:I

    invoke-virtual {v5}, Lcom/google/android/libraries/social/e/n;->f()Z

    move-result v6

    if-eqz v6, :cond_a

    iget-object v6, p0, Lcom/google/android/libraries/social/e/f;->a:Ljava/nio/ByteBuffer;

    iget v7, p0, Lcom/google/android/libraries/social/e/f;->e:I

    add-int/2addr v0, v7

    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    invoke-virtual {v5}, Lcom/google/android/libraries/social/e/n;->c()S

    move-result v0

    packed-switch v0, :pswitch_data_1

    :pswitch_2
    goto :goto_6

    :pswitch_3
    invoke-virtual {v5}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v0

    new-array v0, v0, [B

    invoke-virtual {v5, v0}, Lcom/google/android/libraries/social/e/n;->b([B)V

    iget-object v5, p0, Lcom/google/android/libraries/social/e/f;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    goto :goto_6

    :pswitch_4
    invoke-virtual {v5}, Lcom/google/android/libraries/social/e/n;->i()[B

    move-result-object v0

    array-length v6, v0

    invoke-virtual {v5}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v5

    if-ne v6, v5, :cond_b

    array-length v5, v0

    add-int/lit8 v5, v5, -0x1

    const/4 v6, 0x0

    aput-byte v6, v0, v5

    iget-object v5, p0, Lcom/google/android/libraries/social/e/f;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    goto :goto_6

    :cond_b
    iget-object v5, p0, Lcom/google/android/libraries/social/e/f;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/google/android/libraries/social/e/f;->a:Ljava/nio/ByteBuffer;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto :goto_6

    :pswitch_5
    invoke-virtual {v5}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v6

    move v0, v1

    :goto_7
    if-ge v0, v6, :cond_a

    iget-object v7, p0, Lcom/google/android/libraries/social/e/f;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v0}, Lcom/google/android/libraries/social/e/n;->e(I)J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :pswitch_6
    invoke-virtual {v5}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v6

    move v0, v1

    :goto_8
    if-ge v0, v6, :cond_a

    invoke-virtual {v5, v0}, Lcom/google/android/libraries/social/e/n;->f(I)Lcom/google/android/libraries/social/e/r;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/libraries/social/e/f;->a:Ljava/nio/ByteBuffer;

    iget-wide v10, v7, Lcom/google/android/libraries/social/e/r;->a:J

    long-to-int v9, v10

    invoke-virtual {v8, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v8, p0, Lcom/google/android/libraries/social/e/f;->a:Ljava/nio/ByteBuffer;

    iget-wide v10, v7, Lcom/google/android/libraries/social/e/r;->b:J

    long-to-int v7, v10

    invoke-virtual {v8, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :pswitch_7
    invoke-virtual {v5}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v6

    move v0, v1

    :goto_9
    if-ge v0, v6, :cond_a

    iget-object v7, p0, Lcom/google/android/libraries/social/e/f;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v0}, Lcom/google/android/libraries/social/e/n;->e(I)J

    move-result-wide v8

    long-to-int v8, v8

    int-to-short v8, v8

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 138
    :cond_c
    invoke-static {v3}, Lcom/google/android/libraries/social/e/c;->a(Ljava/io/Closeable;)V

    move v0, v2

    .line 140
    goto/16 :goto_4

    .line 138
    :catchall_1
    move-exception v0

    move-object v1, v4

    goto/16 :goto_3

    :cond_d
    move v0, v1

    goto/16 :goto_0

    .line 105
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 136
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_5
        :pswitch_6
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
