.class final Lcom/google/android/libraries/social/autobackup/m;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/libraries/social/account/b;

.field private b:Lcom/google/android/libraries/social/autobackup/n;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 73
    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/m;->a:Lcom/google/android/libraries/social/account/b;

    .line 74
    return-void
.end method

.method private a(IZLandroid/content/SyncResult;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    .line 125
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/m;->a:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/account/b;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/m;->a:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/libraries/social/account/c;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 127
    :goto_0
    if-nez v0, :cond_2

    .line 128
    const-string v0, "iu.SyncService"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    const-string v0, "iu.SyncService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "====> Account is not signed in: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    :cond_0
    :goto_1
    return-void

    .line 125
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 134
    :cond_2
    const-string v0, "iu.SyncService"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 135
    if-eqz p2, :cond_4

    .line 136
    const-string v0, "iu.SyncService"

    const-string v1, "====> Periodic up sync"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    :cond_3
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/m;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/google/android/libraries/social/autobackup/h;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 144
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/h;

    .line 145
    invoke-interface {v0}, Lcom/google/android/libraries/social/autobackup/h;->a()V

    goto :goto_3

    .line 138
    :cond_4
    const-string v0, "iu.SyncService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "====> Manual or requested up sync account="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 149
    :cond_5
    const/4 v0, -0x1

    if-ne p1, v0, :cond_7

    const-string v0, "iu.SyncService"

    const-string v1, "----> performUpSync account not found"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    :cond_6
    :goto_4
    const-string v0, "iu.SyncService"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    const-string v0, "iu.SyncService"

    const-string v1, "====> Sync complete"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 149
    :cond_7
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/m;->a:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/m;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/google/android/libraries/social/autobackup/ac;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/ac;

    invoke-interface {v0}, Lcom/google/android/libraries/social/autobackup/ac;->a()I

    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/m;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/i;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/i;

    move-result-object v0

    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v1

    if-eqz v1, :cond_8

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    const-string v1, "iu.SyncService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "----> performUpSync error for account: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    goto :goto_4

    :cond_8
    :try_start_3
    new-instance v1, Lcom/google/android/libraries/social/autobackup/n;

    invoke-direct {v1, p1, p3}, Lcom/google/android/libraries/social/autobackup/n;-><init>(ILandroid/content/SyncResult;)V

    iput-object v1, p0, Lcom/google/android/libraries/social/autobackup/m;->b:Lcom/google/android/libraries/social/autobackup/n;

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    const-string v1, "iu.SyncService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "iu.SyncService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "----> Start up sync for account: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/m;->b:Lcom/google/android/libraries/social/autobackup/n;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/i;->a(Lcom/google/android/libraries/social/autobackup/n;)V

    const-string v0, "iu.SyncService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "iu.SyncService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "----> Up sync finished for account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", cancelled? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/m;->b:Lcom/google/android/libraries/social/autobackup/n;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/n;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_4
.end method


# virtual methods
.method public final onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 90
    if-eqz p2, :cond_1

    const-string v2, "initialize"

    invoke-virtual {p2, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v0

    .line 92
    :goto_0
    if-eqz v2, :cond_3

    .line 93
    new-instance v2, Landroid/accounts/Account;

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v4, "com.google"

    invoke-direct {v2, v3, v4}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/m;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/libraries/social/autobackup/aj;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/m;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->c(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    :goto_1
    invoke-static {v2, v3, v0}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 121
    :cond_0
    return-void

    :cond_1
    move v2, v1

    .line 90
    goto :goto_0

    :cond_2
    move v0, v1

    .line 93
    goto :goto_1

    .line 101
    :cond_3
    if-eqz p2, :cond_4

    const-string v2, "sync_periodic"

    invoke-virtual {p2, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_4

    move v1, v0

    .line 105
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/m;->a:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0}, Lcom/google/android/libraries/social/account/b;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 107
    :try_start_0
    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/m;->a:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v3, v0}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v3

    const-string v4, "account_name"

    invoke-interface {v3, v4}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 110
    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 111
    invoke-direct {p0, v0, v1, p5}, Lcom/google/android/libraries/social/autobackup/m;->a(IZLandroid/content/SyncResult;)V
    :try_end_0
    .catch Lcom/google/android/libraries/social/account/e; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 113
    :catch_0
    move-exception v0

    .line 116
    const-string v3, "iu.SyncService"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 117
    const-string v3, "iu.SyncService"

    const-string v4, "Account became invalid during sync"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method public final declared-synchronized onSyncCanceled()V
    .locals 2

    .prologue
    .line 78
    monitor-enter p0

    :try_start_0
    const-string v0, "iu.SyncService"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    const-string v0, "iu.SyncService"

    const-string v1, "Received cancel request"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    :cond_0
    invoke-super {p0}, Landroid/content/AbstractThreadedSyncAdapter;->onSyncCanceled()V

    .line 82
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/m;->b:Lcom/google/android/libraries/social/autobackup/n;

    if-eqz v0, :cond_1

    .line 83
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/m;->b:Lcom/google/android/libraries/social/autobackup/n;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/n;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    :cond_1
    monitor-exit p0

    return-void

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
