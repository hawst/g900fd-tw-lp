.class final Lcom/google/android/libraries/social/mediaupload/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;

.field final c:Z

.field final d:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 370
    iput-object p1, p0, Lcom/google/android/libraries/social/mediaupload/m;->a:Ljava/lang/String;

    .line 371
    iput-object p2, p0, Lcom/google/android/libraries/social/mediaupload/m;->b:Ljava/lang/String;

    .line 372
    iput-boolean p3, p0, Lcom/google/android/libraries/social/mediaupload/m;->c:Z

    .line 373
    iput-object p4, p0, Lcom/google/android/libraries/social/mediaupload/m;->d:Ljava/lang/String;

    .line 374
    return-void
.end method

.method public static a(Lcom/google/android/libraries/social/mediaupload/q;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 420
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 422
    :try_start_0
    const-string v1, "resumeUrl"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 423
    const-string v1, "resumeFingerprint"

    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediaupload/q;->h()Lcom/google/android/libraries/social/m/a;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 424
    const-string v1, "resumeForceResize"

    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediaupload/q;->b()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 425
    const-string v1, "resumeContentType"

    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediaupload/q;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 426
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 428
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method
