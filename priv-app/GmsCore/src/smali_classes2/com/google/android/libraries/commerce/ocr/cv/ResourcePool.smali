.class public Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/cv/RecyclablePool;


# static fields
.field private static final TAG:Ljava/lang/String; = "ResourcePool"


# instance fields
.field private final available:Ljava/util/Stack;

.field private isClosed:Z

.field private final pool:Ljava/util/ArrayList;

.field private final poolCapacity:I

.field private final provider:Lcom/google/android/libraries/commerce/ocr/util/Provider;


# direct methods
.method public constructor <init>(ILcom/google/android/libraries/commerce/ocr/util/Provider;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput p1, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->poolCapacity:I

    .line 38
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->provider:Lcom/google/android/libraries/commerce/ocr/util/Provider;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->pool:Ljava/util/ArrayList;

    .line 40
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->available:Ljava/util/Stack;

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->isClosed:Z

    .line 42
    return-void
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 56
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->isClosed:Z

    .line 57
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->pool:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 58
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->available:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    monitor-exit p0

    return-void

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized evict()V
    .locals 1

    .prologue
    .line 66
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->isClosed:Z

    .line 67
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->pool:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 68
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->available:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    monitor-exit p0

    return-void

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getAvailableResources()I
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->available:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    return v0
.end method

.method declared-synchronized isAvailable(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 128
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->available:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isInPool(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 123
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->pool:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized obtain()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 86
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->isClosed:Z

    if-eqz v0, :cond_0

    .line 87
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool$ResourcePoolClosedException;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool$ResourcePoolClosedException;-><init>(Lcom/google/android/libraries/commerce/ocr/cv/b;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 90
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->available:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 91
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->available:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 97
    :goto_0
    monitor-exit p0

    return-object v0

    .line 93
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->pool:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->poolCapacity:I

    if-ge v0, v1, :cond_2

    .line 94
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->provider:Lcom/google/android/libraries/commerce/ocr/util/Provider;

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/util/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    .line 95
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->pool:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    const-string v1, "ResourcePool"

    const-string v2, "add (%s) to pool. Size: %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->pool:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 100
    :cond_2
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Pool is exhausted"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public declared-synchronized obtainSafePoolable()Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;
    .locals 2

    .prologue
    .line 109
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->obtain()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;-><init>(Lcom/google/android/libraries/commerce/ocr/cv/RecyclablePool;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool$ResourcePoolClosedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    :goto_0
    monitor-exit p0

    return-object v0

    :catch_0
    move-exception v0

    .line 115
    :goto_1
    :try_start_1
    const-string v0, "ResourcePool"

    const-string v1, "Pool limit reached, returning unmanaged resource."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->provider:Lcom/google/android/libraries/commerce/ocr/util/Provider;

    invoke-interface {v1}, Lcom/google/android/libraries/commerce/ocr/util/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;-><init>(Lcom/google/android/libraries/commerce/ocr/cv/RecyclablePool;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 109
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 114
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public declared-synchronized recycle(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 135
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->isInPool(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 139
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 138
    :cond_1
    :try_start_1
    iget-boolean v2, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->isClosed:Z

    if-nez v2, :cond_3

    move v2, v1

    :goto_1
    const-string v3, "Object should not be in a *closed* pool"

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 139
    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->available:Ljava/util/Stack;

    invoke-virtual {v2, p1}, Ljava/util/Stack;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->available:Ljava/util/Stack;

    invoke-virtual {v2, p1}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v2, v0

    .line 138
    goto :goto_1

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
