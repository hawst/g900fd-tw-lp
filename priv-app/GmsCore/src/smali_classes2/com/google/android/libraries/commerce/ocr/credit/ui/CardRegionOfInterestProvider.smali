.class public final Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;


# static fields
.field private static final CARD_NUMBER_BOTTOM_TO_CARD_HEIGHT_RATIO:F = 0.65f

.field private static final CARD_NUMBER_LEFT_TO_CARD_WIDTH_RATIO:F = 0.1f

.field private static final CARD_NUMBER_RIGHT_TO_CARD_WIDTH_RATIO:F = 0.9f

.field private static final CARD_NUMBER_TOP_TO_CARD_HEIGHT_RATIO:F = 0.52f

.field private static final CC_WIDTH_TO_HEIGHT_RATIO:F = 1.5857725f

.field private static final NON_CARD_BLUR_DETECTION_ROI_TO_BOUNDING_BOX_SIZE_RATIO:F = 0.25f

.field private static final OUTER_BOUNDING_BOX_WIDTH_TO_SCREEN_WIDTH_RATIO:F = 0.99f

.field private static final TAG:Ljava/lang/String; = "CardRegionOfInterestProvider"


# instance fields
.field private blurDetectorRoiRect:Landroid/graphics/Rect;

.field private boundingBoxRect:Landroid/graphics/Rect;

.field private boundingBoxRectRelativeToImage:Landroid/graphics/Rect;

.field private final calculationStrategy:Lcom/google/android/libraries/commerce/ocr/util/Function;

.field private final cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

.field private final edgeFinderWidthToOuterBoundingBoxEdgeLengthRatio:F

.field private midBoundingBoxRect:Landroid/graphics/Rect;

.field private final screenManager:Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;

.field private final shapeModifier:Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;FZ)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->screenManager:Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;

    .line 56
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    .line 57
    iput-object p3, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->shapeModifier:Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;

    .line 58
    iput p4, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->edgeFinderWidthToOuterBoundingBoxEdgeLengthRatio:F

    .line 60
    invoke-direct {p0, p5}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->getBlurRegionCalculator(Z)Lcom/google/android/libraries/commerce/ocr/util/Function;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->calculationStrategy:Lcom/google/android/libraries/commerce/ocr/util/Function;

    .line 61
    return-void
.end method

.method private calculateOuterBoundingBoxArea(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 4

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->screenManager:Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;->isInPortraitMode()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    .line 131
    :goto_0
    const v1, 0x3f7d70a4    # 0.99f

    mul-float/2addr v0, v1

    .line 132
    const v1, 0x3fcafa98

    div-float v1, v0, v1

    .line 134
    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->shapeModifier:Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;

    iget-object v3, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->shapeModifier:Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;

    invoke-virtual {v3, p1, v0, v1}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;->getCenteredRect(Landroid/graphics/Rect;FF)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;->toRect(Landroid/graphics/RectF;)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0

    .line 129
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    goto :goto_0
.end method

.method private calculateRelativeToImage(Landroid/graphics/Rect;Landroid/graphics/Rect;Z)Landroid/graphics/Rect;
    .locals 4

    .prologue
    const/16 v3, 0xb4

    .line 112
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;->getOrientation()I

    move-result v0

    if-lt v0, v3, :cond_0

    .line 114
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->shapeModifier:Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;->getShapeModifier(Landroid/graphics/Rect;)Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;

    move-result-object v0

    invoke-virtual {p2}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    invoke-virtual {v0, v3, v1, v2}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->rotate(IFF)Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->getAsRect()Landroid/graphics/Rect;

    move-result-object p1

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;->getPreviewSize()Landroid/graphics/Point;

    move-result-object v0

    .line 120
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->screenManager:Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;

    invoke-virtual {v1}, Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;->isInPortraitMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 121
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->shapeModifier:Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;

    invoke-virtual {v1, p1, p2, v0}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;->scaleToNewResolutionThenSwapDimensions(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Point;)Landroid/graphics/Rect;

    move-result-object v0

    .line 125
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->shapeModifier:Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;

    invoke-virtual {v1, p1, p2, v0}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;->scaleToNewResolution(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Point;)Landroid/graphics/Rect;

    move-result-object v0

    goto :goto_0
.end method

.method private getBlurRegionCalculator(Z)Lcom/google/android/libraries/commerce/ocr/util/Function;
    .locals 1

    .prologue
    .line 139
    if-eqz p1, :cond_0

    .line 140
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/credit/ui/a;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/a;-><init>(Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;)V

    .line 155
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/credit/ui/b;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/b;-><init>(Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;)V

    goto :goto_0
.end method


# virtual methods
.method public final getBlurDetectorROI()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->blurDetectorRoiRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final getBoundingBoxRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->boundingBoxRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final getBoundingBoxRectRelativeToCameraPreview()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->boundingBoxRectRelativeToImage:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final getMidBoundingBoxRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->midBoundingBoxRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final onImageAreaChange(Landroid/graphics/Rect;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->blurDetectorRoiRect:Landroid/graphics/Rect;

    .line 86
    invoke-direct {p0, p1}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->calculateOuterBoundingBoxArea(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->boundingBoxRect:Landroid/graphics/Rect;

    .line 87
    const-string v0, "CardRegionOfInterestProvider"

    const-string v1, "Visible Area: %s, Bounding box: %s"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p1, v2, v4

    iget-object v3, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->boundingBoxRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->boundingBoxRect:Landroid/graphics/Rect;

    invoke-direct {p0, v0, p1, v4}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->calculateRelativeToImage(Landroid/graphics/Rect;Landroid/graphics/Rect;Z)Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->boundingBoxRectRelativeToImage:Landroid/graphics/Rect;

    .line 93
    const-string v0, "CardRegionOfInterestProvider"

    const-string v1, "Bounding box relative to image: %s"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->boundingBoxRectRelativeToImage:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->shapeModifier:Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->boundingBoxRect:Landroid/graphics/Rect;

    iget v2, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->edgeFinderWidthToOuterBoundingBoxEdgeLengthRatio:F

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;->resizeBoundingBox(Landroid/graphics/Rect;F)Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->midBoundingBoxRect:Landroid/graphics/Rect;

    .line 100
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->calculationStrategy:Lcom/google/android/libraries/commerce/ocr/util/Function;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->midBoundingBoxRect:Landroid/graphics/Rect;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/commerce/ocr/util/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 101
    invoke-direct {p0, v0, p1, v5}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->calculateRelativeToImage(Landroid/graphics/Rect;Landroid/graphics/Rect;Z)Landroid/graphics/Rect;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->blurDetectorRoiRect:Landroid/graphics/Rect;

    .line 103
    const-string v1, "CardRegionOfInterestProvider"

    const-string v2, "Blur detection on screen bounding box: %s, blur ROI box: %s, preview size: %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->blurDetectorRoiRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;->getPreviewSize()Landroid/graphics/Point;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    return-void
.end method
