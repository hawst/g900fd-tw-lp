.class public abstract Lcom/google/android/libraries/social/rpc/a/e;
.super Lcom/google/android/libraries/social/rpc/h;
.source "SourceFile"


# instance fields
.field protected final f:Lcom/google/protobuf/nano/j;

.field public final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private i:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/social/rpc/q;Ljava/lang/String;Ljava/lang/String;Lcom/google/protobuf/nano/j;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 52
    iget-object v1, p2, Lcom/google/android/libraries/social/rpc/q;->c:Lcom/google/android/libraries/social/rpc/a;

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/libraries/social/rpc/a/h;

    iget-object v2, p2, Lcom/google/android/libraries/social/rpc/q;->a:Ljava/lang/String;

    invoke-direct {v0, p1, v2, p7, v1}, Lcom/google/android/libraries/social/rpc/a/h;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/social/rpc/a;)V

    :goto_0
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/libraries/social/rpc/h;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/rpc/q;Ljava/lang/String;Lcom/google/android/libraries/social/rpc/l;)V

    .line 57
    iput-object p4, p0, Lcom/google/android/libraries/social/rpc/a/e;->g:Ljava/lang/String;

    .line 58
    iput-object p5, p0, Lcom/google/android/libraries/social/rpc/a/e;->f:Lcom/google/protobuf/nano/j;

    .line 59
    iput-object p6, p0, Lcom/google/android/libraries/social/rpc/a/e;->h:Ljava/lang/String;

    .line 60
    return-void

    .line 52
    :cond_0
    new-instance v0, Lcom/google/android/libraries/social/rpc/a/d;

    iget-object v1, p2, Lcom/google/android/libraries/social/rpc/q;->a:Ljava/lang/String;

    invoke-direct {v0, p1, v1, p7}, Lcom/google/android/libraries/social/rpc/a/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a([B)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 116
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/rpc/a/e;->b([B)Lcom/google/protobuf/nano/j;

    .line 118
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/rpc/g;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/rpc/h;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Lcom/google/android/libraries/social/rpc/g;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/a/e;->f:Lcom/google/protobuf/nano/j;

    invoke-static {v0}, Lcom/google/protobuf/nano/k;->a(Lcom/google/protobuf/nano/j;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/rpc/a/e;->c(Ljava/lang/String;)V

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/a/e;->f:Lcom/google/protobuf/nano/j;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/rpc/a/e;->b(Lcom/google/protobuf/nano/j;)V

    .line 123
    return-void

    .line 118
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public a([BLjava/lang/String;)V
    .locals 4

    .prologue
    .line 127
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/social/rpc/h;->a([BLjava/lang/String;)V

    .line 128
    const-string v0, "ApiaryProtoOperation"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    const-string v0, "ApiaryProtoOperation"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HttpOperation error: Response follows: \n"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-direct {v2, p1, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    :cond_0
    return-void
.end method

.method public b([B)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/a/e;->f:Lcom/google/protobuf/nano/j;

    if-eqz v0, :cond_0

    .line 138
    const/4 v0, 0x0

    array-length v1, p1

    invoke-static {p1, v0, v1}, Lcom/google/protobuf/nano/a;->a([BII)Lcom/google/protobuf/nano/a;

    move-result-object v0

    .line 139
    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/a/e;->f:Lcom/google/protobuf/nano/j;

    invoke-virtual {v1, v0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;

    .line 140
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/rpc/a/e;->i:Z

    .line 141
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/a/e;->f:Lcom/google/protobuf/nano/j;

    .line 143
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Lcom/google/protobuf/nano/j;)V
    .locals 0

    .prologue
    .line 105
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/a/e;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    const-string v0, "application/x-protobuf"

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 5

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/a/e;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/a/e;->h:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/rpc/a/e;->h()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/social/rpc/a/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/a/e;->g:Ljava/lang/String;

    return-object v0
.end method
