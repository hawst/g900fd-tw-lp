.class final Lcom/google/android/libraries/social/autobackup/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:I

.field final c:Lcom/google/android/libraries/social/mediaupload/c;

.field final d:Lcom/google/android/libraries/social/autobackup/f;

.field e:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;


# direct methods
.method constructor <init>(Landroid/content/Context;ILcom/google/android/libraries/social/autobackup/f;)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/d;->a:Landroid/content/Context;

    .line 49
    iput p2, p0, Lcom/google/android/libraries/social/autobackup/d;->b:I

    .line 50
    iput-object p3, p0, Lcom/google/android/libraries/social/autobackup/d;->d:Lcom/google/android/libraries/social/autobackup/f;

    .line 51
    new-instance v0, Lcom/google/android/libraries/social/autobackup/e;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/social/autobackup/e;-><init>(Lcom/google/android/libraries/social/autobackup/d;)V

    .line 61
    new-instance v1, Lcom/google/android/libraries/social/mediaupload/c;

    invoke-direct {v1, p1, p2, v0}, Lcom/google/android/libraries/social/mediaupload/c;-><init>(Landroid/content/Context;ILcom/google/android/libraries/social/mediaupload/p;)V

    iput-object v1, p0, Lcom/google/android/libraries/social/autobackup/d;->c:Lcom/google/android/libraries/social/mediaupload/c;

    .line 62
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/d;->a:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/experiments/c;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/experiments/c;

    sget-object v1, Lcom/google/android/libraries/social/autobackup/c;->c:Lcom/google/android/libraries/social/experiments/a;

    invoke-interface {v0, v1, p2}, Lcom/google/android/libraries/social/experiments/c;->a(Lcom/google/android/libraries/social/experiments/a;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/d;->c:Lcom/google/android/libraries/social/mediaupload/c;

    const-string v1, "uploadmediabackground"

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/mediaupload/c;->a(Ljava/lang/String;)V

    .line 63
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 94
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/d;->c:Lcom/google/android/libraries/social/mediaupload/c;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/mediaupload/c;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    monitor-exit p0

    return-void

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
