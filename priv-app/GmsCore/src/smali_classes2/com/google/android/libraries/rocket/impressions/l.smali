.class public final enum Lcom/google/android/libraries/rocket/impressions/l;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/libraries/rocket/impressions/l;

.field public static final enum b:Lcom/google/android/libraries/rocket/impressions/l;

.field public static final enum c:Lcom/google/android/libraries/rocket/impressions/l;

.field public static final enum d:Lcom/google/android/libraries/rocket/impressions/l;

.field private static final synthetic e:[Lcom/google/android/libraries/rocket/impressions/l;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 50
    new-instance v0, Lcom/google/android/libraries/rocket/impressions/l;

    const-string v1, "NOT_STARTED"

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/rocket/impressions/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/rocket/impressions/l;->a:Lcom/google/android/libraries/rocket/impressions/l;

    .line 51
    new-instance v0, Lcom/google/android/libraries/rocket/impressions/l;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/libraries/rocket/impressions/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/rocket/impressions/l;->b:Lcom/google/android/libraries/rocket/impressions/l;

    .line 52
    new-instance v0, Lcom/google/android/libraries/rocket/impressions/l;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v4}, Lcom/google/android/libraries/rocket/impressions/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/rocket/impressions/l;->c:Lcom/google/android/libraries/rocket/impressions/l;

    .line 53
    new-instance v0, Lcom/google/android/libraries/rocket/impressions/l;

    const-string v1, "FINISHED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/libraries/rocket/impressions/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/rocket/impressions/l;->d:Lcom/google/android/libraries/rocket/impressions/l;

    .line 49
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/libraries/rocket/impressions/l;

    sget-object v1, Lcom/google/android/libraries/rocket/impressions/l;->a:Lcom/google/android/libraries/rocket/impressions/l;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/libraries/rocket/impressions/l;->b:Lcom/google/android/libraries/rocket/impressions/l;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/libraries/rocket/impressions/l;->c:Lcom/google/android/libraries/rocket/impressions/l;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/libraries/rocket/impressions/l;->d:Lcom/google/android/libraries/rocket/impressions/l;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/libraries/rocket/impressions/l;->e:[Lcom/google/android/libraries/rocket/impressions/l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/libraries/rocket/impressions/l;
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/google/android/libraries/rocket/impressions/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/rocket/impressions/l;

    return-object v0
.end method

.method public static values()[Lcom/google/android/libraries/rocket/impressions/l;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/google/android/libraries/rocket/impressions/l;->e:[Lcom/google/android/libraries/rocket/impressions/l;

    invoke-virtual {v0}, [Lcom/google/android/libraries/rocket/impressions/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/libraries/rocket/impressions/l;

    return-object v0
.end method
