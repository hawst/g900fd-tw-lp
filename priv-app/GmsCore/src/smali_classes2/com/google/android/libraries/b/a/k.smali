.class public final Lcom/google/android/libraries/b/a/k;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/lang/Thread;

.field private static b:Landroid/os/Handler;


# direct methods
.method public static a()Z
    .locals 2

    .prologue
    .line 25
    sget-object v0, Lcom/google/android/libraries/b/a/k;->a:Ljava/lang/Thread;

    if-nez v0, :cond_0

    .line 26
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/b/a/k;->a:Ljava/lang/Thread;

    .line 28
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/b/a/k;->a:Ljava/lang/Thread;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()Landroid/os/Handler;
    .locals 2

    .prologue
    .line 60
    sget-object v0, Lcom/google/android/libraries/b/a/k;->b:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/libraries/b/a/k;->b:Landroid/os/Handler;

    .line 64
    :cond_0
    sget-object v0, Lcom/google/android/libraries/b/a/k;->b:Landroid/os/Handler;

    return-object v0
.end method
