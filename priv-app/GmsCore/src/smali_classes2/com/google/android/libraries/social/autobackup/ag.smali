.class final Lcom/google/android/libraries/social/autobackup/ag;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final f:Lcom/google/android/libraries/social/autobackup/ah;

.field private static final g:[Ljava/lang/String;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/net/Uri;

.field private final d:[Ljava/lang/String;

.field private final e:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 822
    new-instance v1, Lcom/google/android/libraries/social/autobackup/ah;

    const-wide/16 v2, -0x1

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/libraries/social/autobackup/ah;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V

    sput-object v1, Lcom/google/android/libraries/social/autobackup/ag;->f:Lcom/google/android/libraries/social/autobackup/ah;

    .line 829
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "MAX(_id)"

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/libraries/social/autobackup/ag;->g:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 841
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 818
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/ag;->d:[Ljava/lang/String;

    .line 842
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/ag;->a:Ljava/lang/String;

    .line 843
    iput-object p2, p0, Lcom/google/android/libraries/social/autobackup/ag;->b:Ljava/lang/String;

    .line 844
    const-string v0, "photo"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 845
    const-string v0, "external"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 846
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/ag;->c:Landroid/net/Uri;

    .line 853
    :goto_0
    invoke-static {}, Lcom/google/android/libraries/social/autobackup/ad;->c()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/ag;->e:[Ljava/lang/String;

    .line 863
    :goto_1
    return-void

    .line 847
    :cond_0
    const-string v0, "phoneStorage"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 848
    sget-object v0, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->a:Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/ag;->c:Landroid/net/Uri;

    goto :goto_0

    .line 850
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid volume name; must be one of the defined volumes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 854
    :cond_2
    const-string v0, "video"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 855
    const-string v0, "external"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 856
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/ag;->c:Landroid/net/Uri;

    .line 863
    :goto_2
    invoke-static {}, Lcom/google/android/libraries/social/autobackup/ad;->d()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/ag;->e:[Ljava/lang/String;

    goto :goto_1

    .line 857
    :cond_3
    const-string v0, "phoneStorage"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 858
    sget-object v0, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->b:Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/ag;->c:Landroid/net/Uri;

    goto :goto_2

    .line 860
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid volume name; must be one of the defined volumes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 865
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid media type; must be one of the defined types"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Landroid/database/Cursor;)Lcom/google/android/libraries/social/autobackup/ah;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 917
    if-eqz p0, :cond_2

    :try_start_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 918
    const/4 v0, 0x2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/DCIM/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 919
    if-eqz v0, :cond_0

    move-object v4, v5

    .line 921
    :goto_0
    if-eqz v0, :cond_1

    .line 923
    :goto_1
    const/4 v0, 0x4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x3

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->a(Ljava/lang/String;IJ)Ljava/lang/String;

    move-result-object v6

    .line 927
    new-instance v1, Lcom/google/android/libraries/social/autobackup/ah;

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v7, 0x0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/libraries/social/autobackup/ah;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 930
    invoke-static {p0}, Lcom/google/android/libraries/social/g/a/f;->a(Landroid/database/Cursor;)V

    .line 932
    :goto_2
    return-object v1

    .line 919
    :cond_0
    const/4 v1, 0x1

    :try_start_1
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 921
    :cond_1
    const/4 v0, 0x6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    goto :goto_1

    .line 930
    :cond_2
    invoke-static {p0}, Lcom/google/android/libraries/social/g/a/f;->a(Landroid/database/Cursor;)V

    .line 932
    sget-object v1, Lcom/google/android/libraries/social/autobackup/ag;->f:Lcom/google/android/libraries/social/autobackup/ah;

    goto :goto_2

    .line 930
    :catchall_0
    move-exception v0

    invoke-static {p0}, Lcom/google/android/libraries/social/g/a/f;->a(Landroid/database/Cursor;)V

    throw v0
.end method

.method static synthetic a(Lcom/google/android/libraries/social/autobackup/ag;Landroid/content/ContentResolver;)Lcom/google/android/libraries/social/autobackup/ah;
    .locals 6

    .prologue
    .line 810
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ag;->d:[Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "-1"

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/ag;->c:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/ag;->e:[Ljava/lang/String;

    const-string v3, "_id > ? AND _data IS NOT NULL"

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/ag;->d:[Ljava/lang/String;

    const-string v5, "_id DESC LIMIT 1"

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/ag;->a(Landroid/database/Cursor;)Lcom/google/android/libraries/social/autobackup/ah;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/libraries/social/autobackup/ag;Landroid/content/ContentResolver;J)Lcom/google/android/libraries/social/autobackup/ah;
    .locals 6

    .prologue
    .line 810
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ag;->d:[Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/ag;->c:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/ag;->e:[Ljava/lang/String;

    const-string v3, "_id > ? AND _data IS NOT NULL"

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/ag;->d:[Ljava/lang/String;

    const-string v5, "_id LIMIT 1"

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/ag;->a(Landroid/database/Cursor;)Lcom/google/android/libraries/social/autobackup/ah;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/libraries/social/autobackup/au;J)V
    .locals 8

    .prologue
    .line 939
    invoke-virtual {p1}, Lcom/google/android/libraries/social/autobackup/au;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 940
    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 941
    const-string v2, "volume_name"

    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/ag;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 942
    const-string v2, "media_type"

    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/ag;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 943
    const-string v2, "last_media_id"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 946
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/ag;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/ag;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 947
    const-string v3, "SELECT count(*) FROM media_tracker WHERE media_type = ? AND volume_name = ?"

    invoke-static {v0, v3, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 948
    const-string v2, "media_tracker"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 952
    :goto_0
    return-void

    .line 950
    :cond_0
    const-string v3, "media_tracker"

    const-string v4, "media_type = ? AND volume_name = ?"

    invoke-virtual {v0, v3, v1, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(Landroid/content/ContentResolver;)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 875
    .line 877
    :try_start_0
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/ag;->c:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/libraries/social/autobackup/ag;->g:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 879
    if-eqz v1, :cond_2

    .line 880
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    .line 887
    :goto_0
    invoke-static {v1}, Lcom/google/android/libraries/social/g/a/f;->a(Landroid/database/Cursor;)V

    .line 890
    :goto_1
    return v0

    :cond_0
    move v0, v6

    .line 880
    goto :goto_0

    :cond_1
    move v0, v6

    goto :goto_0

    .line 887
    :cond_2
    invoke-static {v1}, Lcom/google/android/libraries/social/g/a/f;->a(Landroid/database/Cursor;)V

    :goto_2
    move v0, v6

    .line 890
    goto :goto_1

    .line 882
    :catch_0
    move-exception v0

    move-object v1, v7

    .line 883
    :goto_3
    :try_start_2
    const-string v2, "iu.UploadsManager"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 884
    const-string v2, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "exception loading config: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/ag;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 887
    :cond_3
    invoke-static {v1}, Lcom/google/android/libraries/social/g/a/f;->a(Landroid/database/Cursor;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    move-object v1, v7

    :goto_4
    invoke-static {v1}, Lcom/google/android/libraries/social/g/a/f;->a(Landroid/database/Cursor;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_4

    .line 882
    :catch_1
    move-exception v0

    goto :goto_3
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 955
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ag;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 960
    instance-of v1, p1, Lcom/google/android/libraries/social/autobackup/ag;

    if-nez v1, :cond_1

    .line 964
    :cond_0
    :goto_0
    return v0

    .line 963
    :cond_1
    check-cast p1, Lcom/google/android/libraries/social/autobackup/ag;

    .line 964
    iget-object v1, p1, Lcom/google/android/libraries/social/autobackup/ag;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/ag;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/android/libraries/social/autobackup/ag;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/ag;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 970
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ag;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 972
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/ag;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 973
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 978
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/ag;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/ag;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
