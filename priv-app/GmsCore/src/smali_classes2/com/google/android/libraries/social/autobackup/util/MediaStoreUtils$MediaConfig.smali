.class public Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils$MediaConfig;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public a:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 522
    new-instance v0, Lcom/google/android/libraries/social/autobackup/util/d;

    invoke-direct {v0}, Lcom/google/android/libraries/social/autobackup/util/d;-><init>()V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils$MediaConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 536
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils$MediaConfig;->a:Landroid/net/Uri;

    .line 537
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 545
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils$MediaConfig;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 551
    return-void
.end method
