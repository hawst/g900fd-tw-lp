.class public Lcom/google/android/libraries/social/experiments/impl/ExperimentServiceModule;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/a/f;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/libraries/social/a/a;)V
    .locals 3

    .prologue
    .line 21
    const-class v0, Lcom/google/android/libraries/social/experiments/c;

    if-ne p2, v0, :cond_1

    .line 22
    const-class v0, Lcom/google/android/libraries/social/experiments/impl/c;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/experiments/impl/c;

    .line 23
    new-instance v1, Lcom/google/android/libraries/social/experiments/impl/g;

    invoke-direct {v1}, Lcom/google/android/libraries/social/experiments/impl/g;-><init>()V

    .line 25
    new-instance v2, Lcom/google/android/libraries/social/experiments/impl/e;

    invoke-direct {v2, p1, v0, v1}, Lcom/google/android/libraries/social/experiments/impl/e;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/experiments/impl/c;Lcom/google/android/libraries/social/experiments/impl/f;)V

    .line 27
    const-class v0, Lcom/google/android/libraries/social/experiments/c;

    invoke-virtual {p3, v0, v2}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    .line 33
    :cond_0
    :goto_0
    return-void

    .line 28
    :cond_1
    const-class v0, Lcom/google/android/libraries/social/k/a/a;

    if-ne p2, v0, :cond_2

    .line 29
    const-class v0, Lcom/google/android/libraries/social/k/a/a;

    new-instance v1, Lcom/google/android/libraries/social/experiments/impl/b;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/social/experiments/impl/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 30
    :cond_2
    const-class v0, Lcom/google/android/libraries/social/experiments/impl/c;

    if-ne p2, v0, :cond_0

    .line 31
    const-class v0, Lcom/google/android/libraries/social/experiments/impl/c;

    new-instance v1, Lcom/google/android/libraries/social/experiments/impl/d;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/social/experiments/impl/d;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto :goto_0
.end method
