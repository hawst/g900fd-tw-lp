.class public Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCenteredRect(Landroid/graphics/Rect;FF)Landroid/graphics/RectF;
    .locals 6

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 42
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, p2

    div-float/2addr v0, v2

    .line 43
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v1, p3

    div-float/2addr v1, v2

    .line 44
    new-instance v2, Landroid/graphics/RectF;

    iget v3, p1, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    add-float/2addr v3, v0

    iget v4, p1, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    add-float/2addr v4, v1

    iget v5, p1, Landroid/graphics/Rect;->right:I

    int-to-float v5, v5

    sub-float v0, v5, v0

    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    sub-float v1, v5, v1

    invoke-direct {v2, v3, v4, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v2
.end method

.method public getShapeModifier(Landroid/graphics/Rect;)Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;
    .locals 2

    .prologue
    .line 59
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;-><init>(Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;Landroid/graphics/Rect;Lcom/google/android/libraries/commerce/ocr/util/b;)V

    return-object v0
.end method

.method public resizeBoundingBox(Landroid/graphics/Rect;F)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1, p2}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;->resizeBoundingBoxF(Landroid/graphics/Rect;F)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;->toRect(Landroid/graphics/RectF;)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public resizeBoundingBoxF(Landroid/graphics/Rect;F)Landroid/graphics/RectF;
    .locals 3

    .prologue
    .line 31
    if-nez p1, :cond_0

    .line 32
    const/4 v0, 0x0

    .line 38
    :goto_0
    return-object v0

    .line 35
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p2

    .line 36
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    .line 37
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    .line 38
    invoke-virtual {p0, p1, v1, v0}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;->getCenteredRect(Landroid/graphics/Rect;FF)Landroid/graphics/RectF;

    move-result-object v0

    goto :goto_0
.end method

.method public scaleToNewResolution(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Point;)Landroid/graphics/Rect;
    .locals 4

    .prologue
    .line 91
    invoke-virtual {p2, p1}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v0

    const-string v1, "Container[%s] must hold target[%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 93
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;-><init>(Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;Landroid/graphics/Rect;Lcom/google/android/libraries/commerce/ocr/util/b;)V

    new-instance v1, Landroid/graphics/Point;

    iget v2, p2, Landroid/graphics/Rect;->right:I

    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v0, v1, p3}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->scale(Landroid/graphics/Point;Landroid/graphics/Point;)Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->getAsRect()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public scaleToNewResolutionThenSwapDimensions(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Point;)Landroid/graphics/Rect;
    .locals 4

    .prologue
    .line 79
    invoke-virtual {p2, p1}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v0

    const-string v1, "Container[%s] must hold target[%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 82
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;-><init>(Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;Landroid/graphics/Rect;Lcom/google/android/libraries/commerce/ocr/util/b;)V

    new-instance v1, Landroid/graphics/Point;

    iget v2, p2, Landroid/graphics/Rect;->right:I

    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v0, v1, p3}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->scale(Landroid/graphics/Point;Landroid/graphics/Point;)Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->swapDimensions()Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->getAsRect()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public toRect(Landroid/graphics/Point;)Landroid/graphics/Rect;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 51
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    iget v2, p1, Landroid/graphics/Point;->x:I

    iget v3, p1, Landroid/graphics/Point;->y:I

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public toRect(Landroid/graphics/RectF;)Landroid/graphics/Rect;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 55
    if-nez p1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;-><init>(Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;Landroid/graphics/RectF;Lcom/google/android/libraries/commerce/ocr/util/b;)V

    invoke-virtual {v1}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->getAsRect()Landroid/graphics/Rect;

    move-result-object v0

    goto :goto_0
.end method
