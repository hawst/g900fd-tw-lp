.class final Lcom/google/android/libraries/social/mediaupload/a;
.super Lcom/google/android/libraries/social/mediaupload/r;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;

.field private final j:Landroid/content/Context;

.field private final k:Ljava/lang/String;

.field private final l:Lcom/google/android/libraries/social/mediaupload/q;

.field private final m:Ljava/lang/String;

.field private final n:Ljava/lang/String;

.field private final o:Z

.field private final p:I

.field private final q:Ljava/lang/String;

.field private final r:Ljava/lang/String;

.field private s:Le/a/a/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/social/rpc/l;Ljava/lang/String;Lcom/google/android/libraries/social/mediaupload/q;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p2}, Lcom/google/android/libraries/social/mediaupload/r;-><init>(Lcom/google/android/libraries/social/rpc/l;)V

    .line 53
    iput-object p1, p0, Lcom/google/android/libraries/social/mediaupload/a;->j:Landroid/content/Context;

    .line 54
    iput-object p3, p0, Lcom/google/android/libraries/social/mediaupload/a;->k:Ljava/lang/String;

    .line 55
    iput-object p4, p0, Lcom/google/android/libraries/social/mediaupload/a;->l:Lcom/google/android/libraries/social/mediaupload/q;

    .line 56
    iput-object p5, p0, Lcom/google/android/libraries/social/mediaupload/a;->m:Ljava/lang/String;

    .line 57
    iput-object p6, p0, Lcom/google/android/libraries/social/mediaupload/a;->n:Ljava/lang/String;

    .line 58
    iput-boolean p7, p0, Lcom/google/android/libraries/social/mediaupload/a;->o:Z

    .line 59
    iput p8, p0, Lcom/google/android/libraries/social/mediaupload/a;->p:I

    .line 60
    iput-object p9, p0, Lcom/google/android/libraries/social/mediaupload/a;->q:Ljava/lang/String;

    .line 61
    iput-object p10, p0, Lcom/google/android/libraries/social/mediaupload/a;->r:Ljava/lang/String;

    .line 62
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    .line 66
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/google/android/libraries/social/mediaupload/r;->c:Lcom/google/android/libraries/social/rpc/l;

    iget-object v2, p0, Lcom/google/android/libraries/social/mediaupload/a;->k:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/libraries/social/rpc/l;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 68
    const-string v1, "X-Upload-Content-Type"

    iget-object v2, p0, Lcom/google/android/libraries/social/mediaupload/a;->l:Lcom/google/android/libraries/social/mediaupload/q;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/mediaupload/q;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    iget-object v1, p0, Lcom/google/android/libraries/social/mediaupload/a;->l:Lcom/google/android/libraries/social/mediaupload/q;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/mediaupload/q;->k()J

    move-result-wide v2

    .line 70
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 71
    const-string v1, "X-Upload-Content-Length"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    :cond_0
    const-string v1, "X-Goog-Hash"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sha1="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/libraries/social/mediaupload/a;->l:Lcom/google/android/libraries/social/mediaupload/q;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/mediaupload/q;->i()Lcom/google/android/libraries/social/m/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/libraries/social/m/a;->c()[B

    move-result-object v3

    invoke-static {v3, v7}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    iget-object v1, p0, Lcom/google/android/libraries/social/mediaupload/a;->j:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/libraries/social/mediaupload/a;->k:Ljava/lang/String;

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/libraries/social/mediaupload/a;->i:Le/a/a/l;

    invoke-static {v1, v2, v3, v0, v4}, Lcom/google/android/libraries/social/n/a;->a(Landroid/content/Context;Ljava/lang/String;ILjava/util/Map;Le/a/a/l;)Le/a/a/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/mediaupload/a;->s:Le/a/a/g;

    .line 79
    new-instance v6, Lcom/google/c/e/b/a/a/q;

    invoke-direct {v6}, Lcom/google/c/e/b/a/a/q;-><init>()V

    .line 80
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/a;->j:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/mediaupload/b;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/a;->j:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/libraries/social/mediaupload/a;->r:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/libraries/social/mediaupload/a;->m:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/libraries/social/mediaupload/a;->n:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/libraries/social/mediaupload/a;->l:Lcom/google/android/libraries/social/mediaupload/q;

    iget v5, p0, Lcom/google/android/libraries/social/mediaupload/a;->p:I

    invoke-static/range {v0 .. v5}, Lcom/google/android/libraries/social/mediaupload/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/social/mediaupload/q;I)Lcom/google/c/e/b/d/a/e;

    move-result-object v0

    iput-object v0, v6, Lcom/google/c/e/b/a/a/q;->a:Lcom/google/c/e/b/d/a/e;

    .line 83
    iget-object v1, p0, Lcom/google/android/libraries/social/mediaupload/a;->j:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/libraries/social/mediaupload/a;->q:Ljava/lang/String;

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/google/android/libraries/social/mediaupload/a;->o:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x32

    :goto_0
    invoke-static {v1, v6, v2, v3, v0}, Lcom/google/android/libraries/social/rpc/b/e;->a(Landroid/content/Context;Lcom/google/protobuf/nano/j;Ljava/lang/String;ZI)V

    .line 86
    invoke-static {v6}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    .line 87
    iget-object v1, p0, Lcom/google/android/libraries/social/mediaupload/a;->s:Le/a/a/g;

    const-string v2, "application/x-protobuf"

    invoke-interface {v1, v2, v0}, Le/a/a/g;->a(Ljava/lang/String;[B)V

    .line 89
    const-string v0, "MediaUploader"

    invoke-static {v0, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    const-string v0, "MediaUploader"

    const-string v1, "UploadMediaRequest [initial]"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    const-string v0, "MediaUploader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "requestUrl: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/libraries/social/mediaupload/a;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    const-string v0, "MediaUploader"

    invoke-virtual {v6}, Lcom/google/c/e/b/a/a/q;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v0, v1}, Lcom/google/android/libraries/b/a/d;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 94
    :cond_1
    return-void

    .line 83
    :cond_2
    const/16 v0, 0x64

    goto :goto_0
.end method

.method protected final a(Le/a/a/g;)V
    .locals 1

    .prologue
    .line 103
    const-string v0, "Location"

    invoke-interface {p1, v0}, Le/a/a/g;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/mediaupload/a;->a:Ljava/lang/String;

    .line 104
    return-void
.end method

.method protected final b()Le/a/a/g;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/a;->s:Le/a/a/g;

    return-object v0
.end method
