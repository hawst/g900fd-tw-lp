.class public final Lcom/google/android/libraries/social/rpc/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/rpc/g;


# instance fields
.field private final a:Lcom/google/android/libraries/social/o/a;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/o/a;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/libraries/social/rpc/n;->a:Lcom/google/android/libraries/social/o/a;

    .line 20
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/libraries/social/rpc/i;)V
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/n;->a:Lcom/google/android/libraries/social/o/a;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p1, Lcom/google/android/libraries/social/rpc/i;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/rpc/j;

    iget-object v0, v0, Lcom/google/android/libraries/social/rpc/j;->h:[Ljava/lang/String;

    invoke-static {v1, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    iget-wide v0, p1, Lcom/google/android/libraries/social/rpc/i;->c:J

    iget-wide v0, p1, Lcom/google/android/libraries/social/rpc/i;->d:J

    iget-wide v0, p1, Lcom/google/android/libraries/social/rpc/i;->f:J

    invoke-virtual {p1}, Lcom/google/android/libraries/social/rpc/i;->b()J

    invoke-virtual {p1}, Lcom/google/android/libraries/social/rpc/i;->a()J

    .line 49
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x1

    return v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 35
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 40
    return-void
.end method
