.class public Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final cardNum:Ljava/lang/String;

.field private final expMonth:I

.field private final expYear:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/pub/a;

    invoke-direct {v0}, Lcom/google/android/libraries/commerce/ocr/pub/a;-><init>()V

    sput-object v0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;->cardNum:Ljava/lang/String;

    .line 23
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;->expYear:I

    .line 24
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;->expMonth:I

    .line 25
    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->getCardNumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;->cardNum:Ljava/lang/String;

    .line 29
    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->getExpirationYear()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;->expYear:I

    .line 30
    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->getExpirationMonth()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;->expMonth:I

    .line 31
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method

.method public getCardNum()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;->cardNum:Ljava/lang/String;

    return-object v0
.end method

.method public getExpMonth()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;->expMonth:I

    return v0
.end method

.method public getExpYear()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;->expYear:I

    return v0
.end method

.method public hasExpDate()Z
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;->expYear:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;->expMonth:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 75
    const-class v0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "card_num"

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;->cardNum:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "exp_year"

    iget v2, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;->expYear:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "exp_month"

    iget v2, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;->expMonth:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/bv;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;->cardNum:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 41
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;->expYear:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 42
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;->expMonth:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 43
    return-void
.end method
