.class final Lcom/google/android/libraries/social/account/impl/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/account/j;


# instance fields
.field final synthetic a:Lcom/google/android/libraries/social/account/impl/a;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/social/account/impl/a;)V
    .locals 0

    .prologue
    .line 934
    iput-object p1, p0, Lcom/google/android/libraries/social/account/impl/d;->a:Lcom/google/android/libraries/social/account/impl/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 938
    const-string v0, "upgrade:account_status"

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/libraries/social/account/f;Lcom/google/android/libraries/social/account/c;)V
    .locals 2

    .prologue
    .line 944
    const-string v0, "non_google_plus"

    invoke-interface {p3, v0}, Lcom/google/android/libraries/social/account/c;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 945
    const-string v0, "non_google_plus"

    invoke-interface {p2, v0}, Lcom/google/android/libraries/social/account/f;->g(Ljava/lang/String;)Lcom/google/android/libraries/social/account/f;

    .line 946
    const-string v0, "account_status"

    const/4 v1, 0x2

    invoke-interface {p2, v0, v1}, Lcom/google/android/libraries/social/account/f;->b(Ljava/lang/String;I)Lcom/google/android/libraries/social/account/f;

    .line 956
    :goto_0
    return-void

    .line 947
    :cond_0
    const-string v0, "notifications_only"

    invoke-interface {p3, v0}, Lcom/google/android/libraries/social/account/c;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 948
    const-string v0, "notifications_only"

    invoke-interface {p2, v0}, Lcom/google/android/libraries/social/account/f;->g(Ljava/lang/String;)Lcom/google/android/libraries/social/account/f;

    .line 949
    const-string v0, "account_status"

    const/4 v1, 0x3

    invoke-interface {p2, v0, v1}, Lcom/google/android/libraries/social/account/f;->b(Ljava/lang/String;I)Lcom/google/android/libraries/social/account/f;

    goto :goto_0

    .line 950
    :cond_1
    const-string v0, "logged_in"

    invoke-interface {p3, v0}, Lcom/google/android/libraries/social/account/c;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 951
    const-string v0, "logged_in"

    invoke-interface {p2, v0}, Lcom/google/android/libraries/social/account/f;->g(Ljava/lang/String;)Lcom/google/android/libraries/social/account/f;

    .line 952
    const-string v0, "account_status"

    const/4 v1, 0x4

    invoke-interface {p2, v0, v1}, Lcom/google/android/libraries/social/account/f;->b(Ljava/lang/String;I)Lcom/google/android/libraries/social/account/f;

    goto :goto_0

    .line 954
    :cond_2
    const-string v0, "account_status"

    const/4 v1, 0x5

    invoke-interface {p2, v0, v1}, Lcom/google/android/libraries/social/account/f;->b(Ljava/lang/String;I)Lcom/google/android/libraries/social/account/f;

    goto :goto_0
.end method
