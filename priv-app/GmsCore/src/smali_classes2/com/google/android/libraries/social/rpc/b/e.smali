.class public final Lcom/google/android/libraries/social/rpc/b/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/libraries/social/rpc/b/e;->a:Ljava/util/Map;

    return-void
.end method

.method private static a(Lcom/google/protobuf/nano/j;)Ljava/lang/reflect/Field;
    .locals 4

    .prologue
    .line 162
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 164
    sget-object v2, Lcom/google/android/libraries/social/rpc/b/e;->a:Ljava/util/Map;

    monitor-enter v2

    .line 165
    :try_start_0
    sget-object v0, Lcom/google/android/libraries/social/rpc/b/e;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Field;

    .line 166
    if-nez v0, :cond_0

    .line 169
    const-string v0, "apiHeader"

    invoke-virtual {v1, v0}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 170
    sget-object v3, Lcom/google/android/libraries/social/rpc/b/e;->a:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    return-object v0

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method static a(Landroid/content/Context;Lcom/google/protobuf/nano/j;)V
    .locals 4

    .prologue
    .line 88
    const-class v0, Lcom/google/android/libraries/social/consistencytoken/a;

    invoke-static {p0, v0}, Lcom/google/android/libraries/social/a/a;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/consistencytoken/a;

    .line 90
    if-nez v0, :cond_1

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 99
    :cond_1
    :try_start_0
    invoke-static {p1}, Lcom/google/android/libraries/social/rpc/b/e;->a(Lcom/google/protobuf/nano/j;)Ljava/lang/reflect/Field;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/c/e/b/a/a/n;

    .line 100
    if-eqz v1, :cond_2

    iget-object v1, v1, Lcom/google/c/e/b/a/a/n;->e:Lcom/google/c/d/a/a/b;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    .line 114
    :goto_1
    if-eqz v1, :cond_0

    .line 115
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/consistencytoken/a;->a(Lcom/google/c/d/a/a/b;)V

    goto :goto_0

    .line 100
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 101
    :catch_0
    move-exception v0

    .line 103
    const-string v1, "PlusiUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No API header found in the response:\n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 105
    :catch_1
    move-exception v0

    .line 107
    const-string v1, "PlusiUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No API header found in the response:\n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 109
    :catch_2
    move-exception v0

    .line 111
    const-string v1, "PlusiUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No API header found in the response:\n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/protobuf/nano/j;Ljava/lang/String;ZI)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x2

    .line 68
    new-instance v2, Lf/a/a/b;

    invoke-direct {v2}, Lf/a/a/b;-><init>()V

    new-instance v0, Lf/a/a/f;

    invoke-direct {v0}, Lf/a/a/f;-><init>()V

    invoke-static {p0}, Lcom/google/android/libraries/social/u/a;->b(Landroid/content/Context;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lf/a/a/f;->a:Ljava/lang/Integer;

    invoke-static {p0}, Lcom/google/android/libraries/social/u/a;->c(Landroid/content/Context;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lf/a/a/f;->b:Ljava/lang/Integer;

    invoke-static {p0}, Lcom/google/android/libraries/social/u/a;->d(Landroid/content/Context;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lf/a/a/f;->c:Ljava/lang/Integer;

    invoke-static {p0}, Lcom/google/android/libraries/social/u/a;->a(Landroid/content/Context;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lf/a/a/b;->j:Ljava/lang/Integer;

    iput-object v0, v2, Lf/a/a/b;->p:Lf/a/a/f;

    iput-object p2, v2, Lf/a/a/b;->i:Ljava/lang/String;

    const-class v0, Lcom/google/android/libraries/social/rpc/p;

    invoke-static {p0, v0}, Lcom/google/android/libraries/social/a/a;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/rpc/p;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/google/android/libraries/social/rpc/p;->c()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iput-object v0, v2, Lf/a/a/b;->m:Ljava/lang/String;

    :cond_0
    new-instance v0, Lcom/google/ac/a/a/b;

    invoke-direct {v0}, Lcom/google/ac/a/a/b;-><init>()V

    if-eqz p3, :cond_3

    const/16 v3, 0x64

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/ac/a/a/b;->b:Ljava/lang/Integer;

    :goto_1
    invoke-static {p0}, Lcom/google/android/libraries/b/a/c;->a(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/ac/a/a/b;->a:Ljava/lang/Integer;

    :goto_2
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/ac/a/a/b;->c:Ljava/lang/Integer;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/ac/a/a/b;->d:Ljava/lang/Integer;

    iput-object v0, v2, Lf/a/a/b;->n:Lcom/google/ac/a/a/b;

    .line 72
    :try_start_0
    invoke-static {p1}, Lcom/google/android/libraries/social/rpc/b/e;->a(Lcom/google/protobuf/nano/j;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 73
    new-instance v4, Lcom/google/c/e/b/a/a/m;

    invoke-direct {v4}, Lcom/google/c/e/b/a/a/m;-><init>()V

    .line 74
    iput-object v2, v4, Lcom/google/c/e/b/a/a/m;->b:Lf/a/a/b;

    .line 77
    if-eqz p0, :cond_1

    const-class v0, Lcom/google/android/libraries/social/consistencytoken/a;

    invoke-static {p0, v0}, Lcom/google/android/libraries/social/a/a;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/consistencytoken/a;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/libraries/social/consistencytoken/a;->a()Ljava/lang/String;

    move-result-object v1

    :cond_1
    iput-object v1, v4, Lcom/google/c/e/b/a/a/m;->d:Ljava/lang/String;

    .line 79
    invoke-virtual {v3, p1, v4}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 85
    :goto_3
    return-void

    :cond_2
    move-object v0, v1

    .line 68
    goto :goto_0

    :cond_3
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/ac/a/a/b;->b:Ljava/lang/Integer;

    goto :goto_1

    :cond_4
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/ac/a/a/b;->a:Ljava/lang/Integer;

    goto :goto_2

    .line 80
    :catch_0
    move-exception v0

    .line 81
    const-string v1, "PlusiUtils"

    const-string v2, "Failed to find apiHeader field on an http request, this should not happen"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 82
    :catch_1
    move-exception v0

    .line 83
    const-string v1, "PlusiUtils"

    const-string v2, "apiHeader field on http request was not accessible, this should not happen"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3
.end method
