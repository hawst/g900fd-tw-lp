.class final Lcom/google/android/libraries/social/autobackup/aa;
.super Lcom/google/android/libraries/social/autobackup/z;
.source "SourceFile"


# static fields
.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field private static final d:Ljava/lang/String;

.field private static final e:[Ljava/lang/String;

.field private static final f:[Ljava/lang/String;

.field private static final g:[Ljava/lang/String;


# instance fields
.field private h:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 712
    const-string v0, "datetaken"

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/aa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/autobackup/aa;->b:Ljava/lang/String;

    .line 714
    const-string v0, "date_added"

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/aa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/autobackup/aa;->c:Ljava/lang/String;

    .line 716
    const-string v0, "date_modified"

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/aa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/autobackup/aa;->d:Ljava/lang/String;

    .line 719
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "bucket_id"

    aput-object v1, v0, v5

    const-string v1, "bucket_display_name"

    aput-object v1, v0, v6

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MAX("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/libraries/social/autobackup/aa;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    const-string v1, "COUNT(*)"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MAX("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/libraries/social/autobackup/aa;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MAX("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/libraries/social/autobackup/aa;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/libraries/social/autobackup/aa;->e:[Ljava/lang/String;

    .line 740
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/google/android/libraries/social/autobackup/aa;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " as corrected_date_taken"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/google/android/libraries/social/autobackup/aa;->c:Ljava/lang/String;

    sget-object v3, Lcom/google/android/libraries/social/autobackup/aa;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/libraries/social/autobackup/aa;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " as corrected_added_modified"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    const-string v1, "mime_type"

    aput-object v1, v0, v7

    const-string v1, "0 as aliased_orientation"

    aput-object v1, v0, v8

    sput-object v0, Lcom/google/android/libraries/social/autobackup/aa;->f:[Ljava/lang/String;

    .line 749
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/libraries/social/autobackup/aa;->g:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 758
    invoke-direct {p0}, Lcom/google/android/libraries/social/autobackup/z;-><init>()V

    .line 759
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/aa;->h:Landroid/net/Uri;

    .line 760
    return-void
.end method


# virtual methods
.method public final a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 769
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/aa;->h:Landroid/net/Uri;

    return-object v0
.end method

.method protected final a(I)Ljava/lang/String;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 779
    packed-switch p1, :pswitch_data_0

    .line 788
    :goto_0
    :pswitch_0
    return-object v0

    .line 783
    :pswitch_1
    const-string v0, "bucket_id NOT NULL AND bucket_display_name NOT NULL AND _data LIKE \'%/DCIM/%\') GROUP BY (2"

    goto :goto_0

    .line 785
    :pswitch_2
    const-string v0, "bucket_id NOT NULL AND bucket_display_name NOT NULL AND _data NOT LIKE \'%/DCIM/%\') GROUP BY (2"

    goto :goto_0

    .line 779
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected final b()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 774
    sget-object v0, Lcom/google/android/libraries/social/autobackup/aa;->e:[Ljava/lang/String;

    return-object v0
.end method

.method protected final c()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 798
    sget-object v0, Lcom/google/android/libraries/social/autobackup/aa;->f:[Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 803
    const-string v0, "bucket_id = ?"

    return-object v0
.end method
