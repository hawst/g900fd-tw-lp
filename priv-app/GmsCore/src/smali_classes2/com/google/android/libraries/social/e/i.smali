.class final Lcom/google/android/libraries/social/e/i;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/nio/charset/Charset;

.field private static final s:S

.field private static final t:S

.field private static final u:S

.field private static final v:S

.field private static final w:S

.field private static final x:S

.field private static final y:S


# instance fields
.field private final b:Lcom/google/android/libraries/social/e/b;

.field private final c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:Lcom/google/android/libraries/social/e/n;

.field private h:Lcom/google/android/libraries/social/e/l;

.field private i:Lcom/google/android/libraries/social/e/n;

.field private j:Lcom/google/android/libraries/social/e/n;

.field private k:Z

.field private l:Z

.field private m:I

.field private n:I

.field private o:[B

.field private p:I

.field private q:I

.field private final r:Lcom/google/android/libraries/social/e/c;

.field private final z:Ljava/util/TreeMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 145
    const-string v0, "US-ASCII"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/e/i;->a:Ljava/nio/charset/Charset;

    .line 168
    sget v0, Lcom/google/android/libraries/social/e/c;->C:I

    invoke-static {v0}, Lcom/google/android/libraries/social/e/c;->a(I)S

    move-result v0

    sput-short v0, Lcom/google/android/libraries/social/e/i;->s:S

    .line 170
    sget v0, Lcom/google/android/libraries/social/e/c;->D:I

    invoke-static {v0}, Lcom/google/android/libraries/social/e/c;->a(I)S

    move-result v0

    sput-short v0, Lcom/google/android/libraries/social/e/i;->t:S

    .line 171
    sget v0, Lcom/google/android/libraries/social/e/c;->am:I

    invoke-static {v0}, Lcom/google/android/libraries/social/e/c;->a(I)S

    move-result v0

    sput-short v0, Lcom/google/android/libraries/social/e/i;->u:S

    .line 173
    sget v0, Lcom/google/android/libraries/social/e/c;->E:I

    invoke-static {v0}, Lcom/google/android/libraries/social/e/c;->a(I)S

    move-result v0

    sput-short v0, Lcom/google/android/libraries/social/e/i;->v:S

    .line 175
    sget v0, Lcom/google/android/libraries/social/e/c;->F:I

    invoke-static {v0}, Lcom/google/android/libraries/social/e/c;->a(I)S

    move-result v0

    sput-short v0, Lcom/google/android/libraries/social/e/i;->w:S

    .line 177
    sget v0, Lcom/google/android/libraries/social/e/c;->i:I

    invoke-static {v0}, Lcom/google/android/libraries/social/e/c;->a(I)S

    move-result v0

    sput-short v0, Lcom/google/android/libraries/social/e/i;->x:S

    .line 179
    sget v0, Lcom/google/android/libraries/social/e/c;->m:I

    invoke-static {v0}, Lcom/google/android/libraries/social/e/c;->a(I)S

    move-result v0

    sput-short v0, Lcom/google/android/libraries/social/e/i;->y:S

    return-void
.end method

.method private constructor <init>(Ljava/io/InputStream;ILcom/google/android/libraries/social/e/c;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    iput v4, p0, Lcom/google/android/libraries/social/e/i;->d:I

    .line 152
    iput v4, p0, Lcom/google/android/libraries/social/e/i;->e:I

    .line 160
    iput-boolean v4, p0, Lcom/google/android/libraries/social/e/i;->l:Z

    .line 162
    iput v4, p0, Lcom/google/android/libraries/social/e/i;->n:I

    .line 182
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/e/i;->z:Ljava/util/TreeMap;

    .line 206
    if-nez p1, :cond_0

    .line 207
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Null argument inputStream to ExifParser"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 212
    :cond_0
    iput-object p3, p0, Lcom/google/android/libraries/social/e/i;->r:Lcom/google/android/libraries/social/e/c;

    .line 213
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/e/i;->a(Ljava/io/InputStream;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/e/i;->l:Z

    .line 214
    new-instance v0, Lcom/google/android/libraries/social/e/b;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/social/e/b;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    .line 215
    iput p2, p0, Lcom/google/android/libraries/social/e/i;->c:I

    .line 216
    iget-boolean v0, p0, Lcom/google/android/libraries/social/e/i;->l:Z

    if-nez v0, :cond_2

    .line 234
    :cond_1
    :goto_0
    return-void

    .line 220
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/b;->c()S

    move-result v0

    const/16 v1, 0x4949

    if-ne v1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/e/b;->a(Ljava/nio/ByteOrder;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/b;->c()S

    move-result v0

    const/16 v1, 0x2a

    if-eq v0, v1, :cond_5

    new-instance v0, Lcom/google/android/libraries/social/e/e;

    const-string v1, "Invalid TIFF header"

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/e/e;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    const/16 v1, 0x4d4d

    if-ne v1, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    sget-object v1, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/e/b;->a(Ljava/nio/ByteOrder;)V

    goto :goto_1

    :cond_4
    new-instance v0, Lcom/google/android/libraries/social/e/e;

    const-string v1, "Invalid TIFF header"

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/e/e;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :cond_5
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/b;->f()J

    move-result-wide v0

    .line 222
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_6

    .line 223
    new-instance v2, Lcom/google/android/libraries/social/e/e;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid offset "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/libraries/social/e/e;-><init>(Ljava/lang/String;)V

    throw v2

    .line 225
    :cond_6
    long-to-int v2, v0

    iput v2, p0, Lcom/google/android/libraries/social/e/i;->p:I

    .line 226
    iput v4, p0, Lcom/google/android/libraries/social/e/i;->f:I

    .line 227
    invoke-direct {p0, v4}, Lcom/google/android/libraries/social/e/i;->a(I)Z

    move-result v2

    if-nez v2, :cond_7

    invoke-direct {p0}, Lcom/google/android/libraries/social/e/i;->l()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 228
    :cond_7
    invoke-direct {p0, v4, v0, v1}, Lcom/google/android/libraries/social/e/i;->a(IJ)V

    .line 229
    const-wide/16 v2, 0x8

    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 230
    long-to-int v0, v0

    add-int/lit8 v0, v0, -0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/libraries/social/e/i;->o:[B

    .line 231
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->o:[B

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/e/i;->a([B)I

    goto :goto_0
.end method

.method protected static a(Ljava/io/InputStream;ILcom/google/android/libraries/social/e/c;)Lcom/google/android/libraries/social/e/i;
    .locals 1

    .prologue
    .line 244
    new-instance v0, Lcom/google/android/libraries/social/e/i;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/libraries/social/e/i;-><init>(Ljava/io/InputStream;ILcom/google/android/libraries/social/e/c;)V

    return-object v0
.end method

.method protected static a(Ljava/io/InputStream;Lcom/google/android/libraries/social/e/c;)Lcom/google/android/libraries/social/e/i;
    .locals 2

    .prologue
    .line 257
    new-instance v0, Lcom/google/android/libraries/social/e/i;

    const/16 v1, 0x3f

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/libraries/social/e/i;-><init>(Ljava/io/InputStream;ILcom/google/android/libraries/social/e/c;)V

    return-object v0
.end method

.method private a(IJ)V
    .locals 4

    .prologue
    .line 525
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->z:Ljava/util/TreeMap;

    long-to-int v1, p2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/google/android/libraries/social/e/k;

    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/e/i;->a(I)Z

    move-result v3

    invoke-direct {v2, p1, v3}, Lcom/google/android/libraries/social/e/k;-><init>(IZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 526
    return-void
.end method

.method private a(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 185
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 197
    :cond_0
    :goto_0
    return v0

    .line 187
    :pswitch_0
    iget v2, p0, Lcom/google/android/libraries/social/e/i;->c:I

    and-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 189
    :pswitch_1
    iget v2, p0, Lcom/google/android/libraries/social/e/i;->c:I

    and-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 191
    :pswitch_2
    iget v2, p0, Lcom/google/android/libraries/social/e/i;->c:I

    and-int/lit8 v2, v2, 0x4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 193
    :pswitch_3
    iget v2, p0, Lcom/google/android/libraries/social/e/i;->c:I

    and-int/lit8 v2, v2, 0x8

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 195
    :pswitch_4
    iget v2, p0, Lcom/google/android/libraries/social/e/i;->c:I

    and-int/lit8 v2, v2, 0x10

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 185
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method private a(II)Z
    .locals 1

    .prologue
    .line 643
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->r:Lcom/google/android/libraries/social/e/c;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/c;->a()Landroid/util/SparseIntArray;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    .line 644
    if-nez v0, :cond_0

    .line 645
    const/4 v0, 0x0

    .line 647
    :goto_0
    return v0

    :cond_0
    invoke-static {v0, p1}, Lcom/google/android/libraries/social/e/c;->a(II)Z

    move-result v0

    goto :goto_0
.end method

.method private a(Ljava/io/InputStream;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 759
    new-instance v3, Lcom/google/android/libraries/social/e/b;

    invoke-direct {v3, p1}, Lcom/google/android/libraries/social/e/b;-><init>(Ljava/io/InputStream;)V

    .line 760
    invoke-virtual {v3}, Lcom/google/android/libraries/social/e/b;->c()S

    move-result v1

    const/16 v2, -0x28

    if-eq v1, v2, :cond_0

    .line 761
    new-instance v0, Lcom/google/android/libraries/social/e/e;

    const-string v1, "Invalid JPEG format"

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/e/e;-><init>(Ljava/lang/String;)V

    throw v0

    .line 764
    :cond_0
    invoke-virtual {v3}, Lcom/google/android/libraries/social/e/b;->c()S

    move-result v1

    move v2, v1

    .line 765
    :goto_0
    const/16 v1, -0x27

    if-eq v2, v1, :cond_1

    invoke-static {v2}, Lcom/google/android/libraries/social/e/p;->a(S)Z

    move-result v1

    if-nez v1, :cond_1

    .line 767
    invoke-virtual {v3}, Lcom/google/android/libraries/social/e/b;->d()I

    move-result v1

    .line 770
    const/16 v4, -0x1f

    if-ne v2, v4, :cond_2

    .line 771
    const/16 v2, 0x8

    if-lt v1, v2, :cond_2

    .line 774
    invoke-virtual {v3}, Lcom/google/android/libraries/social/e/b;->e()I

    move-result v2

    .line 775
    invoke-virtual {v3}, Lcom/google/android/libraries/social/e/b;->c()S

    move-result v4

    .line 776
    add-int/lit8 v1, v1, -0x6

    .line 777
    const v5, 0x45786966

    if-ne v2, v5, :cond_2

    if-nez v4, :cond_2

    .line 778
    invoke-virtual {v3}, Lcom/google/android/libraries/social/e/b;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/e/i;->q:I

    .line 779
    iput v1, p0, Lcom/google/android/libraries/social/e/i;->m:I

    .line 780
    iget v0, p0, Lcom/google/android/libraries/social/e/i;->q:I

    iget v1, p0, Lcom/google/android/libraries/social/e/i;->m:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/libraries/social/e/i;->n:I

    .line 781
    const/4 v0, 0x1

    .line 791
    :cond_1
    :goto_1
    return v0

    .line 785
    :cond_2
    const/4 v2, 0x2

    if-lt v1, v2, :cond_3

    add-int/lit8 v2, v1, -0x2

    int-to-long v4, v2

    add-int/lit8 v1, v1, -0x2

    int-to-long v6, v1

    invoke-virtual {v3, v6, v7}, Lcom/google/android/libraries/social/e/b;->skip(J)J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-eqz v1, :cond_4

    .line 786
    :cond_3
    const-string v1, "ExifParser"

    const-string v2, "Invalid JPEG format."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 789
    :cond_4
    invoke-virtual {v3}, Lcom/google/android/libraries/social/e/b;->c()S

    move-result v1

    move v2, v1

    .line 790
    goto :goto_0
.end method

.method private b(I)V
    .locals 4

    .prologue
    .line 501
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/social/e/b;->a(J)V

    .line 502
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v0, p1, :cond_0

    .line 503
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->pollFirstEntry()Ljava/util/Map$Entry;

    goto :goto_0

    .line 505
    :cond_0
    return-void
.end method

.method private c(Lcom/google/android/libraries/social/e/n;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x3

    const/4 v0, 0x0

    .line 592
    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v1

    if-nez v1, :cond_1

    .line 640
    :cond_0
    :goto_0
    return-void

    .line 595
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->b()S

    move-result v1

    .line 596
    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->a()I

    move-result v2

    .line 597
    sget-short v3, Lcom/google/android/libraries/social/e/i;->s:S

    if-ne v1, v3, :cond_3

    sget v3, Lcom/google/android/libraries/social/e/c;->C:I

    invoke-direct {p0, v2, v3}, Lcom/google/android/libraries/social/e/i;->a(II)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 598
    invoke-direct {p0, v5}, Lcom/google/android/libraries/social/e/i;->a(I)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-direct {p0, v4}, Lcom/google/android/libraries/social/e/i;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 600
    :cond_2
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/e/n;->e(I)J

    move-result-wide v0

    invoke-direct {p0, v5, v0, v1}, Lcom/google/android/libraries/social/e/i;->a(IJ)V

    goto :goto_0

    .line 602
    :cond_3
    sget-short v3, Lcom/google/android/libraries/social/e/i;->t:S

    if-ne v1, v3, :cond_4

    sget v3, Lcom/google/android/libraries/social/e/c;->D:I

    invoke-direct {p0, v2, v3}, Lcom/google/android/libraries/social/e/i;->a(II)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 603
    invoke-direct {p0, v6}, Lcom/google/android/libraries/social/e/i;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 604
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/e/n;->e(I)J

    move-result-wide v0

    invoke-direct {p0, v6, v0, v1}, Lcom/google/android/libraries/social/e/i;->a(IJ)V

    goto :goto_0

    .line 606
    :cond_4
    sget-short v3, Lcom/google/android/libraries/social/e/i;->u:S

    if-ne v1, v3, :cond_5

    sget v3, Lcom/google/android/libraries/social/e/c;->am:I

    invoke-direct {p0, v2, v3}, Lcom/google/android/libraries/social/e/i;->a(II)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 608
    invoke-direct {p0, v4}, Lcom/google/android/libraries/social/e/i;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 609
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/e/n;->e(I)J

    move-result-wide v0

    invoke-direct {p0, v4, v0, v1}, Lcom/google/android/libraries/social/e/i;->a(IJ)V

    goto :goto_0

    .line 611
    :cond_5
    sget-short v3, Lcom/google/android/libraries/social/e/i;->v:S

    if-ne v1, v3, :cond_6

    sget v3, Lcom/google/android/libraries/social/e/c;->E:I

    invoke-direct {p0, v2, v3}, Lcom/google/android/libraries/social/e/i;->a(II)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 613
    invoke-direct {p0}, Lcom/google/android/libraries/social/e/i;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 614
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/e/n;->e(I)J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/libraries/social/e/i;->z:Ljava/util/TreeMap;

    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/google/android/libraries/social/e/l;

    invoke-direct {v1}, Lcom/google/android/libraries/social/e/l;-><init>()V

    invoke-virtual {v2, v0, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 616
    :cond_6
    sget-short v3, Lcom/google/android/libraries/social/e/i;->w:S

    if-ne v1, v3, :cond_7

    sget v3, Lcom/google/android/libraries/social/e/c;->F:I

    invoke-direct {p0, v2, v3}, Lcom/google/android/libraries/social/e/i;->a(II)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 618
    invoke-direct {p0}, Lcom/google/android/libraries/social/e/i;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 619
    iput-object p1, p0, Lcom/google/android/libraries/social/e/i;->j:Lcom/google/android/libraries/social/e/n;

    goto/16 :goto_0

    .line 621
    :cond_7
    sget-short v3, Lcom/google/android/libraries/social/e/i;->x:S

    if-ne v1, v3, :cond_9

    sget v3, Lcom/google/android/libraries/social/e/c;->i:I

    invoke-direct {p0, v2, v3}, Lcom/google/android/libraries/social/e/i;->a(II)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 622
    invoke-direct {p0}, Lcom/google/android/libraries/social/e/i;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 623
    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->f()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 624
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 625
    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->c()S

    .line 626
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/e/n;->e(I)J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/libraries/social/e/i;->z:Ljava/util/TreeMap;

    long-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/google/android/libraries/social/e/l;

    invoke-direct {v3, v0}, Lcom/google/android/libraries/social/e/l;-><init>(I)V

    invoke-virtual {v1, v2, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 624
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 632
    :cond_8
    iget-object v1, p0, Lcom/google/android/libraries/social/e/i;->z:Ljava/util/TreeMap;

    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->j()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/google/android/libraries/social/e/j;

    invoke-direct {v3, p1, v0}, Lcom/google/android/libraries/social/e/j;-><init>(Lcom/google/android/libraries/social/e/n;Z)V

    invoke-virtual {v1, v2, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 635
    :cond_9
    sget-short v0, Lcom/google/android/libraries/social/e/i;->y:S

    if-ne v1, v0, :cond_0

    sget v0, Lcom/google/android/libraries/social/e/c;->m:I

    invoke-direct {p0, v2, v0}, Lcom/google/android/libraries/social/e/i;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/libraries/social/e/i;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 638
    iput-object p1, p0, Lcom/google/android/libraries/social/e/i;->i:Lcom/google/android/libraries/social/e/n;

    goto/16 :goto_0
.end method

.method private k()Z
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Lcom/google/android/libraries/social/e/i;->c:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l()Z
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 395
    iget v2, p0, Lcom/google/android/libraries/social/e/i;->f:I

    packed-switch v2, :pswitch_data_0

    .line 406
    :cond_0
    :goto_0
    return v0

    .line 397
    :pswitch_0
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/google/android/libraries/social/e/i;->a(I)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x4

    invoke-direct {p0, v2}, Lcom/google/android/libraries/social/e/i;->a(I)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0, v3}, Lcom/google/android/libraries/social/e/i;->a(I)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0, v1}, Lcom/google/android/libraries/social/e/i;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 401
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/libraries/social/e/i;->k()Z

    move-result v0

    goto :goto_0

    .line 404
    :pswitch_2
    invoke-direct {p0, v3}, Lcom/google/android/libraries/social/e/i;->a(I)Z

    move-result v0

    goto :goto_0

    .line 395
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private m()Lcom/google/android/libraries/social/e/n;
    .locals 12

    .prologue
    const-wide/32 v10, 0x7fffffff

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 538
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/b;->c()S

    move-result v1

    .line 539
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/b;->c()S

    move-result v2

    .line 540
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/b;->f()J

    move-result-wide v8

    .line 541
    cmp-long v0, v8, v10

    if-lez v0, :cond_0

    .line 542
    new-instance v0, Lcom/google/android/libraries/social/e/e;

    const-string v1, "Number of component is larger then Integer.MAX_VALUE"

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/e/e;-><init>(Ljava/lang/String;)V

    throw v0

    .line 546
    :cond_0
    invoke-static {v2}, Lcom/google/android/libraries/social/e/n;->a(S)Z

    move-result v0

    if-nez v0, :cond_1

    .line 547
    const-string v0, "ExifParser"

    const-string v3, "Tag %04x: Invalid data type %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    aput-object v1, v4, v6

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    const-wide/16 v2, 0x4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/social/e/b;->skip(J)J

    .line 549
    const/4 v0, 0x0

    .line 583
    :goto_0
    return-object v0

    .line 552
    :cond_1
    new-instance v0, Lcom/google/android/libraries/social/e/n;

    long-to-int v3, v8

    iget v4, p0, Lcom/google/android/libraries/social/e/i;->f:I

    long-to-int v7, v8

    if-eqz v7, :cond_2

    :goto_1
    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/social/e/n;-><init>(SSIIZ)V

    .line 554
    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/n;->d()I

    move-result v1

    .line 555
    const/4 v3, 0x4

    if-le v1, v3, :cond_5

    .line 556
    iget-object v1, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/b;->f()J

    move-result-wide v4

    .line 557
    cmp-long v1, v4, v10

    if-lez v1, :cond_3

    .line 558
    new-instance v0, Lcom/google/android/libraries/social/e/e;

    const-string v1, "offset is larger then Integer.MAX_VALUE"

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/e/e;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v5, v6

    .line 552
    goto :goto_1

    .line 563
    :cond_3
    iget-object v1, p0, Lcom/google/android/libraries/social/e/i;->o:[B

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/google/android/libraries/social/e/i;->p:I

    int-to-long v10, v1

    cmp-long v1, v4, v10

    if-gez v1, :cond_4

    const/4 v1, 0x7

    if-ne v2, v1, :cond_4

    .line 565
    long-to-int v1, v8

    new-array v1, v1, [B

    .line 566
    iget-object v2, p0, Lcom/google/android/libraries/social/e/i;->o:[B

    long-to-int v3, v4

    add-int/lit8 v3, v3, -0x8

    long-to-int v4, v8

    invoke-static {v2, v3, v1, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 568
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/e/n;->a([B)Z

    goto :goto_0

    .line 570
    :cond_4
    long-to-int v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/e/n;->g(I)V

    goto :goto_0

    .line 573
    :cond_5
    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/n;->k()Z

    move-result v2

    .line 575
    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/e/n;->a(Z)V

    .line 577
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/e/i;->b(Lcom/google/android/libraries/social/e/n;)V

    .line 578
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/e/n;->a(Z)V

    .line 579
    iget-object v2, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    rsub-int/lit8 v1, v1, 0x4

    int-to-long v4, v1

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/social/e/b;->skip(J)J

    .line 581
    iget-object v1, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/b;->a()I

    move-result v1

    add-int/lit8 v1, v1, -0x4

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/e/n;->g(I)V

    goto :goto_0
.end method

.method private n()J
    .locals 4

    .prologue
    .line 851
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/b;->e()I

    move-result v0

    int-to-long v0, v0

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method protected final a()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x4

    const/4 v2, 0x5

    const/4 v0, 0x1

    .line 275
    :cond_0
    iget-boolean v3, p0, Lcom/google/android/libraries/social/e/i;->l:Z

    if-nez v3, :cond_2

    move v0, v2

    .line 356
    :cond_1
    :goto_0
    return v0

    .line 278
    :cond_2
    iget-object v3, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/e/b;->a()I

    move-result v3

    .line 279
    iget v4, p0, Lcom/google/android/libraries/social/e/i;->d:I

    add-int/lit8 v4, v4, 0x2

    iget v5, p0, Lcom/google/android/libraries/social/e/i;->e:I

    mul-int/lit8 v5, v5, 0xc

    add-int/2addr v4, v5

    .line 280
    if-ge v3, v4, :cond_3

    .line 281
    invoke-direct {p0}, Lcom/google/android/libraries/social/e/i;->m()Lcom/google/android/libraries/social/e/n;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/libraries/social/e/i;->g:Lcom/google/android/libraries/social/e/n;

    .line 282
    iget-object v3, p0, Lcom/google/android/libraries/social/e/i;->g:Lcom/google/android/libraries/social/e/n;

    if-eqz v3, :cond_0

    .line 283
    iget-boolean v1, p0, Lcom/google/android/libraries/social/e/i;->k:Z

    if-eqz v1, :cond_1

    .line 286
    iget-object v1, p0, Lcom/google/android/libraries/social/e/i;->g:Lcom/google/android/libraries/social/e/n;

    invoke-direct {p0, v1}, Lcom/google/android/libraries/social/e/i;->c(Lcom/google/android/libraries/social/e/n;)V

    goto :goto_0

    .line 289
    :cond_3
    if-ne v3, v4, :cond_5

    .line 291
    iget v3, p0, Lcom/google/android/libraries/social/e/i;->f:I

    if-nez v3, :cond_6

    .line 292
    invoke-direct {p0}, Lcom/google/android/libraries/social/e/i;->n()J

    move-result-wide v4

    .line 293
    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/e/i;->a(I)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-direct {p0}, Lcom/google/android/libraries/social/e/i;->k()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 294
    :cond_4
    cmp-long v1, v4, v6

    if-eqz v1, :cond_5

    .line 295
    invoke-direct {p0, v0, v4, v5}, Lcom/google/android/libraries/social/e/i;->a(IJ)V

    .line 315
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    .line 316
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->pollFirstEntry()Ljava/util/Map$Entry;

    move-result-object v3

    .line 317
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 319
    :try_start_0
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/e/i;->b(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 325
    instance-of v0, v1, Lcom/google/android/libraries/social/e/k;

    if-eqz v0, :cond_a

    move-object v0, v1

    .line 326
    check-cast v0, Lcom/google/android/libraries/social/e/k;

    iget v0, v0, Lcom/google/android/libraries/social/e/k;->a:I

    iput v0, p0, Lcom/google/android/libraries/social/e/i;->f:I

    .line 327
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/b;->d()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/e/i;->e:I

    .line 328
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/e/i;->d:I

    .line 330
    iget v0, p0, Lcom/google/android/libraries/social/e/i;->e:I

    mul-int/lit8 v0, v0, 0xc

    iget v3, p0, Lcom/google/android/libraries/social/e/i;->d:I

    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x2

    iget v3, p0, Lcom/google/android/libraries/social/e/i;->m:I

    if-le v0, v3, :cond_8

    .line 331
    const-string v0, "ExifParser"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Invalid size of IFD "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/libraries/social/e/i;->f:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 332
    goto/16 :goto_0

    .line 301
    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    .line 302
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/e/b;->a()I

    move-result v3

    sub-int/2addr v0, v3

    .line 305
    :goto_2
    if-ge v0, v1, :cond_7

    .line 306
    const-string v1, "ExifParser"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid size of link to next IFD: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 308
    :cond_7
    invoke-direct {p0}, Lcom/google/android/libraries/social/e/i;->n()J

    move-result-wide v0

    .line 309
    cmp-long v3, v0, v6

    if-eqz v3, :cond_5

    .line 310
    const-string v3, "ExifParser"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid link to next IFD: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 321
    :catch_0
    move-exception v0

    const-string v0, "ExifParser"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to skip to data at: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", the file may be broken."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 335
    :cond_8
    invoke-direct {p0}, Lcom/google/android/libraries/social/e/i;->l()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/e/i;->k:Z

    .line 336
    check-cast v1, Lcom/google/android/libraries/social/e/k;

    iget-boolean v0, v1, Lcom/google/android/libraries/social/e/k;->b:Z

    if-eqz v0, :cond_9

    .line 337
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 339
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/libraries/social/e/i;->b()V

    goto/16 :goto_1

    .line 341
    :cond_a
    instance-of v0, v1, Lcom/google/android/libraries/social/e/l;

    if-eqz v0, :cond_b

    .line 342
    check-cast v1, Lcom/google/android/libraries/social/e/l;

    iput-object v1, p0, Lcom/google/android/libraries/social/e/i;->h:Lcom/google/android/libraries/social/e/l;

    .line 343
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->h:Lcom/google/android/libraries/social/e/l;

    iget v0, v0, Lcom/google/android/libraries/social/e/l;->b:I

    goto/16 :goto_0

    .line 345
    :cond_b
    check-cast v1, Lcom/google/android/libraries/social/e/j;

    .line 346
    iget-object v0, v1, Lcom/google/android/libraries/social/e/j;->a:Lcom/google/android/libraries/social/e/n;

    iput-object v0, p0, Lcom/google/android/libraries/social/e/i;->g:Lcom/google/android/libraries/social/e/n;

    .line 347
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->g:Lcom/google/android/libraries/social/e/n;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/n;->c()S

    move-result v0

    const/4 v3, 0x7

    if-eq v0, v3, :cond_c

    .line 348
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->g:Lcom/google/android/libraries/social/e/n;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/e/i;->b(Lcom/google/android/libraries/social/e/n;)V

    .line 349
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->g:Lcom/google/android/libraries/social/e/n;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/e/i;->c(Lcom/google/android/libraries/social/e/n;)V

    .line 351
    :cond_c
    iget-boolean v0, v1, Lcom/google/android/libraries/social/e/j;->b:Z

    if-eqz v0, :cond_5

    .line 352
    const/4 v0, 0x2

    goto/16 :goto_0

    :cond_d
    move v0, v2

    .line 356
    goto/16 :goto_0

    :cond_e
    move v0, v1

    goto/16 :goto_2
.end method

.method protected final a([B)I
    .locals 1

    .prologue
    .line 813
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/e/b;->read([B)I

    move-result v0

    return v0
.end method

.method protected final a(Lcom/google/android/libraries/social/e/n;)V
    .locals 4

    .prologue
    .line 517
    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->j()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/b;->a()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 518
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->z:Ljava/util/TreeMap;

    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->j()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/google/android/libraries/social/e/j;

    const/4 v3, 0x1

    invoke-direct {v2, p1, v3}, Lcom/google/android/libraries/social/e/j;-><init>(Lcom/google/android/libraries/social/e/n;Z)V

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 520
    :cond_0
    return-void
.end method

.method protected final b()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 367
    iget v0, p0, Lcom/google/android/libraries/social/e/i;->d:I

    add-int/lit8 v0, v0, 0x2

    iget v1, p0, Lcom/google/android/libraries/social/e/i;->e:I

    mul-int/lit8 v1, v1, 0xc

    add-int/2addr v1, v0

    .line 368
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/b;->a()I

    move-result v0

    .line 369
    if-le v0, v1, :cond_1

    .line 392
    :cond_0
    :goto_0
    return-void

    .line 372
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/libraries/social/e/i;->k:Z

    if-eqz v2, :cond_3

    .line 373
    :cond_2
    :goto_1
    if-ge v0, v1, :cond_4

    .line 374
    invoke-direct {p0}, Lcom/google/android/libraries/social/e/i;->m()Lcom/google/android/libraries/social/e/n;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/libraries/social/e/i;->g:Lcom/google/android/libraries/social/e/n;

    .line 375
    add-int/lit8 v0, v0, 0xc

    .line 376
    iget-object v2, p0, Lcom/google/android/libraries/social/e/i;->g:Lcom/google/android/libraries/social/e/n;

    if-eqz v2, :cond_2

    .line 377
    iget-object v2, p0, Lcom/google/android/libraries/social/e/i;->g:Lcom/google/android/libraries/social/e/n;

    invoke-direct {p0, v2}, Lcom/google/android/libraries/social/e/i;->c(Lcom/google/android/libraries/social/e/n;)V

    goto :goto_1

    .line 382
    :cond_3
    invoke-direct {p0, v1}, Lcom/google/android/libraries/social/e/i;->b(I)V

    .line 384
    :cond_4
    invoke-direct {p0}, Lcom/google/android/libraries/social/e/i;->n()J

    move-result-wide v0

    .line 386
    iget v2, p0, Lcom/google/android/libraries/social/e/i;->f:I

    if-nez v2, :cond_0

    invoke-direct {p0, v4}, Lcom/google/android/libraries/social/e/i;->a(I)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-direct {p0}, Lcom/google/android/libraries/social/e/i;->k()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 388
    :cond_5
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 389
    invoke-direct {p0, v4, v0, v1}, Lcom/google/android/libraries/social/e/i;->a(IJ)V

    goto :goto_0
.end method

.method protected final b(Lcom/google/android/libraries/social/e/n;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 652
    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->c()S

    move-result v0

    .line 653
    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    const/4 v2, 0x7

    if-eq v0, v2, :cond_0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 655
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v2

    .line 656
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 657
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/e/b;->a()I

    move-result v3

    add-int/2addr v2, v3

    if-ge v0, v2, :cond_1

    .line 659
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 660
    instance-of v2, v0, Lcom/google/android/libraries/social/e/l;

    if-eqz v2, :cond_2

    .line 662
    const-string v0, "ExifParser"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Thumbnail overlaps value for tag: \n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 663
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->pollFirstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    .line 664
    const-string v2, "ExifParser"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid thumbnail offset: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->c()S

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 739
    :goto_1
    :pswitch_0
    return-void

    .line 667
    :cond_2
    instance-of v2, v0, Lcom/google/android/libraries/social/e/k;

    if-eqz v2, :cond_4

    .line 668
    const-string v2, "ExifParser"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Ifd "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/google/android/libraries/social/e/k;

    iget v0, v0, Lcom/google/android/libraries/social/e/k;->a:I

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " overlaps value for tag: \n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->z:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/e/b;->a()I

    move-result v2

    sub-int/2addr v0, v2

    .line 677
    const-string v2, "ExifParser"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid size of tag: \n"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " setting count to: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 679
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/e/n;->c(I)V

    goto :goto_0

    .line 670
    :cond_4
    instance-of v2, v0, Lcom/google/android/libraries/social/e/j;

    if-eqz v2, :cond_3

    .line 671
    const-string v2, "ExifParser"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Tag value for tag: \n"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/google/android/libraries/social/e/j;

    iget-object v0, v0, Lcom/google/android/libraries/social/e/j;->a:Lcom/google/android/libraries/social/e/n;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/n;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " overlaps value for tag: \n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 687
    :pswitch_1
    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v0

    new-array v0, v0, [B

    .line 688
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/e/i;->a([B)I

    .line 689
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/e/n;->a([B)Z

    goto/16 :goto_1

    .line 693
    :pswitch_2
    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v0

    sget-object v1, Lcom/google/android/libraries/social/e/i;->a:Ljava/nio/charset/Charset;

    if-lez v0, :cond_5

    iget-object v2, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/libraries/social/e/b;->a(ILjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/e/n;->a(Ljava/lang/String;)Z

    goto/16 :goto_1

    :cond_5
    const-string v0, ""

    goto :goto_3

    .line 696
    :pswitch_3
    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v0

    new-array v0, v0, [J

    .line 697
    array-length v2, v0

    :goto_4
    if-ge v1, v2, :cond_6

    .line 698
    invoke-direct {p0}, Lcom/google/android/libraries/social/e/i;->n()J

    move-result-wide v4

    aput-wide v4, v0, v1

    .line 697
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 700
    :cond_6
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/e/n;->a([J)Z

    goto/16 :goto_1

    .line 704
    :pswitch_4
    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/libraries/social/e/r;

    .line 705
    array-length v2, v0

    :goto_5
    if-ge v1, v2, :cond_7

    .line 706
    invoke-direct {p0}, Lcom/google/android/libraries/social/e/i;->n()J

    move-result-wide v4

    invoke-direct {p0}, Lcom/google/android/libraries/social/e/i;->n()J

    move-result-wide v6

    new-instance v3, Lcom/google/android/libraries/social/e/r;

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/google/android/libraries/social/e/r;-><init>(JJ)V

    aput-object v3, v0, v1

    .line 705
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 708
    :cond_7
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/e/n;->a([Lcom/google/android/libraries/social/e/r;)Z

    goto/16 :goto_1

    .line 712
    :pswitch_5
    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v0

    new-array v0, v0, [I

    .line 713
    array-length v2, v0

    :goto_6
    if-ge v1, v2, :cond_8

    .line 714
    iget-object v3, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/e/b;->c()S

    move-result v3

    const v4, 0xffff

    and-int/2addr v3, v4

    aput v3, v0, v1

    .line 713
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 716
    :cond_8
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/e/n;->a([I)Z

    goto/16 :goto_1

    .line 720
    :pswitch_6
    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v0

    new-array v0, v0, [I

    .line 721
    array-length v2, v0

    :goto_7
    if-ge v1, v2, :cond_9

    .line 722
    iget-object v3, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/e/b;->e()I

    move-result v3

    aput v3, v0, v1

    .line 721
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 724
    :cond_9
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/e/n;->a([I)Z

    goto/16 :goto_1

    .line 728
    :pswitch_7
    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v0

    new-array v2, v0, [Lcom/google/android/libraries/social/e/r;

    .line 729
    array-length v3, v2

    move v0, v1

    :goto_8
    if-ge v0, v3, :cond_a

    .line 730
    iget-object v1, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/b;->e()I

    move-result v1

    iget-object v4, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v4}, Lcom/google/android/libraries/social/e/b;->e()I

    move-result v4

    new-instance v5, Lcom/google/android/libraries/social/e/r;

    int-to-long v6, v1

    int-to-long v8, v4

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/google/android/libraries/social/e/r;-><init>(JJ)V

    aput-object v5, v2, v0

    .line 729
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 732
    :cond_a
    invoke-virtual {p1, v2}, Lcom/google/android/libraries/social/e/n;->a([Lcom/google/android/libraries/social/e/r;)Z

    goto/16 :goto_1

    .line 684
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method protected final c()Lcom/google/android/libraries/social/e/n;
    .locals 1

    .prologue
    .line 435
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->g:Lcom/google/android/libraries/social/e/n;

    return-object v0
.end method

.method protected final d()I
    .locals 1

    .prologue
    .line 455
    iget v0, p0, Lcom/google/android/libraries/social/e/i;->f:I

    return v0
.end method

.method protected final e()I
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->h:Lcom/google/android/libraries/social/e/l;

    iget v0, v0, Lcom/google/android/libraries/social/e/l;->a:I

    return v0
.end method

.method protected final f()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 483
    iget-object v1, p0, Lcom/google/android/libraries/social/e/i;->i:Lcom/google/android/libraries/social/e/n;

    if-nez v1, :cond_0

    .line 486
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/social/e/i;->i:Lcom/google/android/libraries/social/e/n;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/e/n;->e(I)J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0
.end method

.method protected final g()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 494
    iget-object v1, p0, Lcom/google/android/libraries/social/e/i;->j:Lcom/google/android/libraries/social/e/n;

    if-nez v1, :cond_0

    .line 497
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/social/e/i;->j:Lcom/google/android/libraries/social/e/n;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/e/n;->e(I)J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0
.end method

.method protected final h()I
    .locals 1

    .prologue
    .line 795
    iget v0, p0, Lcom/google/android/libraries/social/e/i;->n:I

    return v0
.end method

.method protected final i()I
    .locals 1

    .prologue
    .line 799
    iget v0, p0, Lcom/google/android/libraries/social/e/i;->q:I

    return v0
.end method

.method protected final j()Ljava/nio/ByteOrder;
    .locals 1

    .prologue
    .line 919
    iget-object v0, p0, Lcom/google/android/libraries/social/e/i;->b:Lcom/google/android/libraries/social/e/b;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/b;->b()Ljava/nio/ByteOrder;

    move-result-object v0

    return-object v0
.end method
