.class public interface abstract Lcom/google/android/libraries/social/autobackup/ar;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field public static final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 47
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id AS outer_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "media_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "media_url AS outer_media_url"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "upload_state"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "upload_status"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "upload_account_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "upload_finish_time"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "CAST(bytes_uploaded AS REAL) / bytes_total"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "is_image"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/libraries/social/autobackup/ar;->a:[Ljava/lang/String;

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "upload_state = 300 AND NOT EXISTS (SELECT media_url AS inner_media_url FROM "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/g/a/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE inner_media_url = outer_media_url AND outer_id < _id AND upload_state"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " != 300)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/autobackup/ar;->b:Ljava/lang/String;

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "(upload_state IN (100, 200, 400) OR ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/libraries/social/autobackup/ar;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")) AND (album_id IS NULL OR album_id == \"instant\")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/autobackup/ar;->c:Ljava/lang/String;

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "upload_account_id = ? AND ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/libraries/social/autobackup/ar;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/autobackup/ar;->d:Ljava/lang/String;

    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "upload_account_id = ? AND bucket_id = ? AND ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/libraries/social/autobackup/ar;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/autobackup/ar;->e:Ljava/lang/String;

    return-void
.end method
