.class public Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;


# static fields
.field static final VIBRATION_TIME_IN_MS:I = 0x32


# instance fields
.field private final cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

.field private final exitHandler:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

.field private final isUserNotificationNeeded:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final previewOverlayPresenter:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayPresenter;

.field private final vibrator:Landroid/os/Vibrator;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayPresenter;Landroid/os/Vibrator;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    .line 41
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;->exitHandler:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

    .line 42
    iput-object p3, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;->previewOverlayPresenter:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayPresenter;

    .line 43
    iput-object p4, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;->vibrator:Landroid/os/Vibrator;

    .line 44
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;->isUserNotificationNeeded:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 45
    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/libraries/commerce/ocr/OcrException;)V
    .locals 4

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;->isUserNotificationNeeded:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 71
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/OcrException;->getType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 72
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;->exitHandler:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

    const/16 v1, 0x2714

    const/4 v2, 0x0

    sget-object v3, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;->exit(ILcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;Landroid/os/Bundle;)V

    .line 76
    :cond_0
    return-void
.end method

.method public onOcrAttempt()Z
    .locals 4

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;->stopPreview()V

    .line 50
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;->isUserNotificationNeeded:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;->vibrator:Landroid/os/Vibrator;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    .line 53
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onRecognized(Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;)V
    .locals 1

    .prologue
    .line 58
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;->onRecognized(Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;Landroid/os/Bundle;)V

    .line 59
    return-void
.end method

.method public onRecognized(Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;->isUserNotificationNeeded:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 64
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;-><init>(Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;)V

    .line 65
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;->exitHandler:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0, p2}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;->exit(ILcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;Landroid/os/Bundle;)V

    .line 66
    return-void
.end method

.method public bridge synthetic onRecognized(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;->onRecognized(Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;)V

    return-void
.end method

.method public bridge synthetic onRecognized(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;

    check-cast p2, Landroid/os/Bundle;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;->onRecognized(Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;Landroid/os/Bundle;)V

    return-void
.end method

.method public onUnrecognized()V
    .locals 1

    .prologue
    .line 80
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;->onUnrecognized(Landroid/os/Bundle;)V

    .line 81
    return-void
.end method

.method public onUnrecognized(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;->isUserNotificationNeeded:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 86
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;->restartPreview()V

    .line 87
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;->previewOverlayPresenter:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayPresenter;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayPresenter;->renderInvalidCardMessage()V

    .line 88
    return-void
.end method

.method public bridge synthetic onUnrecognized(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Landroid/os/Bundle;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrResponseHandler;->onUnrecognized(Landroid/os/Bundle;)V

    return-void
.end method
