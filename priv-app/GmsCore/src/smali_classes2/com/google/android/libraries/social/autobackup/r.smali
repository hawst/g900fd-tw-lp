.class public final Lcom/google/android/libraries/social/autobackup/r;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Landroid/net/Uri;

.field private static final b:[Ljava/lang/String;

.field private static c:Lcom/google/android/libraries/social/autobackup/r;


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/libraries/social/autobackup/au;

.field private final f:Lcom/google/android/libraries/social/autobackup/b/a;

.field private final g:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

.field private final h:Ljava/util/Set;

.field private final i:Lcom/google/android/libraries/social/account/b;

.field private final j:Lcom/google/android/libraries/social/autobackup/an;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 56
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v3

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->a:Landroid/net/Uri;

    aput-object v1, v0, v5

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->b:Landroid/net/Uri;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/libraries/social/autobackup/r;->a:[Landroid/net/Uri;

    .line 63
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "mime_type"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/libraries/social/autobackup/r;->b:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/r;->d:Landroid/content/Context;

    .line 80
    const-class v0, Lcom/google/android/libraries/social/autobackup/au;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/au;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/r;->e:Lcom/google/android/libraries/social/autobackup/au;

    .line 81
    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/b/a;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/r;->f:Lcom/google/android/libraries/social/autobackup/b/a;

    .line 82
    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/r;->g:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    .line 83
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/r;->h:Ljava/util/Set;

    .line 84
    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/r;->i:Lcom/google/android/libraries/social/account/b;

    .line 85
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/r;->d:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/autobackup/an;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/an;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/r;->j:Lcom/google/android/libraries/social/autobackup/an;

    .line 86
    return-void
.end method

.method private a(ILjava/util/List;Ljava/util/List;)I
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 272
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/r;->i:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v1, p1}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v1

    .line 275
    new-instance v4, Lcom/google/android/libraries/social/r/a;

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/r;->d:Landroid/content/Context;

    const-string v3, "gaia_id"

    invoke-interface {v1, v3}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v2, p1, v1, p2}, Lcom/google/android/libraries/social/r/a;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;)V

    .line 277
    invoke-virtual {v4}, Lcom/google/android/libraries/social/r/a;->a()V

    .line 279
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/r;->e:Lcom/google/android/libraries/social/autobackup/au;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/au;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 280
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v6

    move v3, v0

    move v2, v0

    :goto_0
    if-ge v3, v6, :cond_1

    .line 281
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 282
    invoke-interface {p3, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 283
    invoke-virtual {v4, v0}, Lcom/google/android/libraries/social/r/a;->a(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 285
    const-wide/16 v8, 0x0

    iput-wide v8, v1, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->id:J

    .line 286
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(Ljava/lang/String;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 287
    invoke-virtual {v1, p1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->e(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 288
    const/16 v0, 0x190

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    const/16 v7, 0x22

    invoke-virtual {v0, v7}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 290
    sget-object v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-virtual {v0, v5, v1}, Lcom/google/android/libraries/social/g/a/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/libraries/social/g/a/a;)J

    .line 291
    add-int/lit8 v0, v2, 0x1

    .line 293
    const-string v2, "iu.FingerprintManager"

    const/4 v7, 0x4

    invoke-static {v2, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 294
    const-string v2, "iu.FingerprintManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "+++ Found previously uploaded media: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    :cond_0
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto :goto_0

    .line 298
    :cond_1
    return v2

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/r;
    .locals 2

    .prologue
    .line 89
    const-class v1, Lcom/google/android/libraries/social/autobackup/r;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/r;->c:Lcom/google/android/libraries/social/autobackup/r;

    if-nez v0, :cond_0

    .line 90
    new-instance v0, Lcom/google/android/libraries/social/autobackup/r;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/social/autobackup/r;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/r;->c:Lcom/google/android/libraries/social/autobackup/r;

    .line 92
    :cond_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/r;->c:Lcom/google/android/libraries/social/autobackup/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private c()V
    .locals 12

    .prologue
    const/4 v3, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 205
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/r;->i:Lcom/google/android/libraries/social/account/b;

    new-array v1, v1, [I

    aput v3, v1, v2

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/b;->a([I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 207
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/r;->g:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    iget-boolean v0, v0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a:Z

    if-nez v0, :cond_2

    .line 217
    :cond_1
    return-void

    .line 209
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/r;->j:Lcom/google/android/libraries/social/autobackup/an;

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/r;->d:Landroid/content/Context;

    invoke-static {v0, v4}, Lcom/google/android/libraries/social/autobackup/an;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/r;->e:Lcom/google/android/libraries/social/autobackup/au;

    invoke-static {v0, v4}, Lcom/google/android/libraries/social/autobackup/ab;->d(Lcom/google/android/libraries/social/autobackup/au;I)Landroid/database/Cursor;

    move-result-object v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Lcom/google/android/libraries/social/account/e; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v2

    :cond_3
    :goto_1
    :try_start_1
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {v5}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(Landroid/database/Cursor;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->l()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    iget-object v9, p0, Lcom/google/android/libraries/social/autobackup/r;->d:Landroid/content/Context;

    invoke-virtual {v8}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/libraries/social/mediaupload/ae;->a(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "file://"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v9, v1}, Lcom/google/android/libraries/social/mediaupload/ae;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_4
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v9, p0, Lcom/google/android/libraries/social/autobackup/r;->f:Lcom/google/android/libraries/social/autobackup/b/a;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Lcom/google/android/libraries/social/autobackup/b/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_5
    if-eqz v1, :cond_3

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    const/16 v8, 0x64

    if-ne v1, v8, :cond_3

    invoke-direct {p0, v4, v6, v7}, Lcom/google/android/libraries/social/autobackup/r;->a(ILjava/util/List;Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    invoke-interface {v6}, Ljava/util/List;->clear()V

    invoke-interface {v7}, Ljava/util/List;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catch Lcom/google/android/libraries/social/account/e; {:try_start_2 .. :try_end_2} :catch_0

    .line 214
    :catch_0
    move-exception v0

    const-string v0, "iu.FingerprintManager"

    const-string v1, "Account was removed."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 210
    :cond_6
    :try_start_3
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_7

    invoke-direct {p0, v4, v6, v7}, Lcom/google/android/libraries/social/autobackup/r;->a(ILjava/util/List;Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    const-string v1, "iu.FingerprintManager"

    const/4 v5, 0x4

    invoke-static {v1, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "iu.FingerprintManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Synced photo uploads, account="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", matched photos="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    if-lez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/r;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/social/autobackup/aj;->b:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 211
    :cond_9
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/r;->j:Lcom/google/android/libraries/social/autobackup/an;

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/r;->d:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, v4, v1}, Lcom/google/android/libraries/social/autobackup/an;->a(Landroid/content/Context;IZ)V
    :try_end_3
    .catch Lcom/google/android/libraries/social/account/e; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()I
    .locals 21

    .prologue
    .line 106
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/r;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 107
    const/4 v10, 0x0

    .line 108
    const/4 v9, 0x0

    .line 109
    const/4 v8, 0x0

    .line 111
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 113
    const-string v3, "iu.FingerprintManager"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 114
    const-string v3, "iu.FingerprintManager"

    const-string v4, "Start processing all media"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/autobackup/r;->f:Lcom/google/android/libraries/social/autobackup/b/a;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/autobackup/b/a;->a()Ljava/util/Set;

    move-result-object v14

    .line 120
    sget-object v15, Lcom/google/android/libraries/social/autobackup/r;->a:[Landroid/net/Uri;

    array-length v0, v15

    move/from16 v16, v0

    const/4 v3, 0x0

    move v11, v3

    :goto_0
    move/from16 v0, v16

    if-ge v11, v0, :cond_8

    aget-object v3, v15, v11

    .line 121
    const-string v4, "iu.FingerprintManager"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 122
    const-string v4, "iu.FingerprintManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Start processing media store URI: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :cond_1
    sget-object v4, Lcom/google/android/libraries/social/autobackup/r;->b:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v7

    .line 127
    if-eqz v7, :cond_c

    move v4, v8

    move v5, v9

    move v6, v10

    .line 128
    :goto_1
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_7

    .line 133
    const/4 v8, 0x0

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 134
    invoke-static {v3, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    .line 137
    const-string v9, "iu.FingerprintManager"

    const/4 v10, 0x2

    invoke-static {v9, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 138
    const-string v9, "iu.FingerprintManager"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v17, "processing media: "

    move-object/from16 v0, v17

    invoke-direct {v10, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    :cond_2
    const/4 v9, 0x1

    invoke-interface {v7, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 142
    invoke-static {v9}, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->a(Ljava/lang/String;)Z

    move-result v10

    .line 144
    if-eqz v10, :cond_5

    invoke-interface {v14, v8}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_5

    .line 146
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/libraries/social/autobackup/r;->f:Lcom/google/android/libraries/social/autobackup/b/a;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v8, v1}, Lcom/google/android/libraries/social/autobackup/b/a;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v17

    .line 150
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_3

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 151
    :cond_3
    add-int/lit8 v4, v4, 0x1

    .line 152
    const-string v18, "iu.FingerprintManager"

    const/16 v19, 0x4

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 153
    const-string v18, "iu.FingerprintManager"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "Not inserting fingerprint into all photos because has empty content uri or fingerprint. uri: "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " fingerprint: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :cond_4
    add-int/lit8 v5, v5, 0x1

    .line 160
    const-string v17, "iu.FingerprintManager"

    const/16 v18, 0x3

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 161
    const-string v17, "iu.FingerprintManager"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "generated fingerprint for: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    :cond_5
    if-nez v10, :cond_6

    const-string v10, "iu.FingerprintManager"

    const/16 v17, 0x3

    move/from16 v0, v17

    invoke-static {v10, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 165
    const-string v10, "iu.FingerprintManager"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "non media mime type; media: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v17, ", type: "

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v10, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 167
    :cond_6
    add-int/lit8 v6, v6, 0x1

    .line 168
    goto/16 :goto_1

    .line 170
    :cond_7
    :try_start_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move v3, v4

    move v4, v5

    move v5, v6

    .line 120
    :goto_2
    add-int/lit8 v6, v11, 0x1

    move v11, v6

    move v8, v3

    move v9, v4

    move v10, v5

    goto/16 :goto_0

    .line 170
    :catchall_0
    move-exception v2

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 106
    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2

    .line 176
    :cond_8
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/r;->f:Lcom/google/android/libraries/social/autobackup/b/a;

    invoke-virtual {v2, v14}, Lcom/google/android/libraries/social/autobackup/b/a;->a(Ljava/util/Collection;)V

    .line 177
    invoke-interface {v14}, Ljava/util/Set;->size()I

    move-result v2

    add-int/lit8 v3, v2, 0x0

    .line 179
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/r;->h:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_3

    .line 183
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/autobackup/r;->d:Landroid/content/Context;

    const-class v4, Lcom/google/android/libraries/social/autobackup/ac;

    invoke-static {v2, v4}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/social/autobackup/ac;

    invoke-interface {v2}, Lcom/google/android/libraries/social/autobackup/ac;->b()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 184
    invoke-direct/range {p0 .. p0}, Lcom/google/android/libraries/social/autobackup/r;->c()V

    .line 187
    :cond_a
    const-string v2, "iu.FingerprintManager"

    const/4 v4, 0x4

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v2, "iu.FingerprintManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Finished generating fingerprints; "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v12

    const-string v5, "%.3f seconds"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    long-to-double v6, v6

    const-wide v14, 0x408f400000000000L    # 1000.0

    div-double/2addr v6, v14

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v11, v12

    invoke-static {v5, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "iu.FingerprintManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "  numSeen="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " numGenerated="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " numDeleted="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " numFailed="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 189
    :cond_b
    monitor-exit p0

    return v10

    :cond_c
    move v3, v8

    move v4, v9

    move v5, v10

    goto/16 :goto_2
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 196
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/r;->i:Lcom/google/android/libraries/social/account/b;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x4

    aput v2, v1, v3

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/b;->a([I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 197
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/r;->j:Lcom/google/android/libraries/social/autobackup/an;

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/r;->d:Landroid/content/Context;

    invoke-static {v2, v0, v3}, Lcom/google/android/libraries/social/autobackup/an;->a(Landroid/content/Context;IZ)V

    goto :goto_0

    .line 199
    :cond_0
    return-void
.end method
