.class public Lcom/google/android/libraries/social/networkcapability/impl/NetworkCapabilityModule;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/a/f;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/libraries/social/a/a;)V
    .locals 2

    .prologue
    .line 22
    const-class v0, Lcom/google/android/libraries/social/networkcapability/a;

    if-ne p2, v0, :cond_1

    .line 23
    const-class v0, Lcom/google/android/libraries/social/networkcapability/a;

    new-instance v1, Lcom/google/android/libraries/social/networkcapability/impl/b;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/social/networkcapability/impl/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    .line 29
    :cond_0
    :goto_0
    return-void

    .line 24
    :cond_1
    const-class v0, Lcom/google/android/libraries/social/networkcapability/impl/d;

    if-ne p2, v0, :cond_2

    .line 25
    const-class v0, Lcom/google/android/libraries/social/networkcapability/impl/d;

    new-instance v1, Lcom/google/android/libraries/social/networkcapability/impl/d;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/social/networkcapability/impl/d;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto :goto_0

    .line 26
    :cond_2
    const-class v0, Lcom/google/android/libraries/social/o/b;

    if-ne p2, v0, :cond_0

    .line 27
    const-class v0, Lcom/google/android/libraries/social/o/b;

    const-class v1, Lcom/google/android/libraries/social/networkcapability/impl/d;

    invoke-virtual {p3, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0
.end method
