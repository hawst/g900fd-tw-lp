.class public final Lcom/google/android/libraries/social/i/a/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/lang/reflect/Method;


# direct methods
.method private static a()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Could not access method FragmentManager#noteStateNotSaved"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Landroid/support/v4/app/v;)V
    .locals 3

    .prologue
    .line 22
    sget-object v0, Lcom/google/android/libraries/social/i/a/d;->a:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 24
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "noteStateNotSaved"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/i/a/d;->a:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    :cond_0
    :goto_0
    :try_start_1
    sget-object v0, Lcom/google/android/libraries/social/i/a/d;->a:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_2

    .line 36
    :goto_1
    return-void

    .line 26
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/google/android/libraries/social/i/a/d;->a()V

    goto :goto_0

    .line 32
    :catch_1
    move-exception v0

    invoke-static {}, Lcom/google/android/libraries/social/i/a/d;->a()V

    goto :goto_1

    .line 34
    :catch_2
    move-exception v0

    invoke-static {}, Lcom/google/android/libraries/social/i/a/d;->a()V

    goto :goto_1
.end method
