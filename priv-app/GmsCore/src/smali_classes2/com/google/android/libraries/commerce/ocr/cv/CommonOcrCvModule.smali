.class public Lcom/google/android/libraries/commerce/ocr/cv/CommonOcrCvModule;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final POOL_CAPACITY:I = 0x5


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public provideOcrImageResourcePool()Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;
    .locals 3

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;

    const/4 v1, 0x5

    new-instance v2, Lcom/google/android/libraries/commerce/ocr/cv/a;

    invoke-direct {v2, p0}, Lcom/google/android/libraries/commerce/ocr/cv/a;-><init>(Lcom/google/android/libraries/commerce/ocr/cv/CommonOcrCvModule;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;-><init>(ILcom/google/android/libraries/commerce/ocr/util/Provider;)V

    return-object v0
.end method
