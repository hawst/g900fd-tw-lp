.class public final Lcom/google/android/libraries/social/mediaupload/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/os/Bundle;

.field private static final b:Ljava/util/Set;

.field private static final c:Ljava/util/regex/Pattern;


# instance fields
.field private d:Lcom/google/android/libraries/social/mediaupload/z;

.field private e:Ljava/lang/String;

.field private final f:Landroid/content/Context;

.field private final g:Lcom/google/android/libraries/social/rpc/a/d;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Lcom/google/android/libraries/social/mediaupload/p;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 454
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "uploadType"

    const-string v2, "resumable"

    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/social/mediaupload/c;->a:Landroid/os/Bundle;

    .line 462
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 464
    sput-object v0, Lcom/google/android/libraries/social/mediaupload/c;->b:Ljava/util/Set;

    const-string v1, "application/placeholder-image"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 465
    sget-object v0, Lcom/google/android/libraries/social/mediaupload/c;->b:Ljava/util/Set;

    const-string v1, "application/stitching-preview"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 469
    const-string v0, "bytes=(\\d+)-(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/mediaupload/c;->c:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/google/android/libraries/social/mediaupload/p;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 490
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491
    iput-object p1, p0, Lcom/google/android/libraries/social/mediaupload/c;->f:Landroid/content/Context;

    .line 492
    iput-object p3, p0, Lcom/google/android/libraries/social/mediaupload/c;->j:Lcom/google/android/libraries/social/mediaupload/p;

    .line 493
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 494
    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    .line 496
    invoke-interface {v0, p2}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v0

    .line 497
    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/social/mediaupload/c;->h:Ljava/lang/String;

    .line 498
    const-string v1, "effective_gaia_id"

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/social/mediaupload/c;->i:Ljava/lang/String;

    .line 499
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 501
    new-instance v1, Lcom/google/android/libraries/social/rpc/a/d;

    const-string v2, "oauth2:https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.stream.read https://www.googleapis.com/auth/plus.stream.write https://www.googleapis.com/auth/plus.circles.write https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/plus.photos.readwrite https://www.googleapis.com/auth/plus.native"

    invoke-direct {v1, p1, v0, v2}, Lcom/google/android/libraries/social/rpc/a/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/libraries/social/mediaupload/c;->g:Lcom/google/android/libraries/social/rpc/a/d;

    .line 508
    :goto_0
    return-void

    .line 504
    :cond_0
    iput-object v1, p0, Lcom/google/android/libraries/social/mediaupload/c;->h:Ljava/lang/String;

    .line 505
    iput-object v1, p0, Lcom/google/android/libraries/social/mediaupload/c;->i:Ljava/lang/String;

    .line 506
    iput-object v1, p0, Lcom/google/android/libraries/social/mediaupload/c;->g:Lcom/google/android/libraries/social/rpc/a/d;

    goto :goto_0
.end method

.method private static a(Lcom/google/android/libraries/social/mediaupload/r;Lcom/google/android/libraries/social/mediaupload/q;)Lcom/google/android/libraries/social/mediaupload/k;
    .locals 12

    .prologue
    const-wide/32 v6, 0x100000

    const-wide/16 v2, -0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 866
    if-nez p0, :cond_0

    new-instance v0, Lcom/google/android/libraries/social/mediaupload/o;

    const-string v1, "null HttpEntity in response"

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/mediaupload/o;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v0, "MediaUploader"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/libraries/social/mediaupload/r;->e:J

    long-to-int v0, v0

    const-string v1, "MediaUploader"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "parseResult: length: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/r;->f:[B

    array-length v1, v0

    invoke-static {v0, v9, v1}, Lcom/google/protobuf/nano/a;->a([BII)Lcom/google/protobuf/nano/a;

    move-result-object v0

    new-instance v1, Lcom/google/c/e/b/a/a/r;

    invoke-direct {v1}, Lcom/google/c/e/b/a/a/r;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/c/e/b/a/a/r;->a(Lcom/google/protobuf/nano/a;)Lcom/google/c/e/b/a/a/r;

    iget-object v10, v1, Lcom/google/c/e/b/a/a/r;->a:Lcom/google/c/e/b/d/a/f;

    .line 867
    if-nez v10, :cond_2

    .line 868
    new-instance v0, Lcom/google/android/libraries/social/mediaupload/o;

    const-string v1, "Unable to parse UploadMediaResponse"

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/mediaupload/o;-><init>(Ljava/lang/String;)V

    throw v0

    .line 870
    :cond_2
    if-eqz v10, :cond_3

    iget-object v0, v10, Lcom/google/c/e/b/d/a/f;->c:Lcom/google/c/f/a/a/g;

    if-nez v0, :cond_5

    :cond_3
    move-object v1, v8

    .line 871
    :goto_0
    iget-object v6, v10, Lcom/google/c/e/b/d/a/f;->a:Lcom/google/c/f/a/a/e;

    .line 872
    iget-object v0, v6, Lcom/google/c/f/a/a/e;->a:Lcom/google/c/f/a/a/d;

    if-eqz v0, :cond_7

    iget-object v0, v6, Lcom/google/c/f/a/a/e;->a:Lcom/google/c/f/a/a/d;

    iget-object v2, v0, Lcom/google/c/f/a/a/d;->a:Ljava/lang/String;

    .line 873
    :goto_1
    iget-object v0, v6, Lcom/google/c/f/a/a/e;->c:Lcom/google/c/f/a/a/c;

    if-eqz v0, :cond_4

    iget-object v0, v6, Lcom/google/c/f/a/a/e;->c:Lcom/google/c/f/a/a/c;

    iget-object v8, v0, Lcom/google/c/f/a/a/c;->b:Ljava/lang/String;

    .line 874
    :cond_4
    iget-object v0, v6, Lcom/google/c/f/a/a/e;->d:Ljava/lang/Double;

    if-nez v0, :cond_8

    const-wide/16 v4, 0x0

    :goto_2
    const-wide v10, 0x408f400000000000L    # 1000.0

    mul-double/2addr v4, v10

    double-to-long v3, v4

    .line 875
    new-instance v0, Lcom/google/android/libraries/social/mediaupload/k;

    iget-object v5, v6, Lcom/google/c/f/a/a/e;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/libraries/social/mediaupload/q;->k()J

    move-result-wide v6

    invoke-direct/range {v0 .. v9}, Lcom/google/android/libraries/social/mediaupload/k;-><init>(Lcom/google/android/libraries/social/mediaupload/w;Ljava/lang/String;JLjava/lang/String;JLjava/lang/String;B)V

    return-object v0

    .line 870
    :cond_5
    iget-object v4, v10, Lcom/google/c/e/b/d/a/f;->c:Lcom/google/c/f/a/a/g;

    if-eqz v4, :cond_a

    iget-object v0, v4, Lcom/google/c/f/a/a/g;->b:Ljava/lang/Long;

    if-eqz v0, :cond_9

    iget-object v0, v4, Lcom/google/c/f/a/a/g;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    div-long/2addr v0, v6

    :goto_3
    iget-object v5, v4, Lcom/google/c/f/a/a/g;->a:Ljava/lang/Long;

    if-eqz v5, :cond_6

    iget-object v2, v4, Lcom/google/c/f/a/a/g;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    div-long/2addr v2, v6

    :cond_6
    iget-object v5, v4, Lcom/google/c/f/a/a/g;->d:Ljava/lang/Boolean;

    invoke-static {v5}, Lcom/google/android/libraries/b/a/g;->a(Ljava/lang/Boolean;)Z

    move-result v6

    iget-object v4, v4, Lcom/google/c/f/a/a/g;->c:Ljava/lang/Boolean;

    invoke-static {v4}, Lcom/google/android/libraries/b/a/g;->a(Ljava/lang/Boolean;)Z

    move-result v7

    move-wide v4, v2

    move-wide v2, v0

    :goto_4
    new-instance v1, Lcom/google/android/libraries/social/mediaupload/w;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/libraries/social/mediaupload/w;-><init>(JJZZ)V

    goto :goto_0

    :cond_7
    move-object v2, v8

    .line 872
    goto :goto_1

    .line 874
    :cond_8
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    goto :goto_2

    :cond_9
    move-wide v0, v2

    goto :goto_3

    :cond_a
    move v7, v9

    move v6, v9

    move-wide v4, v2

    goto :goto_4
.end method

.method private a(Ljava/lang/String;Lcom/google/android/libraries/social/mediaupload/q;Ljava/lang/String;ZJ)Lcom/google/android/libraries/social/mediaupload/k;
    .locals 19

    .prologue
    .line 789
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/social/mediaupload/c;->j:Lcom/google/android/libraries/social/mediaupload/p;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/libraries/social/mediaupload/q;->g()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/libraries/social/mediaupload/q;->k()J

    move-result-wide v10

    move-wide/from16 v8, p5

    invoke-interface/range {v6 .. v11}, Lcom/google/android/libraries/social/mediaupload/p;->a(Landroid/net/Uri;JJ)V

    .line 792
    const-string v6, "MediaUploader"

    const/4 v7, 0x4

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 793
    const-string v6, "MediaUploader"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "--- UPLOAD task: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 796
    :cond_0
    move-object/from16 v0, p2

    move-wide/from16 v1, p5

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/mediaupload/q;->a(J)Ljava/io/InputStream;

    move-result-object v16

    .line 800
    if-eqz p3, :cond_2

    .line 801
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/libraries/social/mediaupload/q;->h()Lcom/google/android/libraries/social/m/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/libraries/social/m/a;->a()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    move-object/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    :cond_1
    new-instance v6, Lcom/google/android/libraries/social/mediaupload/h;

    const-string v7, "Fingerprint mismatch"

    invoke-direct {v6, v7}, Lcom/google/android/libraries/social/mediaupload/h;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 855
    :catch_0
    move-exception v6

    .line 856
    :try_start_1
    new-instance v7, Lcom/google/android/libraries/social/mediaupload/l;

    invoke-virtual {v6}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/mediaupload/m;->a(Lcom/google/android/libraries/social/mediaupload/q;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v6, v8}, Lcom/google/android/libraries/social/mediaupload/l;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 859
    :catchall_0
    move-exception v6

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/libraries/social/mediaupload/c;->d:Lcom/google/android/libraries/social/mediaupload/z;

    .line 860
    invoke-static/range {v16 .. v16}, Lcom/google/android/libraries/b/a/a;->a(Ljava/io/Closeable;)V

    throw v6

    .line 804
    :cond_2
    :try_start_2
    new-instance v18, Lcom/google/android/libraries/social/mediaupload/d;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-wide/from16 v2, p5

    move-object/from16 v4, p2

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/social/mediaupload/d;-><init>(Lcom/google/android/libraries/social/mediaupload/c;JLcom/google/android/libraries/social/mediaupload/q;)V

    .line 815
    new-instance v7, Lcom/google/android/libraries/social/mediaupload/z;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/libraries/social/mediaupload/c;->f:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/libraries/social/mediaupload/c;->g:Lcom/google/android/libraries/social/rpc/a/d;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/libraries/social/mediaupload/q;->e()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/libraries/social/mediaupload/q;->k()J

    move-result-wide v14

    move-object/from16 v10, p1

    move-wide/from16 v12, p5

    move/from16 v17, p4

    invoke-direct/range {v7 .. v18}, Lcom/google/android/libraries/social/mediaupload/z;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/rpc/l;Ljava/lang/String;Ljava/lang/String;JJLjava/io/InputStream;ZLcom/google/android/libraries/social/mediaupload/ad;)V

    .line 819
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/libraries/social/mediaupload/c;->d:Lcom/google/android/libraries/social/mediaupload/z;

    .line 821
    invoke-static {v7}, Lcom/google/android/libraries/social/mediaupload/c;->a(Lcom/google/android/libraries/social/mediaupload/r;)Lcom/google/android/libraries/social/mediaupload/r;

    .line 823
    iget v6, v7, Lcom/google/android/libraries/social/mediaupload/r;->d:I

    .line 824
    invoke-static {v6}, Lcom/google/android/libraries/social/mediaupload/c;->a(I)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 826
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/libraries/social/mediaupload/q;->k()J

    move-result-wide v8

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/libraries/social/mediaupload/q;->k()J

    move-result-wide v10

    move-object/from16 v0, v18

    invoke-interface {v0, v8, v9, v10, v11}, Lcom/google/android/libraries/social/mediaupload/ad;->a(JJ)V

    .line 828
    move-object/from16 v0, p2

    invoke-static {v7, v0}, Lcom/google/android/libraries/social/mediaupload/c;->a(Lcom/google/android/libraries/social/mediaupload/r;Lcom/google/android/libraries/social/mediaupload/q;)Lcom/google/android/libraries/social/mediaupload/k;

    move-result-object v6

    .line 831
    const-string v7, "MediaUploader"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 832
    const-string v7, "MediaUploader"

    const-string v8, "UPLOAD_SUCCESS"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 834
    :cond_3
    invoke-static {}, Lcom/google/android/libraries/social/mediaupload/t;->a()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 859
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/libraries/social/mediaupload/c;->d:Lcom/google/android/libraries/social/mediaupload/z;

    .line 860
    invoke-static/range {v16 .. v16}, Lcom/google/android/libraries/b/a/a;->a(Ljava/io/Closeable;)V

    return-object v6

    .line 836
    :cond_4
    :try_start_3
    invoke-static {v6}, Lcom/google/android/libraries/social/mediaupload/c;->b(I)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 837
    new-instance v6, Lcom/google/android/libraries/social/mediaupload/o;

    const-string v7, "uploaded full stream but server returned incomplete"

    invoke-direct {v6, v7}, Lcom/google/android/libraries/social/mediaupload/o;-><init>(Ljava/lang/String;)V

    throw v6

    .line 838
    :cond_5
    const/16 v8, 0x190

    if-ne v6, v8, :cond_6

    .line 842
    new-instance v7, Lcom/google/android/libraries/social/mediaupload/o;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "upload failed (bad payload, file too large) "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v7, v6}, Lcom/google/android/libraries/social/mediaupload/o;-><init>(Ljava/lang/String;)V

    throw v7

    .line 843
    :cond_6
    const/16 v8, 0x1f4

    if-lt v6, v8, :cond_7

    const/16 v8, 0x258

    if-ge v6, v8, :cond_7

    .line 846
    new-instance v7, Lcom/google/android/libraries/social/mediaupload/l;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "upload transient error"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/mediaupload/m;->a(Lcom/google/android/libraries/social/mediaupload/q;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v6, v8}, Lcom/google/android/libraries/social/mediaupload/l;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 849
    :cond_7
    iget-object v8, v7, Lcom/google/android/libraries/social/mediaupload/r;->g:Ljava/io/IOException;

    if-nez v8, :cond_8

    iget-boolean v8, v7, Lcom/google/android/libraries/social/mediaupload/r;->h:Z

    if-eqz v8, :cond_9

    .line 850
    :cond_8
    new-instance v6, Lcom/google/android/libraries/social/mediaupload/l;

    iget-object v7, v7, Lcom/google/android/libraries/social/mediaupload/r;->g:Ljava/io/IOException;

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/mediaupload/m;->a(Lcom/google/android/libraries/social/mediaupload/q;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/google/android/libraries/social/mediaupload/l;-><init>(Ljava/lang/Exception;Ljava/lang/String;)V

    throw v6

    .line 853
    :cond_9
    new-instance v7, Lcom/google/android/libraries/social/mediaupload/o;

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v7, v6}, Lcom/google/android/libraries/social/mediaupload/o;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method static synthetic a(Lcom/google/android/libraries/social/mediaupload/c;)Lcom/google/android/libraries/social/mediaupload/p;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/c;->j:Lcom/google/android/libraries/social/mediaupload/p;

    return-object v0
.end method

.method private static a(Lcom/google/android/libraries/social/mediaupload/r;)Lcom/google/android/libraries/social/mediaupload/r;
    .locals 4

    .prologue
    .line 756
    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediaupload/r;->a()V

    .line 757
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 758
    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediaupload/r;->c()V

    .line 759
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long v0, v2, v0

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/mediaupload/t;->a(J)V

    .line 762
    iget v0, p0, Lcom/google/android/libraries/social/mediaupload/r;->d:I

    .line 763
    const/16 v1, 0x191

    if-eq v0, v1, :cond_0

    const/16 v1, 0x193

    if-ne v0, v1, :cond_2

    .line 766
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/r;->c:Lcom/google/android/libraries/social/rpc/l;

    invoke-interface {v0}, Lcom/google/android/libraries/social/rpc/l;->a()V

    .line 767
    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediaupload/r;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 775
    const-string v0, "MediaUploader"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 776
    const-string v0, "MediaUploader"

    const-string v1, "executeWithAuthRetry: attempt #2"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 778
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 779
    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediaupload/r;->c()V

    .line 780
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long v0, v2, v0

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/mediaupload/t;->a(J)V

    .line 782
    :cond_2
    return-object p0

    .line 768
    :catch_0
    move-exception v0

    .line 769
    const-string v1, "MediaUploader"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 770
    const-string v1, "MediaUploader"

    const-string v2, "authentication failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 772
    :cond_3
    new-instance v1, Lcom/google/android/libraries/social/mediaupload/n;

    invoke-direct {v1, v0}, Lcom/google/android/libraries/social/mediaupload/n;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static synthetic a(Landroid/database/Cursor;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 52
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/libraries/social/mediaupload/f;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No content for URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/mediaupload/f;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private static a(I)Z
    .locals 1

    .prologue
    .line 915
    const/16 v0, 0xc8

    if-eq p0, v0, :cond_0

    const/16 v0, 0xc9

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(I)Z
    .locals 1

    .prologue
    .line 919
    const/16 v0, 0x134

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/mediaupload/k;
    .locals 9

    .prologue
    const/16 v6, 0x191

    .line 620
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    new-instance v8, Lcom/google/android/libraries/social/mediaupload/m;

    const-string v1, "resumeUrl"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "resumeFingerprint"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "resumeForceResize"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    const-string v4, "resumeContentType"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v1, v2, v3, v0}, Lcom/google/android/libraries/social/mediaupload/m;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 624
    new-instance v0, Lcom/google/android/libraries/social/mediaupload/q;

    iget-object v1, p0, Lcom/google/android/libraries/social/mediaupload/c;->f:Landroid/content/Context;

    iget-object v4, v8, Lcom/google/android/libraries/social/mediaupload/m;->d:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/social/mediaupload/q;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lcom/google/z/a/a/a/b;)V

    .line 627
    iget-boolean v1, v8, Lcom/google/android/libraries/social/mediaupload/m;->c:Z

    if-eqz v1, :cond_0

    .line 628
    invoke-virtual {v0}, Lcom/google/android/libraries/social/mediaupload/q;->a()V

    .line 630
    :cond_0
    new-instance v1, Lcom/google/android/libraries/social/mediaupload/x;

    iget-object v2, p0, Lcom/google/android/libraries/social/mediaupload/c;->f:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/libraries/social/mediaupload/c;->g:Lcom/google/android/libraries/social/rpc/a/d;

    iget-object v4, v8, Lcom/google/android/libraries/social/mediaupload/m;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/libraries/social/mediaupload/x;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/rpc/l;Ljava/lang/String;)V

    .line 632
    :try_start_1
    invoke-static {v1}, Lcom/google/android/libraries/social/mediaupload/c;->a(Lcom/google/android/libraries/social/mediaupload/r;)Lcom/google/android/libraries/social/mediaupload/r;

    .line 634
    iget v2, v1, Lcom/google/android/libraries/social/mediaupload/r;->d:I

    .line 635
    invoke-static {v2}, Lcom/google/android/libraries/social/mediaupload/c;->a(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 636
    const-string v2, "MediaUploader"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 637
    const-string v2, "MediaUploader"

    const-string v3, "nothing to resume; upload already complete"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 639
    :cond_1
    invoke-static {v1, v0}, Lcom/google/android/libraries/social/mediaupload/c;->a(Lcom/google/android/libraries/social/mediaupload/r;Lcom/google/android/libraries/social/mediaupload/q;)Lcom/google/android/libraries/social/mediaupload/k;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 647
    :goto_0
    return-object v0

    .line 622
    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/libraries/social/mediaupload/l;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed decoding resume token: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/mediaupload/l;-><init>(Ljava/lang/String;)V

    throw v0

    .line 640
    :cond_2
    :try_start_2
    invoke-static {v2}, Lcom/google/android/libraries/social/mediaupload/c;->b(I)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, v1, Lcom/google/android/libraries/social/mediaupload/x;->a:Ljava/lang/String;

    if-eqz v3, :cond_5

    .line 642
    iget-object v1, v1, Lcom/google/android/libraries/social/mediaupload/x;->a:Ljava/lang/String;

    .line 643
    if-eqz v1, :cond_3

    sget-object v2, Lcom/google/android/libraries/social/mediaupload/c;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x1

    add-long v6, v2, v4

    .line 644
    :goto_1
    const-wide/16 v2, 0x0

    cmp-long v2, v6, v2

    if-gez v2, :cond_4

    .line 645
    new-instance v0, Lcom/google/android/libraries/social/mediaupload/l;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "negative range offset: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/mediaupload/l;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 656
    :catch_1
    move-exception v0

    .line 657
    new-instance v1, Lcom/google/android/libraries/social/mediaupload/l;

    invoke-direct {v1, v0, p3}, Lcom/google/android/libraries/social/mediaupload/l;-><init>(Ljava/lang/Exception;Ljava/lang/String;)V

    throw v1

    .line 643
    :cond_3
    const-wide/16 v6, -0x1

    goto :goto_1

    .line 647
    :cond_4
    :try_start_3
    iget-object v2, v8, Lcom/google/android/libraries/social/mediaupload/m;->a:Ljava/lang/String;

    iget-object v4, v8, Lcom/google/android/libraries/social/mediaupload/m;->b:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v3, v0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/libraries/social/mediaupload/c;->a(Ljava/lang/String;Lcom/google/android/libraries/social/mediaupload/q;Ljava/lang/String;ZJ)Lcom/google/android/libraries/social/mediaupload/k;

    move-result-object v0

    goto :goto_0

    .line 649
    :cond_5
    if-ne v2, v6, :cond_6

    .line 652
    new-instance v0, Lcom/google/android/libraries/social/mediaupload/n;

    const/16 v1, 0x191

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/mediaupload/n;-><init>(Ljava/lang/String;)V

    throw v0

    .line 654
    :cond_6
    new-instance v0, Lcom/google/android/libraries/social/mediaupload/l;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unexpected response: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, v1, Lcom/google/android/libraries/social/mediaupload/r;->d:I

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/mediaupload/l;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZ)Lcom/google/android/libraries/social/mediaupload/k;
    .locals 13

    .prologue
    .line 590
    const/4 v6, 0x0

    .line 592
    :try_start_0
    new-instance v0, Lcom/google/android/libraries/social/mediaupload/q;

    iget-object v1, p0, Lcom/google/android/libraries/social/mediaupload/c;->f:Landroid/content/Context;

    const/4 v5, 0x0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/social/mediaupload/q;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lcom/google/z/a/a/a/b;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 593
    if-nez p6, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/google/android/libraries/social/mediaupload/c;->f:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/google/android/libraries/social/mediaupload/ae;->b(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 594
    invoke-virtual {v0}, Lcom/google/android/libraries/social/mediaupload/q;->a()V

    .line 596
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/libraries/social/mediaupload/q;->e()Ljava/lang/String;

    move-result-object v2

    sget-object v1, Lcom/google/android/libraries/social/mediaupload/c;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Lcom/google/android/libraries/social/mediaupload/e;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/libraries/social/mediaupload/e;-><init>(Ljava/lang/String;Z)V

    throw v1
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 598
    :catch_0
    move-exception v1

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    .line 599
    :goto_0
    :try_start_2
    new-instance v2, Lcom/google/android/libraries/social/mediaupload/g;

    invoke-direct {v2, v0}, Lcom/google/android/libraries/social/mediaupload/g;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 603
    :catchall_0
    move-exception v0

    move-object v6, v1

    .line 604
    :goto_1
    if-eqz v6, :cond_1

    :try_start_3
    invoke-virtual {v6}, Lcom/google/android/libraries/social/mediaupload/q;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 605
    new-instance v1, Ljava/io/File;

    invoke-virtual {v6}, Lcom/google/android/libraries/social/mediaupload/q;->c()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 609
    :cond_1
    :goto_2
    throw v0

    .line 596
    :cond_2
    if-eqz v2, :cond_4

    :try_start_4
    const-string v1, "image/"

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "video/"

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    const/4 v1, 0x1

    :goto_3
    if-nez v1, :cond_5

    new-instance v1, Lcom/google/android/libraries/social/mediaupload/e;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/google/android/libraries/social/mediaupload/e;-><init>(Ljava/lang/String;Z)V

    throw v1
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 600
    :catch_1
    move-exception v1

    move-object v6, v0

    move-object v0, v1

    .line 601
    :goto_4
    :try_start_5
    new-instance v1, Lcom/google/android/libraries/social/mediaupload/l;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/android/libraries/social/mediaupload/l;-><init>(Ljava/lang/Exception;Ljava/lang/String;)V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 603
    :catchall_1
    move-exception v0

    goto :goto_1

    .line 596
    :cond_4
    const/4 v1, 0x0

    goto :goto_3

    .line 597
    :cond_5
    :try_start_6
    iget-object v1, p0, Lcom/google/android/libraries/social/mediaupload/c;->e:Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/android/libraries/social/mediaupload/c;->e:Ljava/lang/String;

    :goto_5
    iget-object v2, p0, Lcom/google/android/libraries/social/mediaupload/c;->f:Landroid/content/Context;

    const-string v3, "plusi"

    const/4 v4, 0x1

    sget-object v5, Lcom/google/android/libraries/social/mediaupload/c;->a:Landroid/os/Bundle;

    invoke-static {v2, v3, v1, v4, v5}, Lcom/google/android/libraries/social/rpc/a/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;)Ljava/lang/String;

    move-result-object v4

    new-instance v1, Lcom/google/android/libraries/social/mediaupload/a;

    iget-object v2, p0, Lcom/google/android/libraries/social/mediaupload/c;->f:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/libraries/social/mediaupload/c;->g:Lcom/google/android/libraries/social/rpc/a/d;

    iget-object v10, p0, Lcom/google/android/libraries/social/mediaupload/c;->i:Ljava/lang/String;

    iget-object v11, p0, Lcom/google/android/libraries/social/mediaupload/c;->h:Ljava/lang/String;

    move-object v5, v0

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move/from16 v8, p8

    move/from16 v9, p7

    invoke-direct/range {v1 .. v11}, Lcom/google/android/libraries/social/mediaupload/a;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/rpc/l;Ljava/lang/String;Lcom/google/android/libraries/social/mediaupload/q;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/libraries/social/mediaupload/c;->a(Lcom/google/android/libraries/social/mediaupload/r;)Lcom/google/android/libraries/social/mediaupload/r;

    iget v2, v1, Lcom/google/android/libraries/social/mediaupload/r;->d:I

    invoke-static {v2}, Lcom/google/android/libraries/social/mediaupload/c;->a(I)Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v2, v1, Lcom/google/android/libraries/social/mediaupload/a;->a:Ljava/lang/String;

    const/4 v4, 0x0

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-object v3, v0

    move/from16 v5, p8

    invoke-direct/range {v1 .. v7}, Lcom/google/android/libraries/social/mediaupload/c;->a(Ljava/lang/String;Lcom/google/android/libraries/social/mediaupload/q;Ljava/lang/String;ZJ)Lcom/google/android/libraries/social/mediaupload/k;
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    move-result-object v1

    .line 604
    :try_start_7
    invoke-virtual {v0}, Lcom/google/android/libraries/social/mediaupload/q;->b()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 605
    new-instance v2, Ljava/io/File;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/mediaupload/q;->c()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    .line 609
    :cond_6
    :goto_6
    return-object v1

    .line 597
    :cond_7
    if-eqz p8, :cond_8

    :try_start_8
    const-string v1, "uploadmediabackground"

    goto :goto_5

    :cond_8
    const-string v1, "uploadmedia"

    goto :goto_5

    :cond_9
    const/16 v1, 0x190

    if-ne v2, v1, :cond_a

    new-instance v1, Lcom/google/android/libraries/social/mediaupload/o;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "upload failed (bad payload, file too large) "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/libraries/social/mediaupload/o;-><init>(Ljava/lang/String;)V

    throw v1

    .line 603
    :catchall_2
    move-exception v1

    move-object v6, v0

    move-object v0, v1

    goto/16 :goto_1

    .line 597
    :cond_a
    const/16 v1, 0x191

    if-ne v2, v1, :cond_b

    new-instance v1, Lcom/google/android/libraries/social/mediaupload/n;

    const/16 v2, 0x191

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/libraries/social/mediaupload/n;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_b
    if-eqz v2, :cond_c

    const/16 v1, 0x1f4

    if-lt v2, v1, :cond_e

    const/16 v1, 0x258

    if-ge v2, v1, :cond_e

    :cond_c
    const/16 v1, 0x1f7

    if-ne v2, v1, :cond_d

    new-instance v1, Lcom/google/android/libraries/social/mediaupload/l;

    const-string v2, "Server throttle code 503"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/libraries/social/mediaupload/l;-><init>(Ljava/lang/String;B)V

    :goto_7
    throw v1

    :cond_d
    new-instance v1, Lcom/google/android/libraries/social/mediaupload/l;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "upload transient error:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/libraries/social/mediaupload/l;-><init>(Ljava/lang/String;)V

    goto :goto_7

    :cond_e
    new-instance v1, Lcom/google/android/libraries/social/mediaupload/o;

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/libraries/social/mediaupload/o;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :catch_2
    move-exception v1

    goto/16 :goto_2

    .line 600
    :catch_3
    move-exception v0

    goto/16 :goto_4

    .line 598
    :catch_4
    move-exception v0

    move-object v1, v6

    goto/16 :goto_0

    :catch_5
    move-exception v0

    goto :goto_6
.end method

.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 665
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/c;->d:Lcom/google/android/libraries/social/mediaupload/z;

    if-eqz v0, :cond_0

    .line 666
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/c;->d:Lcom/google/android/libraries/social/mediaupload/z;

    iget-object v0, v0, Lcom/google/android/libraries/social/mediaupload/z;->a:Le/a/a/g;

    invoke-interface {v0}, Le/a/a/g;->c()V

    .line 667
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/mediaupload/c;->d:Lcom/google/android/libraries/social/mediaupload/z;

    .line 669
    const-string v0, "MediaUploader"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 670
    const-string v0, "MediaUploader"

    const-string v1, "Current upload aborted"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 673
    :cond_0
    monitor-exit p0

    return-void

    .line 665
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 685
    iput-object p1, p0, Lcom/google/android/libraries/social/mediaupload/c;->e:Ljava/lang/String;

    .line 686
    return-void
.end method
