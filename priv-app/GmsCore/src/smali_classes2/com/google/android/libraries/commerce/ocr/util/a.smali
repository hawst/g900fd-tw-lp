.class final Lcom/google/android/libraries/commerce/ocr/util/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/RejectedExecutionHandler;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 63
    if-nez p0, :cond_0

    .line 69
    :goto_0
    return-void

    .line 66
    :cond_0
    instance-of v0, p0, Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory$RetirableRunnable;

    if-eqz v0, :cond_1

    .line 67
    check-cast p0, Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory$RetirableRunnable;

    .line 68
    invoke-interface {p0}, Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory$RetirableRunnable;->retire()V

    goto :goto_0

    .line 71
    :cond_1
    new-instance v0, Ljava/util/concurrent/RejectedExecutionException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Must be of type RetirableRunnable, but was: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/RejectedExecutionException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final rejectedExecution(Ljava/lang/Runnable;Ljava/util/concurrent/ThreadPoolExecutor;)V
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p2}, Ljava/util/concurrent/ThreadPoolExecutor;->isShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    invoke-virtual {p2}, Ljava/util/concurrent/ThreadPoolExecutor;->getQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 55
    invoke-virtual {p2, p1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 56
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/util/a;->a(Ljava/lang/Runnable;)V

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_0
    invoke-static {p1}, Lcom/google/android/libraries/commerce/ocr/util/a;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
