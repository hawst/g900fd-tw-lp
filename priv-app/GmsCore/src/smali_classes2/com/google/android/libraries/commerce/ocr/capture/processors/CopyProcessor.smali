.class public Lcom/google/android/libraries/commerce/ocr/capture/processors/CopyProcessor;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;


# static fields
.field private static final TAG:Ljava/lang/String; = "CopyForBackgroundProcessor"


# instance fields
.field private final intervalPolicy:Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;

.field private final ocrImagePool:Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/CopyProcessor;->intervalPolicy:Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;

    .line 22
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/CopyProcessor;->ocrImagePool:Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;

    .line 23
    return-void
.end method


# virtual methods
.method public discard(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method public bridge synthetic discard(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/CopyProcessor;->discard(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)V

    return-void
.end method

.method public isProcessingNeeded(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)Z
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/CopyProcessor;->intervalPolicy:Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;->isOverInterval()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic isProcessingNeeded(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 12
    check-cast p1, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/CopyProcessor;->isProcessingNeeded(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)Z

    move-result v0

    return v0
.end method

.method public process(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;
    .locals 3

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/CopyProcessor;->ocrImagePool:Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->obtainSafePoolable()Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    move-result-object v1

    .line 35
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getData()[B

    move-result-object v2

    array-length v2, v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->init(I)Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->copyFrom(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 43
    :goto_0
    return-object v0

    .line 39
    :catch_0
    move-exception v0

    const-string v0, "CopyForBackgroundProcessor"

    const-string v2, "Ran out of memory for a frame, skipping"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    invoke-virtual {v1}, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->recycle()V

    .line 41
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic process(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    check-cast p1, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/CopyProcessor;->process(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    move-result-object v0

    return-object v0
.end method

.method public shutdown()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/CopyProcessor;->ocrImagePool:Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/cv/ResourcePool;->evict()V

    .line 54
    return-void
.end method
