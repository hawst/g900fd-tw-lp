.class public Lcom/google/android/libraries/social/i/a/b;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field protected final c:Lcom/google/android/libraries/social/i/r;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 21
    new-instance v0, Lcom/google/android/libraries/social/i/r;

    invoke-direct {v0}, Lcom/google/android/libraries/social/i/r;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/i/a/b;->c:Lcom/google/android/libraries/social/i/r;

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/b;->c:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/i/r;->a(Landroid/os/Bundle;)V

    .line 55
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 56
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/b;->c:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/r;->s()V

    .line 105
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 106
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/b;->c:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/i/r;->a(Landroid/app/Activity;)V

    .line 49
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 50
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/b;->c:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/i/r;->c(Landroid/os/Bundle;)V

    .line 30
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 31
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/b;->c:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0, p3}, Lcom/google/android/libraries/social/i/r;->b(Landroid/os/Bundle;)V

    .line 37
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/b;->c:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/r;->c()V

    .line 99
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 100
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/b;->c:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/r;->d()V

    .line 87
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 88
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/b;->c:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/r;->e()V

    .line 117
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 118
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/b;->c:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/r;->b()V

    .line 75
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 76
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/google/android/libraries/social/i/a/b;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/social/i/a/d;->a(Landroid/support/v4/app/v;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/b;->c:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/r;->q()V

    .line 69
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 70
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/b;->c:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/i/r;->d(Landroid/os/Bundle;)V

    .line 93
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 94
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/android/libraries/social/i/a/b;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/social/i/a/d;->a(Landroid/support/v4/app/v;)V

    .line 61
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/b;->c:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/r;->p()V

    .line 62
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 63
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/b;->c:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/r;->r()V

    .line 81
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 82
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/b;->c:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/social/i/r;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 43
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 44
    return-void
.end method

.method public setMenuVisibility(Z)V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/libraries/social/i/a/b;->c:Lcom/google/android/libraries/social/i/r;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/i/r;->a()V

    .line 111
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 112
    return-void
.end method
