.class final Lcom/google/android/libraries/social/autobackup/az;
.super Landroid/os/Handler;
.source "SourceFile"


# direct methods
.method constructor <init>(Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 479
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 480
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 484
    invoke-static {}, Lcom/google/android/libraries/social/autobackup/ax;->c()Lcom/google/android/libraries/social/autobackup/ax;

    move-result-object v0

    if-nez v0, :cond_0

    .line 519
    :goto_0
    return-void

    .line 488
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 522
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown message: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 490
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Landroid/database/Cursor;

    if-eqz v0, :cond_1

    .line 491
    invoke-static {}, Lcom/google/android/libraries/social/autobackup/ax;->c()Lcom/google/android/libraries/social/autobackup/ax;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/ax;->b(Lcom/google/android/libraries/social/autobackup/ax;)Lcom/google/android/libraries/social/autobackup/ap;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/autobackup/ap;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 493
    :cond_1
    invoke-static {}, Lcom/google/android/libraries/social/autobackup/ax;->c()Lcom/google/android/libraries/social/autobackup/ax;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/ax;->b(Lcom/google/android/libraries/social/autobackup/ax;)Lcom/google/android/libraries/social/autobackup/ap;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/ap;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 499
    :pswitch_1
    invoke-static {}, Lcom/google/android/libraries/social/autobackup/ax;->c()Lcom/google/android/libraries/social/autobackup/ax;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/google/android/libraries/social/autobackup/ax;->a(Lcom/google/android/libraries/social/autobackup/ax;I)V

    goto :goto_0

    .line 503
    :pswitch_2
    invoke-static {}, Lcom/google/android/libraries/social/autobackup/ax;->c()Lcom/google/android/libraries/social/autobackup/ax;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/ax;->c(Lcom/google/android/libraries/social/autobackup/ax;)V

    goto :goto_0

    .line 507
    :pswitch_3
    invoke-static {}, Lcom/google/android/libraries/social/autobackup/ax;->c()Lcom/google/android/libraries/social/autobackup/ax;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/google/android/libraries/social/autobackup/ax;->b(Lcom/google/android/libraries/social/autobackup/ax;I)V

    goto :goto_0

    .line 511
    :pswitch_4
    invoke-static {}, Lcom/google/android/libraries/social/autobackup/ax;->c()Lcom/google/android/libraries/social/autobackup/ax;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/ax;->a(Lcom/google/android/libraries/social/autobackup/ax;)V

    goto :goto_0

    .line 515
    :pswitch_5
    const-string v0, "iu.UploadsManager"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 516
    const-string v0, "iu.UploadsManager"

    const-string v1, "Try to reset UploadsManager again!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 518
    :cond_2
    invoke-static {}, Lcom/google/android/libraries/social/autobackup/ax;->c()Lcom/google/android/libraries/social/autobackup/ax;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/ax;->d(Lcom/google/android/libraries/social/autobackup/ax;)V

    goto/16 :goto_0

    .line 488
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
