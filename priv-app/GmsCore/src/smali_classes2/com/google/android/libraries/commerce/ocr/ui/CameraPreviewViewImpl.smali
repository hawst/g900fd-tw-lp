.class public Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;


# instance fields
.field private cameraPreviewSize:Landroid/graphics/Point;

.field private final contentSize:Landroid/graphics/Point;

.field private presenter:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;

.field private previewAreaFinalized:Z

.field private final previewView:Landroid/view/SurfaceView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->previewAreaFinalized:Z

    .line 29
    new-instance v0, Landroid/view/SurfaceView;

    invoke-direct {v0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->previewView:Landroid/view/SurfaceView;

    .line 30
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->contentSize:Landroid/graphics/Point;

    .line 31
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->previewView:Landroid/view/SurfaceView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->addView(Landroid/view/View;)V

    .line 32
    invoke-virtual {p0, p2}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->setPresenter(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;)V

    .line 33
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 49
    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->getChildCount()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "Preview view must have only one child view"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 50
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 51
    sub-int v3, p4, p2

    .line 52
    sub-int v4, p5, p3

    .line 53
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->cameraPreviewSize:Landroid/graphics/Point;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->cameraPreviewSize:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    .line 55
    :goto_1
    iget-object v6, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->cameraPreviewSize:Landroid/graphics/Point;

    if-eqz v6, :cond_0

    iget-object v4, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->cameraPreviewSize:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    .line 61
    :cond_0
    new-instance v6, Landroid/graphics/Rect;

    mul-int/2addr v4, v3

    div-int v0, v4, v0

    invoke-direct {v6, v2, v2, v3, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 62
    iget v0, v6, Landroid/graphics/Rect;->left:I

    iget v2, v6, Landroid/graphics/Rect;->top:I

    iget v3, v6, Landroid/graphics/Rect;->right:I

    iget v4, v6, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v5, v0, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 64
    iget-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->previewAreaFinalized:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->cameraPreviewSize:Landroid/graphics/Point;

    if-eqz v0, :cond_1

    .line 65
    iput-boolean v1, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->previewAreaFinalized:Z

    .line 66
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->presenter:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->contentSize:Landroid/graphics/Point;

    invoke-interface {v0, v6, v1}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;->onPreviewLayoutFinalized(Landroid/graphics/Rect;Landroid/graphics/Point;)V

    .line 68
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 49
    goto :goto_0

    :cond_3
    move v0, v3

    .line 53
    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-static {v0, p1}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->resolveSize(II)I

    move-result v0

    .line 41
    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->getSuggestedMinimumHeight()I

    move-result v1

    invoke-static {v1, p2}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->resolveSize(II)I

    move-result v1

    .line 43
    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->contentSize:Landroid/graphics/Point;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 44
    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->setMeasuredDimension(II)V

    .line 45
    return-void
.end method

.method public setPresenter(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;)V
    .locals 2

    .prologue
    .line 90
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->presenter:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;

    .line 91
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->previewView:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 92
    invoke-interface {v0, p1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 93
    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 94
    return-void
.end method

.method public setPreviewPadding(II)V
    .locals 3

    .prologue
    .line 72
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, p1, p2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->cameraPreviewSize:Landroid/graphics/Point;

    .line 73
    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->requestLayout()V

    .line 79
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->presenter:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewViewImpl;->contentSize:Landroid/graphics/Point;

    invoke-interface {v0, v1, v2}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView$Presenter;->onPreviewLayoutFinalized(Landroid/graphics/Rect;Landroid/graphics/Point;)V

    .line 80
    return-void
.end method

.method public toView()Landroid/view/View;
    .locals 0

    .prologue
    .line 84
    return-object p0
.end method
