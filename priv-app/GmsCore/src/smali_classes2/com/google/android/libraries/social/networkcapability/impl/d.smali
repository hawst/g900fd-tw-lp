.class Lcom/google/android/libraries/social/networkcapability/impl/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/o/b;


# static fields
.field private static final a:Lcom/google/android/libraries/social/f/a;


# instance fields
.field private final b:Ljava/util/List;

.field private final c:Lcom/google/android/libraries/social/networkcapability/impl/c;

.field private final d:Lcom/google/android/libraries/social/networkcapability/impl/e;

.field private final e:Landroid/content/Context;

.field private f:J

.field private g:J

.field private h:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/libraries/social/f/a;

    const-string v1, "visual_debug_network_req"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/social/f/a;-><init>(Ljava/lang/String;B)V

    sput-object v0, Lcom/google/android/libraries/social/networkcapability/impl/d;->a:Lcom/google/android/libraries/social/f/a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/networkcapability/impl/d;->b:Ljava/util/List;

    .line 42
    iput-wide v2, p0, Lcom/google/android/libraries/social/networkcapability/impl/d;->f:J

    .line 43
    iput-wide v2, p0, Lcom/google/android/libraries/social/networkcapability/impl/d;->g:J

    .line 44
    iput-wide v2, p0, Lcom/google/android/libraries/social/networkcapability/impl/d;->h:J

    .line 50
    iput-object p1, p0, Lcom/google/android/libraries/social/networkcapability/impl/d;->e:Landroid/content/Context;

    .line 51
    new-instance v0, Lcom/google/android/libraries/social/networkcapability/impl/c;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/social/networkcapability/impl/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/networkcapability/impl/d;->c:Lcom/google/android/libraries/social/networkcapability/impl/c;

    .line 52
    new-instance v0, Lcom/google/android/libraries/social/networkcapability/impl/g;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/social/networkcapability/impl/g;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/networkcapability/impl/d;->d:Lcom/google/android/libraries/social/networkcapability/impl/e;

    .line 53
    const-string v0, "NetworkLogImpl"

    const-string v1, "Loaded NetworkSpeedPredictor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    return-void
.end method
