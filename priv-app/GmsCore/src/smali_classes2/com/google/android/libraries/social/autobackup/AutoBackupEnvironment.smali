.class public final Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static e:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;


# instance fields
.field a:Z

.field b:Z

.field c:Z

.field d:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a(Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;Landroid/content/Context;Landroid/content/Intent;)Z

    .line 68
    invoke-static {p0, p1}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a(Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;Landroid/content/Context;)Z

    .line 69
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;
    .locals 2

    .prologue
    .line 38
    const-class v1, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->e:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->e:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    .line 41
    :cond_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->e:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 38
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a()Z
    .locals 2

    .prologue
    .line 61
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 62
    const-string v1, "mounted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "mounted_ro"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;Landroid/content/Context;)Z
    .locals 10

    .prologue
    const/4 v2, 0x1

    .line 114
    const/4 v1, 0x0

    .line 116
    const-class v0, Lcom/google/android/libraries/social/networkcapability/a;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/networkcapability/a;

    .line 118
    iget-boolean v3, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->b:Z

    .line 119
    invoke-interface {v0}, Lcom/google/android/libraries/social/networkcapability/a;->c()Z

    move-result v4

    .line 120
    if-eq v4, v3, :cond_0

    .line 122
    iput-boolean v4, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->b:Z

    move v1, v2

    .line 124
    :cond_0
    iget-boolean v5, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->c:Z

    .line 125
    invoke-interface {v0}, Lcom/google/android/libraries/social/networkcapability/a;->d()Z

    move-result v6

    .line 126
    if-eq v6, v5, :cond_1

    .line 128
    iput-boolean v6, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->c:Z

    move v1, v2

    .line 130
    :cond_1
    iget-boolean v7, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->d:Z

    .line 131
    invoke-interface {v0}, Lcom/google/android/libraries/social/networkcapability/a;->e()Z

    move-result v8

    .line 132
    if-eq v8, v7, :cond_6

    .line 134
    iput-boolean v8, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->d:Z

    .line 137
    :goto_0
    const-string v0, "iu.Environment"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 138
    const-string v1, "iu.Environment"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v9, "update connectivity state; isNetworkMetered? "

    invoke-direct {v0, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    if-eq v3, v4, :cond_3

    const-string v0, "*"

    :goto_1
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", isRoaming? "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eq v5, v6, :cond_4

    const-string v0, "*"

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", isBackgroundDataAllowed? "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eq v7, v8, :cond_5

    const-string v0, "*"

    :goto_3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    :cond_2
    return v2

    .line 138
    :cond_3
    const-string v0, ""

    goto :goto_1

    :cond_4
    const-string v0, ""

    goto :goto_2

    :cond_5
    const-string v0, ""

    goto :goto_3

    :cond_6
    move v2, v1

    goto :goto_0
.end method

.method static a(Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 74
    .line 75
    iget-boolean v3, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a:Z

    .line 76
    if-nez p2, :cond_3

    .line 78
    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 81
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {p1, v5, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/content/ReceiverCallNotAllowedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 89
    :goto_0
    if-eqz v2, :cond_0

    .line 90
    const-string v4, "plugged"

    const/4 v5, -0x1

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 91
    if-lez v2, :cond_2

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a:Z

    .line 92
    const-string v0, "iu.Environment"

    const/4 v4, 0x3

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    const-string v4, "iu.Environment"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v0, "starting battery state: "

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "UNKNOWN ("

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :cond_0
    :goto_3
    const-string v0, "iu.Environment"

    const/4 v2, 0x4

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    const-string v2, "iu.Environment"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "update battery state; isPlugged? "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a:Z

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a:Z

    if-eq v3, v0, :cond_4

    const-string v0, "*"

    :goto_4
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :cond_1
    return v1

    :cond_2
    move v0, v1

    .line 91
    goto :goto_1

    .line 93
    :pswitch_1
    const-string v0, "BATTERY_PLUGGED_AC"

    goto :goto_2

    :pswitch_2
    const-string v0, "BATTERY_PLUGGED_USB"

    goto :goto_2

    :pswitch_3
    const-string v0, "BATTERY_PLUGGED_WIRELESS"

    goto :goto_2

    .line 97
    :cond_3
    const-string v2, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 98
    if-eq v2, v3, :cond_0

    .line 100
    iput-boolean v2, p0, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a:Z

    move v1, v0

    goto :goto_3

    .line 105
    :cond_4
    const-string v0, ""

    goto :goto_4

    :catch_0
    move-exception v4

    goto/16 :goto_0

    .line 93
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
