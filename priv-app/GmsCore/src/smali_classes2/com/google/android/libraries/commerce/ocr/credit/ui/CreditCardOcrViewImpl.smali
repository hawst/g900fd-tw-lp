.class public Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/ui/OcrView;


# instance fields
.field private final cameraPreview:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;

.field private final context:Landroid/content/Context;

.field private final handler:Landroid/os/Handler;

.field private final previewOverlayView:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewImpl;->cameraPreview:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;

    .line 24
    iput-object p3, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewImpl;->previewOverlayView:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    .line 25
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewImpl;->context:Landroid/content/Context;

    .line 26
    iput-object p4, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewImpl;->handler:Landroid/os/Handler;

    .line 27
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewImpl;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewImpl;->context:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public getCameraPreviewView()Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewImpl;->cameraPreview:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;

    return-object v0
.end method

.method public getPreviewOverlayView()Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewImpl;->previewOverlayView:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    return-object v0
.end method

.method public bridge synthetic getPreviewOverlayView()Lcom/google/android/libraries/commerce/ocr/ui/PreviewOverlayView;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewImpl;->getPreviewOverlayView()Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    move-result-object v0

    return-object v0
.end method

.method public showErrorMessage(I)V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewImpl;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/libraries/commerce/ocr/credit/ui/c;

    invoke-direct {v1, p0, p1}, Lcom/google/android/libraries/commerce/ocr/credit/ui/c;-><init>(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardOcrViewImpl;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 48
    return-void
.end method
