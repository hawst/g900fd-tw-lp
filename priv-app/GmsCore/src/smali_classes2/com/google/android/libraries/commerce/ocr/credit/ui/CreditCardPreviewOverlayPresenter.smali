.class public Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayPresenter;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/ui/PreviewOverlayView$Presenter;


# instance fields
.field private view:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreviewLayoutFinalized(Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayPresenter;->view:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    invoke-virtual {v0, p2}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->showBoundingBox(Landroid/graphics/Rect;)V

    .line 24
    return-void
.end method

.method public renderInvalidCardMessage()V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayPresenter;->view:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->renderInvalidCardMessage()V

    .line 29
    return-void
.end method

.method public setView(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)V
    .locals 0

    .prologue
    .line 18
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayPresenter;->view:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    .line 19
    return-void
.end method

.method public bridge synthetic setView(Lcom/google/android/libraries/commerce/ocr/ui/PreviewOverlayView;)V
    .locals 0

    .prologue
    .line 10
    check-cast p1, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayPresenter;->setView(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)V

    return-void
.end method

.method public snapAndPerformOcr()V
    .locals 0

    .prologue
    .line 34
    return-void
.end method
