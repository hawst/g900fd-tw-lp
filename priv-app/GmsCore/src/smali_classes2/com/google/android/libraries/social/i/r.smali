.class public final Lcom/google/android/libraries/social/i/r;
.super Lcom/google/android/libraries/social/i/w;
.source "SourceFile"


# instance fields
.field private c:Lcom/google/android/libraries/social/i/ab;

.field private d:Lcom/google/android/libraries/social/i/ab;

.field private e:Lcom/google/android/libraries/social/i/ab;

.field private f:Lcom/google/android/libraries/social/i/ab;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/libraries/social/i/w;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 53
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/libraries/social/i/r;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 54
    iget-object v1, p0, Lcom/google/android/libraries/social/i/r;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 55
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 59
    :cond_0
    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/google/android/libraries/social/i/s;

    invoke-direct {v0, p0, p1}, Lcom/google/android/libraries/social/i/s;-><init>(Lcom/google/android/libraries/social/i/r;Landroid/app/Activity;)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/i/r;->a(Lcom/google/android/libraries/social/i/ab;)Lcom/google/android/libraries/social/i/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/i/r;->c:Lcom/google/android/libraries/social/i/ab;

    .line 38
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lcom/google/android/libraries/social/i/t;

    invoke-direct {v0, p0, p1}, Lcom/google/android/libraries/social/i/t;-><init>(Lcom/google/android/libraries/social/i/r;Landroid/os/Bundle;)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/i/r;->a(Lcom/google/android/libraries/social/i/ab;)Lcom/google/android/libraries/social/i/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/i/r;->d:Lcom/google/android/libraries/social/i/ab;

    .line 50
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 74
    new-instance v0, Lcom/google/android/libraries/social/i/v;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/android/libraries/social/i/v;-><init>(Lcom/google/android/libraries/social/i/r;Landroid/os/Bundle;Landroid/view/View;)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/i/r;->a(Lcom/google/android/libraries/social/i/ab;)Lcom/google/android/libraries/social/i/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/i/r;->e:Lcom/google/android/libraries/social/i/ab;

    .line 83
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/google/android/libraries/social/i/u;

    invoke-direct {v0, p0, p1}, Lcom/google/android/libraries/social/i/u;-><init>(Lcom/google/android/libraries/social/i/r;Landroid/os/Bundle;)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/i/r;->a(Lcom/google/android/libraries/social/i/ab;)Lcom/google/android/libraries/social/i/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/i/r;->f:Lcom/google/android/libraries/social/i/ab;

    .line 71
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 97
    invoke-super {p0}, Lcom/google/android/libraries/social/i/w;->c()V

    .line 98
    iget-object v0, p0, Lcom/google/android/libraries/social/i/r;->d:Lcom/google/android/libraries/social/i/ab;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/i/r;->b(Lcom/google/android/libraries/social/i/ab;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/libraries/social/i/r;->f:Lcom/google/android/libraries/social/i/ab;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/i/r;->b(Lcom/google/android/libraries/social/i/ab;)V

    .line 100
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/libraries/social/i/r;->e:Lcom/google/android/libraries/social/i/ab;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/i/r;->b(Lcom/google/android/libraries/social/i/ab;)V

    .line 87
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/libraries/social/i/r;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 88
    iget-object v1, p0, Lcom/google/android/libraries/social/i/r;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 93
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/libraries/social/i/r;->c:Lcom/google/android/libraries/social/i/ab;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/i/r;->b(Lcom/google/android/libraries/social/i/ab;)V

    .line 105
    return-void
.end method
