.class final Lcom/google/android/libraries/social/e/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[B

.field private static final b:[B

.field private static final c:[B


# instance fields
.field private final d:[Lcom/google/android/libraries/social/e/o;

.field private e:[B

.field private f:Ljava/util/ArrayList;

.field private final g:Ljava/nio/ByteOrder;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 39
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/libraries/social/e/d;->a:[B

    .line 42
    new-array v0, v1, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/libraries/social/e/d;->b:[B

    .line 45
    new-array v0, v1, [B

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/libraries/social/e/d;->c:[B

    return-void

    .line 39
    :array_0
    .array-data 1
        0x41t
        0x53t
        0x43t
        0x49t
        0x49t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 42
    :array_1
    .array-data 1
        0x4at
        0x49t
        0x53t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 45
    :array_2
    .array-data 1
        0x55t
        0x4et
        0x49t
        0x43t
        0x4ft
        0x44t
        0x45t
        0x0t
    .end array-data
.end method

.method constructor <init>(Ljava/nio/ByteOrder;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/libraries/social/e/o;

    iput-object v0, p0, Lcom/google/android/libraries/social/e/d;->d:[Lcom/google/android/libraries/social/e/o;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/e/d;->f:Ljava/util/ArrayList;

    .line 55
    iput-object p1, p0, Lcom/google/android/libraries/social/e/d;->g:Ljava/nio/ByteOrder;

    .line 56
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/libraries/social/e/n;)Lcom/google/android/libraries/social/e/n;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 172
    if-eqz p1, :cond_1

    .line 173
    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/n;->a()I

    move-result v1

    .line 174
    if-eqz p1, :cond_1

    invoke-static {v1}, Lcom/google/android/libraries/social/e/n;->a(I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/e/d;->d:[Lcom/google/android/libraries/social/e/o;

    aget-object v0, v0, v1

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/libraries/social/e/o;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/e/o;-><init>(I)V

    iget-object v2, p0, Lcom/google/android/libraries/social/e/d;->d:[Lcom/google/android/libraries/social/e/o;

    aput-object v0, v2, v1

    :cond_0
    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/e/o;->a(Lcom/google/android/libraries/social/e/n;)Lcom/google/android/libraries/social/e/n;

    move-result-object v0

    .line 176
    :cond_1
    return-object v0
.end method

.method protected final a(SI)Lcom/google/android/libraries/social/e/n;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/libraries/social/e/d;->d:[Lcom/google/android/libraries/social/e/o;

    aget-object v0, v0, p2

    .line 164
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/e/o;->a(S)Lcom/google/android/libraries/social/e/n;

    move-result-object v0

    goto :goto_0
.end method

.method protected final a(I[B)V
    .locals 3

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/libraries/social/e/d;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/libraries/social/e/d;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 94
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/e/d;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_1
    if-ge v0, p1, :cond_1

    .line 90
    iget-object v1, p0, Lcom/google/android/libraries/social/e/d;->f:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/e/d;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/libraries/social/e/o;)V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/libraries/social/e/d;->d:[Lcom/google/android/libraries/social/e/o;

    invoke-virtual {p1}, Lcom/google/android/libraries/social/e/o;->c()I

    move-result v1

    aput-object p1, v0, v1

    .line 143
    return-void
.end method

.method protected final a([B)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/libraries/social/e/d;->e:[B

    .line 73
    return-void
.end method

.method protected final a()[B
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/libraries/social/e/d;->e:[B

    return-object v0
.end method

.method protected final a(I)[B
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/libraries/social/e/d;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method protected final b(I)Lcom/google/android/libraries/social/e/o;
    .locals 1

    .prologue
    .line 131
    invoke-static {p1}, Lcom/google/android/libraries/social/e/n;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/google/android/libraries/social/e/d;->d:[Lcom/google/android/libraries/social/e/o;

    aget-object v0, v0, p1

    .line 134
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final b(SI)V
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/libraries/social/e/d;->d:[Lcom/google/android/libraries/social/e/o;

    aget-object v0, v0, p2

    .line 209
    if-nez v0, :cond_0

    .line 213
    :goto_0
    return-void

    .line 212
    :cond_0
    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/e/o;->b(S)V

    goto :goto_0
.end method

.method protected final b()Z
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/libraries/social/e/d;->e:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final c()I
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/libraries/social/e/d;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method protected final d()Z
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/libraries/social/e/d;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final e()Ljava/nio/ByteOrder;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/libraries/social/e/d;->g:Ljava/nio/ByteOrder;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 321
    if-ne p0, p1, :cond_1

    move v3, v4

    .line 348
    :cond_0
    :goto_0
    return v3

    .line 324
    :cond_1
    if-eqz p1, :cond_0

    .line 327
    instance-of v0, p1, Lcom/google/android/libraries/social/e/d;

    if-eqz v0, :cond_0

    .line 328
    check-cast p1, Lcom/google/android/libraries/social/e/d;

    .line 329
    iget-object v0, p1, Lcom/google/android/libraries/social/e/d;->g:Ljava/nio/ByteOrder;

    iget-object v1, p0, Lcom/google/android/libraries/social/e/d;->g:Ljava/nio/ByteOrder;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/google/android/libraries/social/e/d;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/libraries/social/e/d;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/google/android/libraries/social/e/d;->e:[B

    iget-object v1, p0, Lcom/google/android/libraries/social/e/d;->e:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    move v2, v3

    .line 334
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/social/e/d;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 335
    iget-object v0, p1, Lcom/google/android/libraries/social/e/d;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iget-object v1, p0, Lcom/google/android/libraries/social/e/d;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 334
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v3

    .line 339
    :goto_2
    const/4 v1, 0x5

    if-ge v0, v1, :cond_4

    .line 340
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v1

    .line 341
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v2

    .line 342
    if-eq v1, v2, :cond_3

    if-eqz v1, :cond_3

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/e/o;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 339
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    move v3, v4

    .line 346
    goto :goto_0
.end method

.method protected final f()Ljava/util/List;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 259
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 260
    iget-object v4, p0, Lcom/google/android/libraries/social/e/d;->d:[Lcom/google/android/libraries/social/e/o;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_1

    aget-object v1, v4, v3

    .line 261
    if-eqz v1, :cond_0

    .line 262
    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/o;->b()[Lcom/google/android/libraries/social/e/n;

    move-result-object v6

    .line 263
    if-eqz v6, :cond_0

    .line 264
    array-length v7, v6

    move v1, v2

    :goto_1
    if-ge v1, v7, :cond_0

    aget-object v8, v6, v1

    .line 265
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 264
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 260
    :cond_0
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 270
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 271
    const/4 v0, 0x0

    .line 273
    :cond_2
    return-object v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/libraries/social/e/d;->g:Ljava/nio/ByteOrder;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 357
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/libraries/social/e/d;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 358
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/libraries/social/e/d;->e:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    add-int/2addr v0, v1

    .line 359
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/libraries/social/e/d;->d:[Lcom/google/android/libraries/social/e/o;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 360
    return v0
.end method
