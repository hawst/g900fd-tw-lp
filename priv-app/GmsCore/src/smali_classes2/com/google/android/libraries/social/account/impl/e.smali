.class Lcom/google/android/libraries/social/account/impl/e;
.super Lcom/google/android/libraries/social/account/impl/g;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/account/d;


# instance fields
.field final synthetic a:Lcom/google/android/libraries/social/account/impl/a;

.field private final f:I


# direct methods
.method constructor <init>(Lcom/google/android/libraries/social/account/impl/a;Landroid/content/SharedPreferences$Editor;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 657
    iput-object p1, p0, Lcom/google/android/libraries/social/account/impl/e;->a:Lcom/google/android/libraries/social/account/impl/a;

    .line 658
    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/libraries/social/account/impl/g;-><init>(Lcom/google/android/libraries/social/account/impl/a;Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    .line 659
    iput p3, p0, Lcom/google/android/libraries/social/account/impl/e;->f:I

    .line 660
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/libraries/social/account/d;
    .locals 0

    .prologue
    .line 694
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/account/impl/g;->b(I)Lcom/google/android/libraries/social/account/f;

    .line 695
    return-object p0
.end method

.method public final a(Ljava/lang/String;F)Lcom/google/android/libraries/social/account/d;
    .locals 0

    .prologue
    .line 688
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/social/account/impl/g;->b(Ljava/lang/String;F)Lcom/google/android/libraries/social/account/f;

    .line 689
    return-object p0
.end method

.method public final a(Ljava/lang/String;I)Lcom/google/android/libraries/social/account/d;
    .locals 0

    .prologue
    .line 676
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/social/account/impl/g;->b(Ljava/lang/String;I)Lcom/google/android/libraries/social/account/f;

    .line 677
    return-object p0
.end method

.method public final a(Ljava/lang/String;J)Lcom/google/android/libraries/social/account/d;
    .locals 0

    .prologue
    .line 682
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/libraries/social/account/impl/g;->b(Ljava/lang/String;J)Lcom/google/android/libraries/social/account/f;

    .line 683
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/account/d;
    .locals 0

    .prologue
    .line 664
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/social/account/impl/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/account/f;

    .line 665
    return-object p0
.end method

.method public final a(Ljava/lang/String;Z)Lcom/google/android/libraries/social/account/d;
    .locals 0

    .prologue
    .line 670
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/social/account/impl/g;->b(Ljava/lang/String;Z)Lcom/google/android/libraries/social/account/f;

    .line 671
    return-object p0
.end method

.method public final bridge synthetic b(I)Lcom/google/android/libraries/social/account/f;
    .locals 0

    .prologue
    .line 654
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/account/impl/g;->b(I)Lcom/google/android/libraries/social/account/f;

    return-object p0
.end method

.method public final synthetic b(Ljava/lang/String;F)Lcom/google/android/libraries/social/account/f;
    .locals 1

    .prologue
    .line 654
    invoke-virtual {p0, p1, p2}, Lcom/google/android/libraries/social/account/impl/e;->a(Ljava/lang/String;F)Lcom/google/android/libraries/social/account/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/String;I)Lcom/google/android/libraries/social/account/f;
    .locals 1

    .prologue
    .line 654
    invoke-virtual {p0, p1, p2}, Lcom/google/android/libraries/social/account/impl/e;->a(Ljava/lang/String;I)Lcom/google/android/libraries/social/account/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/String;J)Lcom/google/android/libraries/social/account/f;
    .locals 2

    .prologue
    .line 654
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/libraries/social/account/impl/e;->a(Ljava/lang/String;J)Lcom/google/android/libraries/social/account/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/account/f;
    .locals 1

    .prologue
    .line 654
    invoke-virtual {p0, p1, p2}, Lcom/google/android/libraries/social/account/impl/e;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/account/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/String;Z)Lcom/google/android/libraries/social/account/f;
    .locals 1

    .prologue
    .line 654
    invoke-virtual {p0, p1, p2}, Lcom/google/android/libraries/social/account/impl/e;->a(Ljava/lang/String;Z)Lcom/google/android/libraries/social/account/d;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 711
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/e;->b:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 712
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/e;->a:Lcom/google/android/libraries/social/account/impl/a;

    invoke-static {v0}, Lcom/google/android/libraries/social/account/impl/a;->c(Lcom/google/android/libraries/social/account/impl/a;)V

    .line 713
    iget v0, p0, Lcom/google/android/libraries/social/account/impl/e;->f:I

    return v0
.end method

.method public final synthetic d(Ljava/lang/String;)Lcom/google/android/libraries/social/account/c;
    .locals 1

    .prologue
    .line 654
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/account/impl/e;->f(Ljava/lang/String;)Lcom/google/android/libraries/social/account/d;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/String;)Lcom/google/android/libraries/social/account/d;
    .locals 0

    .prologue
    .line 700
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/account/impl/g;->g(Ljava/lang/String;)Lcom/google/android/libraries/social/account/f;

    .line 701
    return-object p0
.end method

.method public final f(Ljava/lang/String;)Lcom/google/android/libraries/social/account/d;
    .locals 6

    .prologue
    .line 706
    new-instance v0, Lcom/google/android/libraries/social/account/impl/e;

    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/e;->a:Lcom/google/android/libraries/social/account/impl/a;

    iget-object v2, p0, Lcom/google/android/libraries/social/account/impl/e;->b:Landroid/content/SharedPreferences$Editor;

    iget v3, p0, Lcom/google/android/libraries/social/account/impl/e;->f:I

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/google/android/libraries/social/account/impl/e;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/social/account/impl/e;-><init>(Lcom/google/android/libraries/social/account/impl/a;Landroid/content/SharedPreferences$Editor;ILjava/lang/String;)V

    return-object v0
.end method

.method public final bridge synthetic g(Ljava/lang/String;)Lcom/google/android/libraries/social/account/f;
    .locals 0

    .prologue
    .line 654
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/account/impl/g;->g(Ljava/lang/String;)Lcom/google/android/libraries/social/account/f;

    return-object p0
.end method

.method public final synthetic h(Ljava/lang/String;)Lcom/google/android/libraries/social/account/f;
    .locals 1

    .prologue
    .line 654
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/account/impl/e;->f(Ljava/lang/String;)Lcom/google/android/libraries/social/account/d;

    move-result-object v0

    return-object v0
.end method
