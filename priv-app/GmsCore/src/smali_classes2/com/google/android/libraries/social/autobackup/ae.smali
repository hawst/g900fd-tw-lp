.class final Lcom/google/android/libraries/social/autobackup/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/libraries/social/autobackup/ad;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/social/autobackup/ad;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/ae;->b:Lcom/google/android/libraries/social/autobackup/ad;

    iput-object p2, p0, Lcom/google/android/libraries/social/autobackup/ae;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ae;->b:Lcom/google/android/libraries/social/autobackup/ad;

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/ad;->a(Lcom/google/android/libraries/social/autobackup/ad;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 226
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ae;->b:Lcom/google/android/libraries/social/autobackup/ad;

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/ad;->b(Lcom/google/android/libraries/social/autobackup/ad;)Lcom/google/android/libraries/social/autobackup/au;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/au;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 227
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 229
    :try_start_1
    const-string v0, "exclude_bucket"

    const-string v3, "bucket_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/libraries/social/autobackup/ae;->a:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 233
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/ae;->b:Lcom/google/android/libraries/social/autobackup/ad;

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/ad;->c(Lcom/google/android/libraries/social/autobackup/ad;)Landroid/content/Context;

    move-result-object v0

    const-class v3, Lcom/google/android/libraries/social/autobackup/o;

    invoke-static {v0, v3}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/o;

    .line 236
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/o;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 237
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/ae;->a:Ljava/lang/String;

    invoke-static {v2, v0, v4}, Lcom/google/android/libraries/social/autobackup/ab;->a(Landroid/database/sqlite/SQLiteDatabase;ILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 246
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 248
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 240
    :cond_0
    :try_start_3
    new-instance v0, Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/ae;->b:Lcom/google/android/libraries/social/autobackup/ad;

    invoke-static {v3}, Lcom/google/android/libraries/social/autobackup/ad;->c(Lcom/google/android/libraries/social/autobackup/ad;)Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/google/android/libraries/social/mediamonitor/MediaMonitor;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 241
    const-string v3, "com.google.android.libraries.social.mediamonitor.FORCE_RESCAN"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 242
    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/ae;->b:Lcom/google/android/libraries/social/autobackup/ad;

    invoke-static {v3}, Lcom/google/android/libraries/social/autobackup/ad;->c(Lcom/google/android/libraries/social/autobackup/ad;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 244
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 246
    :try_start_4
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 248
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    return-void
.end method
