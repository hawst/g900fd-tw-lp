.class public abstract Lcom/google/android/libraries/commerce/ocr/util/Ticker;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final SYSTEM_TICKER:Lcom/google/android/libraries/commerce/ocr/util/Ticker;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/util/d;

    invoke-direct {v0}, Lcom/google/android/libraries/commerce/ocr/util/d;-><init>()V

    sput-object v0, Lcom/google/android/libraries/commerce/ocr/util/Ticker;->SYSTEM_TICKER:Lcom/google/android/libraries/commerce/ocr/util/Ticker;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static systemTicker()Lcom/google/android/libraries/commerce/ocr/util/Ticker;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/google/android/libraries/commerce/ocr/util/Ticker;->SYSTEM_TICKER:Lcom/google/android/libraries/commerce/ocr/util/Ticker;

    return-object v0
.end method


# virtual methods
.method public abstract read()J
.end method
