.class public final Lcom/google/android/libraries/social/autobackup/u;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:[Lcom/google/android/libraries/social/autobackup/z;


# direct methods
.method private static a(Landroid/content/Context;Lcom/google/android/libraries/social/autobackup/z;IILcom/google/android/libraries/social/autobackup/w;Landroid/database/Cursor;I)V
    .locals 9

    .prologue
    .line 196
    const/4 v0, 0x5

    invoke-interface {p5, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 197
    const/4 v2, 0x6

    invoke-interface {p5, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 198
    const/4 v2, 0x3

    invoke-interface {p5, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 200
    iget-wide v6, p4, Lcom/google/android/libraries/social/autobackup/w;->d:J

    iget-boolean v8, p4, Lcom/google/android/libraries/social/autobackup/w;->e:Z

    if-eqz v8, :cond_1

    move-wide v0, v2

    :goto_0
    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p4, Lcom/google/android/libraries/social/autobackup/w;->d:J

    .line 202
    iget v0, p4, Lcom/google/android/libraries/social/autobackup/w;->c:I

    const/4 v1, 0x4

    invoke-interface {p5, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p4, Lcom/google/android/libraries/social/autobackup/w;->c:I

    .line 203
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p4, Lcom/google/android/libraries/social/autobackup/w;->a:Ljava/lang/Integer;

    .line 207
    iget-object v6, p4, Lcom/google/android/libraries/social/autobackup/w;->f:Ljava/util/TreeSet;

    .line 209
    if-lez p6, :cond_2

    invoke-virtual {v6}, Ljava/util/TreeSet;->size()I

    move-result v0

    if-lt v0, p6, :cond_0

    invoke-virtual {v6}, Ljava/util/TreeSet;->last()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/y;

    iget-wide v0, v0, Lcom/google/android/libraries/social/autobackup/y;->c:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 211
    :cond_0
    iget-boolean v3, p4, Lcom/google/android/libraries/social/autobackup/w;->e:Z

    move-object v0, p1

    move-object v1, p0

    move v2, p3

    move v4, p2

    move v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/libraries/social/autobackup/z;->a(Landroid/content/Context;IZII)Ljava/util/ArrayList;

    move-result-object v0

    .line 214
    invoke-virtual {v6, v0}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    .line 216
    invoke-virtual {v6}, Ljava/util/TreeSet;->size()I

    move-result v0

    sub-int/2addr v0, p6

    .line 218
    :goto_1
    if-lez v0, :cond_2

    .line 219
    invoke-virtual {v6}, Ljava/util/TreeSet;->last()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 220
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 200
    :cond_1
    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0

    .line 223
    :cond_2
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/util/ArrayList;II)V
    .locals 8

    .prologue
    .line 102
    new-instance v7, Landroid/util/SparseArray;

    invoke-direct {v7}, Landroid/util/SparseArray;-><init>()V

    .line 103
    const/4 v2, 0x0

    :goto_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/u;->a:[Lcom/google/android/libraries/social/autobackup/z;

    array-length v0, v0

    if-ge v2, v0, :cond_4

    .line 104
    sget-object v0, Lcom/google/android/libraries/social/autobackup/u;->a:[Lcom/google/android/libraries/social/autobackup/z;

    aget-object v1, v0, v2

    .line 105
    const/4 v5, 0x0

    .line 116
    const/4 v0, 0x2

    :try_start_0
    invoke-virtual {v1, p0, v0}, Lcom/google/android/libraries/social/autobackup/z;->a(Landroid/content/Context;I)Landroid/database/Cursor;

    move-result-object v5

    .line 118
    if-eqz v5, :cond_1

    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    :cond_0
    const/4 v0, 0x1

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 123
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v7, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/w;

    .line 125
    if-nez v0, :cond_6

    .line 126
    new-instance v4, Lcom/google/android/libraries/social/autobackup/w;

    invoke-direct {v4}, Lcom/google/android/libraries/social/autobackup/w;-><init>()V

    .line 127
    const/4 v0, 0x2

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/libraries/social/autobackup/w;->b:Ljava/lang/String;

    .line 129
    iput-object v3, v4, Lcom/google/android/libraries/social/autobackup/w;->a:Ljava/lang/Integer;

    .line 131
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v7, v0, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 134
    :goto_1
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object v0, p0

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/libraries/social/autobackup/u;->a(Landroid/content/Context;Lcom/google/android/libraries/social/autobackup/z;IILcom/google/android/libraries/social/autobackup/w;Landroid/database/Cursor;I)V

    .line 136
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 139
    :cond_1
    if-eqz v5, :cond_2

    .line 140
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 103
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 139
    :catchall_0
    move-exception v0

    if-eqz v5, :cond_3

    .line 140
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 145
    :cond_4
    invoke-virtual {v7}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_2
    if-ltz v0, :cond_5

    .line 146
    invoke-virtual {v7, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 148
    :cond_5
    return-void

    :cond_6
    move-object v4, v0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;I)[Lcom/google/android/libraries/social/autobackup/w;
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 62
    sget-object v1, Lcom/google/android/libraries/social/autobackup/u;->a:[Lcom/google/android/libraries/social/autobackup/z;

    if-nez v1, :cond_0

    const/4 v1, 0x4

    new-array v1, v1, [Lcom/google/android/libraries/social/autobackup/z;

    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/libraries/social/autobackup/x;

    sget-object v4, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v3, v4}, Lcom/google/android/libraries/social/autobackup/x;-><init>(Landroid/net/Uri;)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lcom/google/android/libraries/social/autobackup/x;

    sget-object v4, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->a:Landroid/net/Uri;

    invoke-direct {v3, v4}, Lcom/google/android/libraries/social/autobackup/x;-><init>(Landroid/net/Uri;)V

    aput-object v3, v1, v2

    new-instance v2, Lcom/google/android/libraries/social/autobackup/aa;

    sget-object v3, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v2, v3}, Lcom/google/android/libraries/social/autobackup/aa;-><init>(Landroid/net/Uri;)V

    aput-object v2, v1, v5

    const/4 v2, 0x3

    new-instance v3, Lcom/google/android/libraries/social/autobackup/aa;

    sget-object v4, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->b:Landroid/net/Uri;

    invoke-direct {v3, v4}, Lcom/google/android/libraries/social/autobackup/aa;-><init>(Landroid/net/Uri;)V

    aput-object v3, v1, v2

    sput-object v1, Lcom/google/android/libraries/social/autobackup/u;->a:[Lcom/google/android/libraries/social/autobackup/z;

    .line 64
    :cond_0
    invoke-static {p0, v0, p1, v5}, Lcom/google/android/libraries/social/autobackup/u;->a(Landroid/content/Context;Ljava/util/ArrayList;II)V

    .line 72
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/libraries/social/autobackup/w;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/libraries/social/autobackup/w;

    .line 77
    new-instance v1, Lcom/google/android/libraries/social/autobackup/v;

    invoke-direct {v1}, Lcom/google/android/libraries/social/autobackup/v;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 85
    return-object v0
.end method
