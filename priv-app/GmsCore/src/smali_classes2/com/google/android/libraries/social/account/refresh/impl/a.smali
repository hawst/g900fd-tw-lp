.class public final Lcom/google/android/libraries/social/account/refresh/impl/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/account/refresh/a;


# instance fields
.field private final a:Lcom/google/android/libraries/social/account/b;

.field private final b:Lcom/google/android/libraries/social/account/k;

.field private final c:Lcom/google/android/libraries/social/account/refresh/a/a;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    iput-object v0, p0, Lcom/google/android/libraries/social/account/refresh/impl/a;->a:Lcom/google/android/libraries/social/account/b;

    .line 38
    const-class v0, Lcom/google/android/libraries/social/account/k;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/k;

    iput-object v0, p0, Lcom/google/android/libraries/social/account/refresh/impl/a;->b:Lcom/google/android/libraries/social/account/k;

    .line 39
    const-class v0, Lcom/google/android/libraries/social/account/refresh/a/a;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/refresh/a/a;

    iput-object v0, p0, Lcom/google/android/libraries/social/account/refresh/impl/a;->c:Lcom/google/android/libraries/social/account/refresh/a/a;

    .line 40
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 9

    .prologue
    .line 44
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 45
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 46
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 47
    iget-object v0, p0, Lcom/google/android/libraries/social/account/refresh/impl/a;->b:Lcom/google/android/libraries/social/account/k;

    invoke-interface {v0}, Lcom/google/android/libraries/social/account/k;->a()[Ljava/lang/String;

    move-result-object v1

    array-length v5, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_1

    aget-object v6, v1, v0

    .line 48
    iget-object v7, p0, Lcom/google/android/libraries/social/account/refresh/impl/a;->c:Lcom/google/android/libraries/social/account/refresh/a/a;

    invoke-interface {v7, v6}, Lcom/google/android/libraries/social/account/refresh/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 49
    if-eqz v7, :cond_0

    .line 50
    invoke-interface {v3, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    invoke-interface {v2, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    :cond_0
    invoke-interface {v4, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 56
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/account/refresh/impl/a;->a:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0}, Lcom/google/android/libraries/social/account/b;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 57
    iget-object v0, p0, Lcom/google/android/libraries/social/account/refresh/impl/a;->a:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, v6}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v0

    .line 59
    const-string v1, "effective_gaia_id"

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    .line 60
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 63
    const-string v7, "stable_account_id"

    invoke-interface {v0, v7}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 64
    if-nez v0, :cond_3

    .line 65
    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 66
    iget-object v7, p0, Lcom/google/android/libraries/social/account/refresh/impl/a;->a:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v7, v6}, Lcom/google/android/libraries/social/account/b;->b(I)Lcom/google/android/libraries/social/account/d;

    move-result-object v7

    const-string v8, "stable_account_id"

    invoke-interface {v7, v8, v0}, Lcom/google/android/libraries/social/account/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/account/d;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/libraries/social/account/d;->c()I

    .line 72
    :cond_3
    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 73
    if-eqz v0, :cond_5

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 74
    iget-object v7, p0, Lcom/google/android/libraries/social/account/refresh/impl/a;->a:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v7, v1, v0}, Lcom/google/android/libraries/social/account/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    :goto_2
    invoke-interface {v4, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 78
    iget-object v0, p0, Lcom/google/android/libraries/social/account/refresh/impl/a;->a:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, v6}, Lcom/google/android/libraries/social/account/b;->d(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 44
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 81
    :cond_4
    monitor-exit p0

    return-void

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/libraries/social/account/refresh/impl/a;->a:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v0

    .line 87
    const-string v1, "effective_gaia_id"

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 95
    :goto_0
    return-void

    .line 90
    :cond_0
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 91
    iget-object v1, p0, Lcom/google/android/libraries/social/account/refresh/impl/a;->c:Lcom/google/android/libraries/social/account/refresh/a/a;

    invoke-interface {v1, v0}, Lcom/google/android/libraries/social/account/refresh/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 92
    iget-object v1, p0, Lcom/google/android/libraries/social/account/refresh/impl/a;->a:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v1, p1}, Lcom/google/android/libraries/social/account/b;->b(I)Lcom/google/android/libraries/social/account/d;

    move-result-object v1

    const-string v2, "stable_account_id"

    invoke-interface {v1, v2, v0}, Lcom/google/android/libraries/social/account/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/account/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/libraries/social/account/d;->c()I

    goto :goto_0
.end method
