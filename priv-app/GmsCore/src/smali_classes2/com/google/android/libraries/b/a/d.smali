.class public final Lcom/google/android/libraries/b/a/d;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 22
    invoke-static {p1, p0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 27
    :goto_0
    return-void

    .line 26
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/libraries/b/a/d;->b(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 30
    .line 31
    invoke-virtual {p2, v3, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 32
    :goto_0
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 33
    invoke-virtual {p2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p1, v1}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 34
    add-int/lit8 v1, v0, 0x1

    .line 35
    invoke-virtual {p2, v3, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    goto :goto_0

    .line 37
    :cond_0
    invoke-virtual {p2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 38
    return-void
.end method
