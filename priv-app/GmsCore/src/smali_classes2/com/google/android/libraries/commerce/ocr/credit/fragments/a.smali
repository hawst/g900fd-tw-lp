.class final Lcom/google/android/libraries/commerce/ocr/credit/fragments/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;

.field final synthetic c:Landroid/os/Bundle;

.field final synthetic d:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;ILcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/a;->d:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

    iput p2, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/a;->a:I

    iput-object p3, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/a;->b:Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;

    iput-object p4, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/a;->c:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 46
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/a;->d:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;->fragment:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;
    invoke-static {v1}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;->access$100(Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;)Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/libraries/commerce/ocr/R$string;->ocr_error_title:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/libraries/commerce/ocr/R$string;->ocr_cc_enter_manually_details:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/libraries/commerce/ocr/R$string;->ocr_confirm:I

    new-instance v2, Lcom/google/android/libraries/commerce/ocr/credit/fragments/b;

    invoke-direct {v2, p0}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/b;-><init>(Lcom/google/android/libraries/commerce/ocr/credit/fragments/a;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 56
    return-void
.end method
