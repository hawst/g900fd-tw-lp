.class final Lcom/google/android/libraries/social/e/h;
.super Ljava/io/FilterOutputStream;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/libraries/social/e/d;

.field private b:I

.field private c:I

.field private d:I

.field private e:[B

.field private f:Ljava/nio/ByteBuffer;

.field private final g:Lcom/google/android/libraries/social/e/c;


# direct methods
.method protected constructor <init>(Ljava/io/OutputStream;Lcom/google/android/libraries/social/e/c;)V
    .locals 2

    .prologue
    .line 90
    new-instance v0, Ljava/io/BufferedOutputStream;

    const/high16 v1, 0x10000

    invoke-direct {v0, p1, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    invoke-direct {p0, v0}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 82
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/social/e/h;->b:I

    .line 85
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/libraries/social/e/h;->e:[B

    .line 86
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/e/h;->f:Ljava/nio/ByteBuffer;

    .line 91
    iput-object p2, p0, Lcom/google/android/libraries/social/e/h;->g:Lcom/google/android/libraries/social/e/c;

    .line 92
    return-void
.end method

.method private a(I[BII)I
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    sub-int v0, p1, v0

    .line 112
    if-le p4, v0, :cond_0

    move p4, v0

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p2, p3, p4}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 114
    return p4
.end method

.method private static a(Lcom/google/android/libraries/social/e/o;I)I
    .locals 8

    .prologue
    .line 318
    invoke-virtual {p0}, Lcom/google/android/libraries/social/e/o;->d()I

    move-result v0

    mul-int/lit8 v0, v0, 0xc

    add-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x4

    add-int v1, p1, v0

    .line 319
    invoke-virtual {p0}, Lcom/google/android/libraries/social/e/o;->b()[Lcom/google/android/libraries/social/e/n;

    move-result-object v2

    .line 320
    array-length v3, v2

    const/4 v0, 0x0

    move v7, v0

    move v0, v1

    move v1, v7

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 321
    invoke-virtual {v4}, Lcom/google/android/libraries/social/e/n;->d()I

    move-result v5

    const/4 v6, 0x4

    if-le v5, v6, :cond_0

    .line 322
    invoke-virtual {v4, v0}, Lcom/google/android/libraries/social/e/n;->g(I)V

    .line 323
    invoke-virtual {v4}, Lcom/google/android/libraries/social/e/n;->d()I

    move-result v4

    add-int/2addr v0, v4

    .line 320
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 326
    :cond_1
    return v0
.end method

.method private a()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 214
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    if-nez v0, :cond_1

    .line 245
    :cond_0
    return-void

    .line 220
    :cond_1
    iget-object v3, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3}, Lcom/google/android/libraries/social/e/d;->f()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_3

    move v1, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-virtual {v3}, Lcom/google/android/libraries/social/e/d;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/e/n;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/n;->h()Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_2

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/n;->b()S

    move-result v6

    invoke-static {v6}, Lcom/google/android/libraries/social/e/c;->a(S)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/n;->b()S

    move-result v6

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/n;->a()I

    move-result v7

    invoke-virtual {v3, v6, v7}, Lcom/google/android/libraries/social/e/d;->b(SI)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 221
    :cond_3
    invoke-direct {p0}, Lcom/google/android/libraries/social/e/h;->b()V

    .line 222
    invoke-direct {p0}, Lcom/google/android/libraries/social/e/h;->c()I

    move-result v0

    .line 223
    add-int/lit8 v1, v0, 0x8

    const v3, 0xffff

    if-le v1, v3, :cond_4

    .line 224
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Exif header is too large (>64Kb)"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 226
    :cond_4
    new-instance v1, Lcom/google/android/libraries/social/e/q;

    iget-object v3, p0, Lcom/google/android/libraries/social/e/h;->out:Ljava/io/OutputStream;

    invoke-direct {v1, v3}, Lcom/google/android/libraries/social/e/q;-><init>(Ljava/io/OutputStream;)V

    .line 227
    sget-object v3, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/social/e/q;->a(Ljava/nio/ByteOrder;)Lcom/google/android/libraries/social/e/q;

    .line 228
    const/16 v3, -0x1f

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/social/e/q;->a(S)Lcom/google/android/libraries/social/e/q;

    .line 229
    add-int/lit8 v0, v0, 0x8

    int-to-short v0, v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/e/q;->a(S)Lcom/google/android/libraries/social/e/q;

    .line 230
    const v0, 0x45786966

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/e/q;->a(I)Lcom/google/android/libraries/social/e/q;

    .line 231
    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/e/q;->a(S)Lcom/google/android/libraries/social/e/q;

    .line 232
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/d;->e()Ljava/nio/ByteOrder;

    move-result-object v0

    sget-object v3, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    if-ne v0, v3, :cond_8

    .line 233
    const/16 v0, 0x4d4d

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/e/q;->a(S)Lcom/google/android/libraries/social/e/q;

    .line 237
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/d;->e()Ljava/nio/ByteOrder;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/e/q;->a(Ljava/nio/ByteOrder;)Lcom/google/android/libraries/social/e/q;

    .line 238
    const/16 v0, 0x2a

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/e/q;->a(S)Lcom/google/android/libraries/social/e/q;

    .line 239
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/e/q;->a(I)Lcom/google/android/libraries/social/e/q;

    .line 240
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/e/h;->a(Lcom/google/android/libraries/social/e/o;Lcom/google/android/libraries/social/e/q;)V

    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/e/h;->a(Lcom/google/android/libraries/social/e/o;Lcom/google/android/libraries/social/e/q;)V

    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/e/h;->a(Lcom/google/android/libraries/social/e/o;Lcom/google/android/libraries/social/e/q;)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/e/h;->a(Lcom/google/android/libraries/social/e/o;Lcom/google/android/libraries/social/e/q;)V

    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v0, v8}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v0, v8}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/e/h;->a(Lcom/google/android/libraries/social/e/o;Lcom/google/android/libraries/social/e/q;)V

    .line 241
    :cond_7
    invoke-direct {p0, v1}, Lcom/google/android/libraries/social/e/h;->a(Lcom/google/android/libraries/social/e/q;)V

    .line 242
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/e/n;

    .line 243
    iget-object v2, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/social/e/d;->a(Lcom/google/android/libraries/social/e/n;)Lcom/google/android/libraries/social/e/n;

    goto :goto_2

    .line 235
    :cond_8
    const/16 v0, 0x4949

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/e/q;->a(S)Lcom/google/android/libraries/social/e/q;

    goto :goto_1
.end method

.method private static a(Lcom/google/android/libraries/social/e/n;Lcom/google/android/libraries/social/e/q;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 492
    invoke-virtual {p0}, Lcom/google/android/libraries/social/e/n;->f()Z

    move-result v1

    if-nez v1, :cond_1

    .line 531
    :cond_0
    :goto_0
    return-void

    .line 496
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/e/n;->c()S

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 521
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v0

    new-array v0, v0, [B

    .line 522
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/e/n;->b([B)V

    .line 523
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/e/q;->write([B)V

    goto :goto_0

    .line 498
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/e/n;->i()[B

    move-result-object v1

    .line 499
    array-length v2, v1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 500
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aput-byte v0, v1, v2

    .line 501
    invoke-virtual {p1, v1}, Lcom/google/android/libraries/social/e/q;->write([B)V

    goto :goto_0

    .line 503
    :cond_2
    invoke-virtual {p1, v1}, Lcom/google/android/libraries/social/e/q;->write([B)V

    .line 504
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/e/q;->write(I)V

    goto :goto_0

    .line 509
    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v1

    :goto_1
    if-ge v0, v1, :cond_0

    .line 510
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/e/n;->e(I)J

    move-result-wide v2

    long-to-int v2, v2

    invoke-virtual {p1, v2}, Lcom/google/android/libraries/social/e/q;->a(I)Lcom/google/android/libraries/social/e/q;

    .line 509
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 515
    :pswitch_4
    invoke-virtual {p0}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v1

    :goto_2
    if-ge v0, v1, :cond_0

    .line 516
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/e/n;->f(I)Lcom/google/android/libraries/social/e/r;

    move-result-object v2

    iget-wide v4, v2, Lcom/google/android/libraries/social/e/r;->a:J

    long-to-int v3, v4

    invoke-virtual {p1, v3}, Lcom/google/android/libraries/social/e/q;->a(I)Lcom/google/android/libraries/social/e/q;

    iget-wide v2, v2, Lcom/google/android/libraries/social/e/r;->b:J

    long-to-int v2, v2

    invoke-virtual {p1, v2}, Lcom/google/android/libraries/social/e/q;->a(I)Lcom/google/android/libraries/social/e/q;

    .line 515
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 526
    :pswitch_5
    invoke-virtual {p0}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v1

    :goto_3
    if-ge v0, v1, :cond_0

    .line 527
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/e/n;->e(I)J

    move-result-wide v2

    long-to-int v2, v2

    int-to-short v2, v2

    invoke-virtual {p1, v2}, Lcom/google/android/libraries/social/e/q;->a(S)Lcom/google/android/libraries/social/e/q;

    .line 526
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 496
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static a(Lcom/google/android/libraries/social/e/o;Lcom/google/android/libraries/social/e/q;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v0, 0x0

    .line 291
    invoke-virtual {p0}, Lcom/google/android/libraries/social/e/o;->b()[Lcom/google/android/libraries/social/e/n;

    move-result-object v3

    .line 292
    array-length v1, v3

    int-to-short v1, v1

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/social/e/q;->a(S)Lcom/google/android/libraries/social/e/q;

    .line 293
    array-length v4, v3

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v1, v3, v2

    .line 294
    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/n;->b()S

    move-result v5

    invoke-virtual {p1, v5}, Lcom/google/android/libraries/social/e/q;->a(S)Lcom/google/android/libraries/social/e/q;

    .line 295
    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/n;->c()S

    move-result v5

    invoke-virtual {p1, v5}, Lcom/google/android/libraries/social/e/q;->a(S)Lcom/google/android/libraries/social/e/q;

    .line 296
    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/n;->e()I

    move-result v5

    invoke-virtual {p1, v5}, Lcom/google/android/libraries/social/e/q;->a(I)Lcom/google/android/libraries/social/e/q;

    .line 300
    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/n;->d()I

    move-result v5

    if-le v5, v6, :cond_1

    .line 301
    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/n;->j()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/social/e/q;->a(I)Lcom/google/android/libraries/social/e/q;

    .line 293
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 303
    :cond_1
    invoke-static {v1, p1}, Lcom/google/android/libraries/social/e/h;->a(Lcom/google/android/libraries/social/e/n;Lcom/google/android/libraries/social/e/q;)V

    .line 304
    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/n;->d()I

    move-result v1

    rsub-int/lit8 v5, v1, 0x4

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_0

    .line 305
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/e/q;->write(I)V

    .line 304
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 309
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/e/o;->e()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/social/e/q;->a(I)Lcom/google/android/libraries/social/e/q;

    .line 310
    array-length v1, v3

    :goto_2
    if-ge v0, v1, :cond_4

    aget-object v2, v3, v0

    .line 311
    invoke-virtual {v2}, Lcom/google/android/libraries/social/e/n;->d()I

    move-result v4

    if-le v4, v6, :cond_3

    .line 312
    invoke-static {v2, p1}, Lcom/google/android/libraries/social/e/h;->a(Lcom/google/android/libraries/social/e/n;Lcom/google/android/libraries/social/e/q;)V

    .line 310
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 315
    :cond_4
    return-void
.end method

.method private a(Lcom/google/android/libraries/social/e/q;)V
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/d;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 264
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/d;->a()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/e/q;->write([B)V

    .line 270
    :cond_0
    return-void

    .line 265
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/e/d;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/d;->c()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 267
    iget-object v1, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/e/d;->a(I)[B

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/social/e/q;->write([B)V

    .line 266
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 331
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v0

    .line 332
    if-nez v0, :cond_0

    .line 333
    new-instance v0, Lcom/google/android/libraries/social/e/o;

    invoke-direct {v0, v2}, Lcom/google/android/libraries/social/e/o;-><init>(I)V

    .line 334
    iget-object v1, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/e/d;->a(Lcom/google/android/libraries/social/e/o;)V

    .line 336
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/social/e/h;->g:Lcom/google/android/libraries/social/e/c;

    sget v3, Lcom/google/android/libraries/social/e/c;->C:I

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/social/e/c;->c(I)Lcom/google/android/libraries/social/e/n;

    move-result-object v1

    .line 337
    if-nez v1, :cond_1

    .line 338
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No definition for crucial exif tag: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/libraries/social/e/c;->C:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 341
    :cond_1
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/e/o;->a(Lcom/google/android/libraries/social/e/n;)Lcom/google/android/libraries/social/e/n;

    .line 344
    iget-object v1, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v1, v4}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v1

    .line 345
    if-nez v1, :cond_2

    .line 346
    new-instance v1, Lcom/google/android/libraries/social/e/o;

    invoke-direct {v1, v4}, Lcom/google/android/libraries/social/e/o;-><init>(I)V

    .line 347
    iget-object v3, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v3, v1}, Lcom/google/android/libraries/social/e/d;->a(Lcom/google/android/libraries/social/e/o;)V

    .line 351
    :cond_2
    iget-object v3, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v3

    .line 352
    if-eqz v3, :cond_4

    .line 353
    iget-object v3, p0, Lcom/google/android/libraries/social/e/h;->g:Lcom/google/android/libraries/social/e/c;

    sget v4, Lcom/google/android/libraries/social/e/c;->D:I

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/social/e/c;->c(I)Lcom/google/android/libraries/social/e/n;

    move-result-object v3

    .line 354
    if-nez v3, :cond_3

    .line 355
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No definition for crucial exif tag: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/libraries/social/e/c;->D:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 358
    :cond_3
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/e/o;->a(Lcom/google/android/libraries/social/e/n;)Lcom/google/android/libraries/social/e/n;

    .line 362
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v0

    .line 363
    if-eqz v0, :cond_6

    .line 364
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->g:Lcom/google/android/libraries/social/e/c;

    sget v3, Lcom/google/android/libraries/social/e/c;->am:I

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/e/c;->c(I)Lcom/google/android/libraries/social/e/n;

    move-result-object v0

    .line 366
    if-nez v0, :cond_5

    .line 367
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No definition for crucial exif tag: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/libraries/social/e/c;->am:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 370
    :cond_5
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/e/o;->a(Lcom/google/android/libraries/social/e/n;)Lcom/google/android/libraries/social/e/n;

    .line 373
    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v0

    .line 376
    iget-object v1, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/d;->b()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 378
    if-nez v0, :cond_7

    .line 379
    new-instance v0, Lcom/google/android/libraries/social/e/o;

    invoke-direct {v0, v5}, Lcom/google/android/libraries/social/e/o;-><init>(I)V

    .line 380
    iget-object v1, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/e/d;->a(Lcom/google/android/libraries/social/e/o;)V

    .line 383
    :cond_7
    iget-object v1, p0, Lcom/google/android/libraries/social/e/h;->g:Lcom/google/android/libraries/social/e/c;

    sget v2, Lcom/google/android/libraries/social/e/c;->E:I

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/e/c;->c(I)Lcom/google/android/libraries/social/e/n;

    move-result-object v1

    .line 385
    if-nez v1, :cond_8

    .line 386
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No definition for crucial exif tag: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/libraries/social/e/c;->E:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 390
    :cond_8
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/e/o;->a(Lcom/google/android/libraries/social/e/n;)Lcom/google/android/libraries/social/e/n;

    .line 391
    iget-object v1, p0, Lcom/google/android/libraries/social/e/h;->g:Lcom/google/android/libraries/social/e/c;

    sget v2, Lcom/google/android/libraries/social/e/c;->F:I

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/e/c;->c(I)Lcom/google/android/libraries/social/e/n;

    move-result-object v1

    .line 393
    if-nez v1, :cond_9

    .line 394
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No definition for crucial exif tag: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/libraries/social/e/c;->F:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 398
    :cond_9
    iget-object v2, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/e/d;->a()[B

    move-result-object v2

    array-length v2, v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/e/n;->d(I)Z

    .line 399
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/e/o;->a(Lcom/google/android/libraries/social/e/n;)Lcom/google/android/libraries/social/e/n;

    .line 402
    sget v1, Lcom/google/android/libraries/social/e/c;->i:I

    invoke-static {v1}, Lcom/google/android/libraries/social/e/c;->a(I)S

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/e/o;->b(S)V

    .line 403
    sget v1, Lcom/google/android/libraries/social/e/c;->m:I

    invoke-static {v1}, Lcom/google/android/libraries/social/e/c;->a(I)S

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/e/o;->b(S)V

    .line 440
    :cond_a
    :goto_0
    return-void

    .line 404
    :cond_b
    iget-object v1, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/d;->d()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 405
    if-nez v0, :cond_c

    .line 406
    new-instance v0, Lcom/google/android/libraries/social/e/o;

    invoke-direct {v0, v5}, Lcom/google/android/libraries/social/e/o;-><init>(I)V

    .line 407
    iget-object v1, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/e/d;->a(Lcom/google/android/libraries/social/e/o;)V

    .line 409
    :cond_c
    iget-object v1, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/d;->c()I

    move-result v1

    .line 410
    iget-object v3, p0, Lcom/google/android/libraries/social/e/h;->g:Lcom/google/android/libraries/social/e/c;

    sget v4, Lcom/google/android/libraries/social/e/c;->i:I

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/social/e/c;->c(I)Lcom/google/android/libraries/social/e/n;

    move-result-object v3

    .line 411
    if-nez v3, :cond_d

    .line 412
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No definition for crucial exif tag: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/libraries/social/e/c;->i:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 415
    :cond_d
    iget-object v4, p0, Lcom/google/android/libraries/social/e/h;->g:Lcom/google/android/libraries/social/e/c;

    sget v5, Lcom/google/android/libraries/social/e/c;->m:I

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/social/e/c;->c(I)Lcom/google/android/libraries/social/e/n;

    move-result-object v4

    .line 417
    if-nez v4, :cond_e

    .line 418
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No definition for crucial exif tag: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/libraries/social/e/c;->m:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 421
    :cond_e
    new-array v5, v1, [J

    move v1, v2

    .line 422
    :goto_1
    iget-object v2, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/e/d;->c()I

    move-result v2

    if-ge v1, v2, :cond_f

    .line 423
    iget-object v2, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v2, v1}, Lcom/google/android/libraries/social/e/d;->a(I)[B

    move-result-object v2

    array-length v2, v2

    int-to-long v6, v2

    aput-wide v6, v5, v1

    .line 422
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 425
    :cond_f
    invoke-virtual {v4, v5}, Lcom/google/android/libraries/social/e/n;->a([J)Z

    .line 426
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/e/o;->a(Lcom/google/android/libraries/social/e/n;)Lcom/google/android/libraries/social/e/n;

    .line 427
    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/e/o;->a(Lcom/google/android/libraries/social/e/n;)Lcom/google/android/libraries/social/e/n;

    .line 429
    sget v1, Lcom/google/android/libraries/social/e/c;->E:I

    invoke-static {v1}, Lcom/google/android/libraries/social/e/c;->a(I)S

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/e/o;->b(S)V

    .line 430
    sget v1, Lcom/google/android/libraries/social/e/c;->F:I

    invoke-static {v1}, Lcom/google/android/libraries/social/e/c;->a(I)S

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/e/o;->b(S)V

    goto/16 :goto_0

    .line 432
    :cond_10
    if-eqz v0, :cond_a

    .line 434
    sget v1, Lcom/google/android/libraries/social/e/c;->i:I

    invoke-static {v1}, Lcom/google/android/libraries/social/e/c;->a(I)S

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/e/o;->b(S)V

    .line 435
    sget v1, Lcom/google/android/libraries/social/e/c;->m:I

    invoke-static {v1}, Lcom/google/android/libraries/social/e/c;->a(I)S

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/e/o;->b(S)V

    .line 436
    sget v1, Lcom/google/android/libraries/social/e/c;->E:I

    invoke-static {v1}, Lcom/google/android/libraries/social/e/c;->a(I)S

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/e/o;->b(S)V

    .line 437
    sget v1, Lcom/google/android/libraries/social/e/c;->F:I

    invoke-static {v1}, Lcom/google/android/libraries/social/e/c;->a(I)S

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/e/o;->b(S)V

    goto/16 :goto_0
.end method

.method private c()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 443
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v2

    .line 445
    const/16 v0, 0x8

    invoke-static {v2, v0}, Lcom/google/android/libraries/social/e/h;->a(Lcom/google/android/libraries/social/e/o;I)I

    move-result v0

    .line 446
    sget v3, Lcom/google/android/libraries/social/e/c;->C:I

    invoke-static {v3}, Lcom/google/android/libraries/social/e/c;->a(I)S

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/e/o;->a(S)Lcom/google/android/libraries/social/e/n;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/libraries/social/e/n;->d(I)Z

    .line 448
    iget-object v3, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v3

    .line 449
    invoke-static {v3, v0}, Lcom/google/android/libraries/social/e/h;->a(Lcom/google/android/libraries/social/e/o;I)I

    move-result v0

    .line 451
    iget-object v4, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v4

    .line 452
    if-eqz v4, :cond_0

    .line 453
    sget v5, Lcom/google/android/libraries/social/e/c;->am:I

    invoke-static {v5}, Lcom/google/android/libraries/social/e/c;->a(I)S

    move-result v5

    invoke-virtual {v3, v5}, Lcom/google/android/libraries/social/e/o;->a(S)Lcom/google/android/libraries/social/e/n;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/libraries/social/e/n;->d(I)Z

    .line 455
    invoke-static {v4, v0}, Lcom/google/android/libraries/social/e/h;->a(Lcom/google/android/libraries/social/e/o;I)I

    move-result v0

    .line 458
    :cond_0
    iget-object v3, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v3

    .line 459
    if-eqz v3, :cond_1

    .line 460
    sget v4, Lcom/google/android/libraries/social/e/c;->D:I

    invoke-static {v4}, Lcom/google/android/libraries/social/e/c;->a(I)S

    move-result v4

    invoke-virtual {v2, v4}, Lcom/google/android/libraries/social/e/o;->a(S)Lcom/google/android/libraries/social/e/n;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/android/libraries/social/e/n;->d(I)Z

    .line 461
    invoke-static {v3, v0}, Lcom/google/android/libraries/social/e/h;->a(Lcom/google/android/libraries/social/e/o;I)I

    move-result v0

    .line 464
    :cond_1
    iget-object v3, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/social/e/d;->b(I)Lcom/google/android/libraries/social/e/o;

    move-result-object v3

    .line 465
    if-eqz v3, :cond_2

    .line 466
    invoke-virtual {v2, v0}, Lcom/google/android/libraries/social/e/o;->a(I)V

    .line 467
    invoke-static {v3, v0}, Lcom/google/android/libraries/social/e/h;->a(Lcom/google/android/libraries/social/e/o;I)I

    move-result v0

    .line 471
    :cond_2
    iget-object v2, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/e/d;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 472
    sget v1, Lcom/google/android/libraries/social/e/c;->E:I

    invoke-static {v1}, Lcom/google/android/libraries/social/e/c;->a(I)S

    move-result v1

    invoke-virtual {v3, v1}, Lcom/google/android/libraries/social/e/o;->a(S)Lcom/google/android/libraries/social/e/n;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/e/n;->d(I)Z

    .line 474
    iget-object v1, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/e/d;->a()[B

    move-result-object v1

    array-length v1, v1

    add-int/2addr v1, v0

    .line 485
    :goto_0
    return v1

    .line 475
    :cond_3
    iget-object v2, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/e/d;->d()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 476
    iget-object v2, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/e/d;->c()I

    move-result v2

    .line 477
    new-array v2, v2, [J

    move v6, v1

    move v1, v0

    move v0, v6

    .line 478
    :goto_1
    iget-object v4, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v4}, Lcom/google/android/libraries/social/e/d;->c()I

    move-result v4

    if-ge v0, v4, :cond_4

    .line 479
    int-to-long v4, v1

    aput-wide v4, v2, v0

    .line 480
    iget-object v4, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    invoke-virtual {v4, v0}, Lcom/google/android/libraries/social/e/d;->a(I)[B

    move-result-object v4

    array-length v4, v4

    add-int/2addr v1, v4

    .line 478
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 482
    :cond_4
    sget v0, Lcom/google/android/libraries/social/e/c;->i:I

    invoke-static {v0}, Lcom/google/android/libraries/social/e/c;->a(I)S

    move-result v0

    invoke-virtual {v3, v0}, Lcom/google/android/libraries/social/e/o;->a(S)Lcom/google/android/libraries/social/e/n;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/e/n;->a([J)Z

    goto :goto_0

    :cond_5
    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/google/android/libraries/social/e/d;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/libraries/social/e/h;->a:Lcom/google/android/libraries/social/e/d;

    .line 100
    return-void
.end method

.method public final write(I)V
    .locals 3

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->e:[B

    const/4 v1, 0x0

    and-int/lit16 v2, p1, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 202
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->e:[B

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/e/h;->write([B)V

    .line 203
    return-void
.end method

.method public final write([B)V
    .locals 2

    .prologue
    .line 210
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/libraries/social/e/h;->write([BII)V

    .line 211
    return-void
.end method

.method public final write([BII)V
    .locals 6

    .prologue
    const v5, 0xffff

    const/4 v4, 0x4

    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 123
    :goto_0
    iget v0, p0, Lcom/google/android/libraries/social/e/h;->c:I

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/google/android/libraries/social/e/h;->d:I

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/google/android/libraries/social/e/h;->b:I

    if-eq v0, v2, :cond_b

    :cond_0
    if-lez p3, :cond_b

    .line 125
    iget v0, p0, Lcom/google/android/libraries/social/e/h;->c:I

    if-lez v0, :cond_1

    .line 126
    iget v0, p0, Lcom/google/android/libraries/social/e/h;->c:I

    if-le p3, v0, :cond_4

    iget v0, p0, Lcom/google/android/libraries/social/e/h;->c:I

    .line 127
    :goto_1
    sub-int/2addr p3, v0

    .line 128
    iget v1, p0, Lcom/google/android/libraries/social/e/h;->c:I

    sub-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/libraries/social/e/h;->c:I

    .line 129
    add-int/2addr p2, v0

    .line 131
    :cond_1
    iget v0, p0, Lcom/google/android/libraries/social/e/h;->d:I

    if-lez v0, :cond_2

    .line 132
    iget v0, p0, Lcom/google/android/libraries/social/e/h;->d:I

    if-le p3, v0, :cond_5

    iget v0, p0, Lcom/google/android/libraries/social/e/h;->d:I

    .line 133
    :goto_2
    iget-object v1, p0, Lcom/google/android/libraries/social/e/h;->out:Ljava/io/OutputStream;

    invoke-virtual {v1, p1, p2, v0}, Ljava/io/OutputStream;->write([BII)V

    .line 134
    sub-int/2addr p3, v0

    .line 135
    iget v1, p0, Lcom/google/android/libraries/social/e/h;->d:I

    sub-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/libraries/social/e/h;->d:I

    .line 136
    add-int/2addr p2, v0

    .line 138
    :cond_2
    if-nez p3, :cond_6

    .line 193
    :cond_3
    :goto_3
    return-void

    :cond_4
    move v0, p3

    .line 126
    goto :goto_1

    :cond_5
    move v0, p3

    .line 132
    goto :goto_2

    .line 141
    :cond_6
    iget v0, p0, Lcom/google/android/libraries/social/e/h;->b:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 143
    :pswitch_0
    invoke-direct {p0, v2, p1, p2, p3}, Lcom/google/android/libraries/social/e/h;->a(I[BII)I

    move-result v0

    .line 144
    add-int/2addr p2, v0

    .line 145
    sub-int/2addr p3, v0

    .line 146
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-lt v0, v2, :cond_3

    .line 149
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 150
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    const/16 v1, -0x28

    if-eq v0, v1, :cond_7

    .line 151
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Not a valid jpeg image, cannot write exif"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 153
    :cond_7
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->out:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/google/android/libraries/social/e/h;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 154
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/social/e/h;->b:I

    .line 155
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 156
    invoke-direct {p0}, Lcom/google/android/libraries/social/e/h;->a()V

    goto :goto_0

    .line 161
    :pswitch_1
    invoke-direct {p0, v4, p1, p2, p3}, Lcom/google/android/libraries/social/e/h;->a(I[BII)I

    move-result v0

    .line 162
    add-int/2addr p2, v0

    .line 163
    sub-int/2addr p3, v0

    .line 165
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-ne v0, v2, :cond_8

    .line 166
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    .line 167
    const/16 v1, -0x27

    if-ne v0, v1, :cond_8

    .line 168
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->out:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/google/android/libraries/social/e/h;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 169
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 172
    :cond_8
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-lt v0, v4, :cond_3

    .line 175
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 176
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    .line 177
    const/16 v1, -0x1f

    if-ne v0, v1, :cond_9

    .line 178
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    and-int/2addr v0, v5

    add-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/android/libraries/social/e/h;->c:I

    .line 179
    iput v2, p0, Lcom/google/android/libraries/social/e/h;->b:I

    .line 187
    :goto_4
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    goto/16 :goto_0

    .line 180
    :cond_9
    invoke-static {v0}, Lcom/google/android/libraries/social/e/p;->a(S)Z

    move-result v0

    if-nez v0, :cond_a

    .line 181
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->out:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/google/android/libraries/social/e/h;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {v0, v1, v3, v4}, Ljava/io/OutputStream;->write([BII)V

    .line 182
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    and-int/2addr v0, v5

    add-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/android/libraries/social/e/h;->d:I

    goto :goto_4

    .line 184
    :cond_a
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->out:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/google/android/libraries/social/e/h;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {v0, v1, v3, v4}, Ljava/io/OutputStream;->write([BII)V

    .line 185
    iput v2, p0, Lcom/google/android/libraries/social/e/h;->b:I

    goto :goto_4

    .line 190
    :cond_b
    if-lez p3, :cond_3

    .line 191
    iget-object v0, p0, Lcom/google/android/libraries/social/e/h;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    goto/16 :goto_3

    .line 141
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
