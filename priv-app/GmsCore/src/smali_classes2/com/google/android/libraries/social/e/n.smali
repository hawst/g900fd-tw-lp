.class public final Lcom/google/android/libraries/social/e/n;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/nio/charset/Charset;

.field private static final b:[I

.field private static final j:Ljava/text/SimpleDateFormat;


# instance fields
.field private final c:S

.field private final d:S

.field private e:Z

.field private f:I

.field private g:I

.field private h:Ljava/lang/Object;

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x2

    const/4 v3, 0x4

    const/4 v2, 0x1

    .line 73
    const-string v0, "US-ASCII"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/e/n;->a:Ljava/nio/charset/Charset;

    .line 74
    const/16 v0, 0xb

    new-array v0, v0, [I

    .line 81
    sput-object v0, Lcom/google/android/libraries/social/e/n;->b:[I

    aput v2, v0, v2

    .line 82
    sget-object v0, Lcom/google/android/libraries/social/e/n;->b:[I

    aput v2, v0, v4

    .line 83
    sget-object v0, Lcom/google/android/libraries/social/e/n;->b:[I

    const/4 v1, 0x3

    aput v4, v0, v1

    .line 84
    sget-object v0, Lcom/google/android/libraries/social/e/n;->b:[I

    aput v3, v0, v3

    .line 85
    sget-object v0, Lcom/google/android/libraries/social/e/n;->b:[I

    const/4 v1, 0x5

    aput v5, v0, v1

    .line 86
    sget-object v0, Lcom/google/android/libraries/social/e/n;->b:[I

    const/4 v1, 0x7

    aput v2, v0, v1

    .line 87
    sget-object v0, Lcom/google/android/libraries/social/e/n;->b:[I

    const/16 v1, 0x9

    aput v3, v0, v1

    .line 88
    sget-object v0, Lcom/google/android/libraries/social/e/n;->b:[I

    const/16 v1, 0xa

    aput v5, v0, v1

    .line 108
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy:MM:dd kk:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/social/e/n;->j:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method constructor <init>(SSIIZ)V
    .locals 1

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    iput-short p1, p0, Lcom/google/android/libraries/social/e/n;->c:S

    .line 133
    iput-short p2, p0, Lcom/google/android/libraries/social/e/n;->d:S

    .line 134
    iput p3, p0, Lcom/google/android/libraries/social/e/n;->f:I

    .line 135
    iput-boolean p5, p0, Lcom/google/android/libraries/social/e/n;->e:Z

    .line 136
    iput p4, p0, Lcom/google/android/libraries/social/e/n;->g:I

    .line 137
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    .line 138
    return-void
.end method

.method public static a(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 114
    if-eqz p0, :cond_0

    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    const/4 v1, 0x3

    if-eq p0, v1, :cond_0

    const/4 v1, 0x4

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(S)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 123
    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    const/4 v1, 0x3

    if-eq p0, v1, :cond_0

    const/4 v1, 0x4

    if-eq p0, v1, :cond_0

    const/4 v1, 0x5

    if-eq p0, v1, :cond_0

    const/4 v1, 0x7

    if-eq p0, v1, :cond_0

    const/16 v1, 0x9

    if-eq p0, v1, :cond_0

    const/16 v1, 0xa

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(S)Ljava/lang/String;
    .locals 1

    .prologue
    .line 906
    packed-switch p0, :pswitch_data_0

    .line 924
    :pswitch_0
    const-string v0, ""

    :goto_0
    return-object v0

    .line 908
    :pswitch_1
    const-string v0, "UNSIGNED_BYTE"

    goto :goto_0

    .line 910
    :pswitch_2
    const-string v0, "ASCII"

    goto :goto_0

    .line 912
    :pswitch_3
    const-string v0, "UNSIGNED_SHORT"

    goto :goto_0

    .line 914
    :pswitch_4
    const-string v0, "UNSIGNED_LONG"

    goto :goto_0

    .line 916
    :pswitch_5
    const-string v0, "UNSIGNED_RATIONAL"

    goto :goto_0

    .line 918
    :pswitch_6
    const-string v0, "UNDEFINED"

    goto :goto_0

    .line 920
    :pswitch_7
    const-string v0, "LONG"

    goto :goto_0

    .line 922
    :pswitch_8
    const-string v0, "RATIONAL"

    goto :goto_0

    .line 906
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private h(I)Z
    .locals 1

    .prologue
    .line 899
    iget-boolean v0, p0, Lcom/google/android/libraries/social/e/n;->e:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/libraries/social/e/n;->f:I

    if-eq v0, p1, :cond_0

    .line 900
    const/4 v0, 0x1

    .line 902
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/google/android/libraries/social/e/n;->g:I

    return v0
.end method

.method protected final a(Z)V
    .locals 0

    .prologue
    .line 891
    iput-boolean p1, p0, Lcom/google/android/libraries/social/e/n;->e:Z

    .line 892
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const v5, 0xffff

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 466
    if-nez p1, :cond_1

    .line 521
    :cond_0
    :goto_0
    return v1

    .line 468
    :cond_1
    instance-of v0, p1, Ljava/lang/Short;

    if-eqz v0, :cond_2

    .line 469
    check-cast p1, Ljava/lang/Short;

    invoke-virtual {p1}, Ljava/lang/Short;->shortValue()S

    move-result v0

    and-int/2addr v0, v5

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/e/n;->d(I)Z

    move-result v1

    goto :goto_0

    .line 470
    :cond_2
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 471
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/e/n;->a(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0

    .line 472
    :cond_3
    instance-of v0, p1, [I

    if-eqz v0, :cond_4

    .line 473
    check-cast p1, [I

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/e/n;->a([I)Z

    move-result v1

    goto :goto_0

    .line 474
    :cond_4
    instance-of v0, p1, [J

    if-eqz v0, :cond_5

    .line 475
    check-cast p1, [J

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/e/n;->a([J)Z

    move-result v1

    goto :goto_0

    .line 476
    :cond_5
    instance-of v0, p1, Lcom/google/android/libraries/social/e/r;

    if-eqz v0, :cond_6

    .line 477
    check-cast p1, Lcom/google/android/libraries/social/e/r;

    new-array v0, v4, [Lcom/google/android/libraries/social/e/r;

    aput-object p1, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/e/n;->a([Lcom/google/android/libraries/social/e/r;)Z

    move-result v1

    goto :goto_0

    .line 478
    :cond_6
    instance-of v0, p1, [Lcom/google/android/libraries/social/e/r;

    if-eqz v0, :cond_7

    .line 479
    check-cast p1, [Lcom/google/android/libraries/social/e/r;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/e/n;->a([Lcom/google/android/libraries/social/e/r;)Z

    move-result v1

    goto :goto_0

    .line 480
    :cond_7
    instance-of v0, p1, [B

    if-eqz v0, :cond_8

    .line 481
    check-cast p1, [B

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/e/n;->a([B)Z

    move-result v1

    goto :goto_0

    .line 482
    :cond_8
    instance-of v0, p1, Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 483
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/e/n;->d(I)Z

    move-result v1

    goto :goto_0

    .line 484
    :cond_9
    instance-of v0, p1, Ljava/lang/Long;

    if-eqz v0, :cond_a

    .line 485
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    new-array v0, v4, [J

    aput-wide v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/e/n;->a([J)Z

    move-result v1

    goto :goto_0

    .line 486
    :cond_a
    instance-of v0, p1, Ljava/lang/Byte;

    if-eqz v0, :cond_b

    .line 487
    check-cast p1, Ljava/lang/Byte;

    invoke-virtual {p1}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    new-array v2, v4, [B

    aput-byte v0, v2, v1

    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/e/n;->a([B)Z

    move-result v1

    goto/16 :goto_0

    .line 488
    :cond_b
    instance-of v0, p1, [Ljava/lang/Short;

    if-eqz v0, :cond_e

    .line 490
    check-cast p1, [Ljava/lang/Short;

    .line 491
    array-length v0, p1

    new-array v3, v0, [I

    move v0, v1

    .line 492
    :goto_1
    array-length v2, p1

    if-ge v0, v2, :cond_d

    .line 493
    aget-object v2, p1, v0

    if-nez v2, :cond_c

    move v2, v1

    :goto_2
    aput v2, v3, v0

    .line 492
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 493
    :cond_c
    aget-object v2, p1, v0

    invoke-virtual {v2}, Ljava/lang/Short;->shortValue()S

    move-result v2

    and-int/2addr v2, v5

    goto :goto_2

    .line 495
    :cond_d
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/e/n;->a([I)Z

    move-result v1

    goto/16 :goto_0

    .line 496
    :cond_e
    instance-of v0, p1, [Ljava/lang/Integer;

    if-eqz v0, :cond_11

    .line 498
    check-cast p1, [Ljava/lang/Integer;

    .line 499
    array-length v0, p1

    new-array v3, v0, [I

    move v0, v1

    .line 500
    :goto_3
    array-length v2, p1

    if-ge v0, v2, :cond_10

    .line 501
    aget-object v2, p1, v0

    if-nez v2, :cond_f

    move v2, v1

    :goto_4
    aput v2, v3, v0

    .line 500
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 501
    :cond_f
    aget-object v2, p1, v0

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_4

    .line 503
    :cond_10
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/e/n;->a([I)Z

    move-result v1

    goto/16 :goto_0

    .line 504
    :cond_11
    instance-of v0, p1, [Ljava/lang/Long;

    if-eqz v0, :cond_14

    .line 506
    check-cast p1, [Ljava/lang/Long;

    .line 507
    array-length v0, p1

    new-array v0, v0, [J

    .line 508
    :goto_5
    array-length v2, p1

    if-ge v1, v2, :cond_13

    .line 509
    aget-object v2, p1, v1

    if-nez v2, :cond_12

    const-wide/16 v2, 0x0

    :goto_6
    aput-wide v2, v0, v1

    .line 508
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 509
    :cond_12
    aget-object v2, p1, v1

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto :goto_6

    .line 511
    :cond_13
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/e/n;->a([J)Z

    move-result v1

    goto/16 :goto_0

    .line 512
    :cond_14
    instance-of v0, p1, [Ljava/lang/Byte;

    if-eqz v0, :cond_0

    .line 514
    check-cast p1, [Ljava/lang/Byte;

    .line 515
    array-length v0, p1

    new-array v3, v0, [B

    move v0, v1

    .line 516
    :goto_7
    array-length v2, p1

    if-ge v0, v2, :cond_16

    .line 517
    aget-object v2, p1, v0

    if-nez v2, :cond_15

    move v2, v1

    :goto_8
    aput-byte v2, v3, v0

    .line 516
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 517
    :cond_15
    aget-object v2, p1, v0

    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    goto :goto_8

    .line 519
    :cond_16
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/e/n;->a([B)Z

    move-result v1

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 331
    iget-short v0, p0, Lcom/google/android/libraries/social/e/n;->d:S

    if-eq v0, v4, :cond_0

    iget-short v0, p0, Lcom/google/android/libraries/social/e/n;->d:S

    const/4 v3, 0x7

    if-eq v0, v3, :cond_0

    move v0, v1

    .line 354
    :goto_0
    return v0

    .line 335
    :cond_0
    sget-object v0, Lcom/google/android/libraries/social/e/n;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 337
    array-length v3, v0

    if-lez v3, :cond_3

    .line 338
    iget-short v3, p0, Lcom/google/android/libraries/social/e/n;->d:S

    if-ne v3, v4, :cond_1

    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    aget-byte v3, v0, v3

    if-eqz v3, :cond_1

    .line 339
    iget-boolean v3, p0, Lcom/google/android/libraries/social/e/n;->e:Z

    if-eqz v3, :cond_2

    array-length v3, v0

    iget v4, p0, Lcom/google/android/libraries/social/e/n;->f:I

    if-ne v3, v4, :cond_2

    .line 340
    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    aput-byte v1, v0, v3

    .line 348
    :cond_1
    :goto_1
    array-length v3, v0

    .line 349
    invoke-direct {p0, v3}, Lcom/google/android/libraries/social/e/n;->h(I)Z

    move-result v4

    if-eqz v4, :cond_4

    move v0, v1

    .line 350
    goto :goto_0

    .line 342
    :cond_2
    array-length v3, v0

    add-int/lit8 v3, v3, 0x1

    invoke-static {v0, v3}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    goto :goto_1

    .line 345
    :cond_3
    iget-short v3, p0, Lcom/google/android/libraries/social/e/n;->d:S

    if-ne v3, v4, :cond_1

    iget v3, p0, Lcom/google/android/libraries/social/e/n;->f:I

    if-ne v3, v2, :cond_1

    .line 346
    new-array v0, v2, [B

    aput-byte v1, v0, v1

    goto :goto_1

    .line 352
    :cond_4
    iput v3, p0, Lcom/google/android/libraries/social/e/n;->f:I

    .line 353
    iput-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    move v0, v2

    .line 354
    goto :goto_0
.end method

.method public final a([B)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 436
    array-length v2, p1

    invoke-direct {p0, v2}, Lcom/google/android/libraries/social/e/n;->h(I)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-short v3, p0, Lcom/google/android/libraries/social/e/n;->d:S

    if-eq v3, v1, :cond_2

    iget-short v3, p0, Lcom/google/android/libraries/social/e/n;->d:S

    const/4 v4, 0x7

    if-ne v3, v4, :cond_0

    :cond_2
    new-array v3, v2, [B

    iput-object v3, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    invoke-static {p1, v0, v3, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput v2, p0, Lcom/google/android/libraries/social/e/n;->f:I

    move v0, v1

    goto :goto_0
.end method

.method public final a([I)Z
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v4, 0x3

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 240
    array-length v2, p1

    invoke-direct {p0, v2}, Lcom/google/android/libraries/social/e/n;->h(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 259
    :cond_0
    :goto_0
    return v0

    .line 243
    :cond_1
    iget-short v2, p0, Lcom/google/android/libraries/social/e/n;->d:S

    if-eq v2, v4, :cond_2

    iget-short v2, p0, Lcom/google/android/libraries/social/e/n;->d:S

    const/16 v3, 0x9

    if-eq v2, v3, :cond_2

    iget-short v2, p0, Lcom/google/android/libraries/social/e/n;->d:S

    if-ne v2, v6, :cond_0

    .line 247
    :cond_2
    iget-short v2, p0, Lcom/google/android/libraries/social/e/n;->d:S

    if-ne v2, v4, :cond_4

    array-length v3, p1

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_7

    aget v4, p1, v2

    const v5, 0xffff

    if-gt v4, v5, :cond_3

    if-gez v4, :cond_6

    :cond_3
    move v2, v1

    :goto_2
    if-nez v2, :cond_0

    .line 249
    :cond_4
    iget-short v2, p0, Lcom/google/android/libraries/social/e/n;->d:S

    if-ne v2, v6, :cond_5

    array-length v3, p1

    move v2, v0

    :goto_3
    if-ge v2, v3, :cond_9

    aget v4, p1, v2

    if-gez v4, :cond_8

    move v2, v1

    :goto_4
    if-nez v2, :cond_0

    .line 253
    :cond_5
    array-length v2, p1

    new-array v2, v2, [J

    .line 254
    :goto_5
    array-length v3, p1

    if-ge v0, v3, :cond_a

    .line 255
    aget v3, p1, v0

    int-to-long v4, v3

    aput-wide v4, v2, v0

    .line 254
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 247
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_7
    move v2, v0

    goto :goto_2

    .line 249
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_9
    move v2, v0

    goto :goto_4

    .line 257
    :cond_a
    iput-object v2, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    .line 258
    array-length v0, p1

    iput v0, p0, Lcom/google/android/libraries/social/e/n;->f:I

    move v0, v1

    .line 259
    goto :goto_0
.end method

.method public final a([J)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 290
    array-length v2, p1

    invoke-direct {p0, v2}, Lcom/google/android/libraries/social/e/n;->h(I)Z

    move-result v2

    if-nez v2, :cond_0

    iget-short v2, p0, Lcom/google/android/libraries/social/e/n;->d:S

    const/4 v3, 0x4

    if-eq v2, v3, :cond_1

    .line 298
    :cond_0
    :goto_0
    return v0

    .line 293
    :cond_1
    array-length v3, p1

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_4

    aget-wide v4, p1, v2

    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-ltz v6, :cond_2

    const-wide v6, 0xffffffffL

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    :cond_2
    move v2, v1

    :goto_2
    if-nez v2, :cond_0

    .line 296
    iput-object p1, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    .line 297
    array-length v0, p1

    iput v0, p0, Lcom/google/android/libraries/social/e/n;->f:I

    move v0, v1

    .line 298
    goto :goto_0

    .line 293
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    move v2, v0

    goto :goto_2
.end method

.method public final a([Lcom/google/android/libraries/social/e/r;)Z
    .locals 14

    .prologue
    const-wide/32 v12, -0x80000000

    const/16 v10, 0xa

    const/4 v3, 0x5

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 372
    array-length v2, p1

    invoke-direct {p0, v2}, Lcom/google/android/libraries/social/e/n;->h(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 386
    :cond_0
    :goto_0
    return v0

    .line 375
    :cond_1
    iget-short v2, p0, Lcom/google/android/libraries/social/e/n;->d:S

    if-eq v2, v3, :cond_2

    iget-short v2, p0, Lcom/google/android/libraries/social/e/n;->d:S

    if-ne v2, v10, :cond_0

    .line 378
    :cond_2
    iget-short v2, p0, Lcom/google/android/libraries/social/e/n;->d:S

    if-ne v2, v3, :cond_4

    array-length v3, p1

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_8

    aget-object v4, p1, v2

    iget-wide v6, v4, Lcom/google/android/libraries/social/e/r;->a:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-ltz v5, :cond_3

    iget-wide v6, v4, Lcom/google/android/libraries/social/e/r;->b:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-ltz v5, :cond_3

    iget-wide v6, v4, Lcom/google/android/libraries/social/e/r;->a:J

    const-wide v8, 0xffffffffL

    cmp-long v5, v6, v8

    if-gtz v5, :cond_3

    iget-wide v4, v4, Lcom/google/android/libraries/social/e/r;->b:J

    const-wide v6, 0xffffffffL

    cmp-long v4, v4, v6

    if-lez v4, :cond_7

    :cond_3
    move v2, v1

    :goto_2
    if-nez v2, :cond_0

    .line 380
    :cond_4
    iget-short v2, p0, Lcom/google/android/libraries/social/e/n;->d:S

    if-ne v2, v10, :cond_6

    array-length v3, p1

    move v2, v0

    :goto_3
    if-ge v2, v3, :cond_a

    aget-object v4, p1, v2

    iget-wide v6, v4, Lcom/google/android/libraries/social/e/r;->a:J

    cmp-long v5, v6, v12

    if-ltz v5, :cond_5

    iget-wide v6, v4, Lcom/google/android/libraries/social/e/r;->b:J

    cmp-long v5, v6, v12

    if-ltz v5, :cond_5

    iget-wide v6, v4, Lcom/google/android/libraries/social/e/r;->a:J

    const-wide/32 v8, 0x7fffffff

    cmp-long v5, v6, v8

    if-gtz v5, :cond_5

    iget-wide v4, v4, Lcom/google/android/libraries/social/e/r;->b:J

    const-wide/32 v6, 0x7fffffff

    cmp-long v4, v4, v6

    if-lez v4, :cond_9

    :cond_5
    move v2, v1

    :goto_4
    if-nez v2, :cond_0

    .line 384
    :cond_6
    iput-object p1, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    .line 385
    array-length v0, p1

    iput v0, p0, Lcom/google/android/libraries/social/e/n;->f:I

    move v0, v1

    .line 386
    goto :goto_0

    .line 378
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_8
    move v2, v0

    goto :goto_2

    .line 380
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_a
    move v2, v0

    goto :goto_4
.end method

.method public final b()S
    .locals 1

    .prologue
    .line 177
    iget-short v0, p0, Lcom/google/android/libraries/social/e/n;->c:S

    return v0
.end method

.method protected final b(I)V
    .locals 0

    .prologue
    .line 170
    iput p1, p0, Lcom/google/android/libraries/social/e/n;->g:I

    .line 171
    return-void
.end method

.method protected final b([B)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 853
    array-length v0, p1

    iget-short v1, p0, Lcom/google/android/libraries/social/e/n;->d:S

    const/4 v2, 0x7

    if-eq v1, v2, :cond_0

    iget-short v1, p0, Lcom/google/android/libraries/social/e/n;->d:S

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get BYTE value from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-short v2, p0, Lcom/google/android/libraries/social/e/n;->d:S

    invoke-static {v2}, Lcom/google/android/libraries/social/e/n;->b(S)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    iget v2, p0, Lcom/google/android/libraries/social/e/n;->f:I

    if-le v0, v2, :cond_1

    iget v0, p0, Lcom/google/android/libraries/social/e/n;->f:I

    :cond_1
    invoke-static {v1, v3, p1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 854
    return-void
.end method

.method public final c()S
    .locals 1

    .prologue
    .line 193
    iget-short v0, p0, Lcom/google/android/libraries/social/e/n;->d:S

    return v0
.end method

.method protected final c(I)V
    .locals 0

    .prologue
    .line 217
    iput p1, p0, Lcom/google/android/libraries/social/e/n;->f:I

    .line 218
    return-void
.end method

.method public final d()I
    .locals 3

    .prologue
    .line 200
    iget v0, p0, Lcom/google/android/libraries/social/e/n;->f:I

    iget-short v1, p0, Lcom/google/android/libraries/social/e/n;->d:S

    sget-object v2, Lcom/google/android/libraries/social/e/n;->b:[I

    aget v1, v2, v1

    mul-int/2addr v0, v1

    return v0
.end method

.method public final d(I)Z
    .locals 2

    .prologue
    .line 274
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p1, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/e/n;->a([I)Z

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 209
    iget v0, p0, Lcom/google/android/libraries/social/e/n;->f:I

    return v0
.end method

.method protected final e(I)J
    .locals 3

    .prologue
    .line 805
    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    instance-of v0, v0, [J

    if-eqz v0, :cond_0

    .line 806
    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    check-cast v0, [J

    aget-wide v0, v0, p1

    .line 808
    :goto_0
    return-wide v0

    .line 807
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_1

    .line 808
    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    check-cast v0, [B

    aget-byte v0, v0, p1

    int-to-long v0, v0

    goto :goto_0

    .line 810
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get integer value from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-short v2, p0, Lcom/google/android/libraries/social/e/n;->d:S

    invoke-static {v2}, Lcom/google/android/libraries/social/e/n;->b(S)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 979
    if-nez p1, :cond_1

    .line 1014
    :cond_0
    :goto_0
    return v0

    .line 982
    :cond_1
    instance-of v1, p1, Lcom/google/android/libraries/social/e/n;

    if-eqz v1, :cond_0

    .line 983
    check-cast p1, Lcom/google/android/libraries/social/e/n;

    .line 984
    iget-short v1, p1, Lcom/google/android/libraries/social/e/n;->c:S

    iget-short v2, p0, Lcom/google/android/libraries/social/e/n;->c:S

    if-ne v1, v2, :cond_0

    iget v1, p1, Lcom/google/android/libraries/social/e/n;->f:I

    iget v2, p0, Lcom/google/android/libraries/social/e/n;->f:I

    if-ne v1, v2, :cond_0

    iget-short v1, p1, Lcom/google/android/libraries/social/e/n;->d:S

    iget-short v2, p0, Lcom/google/android/libraries/social/e/n;->d:S

    if-ne v1, v2, :cond_0

    .line 989
    iget-object v1, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    if-eqz v1, :cond_5

    .line 990
    iget-object v1, p1, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 992
    iget-object v1, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    instance-of v1, v1, [J

    if-eqz v1, :cond_2

    .line 993
    iget-object v1, p1, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    instance-of v1, v1, [J

    if-eqz v1, :cond_0

    .line 996
    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    check-cast v0, [J

    iget-object v1, p1, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    check-cast v1, [J

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v0

    goto :goto_0

    .line 997
    :cond_2
    iget-object v1, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    instance-of v1, v1, [Lcom/google/android/libraries/social/e/r;

    if-eqz v1, :cond_3

    .line 998
    iget-object v1, p1, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    instance-of v1, v1, [Lcom/google/android/libraries/social/e/r;

    if-eqz v1, :cond_0

    .line 1001
    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    check-cast v0, [Lcom/google/android/libraries/social/e/r;

    iget-object v1, p1, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    check-cast v1, [Lcom/google/android/libraries/social/e/r;

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 1002
    :cond_3
    iget-object v1, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    instance-of v1, v1, [B

    if-eqz v1, :cond_4

    .line 1003
    iget-object v1, p1, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    instance-of v1, v1, [B

    if-eqz v1, :cond_0

    .line 1006
    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    check-cast v0, [B

    iget-object v1, p1, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    check-cast v1, [B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    goto :goto_0

    .line 1008
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    iget-object v1, p1, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 1011
    :cond_5
    iget-object v1, p1, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected final f(I)Lcom/google/android/libraries/social/e/r;
    .locals 3

    .prologue
    .line 842
    iget-short v0, p0, Lcom/google/android/libraries/social/e/n;->d:S

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    iget-short v0, p0, Lcom/google/android/libraries/social/e/n;->d:S

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 843
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get RATIONAL value from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-short v2, p0, Lcom/google/android/libraries/social/e/n;->d:S

    invoke-static {v2}, Lcom/google/android/libraries/social/e/n;->b(S)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 846
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    check-cast v0, [Lcom/google/android/libraries/social/e/r;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 549
    iget-object v1, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    if-nez v1, :cond_1

    .line 564
    :cond_0
    :goto_0
    return-object v0

    .line 551
    :cond_1
    iget-object v1, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 552
    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 553
    :cond_2
    iget-object v1, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    instance-of v1, v1, [B

    if-eqz v1, :cond_0

    .line 558
    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    check-cast v0, [B

    .line 559
    array-length v1, v0

    if-lez v1, :cond_3

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-byte v1, v0, v1

    if-nez v1, :cond_3

    .line 560
    new-instance v1, Ljava/lang/String;

    const/4 v2, 0x0

    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    sget-object v4, Lcom/google/android/libraries/social/e/n;->a:Ljava/nio/charset/Charset;

    invoke-direct {v1, v0, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    move-object v0, v1

    goto :goto_0

    .line 562
    :cond_3
    new-instance v1, Ljava/lang/String;

    sget-object v2, Lcom/google/android/libraries/social/e/n;->a:Ljava/nio/charset/Charset;

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    move-object v0, v1

    goto :goto_0
.end method

.method protected final g(I)V
    .locals 0

    .prologue
    .line 887
    iput p1, p0, Lcom/google/android/libraries/social/e/n;->i:I

    .line 888
    return-void
.end method

.method public final h()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 733
    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    return-object v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1021
    iget-short v0, p0, Lcom/google/android/libraries/social/e/n;->c:S

    add-int/lit16 v0, v0, 0x20f

    .line 1023
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/libraries/social/e/n;->f:I

    add-int/2addr v0, v1

    .line 1024
    mul-int/lit8 v0, v0, 0x1f

    iget-short v1, p0, Lcom/google/android/libraries/social/e/n;->d:S

    add-int/2addr v0, v1

    .line 1025
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 1026
    return v0

    .line 1025
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method protected final i()[B
    .locals 1

    .prologue
    .line 832
    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    check-cast v0, [B

    return-object v0
.end method

.method protected final j()I
    .locals 1

    .prologue
    .line 880
    iget v0, p0, Lcom/google/android/libraries/social/e/n;->i:I

    return v0
.end method

.method protected final k()Z
    .locals 1

    .prologue
    .line 895
    iget-boolean v0, p0, Lcom/google/android/libraries/social/e/n;->e:Z

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1031
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tag id: %04X\n"

    new-array v2, v5, [Ljava/lang/Object;

    iget-short v3, p0, Lcom/google/android/libraries/social/e/n;->c:S

    invoke-static {v3}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ifd id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/social/e/n;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\ntype: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-short v1, p0, Lcom/google/android/libraries/social/e/n;->d:S

    invoke-static {v1}, Lcom/google/android/libraries/social/e/n;->b(S)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\ncount: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/social/e/n;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\noffset: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/social/e/n;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nvalue: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_2

    iget-short v0, p0, Lcom/google/android/libraries/social/e/n;->d:S

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/e/n;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    check-cast v0, [B

    invoke-static {v0}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    instance-of v0, v0, [J

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    check-cast v0, [J

    array-length v0, v0

    if-ne v0, v5, :cond_3

    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    check-cast v0, [J

    aget-wide v2, v0, v4

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    check-cast v0, [J

    invoke-static {v0}, Ljava/util/Arrays;->toString([J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    instance-of v0, v0, [Ljava/lang/Object;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    array-length v0, v0

    if-ne v0, v5, :cond_6

    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    aget-object v0, v0, v4

    if-nez v0, :cond_5

    const-string v0, ""

    goto :goto_0

    :cond_5
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/google/android/libraries/social/e/n;->h:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
