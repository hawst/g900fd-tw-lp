.class public Lcom/google/android/libraries/commerce/ocr/cv/BlurDetectorImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/cv/BlurDetector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private native isBlurryNative([BIIIIII)Z
.end method


# virtual methods
.method public isBlurred(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;Landroid/graphics/Rect;)Z
    .locals 8

    .prologue
    .line 19
    if-nez p2, :cond_0

    .line 20
    const/4 v0, 0x1

    .line 23
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getData()[B

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getHeight()I

    move-result v3

    iget v4, p2, Landroid/graphics/Rect;->left:I

    iget v5, p2, Landroid/graphics/Rect;->top:I

    iget v6, p2, Landroid/graphics/Rect;->right:I

    iget v7, p2, Landroid/graphics/Rect;->bottom:I

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/libraries/commerce/ocr/cv/BlurDetectorImpl;->isBlurryNative([BIIIIII)Z

    move-result v0

    goto :goto_0
.end method
