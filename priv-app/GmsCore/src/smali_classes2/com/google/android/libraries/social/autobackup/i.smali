.class public Lcom/google/android/libraries/social/autobackup/i;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/libraries/social/autobackup/i;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/HashSet;

.field private final d:Landroid/os/Handler;

.field private final e:Lcom/google/android/libraries/social/autobackup/au;

.field private final f:Lcom/google/android/libraries/social/autobackup/al;

.field private final g:Lcom/google/android/libraries/social/account/b;

.field private volatile h:Lcom/google/android/libraries/social/autobackup/n;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->c:Ljava/util/HashSet;

    .line 72
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/i;->b:Landroid/content/Context;

    .line 73
    const-class v0, Lcom/google/android/libraries/social/autobackup/au;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/au;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->e:Lcom/google/android/libraries/social/autobackup/au;

    .line 74
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/al;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->f:Lcom/google/android/libraries/social/autobackup/al;

    .line 75
    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->g:Lcom/google/android/libraries/social/account/b;

    .line 77
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "iu-sync-manager"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 79
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 80
    new-instance v1, Lcom/google/android/libraries/social/autobackup/k;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lcom/google/android/libraries/social/autobackup/k;-><init>(Lcom/google/android/libraries/social/autobackup/i;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/libraries/social/autobackup/i;->d:Landroid/os/Handler;

    .line 81
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->d:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 82
    new-instance v0, Lcom/google/android/libraries/social/autobackup/j;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/social/autobackup/j;-><init>(Lcom/google/android/libraries/social/autobackup/i;)V

    .line 91
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/i;->b:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    .line 93
    return-void
.end method

.method static synthetic a(Lcom/google/android/libraries/social/autobackup/i;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->d:Landroid/os/Handler;

    return-object v0
.end method

.method private a(IZ)Lcom/google/android/libraries/social/autobackup/am;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x4

    .line 304
    :goto_0
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/i;->e:Lcom/google/android/libraries/social/autobackup/au;

    invoke-static {v1, p1}, Lcom/google/android/libraries/social/autobackup/ab;->b(Lcom/google/android/libraries/social/autobackup/au;I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v1

    .line 306
    if-nez v1, :cond_a

    .line 307
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/i;->b:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/google/android/libraries/social/autobackup/ab;->a(Landroid/content/Context;I)Z

    .line 308
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/i;->e:Lcom/google/android/libraries/social/autobackup/au;

    invoke-static {v1, p1}, Lcom/google/android/libraries/social/autobackup/ab;->b(Lcom/google/android/libraries/social/autobackup/au;I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v1

    move-object v2, v1

    .line 311
    :goto_1
    if-nez v2, :cond_1

    .line 312
    const-string v1, "iu.SyncManager"

    invoke-static {v1, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 313
    const-string v1, "iu.SyncManager"

    const-string v2, "NEXT; no task"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    :cond_0
    :goto_2
    return-object v0

    .line 318
    :cond_1
    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v1

    .line 319
    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/i;->b:Landroid/content/Context;

    invoke-static {v3, v1}, Lcom/google/android/libraries/social/autobackup/i;->a(Landroid/content/Context;I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 322
    const-string v2, "iu.SyncManager"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 323
    const-string v2, "iu.SyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "invalid account, remove all uploads in DB: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/libraries/social/g/a/f;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    :cond_2
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/i;->e:Lcom/google/android/libraries/social/autobackup/au;

    invoke-static {v2, v1}, Lcom/google/android/libraries/social/autobackup/ab;->a(Lcom/google/android/libraries/social/autobackup/au;I)V

    goto :goto_0

    .line 330
    :cond_3
    new-instance v1, Lcom/google/android/libraries/social/autobackup/am;

    iget-object v3, p0, Lcom/google/android/libraries/social/autobackup/i;->b:Landroid/content/Context;

    invoke-direct {v1, v3, v2}, Lcom/google/android/libraries/social/autobackup/am;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)V

    .line 331
    invoke-static {}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a()Z

    move-result v2

    if-nez v2, :cond_7

    .line 332
    const-string v2, "iu.SyncManager"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 333
    const-string v2, "iu.SyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "--- NEW; skip: no storage; task: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    :cond_4
    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/autobackup/am;->a(I)V

    .line 344
    :cond_5
    :goto_3
    iget v2, v1, Lcom/google/android/libraries/social/autobackup/am;->a:I

    .line 345
    if-eqz p2, :cond_8

    invoke-direct {p0, v2}, Lcom/google/android/libraries/social/autobackup/i;->e(I)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/am;->a()Z

    move-result v3

    if-nez v3, :cond_8

    .line 346
    :cond_6
    const-string v2, "iu.SyncManager"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 347
    const-string v2, "iu.SyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "NEXT; rejected; task: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 337
    :cond_7
    const-string v2, "iu.SyncManager"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 338
    const-string v2, "iu.SyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "--- NEW; upload; task: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 351
    :cond_8
    const/4 v3, -0x1

    if-eq p1, v3, :cond_9

    if-eq v2, p1, :cond_9

    .line 352
    const-string v2, "iu.SyncManager"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 353
    const-string v2, "iu.SyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "NEXT; wrong account; task: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_9
    move-object v0, v1

    .line 357
    goto/16 :goto_2

    :cond_a
    move-object v2, v1

    goto/16 :goto_1
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/i;
    .locals 3

    .prologue
    .line 65
    const-class v1, Lcom/google/android/libraries/social/autobackup/i;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/i;->a:Lcom/google/android/libraries/social/autobackup/i;

    if-nez v0, :cond_0

    .line 66
    new-instance v0, Lcom/google/android/libraries/social/autobackup/i;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/libraries/social/autobackup/i;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/i;->a:Lcom/google/android/libraries/social/autobackup/i;

    .line 68
    :cond_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/i;->a:Lcom/google/android/libraries/social/autobackup/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/libraries/social/autobackup/i;I)V
    .locals 3

    .prologue
    const/16 v2, 0x28

    .line 43
    const-string v0, "iu.SyncManager"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "iu.SyncManager"

    const-string v1, "--- CANCEL upload all"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->e:Lcom/google/android/libraries/social/autobackup/au;

    invoke-static {v0, p1, v2}, Lcom/google/android/libraries/social/autobackup/ab;->a(Lcom/google/android/libraries/social/autobackup/au;II)V

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->h:Lcom/google/android/libraries/social/autobackup/n;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/n;->c()Lcom/google/android/libraries/social/autobackup/am;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v1, v0, Lcom/google/android/libraries/social/autobackup/am;->b:I

    shr-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/am;->b()V

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/google/android/libraries/social/autobackup/i;J)V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->h:Lcom/google/android/libraries/social/autobackup/n;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/n;->c()Lcom/google/android/libraries/social/autobackup/am;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/social/autobackup/am;->a(J)Z

    :cond_0
    return-void
.end method

.method private static a(Landroid/content/Context;I)Z
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v1, 0x0

    .line 540
    if-ne p1, v6, :cond_0

    move v0, v1

    .line 552
    :goto_0
    return v0

    .line 544
    :cond_0
    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p0, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    .line 545
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 546
    const-string v3, "com.google"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 547
    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v5}, Lcom/google/android/libraries/social/account/b;->b(Ljava/lang/String;)I

    move-result v5

    .line 548
    if-eq v5, v6, :cond_1

    if-ne v5, p1, :cond_1

    .line 549
    const/4 v0, 0x1

    goto :goto_0

    .line 546
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 552
    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/libraries/social/autobackup/i;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x1

    .line 43
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->h:Lcom/google/android/libraries/social/autobackup/n;

    :goto_0
    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->d:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v0, -0x1

    invoke-direct {p0, v0, v4}, Lcom/google/android/libraries/social/autobackup/i;->a(IZ)Lcom/google/android/libraries/social/autobackup/am;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v1, v0, Lcom/google/android/libraries/social/autobackup/am;->a:I

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/i;->b:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/google/android/libraries/social/autobackup/i;->a(Landroid/content/Context;I)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "ignore_settings"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v3, "iu.SyncManager"

    invoke-static {v3, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "iu.SyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "REQUEST sync for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->g:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/accounts/Account;

    const-string v3, "com.google"

    invoke-direct {v1, v0, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/aj;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const-string v0, "iu.SyncManager"

    const/4 v2, 0x5

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "iu.SyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "account: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " has been removed ?!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/i;->c:Ljava/util/HashSet;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->c:Ljava/util/HashSet;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->h:Lcom/google/android/libraries/social/autobackup/n;

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/n;->c()Lcom/google/android/libraries/social/autobackup/am;

    move-result-object v1

    if-nez v1, :cond_5

    const-string v0, "iu.SyncManager"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "iu.SyncManager"

    const-string v1, "stop update; session has no current task"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    iget v2, v1, Lcom/google/android/libraries/social/autobackup/am;->a:I

    invoke-direct {p0, v2}, Lcom/google/android/libraries/social/autobackup/i;->e(I)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/am;->a()Z

    move-result v2

    if-nez v2, :cond_8

    :cond_6
    const-string v0, "iu.SyncManager"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "iu.SyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "STOP task: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; task rejected"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/am;->c()V

    goto/16 :goto_1

    :cond_8
    iget v0, v0, Lcom/google/android/libraries/social/autobackup/n;->a:I

    invoke-direct {p0, v0, v4}, Lcom/google/android/libraries/social/autobackup/i;->a(IZ)Lcom/google/android/libraries/social/autobackup/am;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v2, v0, Lcom/google/android/libraries/social/autobackup/am;->b:I

    iget v3, v1, Lcom/google/android/libraries/social/autobackup/am;->b:I

    if-ge v2, v3, :cond_1

    const-string v2, "iu.SyncManager"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "iu.SyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "STOP task: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; higher priority task: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/am;->c()V

    goto/16 :goto_1
.end method

.method static synthetic b(Lcom/google/android/libraries/social/autobackup/i;I)V
    .locals 3

    .prologue
    const/16 v2, 0x1e

    .line 43
    const-string v0, "iu.SyncManager"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "iu.SyncManager"

    const-string v1, "--- CANCEL auto backup uploads"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->e:Lcom/google/android/libraries/social/autobackup/au;

    invoke-static {v0, p1, v2}, Lcom/google/android/libraries/social/autobackup/ab;->a(Lcom/google/android/libraries/social/autobackup/au;II)V

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->h:Lcom/google/android/libraries/social/autobackup/n;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/n;->c()Lcom/google/android/libraries/social/autobackup/am;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v1, v0, Lcom/google/android/libraries/social/autobackup/am;->b:I

    shr-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/am;->b()V

    :cond_1
    return-void
.end method

.method static synthetic c(Lcom/google/android/libraries/social/autobackup/i;I)Lcom/google/android/libraries/social/autobackup/am;
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/autobackup/i;->a(IZ)Lcom/google/android/libraries/social/autobackup/am;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/libraries/social/autobackup/i;)V
    .locals 12

    .prologue
    const/4 v5, 0x2

    const/4 v8, 0x4

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 43
    const-string v0, "iu.SyncManager"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "iu.SyncManager"

    const-string v1, "SYNC; picasa accounts"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/i;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/libraries/social/autobackup/aj;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "auto_upload_enabled"

    aput-object v4, v2, v7

    const-string v4, "auto_upload_account_id"

    aput-object v4, v2, v6

    const-string v4, "instant_share_eventid"

    aput-object v4, v2, v5

    const/4 v4, 0x3

    const-string v5, "instant_share_starttime"

    aput-object v5, v2, v4

    const-string v4, "instant_share_endtime"

    aput-object v4, v2, v8

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v0, "iu.SyncManager"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "iu.SyncManager"

    const-string v1, "failed to query system settings"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "iu.SyncManager"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "iu.SyncManager"

    const-string v1, "no system settings found"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_8

    move v1, v6

    :goto_1
    const/4 v0, 0x1

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v0, 0x2

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x3

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v8, 0x4

    invoke-interface {v2, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    if-eqz v0, :cond_9

    cmp-long v0, v10, v4

    if-ltz v0, :cond_9

    cmp-long v0, v10, v8

    if-gtz v0, :cond_9

    move v0, v6

    :goto_2
    if-nez v1, :cond_5

    if-eqz v0, :cond_7

    :cond_5
    const/4 v0, -0x1

    if-eq v3, v0, :cond_7

    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->b:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/google/android/libraries/social/autobackup/i;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "iu.SyncManager"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "iu.SyncManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "REMOVE sync account: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/i;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/libraries/social/autobackup/aj;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_7
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :cond_8
    move v1, v7

    goto :goto_1

    :cond_9
    move v0, v7

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private d(I)Lcom/google/android/libraries/social/autobackup/am;
    .locals 4

    .prologue
    .line 277
    const-string v0, "AutoBackupSyncManager.getNextSyncTask"

    invoke-static {v0}, Lcom/google/android/libraries/social/mediaupload/t;->a(Ljava/lang/String;)I

    move-result v1

    .line 278
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v2, Lcom/google/android/libraries/social/autobackup/l;

    invoke-direct {v2, p0, p1}, Lcom/google/android/libraries/social/autobackup/l;-><init>(Lcom/google/android/libraries/social/autobackup/i;I)V

    invoke-direct {v0, v2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 281
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/i;->d:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 283
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/am;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 289
    invoke-static {v1}, Lcom/google/android/libraries/social/mediaupload/t;->a(I)V

    .line 291
    :goto_0
    return-object v0

    .line 284
    :catch_0
    move-exception v0

    .line 285
    :try_start_1
    const-string v2, "iu.SyncManager"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 286
    const-string v2, "iu.SyncManager"

    const-string v3, "fail to get next task"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 289
    :cond_0
    invoke-static {v1}, Lcom/google/android/libraries/social/mediaupload/t;->a(I)V

    .line 291
    const/4 v0, 0x0

    goto :goto_0

    .line 289
    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcom/google/android/libraries/social/mediaupload/t;->a(I)V

    throw v0
.end method

.method private e(I)Z
    .locals 3

    .prologue
    .line 533
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/i;->c:Ljava/util/HashSet;

    monitor-enter v1

    .line 534
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->c:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 535
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(I)V
    .locals 6

    .prologue
    .line 96
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->b:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/autobackup/au;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/au;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/au;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 98
    sget-object v1, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lcom/google/android/libraries/social/g/a/d;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/g/a/d;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "upload_account_id == ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 101
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/i;->g:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v1, p1}, Lcom/google/android/libraries/social/account/b;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 102
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/i;->g:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v1, p1}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v1

    .line 103
    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/i;->f:Lcom/google/android/libraries/social/autobackup/al;

    const-string v2, "gaia_id"

    invoke-interface {v1, v2}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/autobackup/al;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    :cond_0
    monitor-exit p0

    return-void

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->d:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 192
    return-void
.end method

.method public final a(Lcom/google/android/libraries/social/autobackup/n;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    .line 116
    invoke-static {p1}, Lcom/google/android/libraries/b/a/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/n;

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->h:Lcom/google/android/libraries/social/autobackup/n;

    .line 118
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/i;->c:Ljava/util/HashSet;

    monitor-enter v1

    .line 119
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 120
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    :try_start_1
    iget v1, p1, Lcom/google/android/libraries/social/autobackup/n;->a:I

    .line 126
    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/libraries/social/autobackup/i;->d(I)Lcom/google/android/libraries/social/autobackup/am;

    move-result-object v2

    .line 127
    if-nez v2, :cond_4

    const/4 v0, -0x1

    .line 130
    :goto_0
    if-eqz v2, :cond_5

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/autobackup/i;->e(I)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/am;->a()Z

    move-result v3

    if-nez v3, :cond_5

    .line 131
    :cond_1
    const-string v0, "iu.SyncManager"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 132
    const-string v0, "iu.SyncManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "SYNC; not accepted; task: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/am;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    :cond_2
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->h:Lcom/google/android/libraries/social/autobackup/n;

    .line 178
    invoke-virtual {p1}, Lcom/google/android/libraries/social/autobackup/n;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 182
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/autobackup/i;->b(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 185
    :cond_3
    iput-object v6, p0, Lcom/google/android/libraries/social/autobackup/i;->h:Lcom/google/android/libraries/social/autobackup/n;

    .line 186
    return-void

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 127
    :cond_4
    :try_start_2
    iget v0, v2, Lcom/google/android/libraries/social/autobackup/am;->a:I

    goto :goto_0

    .line 137
    :cond_5
    invoke-virtual {p1, v2}, Lcom/google/android/libraries/social/autobackup/n;->a(Lcom/google/android/libraries/social/autobackup/am;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 138
    if-eqz v2, :cond_2

    if-ne v1, v0, :cond_2

    .line 139
    :try_start_3
    const-string v0, "iu.SyncManager"

    const/4 v3, 0x4

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 144
    const-string v0, "iu.SyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SYNC; start task: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :cond_6
    iget-object v0, p1, Lcom/google/android/libraries/social/autobackup/n;->b:Landroid/content/SyncResult;

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/social/autobackup/am;->a(Landroid/content/SyncResult;)V

    .line 149
    const-string v0, "iu.SyncManager"

    const/4 v2, 0x4

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 150
    const-string v0, "iu.SyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SYNC; complete; result: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/google/android/libraries/social/autobackup/n;->b:Landroid/content/SyncResult;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 158
    :cond_7
    const/4 v0, 0x0

    :try_start_4
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/autobackup/n;->a(Lcom/google/android/libraries/social/autobackup/am;)Z

    .line 161
    :goto_2
    iget-object v0, p1, Lcom/google/android/libraries/social/autobackup/n;->b:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    cmp-long v0, v2, v8

    if-gtz v0, :cond_8

    iget-object v0, p1, Lcom/google/android/libraries/social/autobackup/n;->b:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    cmp-long v0, v2, v8

    if-gtz v0, :cond_8

    iget-object v0, p1, Lcom/google/android/libraries/social/autobackup/n;->b:Landroid/content/SyncResult;

    iget-wide v2, v0, Landroid/content/SyncResult;->delayUntil:J

    cmp-long v0, v2, v8

    if-lez v0, :cond_0

    .line 164
    :cond_8
    const-string v0, "iu.SyncManager"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 165
    const-string v0, "iu.SyncManager"

    const-string v1, "stop sync session due to io error"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    :cond_9
    invoke-virtual {p1}, Lcom/google/android/libraries/social/autobackup/n;->a()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_1

    .line 185
    :catchall_1
    move-exception v0

    iput-object v6, p0, Lcom/google/android/libraries/social/autobackup/i;->h:Lcom/google/android/libraries/social/autobackup/n;

    throw v0

    .line 152
    :catch_0
    move-exception v0

    .line 153
    :try_start_5
    const-string v2, "iu.SyncManager"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 154
    const-string v2, "iu.SyncManager"

    const-string v3, "SYNC; fail"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 156
    :cond_a
    iget-object v0, p1, Lcom/google/android/libraries/social/autobackup/n;->b:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 158
    const/4 v0, 0x0

    :try_start_6
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/autobackup/n;->a(Lcom/google/android/libraries/social/autobackup/am;)Z

    goto :goto_2

    :catchall_2
    move-exception v0

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/social/autobackup/n;->a(Lcom/google/android/libraries/social/autobackup/am;)Z

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->d:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 197
    return-void
.end method

.method public final b(J)V
    .locals 3

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->d:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 211
    return-void
.end method

.method public final c(I)V
    .locals 3

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/i;->d:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 201
    return-void
.end method
