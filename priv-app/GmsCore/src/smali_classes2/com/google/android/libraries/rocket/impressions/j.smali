.class public final Lcom/google/android/libraries/rocket/impressions/j;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/libraries/rocket/impressions/j;->a:Z

    return-void
.end method

.method private static varargs a(ILjava/lang/String;[Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 40
    const-string v0, "RocketImpressions"

    invoke-static {v0, p0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    if-gt p0, v0, :cond_0

    sget-boolean v0, Lcom/google/android/libraries/rocket/impressions/j;->a:Z

    if-eqz v0, :cond_1

    .line 41
    :cond_0
    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 42
    packed-switch p0, :pswitch_data_0

    .line 59
    const-string v1, "RocketImpressions"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unknown log level "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    const-string v1, "RocketImpressions"

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    :cond_1
    :goto_0
    return-void

    .line 44
    :pswitch_0
    const-string v1, "RocketImpressions"

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 47
    :pswitch_1
    const-string v1, "RocketImpressions"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 50
    :pswitch_2
    const-string v1, "RocketImpressions"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 53
    :pswitch_3
    const-string v1, "RocketImpressions"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 56
    :pswitch_4
    const-string v1, "RocketImpressions"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 42
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method static varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x2

    invoke-static {v0, p0, p1}, Lcom/google/android/libraries/rocket/impressions/j;->a(ILjava/lang/String;[Ljava/lang/Object;)V

    .line 29
    return-void
.end method

.method static varargs b(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x5

    invoke-static {v0, p0, p1}, Lcom/google/android/libraries/rocket/impressions/j;->a(ILjava/lang/String;[Ljava/lang/Object;)V

    .line 33
    return-void
.end method

.method static varargs c(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x6

    invoke-static {v0, p0, p1}, Lcom/google/android/libraries/rocket/impressions/j;->a(ILjava/lang/String;[Ljava/lang/Object;)V

    .line 37
    return-void
.end method
