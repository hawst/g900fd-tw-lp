.class final Lcom/google/android/libraries/commerce/ocr/capture/pipeline/c;
.super Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;
.source "SourceFile"


# direct methods
.method protected constructor <init>(Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;)V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;-><init>(Ljava/util/concurrent/ExecutorService;Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;)V

    .line 24
    return-void
.end method


# virtual methods
.method protected final synthetic processNext(Ljava/util/ArrayList;Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 16
    check-cast p2, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    new-instance v1, Lcom/google/android/libraries/commerce/ocr/cv/SafeCountingPool;

    invoke-direct {v1, p2}, Lcom/google/android/libraries/commerce/ocr/cv/SafeCountingPool;-><init>(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)V

    invoke-virtual {v1}, Lcom/google/android/libraries/commerce/ocr/cv/SafeCountingPool;->useReference()Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    move-result-object v2

    :try_start_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    invoke-virtual {v1}, Lcom/google/android/libraries/commerce/ocr/cv/SafeCountingPool;->useReference()Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    move-result-object v4

    invoke-virtual {p0, v0, v4}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/c;->processNext(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->recycle()V

    throw v0

    :cond_0
    invoke-virtual {v2}, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->recycle()V

    return-void
.end method
