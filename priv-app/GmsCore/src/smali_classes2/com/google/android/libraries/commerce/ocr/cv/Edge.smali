.class public Lcom/google/android/libraries/commerce/ocr/cv/Edge;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final direction:Landroid/graphics/PointF;

.field private final origin:Landroid/graphics/PointF;


# direct methods
.method public constructor <init>(Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/cv/Edge;->origin:Landroid/graphics/PointF;

    .line 25
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/cv/Edge;->direction:Landroid/graphics/PointF;

    .line 26
    return-void
.end method


# virtual methods
.method public getDirection()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/Edge;->direction:Landroid/graphics/PointF;

    return-object v0
.end method

.method public getOrigin()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/Edge;->origin:Landroid/graphics/PointF;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 38
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "origin.x"

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/cv/Edge;->origin:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "origin.y"

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/cv/Edge;->origin:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "direction.x"

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/cv/Edge;->direction:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "direction.y"

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/cv/Edge;->direction:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/bv;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
