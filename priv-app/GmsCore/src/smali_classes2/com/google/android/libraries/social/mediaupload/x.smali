.class final Lcom/google/android/libraries/social/mediaupload/x;
.super Lcom/google/android/libraries/social/mediaupload/r;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;

.field private final j:Landroid/content/Context;

.field private final k:Ljava/lang/String;

.field private l:Le/a/a/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/social/rpc/l;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p2}, Lcom/google/android/libraries/social/mediaupload/r;-><init>(Lcom/google/android/libraries/social/rpc/l;)V

    .line 30
    iput-object p1, p0, Lcom/google/android/libraries/social/mediaupload/x;->j:Landroid/content/Context;

    .line 31
    iput-object p3, p0, Lcom/google/android/libraries/social/mediaupload/x;->k:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 38
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/google/android/libraries/social/mediaupload/r;->c:Lcom/google/android/libraries/social/rpc/l;

    iget-object v2, p0, Lcom/google/android/libraries/social/mediaupload/x;->k:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/libraries/social/rpc/l;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 40
    const-string v1, "Content-Range"

    const-string v2, "bytes */*"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    iget-object v1, p0, Lcom/google/android/libraries/social/mediaupload/x;->j:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/libraries/social/mediaupload/x;->k:Ljava/lang/String;

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/libraries/social/mediaupload/x;->i:Le/a/a/l;

    invoke-static {v1, v2, v3, v0, v4}, Lcom/google/android/libraries/social/n/a;->a(Landroid/content/Context;Ljava/lang/String;ILjava/util/Map;Le/a/a/l;)Le/a/a/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/mediaupload/x;->l:Le/a/a/g;

    .line 44
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/x;->l:Le/a/a/g;

    const-string v1, "PUT"

    invoke-interface {v0, v1}, Le/a/a/g;->a(Ljava/lang/String;)V

    .line 46
    const-string v0, "MediaUploader"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    const-string v0, "MediaUploader"

    const-string v1, "UploadMediaRequest [resume]"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    :cond_0
    return-void
.end method

.method protected final a(Le/a/a/g;)V
    .locals 1

    .prologue
    .line 58
    const-string v0, "Range"

    invoke-interface {p1, v0}, Le/a/a/g;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/mediaupload/x;->a:Ljava/lang/String;

    .line 59
    return-void
.end method

.method protected final b()Le/a/a/g;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/x;->l:Le/a/a/g;

    return-object v0
.end method
