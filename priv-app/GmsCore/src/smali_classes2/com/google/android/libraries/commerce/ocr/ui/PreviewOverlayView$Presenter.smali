.class public interface abstract Lcom/google/android/libraries/commerce/ocr/ui/PreviewOverlayView$Presenter;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract onPreviewLayoutFinalized(Landroid/graphics/Rect;Landroid/graphics/Rect;)V
.end method

.method public abstract renderInvalidCardMessage()V
.end method

.method public abstract setView(Lcom/google/android/libraries/commerce/ocr/ui/PreviewOverlayView;)V
.end method

.method public abstract snapAndPerformOcr()V
.end method
