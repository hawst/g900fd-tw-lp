.class public Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private boundaries:Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;

.field private final cardImage:Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

.field private final expirationDateImage:Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

.field private final rectifiedCard:Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;->boundaries:Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;

    .line 96
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;->rectifiedCard:Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    .line 97
    iput-object p3, p0, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;->expirationDateImage:Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    .line 98
    iput-object p4, p0, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;->cardImage:Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    .line 99
    return-void
.end method


# virtual methods
.method public getBoundaries()Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;->boundaries:Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;

    return-object v0
.end method

.method public getCardImage()Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;->cardImage:Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    return-object v0
.end method

.method public getExpirationDateImage()Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;->expirationDateImage:Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    return-object v0
.end method

.method public getRectifiedCard()Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;->rectifiedCard:Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    return-object v0
.end method

.method public setBoundaries(Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;->boundaries:Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;

    .line 111
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 139
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "boundaries"

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;->boundaries:Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "rectifiedCard"

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;->rectifiedCard:Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/bv;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
