.class final Lcom/google/android/libraries/social/account/impl/h;
.super Lcom/google/android/libraries/social/account/impl/e;
.source "SourceFile"


# instance fields
.field final synthetic f:Lcom/google/android/libraries/social/account/impl/a;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/social/account/impl/a;Landroid/content/SharedPreferences$Editor;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 722
    iput-object p1, p0, Lcom/google/android/libraries/social/account/impl/h;->f:Lcom/google/android/libraries/social/account/impl/a;

    .line 723
    invoke-direct {p0, p1, p2, p3, p6}, Lcom/google/android/libraries/social/account/impl/e;-><init>(Lcom/google/android/libraries/social/account/impl/a;Landroid/content/SharedPreferences$Editor;ILjava/lang/String;)V

    .line 724
    iput-object p4, p0, Lcom/google/android/libraries/social/account/impl/h;->g:Ljava/lang/String;

    .line 725
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/account/impl/h;->h:Ljava/lang/String;

    .line 726
    return-void
.end method


# virtual methods
.method public final c()I
    .locals 4

    .prologue
    .line 730
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/h;->f:Lcom/google/android/libraries/social/account/impl/a;

    monitor-enter v1

    .line 731
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/account/impl/h;->f:Lcom/google/android/libraries/social/account/impl/a;

    iget-object v2, p0, Lcom/google/android/libraries/social/account/impl/h;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/libraries/social/account/impl/h;->h:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/social/account/impl/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 732
    new-instance v0, Lcom/google/android/libraries/social/account/g;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Duplicate account: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/libraries/social/account/impl/h;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/libraries/social/account/g;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 734
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 735
    invoke-super {p0}, Lcom/google/android/libraries/social/account/impl/e;->c()I

    move-result v0

    .line 736
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/h;->f:Lcom/google/android/libraries/social/account/impl/a;

    invoke-static {v1}, Lcom/google/android/libraries/social/account/impl/a;->d(Lcom/google/android/libraries/social/account/impl/a;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 739
    :cond_1
    return v0
.end method
