.class public final Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;
.super Landroid/app/Service;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/a/d;


# static fields
.field private static a:Lcom/google/android/libraries/social/autobackup/m;

.field private static b:Lcom/google/android/libraries/social/a/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 67
    return-void
.end method

.method private static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/m;
    .locals 3

    .prologue
    .line 46
    const-class v1, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->a:Lcom/google/android/libraries/social/autobackup/m;

    if-nez v0, :cond_0

    .line 47
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/social/a/a;->b(Landroid/content/Context;)Lcom/google/android/libraries/social/a/a;

    move-result-object v0

    .line 48
    new-instance v2, Lcom/google/android/libraries/social/a/a;

    invoke-direct {v2, p0, v0}, Lcom/google/android/libraries/social/a/a;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/a/a;)V

    sput-object v2, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->b:Lcom/google/android/libraries/social/a/a;

    .line 49
    new-instance v0, Lcom/google/android/libraries/social/autobackup/m;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/social/autobackup/m;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->a:Lcom/google/android/libraries/social/autobackup/m;

    .line 51
    :cond_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->a:Lcom/google/android/libraries/social/autobackup/m;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 4

    .prologue
    .line 213
    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p0, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    .line 214
    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/account/b;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 216
    :goto_0
    invoke-static {p0}, Lcom/google/android/libraries/social/autobackup/aj;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 217
    new-instance v2, Landroid/accounts/Account;

    const-string v3, "com.google"

    invoke-direct {v2, v0, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    const/4 v0, 0x0

    invoke-static {v2, v1, v0}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 219
    invoke-static {v2, v1}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 221
    invoke-static {p0}, Lcom/google/android/libraries/social/autobackup/i;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/i;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/autobackup/i;->a(I)V

    .line 222
    return-void

    .line 214
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x5460

    const/4 v2, 0x1

    .line 253
    invoke-static {p0}, Lcom/google/android/libraries/social/autobackup/aj;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 254
    const/4 v0, 0x0

    .line 255
    invoke-static {p1, v3}, Landroid/content/ContentResolver;->getPeriodicSyncs(Landroid/accounts/Account;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 256
    if-eqz v1, :cond_1

    .line 257
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/PeriodicSync;

    .line 258
    iget-wide v6, v0, Landroid/content/PeriodicSync;->period:J

    cmp-long v5, v6, v8

    if-nez v5, :cond_0

    move v1, v2

    .line 259
    goto :goto_0

    .line 261
    :cond_0
    iget-object v0, v0, Landroid/content/PeriodicSync;->extras:Landroid/os/Bundle;

    invoke-static {p1, v3, v0}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_1
    move v1, v0

    .line 266
    :cond_2
    if-nez v1, :cond_3

    .line 267
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 268
    const-string v1, "sync_periodic"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 269
    invoke-static {p1, v3, v0, v8, v9}, Landroid/content/ContentResolver;->addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    .line 271
    :cond_3
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 203
    invoke-static {p0}, Lcom/google/android/libraries/social/autobackup/aj;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 204
    new-instance v1, Landroid/accounts/Account;

    const-string v2, "com.google"

    invoke-direct {v1, p1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 206
    invoke-static {p0, v1}, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->a(Landroid/content/Context;Landroid/accounts/Account;)V

    .line 207
    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 226
    invoke-static {p0}, Lcom/google/android/libraries/social/autobackup/aj;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 227
    new-instance v1, Landroid/accounts/Account;

    const-string v2, "com.google"

    invoke-direct {v1, p1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 229
    return-void
.end method

.method static synthetic c(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 32
    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p0, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/account/b;->b(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/b;->c(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/libraries/social/account/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final U_()Lcom/google/android/libraries/social/a/a;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->b:Lcom/google/android/libraries/social/a/a;

    return-object v0
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 56
    invoke-static {p0}, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/m;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method
