.class public Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public cardNumber:Ljava/lang/String;

.field public expirationDateConfidence:D

.field public expirationDateNumDigits:I

.field public expirationDatePredictionStatus:I

.field public expirationMonth:I

.field public expirationYear:I

.field public meanDigitConfidence:D

.field public minDigitConfidence:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clearExpirationDate()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    iput v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->expirationYear:I

    .line 47
    iput v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->expirationMonth:I

    .line 48
    return-void
.end method

.method public getCardNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->cardNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getExpirationDateConfidence()D
    .locals 2

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->expirationDateConfidence:D

    return-wide v0
.end method

.method public getExpirationDateNumDigits()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->expirationDateNumDigits:I

    return v0
.end method

.method public getExpirationDatePredictionStatus()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->expirationDatePredictionStatus:I

    return v0
.end method

.method public getExpirationMonth()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->expirationMonth:I

    return v0
.end method

.method public getExpirationYear()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->expirationYear:I

    return v0
.end method

.method public getMeanDigitConfidence()D
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->meanDigitConfidence:D

    return-wide v0
.end method

.method public getMinDigitConfidence()D
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$CreditCardResult;->minDigitConfidence:D

    return-wide v0
.end method
