.class public Lcom/google/android/libraries/commerce/ocr/capture/StopOnServerLimitDecoratingHandler;
.super Lcom/google/android/libraries/commerce/ocr/capture/DecoratingOcrHandler;
.source "SourceFile"


# static fields
.field private static final TAG:Ljava/lang/String; = "StopOnServerLimitDecoratingHandler"


# instance fields
.field private pipeline:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

.field private final serverErrorLimit:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;I)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/DecoratingOcrHandler;-><init>(Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;)V

    .line 28
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, p2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/StopOnServerLimitDecoratingHandler;->serverErrorLimit:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 29
    return-void
.end method

.method public static create(Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;I)Lcom/google/android/libraries/commerce/ocr/capture/StopOnServerLimitDecoratingHandler;
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/capture/StopOnServerLimitDecoratingHandler;

    invoke-direct {v0, p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/StopOnServerLimitDecoratingHandler;-><init>(Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;I)V

    return-object v0
.end method

.method private stopProcessing()V
    .locals 3

    .prologue
    .line 56
    const-string v0, "StopOnServerLimitDecoratingHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Stopping frame processor: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/capture/StopOnServerLimitDecoratingHandler;->pipeline:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/StopOnServerLimitDecoratingHandler;->pipeline:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/StopOnServerLimitDecoratingHandler;->pipeline:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->shutdown()V

    .line 60
    :cond_0
    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/libraries/commerce/ocr/OcrException;)V
    .locals 1

    .prologue
    .line 37
    if-eqz p1, :cond_0

    .line 38
    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/OcrException;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 52
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/DecoratingOcrHandler;->onError(Lcom/google/android/libraries/commerce/ocr/OcrException;)V

    .line 53
    return-void

    .line 40
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/libraries/commerce/ocr/capture/StopOnServerLimitDecoratingHandler;->stopProcessing()V

    goto :goto_0

    .line 43
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/StopOnServerLimitDecoratingHandler;->serverErrorLimit:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    .line 44
    invoke-direct {p0}, Lcom/google/android/libraries/commerce/ocr/capture/StopOnServerLimitDecoratingHandler;->stopProcessing()V

    goto :goto_0

    .line 38
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setPipeline(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/capture/StopOnServerLimitDecoratingHandler;->pipeline:Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    .line 33
    return-void
.end method
