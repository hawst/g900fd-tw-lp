.class public Lcom/google/android/libraries/commerce/ocr/credit/CreditCardOcrActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment$CreditCardOcrCaptureListener;


# static fields
.field public static final FRAGMENT_TAG:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "CreditCardOcrActivity"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CreditCardOcrActivity-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/commerce/ocr/credit/CreditCardOcrActivity;->FRAGMENT_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    return-void
.end method

.method private isInPortraitMode()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 76
    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/credit/CreditCardOcrActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 33
    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/credit/CreditCardOcrActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "FULLSCREEN_MODE"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 34
    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/libraries/commerce/ocr/R$style;->Theme_Ocr:I

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/commerce/ocr/credit/CreditCardOcrActivity;->setTheme(I)V

    .line 35
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 37
    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/credit/CreditCardOcrActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "LOCK_TO_PORTRAIT_MODE"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 39
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/commerce/ocr/credit/CreditCardOcrActivity;->setRequestedOrientation(I)V

    .line 40
    invoke-direct {p0}, Lcom/google/android/libraries/commerce/ocr/credit/CreditCardOcrActivity;->isInPortraitMode()Z

    move-result v0

    if-nez v0, :cond_2

    .line 60
    :cond_0
    :goto_1
    return-void

    .line 34
    :cond_1
    sget v0, Lcom/google/android/libraries/commerce/ocr/R$style;->Theme_Ocr_Popup:I

    goto :goto_0

    .line 46
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/credit/CreditCardOcrActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 49
    sget v0, Lcom/google/android/libraries/commerce/ocr/R$string;->ocr_cc_scan_card_details:I

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/commerce/ocr/credit/CreditCardOcrActivity;->setTitle(I)V

    .line 51
    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/credit/CreditCardOcrActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/commerce/ocr/credit/CreditCardOcrActivity;->FRAGMENT_TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 53
    :cond_3
    const-string v0, "CreditCardOcrActivity"

    const-string v1, "Creating from savedInstanceState"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;

    invoke-direct {v0}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;-><init>()V

    .line 56
    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/credit/CreditCardOcrActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->setArguments(Landroid/os/Bundle;)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/credit/CreditCardOcrActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    const v2, 0x1020002

    sget-object v3, Lcom/google/android/libraries/commerce/ocr/credit/CreditCardOcrActivity;->FRAGMENT_TAG:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    goto :goto_1
.end method

.method public onCreditCardOcrCaptured(Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 64
    if-nez p1, :cond_0

    .line 65
    invoke-virtual {p0, p2}, Lcom/google/android/libraries/commerce/ocr/credit/CreditCardOcrActivity;->setResult(I)V

    .line 69
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/credit/CreditCardOcrActivity;->finish()V

    .line 70
    return-void

    .line 67
    :cond_0
    invoke-virtual {p0, p2, p1}, Lcom/google/android/libraries/commerce/ocr/credit/CreditCardOcrActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0
.end method
