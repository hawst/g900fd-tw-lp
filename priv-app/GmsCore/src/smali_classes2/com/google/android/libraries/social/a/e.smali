.class public final Lcom/google/android/libraries/social/a/e;
.super Landroid/content/ContextWrapper;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/a/d;


# instance fields
.field private final a:Lcom/google/android/libraries/social/a/a;

.field private b:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 46
    new-instance v0, Lcom/google/android/libraries/social/a/a;

    invoke-direct {v0}, Lcom/google/android/libraries/social/a/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/a/e;->a:Lcom/google/android/libraries/social/a/a;

    .line 47
    return-void
.end method


# virtual methods
.method public final U_()Lcom/google/android/libraries/social/a/a;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/libraries/social/a/e;->a:Lcom/google/android/libraries/social/a/a;

    return-object v0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/a/e;->attachBaseContext(Landroid/content/Context;)V

    .line 51
    iget-object v0, p0, Lcom/google/android/libraries/social/a/e;->a:Lcom/google/android/libraries/social/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;)V

    .line 52
    return-void
.end method

.method public final a(Lcom/google/android/libraries/social/a/a;)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/libraries/social/a/e;->a:Lcom/google/android/libraries/social/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/a/a;->a(Lcom/google/android/libraries/social/a/a;)V

    .line 56
    return-void
.end method

.method public final getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 66
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    iget-object v0, p0, Lcom/google/android/libraries/social/a/e;->b:Landroid/view/LayoutInflater;

    if-nez v0, :cond_0

    .line 68
    invoke-super {p0, p1}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 69
    invoke-virtual {v0, p0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/a/e;->b:Landroid/view/LayoutInflater;

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/a/e;->b:Landroid/view/LayoutInflater;

    .line 74
    :goto_0
    return-object v0

    :cond_1
    invoke-super {p0, p1}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method
