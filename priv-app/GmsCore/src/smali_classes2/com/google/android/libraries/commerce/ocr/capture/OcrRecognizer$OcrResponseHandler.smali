.class public interface abstract Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract onError(Lcom/google/android/libraries/commerce/ocr/OcrException;)V
.end method

.method public abstract onOcrAttempt()Z
.end method

.method public abstract onRecognized(Ljava/lang/Object;)V
.end method

.method public abstract onRecognized(Ljava/lang/Object;Ljava/lang/Object;)V
.end method

.method public abstract onUnrecognized()V
.end method

.method public abstract onUnrecognized(Ljava/lang/Object;)V
.end method
