.class public Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final EXPECTED_CHILD_NUM:I = 0x3


# instance fields
.field private final children:Ljava/util/ArrayList;

.field protected final executor:Ljava/util/concurrent/ExecutorService;

.field private final isShutdown:Ljava/util/concurrent/atomic/AtomicBoolean;

.field protected final processor:Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;


# direct methods
.method protected constructor <init>(Ljava/util/concurrent/ExecutorService;Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->processor:Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;

    .line 38
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->executor:Ljava/util/concurrent/ExecutorService;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->children:Ljava/util/ArrayList;

    .line 40
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->isShutdown:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 41
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->onProcess(Ljava/lang/Object;)V

    return-void
.end method

.method private onProcess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->processor:Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;->isProcessingNeeded(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->processor:Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;->discard(Ljava/lang/Object;)V

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->processor:Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;->process(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 87
    if-eqz v0, :cond_0

    .line 90
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->children:Ljava/util/ArrayList;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->processNext(Ljava/util/ArrayList;Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method protected createProcessingTask(Ljava/lang/Object;)Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory$RetirableRunnable;
    .locals 1

    .prologue
    .line 105
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/b;

    invoke-direct {v0, p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/b;-><init>(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;Ljava/lang/Object;)V

    return-object v0
.end method

.method public isShutdown()Z
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->isShutdown:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method pipeTo(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->children:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    return-void
.end method

.method public final process(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->isShutdown:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    :goto_0
    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->executor:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_1

    .line 74
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->executor:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->createProcessingTask(Ljava/lang/Object;)Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory$RetirableRunnable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 76
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->onProcess(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected processNext(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 101
    invoke-virtual {p1, p2}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->process(Ljava/lang/Object;)V

    .line 102
    return-void
.end method

.method protected processNext(Ljava/util/ArrayList;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 95
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    .line 96
    invoke-virtual {p0, v0, p2}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->processNext(Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;Ljava/lang/Object;)V

    goto :goto_0

    .line 98
    :cond_0
    return-void
.end method

.method public shutdown()V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->processor:Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;->shutdown()V

    .line 55
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->executor:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->executor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->isShutdown:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;

    .line 60
    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/capture/pipeline/PipelineNode;->shutdown()V

    goto :goto_0

    .line 62
    :cond_1
    return-void
.end method
