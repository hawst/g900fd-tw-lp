.class public Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

.field private final fragment:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;->fragment:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;

    .line 26
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    .line 27
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;ILcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;->cleanupAndExit(ILcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;)Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;->fragment:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;

    return-object v0
.end method

.method private cleanupAndExit(ILcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 62
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 63
    if-nez p3, :cond_0

    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    .line 64
    :cond_0
    if-eqz p2, :cond_1

    .line 65
    const-string v1, "CREDIT_CARD_OCR_RESULT"

    invoke-virtual {p3, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 67
    :cond_1
    invoke-virtual {v0, p3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 68
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;->fragment:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->capturedResult(Landroid/content/Intent;I)V

    .line 69
    return-void
.end method

.method private showManualEntryDialog(ILcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;->fragment:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardOcrFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    new-instance v1, Lcom/google/android/libraries/commerce/ocr/credit/fragments/a;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/a;-><init>(Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;ILcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;Landroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 58
    return-void
.end method


# virtual methods
.method public exit(ILcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;->closeDriver()V

    .line 31
    const/16 v0, 0x2714

    if-eq p1, v0, :cond_0

    const/16 v0, 0x2713

    if-ne p1, v0, :cond_1

    .line 33
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;->showManualEntryDialog(ILcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;Landroid/os/Bundle;)V

    .line 37
    :goto_0
    return-void

    .line 35
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;->cleanupAndExit(ILcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;Landroid/os/Bundle;)V

    goto :goto_0
.end method
