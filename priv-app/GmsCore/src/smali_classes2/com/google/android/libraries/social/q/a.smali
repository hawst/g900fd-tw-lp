.class public final Lcom/google/android/libraries/social/q/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/libraries/social/h/b/a;

.field private final b:Landroid/net/Uri;

.field private c:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const-wide/16 v0, 0xbb8

    iput-wide v0, p0, Lcom/google/android/libraries/social/q/a;->c:J

    .line 40
    const-class v0, Lcom/google/android/libraries/social/h/b/a;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/h/b/a;

    iput-object v0, p0, Lcom/google/android/libraries/social/q/a;->a:Lcom/google/android/libraries/social/h/b/a;

    .line 41
    iput-object p2, p0, Lcom/google/android/libraries/social/q/a;->b:Landroid/net/Uri;

    .line 42
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 8

    .prologue
    .line 53
    new-instance v0, Lcom/google/android/libraries/social/q/b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/q/b;-><init>(B)V

    .line 54
    iget-object v1, p0, Lcom/google/android/libraries/social/q/a;->a:Lcom/google/android/libraries/social/h/b/a;

    iget-object v2, p0, Lcom/google/android/libraries/social/q/a;->b:Landroid/net/Uri;

    invoke-interface {v1, v2, v0}, Lcom/google/android/libraries/social/h/b/a;->a(Landroid/net/Uri;Lcom/google/android/libraries/social/h/b/d;)V

    .line 56
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/libraries/social/q/a;->c:J

    add-long/2addr v2, v4

    .line 57
    :goto_0
    iget-boolean v1, v0, Lcom/google/android/libraries/social/q/b;->a:Z

    if-nez v1, :cond_0

    .line 58
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v4, v2, v4

    .line 59
    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-gtz v1, :cond_1

    .line 60
    const-string v1, "BlockingPanoDetector"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 61
    const-string v1, "BlockingPanoDetector"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "pano wait time expired, assume image is not a pano; uri="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/libraries/social/q/a;->b:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :cond_0
    iget-boolean v0, v0, Lcom/google/android/libraries/social/q/b;->b:Z

    return v0

    .line 66
    :cond_1
    const-wide/16 v6, 0x5

    :try_start_0
    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 67
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 71
    :catch_0
    move-exception v1

    goto :goto_0
.end method
