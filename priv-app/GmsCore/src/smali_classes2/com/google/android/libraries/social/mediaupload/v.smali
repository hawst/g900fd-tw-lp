.class final Lcom/google/android/libraries/social/mediaupload/v;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/libraries/social/mediaupload/v;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:I

.field public e:J

.field public f:J

.field public g:J

.field public h:J

.field public i:J

.field public j:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized a(Ljava/lang/String;)Lcom/google/android/libraries/social/mediaupload/v;
    .locals 4

    .prologue
    .line 72
    const-class v1, Lcom/google/android/libraries/social/mediaupload/v;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/libraries/social/mediaupload/t;->a:Lcom/google/android/libraries/social/mediaupload/v;

    .line 73
    if-nez v0, :cond_0

    .line 74
    new-instance v0, Lcom/google/android/libraries/social/mediaupload/v;

    invoke-direct {v0}, Lcom/google/android/libraries/social/mediaupload/v;-><init>()V

    .line 78
    :goto_0
    iput-object p0, v0, Lcom/google/android/libraries/social/mediaupload/v;->b:Ljava/lang/String;

    .line 79
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/libraries/social/mediaupload/v;->g:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    monitor-exit v1

    return-object v0

    .line 76
    :cond_0
    :try_start_1
    iget-object v2, v0, Lcom/google/android/libraries/social/mediaupload/v;->a:Lcom/google/android/libraries/social/mediaupload/v;

    sput-object v2, Lcom/google/android/libraries/social/mediaupload/t;->a:Lcom/google/android/libraries/social/mediaupload/v;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized b(Lcom/google/android/libraries/social/mediaupload/v;)V
    .locals 2

    .prologue
    .line 84
    const-class v1, Lcom/google/android/libraries/social/mediaupload/v;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/libraries/social/mediaupload/t;->a:Lcom/google/android/libraries/social/mediaupload/v;

    iput-object v0, p0, Lcom/google/android/libraries/social/mediaupload/v;->a:Lcom/google/android/libraries/social/mediaupload/v;

    .line 85
    sput-object p0, Lcom/google/android/libraries/social/mediaupload/t;->a:Lcom/google/android/libraries/social/mediaupload/v;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    monitor-exit v1

    return-void

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/mediaupload/v;->b:Ljava/lang/String;

    .line 90
    iput v1, p0, Lcom/google/android/libraries/social/mediaupload/v;->c:I

    .line 91
    iput v1, p0, Lcom/google/android/libraries/social/mediaupload/v;->d:I

    .line 92
    iput-wide v2, p0, Lcom/google/android/libraries/social/mediaupload/v;->e:J

    .line 93
    iput-wide v2, p0, Lcom/google/android/libraries/social/mediaupload/v;->f:J

    .line 94
    iput-wide v2, p0, Lcom/google/android/libraries/social/mediaupload/v;->i:J

    .line 95
    iput v1, p0, Lcom/google/android/libraries/social/mediaupload/v;->j:I

    .line 96
    invoke-static {p0}, Lcom/google/android/libraries/social/mediaupload/v;->b(Lcom/google/android/libraries/social/mediaupload/v;)V

    .line 97
    return-void
.end method

.method public final a(Lcom/google/android/libraries/social/mediaupload/v;)V
    .locals 4

    .prologue
    .line 100
    iget v0, p0, Lcom/google/android/libraries/social/mediaupload/v;->c:I

    iget v1, p1, Lcom/google/android/libraries/social/mediaupload/v;->c:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/libraries/social/mediaupload/v;->c:I

    .line 101
    iget v0, p0, Lcom/google/android/libraries/social/mediaupload/v;->d:I

    iget v1, p1, Lcom/google/android/libraries/social/mediaupload/v;->d:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/libraries/social/mediaupload/v;->d:I

    .line 102
    iget-wide v0, p0, Lcom/google/android/libraries/social/mediaupload/v;->e:J

    iget-wide v2, p1, Lcom/google/android/libraries/social/mediaupload/v;->e:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/libraries/social/mediaupload/v;->e:J

    .line 103
    iget-wide v0, p0, Lcom/google/android/libraries/social/mediaupload/v;->f:J

    iget-wide v2, p1, Lcom/google/android/libraries/social/mediaupload/v;->f:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/libraries/social/mediaupload/v;->f:J

    .line 104
    iget-wide v0, p0, Lcom/google/android/libraries/social/mediaupload/v;->i:J

    iget-wide v2, p1, Lcom/google/android/libraries/social/mediaupload/v;->i:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/libraries/social/mediaupload/v;->i:J

    .line 105
    iget v0, p0, Lcom/google/android/libraries/social/mediaupload/v;->j:I

    iget v1, p1, Lcom/google/android/libraries/social/mediaupload/v;->j:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/libraries/social/mediaupload/v;->j:I

    .line 106
    return-void
.end method
