.class public Lcom/google/android/libraries/social/mediaupload/b;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/social/mediaupload/q;I)Lcom/google/c/e/b/d/a/e;
    .locals 6

    .prologue
    .line 25
    new-instance v0, Lcom/google/c/e/b/d/a/e;

    invoke-direct {v0}, Lcom/google/c/e/b/d/a/e;-><init>()V

    .line 26
    invoke-virtual {p4}, Lcom/google/android/libraries/social/mediaupload/q;->h()Lcom/google/android/libraries/social/m/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/social/m/a;->a()Ljava/lang/String;

    move-result-object v1

    .line 27
    iput-object p1, v0, Lcom/google/c/e/b/d/a/e;->a:Ljava/lang/String;

    .line 28
    iput-object p2, v0, Lcom/google/c/e/b/d/a/e;->b:Ljava/lang/String;

    .line 29
    iput-object p3, v0, Lcom/google/c/e/b/d/a/e;->h:Ljava/lang/String;

    .line 30
    iput-object v1, v0, Lcom/google/c/e/b/d/a/e;->l:Ljava/lang/String;

    .line 31
    invoke-static {v1}, Lcom/google/android/libraries/social/m/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 32
    iput-object v1, v0, Lcom/google/c/e/b/d/a/e;->g:Ljava/lang/String;

    .line 33
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    iput-object v2, v0, Lcom/google/c/e/b/d/a/e;->n:[Ljava/lang/String;

    .line 34
    invoke-virtual {p4}, Lcom/google/android/libraries/social/mediaupload/q;->j()Ljava/lang/String;

    move-result-object v1

    .line 35
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 36
    iput-object v1, v0, Lcom/google/c/e/b/d/a/e;->p:Ljava/lang/String;

    .line 38
    :cond_0
    invoke-virtual {p4}, Lcom/google/android/libraries/social/mediaupload/q;->b()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/e/b/d/a/e;->e:Ljava/lang/Boolean;

    .line 39
    new-instance v1, Lcom/google/c/e/b/d/a/g;

    invoke-direct {v1}, Lcom/google/c/e/b/d/a/g;-><init>()V

    iput-object v1, v0, Lcom/google/c/e/b/d/a/e;->m:Lcom/google/c/e/b/d/a/g;

    .line 40
    iget-object v1, v0, Lcom/google/c/e/b/d/a/e;->m:Lcom/google/c/e/b/d/a/g;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/c/e/b/d/a/g;->a:Ljava/lang/Integer;

    .line 41
    invoke-virtual {p4}, Lcom/google/android/libraries/social/mediaupload/q;->f()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/e/b/d/a/e;->j:Ljava/lang/Long;

    .line 43
    invoke-virtual {p4}, Lcom/google/android/libraries/social/mediaupload/q;->l()Lcom/google/z/a/a/a/b;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 44
    new-instance v1, Lcom/google/c/e/b/d/a/b;

    invoke-direct {v1}, Lcom/google/c/e/b/d/a/b;-><init>()V

    iput-object v1, v0, Lcom/google/c/e/b/d/a/e;->k:Lcom/google/c/e/b/d/a/b;

    .line 45
    iget-object v1, v0, Lcom/google/c/e/b/d/a/e;->k:Lcom/google/c/e/b/d/a/b;

    new-instance v2, Lcom/google/c/e/b/d/a/c;

    invoke-direct {v2}, Lcom/google/c/e/b/d/a/c;-><init>()V

    iput-object v2, v1, Lcom/google/c/e/b/d/a/b;->a:Lcom/google/c/e/b/d/a/c;

    .line 46
    iget-object v1, v0, Lcom/google/c/e/b/d/a/e;->k:Lcom/google/c/e/b/d/a/b;

    iget-object v1, v1, Lcom/google/c/e/b/d/a/b;->a:Lcom/google/c/e/b/d/a/c;

    invoke-virtual {p4}, Lcom/google/android/libraries/social/mediaupload/q;->l()Lcom/google/z/a/a/a/b;

    move-result-object v2

    iput-object v2, v1, Lcom/google/c/e/b/d/a/c;->a:Lcom/google/z/a/a/a/b;

    .line 54
    :cond_1
    :goto_0
    invoke-virtual {p4}, Lcom/google/android/libraries/social/mediaupload/q;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/e/b/d/a/e;->f:Ljava/lang/String;

    .line 55
    return-object v0

    .line 47
    :cond_2
    invoke-virtual {p4}, Lcom/google/android/libraries/social/mediaupload/q;->g()Landroid/net/Uri;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/libraries/social/mediaupload/ae;->d(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 48
    new-instance v1, Lcom/google/c/e/b/d/a/b;

    invoke-direct {v1}, Lcom/google/c/e/b/d/a/b;-><init>()V

    iput-object v1, v0, Lcom/google/c/e/b/d/a/e;->k:Lcom/google/c/e/b/d/a/b;

    .line 49
    iget-object v1, v0, Lcom/google/c/e/b/d/a/e;->k:Lcom/google/c/e/b/d/a/b;

    new-instance v2, Lcom/google/c/e/b/d/a/c;

    invoke-direct {v2}, Lcom/google/c/e/b/d/a/c;-><init>()V

    iput-object v2, v1, Lcom/google/c/e/b/d/a/b;->a:Lcom/google/c/e/b/d/a/c;

    .line 50
    iget-object v1, v0, Lcom/google/c/e/b/d/a/e;->k:Lcom/google/c/e/b/d/a/b;

    iget-object v1, v1, Lcom/google/c/e/b/d/a/b;->a:Lcom/google/c/e/b/d/a/c;

    new-instance v2, Lcom/google/z/a/a/a/b;

    invoke-direct {v2}, Lcom/google/z/a/a/a/b;-><init>()V

    iput-object v2, v1, Lcom/google/c/e/b/d/a/c;->a:Lcom/google/z/a/a/a/b;

    .line 51
    iget-object v1, v0, Lcom/google/c/e/b/d/a/e;->k:Lcom/google/c/e/b/d/a/b;

    iget-object v1, v1, Lcom/google/c/e/b/d/a/b;->a:Lcom/google/c/e/b/d/a/c;

    iget-object v1, v1, Lcom/google/c/e/b/d/a/c;->a:Lcom/google/z/a/a/a/b;

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/z/a/a/a/b;->a:Ljava/lang/Integer;

    goto :goto_0
.end method
