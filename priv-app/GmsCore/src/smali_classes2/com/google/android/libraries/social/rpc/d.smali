.class public final Lcom/google/android/libraries/social/rpc/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/rpc/f;


# instance fields
.field private a:Landroid/content/Context;

.field private b:[Lcom/google/android/libraries/social/rpc/k;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/rpc/d;->c:Z

    .line 22
    iput-object p1, p0, Lcom/google/android/libraries/social/rpc/d;->a:Landroid/content/Context;

    .line 23
    return-void
.end method

.method private declared-synchronized a()V
    .locals 2

    .prologue
    .line 39
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/libraries/social/rpc/d;->c:Z

    if-nez v0, :cond_1

    .line 40
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/d;->a:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/rpc/k;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 42
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 43
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/libraries/social/rpc/k;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/libraries/social/rpc/k;

    iput-object v0, p0, Lcom/google/android/libraries/social/rpc/d;->b:[Lcom/google/android/libraries/social/rpc/k;

    .line 45
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/rpc/d;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47
    :cond_1
    monitor-exit p0

    return-void

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/android/libraries/social/rpc/h;)V
    .locals 3

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/google/android/libraries/social/rpc/d;->c:Z

    if-nez v0, :cond_0

    .line 28
    invoke-direct {p0}, Lcom/google/android/libraries/social/rpc/d;->a()V

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/d;->b:[Lcom/google/android/libraries/social/rpc/k;

    if-eqz v0, :cond_1

    .line 31
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/d;->b:[Lcom/google/android/libraries/social/rpc/k;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 32
    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/d;->b:[Lcom/google/android/libraries/social/rpc/k;

    aget-object v1, v1, v0

    iget-object v2, p1, Lcom/google/android/libraries/social/rpc/h;->b:Landroid/content/Context;

    invoke-interface {v1}, Lcom/google/android/libraries/social/rpc/k;->a()Lcom/google/android/libraries/social/rpc/h;

    move-result-object p1

    .line 31
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 35
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/libraries/social/rpc/h;->b()V

    .line 36
    return-void
.end method
