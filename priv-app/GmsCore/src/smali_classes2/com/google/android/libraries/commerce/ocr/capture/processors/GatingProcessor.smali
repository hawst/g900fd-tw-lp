.class public Lcom/google/android/libraries/commerce/ocr/capture/processors/GatingProcessor;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;


# instance fields
.field private final isAllowed:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/GatingProcessor;->isAllowed:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 19
    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/capture/processors/GatingProcessor;->block()V

    .line 20
    return-void
.end method


# virtual methods
.method public allow()V
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/GatingProcessor;->isAllowed:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 24
    return-void
.end method

.method public block()V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/GatingProcessor;->isAllowed:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 28
    return-void
.end method

.method public discard(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 46
    instance-of v0, p1, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    if-eqz v0, :cond_0

    .line 47
    check-cast p1, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    .line 48
    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->recycle()V

    .line 50
    :cond_0
    return-void
.end method

.method public isProcessingNeeded(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/GatingProcessor;->isAllowed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public process(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/GatingProcessor;->isProcessingNeeded(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    :goto_0
    return-object p1

    .line 40
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/GatingProcessor;->discard(Ljava/lang/Object;)V

    .line 41
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public shutdown()V
    .locals 0

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/capture/processors/GatingProcessor;->block()V

    .line 55
    return-void
.end method
