.class public Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer;


# static fields
.field private static final DEBUG_IMAGE_COMPRESSION_RATE:I = 0x5a

.field private static final DETECTED_CARD_IMAGE_FOLDER:Ljava/lang/String; = "detected"

.field private static final TAG:Ljava/lang/String; = "CreditCardOcrRecognizer"


# instance fields
.field private final cardRectifier:Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier;

.field private detectionAndRectificationTime:J

.field private final edgeChangeListener:Lcom/google/android/libraries/commerce/ocr/capture/EdgeChangeListener;

.field private final edgeFinderWidthToOuterBoundingBoxEdgeLengthRatio:F

.field private final exitHandler:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

.field private final imageCaptureMode:Z

.field private final imageUtil:Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;

.field private final nativeRecognizer:Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer;

.field private final recognizeExpirationDate:Z

.field private final roiProvider:Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;

.field private final saveBlurDetectorRoi:Z

.field private final thresholdExpirationDateMinConfidence4Digit:F

.field private final thresholdExpirationDateMinConfidence6Digit:F

.field private final thresholdMeanDigitConfidence:F

.field private final thresholdMinDigitConfidence:F

.field private final timeoutManager:Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;

.field private final validateResult:Z

.field private final validator:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer;Lcom/google/android/libraries/commerce/ocr/capture/EdgeChangeListener;Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier;Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator;ZFFFZFFZ)V
    .locals 2

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->imageUtil:Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;

    .line 79
    iput-object p3, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->edgeChangeListener:Lcom/google/android/libraries/commerce/ocr/capture/EdgeChangeListener;

    .line 80
    iput-object p4, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->exitHandler:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

    .line 81
    move/from16 v0, p16

    iput-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->imageCaptureMode:Z

    .line 82
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->saveBlurDetectorRoi:Z

    .line 83
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->nativeRecognizer:Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer;

    .line 84
    iput-object p7, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->cardRectifier:Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier;

    .line 85
    iput-object p8, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->validator:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator;

    .line 86
    iput-boolean p9, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->validateResult:Z

    .line 87
    iput p10, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->edgeFinderWidthToOuterBoundingBoxEdgeLengthRatio:F

    .line 89
    iput p12, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->thresholdMeanDigitConfidence:F

    .line 90
    iput p11, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->thresholdMinDigitConfidence:F

    .line 91
    iput-boolean p13, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->recognizeExpirationDate:Z

    .line 92
    move/from16 v0, p14

    iput v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->thresholdExpirationDateMinConfidence4Digit:F

    .line 93
    move/from16 v0, p15

    iput v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->thresholdExpirationDateMinConfidence6Digit:F

    .line 94
    iput-object p5, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->roiProvider:Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;

    .line 95
    iput-object p6, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->timeoutManager:Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;

    .line 96
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;)Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->timeoutManager:Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;)Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->validateResult:Z

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;)J
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->stopAndGetTime(Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$300(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;)Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->imageUtil:Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;)J
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->detectionAndRectificationTime:J

    return-wide v0
.end method

.method static synthetic access$500(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;)Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->validator:Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardResponseValidator;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;)F
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->thresholdMeanDigitConfidence:F

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;)F
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->thresholdMinDigitConfidence:F

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;)F
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->thresholdExpirationDateMinConfidence4Digit:F

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;)F
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->thresholdExpirationDateMinConfidence6Digit:F

    return v0
.end method

.method private static final createTimeoutTask(Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;)Ljava/util/TimerTask;
    .locals 1

    .prologue
    .line 121
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/credit/capture/a;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/commerce/ocr/credit/capture/a;-><init>(Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;)V

    return-object v0
.end method

.method private getMinRadonStdDev(Landroid/graphics/Point;)F
    .locals 4

    .prologue
    const/16 v2, 0x3c0

    const/high16 v0, 0x3f400000    # 0.75f

    .line 280
    iget v1, p1, Landroid/graphics/Point;->x:I

    .line 282
    if-lt v1, v2, :cond_1

    .line 283
    const v0, 0x3f1eb852    # 0.62f

    .line 290
    :cond_0
    :goto_0
    const-string v1, "CreditCardOcrRecognizer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Min radon std dev is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for resolution  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/graphics/Point;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    return v0

    .line 284
    :cond_1
    if-ge v1, v2, :cond_0

    const/16 v2, 0x280

    if-lt v1, v2, :cond_0

    .line 286
    const v2, 0x3e051eb8    # 0.13f

    add-int/lit16 v1, v1, -0x280

    int-to-float v1, v1

    mul-float/2addr v1, v2

    const/high16 v2, 0x43a00000    # 320.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    goto :goto_0
.end method

.method private performOcrOnRectifiedImage(Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;)V
    .locals 6

    .prologue
    .line 132
    invoke-direct {p0, p2}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->saveBlurDetectorRoiIfNecessary(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)V

    .line 134
    invoke-static {}, Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;->createStarted()Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;

    move-result-object v4

    .line 135
    invoke-static {p3}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->createTimeoutTask(Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;)Ljava/util/TimerTask;

    move-result-object v2

    .line 137
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->timeoutManager:Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/commerce/ocr/util/TimeoutManager;->startTimeout(Ljava/util/TimerTask;)Z

    .line 139
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;

    move-object v1, p0

    move-object v3, p3

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/commerce/ocr/credit/capture/b;-><init>(Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;Ljava/util/TimerTask;Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;)V

    .line 206
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->nativeRecognizer:Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer;

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;->getRectifiedCard()Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getData()[B

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;->getExpirationDateImage()Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getData()[B

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->recognizeExpirationDate:Z

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer;->recognizeCard([B[BZLcom/google/android/libraries/commerce/ocr/cv/localrecognition/NativeCreditCardRecognizer$Callback;)V

    .line 210
    return-void
.end method

.method private rectify(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 216
    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getOrientation()I

    move-result v0

    rem-int/lit16 v0, v0, 0xb4

    const/16 v1, 0x5a

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 218
    :goto_0
    if-eqz v0, :cond_1

    .line 226
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->imageUtil:Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->roiProvider:Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;

    invoke-interface {v1}, Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;->getBoundingBoxRectRelativeToCameraPreview()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getResolution()Landroid/graphics/Point;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;->rotate90(Landroid/graphics/Rect;Landroid/graphics/Point;)Landroid/graphics/Rect;

    move-result-object v2

    .line 232
    :goto_1
    invoke-static {}, Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;->createStarted()Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;

    move-result-object v6

    .line 233
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->cardRectifier:Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier;

    iget v3, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->edgeFinderWidthToOuterBoundingBoxEdgeLengthRatio:F

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getResolution()Landroid/graphics/Point;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->getMinRadonStdDev(Landroid/graphics/Point;)F

    move-result v4

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier;->rectify(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;Landroid/graphics/Rect;FFZ)Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;

    move-result-object v0

    .line 236
    invoke-direct {p0, v6}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->stopAndGetTime(Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->detectionAndRectificationTime:J

    .line 237
    const-string v1, "CreditCardOcrRecognizer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "rectify: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->detectionAndRectificationTime:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    return-object v0

    :cond_0
    move v0, v5

    .line 216
    goto :goto_0

    .line 229
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->roiProvider:Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;->getBoundingBoxRectRelativeToCameraPreview()Landroid/graphics/Rect;

    move-result-object v2

    goto :goto_1
.end method

.method private saveBlurDetectorRoiIfNecessary(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)V
    .locals 4

    .prologue
    .line 257
    iget-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->saveBlurDetectorRoi:Z

    if-nez v0, :cond_0

    .line 266
    :goto_0
    return-void

    .line 261
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->imageUtil:Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->roiProvider:Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;

    iget v2, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->edgeFinderWidthToOuterBoundingBoxEdgeLengthRatio:F

    const-string v3, "detected"

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;->saveBlurDetectorROI(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;FLjava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 263
    :catch_0
    move-exception v0

    .line 264
    const-string v1, "CreditCardOcrRecognizer"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private saveDebugRectsOnImage(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)V
    .locals 11

    .prologue
    const/16 v5, 0x64

    const/4 v10, 0x0

    .line 243
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->roiProvider:Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;->getBoundingBoxRectRelativeToCameraPreview()Landroid/graphics/Rect;

    move-result-object v6

    .line 244
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->roiProvider:Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;->getBlurDetectorROI()Landroid/graphics/Rect;

    move-result-object v7

    .line 246
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->imageUtil:Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getData()[B

    move-result-object v1

    invoke-virtual {v1}, [B->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getResolution()Landroid/graphics/Point;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getFormat()I

    move-result v3

    new-instance v4, Landroid/graphics/Rect;

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getWidth()I

    move-result v8

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getHeight()I

    move-result v9

    invoke-direct {v4, v10, v10, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;->convertPreviewToJPEG([BLandroid/graphics/Point;ILandroid/graphics/Rect;I)[B

    move-result-object v0

    .line 249
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->imageUtil:Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;

    const/4 v2, 0x3

    new-array v2, v2, [Landroid/graphics/Rect;

    aput-object v7, v2, v10

    const/4 v3, 0x1

    new-instance v4, Landroid/graphics/Rect;

    const/16 v7, 0x32

    const/16 v8, 0xa

    invoke-direct {v4, v10, v10, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object v6, v2, v3

    invoke-virtual {v1, v0, v5, v2}, Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;->drawOnImageAndJpegConvert([BI[Landroid/graphics/Rect;)[B

    move-result-object v0

    .line 252
    new-instance v1, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    const/16 v2, 0x100

    new-instance v3, Landroid/graphics/Point;

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getWidth()I

    move-result v4

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getHeight()I

    move-result v5

    invoke-direct {v3, v4, v5}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getOrientation()I

    move-result v4

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;-><init>([BILandroid/graphics/Point;I)V

    invoke-direct {p0, v1}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->saveDetectedImage(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)V

    .line 254
    return-void
.end method

.method private saveDetectedImage(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)V
    .locals 4

    .prologue
    .line 271
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->imageUtil:Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->roiProvider:Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;

    iget v2, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->edgeFinderWidthToOuterBoundingBoxEdgeLengthRatio:F

    const-string v3, "detected"

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;->saveFullCardImage(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;FLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 273
    const-string v1, "CreditCardOcrRecognizer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Saved OCR image to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    :goto_0
    return-void

    .line 274
    :catch_0
    move-exception v0

    .line 275
    const-string v1, "CreditCardOcrRecognizer"

    const-string v2, "Failed saving image"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private stopAndGetTime(Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;)J
    .locals 2

    .prologue
    .line 296
    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;->stop()Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;->elapsed(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public performOcr(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 101
    invoke-direct {p0, p1}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->rectify(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;

    move-result-object v0

    .line 102
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->edgeChangeListener:Lcom/google/android/libraries/commerce/ocr/capture/EdgeChangeListener;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;->getBoundaries()Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/libraries/commerce/ocr/capture/EdgeChangeListener;->onEdgeChange(Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;)V

    .line 104
    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;->getRectifiedCard()Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 105
    invoke-interface {p2}, Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;->onOcrAttempt()Z

    .line 107
    iget-boolean v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->imageCaptureMode:Z

    if-nez v1, :cond_1

    .line 108
    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->performOcrOnRectifiedImage(Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;Lcom/google/android/libraries/commerce/ocr/capture/OcrRecognizer$OcrResponseHandler;)V

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->saveDebugRectsOnImage(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)V

    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->saveDetectedImage(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)V

    .line 112
    invoke-direct {p0, p1}, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->saveBlurDetectorRoiIfNecessary(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/capture/CreditCardOcrRecognizer;->exitHandler:Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;

    const/16 v1, 0x2712

    invoke-virtual {v0, v1, v3, v3}, Lcom/google/android/libraries/commerce/ocr/credit/fragments/CreditCardExitHandler;->exit(ILcom/google/android/libraries/commerce/ocr/pub/CreditCardOCRParcelable;Landroid/os/Bundle;)V

    goto :goto_0
.end method
