.class public Lcom/google/android/libraries/commerce/ocr/cv/SafeCountingPool;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/cv/RecyclablePool;


# static fields
.field private static final TAG:Ljava/lang/String; = "SafeCountingPool"


# instance fields
.field private final numReferences:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final safePoolable:Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/cv/SafeCountingPool;->safePoolable:Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    .line 30
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/SafeCountingPool;->numReferences:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 31
    return-void
.end method


# virtual methods
.method public recycle(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 40
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/cv/SafeCountingPool;->safePoolable:Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    invoke-virtual {v1}, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->get()Ljava/lang/Object;

    move-result-object v1

    if-eq p1, v1, :cond_1

    .line 41
    const-string v1, "SafeCountingPool"

    const-string v2, "Resource recycled was not the same as the one managed by this pool"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    :cond_0
    :goto_0
    return v0

    .line 45
    :cond_1
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/cv/SafeCountingPool;->numReferences:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v1

    if-gtz v1, :cond_0

    .line 46
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/SafeCountingPool;->safePoolable:Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->recycle()V

    .line 47
    const-string v0, "SafeCountingPool"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Resource released from counting pool: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/cv/SafeCountingPool;->safePoolable:Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public useReference()Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/cv/SafeCountingPool;->numReferences:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 35
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/cv/SafeCountingPool;->safePoolable:Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    invoke-virtual {v1}, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;-><init>(Lcom/google/android/libraries/commerce/ocr/cv/RecyclablePool;Ljava/lang/Object;)V

    return-object v0
.end method
