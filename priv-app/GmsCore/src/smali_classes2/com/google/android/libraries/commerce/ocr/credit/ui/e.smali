.class final Lcom/google/android/libraries/commerce/ocr/credit/ui/e;
.super Landroid/view/View;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

.field private final b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/RectF;

.field private final e:Ljava/lang/String;

.field private final f:I


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 176
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    .line 177
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 178
    const/high16 v0, 0x41000000    # 8.0f

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->context:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->access$000(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->f:I

    .line 180
    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/libraries/commerce/ocr/R$string;->ocr_cc_sample_number:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->e:Ljava/lang/String;

    .line 182
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->c:Landroid/graphics/Paint;

    .line 183
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->c:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 184
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->c:Landroid/graphics/Paint;

    const/16 v1, 0x9b

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 185
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 186
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 187
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->d:Landroid/graphics/RectF;

    .line 189
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->b:Landroid/graphics/Paint;

    .line 190
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->b:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 191
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->b:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 192
    return-void
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->cardAreaPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->access$400(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 299
    return-void
.end method


# virtual methods
.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    const/16 v8, 0x9b

    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v11, 0x3f800000    # 1.0f

    .line 200
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->uiOption:I
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->access$100(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 202
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->d:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->roiProvider:Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;
    invoke-static {v1}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->access$200(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->getMidBoundingBoxRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 203
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->d:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->visiblePreviewArea:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->access$300(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Landroid/graphics/Rect;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->cardAreaPaint:Landroid/graphics/Paint;
    invoke-static {v2}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->access$400(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Landroid/graphics/Paint;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget v1, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->f:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->f:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    iget v1, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    const v3, 0x3da7ef9e    # 0.082f

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v3

    const v4, 0x3f08f5c3    # 0.535f

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v3

    const v4, 0x3deb851f    # 0.115f

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->c:Landroid/graphics/Paint;

    invoke-virtual {v4, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v4, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->c:Landroid/graphics/Paint;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v4, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->c:Landroid/graphics/Paint;

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v4, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->e:Ljava/lang/String;

    add-float v5, v1, v7

    add-float v6, v2, v3

    add-float/2addr v6, v7

    iget-object v7, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v5, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget v4, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->f:I

    int-to-float v4, v4

    iget v5, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->f:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v4, v5, v6}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->c:Landroid/graphics/Paint;

    const/high16 v4, -0x1000000

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v8}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->e:Ljava/lang/String;

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 206
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->roiProvider:Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->access$200(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->getMidBoundingBoxRect()Landroid/graphics/Rect;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 209
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->roiProvider:Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->access$200(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->getMidBoundingBoxRect()Landroid/graphics/Rect;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 210
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->roiProvider:Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->access$200(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CardRegionOfInterestProvider;->getMidBoundingBoxRect()Landroid/graphics/Rect;

    move-result-object v0

    iget v6, v0, Landroid/graphics/Rect;->left:I

    iget v7, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-float v0, v9

    const/high16 v1, 0x3e800000    # 0.25f

    mul-float v10, v0, v1

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    int-to-float v1, v6

    int-to-float v2, v7

    add-float/2addr v2, v10

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    int-to-float v1, v6

    int-to-float v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    int-to-float v1, v6

    add-float/2addr v1, v10

    int-to-float v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    add-int v2, v6, v8

    int-to-float v2, v2

    sub-float/2addr v2, v10

    int-to-float v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    add-int v2, v6, v8

    int-to-float v2, v2

    int-to-float v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    add-int v2, v6, v8

    int-to-float v2, v2

    int-to-float v3, v7

    add-float/2addr v3, v10

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    int-to-float v3, v6

    add-int v4, v7, v9

    int-to-float v4, v4

    sub-float/2addr v4, v10

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    int-to-float v3, v6

    add-int v4, v7, v9

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    int-to-float v3, v6

    add-float/2addr v3, v10

    add-int v4, v7, v9

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    add-int v4, v6, v8

    int-to-float v4, v4

    sub-float/2addr v4, v10

    add-int v5, v7, v9

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    add-int v4, v6, v8

    int-to-float v4, v4

    add-int v5, v7, v9

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    add-int v4, v6, v8

    int-to-float v4, v4

    add-int v5, v7, v9

    int-to-float v5, v5

    sub-float/2addr v5, v10

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v4, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->edgeLinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->access$500(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->edgeLinePaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->access$500(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->edgeLinePaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->access$500(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->edgeLinePaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->access$500(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->cardBoundaries:Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->access$600(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->cardBoundaries:Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->access$600(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->getTop()Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    move-result-object v0

    if-eqz v0, :cond_1

    int-to-float v0, v6

    add-float/2addr v0, v10

    sub-float v1, v0, v11

    int-to-float v2, v7

    add-int v0, v6, v8

    int-to-float v0, v0

    sub-float/2addr v0, v10

    add-float v3, v0, v11

    int-to-float v4, v7

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->edgeLinePaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->access$500(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Landroid/graphics/Paint;

    move-result-object v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->cardBoundaries:Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->access$600(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->getLeft()Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    move-result-object v0

    if-eqz v0, :cond_2

    int-to-float v1, v6

    int-to-float v0, v7

    add-float/2addr v0, v10

    sub-float v2, v0, v11

    int-to-float v3, v6

    add-int v0, v7, v9

    int-to-float v0, v0

    sub-float/2addr v0, v10

    add-float v4, v0, v11

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->edgeLinePaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->access$500(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Landroid/graphics/Paint;

    move-result-object v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->cardBoundaries:Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->access$600(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->getRight()Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    move-result-object v0

    if-eqz v0, :cond_3

    add-int v0, v6, v8

    int-to-float v1, v0

    int-to-float v0, v7

    add-float/2addr v0, v10

    sub-float v2, v0, v11

    add-int v0, v6, v8

    int-to-float v3, v0

    add-int v0, v7, v9

    int-to-float v0, v0

    sub-float/2addr v0, v10

    add-float v4, v0, v11

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->edgeLinePaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->access$500(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Landroid/graphics/Paint;

    move-result-object v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->cardBoundaries:Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->access$600(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;->getBottom()Lcom/google/android/libraries/commerce/ocr/cv/Edge;

    move-result-object v0

    if-eqz v0, :cond_0

    int-to-float v0, v6

    add-float/2addr v0, v10

    sub-float v1, v0, v11

    add-int v0, v7, v9

    int-to-float v2, v0

    add-int v0, v6, v8

    int-to-float v0, v0

    sub-float/2addr v0, v10

    add-float v3, v0, v11

    add-int v0, v7, v9

    int-to-float v4, v0

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/credit/ui/e;->a:Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;

    # getter for: Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->edgeLinePaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;->access$500(Lcom/google/android/libraries/commerce/ocr/credit/ui/CreditCardPreviewOverlayView;)Landroid/graphics/Paint;

    move-result-object v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
