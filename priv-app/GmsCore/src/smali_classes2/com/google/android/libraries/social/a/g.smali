.class public final Lcom/google/android/libraries/social/a/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/a/f;


# instance fields
.field private final a:[Lcom/google/android/libraries/social/a/f;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 33
    new-instance v1, Lcom/google/android/libraries/social/s/a;

    invoke-direct {v1, v0}, Lcom/google/android/libraries/social/s/a;-><init>(Landroid/content/Context;)V

    .line 34
    const-string v0, "MODULE"

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/s/a;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 35
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/libraries/social/a/f;

    .line 36
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/libraries/social/a/f;

    iput-object v0, p0, Lcom/google/android/libraries/social/a/g;->a:[Lcom/google/android/libraries/social/a/f;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/libraries/social/a/a;)V
    .locals 2

    .prologue
    .line 41
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/libraries/social/a/g;->a:[Lcom/google/android/libraries/social/a/f;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 42
    iget-object v1, p0, Lcom/google/android/libraries/social/a/g;->a:[Lcom/google/android/libraries/social/a/f;

    aget-object v1, v1, v0

    invoke-interface {v1, p1, p2, p3}, Lcom/google/android/libraries/social/a/f;->a(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/libraries/social/a/a;)V

    .line 41
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 44
    :cond_0
    return-void
.end method
