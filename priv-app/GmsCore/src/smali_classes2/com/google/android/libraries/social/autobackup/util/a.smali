.class public Lcom/google/android/libraries/social/autobackup/util/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/util/a;->a:Landroid/content/ContentResolver;

    .line 30
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 38
    invoke-static {}, Lcom/google/android/libraries/b/a/k;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Must be called on a background thread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/util/a;->a:Landroid/content/ContentResolver;

    const-string v1, "plusone:autobackup_logged_out_notification_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
