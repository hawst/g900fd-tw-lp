.class public final Lcom/google/android/libraries/social/rpc/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/libraries/social/rpc/a/a;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/libraries/social/rpc/a/b;
    .locals 3

    .prologue
    .line 142
    sget-object v1, Lcom/google/android/libraries/social/rpc/a/a;->a:Ljava/util/Map;

    monitor-enter v1

    .line 143
    :try_start_0
    sget-object v0, Lcom/google/android/libraries/social/rpc/a/a;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/rpc/a/b;

    .line 144
    if-nez v0, :cond_0

    .line 145
    new-instance v0, Lcom/google/android/libraries/social/rpc/a/c;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/social/rpc/a/c;-><init>(Ljava/lang/String;)V

    .line 146
    sget-object v2, Lcom/google/android/libraries/social/rpc/a/a;->a:Ljava/util/Map;

    invoke-interface {v2, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 149
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
