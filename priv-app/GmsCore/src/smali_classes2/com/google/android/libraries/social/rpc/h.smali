.class public Lcom/google/android/libraries/social/rpc/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Le/a/a/l;


# static fields
.field public static final a:Lcom/google/android/libraries/social/f/a;

.field private static final f:Lcom/google/android/libraries/social/f/a;

.field private static final g:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field protected final b:Landroid/content/Context;

.field protected final c:Lcom/google/android/libraries/social/rpc/q;

.field protected final d:I

.field public final e:Ljava/util/List;

.field private final h:Ljava/lang/String;

.field private final i:Landroid/os/ConditionVariable;

.field private final j:Ljava/lang/String;

.field private final k:Lcom/google/android/libraries/social/rpc/l;

.field private final l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Lcom/google/android/libraries/social/rpc/i;

.field private o:[B

.field private p:Le/a/a/g;

.field private q:I

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/Exception;

.field private t:I

.field private u:Ljava/nio/channels/WritableByteChannel;

.field private v:I

.field private w:Z

.field private x:Lcom/google/android/libraries/social/t/a/a;

.field private final y:Lcom/google/android/libraries/social/rpc/m;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Lcom/google/android/libraries/social/f/a;

    const-string v1, "debug.rpc.dogfood"

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/f/a;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/social/rpc/h;->a:Lcom/google/android/libraries/social/f/a;

    .line 51
    new-instance v0, Lcom/google/android/libraries/social/f/a;

    const-string v1, "debug.rpc.metrics"

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/f/a;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/social/rpc/h;->f:Lcom/google/android/libraries/social/f/a;

    .line 59
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/google/android/libraries/social/rpc/h;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/social/rpc/q;Ljava/lang/String;Lcom/google/android/libraries/social/rpc/l;)V
    .locals 6

    .prologue
    .line 96
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/social/rpc/h;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/rpc/q;Ljava/lang/String;Lcom/google/android/libraries/social/rpc/l;B)V

    .line 97
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/social/rpc/q;Ljava/lang/String;Lcom/google/android/libraries/social/rpc/l;B)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    sget-object v0, Lcom/google/android/libraries/social/rpc/h;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/rpc/h;->d:I

    .line 66
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->i:Landroid/os/ConditionVariable;

    .line 75
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/social/rpc/h;->q:I

    .line 78
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/libraries/social/rpc/h;->t:I

    .line 111
    iput-object p1, p0, Lcom/google/android/libraries/social/rpc/h;->b:Landroid/content/Context;

    .line 112
    iput-object p2, p0, Lcom/google/android/libraries/social/rpc/h;->c:Lcom/google/android/libraries/social/rpc/q;

    .line 113
    iput-object p3, p0, Lcom/google/android/libraries/social/rpc/h;->j:Ljava/lang/String;

    .line 114
    iput-object p4, p0, Lcom/google/android/libraries/social/rpc/h;->k:Lcom/google/android/libraries/social/rpc/l;

    .line 115
    iput-object v1, p0, Lcom/google/android/libraries/social/rpc/h;->h:Ljava/lang/String;

    .line 116
    iput-object v1, p0, Lcom/google/android/libraries/social/rpc/h;->l:Ljava/lang/String;

    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->b:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/rpc/g;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->e:Ljava/util/List;

    .line 118
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->b:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/t/a/a;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/t/a/a;

    iput-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->x:Lcom/google/android/libraries/social/t/a/a;

    .line 119
    new-instance v0, Lcom/google/android/libraries/social/rpc/m;

    invoke-direct {v0}, Lcom/google/android/libraries/social/rpc/m;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->y:Lcom/google/android/libraries/social/rpc/m;

    .line 120
    return-void
.end method

.method private a(ILjava/lang/Exception;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v0, 0xc8

    .line 543
    if-ne p1, v0, :cond_1

    if-eqz p2, :cond_1

    .line 544
    const/4 p1, 0x0

    .line 557
    :cond_0
    :goto_0
    iput p1, p0, Lcom/google/android/libraries/social/rpc/h;->q:I

    .line 558
    iput-object v2, p0, Lcom/google/android/libraries/social/rpc/h;->r:Ljava/lang/String;

    .line 559
    iput-object p2, p0, Lcom/google/android/libraries/social/rpc/h;->s:Ljava/lang/Exception;

    .line 560
    return-void

    .line 545
    :cond_1
    if-eq p1, v0, :cond_2

    if-eqz p1, :cond_2

    if-nez p2, :cond_2

    .line 546
    new-instance p2, Lorg/apache/http/client/HttpResponseException;

    invoke-direct {p2, p1, v2}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    goto :goto_0

    .line 547
    :cond_2
    if-nez p1, :cond_0

    .line 549
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->x:Lcom/google/android/libraries/social/t/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->x:Lcom/google/android/libraries/social/t/a/a;

    instance-of v0, v0, Lcom/google/android/libraries/social/t/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->p:Le/a/a/g;

    if-eqz v0, :cond_0

    .line 551
    const-string v0, "HttpOperation"

    const-string v1, "logging failed rpc request."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->x:Lcom/google/android/libraries/social/t/a/a;

    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->b:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->c:Lcom/google/android/libraries/social/rpc/q;

    iget-object v0, v0, Lcom/google/android/libraries/social/rpc/q;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->p:Le/a/a/g;

    invoke-interface {v0}, Le/a/a/g;->a()Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->p:Le/a/a/g;

    invoke-interface {v0}, Le/a/a/g;->f()I

    invoke-virtual {p0}, Lcom/google/android/libraries/social/rpc/h;->c()Ljava/lang/String;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 209
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/h;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 211
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/rpc/g;

    .line 212
    invoke-virtual {p0}, Lcom/google/android/libraries/social/rpc/h;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/google/android/libraries/social/rpc/g;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 213
    iget-object v3, p0, Lcom/google/android/libraries/social/rpc/h;->b:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/libraries/social/rpc/h;->c:Lcom/google/android/libraries/social/rpc/q;

    iget-object v3, v3, Lcom/google/android/libraries/social/rpc/q;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/rpc/h;->c()Ljava/lang/String;

    iget v3, p0, Lcom/google/android/libraries/social/rpc/h;->d:I

    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/rpc/g;->c(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 216
    :catch_0
    move-exception v0

    .line 217
    const-string v3, "HttpOperation"

    const-string v4, "Couldn\'t log request"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 220
    :cond_1
    return-void
.end method

.method private h()V
    .locals 5

    .prologue
    .line 198
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/h;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 200
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/rpc/g;

    iget-object v3, p0, Lcom/google/android/libraries/social/rpc/h;->b:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/libraries/social/rpc/h;->c:Lcom/google/android/libraries/social/rpc/q;

    iget-object v3, v3, Lcom/google/android/libraries/social/rpc/q;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/rpc/h;->c()Ljava/lang/String;

    iget v3, p0, Lcom/google/android/libraries/social/rpc/h;->d:I

    iget-object v3, p0, Lcom/google/android/libraries/social/rpc/h;->n:Lcom/google/android/libraries/social/rpc/i;

    iget v4, p0, Lcom/google/android/libraries/social/rpc/h;->q:I

    iget-object v4, p0, Lcom/google/android/libraries/social/rpc/h;->s:Ljava/lang/Exception;

    iget-object v4, p0, Lcom/google/android/libraries/social/rpc/h;->z:Ljava/lang/String;

    invoke-interface {v0, v3}, Lcom/google/android/libraries/social/rpc/g;->a(Lcom/google/android/libraries/social/rpc/i;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 202
    :catch_0
    move-exception v0

    .line 203
    const-string v3, "HttpOperation"

    const-string v4, "Couldn\'t save network data"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 206
    :cond_0
    return-void
.end method

.method private i()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 237
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    .line 238
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/rpc/g;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/rpc/h;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Lcom/google/android/libraries/social/rpc/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239
    const/4 v0, 0x1

    .line 242
    :goto_1
    return v0

    .line 237
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 242
    goto :goto_1
.end method

.method private j()V
    .locals 10

    .prologue
    const/16 v9, 0x191

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 256
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->y:Lcom/google/android/libraries/social/rpc/m;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/rpc/m;->a()V

    .line 260
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->k:Lcom/google/android/libraries/social/rpc/l;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/rpc/h;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/rpc/l;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    .line 261
    const-string v0, "HttpOperation"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "HTTP headers:\n"

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    const-string v1, "Authorization"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "Authorization: <removed>"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    const-string v0, "\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 309
    :catch_0
    move-exception v0

    .line 310
    const/4 v1, 0x0

    :try_start_1
    invoke-direct {p0, v1, v0}, Lcom/google/android/libraries/social/rpc/h;->a(ILjava/lang/Exception;)V

    .line 311
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->s:Ljava/lang/Exception;

    if-nez v0, :cond_b

    move v0, v6

    :goto_2
    if-nez v0, :cond_0

    .line 312
    const-string v0, "HttpOperation"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/libraries/social/rpc/h;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] Unexpected exception"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/social/rpc/h;->s:Ljava/lang/Exception;

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 315
    :cond_0
    iput-object v8, p0, Lcom/google/android/libraries/social/rpc/h;->o:[B

    .line 316
    iput-object v8, p0, Lcom/google/android/libraries/social/rpc/h;->p:Le/a/a/g;

    .line 317
    :goto_3
    return-void

    .line 261
    :cond_1
    :try_start_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ": "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 315
    :catchall_0
    move-exception v0

    iput-object v8, p0, Lcom/google/android/libraries/social/rpc/h;->o:[B

    .line 316
    iput-object v8, p0, Lcom/google/android/libraries/social/rpc/h;->p:Le/a/a/g;

    throw v0

    .line 261
    :cond_2
    :try_start_3
    const-string v0, "HttpOperation"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    :cond_3
    monitor-enter p0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 264
    :try_start_4
    iget-boolean v0, p0, Lcom/google/android/libraries/social/rpc/h;->w:Z

    if-eqz v0, :cond_4

    .line 265
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 315
    iput-object v8, p0, Lcom/google/android/libraries/social/rpc/h;->o:[B

    .line 316
    iput-object v8, p0, Lcom/google/android/libraries/social/rpc/h;->p:Le/a/a/g;

    goto :goto_3

    .line 268
    :cond_4
    :try_start_5
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->u:Ljava/nio/channels/WritableByteChannel;

    if-eqz v0, :cond_9

    .line 269
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->b:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/rpc/h;->g()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/google/android/libraries/social/rpc/h;->t:I

    iget-object v4, p0, Lcom/google/android/libraries/social/rpc/h;->u:Ljava/nio/channels/WritableByteChannel;

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/libraries/social/n/a;->a(Landroid/content/Context;Ljava/lang/String;ILjava/util/Map;Ljava/nio/channels/WritableByteChannel;Le/a/a/l;)Le/a/a/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->p:Le/a/a/g;

    .line 277
    :goto_4
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->i:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->close()V

    .line 278
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 280
    :try_start_6
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->y:Lcom/google/android/libraries/social/rpc/m;

    iget v1, v0, Lcom/google/android/libraries/social/rpc/m;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/libraries/social/rpc/m;->c:I

    .line 282
    invoke-virtual {p0}, Lcom/google/android/libraries/social/rpc/h;->d()[B

    move-result-object v0

    .line 283
    if-eqz v0, :cond_5

    .line 284
    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/h;->p:Le/a/a/g;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/rpc/h;->f()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Le/a/a/g;->a(Ljava/lang/String;[B)V

    .line 285
    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/h;->y:Lcom/google/android/libraries/social/rpc/m;

    array-length v0, v0

    int-to-long v2, v0

    iput-wide v2, v1, Lcom/google/android/libraries/social/rpc/m;->a:J

    .line 287
    invoke-direct {p0}, Lcom/google/android/libraries/social/rpc/h;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 288
    invoke-virtual {p0}, Lcom/google/android/libraries/social/rpc/h;->e()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/rpc/h;->a(Ljava/lang/String;)V

    .line 291
    :cond_5
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->p:Le/a/a/g;

    invoke-interface {v0}, Le/a/a/g;->b()V

    .line 304
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->i:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 307
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->p:Le/a/a/g;

    .line 308
    iget v0, p0, Lcom/google/android/libraries/social/rpc/h;->q:I

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_a

    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->n:Lcom/google/android/libraries/social/rpc/i;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->n:Lcom/google/android/libraries/social/rpc/i;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/libraries/social/rpc/i;->e:J

    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->o:[B

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->y:Lcom/google/android/libraries/social/rpc/m;

    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/h;->o:[B

    array-length v1, v1

    int-to-long v2, v1

    iput-wide v2, v0, Lcom/google/android/libraries/social/rpc/m;->b:J

    :cond_7
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->o:[B

    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/h;->m:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/rpc/h;->a([B)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 315
    :cond_8
    :goto_5
    iput-object v8, p0, Lcom/google/android/libraries/social/rpc/h;->o:[B

    .line 316
    iput-object v8, p0, Lcom/google/android/libraries/social/rpc/h;->p:Le/a/a/g;

    goto/16 :goto_3

    .line 272
    :cond_9
    :try_start_7
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->b:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/rpc/h;->g()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/google/android/libraries/social/rpc/h;->t:I

    invoke-static {v0, v1, v2, v3, p0}, Lcom/google/android/libraries/social/n/a;->a(Landroid/content/Context;Ljava/lang/String;ILjava/util/Map;Le/a/a/l;)Le/a/a/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->p:Le/a/a/g;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_4

    .line 278
    :catchall_1
    move-exception v0

    :try_start_8
    monitor-exit p0

    throw v0

    .line 308
    :cond_a
    iget v0, p0, Lcom/google/android/libraries/social/rpc/h;->q:I

    if-eq v0, v9, :cond_8

    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->o:[B

    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/h;->m:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/rpc/h;->a([BLjava/lang/String;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_5

    .line 311
    :cond_b
    :try_start_9
    instance-of v1, v0, Ljava/net/SocketException;

    if-eqz v1, :cond_c

    move v0, v6

    goto/16 :goto_2

    :cond_c
    instance-of v1, v0, Ljava/net/UnknownHostException;

    if-eqz v1, :cond_d

    move v0, v6

    goto/16 :goto_2

    :cond_d
    instance-of v1, v0, Ljavax/net/ssl/SSLException;

    if-eqz v1, :cond_e

    move v0, v6

    goto/16 :goto_2

    :cond_e
    instance-of v1, v0, Lorg/apache/http/client/HttpResponseException;

    if-eqz v1, :cond_f

    check-cast v0, Lorg/apache/http/client/HttpResponseException;

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result v0

    if-ne v0, v9, :cond_f

    move v0, v6

    goto/16 :goto_2

    :cond_f
    move v0, v7

    goto/16 :goto_2
.end method

.method private k()V
    .locals 3

    .prologue
    .line 478
    iget v0, p0, Lcom/google/android/libraries/social/rpc/h;->v:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/social/rpc/h;->v:I

    .line 479
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->s:Ljava/lang/Exception;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/rpc/h;->a(Ljava/lang/Exception;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/libraries/social/rpc/h;->v:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 481
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->s:Ljava/lang/Exception;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/rpc/h;->a(Ljava/lang/Exception;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 482
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->k:Lcom/google/android/libraries/social/rpc/l;

    invoke-interface {v0}, Lcom/google/android/libraries/social/rpc/l;->a()V

    .line 485
    :cond_0
    const-string v0, "HttpOperation"

    const-string v1, "====> Restarting operation..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    invoke-direct {p0}, Lcom/google/android/libraries/social/rpc/h;->j()V

    .line 487
    invoke-direct {p0}, Lcom/google/android/libraries/social/rpc/h;->k()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 496
    :goto_0
    return-void

    .line 489
    :catch_0
    move-exception v0

    .line 490
    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/google/android/libraries/social/rpc/h;->a(ILjava/lang/Exception;)V

    .line 491
    const-string v1, "HttpOperation"

    const-string v2, "Failed to invalidate auth token"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 495
    :cond_1
    iget v0, p0, Lcom/google/android/libraries/social/rpc/h;->q:I

    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->r:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->s:Ljava/lang/Exception;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->b:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/rpc/f;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/rpc/f;

    .line 147
    invoke-interface {v0, p0}, Lcom/google/android/libraries/social/rpc/f;->a(Lcom/google/android/libraries/social/rpc/h;)V

    .line 148
    return-void
.end method

.method public final a(Le/a/a/g;)V
    .locals 4

    .prologue
    .line 382
    invoke-interface {p1}, Le/a/a/g;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->z:Ljava/lang/String;

    .line 383
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->n:Lcom/google/android/libraries/social/rpc/i;

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->n:Lcom/google/android/libraries/social/rpc/i;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/libraries/social/rpc/i;->d:J

    .line 385
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->y:Lcom/google/android/libraries/social/rpc/m;

    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/h;->z:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/libraries/social/rpc/m;->d:Ljava/lang/String;

    .line 387
    :cond_0
    return-void
.end method

.method public a([B)V
    .locals 1

    .prologue
    .line 356
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/rpc/h;->c(Ljava/lang/String;)V

    .line 357
    return-void
.end method

.method public a([BLjava/lang/String;)V
    .locals 1

    .prologue
    .line 361
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/rpc/h;->c(Ljava/lang/String;)V

    .line 362
    return-void
.end method

.method public a(Ljava/lang/Exception;)Z
    .locals 1

    .prologue
    .line 529
    instance-of v0, p1, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_0

    .line 530
    check-cast p1, Lorg/apache/http/client/HttpResponseException;

    .line 531
    invoke-virtual {p1}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 536
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 533
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 531
    nop

    :pswitch_data_0
    .packed-switch 0x191
        :pswitch_0
    .end packed-switch
.end method

.method final b()V
    .locals 12

    .prologue
    const/4 v2, 0x1

    const-wide/16 v10, 0x0

    const/4 v1, 0x0

    .line 151
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->c:Lcom/google/android/libraries/social/rpc/q;

    iget-object v0, v0, Lcom/google/android/libraries/social/rpc/q;->e:Lcom/google/android/libraries/social/rpc/i;

    iput-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->n:Lcom/google/android/libraries/social/rpc/i;

    .line 152
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->n:Lcom/google/android/libraries/social/rpc/i;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/libraries/social/rpc/h;->f:Lcom/google/android/libraries/social/f/a;

    .line 154
    :cond_0
    sget-object v0, Lcom/google/android/libraries/social/rpc/h;->a:Lcom/google/android/libraries/social/f/a;

    .line 159
    const-string v0, "HttpOperation"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    const-string v0, "HttpOperation"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Starting op: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/libraries/social/rpc/h;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->c:Lcom/google/android/libraries/social/rpc/q;

    iget-boolean v0, v0, Lcom/google/android/libraries/social/rpc/q;->d:Z

    if-eqz v0, :cond_2

    .line 167
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/libraries/social/rpc/h;->t:I

    .line 170
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->n:Lcom/google/android/libraries/social/rpc/i;

    if-eqz v0, :cond_4

    .line 173
    iget-object v3, p0, Lcom/google/android/libraries/social/rpc/h;->n:Lcom/google/android/libraries/social/rpc/i;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/rpc/h;->c()Ljava/lang/String;

    move-result-object v4

    new-array v5, v2, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/rpc/h;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    iget-object v0, v3, Lcom/google/android/libraries/social/rpc/i;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/rpc/j;

    iput-object v0, v3, Lcom/google/android/libraries/social/rpc/i;->b:Lcom/google/android/libraries/social/rpc/j;

    iget-object v0, v3, Lcom/google/android/libraries/social/rpc/i;->b:Lcom/google/android/libraries/social/rpc/j;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/libraries/social/rpc/j;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/rpc/j;-><init>(B)V

    iput-object v0, v3, Lcom/google/android/libraries/social/rpc/i;->b:Lcom/google/android/libraries/social/rpc/j;

    iget-object v0, v3, Lcom/google/android/libraries/social/rpc/i;->b:Lcom/google/android/libraries/social/rpc/j;

    iput-object v4, v0, Lcom/google/android/libraries/social/rpc/j;->a:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/android/libraries/social/rpc/i;->b:Lcom/google/android/libraries/social/rpc/j;

    iput-object v5, v0, Lcom/google/android/libraries/social/rpc/j;->h:[Ljava/lang/String;

    iget-object v0, v3, Lcom/google/android/libraries/social/rpc/i;->a:Ljava/util/HashMap;

    iget-object v5, v3, Lcom/google/android/libraries/social/rpc/i;->b:Lcom/google/android/libraries/social/rpc/j;

    invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v3, Lcom/google/android/libraries/social/rpc/i;->c:J

    iput-wide v10, v3, Lcom/google/android/libraries/social/rpc/i;->e:J

    .line 176
    :cond_4
    invoke-direct {p0}, Lcom/google/android/libraries/social/rpc/h;->j()V

    .line 178
    invoke-direct {p0}, Lcom/google/android/libraries/social/rpc/h;->k()V

    .line 180
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->n:Lcom/google/android/libraries/social/rpc/i;

    if-eqz v0, :cond_6

    .line 182
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->n:Lcom/google/android/libraries/social/rpc/i;

    iget-object v3, p0, Lcom/google/android/libraries/social/rpc/h;->y:Lcom/google/android/libraries/social/rpc/m;

    iget-object v4, v0, Lcom/google/android/libraries/social/rpc/i;->b:Lcom/google/android/libraries/social/rpc/j;

    iget-wide v6, v3, Lcom/google/android/libraries/social/rpc/m;->b:J

    iget-wide v8, v4, Lcom/google/android/libraries/social/rpc/j;->e:J

    add-long/2addr v6, v8

    iput-wide v6, v4, Lcom/google/android/libraries/social/rpc/j;->e:J

    iget-object v4, v0, Lcom/google/android/libraries/social/rpc/i;->b:Lcom/google/android/libraries/social/rpc/j;

    iget-wide v6, v3, Lcom/google/android/libraries/social/rpc/m;->a:J

    iget-wide v8, v4, Lcom/google/android/libraries/social/rpc/j;->f:J

    add-long/2addr v6, v8

    iput-wide v6, v4, Lcom/google/android/libraries/social/rpc/j;->f:J

    iget-object v4, v0, Lcom/google/android/libraries/social/rpc/i;->b:Lcom/google/android/libraries/social/rpc/j;

    iget v5, v3, Lcom/google/android/libraries/social/rpc/m;->c:I

    int-to-long v6, v5

    iget-wide v8, v4, Lcom/google/android/libraries/social/rpc/j;->d:J

    add-long/2addr v6, v8

    iput-wide v6, v4, Lcom/google/android/libraries/social/rpc/j;->d:J

    iget-object v0, v0, Lcom/google/android/libraries/social/rpc/i;->b:Lcom/google/android/libraries/social/rpc/j;

    iget-object v3, v3, Lcom/google/android/libraries/social/rpc/m;->d:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/android/libraries/social/rpc/j;->g:Ljava/lang/String;

    .line 183
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->y:Lcom/google/android/libraries/social/rpc/m;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/rpc/m;->a()V

    .line 184
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->n:Lcom/google/android/libraries/social/rpc/i;

    iget-wide v4, v0, Lcom/google/android/libraries/social/rpc/i;->e:J

    cmp-long v3, v4, v10

    if-eqz v3, :cond_5

    iget-object v3, v0, Lcom/google/android/libraries/social/rpc/i;->b:Lcom/google/android/libraries/social/rpc/j;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, v0, Lcom/google/android/libraries/social/rpc/i;->e:J

    sub-long/2addr v4, v6

    iget-wide v6, v3, Lcom/google/android/libraries/social/rpc/j;->c:J

    add-long/2addr v4, v6

    iput-wide v4, v3, Lcom/google/android/libraries/social/rpc/j;->c:J

    iput-wide v10, v0, Lcom/google/android/libraries/social/rpc/i;->e:J

    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v0, Lcom/google/android/libraries/social/rpc/i;->f:J

    iget-object v3, v0, Lcom/google/android/libraries/social/rpc/i;->b:Lcom/google/android/libraries/social/rpc/j;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, v0, Lcom/google/android/libraries/social/rpc/i;->c:J

    sub-long/2addr v4, v6

    iget-wide v6, v3, Lcom/google/android/libraries/social/rpc/j;->b:J

    add-long/2addr v4, v6

    iput-wide v4, v3, Lcom/google/android/libraries/social/rpc/j;->b:J

    .line 185
    invoke-direct {p0}, Lcom/google/android/libraries/social/rpc/h;->h()V

    .line 187
    :cond_6
    iget v0, p0, Lcom/google/android/libraries/social/rpc/h;->q:I

    const/16 v3, 0xc8

    if-ne v0, v3, :cond_7

    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->s:Ljava/lang/Exception;

    if-eqz v0, :cond_9

    :cond_7
    move v0, v2

    :goto_0
    if-eqz v0, :cond_8

    const-string v0, "HttpOperation"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 193
    const-string v0, "HttpOperation"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/libraries/social/rpc/h;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] failed due to error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/libraries/social/rpc/h;->q:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/social/rpc/h;->s:Ljava/lang/Exception;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    :cond_8
    return-void

    :cond_9
    move v0, v1

    .line 187
    goto :goto_0
.end method

.method public final b(Le/a/a/g;)V
    .locals 2

    .prologue
    .line 366
    invoke-interface {p1}, Le/a/a/g;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 367
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/rpc/h;->w:Z

    .line 377
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->i:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 378
    return-void

    .line 369
    :cond_0
    invoke-interface {p1}, Le/a/a/g;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->m:Ljava/lang/String;

    .line 370
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->u:Ljava/nio/channels/WritableByteChannel;

    if-nez v0, :cond_1

    .line 371
    invoke-interface {p1}, Le/a/a/g;->h()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->o:[B

    .line 374
    :cond_1
    invoke-interface {p1}, Le/a/a/g;->f()I

    move-result v0

    invoke-interface {p1}, Le/a/a/g;->g()Ljava/io/IOException;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/social/rpc/h;->a(ILjava/lang/Exception;)V

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 416
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final c(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 223
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/libraries/social/rpc/h;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 225
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/rpc/g;

    .line 226
    invoke-virtual {p0}, Lcom/google/android/libraries/social/rpc/h;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/google/android/libraries/social/rpc/g;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 227
    iget-object v3, p0, Lcom/google/android/libraries/social/rpc/h;->b:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/libraries/social/rpc/h;->c:Lcom/google/android/libraries/social/rpc/q;

    iget-object v3, v3, Lcom/google/android/libraries/social/rpc/q;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/rpc/h;->c()Ljava/lang/String;

    iget v3, p0, Lcom/google/android/libraries/social/rpc/h;->d:I

    iget v3, p0, Lcom/google/android/libraries/social/rpc/h;->q:I

    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/rpc/g;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 230
    :catch_0
    move-exception v0

    .line 231
    const-string v3, "HttpOperation"

    const-string v4, "Couldn\'t log response"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 234
    :cond_1
    return-void
.end method

.method public d()[B
    .locals 1

    .prologue
    .line 449
    const/4 v0, 0x0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 474
    const/4 v0, 0x0

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->l:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 587
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/h;->h:Ljava/lang/String;

    return-object v0
.end method
