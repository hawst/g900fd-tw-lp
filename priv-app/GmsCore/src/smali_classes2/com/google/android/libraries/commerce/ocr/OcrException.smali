.class public Lcom/google/android/libraries/commerce/ocr/OcrException;
.super Ljava/lang/Exception;
.source "SourceFile"


# static fields
.field public static final OUT_OF_MEMORY:I = 0x5

.field public static final REQUEST_LIMIT:I = 0x3

.field public static final SERVER_ERROR:I = 0x1

.field public static final TIMEOUT:I = 0x2

.field public static final UNKNOWN:I = 0x0

.field public static final ZXING:I = 0x4


# instance fields
.field private final type:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/commerce/ocr/OcrException;-><init>(ILjava/lang/Throwable;)V

    .line 16
    return-void
.end method

.method public constructor <init>(ILjava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "OcrException of type="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 20
    iput p1, p0, Lcom/google/android/libraries/commerce/ocr/OcrException;->type:I

    .line 21
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 54
    if-eqz p1, :cond_0

    instance-of v2, p1, Lcom/google/android/libraries/commerce/ocr/OcrException;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 62
    :cond_1
    :goto_0
    return v0

    .line 57
    :cond_2
    if-eq p0, p1, :cond_1

    .line 61
    check-cast p1, Lcom/google/android/libraries/commerce/ocr/OcrException;

    .line 62
    iget v2, p0, Lcom/google/android/libraries/commerce/ocr/OcrException;->type:I

    iget v3, p1, Lcom/google/android/libraries/commerce/ocr/OcrException;->type:I

    if-ne v2, v3, :cond_3

    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/OcrException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/OcrException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/OcrException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/OcrException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/OcrException;->type:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 68
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/libraries/commerce/ocr/OcrException;->type:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-super {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/OcrException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
