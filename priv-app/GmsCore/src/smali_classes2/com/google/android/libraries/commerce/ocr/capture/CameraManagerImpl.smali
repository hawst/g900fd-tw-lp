.class public Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;
.implements Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;


# static fields
.field private static final MIN_SDK_VERSION_FOR_PREVIEW_CALLBACK_BUFFER:I = 0xb

.field private static final TAG:Ljava/lang/String; = "CameraManagerImpl"


# instance fields
.field private final attemptToUseContinuousPictureFocusMode:Z

.field camera:Landroid/hardware/Camera;

.field containerSize:Landroid/graphics/Point;

.field private final continuousPreviewImageCallbacks:Ljava/util/List;

.field private focusing:Z

.field inContinuousPictureFocusMode:Z

.field isCameraActive:Z

.field oneShotImageCallback:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$ImageCallback;

.field private orientation:I

.field private final orientationOffset:I

.field private pictureSize:Landroid/graphics/Point;

.field private previewCallbackBuffer:Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

.field private previewFormat:I

.field private previewSize:Landroid/graphics/Point;

.field private final screenManager:Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;

.field private final shapeModifier:Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;

.field private final sizeSelector:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;

.field private final targetPictureSize:Landroid/graphics/Point;

.field private final targetPreviewSize:Landroid/graphics/Point;

.field private final usePreviewCallbackBuffer:Z


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;ILcom/google/android/libraries/commerce/ocr/util/ShapeModifier;Landroid/graphics/Point;Landroid/graphics/Point;Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;Z)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->screenManager:Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;

    .line 79
    iput p2, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->orientationOffset:I

    .line 80
    iput-object p3, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->shapeModifier:Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;

    .line 81
    iput-object p4, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->targetPreviewSize:Landroid/graphics/Point;

    .line 82
    iput-object p5, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->targetPictureSize:Landroid/graphics/Point;

    .line 83
    iput-boolean p7, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->attemptToUseContinuousPictureFocusMode:Z

    .line 84
    invoke-direct {p0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->usePreviewCallbackBuffer()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->usePreviewCallbackBuffer:Z

    .line 85
    const/16 v0, 0x11

    iput v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->previewFormat:I

    .line 86
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->continuousPreviewImageCallbacks:Ljava/util/List;

    .line 87
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;

    invoke-direct {v0, p0, p6}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;-><init>(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;)V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->sizeSelector:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;

    .line 88
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;Z)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->setFocusing(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;)Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->previewSize:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->previewSize:Landroid/graphics/Point;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;)Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->targetPreviewSize:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;II)Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->adjustForScreenOrientation(II)Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;)Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->shapeModifier:Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;)Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->pictureSize:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;)Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->targetPictureSize:Landroid/graphics/Point;

    return-object v0
.end method

.method private declared-synchronized addCallbackBuffer()V
    .locals 2

    .prologue
    .line 343
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->previewCallbackBuffer:Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    invoke-virtual {v1}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getData()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->addCallbackBuffer([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346
    :cond_0
    monitor-exit p0

    return-void

    .line 343
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private adjustForScreenOrientation(II)Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->screenManager:Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;->getScreenOrientation()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 435
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, p2, p1}, Landroid/graphics/Point;-><init>(II)V

    :goto_0
    return-object v0

    .line 431
    :pswitch_0
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, p1, p2}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_0

    .line 428
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private getPreviewBufferSize()I
    .locals 3

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    .line 286
    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v1

    .line 287
    iget v2, v1, Landroid/hardware/Camera$Size;->width:I

    iget v1, v1, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v1, v2

    .line 288
    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPreviewFormat()I

    move-result v0

    invoke-static {v0}, Landroid/graphics/ImageFormat;->getBitsPerPixel(I)I

    move-result v0

    mul-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x8

    .line 293
    add-int/lit8 v0, v0, 0x10

    .line 294
    return v0
.end method

.method private requestFullImage(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$ImageCallback;)V
    .locals 4

    .prologue
    .line 404
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->isCameraActive:Z

    .line 406
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/libraries/commerce/ocr/capture/b;

    invoke-direct {v3, p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/b;-><init>(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$ImageCallback;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/Camera;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 420
    :goto_0
    return-void

    .line 416
    :catch_0
    move-exception v0

    .line 418
    const-string v1, "CameraManagerImpl"

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setCameraParameters()V
    .locals 4

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    .line 302
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->sizeSelector:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->getPreviewSize(Landroid/hardware/Camera$Parameters;)Landroid/graphics/Point;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->previewSize:Landroid/graphics/Point;

    .line 303
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->sizeSelector:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->getPictureSize(Landroid/hardware/Camera$Parameters;)Landroid/graphics/Point;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->pictureSize:Landroid/graphics/Point;

    .line 305
    const-string v1, "CameraManagerImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "pic size: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->pictureSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->pictureSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->pictureSize:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->pictureSize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    .line 307
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->previewSize:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->previewSize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 309
    invoke-direct {p0, v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->setContinuousPictureFocusModeIfSupported(Landroid/hardware/Camera$Parameters;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->inContinuousPictureFocusMode:Z

    .line 310
    const-string v1, "CameraManagerImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Use continuous-picture-focus-mode: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->inContinuousPictureFocusMode:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPreviewFormat()I

    move-result v1

    iput v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->previewFormat:I

    .line 312
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 313
    return-void
.end method

.method private setContinuousPictureFocusModeIfSupported(Landroid/hardware/Camera$Parameters;)Z
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 327
    iget-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->attemptToUseContinuousPictureFocusMode:Z

    if-nez v0, :cond_0

    .line 339
    :goto_0
    return v1

    .line 332
    :cond_0
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getFocusMode()Ljava/lang/String;

    move-result-object v0

    .line 333
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v2

    const-string v3, "continuous-picture"

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 335
    const/4 v1, 0x1

    .line 336
    const-string v0, "continuous-picture"

    .line 338
    :cond_1
    invoke-virtual {p1, v0}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private declared-synchronized setFocusing(Z)V
    .locals 1

    .prologue
    .line 236
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->focusing:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237
    monitor-exit p0

    return-void

    .line 236
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V
    .locals 1

    .prologue
    .line 268
    iget-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->usePreviewCallbackBuffer:Z

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    .line 273
    :goto_0
    return-void

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    goto :goto_0
.end method

.method private usePreviewCallbackBuffer()Z
    .locals 2

    .prologue
    .line 281
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized addContinuousPreviewImageCallback(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$ImageCallback;)V
    .locals 1

    .prologue
    .line 246
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->continuousPreviewImageCallbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 247
    monitor-exit p0

    return-void

    .line 246
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public adjustForScreenOrientation(Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 2

    .prologue
    .line 440
    iget v0, p1, Landroid/graphics/Point;->x:I

    iget v1, p1, Landroid/graphics/Point;->y:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->adjustForScreenOrientation(II)Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized closeDriver()V
    .locals 2

    .prologue
    .line 140
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 141
    const-string v0, "CameraManagerImpl"

    const-string v1, "close camera driver"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 143
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 144
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->isCameraActive:Z

    .line 145
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 146
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->continuousPreviewImageCallbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 149
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->previewCallbackBuffer:Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    monitor-exit p0

    return-void

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 373
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->orientation:I

    return v0
.end method

.method protected getOrientationRelativeToDisplay()I
    .locals 4

    .prologue
    .line 357
    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 358
    const/4 v1, 0x0

    invoke-static {v1, v0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 359
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->screenManager:Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;

    invoke-virtual {v1}, Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;->getDisplayRotation()I

    move-result v1

    iget v2, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->orientationOffset:I

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x5a

    .line 361
    iget v2, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 363
    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/2addr v0, v1

    rem-int/lit16 v0, v0, 0x168

    .line 364
    rsub-int v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    .line 368
    :goto_0
    return v0

    .line 366
    :cond_0
    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    sub-int/2addr v0, v1

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    goto :goto_0
.end method

.method public getPictureSize()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->pictureSize:Landroid/graphics/Point;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->adjustForScreenOrientation(Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method public getPreviewFormat()I
    .locals 1

    .prologue
    .line 393
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->previewFormat:I

    return v0
.end method

.method public getPreviewSize()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->previewSize:Landroid/graphics/Point;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->adjustForScreenOrientation(Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method public getScreenFillingPreviewSize()Landroid/graphics/Point;
    .locals 3

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->sizeSelector:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->containerSize:Landroid/graphics/Point;

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->previewSize:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->getScreenFillingPreviewSize(Landroid/graphics/Point;Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized isFocusing()Z
    .locals 1

    .prologue
    .line 241
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->focusing:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isInContinuousPictureFocusMode()Z
    .locals 1

    .prologue
    .line 667
    iget-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->inContinuousPictureFocusMode:Z

    return v0
.end method

.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 4

    .prologue
    .line 93
    if-eqz p1, :cond_2

    .line 95
    iget-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->usePreviewCallbackBuffer:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->previewCallbackBuffer:Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    move-object v1, v0

    .line 100
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->oneShotImageCallback:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$ImageCallback;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->oneShotImageCallback:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$ImageCallback;

    .line 102
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->oneShotImageCallback:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$ImageCallback;

    .line 103
    invoke-interface {v0, v1}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$ImageCallback;->onImage(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)V

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->continuousPreviewImageCallbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 107
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->continuousPreviewImageCallbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$ImageCallback;

    .line 108
    invoke-interface {v0, v1}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$ImageCallback;->onImage(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)V

    goto :goto_1

    .line 95
    :cond_1
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->getPreviewFormat()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->previewSize:Landroid/graphics/Point;

    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->getOrientation()I

    move-result v3

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;-><init>([BILandroid/graphics/Point;I)V

    move-object v1, v0

    goto :goto_0

    .line 113
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->usePreviewCallbackBuffer:Z

    if-eqz v0, :cond_3

    .line 114
    invoke-direct {p0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->addCallbackBuffer()V

    .line 116
    :cond_3
    return-void
.end method

.method public declared-synchronized openDriver(Landroid/graphics/Point;Landroid/view/SurfaceHolder;Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;)V
    .locals 2

    .prologue
    .line 122
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->containerSize:Landroid/graphics/Point;

    .line 123
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    if-nez v0, :cond_0

    .line 124
    const-string v0, "CameraManagerImpl"

    const-string v1, "open camera driver"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    .line 126
    invoke-direct {p0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->setCameraParameters()V

    .line 127
    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->setOrientation()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 129
    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0, p2}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 134
    :goto_0
    :try_start_2
    invoke-interface {p3}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;->onFinish()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 136
    :cond_0
    monitor-exit p0

    return-void

    .line 132
    :catch_0
    move-exception v0

    :try_start_3
    const-string v0, "CameraManagerImpl"

    const-string v1, "camera driver failed to set preview display"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public requestAutoFocus()V
    .locals 1

    .prologue
    .line 230
    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->isFocusing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 231
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->requestAutoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    .line 233
    :cond_0
    return-void
.end method

.method public requestAutoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 197
    iget-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->inContinuousPictureFocusMode:Z

    if-eqz v0, :cond_1

    .line 200
    if-eqz p1, :cond_0

    .line 201
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    invoke-interface {p1, v1, v0}, Landroid/hardware/Camera$AutoFocusCallback;->onAutoFocus(ZLandroid/hardware/Camera;)V

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 206
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->isCameraActive:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->isFocusing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 207
    invoke-direct {p0, v1}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->setFocusing(Z)V

    .line 209
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    new-instance v1, Lcom/google/android/libraries/commerce/ocr/capture/a;

    invoke-direct {v1, p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/a;-><init>(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;Landroid/hardware/Camera$AutoFocusCallback;)V

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 218
    :catch_0
    move-exception v0

    .line 219
    const-string v1, "CameraManagerImpl"

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    invoke-direct {p0, v2}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->setFocusing(Z)V

    .line 221
    if-eqz p1, :cond_0

    .line 222
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    invoke-interface {p1, v2, v0}, Landroid/hardware/Camera$AutoFocusCallback;->onAutoFocus(ZLandroid/hardware/Camera;)V

    goto :goto_0
.end method

.method public requestImage(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$ImageCallback;B)V
    .locals 3

    .prologue
    .line 175
    iget-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->isCameraActive:Z

    if-nez v0, :cond_1

    .line 176
    const-string v0, "CameraManagerImpl"

    const-string v1, "Requesting image from inactive camera results in no-op."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    packed-switch p2, :pswitch_data_0

    .line 191
    const-string v0, "CameraManagerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported image format "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 182
    :pswitch_0
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->oneShotImageCallback:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$ImageCallback;

    .line 183
    iget-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->usePreviewCallbackBuffer:Z

    if-eqz v0, :cond_0

    .line 184
    invoke-direct {p0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->addCallbackBuffer()V

    goto :goto_0

    .line 188
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->requestFullImage(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$ImageCallback;)V

    goto :goto_0

    .line 180
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public restartPreview()V
    .locals 1

    .prologue
    .line 264
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->startPreview(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;)V

    .line 265
    return-void
.end method

.method public setOrientation()V
    .locals 3

    .prologue
    .line 350
    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->getOrientationRelativeToDisplay()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->orientation:I

    .line 351
    const-string v0, "CameraManagerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Orientation set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->orientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    iget v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->orientation:I

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    .line 353
    return-void
.end method

.method public startPreview(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;)V
    .locals 5

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->isCameraActive:Z

    if-nez v0, :cond_1

    .line 155
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->setFocusing(Z)V

    .line 156
    invoke-direct {p0, p0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 157
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V

    .line 158
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->isCameraActive:Z

    .line 161
    iget-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->usePreviewCallbackBuffer:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->previewCallbackBuffer:Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    if-nez v0, :cond_0

    .line 162
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    invoke-direct {p0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->getPreviewBufferSize()I

    move-result v1

    new-array v1, v1, [B

    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->getPreviewFormat()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->previewSize:Landroid/graphics/Point;

    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->getOrientation()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;-><init>([BILandroid/graphics/Point;I)V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->previewCallbackBuffer:Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    .line 164
    invoke-direct {p0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->addCallbackBuffer()V

    .line 167
    :cond_0
    if-eqz p1, :cond_1

    .line 168
    invoke-interface {p1}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;->onFinish()V

    .line 171
    :cond_1
    return-void
.end method

.method public declared-synchronized stopPreview()V
    .locals 1

    .prologue
    .line 251
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->isCameraActive:Z

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelAutoFocus()V

    .line 256
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 257
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->isCameraActive:Z

    .line 259
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->oneShotImageCallback:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$ImageCallback;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 260
    monitor-exit p0

    return-void

    .line 251
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public supportsAutoFocus()Z
    .locals 2

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v0

    .line 400
    const-string v1, "auto"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
