.class public final Lcom/google/android/libraries/social/rpc/a/h;
.super Lcom/google/android/libraries/social/rpc/a/d;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/libraries/social/rpc/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/social/rpc/a;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/social/rpc/a/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    iput-object p4, p0, Lcom/google/android/libraries/social/rpc/a/h;->a:Lcom/google/android/libraries/social/rpc/a;

    .line 37
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 52
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Le/a/a/n;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 53
    const-string v0, "; G+ SDK/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/a/h;->a:Lcom/google/android/libraries/social/rpc/a;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/rpc/a;->f()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "1.0.0"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/a/h;->a:Lcom/google/android/libraries/social/rpc/a;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/rpc/a;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Ljava/util/Map;
    .locals 8

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/rpc/a/d;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    .line 42
    const-string v3, "X-Container-Url"

    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/a/h;->a:Lcom/google/android/libraries/social/rpc/a;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/rpc/a;->e()Lcom/google/android/libraries/social/rpc/a;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/rpc/a;->e()Lcom/google/android/libraries/social/rpc/a;

    move-result-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/libraries/social/rpc/a;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/google/android/libraries/social/rpc/a;->d()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/libraries/social/rpc/a;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/libraries/social/rpc/a;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/libraries/social/rpc/a;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "http://"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, ".apps.googleusercontent.com/"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    if-eqz v4, :cond_1

    const-string v6, "client_id"

    invoke-virtual {v1, v6, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    if-eqz v5, :cond_2

    const-string v4, "api_key"

    invoke-virtual {v1, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_2
    if-eqz v0, :cond_3

    const-string v4, "pkg"

    invoke-virtual {v1, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_3
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    return-object v2

    .line 42
    :cond_4
    const-string v1, "0"

    goto :goto_0
.end method
