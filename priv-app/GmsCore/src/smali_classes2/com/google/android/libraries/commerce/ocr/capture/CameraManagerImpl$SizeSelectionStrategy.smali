.class public abstract enum Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;

.field public static final enum RATIO:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;

.field public static final enum RATIO_AND_HEIGHT:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 602
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/capture/c;

    const-string v1, "RATIO_AND_HEIGHT"

    invoke-direct {v0, v1}, Lcom/google/android/libraries/commerce/ocr/capture/c;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;->RATIO_AND_HEIGHT:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;

    .line 628
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/capture/d;

    const-string v1, "RATIO"

    invoke-direct {v0, v1}, Lcom/google/android/libraries/commerce/ocr/capture/d;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;->RATIO:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;

    .line 593
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;->RATIO_AND_HEIGHT:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;->RATIO:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;->$VALUES:[Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 593
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/google/android/libraries/commerce/ocr/capture/a;)V
    .locals 0

    .prologue
    .line 593
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;
    .locals 1

    .prologue
    .line 593
    const-class v0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;

    return-object v0
.end method

.method public static values()[Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;
    .locals 1

    .prologue
    .line 593
    sget-object v0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;->$VALUES:[Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;

    invoke-virtual {v0}, [Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;

    return-object v0
.end method


# virtual methods
.method public abstract getClosest(Ljava/util/List;Lcom/google/android/gms/common/util/ap;Landroid/graphics/Point;Landroid/graphics/Point;)Landroid/hardware/Camera$Size;
.end method

.method protected isCloserToTargetSize(Landroid/hardware/Camera$Size;Landroid/hardware/Camera$Size;D)Z
    .locals 3

    .prologue
    .line 661
    const-wide/16 v0, 0x0

    cmpl-double v0, p3, v0

    if-nez v0, :cond_0

    iget v0, p1, Landroid/hardware/Camera$Size;->width:I

    iget v1, p2, Landroid/hardware/Camera$Size;->width:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
