.class public final Lcom/google/android/libraries/social/mediaupload/l;
.super Ljava/lang/Exception;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Z


# direct methods
.method public constructor <init>(Ljava/lang/Exception;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1067
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    .line 1068
    iput-object p2, p0, Lcom/google/android/libraries/social/mediaupload/l;->a:Ljava/lang/String;

    .line 1069
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/mediaupload/l;->b:Z

    .line 1070
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1051
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/mediaupload/l;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1052
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;B)V
    .locals 1

    .prologue
    .line 1055
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 1056
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/mediaupload/l;->b:Z

    .line 1057
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/mediaupload/l;->a:Ljava/lang/String;

    .line 1058
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1061
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 1062
    iput-object p2, p0, Lcom/google/android/libraries/social/mediaupload/l;->a:Ljava/lang/String;

    .line 1063
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/mediaupload/l;->b:Z

    .line 1064
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1073
    iget-object v0, p0, Lcom/google/android/libraries/social/mediaupload/l;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1077
    iget-boolean v0, p0, Lcom/google/android/libraries/social/mediaupload/l;->b:Z

    return v0
.end method
