.class public Lcom/google/android/libraries/commerce/ocr/pub/Intents;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final AUTO_SNAP_MODEL_BLACKLIST:Ljava/lang/String; = "AUTO_SNAP_MODEL_BLACKLIST"

.field public static final CONTINUOUS_PICTURE_FOCUS_MODE_MODEL_BLACKLIST:Ljava/lang/String; = "CONTINUOUS_PICTURE_FOCUS_MODE_MODEL_BLACKLIST"

.field public static final CREDIT_CARD_OCR_RESULT:Ljava/lang/String; = "CREDIT_CARD_OCR_RESULT"

.field public static final DEBUG_CONTINUOUS_SAVE:Ljava/lang/String; = "DEBUG_CONTINUOUS_SAVE"

.field public static final DEBUG_EXPIRATION_DATE_CONFIDENCE:Ljava/lang/String; = "DEBUG_EXPIRATION_DATE_CONFIDENCE"

.field public static final DEBUG_EXPIRATION_DATE_PREDICTION_STATUS:Ljava/lang/String; = "DEBUG_EXPIRATION_DATE_PREDICTION_STATUS"

.field public static final DEBUG_IMAGE_CAPTURE_MODE:Ljava/lang/String; = "DEBUG_IMAGE_CAPTURE_MODE"

.field public static final DEBUG_IMAGE_COMPRESSION_TIME:Ljava/lang/String; = "DEBUG_IMAGE_COMPRESSION_TIME"

.field public static final DEBUG_MEAN_DIGIT_CONFIDENCE:Ljava/lang/String; = "DEBUG_MEAN_DIGIT_CONFIDENCE"

.field public static final DEBUG_MIN_DIGIT_CONFIDENCE:Ljava/lang/String; = "DEBUG_MIN_DIGIT_CONFIDENCE"

.field public static final DEBUG_OCR_SCAN_TIME:Ljava/lang/String; = "DEBUG_OCR_SCAN_TIME"

.field public static final DEBUG_OCR_TIME:Ljava/lang/String; = "DEBUG_OCR_TIME"

.field public static final DEBUG_RECTIFICATION_TIME:Ljava/lang/String; = "DEBUG_RECTIFICATION_TIME"

.field public static final DEBUG_RECTIFIED_EXPIRATION_DATE_IMAGE:Ljava/lang/String; = "DEBUG_RECTIFIED_EXPIRATION_DATE_IMAGE"

.field public static final DEBUG_RECTIFIED_OCR_IMAGE:Ljava/lang/String; = "DEBUG_RECTIFIED_OCR_IMAGE"

.field public static final DEBUG_REQUEST_SIZE:Ljava/lang/String; = "DEBUG_REQUEST_SIZE"

.field public static final DEBUG_SAVE_OCR_IMAGE:Ljava/lang/String; = "DEBUG_SAVE_OCR_IMAGE"

.field public static final DEBUG_VALIDATE_RESULT:Ljava/lang/String; = "DEBUG_VALIDATE_RESULT"

.field public static final EDGE_FINDER_WIDTH_TO_OUTER_BOUNDING_BOX_EDGE_LENGTH_RATIO:Ljava/lang/String; = "EDGE_FINDER_WIDTH_TO_OUTER_BOUNDING_BOX_EDGE_LENGTH_RATIO"

.field public static final FULLSCREEN_MODE:Ljava/lang/String; = "FULLSCREEN_MODE"

.field public static final LOCK_TO_PORTRAIT_MODE:Ljava/lang/String; = "LOCK_TO_PORTRAIT_MODE"

.field public static final MIN_PERFORM_OCR_INTERVAL_IN_MS:Ljava/lang/String; = "MIN_PERFORM_LOYALTY_CARD_OCR_INTERVAL_IN_MS"

.field public static final ORIENTATION_OFFSET:Ljava/lang/String; = "ORIENTATION_OFFSET"

.field public static final RECOGNIZE_EXPIRATION_DATE:Ljava/lang/String; = "RECOGNIZE_EXPIRATION_DATE"

.field public static final RESULT_CAMERA_ERROR:I = 0x2713

.field public static final RESULT_CARD_DETECTION_TIMEOUT:I = 0x2711

.field public static final RESULT_IMAGE_CAPTURE_MODE:I = 0x2712

.field public static final RESULT_OCR_ERROR:I = 0x2716

.field public static final RESULT_OCR_REQUEST_LIMIT_REACHED:I = 0x2718

.field public static final RESULT_OCR_TIMEOUT:I = 0x2714

.field public static final RESULT_OCR_UNRECOGNIZABLE:I = 0x2715

.field public static final RESULT_OCR_USER_SKIPPED:I = 0x2717

.field public static final RESULT_OUT_OF_MEMORY:I = 0x2719

.field public static final THRESHOLD_EXPIRATION_DATE_MIN_CONFIDENCE_4_DIGIT:Ljava/lang/String; = "THRESHOLD_EXPIRATION_DATE_MIN_CONFIDENCE_4_DIGIT"

.field public static final THRESHOLD_EXPIRATION_DATE_MIN_CONFIDENCE_6_DIGIT:Ljava/lang/String; = "THRESHOLD_EXPIRATION_DATE_MIN_CONFIDENCE_6_DIGIT"

.field public static final THRESHOLD_MEAN_DIGIT_CONFIDENCE:Ljava/lang/String; = "THRESHOLD_MEAN_DIGIT_CONFIDENCE"

.field public static final THRESHOLD_MIN_DIGIT_CONFIDENCE:Ljava/lang/String; = "THRESHOLD_MIN_DIGIT_CONFIDENCE"

.field public static final UI_OPTION:Ljava/lang/String; = "UI_OPTION"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    return-void
.end method
