.class public abstract Lcom/google/android/libraries/social/experiments/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/experiments/c;


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final b:Lcom/google/android/libraries/social/account/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/libraries/social/experiments/d;->a:Landroid/content/Context;

    .line 24
    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    iput-object v0, p0, Lcom/google/android/libraries/social/experiments/d;->b:Lcom/google/android/libraries/social/account/b;

    .line 25
    return-void
.end method


# virtual methods
.method public abstract a(Lcom/google/android/libraries/social/experiments/a;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public final a(Lcom/google/android/libraries/social/experiments/a;I)Z
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/libraries/social/experiments/d;->b:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, p2}, Lcom/google/android/libraries/social/account/b;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/experiments/d;->a:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/account/b;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, p2}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/social/experiments/d;->a(Lcom/google/android/libraries/social/experiments/a;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
