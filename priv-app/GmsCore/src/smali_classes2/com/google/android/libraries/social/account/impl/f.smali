.class final Lcom/google/android/libraries/social/account/impl/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/account/c;


# instance fields
.field final synthetic a:Lcom/google/android/libraries/social/account/impl/a;

.field private final b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/social/account/impl/a;I)V
    .locals 1

    .prologue
    .line 752
    invoke-static {p2}, Lcom/google/android/libraries/social/account/impl/a;->e(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/account/impl/f;-><init>(Lcom/google/android/libraries/social/account/impl/a;Ljava/lang/String;)V

    .line 753
    return-void
.end method

.method private constructor <init>(Lcom/google/android/libraries/social/account/impl/a;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 747
    iput-object p1, p0, Lcom/google/android/libraries/social/account/impl/f;->a:Lcom/google/android/libraries/social/account/impl/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 748
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/account/impl/f;->b:Ljava/lang/String;

    .line 749
    return-void
.end method

.method private e(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 817
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/f;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 803
    const-string v0, "account_status"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/account/impl/f;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/f;->a:Lcom/google/android/libraries/social/account/impl/a;

    invoke-static {v1}, Lcom/google/android/libraries/social/account/impl/a;->b(Lcom/google/android/libraries/social/account/impl/a;)Landroid/content/SharedPreferences;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 757
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/account/impl/f;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 758
    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/f;->a:Lcom/google/android/libraries/social/account/impl/a;

    invoke-static {v1}, Lcom/google/android/libraries/social/account/impl/a;->b(Lcom/google/android/libraries/social/account/impl/a;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 769
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/account/impl/f;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/f;->a:Lcom/google/android/libraries/social/account/impl/a;

    invoke-static {v1}, Lcom/google/android/libraries/social/account/impl/a;->b(Lcom/google/android/libraries/social/account/impl/a;)Landroid/content/SharedPreferences;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 808
    invoke-virtual {p0}, Lcom/google/android/libraries/social/account/impl/f;->a()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 780
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/account/impl/f;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/f;->a:Lcom/google/android/libraries/social/account/impl/a;

    invoke-static {v1}, Lcom/google/android/libraries/social/account/impl/a;->b(Lcom/google/android/libraries/social/account/impl/a;)Landroid/content/SharedPreferences;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final d(Ljava/lang/String;)Lcom/google/android/libraries/social/account/c;
    .locals 4

    .prologue
    .line 813
    new-instance v0, Lcom/google/android/libraries/social/account/impl/f;

    iget-object v1, p0, Lcom/google/android/libraries/social/account/impl/f;->a:Lcom/google/android/libraries/social/account/impl/a;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/libraries/social/account/impl/f;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/social/account/impl/f;-><init>(Lcom/google/android/libraries/social/account/impl/a;Ljava/lang/String;)V

    return-object v0
.end method
