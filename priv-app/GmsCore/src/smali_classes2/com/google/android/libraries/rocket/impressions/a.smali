.class public abstract Lcom/google/android/libraries/rocket/impressions/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/rocket/impressions/o;


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/rocket/impressions/a;->a:Z

    .line 13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/rocket/impressions/a;->a:Z

    .line 14
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/google/android/libraries/rocket/impressions/a;->a:Z

    if-eqz v0, :cond_0

    .line 22
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot invoke "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " on a closed transport"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 24
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method protected abstract a(Lcom/google/c/b/b/a/a/f;)V
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 28
    const-string v0, "close"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/rocket/impressions/a;->a(Ljava/lang/String;)V

    .line 29
    invoke-virtual {p0}, Lcom/google/android/libraries/rocket/impressions/a;->a()V

    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/rocket/impressions/a;->a:Z

    .line 31
    return-void
.end method

.method public final b(Lcom/google/c/b/b/a/a/f;)V
    .locals 1

    .prologue
    .line 41
    const-string v0, "log"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/rocket/impressions/a;->a(Ljava/lang/String;)V

    .line 42
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/rocket/impressions/a;->a(Lcom/google/c/b/b/a/a/f;)V

    .line 43
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/google/android/libraries/rocket/impressions/a;->a:Z

    return v0
.end method
