.class public Lcom/google/android/libraries/social/rpc/a/f;
.super Lcom/google/android/libraries/social/rpc/a/e;
.source "SourceFile"


# instance fields
.field protected final h:Lcom/google/protobuf/nano/j;

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/social/rpc/q;Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 43
    const-string v3, "POST"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/libraries/social/rpc/a/e;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/rpc/q;Ljava/lang/String;Ljava/lang/String;Lcom/google/protobuf/nano/j;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    iput-object p4, p0, Lcom/google/android/libraries/social/rpc/a/f;->h:Lcom/google/protobuf/nano/j;

    .line 52
    return-void
.end method

.method private i()V
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/google/android/libraries/social/rpc/a/f;->i:Z

    if-eqz v0, :cond_0

    .line 72
    :goto_0
    return-void

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/a/f;->h:Lcom/google/protobuf/nano/j;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/rpc/a/f;->c(Lcom/google/protobuf/nano/j;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/a/f;->h:Lcom/google/protobuf/nano/j;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/rpc/a/f;->a(Lcom/google/protobuf/nano/j;)V

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/rpc/a/f;->i:Z

    goto :goto_0
.end method


# virtual methods
.method protected a(Lcom/google/protobuf/nano/j;)V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method protected c(Lcom/google/protobuf/nano/j;)V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/google/android/libraries/social/rpc/a/f;->i()V

    .line 77
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/a/f;->h:Lcom/google/protobuf/nano/j;

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/google/android/libraries/social/rpc/a/f;->i()V

    .line 83
    iget-object v0, p0, Lcom/google/android/libraries/social/rpc/a/f;->h:Lcom/google/protobuf/nano/j;

    invoke-static {v0}, Lcom/google/protobuf/nano/k;->a(Lcom/google/protobuf/nano/j;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
