.class public final Lcom/google/android/libraries/social/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/libraries/social/f/a;

.field private static final b:Ljava/lang/Object;


# instance fields
.field private c:Landroid/content/Context;

.field private d:Lcom/google/android/libraries/social/a/a;

.field private e:Ljava/lang/String;

.field private final f:Ljava/util/Map;

.field private final g:Ljava/util/Map;

.field private final h:Ljava/util/HashSet;

.field private final i:Ljava/util/ArrayList;

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    new-instance v0, Lcom/google/android/libraries/social/f/a;

    const-string v1, "debug.social.app.check_duplicates"

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/f/a;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/social/a/a;->a:Lcom/google/android/libraries/social/f/a;

    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/libraries/social/a/a;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/a/a;->f:Ljava/util/Map;

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/a/a;->g:Ljava/util/Map;

    .line 43
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/a/a;->h:Ljava/util/HashSet;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/a/a;->i:Ljava/util/ArrayList;

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/a/a;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/a/a;)V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/social/a/a;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/a/a;->f:Ljava/util/Map;

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/a/a;->g:Ljava/util/Map;

    .line 43
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/a/a;->h:Ljava/util/HashSet;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/a/a;->i:Ljava/util/ArrayList;

    .line 78
    iput-object p1, p0, Lcom/google/android/libraries/social/a/a;->c:Landroid/content/Context;

    .line 79
    iput-object p2, p0, Lcom/google/android/libraries/social/a/a;->d:Lcom/google/android/libraries/social/a/a;

    .line 80
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/a/a;->e:Ljava/lang/String;

    .line 81
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/support/v4/app/Fragment;)Lcom/google/android/libraries/social/a/a;
    .locals 1

    .prologue
    .line 511
    :goto_0
    if-eqz p1, :cond_1

    .line 512
    invoke-static {p1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    move-result-object v0

    .line 513
    if-eqz v0, :cond_0

    .line 518
    :goto_1
    return-object v0

    .line 516
    :cond_0
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object p1

    goto :goto_0

    .line 518
    :cond_1
    invoke-static {p0}, Lcom/google/android/libraries/social/a/a;->b(Landroid/content/Context;)Lcom/google/android/libraries/social/a/a;

    move-result-object v0

    goto :goto_1
.end method

.method private static a(Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;
    .locals 3

    .prologue
    .line 564
    instance-of v0, p0, Landroid/content/Context;

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 565
    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-ne p0, v0, :cond_1

    .line 566
    check-cast p0, Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/photos/b;->a(Landroid/content/Context;)Lcom/google/android/gms/photos/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/photos/b;->U_()Lcom/google/android/libraries/social/a/a;

    move-result-object v0

    .line 576
    :cond_0
    :goto_0
    return-object v0

    .line 569
    :cond_1
    instance-of v0, p0, Lcom/google/android/libraries/social/a/d;

    if-eqz v0, :cond_2

    move-object v0, p0

    .line 570
    check-cast v0, Lcom/google/android/libraries/social/a/d;

    invoke-interface {v0}, Lcom/google/android/libraries/social/a/d;->U_()Lcom/google/android/libraries/social/a/a;

    move-result-object v0

    .line 571
    if-nez v0, :cond_0

    .line 572
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BinderContext must not return null Binder: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 576
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 445
    invoke-static {p0}, Lcom/google/android/libraries/social/a/a;->b(Landroid/content/Context;)Lcom/google/android/libraries/social/a/a;

    move-result-object v0

    .line 446
    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 336
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/a/a;->b()V

    .line 338
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 339
    if-eqz v0, :cond_1

    .line 340
    sget-object v1, Lcom/google/android/libraries/social/a/a;->b:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    .line 341
    new-instance v0, Lcom/google/android/libraries/social/a/c;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bind call too late - someone already tried to get: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/a/c;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 336
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 344
    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/libraries/social/a/b;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Duplicate binding: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/a/b;-><init>(Ljava/lang/String;)V

    throw v0

    .line 347
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a;->f:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 348
    monitor-exit p0

    return-void
.end method

.method public static b(Landroid/content/Context;)Lcom/google/android/libraries/social/a/a;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 525
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    move v1, v2

    move-object v0, p0

    .line 529
    :cond_0
    invoke-static {v0}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    move-result-object v3

    .line 530
    if-eqz v3, :cond_1

    .line 531
    return-object v3

    .line 534
    :cond_1
    if-ne v0, v4, :cond_2

    const/4 v3, 0x1

    :goto_0
    or-int/2addr v1, v3

    .line 536
    instance-of v3, v0, Landroid/content/ContextWrapper;

    if-eqz v3, :cond_3

    .line 537
    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    .line 539
    if-nez v0, :cond_4

    .line 540
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid ContextWrapper -- If this is a Robolectric test, have you called ActivityController.create()?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v3, v2

    .line 534
    goto :goto_0

    .line 544
    :cond_3
    if-nez v1, :cond_5

    move-object v0, v4

    .line 549
    :cond_4
    :goto_1
    if-nez v0, :cond_0

    .line 551
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "didn\'t find a binder"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 547
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 454
    invoke-static {p0}, Lcom/google/android/libraries/social/a/a;->b(Landroid/content/Context;)Lcom/google/android/libraries/social/a/a;

    move-result-object v0

    .line 455
    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/a/a;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 330
    iget-boolean v0, p0, Lcom/google/android/libraries/social/a/a;->j:Z

    if-eqz v0, :cond_0

    .line 331
    new-instance v0, Lcom/google/android/libraries/social/a/c;

    const-string v1, "This binder is sealed for modification"

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/a/c;-><init>(Ljava/lang/String;)V

    throw v0

    .line 333
    :cond_0
    return-void
.end method

.method private declared-synchronized b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 353
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/a/a;->b()V

    .line 355
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 356
    if-nez v0, :cond_0

    .line 357
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 358
    iget-object v1, p0, Lcom/google/android/libraries/social/a/a;->g:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 361
    monitor-exit p0

    return-void

    .line 353
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;
    .locals 1

    .prologue
    .line 462
    invoke-static {p0}, Lcom/google/android/libraries/social/a/a;->b(Landroid/content/Context;)Lcom/google/android/libraries/social/a/a;

    move-result-object v0

    .line 463
    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/a/a;->c(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized d(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 366
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a;->c:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 367
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Binder not initialized yet."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 366
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 370
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 371
    if-eqz v0, :cond_3

    .line 372
    sget-object v1, Lcom/google/android/libraries/social/a/a;->b:Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eq v0, v1, :cond_2

    .line 398
    :cond_1
    :goto_0
    monitor-exit p0

    return-object v0

    .line 372
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 376
    :cond_3
    :try_start_2
    iget-boolean v2, p0, Lcom/google/android/libraries/social/a/a;->j:Z

    .line 377
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/a/a;->j:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 379
    :try_start_3
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 380
    :goto_1
    if-ge v1, v3, :cond_5

    .line 381
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/a/f;

    iget-object v4, p0, Lcom/google/android/libraries/social/a/a;->c:Landroid/content/Context;

    invoke-interface {v0, v4, p1, p0}, Lcom/google/android/libraries/social/a/f;->a(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/libraries/social/a/a;)V

    .line 383
    sget-object v0, Lcom/google/android/libraries/social/a/a;->a:Lcom/google/android/libraries/social/f/a;

    .line 384
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .line 385
    if-eqz v0, :cond_4

    .line 391
    :try_start_4
    iput-boolean v2, p0, Lcom/google/android/libraries/social/a/a;->j:Z

    goto :goto_0

    .line 380
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 391
    :cond_5
    iput-boolean v2, p0, Lcom/google/android/libraries/social/a/a;->j:Z

    .line 394
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 395
    if-nez v0, :cond_1

    .line 396
    iget-object v1, p0, Lcom/google/android/libraries/social/a/a;->f:Ljava/util/Map;

    sget-object v2, Lcom/google/android/libraries/social/a/a;->b:Ljava/lang/Object;

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 391
    :catchall_1
    move-exception v0

    iput-boolean v2, p0, Lcom/google/android/libraries/social/a/a;->j:Z

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method private declared-synchronized e(Ljava/lang/Class;)Ljava/util/List;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 404
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a;->c:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 405
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Binder not initialized yet."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 404
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 408
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 409
    if-nez v0, :cond_3

    .line 410
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 411
    iget-object v2, p0, Lcom/google/android/libraries/social/a/a;->g:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v0

    .line 415
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a;->h:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 416
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a;->h:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 417
    iget-boolean v3, p0, Lcom/google/android/libraries/social/a/a;->j:Z

    .line 418
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/a/a;->j:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 420
    :try_start_2
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 421
    :goto_1
    if-ge v1, v4, :cond_1

    .line 422
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/a/f;

    iget-object v5, p0, Lcom/google/android/libraries/social/a/a;->c:Landroid/content/Context;

    invoke-interface {v0, v5, p1, p0}, Lcom/google/android/libraries/social/a/f;->a(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/libraries/social/a/a;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 421
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 425
    :cond_1
    :try_start_3
    iput-boolean v3, p0, Lcom/google/android/libraries/social/a/a;->j:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 429
    :cond_2
    monitor-exit p0

    return-object v2

    .line 425
    :catchall_1
    move-exception v0

    :try_start_4
    iput-boolean v3, p0, Lcom/google/android/libraries/social/a/a;->j:Z

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_3
    move-object v2, v0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/libraries/social/a/f;)Lcom/google/android/libraries/social/a/a;
    .locals 1

    .prologue
    .line 180
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/a/a;->b()V

    .line 182
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    monitor-exit p0

    return-object p0

    .line 180
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 115
    return-object p0
.end method

.method public final a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 192
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/a/a;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 193
    if-eqz v0, :cond_0

    .line 194
    return-object v0

    .line 197
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unbound type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nSearched binders:\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    iget-object v1, p0, Lcom/google/android/libraries/social/a/a;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p0, Lcom/google/android/libraries/social/a/a;->d:Lcom/google/android/libraries/social/a/a;

    if-eqz p0, :cond_1

    const-string v1, " ->\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 198
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 199
    const-string v2, "Binder"

    invoke-static {v2, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 200
    throw v1
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 326
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/a/a;->j:Z

    .line 327
    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 96
    iput-object p1, p0, Lcom/google/android/libraries/social/a/a;->c:Landroid/content/Context;

    .line 98
    iget-object v0, p0, Lcom/google/android/libraries/social/a/a;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 99
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/a/a;->e:Ljava/lang/String;

    .line 101
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/libraries/social/a/a;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/google/android/libraries/social/a/a;->d:Lcom/google/android/libraries/social/a/a;

    .line 108
    return-void
.end method

.method public final a(Ljava/lang/Class;Ljava/util/Collection;)V
    .locals 2

    .prologue
    .line 138
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 139
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 140
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/libraries/social/a/a;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 142
    :cond_0
    return-void
.end method

.method public final varargs a(Ljava/lang/Class;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 129
    const/4 v0, 0x0

    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 130
    aget-object v1, p2, v0

    invoke-direct {p0, p1, v1}, Lcom/google/android/libraries/social/a/a;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 129
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 132
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 560
    iput-object p1, p0, Lcom/google/android/libraries/social/a/a;->e:Ljava/lang/String;

    .line 561
    return-void
.end method

.method public final b(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 224
    .line 226
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/a/a;->d(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 227
    if-eqz v0, :cond_1

    .line 232
    :goto_0
    return-object v0

    .line 230
    :cond_1
    iget-object p0, p0, Lcom/google/android/libraries/social/a/a;->d:Lcom/google/android/libraries/social/a/a;

    .line 231
    if-nez p0, :cond_0

    .line 232
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/social/a/a;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 123
    return-void
.end method

.method public final c(Ljava/lang/Class;)Ljava/util/List;
    .locals 2

    .prologue
    .line 241
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 244
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/a/a;->e(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v1

    .line 245
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 246
    iget-object p0, p0, Lcom/google/android/libraries/social/a/a;->d:Lcom/google/android/libraries/social/a/a;

    .line 247
    if-nez p0, :cond_0

    .line 248
    return-object v0
.end method
