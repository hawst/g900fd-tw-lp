.class public Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final ALL_CPU_ABIS:Ljava/util/HashSet;

.field public static final CPU_ARM64_V8A:Ljava/lang/String; = "arm64-v8a"

.field public static final CPU_ARMEABI:Ljava/lang/String; = "armeabi"

.field public static final CPU_ARMEABI_V7A:Ljava/lang/String; = "armeabi-v7a"

.field public static final CPU_MIPS:Ljava/lang/String; = "mips"

.field public static final CPU_X86:Ljava/lang/String; = "x86"

.field private static final DELIMETER:Ljava/lang/String; = ","

.field public static final EMPTY_BLACKLIST:Ljava/util/HashSet;

.field static final MIN_ORIENTATION_SDK:I = 0xd

.field static final MIN_SDK:I = 0x9

.field public static final NATIVE_CPU_ABIS:Ljava/util/HashSet;

.field private static final TAG:Ljava/lang/String; = "FeaturePrerequisiteCheckerUtil"


# instance fields
.field private final buildVersion:I

.field private final cameraInfoProvider:Lcom/google/android/libraries/commerce/ocr/util/Function;

.field private final model:Ljava/lang/String;

.field private final packageManager:Landroid/content/pm/PackageManager;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 33
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->EMPTY_BLACKLIST:Ljava/util/HashSet;

    .line 34
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "armeabi"

    aput-object v1, v0, v3

    const-string v1, "armeabi-v7a"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "arm64-v8a"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "x86"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "mips"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->ALL_CPU_ABIS:Ljava/util/HashSet;

    .line 36
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "armeabi-v7a"

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->NATIVE_CPU_ABIS:Ljava/util/HashSet;

    return-void
.end method

.method protected constructor <init>(ILjava/lang/String;Lcom/google/android/libraries/commerce/ocr/util/Function;Landroid/content/pm/PackageManager;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput p1, p0, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->buildVersion:I

    .line 57
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->model:Ljava/lang/String;

    .line 58
    iput-object p3, p0, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->cameraInfoProvider:Lcom/google/android/libraries/commerce/ocr/util/Function;

    .line 59
    iput-object p4, p0, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->packageManager:Landroid/content/pm/PackageManager;

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/pm/PackageManager;)V
    .locals 3

    .prologue
    .line 49
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->getCameraInfoFunction()Lcom/google/android/libraries/commerce/ocr/util/Function;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2, p1}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;-><init>(ILjava/lang/String;Lcom/google/android/libraries/commerce/ocr/util/Function;Landroid/content/pm/PackageManager;)V

    .line 50
    return-void
.end method

.method public static getCameraInfoFunction()Lcom/google/android/libraries/commerce/ocr/util/Function;
    .locals 1

    .prologue
    .line 148
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/pub/b;

    invoke-direct {v0}, Lcom/google/android/libraries/commerce/ocr/pub/b;-><init>()V

    return-object v0
.end method

.method public static varargs newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;
    .locals 2

    .prologue
    .line 169
    new-instance v0, Ljava/util/HashSet;

    array-length v1, p0

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 170
    invoke-static {v0, p0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 171
    return-object v0
.end method

.method public static split(Ljava/lang/String;)Ljava/util/HashSet;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 161
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 162
    array-length v1, v0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    aget-object v1, v0, v3

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 163
    new-array v0, v3, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    .line 165
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public buildVersionOK()Z
    .locals 3

    .prologue
    .line 63
    const-string v0, "FeaturePrerequisiteCheckerUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Build version: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->buildVersion:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->buildVersion:I

    const/16 v1, 0x9

    if-ge v0, v1, :cond_0

    .line 65
    const-string v0, "FeaturePrerequisiteCheckerUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Build version "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->buildVersion:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is less than 9"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    const/4 v0, 0x0

    .line 68
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cameraOK()Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 100
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v4

    move v3, v2

    .line 101
    :goto_0
    if-ge v3, v4, :cond_2

    .line 102
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->cameraInfoProvider:Lcom/google/android/libraries/commerce/ocr/util/Function;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/libraries/commerce/ocr/util/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$CameraInfo;

    .line 103
    const-string v5, "FeaturePrerequisiteCheckerUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "Camera #"

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " is a "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v1, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-nez v1, :cond_0

    const-string v1, "rear-"

    :goto_1
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "facing camera."

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-nez v0, :cond_1

    .line 107
    const/4 v0, 0x1

    .line 111
    :goto_2
    return v0

    .line 103
    :cond_0
    const-string v1, "forward-"

    goto :goto_1

    .line 101
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 110
    :cond_2
    const-string v0, "FeaturePrerequisiteCheckerUtil"

    const-string v1, "No rear-facing camera detected."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 111
    goto :goto_2
.end method

.method public cpuAbiOK(Ljava/util/HashSet;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 89
    const-string v2, "FeaturePrerequisiteCheckerUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "CPU_ABI: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    const-string v2, "FeaturePrerequisiteCheckerUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "CPU_ABI2: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    sget-object v2, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 92
    const-string v2, "FeaturePrerequisiteCheckerUtil"

    const-string v3, "CPU ABIs [%s, %s] are not supported."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    sget-object v5, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    aput-object v5, v4, v0

    sget-object v5, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public isConnectionOK(Ljava/util/HashSet;Landroid/net/ConnectivityManager;Landroid/telephony/TelephonyManager;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 120
    invoke-virtual {p2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 121
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v3

    if-nez v3, :cond_2

    .line 122
    :cond_0
    const-string v0, "FeaturePrerequisiteCheckerUtil"

    const-string v2, "No active network connection"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 137
    :cond_1
    :goto_0
    :sswitch_0
    return v0

    .line 126
    :cond_2
    const-string v3, "FeaturePrerequisiteCheckerUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Connection type = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    move v0, v1

    .line 137
    goto :goto_0

    .line 133
    :sswitch_1
    invoke-virtual {p3}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v2

    .line 134
    const-string v3, "FeaturePrerequisiteCheckerUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Mobile connection type = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    invoke-virtual {p3}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 127
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_0
        0x6 -> :sswitch_0
        0x9 -> :sswitch_0
    .end sparse-switch
.end method

.method public modelOK(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 72
    invoke-static {p1}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->split(Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->modelOK(Ljava/util/HashSet;)Z

    move-result v0

    return v0
.end method

.method public modelOK(Ljava/util/HashSet;)Z
    .locals 3

    .prologue
    .line 77
    const-string v0, "FeaturePrerequisiteCheckerUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Model: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->model:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    invoke-virtual {p1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 80
    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->model:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    const-string v0, "FeaturePrerequisiteCheckerUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Model "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->model:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is blacklisted."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    const/4 v0, 0x0

    .line 85
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public orientationOk()Z
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    .prologue
    .line 143
    iget v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->buildVersion:I

    const/16 v1, 0xd

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->packageManager:Landroid/content/pm/PackageManager;

    const-string v1, "android.hardware.screen.portrait"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
