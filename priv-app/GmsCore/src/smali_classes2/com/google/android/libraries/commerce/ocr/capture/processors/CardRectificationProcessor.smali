.class public Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/capture/processors/Processor;


# static fields
.field private static final RADON_STD_DEV_640:F = 0.63750005f

.field private static final RADON_STD_DEV_960:F = 0.527f

.field private static final TAG:Ljava/lang/String; = "CardRectificationProcessor"


# instance fields
.field private final cardRectifier:Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier;

.field private edgeChangeListener:Lcom/google/android/libraries/commerce/ocr/capture/EdgeChangeListener;

.field private final edgeFinderWidthToOuterBoundingBoxEdgeLengthRatio:F

.field private final imageUtil:Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;

.field private final intervalPolicy:Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;

.field private final isFullCardImageReturned:Z

.field private final roiProvider:Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier;Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;FZ)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->intervalPolicy:Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;

    .line 49
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->imageUtil:Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;

    .line 50
    iput-object p3, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->cardRectifier:Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier;

    .line 51
    iput-object p4, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->roiProvider:Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;

    .line 52
    iput p5, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->edgeFinderWidthToOuterBoundingBoxEdgeLengthRatio:F

    .line 54
    iput-boolean p6, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->isFullCardImageReturned:Z

    .line 55
    return-void
.end method

.method private getMinRadonStdDev(Landroid/graphics/Point;)F
    .locals 4

    .prologue
    const/16 v3, 0x3c0

    const v0, 0x3f233334    # 0.63750005f

    .line 125
    iget v1, p1, Landroid/graphics/Point;->x:I

    .line 127
    if-lt v1, v3, :cond_1

    .line 128
    const v0, 0x3f06e979    # 0.527f

    .line 136
    :cond_0
    :goto_0
    const-string v1, "CardRectificationProcessor"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Min radon std dev is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for resolution  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/graphics/Point;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    return v0

    .line 129
    :cond_1
    const/16 v2, 0x280

    if-lt v1, v2, :cond_0

    if-ge v1, v3, :cond_0

    .line 131
    const v2, 0x3e645a1c    # 0.22299999f

    add-int/lit16 v1, v1, -0x280

    int-to-float v1, v1

    mul-float/2addr v1, v2

    const/high16 v2, 0x43a00000    # 320.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public discard(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)V
    .locals 0

    .prologue
    .line 109
    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->recycle()V

    .line 110
    return-void
.end method

.method public bridge synthetic discard(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->discard(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)V

    return-void
.end method

.method public isProcessingNeeded(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)Z
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->intervalPolicy:Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/capture/processors/IntervalPolicy;->isOverInterval()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic isProcessingNeeded(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 22
    check-cast p1, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->isProcessingNeeded(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)Z

    move-result v0

    return v0
.end method

.method public process(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;
    .locals 3

    .prologue
    .line 68
    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;

    .line 70
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->rectify(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;

    move-result-object v0

    .line 71
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->edgeChangeListener:Lcom/google/android/libraries/commerce/ocr/capture/EdgeChangeListener;

    if-eqz v1, :cond_0

    .line 72
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->edgeChangeListener:Lcom/google/android/libraries/commerce/ocr/capture/EdgeChangeListener;

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;->getBoundaries()Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/libraries/commerce/ocr/capture/EdgeChangeListener;->onEdgeChange(Lcom/google/android/libraries/commerce/ocr/cv/Boundaries;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->discard(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->discard(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)V

    throw v0
.end method

.method public bridge synthetic process(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    check-cast p1, Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->process(Lcom/google/android/libraries/commerce/ocr/cv/SafePoolable;)Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;

    move-result-object v0

    return-object v0
.end method

.method rectify(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;)Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;
    .locals 7

    .prologue
    .line 83
    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getOrientation()I

    move-result v0

    rem-int/lit16 v0, v0, 0xb4

    const/16 v1, 0x5a

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 85
    :goto_0
    if-eqz v0, :cond_1

    .line 93
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->imageUtil:Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->roiProvider:Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;

    invoke-interface {v1}, Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;->getBoundingBoxRectRelativeToCameraPreview()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getResolution()Landroid/graphics/Point;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/commerce/ocr/capture/ImageUtil;->rotate90(Landroid/graphics/Rect;Landroid/graphics/Point;)Landroid/graphics/Rect;

    move-result-object v2

    .line 99
    :goto_1
    invoke-static {}, Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;->createStarted()Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;

    move-result-object v6

    .line 100
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->cardRectifier:Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier;

    iget v3, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->edgeFinderWidthToOuterBoundingBoxEdgeLengthRatio:F

    invoke-virtual {p1}, Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;->getResolution()Landroid/graphics/Point;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->getMinRadonStdDev(Landroid/graphics/Point;)F

    move-result v4

    iget-boolean v5, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->isFullCardImageReturned:Z

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier;->rectify(Lcom/google/android/libraries/commerce/ocr/cv/OcrImage;Landroid/graphics/Rect;FFZ)Lcom/google/android/libraries/commerce/ocr/cv/CardRectifier$Result;

    move-result-object v0

    .line 103
    const-string v1, "CardRectificationProcessor"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "rectify: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;->stop()Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;

    move-result-object v3

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/commerce/ocr/util/Stopwatch;->elapsed(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    return-object v0

    .line 83
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->roiProvider:Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/ui/OcrRegionOfInterestProvider;->getBoundingBoxRectRelativeToCameraPreview()Landroid/graphics/Rect;

    move-result-object v2

    goto :goto_1
.end method

.method public setEdgeChangeListener(Lcom/google/android/libraries/commerce/ocr/capture/EdgeChangeListener;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/capture/processors/CardRectificationProcessor;->edgeChangeListener:Lcom/google/android/libraries/commerce/ocr/capture/EdgeChangeListener;

    .line 59
    return-void
.end method

.method public shutdown()V
    .locals 0

    .prologue
    .line 115
    return-void
.end method
