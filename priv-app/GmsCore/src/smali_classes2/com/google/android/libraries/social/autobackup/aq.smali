.class public final Lcom/google/android/libraries/social/autobackup/aq;
.super Lcom/google/android/libraries/social/c/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/p/e;


# instance fields
.field private final g:Ljava/lang/String;

.field private final h:[Ljava/lang/String;

.field private final i:Landroid/support/v4/a/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 198
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/c/a;-><init>(Landroid/content/Context;)V

    .line 195
    new-instance v0, Landroid/support/v4/a/k;

    invoke-direct {v0, p0}, Landroid/support/v4/a/k;-><init>(Landroid/support/v4/a/j;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/aq;->i:Landroid/support/v4/a/k;

    .line 199
    iput-object v2, p0, Lcom/google/android/libraries/social/autobackup/aq;->g:Ljava/lang/String;

    .line 200
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    new-array v0, v4, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/aq;->h:[Ljava/lang/String;

    .line 205
    :goto_0
    return-void

    .line 203
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    aput-object v2, v0, v4

    iput-object v0, p0, Lcom/google/android/libraries/social/autobackup/aq;->h:[Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final synthetic am_()V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0}, Landroid/support/v4/a/j;->a()V

    return-void
.end method

.method protected final k()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 209
    iget-object v0, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 210
    sget-object v1, Lcom/google/android/libraries/social/autobackup/aj;->b:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/aq;->i:Landroid/support/v4/a/k;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 212
    iget-object v1, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/libraries/social/autobackup/aj;->a(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/aq;->i:Landroid/support/v4/a/k;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 214
    iget-object v1, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/libraries/social/autobackup/aj;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/social/autobackup/aq;->i:Landroid/support/v4/a/k;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 216
    iget-object v0, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/autobackup/o;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/o;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/o;->a()Lcom/google/android/libraries/social/p/c;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/libraries/social/p/c;->a(Lcom/google/android/libraries/social/p/e;)V

    .line 219
    const/4 v0, 0x1

    return v0
.end method

.method protected final l()Z
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/aq;->i:Landroid/support/v4/a/k;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 225
    iget-object v0, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/autobackup/o;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/o;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/o;->a()Lcom/google/android/libraries/social/p/c;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/libraries/social/p/c;->b(Lcom/google/android/libraries/social/p/e;)V

    .line 227
    const/4 v0, 0x1

    return v0
.end method

.method public final m()Lcom/google/android/libraries/social/autobackup/as;
    .locals 14

    .prologue
    .line 232
    new-instance v6, Lcom/google/android/libraries/social/autobackup/as;

    invoke-direct {v6}, Lcom/google/android/libraries/social/autobackup/as;-><init>()V

    .line 233
    iget-object v0, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/autobackup/o;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/o;

    .line 236
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/o;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 237
    const/4 v0, 0x0

    iput-boolean v0, v6, Lcom/google/android/libraries/social/autobackup/as;->e:Z

    move-object v0, v6

    .line 317
    :goto_0
    return-object v0

    .line 240
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, v6, Lcom/google/android/libraries/social/autobackup/as;->e:Z

    .line 244
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/o;->d()I

    move-result v0

    iput v0, v6, Lcom/google/android/libraries/social/autobackup/as;->d:I

    .line 245
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/aq;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 246
    iget-object v0, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/autobackup/ac;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/ac;

    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/aq;->g:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/autobackup/ac;->c(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v6, Lcom/google/android/libraries/social/autobackup/as;->f:Z

    .line 249
    :cond_1
    iget-object v0, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 250
    const/4 v7, 0x0

    .line 253
    :try_start_0
    iget-object v1, p0, Lcom/google/android/libraries/social/autobackup/aq;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 254
    sget-object v3, Lcom/google/android/libraries/social/autobackup/ar;->d:Ljava/lang/String;

    .line 258
    :goto_1
    iget-object v1, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/libraries/social/autobackup/aj;->a(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/libraries/social/autobackup/ar;->a:[Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/libraries/social/autobackup/aq;->h:[Ljava/lang/String;

    const-string v5, "upload_state DESC, _id DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 260
    if-nez v1, :cond_4

    .line 261
    if-eqz v1, :cond_2

    .line 313
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 256
    :cond_3
    :try_start_1
    sget-object v3, Lcom/google/android/libraries/social/autobackup/ar;->e:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 264
    :cond_4
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 265
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    .line 266
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v3, v6, Lcom/google/android/libraries/social/autobackup/as;->a:Ljava/util/Map;

    .line 267
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, v6, Lcom/google/android/libraries/social/autobackup/as;->b:Ljava/util/Map;

    .line 268
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v3, v6, Lcom/google/android/libraries/social/autobackup/as;->c:Ljava/util/Map;

    .line 269
    const/4 v2, 0x0

    iput-boolean v2, v6, Lcom/google/android/libraries/social/autobackup/as;->m:Z

    .line 270
    :cond_5
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 271
    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 272
    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 273
    const/4 v7, 0x4

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 274
    const/4 v8, 0x6

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    sub-long v8, v4, v8

    .line 275
    iget-object v10, v6, Lcom/google/android/libraries/social/autobackup/as;->c:Ljava/util/Map;

    const/4 v11, 0x6

    invoke-interface {v1, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-interface {v10, v3, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    invoke-static {v2, v7, v8, v9}, Lcom/google/android/libraries/social/autobackup/at;->a(IIJ)Lcom/google/android/libraries/social/autobackup/at;

    move-result-object v7

    .line 278
    const/16 v2, 0x8

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    .line 279
    :goto_3
    iget-object v8, v6, Lcom/google/android/libraries/social/autobackup/as;->a:Ljava/util/Map;

    invoke-interface {v8, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 280
    sget-object v8, Lcom/google/android/libraries/social/autobackup/at;->b:Lcom/google/android/libraries/social/autobackup/at;

    if-ne v7, v8, :cond_a

    .line 281
    const/4 v8, 0x2

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/google/android/libraries/social/autobackup/as;->n:Ljava/lang/String;

    .line 282
    if-eqz v2, :cond_9

    .line 283
    iget v2, v6, Lcom/google/android/libraries/social/autobackup/as;->l:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v6, Lcom/google/android/libraries/social/autobackup/as;->l:I

    .line 287
    :goto_4
    iget v2, v6, Lcom/google/android/libraries/social/autobackup/as;->g:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v6, Lcom/google/android/libraries/social/autobackup/as;->g:I

    .line 304
    :cond_6
    :goto_5
    iget-object v2, v6, Lcom/google/android/libraries/social/autobackup/as;->a:Ljava/util/Map;

    invoke-interface {v2, v3, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    sget-object v2, Lcom/google/android/libraries/social/autobackup/at;->c:Lcom/google/android/libraries/social/autobackup/at;

    if-ne v7, v2, :cond_5

    .line 306
    const/4 v2, 0x7

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    .line 307
    iget-object v7, v6, Lcom/google/android/libraries/social/autobackup/as;->b:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v7, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 312
    :catchall_0
    move-exception v0

    :goto_6
    if-eqz v1, :cond_7

    .line 313
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v0

    .line 278
    :cond_8
    const/4 v2, 0x0

    goto :goto_3

    .line 285
    :cond_9
    :try_start_3
    iget v2, v6, Lcom/google/android/libraries/social/autobackup/as;->k:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v6, Lcom/google/android/libraries/social/autobackup/as;->k:I

    goto :goto_4

    .line 288
    :cond_a
    sget-object v2, Lcom/google/android/libraries/social/autobackup/at;->c:Lcom/google/android/libraries/social/autobackup/at;

    if-ne v7, v2, :cond_b

    .line 289
    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v6, Lcom/google/android/libraries/social/autobackup/as;->n:Ljava/lang/String;

    .line 290
    iget v2, v6, Lcom/google/android/libraries/social/autobackup/as;->h:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v6, Lcom/google/android/libraries/social/autobackup/as;->h:I

    goto :goto_5

    .line 291
    :cond_b
    sget-object v2, Lcom/google/android/libraries/social/autobackup/at;->e:Lcom/google/android/libraries/social/autobackup/at;

    if-ne v7, v2, :cond_c

    .line 292
    iget v2, v6, Lcom/google/android/libraries/social/autobackup/as;->j:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v6, Lcom/google/android/libraries/social/autobackup/as;->j:I

    goto :goto_5

    .line 293
    :cond_c
    sget-object v2, Lcom/google/android/libraries/social/autobackup/at;->d:Lcom/google/android/libraries/social/autobackup/at;

    if-ne v7, v2, :cond_d

    .line 294
    iget v2, v6, Lcom/google/android/libraries/social/autobackup/as;->j:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v6, Lcom/google/android/libraries/social/autobackup/as;->j:I

    .line 295
    const/4 v2, 0x1

    iput-boolean v2, v6, Lcom/google/android/libraries/social/autobackup/as;->m:Z

    goto :goto_5

    .line 296
    :cond_d
    sget-object v2, Lcom/google/android/libraries/social/autobackup/at;->f:Lcom/google/android/libraries/social/autobackup/at;

    if-ne v7, v2, :cond_6

    .line 297
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/libraries/social/autobackup/util/MediaStoreUtils;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v2, v8, v10

    if-gtz v2, :cond_e

    .line 298
    iget-object v2, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/libraries/social/autobackup/aj;->a(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    const-string v8, "media_url = ?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v3, v9, v10

    invoke-virtual {v0, v2, v8, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_5

    .line 301
    :cond_e
    iget v2, v6, Lcom/google/android/libraries/social/autobackup/as;->i:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v6, Lcom/google/android/libraries/social/autobackup/as;->i:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_5

    .line 312
    :cond_f
    if-eqz v1, :cond_10

    .line 313
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_10
    move-object v0, v6

    .line 317
    goto/16 :goto_0

    .line 312
    :catchall_1
    move-exception v0

    move-object v1, v7

    goto :goto_6
.end method

.method public final synthetic n()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/aq;->m()Lcom/google/android/libraries/social/autobackup/as;

    move-result-object v0

    return-object v0
.end method
