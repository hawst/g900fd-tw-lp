.class public final Lcom/google/android/libraries/social/mediaupload/t;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static a:Lcom/google/android/libraries/social/mediaupload/v;

.field private static final b:J

.field private static final c:Ljava/lang/ThreadLocal;

.field private static d:Ljava/lang/Class;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/libraries/social/mediaupload/t;->a:Lcom/google/android/libraries/social/mediaupload/v;

    .line 47
    const-string v0, "picasasync.metrics.time"

    invoke-static {v0}, Lcom/google/android/libraries/social/mediaupload/y;->a(Ljava/lang/String;)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/libraries/social/mediaupload/t;->b:J

    .line 50
    new-instance v0, Lcom/google/android/libraries/social/mediaupload/u;

    invoke-direct {v0}, Lcom/google/android/libraries/social/mediaupload/u;-><init>()V

    sput-object v0, Lcom/google/android/libraries/social/mediaupload/t;->c:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public static a(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 154
    sget-object v0, Lcom/google/android/libraries/social/mediaupload/t;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 155
    invoke-static {p0}, Lcom/google/android/libraries/social/mediaupload/v;->a(Ljava/lang/String;)Lcom/google/android/libraries/social/mediaupload/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public static a()V
    .locals 6

    .prologue
    .line 299
    sget-object v0, Lcom/google/android/libraries/social/mediaupload/t;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 300
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 301
    if-lez v1, :cond_0

    .line 302
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/mediaupload/v;

    .line 303
    iget v1, v0, Lcom/google/android/libraries/social/mediaupload/v;->j:I

    int-to-long v2, v1

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    long-to-int v1, v2

    iput v1, v0, Lcom/google/android/libraries/social/mediaupload/v;->j:I

    .line 305
    :cond_0
    return-void
.end method

.method public static a(I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 164
    invoke-static {v0, p0, v0}, Lcom/google/android/libraries/social/mediaupload/t;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 165
    return-void
.end method

.method public static a(J)V
    .locals 4

    .prologue
    .line 290
    sget-object v0, Lcom/google/android/libraries/social/mediaupload/t;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 291
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 292
    if-lez v1, :cond_0

    .line 293
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/mediaupload/v;

    .line 294
    iget-wide v2, v0, Lcom/google/android/libraries/social/mediaupload/v;->i:J

    add-long/2addr v2, p0

    iput-wide v2, v0, Lcom/google/android/libraries/social/mediaupload/v;->i:J

    .line 296
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;)V
    .locals 12

    .prologue
    const/4 v10, 0x1

    const-wide/16 v8, 0x0

    .line 176
    sget-object v0, Lcom/google/android/libraries/social/mediaupload/t;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 177
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gt p1, v1, :cond_0

    if-gtz p1, :cond_1

    .line 178
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "size: %s, id: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v10

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 182
    :cond_1
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_4

    .line 183
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/social/mediaupload/v;

    .line 184
    const-string v2, "MetricsUtils"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 185
    const-string v2, "MetricsUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "WARNING: unclosed metrics: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 189
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/social/mediaupload/v;

    invoke-virtual {v2, v1}, Lcom/google/android/libraries/social/mediaupload/v;->a(Lcom/google/android/libraries/social/mediaupload/v;)V

    .line 191
    :cond_3
    invoke-virtual {v1}, Lcom/google/android/libraries/social/mediaupload/v;->a()V

    goto :goto_0

    .line 194
    :cond_4
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/social/mediaupload/v;

    .line 195
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/libraries/social/mediaupload/v;->h:J

    .line 197
    sget-wide v2, Lcom/google/android/libraries/social/mediaupload/t;->b:J

    cmp-long v2, v2, v8

    if-ltz v2, :cond_d

    iget-wide v2, v1, Lcom/google/android/libraries/social/mediaupload/v;->h:J

    iget-wide v4, v1, Lcom/google/android/libraries/social/mediaupload/v;->g:J

    sub-long/2addr v2, v4

    sget-wide v4, Lcom/google/android/libraries/social/mediaupload/t;->b:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_d

    .line 199
    const-string v2, "MetricsUtils"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 200
    const-string v2, "MetricsUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/google/android/libraries/social/mediaupload/v;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, v1, Lcom/google/android/libraries/social/mediaupload/v;->c:I

    if-eqz v4, :cond_5

    const-string v4, " query-result:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/google/android/libraries/social/mediaupload/v;->c:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_5
    iget v4, v1, Lcom/google/android/libraries/social/mediaupload/v;->d:I

    if-eqz v4, :cond_6

    const-string v4, " update:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/google/android/libraries/social/mediaupload/v;->d:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_6
    iget-wide v4, v1, Lcom/google/android/libraries/social/mediaupload/v;->e:J

    cmp-long v4, v4, v8

    if-eqz v4, :cond_7

    const-string v4, " in:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, v1, Lcom/google/android/libraries/social/mediaupload/v;->e:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    :cond_7
    iget-wide v4, v1, Lcom/google/android/libraries/social/mediaupload/v;->f:J

    cmp-long v4, v4, v8

    if-eqz v4, :cond_8

    const-string v4, " out:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, v1, Lcom/google/android/libraries/social/mediaupload/v;->f:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    :cond_8
    iget-wide v4, v1, Lcom/google/android/libraries/social/mediaupload/v;->i:J

    cmp-long v4, v4, v8

    if-lez v4, :cond_9

    const-string v4, " net-time:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, v1, Lcom/google/android/libraries/social/mediaupload/v;->i:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    :cond_9
    iget v4, v1, Lcom/google/android/libraries/social/mediaupload/v;->j:I

    if-le v4, v10, :cond_a

    const-string v4, " net-op:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/google/android/libraries/social/mediaupload/v;->j:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_a
    iget-wide v4, v1, Lcom/google/android/libraries/social/mediaupload/v;->h:J

    iget-wide v6, v1, Lcom/google/android/libraries/social/mediaupload/v;->g:J

    sub-long/2addr v4, v6

    cmp-long v6, v4, v8

    if-lez v6, :cond_b

    const-string v6, " time:"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    :cond_b
    if-eqz p2, :cond_c

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, " report:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_c
    const/16 v4, 0x5d

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    :cond_d
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_e

    .line 205
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/mediaupload/v;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/mediaupload/v;->a(Lcom/google/android/libraries/social/mediaupload/v;)V

    .line 208
    :cond_e
    if-eqz p0, :cond_f

    if-eqz p2, :cond_f

    iget v0, v1, Lcom/google/android/libraries/social/mediaupload/v;->j:I

    if-lez v0, :cond_f

    .line 209
    iget-wide v2, v1, Lcom/google/android/libraries/social/mediaupload/v;->h:J

    iget-wide v4, v1, Lcom/google/android/libraries/social/mediaupload/v;->g:J

    sub-long/2addr v2, v4

    iget-wide v4, v1, Lcom/google/android/libraries/social/mediaupload/v;->i:J

    iget v0, v1, Lcom/google/android/libraries/social/mediaupload/v;->j:I

    iget-wide v6, v1, Lcom/google/android/libraries/social/mediaupload/v;->f:J

    iget-wide v8, v1, Lcom/google/android/libraries/social/mediaupload/v;->e:J

    sget-object v10, Lcom/google/android/libraries/social/mediaupload/t;->d:Ljava/lang/Class;

    if-eqz v10, :cond_f

    new-instance v10, Landroid/content/Intent;

    sget-object v11, Lcom/google/android/libraries/social/mediaupload/t;->d:Ljava/lang/Class;

    invoke-direct {v10, p0, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v11, "com.google.android.apps.plus.iu.op_report"

    invoke-virtual {v10, v11}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v11, "op_name"

    invoke-virtual {v10, v11, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v11, "total_time"

    invoke-virtual {v10, v11, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v2, "net_duration"

    invoke-virtual {v10, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v2, "transaction_count"

    invoke-virtual {v10, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "sent_bytes"

    invoke-virtual {v10, v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v0, "received_bytes"

    invoke-virtual {v10, v0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-virtual {p0, v10}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 216
    :cond_f
    invoke-virtual {v1}, Lcom/google/android/libraries/social/mediaupload/v;->a()V

    .line 217
    return-void
.end method

.method public static b(I)V
    .locals 2

    .prologue
    .line 263
    sget-object v0, Lcom/google/android/libraries/social/mediaupload/t;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 264
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 265
    if-lez v1, :cond_0

    .line 266
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/mediaupload/v;

    .line 267
    iget v1, v0, Lcom/google/android/libraries/social/mediaupload/v;->c:I

    add-int/2addr v1, p0

    iput v1, v0, Lcom/google/android/libraries/social/mediaupload/v;->c:I

    .line 269
    :cond_0
    return-void
.end method
