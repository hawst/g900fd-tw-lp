.class public final Lcom/google/android/libraries/rocket/impressions/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/libraries/rocket/impressions/f;

.field public final b:Lcom/google/c/b/b/a/a/e;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-static {}, Lcom/google/android/libraries/rocket/impressions/n;->c()Lcom/google/android/libraries/rocket/impressions/f;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/rocket/impressions/g;-><init>(Lcom/google/android/libraries/rocket/impressions/f;)V

    .line 35
    return-void
.end method

.method private constructor <init>(Lcom/google/android/libraries/rocket/impressions/f;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Lcom/google/c/b/b/a/a/e;

    invoke-direct {v0}, Lcom/google/c/b/b/a/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    .line 39
    iput-object p1, p0, Lcom/google/android/libraries/rocket/impressions/g;->a:Lcom/google/android/libraries/rocket/impressions/f;

    .line 40
    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/rocket/impressions/g;)V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p1, Lcom/google/android/libraries/rocket/impressions/g;->a:Lcom/google/android/libraries/rocket/impressions/f;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/rocket/impressions/g;-><init>(Lcom/google/android/libraries/rocket/impressions/f;)V

    .line 51
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v1, p1, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    invoke-static {v1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    return-void

    .line 52
    :catch_0
    move-exception v0

    .line 53
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static a(Lcom/google/android/libraries/rocket/impressions/f;ILjava/lang/Long;Ljava/lang/Long;)Lcom/google/android/libraries/rocket/impressions/g;
    .locals 4

    .prologue
    const/16 v1, 0x2cc

    .line 260
    new-instance v0, Lcom/google/android/libraries/rocket/impressions/g;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/rocket/impressions/g;-><init>(Lcom/google/android/libraries/rocket/impressions/f;)V

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/rocket/impressions/g;->a(I)Lcom/google/android/libraries/rocket/impressions/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/rocket/impressions/g;->a()Lcom/google/android/libraries/rocket/impressions/g;

    move-result-object v0

    .line 264
    if-eqz p2, :cond_2

    .line 265
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/rocket/impressions/g;->a(J)Lcom/google/android/libraries/rocket/impressions/g;

    .line 271
    :cond_0
    if-eqz p3, :cond_3

    .line 272
    new-instance v1, Lcom/google/c/b/b/a/a/b/al;

    invoke-direct {v1}, Lcom/google/c/b/b/a/a/b/al;-><init>()V

    .line 273
    new-instance v2, Lcom/google/c/b/b/a/a/b/am;

    invoke-direct {v2}, Lcom/google/c/b/b/a/a/b/am;-><init>()V

    iput-object v2, v1, Lcom/google/c/b/b/a/a/b/al;->c:Lcom/google/c/b/b/a/a/b/am;

    .line 274
    iget-object v2, v1, Lcom/google/c/b/b/a/a/b/al;->c:Lcom/google/c/b/b/a/a/b/am;

    iput-object p3, v2, Lcom/google/c/b/b/a/a/b/am;->a:Ljava/lang/Long;

    .line 275
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/rocket/impressions/g;->a(Lcom/google/c/b/b/a/a/b/al;)Lcom/google/android/libraries/rocket/impressions/g;

    .line 281
    :cond_1
    return-object v0

    .line 266
    :cond_2
    if-eq p1, v1, :cond_0

    .line 267
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot have a null lastHeartbeatSeqNum for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 276
    :cond_3
    if-eq p1, v1, :cond_1

    .line 277
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot have a null lastHeartbeatClientTimeUsec for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Lcom/google/android/libraries/rocket/impressions/f;JJ)Lcom/google/android/libraries/rocket/impressions/g;
    .locals 3

    .prologue
    .line 251
    const/16 v0, 0x481

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/libraries/rocket/impressions/g;->a(Lcom/google/android/libraries/rocket/impressions/f;ILjava/lang/Long;Ljava/lang/Long;)Lcom/google/android/libraries/rocket/impressions/g;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/rocket/impressions/g;
    .locals 4

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/g;->a:Lcom/google/android/libraries/rocket/impressions/f;

    invoke-interface {v0}, Lcom/google/android/libraries/rocket/impressions/f;->b()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    new-instance v3, Lcom/google/c/b/b/a/a/a;

    invoke-direct {v3}, Lcom/google/c/b/b/a/a/a;-><init>()V

    iput-object v3, v2, Lcom/google/c/b/b/a/a/e;->h:Lcom/google/c/b/b/a/a/a;

    iget-object v2, p0, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v2, v2, Lcom/google/c/b/b/a/a/e;->h:Lcom/google/c/b/b/a/a/a;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/c/b/b/a/a/a;->c:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v2, v2, Lcom/google/c/b/b/a/a/e;->h:Lcom/google/c/b/b/a/a/a;

    new-instance v3, Lcom/google/c/b/b/a/a/c;

    invoke-direct {v3}, Lcom/google/c/b/b/a/a/c;-><init>()V

    iput-object v3, v2, Lcom/google/c/b/b/a/a/a;->a:Lcom/google/c/b/b/a/a/c;

    iget-object v2, p0, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iget-object v2, v2, Lcom/google/c/b/b/a/a/e;->h:Lcom/google/c/b/b/a/a/a;

    iget-object v2, v2, Lcom/google/c/b/b/a/a/a;->a:Lcom/google/c/b/b/a/a/c;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v2, Lcom/google/c/b/b/a/a/c;->a:Ljava/lang/Long;

    return-object p0
.end method

.method public final a(I)Lcom/google/android/libraries/rocket/impressions/g;
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/e;->b:Ljava/lang/Integer;

    .line 141
    return-object p0
.end method

.method public final a(J)Lcom/google/android/libraries/rocket/impressions/g;
    .locals 3

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/e;->f:Ljava/lang/Long;

    .line 227
    return-object p0
.end method

.method public final a(Lcom/google/c/b/b/a/a/b/al;)Lcom/google/android/libraries/rocket/impressions/g;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    iput-object p1, v0, Lcom/google/c/b/b/a/a/e;->e:Lcom/google/c/b/b/a/a/b/al;

    .line 69
    return-object p0
.end method

.method public final b(J)Lcom/google/android/libraries/rocket/impressions/g;
    .locals 3

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/libraries/rocket/impressions/g;->b:Lcom/google/c/b/b/a/a/e;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/c/b/b/a/a/e;->c:Ljava/lang/Long;

    .line 232
    return-object p0
.end method
