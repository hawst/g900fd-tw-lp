.class public Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final sizeSelectionStrategy:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;

.field final synthetic this$0:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;)V
    .locals 0

    .prologue
    .line 452
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->this$0:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 453
    iput-object p2, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->sizeSelectionStrategy:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;

    .line 454
    return-void
.end method

.method private getContainerSize()Landroid/graphics/Point;
    .locals 3

    .prologue
    .line 513
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->this$0:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->this$0:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;

    iget-object v1, v1, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->containerSize:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->this$0:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;

    iget-object v2, v2, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->containerSize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    # invokes: Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->adjustForScreenOrientation(II)Landroid/graphics/Point;
    invoke-static {v0, v1, v2}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->access$300(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;II)Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method private getMaxDpiFilter(Landroid/graphics/Point;)Lcom/google/android/gms/common/util/ap;
    .locals 2

    .prologue
    .line 500
    iget v0, p1, Landroid/graphics/Point;->x:I

    iget v1, p1, Landroid/graphics/Point;->y:I

    mul-int/2addr v0, v1

    .line 501
    new-instance v1, Lcom/google/android/libraries/commerce/ocr/capture/e;

    invoke-direct {v1, p0, v0}, Lcom/google/android/libraries/commerce/ocr/capture/e;-><init>(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;I)V

    return-object v1
.end method

.method private getScreenFillingPreviewSizeForLandscape(Landroid/graphics/Point;Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 4

    .prologue
    .line 532
    iget v0, p1, Landroid/graphics/Point;->x:I

    .line 533
    iget v1, p1, Landroid/graphics/Point;->y:I

    .line 535
    iget-object v2, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->this$0:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;

    # getter for: Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->shapeModifier:Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;
    invoke-static {v2}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->access$400(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;)Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;->toRect(Landroid/graphics/Point;)Landroid/graphics/Rect;

    move-result-object v2

    .line 536
    iget-object v3, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->this$0:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;

    # getter for: Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->shapeModifier:Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;
    invoke-static {v3}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->access$400(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;)Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;->getShapeModifier(Landroid/graphics/Rect;)Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;

    move-result-object v3

    int-to-float v0, v0

    invoke-virtual {v3, v0}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->scaleToWidth(F)Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->getAsRect()Landroid/graphics/Rect;

    move-result-object v0

    .line 539
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v3

    if-le v3, v1, :cond_0

    .line 540
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->this$0:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;

    # getter for: Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->shapeModifier:Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->access$400(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;)Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier;->getShapeModifier(Landroid/graphics/Rect;)Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;

    move-result-object v0

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->scaleToHeight(F)Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/util/ShapeModifier$ModifiableRectF;->getAsRect()Landroid/graphics/Rect;

    move-result-object v0

    .line 544
    :cond_0
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-direct {v1, v2, v0}, Landroid/graphics/Point;-><init>(II)V

    return-object v1
.end method


# virtual methods
.method getBestFitSize(Landroid/graphics/Point;Landroid/graphics/Point;Ljava/util/List;)Landroid/hardware/Camera$Size;
    .locals 10

    .prologue
    .line 475
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->sizeSelectionStrategy:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;

    invoke-direct {p0, p2}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->getMaxDpiFilter(Landroid/graphics/Point;)Lcom/google/android/gms/common/util/ap;

    move-result-object v1

    invoke-virtual {v0, p3, v1, p1, p2}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelectionStrategy;->getClosest(Ljava/util/List;Lcom/google/android/gms/common/util/ap;Landroid/graphics/Point;Landroid/graphics/Point;)Landroid/hardware/Camera$Size;

    move-result-object v2

    .line 479
    if-nez v2, :cond_0

    .line 480
    const-string v0, "CameraManagerImpl"

    const-string v1, "Cannot find supported aspect ratio size, match height only"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    iget v4, p1, Landroid/graphics/Point;->x:I

    .line 482
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 483
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-wide v8, v0

    move-object v1, v2

    move-wide v2, v8

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 484
    iget v6, v0, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v6, v4

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    int-to-double v6, v6

    cmpg-double v6, v6, v2

    if-gez v6, :cond_2

    .line 486
    iget v1, v0, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-double v2, v1

    move-wide v8, v2

    move-object v2, v0

    move-wide v0, v8

    :goto_1
    move-wide v8, v0

    move-object v1, v2

    move-wide v2, v8

    .line 488
    goto :goto_0

    :cond_0
    move-object v1, v2

    .line 490
    :cond_1
    return-object v1

    :cond_2
    move-wide v8, v2

    move-object v2, v1

    move-wide v0, v8

    goto :goto_1
.end method

.method public getPictureSize(Landroid/hardware/Camera$Parameters;)Landroid/graphics/Point;
    .locals 18

    .prologue
    .line 554
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->this$0:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;

    # getter for: Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->pictureSize:Landroid/graphics/Point;
    invoke-static {v2}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->access$500(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;)Landroid/graphics/Point;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 555
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->this$0:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;

    # getter for: Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->pictureSize:Landroid/graphics/Point;
    invoke-static {v2}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->access$500(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;)Landroid/graphics/Point;

    move-result-object v6

    .line 586
    :goto_0
    return-object v6

    .line 559
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v2

    .line 560
    new-instance v6, Landroid/graphics/Point;

    iget v3, v2, Landroid/hardware/Camera$Size;->width:I

    iget v2, v2, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {v6, v3, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 562
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->this$0:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;

    # getter for: Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->targetPictureSize:Landroid/graphics/Point;
    invoke-static {v2}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->access$600(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;)Landroid/graphics/Point;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 563
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->getPreviewSize(Landroid/hardware/Camera$Parameters;)Landroid/graphics/Point;

    move-result-object v2

    .line 564
    iget v3, v2, Landroid/graphics/Point;->x:I

    int-to-double v4, v3

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-double v2, v2

    div-double v10, v4, v2

    .line 565
    const-wide v6, 0x7fffffffffffffffL

    .line 566
    const/4 v2, 0x0

    .line 567
    const-wide v4, 0x7fffffffffffffffL

    .line 568
    const/4 v3, 0x0

    .line 569
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    move-wide v8, v6

    move-object v6, v2

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/Camera$Size;

    .line 570
    iget v7, v2, Landroid/hardware/Camera$Size;->width:I

    int-to-double v14, v7

    iget v7, v2, Landroid/hardware/Camera$Size;->height:I

    int-to-double v0, v7

    move-wide/from16 v16, v0

    div-double v14, v14, v16

    .line 571
    iget v7, v2, Landroid/hardware/Camera$Size;->width:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->this$0:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;

    # getter for: Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->targetPictureSize:Landroid/graphics/Point;
    invoke-static {v13}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->access$600(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;)Landroid/graphics/Point;

    move-result-object v13

    iget v13, v13, Landroid/graphics/Point;->x:I

    sub-int/2addr v7, v13

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v7

    .line 572
    iget v13, v2, Landroid/hardware/Camera$Size;->height:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->this$0:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;

    move-object/from16 v16, v0

    # getter for: Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->targetPictureSize:Landroid/graphics/Point;
    invoke-static/range {v16 .. v16}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->access$600(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;)Landroid/graphics/Point;

    move-result-object v16

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v16, v0

    sub-int v13, v13, v16

    invoke-static {v13}, Ljava/lang/Math;->abs(I)I

    move-result v13

    .line 573
    mul-int/2addr v7, v7

    mul-int/2addr v13, v13

    add-int/2addr v7, v13

    .line 574
    cmpl-double v13, v14, v10

    if-nez v13, :cond_1

    int-to-long v0, v7

    move-wide/from16 v16, v0

    cmp-long v13, v16, v8

    if-gez v13, :cond_1

    .line 575
    int-to-long v8, v7

    .line 576
    new-instance v6, Landroid/graphics/Point;

    iget v7, v2, Landroid/hardware/Camera$Size;->width:I

    iget v2, v2, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {v6, v7, v2}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_1

    .line 577
    :cond_1
    cmpl-double v13, v14, v10

    if-eqz v13, :cond_5

    int-to-long v14, v7

    cmp-long v13, v14, v4

    if-gez v13, :cond_5

    .line 578
    int-to-long v4, v7

    .line 579
    new-instance v3, Landroid/graphics/Point;

    iget v7, v2, Landroid/hardware/Camera$Size;->width:I

    iget v2, v2, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {v3, v7, v2}, Landroid/graphics/Point;-><init>(II)V

    move-object v2, v3

    :goto_2
    move-object v3, v2

    .line 581
    goto :goto_1

    .line 582
    :cond_2
    if-eqz v6, :cond_4

    .line 585
    :cond_3
    :goto_3
    const-string v2, "CameraManagerImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Picture size: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    move-object v6, v3

    .line 582
    goto :goto_3

    :cond_5
    move-object v2, v3

    goto :goto_2
.end method

.method public getPreviewSize(Landroid/hardware/Camera$Parameters;)Landroid/graphics/Point;
    .locals 4

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->this$0:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;

    # getter for: Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->previewSize:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->access$100(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;)Landroid/graphics/Point;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 461
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->this$0:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;

    # getter for: Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->previewSize:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->access$100(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;)Landroid/graphics/Point;

    move-result-object v0

    .line 470
    :goto_0
    return-object v0

    .line 464
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->getContainerSize()Landroid/graphics/Point;

    move-result-object v0

    .line 465
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->this$0:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;

    # getter for: Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->targetPreviewSize:Landroid/graphics/Point;
    invoke-static {v1}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->access$200(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;)Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->getBestFitSize(Landroid/graphics/Point;Landroid/graphics/Point;Ljava/util/List;)Landroid/hardware/Camera$Size;

    move-result-object v0

    .line 468
    const-string v1, "CameraManagerImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Setting preview size = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v0, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->this$0:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;

    new-instance v2, Landroid/graphics/Point;

    iget v3, v0, Landroid/hardware/Camera$Size;->width:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {v2, v3, v0}, Landroid/graphics/Point;-><init>(II)V

    # setter for: Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->previewSize:Landroid/graphics/Point;
    invoke-static {v1, v2}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->access$102(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 470
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->this$0:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;

    # getter for: Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->previewSize:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->access$100(Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;)Landroid/graphics/Point;

    move-result-object v0

    goto :goto_0
.end method

.method getScreenFillingPreviewSize(Landroid/graphics/Point;Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 2

    .prologue
    .line 518
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->this$0:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;

    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->this$0:Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;

    invoke-virtual {v1, p1}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->adjustForScreenOrientation(Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl$SizeSelector;->getScreenFillingPreviewSizeForLandscape(Landroid/graphics/Point;Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManagerImpl;->adjustForScreenOrientation(Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method
