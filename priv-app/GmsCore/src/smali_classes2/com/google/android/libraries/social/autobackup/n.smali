.class final Lcom/google/android/libraries/social/autobackup/n;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:I

.field public final b:Landroid/content/SyncResult;

.field private c:Lcom/google/android/libraries/social/autobackup/am;

.field private d:Z


# direct methods
.method constructor <init>(ILandroid/content/SyncResult;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput p1, p0, Lcom/google/android/libraries/social/autobackup/n;->a:I

    .line 25
    iput-object p2, p0, Lcom/google/android/libraries/social/autobackup/n;->b:Landroid/content/SyncResult;

    .line 26
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 30
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/n;->d:Z

    .line 31
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/n;->c:Lcom/google/android/libraries/social/autobackup/am;

    .line 32
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/libraries/social/autobackup/n;->c:Lcom/google/android/libraries/social/autobackup/am;

    .line 33
    if-eqz v0, :cond_0

    .line 34
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/am;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_0
    monitor-exit p0

    return-void

    .line 30
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/libraries/social/autobackup/am;)Z
    .locals 1

    .prologue
    .line 45
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/n;->d:Z

    if-nez v0, :cond_0

    .line 46
    iput-object p1, p0, Lcom/google/android/libraries/social/autobackup/n;->c:Lcom/google/android/libraries/social/autobackup/am;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47
    const/4 v0, 0x1

    .line 49
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Z
    .locals 1

    .prologue
    .line 40
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/libraries/social/autobackup/n;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Lcom/google/android/libraries/social/autobackup/am;
    .locals 1

    .prologue
    .line 54
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/autobackup/n;->c:Lcom/google/android/libraries/social/autobackup/am;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
