.class public final Lcom/google/android/libraries/social/experiments/impl/e;
.super Lcom/google/android/libraries/social/experiments/d;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/experiments/b;


# instance fields
.field private c:Ljava/util/List;

.field private d:Ljava/util/HashSet;

.field private e:Z

.field private final f:Ljava/util/HashMap;

.field private final g:Ljava/util/ArrayList;

.field private final h:Lcom/google/android/libraries/social/experiments/impl/c;

.field private final i:Lcom/google/android/libraries/social/experiments/impl/f;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/social/experiments/impl/c;Lcom/google/android/libraries/social/experiments/impl/f;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/experiments/d;-><init>(Landroid/content/Context;)V

    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/experiments/impl/e;->f:Ljava/util/HashMap;

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/experiments/impl/e;->g:Ljava/util/ArrayList;

    .line 42
    iput-object p2, p0, Lcom/google/android/libraries/social/experiments/impl/e;->h:Lcom/google/android/libraries/social/experiments/impl/c;

    .line 43
    iput-object p3, p0, Lcom/google/android/libraries/social/experiments/impl/e;->i:Lcom/google/android/libraries/social/experiments/impl/f;

    .line 44
    iget-object v0, p0, Lcom/google/android/libraries/social/experiments/impl/e;->h:Lcom/google/android/libraries/social/experiments/impl/c;

    invoke-interface {v0, p0}, Lcom/google/android/libraries/social/experiments/impl/c;->a(Lcom/google/android/libraries/social/experiments/b;)V

    .line 45
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/libraries/social/experiments/a;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 49
    invoke-virtual {p1}, Lcom/google/android/libraries/social/experiments/a;->b()Ljava/lang/String;

    move-result-object v1

    .line 50
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/libraries/social/experiments/impl/e;->e:Z

    if-eqz v0, :cond_2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/experiments/impl/e;->d:Ljava/util/HashSet;

    invoke-virtual {p1}, Lcom/google/android/libraries/social/experiments/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    const-string v0, "ExperimentServiceImpl"

    const-string v2, "Experiment %s has not been registered"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/libraries/social/experiments/a;->c()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/social/experiments/impl/e;->f:Ljava/util/HashMap;

    monitor-enter v2

    .line 59
    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/social/experiments/impl/e;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 60
    if-nez v0, :cond_1

    .line 61
    iget-object v0, p0, Lcom/google/android/libraries/social/experiments/impl/e;->h:Lcom/google/android/libraries/social/experiments/impl/c;

    invoke-interface {v0, p2}, Lcom/google/android/libraries/social/experiments/impl/c;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 63
    if-eqz v0, :cond_1

    .line 64
    iget-object v3, p0, Lcom/google/android/libraries/social/experiments/impl/e;->f:Ljava/util/HashMap;

    invoke-virtual {v3, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 69
    if-eqz v0, :cond_6

    .line 70
    invoke-virtual {p1}, Lcom/google/android/libraries/social/experiments/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 71
    if-eqz v0, :cond_5

    .line 74
    :goto_1
    iget-object v1, p0, Lcom/google/android/libraries/social/experiments/impl/e;->i:Lcom/google/android/libraries/social/experiments/impl/f;

    invoke-interface {v1, p1, v0}, Lcom/google/android/libraries/social/experiments/impl/f;->a(Lcom/google/android/libraries/social/experiments/a;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 76
    return-object v0

    .line 50
    :cond_2
    monitor-exit p0

    iget-object v0, p0, Lcom/google/android/libraries/social/experiments/impl/e;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/libraries/social/experiments/a;

    invoke-static {v0, v2}, Lcom/google/android/libraries/social/a/a;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    monitor-enter p0

    :try_start_2
    iget-boolean v2, p0, Lcom/google/android/libraries/social/experiments/impl/e;->e:Z

    if-nez v2, :cond_4

    iput-object v0, p0, Lcom/google/android/libraries/social/experiments/impl/e;->c:Ljava/util/List;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/experiments/impl/e;->d:Ljava/util/HashSet;

    iget-object v0, p0, Lcom/google/android/libraries/social/experiments/impl/e;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v2, v3

    :goto_2
    if-ge v2, v4, :cond_3

    iget-object v5, p0, Lcom/google/android/libraries/social/experiments/impl/e;->d:Ljava/util/HashSet;

    iget-object v0, p0, Lcom/google/android/libraries/social/experiments/impl/e;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/experiments/a;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/experiments/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    const/4 v0, 0x1

    :try_start_3
    iput-boolean v0, p0, Lcom/google/android/libraries/social/experiments/impl/e;->e:Z

    :cond_4
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 67
    :catchall_2
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_5
    move-object v0, v1

    .line 71
    goto :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_1
.end method
