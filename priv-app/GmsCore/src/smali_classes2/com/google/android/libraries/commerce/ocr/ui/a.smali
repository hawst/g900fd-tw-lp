.class final Lcom/google/android/libraries/commerce/ocr/ui/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;


# instance fields
.field final synthetic a:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/ui/a;->a:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFinish()V
    .locals 3

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/a;->a:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;

    # getter for: Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->access$000(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;)Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;->getPreviewSize()Landroid/graphics/Point;

    move-result-object v0

    .line 67
    iget-object v1, p0, Lcom/google/android/libraries/commerce/ocr/ui/a;->a:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;

    # getter for: Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->view:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;
    invoke-static {v1}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->access$100(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;)Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;

    move-result-object v1

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-interface {v1, v2, v0}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewView;->setPreviewPadding(II)V

    .line 68
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/ui/a;->a:Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;

    # getter for: Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->cameraManager:Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;
    invoke-static {v0}, Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;->access$000(Lcom/google/android/libraries/commerce/ocr/ui/CameraPreviewPresenterImpl;)Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;

    move-result-object v0

    new-instance v1, Lcom/google/android/libraries/commerce/ocr/ui/b;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/commerce/ocr/ui/b;-><init>(Lcom/google/android/libraries/commerce/ocr/ui/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/libraries/commerce/ocr/capture/CameraManager;->startPreview(Lcom/google/android/libraries/commerce/ocr/capture/CameraManager$OnFinishCallback;)V

    .line 80
    return-void
.end method
