.class public final Lcom/google/android/libraries/social/g/a/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:[J

.field private static final b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0x100

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 44
    new-array v0, v8, [J

    sput-object v0, Lcom/google/android/libraries/social/g/a/f;->a:[J

    .line 46
    new-instance v0, Lcom/google/android/libraries/social/f/a;

    const-string v3, "gallery3d_debug_build"

    invoke-direct {v0, v3}, Lcom/google/android/libraries/social/f/a;-><init>(Ljava/lang/String;)V

    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v3, "eng"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v3, "userdebug"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    sput-boolean v0, Lcom/google/android/libraries/social/g/a/f;->b:Z

    move v3, v1

    .line 155
    :goto_1
    if-ge v3, v8, :cond_4

    .line 156
    int-to-long v4, v3

    move v0, v1

    move-wide v6, v4

    .line 157
    :goto_2
    const/16 v4, 0x8

    if-ge v0, v4, :cond_3

    .line 158
    long-to-int v4, v6

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_2

    const-wide v4, -0x6a536cd653b4364bL    # -2.848111467964452E-204

    .line 159
    :goto_3
    shr-long/2addr v6, v2

    xor-long/2addr v4, v6

    .line 157
    add-int/lit8 v0, v0, 0x1

    move-wide v6, v4

    goto :goto_2

    :cond_1
    move v0, v1

    .line 46
    goto :goto_0

    .line 158
    :cond_2
    const-wide/16 v4, 0x0

    goto :goto_3

    .line 161
    :cond_3
    sget-object v0, Lcom/google/android/libraries/social/g/a/f;->a:[J

    aput-wide v6, v0, v3

    .line 155
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 163
    :cond_4
    return-void
.end method

.method public static a(Ljava/lang/String;)I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 272
    if-nez p0, :cond_0

    .line 278
    :goto_0
    return v0

    .line 276
    :cond_0
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 278
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 335
    if-nez p0, :cond_1

    .line 336
    const/4 v0, 0x0

    .line 340
    :cond_0
    :goto_0
    return-object v0

    .line 338
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 339
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x20

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 340
    sget-boolean v2, Lcom/google/android/libraries/social/g/a/f;->b:Z

    if-nez v2, :cond_0

    const-string v0, "********************************"

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 226
    if-eqz p0, :cond_0

    .line 227
    :try_start_0
    invoke-interface {p0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 229
    :catch_0
    move-exception v0

    .line 230
    const-string v1, "Utils"

    const-string v2, "fail to close"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static a(Z)V
    .locals 1

    .prologue
    .line 54
    if-nez p0, :cond_0

    .line 55
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 57
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 72
    if-eq p0, p1, :cond_0

    if-eqz p0, :cond_1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
