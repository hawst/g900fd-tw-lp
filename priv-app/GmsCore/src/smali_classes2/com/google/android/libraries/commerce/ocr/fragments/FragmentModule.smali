.class public Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final fragment:Landroid/support/v4/app/Fragment;

.field private final screenManager:Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/Fragment;)V
    .locals 3

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;->fragment:Landroid/support/v4/app/Fragment;

    .line 31
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;

    invoke-direct {p0}, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;->provideConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;->provideDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;-><init>(Landroid/content/res/Configuration;Landroid/view/Display;)V

    iput-object v0, p0, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;->screenManager:Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;

    .line 32
    return-void
.end method

.method private provideConfiguration()Landroid/content/res/Configuration;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;->fragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    return-object v0
.end method

.method private provideDisplay()Landroid/view/Display;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;->fragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public provideBackgroundHandler()Landroid/os/Handler;
    .locals 3

    .prologue
    .line 75
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v1, Lcom/google/android/libraries/commerce/ocr/fragments/a;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/commerce/ocr/fragments/a;-><init>(Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;)V

    invoke-direct {v0, v1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 79
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/google/android/libraries/commerce/ocr/fragments/b;

    invoke-direct {v2, p0, v0}, Lcom/google/android/libraries/commerce/ocr/fragments/b;-><init>(Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;Ljava/util/concurrent/FutureTask;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 88
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Could not start background thread"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public provideContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;->fragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    return-object v0
.end method

.method public provideExecutorFactory()Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory;
    .locals 1

    .prologue
    .line 95
    new-instance v0, Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory;

    invoke-direct {v0}, Lcom/google/android/libraries/commerce/ocr/util/ExecutorServiceFactory;-><init>()V

    return-object v0
.end method

.method public provideFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;->fragment:Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method public provideHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 70
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    return-object v0
.end method

.method public providePackageManager()Landroid/content/pm/PackageManager;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;->provideContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    return-object v0
.end method

.method public provideResources()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;->fragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public provideScreenManager()Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;->screenManager:Lcom/google/android/libraries/commerce/ocr/util/ScreenManager;

    return-object v0
.end method

.method public provideVibrator()Landroid/os/Vibrator;
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/commerce/ocr/fragments/FragmentModule;->fragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    return-object v0
.end method
