.class public final Lcom/google/android/c/b/ag;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/android/c/b/ag;


# instance fields
.field public a:I

.field public b:[Lcom/google/android/c/b/af;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9171
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 9172
    iput v0, p0, Lcom/google/android/c/b/ag;->d:I

    iput v0, p0, Lcom/google/android/c/b/ag;->a:I

    iput v0, p0, Lcom/google/android/c/b/ag;->e:I

    invoke-static {}, Lcom/google/android/c/b/af;->a()[Lcom/google/android/c/b/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/ag;->b:[Lcom/google/android/c/b/af;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/ag;->cachedSize:I

    .line 9173
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/ag;
    .locals 2

    .prologue
    .line 9133
    sget-object v0, Lcom/google/android/c/b/ag;->c:[Lcom/google/android/c/b/ag;

    if-nez v0, :cond_1

    .line 9134
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 9136
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/ag;->c:[Lcom/google/android/c/b/ag;

    if-nez v0, :cond_0

    .line 9137
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/ag;

    sput-object v0, Lcom/google/android/c/b/ag;->c:[Lcom/google/android/c/b/ag;

    .line 9139
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9141
    :cond_1
    sget-object v0, Lcom/google/android/c/b/ag;->c:[Lcom/google/android/c/b/ag;

    return-object v0

    .line 9139
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final b()I
    .locals 1

    .prologue
    .line 9152
    iget v0, p0, Lcom/google/android/c/b/ag;->e:I

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 9160
    iget v0, p0, Lcom/google/android/c/b/ag;->d:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 9204
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 9205
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/ag;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9207
    iget v1, p0, Lcom/google/android/c/b/ag;->d:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 9208
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/ag;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9211
    :cond_0
    iget-object v1, p0, Lcom/google/android/c/b/ag;->b:[Lcom/google/android/c/b/af;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/c/b/ag;->b:[Lcom/google/android/c/b/af;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 9212
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/android/c/b/ag;->b:[Lcom/google/android/c/b/af;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 9213
    iget-object v2, p0, Lcom/google/android/c/b/ag;->b:[Lcom/google/android/c/b/af;

    aget-object v2, v2, v0

    .line 9214
    if-eqz v2, :cond_1

    .line 9215
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 9212
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 9220
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 9127
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/ag;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/ag;->e:I

    iget v0, p0, Lcom/google/android/c/b/ag;->d:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/ag;->d:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/c/b/ag;->b:[Lcom/google/android/c/b/af;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/c/b/af;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/c/b/ag;->b:[Lcom/google/android/c/b/af;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/c/b/af;

    invoke-direct {v3}, Lcom/google/android/c/b/af;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/c/b/ag;->b:[Lcom/google/android/c/b/af;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/c/b/af;

    invoke-direct {v3}, Lcom/google/android/c/b/af;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/c/b/ag;->b:[Lcom/google/android/c/b/af;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 9187
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/ag;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 9188
    iget v0, p0, Lcom/google/android/c/b/ag;->d:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 9189
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/ag;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 9191
    :cond_0
    iget-object v0, p0, Lcom/google/android/c/b/ag;->b:[Lcom/google/android/c/b/af;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/c/b/ag;->b:[Lcom/google/android/c/b/af;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 9192
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/c/b/ag;->b:[Lcom/google/android/c/b/af;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 9193
    iget-object v1, p0, Lcom/google/android/c/b/ag;->b:[Lcom/google/android/c/b/af;

    aget-object v1, v1, v0

    .line 9194
    if-eqz v1, :cond_1

    .line 9195
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9192
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 9199
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 9200
    return-void
.end method
