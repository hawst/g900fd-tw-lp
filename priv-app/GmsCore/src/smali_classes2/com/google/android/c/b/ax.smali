.class public final Lcom/google/android/c/b/ax;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/android/c/b/ax;


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field private d:I

.field private e:Ljava/lang/String;

.field private f:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15533
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 15534
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/c/b/ax;->d:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/ax;->a:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/ax;->b:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/ax;->e:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/c/b/ax;->f:[B

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/ax;->cachedSize:I

    .line 15535
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/ax;
    .locals 2

    .prologue
    .line 15470
    sget-object v0, Lcom/google/android/c/b/ax;->c:[Lcom/google/android/c/b/ax;

    if-nez v0, :cond_1

    .line 15471
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 15473
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/ax;->c:[Lcom/google/android/c/b/ax;

    if-nez v0, :cond_0

    .line 15474
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/ax;

    sput-object v0, Lcom/google/android/c/b/ax;->c:[Lcom/google/android/c/b/ax;

    .line 15476
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15478
    :cond_1
    sget-object v0, Lcom/google/android/c/b/ax;->c:[Lcom/google/android/c/b/ax;

    return-object v0

    .line 15476
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/c/b/ax;
    .locals 1

    .prologue
    .line 15495
    if-nez p1, :cond_0

    .line 15496
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 15498
    :cond_0
    iput-object p1, p0, Lcom/google/android/c/b/ax;->e:Ljava/lang/String;

    .line 15499
    iget v0, p0, Lcom/google/android/c/b/ax;->d:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/ax;->d:I

    .line 15500
    return-object p0
.end method

.method public final a([B)Lcom/google/android/c/b/ax;
    .locals 1

    .prologue
    .line 15517
    if-nez p1, :cond_0

    .line 15518
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 15520
    :cond_0
    iput-object p1, p0, Lcom/google/android/c/b/ax;->f:[B

    .line 15521
    iget v0, p0, Lcom/google/android/c/b/ax;->d:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/ax;->d:I

    .line 15522
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 15563
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 15564
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/c/b/ax;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15566
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/ax;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 15568
    iget v1, p0, Lcom/google/android/c/b/ax;->d:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 15569
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/c/b/ax;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15572
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/ax;->d:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 15573
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/c/b/ax;->f:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 15576
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 15455
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/ax;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/c/b/ax;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/ax;->e:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/ax;->d:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/ax;->d:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/ax;->f:[B

    iget v0, p0, Lcom/google/android/c/b/ax;->d:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/ax;->d:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 15550
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/c/b/ax;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 15551
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/ax;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 15552
    iget v0, p0, Lcom/google/android/c/b/ax;->d:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 15553
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/c/b/ax;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 15555
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/ax;->d:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 15556
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/c/b/ax;->f:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 15558
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 15559
    return-void
.end method
