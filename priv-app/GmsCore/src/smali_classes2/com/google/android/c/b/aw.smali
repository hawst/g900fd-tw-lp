.class public final Lcom/google/android/c/b/aw;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field private b:I

.field private c:I

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 16213
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 16214
    iput v1, p0, Lcom/google/android/c/b/aw;->b:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/aw;->a:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/c/b/aw;->c:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/c/b/aw;->d:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/aw;->cachedSize:I

    .line 16215
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 16178
    iget v0, p0, Lcom/google/android/c/b/aw;->c:I

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 16197
    iget-boolean v0, p0, Lcom/google/android/c/b/aw;->d:Z

    return v0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 16241
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 16242
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/c/b/aw;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16244
    iget v1, p0, Lcom/google/android/c/b/aw;->b:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 16245
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/aw;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 16248
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/aw;->b:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 16249
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/c/b/aw;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 16252
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 16153
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/aw;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/aw;->c:I

    iget v0, p0, Lcom/google/android/c/b/aw;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/aw;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/aw;->d:Z

    iget v0, p0, Lcom/google/android/c/b/aw;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/aw;->b:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 16229
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/c/b/aw;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 16230
    iget v0, p0, Lcom/google/android/c/b/aw;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 16231
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/aw;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 16233
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/aw;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 16234
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/c/b/aw;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 16236
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 16237
    return-void
.end method
