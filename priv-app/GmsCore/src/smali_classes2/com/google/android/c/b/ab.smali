.class public final Lcom/google/android/c/b/ab;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17732
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 17733
    iput v1, p0, Lcom/google/android/c/b/ab;->a:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/ab;->b:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/android/c/b/ab;->c:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/ab;->cachedSize:I

    .line 17734
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17694
    iget-object v0, p0, Lcom/google/android/c/b/ab;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 17716
    iget-boolean v0, p0, Lcom/google/android/c/b/ab;->c:Z

    return v0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 17758
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 17759
    iget v1, p0, Lcom/google/android/c/b/ab;->a:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 17760
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/c/b/ab;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17763
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/ab;->a:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 17764
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/c/b/ab;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 17767
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 17672
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/ab;->b:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/ab;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/ab;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/ab;->c:Z

    iget v0, p0, Lcom/google/android/c/b/ab;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/ab;->a:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 17747
    iget v0, p0, Lcom/google/android/c/b/ab;->a:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 17748
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/c/b/ab;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 17750
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/ab;->a:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 17751
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/c/b/ab;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 17753
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 17754
    return-void
.end method
