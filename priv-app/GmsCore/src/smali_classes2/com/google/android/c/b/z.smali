.class public final Lcom/google/android/c/b/z;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile a:[Lcom/google/android/c/b/z;


# instance fields
.field private b:I

.field private c:I

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6994
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 6995
    iput v0, p0, Lcom/google/android/c/b/z;->b:I

    iput v0, p0, Lcom/google/android/c/b/z;->c:I

    iput v0, p0, Lcom/google/android/c/b/z;->d:I

    iput-boolean v0, p0, Lcom/google/android/c/b/z;->e:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/z;->cachedSize:I

    .line 6996
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/z;
    .locals 2

    .prologue
    .line 6924
    sget-object v0, Lcom/google/android/c/b/z;->a:[Lcom/google/android/c/b/z;

    if-nez v0, :cond_1

    .line 6925
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 6927
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/z;->a:[Lcom/google/android/c/b/z;

    if-nez v0, :cond_0

    .line 6928
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/z;

    sput-object v0, Lcom/google/android/c/b/z;->a:[Lcom/google/android/c/b/z;

    .line 6930
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6932
    :cond_1
    sget-object v0, Lcom/google/android/c/b/z;->a:[Lcom/google/android/c/b/z;

    return-object v0

    .line 6930
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final b()I
    .locals 1

    .prologue
    .line 6940
    iget v0, p0, Lcom/google/android/c/b/z;->c:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 6959
    iget v0, p0, Lcom/google/android/c/b/z;->d:I

    return v0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 7024
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 7025
    iget v1, p0, Lcom/google/android/c/b/z;->b:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 7026
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/z;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7029
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/z;->b:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 7030
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/z;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7033
    :cond_1
    iget v1, p0, Lcom/google/android/c/b/z;->b:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 7034
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/c/b/z;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7037
    :cond_2
    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 6978
    iget-boolean v0, p0, Lcom/google/android/c/b/z;->e:Z

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 6986
    iget v0, p0, Lcom/google/android/c/b/z;->b:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 6918
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/z;->c:I

    iget v0, p0, Lcom/google/android/c/b/z;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/z;->b:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/z;->d:I

    iget v0, p0, Lcom/google/android/c/b/z;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/z;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/z;->e:Z

    iget v0, p0, Lcom/google/android/c/b/z;->b:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/c/b/z;->b:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 7010
    iget v0, p0, Lcom/google/android/c/b/z;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 7011
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/z;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7013
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/z;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 7014
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/z;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7016
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/z;->b:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 7017
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/c/b/z;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 7019
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 7020
    return-void
.end method
