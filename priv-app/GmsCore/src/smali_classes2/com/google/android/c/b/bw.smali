.class public final Lcom/google/android/c/b/bw;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile b:[Lcom/google/android/c/b/bw;


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7106
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 7107
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/c/b/bw;->a:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/bw;->cachedSize:I

    .line 7108
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/bw;
    .locals 2

    .prologue
    .line 7092
    sget-object v0, Lcom/google/android/c/b/bw;->b:[Lcom/google/android/c/b/bw;

    if-nez v0, :cond_1

    .line 7093
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 7095
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/bw;->b:[Lcom/google/android/c/b/bw;

    if-nez v0, :cond_0

    .line 7096
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/bw;

    sput-object v0, Lcom/google/android/c/b/bw;->b:[Lcom/google/android/c/b/bw;

    .line 7098
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7100
    :cond_1
    sget-object v0, Lcom/google/android/c/b/bw;->b:[Lcom/google/android/c/b/bw;

    return-object v0

    .line 7098
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 7125
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 7126
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/c/b/bw;->a:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7128
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 7086
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/bw;->a:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 7119
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/c/b/bw;->a:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 7120
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 7121
    return-void
.end method
