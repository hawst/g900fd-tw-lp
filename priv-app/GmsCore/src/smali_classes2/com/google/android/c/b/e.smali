.class public final Lcom/google/android/c/b/e;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9746
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 9747
    iput v0, p0, Lcom/google/android/c/b/e;->b:I

    iput v0, p0, Lcom/google/android/c/b/e;->a:I

    iput v0, p0, Lcom/google/android/c/b/e;->c:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/e;->cachedSize:I

    .line 9748
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 9730
    iget v0, p0, Lcom/google/android/c/b/e;->c:I

    return v0
.end method

.method public final a(I)Lcom/google/android/c/b/e;
    .locals 1

    .prologue
    .line 9733
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/e;->c:I

    .line 9734
    iget v0, p0, Lcom/google/android/c/b/e;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/e;->b:I

    .line 9735
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 9770
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 9771
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/e;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9773
    iget v1, p0, Lcom/google/android/c/b/e;->b:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 9774
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/e;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9777
    :cond_0
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 9705
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/e;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/e;->c:I

    iget v0, p0, Lcom/google/android/c/b/e;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/e;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 9761
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/e;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 9762
    iget v0, p0, Lcom/google/android/c/b/e;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 9763
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/e;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 9765
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 9766
    return-void
.end method
