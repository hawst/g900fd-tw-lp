.class public final Lcom/google/android/c/b/cy;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Ljava/lang/String;

.field private c:I

.field private d:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3847
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 3848
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/c/b/cy;->c:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/cy;->a:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/c/b/cy;->b:[Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/c/b/cy;->d:[B

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/cy;->cachedSize:I

    .line 3849
    return-void
.end method


# virtual methods
.method public final a()[B
    .locals 1

    .prologue
    .line 3828
    iget-object v0, p0, Lcom/google/android/c/b/cy;->d:[B

    return-object v0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 3880
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v1

    .line 3881
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/c/b/cy;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int v3, v1, v2

    .line 3883
    iget-object v1, p0, Lcom/google/android/c/b/cy;->b:[Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/c/b/cy;->b:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_3

    move v1, v0

    move v2, v0

    .line 3886
    :goto_0
    iget-object v4, p0, Lcom/google/android/c/b/cy;->b:[Ljava/lang/String;

    array-length v4, v4

    if-ge v0, v4, :cond_1

    .line 3887
    iget-object v4, p0, Lcom/google/android/c/b/cy;->b:[Ljava/lang/String;

    aget-object v4, v4, v0

    .line 3888
    if-eqz v4, :cond_0

    .line 3889
    add-int/lit8 v2, v2, 0x1

    .line 3890
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 3886
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3894
    :cond_1
    add-int v0, v3, v1

    .line 3895
    mul-int/lit8 v1, v2, 0x1

    add-int/2addr v0, v1

    .line 3897
    :goto_1
    iget v1, p0, Lcom/google/android/c/b/cy;->c:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    .line 3898
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/c/b/cy;->d:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 3901
    :cond_2
    return v0

    :cond_3
    move v0, v3

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3800
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/cy;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/c/b/cy;->b:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/c/b/cy;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/c/b/cy;->b:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/android/c/b/cy;->b:[Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/cy;->d:[B

    iget v0, p0, Lcom/google/android/c/b/cy;->c:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/cy;->c:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 3863
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/c/b/cy;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3864
    iget-object v0, p0, Lcom/google/android/c/b/cy;->b:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/c/b/cy;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 3865
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/c/b/cy;->b:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 3866
    iget-object v1, p0, Lcom/google/android/c/b/cy;->b:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 3867
    if-eqz v1, :cond_0

    .line 3868
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3865
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3872
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/cy;->c:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 3873
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/c/b/cy;->d:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 3875
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3876
    return-void
.end method
