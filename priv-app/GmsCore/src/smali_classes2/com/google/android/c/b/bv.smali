.class public final Lcom/google/android/c/b/bv;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile b:[Lcom/google/android/c/b/bv;


# instance fields
.field public a:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6844
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 6845
    iput v0, p0, Lcom/google/android/c/b/bv;->c:I

    iput v0, p0, Lcom/google/android/c/b/bv;->a:I

    iput v0, p0, Lcom/google/android/c/b/bv;->d:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/bv;->cachedSize:I

    .line 6846
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/bv;
    .locals 2

    .prologue
    .line 6809
    sget-object v0, Lcom/google/android/c/b/bv;->b:[Lcom/google/android/c/b/bv;

    if-nez v0, :cond_1

    .line 6810
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 6812
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/bv;->b:[Lcom/google/android/c/b/bv;

    if-nez v0, :cond_0

    .line 6813
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/bv;

    sput-object v0, Lcom/google/android/c/b/bv;->b:[Lcom/google/android/c/b/bv;

    .line 6815
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6817
    :cond_1
    sget-object v0, Lcom/google/android/c/b/bv;->b:[Lcom/google/android/c/b/bv;

    return-object v0

    .line 6815
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 6868
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 6869
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/bv;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6871
    iget v1, p0, Lcom/google/android/c/b/bv;->c:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 6872
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/bv;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6875
    :cond_0
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 6803
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/bv;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/bv;->d:I

    iget v0, p0, Lcom/google/android/c/b/bv;->c:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bv;->c:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 6859
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/bv;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 6860
    iget v0, p0, Lcom/google/android/c/b/bv;->c:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 6861
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/bv;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 6863
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 6864
    return-void
.end method
