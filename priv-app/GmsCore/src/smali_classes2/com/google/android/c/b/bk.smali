.class public final Lcom/google/android/c/b/bk;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10577
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 10578
    iput v0, p0, Lcom/google/android/c/b/bk;->b:I

    iput v0, p0, Lcom/google/android/c/b/bk;->a:I

    iput v0, p0, Lcom/google/android/c/b/bk;->c:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/bk;->cachedSize:I

    .line 10579
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 10561
    iget v0, p0, Lcom/google/android/c/b/bk;->c:I

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 10569
    iget v0, p0, Lcom/google/android/c/b/bk;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 10601
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 10602
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/bk;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10604
    iget v1, p0, Lcom/google/android/c/b/bk;->b:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 10605
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/bk;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10608
    :cond_0
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 10536
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/bk;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/bk;->c:I

    iget v0, p0, Lcom/google/android/c/b/bk;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bk;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 10592
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/bk;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 10593
    iget v0, p0, Lcom/google/android/c/b/bk;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 10594
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/bk;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 10596
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 10597
    return-void
.end method
