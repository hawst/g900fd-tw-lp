.class public final Lcom/google/android/c/b/bq;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:Lcom/google/android/c/b/br;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3540
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 3541
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/c/b/bq;->a:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bq;->b:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/c/b/bq;->c:Lcom/google/android/c/b/br;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/bq;->cachedSize:I

    .line 3542
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3565
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 3566
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/bq;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3568
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/bq;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3570
    iget-object v1, p0, Lcom/google/android/c/b/bq;->c:Lcom/google/android/c/b/br;

    if-eqz v1, :cond_0

    .line 3571
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/c/b/bq;->c:Lcom/google/android/c/b/br;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3574
    :cond_0
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 3406
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/bq;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/c/b/bq;->b:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/c/b/bq;->c:Lcom/google/android/c/b/br;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/c/b/br;

    invoke-direct {v0}, Lcom/google/android/c/b/br;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/bq;->c:Lcom/google/android/c/b/br;

    :cond_1
    iget-object v0, p0, Lcom/google/android/c/b/bq;->c:Lcom/google/android/c/b/br;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 3555
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/bq;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3556
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/bq;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3557
    iget-object v0, p0, Lcom/google/android/c/b/bq;->c:Lcom/google/android/c/b/br;

    if-eqz v0, :cond_0

    .line 3558
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/c/b/bq;->c:Lcom/google/android/c/b/br;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3560
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3561
    return-void
.end method
