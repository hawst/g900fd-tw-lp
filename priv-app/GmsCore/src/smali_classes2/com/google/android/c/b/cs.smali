.class public final Lcom/google/android/c/b/cs;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile b:[Lcom/google/android/c/b/cs;


# instance fields
.field public a:I

.field private c:I

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6636
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 6637
    iput v0, p0, Lcom/google/android/c/b/cs;->c:I

    iput v0, p0, Lcom/google/android/c/b/cs;->a:I

    iput-boolean v0, p0, Lcom/google/android/c/b/cs;->d:Z

    iput-boolean v0, p0, Lcom/google/android/c/b/cs;->e:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/cs;->cachedSize:I

    .line 6638
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/cs;
    .locals 2

    .prologue
    .line 6582
    sget-object v0, Lcom/google/android/c/b/cs;->b:[Lcom/google/android/c/b/cs;

    if-nez v0, :cond_1

    .line 6583
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 6585
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/cs;->b:[Lcom/google/android/c/b/cs;

    if-nez v0, :cond_0

    .line 6586
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/cs;

    sput-object v0, Lcom/google/android/c/b/cs;->b:[Lcom/google/android/c/b/cs;

    .line 6588
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6590
    :cond_1
    sget-object v0, Lcom/google/android/c/b/cs;->b:[Lcom/google/android/c/b/cs;

    return-object v0

    .line 6588
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 6664
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 6665
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/cs;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6667
    iget v1, p0, Lcom/google/android/c/b/cs;->c:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 6668
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/c/b/cs;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6671
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/cs;->c:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 6672
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/c/b/cs;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6675
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 6576
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/cs;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/cs;->d:Z

    iget v0, p0, Lcom/google/android/c/b/cs;->c:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/cs;->c:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/cs;->e:Z

    iget v0, p0, Lcom/google/android/c/b/cs;->c:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/cs;->c:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 6652
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/cs;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 6653
    iget v0, p0, Lcom/google/android/c/b/cs;->c:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 6654
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/c/b/cs;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 6656
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/cs;->c:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 6657
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/c/b/cs;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 6659
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 6660
    return-void
.end method
