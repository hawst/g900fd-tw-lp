.class public final Lcom/google/android/c/b/v;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile a:[Lcom/google/android/c/b/v;


# instance fields
.field private b:I

.field private c:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7301
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 7302
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/c/b/v;->b:I

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/c/b/v;->c:[B

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/v;->cachedSize:I

    .line 7303
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/v;
    .locals 2

    .prologue
    .line 7266
    sget-object v0, Lcom/google/android/c/b/v;->a:[Lcom/google/android/c/b/v;

    if-nez v0, :cond_1

    .line 7267
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 7269
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/v;->a:[Lcom/google/android/c/b/v;

    if-nez v0, :cond_0

    .line 7270
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/v;

    sput-object v0, Lcom/google/android/c/b/v;->a:[Lcom/google/android/c/b/v;

    .line 7272
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7274
    :cond_1
    sget-object v0, Lcom/google/android/c/b/v;->a:[Lcom/google/android/c/b/v;

    return-object v0

    .line 7272
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 7323
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 7324
    iget v1, p0, Lcom/google/android/c/b/v;->b:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 7325
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/c/b/v;->c:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 7328
    :cond_0
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 7260
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/v;->c:[B

    iget v0, p0, Lcom/google/android/c/b/v;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/v;->b:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 7315
    iget v0, p0, Lcom/google/android/c/b/v;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 7316
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/c/b/v;->c:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 7318
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 7319
    return-void
.end method
