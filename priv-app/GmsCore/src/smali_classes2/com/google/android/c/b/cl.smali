.class public final Lcom/google/android/c/b/cl;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5054
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 5055
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/c/b/cl;->a:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/cl;->cachedSize:I

    .line 5056
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 5073
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 5074
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/cl;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5076
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5034
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_2
    iput v0, p0, Lcom/google/android/c/b/cl;->a:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0xff -> :sswitch_2
        -0xfe -> :sswitch_2
        -0xfd -> :sswitch_2
        -0xfb -> :sswitch_2
        -0x14 -> :sswitch_2
        -0x13 -> :sswitch_2
        -0x12 -> :sswitch_2
        -0x11 -> :sswitch_2
        -0x10 -> :sswitch_2
        -0xf -> :sswitch_2
        -0xe -> :sswitch_2
        -0xd -> :sswitch_2
        -0xc -> :sswitch_2
        -0xb -> :sswitch_2
        -0xa -> :sswitch_2
        -0x9 -> :sswitch_2
        -0x8 -> :sswitch_2
        -0x7 -> :sswitch_2
        -0x6 -> :sswitch_2
        -0x5 -> :sswitch_2
        -0x4 -> :sswitch_2
        -0x3 -> :sswitch_2
        -0x2 -> :sswitch_2
        -0x1 -> :sswitch_2
        0x0 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 5067
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/cl;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5068
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5069
    return-void
.end method
