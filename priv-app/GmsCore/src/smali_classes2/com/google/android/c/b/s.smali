.class public final Lcom/google/android/c/b/s;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile b:[Lcom/google/android/c/b/s;


# instance fields
.field public a:I

.field private c:I

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6489
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 6490
    iput v0, p0, Lcom/google/android/c/b/s;->c:I

    iput v0, p0, Lcom/google/android/c/b/s;->a:I

    iput v0, p0, Lcom/google/android/c/b/s;->d:I

    iput v0, p0, Lcom/google/android/c/b/s;->e:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/s;->cachedSize:I

    .line 6491
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/s;
    .locals 2

    .prologue
    .line 6435
    sget-object v0, Lcom/google/android/c/b/s;->b:[Lcom/google/android/c/b/s;

    if-nez v0, :cond_1

    .line 6436
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 6438
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/s;->b:[Lcom/google/android/c/b/s;

    if-nez v0, :cond_0

    .line 6439
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/s;

    sput-object v0, Lcom/google/android/c/b/s;->b:[Lcom/google/android/c/b/s;

    .line 6441
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6443
    :cond_1
    sget-object v0, Lcom/google/android/c/b/s;->b:[Lcom/google/android/c/b/s;

    return-object v0

    .line 6441
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final b()I
    .locals 1

    .prologue
    .line 6454
    iget v0, p0, Lcom/google/android/c/b/s;->d:I

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 6462
    iget v0, p0, Lcom/google/android/c/b/s;->c:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 6517
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 6518
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/s;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6520
    iget v1, p0, Lcom/google/android/c/b/s;->c:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 6521
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/s;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6524
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/s;->c:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 6525
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/c/b/s;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6528
    :cond_1
    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 6473
    iget v0, p0, Lcom/google/android/c/b/s;->e:I

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 6481
    iget v0, p0, Lcom/google/android/c/b/s;->c:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 6429
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/s;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/s;->d:I

    iget v0, p0, Lcom/google/android/c/b/s;->c:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/s;->c:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/s;->e:I

    iget v0, p0, Lcom/google/android/c/b/s;->c:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/s;->c:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 6505
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/s;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 6506
    iget v0, p0, Lcom/google/android/c/b/s;->c:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 6507
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/s;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 6509
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/s;->c:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 6510
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/c/b/s;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 6512
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 6513
    return-void
.end method
