.class public final Lcom/google/android/c/b/ay;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/c/b/ax;

.field public b:[Lcom/google/android/c/b/be;

.field private c:I

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 15700
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 15701
    iput v1, p0, Lcom/google/android/c/b/ay;->c:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/c/b/ay;->a:Lcom/google/android/c/b/ax;

    iput v1, p0, Lcom/google/android/c/b/ay;->d:I

    iput v1, p0, Lcom/google/android/c/b/ay;->e:I

    invoke-static {}, Lcom/google/android/c/b/be;->a()[Lcom/google/android/c/b/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/ay;->b:[Lcom/google/android/c/b/be;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/ay;->cachedSize:I

    .line 15702
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/c/b/ay;
    .locals 1

    .prologue
    .line 15665
    iput p1, p0, Lcom/google/android/c/b/ay;->d:I

    .line 15666
    iget v0, p0, Lcom/google/android/c/b/ay;->c:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/ay;->c:I

    .line 15667
    return-object p0
.end method

.method public final b(I)Lcom/google/android/c/b/ay;
    .locals 1

    .prologue
    .line 15684
    iput p1, p0, Lcom/google/android/c/b/ay;->e:I

    .line 15685
    iget v0, p0, Lcom/google/android/c/b/ay;->c:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/ay;->c:I

    .line 15686
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 15739
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 15740
    iget-object v1, p0, Lcom/google/android/c/b/ay;->a:Lcom/google/android/c/b/ax;

    if-eqz v1, :cond_0

    .line 15741
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/c/b/ay;->a:Lcom/google/android/c/b/ax;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15744
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/ay;->c:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 15745
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/ay;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 15748
    :cond_1
    iget v1, p0, Lcom/google/android/c/b/ay;->c:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 15749
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/c/b/ay;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 15752
    :cond_2
    iget-object v1, p0, Lcom/google/android/c/b/ay;->b:[Lcom/google/android/c/b/be;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/c/b/ay;->b:[Lcom/google/android/c/b/be;

    array-length v1, v1

    if-lez v1, :cond_5

    .line 15753
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/android/c/b/ay;->b:[Lcom/google/android/c/b/be;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 15754
    iget-object v2, p0, Lcom/google/android/c/b/ay;->b:[Lcom/google/android/c/b/be;

    aget-object v2, v2, v0

    .line 15755
    if-eqz v2, :cond_3

    .line 15756
    const/4 v3, 0x4

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 15753
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 15761
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 15637
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/c/b/ay;->a:Lcom/google/android/c/b/ax;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/c/b/ax;

    invoke-direct {v0}, Lcom/google/android/c/b/ax;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/ay;->a:Lcom/google/android/c/b/ax;

    :cond_1
    iget-object v0, p0, Lcom/google/android/c/b/ay;->a:Lcom/google/android/c/b/ax;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/ay;->d:I

    iget v0, p0, Lcom/google/android/c/b/ay;->c:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/ay;->c:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/ay;->e:I

    iget v0, p0, Lcom/google/android/c/b/ay;->c:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/ay;->c:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/c/b/ay;->b:[Lcom/google/android/c/b/be;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/c/b/be;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/android/c/b/ay;->b:[Lcom/google/android/c/b/be;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/android/c/b/be;

    invoke-direct {v3}, Lcom/google/android/c/b/be;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/c/b/ay;->b:[Lcom/google/android/c/b/be;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/android/c/b/be;

    invoke-direct {v3}, Lcom/google/android/c/b/be;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/c/b/ay;->b:[Lcom/google/android/c/b/be;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 15717
    iget-object v0, p0, Lcom/google/android/c/b/ay;->a:Lcom/google/android/c/b/ax;

    if-eqz v0, :cond_0

    .line 15718
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/c/b/ay;->a:Lcom/google/android/c/b/ax;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 15720
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/ay;->c:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 15721
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/ay;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 15723
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/ay;->c:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 15724
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/c/b/ay;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 15726
    :cond_2
    iget-object v0, p0, Lcom/google/android/c/b/ay;->b:[Lcom/google/android/c/b/be;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/c/b/ay;->b:[Lcom/google/android/c/b/be;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 15727
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/c/b/ay;->b:[Lcom/google/android/c/b/be;

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 15728
    iget-object v1, p0, Lcom/google/android/c/b/ay;->b:[Lcom/google/android/c/b/be;

    aget-object v1, v1, v0

    .line 15729
    if-eqz v1, :cond_3

    .line 15730
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 15727
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 15734
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 15735
    return-void
.end method
