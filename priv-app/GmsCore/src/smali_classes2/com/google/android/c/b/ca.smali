.class public final Lcom/google/android/c/b/ca;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/c/b/an;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14916
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 14917
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/c/b/ca;->b:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/c/b/ca;->a:Lcom/google/android/c/b/an;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/ca;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/ca;->d:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/ca;->cachedSize:I

    .line 14918
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14875
    iget-object v0, p0, Lcom/google/android/c/b/ca;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14897
    iget-object v0, p0, Lcom/google/android/c/b/ca;->d:Ljava/lang/String;

    return-object v0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 14946
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 14947
    iget-object v1, p0, Lcom/google/android/c/b/ca;->a:Lcom/google/android/c/b/an;

    if-eqz v1, :cond_0

    .line 14948
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/c/b/ca;->a:Lcom/google/android/c/b/an;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14951
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/ca;->b:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 14952
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/c/b/ca;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14955
    :cond_1
    iget v1, p0, Lcom/google/android/c/b/ca;->b:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 14956
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/c/b/ca;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14959
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 14850
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/c/b/ca;->a:Lcom/google/android/c/b/an;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/c/b/an;

    invoke-direct {v0}, Lcom/google/android/c/b/an;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/ca;->a:Lcom/google/android/c/b/an;

    :cond_1
    iget-object v0, p0, Lcom/google/android/c/b/ca;->a:Lcom/google/android/c/b/an;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/ca;->c:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/ca;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/ca;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/ca;->d:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/ca;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/ca;->b:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 14932
    iget-object v0, p0, Lcom/google/android/c/b/ca;->a:Lcom/google/android/c/b/an;

    if-eqz v0, :cond_0

    .line 14933
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/c/b/ca;->a:Lcom/google/android/c/b/an;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 14935
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/ca;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 14936
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/c/b/ca;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 14938
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/ca;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 14939
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/c/b/ca;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 14941
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 14942
    return-void
.end method
