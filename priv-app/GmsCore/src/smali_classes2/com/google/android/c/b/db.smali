.class public final Lcom/google/android/c/b/db;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9896
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 9897
    iput v1, p0, Lcom/google/android/c/b/db;->a:I

    iput v1, p0, Lcom/google/android/c/b/db;->b:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/db;->c:I

    iput v1, p0, Lcom/google/android/c/b/db;->d:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/db;->cachedSize:I

    .line 9898
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/c/b/db;
    .locals 1

    .prologue
    .line 9864
    iput p1, p0, Lcom/google/android/c/b/db;->c:I

    .line 9865
    iget v0, p0, Lcom/google/android/c/b/db;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/db;->a:I

    .line 9866
    return-object p0
.end method

.method public final b(I)Lcom/google/android/c/b/db;
    .locals 1

    .prologue
    .line 9883
    iput p1, p0, Lcom/google/android/c/b/db;->d:I

    .line 9884
    iget v0, p0, Lcom/google/android/c/b/db;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/c/b/db;->a:I

    .line 9885
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 9926
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 9927
    iget v1, p0, Lcom/google/android/c/b/db;->a:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 9928
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/db;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9931
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/db;->a:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 9932
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/db;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9935
    :cond_1
    iget v1, p0, Lcom/google/android/c/b/db;->a:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 9936
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/c/b/db;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9939
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 9820
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/db;->b:I

    iget v0, p0, Lcom/google/android/c/b/db;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/db;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/c/b/db;->c:I

    iget v0, p0, Lcom/google/android/c/b/db;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/db;->a:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/android/c/b/db;->d:I

    iget v0, p0, Lcom/google/android/c/b/db;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/c/b/db;->a:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 9912
    iget v0, p0, Lcom/google/android/c/b/db;->a:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 9913
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/db;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 9915
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/db;->a:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 9916
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/db;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 9918
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/db;->a:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 9919
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/c/b/db;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 9921
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 9922
    return-void
.end method
