.class public final Lcom/google/android/c/b/bh;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/c/b/bg;

.field public b:[Lcom/google/android/c/b/ax;

.field private c:I

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 15323
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 15324
    iput v1, p0, Lcom/google/android/c/b/bh;->c:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/c/b/bh;->a:Lcom/google/android/c/b/bg;

    iput v1, p0, Lcom/google/android/c/b/bh;->d:I

    iput v1, p0, Lcom/google/android/c/b/bh;->e:I

    invoke-static {}, Lcom/google/android/c/b/ax;->a()[Lcom/google/android/c/b/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/bh;->b:[Lcom/google/android/c/b/ax;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/bh;->cachedSize:I

    .line 15325
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/c/b/bh;
    .locals 1

    .prologue
    .line 15288
    iput p1, p0, Lcom/google/android/c/b/bh;->d:I

    .line 15289
    iget v0, p0, Lcom/google/android/c/b/bh;->c:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bh;->c:I

    .line 15290
    return-object p0
.end method

.method public final b(I)Lcom/google/android/c/b/bh;
    .locals 1

    .prologue
    .line 15307
    iput p1, p0, Lcom/google/android/c/b/bh;->e:I

    .line 15308
    iget v0, p0, Lcom/google/android/c/b/bh;->c:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/bh;->c:I

    .line 15309
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 15362
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 15363
    iget-object v1, p0, Lcom/google/android/c/b/bh;->a:Lcom/google/android/c/b/bg;

    if-eqz v1, :cond_0

    .line 15364
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/c/b/bh;->a:Lcom/google/android/c/b/bg;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15367
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/bh;->c:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 15368
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/bh;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 15371
    :cond_1
    iget v1, p0, Lcom/google/android/c/b/bh;->c:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 15372
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/c/b/bh;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 15375
    :cond_2
    iget-object v1, p0, Lcom/google/android/c/b/bh;->b:[Lcom/google/android/c/b/ax;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/c/b/bh;->b:[Lcom/google/android/c/b/ax;

    array-length v1, v1

    if-lez v1, :cond_5

    .line 15376
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/android/c/b/bh;->b:[Lcom/google/android/c/b/ax;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 15377
    iget-object v2, p0, Lcom/google/android/c/b/bh;->b:[Lcom/google/android/c/b/ax;

    aget-object v2, v2, v0

    .line 15378
    if-eqz v2, :cond_3

    .line 15379
    const/4 v3, 0x4

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 15376
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 15384
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 15260
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/c/b/bh;->a:Lcom/google/android/c/b/bg;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/c/b/bg;

    invoke-direct {v0}, Lcom/google/android/c/b/bg;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/bh;->a:Lcom/google/android/c/b/bg;

    :cond_1
    iget-object v0, p0, Lcom/google/android/c/b/bh;->a:Lcom/google/android/c/b/bg;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/bh;->d:I

    iget v0, p0, Lcom/google/android/c/b/bh;->c:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bh;->c:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/bh;->e:I

    iget v0, p0, Lcom/google/android/c/b/bh;->c:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/bh;->c:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/c/b/bh;->b:[Lcom/google/android/c/b/ax;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/c/b/ax;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/android/c/b/bh;->b:[Lcom/google/android/c/b/ax;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/android/c/b/ax;

    invoke-direct {v3}, Lcom/google/android/c/b/ax;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/c/b/bh;->b:[Lcom/google/android/c/b/ax;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/android/c/b/ax;

    invoke-direct {v3}, Lcom/google/android/c/b/ax;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/c/b/bh;->b:[Lcom/google/android/c/b/ax;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 15340
    iget-object v0, p0, Lcom/google/android/c/b/bh;->a:Lcom/google/android/c/b/bg;

    if-eqz v0, :cond_0

    .line 15341
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/c/b/bh;->a:Lcom/google/android/c/b/bg;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 15343
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/bh;->c:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 15344
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/bh;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 15346
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/bh;->c:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 15347
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/c/b/bh;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 15349
    :cond_2
    iget-object v0, p0, Lcom/google/android/c/b/bh;->b:[Lcom/google/android/c/b/ax;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/c/b/bh;->b:[Lcom/google/android/c/b/ax;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 15350
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/c/b/bh;->b:[Lcom/google/android/c/b/ax;

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 15351
    iget-object v1, p0, Lcom/google/android/c/b/bh;->b:[Lcom/google/android/c/b/ax;

    aget-object v1, v1, v0

    .line 15352
    if-eqz v1, :cond_3

    .line 15353
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 15350
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 15357
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 15358
    return-void
.end method
