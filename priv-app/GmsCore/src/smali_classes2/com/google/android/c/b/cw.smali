.class public final Lcom/google/android/c/b/cw;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/android/c/b/cx;

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 11483
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 11484
    iput v1, p0, Lcom/google/android/c/b/cw;->b:I

    invoke-static {}, Lcom/google/android/c/b/cx;->a()[Lcom/google/android/c/b/cx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/cw;->a:[Lcom/google/android/c/b/cx;

    iput v1, p0, Lcom/google/android/c/b/cw;->c:I

    iput v1, p0, Lcom/google/android/c/b/cw;->d:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/cw;->cachedSize:I

    .line 11485
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 11448
    iget v0, p0, Lcom/google/android/c/b/cw;->c:I

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 11467
    iget v0, p0, Lcom/google/android/c/b/cw;->d:I

    return v0
.end method

.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 11518
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v1

    .line 11519
    iget-object v0, p0, Lcom/google/android/c/b/cw;->a:[Lcom/google/android/c/b/cx;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/c/b/cw;->a:[Lcom/google/android/c/b/cx;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 11520
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/c/b/cw;->a:[Lcom/google/android/c/b/cx;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 11521
    iget-object v2, p0, Lcom/google/android/c/b/cw;->a:[Lcom/google/android/c/b/cx;

    aget-object v2, v2, v0

    .line 11522
    if-eqz v2, :cond_0

    .line 11523
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 11520
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 11528
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/cw;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 11529
    const/4 v0, 0x2

    iget v2, p0, Lcom/google/android/c/b/cw;->c:I

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 11532
    :cond_2
    iget v0, p0, Lcom/google/android/c/b/cw;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 11533
    const/4 v0, 0x3

    iget v2, p0, Lcom/google/android/c/b/cw;->d:I

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 11536
    :cond_3
    return v1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 11321
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/c/b/cw;->a:[Lcom/google/android/c/b/cx;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/c/b/cx;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/c/b/cw;->a:[Lcom/google/android/c/b/cx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/c/b/cx;

    invoke-direct {v3}, Lcom/google/android/c/b/cx;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/c/b/cw;->a:[Lcom/google/android/c/b/cx;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/c/b/cx;

    invoke-direct {v3}, Lcom/google/android/c/b/cx;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/c/b/cw;->a:[Lcom/google/android/c/b/cx;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/cw;->c:I

    iget v0, p0, Lcom/google/android/c/b/cw;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/cw;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/android/c/b/cw;->d:I

    iget v0, p0, Lcom/google/android/c/b/cw;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/cw;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 11499
    iget-object v0, p0, Lcom/google/android/c/b/cw;->a:[Lcom/google/android/c/b/cx;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/c/b/cw;->a:[Lcom/google/android/c/b/cx;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 11500
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/c/b/cw;->a:[Lcom/google/android/c/b/cx;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 11501
    iget-object v1, p0, Lcom/google/android/c/b/cw;->a:[Lcom/google/android/c/b/cx;

    aget-object v1, v1, v0

    .line 11502
    if-eqz v1, :cond_0

    .line 11503
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 11500
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 11507
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/cw;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 11508
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/cw;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 11510
    :cond_2
    iget v0, p0, Lcom/google/android/c/b/cw;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 11511
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/c/b/cw;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 11513
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 11514
    return-void
.end method
