.class public final Lcom/google/android/c/b/ai;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile a:[Lcom/google/android/c/b/ai;


# instance fields
.field private b:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7696
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 7697
    iput v0, p0, Lcom/google/android/c/b/ai;->b:I

    iput v0, p0, Lcom/google/android/c/b/ai;->c:I

    iput v0, p0, Lcom/google/android/c/b/ai;->d:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/ai;->cachedSize:I

    .line 7698
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/ai;
    .locals 2

    .prologue
    .line 7645
    sget-object v0, Lcom/google/android/c/b/ai;->a:[Lcom/google/android/c/b/ai;

    if-nez v0, :cond_1

    .line 7646
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 7648
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/ai;->a:[Lcom/google/android/c/b/ai;

    if-nez v0, :cond_0

    .line 7649
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/ai;

    sput-object v0, Lcom/google/android/c/b/ai;->a:[Lcom/google/android/c/b/ai;

    .line 7651
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7653
    :cond_1
    sget-object v0, Lcom/google/android/c/b/ai;->a:[Lcom/google/android/c/b/ai;

    return-object v0

    .line 7651
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final b()I
    .locals 1

    .prologue
    .line 7661
    iget v0, p0, Lcom/google/android/c/b/ai;->c:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 7680
    iget v0, p0, Lcom/google/android/c/b/ai;->d:I

    return v0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 7722
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 7723
    iget v1, p0, Lcom/google/android/c/b/ai;->b:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 7724
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/ai;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7727
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/ai;->b:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 7728
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/ai;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7731
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 7639
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/ai;->c:I

    iget v0, p0, Lcom/google/android/c/b/ai;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/ai;->b:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/ai;->d:I

    iget v0, p0, Lcom/google/android/c/b/ai;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/ai;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 7711
    iget v0, p0, Lcom/google/android/c/b/ai;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 7712
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/ai;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7714
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/ai;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 7715
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/ai;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7717
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 7718
    return-void
.end method
