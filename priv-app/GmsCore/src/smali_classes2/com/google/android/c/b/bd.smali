.class public final Lcom/google/android/c/b/bd;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/android/c/b/f;

.field public b:[Lcom/google/android/c/b/cz;

.field private c:I

.field private d:I

.field private e:I

.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1978
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1979
    iput v1, p0, Lcom/google/android/c/b/bd;->c:I

    iput v0, p0, Lcom/google/android/c/b/bd;->d:I

    iput v0, p0, Lcom/google/android/c/b/bd;->e:I

    invoke-static {}, Lcom/google/android/c/b/f;->a()[Lcom/google/android/c/b/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/bd;->a:[Lcom/google/android/c/b/f;

    invoke-static {}, Lcom/google/android/c/b/cz;->a()[Lcom/google/android/c/b/cz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/bd;->b:[Lcom/google/android/c/b/cz;

    iput-boolean v1, p0, Lcom/google/android/c/b/bd;->f:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/bd;->cachedSize:I

    .line 1980
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1918
    iget v0, p0, Lcom/google/android/c/b/bd;->d:I

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1937
    iget v0, p0, Lcom/google/android/c/b/bd;->e:I

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1945
    iget v0, p0, Lcom/google/android/c/b/bd;->c:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2026
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2027
    iget v2, p0, Lcom/google/android/c/b/bd;->c:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 2028
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/c/b/bd;->d:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2031
    :cond_0
    iget v2, p0, Lcom/google/android/c/b/bd;->c:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 2032
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/c/b/bd;->e:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2035
    :cond_1
    iget-object v2, p0, Lcom/google/android/c/b/bd;->a:[Lcom/google/android/c/b/f;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/c/b/bd;->a:[Lcom/google/android/c/b/f;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v0

    move v0, v1

    .line 2036
    :goto_0
    iget-object v3, p0, Lcom/google/android/c/b/bd;->a:[Lcom/google/android/c/b/f;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 2037
    iget-object v3, p0, Lcom/google/android/c/b/bd;->a:[Lcom/google/android/c/b/f;

    aget-object v3, v3, v0

    .line 2038
    if-eqz v3, :cond_2

    .line 2039
    const/4 v4, 0x3

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2036
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 2044
    :cond_4
    iget-object v2, p0, Lcom/google/android/c/b/bd;->b:[Lcom/google/android/c/b/cz;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/c/b/bd;->b:[Lcom/google/android/c/b/cz;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 2045
    :goto_1
    iget-object v2, p0, Lcom/google/android/c/b/bd;->b:[Lcom/google/android/c/b/cz;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 2046
    iget-object v2, p0, Lcom/google/android/c/b/bd;->b:[Lcom/google/android/c/b/cz;

    aget-object v2, v2, v1

    .line 2047
    if-eqz v2, :cond_5

    .line 2048
    const/4 v3, 0x4

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2045
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2053
    :cond_6
    iget v1, p0, Lcom/google/android/c/b/bd;->c:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_7

    .line 2054
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/c/b/bd;->f:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2057
    :cond_7
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1896
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/c/b/bd;->d:I

    iget v0, p0, Lcom/google/android/c/b/bd;->c:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bd;->c:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/android/c/b/bd;->e:I

    iget v0, p0, Lcom/google/android/c/b/bd;->c:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/bd;->c:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/c/b/bd;->a:[Lcom/google/android/c/b/f;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/c/b/f;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/c/b/bd;->a:[Lcom/google/android/c/b/f;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/c/b/f;

    invoke-direct {v3}, Lcom/google/android/c/b/f;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/c/b/bd;->a:[Lcom/google/android/c/b/f;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/c/b/f;

    invoke-direct {v3}, Lcom/google/android/c/b/f;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/c/b/bd;->a:[Lcom/google/android/c/b/f;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/c/b/bd;->b:[Lcom/google/android/c/b/cz;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/c/b/cz;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/c/b/bd;->b:[Lcom/google/android/c/b/cz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/android/c/b/cz;

    invoke-direct {v3}, Lcom/google/android/c/b/cz;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/android/c/b/bd;->b:[Lcom/google/android/c/b/cz;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/android/c/b/cz;

    invoke-direct {v3}, Lcom/google/android/c/b/cz;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/c/b/bd;->b:[Lcom/google/android/c/b/cz;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/bd;->f:Z

    iget v0, p0, Lcom/google/android/c/b/bd;->c:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/c/b/bd;->c:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1996
    iget v0, p0, Lcom/google/android/c/b/bd;->c:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1997
    const/4 v0, 0x1

    iget v2, p0, Lcom/google/android/c/b/bd;->d:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1999
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/bd;->c:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2000
    const/4 v0, 0x2

    iget v2, p0, Lcom/google/android/c/b/bd;->e:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2002
    :cond_1
    iget-object v0, p0, Lcom/google/android/c/b/bd;->a:[Lcom/google/android/c/b/f;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/c/b/bd;->a:[Lcom/google/android/c/b/f;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 2003
    :goto_0
    iget-object v2, p0, Lcom/google/android/c/b/bd;->a:[Lcom/google/android/c/b/f;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 2004
    iget-object v2, p0, Lcom/google/android/c/b/bd;->a:[Lcom/google/android/c/b/f;

    aget-object v2, v2, v0

    .line 2005
    if-eqz v2, :cond_2

    .line 2006
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2003
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2010
    :cond_3
    iget-object v0, p0, Lcom/google/android/c/b/bd;->b:[Lcom/google/android/c/b/cz;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/c/b/bd;->b:[Lcom/google/android/c/b/cz;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 2011
    :goto_1
    iget-object v0, p0, Lcom/google/android/c/b/bd;->b:[Lcom/google/android/c/b/cz;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 2012
    iget-object v0, p0, Lcom/google/android/c/b/bd;->b:[Lcom/google/android/c/b/cz;

    aget-object v0, v0, v1

    .line 2013
    if-eqz v0, :cond_4

    .line 2014
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2011
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2018
    :cond_5
    iget v0, p0, Lcom/google/android/c/b/bd;->c:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_6

    .line 2019
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/c/b/bd;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2021
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2022
    return-void
.end method
