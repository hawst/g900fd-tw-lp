.class public final Lcom/google/android/c/b/cq;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/android/c/b/co;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Z

.field private m:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1144
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1145
    iput v1, p0, Lcom/google/android/c/b/cq;->b:I

    invoke-static {}, Lcom/google/android/c/b/co;->a()[Lcom/google/android/c/b/co;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/cq;->a:[Lcom/google/android/c/b/co;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/cq;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/cq;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/cq;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/cq;->f:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/c/b/cq;->g:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/cq;->h:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/cq;->i:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/cq;->j:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/cq;->k:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/android/c/b/cq;->l:Z

    iput-boolean v1, p0, Lcom/google/android/c/b/cq;->m:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/cq;->cachedSize:I

    .line 1146
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 914
    iget-object v0, p0, Lcom/google/android/c/b/cq;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 936
    iget-object v0, p0, Lcom/google/android/c/b/cq;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 958
    iget-object v0, p0, Lcom/google/android/c/b/cq;->e:Ljava/lang/String;

    return-object v0
.end method

.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 1215
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v1

    .line 1216
    iget-object v0, p0, Lcom/google/android/c/b/cq;->a:[Lcom/google/android/c/b/co;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/c/b/cq;->a:[Lcom/google/android/c/b/co;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 1217
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/c/b/cq;->a:[Lcom/google/android/c/b/co;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1218
    iget-object v2, p0, Lcom/google/android/c/b/cq;->a:[Lcom/google/android/c/b/co;

    aget-object v2, v2, v0

    .line 1219
    if-eqz v2, :cond_0

    .line 1220
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1217
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1225
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 1226
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/c/b/cq;->c:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 1229
    :cond_2
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 1230
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/android/c/b/cq;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 1233
    :cond_3
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 1234
    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/android/c/b/cq;->e:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 1237
    :cond_4
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_5

    .line 1238
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/android/c/b/cq;->f:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 1241
    :cond_5
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_6

    .line 1242
    const/4 v0, 0x6

    iget v2, p0, Lcom/google/android/c/b/cq;->g:I

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 1245
    :cond_6
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_7

    .line 1246
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/android/c/b/cq;->h:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 1249
    :cond_7
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_8

    .line 1250
    const/16 v0, 0x8

    iget-object v2, p0, Lcom/google/android/c/b/cq;->i:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 1253
    :cond_8
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_9

    .line 1254
    const/16 v0, 0x9

    iget-object v2, p0, Lcom/google/android/c/b/cq;->j:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 1257
    :cond_9
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_a

    .line 1258
    const/16 v0, 0xa

    iget-object v2, p0, Lcom/google/android/c/b/cq;->k:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 1261
    :cond_a
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_b

    .line 1262
    const/16 v0, 0xb

    iget-boolean v2, p0, Lcom/google/android/c/b/cq;->l:Z

    invoke-static {v0}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 1265
    :cond_b
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_c

    .line 1266
    const/16 v0, 0xc

    iget-boolean v2, p0, Lcom/google/android/c/b/cq;->m:Z

    invoke-static {v0}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 1269
    :cond_c
    return v1
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 980
    iget-object v0, p0, Lcom/google/android/c/b/cq;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1128
    iget-boolean v0, p0, Lcom/google/android/c/b/cq;->m:Z

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1136
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 889
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/c/b/cq;->a:[Lcom/google/android/c/b/co;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/c/b/co;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/c/b/cq;->a:[Lcom/google/android/c/b/co;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/c/b/co;

    invoke-direct {v3}, Lcom/google/android/c/b/co;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/c/b/cq;->a:[Lcom/google/android/c/b/co;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/c/b/co;

    invoke-direct {v3}, Lcom/google/android/c/b/co;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/c/b/cq;->a:[Lcom/google/android/c/b/co;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/cq;->c:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/cq;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/cq;->d:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/cq;->b:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/cq;->e:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/c/b/cq;->b:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/cq;->f:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/c/b/cq;->b:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/c/b/cq;->g:I

    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/android/c/b/cq;->b:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/cq;->h:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/android/c/b/cq;->b:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/cq;->i:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/android/c/b/cq;->b:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/cq;->j:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/android/c/b/cq;->b:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/cq;->k:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/android/c/b/cq;->b:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/cq;->l:Z

    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/android/c/b/cq;->b:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/cq;->m:Z

    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/android/c/b/cq;->b:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 1169
    iget-object v0, p0, Lcom/google/android/c/b/cq;->a:[Lcom/google/android/c/b/co;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/c/b/cq;->a:[Lcom/google/android/c/b/co;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 1170
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/c/b/cq;->a:[Lcom/google/android/c/b/co;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1171
    iget-object v1, p0, Lcom/google/android/c/b/cq;->a:[Lcom/google/android/c/b/co;

    aget-object v1, v1, v0

    .line 1172
    if-eqz v1, :cond_0

    .line 1173
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1170
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1177
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 1178
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/c/b/cq;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1180
    :cond_2
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 1181
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/c/b/cq;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1183
    :cond_3
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 1184
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/c/b/cq;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1186
    :cond_4
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_5

    .line 1187
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/c/b/cq;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1189
    :cond_5
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_6

    .line 1190
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/c/b/cq;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1192
    :cond_6
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_7

    .line 1193
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/c/b/cq;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1195
    :cond_7
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_8

    .line 1196
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/c/b/cq;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1198
    :cond_8
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_9

    .line 1199
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/c/b/cq;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1201
    :cond_9
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_a

    .line 1202
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/c/b/cq;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1204
    :cond_a
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_b

    .line 1205
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/google/android/c/b/cq;->l:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1207
    :cond_b
    iget v0, p0, Lcom/google/android/c/b/cq;->b:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_c

    .line 1208
    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/google/android/c/b/cq;->m:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1210
    :cond_c
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1211
    return-void
.end method
