.class public final Lcom/google/android/c/b/az;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:[B

.field private f:Ljava/lang/String;

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14307
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 14308
    iput v1, p0, Lcom/google/android/c/b/az;->a:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/az;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/az;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/az;->d:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/c/b/az;->e:[B

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/az;->f:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/c/b/az;->g:I

    iput v1, p0, Lcom/google/android/c/b/az;->h:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/az;->cachedSize:I

    .line 14309
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/c/b/az;
    .locals 1

    .prologue
    .line 14275
    iput p1, p0, Lcom/google/android/c/b/az;->g:I

    .line 14276
    iget v0, p0, Lcom/google/android/c/b/az;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/android/c/b/az;->a:I

    .line 14277
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/c/b/az;
    .locals 1

    .prologue
    .line 14165
    if-nez p1, :cond_0

    .line 14166
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14168
    :cond_0
    iput-object p1, p0, Lcom/google/android/c/b/az;->b:Ljava/lang/String;

    .line 14169
    iget v0, p0, Lcom/google/android/c/b/az;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/az;->a:I

    .line 14170
    return-object p0
.end method

.method public final a([B)Lcom/google/android/c/b/az;
    .locals 1

    .prologue
    .line 14231
    if-nez p1, :cond_0

    .line 14232
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14234
    :cond_0
    iput-object p1, p0, Lcom/google/android/c/b/az;->e:[B

    .line 14235
    iget v0, p0, Lcom/google/android/c/b/az;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/c/b/az;->a:I

    .line 14236
    return-object p0
.end method

.method public final b(I)Lcom/google/android/c/b/az;
    .locals 1

    .prologue
    .line 14294
    iput p1, p0, Lcom/google/android/c/b/az;->h:I

    .line 14295
    iget v0, p0, Lcom/google/android/c/b/az;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/android/c/b/az;->a:I

    .line 14296
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/c/b/az;
    .locals 1

    .prologue
    .line 14187
    if-nez p1, :cond_0

    .line 14188
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14190
    :cond_0
    iput-object p1, p0, Lcom/google/android/c/b/az;->c:Ljava/lang/String;

    .line 14191
    iget v0, p0, Lcom/google/android/c/b/az;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/az;->a:I

    .line 14192
    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/c/b/az;
    .locals 1

    .prologue
    .line 14209
    if-nez p1, :cond_0

    .line 14210
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14212
    :cond_0
    iput-object p1, p0, Lcom/google/android/c/b/az;->d:Ljava/lang/String;

    .line 14213
    iget v0, p0, Lcom/google/android/c/b/az;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/c/b/az;->a:I

    .line 14214
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 14353
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 14354
    iget v1, p0, Lcom/google/android/c/b/az;->a:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 14355
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/c/b/az;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14358
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/az;->a:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 14359
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/c/b/az;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14362
    :cond_1
    iget v1, p0, Lcom/google/android/c/b/az;->a:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 14363
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/c/b/az;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14366
    :cond_2
    iget v1, p0, Lcom/google/android/c/b/az;->a:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 14367
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/c/b/az;->e:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 14370
    :cond_3
    iget v1, p0, Lcom/google/android/c/b/az;->a:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 14371
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/c/b/az;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14374
    :cond_4
    iget v1, p0, Lcom/google/android/c/b/az;->a:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 14375
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/c/b/az;->g:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14378
    :cond_5
    iget v1, p0, Lcom/google/android/c/b/az;->a:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_6

    .line 14379
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/c/b/az;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14382
    :cond_6
    return v0
.end method

.method public final d(Ljava/lang/String;)Lcom/google/android/c/b/az;
    .locals 1

    .prologue
    .line 14253
    if-nez p1, :cond_0

    .line 14254
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14256
    :cond_0
    iput-object p1, p0, Lcom/google/android/c/b/az;->f:Ljava/lang/String;

    .line 14257
    iget v0, p0, Lcom/google/android/c/b/az;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/android/c/b/az;->a:I

    .line 14258
    return-object p0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 14140
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/az;->b:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/az;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/az;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/az;->c:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/az;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/az;->a:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/az;->d:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/az;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/c/b/az;->a:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/az;->e:[B

    iget v0, p0, Lcom/google/android/c/b/az;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/c/b/az;->a:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/az;->f:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/az;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/android/c/b/az;->a:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/az;->g:I

    iget v0, p0, Lcom/google/android/c/b/az;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/android/c/b/az;->a:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/az;->h:I

    iget v0, p0, Lcom/google/android/c/b/az;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/android/c/b/az;->a:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 14327
    iget v0, p0, Lcom/google/android/c/b/az;->a:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 14328
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/c/b/az;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 14330
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/az;->a:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 14331
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/c/b/az;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 14333
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/az;->a:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 14334
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/c/b/az;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 14336
    :cond_2
    iget v0, p0, Lcom/google/android/c/b/az;->a:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 14337
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/c/b/az;->e:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 14339
    :cond_3
    iget v0, p0, Lcom/google/android/c/b/az;->a:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 14340
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/c/b/az;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 14342
    :cond_4
    iget v0, p0, Lcom/google/android/c/b/az;->a:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 14343
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/c/b/az;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 14345
    :cond_5
    iget v0, p0, Lcom/google/android/c/b/az;->a:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_6

    .line 14346
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/android/c/b/az;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 14348
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 14349
    return-void
.end method
