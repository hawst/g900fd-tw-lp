.class public final Lcom/google/android/c/b/bi;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/c/b/f;

.field private b:I

.field private c:I

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2499
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2500
    iput v1, p0, Lcom/google/android/c/b/bi;->b:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bi;->c:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/c/b/bi;->a:Lcom/google/android/c/b/f;

    iput-boolean v1, p0, Lcom/google/android/c/b/bi;->d:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/bi;->cachedSize:I

    .line 2501
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2461
    iget v0, p0, Lcom/google/android/c/b/bi;->c:I

    return v0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2529
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2530
    iget v1, p0, Lcom/google/android/c/b/bi;->b:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2531
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/bi;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2534
    :cond_0
    iget-object v1, p0, Lcom/google/android/c/b/bi;->a:Lcom/google/android/c/b/f;

    if-eqz v1, :cond_1

    .line 2535
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/c/b/bi;->a:Lcom/google/android/c/b/f;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2538
    :cond_1
    iget v1, p0, Lcom/google/android/c/b/bi;->b:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 2539
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/c/b/bi;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2542
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2439
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/c/b/bi;->c:I

    iget v0, p0, Lcom/google/android/c/b/bi;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bi;->b:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/c/b/bi;->a:Lcom/google/android/c/b/f;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/c/b/f;

    invoke-direct {v0}, Lcom/google/android/c/b/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/bi;->a:Lcom/google/android/c/b/f;

    :cond_1
    iget-object v0, p0, Lcom/google/android/c/b/bi;->a:Lcom/google/android/c/b/f;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/bi;->d:Z

    iget v0, p0, Lcom/google/android/c/b/bi;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/bi;->b:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2515
    iget v0, p0, Lcom/google/android/c/b/bi;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2516
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/bi;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2518
    :cond_0
    iget-object v0, p0, Lcom/google/android/c/b/bi;->a:Lcom/google/android/c/b/f;

    if-eqz v0, :cond_1

    .line 2519
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/c/b/bi;->a:Lcom/google/android/c/b/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2521
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/bi;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 2522
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/c/b/bi;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2524
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2525
    return-void
.end method
