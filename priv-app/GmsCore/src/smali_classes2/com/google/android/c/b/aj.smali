.class public final Lcom/google/android/c/b/aj;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:Lcom/google/android/c/b/cw;

.field public c:Lcom/google/android/c/b/aq;

.field public d:Lcom/google/android/c/b/b;

.field public e:Lcom/google/android/c/b/cf;

.field public f:Lcom/google/android/c/b/cw;

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 10925
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 10926
    iput v3, p0, Lcom/google/android/c/b/aj;->g:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/c/b/aj;->a:J

    iput v3, p0, Lcom/google/android/c/b/aj;->h:I

    iput-object v2, p0, Lcom/google/android/c/b/aj;->b:Lcom/google/android/c/b/cw;

    iput-object v2, p0, Lcom/google/android/c/b/aj;->c:Lcom/google/android/c/b/aq;

    iput-object v2, p0, Lcom/google/android/c/b/aj;->d:Lcom/google/android/c/b/b;

    iput-object v2, p0, Lcom/google/android/c/b/aj;->e:Lcom/google/android/c/b/cf;

    iput-object v2, p0, Lcom/google/android/c/b/aj;->f:Lcom/google/android/c/b/cw;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/aj;->cachedSize:I

    .line 10927
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 10969
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 10970
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/c/b/aj;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 10972
    iget v1, p0, Lcom/google/android/c/b/aj;->g:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 10973
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/aj;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10976
    :cond_0
    iget-object v1, p0, Lcom/google/android/c/b/aj;->b:Lcom/google/android/c/b/cw;

    if-eqz v1, :cond_1

    .line 10977
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/c/b/aj;->b:Lcom/google/android/c/b/cw;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10980
    :cond_1
    iget-object v1, p0, Lcom/google/android/c/b/aj;->c:Lcom/google/android/c/b/aq;

    if-eqz v1, :cond_2

    .line 10981
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/c/b/aj;->c:Lcom/google/android/c/b/aq;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10984
    :cond_2
    iget-object v1, p0, Lcom/google/android/c/b/aj;->d:Lcom/google/android/c/b/b;

    if-eqz v1, :cond_3

    .line 10985
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/c/b/aj;->d:Lcom/google/android/c/b/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10988
    :cond_3
    iget-object v1, p0, Lcom/google/android/c/b/aj;->e:Lcom/google/android/c/b/cf;

    if-eqz v1, :cond_4

    .line 10989
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/c/b/aj;->e:Lcom/google/android/c/b/cf;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10992
    :cond_4
    iget-object v1, p0, Lcom/google/android/c/b/aj;->f:Lcom/google/android/c/b/cw;

    if-eqz v1, :cond_5

    .line 10993
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/c/b/aj;->f:Lcom/google/android/c/b/cw;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10996
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 10869
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/c/b/aj;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/aj;->h:I

    iget v0, p0, Lcom/google/android/c/b/aj;->g:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/aj;->g:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/c/b/aj;->b:Lcom/google/android/c/b/cw;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/c/b/cw;

    invoke-direct {v0}, Lcom/google/android/c/b/cw;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/aj;->b:Lcom/google/android/c/b/cw;

    :cond_1
    iget-object v0, p0, Lcom/google/android/c/b/aj;->b:Lcom/google/android/c/b/cw;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/c/b/aj;->c:Lcom/google/android/c/b/aq;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/c/b/aq;

    invoke-direct {v0}, Lcom/google/android/c/b/aq;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/aj;->c:Lcom/google/android/c/b/aq;

    :cond_2
    iget-object v0, p0, Lcom/google/android/c/b/aj;->c:Lcom/google/android/c/b/aq;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/c/b/aj;->d:Lcom/google/android/c/b/b;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/c/b/b;

    invoke-direct {v0}, Lcom/google/android/c/b/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/aj;->d:Lcom/google/android/c/b/b;

    :cond_3
    iget-object v0, p0, Lcom/google/android/c/b/aj;->d:Lcom/google/android/c/b/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/android/c/b/aj;->e:Lcom/google/android/c/b/cf;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/c/b/cf;

    invoke-direct {v0}, Lcom/google/android/c/b/cf;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/aj;->e:Lcom/google/android/c/b/cf;

    :cond_4
    iget-object v0, p0, Lcom/google/android/c/b/aj;->e:Lcom/google/android/c/b/cf;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/android/c/b/aj;->f:Lcom/google/android/c/b/cw;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/c/b/cw;

    invoke-direct {v0}, Lcom/google/android/c/b/cw;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/aj;->f:Lcom/google/android/c/b/cw;

    :cond_5
    iget-object v0, p0, Lcom/google/android/c/b/aj;->f:Lcom/google/android/c/b/cw;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 10945
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/c/b/aj;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 10946
    iget v0, p0, Lcom/google/android/c/b/aj;->g:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 10947
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/aj;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 10949
    :cond_0
    iget-object v0, p0, Lcom/google/android/c/b/aj;->b:Lcom/google/android/c/b/cw;

    if-eqz v0, :cond_1

    .line 10950
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/c/b/aj;->b:Lcom/google/android/c/b/cw;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 10952
    :cond_1
    iget-object v0, p0, Lcom/google/android/c/b/aj;->c:Lcom/google/android/c/b/aq;

    if-eqz v0, :cond_2

    .line 10953
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/c/b/aj;->c:Lcom/google/android/c/b/aq;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 10955
    :cond_2
    iget-object v0, p0, Lcom/google/android/c/b/aj;->d:Lcom/google/android/c/b/b;

    if-eqz v0, :cond_3

    .line 10956
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/c/b/aj;->d:Lcom/google/android/c/b/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 10958
    :cond_3
    iget-object v0, p0, Lcom/google/android/c/b/aj;->e:Lcom/google/android/c/b/cf;

    if-eqz v0, :cond_4

    .line 10959
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/c/b/aj;->e:Lcom/google/android/c/b/cf;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 10961
    :cond_4
    iget-object v0, p0, Lcom/google/android/c/b/aj;->f:Lcom/google/android/c/b/cw;

    if-eqz v0, :cond_5

    .line 10962
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/c/b/aj;->f:Lcom/google/android/c/b/cw;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 10964
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 10965
    return-void
.end method
