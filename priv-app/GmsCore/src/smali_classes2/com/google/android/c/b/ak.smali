.class public final Lcom/google/android/c/b/ak;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[I

.field public b:[Lcom/google/android/c/b/am;

.field public c:[Lcom/google/android/c/b/al;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2911
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2912
    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/android/c/b/ak;->a:[I

    invoke-static {}, Lcom/google/android/c/b/am;->a()[Lcom/google/android/c/b/am;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/ak;->b:[Lcom/google/android/c/b/am;

    invoke-static {}, Lcom/google/android/c/b/al;->a()[Lcom/google/android/c/b/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/ak;->c:[Lcom/google/android/c/b/al;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/ak;->cachedSize:I

    .line 2913
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2960
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v3

    .line 2961
    iget-object v0, p0, Lcom/google/android/c/b/ak;->a:[I

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/c/b/ak;->a:[I

    array-length v0, v0

    if-lez v0, :cond_6

    move v0, v1

    move v2, v1

    .line 2963
    :goto_0
    iget-object v4, p0, Lcom/google/android/c/b/ak;->a:[I

    array-length v4, v4

    if-ge v0, v4, :cond_0

    .line 2964
    iget-object v4, p0, Lcom/google/android/c/b/ak;->a:[I

    aget v4, v4, v0

    .line 2965
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v4

    add-int/2addr v2, v4

    .line 2963
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2968
    :cond_0
    add-int v0, v3, v2

    .line 2969
    add-int/lit8 v0, v0, 0x1

    .line 2970
    invoke-static {v2}, Lcom/google/protobuf/nano/b;->e(I)I

    move-result v2

    add-int/2addr v0, v2

    .line 2973
    :goto_1
    iget-object v2, p0, Lcom/google/android/c/b/ak;->b:[Lcom/google/android/c/b/am;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/c/b/ak;->b:[Lcom/google/android/c/b/am;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 2974
    :goto_2
    iget-object v3, p0, Lcom/google/android/c/b/ak;->b:[Lcom/google/android/c/b/am;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 2975
    iget-object v3, p0, Lcom/google/android/c/b/ak;->b:[Lcom/google/android/c/b/am;

    aget-object v3, v3, v0

    .line 2976
    if-eqz v3, :cond_1

    .line 2977
    const/4 v4, 0x2

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2974
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move v0, v2

    .line 2982
    :cond_3
    iget-object v2, p0, Lcom/google/android/c/b/ak;->c:[Lcom/google/android/c/b/al;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/c/b/ak;->c:[Lcom/google/android/c/b/al;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 2983
    :goto_3
    iget-object v2, p0, Lcom/google/android/c/b/ak;->c:[Lcom/google/android/c/b/al;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 2984
    iget-object v2, p0, Lcom/google/android/c/b/ak;->c:[Lcom/google/android/c/b/al;

    aget-object v2, v2, v1

    .line 2985
    if-eqz v2, :cond_4

    .line 2986
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2983
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2991
    :cond_5
    return v0

    :cond_6
    move v0, v3

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2703
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/c/b/ak;->a:[I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/c/b/ak;->a:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/c/b/ak;->a:[I

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Lcom/google/android/c/b/ak;->a:[I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v2, p0, Lcom/google/android/c/b/ak;->a:[I

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_5

    iget-object v4, p0, Lcom/google/android/c/b/ak;->a:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Lcom/google/android/c/b/ak;->a:[I

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Lcom/google/android/c/b/ak;->a:[I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_3
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/c/b/ak;->b:[Lcom/google/android/c/b/am;

    if-nez v0, :cond_9

    move v0, v1

    :goto_6
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/c/b/am;

    if-eqz v0, :cond_8

    iget-object v3, p0, Lcom/google/android/c/b/ak;->b:[Lcom/google/android/c/b/am;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_7
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_a

    new-instance v3, Lcom/google/android/c/b/am;

    invoke-direct {v3}, Lcom/google/android/c/b/am;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_9
    iget-object v0, p0, Lcom/google/android/c/b/ak;->b:[Lcom/google/android/c/b/am;

    array-length v0, v0

    goto :goto_6

    :cond_a
    new-instance v3, Lcom/google/android/c/b/am;

    invoke-direct {v3}, Lcom/google/android/c/b/am;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/c/b/ak;->b:[Lcom/google/android/c/b/am;

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/c/b/ak;->c:[Lcom/google/android/c/b/al;

    if-nez v0, :cond_c

    move v0, v1

    :goto_8
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/c/b/al;

    if-eqz v0, :cond_b

    iget-object v3, p0, Lcom/google/android/c/b/ak;->c:[Lcom/google/android/c/b/al;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    :goto_9
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_d

    new-instance v3, Lcom/google/android/c/b/al;

    invoke-direct {v3}, Lcom/google/android/c/b/al;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_c
    iget-object v0, p0, Lcom/google/android/c/b/ak;->c:[Lcom/google/android/c/b/al;

    array-length v0, v0

    goto :goto_8

    :cond_d
    new-instance v3, Lcom/google/android/c/b/al;

    invoke-direct {v3}, Lcom/google/android/c/b/al;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/c/b/ak;->c:[Lcom/google/android/c/b/al;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2926
    iget-object v0, p0, Lcom/google/android/c/b/ak;->a:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/c/b/ak;->a:[I

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    move v2, v1

    .line 2928
    :goto_0
    iget-object v3, p0, Lcom/google/android/c/b/ak;->a:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 2929
    iget-object v3, p0, Lcom/google/android/c/b/ak;->a:[I

    aget v3, v3, v0

    .line 2930
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 2928
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2933
    :cond_0
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/b;->d(I)V

    .line 2934
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/b;->d(I)V

    move v0, v1

    .line 2935
    :goto_1
    iget-object v2, p0, Lcom/google/android/c/b/ak;->a:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 2936
    iget-object v2, p0, Lcom/google/android/c/b/ak;->a:[I

    aget v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/b;->a(I)V

    .line 2935
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2939
    :cond_1
    iget-object v0, p0, Lcom/google/android/c/b/ak;->b:[Lcom/google/android/c/b/am;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/c/b/ak;->b:[Lcom/google/android/c/b/am;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 2940
    :goto_2
    iget-object v2, p0, Lcom/google/android/c/b/ak;->b:[Lcom/google/android/c/b/am;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 2941
    iget-object v2, p0, Lcom/google/android/c/b/ak;->b:[Lcom/google/android/c/b/am;

    aget-object v2, v2, v0

    .line 2942
    if-eqz v2, :cond_2

    .line 2943
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2940
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2947
    :cond_3
    iget-object v0, p0, Lcom/google/android/c/b/ak;->c:[Lcom/google/android/c/b/al;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/c/b/ak;->c:[Lcom/google/android/c/b/al;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 2948
    :goto_3
    iget-object v0, p0, Lcom/google/android/c/b/ak;->c:[Lcom/google/android/c/b/al;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 2949
    iget-object v0, p0, Lcom/google/android/c/b/ak;->c:[Lcom/google/android/c/b/al;

    aget-object v0, v0, v1

    .line 2950
    if-eqz v0, :cond_4

    .line 2951
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2948
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2955
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2956
    return-void
.end method
