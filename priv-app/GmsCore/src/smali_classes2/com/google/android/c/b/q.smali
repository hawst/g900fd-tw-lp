.class public final Lcom/google/android/c/b/q;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4041
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 4042
    iput v0, p0, Lcom/google/android/c/b/q;->a:I

    iput v0, p0, Lcom/google/android/c/b/q;->b:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/q;->cachedSize:I

    .line 4043
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 4062
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 4063
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/q;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->h(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4065
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/q;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4067
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 4018
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/q;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/q;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 4055
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/q;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->e(II)V

    .line 4056
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/q;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 4057
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4058
    return-void
.end method
