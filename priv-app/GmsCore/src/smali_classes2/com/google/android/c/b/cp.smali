.class public final Lcom/google/android/c/b/cp;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:[B

.field private c:[B

.field private d:[B

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 784
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 785
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/c/b/cp;->a:I

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/c/b/cp;->b:[B

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/c/b/cp;->c:[B

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/c/b/cp;->d:[B

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/cp;->e:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/cp;->cachedSize:I

    .line 786
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/c/b/cp;
    .locals 1

    .prologue
    .line 768
    if-nez p1, :cond_0

    .line 769
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 771
    :cond_0
    iput-object p1, p0, Lcom/google/android/c/b/cp;->e:Ljava/lang/String;

    .line 772
    iget v0, p0, Lcom/google/android/c/b/cp;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/c/b/cp;->a:I

    .line 773
    return-object p0
.end method

.method public final a([B)Lcom/google/android/c/b/cp;
    .locals 1

    .prologue
    .line 702
    if-nez p1, :cond_0

    .line 703
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 705
    :cond_0
    iput-object p1, p0, Lcom/google/android/c/b/cp;->b:[B

    .line 706
    iget v0, p0, Lcom/google/android/c/b/cp;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/cp;->a:I

    .line 707
    return-object p0
.end method

.method public final b([B)Lcom/google/android/c/b/cp;
    .locals 1

    .prologue
    .line 724
    if-nez p1, :cond_0

    .line 725
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 727
    :cond_0
    iput-object p1, p0, Lcom/google/android/c/b/cp;->c:[B

    .line 728
    iget v0, p0, Lcom/google/android/c/b/cp;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/cp;->a:I

    .line 729
    return-object p0
.end method

.method public final c([B)Lcom/google/android/c/b/cp;
    .locals 1

    .prologue
    .line 746
    if-nez p1, :cond_0

    .line 747
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 749
    :cond_0
    iput-object p1, p0, Lcom/google/android/c/b/cp;->d:[B

    .line 750
    iget v0, p0, Lcom/google/android/c/b/cp;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/c/b/cp;->a:I

    .line 751
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 818
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 819
    iget v1, p0, Lcom/google/android/c/b/cp;->a:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 820
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/c/b/cp;->b:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 823
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/cp;->a:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 824
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/c/b/cp;->c:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 827
    :cond_1
    iget v1, p0, Lcom/google/android/c/b/cp;->a:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 828
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/c/b/cp;->d:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 831
    :cond_2
    iget v1, p0, Lcom/google/android/c/b/cp;->a:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 832
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/c/b/cp;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 835
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 677
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/cp;->b:[B

    iget v0, p0, Lcom/google/android/c/b/cp;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/cp;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/cp;->c:[B

    iget v0, p0, Lcom/google/android/c/b/cp;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/cp;->a:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/cp;->d:[B

    iget v0, p0, Lcom/google/android/c/b/cp;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/c/b/cp;->a:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/cp;->e:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/cp;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/c/b/cp;->a:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 801
    iget v0, p0, Lcom/google/android/c/b/cp;->a:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 802
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/c/b/cp;->b:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 804
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/cp;->a:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 805
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/c/b/cp;->c:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 807
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/cp;->a:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 808
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/c/b/cp;->d:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 810
    :cond_2
    iget v0, p0, Lcom/google/android/c/b/cp;->a:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 811
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/c/b/cp;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 813
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 814
    return-void
.end method
