.class public final Lcom/google/android/c/b/af;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/android/c/b/af;


# instance fields
.field public a:I

.field public b:I

.field public c:Z

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9024
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 9025
    iput v0, p0, Lcom/google/android/c/b/af;->e:I

    iput v0, p0, Lcom/google/android/c/b/af;->a:I

    iput v0, p0, Lcom/google/android/c/b/af;->b:I

    iput-boolean v0, p0, Lcom/google/android/c/b/af;->c:Z

    iput v0, p0, Lcom/google/android/c/b/af;->f:I

    iput v0, p0, Lcom/google/android/c/b/af;->g:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/af;->cachedSize:I

    .line 9026
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/af;
    .locals 2

    .prologue
    .line 8964
    sget-object v0, Lcom/google/android/c/b/af;->d:[Lcom/google/android/c/b/af;

    if-nez v0, :cond_1

    .line 8965
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 8967
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/af;->d:[Lcom/google/android/c/b/af;

    if-nez v0, :cond_0

    .line 8968
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/af;

    sput-object v0, Lcom/google/android/c/b/af;->d:[Lcom/google/android/c/b/af;

    .line 8970
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8972
    :cond_1
    sget-object v0, Lcom/google/android/c/b/af;->d:[Lcom/google/android/c/b/af;

    return-object v0

    .line 8970
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final b()I
    .locals 1

    .prologue
    .line 8989
    iget v0, p0, Lcom/google/android/c/b/af;->f:I

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 8997
    iget v0, p0, Lcom/google/android/c/b/af;->e:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 9056
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 9057
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/af;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9059
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/af;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9061
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/c/b/af;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 9063
    iget v1, p0, Lcom/google/android/c/b/af;->e:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 9064
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/c/b/af;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9067
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/af;->e:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 9068
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/c/b/af;->g:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9071
    :cond_1
    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 9008
    iget v0, p0, Lcom/google/android/c/b/af;->g:I

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 9016
    iget v0, p0, Lcom/google/android/c/b/af;->e:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8958
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/af;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/af;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/af;->c:Z

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/af;->f:I

    iget v0, p0, Lcom/google/android/c/b/af;->e:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/af;->e:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/af;->g:I

    iget v0, p0, Lcom/google/android/c/b/af;->e:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/af;->e:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 9042
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/af;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 9043
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/af;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 9044
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/c/b/af;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 9045
    iget v0, p0, Lcom/google/android/c/b/af;->e:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 9046
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/c/b/af;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 9048
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/af;->e:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 9049
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/c/b/af;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 9051
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 9052
    return-void
.end method
