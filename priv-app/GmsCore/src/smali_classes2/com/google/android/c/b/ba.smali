.class public final Lcom/google/android/c/b/ba;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/String;

.field private d:I

.field private e:Z

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14002
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 14003
    iput v1, p0, Lcom/google/android/c/b/ba;->a:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/ba;->b:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/ba;->c:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/c/b/ba;->d:I

    iput-boolean v1, p0, Lcom/google/android/c/b/ba;->e:Z

    iput-boolean v1, p0, Lcom/google/android/c/b/ba;->f:Z

    iput-boolean v1, p0, Lcom/google/android/c/b/ba;->g:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/ba;->cachedSize:I

    .line 14004
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/c/b/ba;
    .locals 1

    .prologue
    .line 13891
    iput p1, p0, Lcom/google/android/c/b/ba;->b:I

    .line 13892
    iget v0, p0, Lcom/google/android/c/b/ba;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/ba;->a:I

    .line 13893
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/c/b/ba;
    .locals 1

    .prologue
    .line 13910
    if-nez p1, :cond_0

    .line 13911
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 13913
    :cond_0
    iput-object p1, p0, Lcom/google/android/c/b/ba;->c:Ljava/lang/String;

    .line 13914
    iget v0, p0, Lcom/google/android/c/b/ba;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/ba;->a:I

    .line 13915
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/c/b/ba;
    .locals 1

    .prologue
    .line 13951
    iput-boolean p1, p0, Lcom/google/android/c/b/ba;->e:Z

    .line 13952
    iget v0, p0, Lcom/google/android/c/b/ba;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/c/b/ba;->a:I

    .line 13953
    return-object p0
.end method

.method public final b(I)Lcom/google/android/c/b/ba;
    .locals 1

    .prologue
    .line 13932
    iput p1, p0, Lcom/google/android/c/b/ba;->d:I

    .line 13933
    iget v0, p0, Lcom/google/android/c/b/ba;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/c/b/ba;->a:I

    .line 13934
    return-object p0
.end method

.method public final b(Z)Lcom/google/android/c/b/ba;
    .locals 1

    .prologue
    .line 13970
    iput-boolean p1, p0, Lcom/google/android/c/b/ba;->f:Z

    .line 13971
    iget v0, p0, Lcom/google/android/c/b/ba;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/android/c/b/ba;->a:I

    .line 13972
    return-object p0
.end method

.method public final c(Z)Lcom/google/android/c/b/ba;
    .locals 1

    .prologue
    .line 13989
    iput-boolean p1, p0, Lcom/google/android/c/b/ba;->g:Z

    .line 13990
    iget v0, p0, Lcom/google/android/c/b/ba;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/android/c/b/ba;->a:I

    .line 13991
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 14044
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 14045
    iget v1, p0, Lcom/google/android/c/b/ba;->a:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 14046
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/ba;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14049
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/ba;->a:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 14050
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/c/b/ba;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14053
    :cond_1
    iget v1, p0, Lcom/google/android/c/b/ba;->a:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 14054
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/c/b/ba;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14057
    :cond_2
    iget v1, p0, Lcom/google/android/c/b/ba;->a:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 14058
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/c/b/ba;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 14061
    :cond_3
    iget v1, p0, Lcom/google/android/c/b/ba;->a:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 14062
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/c/b/ba;->f:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 14065
    :cond_4
    iget v1, p0, Lcom/google/android/c/b/ba;->a:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 14066
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/android/c/b/ba;->g:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 14069
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 13859
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/c/b/ba;->b:I

    iget v0, p0, Lcom/google/android/c/b/ba;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/ba;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/ba;->c:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/ba;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/ba;->a:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/ba;->d:I

    iget v0, p0, Lcom/google/android/c/b/ba;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/c/b/ba;->a:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/ba;->e:Z

    iget v0, p0, Lcom/google/android/c/b/ba;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/c/b/ba;->a:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/ba;->f:Z

    iget v0, p0, Lcom/google/android/c/b/ba;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/android/c/b/ba;->a:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/ba;->g:Z

    iget v0, p0, Lcom/google/android/c/b/ba;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/android/c/b/ba;->a:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 14021
    iget v0, p0, Lcom/google/android/c/b/ba;->a:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 14022
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/ba;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 14024
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/ba;->a:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 14025
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/c/b/ba;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 14027
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/ba;->a:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 14028
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/c/b/ba;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 14030
    :cond_2
    iget v0, p0, Lcom/google/android/c/b/ba;->a:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 14031
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/c/b/ba;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 14033
    :cond_3
    iget v0, p0, Lcom/google/android/c/b/ba;->a:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 14034
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/c/b/ba;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 14036
    :cond_4
    iget v0, p0, Lcom/google/android/c/b/ba;->a:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 14037
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/android/c/b/ba;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 14039
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 14040
    return-void
.end method
