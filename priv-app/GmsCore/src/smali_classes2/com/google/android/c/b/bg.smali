.class public final Lcom/google/android/c/b/bg;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/android/c/b/bg;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field private d:I

.field private e:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15178
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 15179
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/c/b/bg;->d:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/bg;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/bg;->b:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/c/b/bg;->e:[B

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/bg;->cachedSize:I

    .line 15180
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/bg;
    .locals 2

    .prologue
    .line 15137
    sget-object v0, Lcom/google/android/c/b/bg;->c:[Lcom/google/android/c/b/bg;

    if-nez v0, :cond_1

    .line 15138
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 15140
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/bg;->c:[Lcom/google/android/c/b/bg;

    if-nez v0, :cond_0

    .line 15141
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/bg;

    sput-object v0, Lcom/google/android/c/b/bg;->c:[Lcom/google/android/c/b/bg;

    .line 15143
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15145
    :cond_1
    sget-object v0, Lcom/google/android/c/b/bg;->c:[Lcom/google/android/c/b/bg;

    return-object v0

    .line 15143
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a([B)Lcom/google/android/c/b/bg;
    .locals 1

    .prologue
    .line 15162
    if-nez p1, :cond_0

    .line 15163
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 15165
    :cond_0
    iput-object p1, p0, Lcom/google/android/c/b/bg;->e:[B

    .line 15166
    iget v0, p0, Lcom/google/android/c/b/bg;->d:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bg;->d:I

    .line 15167
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 15204
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 15205
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/c/b/bg;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15207
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/c/b/bg;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15209
    iget v1, p0, Lcom/google/android/c/b/bg;->d:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 15210
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/c/b/bg;->e:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 15213
    :cond_0
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 15131
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/bg;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/bg;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/bg;->e:[B

    iget v0, p0, Lcom/google/android/c/b/bg;->d:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bg;->d:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 15194
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/c/b/bg;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 15195
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/c/b/bg;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 15196
    iget v0, p0, Lcom/google/android/c/b/bg;->d:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 15197
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/c/b/bg;->e:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 15199
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 15200
    return-void
.end method
