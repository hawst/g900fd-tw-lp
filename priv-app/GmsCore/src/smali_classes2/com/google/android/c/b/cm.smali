.class public final Lcom/google/android/c/b/cm;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/android/c/b/cn;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1806
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1807
    invoke-static {}, Lcom/google/android/c/b/cn;->a()[Lcom/google/android/c/b/cn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/cm;->a:[Lcom/google/android/c/b/cn;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/cm;->cachedSize:I

    .line 1808
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 1832
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v1

    .line 1833
    iget-object v0, p0, Lcom/google/android/c/b/cm;->a:[Lcom/google/android/c/b/cn;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/c/b/cm;->a:[Lcom/google/android/c/b/cn;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 1834
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/c/b/cm;->a:[Lcom/google/android/c/b/cn;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1835
    iget-object v2, p0, Lcom/google/android/c/b/cm;->a:[Lcom/google/android/c/b/cn;

    aget-object v2, v2, v0

    .line 1836
    if-eqz v2, :cond_0

    .line 1837
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1834
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1842
    :cond_1
    return v1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1681
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/c/b/cm;->a:[Lcom/google/android/c/b/cn;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/c/b/cn;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/c/b/cm;->a:[Lcom/google/android/c/b/cn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/c/b/cn;

    invoke-direct {v3}, Lcom/google/android/c/b/cn;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/c/b/cm;->a:[Lcom/google/android/c/b/cn;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/c/b/cn;

    invoke-direct {v3}, Lcom/google/android/c/b/cn;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/c/b/cm;->a:[Lcom/google/android/c/b/cn;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 1819
    iget-object v0, p0, Lcom/google/android/c/b/cm;->a:[Lcom/google/android/c/b/cn;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/c/b/cm;->a:[Lcom/google/android/c/b/cn;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 1820
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/c/b/cm;->a:[Lcom/google/android/c/b/cn;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1821
    iget-object v1, p0, Lcom/google/android/c/b/cm;->a:[Lcom/google/android/c/b/cn;

    aget-object v1, v1, v0

    .line 1822
    if-eqz v1, :cond_0

    .line 1823
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1820
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1827
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1828
    return-void
.end method
