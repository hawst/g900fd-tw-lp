.class public final Lcom/google/android/c/b/l;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12133
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 12134
    iput v0, p0, Lcom/google/android/c/b/l;->a:I

    iput-boolean v0, p0, Lcom/google/android/c/b/l;->b:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/l;->cachedSize:I

    .line 12135
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 12154
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 12155
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/l;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12157
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/c/b/l;->b:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12159
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 12110
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_2
    iput v0, p0, Lcom/google/android/c/b/l;->a:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/l;->b:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_3
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0xff -> :sswitch_2
        -0xfe -> :sswitch_2
        -0xfd -> :sswitch_2
        -0xfb -> :sswitch_2
        -0x14 -> :sswitch_2
        -0x13 -> :sswitch_2
        -0x12 -> :sswitch_2
        -0x11 -> :sswitch_2
        -0x10 -> :sswitch_2
        -0xf -> :sswitch_2
        -0xe -> :sswitch_2
        -0xd -> :sswitch_2
        -0xc -> :sswitch_2
        -0xb -> :sswitch_2
        -0xa -> :sswitch_2
        -0x9 -> :sswitch_2
        -0x8 -> :sswitch_2
        -0x7 -> :sswitch_2
        -0x6 -> :sswitch_2
        -0x5 -> :sswitch_2
        -0x4 -> :sswitch_2
        -0x3 -> :sswitch_2
        -0x2 -> :sswitch_2
        -0x1 -> :sswitch_2
        0x0 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 12147
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/l;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 12148
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/c/b/l;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 12149
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 12150
    return-void
.end method
