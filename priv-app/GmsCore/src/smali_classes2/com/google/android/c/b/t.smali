.class public final Lcom/google/android/c/b/t;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:[I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9569
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 9570
    iput v1, p0, Lcom/google/android/c/b/t;->c:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/t;->a:I

    iput v1, p0, Lcom/google/android/c/b/t;->d:I

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/android/c/b/t;->b:[I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/t;->cachedSize:I

    .line 9571
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 9550
    iget v0, p0, Lcom/google/android/c/b/t;->d:I

    return v0
.end method

.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 9599
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 9600
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/c/b/t;->a:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 9602
    iget v2, p0, Lcom/google/android/c/b/t;->c:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 9603
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/c/b/t;->d:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 9606
    :cond_0
    iget-object v2, p0, Lcom/google/android/c/b/t;->b:[I

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/c/b/t;->b:[I

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v1

    .line 9608
    :goto_0
    iget-object v3, p0, Lcom/google/android/c/b/t;->b:[I

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 9609
    iget-object v3, p0, Lcom/google/android/c/b/t;->b:[I

    aget v3, v3, v1

    .line 9610
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->e(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 9608
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 9613
    :cond_1
    add-int/2addr v0, v2

    .line 9614
    iget-object v1, p0, Lcom/google/android/c/b/t;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 9616
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 9519
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/c/b/t;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/t;->d:I

    iget v0, p0, Lcom/google/android/c/b/t;->c:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/t;->c:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/c/b/t;->b:[I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/c/b/t;->b:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/c/b/t;->b:[I

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Lcom/google/android/c/b/t;->b:[I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v2, p0, Lcom/google/android/c/b/t;->b:[I

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_5

    iget-object v4, p0, Lcom/google/android/c/b/t;->b:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Lcom/google/android/c/b/t;->b:[I

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Lcom/google/android/c/b/t;->b:[I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 9585
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/t;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 9586
    iget v0, p0, Lcom/google/android/c/b/t;->c:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 9587
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/t;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 9589
    :cond_0
    iget-object v0, p0, Lcom/google/android/c/b/t;->b:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/c/b/t;->b:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 9590
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/c/b/t;->b:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 9591
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/c/b/t;->b:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 9590
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 9594
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 9595
    return-void
.end method
