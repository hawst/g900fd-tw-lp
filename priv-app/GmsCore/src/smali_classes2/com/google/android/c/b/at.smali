.class public final Lcom/google/android/c/b/at;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/android/c/b/at;


# instance fields
.field public a:J

.field public b:I

.field public c:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 6300
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 6301
    iput v2, p0, Lcom/google/android/c/b/at;->e:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/c/b/at;->a:J

    iput v2, p0, Lcom/google/android/c/b/at;->b:I

    iput v2, p0, Lcom/google/android/c/b/at;->c:I

    iput v2, p0, Lcom/google/android/c/b/at;->f:I

    iput v2, p0, Lcom/google/android/c/b/at;->g:I

    iput v2, p0, Lcom/google/android/c/b/at;->h:I

    iput v2, p0, Lcom/google/android/c/b/at;->i:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/at;->cachedSize:I

    .line 6302
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/at;
    .locals 2

    .prologue
    .line 6202
    sget-object v0, Lcom/google/android/c/b/at;->d:[Lcom/google/android/c/b/at;

    if-nez v0, :cond_1

    .line 6203
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 6205
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/at;->d:[Lcom/google/android/c/b/at;

    if-nez v0, :cond_0

    .line 6206
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/at;

    sput-object v0, Lcom/google/android/c/b/at;->d:[Lcom/google/android/c/b/at;

    .line 6208
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6210
    :cond_1
    sget-object v0, Lcom/google/android/c/b/at;->d:[Lcom/google/android/c/b/at;

    return-object v0

    .line 6208
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final b()I
    .locals 1

    .prologue
    .line 6227
    iget v0, p0, Lcom/google/android/c/b/at;->f:I

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 6235
    iget v0, p0, Lcom/google/android/c/b/at;->e:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 6340
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 6341
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/c/b/at;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 6343
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/at;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6345
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/c/b/at;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6347
    iget v1, p0, Lcom/google/android/c/b/at;->e:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 6348
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/c/b/at;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6351
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/at;->e:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 6352
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/c/b/at;->g:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6355
    :cond_1
    iget v1, p0, Lcom/google/android/c/b/at;->e:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 6356
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/c/b/at;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6359
    :cond_2
    iget v1, p0, Lcom/google/android/c/b/at;->e:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 6360
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/c/b/at;->i:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6363
    :cond_3
    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 6246
    iget v0, p0, Lcom/google/android/c/b/at;->g:I

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 6254
    iget v0, p0, Lcom/google/android/c/b/at;->e:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 6265
    iget v0, p0, Lcom/google/android/c/b/at;->h:I

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 6273
    iget v0, p0, Lcom/google/android/c/b/at;->e:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 6284
    iget v0, p0, Lcom/google/android/c/b/at;->i:I

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 6292
    iget v0, p0, Lcom/google/android/c/b/at;->e:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 6196
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/c/b/at;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/at;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/at;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/at;->f:I

    iget v0, p0, Lcom/google/android/c/b/at;->e:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/at;->e:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/at;->g:I

    iget v0, p0, Lcom/google/android/c/b/at;->e:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/at;->e:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/at;->h:I

    iget v0, p0, Lcom/google/android/c/b/at;->e:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/c/b/at;->e:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/at;->i:I

    iget v0, p0, Lcom/google/android/c/b/at;->e:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/c/b/at;->e:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 6320
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/c/b/at;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 6321
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/at;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 6322
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/c/b/at;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 6323
    iget v0, p0, Lcom/google/android/c/b/at;->e:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 6324
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/c/b/at;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 6326
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/at;->e:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 6327
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/c/b/at;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 6329
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/at;->e:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 6330
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/c/b/at;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 6332
    :cond_2
    iget v0, p0, Lcom/google/android/c/b/at;->e:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 6333
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/android/c/b/at;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 6335
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 6336
    return-void
.end method
