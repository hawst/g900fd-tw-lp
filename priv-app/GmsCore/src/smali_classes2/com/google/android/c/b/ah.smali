.class public final Lcom/google/android/c/b/ah;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile a:[Lcom/google/android/c/b/ah;


# instance fields
.field private b:I

.field private c:I

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8866
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 8867
    iput v0, p0, Lcom/google/android/c/b/ah;->b:I

    iput v0, p0, Lcom/google/android/c/b/ah;->c:I

    iput v0, p0, Lcom/google/android/c/b/ah;->d:I

    iput v0, p0, Lcom/google/android/c/b/ah;->e:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/ah;->cachedSize:I

    .line 8868
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/ah;
    .locals 2

    .prologue
    .line 8796
    sget-object v0, Lcom/google/android/c/b/ah;->a:[Lcom/google/android/c/b/ah;

    if-nez v0, :cond_1

    .line 8797
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 8799
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/ah;->a:[Lcom/google/android/c/b/ah;

    if-nez v0, :cond_0

    .line 8800
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/ah;

    sput-object v0, Lcom/google/android/c/b/ah;->a:[Lcom/google/android/c/b/ah;

    .line 8802
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8804
    :cond_1
    sget-object v0, Lcom/google/android/c/b/ah;->a:[Lcom/google/android/c/b/ah;

    return-object v0

    .line 8802
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final b()I
    .locals 1

    .prologue
    .line 8812
    iget v0, p0, Lcom/google/android/c/b/ah;->c:I

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 8820
    iget v0, p0, Lcom/google/android/c/b/ah;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 8896
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 8897
    iget v1, p0, Lcom/google/android/c/b/ah;->b:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 8898
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/ah;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8901
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/ah;->b:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 8902
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/ah;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8905
    :cond_1
    iget v1, p0, Lcom/google/android/c/b/ah;->b:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 8906
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/c/b/ah;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8909
    :cond_2
    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 8831
    iget v0, p0, Lcom/google/android/c/b/ah;->d:I

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 8839
    iget v0, p0, Lcom/google/android/c/b/ah;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 8850
    iget v0, p0, Lcom/google/android/c/b/ah;->e:I

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 8858
    iget v0, p0, Lcom/google/android/c/b/ah;->b:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8790
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/ah;->c:I

    iget v0, p0, Lcom/google/android/c/b/ah;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/ah;->b:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/ah;->d:I

    iget v0, p0, Lcom/google/android/c/b/ah;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/ah;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/ah;->e:I

    iget v0, p0, Lcom/google/android/c/b/ah;->b:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/c/b/ah;->b:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 8882
    iget v0, p0, Lcom/google/android/c/b/ah;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 8883
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/ah;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8885
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/ah;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 8886
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/ah;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8888
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/ah;->b:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 8889
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/c/b/ah;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8891
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 8892
    return-void
.end method
