.class public final Lcom/google/android/c/b/bu;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile a:[Lcom/google/android/c/b/bu;


# instance fields
.field private b:I

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7405
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 7406
    iput v0, p0, Lcom/google/android/c/b/bu;->b:I

    iput-boolean v0, p0, Lcom/google/android/c/b/bu;->c:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/bu;->cachedSize:I

    .line 7407
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/bu;
    .locals 2

    .prologue
    .line 7373
    sget-object v0, Lcom/google/android/c/b/bu;->a:[Lcom/google/android/c/b/bu;

    if-nez v0, :cond_1

    .line 7374
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 7376
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/bu;->a:[Lcom/google/android/c/b/bu;

    if-nez v0, :cond_0

    .line 7377
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/bu;

    sput-object v0, Lcom/google/android/c/b/bu;->a:[Lcom/google/android/c/b/bu;

    .line 7379
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7381
    :cond_1
    sget-object v0, Lcom/google/android/c/b/bu;->a:[Lcom/google/android/c/b/bu;

    return-object v0

    .line 7379
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final b()Z
    .locals 1

    .prologue
    .line 7389
    iget-boolean v0, p0, Lcom/google/android/c/b/bu;->c:Z

    return v0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 7427
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 7428
    iget v1, p0, Lcom/google/android/c/b/bu;->b:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 7429
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/c/b/bu;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7432
    :cond_0
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 7367
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/bu;->c:Z

    iget v0, p0, Lcom/google/android/c/b/bu;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bu;->b:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 7419
    iget v0, p0, Lcom/google/android/c/b/bu;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 7420
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/c/b/bu;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 7422
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 7423
    return-void
.end method
