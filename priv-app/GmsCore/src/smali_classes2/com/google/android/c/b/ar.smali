.class public final Lcom/google/android/c/b/ar;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/android/c/b/ar;


# instance fields
.field public a:I

.field public b:Z

.field public c:I

.field private e:I

.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11124
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 11125
    iput v0, p0, Lcom/google/android/c/b/ar;->e:I

    iput v0, p0, Lcom/google/android/c/b/ar;->a:I

    iput-boolean v0, p0, Lcom/google/android/c/b/ar;->b:Z

    iput v0, p0, Lcom/google/android/c/b/ar;->c:I

    iput-boolean v0, p0, Lcom/google/android/c/b/ar;->f:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/ar;->cachedSize:I

    .line 11126
    return-void
.end method

.method public static a([B)Lcom/google/android/c/b/ar;
    .locals 1

    .prologue
    .line 11204
    new-instance v0, Lcom/google/android/c/b/ar;

    invoke-direct {v0}, Lcom/google/android/c/b/ar;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/c/b/ar;

    return-object v0
.end method

.method public static a()[Lcom/google/android/c/b/ar;
    .locals 2

    .prologue
    .line 11083
    sget-object v0, Lcom/google/android/c/b/ar;->d:[Lcom/google/android/c/b/ar;

    if-nez v0, :cond_1

    .line 11084
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 11086
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/ar;->d:[Lcom/google/android/c/b/ar;

    if-nez v0, :cond_0

    .line 11087
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/ar;

    sput-object v0, Lcom/google/android/c/b/ar;->d:[Lcom/google/android/c/b/ar;

    .line 11089
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 11091
    :cond_1
    sget-object v0, Lcom/google/android/c/b/ar;->d:[Lcom/google/android/c/b/ar;

    return-object v0

    .line 11089
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final b()Z
    .locals 1

    .prologue
    .line 11108
    iget-boolean v0, p0, Lcom/google/android/c/b/ar;->f:Z

    return v0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 11152
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 11153
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/ar;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11155
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/c/b/ar;->b:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 11157
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/c/b/ar;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11159
    iget v1, p0, Lcom/google/android/c/b/ar;->e:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 11160
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/c/b/ar;->f:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 11163
    :cond_0
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 11077
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/ar;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/ar;->b:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/ar;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/ar;->f:Z

    iget v0, p0, Lcom/google/android/c/b/ar;->e:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/ar;->e:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 11141
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/ar;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 11142
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/c/b/ar;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 11143
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/c/b/ar;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 11144
    iget v0, p0, Lcom/google/android/c/b/ar;->e:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 11145
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/c/b/ar;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 11147
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 11148
    return-void
.end method
