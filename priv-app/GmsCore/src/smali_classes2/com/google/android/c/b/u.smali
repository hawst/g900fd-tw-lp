.class public final Lcom/google/android/c/b/u;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile b:[Lcom/google/android/c/b/u;


# instance fields
.field public a:[I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7896
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 7897
    iput v0, p0, Lcom/google/android/c/b/u;->c:I

    iput v0, p0, Lcom/google/android/c/b/u;->d:I

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/android/c/b/u;->a:[I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/u;->cachedSize:I

    .line 7898
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/u;
    .locals 2

    .prologue
    .line 7861
    sget-object v0, Lcom/google/android/c/b/u;->b:[Lcom/google/android/c/b/u;

    if-nez v0, :cond_1

    .line 7862
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 7864
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/u;->b:[Lcom/google/android/c/b/u;

    if-nez v0, :cond_0

    .line 7865
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/u;

    sput-object v0, Lcom/google/android/c/b/u;->b:[Lcom/google/android/c/b/u;

    .line 7867
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7869
    :cond_1
    sget-object v0, Lcom/google/android/c/b/u;->b:[Lcom/google/android/c/b/u;

    return-object v0

    .line 7867
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 7924
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 7925
    iget v2, p0, Lcom/google/android/c/b/u;->c:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 7926
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/c/b/u;->d:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 7929
    :cond_0
    iget-object v2, p0, Lcom/google/android/c/b/u;->a:[I

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/c/b/u;->a:[I

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v1

    .line 7931
    :goto_0
    iget-object v3, p0, Lcom/google/android/c/b/u;->a:[I

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 7932
    iget-object v3, p0, Lcom/google/android/c/b/u;->a:[I

    aget v3, v3, v1

    .line 7933
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 7931
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 7936
    :cond_1
    add-int/2addr v0, v2

    .line 7937
    iget-object v1, p0, Lcom/google/android/c/b/u;->a:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7939
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 7855
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/u;->d:I

    iget v0, p0, Lcom/google/android/c/b/u;->c:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/u;->c:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/c/b/u;->a:[I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/c/b/u;->a:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/c/b/u;->a:[I

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Lcom/google/android/c/b/u;->a:[I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v2, p0, Lcom/google/android/c/b/u;->a:[I

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_5

    iget-object v4, p0, Lcom/google/android/c/b/u;->a:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Lcom/google/android/c/b/u;->a:[I

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Lcom/google/android/c/b/u;->a:[I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 7911
    iget v0, p0, Lcom/google/android/c/b/u;->c:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 7912
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/u;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7914
    :cond_0
    iget-object v0, p0, Lcom/google/android/c/b/u;->a:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/c/b/u;->a:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 7915
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/c/b/u;->a:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 7916
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/c/b/u;->a:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7915
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7919
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 7920
    return-void
.end method
