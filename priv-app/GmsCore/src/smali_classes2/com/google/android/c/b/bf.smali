.class public final Lcom/google/android/c/b/bf;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/c/b/be;

.field private b:I

.field private c:[B

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 16059
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 16060
    iput v1, p0, Lcom/google/android/c/b/bf;->b:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/c/b/bf;->a:Lcom/google/android/c/b/be;

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/c/b/bf;->c:[B

    iput v1, p0, Lcom/google/android/c/b/bf;->d:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/bf;->cachedSize:I

    .line 16061
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/c/b/bf;
    .locals 1

    .prologue
    .line 16046
    iput p1, p0, Lcom/google/android/c/b/bf;->d:I

    .line 16047
    iget v0, p0, Lcom/google/android/c/b/bf;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/bf;->b:I

    .line 16048
    return-object p0
.end method

.method public final a([B)Lcom/google/android/c/b/bf;
    .locals 1

    .prologue
    .line 16024
    if-nez p1, :cond_0

    .line 16025
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 16027
    :cond_0
    iput-object p1, p0, Lcom/google/android/c/b/bf;->c:[B

    .line 16028
    iget v0, p0, Lcom/google/android/c/b/bf;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bf;->b:I

    .line 16029
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 16089
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 16090
    iget-object v1, p0, Lcom/google/android/c/b/bf;->a:Lcom/google/android/c/b/be;

    if-eqz v1, :cond_0

    .line 16091
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/c/b/bf;->a:Lcom/google/android/c/b/be;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16094
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/bf;->b:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 16095
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/c/b/bf;->c:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 16098
    :cond_1
    iget v1, p0, Lcom/google/android/c/b/bf;->b:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 16099
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/c/b/bf;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 16102
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 15996
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/c/b/bf;->a:Lcom/google/android/c/b/be;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/c/b/be;

    invoke-direct {v0}, Lcom/google/android/c/b/be;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/bf;->a:Lcom/google/android/c/b/be;

    :cond_1
    iget-object v0, p0, Lcom/google/android/c/b/bf;->a:Lcom/google/android/c/b/be;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/bf;->c:[B

    iget v0, p0, Lcom/google/android/c/b/bf;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bf;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/bf;->d:I

    iget v0, p0, Lcom/google/android/c/b/bf;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/bf;->b:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 16075
    iget-object v0, p0, Lcom/google/android/c/b/bf;->a:Lcom/google/android/c/b/be;

    if-eqz v0, :cond_0

    .line 16076
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/c/b/bf;->a:Lcom/google/android/c/b/be;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 16078
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/bf;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 16079
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/c/b/bf;->c:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 16081
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/bf;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 16082
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/c/b/bf;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 16084
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 16085
    return-void
.end method
