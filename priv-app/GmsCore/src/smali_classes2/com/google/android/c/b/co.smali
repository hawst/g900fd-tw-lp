.class public final Lcom/google/android/c/b/co;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile n:[Lcom/google/android/c/b/co;


# instance fields
.field public a:I

.field public b:Lcom/google/android/c/b/cm;

.field public c:Lcom/google/android/c/b/bd;

.field public d:Lcom/google/android/c/b/ak;

.field public e:Lcom/google/android/c/b/bi;

.field public f:Lcom/google/android/c/b/m;

.field public g:Lcom/google/android/c/b/ce;

.field public h:Lcom/google/android/c/b/bq;

.field public i:Lcom/google/android/c/b/bb;

.field public j:Lcom/google/android/c/b/cb;

.field public k:Lcom/google/android/c/b/av;

.field public l:Lcom/google/android/c/b/cy;

.field public m:Lcom/google/android/c/b/ad;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1441
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1442
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/c/b/co;->a:I

    iput-object v1, p0, Lcom/google/android/c/b/co;->b:Lcom/google/android/c/b/cm;

    iput-object v1, p0, Lcom/google/android/c/b/co;->c:Lcom/google/android/c/b/bd;

    iput-object v1, p0, Lcom/google/android/c/b/co;->d:Lcom/google/android/c/b/ak;

    iput-object v1, p0, Lcom/google/android/c/b/co;->e:Lcom/google/android/c/b/bi;

    iput-object v1, p0, Lcom/google/android/c/b/co;->f:Lcom/google/android/c/b/m;

    iput-object v1, p0, Lcom/google/android/c/b/co;->g:Lcom/google/android/c/b/ce;

    iput-object v1, p0, Lcom/google/android/c/b/co;->h:Lcom/google/android/c/b/bq;

    iput-object v1, p0, Lcom/google/android/c/b/co;->i:Lcom/google/android/c/b/bb;

    iput-object v1, p0, Lcom/google/android/c/b/co;->j:Lcom/google/android/c/b/cb;

    iput-object v1, p0, Lcom/google/android/c/b/co;->k:Lcom/google/android/c/b/av;

    iput-object v1, p0, Lcom/google/android/c/b/co;->l:Lcom/google/android/c/b/cy;

    iput-object v1, p0, Lcom/google/android/c/b/co;->m:Lcom/google/android/c/b/ad;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/co;->cachedSize:I

    .line 1443
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/co;
    .locals 2

    .prologue
    .line 1391
    sget-object v0, Lcom/google/android/c/b/co;->n:[Lcom/google/android/c/b/co;

    if-nez v0, :cond_1

    .line 1392
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1394
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/co;->n:[Lcom/google/android/c/b/co;

    if-nez v0, :cond_0

    .line 1395
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/co;

    sput-object v0, Lcom/google/android/c/b/co;->n:[Lcom/google/android/c/b/co;

    .line 1397
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1399
    :cond_1
    sget-object v0, Lcom/google/android/c/b/co;->n:[Lcom/google/android/c/b/co;

    return-object v0

    .line 1397
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1508
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1509
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/co;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1511
    iget-object v1, p0, Lcom/google/android/c/b/co;->b:Lcom/google/android/c/b/cm;

    if-eqz v1, :cond_0

    .line 1512
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/c/b/co;->b:Lcom/google/android/c/b/cm;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1515
    :cond_0
    iget-object v1, p0, Lcom/google/android/c/b/co;->c:Lcom/google/android/c/b/bd;

    if-eqz v1, :cond_1

    .line 1516
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/c/b/co;->c:Lcom/google/android/c/b/bd;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1519
    :cond_1
    iget-object v1, p0, Lcom/google/android/c/b/co;->d:Lcom/google/android/c/b/ak;

    if-eqz v1, :cond_2

    .line 1520
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/c/b/co;->d:Lcom/google/android/c/b/ak;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1523
    :cond_2
    iget-object v1, p0, Lcom/google/android/c/b/co;->e:Lcom/google/android/c/b/bi;

    if-eqz v1, :cond_3

    .line 1524
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/c/b/co;->e:Lcom/google/android/c/b/bi;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1527
    :cond_3
    iget-object v1, p0, Lcom/google/android/c/b/co;->f:Lcom/google/android/c/b/m;

    if-eqz v1, :cond_4

    .line 1528
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/c/b/co;->f:Lcom/google/android/c/b/m;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1531
    :cond_4
    iget-object v1, p0, Lcom/google/android/c/b/co;->g:Lcom/google/android/c/b/ce;

    if-eqz v1, :cond_5

    .line 1532
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/c/b/co;->g:Lcom/google/android/c/b/ce;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1535
    :cond_5
    iget-object v1, p0, Lcom/google/android/c/b/co;->h:Lcom/google/android/c/b/bq;

    if-eqz v1, :cond_6

    .line 1536
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/c/b/co;->h:Lcom/google/android/c/b/bq;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1539
    :cond_6
    iget-object v1, p0, Lcom/google/android/c/b/co;->i:Lcom/google/android/c/b/bb;

    if-eqz v1, :cond_7

    .line 1540
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/c/b/co;->i:Lcom/google/android/c/b/bb;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1543
    :cond_7
    iget-object v1, p0, Lcom/google/android/c/b/co;->j:Lcom/google/android/c/b/cb;

    if-eqz v1, :cond_8

    .line 1544
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/c/b/co;->j:Lcom/google/android/c/b/cb;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1547
    :cond_8
    iget-object v1, p0, Lcom/google/android/c/b/co;->k:Lcom/google/android/c/b/av;

    if-eqz v1, :cond_9

    .line 1548
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/c/b/co;->k:Lcom/google/android/c/b/av;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1551
    :cond_9
    iget-object v1, p0, Lcom/google/android/c/b/co;->l:Lcom/google/android/c/b/cy;

    if-eqz v1, :cond_a

    .line 1552
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/c/b/co;->l:Lcom/google/android/c/b/cy;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1555
    :cond_a
    iget-object v1, p0, Lcom/google/android/c/b/co;->m:Lcom/google/android/c/b/ad;

    if-eqz v1, :cond_b

    .line 1556
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/c/b/co;->m:Lcom/google/android/c/b/ad;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1559
    :cond_b
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1385
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/co;->a:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/c/b/co;->b:Lcom/google/android/c/b/cm;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/c/b/cm;

    invoke-direct {v0}, Lcom/google/android/c/b/cm;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/co;->b:Lcom/google/android/c/b/cm;

    :cond_1
    iget-object v0, p0, Lcom/google/android/c/b/co;->b:Lcom/google/android/c/b/cm;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/c/b/co;->c:Lcom/google/android/c/b/bd;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/c/b/bd;

    invoke-direct {v0}, Lcom/google/android/c/b/bd;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/co;->c:Lcom/google/android/c/b/bd;

    :cond_2
    iget-object v0, p0, Lcom/google/android/c/b/co;->c:Lcom/google/android/c/b/bd;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/c/b/co;->d:Lcom/google/android/c/b/ak;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/c/b/ak;

    invoke-direct {v0}, Lcom/google/android/c/b/ak;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/co;->d:Lcom/google/android/c/b/ak;

    :cond_3
    iget-object v0, p0, Lcom/google/android/c/b/co;->d:Lcom/google/android/c/b/ak;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/c/b/co;->e:Lcom/google/android/c/b/bi;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/c/b/bi;

    invoke-direct {v0}, Lcom/google/android/c/b/bi;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/co;->e:Lcom/google/android/c/b/bi;

    :cond_4
    iget-object v0, p0, Lcom/google/android/c/b/co;->e:Lcom/google/android/c/b/bi;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/android/c/b/co;->f:Lcom/google/android/c/b/m;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/c/b/m;

    invoke-direct {v0}, Lcom/google/android/c/b/m;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/co;->f:Lcom/google/android/c/b/m;

    :cond_5
    iget-object v0, p0, Lcom/google/android/c/b/co;->f:Lcom/google/android/c/b/m;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/android/c/b/co;->g:Lcom/google/android/c/b/ce;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/android/c/b/ce;

    invoke-direct {v0}, Lcom/google/android/c/b/ce;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/co;->g:Lcom/google/android/c/b/ce;

    :cond_6
    iget-object v0, p0, Lcom/google/android/c/b/co;->g:Lcom/google/android/c/b/ce;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/android/c/b/co;->h:Lcom/google/android/c/b/bq;

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/android/c/b/bq;

    invoke-direct {v0}, Lcom/google/android/c/b/bq;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/co;->h:Lcom/google/android/c/b/bq;

    :cond_7
    iget-object v0, p0, Lcom/google/android/c/b/co;->h:Lcom/google/android/c/b/bq;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/android/c/b/co;->i:Lcom/google/android/c/b/bb;

    if-nez v0, :cond_8

    new-instance v0, Lcom/google/android/c/b/bb;

    invoke-direct {v0}, Lcom/google/android/c/b/bb;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/co;->i:Lcom/google/android/c/b/bb;

    :cond_8
    iget-object v0, p0, Lcom/google/android/c/b/co;->i:Lcom/google/android/c/b/bb;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/google/android/c/b/co;->j:Lcom/google/android/c/b/cb;

    if-nez v0, :cond_9

    new-instance v0, Lcom/google/android/c/b/cb;

    invoke-direct {v0}, Lcom/google/android/c/b/cb;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/co;->j:Lcom/google/android/c/b/cb;

    :cond_9
    iget-object v0, p0, Lcom/google/android/c/b/co;->j:Lcom/google/android/c/b/cb;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lcom/google/android/c/b/co;->k:Lcom/google/android/c/b/av;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/android/c/b/av;

    invoke-direct {v0}, Lcom/google/android/c/b/av;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/co;->k:Lcom/google/android/c/b/av;

    :cond_a
    iget-object v0, p0, Lcom/google/android/c/b/co;->k:Lcom/google/android/c/b/av;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lcom/google/android/c/b/co;->l:Lcom/google/android/c/b/cy;

    if-nez v0, :cond_b

    new-instance v0, Lcom/google/android/c/b/cy;

    invoke-direct {v0}, Lcom/google/android/c/b/cy;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/co;->l:Lcom/google/android/c/b/cy;

    :cond_b
    iget-object v0, p0, Lcom/google/android/c/b/co;->l:Lcom/google/android/c/b/cy;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lcom/google/android/c/b/co;->m:Lcom/google/android/c/b/ad;

    if-nez v0, :cond_c

    new-instance v0, Lcom/google/android/c/b/ad;

    invoke-direct {v0}, Lcom/google/android/c/b/ad;-><init>()V

    iput-object v0, p0, Lcom/google/android/c/b/co;->m:Lcom/google/android/c/b/ad;

    :cond_c
    iget-object v0, p0, Lcom/google/android/c/b/co;->m:Lcom/google/android/c/b/ad;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1466
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/co;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1467
    iget-object v0, p0, Lcom/google/android/c/b/co;->b:Lcom/google/android/c/b/cm;

    if-eqz v0, :cond_0

    .line 1468
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/c/b/co;->b:Lcom/google/android/c/b/cm;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1470
    :cond_0
    iget-object v0, p0, Lcom/google/android/c/b/co;->c:Lcom/google/android/c/b/bd;

    if-eqz v0, :cond_1

    .line 1471
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/c/b/co;->c:Lcom/google/android/c/b/bd;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1473
    :cond_1
    iget-object v0, p0, Lcom/google/android/c/b/co;->d:Lcom/google/android/c/b/ak;

    if-eqz v0, :cond_2

    .line 1474
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/c/b/co;->d:Lcom/google/android/c/b/ak;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1476
    :cond_2
    iget-object v0, p0, Lcom/google/android/c/b/co;->e:Lcom/google/android/c/b/bi;

    if-eqz v0, :cond_3

    .line 1477
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/c/b/co;->e:Lcom/google/android/c/b/bi;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1479
    :cond_3
    iget-object v0, p0, Lcom/google/android/c/b/co;->f:Lcom/google/android/c/b/m;

    if-eqz v0, :cond_4

    .line 1480
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/c/b/co;->f:Lcom/google/android/c/b/m;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1482
    :cond_4
    iget-object v0, p0, Lcom/google/android/c/b/co;->g:Lcom/google/android/c/b/ce;

    if-eqz v0, :cond_5

    .line 1483
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/c/b/co;->g:Lcom/google/android/c/b/ce;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1485
    :cond_5
    iget-object v0, p0, Lcom/google/android/c/b/co;->h:Lcom/google/android/c/b/bq;

    if-eqz v0, :cond_6

    .line 1486
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/c/b/co;->h:Lcom/google/android/c/b/bq;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1488
    :cond_6
    iget-object v0, p0, Lcom/google/android/c/b/co;->i:Lcom/google/android/c/b/bb;

    if-eqz v0, :cond_7

    .line 1489
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/c/b/co;->i:Lcom/google/android/c/b/bb;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1491
    :cond_7
    iget-object v0, p0, Lcom/google/android/c/b/co;->j:Lcom/google/android/c/b/cb;

    if-eqz v0, :cond_8

    .line 1492
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/c/b/co;->j:Lcom/google/android/c/b/cb;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1494
    :cond_8
    iget-object v0, p0, Lcom/google/android/c/b/co;->k:Lcom/google/android/c/b/av;

    if-eqz v0, :cond_9

    .line 1495
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/c/b/co;->k:Lcom/google/android/c/b/av;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1497
    :cond_9
    iget-object v0, p0, Lcom/google/android/c/b/co;->l:Lcom/google/android/c/b/cy;

    if-eqz v0, :cond_a

    .line 1498
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/c/b/co;->l:Lcom/google/android/c/b/cy;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1500
    :cond_a
    iget-object v0, p0, Lcom/google/android/c/b/co;->m:Lcom/google/android/c/b/ad;

    if-eqz v0, :cond_b

    .line 1501
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/c/b/co;->m:Lcom/google/android/c/b/ad;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1503
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1504
    return-void
.end method
