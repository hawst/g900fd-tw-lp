.class public final Lcom/google/android/c/b/da;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10058
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 10059
    iput v1, p0, Lcom/google/android/c/b/da;->a:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/da;->b:I

    iput-boolean v1, p0, Lcom/google/android/c/b/da;->c:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/da;->cachedSize:I

    .line 10060
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 10023
    iget v0, p0, Lcom/google/android/c/b/da;->b:I

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 10031
    iget v0, p0, Lcom/google/android/c/b/da;->a:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 10042
    iget-boolean v0, p0, Lcom/google/android/c/b/da;->c:Z

    return v0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 10084
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 10085
    iget v1, p0, Lcom/google/android/c/b/da;->a:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 10086
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/da;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10089
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/da;->a:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 10090
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/c/b/da;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10093
    :cond_1
    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 10050
    iget v0, p0, Lcom/google/android/c/b/da;->a:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 10001
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/c/b/da;->b:I

    iget v0, p0, Lcom/google/android/c/b/da;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/da;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/da;->c:Z

    iget v0, p0, Lcom/google/android/c/b/da;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/da;->a:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 10073
    iget v0, p0, Lcom/google/android/c/b/da;->a:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 10074
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/da;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 10076
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/da;->a:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 10077
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/c/b/da;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 10079
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 10080
    return-void
.end method
