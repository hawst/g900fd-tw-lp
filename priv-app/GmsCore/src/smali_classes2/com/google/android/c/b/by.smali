.class public final Lcom/google/android/c/b/by;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/android/c/b/bz;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14746
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 14747
    iput v1, p0, Lcom/google/android/c/b/by;->b:I

    invoke-static {}, Lcom/google/android/c/b/bz;->a()[Lcom/google/android/c/b/bz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/by;->a:[Lcom/google/android/c/b/bz;

    iput v1, p0, Lcom/google/android/c/b/by;->c:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/by;->cachedSize:I

    .line 14748
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/c/b/by;
    .locals 1

    .prologue
    .line 14733
    iput p1, p0, Lcom/google/android/c/b/by;->c:I

    .line 14734
    iget v0, p0, Lcom/google/android/c/b/by;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/by;->b:I

    .line 14735
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 14777
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v1

    .line 14778
    iget-object v0, p0, Lcom/google/android/c/b/by;->a:[Lcom/google/android/c/b/bz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/c/b/by;->a:[Lcom/google/android/c/b/bz;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 14779
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/c/b/by;->a:[Lcom/google/android/c/b/bz;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 14780
    iget-object v2, p0, Lcom/google/android/c/b/by;->a:[Lcom/google/android/c/b/bz;

    aget-object v2, v2, v0

    .line 14781
    if-eqz v2, :cond_0

    .line 14782
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 14779
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 14787
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/by;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 14788
    const/4 v0, 0x2

    iget v2, p0, Lcom/google/android/c/b/by;->c:I

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 14791
    :cond_2
    return v1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 14451
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/c/b/by;->a:[Lcom/google/android/c/b/bz;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/c/b/bz;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/c/b/by;->a:[Lcom/google/android/c/b/bz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/c/b/bz;

    invoke-direct {v3}, Lcom/google/android/c/b/bz;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/c/b/by;->a:[Lcom/google/android/c/b/bz;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/c/b/bz;

    invoke-direct {v3}, Lcom/google/android/c/b/bz;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/c/b/by;->a:[Lcom/google/android/c/b/bz;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/by;->c:I

    iget v0, p0, Lcom/google/android/c/b/by;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/by;->b:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 14761
    iget-object v0, p0, Lcom/google/android/c/b/by;->a:[Lcom/google/android/c/b/bz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/c/b/by;->a:[Lcom/google/android/c/b/bz;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 14762
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/c/b/by;->a:[Lcom/google/android/c/b/bz;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 14763
    iget-object v1, p0, Lcom/google/android/c/b/by;->a:[Lcom/google/android/c/b/bz;

    aget-object v1, v1, v0

    .line 14764
    if-eqz v1, :cond_0

    .line 14765
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 14762
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 14769
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/by;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 14770
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/by;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 14772
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 14773
    return-void
.end method
