.class public final Lcom/google/android/c/b/be;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/android/c/b/be;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15901
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 15902
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/c/b/be;->d:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/be;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/be;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/be;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/be;->f:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/be;->cachedSize:I

    .line 15903
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/be;
    .locals 2

    .prologue
    .line 15838
    sget-object v0, Lcom/google/android/c/b/be;->c:[Lcom/google/android/c/b/be;

    if-nez v0, :cond_1

    .line 15839
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 15841
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/be;->c:[Lcom/google/android/c/b/be;

    if-nez v0, :cond_0

    .line 15842
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/be;

    sput-object v0, Lcom/google/android/c/b/be;->c:[Lcom/google/android/c/b/be;

    .line 15844
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15846
    :cond_1
    sget-object v0, Lcom/google/android/c/b/be;->c:[Lcom/google/android/c/b/be;

    return-object v0

    .line 15844
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/c/b/be;
    .locals 1

    .prologue
    .line 15863
    if-nez p1, :cond_0

    .line 15864
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 15866
    :cond_0
    iput-object p1, p0, Lcom/google/android/c/b/be;->e:Ljava/lang/String;

    .line 15867
    iget v0, p0, Lcom/google/android/c/b/be;->d:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/be;->d:I

    .line 15868
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/c/b/be;
    .locals 1

    .prologue
    .line 15885
    if-nez p1, :cond_0

    .line 15886
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 15888
    :cond_0
    iput-object p1, p0, Lcom/google/android/c/b/be;->f:Ljava/lang/String;

    .line 15889
    iget v0, p0, Lcom/google/android/c/b/be;->d:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/be;->d:I

    .line 15890
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 15931
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 15932
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/c/b/be;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15934
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/c/b/be;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15936
    iget v1, p0, Lcom/google/android/c/b/be;->d:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 15937
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/c/b/be;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15940
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/be;->d:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 15941
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/c/b/be;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15944
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 15832
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/be;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/be;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/be;->e:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/be;->d:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/be;->d:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/be;->f:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/be;->d:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/be;->d:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 15918
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/c/b/be;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 15919
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/c/b/be;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 15920
    iget v0, p0, Lcom/google/android/c/b/be;->d:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 15921
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/c/b/be;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 15923
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/be;->d:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 15924
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/c/b/be;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 15926
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 15927
    return-void
.end method
