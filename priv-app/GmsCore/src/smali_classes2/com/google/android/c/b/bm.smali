.class public final Lcom/google/android/c/b/bm;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4313
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 4314
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/c/b/bm;->a:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bm;->b:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/bm;->cachedSize:I

    .line 4315
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/c/b/bm;
    .locals 1

    .prologue
    .line 4300
    iput p1, p0, Lcom/google/android/c/b/bm;->b:I

    .line 4301
    iget v0, p0, Lcom/google/android/c/b/bm;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bm;->a:I

    .line 4302
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 4335
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 4336
    iget v1, p0, Lcom/google/android/c/b/bm;->a:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 4337
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/bm;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4340
    :cond_0
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 4275
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/c/b/bm;->b:I

    iget v0, p0, Lcom/google/android/c/b/bm;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bm;->a:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 4327
    iget v0, p0, Lcom/google/android/c/b/bm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 4328
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/bm;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 4330
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4331
    return-void
.end method
