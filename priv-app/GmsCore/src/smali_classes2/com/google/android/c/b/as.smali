.class public final Lcom/google/android/c/b/as;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile a:[Lcom/google/android/c/b/as;


# instance fields
.field private b:I

.field private c:I

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 8094
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 8095
    iput v0, p0, Lcom/google/android/c/b/as;->b:I

    iput v1, p0, Lcom/google/android/c/b/as;->c:I

    iput v1, p0, Lcom/google/android/c/b/as;->d:I

    iput-boolean v0, p0, Lcom/google/android/c/b/as;->e:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/as;->cachedSize:I

    .line 8096
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/as;
    .locals 2

    .prologue
    .line 8024
    sget-object v0, Lcom/google/android/c/b/as;->a:[Lcom/google/android/c/b/as;

    if-nez v0, :cond_1

    .line 8025
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 8027
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/as;->a:[Lcom/google/android/c/b/as;

    if-nez v0, :cond_0

    .line 8028
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/as;

    sput-object v0, Lcom/google/android/c/b/as;->a:[Lcom/google/android/c/b/as;

    .line 8030
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8032
    :cond_1
    sget-object v0, Lcom/google/android/c/b/as;->a:[Lcom/google/android/c/b/as;

    return-object v0

    .line 8030
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 8124
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 8125
    iget v1, p0, Lcom/google/android/c/b/as;->b:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 8126
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/as;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8129
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/as;->b:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 8130
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/as;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8133
    :cond_1
    iget v1, p0, Lcom/google/android/c/b/as;->b:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 8134
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/c/b/as;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8137
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8018
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/c/b/as;->c:I

    iget v0, p0, Lcom/google/android/c/b/as;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/as;->b:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/android/c/b/as;->d:I

    iget v0, p0, Lcom/google/android/c/b/as;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/as;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/as;->e:Z

    iget v0, p0, Lcom/google/android/c/b/as;->b:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/c/b/as;->b:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 8110
    iget v0, p0, Lcom/google/android/c/b/as;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 8111
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/as;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8113
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/as;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 8114
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/as;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8116
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/as;->b:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 8117
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/c/b/as;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 8119
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 8120
    return-void
.end method
