.class public final Lcom/google/android/c/b/cv;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile b:[Lcom/google/android/c/b/cv;


# instance fields
.field public a:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8514
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 8515
    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/android/c/b/cv;->a:[I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/cv;->cachedSize:I

    .line 8516
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/cv;
    .locals 2

    .prologue
    .line 8500
    sget-object v0, Lcom/google/android/c/b/cv;->b:[Lcom/google/android/c/b/cv;

    if-nez v0, :cond_1

    .line 8501
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 8503
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/cv;->b:[Lcom/google/android/c/b/cv;

    if-nez v0, :cond_0

    .line 8504
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/cv;

    sput-object v0, Lcom/google/android/c/b/cv;->b:[Lcom/google/android/c/b/cv;

    .line 8506
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8508
    :cond_1
    sget-object v0, Lcom/google/android/c/b/cv;->b:[Lcom/google/android/c/b/cv;

    return-object v0

    .line 8506
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 8537
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v2

    .line 8538
    iget-object v1, p0, Lcom/google/android/c/b/cv;->a:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/c/b/cv;->a:[I

    array-length v1, v1

    if-lez v1, :cond_1

    move v1, v0

    .line 8540
    :goto_0
    iget-object v3, p0, Lcom/google/android/c/b/cv;->a:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 8541
    iget-object v3, p0, Lcom/google/android/c/b/cv;->a:[I

    aget v3, v3, v0

    .line 8542
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 8540
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8545
    :cond_0
    add-int v0, v2, v1

    .line 8546
    iget-object v1, p0, Lcom/google/android/c/b/cv;->a:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8548
    :goto_1
    return v0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 8494
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/c/b/cv;->a:[I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/c/b/cv;->a:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/c/b/cv;->a:[I

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Lcom/google/android/c/b/cv;->a:[I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v2, p0, Lcom/google/android/c/b/cv;->a:[I

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_5

    iget-object v4, p0, Lcom/google/android/c/b/cv;->a:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Lcom/google/android/c/b/cv;->a:[I

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Lcom/google/android/c/b/cv;->a:[I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 8527
    iget-object v0, p0, Lcom/google/android/c/b/cv;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/c/b/cv;->a:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 8528
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/c/b/cv;->a:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 8529
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/c/b/cv;->a:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8528
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8532
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 8533
    return-void
.end method
