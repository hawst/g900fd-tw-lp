.class public final Lcom/google/android/c/b/f;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/android/c/b/f;


# instance fields
.field public a:I

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2627
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2628
    iput v0, p0, Lcom/google/android/c/b/f;->a:I

    iput v0, p0, Lcom/google/android/c/b/f;->b:I

    iput v0, p0, Lcom/google/android/c/b/f;->c:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/f;->cachedSize:I

    .line 2629
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/f;
    .locals 2

    .prologue
    .line 2607
    sget-object v0, Lcom/google/android/c/b/f;->d:[Lcom/google/android/c/b/f;

    if-nez v0, :cond_1

    .line 2608
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2610
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/f;->d:[Lcom/google/android/c/b/f;

    if-nez v0, :cond_0

    .line 2611
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/f;

    sput-object v0, Lcom/google/android/c/b/f;->d:[Lcom/google/android/c/b/f;

    .line 2613
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2615
    :cond_1
    sget-object v0, Lcom/google/android/c/b/f;->d:[Lcom/google/android/c/b/f;

    return-object v0

    .line 2613
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2650
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2651
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/f;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2653
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/f;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2655
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/c/b/f;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2657
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2601
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/f;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/f;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/f;->c:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2642
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/f;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 2643
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/f;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 2644
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/c/b/f;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 2645
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2646
    return-void
.end method
