.class public final Lcom/google/android/c/b/cg;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/android/c/b/cg;


# instance fields
.field public a:I

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11836
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 11837
    iput v0, p0, Lcom/google/android/c/b/cg;->a:I

    iput v0, p0, Lcom/google/android/c/b/cg;->b:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/cg;->cachedSize:I

    .line 11838
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/cg;
    .locals 2

    .prologue
    .line 11819
    sget-object v0, Lcom/google/android/c/b/cg;->c:[Lcom/google/android/c/b/cg;

    if-nez v0, :cond_1

    .line 11820
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 11822
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/cg;->c:[Lcom/google/android/c/b/cg;

    if-nez v0, :cond_0

    .line 11823
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/cg;

    sput-object v0, Lcom/google/android/c/b/cg;->c:[Lcom/google/android/c/b/cg;

    .line 11825
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 11827
    :cond_1
    sget-object v0, Lcom/google/android/c/b/cg;->c:[Lcom/google/android/c/b/cg;

    return-object v0

    .line 11825
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 11857
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 11858
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/cg;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11860
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/cg;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11862
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 11813
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/cg;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/cg;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 11850
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/cg;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 11851
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/cg;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 11852
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 11853
    return-void
.end method
