.class public final Lcom/google/android/c/b/y;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile a:[Lcom/google/android/c/b/y;


# instance fields
.field private b:I

.field private c:I

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7547
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 7548
    iput v0, p0, Lcom/google/android/c/b/y;->b:I

    iput v0, p0, Lcom/google/android/c/b/y;->c:I

    iput v0, p0, Lcom/google/android/c/b/y;->d:I

    iput v0, p0, Lcom/google/android/c/b/y;->e:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/y;->cachedSize:I

    .line 7549
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/y;
    .locals 2

    .prologue
    .line 7477
    sget-object v0, Lcom/google/android/c/b/y;->a:[Lcom/google/android/c/b/y;

    if-nez v0, :cond_1

    .line 7478
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 7480
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/y;->a:[Lcom/google/android/c/b/y;

    if-nez v0, :cond_0

    .line 7481
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/y;

    sput-object v0, Lcom/google/android/c/b/y;->a:[Lcom/google/android/c/b/y;

    .line 7483
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7485
    :cond_1
    sget-object v0, Lcom/google/android/c/b/y;->a:[Lcom/google/android/c/b/y;

    return-object v0

    .line 7483
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final b()I
    .locals 1

    .prologue
    .line 7493
    iget v0, p0, Lcom/google/android/c/b/y;->c:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 7512
    iget v0, p0, Lcom/google/android/c/b/y;->d:I

    return v0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 7577
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 7578
    iget v1, p0, Lcom/google/android/c/b/y;->b:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 7579
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/y;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7582
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/y;->b:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 7583
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/y;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7586
    :cond_1
    iget v1, p0, Lcom/google/android/c/b/y;->b:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 7587
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/c/b/y;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7590
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 7471
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/y;->c:I

    iget v0, p0, Lcom/google/android/c/b/y;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/y;->b:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/y;->d:I

    iget v0, p0, Lcom/google/android/c/b/y;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/y;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/y;->e:I

    iget v0, p0, Lcom/google/android/c/b/y;->b:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/c/b/y;->b:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 7563
    iget v0, p0, Lcom/google/android/c/b/y;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 7564
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/y;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7566
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/y;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 7567
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/y;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7569
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/y;->b:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 7570
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/c/b/y;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7572
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 7573
    return-void
.end method
