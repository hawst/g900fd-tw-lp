.class public final Lcom/google/android/c/b/bz;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/android/c/b/bz;


# instance fields
.field public a:I

.field public b:I

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:[B


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14577
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 14578
    iput v1, p0, Lcom/google/android/c/b/bz;->d:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bz;->a:I

    iput v1, p0, Lcom/google/android/c/b/bz;->b:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/bz;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/bz;->f:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/bz;->g:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/c/b/bz;->h:[B

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/bz;->cachedSize:I

    .line 14579
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/bz;
    .locals 2

    .prologue
    .line 14470
    sget-object v0, Lcom/google/android/c/b/bz;->c:[Lcom/google/android/c/b/bz;

    if-nez v0, :cond_1

    .line 14471
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 14473
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/bz;->c:[Lcom/google/android/c/b/bz;

    if-nez v0, :cond_0

    .line 14474
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/bz;

    sput-object v0, Lcom/google/android/c/b/bz;->c:[Lcom/google/android/c/b/bz;

    .line 14476
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 14478
    :cond_1
    sget-object v0, Lcom/google/android/c/b/bz;->c:[Lcom/google/android/c/b/bz;

    return-object v0

    .line 14476
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/c/b/bz;
    .locals 1

    .prologue
    .line 14495
    if-nez p1, :cond_0

    .line 14496
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14498
    :cond_0
    iput-object p1, p0, Lcom/google/android/c/b/bz;->e:Ljava/lang/String;

    .line 14499
    iget v0, p0, Lcom/google/android/c/b/bz;->d:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bz;->d:I

    .line 14500
    return-object p0
.end method

.method public final a([B)Lcom/google/android/c/b/bz;
    .locals 1

    .prologue
    .line 14561
    if-nez p1, :cond_0

    .line 14562
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14564
    :cond_0
    iput-object p1, p0, Lcom/google/android/c/b/bz;->h:[B

    .line 14565
    iget v0, p0, Lcom/google/android/c/b/bz;->d:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/c/b/bz;->d:I

    .line 14566
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/c/b/bz;
    .locals 1

    .prologue
    .line 14517
    if-nez p1, :cond_0

    .line 14518
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14520
    :cond_0
    iput-object p1, p0, Lcom/google/android/c/b/bz;->f:Ljava/lang/String;

    .line 14521
    iget v0, p0, Lcom/google/android/c/b/bz;->d:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/bz;->d:I

    .line 14522
    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/c/b/bz;
    .locals 1

    .prologue
    .line 14539
    if-nez p1, :cond_0

    .line 14540
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14542
    :cond_0
    iput-object p1, p0, Lcom/google/android/c/b/bz;->g:Ljava/lang/String;

    .line 14543
    iget v0, p0, Lcom/google/android/c/b/bz;->d:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/c/b/bz;->d:I

    .line 14544
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 14615
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 14616
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/bz;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14618
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/bz;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14620
    iget v1, p0, Lcom/google/android/c/b/bz;->d:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 14621
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/c/b/bz;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14624
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/bz;->d:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 14625
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/c/b/bz;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14628
    :cond_1
    iget v1, p0, Lcom/google/android/c/b/bz;->d:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 14629
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/c/b/bz;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14632
    :cond_2
    iget v1, p0, Lcom/google/android/c/b/bz;->d:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 14633
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/c/b/bz;->h:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 14636
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 14464
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/c/b/bz;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/bz;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/bz;->e:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/bz;->d:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bz;->d:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/bz;->f:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/bz;->d:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/bz;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/bz;->g:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/c/b/bz;->d:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/c/b/bz;->d:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/bz;->h:[B

    iget v0, p0, Lcom/google/android/c/b/bz;->d:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/c/b/bz;->d:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 14596
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/bz;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 14597
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/bz;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 14598
    iget v0, p0, Lcom/google/android/c/b/bz;->d:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 14599
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/c/b/bz;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 14601
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/bz;->d:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 14602
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/c/b/bz;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 14604
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/bz;->d:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 14605
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/c/b/bz;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 14607
    :cond_2
    iget v0, p0, Lcom/google/android/c/b/bz;->d:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 14608
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/c/b/bz;->h:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 14610
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 14611
    return-void
.end method
