.class public final Lcom/google/android/c/b/w;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile b:[Lcom/google/android/c/b/w;


# instance fields
.field public a:[Z

.field private c:I

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8364
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 8365
    iput v0, p0, Lcom/google/android/c/b/w;->c:I

    iput-boolean v0, p0, Lcom/google/android/c/b/w;->d:Z

    iput-boolean v0, p0, Lcom/google/android/c/b/w;->e:Z

    sget-object v0, Lcom/google/protobuf/nano/m;->e:[Z

    iput-object v0, p0, Lcom/google/android/c/b/w;->a:[Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/w;->cachedSize:I

    .line 8366
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/w;
    .locals 2

    .prologue
    .line 8310
    sget-object v0, Lcom/google/android/c/b/w;->b:[Lcom/google/android/c/b/w;

    if-nez v0, :cond_1

    .line 8311
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 8313
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/w;->b:[Lcom/google/android/c/b/w;

    if-nez v0, :cond_0

    .line 8314
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/w;

    sput-object v0, Lcom/google/android/c/b/w;->b:[Lcom/google/android/c/b/w;

    .line 8316
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8318
    :cond_1
    sget-object v0, Lcom/google/android/c/b/w;->b:[Lcom/google/android/c/b/w;

    return-object v0

    .line 8316
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 8396
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 8397
    iget v1, p0, Lcom/google/android/c/b/w;->c:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 8398
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/c/b/w;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8401
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/w;->c:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 8402
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/c/b/w;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8405
    :cond_1
    iget-object v1, p0, Lcom/google/android/c/b/w;->a:[Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/c/b/w;->a:[Z

    array-length v1, v1

    if-lez v1, :cond_2

    .line 8406
    iget-object v1, p0, Lcom/google/android/c/b/w;->a:[Z

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    .line 8407
    add-int/2addr v0, v1

    .line 8408
    iget-object v1, p0, Lcom/google/android/c/b/w;->a:[Z

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8410
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 8304
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/w;->d:Z

    iget v0, p0, Lcom/google/android/c/b/w;->c:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/w;->c:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/w;->e:Z

    iget v0, p0, Lcom/google/android/c/b/w;->c:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/w;->c:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/c/b/w;->a:[Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Z

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/c/b/w;->a:[Z

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v3

    aput-boolean v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/c/b/w;->a:[Z

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v3

    aput-boolean v3, v2, v0

    iput-object v2, p0, Lcom/google/android/c/b/w;->a:[Z

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v2, p0, Lcom/google/android/c/b/w;->a:[Z

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [Z

    if-eqz v2, :cond_5

    iget-object v4, p0, Lcom/google/android/c/b/w;->a:[Z

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v4

    aput-boolean v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Lcom/google/android/c/b/w;->a:[Z

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Lcom/google/android/c/b/w;->a:[Z

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 8380
    iget v0, p0, Lcom/google/android/c/b/w;->c:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 8381
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/c/b/w;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 8383
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/w;->c:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 8384
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/c/b/w;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 8386
    :cond_1
    iget-object v0, p0, Lcom/google/android/c/b/w;->a:[Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/c/b/w;->a:[Z

    array-length v0, v0

    if-lez v0, :cond_2

    .line 8387
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/c/b/w;->a:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 8388
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/c/b/w;->a:[Z

    aget-boolean v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 8387
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8391
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 8392
    return-void
.end method
