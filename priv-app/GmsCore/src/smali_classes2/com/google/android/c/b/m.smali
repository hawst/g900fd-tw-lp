.class public final Lcom/google/android/c/b/m;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3128
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 3129
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/m;->a:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/android/c/b/m;->b:[I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/m;->cachedSize:I

    .line 3130
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 3161
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v1

    .line 3162
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/c/b/m;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v2, v1

    .line 3164
    iget-object v1, p0, Lcom/google/android/c/b/m;->b:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/c/b/m;->b:[I

    array-length v1, v1

    if-lez v1, :cond_1

    move v1, v0

    .line 3166
    :goto_0
    iget-object v3, p0, Lcom/google/android/c/b/m;->b:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 3167
    iget-object v3, p0, Lcom/google/android/c/b/m;->b:[I

    aget v3, v3, v0

    .line 3168
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 3166
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3171
    :cond_0
    add-int v0, v2, v1

    .line 3172
    add-int/lit8 v0, v0, 0x1

    .line 3173
    invoke-static {v1}, Lcom/google/protobuf/nano/b;->e(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 3176
    :goto_1
    return v0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 3105
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/m;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/c/b/m;->b:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Lcom/google/android/c/b/m;->b:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/c/b/m;->b:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Lcom/google/android/c/b/m;->b:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/android/c/b/m;->b:[I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/android/c/b/m;->b:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/google/android/c/b/m;->b:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Lcom/google/android/c/b/m;->b:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Lcom/google/android/c/b/m;->b:[I

    :cond_a
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3142
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/c/b/m;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3143
    iget-object v0, p0, Lcom/google/android/c/b/m;->b:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/c/b/m;->b:[I

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    move v2, v1

    .line 3145
    :goto_0
    iget-object v3, p0, Lcom/google/android/c/b/m;->b:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 3146
    iget-object v3, p0, Lcom/google/android/c/b/m;->b:[I

    aget v3, v3, v0

    .line 3147
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 3145
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3150
    :cond_0
    const/16 v0, 0x12

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/b;->d(I)V

    .line 3151
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/b;->d(I)V

    .line 3152
    :goto_1
    iget-object v0, p0, Lcom/google/android/c/b/m;->b:[I

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 3153
    iget-object v0, p0, Lcom/google/android/c/b/m;->b:[I

    aget v0, v0, v1

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/b;->d(I)V

    .line 3152
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3156
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3157
    return-void
.end method
