.class public final Lcom/google/android/c/b/cz;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile a:[Lcom/google/android/c/b/cz;


# instance fields
.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2295
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2296
    iput v0, p0, Lcom/google/android/c/b/cz;->b:I

    iput v1, p0, Lcom/google/android/c/b/cz;->c:I

    iput v1, p0, Lcom/google/android/c/b/cz;->d:I

    iput v0, p0, Lcom/google/android/c/b/cz;->e:I

    iput v0, p0, Lcom/google/android/c/b/cz;->f:I

    iput v0, p0, Lcom/google/android/c/b/cz;->g:I

    iput v0, p0, Lcom/google/android/c/b/cz;->h:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/cz;->cachedSize:I

    .line 2297
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/cz;
    .locals 2

    .prologue
    .line 2168
    sget-object v0, Lcom/google/android/c/b/cz;->a:[Lcom/google/android/c/b/cz;

    if-nez v0, :cond_1

    .line 2169
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2171
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/cz;->a:[Lcom/google/android/c/b/cz;

    if-nez v0, :cond_0

    .line 2172
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/cz;

    sput-object v0, Lcom/google/android/c/b/cz;->a:[Lcom/google/android/c/b/cz;

    .line 2174
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2176
    :cond_1
    sget-object v0, Lcom/google/android/c/b/cz;->a:[Lcom/google/android/c/b/cz;

    return-object v0

    .line 2174
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final b()I
    .locals 1

    .prologue
    .line 2184
    iget v0, p0, Lcom/google/android/c/b/cz;->c:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2203
    iget v0, p0, Lcom/google/android/c/b/cz;->d:I

    return v0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2337
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2338
    iget v1, p0, Lcom/google/android/c/b/cz;->b:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2339
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/cz;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2342
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/cz;->b:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2343
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/cz;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2346
    :cond_1
    iget v1, p0, Lcom/google/android/c/b/cz;->b:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 2347
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/c/b/cz;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2350
    :cond_2
    iget v1, p0, Lcom/google/android/c/b/cz;->b:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 2351
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/c/b/cz;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2354
    :cond_3
    iget v1, p0, Lcom/google/android/c/b/cz;->b:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 2355
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/c/b/cz;->g:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2358
    :cond_4
    iget v1, p0, Lcom/google/android/c/b/cz;->b:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 2359
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/c/b/cz;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2362
    :cond_5
    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2211
    iget v0, p0, Lcom/google/android/c/b/cz;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2222
    iget v0, p0, Lcom/google/android/c/b/cz;->e:I

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 2230
    iget v0, p0, Lcom/google/android/c/b/cz;->b:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 2241
    iget v0, p0, Lcom/google/android/c/b/cz;->f:I

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 2249
    iget v0, p0, Lcom/google/android/c/b/cz;->b:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 2260
    iget v0, p0, Lcom/google/android/c/b/cz;->g:I

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 2268
    iget v0, p0, Lcom/google/android/c/b/cz;->b:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2279
    iget v0, p0, Lcom/google/android/c/b/cz;->h:I

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 2287
    iget v0, p0, Lcom/google/android/c/b/cz;->b:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2162
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/c/b/cz;->c:I

    iget v0, p0, Lcom/google/android/c/b/cz;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/cz;->b:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/android/c/b/cz;->d:I

    iget v0, p0, Lcom/google/android/c/b/cz;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/cz;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/cz;->e:I

    iget v0, p0, Lcom/google/android/c/b/cz;->b:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/c/b/cz;->b:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/cz;->f:I

    iget v0, p0, Lcom/google/android/c/b/cz;->b:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/c/b/cz;->b:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/cz;->g:I

    iget v0, p0, Lcom/google/android/c/b/cz;->b:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/android/c/b/cz;->b:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/cz;->h:I

    iget v0, p0, Lcom/google/android/c/b/cz;->b:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/android/c/b/cz;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2314
    iget v0, p0, Lcom/google/android/c/b/cz;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2315
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/cz;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2317
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/cz;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2318
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/cz;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2320
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/cz;->b:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 2321
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/c/b/cz;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 2323
    :cond_2
    iget v0, p0, Lcom/google/android/c/b/cz;->b:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 2324
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/c/b/cz;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 2326
    :cond_3
    iget v0, p0, Lcom/google/android/c/b/cz;->b:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 2327
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/c/b/cz;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 2329
    :cond_4
    iget v0, p0, Lcom/google/android/c/b/cz;->b:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 2330
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/c/b/cz;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 2332
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2333
    return-void
.end method
