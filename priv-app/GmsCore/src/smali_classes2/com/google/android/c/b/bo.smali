.class public final Lcom/google/android/c/b/bo;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field private b:I

.field private c:I

.field private d:I

.field private e:[B

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 13511
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 13512
    iput v1, p0, Lcom/google/android/c/b/bo;->b:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/c/b/bo;->a:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bo;->c:I

    iput v1, p0, Lcom/google/android/c/b/bo;->d:I

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/c/b/bo;->e:[B

    iput v1, p0, Lcom/google/android/c/b/bo;->f:I

    iput v1, p0, Lcom/google/android/c/b/bo;->g:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/bo;->cachedSize:I

    .line 13513
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/c/b/bo;
    .locals 1

    .prologue
    .line 13419
    iput p1, p0, Lcom/google/android/c/b/bo;->c:I

    .line 13420
    iget v0, p0, Lcom/google/android/c/b/bo;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bo;->b:I

    .line 13421
    return-object p0
.end method

.method public final a([B)Lcom/google/android/c/b/bo;
    .locals 1

    .prologue
    .line 13457
    if-nez p1, :cond_0

    .line 13458
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 13460
    :cond_0
    iput-object p1, p0, Lcom/google/android/c/b/bo;->e:[B

    .line 13461
    iget v0, p0, Lcom/google/android/c/b/bo;->b:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/c/b/bo;->b:I

    .line 13462
    return-object p0
.end method

.method public final b(I)Lcom/google/android/c/b/bo;
    .locals 1

    .prologue
    .line 13438
    iput p1, p0, Lcom/google/android/c/b/bo;->d:I

    .line 13439
    iget v0, p0, Lcom/google/android/c/b/bo;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/bo;->b:I

    .line 13440
    return-object p0
.end method

.method public final c(I)Lcom/google/android/c/b/bo;
    .locals 1

    .prologue
    .line 13479
    iput p1, p0, Lcom/google/android/c/b/bo;->f:I

    .line 13480
    iget v0, p0, Lcom/google/android/c/b/bo;->b:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/c/b/bo;->b:I

    .line 13481
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 13551
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 13552
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/c/b/bo;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13554
    iget v1, p0, Lcom/google/android/c/b/bo;->b:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 13555
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/c/b/bo;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 13558
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/bo;->b:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 13559
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/c/b/bo;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 13562
    :cond_1
    iget v1, p0, Lcom/google/android/c/b/bo;->b:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 13563
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/c/b/bo;->e:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 13566
    :cond_2
    iget v1, p0, Lcom/google/android/c/b/bo;->b:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 13567
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/c/b/bo;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 13570
    :cond_3
    iget v1, p0, Lcom/google/android/c/b/bo;->b:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 13571
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/c/b/bo;->g:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 13574
    :cond_4
    return v0
.end method

.method public final d(I)Lcom/google/android/c/b/bo;
    .locals 1

    .prologue
    .line 13498
    iput p1, p0, Lcom/google/android/c/b/bo;->g:I

    .line 13499
    iget v0, p0, Lcom/google/android/c/b/bo;->b:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/android/c/b/bo;->b:I

    .line 13500
    return-object p0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 13362
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/bo;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/c/b/bo;->c:I

    iget v0, p0, Lcom/google/android/c/b/bo;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bo;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    :pswitch_1
    goto :goto_0

    :pswitch_2
    iput v0, p0, Lcom/google/android/c/b/bo;->d:I

    iget v0, p0, Lcom/google/android/c/b/bo;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/bo;->b:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/c/b/bo;->e:[B

    iget v0, p0, Lcom/google/android/c/b/bo;->b:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/c/b/bo;->b:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/bo;->f:I

    iget v0, p0, Lcom/google/android/c/b/bo;->b:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/c/b/bo;->b:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/bo;->g:I

    iget v0, p0, Lcom/google/android/c/b/bo;->b:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/android/c/b/bo;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 13530
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/c/b/bo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 13531
    iget v0, p0, Lcom/google/android/c/b/bo;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 13532
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/c/b/bo;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 13534
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/bo;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 13535
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/c/b/bo;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 13537
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/bo;->b:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 13538
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/c/b/bo;->e:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 13540
    :cond_2
    iget v0, p0, Lcom/google/android/c/b/bo;->b:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 13541
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/c/b/bo;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 13543
    :cond_3
    iget v0, p0, Lcom/google/android/c/b/bo;->b:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 13544
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/c/b/bo;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 13546
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 13547
    return-void
.end method
