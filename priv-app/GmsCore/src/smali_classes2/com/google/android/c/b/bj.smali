.class public final Lcom/google/android/c/b/bj;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Z

.field private b:I

.field private c:Z

.field private d:Z

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10436
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 10437
    iput v0, p0, Lcom/google/android/c/b/bj;->b:I

    iput-boolean v0, p0, Lcom/google/android/c/b/bj;->a:Z

    iput-boolean v0, p0, Lcom/google/android/c/b/bj;->c:Z

    iput-boolean v0, p0, Lcom/google/android/c/b/bj;->d:Z

    iput v0, p0, Lcom/google/android/c/b/bj;->e:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/bj;->cachedSize:I

    .line 10438
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/c/b/bj;
    .locals 1

    .prologue
    .line 10423
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/bj;->e:I

    .line 10424
    iget v0, p0, Lcom/google/android/c/b/bj;->b:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/c/b/bj;->b:I

    .line 10425
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/c/b/bj;
    .locals 1

    .prologue
    .line 10385
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/c/b/bj;->c:Z

    .line 10386
    iget v0, p0, Lcom/google/android/c/b/bj;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bj;->b:I

    .line 10387
    return-object p0
.end method

.method public final b(Z)Lcom/google/android/c/b/bj;
    .locals 1

    .prologue
    .line 10404
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/c/b/bj;->d:Z

    .line 10405
    iget v0, p0, Lcom/google/android/c/b/bj;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/bj;->b:I

    .line 10406
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 10468
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 10469
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/c/b/bj;->a:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10471
    iget v1, p0, Lcom/google/android/c/b/bj;->b:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 10472
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/c/b/bj;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10475
    :cond_0
    iget v1, p0, Lcom/google/android/c/b/bj;->b:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 10476
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/c/b/bj;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10479
    :cond_1
    iget v1, p0, Lcom/google/android/c/b/bj;->b:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 10480
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/c/b/bj;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10483
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 10357
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/bj;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/bj;->c:Z

    iget v0, p0, Lcom/google/android/c/b/bj;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/c/b/bj;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/c/b/bj;->d:Z

    iget v0, p0, Lcom/google/android/c/b/bj;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/c/b/bj;->b:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/bj;->e:I

    iget v0, p0, Lcom/google/android/c/b/bj;->b:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/c/b/bj;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 10453
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/c/b/bj;->a:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 10454
    iget v0, p0, Lcom/google/android/c/b/bj;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 10455
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/c/b/bj;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 10457
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/bj;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 10458
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/c/b/bj;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 10460
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/bj;->b:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 10461
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/c/b/bj;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 10463
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 10464
    return-void
.end method
