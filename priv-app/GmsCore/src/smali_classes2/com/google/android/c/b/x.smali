.class public final Lcom/google/android/c/b/x;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile b:[Lcom/google/android/c/b/x;


# instance fields
.field public a:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7795
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 7796
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/c/b/x;->a:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/c/b/x;->cachedSize:I

    .line 7797
    return-void
.end method

.method public static a()[Lcom/google/android/c/b/x;
    .locals 2

    .prologue
    .line 7781
    sget-object v0, Lcom/google/android/c/b/x;->b:[Lcom/google/android/c/b/x;

    if-nez v0, :cond_1

    .line 7782
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 7784
    :try_start_0
    sget-object v0, Lcom/google/android/c/b/x;->b:[Lcom/google/android/c/b/x;

    if-nez v0, :cond_0

    .line 7785
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/c/b/x;

    sput-object v0, Lcom/google/android/c/b/x;->b:[Lcom/google/android/c/b/x;

    .line 7787
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7789
    :cond_1
    sget-object v0, Lcom/google/android/c/b/x;->b:[Lcom/google/android/c/b/x;

    return-object v0

    .line 7787
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 7814
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 7815
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/c/b/x;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7817
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 7775
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/c/b/x;->a:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 7808
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/c/b/x;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7809
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 7810
    return-void
.end method
