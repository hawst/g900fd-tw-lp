.class public Lcom/google/android/apps/common/a/a/g;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/apps/common/a/a/f;

.field private static b:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/common/a/a/g;->b:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/common/a/a/b;Lcom/android/volley/s;)Lcom/google/android/apps/common/a/a/e;
    .locals 10

    .prologue
    .line 68
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    const-class v8, Lcom/google/android/apps/common/a/a/g;

    monitor-enter v8

    .line 72
    :try_start_0
    iget-object v9, p0, Lcom/google/android/apps/common/a/a/b;->b:Ljava/lang/String;

    .line 73
    sget-object v0, Lcom/google/android/apps/common/a/a/g;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/common/a/a/f;

    .line 75
    if-nez v0, :cond_1

    .line 76
    new-instance v0, Lcom/google/android/apps/common/a/a/f;

    iget-object v1, p0, Lcom/google/android/apps/common/a/a/b;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/common/a/a/b;->b:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/apps/common/a/a/b;->a:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/google/android/apps/common/a/a/b;->d:I

    iget v5, p0, Lcom/google/android/apps/common/a/a/b;->f:I

    iget v6, p0, Lcom/google/android/apps/common/a/a/b;->e:I

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/common/a/a/f;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILcom/android/volley/s;)V

    iget-object v1, p0, Lcom/google/android/apps/common/a/a/b;->g:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/common/a/a/f;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 83
    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0

    .line 77
    :cond_0
    :try_start_1
    sget-object v1, Lcom/google/android/apps/common/a/a/g;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v9, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v1, Lcom/google/android/apps/common/a/a/g;->a:Lcom/google/android/apps/common/a/a/f;

    if-nez v1, :cond_1

    .line 79
    sput-object v0, Lcom/google/android/apps/common/a/a/g;->a:Lcom/google/android/apps/common/a/a/f;

    .line 82
    :cond_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method
