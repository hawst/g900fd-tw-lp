.class public final Lcom/google/android/apps/common/a/a/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/common/a/a/e;
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Ljava/util/LinkedHashMap;

.field private b:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

.field private c:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:Lcom/android/volley/s;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILcom/android/volley/s;)V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/common/a/a/f;->a:Ljava/util/LinkedHashMap;

    .line 29
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/common/a/a/f;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 60
    iput-object p1, p0, Lcom/google/android/apps/common/a/a/f;->d:Ljava/lang/String;

    .line 61
    iput-object p2, p0, Lcom/google/android/apps/common/a/a/f;->e:Ljava/lang/String;

    .line 62
    iput-object p3, p0, Lcom/google/android/apps/common/a/a/f;->f:Ljava/lang/String;

    .line 63
    iput p4, p0, Lcom/google/android/apps/common/a/a/f;->g:I

    .line 64
    iput p5, p0, Lcom/google/android/apps/common/a/a/f;->i:I

    .line 65
    iput p6, p0, Lcom/google/android/apps/common/a/a/f;->h:I

    .line 66
    iput-object p7, p0, Lcom/google/android/apps/common/a/a/f;->j:Lcom/android/volley/s;

    .line 67
    const-string v0, "v"

    iget-object v1, p0, Lcom/google/android/apps/common/a/a/f;->f:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/common/a/a/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "s"

    iget-object v1, p0, Lcom/google/android/apps/common/a/a/f;->e:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/common/a/a/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/common/a/a/f;->b:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    .line 68
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/apps/common/a/a/f;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    return-void
.end method

.method public final a(Lcom/google/android/apps/common/a/a/i;)Z
    .locals 4

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/common/a/a/f;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/common/a/a/f;->b:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->getQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    const-string v0, "ReporterDefault"

    const-string v1, "Scheduling task to dispatch reports"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/common/a/a/f;->b:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    iget v1, p0, Lcom/google/android/apps/common/a/a/f;->h:I

    int-to-long v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p0, v2, v3, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 90
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final run()V
    .locals 6

    .prologue
    .line 97
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    .line 99
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget v0, p0, Lcom/google/android/apps/common/a/a/f;->i:I

    if-ge v2, v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/common/a/a/f;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/common/a/a/i;

    .line 101
    if-eqz v0, :cond_1

    .line 102
    iget-object v1, v0, Lcom/google/android/apps/common/a/a/i;->a:Ljava/lang/String;

    .line 106
    invoke-virtual {v3, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 107
    invoke-virtual {v3, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 109
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 110
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    invoke-virtual {v3, v1, v4}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 116
    :cond_1
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 117
    new-instance v2, Lcom/google/android/apps/common/a/a/c;

    iget-object v3, p0, Lcom/google/android/apps/common/a/a/f;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/common/a/a/f;->a:Ljava/util/LinkedHashMap;

    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/apps/common/a/a/c;-><init>(Ljava/lang/String;Ljava/util/Map;Ljava/util/ArrayList;)V

    .line 118
    new-instance v0, Lcom/android/volley/f;

    const/16 v3, 0x9c4

    iget v4, p0, Lcom/google/android/apps/common/a/a/f;->g:I

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v0, v3, v4, v5}, Lcom/android/volley/f;-><init>(IIF)V

    invoke-virtual {v2, v0}, Lcom/google/android/apps/common/a/a/c;->a(Lcom/android/volley/z;)Lcom/android/volley/p;

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/common/a/a/f;->j:Lcom/android/volley/s;

    invoke-virtual {v0, v2}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    goto :goto_2

    .line 125
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/common/a/a/f;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 126
    const-string v0, "ReporterDefault"

    const-string v1, "Scheduling task to dispatch reports"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/common/a/a/f;->b:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    iget v1, p0, Lcom/google/android/apps/common/a/a/f;->h:I

    int-to-long v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p0, v2, v3, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 129
    :cond_3
    return-void
.end method
