.class public final Lcom/google/android/apps/common/a/a/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/String;

.field b:Ljava/util/LinkedHashMap;

.field private final c:J

.field private final d:Ljava/util/LinkedList;

.field private final e:Lcom/google/android/apps/common/a/a/j;

.field private f:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lcom/google/android/apps/common/a/a/d;

    invoke-direct {v0}, Lcom/google/android/apps/common/a/a/d;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/common/a/a/i;-><init>(Ljava/lang/String;Lcom/google/android/apps/common/a/a/j;)V

    .line 68
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/google/android/apps/common/a/a/j;)V
    .locals 2

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/common/a/a/i;->b:Ljava/util/LinkedHashMap;

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/common/a/a/i;->f:Z

    .line 77
    iput-object p1, p0, Lcom/google/android/apps/common/a/a/i;->a:Ljava/lang/String;

    .line 78
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/common/a/a/i;->d:Ljava/util/LinkedList;

    .line 79
    iput-object p2, p0, Lcom/google/android/apps/common/a/a/i;->e:Lcom/google/android/apps/common/a/a/j;

    .line 80
    invoke-interface {p2}, Lcom/google/android/apps/common/a/a/j;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/common/a/a/i;->c:J

    .line 81
    return-void
.end method

.method static a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;)Ljava/util/Map;
    .locals 3

    .prologue
    .line 264
    const/4 v0, 0x0

    .line 265
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 266
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 268
    if-eqz p0, :cond_0

    .line 269
    const-string v1, "action"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    :cond_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 273
    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 276
    :cond_1
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 277
    const-string v1, "it"

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 280
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 281
    const-string v1, "irt"

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    :cond_2
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/common/a/a/h;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/common/a/a/i;->e:Lcom/google/android/apps/common/a/a/j;

    invoke-interface {v0}, Lcom/google/android/apps/common/a/a/j;->a()J

    move-result-wide v0

    .line 134
    new-instance v2, Lcom/google/android/apps/common/a/a/h;

    invoke-direct {v2, v0, v1, v3, v3}, Lcom/google/android/apps/common/a/a/h;-><init>(JLjava/lang/String;Lcom/google/android/apps/common/a/a/h;)V

    .line 135
    return-object v2
.end method

.method public final varargs a(Lcom/google/android/apps/common/a/a/h;[Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 147
    if-nez p1, :cond_0

    .line 148
    const-string v1, "Ticker"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "In action: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/common/a/a/i;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", label item shouldn\'t be null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    :goto_0
    return v0

    .line 151
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/common/a/a/i;->e:Lcom/google/android/apps/common/a/a/j;

    invoke-interface {v1}, Lcom/google/android/apps/common/a/a/j;->a()J

    move-result-wide v2

    .line 152
    array-length v1, p2

    :goto_1
    if-ge v0, v1, :cond_1

    aget-object v4, p2, v0

    .line 153
    new-instance v5, Lcom/google/android/apps/common/a/a/h;

    invoke-direct {v5, v2, v3, v4, p1}, Lcom/google/android/apps/common/a/a/h;-><init>(JLjava/lang/String;Lcom/google/android/apps/common/a/a/h;)V

    .line 154
    iget-object v4, p0, Lcom/google/android/apps/common/a/a/i;->d:Ljava/util/LinkedList;

    invoke-virtual {v4, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 152
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 156
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Ljava/util/Map;
    .locals 11

    .prologue
    const/16 v10, 0x2c

    .line 168
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 169
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/common/a/a/i;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/common/a/a/h;

    .line 173
    iget-object v4, v0, Lcom/google/android/apps/common/a/a/h;->b:Ljava/lang/String;

    .line 174
    iget-object v5, v0, Lcom/google/android/apps/common/a/a/h;->c:Lcom/google/android/apps/common/a/a/h;

    .line 175
    iget-wide v6, v0, Lcom/google/android/apps/common/a/a/h;->a:J

    .line 178
    if-eqz v5, :cond_0

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-lez v0, :cond_0

    .line 179
    iget-wide v8, v5, Lcom/google/android/apps/common/a/a/h;->a:J

    sub-long v8, v6, v8

    .line 180
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v4, 0x2e

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 181
    iget-boolean v0, p0, Lcom/google/android/apps/common/a/a/i;->f:Z

    if-eqz v0, :cond_0

    .line 182
    iget-wide v4, p0, Lcom/google/android/apps/common/a/a/i;->c:J

    sub-long v4, v6, v4

    .line 183
    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/common/a/a/i;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/common/a/a/i;->b:Ljava/util/LinkedHashMap;

    invoke-static {v0, v3, v1, v2}, Lcom/google/android/apps/common/a/a/i;->a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
