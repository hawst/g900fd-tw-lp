.class public final Lcom/google/android/apps/common/a/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:I

.field public b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field public d:I

.field public e:I

.field public f:I

.field g:Ljava/util/Map;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-string v0, "Android %s"

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/common/a/a/b;->h:Ljava/lang/String;

    .line 24
    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/common/a/a/b;->i:Ljava/lang/String;

    .line 26
    iput v5, p0, Lcom/google/android/apps/common/a/a/b;->a:I

    .line 27
    const-string v0, "_s"

    iput-object v0, p0, Lcom/google/android/apps/common/a/a/b;->b:Ljava/lang/String;

    .line 28
    const-string v0, "http://csi.gstatic.com/csi"

    iput-object v0, p0, Lcom/google/android/apps/common/a/a/b;->c:Ljava/lang/String;

    .line 29
    iput v5, p0, Lcom/google/android/apps/common/a/a/b;->d:I

    .line 30
    iput v4, p0, Lcom/google/android/apps/common/a/a/b;->e:I

    .line 31
    iput v4, p0, Lcom/google/android/apps/common/a/a/b;->f:I

    .line 32
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/common/a/a/b;->g:Ljava/util/Map;

    return-void
.end method

.method public static a(ILjava/lang/String;)I
    .locals 4

    .prologue
    .line 185
    if-gtz p0, :cond_0

    .line 186
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "%s must be greater than 0."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 190
    :cond_0
    return p0
.end method
