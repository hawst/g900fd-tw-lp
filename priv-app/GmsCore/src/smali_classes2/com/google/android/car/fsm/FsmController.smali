.class public Lcom/google/android/car/fsm/FsmController;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final k:Ljava/lang/String;

.field private static final l:Ljava/lang/Class;


# instance fields
.field a:Landroid/content/Context;

.field b:Ljava/util/List;

.field c:Ljava/lang/Class;

.field d:Ljava/util/ArrayList;

.field e:Lcom/google/android/car/fsm/j;

.field f:I

.field g:Z

.field h:Ljava/lang/Class;

.field i:Lcom/google/android/car/fsm/h;

.field j:Ljava/util/ArrayList;

.field private final m:Lcom/google/android/car/fsm/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/android/car/fsm/FsmController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/car/fsm/FsmController;->k:Ljava/lang/String;

    .line 36
    const-class v0, Lcom/google/android/car/fsm/j;

    sput-object v0, Lcom/google/android/car/fsm/FsmController;->l:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Class;Ljava/util/List;Lcom/google/android/car/fsm/g;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/car/fsm/FsmController;->f:I

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/car/fsm/FsmController;->j:Ljava/util/ArrayList;

    .line 139
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/car/fsm/FsmController;->a:Landroid/content/Context;

    .line 140
    iput-object p2, p0, Lcom/google/android/car/fsm/FsmController;->c:Ljava/lang/Class;

    .line 141
    iput-object p3, p0, Lcom/google/android/car/fsm/FsmController;->b:Ljava/util/List;

    .line 142
    iput-object p4, p0, Lcom/google/android/car/fsm/FsmController;->m:Lcom/google/android/car/fsm/g;

    .line 143
    if-nez p5, :cond_0

    .line 145
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    .line 150
    :goto_0
    return-void

    .line 148
    :cond_0
    sget-object v0, Lcom/google/android/car/fsm/FsmController;->k:Ljava/lang/String;

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method private a(Ljava/lang/Class;Landroid/os/Parcelable;)Lcom/google/android/car/fsm/j;
    .locals 4

    .prologue
    .line 381
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/car/fsm/j;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 393
    const-class v1, Lcom/google/android/car/fsm/l;

    invoke-virtual {p1, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    if-nez v1, :cond_0

    .line 394
    invoke-direct {p0}, Lcom/google/android/car/fsm/FsmController;->f()V

    .line 395
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No @Transitions annotation on "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 384
    :catch_0
    move-exception v0

    .line 385
    invoke-direct {p0}, Lcom/google/android/car/fsm/FsmController;->f()V

    .line 386
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate state class: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 387
    :catch_1
    move-exception v0

    .line 388
    invoke-direct {p0}, Lcom/google/android/car/fsm/FsmController;->f()V

    .line 389
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate state class: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 398
    :cond_0
    iget-object v1, p0, Lcom/google/android/car/fsm/FsmController;->a:Landroid/content/Context;

    invoke-virtual {v0, v1, p0, p2}, Lcom/google/android/car/fsm/j;->a(Landroid/content/Context;Lcom/google/android/car/fsm/FsmController;Landroid/os/Parcelable;)V

    .line 399
    return-object v0
.end method

.method private static a(Lcom/google/android/car/fsm/j;Ljava/lang/String;)Lcom/google/android/car/fsm/k;
    .locals 4

    .prologue
    .line 344
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/car/fsm/l;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/google/android/car/fsm/l;

    invoke-interface {v0}, Lcom/google/android/car/fsm/l;->a()[Lcom/google/android/car/fsm/k;

    move-result-object v1

    .line 345
    array-length v2, v1

    .line 346
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    .line 347
    aget-object v3, v1, v0

    invoke-interface {v3}, Lcom/google/android/car/fsm/k;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 348
    aget-object v0, v1, v0

    .line 351
    :goto_1
    return-object v0

    .line 346
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 351
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Ljava/lang/Class;Ljava/lang/String;Landroid/os/Parcelable;)V
    .locals 4

    .prologue
    .line 362
    invoke-direct {p0, p1, p3}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/Class;Landroid/os/Parcelable;)Lcom/google/android/car/fsm/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/car/fsm/FsmController;->e:Lcom/google/android/car/fsm/j;

    .line 363
    new-instance v0, Lcom/google/android/car/fsm/FsmController$StackEntry;

    iget-object v1, p0, Lcom/google/android/car/fsm/FsmController;->e:Lcom/google/android/car/fsm/j;

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/car/fsm/FsmController;->f:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/car/fsm/FsmController;->f:I

    invoke-direct {v0, p1, p3, v1, v2}, Lcom/google/android/car/fsm/FsmController$StackEntry;-><init>(Ljava/lang/Class;Landroid/os/Parcelable;ZI)V

    .line 365
    iget-object v1, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 366
    invoke-direct {p0}, Lcom/google/android/car/fsm/FsmController;->e()V

    .line 367
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 403
    invoke-direct {p0}, Lcom/google/android/car/fsm/FsmController;->g()V

    .line 404
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->e:Lcom/google/android/car/fsm/j;

    invoke-virtual {v0}, Lcom/google/android/car/fsm/j;->a()V

    .line 405
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 406
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 407
    iget-object v2, p0, Lcom/google/android/car/fsm/FsmController;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/car/fsm/FsmController;->e:Lcom/google/android/car/fsm/j;

    .line 406
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 409
    :cond_0
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 543
    const-string v0, "FsmController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mCurrentStateClass="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/car/fsm/FsmController;->e:Lcom/google/android/car/fsm/j;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    invoke-direct {p0}, Lcom/google/android/car/fsm/FsmController;->g()V

    .line 545
    return-void
.end method

.method private g()V
    .locals 5

    .prologue
    .line 548
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 549
    const-string v1, "FsmController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "State stack (size="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 551
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/car/fsm/FsmController$StackEntry;

    .line 552
    const-string v2, "FsmController"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "    "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/car/fsm/FsmController$StackEntry;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 554
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 157
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->e:Lcom/google/android/car/fsm/j;

    if-eqz v0, :cond_0

    .line 158
    invoke-direct {p0}, Lcom/google/android/car/fsm/FsmController;->f()V

    .line 159
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "start already called"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v1, v0

    .line 163
    :goto_0
    if-lez v1, :cond_1

    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    add-int/lit8 v2, v1, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/car/fsm/FsmController$StackEntry;

    iget-boolean v0, v0, Lcom/google/android/car/fsm/FsmController$StackEntry;->c:Z

    if-nez v0, :cond_1

    .line 164
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    add-int/lit8 v2, v1, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 165
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 167
    :cond_1
    if-nez v1, :cond_2

    .line 169
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->c:Ljava/lang/Class;

    invoke-direct {p0, v0, v3, v3}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/Class;Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 174
    :goto_1
    return-void

    .line 172
    :cond_2
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/car/fsm/FsmController$StackEntry;

    iget-object v1, v0, Lcom/google/android/car/fsm/FsmController$StackEntry;->a:Ljava/lang/Class;

    iget-object v0, v0, Lcom/google/android/car/fsm/FsmController$StackEntry;->b:Landroid/os/Parcelable;

    invoke-direct {p0, v1, v0}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/Class;Landroid/os/Parcelable;)Lcom/google/android/car/fsm/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/car/fsm/FsmController;->e:Lcom/google/android/car/fsm/j;

    invoke-direct {p0}, Lcom/google/android/car/fsm/FsmController;->e()V

    goto :goto_1
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 498
    new-instance v0, Lcom/google/android/car/fsm/d;

    invoke-direct {v0, p0, p1}, Lcom/google/android/car/fsm/d;-><init>(Lcom/google/android/car/fsm/FsmController;Landroid/content/Intent;)V

    invoke-virtual {p0, v0}, Lcom/google/android/car/fsm/FsmController;->a(Lcom/google/android/car/fsm/e;)V

    .line 504
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 436
    sget-object v0, Lcom/google/android/car/fsm/FsmController;->k:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 437
    return-void
.end method

.method public final a(Lcom/google/android/car/fsm/e;)V
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->i:Lcom/google/android/car/fsm/h;

    if-eqz v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->i:Lcom/google/android/car/fsm/h;

    invoke-interface {p1, v0}, Lcom/google/android/car/fsm/e;->a(Lcom/google/android/car/fsm/h;)V

    .line 512
    :goto_0
    return-void

    .line 510
    :cond_0
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/car/fsm/h;)V
    .locals 2

    .prologue
    .line 515
    iput-object p1, p0, Lcom/google/android/car/fsm/FsmController;->i:Lcom/google/android/car/fsm/h;

    .line 516
    :goto_0
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 517
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->j:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/car/fsm/e;

    invoke-interface {v0, p1}, Lcom/google/android/car/fsm/e;->a(Lcom/google/android/car/fsm/h;)V

    goto :goto_0

    .line 519
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Class;)V
    .locals 4

    .prologue
    .line 440
    iput-object p1, p0, Lcom/google/android/car/fsm/FsmController;->h:Ljava/lang/Class;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "com.google.android.fsm.FsmController.ARG_STATE_ID"

    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/car/fsm/FsmController$StackEntry;

    iget v0, v0, Lcom/google/android/car/fsm/FsmController$StackEntry;->e:I

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    if-eqz p1, :cond_0

    new-instance v0, Lcom/google/android/car/fsm/c;

    const/4 v2, 0x0

    invoke-direct {v0, p0, p1, v1, v2}, Lcom/google/android/car/fsm/c;-><init>(Lcom/google/android/car/fsm/FsmController;Ljava/lang/Class;Landroid/os/Bundle;Z)V

    invoke-virtual {p0, v0}, Lcom/google/android/car/fsm/FsmController;->a(Lcom/google/android/car/fsm/e;)V

    .line 441
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 201
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/os/Parcelable;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    .line 213
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->e:Lcom/google/android/car/fsm/j;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 215
    const-string v0, "FsmController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "fireEvent("

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    iget-boolean v0, p0, Lcom/google/android/car/fsm/FsmController;->g:Z

    if-eqz v0, :cond_1

    .line 217
    const-string v0, "FsmController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "State machine already stopped.  Event will be dropped: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    :cond_0
    return-void

    .line 220
    :cond_1
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->e:Lcom/google/android/car/fsm/j;

    if-nez v0, :cond_2

    .line 221
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Start state has not been initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 225
    :cond_2
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->e:Lcom/google/android/car/fsm/j;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/car/fsm/j;->a(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->e:Lcom/google/android/car/fsm/j;

    invoke-static {v0, p1}, Lcom/google/android/car/fsm/FsmController;->a(Lcom/google/android/car/fsm/j;Ljava/lang/String;)Lcom/google/android/car/fsm/k;

    move-result-object v0

    .line 234
    if-nez v0, :cond_3

    .line 235
    invoke-direct {p0}, Lcom/google/android/car/fsm/FsmController;->f()V

    .line 236
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Transition for event \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' not found on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/car/fsm/FsmController;->e:Lcom/google/android/car/fsm/j;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240
    :cond_3
    invoke-interface {v0}, Lcom/google/android/car/fsm/k;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 247
    iget-object v1, p0, Lcom/google/android/car/fsm/FsmController;->e:Lcom/google/android/car/fsm/j;

    .line 250
    iget-object v1, p0, Lcom/google/android/car/fsm/FsmController;->j:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 253
    invoke-interface {v0}, Lcom/google/android/car/fsm/k;->b()Ljava/lang/Class;

    move-result-object v6

    .line 255
    invoke-interface {v0}, Lcom/google/android/car/fsm/k;->c()Ljava/lang/Class;

    move-result-object v3

    .line 256
    invoke-interface {v0}, Lcom/google/android/car/fsm/k;->d()Ljava/lang/Class;

    move-result-object v4

    .line 257
    sget-object v0, Lcom/google/android/car/fsm/FsmController;->l:Ljava/lang/Class;

    if-ne v3, v0, :cond_4

    sget-object v0, Lcom/google/android/car/fsm/FsmController;->l:Ljava/lang/Class;

    if-eq v4, v0, :cond_5

    :cond_4
    move v1, v2

    .line 259
    :goto_0
    sget-object v0, Lcom/google/android/car/fsm/FsmController;->l:Ljava/lang/Class;

    if-ne v6, v0, :cond_6

    sget-object v0, Lcom/google/android/car/fsm/FsmController;->l:Ljava/lang/Class;

    if-ne v3, v0, :cond_6

    sget-object v0, Lcom/google/android/car/fsm/FsmController;->l:Ljava/lang/Class;

    if-ne v4, v0, :cond_6

    .line 260
    invoke-direct {p0}, Lcom/google/android/car/fsm/FsmController;->f()V

    .line 261
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Transition for "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " on "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/car/fsm/FsmController$StackEntry;

    iget-object v0, v0, Lcom/google/android/car/fsm/FsmController$StackEntry;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " must have at least a push or pop"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 257
    :cond_5
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 265
    :cond_6
    sget-object v0, Lcom/google/android/car/fsm/FsmController;->l:Ljava/lang/Class;

    if-eq v4, v0, :cond_7

    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/car/fsm/FsmController$StackEntry;

    iget-object v0, v0, Lcom/google/android/car/fsm/FsmController$StackEntry;->a:Ljava/lang/Class;

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 266
    invoke-direct {p0}, Lcom/google/android/car/fsm/FsmController;->f()V

    .line 267
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Transition for "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " on "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/car/fsm/FsmController$StackEntry;

    iget-object v0, v0, Lcom/google/android/car/fsm/FsmController$StackEntry;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " can\'t popTo itself."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 273
    :cond_7
    if-eqz v1, :cond_8

    .line 277
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v5, v0, -0x1

    :goto_1
    if-ltz v5, :cond_10

    .line 278
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/car/fsm/FsmController$StackEntry;

    .line 282
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v0, Lcom/google/android/car/fsm/FsmController$StackEntry;->a:Ljava/lang/Class;

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_10

    .line 284
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    iget-object v0, v0, Lcom/google/android/car/fsm/FsmController$StackEntry;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 287
    add-int/lit8 v0, v5, 0x1

    .line 293
    :goto_2
    if-ltz v0, :cond_a

    .line 294
    iget-object v3, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v0, v4}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 303
    :cond_8
    sget-object v0, Lcom/google/android/car/fsm/FsmController;->l:Ljava/lang/Class;

    if-ne v6, v0, :cond_d

    .line 304
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 305
    if-lez v0, :cond_c

    .line 307
    iget-object v1, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/car/fsm/FsmController$StackEntry;

    .line 308
    iget-object v1, v0, Lcom/google/android/car/fsm/FsmController$StackEntry;->a:Ljava/lang/Class;

    iget-object v2, v0, Lcom/google/android/car/fsm/FsmController$StackEntry;->b:Landroid/os/Parcelable;

    invoke-direct {p0, v1, v2}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/Class;Landroid/os/Parcelable;)Lcom/google/android/car/fsm/j;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/car/fsm/FsmController;->e:Lcom/google/android/car/fsm/j;

    iget-object v0, v0, Lcom/google/android/car/fsm/FsmController$StackEntry;->d:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/car/fsm/FsmController;->e()V

    .line 337
    :goto_3
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 338
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/car/fsm/FsmController$StackEntry;

    .line 339
    iget-object v0, v0, Lcom/google/android/car/fsm/FsmController$StackEntry;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 277
    :cond_9
    add-int/lit8 v5, v5, -0x1

    goto/16 :goto_1

    .line 296
    :cond_a
    invoke-direct {p0}, Lcom/google/android/car/fsm/FsmController;->f()V

    .line 297
    sget-object v0, Lcom/google/android/car/fsm/FsmController;->l:Ljava/lang/Class;

    if-eq v3, v0, :cond_b

    move-object v0, v3

    .line 299
    :goto_5
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "State "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " not found in stack"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_b
    move-object v0, v4

    .line 297
    goto :goto_5

    .line 312
    :cond_c
    iput-boolean v2, p0, Lcom/google/android/car/fsm/FsmController;->g:Z

    .line 313
    new-instance v0, Lcom/google/android/car/fsm/b;

    invoke-direct {v0, p0}, Lcom/google/android/car/fsm/b;-><init>(Lcom/google/android/car/fsm/FsmController;)V

    invoke-virtual {p0, v0}, Lcom/google/android/car/fsm/FsmController;->a(Lcom/google/android/car/fsm/e;)V

    goto :goto_3

    .line 322
    :cond_d
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 323
    iget-object v2, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 324
    if-ltz v2, :cond_e

    .line 325
    invoke-direct {p0}, Lcom/google/android/car/fsm/FsmController;->f()V

    .line 326
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "State already appears in the stack at index "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 329
    :cond_e
    if-nez v1, :cond_f

    .line 331
    iget-object v1, p0, Lcom/google/android/car/fsm/FsmController;->d:Ljava/util/ArrayList;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/car/fsm/FsmController$StackEntry;

    iput-object p1, v0, Lcom/google/android/car/fsm/FsmController$StackEntry;->d:Ljava/lang/String;

    .line 334
    :cond_f
    invoke-direct {p0, v6, p1, p2}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/Class;Ljava/lang/String;Landroid/os/Parcelable;)V

    goto/16 :goto_3

    :cond_10
    move v0, v5

    goto/16 :goto_2
.end method

.method public final b()Lcom/google/android/car/fsm/j;
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->e:Lcom/google/android/car/fsm/j;

    return-object v0
.end method

.method public final c()Lcom/google/android/car/fsm/g;
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lcom/google/android/car/fsm/FsmController;->m:Lcom/google/android/car/fsm/g;

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 522
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/car/fsm/FsmController;->i:Lcom/google/android/car/fsm/h;

    .line 523
    return-void
.end method
