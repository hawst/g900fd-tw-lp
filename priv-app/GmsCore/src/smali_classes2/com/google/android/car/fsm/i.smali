.class public Lcom/google/android/car/fsm/i;
.super Landroid/app/Fragment;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:I

.field private c:Lcom/google/android/car/fsm/j;

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/google/android/car/fsm/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/car/fsm/i;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 23
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/car/fsm/i;->d:Z

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/car/fsm/FsmController;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/google/android/car/fsm/i;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/car/fsm/h;

    invoke-interface {v0}, Lcom/google/android/car/fsm/h;->c()Lcom/google/android/car/fsm/FsmController;

    move-result-object v0

    return-object v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 28
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 29
    invoke-virtual {p0}, Lcom/google/android/car/fsm/i;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.google.android.fsm.FsmController.ARG_STATE_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/car/fsm/i;->b:I

    .line 30
    check-cast p1, Lcom/google/android/car/fsm/h;

    invoke-interface {p1}, Lcom/google/android/car/fsm/h;->c()Lcom/google/android/car/fsm/FsmController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/car/fsm/FsmController;->b()Lcom/google/android/car/fsm/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/car/fsm/i;->c:Lcom/google/android/car/fsm/j;

    .line 31
    sget-object v0, Lcom/google/android/car/fsm/i;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Started with mStateId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/car/fsm/i;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/car/fsm/i;->c:Lcom/google/android/car/fsm/j;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    return-void
.end method
