.class public Lcom/google/android/car/emulator/CarEmulatorInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Lcom/google/android/car/emulator/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/car/emulator/a;

    invoke-direct {v0}, Lcom/google/android/car/emulator/a;-><init>()V

    sput-object v0, Lcom/google/android/car/emulator/CarEmulatorInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/car/emulator/c;->a(Landroid/os/IBinder;)Lcom/google/android/car/emulator/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/car/emulator/CarEmulatorInfo;->a:Lcom/google/android/car/emulator/b;

    .line 85
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/google/android/car/emulator/CarEmulatorInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/car/emulator/b;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/car/emulator/CarEmulatorInfo;->a:Lcom/google/android/car/emulator/b;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/car/emulator/CarEmulatorInfo;->a:Lcom/google/android/car/emulator/b;

    invoke-interface {v0}, Lcom/google/android/car/emulator/b;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 70
    return-void
.end method
