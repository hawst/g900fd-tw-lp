.class public Lcom/google/android/car/fsm/g;
.super Landroid/app/Fragment;
.source "SourceFile"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public a:Lcom/google/android/car/fsm/FsmController;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/google/android/car/fsm/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/car/fsm/g;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 21
    return-void
.end method


# virtual methods
.method public final k_()Lcom/google/android/car/fsm/FsmController;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/car/fsm/g;->a:Lcom/google/android/car/fsm/FsmController;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 66
    invoke-super {p0, p1}, Landroid/app/Fragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/car/fsm/g;->a:Lcom/google/android/car/fsm/FsmController;

    invoke-virtual {v0}, Lcom/google/android/car/fsm/FsmController;->b()Lcom/google/android/car/fsm/j;

    move-result-object v0

    if-nez v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/google/android/car/fsm/g;->a:Lcom/google/android/car/fsm/FsmController;

    invoke-virtual {v0}, Lcom/google/android/car/fsm/FsmController;->a()V

    .line 71
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 25
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 26
    iget-object v0, p0, Lcom/google/android/car/fsm/g;->a:Lcom/google/android/car/fsm/FsmController;

    if-eqz v0, :cond_0

    .line 28
    iget-object v1, p0, Lcom/google/android/car/fsm/g;->a:Lcom/google/android/car/fsm/FsmController;

    move-object v0, p1

    check-cast v0, Lcom/google/android/car/fsm/h;

    invoke-virtual {v1, v0}, Lcom/google/android/car/fsm/FsmController;->a(Lcom/google/android/car/fsm/h;)V

    .line 29
    check-cast p1, Lcom/google/android/car/fsm/h;

    iget-object v0, p0, Lcom/google/android/car/fsm/g;->a:Lcom/google/android/car/fsm/FsmController;

    invoke-interface {p1, v0}, Lcom/google/android/car/fsm/h;->a(Lcom/google/android/car/fsm/FsmController;)V

    .line 31
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 43
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 45
    invoke-virtual {p0}, Lcom/google/android/car/fsm/g;->getActivity()Landroid/app/Activity;

    move-result-object v1

    move-object v6, v1

    .line 46
    check-cast v6, Lcom/google/android/car/fsm/h;

    .line 50
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/car/fsm/g;->setRetainInstance(Z)V

    .line 52
    new-instance v0, Lcom/google/android/car/fsm/FsmController;

    invoke-interface {v6}, Lcom/google/android/car/fsm/h;->a()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v6}, Lcom/google/android/car/fsm/h;->h_()Ljava/util/List;

    move-result-object v3

    move-object v4, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/car/fsm/FsmController;-><init>(Landroid/content/Context;Ljava/lang/Class;Ljava/util/List;Lcom/google/android/car/fsm/g;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/google/android/car/fsm/g;->a:Lcom/google/android/car/fsm/FsmController;

    .line 60
    iget-object v0, p0, Lcom/google/android/car/fsm/g;->a:Lcom/google/android/car/fsm/FsmController;

    invoke-interface {v6, v0}, Lcom/google/android/car/fsm/h;->a(Lcom/google/android/car/fsm/FsmController;)V

    .line 61
    iget-object v0, p0, Lcom/google/android/car/fsm/g;->a:Lcom/google/android/car/fsm/FsmController;

    invoke-virtual {v0, v6}, Lcom/google/android/car/fsm/FsmController;->a(Lcom/google/android/car/fsm/h;)V

    .line 62
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/car/fsm/g;->a:Lcom/google/android/car/fsm/FsmController;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/google/android/car/fsm/g;->a:Lcom/google/android/car/fsm/FsmController;

    invoke-virtual {v0}, Lcom/google/android/car/fsm/FsmController;->d()V

    .line 38
    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 39
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 75
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 76
    iget-object v0, p0, Lcom/google/android/car/fsm/g;->a:Lcom/google/android/car/fsm/FsmController;

    invoke-virtual {v0, p1}, Lcom/google/android/car/fsm/FsmController;->a(Landroid/os/Bundle;)V

    .line 77
    return-void
.end method
