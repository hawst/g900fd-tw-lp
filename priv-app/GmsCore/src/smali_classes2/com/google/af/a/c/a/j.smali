.class public final Lcom/google/af/a/c/a/j;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/af/a/c/a/e;

.field public b:Ljava/lang/String;

.field public c:[I

.field public d:Ljava/lang/String;

.field public e:Lcom/google/af/a/c/a/h;

.field public f:Lcom/google/af/a/c/a/a;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 65
    iput-object v1, p0, Lcom/google/af/a/c/a/j;->a:Lcom/google/af/a/c/a/e;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/c/a/j;->b:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/af/a/c/a/j;->c:[I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/c/a/j;->d:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/af/a/c/a/j;->e:Lcom/google/af/a/c/a/h;

    iput-object v1, p0, Lcom/google/af/a/c/a/j;->f:Lcom/google/af/a/c/a/a;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/c/a/j;->cachedSize:I

    .line 66
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 182
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 183
    iget-object v2, p0, Lcom/google/af/a/c/a/j;->a:Lcom/google/af/a/c/a/e;

    if-eqz v2, :cond_0

    .line 184
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/af/a/c/a/j;->a:Lcom/google/af/a/c/a/e;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 187
    :cond_0
    iget-object v2, p0, Lcom/google/af/a/c/a/j;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 188
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/af/a/c/a/j;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 191
    :cond_1
    iget-object v2, p0, Lcom/google/af/a/c/a/j;->c:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/af/a/c/a/j;->c:[I

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    .line 193
    :goto_0
    iget-object v3, p0, Lcom/google/af/a/c/a/j;->c:[I

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 194
    iget-object v3, p0, Lcom/google/af/a/c/a/j;->c:[I

    aget v3, v3, v1

    .line 195
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 193
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 198
    :cond_2
    add-int/2addr v0, v2

    .line 199
    iget-object v1, p0, Lcom/google/af/a/c/a/j;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 201
    :cond_3
    iget-object v1, p0, Lcom/google/af/a/c/a/j;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 202
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/af/a/c/a/j;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 205
    :cond_4
    iget-object v1, p0, Lcom/google/af/a/c/a/j;->e:Lcom/google/af/a/c/a/h;

    if-eqz v1, :cond_5

    .line 206
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/af/a/c/a/j;->e:Lcom/google/af/a/c/a/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 209
    :cond_5
    iget-object v1, p0, Lcom/google/af/a/c/a/j;->f:Lcom/google/af/a/c/a/a;

    if-eqz v1, :cond_6

    .line 210
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/af/a/c/a/j;->f:Lcom/google/af/a/c/a/a;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 213
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 81
    if-ne p1, p0, :cond_1

    .line 133
    :cond_0
    :goto_0
    return v0

    .line 84
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/c/a/j;

    if-nez v2, :cond_2

    move v0, v1

    .line 85
    goto :goto_0

    .line 87
    :cond_2
    check-cast p1, Lcom/google/af/a/c/a/j;

    .line 88
    iget-object v2, p0, Lcom/google/af/a/c/a/j;->a:Lcom/google/af/a/c/a/e;

    if-nez v2, :cond_3

    .line 89
    iget-object v2, p1, Lcom/google/af/a/c/a/j;->a:Lcom/google/af/a/c/a/e;

    if-eqz v2, :cond_4

    move v0, v1

    .line 90
    goto :goto_0

    .line 93
    :cond_3
    iget-object v2, p0, Lcom/google/af/a/c/a/j;->a:Lcom/google/af/a/c/a/e;

    iget-object v3, p1, Lcom/google/af/a/c/a/j;->a:Lcom/google/af/a/c/a/e;

    invoke-virtual {v2, v3}, Lcom/google/af/a/c/a/e;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 94
    goto :goto_0

    .line 97
    :cond_4
    iget-object v2, p0, Lcom/google/af/a/c/a/j;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 98
    iget-object v2, p1, Lcom/google/af/a/c/a/j;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 99
    goto :goto_0

    .line 101
    :cond_5
    iget-object v2, p0, Lcom/google/af/a/c/a/j;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/c/a/j;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 102
    goto :goto_0

    .line 104
    :cond_6
    iget-object v2, p0, Lcom/google/af/a/c/a/j;->c:[I

    iget-object v3, p1, Lcom/google/af/a/c/a/j;->c:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 106
    goto :goto_0

    .line 108
    :cond_7
    iget-object v2, p0, Lcom/google/af/a/c/a/j;->d:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 109
    iget-object v2, p1, Lcom/google/af/a/c/a/j;->d:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    .line 110
    goto :goto_0

    .line 112
    :cond_8
    iget-object v2, p0, Lcom/google/af/a/c/a/j;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/c/a/j;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 113
    goto :goto_0

    .line 115
    :cond_9
    iget-object v2, p0, Lcom/google/af/a/c/a/j;->e:Lcom/google/af/a/c/a/h;

    if-nez v2, :cond_a

    .line 116
    iget-object v2, p1, Lcom/google/af/a/c/a/j;->e:Lcom/google/af/a/c/a/h;

    if-eqz v2, :cond_b

    move v0, v1

    .line 117
    goto :goto_0

    .line 120
    :cond_a
    iget-object v2, p0, Lcom/google/af/a/c/a/j;->e:Lcom/google/af/a/c/a/h;

    iget-object v3, p1, Lcom/google/af/a/c/a/j;->e:Lcom/google/af/a/c/a/h;

    invoke-virtual {v2, v3}, Lcom/google/af/a/c/a/h;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 121
    goto :goto_0

    .line 124
    :cond_b
    iget-object v2, p0, Lcom/google/af/a/c/a/j;->f:Lcom/google/af/a/c/a/a;

    if-nez v2, :cond_c

    .line 125
    iget-object v2, p1, Lcom/google/af/a/c/a/j;->f:Lcom/google/af/a/c/a/a;

    if-eqz v2, :cond_0

    move v0, v1

    .line 126
    goto :goto_0

    .line 129
    :cond_c
    iget-object v2, p0, Lcom/google/af/a/c/a/j;->f:Lcom/google/af/a/c/a/a;

    iget-object v3, p1, Lcom/google/af/a/c/a/j;->f:Lcom/google/af/a/c/a/a;

    invoke-virtual {v2, v3}, Lcom/google/af/a/c/a/a;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 130
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 138
    iget-object v0, p0, Lcom/google/af/a/c/a/j;->a:Lcom/google/af/a/c/a/e;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 141
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/c/a/j;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 143
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/af/a/c/a/j;->c:[I

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v2

    add-int/2addr v0, v2

    .line 145
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/c/a/j;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 147
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/c/a/j;->e:Lcom/google/af/a/c/a/h;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 149
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/af/a/c/a/j;->f:Lcom/google/af/a/c/a/a;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 151
    return v0

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/c/a/j;->a:Lcom/google/af/a/c/a/e;

    invoke-virtual {v0}, Lcom/google/af/a/c/a/e;->hashCode()I

    move-result v0

    goto :goto_0

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/c/a/j;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 145
    :cond_2
    iget-object v0, p0, Lcom/google/af/a/c/a/j;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 147
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/c/a/j;->e:Lcom/google/af/a/c/a/h;

    invoke-virtual {v0}, Lcom/google/af/a/c/a/h;->hashCode()I

    move-result v0

    goto :goto_3

    .line 149
    :cond_4
    iget-object v1, p0, Lcom/google/af/a/c/a/j;->f:Lcom/google/af/a/c/a/a;

    invoke-virtual {v1}, Lcom/google/af/a/c/a/a;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/af/a/c/a/j;->a:Lcom/google/af/a/c/a/e;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/af/a/c/a/e;

    invoke-direct {v0}, Lcom/google/af/a/c/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/c/a/j;->a:Lcom/google/af/a/c/a/e;

    :cond_1
    iget-object v0, p0, Lcom/google/af/a/c/a/j;->a:Lcom/google/af/a/c/a/e;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/c/a/j;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_3
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/af/a/c/a/j;->c:[I

    if-nez v0, :cond_4

    move v0, v2

    :goto_3
    if-nez v0, :cond_5

    array-length v3, v5

    if-ne v1, v3, :cond_5

    iput-object v5, p0, Lcom/google/af/a/c/a/j;->c:[I

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/af/a/c/a/j;->c:[I

    array-length v0, v0

    goto :goto_3

    :cond_5
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_6

    iget-object v4, p0, Lcom/google/af/a/c/a/j;->c:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/af/a/c/a/j;->c:[I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    if-eqz v0, :cond_b

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/af/a/c/a/j;->c:[I

    if-nez v1, :cond_9

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_8

    iget-object v0, p0, Lcom/google/af/a/c/a/j;->c:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_a

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_9
    iget-object v1, p0, Lcom/google/af/a/c/a/j;->c:[I

    array-length v1, v1

    goto :goto_5

    :cond_a
    iput-object v4, p0, Lcom/google/af/a/c/a/j;->c:[I

    :cond_b
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/c/a/j;->d:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/af/a/c/a/j;->e:Lcom/google/af/a/c/a/h;

    if-nez v0, :cond_c

    new-instance v0, Lcom/google/af/a/c/a/h;

    invoke-direct {v0}, Lcom/google/af/a/c/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/c/a/j;->e:Lcom/google/af/a/c/a/h;

    :cond_c
    iget-object v0, p0, Lcom/google/af/a/c/a/j;->e:Lcom/google/af/a/c/a/h;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/af/a/c/a/j;->f:Lcom/google/af/a/c/a/a;

    if-nez v0, :cond_d

    new-instance v0, Lcom/google/af/a/c/a/a;

    invoke-direct {v0}, Lcom/google/af/a/c/a/a;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/c/a/j;->f:Lcom/google/af/a/c/a/a;

    :cond_d
    iget-object v0, p0, Lcom/google/af/a/c/a/j;->f:Lcom/google/af/a/c/a/a;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x22 -> :sswitch_5
        0x2a -> :sswitch_6
        0x32 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/af/a/c/a/j;->a:Lcom/google/af/a/c/a/e;

    if-eqz v0, :cond_0

    .line 158
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/af/a/c/a/j;->a:Lcom/google/af/a/c/a/e;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/c/a/j;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 161
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/af/a/c/a/j;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/c/a/j;->c:[I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/af/a/c/a/j;->c:[I

    array-length v0, v0

    if-lez v0, :cond_2

    .line 164
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/af/a/c/a/j;->c:[I

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 165
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/af/a/c/a/j;->c:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 164
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 168
    :cond_2
    iget-object v0, p0, Lcom/google/af/a/c/a/j;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 169
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/af/a/c/a/j;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 171
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/c/a/j;->e:Lcom/google/af/a/c/a/h;

    if-eqz v0, :cond_4

    .line 172
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/af/a/c/a/j;->e:Lcom/google/af/a/c/a/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 174
    :cond_4
    iget-object v0, p0, Lcom/google/af/a/c/a/j;->f:Lcom/google/af/a/c/a/a;

    if-eqz v0, :cond_5

    .line 175
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/af/a/c/a/j;->f:Lcom/google/af/a/c/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 177
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 178
    return-void
.end method
