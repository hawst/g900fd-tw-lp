.class public final Lcom/google/af/a/c/a;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:J

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:J

.field public g:J

.field public h:J

.field public i:Lcom/google/af/a/c/b;

.field public j:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 53
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/c/a;->a:Ljava/lang/String;

    iput-wide v2, p0, Lcom/google/af/a/c/a;->b:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/c/a;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/c/a;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/c/a;->e:Ljava/lang/String;

    iput-wide v2, p0, Lcom/google/af/a/c/a;->f:J

    iput-wide v2, p0, Lcom/google/af/a/c/a;->g:J

    iput-wide v2, p0, Lcom/google/af/a/c/a;->h:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/af/a/c/a;->i:Lcom/google/af/a/c/b;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/af/a/c/a;->j:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/c/a;->cachedSize:I

    .line 55
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 199
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 200
    iget-object v1, p0, Lcom/google/af/a/c/a;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 201
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/af/a/c/a;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 204
    :cond_0
    iget-wide v2, p0, Lcom/google/af/a/c/a;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 205
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/af/a/c/a;->b:J

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 208
    :cond_1
    iget-object v1, p0, Lcom/google/af/a/c/a;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 209
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/af/a/c/a;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 212
    :cond_2
    iget-object v1, p0, Lcom/google/af/a/c/a;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 213
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/af/a/c/a;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 216
    :cond_3
    iget-object v1, p0, Lcom/google/af/a/c/a;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 217
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/af/a/c/a;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 220
    :cond_4
    iget-wide v2, p0, Lcom/google/af/a/c/a;->f:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 221
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/google/af/a/c/a;->f:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 224
    :cond_5
    iget-wide v2, p0, Lcom/google/af/a/c/a;->g:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_6

    .line 225
    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/google/af/a/c/a;->g:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 228
    :cond_6
    iget-wide v2, p0, Lcom/google/af/a/c/a;->h:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_7

    .line 229
    const/16 v1, 0x8

    iget-wide v2, p0, Lcom/google/af/a/c/a;->h:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 232
    :cond_7
    iget-object v1, p0, Lcom/google/af/a/c/a;->i:Lcom/google/af/a/c/b;

    if-eqz v1, :cond_8

    .line 233
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/af/a/c/a;->i:Lcom/google/af/a/c/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 236
    :cond_8
    iget v1, p0, Lcom/google/af/a/c/a;->j:I

    if-eqz v1, :cond_9

    .line 237
    const/16 v1, 0xa

    iget v2, p0, Lcom/google/af/a/c/a;->j:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 240
    :cond_9
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 74
    if-ne p1, p0, :cond_1

    .line 133
    :cond_0
    :goto_0
    return v0

    .line 77
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/c/a;

    if-nez v2, :cond_2

    move v0, v1

    .line 78
    goto :goto_0

    .line 80
    :cond_2
    check-cast p1, Lcom/google/af/a/c/a;

    .line 81
    iget-object v2, p0, Lcom/google/af/a/c/a;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 82
    iget-object v2, p1, Lcom/google/af/a/c/a;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 83
    goto :goto_0

    .line 85
    :cond_3
    iget-object v2, p0, Lcom/google/af/a/c/a;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/c/a;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 86
    goto :goto_0

    .line 88
    :cond_4
    iget-wide v2, p0, Lcom/google/af/a/c/a;->b:J

    iget-wide v4, p1, Lcom/google/af/a/c/a;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move v0, v1

    .line 89
    goto :goto_0

    .line 91
    :cond_5
    iget-object v2, p0, Lcom/google/af/a/c/a;->c:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 92
    iget-object v2, p1, Lcom/google/af/a/c/a;->c:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    .line 93
    goto :goto_0

    .line 95
    :cond_6
    iget-object v2, p0, Lcom/google/af/a/c/a;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/c/a;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 96
    goto :goto_0

    .line 98
    :cond_7
    iget-object v2, p0, Lcom/google/af/a/c/a;->d:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 99
    iget-object v2, p1, Lcom/google/af/a/c/a;->d:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    .line 100
    goto :goto_0

    .line 102
    :cond_8
    iget-object v2, p0, Lcom/google/af/a/c/a;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/c/a;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 103
    goto :goto_0

    .line 105
    :cond_9
    iget-object v2, p0, Lcom/google/af/a/c/a;->e:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 106
    iget-object v2, p1, Lcom/google/af/a/c/a;->e:Ljava/lang/String;

    if-eqz v2, :cond_b

    move v0, v1

    .line 107
    goto :goto_0

    .line 109
    :cond_a
    iget-object v2, p0, Lcom/google/af/a/c/a;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/c/a;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 110
    goto :goto_0

    .line 112
    :cond_b
    iget-wide v2, p0, Lcom/google/af/a/c/a;->f:J

    iget-wide v4, p1, Lcom/google/af/a/c/a;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_c

    move v0, v1

    .line 113
    goto :goto_0

    .line 115
    :cond_c
    iget-wide v2, p0, Lcom/google/af/a/c/a;->g:J

    iget-wide v4, p1, Lcom/google/af/a/c/a;->g:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_d

    move v0, v1

    .line 116
    goto :goto_0

    .line 118
    :cond_d
    iget-wide v2, p0, Lcom/google/af/a/c/a;->h:J

    iget-wide v4, p1, Lcom/google/af/a/c/a;->h:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_e

    move v0, v1

    .line 119
    goto/16 :goto_0

    .line 121
    :cond_e
    iget-object v2, p0, Lcom/google/af/a/c/a;->i:Lcom/google/af/a/c/b;

    if-nez v2, :cond_f

    .line 122
    iget-object v2, p1, Lcom/google/af/a/c/a;->i:Lcom/google/af/a/c/b;

    if-eqz v2, :cond_10

    move v0, v1

    .line 123
    goto/16 :goto_0

    .line 126
    :cond_f
    iget-object v2, p0, Lcom/google/af/a/c/a;->i:Lcom/google/af/a/c/b;

    iget-object v3, p1, Lcom/google/af/a/c/a;->i:Lcom/google/af/a/c/b;

    invoke-virtual {v2, v3}, Lcom/google/af/a/c/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    move v0, v1

    .line 127
    goto/16 :goto_0

    .line 130
    :cond_10
    iget v2, p0, Lcom/google/af/a/c/a;->j:I

    iget v3, p1, Lcom/google/af/a/c/a;->j:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 131
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    .line 138
    iget-object v0, p0, Lcom/google/af/a/c/a;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 141
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/af/a/c/a;->b:J

    iget-wide v4, p0, Lcom/google/af/a/c/a;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 143
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/c/a;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 145
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/c/a;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 147
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/c/a;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 149
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/af/a/c/a;->f:J

    iget-wide v4, p0, Lcom/google/af/a/c/a;->f:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 151
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/af/a/c/a;->g:J

    iget-wide v4, p0, Lcom/google/af/a/c/a;->g:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 153
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/af/a/c/a;->h:J

    iget-wide v4, p0, Lcom/google/af/a/c/a;->h:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 155
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/af/a/c/a;->i:Lcom/google/af/a/c/b;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 157
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/af/a/c/a;->j:I

    add-int/2addr v0, v1

    .line 158
    return v0

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/c/a;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 143
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/c/a;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 145
    :cond_2
    iget-object v0, p0, Lcom/google/af/a/c/a;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 147
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/c/a;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 155
    :cond_4
    iget-object v1, p0, Lcom/google/af/a/c/a;->i:Lcom/google/af/a/c/b;

    invoke-virtual {v1}, Lcom/google/af/a/c/b;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/c/a;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/af/a/c/a;->b:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/c/a;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/c/a;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/c/a;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/af/a/c/a;->f:J

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/af/a/c/a;->g:J

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/af/a/c/a;->h:J

    goto :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/af/a/c/a;->i:Lcom/google/af/a/c/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/af/a/c/b;

    invoke-direct {v0}, Lcom/google/af/a/c/b;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/c/a;->i:Lcom/google/af/a/c/b;

    :cond_1
    iget-object v0, p0, Lcom/google/af/a/c/a;->i:Lcom/google/af/a/c/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/af/a/c/a;->j:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x11 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 164
    iget-object v0, p0, Lcom/google/af/a/c/a;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/af/a/c/a;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 167
    :cond_0
    iget-wide v0, p0, Lcom/google/af/a/c/a;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 168
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/af/a/c/a;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)V

    .line 170
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/c/a;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 171
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/af/a/c/a;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 173
    :cond_2
    iget-object v0, p0, Lcom/google/af/a/c/a;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 174
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/af/a/c/a;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 176
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/c/a;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 177
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/af/a/c/a;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 179
    :cond_4
    iget-wide v0, p0, Lcom/google/af/a/c/a;->f:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_5

    .line 180
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/af/a/c/a;->f:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 182
    :cond_5
    iget-wide v0, p0, Lcom/google/af/a/c/a;->g:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_6

    .line 183
    const/4 v0, 0x7

    iget-wide v2, p0, Lcom/google/af/a/c/a;->g:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 185
    :cond_6
    iget-wide v0, p0, Lcom/google/af/a/c/a;->h:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_7

    .line 186
    const/16 v0, 0x8

    iget-wide v2, p0, Lcom/google/af/a/c/a;->h:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 188
    :cond_7
    iget-object v0, p0, Lcom/google/af/a/c/a;->i:Lcom/google/af/a/c/b;

    if-eqz v0, :cond_8

    .line 189
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/af/a/c/a;->i:Lcom/google/af/a/c/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 191
    :cond_8
    iget v0, p0, Lcom/google/af/a/c/a;->j:I

    if-eqz v0, :cond_9

    .line 192
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/af/a/c/a;->j:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 194
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 195
    return-void
.end method
