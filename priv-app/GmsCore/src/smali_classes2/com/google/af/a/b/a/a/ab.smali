.class public final Lcom/google/af/a/b/a/a/ab;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1945
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1946
    iput-object v0, p0, Lcom/google/af/a/b/a/a/ab;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ab;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ab;->c:Ljava/lang/Long;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/b/a/a/ab;->cachedSize:I

    .line 1947
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 2015
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2016
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/af/a/b/a/a/ab;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2018
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/af/a/b/a/a/ab;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2020
    iget-object v1, p0, Lcom/google/af/a/b/a/a/ab;->c:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 2021
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/af/a/b/a/a/ab;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2024
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1959
    if-ne p1, p0, :cond_1

    .line 1987
    :cond_0
    :goto_0
    return v0

    .line 1962
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/b/a/a/ab;

    if-nez v2, :cond_2

    move v0, v1

    .line 1963
    goto :goto_0

    .line 1965
    :cond_2
    check-cast p1, Lcom/google/af/a/b/a/a/ab;

    .line 1966
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ab;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 1967
    iget-object v2, p1, Lcom/google/af/a/b/a/a/ab;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 1968
    goto :goto_0

    .line 1970
    :cond_3
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ab;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/ab;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1971
    goto :goto_0

    .line 1973
    :cond_4
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ab;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 1974
    iget-object v2, p1, Lcom/google/af/a/b/a/a/ab;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 1975
    goto :goto_0

    .line 1977
    :cond_5
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ab;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/ab;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1978
    goto :goto_0

    .line 1980
    :cond_6
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ab;->c:Ljava/lang/Long;

    if-nez v2, :cond_7

    .line 1981
    iget-object v2, p1, Lcom/google/af/a/b/a/a/ab;->c:Ljava/lang/Long;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1982
    goto :goto_0

    .line 1984
    :cond_7
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ab;->c:Ljava/lang/Long;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/ab;->c:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1985
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1992
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ab;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1995
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/b/a/a/ab;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1997
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/af/a/b/a/a/ab;->c:Ljava/lang/Long;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 1999
    return v0

    .line 1992
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ab;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1995
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ab;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1997
    :cond_2
    iget-object v1, p0, Lcom/google/af/a/b/a/a/ab;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 1919
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ab;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ab;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ab;->c:Ljava/lang/Long;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 2005
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/af/a/b/a/a/ab;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2006
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/af/a/b/a/a/ab;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2007
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ab;->c:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2008
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/af/a/b/a/a/ab;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2010
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2011
    return-void
.end method
