.class public final Lcom/google/af/a/b/a/a/d;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile h:[Lcom/google/af/a/b/a/a/d;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Integer;

.field public e:Lcom/google/af/a/b/a/a/e;

.field public f:Lcom/google/af/a/b/a/a/g;

.field public g:Lcom/google/af/a/b/a/a/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 487
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 488
    iput-object v0, p0, Lcom/google/af/a/b/a/a/d;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/d;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/d;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/d;->d:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/d;->e:Lcom/google/af/a/b/a/a/e;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/d;->f:Lcom/google/af/a/b/a/a/g;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/d;->g:Lcom/google/af/a/b/a/a/b;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/b/a/a/d;->cachedSize:I

    .line 489
    return-void
.end method

.method public static a()[Lcom/google/af/a/b/a/a/d;
    .locals 2

    .prologue
    .line 455
    sget-object v0, Lcom/google/af/a/b/a/a/d;->h:[Lcom/google/af/a/b/a/a/d;

    if-nez v0, :cond_1

    .line 456
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 458
    :try_start_0
    sget-object v0, Lcom/google/af/a/b/a/a/d;->h:[Lcom/google/af/a/b/a/a/d;

    if-nez v0, :cond_0

    .line 459
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/af/a/b/a/a/d;

    sput-object v0, Lcom/google/af/a/b/a/a/d;->h:[Lcom/google/af/a/b/a/a/d;

    .line 461
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 463
    :cond_1
    sget-object v0, Lcom/google/af/a/b/a/a/d;->h:[Lcom/google/af/a/b/a/a/d;

    return-object v0

    .line 461
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 617
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 618
    iget-object v1, p0, Lcom/google/af/a/b/a/a/d;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 619
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/af/a/b/a/a/d;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 622
    :cond_0
    iget-object v1, p0, Lcom/google/af/a/b/a/a/d;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 623
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/af/a/b/a/a/d;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 626
    :cond_1
    iget-object v1, p0, Lcom/google/af/a/b/a/a/d;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 627
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/af/a/b/a/a/d;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 630
    :cond_2
    iget-object v1, p0, Lcom/google/af/a/b/a/a/d;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 631
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/af/a/b/a/a/d;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 634
    :cond_3
    iget-object v1, p0, Lcom/google/af/a/b/a/a/d;->e:Lcom/google/af/a/b/a/a/e;

    if-eqz v1, :cond_4

    .line 635
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/af/a/b/a/a/d;->e:Lcom/google/af/a/b/a/a/e;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 638
    :cond_4
    iget-object v1, p0, Lcom/google/af/a/b/a/a/d;->f:Lcom/google/af/a/b/a/a/g;

    if-eqz v1, :cond_5

    .line 639
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/af/a/b/a/a/d;->f:Lcom/google/af/a/b/a/a/g;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 642
    :cond_5
    iget-object v1, p0, Lcom/google/af/a/b/a/a/d;->g:Lcom/google/af/a/b/a/a/b;

    if-eqz v1, :cond_6

    .line 643
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/af/a/b/a/a/d;->g:Lcom/google/af/a/b/a/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 646
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 505
    if-ne p1, p0, :cond_1

    .line 566
    :cond_0
    :goto_0
    return v0

    .line 508
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/b/a/a/d;

    if-nez v2, :cond_2

    move v0, v1

    .line 509
    goto :goto_0

    .line 511
    :cond_2
    check-cast p1, Lcom/google/af/a/b/a/a/d;

    .line 512
    iget-object v2, p0, Lcom/google/af/a/b/a/a/d;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 513
    iget-object v2, p1, Lcom/google/af/a/b/a/a/d;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 514
    goto :goto_0

    .line 516
    :cond_3
    iget-object v2, p0, Lcom/google/af/a/b/a/a/d;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/d;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 517
    goto :goto_0

    .line 519
    :cond_4
    iget-object v2, p0, Lcom/google/af/a/b/a/a/d;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 520
    iget-object v2, p1, Lcom/google/af/a/b/a/a/d;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 521
    goto :goto_0

    .line 523
    :cond_5
    iget-object v2, p0, Lcom/google/af/a/b/a/a/d;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/d;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 524
    goto :goto_0

    .line 526
    :cond_6
    iget-object v2, p0, Lcom/google/af/a/b/a/a/d;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 527
    iget-object v2, p1, Lcom/google/af/a/b/a/a/d;->c:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 528
    goto :goto_0

    .line 530
    :cond_7
    iget-object v2, p0, Lcom/google/af/a/b/a/a/d;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/d;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 531
    goto :goto_0

    .line 533
    :cond_8
    iget-object v2, p0, Lcom/google/af/a/b/a/a/d;->d:Ljava/lang/Integer;

    if-nez v2, :cond_9

    .line 534
    iget-object v2, p1, Lcom/google/af/a/b/a/a/d;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_a

    move v0, v1

    .line 535
    goto :goto_0

    .line 537
    :cond_9
    iget-object v2, p0, Lcom/google/af/a/b/a/a/d;->d:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/d;->d:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 538
    goto :goto_0

    .line 539
    :cond_a
    iget-object v2, p0, Lcom/google/af/a/b/a/a/d;->e:Lcom/google/af/a/b/a/a/e;

    if-nez v2, :cond_b

    .line 540
    iget-object v2, p1, Lcom/google/af/a/b/a/a/d;->e:Lcom/google/af/a/b/a/a/e;

    if-eqz v2, :cond_c

    move v0, v1

    .line 541
    goto :goto_0

    .line 544
    :cond_b
    iget-object v2, p0, Lcom/google/af/a/b/a/a/d;->e:Lcom/google/af/a/b/a/a/e;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/d;->e:Lcom/google/af/a/b/a/a/e;

    invoke-virtual {v2, v3}, Lcom/google/af/a/b/a/a/e;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 545
    goto :goto_0

    .line 548
    :cond_c
    iget-object v2, p0, Lcom/google/af/a/b/a/a/d;->f:Lcom/google/af/a/b/a/a/g;

    if-nez v2, :cond_d

    .line 549
    iget-object v2, p1, Lcom/google/af/a/b/a/a/d;->f:Lcom/google/af/a/b/a/a/g;

    if-eqz v2, :cond_e

    move v0, v1

    .line 550
    goto :goto_0

    .line 553
    :cond_d
    iget-object v2, p0, Lcom/google/af/a/b/a/a/d;->f:Lcom/google/af/a/b/a/a/g;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/d;->f:Lcom/google/af/a/b/a/a/g;

    invoke-virtual {v2, v3}, Lcom/google/af/a/b/a/a/g;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 554
    goto/16 :goto_0

    .line 557
    :cond_e
    iget-object v2, p0, Lcom/google/af/a/b/a/a/d;->g:Lcom/google/af/a/b/a/a/b;

    if-nez v2, :cond_f

    .line 558
    iget-object v2, p1, Lcom/google/af/a/b/a/a/d;->g:Lcom/google/af/a/b/a/a/b;

    if-eqz v2, :cond_0

    move v0, v1

    .line 559
    goto/16 :goto_0

    .line 562
    :cond_f
    iget-object v2, p0, Lcom/google/af/a/b/a/a/d;->g:Lcom/google/af/a/b/a/a/b;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/d;->g:Lcom/google/af/a/b/a/a/b;

    invoke-virtual {v2, v3}, Lcom/google/af/a/b/a/a/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 563
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 571
    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 574
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 576
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 578
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->d:Ljava/lang/Integer;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 579
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->e:Lcom/google/af/a/b/a/a/e;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 581
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->f:Lcom/google/af/a/b/a/a/g;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 583
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/af/a/b/a/a/d;->g:Lcom/google/af/a/b/a/a/b;

    if-nez v2, :cond_6

    :goto_6
    add-int/2addr v0, v1

    .line 585
    return v0

    .line 571
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 574
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 576
    :cond_2
    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 578
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_3

    .line 579
    :cond_4
    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->e:Lcom/google/af/a/b/a/a/e;

    invoke-virtual {v0}, Lcom/google/af/a/b/a/a/e;->hashCode()I

    move-result v0

    goto :goto_4

    .line 581
    :cond_5
    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->f:Lcom/google/af/a/b/a/a/g;

    invoke-virtual {v0}, Lcom/google/af/a/b/a/a/g;->hashCode()I

    move-result v0

    goto :goto_5

    .line 583
    :cond_6
    iget-object v1, p0, Lcom/google/af/a/b/a/a/d;->g:Lcom/google/af/a/b/a/a/b;

    invoke-virtual {v1}, Lcom/google/af/a/b/a/a/b;->hashCode()I

    move-result v1

    goto :goto_6
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 445
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/d;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/d;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/d;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/d;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->e:Lcom/google/af/a/b/a/a/e;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/af/a/b/a/a/e;

    invoke-direct {v0}, Lcom/google/af/a/b/a/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/b/a/a/d;->e:Lcom/google/af/a/b/a/a/e;

    :cond_1
    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->e:Lcom/google/af/a/b/a/a/e;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->f:Lcom/google/af/a/b/a/a/g;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/af/a/b/a/a/g;

    invoke-direct {v0}, Lcom/google/af/a/b/a/a/g;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/b/a/a/d;->f:Lcom/google/af/a/b/a/a/g;

    :cond_2
    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->f:Lcom/google/af/a/b/a/a/g;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->g:Lcom/google/af/a/b/a/a/b;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/af/a/b/a/a/b;

    invoke-direct {v0}, Lcom/google/af/a/b/a/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/b/a/a/d;->g:Lcom/google/af/a/b/a/a/b;

    :cond_3
    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->g:Lcom/google/af/a/b/a/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x22 -> :sswitch_2
        0x2a -> :sswitch_3
        0x30 -> :sswitch_4
        0x3a -> :sswitch_5
        0x42 -> :sswitch_6
        0x4a -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 591
    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 592
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/af/a/b/a/a/d;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 594
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 595
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/af/a/b/a/a/d;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 597
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 598
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/af/a/b/a/a/d;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 600
    :cond_2
    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 601
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/af/a/b/a/a/d;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 603
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->e:Lcom/google/af/a/b/a/a/e;

    if-eqz v0, :cond_4

    .line 604
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/af/a/b/a/a/d;->e:Lcom/google/af/a/b/a/a/e;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 606
    :cond_4
    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->f:Lcom/google/af/a/b/a/a/g;

    if-eqz v0, :cond_5

    .line 607
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/af/a/b/a/a/d;->f:Lcom/google/af/a/b/a/a/g;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 609
    :cond_5
    iget-object v0, p0, Lcom/google/af/a/b/a/a/d;->g:Lcom/google/af/a/b/a/a/b;

    if-eqz v0, :cond_6

    .line 610
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/af/a/b/a/a/d;->g:Lcom/google/af/a/b/a/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 612
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 613
    return-void
.end method
