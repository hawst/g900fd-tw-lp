.class public final Lcom/google/af/a/b/a/a/r;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/af/a/b/a/a/q;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2012
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2013
    invoke-static {}, Lcom/google/af/a/b/a/a/q;->a()[Lcom/google/af/a/b/a/a/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/r;->a:[Lcom/google/af/a/b/a/a/q;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/b/a/a/r;->cachedSize:I

    .line 2014
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 2062
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v1

    .line 2063
    iget-object v0, p0, Lcom/google/af/a/b/a/a/r;->a:[Lcom/google/af/a/b/a/a/q;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/af/a/b/a/a/r;->a:[Lcom/google/af/a/b/a/a/q;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 2064
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/af/a/b/a/a/r;->a:[Lcom/google/af/a/b/a/a/q;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 2065
    iget-object v2, p0, Lcom/google/af/a/b/a/a/r;->a:[Lcom/google/af/a/b/a/a/q;

    aget-object v2, v2, v0

    .line 2066
    if-eqz v2, :cond_0

    .line 2067
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 2064
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2072
    :cond_1
    return v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2024
    if-ne p1, p0, :cond_1

    .line 2035
    :cond_0
    :goto_0
    return v0

    .line 2027
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/b/a/a/r;

    if-nez v2, :cond_2

    move v0, v1

    .line 2028
    goto :goto_0

    .line 2030
    :cond_2
    check-cast p1, Lcom/google/af/a/b/a/a/r;

    .line 2031
    iget-object v2, p0, Lcom/google/af/a/b/a/a/r;->a:[Lcom/google/af/a/b/a/a/q;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/r;->a:[Lcom/google/af/a/b/a/a/q;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2033
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2040
    iget-object v0, p0, Lcom/google/af/a/b/a/a/r;->a:[Lcom/google/af/a/b/a/a/q;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 2043
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1992
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/af/a/b/a/a/r;->a:[Lcom/google/af/a/b/a/a/q;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/af/a/b/a/a/q;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/af/a/b/a/a/r;->a:[Lcom/google/af/a/b/a/a/q;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/af/a/b/a/a/q;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/q;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/af/a/b/a/a/r;->a:[Lcom/google/af/a/b/a/a/q;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/af/a/b/a/a/q;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/q;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/af/a/b/a/a/r;->a:[Lcom/google/af/a/b/a/a/q;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 2049
    iget-object v0, p0, Lcom/google/af/a/b/a/a/r;->a:[Lcom/google/af/a/b/a/a/q;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/af/a/b/a/a/r;->a:[Lcom/google/af/a/b/a/a/q;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 2050
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/af/a/b/a/a/r;->a:[Lcom/google/af/a/b/a/a/q;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 2051
    iget-object v1, p0, Lcom/google/af/a/b/a/a/r;->a:[Lcom/google/af/a/b/a/a/q;

    aget-object v1, v1, v0

    .line 2052
    if-eqz v1, :cond_0

    .line 2053
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2050
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2057
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2058
    return-void
.end method
