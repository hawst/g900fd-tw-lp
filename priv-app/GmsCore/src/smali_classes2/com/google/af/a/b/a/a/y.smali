.class public final Lcom/google/af/a/b/a/a/y;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/af/a/b/a/a/q;

.field public b:[Lcom/google/af/a/b/a/a/q;

.field public c:[Lcom/google/af/a/b/a/a/s;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2447
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2448
    invoke-static {}, Lcom/google/af/a/b/a/a/q;->a()[Lcom/google/af/a/b/a/a/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/y;->a:[Lcom/google/af/a/b/a/a/q;

    invoke-static {}, Lcom/google/af/a/b/a/a/q;->a()[Lcom/google/af/a/b/a/a/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/y;->b:[Lcom/google/af/a/b/a/a/q;

    invoke-static {}, Lcom/google/af/a/b/a/a/s;->a()[Lcom/google/af/a/b/a/a/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/y;->c:[Lcom/google/af/a/b/a/a/s;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/b/a/a/y;->cachedSize:I

    .line 2449
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2527
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2528
    iget-object v2, p0, Lcom/google/af/a/b/a/a/y;->a:[Lcom/google/af/a/b/a/a/q;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/af/a/b/a/a/y;->a:[Lcom/google/af/a/b/a/a/q;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 2529
    :goto_0
    iget-object v3, p0, Lcom/google/af/a/b/a/a/y;->a:[Lcom/google/af/a/b/a/a/q;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 2530
    iget-object v3, p0, Lcom/google/af/a/b/a/a/y;->a:[Lcom/google/af/a/b/a/a/q;

    aget-object v3, v3, v0

    .line 2531
    if-eqz v3, :cond_0

    .line 2532
    const/4 v4, 0x1

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2529
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 2537
    :cond_2
    iget-object v2, p0, Lcom/google/af/a/b/a/a/y;->b:[Lcom/google/af/a/b/a/a/q;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/af/a/b/a/a/y;->b:[Lcom/google/af/a/b/a/a/q;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 2538
    :goto_1
    iget-object v3, p0, Lcom/google/af/a/b/a/a/y;->b:[Lcom/google/af/a/b/a/a/q;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 2539
    iget-object v3, p0, Lcom/google/af/a/b/a/a/y;->b:[Lcom/google/af/a/b/a/a/q;

    aget-object v3, v3, v0

    .line 2540
    if-eqz v3, :cond_3

    .line 2541
    const/4 v4, 0x2

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2538
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v2

    .line 2546
    :cond_5
    iget-object v2, p0, Lcom/google/af/a/b/a/a/y;->c:[Lcom/google/af/a/b/a/a/s;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/af/a/b/a/a/y;->c:[Lcom/google/af/a/b/a/a/s;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 2547
    :goto_2
    iget-object v2, p0, Lcom/google/af/a/b/a/a/y;->c:[Lcom/google/af/a/b/a/a/s;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 2548
    iget-object v2, p0, Lcom/google/af/a/b/a/a/y;->c:[Lcom/google/af/a/b/a/a/s;

    aget-object v2, v2, v1

    .line 2549
    if-eqz v2, :cond_6

    .line 2550
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2547
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2555
    :cond_7
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2461
    if-ne p1, p0, :cond_1

    .line 2480
    :cond_0
    :goto_0
    return v0

    .line 2464
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/b/a/a/y;

    if-nez v2, :cond_2

    move v0, v1

    .line 2465
    goto :goto_0

    .line 2467
    :cond_2
    check-cast p1, Lcom/google/af/a/b/a/a/y;

    .line 2468
    iget-object v2, p0, Lcom/google/af/a/b/a/a/y;->a:[Lcom/google/af/a/b/a/a/q;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/y;->a:[Lcom/google/af/a/b/a/a/q;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 2470
    goto :goto_0

    .line 2472
    :cond_3
    iget-object v2, p0, Lcom/google/af/a/b/a/a/y;->b:[Lcom/google/af/a/b/a/a/q;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/y;->b:[Lcom/google/af/a/b/a/a/q;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 2474
    goto :goto_0

    .line 2476
    :cond_4
    iget-object v2, p0, Lcom/google/af/a/b/a/a/y;->c:[Lcom/google/af/a/b/a/a/s;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/y;->c:[Lcom/google/af/a/b/a/a/s;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2478
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2485
    iget-object v0, p0, Lcom/google/af/a/b/a/a/y;->a:[Lcom/google/af/a/b/a/a/q;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 2488
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/af/a/b/a/a/y;->b:[Lcom/google/af/a/b/a/a/q;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2490
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/af/a/b/a/a/y;->c:[Lcom/google/af/a/b/a/a/s;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2492
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2421
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/af/a/b/a/a/y;->a:[Lcom/google/af/a/b/a/a/q;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/af/a/b/a/a/q;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/af/a/b/a/a/y;->a:[Lcom/google/af/a/b/a/a/q;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/af/a/b/a/a/q;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/q;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/af/a/b/a/a/y;->a:[Lcom/google/af/a/b/a/a/q;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/af/a/b/a/a/q;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/q;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/af/a/b/a/a/y;->a:[Lcom/google/af/a/b/a/a/q;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/af/a/b/a/a/y;->b:[Lcom/google/af/a/b/a/a/q;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/af/a/b/a/a/q;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/af/a/b/a/a/y;->b:[Lcom/google/af/a/b/a/a/q;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/af/a/b/a/a/q;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/q;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/af/a/b/a/a/y;->b:[Lcom/google/af/a/b/a/a/q;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/af/a/b/a/a/q;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/q;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/af/a/b/a/a/y;->b:[Lcom/google/af/a/b/a/a/q;

    goto/16 :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/af/a/b/a/a/y;->c:[Lcom/google/af/a/b/a/a/s;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/af/a/b/a/a/s;

    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/google/af/a/b/a/a/y;->c:[Lcom/google/af/a/b/a/a/s;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Lcom/google/af/a/b/a/a/s;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/s;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Lcom/google/af/a/b/a/a/y;->c:[Lcom/google/af/a/b/a/a/s;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Lcom/google/af/a/b/a/a/s;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/s;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/af/a/b/a/a/y;->c:[Lcom/google/af/a/b/a/a/s;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2498
    iget-object v0, p0, Lcom/google/af/a/b/a/a/y;->a:[Lcom/google/af/a/b/a/a/q;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/af/a/b/a/a/y;->a:[Lcom/google/af/a/b/a/a/q;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 2499
    :goto_0
    iget-object v2, p0, Lcom/google/af/a/b/a/a/y;->a:[Lcom/google/af/a/b/a/a/q;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 2500
    iget-object v2, p0, Lcom/google/af/a/b/a/a/y;->a:[Lcom/google/af/a/b/a/a/q;

    aget-object v2, v2, v0

    .line 2501
    if-eqz v2, :cond_0

    .line 2502
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2499
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2506
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/b/a/a/y;->b:[Lcom/google/af/a/b/a/a/q;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/af/a/b/a/a/y;->b:[Lcom/google/af/a/b/a/a/q;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 2507
    :goto_1
    iget-object v2, p0, Lcom/google/af/a/b/a/a/y;->b:[Lcom/google/af/a/b/a/a/q;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 2508
    iget-object v2, p0, Lcom/google/af/a/b/a/a/y;->b:[Lcom/google/af/a/b/a/a/q;

    aget-object v2, v2, v0

    .line 2509
    if-eqz v2, :cond_2

    .line 2510
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2507
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2514
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/b/a/a/y;->c:[Lcom/google/af/a/b/a/a/s;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/af/a/b/a/a/y;->c:[Lcom/google/af/a/b/a/a/s;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 2515
    :goto_2
    iget-object v0, p0, Lcom/google/af/a/b/a/a/y;->c:[Lcom/google/af/a/b/a/a/s;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 2516
    iget-object v0, p0, Lcom/google/af/a/b/a/a/y;->c:[Lcom/google/af/a/b/a/a/s;

    aget-object v0, v0, v1

    .line 2517
    if-eqz v0, :cond_4

    .line 2518
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2515
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2522
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2523
    return-void
.end method
