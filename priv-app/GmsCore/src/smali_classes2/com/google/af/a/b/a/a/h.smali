.class public final Lcom/google/af/a/b/a/a/h;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1487
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1488
    iput-object v0, p0, Lcom/google/af/a/b/a/a/h;->a:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/h;->b:Ljava/lang/Boolean;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/b/a/a/h;->cachedSize:I

    .line 1489
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 1548
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1549
    iget-object v1, p0, Lcom/google/af/a/b/a/a/h;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 1550
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/af/a/b/a/a/h;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1553
    :cond_0
    iget-object v1, p0, Lcom/google/af/a/b/a/a/h;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 1554
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/af/a/b/a/a/h;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1557
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1500
    if-ne p1, p0, :cond_1

    .line 1521
    :cond_0
    :goto_0
    return v0

    .line 1503
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/b/a/a/h;

    if-nez v2, :cond_2

    move v0, v1

    .line 1504
    goto :goto_0

    .line 1506
    :cond_2
    check-cast p1, Lcom/google/af/a/b/a/a/h;

    .line 1507
    iget-object v2, p0, Lcom/google/af/a/b/a/a/h;->a:Ljava/lang/Long;

    if-nez v2, :cond_3

    .line 1508
    iget-object v2, p1, Lcom/google/af/a/b/a/a/h;->a:Ljava/lang/Long;

    if-eqz v2, :cond_4

    move v0, v1

    .line 1509
    goto :goto_0

    .line 1511
    :cond_3
    iget-object v2, p0, Lcom/google/af/a/b/a/a/h;->a:Ljava/lang/Long;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/h;->a:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1512
    goto :goto_0

    .line 1514
    :cond_4
    iget-object v2, p0, Lcom/google/af/a/b/a/a/h;->b:Ljava/lang/Boolean;

    if-nez v2, :cond_5

    .line 1515
    iget-object v2, p1, Lcom/google/af/a/b/a/a/h;->b:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1516
    goto :goto_0

    .line 1518
    :cond_5
    iget-object v2, p0, Lcom/google/af/a/b/a/a/h;->b:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/h;->b:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1519
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1526
    iget-object v0, p0, Lcom/google/af/a/b/a/a/h;->a:Ljava/lang/Long;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1529
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/af/a/b/a/a/h;->b:Ljava/lang/Boolean;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 1531
    return v0

    .line 1526
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/b/a/a/h;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1529
    :cond_1
    iget-object v1, p0, Lcom/google/af/a/b/a/a/h;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 1464
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/h;->a:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/h;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 1537
    iget-object v0, p0, Lcom/google/af/a/b/a/a/h;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1538
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/af/a/b/a/a/h;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 1540
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/b/a/a/h;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1541
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/af/a/b/a/a/h;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1543
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1544
    return-void
.end method
