.class public final Lcom/google/af/a/c/a/n;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:I

.field public c:I

.field public d:Lcom/google/af/a/c/a/b;

.field public e:Ljava/lang/String;

.field public f:Lcom/google/af/a/c/a/d;

.field public g:I

.field public h:I

.field public i:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 50
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 51
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/af/a/c/a/n;->a:J

    iput v2, p0, Lcom/google/af/a/c/a/n;->b:I

    iput v2, p0, Lcom/google/af/a/c/a/n;->c:I

    iput-object v3, p0, Lcom/google/af/a/c/a/n;->d:Lcom/google/af/a/c/a/b;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/c/a/n;->e:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/af/a/c/a/n;->f:Lcom/google/af/a/c/a/d;

    iput v2, p0, Lcom/google/af/a/c/a/n;->g:I

    iput v2, p0, Lcom/google/af/a/c/a/n;->h:I

    iput v2, p0, Lcom/google/af/a/c/a/n;->i:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/c/a/n;->cachedSize:I

    .line 52
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 177
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 178
    iget-wide v2, p0, Lcom/google/af/a/c/a/n;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 179
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/af/a/c/a/n;->a:J

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 182
    :cond_0
    iget v1, p0, Lcom/google/af/a/c/a/n;->b:I

    if-eqz v1, :cond_1

    .line 183
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/af/a/c/a/n;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 186
    :cond_1
    iget-object v1, p0, Lcom/google/af/a/c/a/n;->d:Lcom/google/af/a/c/a/b;

    if-eqz v1, :cond_2

    .line 187
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/af/a/c/a/n;->d:Lcom/google/af/a/c/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 190
    :cond_2
    iget-object v1, p0, Lcom/google/af/a/c/a/n;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 191
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/af/a/c/a/n;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 194
    :cond_3
    iget-object v1, p0, Lcom/google/af/a/c/a/n;->f:Lcom/google/af/a/c/a/d;

    if-eqz v1, :cond_4

    .line 195
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/af/a/c/a/n;->f:Lcom/google/af/a/c/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 198
    :cond_4
    iget v1, p0, Lcom/google/af/a/c/a/n;->g:I

    if-eqz v1, :cond_5

    .line 199
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/af/a/c/a/n;->g:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 202
    :cond_5
    iget v1, p0, Lcom/google/af/a/c/a/n;->h:I

    if-eqz v1, :cond_6

    .line 203
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/af/a/c/a/n;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 206
    :cond_6
    iget v1, p0, Lcom/google/af/a/c/a/n;->c:I

    if-eqz v1, :cond_7

    .line 207
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/af/a/c/a/n;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 210
    :cond_7
    iget v1, p0, Lcom/google/af/a/c/a/n;->i:I

    if-eqz v1, :cond_8

    .line 211
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/af/a/c/a/n;->i:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 214
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 70
    if-ne p1, p0, :cond_1

    .line 120
    :cond_0
    :goto_0
    return v0

    .line 73
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/c/a/n;

    if-nez v2, :cond_2

    move v0, v1

    .line 74
    goto :goto_0

    .line 76
    :cond_2
    check-cast p1, Lcom/google/af/a/c/a/n;

    .line 77
    iget-wide v2, p0, Lcom/google/af/a/c/a/n;->a:J

    iget-wide v4, p1, Lcom/google/af/a/c/a/n;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 78
    goto :goto_0

    .line 80
    :cond_3
    iget v2, p0, Lcom/google/af/a/c/a/n;->b:I

    iget v3, p1, Lcom/google/af/a/c/a/n;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 81
    goto :goto_0

    .line 83
    :cond_4
    iget v2, p0, Lcom/google/af/a/c/a/n;->c:I

    iget v3, p1, Lcom/google/af/a/c/a/n;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 84
    goto :goto_0

    .line 86
    :cond_5
    iget-object v2, p0, Lcom/google/af/a/c/a/n;->d:Lcom/google/af/a/c/a/b;

    if-nez v2, :cond_6

    .line 87
    iget-object v2, p1, Lcom/google/af/a/c/a/n;->d:Lcom/google/af/a/c/a/b;

    if-eqz v2, :cond_7

    move v0, v1

    .line 88
    goto :goto_0

    .line 91
    :cond_6
    iget-object v2, p0, Lcom/google/af/a/c/a/n;->d:Lcom/google/af/a/c/a/b;

    iget-object v3, p1, Lcom/google/af/a/c/a/n;->d:Lcom/google/af/a/c/a/b;

    invoke-virtual {v2, v3}, Lcom/google/af/a/c/a/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 92
    goto :goto_0

    .line 95
    :cond_7
    iget-object v2, p0, Lcom/google/af/a/c/a/n;->e:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 96
    iget-object v2, p1, Lcom/google/af/a/c/a/n;->e:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    .line 97
    goto :goto_0

    .line 99
    :cond_8
    iget-object v2, p0, Lcom/google/af/a/c/a/n;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/c/a/n;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 100
    goto :goto_0

    .line 102
    :cond_9
    iget-object v2, p0, Lcom/google/af/a/c/a/n;->f:Lcom/google/af/a/c/a/d;

    if-nez v2, :cond_a

    .line 103
    iget-object v2, p1, Lcom/google/af/a/c/a/n;->f:Lcom/google/af/a/c/a/d;

    if-eqz v2, :cond_b

    move v0, v1

    .line 104
    goto :goto_0

    .line 107
    :cond_a
    iget-object v2, p0, Lcom/google/af/a/c/a/n;->f:Lcom/google/af/a/c/a/d;

    iget-object v3, p1, Lcom/google/af/a/c/a/n;->f:Lcom/google/af/a/c/a/d;

    invoke-virtual {v2, v3}, Lcom/google/af/a/c/a/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 108
    goto :goto_0

    .line 111
    :cond_b
    iget v2, p0, Lcom/google/af/a/c/a/n;->g:I

    iget v3, p1, Lcom/google/af/a/c/a/n;->g:I

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 112
    goto :goto_0

    .line 114
    :cond_c
    iget v2, p0, Lcom/google/af/a/c/a/n;->h:I

    iget v3, p1, Lcom/google/af/a/c/a/n;->h:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 115
    goto :goto_0

    .line 117
    :cond_d
    iget v2, p0, Lcom/google/af/a/c/a/n;->i:I

    iget v3, p1, Lcom/google/af/a/c/a/n;->i:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 118
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 125
    iget-wide v2, p0, Lcom/google/af/a/c/a/n;->a:J

    iget-wide v4, p0, Lcom/google/af/a/c/a/n;->a:J

    const/16 v0, 0x20

    ushr-long/2addr v4, v0

    xor-long/2addr v2, v4

    long-to-int v0, v2

    add-int/lit16 v0, v0, 0x20f

    .line 128
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/af/a/c/a/n;->b:I

    add-int/2addr v0, v2

    .line 129
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/af/a/c/a/n;->c:I

    add-int/2addr v0, v2

    .line 130
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/c/a/n;->d:Lcom/google/af/a/c/a/b;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 132
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/c/a/n;->e:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 134
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/af/a/c/a/n;->f:Lcom/google/af/a/c/a/d;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 136
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/af/a/c/a/n;->g:I

    add-int/2addr v0, v1

    .line 137
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/af/a/c/a/n;->h:I

    add-int/2addr v0, v1

    .line 138
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/af/a/c/a/n;->i:I

    add-int/2addr v0, v1

    .line 139
    return v0

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/c/a/n;->d:Lcom/google/af/a/c/a/b;

    invoke-virtual {v0}, Lcom/google/af/a/c/a/b;->hashCode()I

    move-result v0

    goto :goto_0

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/c/a/n;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 134
    :cond_2
    iget-object v1, p0, Lcom/google/af/a/c/a/n;->f:Lcom/google/af/a/c/a/d;

    invoke-virtual {v1}, Lcom/google/af/a/c/a/d;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/af/a/c/a/n;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/af/a/c/a/n;->b:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/af/a/c/a/n;->d:Lcom/google/af/a/c/a/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/af/a/c/a/b;

    invoke-direct {v0}, Lcom/google/af/a/c/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/c/a/n;->d:Lcom/google/af/a/c/a/b;

    :cond_1
    iget-object v0, p0, Lcom/google/af/a/c/a/n;->d:Lcom/google/af/a/c/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/c/a/n;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/af/a/c/a/n;->f:Lcom/google/af/a/c/a/d;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/af/a/c/a/d;

    invoke-direct {v0}, Lcom/google/af/a/c/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/c/a/n;->f:Lcom/google/af/a/c/a/d;

    :cond_2
    iget-object v0, p0, Lcom/google/af/a/c/a/n;->f:Lcom/google/af/a/c/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/af/a/c/a/n;->g:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/af/a/c/a/n;->h:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/af/a/c/a/n;->c:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/af/a/c/a/n;->i:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 145
    iget-wide v0, p0, Lcom/google/af/a/c/a/n;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 146
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/af/a/c/a/n;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)V

    .line 148
    :cond_0
    iget v0, p0, Lcom/google/af/a/c/a/n;->b:I

    if-eqz v0, :cond_1

    .line 149
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/af/a/c/a/n;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 151
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/c/a/n;->d:Lcom/google/af/a/c/a/b;

    if-eqz v0, :cond_2

    .line 152
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/af/a/c/a/n;->d:Lcom/google/af/a/c/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 154
    :cond_2
    iget-object v0, p0, Lcom/google/af/a/c/a/n;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 155
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/af/a/c/a/n;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 157
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/c/a/n;->f:Lcom/google/af/a/c/a/d;

    if-eqz v0, :cond_4

    .line 158
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/af/a/c/a/n;->f:Lcom/google/af/a/c/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 160
    :cond_4
    iget v0, p0, Lcom/google/af/a/c/a/n;->g:I

    if-eqz v0, :cond_5

    .line 161
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/af/a/c/a/n;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 163
    :cond_5
    iget v0, p0, Lcom/google/af/a/c/a/n;->h:I

    if-eqz v0, :cond_6

    .line 164
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/af/a/c/a/n;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 166
    :cond_6
    iget v0, p0, Lcom/google/af/a/c/a/n;->c:I

    if-eqz v0, :cond_7

    .line 167
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/af/a/c/a/n;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 169
    :cond_7
    iget v0, p0, Lcom/google/af/a/c/a/n;->i:I

    if-eqz v0, :cond_8

    .line 170
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/af/a/c/a/n;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 172
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 173
    return-void
.end method
