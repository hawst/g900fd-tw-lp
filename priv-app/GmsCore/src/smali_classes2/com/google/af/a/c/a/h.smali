.class public final Lcom/google/af/a/c/a/h;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 51
    iput v0, p0, Lcom/google/af/a/c/a/h;->a:I

    iput v0, p0, Lcom/google/af/a/c/a/h;->b:I

    iput v0, p0, Lcom/google/af/a/c/a/h;->c:I

    iput v0, p0, Lcom/google/af/a/c/a/h;->d:I

    iput v0, p0, Lcom/google/af/a/c/a/h;->e:I

    iput v0, p0, Lcom/google/af/a/c/a/h;->f:I

    iput v0, p0, Lcom/google/af/a/c/a/h;->g:I

    iput v0, p0, Lcom/google/af/a/c/a/h;->h:I

    iput v0, p0, Lcom/google/af/a/c/a/h;->i:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/c/a/h;->cachedSize:I

    .line 52
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 157
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 158
    iget v1, p0, Lcom/google/af/a/c/a/h;->a:I

    if-eqz v1, :cond_0

    .line 159
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/af/a/c/a/h;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 162
    :cond_0
    iget v1, p0, Lcom/google/af/a/c/a/h;->b:I

    if-eqz v1, :cond_1

    .line 163
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/af/a/c/a/h;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 166
    :cond_1
    iget v1, p0, Lcom/google/af/a/c/a/h;->c:I

    if-eqz v1, :cond_2

    .line 167
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/af/a/c/a/h;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    :cond_2
    iget v1, p0, Lcom/google/af/a/c/a/h;->d:I

    if-eqz v1, :cond_3

    .line 171
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/af/a/c/a/h;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    :cond_3
    iget v1, p0, Lcom/google/af/a/c/a/h;->e:I

    if-eqz v1, :cond_4

    .line 175
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/af/a/c/a/h;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 178
    :cond_4
    iget v1, p0, Lcom/google/af/a/c/a/h;->f:I

    if-eqz v1, :cond_5

    .line 179
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/af/a/c/a/h;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 182
    :cond_5
    iget v1, p0, Lcom/google/af/a/c/a/h;->g:I

    if-eqz v1, :cond_6

    .line 183
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/af/a/c/a/h;->g:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 186
    :cond_6
    iget v1, p0, Lcom/google/af/a/c/a/h;->h:I

    if-eqz v1, :cond_7

    .line 187
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/af/a/c/a/h;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 190
    :cond_7
    iget v1, p0, Lcom/google/af/a/c/a/h;->i:I

    if-eqz v1, :cond_8

    .line 191
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/af/a/c/a/h;->i:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 194
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 70
    if-ne p1, p0, :cond_1

    .line 104
    :cond_0
    :goto_0
    return v0

    .line 73
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/c/a/h;

    if-nez v2, :cond_2

    move v0, v1

    .line 74
    goto :goto_0

    .line 76
    :cond_2
    check-cast p1, Lcom/google/af/a/c/a/h;

    .line 77
    iget v2, p0, Lcom/google/af/a/c/a/h;->a:I

    iget v3, p1, Lcom/google/af/a/c/a/h;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 78
    goto :goto_0

    .line 80
    :cond_3
    iget v2, p0, Lcom/google/af/a/c/a/h;->b:I

    iget v3, p1, Lcom/google/af/a/c/a/h;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 81
    goto :goto_0

    .line 83
    :cond_4
    iget v2, p0, Lcom/google/af/a/c/a/h;->c:I

    iget v3, p1, Lcom/google/af/a/c/a/h;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 84
    goto :goto_0

    .line 86
    :cond_5
    iget v2, p0, Lcom/google/af/a/c/a/h;->d:I

    iget v3, p1, Lcom/google/af/a/c/a/h;->d:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 87
    goto :goto_0

    .line 89
    :cond_6
    iget v2, p0, Lcom/google/af/a/c/a/h;->e:I

    iget v3, p1, Lcom/google/af/a/c/a/h;->e:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 90
    goto :goto_0

    .line 92
    :cond_7
    iget v2, p0, Lcom/google/af/a/c/a/h;->f:I

    iget v3, p1, Lcom/google/af/a/c/a/h;->f:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 93
    goto :goto_0

    .line 95
    :cond_8
    iget v2, p0, Lcom/google/af/a/c/a/h;->g:I

    iget v3, p1, Lcom/google/af/a/c/a/h;->g:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 96
    goto :goto_0

    .line 98
    :cond_9
    iget v2, p0, Lcom/google/af/a/c/a/h;->h:I

    iget v3, p1, Lcom/google/af/a/c/a/h;->h:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 99
    goto :goto_0

    .line 101
    :cond_a
    iget v2, p0, Lcom/google/af/a/c/a/h;->i:I

    iget v3, p1, Lcom/google/af/a/c/a/h;->i:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 102
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 109
    iget v0, p0, Lcom/google/af/a/c/a/h;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 111
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/af/a/c/a/h;->b:I

    add-int/2addr v0, v1

    .line 112
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/af/a/c/a/h;->c:I

    add-int/2addr v0, v1

    .line 113
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/af/a/c/a/h;->d:I

    add-int/2addr v0, v1

    .line 114
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/af/a/c/a/h;->e:I

    add-int/2addr v0, v1

    .line 115
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/af/a/c/a/h;->f:I

    add-int/2addr v0, v1

    .line 116
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/af/a/c/a/h;->g:I

    add-int/2addr v0, v1

    .line 117
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/af/a/c/a/h;->h:I

    add-int/2addr v0, v1

    .line 118
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/af/a/c/a/h;->i:I

    add-int/2addr v0, v1

    .line 119
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/af/a/c/a/h;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/af/a/c/a/h;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/af/a/c/a/h;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/af/a/c/a/h;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/af/a/c/a/h;->e:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/af/a/c/a/h;->f:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/af/a/c/a/h;->g:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/af/a/c/a/h;->h:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/af/a/c/a/h;->i:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 125
    iget v0, p0, Lcom/google/af/a/c/a/h;->a:I

    if-eqz v0, :cond_0

    .line 126
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/af/a/c/a/h;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 128
    :cond_0
    iget v0, p0, Lcom/google/af/a/c/a/h;->b:I

    if-eqz v0, :cond_1

    .line 129
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/af/a/c/a/h;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 131
    :cond_1
    iget v0, p0, Lcom/google/af/a/c/a/h;->c:I

    if-eqz v0, :cond_2

    .line 132
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/af/a/c/a/h;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 134
    :cond_2
    iget v0, p0, Lcom/google/af/a/c/a/h;->d:I

    if-eqz v0, :cond_3

    .line 135
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/af/a/c/a/h;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 137
    :cond_3
    iget v0, p0, Lcom/google/af/a/c/a/h;->e:I

    if-eqz v0, :cond_4

    .line 138
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/af/a/c/a/h;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 140
    :cond_4
    iget v0, p0, Lcom/google/af/a/c/a/h;->f:I

    if-eqz v0, :cond_5

    .line 141
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/af/a/c/a/h;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 143
    :cond_5
    iget v0, p0, Lcom/google/af/a/c/a/h;->g:I

    if-eqz v0, :cond_6

    .line 144
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/af/a/c/a/h;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 146
    :cond_6
    iget v0, p0, Lcom/google/af/a/c/a/h;->h:I

    if-eqz v0, :cond_7

    .line 147
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/af/a/c/a/h;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 149
    :cond_7
    iget v0, p0, Lcom/google/af/a/c/a/h;->i:I

    if-eqz v0, :cond_8

    .line 150
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/af/a/c/a/h;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 152
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 153
    return-void
.end method
