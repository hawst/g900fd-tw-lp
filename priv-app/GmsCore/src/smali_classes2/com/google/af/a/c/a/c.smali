.class public final Lcom/google/af/a/c/a/c;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/af/a/c/a/c;


# instance fields
.field public a:I

.field public b:J

.field public c:I

.field public d:[I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 35
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 36
    iput v2, p0, Lcom/google/af/a/c/a/c;->a:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/af/a/c/a/c;->b:J

    iput v2, p0, Lcom/google/af/a/c/a/c;->c:I

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/af/a/c/a/c;->d:[I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/c/a/c;->cachedSize:I

    .line 37
    return-void
.end method

.method public static a()[Lcom/google/af/a/c/a/c;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/af/a/c/a/c;->e:[Lcom/google/af/a/c/a/c;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/af/a/c/a/c;->e:[Lcom/google/af/a/c/a/c;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/af/a/c/a/c;

    sput-object v0, Lcom/google/af/a/c/a/c;->e:[Lcom/google/af/a/c/a/c;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/af/a/c/a/c;->e:[Lcom/google/af/a/c/a/c;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 107
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 108
    iget v2, p0, Lcom/google/af/a/c/a/c;->a:I

    if-eqz v2, :cond_0

    .line 109
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/af/a/c/a/c;->a:I

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 112
    :cond_0
    iget-wide v2, p0, Lcom/google/af/a/c/a/c;->b:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 113
    const/4 v2, 0x2

    iget-wide v4, p0, Lcom/google/af/a/c/a/c;->b:J

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 116
    :cond_1
    iget v2, p0, Lcom/google/af/a/c/a/c;->c:I

    if-eqz v2, :cond_2

    .line 117
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/af/a/c/a/c;->c:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 120
    :cond_2
    iget-object v2, p0, Lcom/google/af/a/c/a/c;->d:[I

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/af/a/c/a/c;->d:[I

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v1

    .line 122
    :goto_0
    iget-object v3, p0, Lcom/google/af/a/c/a/c;->d:[I

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 123
    iget-object v3, p0, Lcom/google/af/a/c/a/c;->d:[I

    aget v3, v3, v1

    .line 124
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 122
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 127
    :cond_3
    add-int/2addr v0, v2

    .line 128
    iget-object v1, p0, Lcom/google/af/a/c/a/c;->d:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 130
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 50
    if-ne p1, p0, :cond_1

    .line 70
    :cond_0
    :goto_0
    return v0

    .line 53
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/c/a/c;

    if-nez v2, :cond_2

    move v0, v1

    .line 54
    goto :goto_0

    .line 56
    :cond_2
    check-cast p1, Lcom/google/af/a/c/a/c;

    .line 57
    iget v2, p0, Lcom/google/af/a/c/a/c;->a:I

    iget v3, p1, Lcom/google/af/a/c/a/c;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 58
    goto :goto_0

    .line 60
    :cond_3
    iget-wide v2, p0, Lcom/google/af/a/c/a/c;->b:J

    iget-wide v4, p1, Lcom/google/af/a/c/a/c;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    .line 61
    goto :goto_0

    .line 63
    :cond_4
    iget v2, p0, Lcom/google/af/a/c/a/c;->c:I

    iget v3, p1, Lcom/google/af/a/c/a/c;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 64
    goto :goto_0

    .line 66
    :cond_5
    iget-object v2, p0, Lcom/google/af/a/c/a/c;->d:[I

    iget-object v3, p1, Lcom/google/af/a/c/a/c;->d:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 68
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    .line 75
    iget v0, p0, Lcom/google/af/a/c/a/c;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 77
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/af/a/c/a/c;->b:J

    iget-wide v4, p0, Lcom/google/af/a/c/a/c;->b:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 79
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/af/a/c/a/c;->c:I

    add-int/2addr v0, v1

    .line 80
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/af/a/c/a/c;->d:[I

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v1

    add-int/2addr v0, v1

    .line 82
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    iput v0, p0, Lcom/google/af/a/c/a/c;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/af/a/c/a/c;->b:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/af/a/c/a/c;->c:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x20

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_1

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_1
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/af/a/c/a/c;->d:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Lcom/google/af/a/c/a/c;->d:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/af/a/c/a/c;->d:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Lcom/google/af/a/c/a/c;->d:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/af/a/c/a/c;->d:[I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_2

    goto :goto_4

    :pswitch_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/af/a/c/a/c;->d:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/google/af/a/c/a/c;->d:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_3

    goto :goto_6

    :pswitch_3
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Lcom/google/af/a/c/a/c;->d:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Lcom/google/af/a/c/a/c;->d:[I

    :cond_a
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x22 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 88
    iget v0, p0, Lcom/google/af/a/c/a/c;->a:I

    if-eqz v0, :cond_0

    .line 89
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/af/a/c/a/c;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->b(II)V

    .line 91
    :cond_0
    iget-wide v0, p0, Lcom/google/af/a/c/a/c;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 92
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/af/a/c/a/c;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 94
    :cond_1
    iget v0, p0, Lcom/google/af/a/c/a/c;->c:I

    if-eqz v0, :cond_2

    .line 95
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/af/a/c/a/c;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 97
    :cond_2
    iget-object v0, p0, Lcom/google/af/a/c/a/c;->d:[I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/af/a/c/a/c;->d:[I

    array-length v0, v0

    if-lez v0, :cond_3

    .line 98
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/af/a/c/a/c;->d:[I

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 99
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/af/a/c/a/c;->d:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 102
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 103
    return-void
.end method
