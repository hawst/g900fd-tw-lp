.class public final Lcom/google/af/a/b/a/a/i;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/af/a/b/a/a/i;


# instance fields
.field public a:Lcom/google/af/a/b/a/a/h;

.field public b:Lcom/google/af/a/b/a/a/h;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1622
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1623
    iput-object v0, p0, Lcom/google/af/a/b/a/a/i;->a:Lcom/google/af/a/b/a/a/h;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/i;->b:Lcom/google/af/a/b/a/a/h;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/b/a/a/i;->cachedSize:I

    .line 1624
    return-void
.end method

.method public static a()[Lcom/google/af/a/b/a/a/i;
    .locals 2

    .prologue
    .line 1605
    sget-object v0, Lcom/google/af/a/b/a/a/i;->c:[Lcom/google/af/a/b/a/a/i;

    if-nez v0, :cond_1

    .line 1606
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1608
    :try_start_0
    sget-object v0, Lcom/google/af/a/b/a/a/i;->c:[Lcom/google/af/a/b/a/a/i;

    if-nez v0, :cond_0

    .line 1609
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/af/a/b/a/a/i;

    sput-object v0, Lcom/google/af/a/b/a/a/i;->c:[Lcom/google/af/a/b/a/a/i;

    .line 1611
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1613
    :cond_1
    sget-object v0, Lcom/google/af/a/b/a/a/i;->c:[Lcom/google/af/a/b/a/a/i;

    return-object v0

    .line 1611
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1687
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1688
    iget-object v1, p0, Lcom/google/af/a/b/a/a/i;->a:Lcom/google/af/a/b/a/a/h;

    if-eqz v1, :cond_0

    .line 1689
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/af/a/b/a/a/i;->a:Lcom/google/af/a/b/a/a/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1692
    :cond_0
    iget-object v1, p0, Lcom/google/af/a/b/a/a/i;->b:Lcom/google/af/a/b/a/a/h;

    if-eqz v1, :cond_1

    .line 1693
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/af/a/b/a/a/i;->b:Lcom/google/af/a/b/a/a/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1696
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1635
    if-ne p1, p0, :cond_1

    .line 1660
    :cond_0
    :goto_0
    return v0

    .line 1638
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/b/a/a/i;

    if-nez v2, :cond_2

    move v0, v1

    .line 1639
    goto :goto_0

    .line 1641
    :cond_2
    check-cast p1, Lcom/google/af/a/b/a/a/i;

    .line 1642
    iget-object v2, p0, Lcom/google/af/a/b/a/a/i;->a:Lcom/google/af/a/b/a/a/h;

    if-nez v2, :cond_3

    .line 1643
    iget-object v2, p1, Lcom/google/af/a/b/a/a/i;->a:Lcom/google/af/a/b/a/a/h;

    if-eqz v2, :cond_4

    move v0, v1

    .line 1644
    goto :goto_0

    .line 1647
    :cond_3
    iget-object v2, p0, Lcom/google/af/a/b/a/a/i;->a:Lcom/google/af/a/b/a/a/h;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/i;->a:Lcom/google/af/a/b/a/a/h;

    invoke-virtual {v2, v3}, Lcom/google/af/a/b/a/a/h;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1648
    goto :goto_0

    .line 1651
    :cond_4
    iget-object v2, p0, Lcom/google/af/a/b/a/a/i;->b:Lcom/google/af/a/b/a/a/h;

    if-nez v2, :cond_5

    .line 1652
    iget-object v2, p1, Lcom/google/af/a/b/a/a/i;->b:Lcom/google/af/a/b/a/a/h;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1653
    goto :goto_0

    .line 1656
    :cond_5
    iget-object v2, p0, Lcom/google/af/a/b/a/a/i;->b:Lcom/google/af/a/b/a/a/h;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/i;->b:Lcom/google/af/a/b/a/a/h;

    invoke-virtual {v2, v3}, Lcom/google/af/a/b/a/a/h;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1657
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1665
    iget-object v0, p0, Lcom/google/af/a/b/a/a/i;->a:Lcom/google/af/a/b/a/a/h;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1668
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/af/a/b/a/a/i;->b:Lcom/google/af/a/b/a/a/h;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 1670
    return v0

    .line 1665
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/b/a/a/i;->a:Lcom/google/af/a/b/a/a/h;

    invoke-virtual {v0}, Lcom/google/af/a/b/a/a/h;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1668
    :cond_1
    iget-object v1, p0, Lcom/google/af/a/b/a/a/i;->b:Lcom/google/af/a/b/a/a/h;

    invoke-virtual {v1}, Lcom/google/af/a/b/a/a/h;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1599
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/af/a/b/a/a/i;->a:Lcom/google/af/a/b/a/a/h;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/af/a/b/a/a/h;

    invoke-direct {v0}, Lcom/google/af/a/b/a/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/b/a/a/i;->a:Lcom/google/af/a/b/a/a/h;

    :cond_1
    iget-object v0, p0, Lcom/google/af/a/b/a/a/i;->a:Lcom/google/af/a/b/a/a/h;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/af/a/b/a/a/i;->b:Lcom/google/af/a/b/a/a/h;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/af/a/b/a/a/h;

    invoke-direct {v0}, Lcom/google/af/a/b/a/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/b/a/a/i;->b:Lcom/google/af/a/b/a/a/h;

    :cond_2
    iget-object v0, p0, Lcom/google/af/a/b/a/a/i;->b:Lcom/google/af/a/b/a/a/h;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1676
    iget-object v0, p0, Lcom/google/af/a/b/a/a/i;->a:Lcom/google/af/a/b/a/a/h;

    if-eqz v0, :cond_0

    .line 1677
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/af/a/b/a/a/i;->a:Lcom/google/af/a/b/a/a/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1679
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/b/a/a/i;->b:Lcom/google/af/a/b/a/a/h;

    if-eqz v0, :cond_1

    .line 1680
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/af/a/b/a/a/i;->b:Lcom/google/af/a/b/a/a/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1682
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1683
    return-void
.end method
