.class public final Lcom/google/af/a/c/a/q;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile j:[Lcom/google/af/a/c/a/q;


# instance fields
.field public a:Lcom/google/af/a/c/a;

.field public b:Lcom/google/af/a/c/a/d;

.field public c:J

.field public d:[Lcom/google/af/a/c/a/c;

.field public e:I

.field public f:Lcom/google/af/a/c/a/p;

.field public g:Ljava/lang/String;

.field public h:I

.field public i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 50
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 51
    iput-object v2, p0, Lcom/google/af/a/c/a/q;->a:Lcom/google/af/a/c/a;

    iput-object v2, p0, Lcom/google/af/a/c/a/q;->b:Lcom/google/af/a/c/a/d;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/af/a/c/a/q;->c:J

    invoke-static {}, Lcom/google/af/a/c/a/c;->a()[Lcom/google/af/a/c/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/c/a/q;->d:[Lcom/google/af/a/c/a/c;

    iput v3, p0, Lcom/google/af/a/c/a/q;->e:I

    iput-object v2, p0, Lcom/google/af/a/c/a/q;->f:Lcom/google/af/a/c/a/p;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/c/a/q;->g:Ljava/lang/String;

    iput v3, p0, Lcom/google/af/a/c/a/q;->h:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/c/a/q;->i:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/c/a/q;->cachedSize:I

    .line 52
    return-void
.end method

.method public static a()[Lcom/google/af/a/c/a/q;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/af/a/c/a/q;->j:[Lcom/google/af/a/c/a/q;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/af/a/c/a/q;->j:[Lcom/google/af/a/c/a/q;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/af/a/c/a/q;

    sput-object v0, Lcom/google/af/a/c/a/q;->j:[Lcom/google/af/a/c/a/q;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/af/a/c/a/q;->j:[Lcom/google/af/a/c/a/q;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 7

    .prologue
    .line 196
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 197
    iget-object v1, p0, Lcom/google/af/a/c/a/q;->a:Lcom/google/af/a/c/a;

    if-eqz v1, :cond_0

    .line 198
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/af/a/c/a/q;->a:Lcom/google/af/a/c/a;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 201
    :cond_0
    iget-object v1, p0, Lcom/google/af/a/c/a/q;->b:Lcom/google/af/a/c/a/d;

    if-eqz v1, :cond_1

    .line 202
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/af/a/c/a/q;->b:Lcom/google/af/a/c/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 205
    :cond_1
    iget-object v1, p0, Lcom/google/af/a/c/a/q;->d:[Lcom/google/af/a/c/a/c;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/af/a/c/a/q;->d:[Lcom/google/af/a/c/a/c;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 206
    const/4 v1, 0x0

    move v6, v1

    move v1, v0

    move v0, v6

    :goto_0
    iget-object v2, p0, Lcom/google/af/a/c/a/q;->d:[Lcom/google/af/a/c/a/c;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 207
    iget-object v2, p0, Lcom/google/af/a/c/a/q;->d:[Lcom/google/af/a/c/a/c;

    aget-object v2, v2, v0

    .line 208
    if-eqz v2, :cond_2

    .line 209
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 206
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 214
    :cond_4
    iget v1, p0, Lcom/google/af/a/c/a/q;->e:I

    if-eqz v1, :cond_5

    .line 215
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/af/a/c/a/q;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 218
    :cond_5
    iget-object v1, p0, Lcom/google/af/a/c/a/q;->f:Lcom/google/af/a/c/a/p;

    if-eqz v1, :cond_6

    .line 219
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/af/a/c/a/q;->f:Lcom/google/af/a/c/a/p;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 222
    :cond_6
    iget-wide v2, p0, Lcom/google/af/a/c/a/q;->c:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_7

    .line 223
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/google/af/a/c/a/q;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 226
    :cond_7
    iget-object v1, p0, Lcom/google/af/a/c/a/q;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 227
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/af/a/c/a/q;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 230
    :cond_8
    iget v1, p0, Lcom/google/af/a/c/a/q;->h:I

    if-eqz v1, :cond_9

    .line 231
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/af/a/c/a/q;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 234
    :cond_9
    iget-object v1, p0, Lcom/google/af/a/c/a/q;->i:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 235
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/af/a/c/a/q;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 238
    :cond_a
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 70
    if-ne p1, p0, :cond_1

    .line 131
    :cond_0
    :goto_0
    return v0

    .line 73
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/c/a/q;

    if-nez v2, :cond_2

    move v0, v1

    .line 74
    goto :goto_0

    .line 76
    :cond_2
    check-cast p1, Lcom/google/af/a/c/a/q;

    .line 77
    iget-object v2, p0, Lcom/google/af/a/c/a/q;->a:Lcom/google/af/a/c/a;

    if-nez v2, :cond_3

    .line 78
    iget-object v2, p1, Lcom/google/af/a/c/a/q;->a:Lcom/google/af/a/c/a;

    if-eqz v2, :cond_4

    move v0, v1

    .line 79
    goto :goto_0

    .line 82
    :cond_3
    iget-object v2, p0, Lcom/google/af/a/c/a/q;->a:Lcom/google/af/a/c/a;

    iget-object v3, p1, Lcom/google/af/a/c/a/q;->a:Lcom/google/af/a/c/a;

    invoke-virtual {v2, v3}, Lcom/google/af/a/c/a;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 83
    goto :goto_0

    .line 86
    :cond_4
    iget-object v2, p0, Lcom/google/af/a/c/a/q;->b:Lcom/google/af/a/c/a/d;

    if-nez v2, :cond_5

    .line 87
    iget-object v2, p1, Lcom/google/af/a/c/a/q;->b:Lcom/google/af/a/c/a/d;

    if-eqz v2, :cond_6

    move v0, v1

    .line 88
    goto :goto_0

    .line 91
    :cond_5
    iget-object v2, p0, Lcom/google/af/a/c/a/q;->b:Lcom/google/af/a/c/a/d;

    iget-object v3, p1, Lcom/google/af/a/c/a/q;->b:Lcom/google/af/a/c/a/d;

    invoke-virtual {v2, v3}, Lcom/google/af/a/c/a/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 92
    goto :goto_0

    .line 95
    :cond_6
    iget-wide v2, p0, Lcom/google/af/a/c/a/q;->c:J

    iget-wide v4, p1, Lcom/google/af/a/c/a/q;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    move v0, v1

    .line 96
    goto :goto_0

    .line 98
    :cond_7
    iget-object v2, p0, Lcom/google/af/a/c/a/q;->d:[Lcom/google/af/a/c/a/c;

    iget-object v3, p1, Lcom/google/af/a/c/a/q;->d:[Lcom/google/af/a/c/a/c;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 100
    goto :goto_0

    .line 102
    :cond_8
    iget v2, p0, Lcom/google/af/a/c/a/q;->e:I

    iget v3, p1, Lcom/google/af/a/c/a/q;->e:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 103
    goto :goto_0

    .line 105
    :cond_9
    iget-object v2, p0, Lcom/google/af/a/c/a/q;->f:Lcom/google/af/a/c/a/p;

    if-nez v2, :cond_a

    .line 106
    iget-object v2, p1, Lcom/google/af/a/c/a/q;->f:Lcom/google/af/a/c/a/p;

    if-eqz v2, :cond_b

    move v0, v1

    .line 107
    goto :goto_0

    .line 110
    :cond_a
    iget-object v2, p0, Lcom/google/af/a/c/a/q;->f:Lcom/google/af/a/c/a/p;

    iget-object v3, p1, Lcom/google/af/a/c/a/q;->f:Lcom/google/af/a/c/a/p;

    invoke-virtual {v2, v3}, Lcom/google/af/a/c/a/p;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 111
    goto :goto_0

    .line 114
    :cond_b
    iget-object v2, p0, Lcom/google/af/a/c/a/q;->g:Ljava/lang/String;

    if-nez v2, :cond_c

    .line 115
    iget-object v2, p1, Lcom/google/af/a/c/a/q;->g:Ljava/lang/String;

    if-eqz v2, :cond_d

    move v0, v1

    .line 116
    goto :goto_0

    .line 118
    :cond_c
    iget-object v2, p0, Lcom/google/af/a/c/a/q;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/c/a/q;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 119
    goto :goto_0

    .line 121
    :cond_d
    iget v2, p0, Lcom/google/af/a/c/a/q;->h:I

    iget v3, p1, Lcom/google/af/a/c/a/q;->h:I

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 122
    goto/16 :goto_0

    .line 124
    :cond_e
    iget-object v2, p0, Lcom/google/af/a/c/a/q;->i:Ljava/lang/String;

    if-nez v2, :cond_f

    .line 125
    iget-object v2, p1, Lcom/google/af/a/c/a/q;->i:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 126
    goto/16 :goto_0

    .line 128
    :cond_f
    iget-object v2, p0, Lcom/google/af/a/c/a/q;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/c/a/q;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 129
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 136
    iget-object v0, p0, Lcom/google/af/a/c/a/q;->a:Lcom/google/af/a/c/a;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 139
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/c/a/q;->b:Lcom/google/af/a/c/a/d;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 141
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/af/a/c/a/q;->c:J

    iget-wide v4, p0, Lcom/google/af/a/c/a/q;->c:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 143
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/af/a/c/a/q;->d:[Lcom/google/af/a/c/a/c;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 145
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/af/a/c/a/q;->e:I

    add-int/2addr v0, v2

    .line 146
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/c/a/q;->f:Lcom/google/af/a/c/a/p;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 148
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/c/a/q;->g:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 150
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/af/a/c/a/q;->h:I

    add-int/2addr v0, v2

    .line 151
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/af/a/c/a/q;->i:Ljava/lang/String;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 153
    return v0

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/c/a/q;->a:Lcom/google/af/a/c/a;

    invoke-virtual {v0}, Lcom/google/af/a/c/a;->hashCode()I

    move-result v0

    goto :goto_0

    .line 139
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/c/a/q;->b:Lcom/google/af/a/c/a/d;

    invoke-virtual {v0}, Lcom/google/af/a/c/a/d;->hashCode()I

    move-result v0

    goto :goto_1

    .line 146
    :cond_2
    iget-object v0, p0, Lcom/google/af/a/c/a/q;->f:Lcom/google/af/a/c/a/p;

    invoke-virtual {v0}, Lcom/google/af/a/c/a/p;->hashCode()I

    move-result v0

    goto :goto_2

    .line 148
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/c/a/q;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 151
    :cond_4
    iget-object v1, p0, Lcom/google/af/a/c/a/q;->i:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/af/a/c/a/q;->a:Lcom/google/af/a/c/a;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/af/a/c/a;

    invoke-direct {v0}, Lcom/google/af/a/c/a;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/c/a/q;->a:Lcom/google/af/a/c/a;

    :cond_1
    iget-object v0, p0, Lcom/google/af/a/c/a/q;->a:Lcom/google/af/a/c/a;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/af/a/c/a/q;->b:Lcom/google/af/a/c/a/d;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/af/a/c/a/d;

    invoke-direct {v0}, Lcom/google/af/a/c/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/c/a/q;->b:Lcom/google/af/a/c/a/d;

    :cond_2
    iget-object v0, p0, Lcom/google/af/a/c/a/q;->b:Lcom/google/af/a/c/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/af/a/c/a/q;->d:[Lcom/google/af/a/c/a/c;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/af/a/c/a/c;

    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/google/af/a/c/a/q;->d:[Lcom/google/af/a/c/a/c;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    new-instance v3, Lcom/google/af/a/c/a/c;

    invoke-direct {v3}, Lcom/google/af/a/c/a/c;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/af/a/c/a/q;->d:[Lcom/google/af/a/c/a/c;

    array-length v0, v0

    goto :goto_1

    :cond_5
    new-instance v3, Lcom/google/af/a/c/a/c;

    invoke-direct {v3}, Lcom/google/af/a/c/a/c;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/af/a/c/a/q;->d:[Lcom/google/af/a/c/a/c;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/af/a/c/a/q;->e:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/af/a/c/a/q;->f:Lcom/google/af/a/c/a/p;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/af/a/c/a/p;

    invoke-direct {v0}, Lcom/google/af/a/c/a/p;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/c/a/q;->f:Lcom/google/af/a/c/a/p;

    :cond_6
    iget-object v0, p0, Lcom/google/af/a/c/a/q;->f:Lcom/google/af/a/c/a/p;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/af/a/c/a/q;->c:J

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/c/a/q;->g:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/af/a/c/a/q;->h:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/c/a/q;->i:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/af/a/c/a/q;->a:Lcom/google/af/a/c/a;

    if-eqz v0, :cond_0

    .line 160
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/af/a/c/a/q;->a:Lcom/google/af/a/c/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/c/a/q;->b:Lcom/google/af/a/c/a/d;

    if-eqz v0, :cond_1

    .line 163
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/af/a/c/a/q;->b:Lcom/google/af/a/c/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/c/a/q;->d:[Lcom/google/af/a/c/a/c;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/af/a/c/a/q;->d:[Lcom/google/af/a/c/a/c;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 166
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/af/a/c/a/q;->d:[Lcom/google/af/a/c/a/c;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 167
    iget-object v1, p0, Lcom/google/af/a/c/a/q;->d:[Lcom/google/af/a/c/a/c;

    aget-object v1, v1, v0

    .line 168
    if-eqz v1, :cond_2

    .line 169
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 166
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 173
    :cond_3
    iget v0, p0, Lcom/google/af/a/c/a/q;->e:I

    if-eqz v0, :cond_4

    .line 174
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/af/a/c/a/q;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 176
    :cond_4
    iget-object v0, p0, Lcom/google/af/a/c/a/q;->f:Lcom/google/af/a/c/a/p;

    if-eqz v0, :cond_5

    .line 177
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/af/a/c/a/q;->f:Lcom/google/af/a/c/a/p;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 179
    :cond_5
    iget-wide v0, p0, Lcom/google/af/a/c/a/q;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_6

    .line 180
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/af/a/c/a/q;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 182
    :cond_6
    iget-object v0, p0, Lcom/google/af/a/c/a/q;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 183
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/af/a/c/a/q;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 185
    :cond_7
    iget v0, p0, Lcom/google/af/a/c/a/q;->h:I

    if-eqz v0, :cond_8

    .line 186
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/af/a/c/a/q;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 188
    :cond_8
    iget-object v0, p0, Lcom/google/af/a/c/a/q;->i:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 189
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/af/a/c/a/q;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 191
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 192
    return-void
.end method
