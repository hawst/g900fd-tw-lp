.class public final Lcom/google/af/a/b/a/a/s;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile b:[Lcom/google/af/a/b/a/a/s;


# instance fields
.field public a:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2330
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2331
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/s;->a:Ljava/lang/Long;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/b/a/a/s;->cachedSize:I

    .line 2332
    return-void
.end method

.method public static a()[Lcom/google/af/a/b/a/a/s;
    .locals 2

    .prologue
    .line 2316
    sget-object v0, Lcom/google/af/a/b/a/a/s;->b:[Lcom/google/af/a/b/a/a/s;

    if-nez v0, :cond_1

    .line 2317
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2319
    :try_start_0
    sget-object v0, Lcom/google/af/a/b/a/a/s;->b:[Lcom/google/af/a/b/a/a/s;

    if-nez v0, :cond_0

    .line 2320
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/af/a/b/a/a/s;

    sput-object v0, Lcom/google/af/a/b/a/a/s;->b:[Lcom/google/af/a/b/a/a/s;

    .line 2322
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2324
    :cond_1
    sget-object v0, Lcom/google/af/a/b/a/a/s;->b:[Lcom/google/af/a/b/a/a/s;

    return-object v0

    .line 2322
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 2378
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2379
    iget-object v1, p0, Lcom/google/af/a/b/a/a/s;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 2380
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/af/a/b/a/a/s;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2383
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2342
    if-ne p1, p0, :cond_1

    .line 2356
    :cond_0
    :goto_0
    return v0

    .line 2345
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/b/a/a/s;

    if-nez v2, :cond_2

    move v0, v1

    .line 2346
    goto :goto_0

    .line 2348
    :cond_2
    check-cast p1, Lcom/google/af/a/b/a/a/s;

    .line 2349
    iget-object v2, p0, Lcom/google/af/a/b/a/a/s;->a:Ljava/lang/Long;

    if-nez v2, :cond_3

    .line 2350
    iget-object v2, p1, Lcom/google/af/a/b/a/a/s;->a:Ljava/lang/Long;

    if-eqz v2, :cond_0

    move v0, v1

    .line 2351
    goto :goto_0

    .line 2353
    :cond_3
    iget-object v2, p0, Lcom/google/af/a/b/a/a/s;->a:Ljava/lang/Long;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/s;->a:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2354
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2361
    iget-object v0, p0, Lcom/google/af/a/b/a/a/s;->a:Ljava/lang/Long;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2364
    return v0

    .line 2361
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/b/a/a/s;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 2310
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/s;->a:Ljava/lang/Long;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 2370
    iget-object v0, p0, Lcom/google/af/a/b/a/a/s;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2371
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/af/a/b/a/a/s;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2373
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2374
    return-void
.end method
