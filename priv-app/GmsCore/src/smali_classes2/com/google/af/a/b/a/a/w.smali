.class public final Lcom/google/af/a/b/a/a/w;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/af/a/b/a/a/c;

.field public b:[Lcom/google/af/a/b/a/a/c;

.field public c:Ljava/lang/String;

.field public d:[Lcom/google/af/a/b/a/a/i;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1041
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1042
    invoke-static {}, Lcom/google/af/a/b/a/a/c;->a()[Lcom/google/af/a/b/a/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/w;->a:[Lcom/google/af/a/b/a/a/c;

    invoke-static {}, Lcom/google/af/a/b/a/a/c;->a()[Lcom/google/af/a/b/a/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/w;->b:[Lcom/google/af/a/b/a/a/c;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/w;->c:Ljava/lang/String;

    invoke-static {}, Lcom/google/af/a/b/a/a/i;->a()[Lcom/google/af/a/b/a/a/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/w;->d:[Lcom/google/af/a/b/a/a/i;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/b/a/a/w;->cachedSize:I

    .line 1043
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1134
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1135
    iget-object v2, p0, Lcom/google/af/a/b/a/a/w;->a:[Lcom/google/af/a/b/a/a/c;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/af/a/b/a/a/w;->a:[Lcom/google/af/a/b/a/a/c;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 1136
    :goto_0
    iget-object v3, p0, Lcom/google/af/a/b/a/a/w;->a:[Lcom/google/af/a/b/a/a/c;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 1137
    iget-object v3, p0, Lcom/google/af/a/b/a/a/w;->a:[Lcom/google/af/a/b/a/a/c;

    aget-object v3, v3, v0

    .line 1138
    if-eqz v3, :cond_0

    .line 1139
    const/4 v4, 0x1

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1136
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1144
    :cond_2
    iget-object v2, p0, Lcom/google/af/a/b/a/a/w;->b:[Lcom/google/af/a/b/a/a/c;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/af/a/b/a/a/w;->b:[Lcom/google/af/a/b/a/a/c;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 1145
    :goto_1
    iget-object v3, p0, Lcom/google/af/a/b/a/a/w;->b:[Lcom/google/af/a/b/a/a/c;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 1146
    iget-object v3, p0, Lcom/google/af/a/b/a/a/w;->b:[Lcom/google/af/a/b/a/a/c;

    aget-object v3, v3, v0

    .line 1147
    if-eqz v3, :cond_3

    .line 1148
    const/4 v4, 0x2

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1145
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v2

    .line 1153
    :cond_5
    iget-object v2, p0, Lcom/google/af/a/b/a/a/w;->c:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 1154
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/af/a/b/a/a/w;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1157
    :cond_6
    iget-object v2, p0, Lcom/google/af/a/b/a/a/w;->d:[Lcom/google/af/a/b/a/a/i;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/af/a/b/a/a/w;->d:[Lcom/google/af/a/b/a/a/i;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 1158
    :goto_2
    iget-object v2, p0, Lcom/google/af/a/b/a/a/w;->d:[Lcom/google/af/a/b/a/a/i;

    array-length v2, v2

    if-ge v1, v2, :cond_8

    .line 1159
    iget-object v2, p0, Lcom/google/af/a/b/a/a/w;->d:[Lcom/google/af/a/b/a/a/i;

    aget-object v2, v2, v1

    .line 1160
    if-eqz v2, :cond_7

    .line 1161
    const/4 v3, 0x4

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1158
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1166
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1056
    if-ne p1, p0, :cond_1

    .line 1082
    :cond_0
    :goto_0
    return v0

    .line 1059
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/b/a/a/w;

    if-nez v2, :cond_2

    move v0, v1

    .line 1060
    goto :goto_0

    .line 1062
    :cond_2
    check-cast p1, Lcom/google/af/a/b/a/a/w;

    .line 1063
    iget-object v2, p0, Lcom/google/af/a/b/a/a/w;->a:[Lcom/google/af/a/b/a/a/c;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/w;->a:[Lcom/google/af/a/b/a/a/c;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1065
    goto :goto_0

    .line 1067
    :cond_3
    iget-object v2, p0, Lcom/google/af/a/b/a/a/w;->b:[Lcom/google/af/a/b/a/a/c;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/w;->b:[Lcom/google/af/a/b/a/a/c;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1069
    goto :goto_0

    .line 1071
    :cond_4
    iget-object v2, p0, Lcom/google/af/a/b/a/a/w;->c:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 1072
    iget-object v2, p1, Lcom/google/af/a/b/a/a/w;->c:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 1073
    goto :goto_0

    .line 1075
    :cond_5
    iget-object v2, p0, Lcom/google/af/a/b/a/a/w;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/w;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1076
    goto :goto_0

    .line 1078
    :cond_6
    iget-object v2, p0, Lcom/google/af/a/b/a/a/w;->d:[Lcom/google/af/a/b/a/a/i;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/w;->d:[Lcom/google/af/a/b/a/a/i;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1080
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1087
    iget-object v0, p0, Lcom/google/af/a/b/a/a/w;->a:[Lcom/google/af/a/b/a/a/c;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 1090
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/af/a/b/a/a/w;->b:[Lcom/google/af/a/b/a/a/c;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1092
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/b/a/a/w;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 1094
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/af/a/b/a/a/w;->d:[Lcom/google/af/a/b/a/a/i;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1096
    return v0

    .line 1092
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/b/a/a/w;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1012
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/af/a/b/a/a/w;->a:[Lcom/google/af/a/b/a/a/c;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/af/a/b/a/a/c;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/af/a/b/a/a/w;->a:[Lcom/google/af/a/b/a/a/c;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/af/a/b/a/a/c;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/c;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/af/a/b/a/a/w;->a:[Lcom/google/af/a/b/a/a/c;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/af/a/b/a/a/c;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/c;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/af/a/b/a/a/w;->a:[Lcom/google/af/a/b/a/a/c;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/af/a/b/a/a/w;->b:[Lcom/google/af/a/b/a/a/c;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/af/a/b/a/a/c;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/af/a/b/a/a/w;->b:[Lcom/google/af/a/b/a/a/c;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/af/a/b/a/a/c;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/c;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/af/a/b/a/a/w;->b:[Lcom/google/af/a/b/a/a/c;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/af/a/b/a/a/c;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/c;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/af/a/b/a/a/w;->b:[Lcom/google/af/a/b/a/a/c;

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/w;->c:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/af/a/b/a/a/w;->d:[Lcom/google/af/a/b/a/a/i;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/af/a/b/a/a/i;

    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/google/af/a/b/a/a/w;->d:[Lcom/google/af/a/b/a/a/i;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Lcom/google/af/a/b/a/a/i;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/i;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Lcom/google/af/a/b/a/a/w;->d:[Lcom/google/af/a/b/a/a/i;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Lcom/google/af/a/b/a/a/i;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/i;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/af/a/b/a/a/w;->d:[Lcom/google/af/a/b/a/a/i;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1102
    iget-object v0, p0, Lcom/google/af/a/b/a/a/w;->a:[Lcom/google/af/a/b/a/a/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/af/a/b/a/a/w;->a:[Lcom/google/af/a/b/a/a/c;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 1103
    :goto_0
    iget-object v2, p0, Lcom/google/af/a/b/a/a/w;->a:[Lcom/google/af/a/b/a/a/c;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1104
    iget-object v2, p0, Lcom/google/af/a/b/a/a/w;->a:[Lcom/google/af/a/b/a/a/c;

    aget-object v2, v2, v0

    .line 1105
    if-eqz v2, :cond_0

    .line 1106
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1103
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1110
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/b/a/a/w;->b:[Lcom/google/af/a/b/a/a/c;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/af/a/b/a/a/w;->b:[Lcom/google/af/a/b/a/a/c;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 1111
    :goto_1
    iget-object v2, p0, Lcom/google/af/a/b/a/a/w;->b:[Lcom/google/af/a/b/a/a/c;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 1112
    iget-object v2, p0, Lcom/google/af/a/b/a/a/w;->b:[Lcom/google/af/a/b/a/a/c;

    aget-object v2, v2, v0

    .line 1113
    if-eqz v2, :cond_2

    .line 1114
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1111
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1118
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/b/a/a/w;->c:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1119
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/af/a/b/a/a/w;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1121
    :cond_4
    iget-object v0, p0, Lcom/google/af/a/b/a/a/w;->d:[Lcom/google/af/a/b/a/a/i;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/af/a/b/a/a/w;->d:[Lcom/google/af/a/b/a/a/i;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 1122
    :goto_2
    iget-object v0, p0, Lcom/google/af/a/b/a/a/w;->d:[Lcom/google/af/a/b/a/a/i;

    array-length v0, v0

    if-ge v1, v0, :cond_6

    .line 1123
    iget-object v0, p0, Lcom/google/af/a/b/a/a/w;->d:[Lcom/google/af/a/b/a/a/i;

    aget-object v0, v0, v1

    .line 1124
    if-eqz v0, :cond_5

    .line 1125
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1122
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1129
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1130
    return-void
.end method
