.class public final Lcom/google/af/a/c/a/a;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 70
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 71
    iput-boolean v0, p0, Lcom/google/af/a/c/a/a;->a:Z

    iput v0, p0, Lcom/google/af/a/c/a/a;->b:I

    iput v0, p0, Lcom/google/af/a/c/a/a;->c:I

    iput v0, p0, Lcom/google/af/a/c/a/a;->d:I

    iput v0, p0, Lcom/google/af/a/c/a/a;->e:I

    iput v0, p0, Lcom/google/af/a/c/a/a;->f:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/c/a/a;->cachedSize:I

    .line 72
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 153
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 154
    iget-boolean v1, p0, Lcom/google/af/a/c/a/a;->a:Z

    if-eqz v1, :cond_0

    .line 155
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/af/a/c/a/a;->a:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 158
    :cond_0
    iget v1, p0, Lcom/google/af/a/c/a/a;->b:I

    if-eqz v1, :cond_1

    .line 159
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/af/a/c/a/a;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 162
    :cond_1
    iget v1, p0, Lcom/google/af/a/c/a/a;->c:I

    if-eqz v1, :cond_2

    .line 163
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/af/a/c/a/a;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 166
    :cond_2
    iget v1, p0, Lcom/google/af/a/c/a/a;->d:I

    if-eqz v1, :cond_3

    .line 167
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/af/a/c/a/a;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    :cond_3
    iget v1, p0, Lcom/google/af/a/c/a/a;->e:I

    if-eqz v1, :cond_4

    .line 171
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/af/a/c/a/a;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    :cond_4
    iget v1, p0, Lcom/google/af/a/c/a/a;->f:I

    if-eqz v1, :cond_5

    .line 175
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/af/a/c/a/a;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 178
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 87
    if-ne p1, p0, :cond_1

    .line 112
    :cond_0
    :goto_0
    return v0

    .line 90
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/c/a/a;

    if-nez v2, :cond_2

    move v0, v1

    .line 91
    goto :goto_0

    .line 93
    :cond_2
    check-cast p1, Lcom/google/af/a/c/a/a;

    .line 94
    iget-boolean v2, p0, Lcom/google/af/a/c/a/a;->a:Z

    iget-boolean v3, p1, Lcom/google/af/a/c/a/a;->a:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 95
    goto :goto_0

    .line 97
    :cond_3
    iget v2, p0, Lcom/google/af/a/c/a/a;->b:I

    iget v3, p1, Lcom/google/af/a/c/a/a;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 98
    goto :goto_0

    .line 100
    :cond_4
    iget v2, p0, Lcom/google/af/a/c/a/a;->c:I

    iget v3, p1, Lcom/google/af/a/c/a/a;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 101
    goto :goto_0

    .line 103
    :cond_5
    iget v2, p0, Lcom/google/af/a/c/a/a;->d:I

    iget v3, p1, Lcom/google/af/a/c/a/a;->d:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 104
    goto :goto_0

    .line 106
    :cond_6
    iget v2, p0, Lcom/google/af/a/c/a/a;->e:I

    iget v3, p1, Lcom/google/af/a/c/a/a;->e:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 107
    goto :goto_0

    .line 109
    :cond_7
    iget v2, p0, Lcom/google/af/a/c/a/a;->f:I

    iget v3, p1, Lcom/google/af/a/c/a/a;->f:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 110
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/google/af/a/c/a/a;->a:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 119
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/af/a/c/a/a;->b:I

    add-int/2addr v0, v1

    .line 120
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/af/a/c/a/a;->c:I

    add-int/2addr v0, v1

    .line 121
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/af/a/c/a/a;->d:I

    add-int/2addr v0, v1

    .line 122
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/af/a/c/a/a;->e:I

    add-int/2addr v0, v1

    .line 123
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/af/a/c/a/a;->f:I

    add-int/2addr v0, v1

    .line 124
    return v0

    .line 117
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/af/a/c/a/a;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/af/a/c/a/a;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/af/a/c/a/a;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/af/a/c/a/a;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/af/a/c/a/a;->e:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    iput v0, p0, Lcom/google/af/a/c/a/a;->f:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/google/af/a/c/a/a;->a:Z

    if-eqz v0, :cond_0

    .line 131
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/af/a/c/a/a;->a:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 133
    :cond_0
    iget v0, p0, Lcom/google/af/a/c/a/a;->b:I

    if-eqz v0, :cond_1

    .line 134
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/af/a/c/a/a;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 136
    :cond_1
    iget v0, p0, Lcom/google/af/a/c/a/a;->c:I

    if-eqz v0, :cond_2

    .line 137
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/af/a/c/a/a;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 139
    :cond_2
    iget v0, p0, Lcom/google/af/a/c/a/a;->d:I

    if-eqz v0, :cond_3

    .line 140
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/af/a/c/a/a;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 142
    :cond_3
    iget v0, p0, Lcom/google/af/a/c/a/a;->e:I

    if-eqz v0, :cond_4

    .line 143
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/af/a/c/a/a;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 145
    :cond_4
    iget v0, p0, Lcom/google/af/a/c/a/a;->f:I

    if-eqz v0, :cond_5

    .line 146
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/af/a/c/a/a;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 148
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 149
    return-void
.end method
