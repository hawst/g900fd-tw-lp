.class public final Lcom/google/af/a/c/a/l;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 33
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/af/a/c/a/l;->a:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/c/a/l;->b:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/af/a/c/a/l;->c:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/c/a/l;->cachedSize:I

    .line 34
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 97
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 98
    iget-wide v2, p0, Lcom/google/af/a/c/a/l;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 99
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/af/a/c/a/l;->a:J

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 102
    :cond_0
    iget-object v1, p0, Lcom/google/af/a/c/a/l;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 103
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/af/a/c/a/l;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 106
    :cond_1
    iget-boolean v1, p0, Lcom/google/af/a/c/a/l;->c:Z

    if-eqz v1, :cond_2

    .line 107
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/af/a/c/a/l;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 110
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 46
    if-ne p1, p0, :cond_1

    .line 66
    :cond_0
    :goto_0
    return v0

    .line 49
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/c/a/l;

    if-nez v2, :cond_2

    move v0, v1

    .line 50
    goto :goto_0

    .line 52
    :cond_2
    check-cast p1, Lcom/google/af/a/c/a/l;

    .line 53
    iget-wide v2, p0, Lcom/google/af/a/c/a/l;->a:J

    iget-wide v4, p1, Lcom/google/af/a/c/a/l;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 54
    goto :goto_0

    .line 56
    :cond_3
    iget-object v2, p0, Lcom/google/af/a/c/a/l;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 57
    iget-object v2, p1, Lcom/google/af/a/c/a/l;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 58
    goto :goto_0

    .line 60
    :cond_4
    iget-object v2, p0, Lcom/google/af/a/c/a/l;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/c/a/l;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 61
    goto :goto_0

    .line 63
    :cond_5
    iget-boolean v2, p0, Lcom/google/af/a/c/a/l;->c:Z

    iget-boolean v3, p1, Lcom/google/af/a/c/a/l;->c:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 64
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/google/af/a/c/a/l;->a:J

    iget-wide v2, p0, Lcom/google/af/a/c/a/l;->a:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    .line 74
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/c/a/l;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 76
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/af/a/c/a/l;->c:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x4cf

    :goto_1
    add-int/2addr v0, v1

    .line 77
    return v0

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/c/a/l;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 76
    :cond_1
    const/16 v0, 0x4d5

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/af/a/c/a/l;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/c/a/l;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/af/a/c/a/l;->c:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/google/af/a/c/a/l;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 84
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/af/a/c/a/l;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)V

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/c/a/l;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 87
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/af/a/c/a/l;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 89
    :cond_1
    iget-boolean v0, p0, Lcom/google/af/a/c/a/l;->c:Z

    if-eqz v0, :cond_2

    .line 90
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/af/a/c/a/l;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 92
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 93
    return-void
.end method
