.class public final Lcom/google/af/a/b/a/a/o;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/af/a/b/a/a/o;


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/String;

.field public c:Lcom/google/af/a/b/a/a/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1290
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1291
    iput-object v0, p0, Lcom/google/af/a/b/a/a/o;->a:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/o;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/o;->c:Lcom/google/af/a/b/a/a/c;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/b/a/a/o;->cachedSize:I

    .line 1292
    return-void
.end method

.method public static a([B)Lcom/google/af/a/b/a/a/o;
    .locals 1

    .prologue
    .line 1414
    new-instance v0, Lcom/google/af/a/b/a/a/o;

    invoke-direct {v0}, Lcom/google/af/a/b/a/a/o;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/af/a/b/a/a/o;

    return-object v0
.end method

.method public static a()[Lcom/google/af/a/b/a/a/o;
    .locals 2

    .prologue
    .line 1270
    sget-object v0, Lcom/google/af/a/b/a/a/o;->d:[Lcom/google/af/a/b/a/a/o;

    if-nez v0, :cond_1

    .line 1271
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1273
    :try_start_0
    sget-object v0, Lcom/google/af/a/b/a/a/o;->d:[Lcom/google/af/a/b/a/a/o;

    if-nez v0, :cond_0

    .line 1274
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/af/a/b/a/a/o;

    sput-object v0, Lcom/google/af/a/b/a/a/o;->d:[Lcom/google/af/a/b/a/a/o;

    .line 1276
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1278
    :cond_1
    sget-object v0, Lcom/google/af/a/b/a/a/o;->d:[Lcom/google/af/a/b/a/a/o;

    return-object v0

    .line 1276
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1364
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1365
    iget-object v1, p0, Lcom/google/af/a/b/a/a/o;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 1366
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/af/a/b/a/a/o;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1369
    :cond_0
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/af/a/b/a/a/o;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1371
    iget-object v1, p0, Lcom/google/af/a/b/a/a/o;->c:Lcom/google/af/a/b/a/a/c;

    if-eqz v1, :cond_1

    .line 1372
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/af/a/b/a/a/o;->c:Lcom/google/af/a/b/a/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1375
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1304
    if-ne p1, p0, :cond_1

    .line 1334
    :cond_0
    :goto_0
    return v0

    .line 1307
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/b/a/a/o;

    if-nez v2, :cond_2

    move v0, v1

    .line 1308
    goto :goto_0

    .line 1310
    :cond_2
    check-cast p1, Lcom/google/af/a/b/a/a/o;

    .line 1311
    iget-object v2, p0, Lcom/google/af/a/b/a/a/o;->a:Ljava/lang/Boolean;

    if-nez v2, :cond_3

    .line 1312
    iget-object v2, p1, Lcom/google/af/a/b/a/a/o;->a:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    move v0, v1

    .line 1313
    goto :goto_0

    .line 1315
    :cond_3
    iget-object v2, p0, Lcom/google/af/a/b/a/a/o;->a:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/o;->a:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1316
    goto :goto_0

    .line 1318
    :cond_4
    iget-object v2, p0, Lcom/google/af/a/b/a/a/o;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 1319
    iget-object v2, p1, Lcom/google/af/a/b/a/a/o;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 1320
    goto :goto_0

    .line 1322
    :cond_5
    iget-object v2, p0, Lcom/google/af/a/b/a/a/o;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/o;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1323
    goto :goto_0

    .line 1325
    :cond_6
    iget-object v2, p0, Lcom/google/af/a/b/a/a/o;->c:Lcom/google/af/a/b/a/a/c;

    if-nez v2, :cond_7

    .line 1326
    iget-object v2, p1, Lcom/google/af/a/b/a/a/o;->c:Lcom/google/af/a/b/a/a/c;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1327
    goto :goto_0

    .line 1330
    :cond_7
    iget-object v2, p0, Lcom/google/af/a/b/a/a/o;->c:Lcom/google/af/a/b/a/a/c;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/o;->c:Lcom/google/af/a/b/a/a/c;

    invoke-virtual {v2, v3}, Lcom/google/af/a/b/a/a/c;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1331
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1339
    iget-object v0, p0, Lcom/google/af/a/b/a/a/o;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1342
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/b/a/a/o;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1344
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/af/a/b/a/a/o;->c:Lcom/google/af/a/b/a/a/c;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 1346
    return v0

    .line 1339
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/b/a/a/o;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1342
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/b/a/a/o;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1344
    :cond_2
    iget-object v1, p0, Lcom/google/af/a/b/a/a/o;->c:Lcom/google/af/a/b/a/a/c;

    invoke-virtual {v1}, Lcom/google/af/a/b/a/a/c;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1264
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/o;->a:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/o;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/af/a/b/a/a/o;->c:Lcom/google/af/a/b/a/a/c;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/af/a/b/a/a/c;

    invoke-direct {v0}, Lcom/google/af/a/b/a/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/b/a/a/o;->c:Lcom/google/af/a/b/a/a/c;

    :cond_1
    iget-object v0, p0, Lcom/google/af/a/b/a/a/o;->c:Lcom/google/af/a/b/a/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1352
    iget-object v0, p0, Lcom/google/af/a/b/a/a/o;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 1353
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/af/a/b/a/a/o;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1355
    :cond_0
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/af/a/b/a/a/o;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1356
    iget-object v0, p0, Lcom/google/af/a/b/a/a/o;->c:Lcom/google/af/a/b/a/a/c;

    if-eqz v0, :cond_1

    .line 1357
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/af/a/b/a/a/o;->c:Lcom/google/af/a/b/a/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1359
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1360
    return-void
.end method
