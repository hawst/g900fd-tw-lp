.class public final Lcom/google/af/a/c/a/d;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 33
    iput-boolean v0, p0, Lcom/google/af/a/c/a/d;->a:Z

    iput-boolean v0, p0, Lcom/google/af/a/c/a/d;->b:Z

    iput-boolean v0, p0, Lcom/google/af/a/c/a/d;->c:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/c/a/d;->cachedSize:I

    .line 34
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 91
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 92
    iget-boolean v1, p0, Lcom/google/af/a/c/a/d;->a:Z

    if-eqz v1, :cond_0

    .line 93
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/af/a/c/a/d;->a:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 96
    :cond_0
    iget-boolean v1, p0, Lcom/google/af/a/c/a/d;->b:Z

    if-eqz v1, :cond_1

    .line 97
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/af/a/c/a/d;->b:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 100
    :cond_1
    iget-boolean v1, p0, Lcom/google/af/a/c/a/d;->c:Z

    if-eqz v1, :cond_2

    .line 101
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/af/a/c/a/d;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 104
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 46
    if-ne p1, p0, :cond_1

    .line 62
    :cond_0
    :goto_0
    return v0

    .line 49
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/c/a/d;

    if-nez v2, :cond_2

    move v0, v1

    .line 50
    goto :goto_0

    .line 52
    :cond_2
    check-cast p1, Lcom/google/af/a/c/a/d;

    .line 53
    iget-boolean v2, p0, Lcom/google/af/a/c/a/d;->a:Z

    iget-boolean v3, p1, Lcom/google/af/a/c/a/d;->a:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 54
    goto :goto_0

    .line 56
    :cond_3
    iget-boolean v2, p0, Lcom/google/af/a/c/a/d;->b:Z

    iget-boolean v3, p1, Lcom/google/af/a/c/a/d;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 57
    goto :goto_0

    .line 59
    :cond_4
    iget-boolean v2, p0, Lcom/google/af/a/c/a/d;->c:Z

    iget-boolean v3, p1, Lcom/google/af/a/c/a/d;->c:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 60
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    .line 67
    iget-boolean v0, p0, Lcom/google/af/a/c/a/d;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 69
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/af/a/c/a/d;->b:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    .line 70
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/google/af/a/c/a/d;->c:Z

    if-eqz v3, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 71
    return v0

    :cond_0
    move v0, v2

    .line 67
    goto :goto_0

    :cond_1
    move v0, v2

    .line 69
    goto :goto_1

    :cond_2
    move v1, v2

    .line 70
    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/af/a/c/a/d;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/af/a/c/a/d;->b:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/af/a/c/a/d;->c:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/google/af/a/c/a/d;->a:Z

    if-eqz v0, :cond_0

    .line 78
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/af/a/c/a/d;->a:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 80
    :cond_0
    iget-boolean v0, p0, Lcom/google/af/a/c/a/d;->b:Z

    if-eqz v0, :cond_1

    .line 81
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/af/a/c/a/d;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 83
    :cond_1
    iget-boolean v0, p0, Lcom/google/af/a/c/a/d;->c:Z

    if-eqz v0, :cond_2

    .line 84
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/af/a/c/a/d;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 86
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 87
    return-void
.end method
