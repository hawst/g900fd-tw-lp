.class public final Lcom/google/af/a/b/a/a/ag;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/af/a/b/a/a/ah;

.field public b:[Lcom/google/af/a/b/a/a/ah;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1740
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1741
    invoke-static {}, Lcom/google/af/a/b/a/a/ah;->a()[Lcom/google/af/a/b/a/a/ah;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ag;->a:[Lcom/google/af/a/b/a/a/ah;

    invoke-static {}, Lcom/google/af/a/b/a/a/ah;->a()[Lcom/google/af/a/b/a/a/ah;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ag;->b:[Lcom/google/af/a/b/a/a/ah;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ag;->c:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/b/a/a/ag;->cachedSize:I

    .line 1742
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1818
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1819
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ag;->a:[Lcom/google/af/a/b/a/a/ah;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/af/a/b/a/a/ag;->a:[Lcom/google/af/a/b/a/a/ah;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 1820
    :goto_0
    iget-object v3, p0, Lcom/google/af/a/b/a/a/ag;->a:[Lcom/google/af/a/b/a/a/ah;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 1821
    iget-object v3, p0, Lcom/google/af/a/b/a/a/ag;->a:[Lcom/google/af/a/b/a/a/ah;

    aget-object v3, v3, v0

    .line 1822
    if-eqz v3, :cond_0

    .line 1823
    const/4 v4, 0x1

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1820
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1828
    :cond_2
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ag;->b:[Lcom/google/af/a/b/a/a/ah;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/af/a/b/a/a/ag;->b:[Lcom/google/af/a/b/a/a/ah;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 1829
    :goto_1
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ag;->b:[Lcom/google/af/a/b/a/a/ah;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 1830
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ag;->b:[Lcom/google/af/a/b/a/a/ah;

    aget-object v2, v2, v1

    .line 1831
    if-eqz v2, :cond_3

    .line 1832
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1829
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1837
    :cond_4
    iget-object v1, p0, Lcom/google/af/a/b/a/a/ag;->c:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1838
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/af/a/b/a/a/ag;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1841
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1754
    if-ne p1, p0, :cond_1

    .line 1776
    :cond_0
    :goto_0
    return v0

    .line 1757
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/b/a/a/ag;

    if-nez v2, :cond_2

    move v0, v1

    .line 1758
    goto :goto_0

    .line 1760
    :cond_2
    check-cast p1, Lcom/google/af/a/b/a/a/ag;

    .line 1761
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ag;->a:[Lcom/google/af/a/b/a/a/ah;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/ag;->a:[Lcom/google/af/a/b/a/a/ah;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1763
    goto :goto_0

    .line 1765
    :cond_3
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ag;->b:[Lcom/google/af/a/b/a/a/ah;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/ag;->b:[Lcom/google/af/a/b/a/a/ah;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1767
    goto :goto_0

    .line 1769
    :cond_4
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ag;->c:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 1770
    iget-object v2, p1, Lcom/google/af/a/b/a/a/ag;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1771
    goto :goto_0

    .line 1773
    :cond_5
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ag;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/ag;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1774
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1781
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ag;->a:[Lcom/google/af/a/b/a/a/ah;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 1784
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/af/a/b/a/a/ag;->b:[Lcom/google/af/a/b/a/a/ah;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1786
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/b/a/a/ag;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 1788
    return v0

    .line 1786
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ag;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1714
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/af/a/b/a/a/ag;->a:[Lcom/google/af/a/b/a/a/ah;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/af/a/b/a/a/ah;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/af/a/b/a/a/ag;->a:[Lcom/google/af/a/b/a/a/ah;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/af/a/b/a/a/ah;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/ah;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ag;->a:[Lcom/google/af/a/b/a/a/ah;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/af/a/b/a/a/ah;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/ah;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/af/a/b/a/a/ag;->a:[Lcom/google/af/a/b/a/a/ah;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/af/a/b/a/a/ag;->b:[Lcom/google/af/a/b/a/a/ah;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/af/a/b/a/a/ah;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/af/a/b/a/a/ag;->b:[Lcom/google/af/a/b/a/a/ah;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/af/a/b/a/a/ah;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/ah;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ag;->b:[Lcom/google/af/a/b/a/a/ah;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/af/a/b/a/a/ah;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/ah;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/af/a/b/a/a/ag;->b:[Lcom/google/af/a/b/a/a/ah;

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ag;->c:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1794
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ag;->a:[Lcom/google/af/a/b/a/a/ah;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/af/a/b/a/a/ag;->a:[Lcom/google/af/a/b/a/a/ah;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 1795
    :goto_0
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ag;->a:[Lcom/google/af/a/b/a/a/ah;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1796
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ag;->a:[Lcom/google/af/a/b/a/a/ah;

    aget-object v2, v2, v0

    .line 1797
    if-eqz v2, :cond_0

    .line 1798
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1795
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1802
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ag;->b:[Lcom/google/af/a/b/a/a/ah;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/af/a/b/a/a/ag;->b:[Lcom/google/af/a/b/a/a/ah;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 1803
    :goto_1
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ag;->b:[Lcom/google/af/a/b/a/a/ah;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 1804
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ag;->b:[Lcom/google/af/a/b/a/a/ah;

    aget-object v0, v0, v1

    .line 1805
    if-eqz v0, :cond_2

    .line 1806
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1803
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1810
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ag;->c:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1811
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/af/a/b/a/a/ag;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1813
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1814
    return-void
.end method
