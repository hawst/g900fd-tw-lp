.class public final Lcom/google/af/a/c/a/f;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[J

.field public b:[J

.field public c:Z

.field public d:J

.field public e:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 38
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 39
    sget-object v0, Lcom/google/protobuf/nano/m;->b:[J

    iput-object v0, p0, Lcom/google/af/a/c/a/f;->a:[J

    sget-object v0, Lcom/google/protobuf/nano/m;->b:[J

    iput-object v0, p0, Lcom/google/af/a/c/a/f;->b:[J

    iput-boolean v2, p0, Lcom/google/af/a/c/a/f;->c:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/af/a/c/a/f;->d:J

    iput-boolean v2, p0, Lcom/google/af/a/c/a/f;->e:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/c/a/f;->cachedSize:I

    .line 40
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 122
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 123
    iget-object v1, p0, Lcom/google/af/a/c/a/f;->a:[J

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/af/a/c/a/f;->a:[J

    array-length v1, v1

    if-lez v1, :cond_0

    .line 124
    iget-object v1, p0, Lcom/google/af/a/c/a/f;->a:[J

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x8

    .line 125
    add-int/2addr v0, v1

    .line 126
    iget-object v1, p0, Lcom/google/af/a/c/a/f;->a:[J

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 128
    :cond_0
    iget-object v1, p0, Lcom/google/af/a/c/a/f;->b:[J

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/af/a/c/a/f;->b:[J

    array-length v1, v1

    if-lez v1, :cond_1

    .line 129
    iget-object v1, p0, Lcom/google/af/a/c/a/f;->b:[J

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x8

    .line 130
    add-int/2addr v0, v1

    .line 131
    iget-object v1, p0, Lcom/google/af/a/c/a/f;->b:[J

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 133
    :cond_1
    iget-boolean v1, p0, Lcom/google/af/a/c/a/f;->c:Z

    if-eqz v1, :cond_2

    .line 134
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/af/a/c/a/f;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 137
    :cond_2
    iget-wide v2, p0, Lcom/google/af/a/c/a/f;->d:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 138
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/af/a/c/a/f;->d:J

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 141
    :cond_3
    iget-boolean v1, p0, Lcom/google/af/a/c/a/f;->e:Z

    if-eqz v1, :cond_4

    .line 142
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/af/a/c/a/f;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 145
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 54
    if-ne p1, p0, :cond_1

    .line 78
    :cond_0
    :goto_0
    return v0

    .line 57
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/c/a/f;

    if-nez v2, :cond_2

    move v0, v1

    .line 58
    goto :goto_0

    .line 60
    :cond_2
    check-cast p1, Lcom/google/af/a/c/a/f;

    .line 61
    iget-object v2, p0, Lcom/google/af/a/c/a/f;->a:[J

    iget-object v3, p1, Lcom/google/af/a/c/a/f;->a:[J

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([J[J)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 63
    goto :goto_0

    .line 65
    :cond_3
    iget-object v2, p0, Lcom/google/af/a/c/a/f;->b:[J

    iget-object v3, p1, Lcom/google/af/a/c/a/f;->b:[J

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([J[J)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 67
    goto :goto_0

    .line 69
    :cond_4
    iget-boolean v2, p0, Lcom/google/af/a/c/a/f;->c:Z

    iget-boolean v3, p1, Lcom/google/af/a/c/a/f;->c:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 70
    goto :goto_0

    .line 72
    :cond_5
    iget-wide v2, p0, Lcom/google/af/a/c/a/f;->d:J

    iget-wide v4, p1, Lcom/google/af/a/c/a/f;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    .line 73
    goto :goto_0

    .line 75
    :cond_6
    iget-boolean v2, p0, Lcom/google/af/a/c/a/f;->e:Z

    iget-boolean v3, p1, Lcom/google/af/a/c/a/f;->e:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 76
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 8

    .prologue
    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    .line 83
    iget-object v0, p0, Lcom/google/af/a/c/a/f;->a:[J

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([J)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 86
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lcom/google/af/a/c/a/f;->b:[J

    invoke-static {v3}, Lcom/google/protobuf/nano/h;->a([J)I

    move-result v3

    add-int/2addr v0, v3

    .line 88
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/af/a/c/a/f;->c:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    .line 89
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/af/a/c/a/f;->d:J

    iget-wide v6, p0, Lcom/google/af/a/c/a/f;->d:J

    const/16 v3, 0x20

    ushr-long/2addr v6, v3

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int/2addr v0, v3

    .line 91
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/google/af/a/c/a/f;->e:Z

    if-eqz v3, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 92
    return v0

    :cond_0
    move v0, v2

    .line 88
    goto :goto_0

    :cond_1
    move v1, v2

    .line 91
    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x9

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/af/a/c/a/f;->a:[J

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [J

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/af/a/c/a/f;->a:[J

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    aput-wide v4, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/af/a/c/a/f;->a:[J

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    aput-wide v4, v2, v0

    iput-object v2, p0, Lcom/google/af/a/c/a/f;->a:[J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v2

    div-int/lit8 v3, v0, 0x8

    iget-object v0, p0, Lcom/google/af/a/c/a/f;->a:[J

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v3, v0

    new-array v3, v3, [J

    if-eqz v0, :cond_4

    iget-object v4, p0, Lcom/google/af/a/c/a/f;->a:[J

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v4, v3

    if-ge v0, v4, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    aput-wide v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/af/a/c/a/f;->a:[J

    array-length v0, v0

    goto :goto_3

    :cond_6
    iput-object v3, p0, Lcom/google/af/a/c/a/f;->a:[J

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->d(I)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x11

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/af/a/c/a/f;->b:[J

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [J

    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/google/af/a/c/a/f;->b:[J

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    aput-wide v4, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Lcom/google/af/a/c/a/f;->b:[J

    array-length v0, v0

    goto :goto_5

    :cond_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    aput-wide v4, v2, v0

    iput-object v2, p0, Lcom/google/af/a/c/a/f;->b:[J

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v2

    div-int/lit8 v3, v0, 0x8

    iget-object v0, p0, Lcom/google/af/a/c/a/f;->b:[J

    if-nez v0, :cond_b

    move v0, v1

    :goto_7
    add-int/2addr v3, v0

    new-array v3, v3, [J

    if-eqz v0, :cond_a

    iget-object v4, p0, Lcom/google/af/a/c/a/f;->b:[J

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    :goto_8
    array-length v4, v3

    if-ge v0, v4, :cond_c

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    aput-wide v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_b
    iget-object v0, p0, Lcom/google/af/a/c/a/f;->b:[J

    array-length v0, v0

    goto :goto_7

    :cond_c
    iput-object v3, p0, Lcom/google/af/a/c/a/f;->b:[J

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/af/a/c/a/f;->c:Z

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/af/a/c/a/f;->d:J

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/af/a/c/a/f;->e:Z

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0xa -> :sswitch_2
        0x11 -> :sswitch_3
        0x12 -> :sswitch_4
        0x18 -> :sswitch_5
        0x21 -> :sswitch_6
        0x28 -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 98
    iget-object v0, p0, Lcom/google/af/a/c/a/f;->a:[J

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/af/a/c/a/f;->a:[J

    array-length v0, v0

    if-lez v0, :cond_0

    move v0, v1

    .line 99
    :goto_0
    iget-object v2, p0, Lcom/google/af/a/c/a/f;->a:[J

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 100
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/af/a/c/a/f;->a:[J

    aget-wide v4, v3, v0

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/b;->c(IJ)V

    .line 99
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/c/a/f;->b:[J

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/af/a/c/a/f;->b:[J

    array-length v0, v0

    if-lez v0, :cond_1

    .line 104
    :goto_1
    iget-object v0, p0, Lcom/google/af/a/c/a/f;->b:[J

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 105
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/af/a/c/a/f;->b:[J

    aget-wide v2, v2, v1

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)V

    .line 104
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 108
    :cond_1
    iget-boolean v0, p0, Lcom/google/af/a/c/a/f;->c:Z

    if-eqz v0, :cond_2

    .line 109
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/af/a/c/a/f;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 111
    :cond_2
    iget-wide v0, p0, Lcom/google/af/a/c/a/f;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 112
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/af/a/c/a/f;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)V

    .line 114
    :cond_3
    iget-boolean v0, p0, Lcom/google/af/a/c/a/f;->e:Z

    if-eqz v0, :cond_4

    .line 115
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/af/a/c/a/f;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 117
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 118
    return-void
.end method
