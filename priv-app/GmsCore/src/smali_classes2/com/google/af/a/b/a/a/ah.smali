.class public final Lcom/google/af/a/b/a/a/ah;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile i:[Lcom/google/af/a/b/a/a/ah;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/Long;

.field public g:Lcom/google/af/a/b/a/a/b;

.field public h:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2317
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2318
    iput-object v0, p0, Lcom/google/af/a/b/a/a/ah;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ah;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ah;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ah;->d:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ah;->e:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ah;->f:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ah;->g:Lcom/google/af/a/b/a/a/b;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ah;->h:Ljava/lang/Integer;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/b/a/a/ah;->cachedSize:I

    .line 2319
    return-void
.end method

.method public static a([B)Lcom/google/af/a/b/a/a/ah;
    .locals 1

    .prologue
    .line 2550
    new-instance v0, Lcom/google/af/a/b/a/a/ah;

    invoke-direct {v0}, Lcom/google/af/a/b/a/a/ah;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/af/a/b/a/a/ah;

    return-object v0
.end method

.method public static a()[Lcom/google/af/a/b/a/a/ah;
    .locals 2

    .prologue
    .line 2282
    sget-object v0, Lcom/google/af/a/b/a/a/ah;->i:[Lcom/google/af/a/b/a/a/ah;

    if-nez v0, :cond_1

    .line 2283
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2285
    :try_start_0
    sget-object v0, Lcom/google/af/a/b/a/a/ah;->i:[Lcom/google/af/a/b/a/a/ah;

    if-nez v0, :cond_0

    .line 2286
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/af/a/b/a/a/ah;

    sput-object v0, Lcom/google/af/a/b/a/a/ah;->i:[Lcom/google/af/a/b/a/a/ah;

    .line 2288
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2290
    :cond_1
    sget-object v0, Lcom/google/af/a/b/a/a/ah;->i:[Lcom/google/af/a/b/a/a/ah;

    return-object v0

    .line 2288
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 2458
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2459
    iget-object v1, p0, Lcom/google/af/a/b/a/a/ah;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2460
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2463
    :cond_0
    iget-object v1, p0, Lcom/google/af/a/b/a/a/ah;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2464
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2467
    :cond_1
    iget-object v1, p0, Lcom/google/af/a/b/a/a/ah;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2468
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2471
    :cond_2
    iget-object v1, p0, Lcom/google/af/a/b/a/a/ah;->d:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 2472
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2475
    :cond_3
    iget-object v1, p0, Lcom/google/af/a/b/a/a/ah;->e:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 2476
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->e:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2479
    :cond_4
    iget-object v1, p0, Lcom/google/af/a/b/a/a/ah;->f:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 2480
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->f:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2483
    :cond_5
    iget-object v1, p0, Lcom/google/af/a/b/a/a/ah;->g:Lcom/google/af/a/b/a/a/b;

    if-eqz v1, :cond_6

    .line 2484
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->g:Lcom/google/af/a/b/a/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2487
    :cond_6
    iget-object v1, p0, Lcom/google/af/a/b/a/a/ah;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 2488
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->h:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2491
    :cond_7
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2336
    if-ne p1, p0, :cond_1

    .line 2401
    :cond_0
    :goto_0
    return v0

    .line 2339
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/b/a/a/ah;

    if-nez v2, :cond_2

    move v0, v1

    .line 2340
    goto :goto_0

    .line 2342
    :cond_2
    check-cast p1, Lcom/google/af/a/b/a/a/ah;

    .line 2343
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 2344
    iget-object v2, p1, Lcom/google/af/a/b/a/a/ah;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 2345
    goto :goto_0

    .line 2347
    :cond_3
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/ah;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 2348
    goto :goto_0

    .line 2350
    :cond_4
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 2351
    iget-object v2, p1, Lcom/google/af/a/b/a/a/ah;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 2352
    goto :goto_0

    .line 2354
    :cond_5
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/ah;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 2355
    goto :goto_0

    .line 2357
    :cond_6
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2358
    iget-object v2, p1, Lcom/google/af/a/b/a/a/ah;->c:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 2359
    goto :goto_0

    .line 2361
    :cond_7
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/ah;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 2362
    goto :goto_0

    .line 2364
    :cond_8
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->d:Ljava/lang/Long;

    if-nez v2, :cond_9

    .line 2365
    iget-object v2, p1, Lcom/google/af/a/b/a/a/ah;->d:Ljava/lang/Long;

    if-eqz v2, :cond_a

    move v0, v1

    .line 2366
    goto :goto_0

    .line 2368
    :cond_9
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->d:Ljava/lang/Long;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/ah;->d:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 2369
    goto :goto_0

    .line 2371
    :cond_a
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->e:Ljava/lang/Long;

    if-nez v2, :cond_b

    .line 2372
    iget-object v2, p1, Lcom/google/af/a/b/a/a/ah;->e:Ljava/lang/Long;

    if-eqz v2, :cond_c

    move v0, v1

    .line 2373
    goto :goto_0

    .line 2375
    :cond_b
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->e:Ljava/lang/Long;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/ah;->e:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 2376
    goto :goto_0

    .line 2378
    :cond_c
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->f:Ljava/lang/Long;

    if-nez v2, :cond_d

    .line 2379
    iget-object v2, p1, Lcom/google/af/a/b/a/a/ah;->f:Ljava/lang/Long;

    if-eqz v2, :cond_e

    move v0, v1

    .line 2380
    goto :goto_0

    .line 2382
    :cond_d
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->f:Ljava/lang/Long;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/ah;->f:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 2383
    goto/16 :goto_0

    .line 2385
    :cond_e
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->g:Lcom/google/af/a/b/a/a/b;

    if-nez v2, :cond_f

    .line 2386
    iget-object v2, p1, Lcom/google/af/a/b/a/a/ah;->g:Lcom/google/af/a/b/a/a/b;

    if-eqz v2, :cond_10

    move v0, v1

    .line 2387
    goto/16 :goto_0

    .line 2390
    :cond_f
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->g:Lcom/google/af/a/b/a/a/b;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/ah;->g:Lcom/google/af/a/b/a/a/b;

    invoke-virtual {v2, v3}, Lcom/google/af/a/b/a/a/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    move v0, v1

    .line 2391
    goto/16 :goto_0

    .line 2394
    :cond_10
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->h:Ljava/lang/Integer;

    if-nez v2, :cond_11

    .line 2395
    iget-object v2, p1, Lcom/google/af/a/b/a/a/ah;->h:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    move v0, v1

    .line 2396
    goto/16 :goto_0

    .line 2398
    :cond_11
    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->h:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/ah;->h:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2399
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2406
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2409
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2411
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 2413
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->d:Ljava/lang/Long;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 2415
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->e:Ljava/lang/Long;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 2417
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->f:Ljava/lang/Long;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 2419
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->g:Lcom/google/af/a/b/a/a/b;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 2421
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/af/a/b/a/a/ah;->h:Ljava/lang/Integer;

    if-nez v2, :cond_7

    :goto_7
    add-int/2addr v0, v1

    .line 2423
    return v0

    .line 2406
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2409
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2411
    :cond_2
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 2413
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_3

    .line 2415
    :cond_4
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->e:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_4

    .line 2417
    :cond_5
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->f:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_5

    .line 2419
    :cond_6
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->g:Lcom/google/af/a/b/a/a/b;

    invoke-virtual {v0}, Lcom/google/af/a/b/a/a/b;->hashCode()I

    move-result v0

    goto :goto_6

    .line 2421
    :cond_7
    iget-object v1, p0, Lcom/google/af/a/b/a/a/ah;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_7
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 2276
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ah;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ah;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ah;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ah;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ah;->e:Ljava/lang/Long;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ah;->f:Ljava/lang/Long;

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->g:Lcom/google/af/a/b/a/a/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/af/a/b/a/a/b;

    invoke-direct {v0}, Lcom/google/af/a/b/a/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ah;->g:Lcom/google/af/a/b/a/a/b;

    :cond_1
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->g:Lcom/google/af/a/b/a/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/ah;->h:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x40 -> :sswitch_6
        0x4a -> :sswitch_7
        0x50 -> :sswitch_8
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 2429
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2430
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/af/a/b/a/a/ah;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2432
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2433
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/af/a/b/a/a/ah;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2435
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2436
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/af/a/b/a/a/ah;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2438
    :cond_2
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 2439
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/af/a/b/a/a/ah;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2441
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->e:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 2442
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/af/a/b/a/a/ah;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2444
    :cond_4
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->f:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 2445
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/af/a/b/a/a/ah;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2447
    :cond_5
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->g:Lcom/google/af/a/b/a/a/b;

    if-eqz v0, :cond_6

    .line 2448
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/af/a/b/a/a/ah;->g:Lcom/google/af/a/b/a/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2450
    :cond_6
    iget-object v0, p0, Lcom/google/af/a/b/a/a/ah;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 2451
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/af/a/b/a/a/ah;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2453
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2454
    return-void
.end method
