.class public final Lcom/google/af/a/b/a/a/g;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 771
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 772
    iput-object v0, p0, Lcom/google/af/a/b/a/a/g;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/g;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/g;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/g;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/g;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/af/a/b/a/a/g;->f:Ljava/lang/Integer;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/b/a/a/g;->cachedSize:I

    .line 773
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 880
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 881
    iget-object v1, p0, Lcom/google/af/a/b/a/a/g;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 882
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/af/a/b/a/a/g;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 885
    :cond_0
    iget-object v1, p0, Lcom/google/af/a/b/a/a/g;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 886
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/af/a/b/a/a/g;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 889
    :cond_1
    iget-object v1, p0, Lcom/google/af/a/b/a/a/g;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 890
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/af/a/b/a/a/g;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 893
    :cond_2
    iget-object v1, p0, Lcom/google/af/a/b/a/a/g;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 894
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/af/a/b/a/a/g;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 897
    :cond_3
    iget-object v1, p0, Lcom/google/af/a/b/a/a/g;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 898
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/af/a/b/a/a/g;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 901
    :cond_4
    iget-object v1, p0, Lcom/google/af/a/b/a/a/g;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 902
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/af/a/b/a/a/g;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 905
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 788
    if-ne p1, p0, :cond_1

    .line 835
    :cond_0
    :goto_0
    return v0

    .line 791
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/b/a/a/g;

    if-nez v2, :cond_2

    move v0, v1

    .line 792
    goto :goto_0

    .line 794
    :cond_2
    check-cast p1, Lcom/google/af/a/b/a/a/g;

    .line 795
    iget-object v2, p0, Lcom/google/af/a/b/a/a/g;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 796
    iget-object v2, p1, Lcom/google/af/a/b/a/a/g;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 797
    goto :goto_0

    .line 799
    :cond_3
    iget-object v2, p0, Lcom/google/af/a/b/a/a/g;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/g;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 800
    goto :goto_0

    .line 802
    :cond_4
    iget-object v2, p0, Lcom/google/af/a/b/a/a/g;->b:Ljava/lang/Integer;

    if-nez v2, :cond_5

    .line 803
    iget-object v2, p1, Lcom/google/af/a/b/a/a/g;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    move v0, v1

    .line 804
    goto :goto_0

    .line 806
    :cond_5
    iget-object v2, p0, Lcom/google/af/a/b/a/a/g;->b:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/g;->b:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 807
    goto :goto_0

    .line 808
    :cond_6
    iget-object v2, p0, Lcom/google/af/a/b/a/a/g;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 809
    iget-object v2, p1, Lcom/google/af/a/b/a/a/g;->c:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 810
    goto :goto_0

    .line 812
    :cond_7
    iget-object v2, p0, Lcom/google/af/a/b/a/a/g;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/g;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 813
    goto :goto_0

    .line 815
    :cond_8
    iget-object v2, p0, Lcom/google/af/a/b/a/a/g;->d:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 816
    iget-object v2, p1, Lcom/google/af/a/b/a/a/g;->d:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    .line 817
    goto :goto_0

    .line 819
    :cond_9
    iget-object v2, p0, Lcom/google/af/a/b/a/a/g;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/g;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 820
    goto :goto_0

    .line 822
    :cond_a
    iget-object v2, p0, Lcom/google/af/a/b/a/a/g;->e:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 823
    iget-object v2, p1, Lcom/google/af/a/b/a/a/g;->e:Ljava/lang/String;

    if-eqz v2, :cond_c

    move v0, v1

    .line 824
    goto :goto_0

    .line 826
    :cond_b
    iget-object v2, p0, Lcom/google/af/a/b/a/a/g;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/g;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 827
    goto :goto_0

    .line 829
    :cond_c
    iget-object v2, p0, Lcom/google/af/a/b/a/a/g;->f:Ljava/lang/Integer;

    if-nez v2, :cond_d

    .line 830
    iget-object v2, p1, Lcom/google/af/a/b/a/a/g;->f:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    move v0, v1

    .line 831
    goto :goto_0

    .line 833
    :cond_d
    iget-object v2, p0, Lcom/google/af/a/b/a/a/g;->f:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/g;->f:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 834
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 840
    iget-object v0, p0, Lcom/google/af/a/b/a/a/g;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 843
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/b/a/a/g;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 844
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/b/a/a/g;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 846
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/b/a/a/g;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 848
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/b/a/a/g;->e:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 850
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/af/a/b/a/a/g;->f:Ljava/lang/Integer;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 851
    return v0

    .line 840
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/b/a/a/g;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 843
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/b/a/a/g;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1

    .line 844
    :cond_2
    iget-object v0, p0, Lcom/google/af/a/b/a/a/g;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 846
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/b/a/a/g;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 848
    :cond_4
    iget-object v0, p0, Lcom/google/af/a/b/a/a/g;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 850
    :cond_5
    iget-object v1, p0, Lcom/google/af/a/b/a/a/g;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 723
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/g;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/g;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/g;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/g;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/g;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/g;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 857
    iget-object v0, p0, Lcom/google/af/a/b/a/a/g;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 858
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/af/a/b/a/a/g;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 860
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/b/a/a/g;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 861
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/af/a/b/a/a/g;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 863
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/b/a/a/g;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 864
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/af/a/b/a/a/g;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 866
    :cond_2
    iget-object v0, p0, Lcom/google/af/a/b/a/a/g;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 867
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/af/a/b/a/a/g;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 869
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/b/a/a/g;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 870
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/af/a/b/a/a/g;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 872
    :cond_4
    iget-object v0, p0, Lcom/google/af/a/b/a/a/g;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 873
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/af/a/b/a/a/g;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 875
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 876
    return-void
.end method
