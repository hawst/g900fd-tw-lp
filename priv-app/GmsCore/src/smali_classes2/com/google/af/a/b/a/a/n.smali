.class public final Lcom/google/af/a/b/a/a/n;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/af/a/b/a/a/o;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1738
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1739
    invoke-static {}, Lcom/google/af/a/b/a/a/o;->a()[Lcom/google/af/a/b/a/a/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/b/a/a/n;->a:[Lcom/google/af/a/b/a/a/o;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/b/a/a/n;->cachedSize:I

    .line 1740
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 1788
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v1

    .line 1789
    iget-object v0, p0, Lcom/google/af/a/b/a/a/n;->a:[Lcom/google/af/a/b/a/a/o;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/af/a/b/a/a/n;->a:[Lcom/google/af/a/b/a/a/o;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 1790
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/af/a/b/a/a/n;->a:[Lcom/google/af/a/b/a/a/o;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1791
    iget-object v2, p0, Lcom/google/af/a/b/a/a/n;->a:[Lcom/google/af/a/b/a/a/o;

    aget-object v2, v2, v0

    .line 1792
    if-eqz v2, :cond_0

    .line 1793
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1790
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1798
    :cond_1
    return v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1750
    if-ne p1, p0, :cond_1

    .line 1761
    :cond_0
    :goto_0
    return v0

    .line 1753
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/b/a/a/n;

    if-nez v2, :cond_2

    move v0, v1

    .line 1754
    goto :goto_0

    .line 1756
    :cond_2
    check-cast p1, Lcom/google/af/a/b/a/a/n;

    .line 1757
    iget-object v2, p0, Lcom/google/af/a/b/a/a/n;->a:[Lcom/google/af/a/b/a/a/o;

    iget-object v3, p1, Lcom/google/af/a/b/a/a/n;->a:[Lcom/google/af/a/b/a/a/o;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1759
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1766
    iget-object v0, p0, Lcom/google/af/a/b/a/a/n;->a:[Lcom/google/af/a/b/a/a/o;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 1769
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1718
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/af/a/b/a/a/n;->a:[Lcom/google/af/a/b/a/a/o;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/af/a/b/a/a/o;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/af/a/b/a/a/n;->a:[Lcom/google/af/a/b/a/a/o;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/af/a/b/a/a/o;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/o;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/af/a/b/a/a/n;->a:[Lcom/google/af/a/b/a/a/o;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/af/a/b/a/a/o;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/o;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/af/a/b/a/a/n;->a:[Lcom/google/af/a/b/a/a/o;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 1775
    iget-object v0, p0, Lcom/google/af/a/b/a/a/n;->a:[Lcom/google/af/a/b/a/a/o;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/af/a/b/a/a/n;->a:[Lcom/google/af/a/b/a/a/o;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 1776
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/af/a/b/a/a/n;->a:[Lcom/google/af/a/b/a/a/o;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1777
    iget-object v1, p0, Lcom/google/af/a/b/a/a/n;->a:[Lcom/google/af/a/b/a/a/o;

    aget-object v1, v1, v0

    .line 1778
    if-eqz v1, :cond_0

    .line 1779
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1776
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1783
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1784
    return-void
.end method
