.class public final Lcom/google/af/a/c/a/i;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:[B

.field public d:Z

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Z

.field public i:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 64
    iput v1, p0, Lcom/google/af/a/c/a/i;->a:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/c/a/i;->b:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/af/a/c/a/i;->c:[B

    iput-boolean v1, p0, Lcom/google/af/a/c/a/i;->d:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/c/a/i;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/c/a/i;->f:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/c/a/i;->g:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/af/a/c/a/i;->h:Z

    iput-boolean v1, p0, Lcom/google/af/a/c/a/i;->i:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/c/a/i;->cachedSize:I

    .line 65
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/nano/a;)Lcom/google/af/a/c/a/i;
    .locals 1

    .prologue
    .line 235
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    .line 236
    sparse-switch v0, :sswitch_data_0

    .line 240
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 241
    :sswitch_0
    return-object p0

    .line 246
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    .line 247
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 257
    :pswitch_0
    iput v0, p0, Lcom/google/af/a/c/a/i;->a:I

    goto :goto_0

    .line 263
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/c/a/i;->b:Ljava/lang/String;

    goto :goto_0

    .line 267
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/c/a/i;->c:[B

    goto :goto_0

    .line 271
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/af/a/c/a/i;->d:Z

    goto :goto_0

    .line 275
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/c/a/i;->e:Ljava/lang/String;

    goto :goto_0

    .line 279
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/c/a/i;->f:Ljava/lang/String;

    goto :goto_0

    .line 283
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/af/a/c/a/i;->h:Z

    goto :goto_0

    .line 287
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/af/a/c/a/i;->i:Z

    goto :goto_0

    .line 291
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/c/a/i;->g:Ljava/lang/String;

    goto :goto_0

    .line 236
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch

    .line 247
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 190
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 191
    iget v1, p0, Lcom/google/af/a/c/a/i;->a:I

    if-eqz v1, :cond_0

    .line 192
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/af/a/c/a/i;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 195
    :cond_0
    iget-object v1, p0, Lcom/google/af/a/c/a/i;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 196
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/af/a/c/a/i;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 199
    :cond_1
    iget-object v1, p0, Lcom/google/af/a/c/a/i;->c:[B

    sget-object v2, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_2

    .line 200
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/af/a/c/a/i;->c:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 203
    :cond_2
    iget-boolean v1, p0, Lcom/google/af/a/c/a/i;->d:Z

    if-eqz v1, :cond_3

    .line 204
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/af/a/c/a/i;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 207
    :cond_3
    iget-object v1, p0, Lcom/google/af/a/c/a/i;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 208
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/af/a/c/a/i;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 211
    :cond_4
    iget-object v1, p0, Lcom/google/af/a/c/a/i;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 212
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/af/a/c/a/i;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 215
    :cond_5
    iget-boolean v1, p0, Lcom/google/af/a/c/a/i;->h:Z

    if-eqz v1, :cond_6

    .line 216
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/af/a/c/a/i;->h:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 219
    :cond_6
    iget-boolean v1, p0, Lcom/google/af/a/c/a/i;->i:Z

    if-eqz v1, :cond_7

    .line 220
    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/af/a/c/a/i;->i:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 223
    :cond_7
    iget-object v1, p0, Lcom/google/af/a/c/a/i;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 224
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/af/a/c/a/i;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 227
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 83
    if-ne p1, p0, :cond_1

    .line 133
    :cond_0
    :goto_0
    return v0

    .line 86
    :cond_1
    instance-of v2, p1, Lcom/google/af/a/c/a/i;

    if-nez v2, :cond_2

    move v0, v1

    .line 87
    goto :goto_0

    .line 89
    :cond_2
    check-cast p1, Lcom/google/af/a/c/a/i;

    .line 90
    iget v2, p0, Lcom/google/af/a/c/a/i;->a:I

    iget v3, p1, Lcom/google/af/a/c/a/i;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 91
    goto :goto_0

    .line 93
    :cond_3
    iget-object v2, p0, Lcom/google/af/a/c/a/i;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 94
    iget-object v2, p1, Lcom/google/af/a/c/a/i;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 95
    goto :goto_0

    .line 97
    :cond_4
    iget-object v2, p0, Lcom/google/af/a/c/a/i;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/c/a/i;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 98
    goto :goto_0

    .line 100
    :cond_5
    iget-object v2, p0, Lcom/google/af/a/c/a/i;->c:[B

    iget-object v3, p1, Lcom/google/af/a/c/a/i;->c:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 101
    goto :goto_0

    .line 103
    :cond_6
    iget-boolean v2, p0, Lcom/google/af/a/c/a/i;->d:Z

    iget-boolean v3, p1, Lcom/google/af/a/c/a/i;->d:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 104
    goto :goto_0

    .line 106
    :cond_7
    iget-object v2, p0, Lcom/google/af/a/c/a/i;->e:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 107
    iget-object v2, p1, Lcom/google/af/a/c/a/i;->e:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    .line 108
    goto :goto_0

    .line 110
    :cond_8
    iget-object v2, p0, Lcom/google/af/a/c/a/i;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/c/a/i;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 111
    goto :goto_0

    .line 113
    :cond_9
    iget-object v2, p0, Lcom/google/af/a/c/a/i;->f:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 114
    iget-object v2, p1, Lcom/google/af/a/c/a/i;->f:Ljava/lang/String;

    if-eqz v2, :cond_b

    move v0, v1

    .line 115
    goto :goto_0

    .line 117
    :cond_a
    iget-object v2, p0, Lcom/google/af/a/c/a/i;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/c/a/i;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 118
    goto :goto_0

    .line 120
    :cond_b
    iget-object v2, p0, Lcom/google/af/a/c/a/i;->g:Ljava/lang/String;

    if-nez v2, :cond_c

    .line 121
    iget-object v2, p1, Lcom/google/af/a/c/a/i;->g:Ljava/lang/String;

    if-eqz v2, :cond_d

    move v0, v1

    .line 122
    goto :goto_0

    .line 124
    :cond_c
    iget-object v2, p0, Lcom/google/af/a/c/a/i;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/af/a/c/a/i;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 125
    goto :goto_0

    .line 127
    :cond_d
    iget-boolean v2, p0, Lcom/google/af/a/c/a/i;->h:Z

    iget-boolean v3, p1, Lcom/google/af/a/c/a/i;->h:Z

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 128
    goto/16 :goto_0

    .line 130
    :cond_e
    iget-boolean v2, p0, Lcom/google/af/a/c/a/i;->i:Z

    iget-boolean v3, p1, Lcom/google/af/a/c/a/i;->i:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 131
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    .line 138
    iget v0, p0, Lcom/google/af/a/c/a/i;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 140
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/c/a/i;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    .line 142
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/af/a/c/a/i;->c:[B

    invoke-static {v4}, Ljava/util/Arrays;->hashCode([B)I

    move-result v4

    add-int/2addr v0, v4

    .line 143
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/af/a/c/a/i;->d:Z

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    add-int/2addr v0, v4

    .line 144
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/c/a/i;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    .line 146
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/c/a/i;->f:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v4

    .line 148
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/af/a/c/a/i;->g:Ljava/lang/String;

    if-nez v4, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 150
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/af/a/c/a/i;->h:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_5
    add-int/2addr v0, v1

    .line 151
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/af/a/c/a/i;->i:Z

    if-eqz v1, :cond_6

    :goto_6
    add-int/2addr v0, v2

    .line 152
    return v0

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/c/a/i;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v3

    .line 143
    goto :goto_1

    .line 144
    :cond_2
    iget-object v0, p0, Lcom/google/af/a/c/a/i;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 146
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/c/a/i;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 148
    :cond_4
    iget-object v1, p0, Lcom/google/af/a/c/a/i;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_5
    move v0, v3

    .line 150
    goto :goto_5

    :cond_6
    move v2, v3

    .line 151
    goto :goto_6
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/af/a/c/a/i;->a(Lcom/google/protobuf/nano/a;)Lcom/google/af/a/c/a/i;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 158
    iget v0, p0, Lcom/google/af/a/c/a/i;->a:I

    if-eqz v0, :cond_0

    .line 159
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/af/a/c/a/i;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/c/a/i;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 162
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/af/a/c/a/i;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 164
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/c/a/i;->c:[B

    sget-object v1, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_2

    .line 165
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/af/a/c/a/i;->c:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 167
    :cond_2
    iget-boolean v0, p0, Lcom/google/af/a/c/a/i;->d:Z

    if-eqz v0, :cond_3

    .line 168
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/af/a/c/a/i;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 170
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/c/a/i;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 171
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/af/a/c/a/i;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 173
    :cond_4
    iget-object v0, p0, Lcom/google/af/a/c/a/i;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 174
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/af/a/c/a/i;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 176
    :cond_5
    iget-boolean v0, p0, Lcom/google/af/a/c/a/i;->h:Z

    if-eqz v0, :cond_6

    .line 177
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/af/a/c/a/i;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 179
    :cond_6
    iget-boolean v0, p0, Lcom/google/af/a/c/a/i;->i:Z

    if-eqz v0, :cond_7

    .line 180
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/af/a/c/a/i;->i:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 182
    :cond_7
    iget-object v0, p0, Lcom/google/af/a/c/a/i;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 183
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/af/a/c/a/i;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 185
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 186
    return-void
.end method
