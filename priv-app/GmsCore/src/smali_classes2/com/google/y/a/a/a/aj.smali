.class public final Lcom/google/y/a/a/a/aj;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Integer;

.field public c:Lcom/google/y/a/a/a/aa;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Integer;

.field public h:Lcom/google/y/a/a/a/ab;

.field public i:Lcom/google/y/a/a/a/ab;

.field public j:[Lcom/google/y/a/a/a/ak;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 60
    iput-object v1, p0, Lcom/google/y/a/a/a/aj;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/y/a/a/a/aj;->b:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/y/a/a/a/aj;->c:Lcom/google/y/a/a/a/aa;

    iput-object v1, p0, Lcom/google/y/a/a/a/aj;->d:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/y/a/a/a/aj;->e:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/y/a/a/a/aj;->f:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/y/a/a/a/aj;->g:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/y/a/a/a/aj;->h:Lcom/google/y/a/a/a/ab;

    iput-object v1, p0, Lcom/google/y/a/a/a/aj;->i:Lcom/google/y/a/a/a/ab;

    invoke-static {}, Lcom/google/y/a/a/a/ak;->a()[Lcom/google/y/a/a/a/ak;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/aj;->j:[Lcom/google/y/a/a/a/ak;

    iput-object v1, p0, Lcom/google/y/a/a/a/aj;->k:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/y/a/a/a/aj;->l:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/y/a/a/a/aj;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/y/a/a/a/aj;->cachedSize:I

    .line 61
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 258
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 259
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 260
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/y/a/a/a/aj;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 263
    :cond_0
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 264
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/y/a/a/a/aj;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 267
    :cond_1
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->c:Lcom/google/y/a/a/a/aa;

    if-eqz v1, :cond_2

    .line 268
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/y/a/a/a/aj;->c:Lcom/google/y/a/a/a/aa;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 271
    :cond_2
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 272
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/y/a/a/a/aj;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 275
    :cond_3
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 276
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/y/a/a/a/aj;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 279
    :cond_4
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 280
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/y/a/a/a/aj;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 283
    :cond_5
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 284
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/y/a/a/a/aj;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 287
    :cond_6
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->h:Lcom/google/y/a/a/a/ab;

    if-eqz v1, :cond_7

    .line 288
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/y/a/a/a/aj;->h:Lcom/google/y/a/a/a/ab;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 291
    :cond_7
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->i:Lcom/google/y/a/a/a/ab;

    if-eqz v1, :cond_8

    .line 292
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/y/a/a/a/aj;->i:Lcom/google/y/a/a/a/ab;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 295
    :cond_8
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->j:[Lcom/google/y/a/a/a/ak;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->j:[Lcom/google/y/a/a/a/ak;

    array-length v1, v1

    if-lez v1, :cond_b

    .line 296
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/y/a/a/a/aj;->j:[Lcom/google/y/a/a/a/ak;

    array-length v2, v2

    if-ge v0, v2, :cond_a

    .line 297
    iget-object v2, p0, Lcom/google/y/a/a/a/aj;->j:[Lcom/google/y/a/a/a/ak;

    aget-object v2, v2, v0

    .line 298
    if-eqz v2, :cond_9

    .line 299
    const/16 v3, 0xa

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 296
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_a
    move v0, v1

    .line 304
    :cond_b
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->k:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 305
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/y/a/a/a/aj;->k:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 308
    :cond_c
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->l:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 309
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/y/a/a/a/aj;->l:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 312
    :cond_d
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 83
    if-ne p1, p0, :cond_1

    .line 84
    const/4 v0, 0x1

    .line 176
    :cond_0
    :goto_0
    return v0

    .line 86
    :cond_1
    instance-of v1, p1, Lcom/google/y/a/a/a/aj;

    if-eqz v1, :cond_0

    .line 89
    check-cast p1, Lcom/google/y/a/a/a/aj;

    .line 90
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->a:Ljava/lang/String;

    if-nez v1, :cond_d

    .line 91
    iget-object v1, p1, Lcom/google/y/a/a/a/aj;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 97
    :cond_2
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->b:Ljava/lang/Integer;

    if-nez v1, :cond_e

    .line 98
    iget-object v1, p1, Lcom/google/y/a/a/a/aj;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 103
    :cond_3
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->c:Lcom/google/y/a/a/a/aa;

    if-nez v1, :cond_f

    .line 104
    iget-object v1, p1, Lcom/google/y/a/a/a/aj;->c:Lcom/google/y/a/a/a/aa;

    if-nez v1, :cond_0

    .line 112
    :cond_4
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->d:Ljava/lang/Integer;

    if-nez v1, :cond_10

    .line 113
    iget-object v1, p1, Lcom/google/y/a/a/a/aj;->d:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 119
    :cond_5
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->e:Ljava/lang/Integer;

    if-nez v1, :cond_11

    .line 120
    iget-object v1, p1, Lcom/google/y/a/a/a/aj;->e:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 126
    :cond_6
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->f:Ljava/lang/Integer;

    if-nez v1, :cond_12

    .line 127
    iget-object v1, p1, Lcom/google/y/a/a/a/aj;->f:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 133
    :cond_7
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->g:Ljava/lang/Integer;

    if-nez v1, :cond_13

    .line 134
    iget-object v1, p1, Lcom/google/y/a/a/a/aj;->g:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 140
    :cond_8
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->h:Lcom/google/y/a/a/a/ab;

    if-nez v1, :cond_14

    .line 141
    iget-object v1, p1, Lcom/google/y/a/a/a/aj;->h:Lcom/google/y/a/a/a/ab;

    if-nez v1, :cond_0

    .line 149
    :cond_9
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->i:Lcom/google/y/a/a/a/ab;

    if-nez v1, :cond_15

    .line 150
    iget-object v1, p1, Lcom/google/y/a/a/a/aj;->i:Lcom/google/y/a/a/a/ab;

    if-nez v1, :cond_0

    .line 158
    :cond_a
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->j:[Lcom/google/y/a/a/a/ak;

    iget-object v2, p1, Lcom/google/y/a/a/a/aj;->j:[Lcom/google/y/a/a/a/ak;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 162
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->k:Ljava/lang/String;

    if-nez v1, :cond_16

    .line 163
    iget-object v1, p1, Lcom/google/y/a/a/a/aj;->k:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 169
    :cond_b
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->l:Ljava/lang/String;

    if-nez v1, :cond_17

    .line 170
    iget-object v1, p1, Lcom/google/y/a/a/a/aj;->l:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 176
    :cond_c
    invoke-virtual {p0, p1}, Lcom/google/y/a/a/a/aj;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 94
    :cond_d
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/y/a/a/a/aj;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 101
    :cond_e
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/y/a/a/a/aj;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto/16 :goto_0

    .line 108
    :cond_f
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->c:Lcom/google/y/a/a/a/aa;

    iget-object v2, p1, Lcom/google/y/a/a/a/aj;->c:Lcom/google/y/a/a/a/aa;

    invoke-virtual {v1, v2}, Lcom/google/y/a/a/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto/16 :goto_0

    .line 116
    :cond_10
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->d:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/y/a/a/a/aj;->d:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0

    .line 123
    :cond_11
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->e:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/y/a/a/a/aj;->e:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 130
    :cond_12
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->f:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/y/a/a/a/aj;->f:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 137
    :cond_13
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->g:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/y/a/a/a/aj;->g:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 145
    :cond_14
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->h:Lcom/google/y/a/a/a/ab;

    iget-object v2, p1, Lcom/google/y/a/a/a/aj;->h:Lcom/google/y/a/a/a/ab;

    invoke-virtual {v1, v2}, Lcom/google/y/a/a/a/ab;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    .line 154
    :cond_15
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->i:Lcom/google/y/a/a/a/ab;

    iget-object v2, p1, Lcom/google/y/a/a/a/aj;->i:Lcom/google/y/a/a/a/ab;

    invoke-virtual {v1, v2}, Lcom/google/y/a/a/a/ab;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0

    .line 166
    :cond_16
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->k:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/y/a/a/a/aj;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    goto/16 :goto_0

    .line 173
    :cond_17
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->l:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/y/a/a/a/aj;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 181
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 184
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 185
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->c:Lcom/google/y/a/a/a/aa;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 187
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->d:Ljava/lang/Integer;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 189
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->e:Ljava/lang/Integer;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 191
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->f:Ljava/lang/Integer;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 193
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->g:Ljava/lang/Integer;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 195
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->h:Lcom/google/y/a/a/a/ab;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 197
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->i:Lcom/google/y/a/a/a/ab;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 199
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/y/a/a/a/aj;->j:[Lcom/google/y/a/a/a/ak;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 201
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->k:Ljava/lang/String;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    .line 203
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/y/a/a/a/aj;->l:Ljava/lang/String;

    if-nez v2, :cond_a

    :goto_a
    add-int/2addr v0, v1

    .line 205
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/y/a/a/a/aj;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 206
    return v0

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 184
    :cond_1
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1

    .line 185
    :cond_2
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->c:Lcom/google/y/a/a/a/aa;

    invoke-virtual {v0}, Lcom/google/y/a/a/a/aa;->hashCode()I

    move-result v0

    goto :goto_2

    .line 187
    :cond_3
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_3

    .line 189
    :cond_4
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_4

    .line 191
    :cond_5
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_5

    .line 193
    :cond_6
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->g:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_6

    .line 195
    :cond_7
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->h:Lcom/google/y/a/a/a/ab;

    invoke-virtual {v0}, Lcom/google/y/a/a/a/ab;->hashCode()I

    move-result v0

    goto :goto_7

    .line 197
    :cond_8
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->i:Lcom/google/y/a/a/a/ab;

    invoke-virtual {v0}, Lcom/google/y/a/a/a/ab;->hashCode()I

    move-result v0

    goto :goto_8

    .line 201
    :cond_9
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_9

    .line 203
    :cond_a
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->l:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_a
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/y/a/a/a/aj;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/aj;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/aj;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->c:Lcom/google/y/a/a/a/aa;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/y/a/a/a/aa;

    invoke-direct {v0}, Lcom/google/y/a/a/a/aa;-><init>()V

    iput-object v0, p0, Lcom/google/y/a/a/a/aj;->c:Lcom/google/y/a/a/a/aa;

    :cond_1
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->c:Lcom/google/y/a/a/a/aa;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/aj;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/aj;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/aj;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/aj;->g:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->h:Lcom/google/y/a/a/a/ab;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/y/a/a/a/ab;

    invoke-direct {v0}, Lcom/google/y/a/a/a/ab;-><init>()V

    iput-object v0, p0, Lcom/google/y/a/a/a/aj;->h:Lcom/google/y/a/a/a/ab;

    :cond_2
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->h:Lcom/google/y/a/a/a/ab;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->i:Lcom/google/y/a/a/a/ab;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/y/a/a/a/ab;

    invoke-direct {v0}, Lcom/google/y/a/a/a/ab;-><init>()V

    iput-object v0, p0, Lcom/google/y/a/a/a/aj;->i:Lcom/google/y/a/a/a/ab;

    :cond_3
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->i:Lcom/google/y/a/a/a/ab;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_b
    const/16 v0, 0x52

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->j:[Lcom/google/y/a/a/a/ak;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/y/a/a/a/ak;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/y/a/a/a/aj;->j:[Lcom/google/y/a/a/a/ak;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/y/a/a/a/ak;

    invoke-direct {v3}, Lcom/google/y/a/a/a/ak;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->j:[Lcom/google/y/a/a/a/ak;

    array-length v0, v0

    goto :goto_1

    :cond_6
    new-instance v3, Lcom/google/y/a/a/a/ak;

    invoke-direct {v3}, Lcom/google/y/a/a/a/ak;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/y/a/a/a/aj;->j:[Lcom/google/y/a/a/a/ak;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/aj;->k:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/aj;->l:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_4
        0x20 -> :sswitch_5
        0x28 -> :sswitch_6
        0x30 -> :sswitch_7
        0x38 -> :sswitch_8
        0x42 -> :sswitch_9
        0x4a -> :sswitch_a
        0x52 -> :sswitch_b
        0x5a -> :sswitch_c
        0x62 -> :sswitch_d
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_3
        0xa -> :sswitch_3
        0x2f -> :sswitch_3
        0x55 -> :sswitch_3
        0x57 -> :sswitch_3
        0x58 -> :sswitch_3
        0x5e -> :sswitch_3
        0x75 -> :sswitch_3
        0x8a -> :sswitch_3
        0x8b -> :sswitch_3
        0x91 -> :sswitch_3
        0x94 -> :sswitch_3
        0xa2 -> :sswitch_3
        0x102 -> :sswitch_3
        0x106 -> :sswitch_3
        0x123 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 213
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 216
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 218
    :cond_1
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->c:Lcom/google/y/a/a/a/aa;

    if-eqz v0, :cond_2

    .line 219
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->c:Lcom/google/y/a/a/a/aa;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 221
    :cond_2
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 222
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 224
    :cond_3
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 225
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 227
    :cond_4
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 228
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 230
    :cond_5
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 231
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 233
    :cond_6
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->h:Lcom/google/y/a/a/a/ab;

    if-eqz v0, :cond_7

    .line 234
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->h:Lcom/google/y/a/a/a/ab;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 236
    :cond_7
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->i:Lcom/google/y/a/a/a/ab;

    if-eqz v0, :cond_8

    .line 237
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->i:Lcom/google/y/a/a/a/ab;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 239
    :cond_8
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->j:[Lcom/google/y/a/a/a/ak;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->j:[Lcom/google/y/a/a/a/ak;

    array-length v0, v0

    if-lez v0, :cond_a

    .line 240
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->j:[Lcom/google/y/a/a/a/ak;

    array-length v1, v1

    if-ge v0, v1, :cond_a

    .line 241
    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->j:[Lcom/google/y/a/a/a/ak;

    aget-object v1, v1, v0

    .line 242
    if-eqz v1, :cond_9

    .line 243
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 240
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 247
    :cond_a
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->k:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 248
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 250
    :cond_b
    iget-object v0, p0, Lcom/google/y/a/a/a/aj;->l:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 251
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/y/a/a/a/aj;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 253
    :cond_c
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 254
    return-void
.end method
