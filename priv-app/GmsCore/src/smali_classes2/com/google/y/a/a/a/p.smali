.class public final Lcom/google/y/a/a/a/p;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:[Lcom/google/y/a/a/a/q;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 33
    iput-object v1, p0, Lcom/google/y/a/a/a/p;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/y/a/a/a/p;->b:Ljava/lang/String;

    invoke-static {}, Lcom/google/y/a/a/a/q;->a()[Lcom/google/y/a/a/a/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/p;->c:[Lcom/google/y/a/a/a/q;

    iput-object v1, p0, Lcom/google/y/a/a/a/p;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/y/a/a/a/p;->cachedSize:I

    .line 34
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 110
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 111
    iget-object v1, p0, Lcom/google/y/a/a/a/p;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 112
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/y/a/a/a/p;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 115
    :cond_0
    iget-object v1, p0, Lcom/google/y/a/a/a/p;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 116
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/y/a/a/a/p;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 119
    :cond_1
    iget-object v1, p0, Lcom/google/y/a/a/a/p;->c:[Lcom/google/y/a/a/a/q;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/y/a/a/a/p;->c:[Lcom/google/y/a/a/a/q;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 120
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/y/a/a/a/p;->c:[Lcom/google/y/a/a/a/q;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 121
    iget-object v2, p0, Lcom/google/y/a/a/a/p;->c:[Lcom/google/y/a/a/a/q;

    aget-object v2, v2, v0

    .line 122
    if-eqz v2, :cond_2

    .line 123
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 120
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 128
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 47
    if-ne p1, p0, :cond_1

    .line 48
    const/4 v0, 0x1

    .line 72
    :cond_0
    :goto_0
    return v0

    .line 50
    :cond_1
    instance-of v1, p1, Lcom/google/y/a/a/a/p;

    if-eqz v1, :cond_0

    .line 53
    check-cast p1, Lcom/google/y/a/a/a/p;

    .line 54
    iget-object v1, p0, Lcom/google/y/a/a/a/p;->a:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 55
    iget-object v1, p1, Lcom/google/y/a/a/a/p;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 61
    :cond_2
    iget-object v1, p0, Lcom/google/y/a/a/a/p;->b:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 62
    iget-object v1, p1, Lcom/google/y/a/a/a/p;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 68
    :cond_3
    iget-object v1, p0, Lcom/google/y/a/a/a/p;->c:[Lcom/google/y/a/a/a/q;

    iget-object v2, p1, Lcom/google/y/a/a/a/p;->c:[Lcom/google/y/a/a/a/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 72
    invoke-virtual {p0, p1}, Lcom/google/y/a/a/a/p;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 58
    :cond_4
    iget-object v1, p0, Lcom/google/y/a/a/a/p;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/y/a/a/a/p;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 65
    :cond_5
    iget-object v1, p0, Lcom/google/y/a/a/a/p;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/y/a/a/a/p;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 77
    iget-object v0, p0, Lcom/google/y/a/a/a/p;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 80
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/y/a/a/a/p;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 82
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/y/a/a/a/p;->c:[Lcom/google/y/a/a/a/q;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/y/a/a/a/p;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 85
    return v0

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/google/y/a/a/a/p;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 80
    :cond_1
    iget-object v1, p0, Lcom/google/y/a/a/a/p;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/y/a/a/a/p;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/p;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/p;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/y/a/a/a/p;->c:[Lcom/google/y/a/a/a/q;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/y/a/a/a/q;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/y/a/a/a/p;->c:[Lcom/google/y/a/a/a/q;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/y/a/a/a/q;

    invoke-direct {v3}, Lcom/google/y/a/a/a/q;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/y/a/a/a/p;->c:[Lcom/google/y/a/a/a/q;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/y/a/a/a/q;

    invoke-direct {v3}, Lcom/google/y/a/a/a/q;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/y/a/a/a/p;->c:[Lcom/google/y/a/a/a/q;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/y/a/a/a/p;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 92
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/y/a/a/a/p;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/google/y/a/a/a/p;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 95
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/y/a/a/a/p;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 97
    :cond_1
    iget-object v0, p0, Lcom/google/y/a/a/a/p;->c:[Lcom/google/y/a/a/a/q;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/y/a/a/a/p;->c:[Lcom/google/y/a/a/a/q;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 98
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/y/a/a/a/p;->c:[Lcom/google/y/a/a/a/q;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 99
    iget-object v1, p0, Lcom/google/y/a/a/a/p;->c:[Lcom/google/y/a/a/a/q;

    aget-object v1, v1, v0

    .line 100
    if-eqz v1, :cond_2

    .line 101
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 98
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 106
    return-void
.end method
