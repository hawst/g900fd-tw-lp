.class public final Lcom/google/y/a/a/a/a;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile j:[Lcom/google/y/a/a/a/a;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/String;

.field public c:Lcom/google/y/a/a/a/f;

.field public d:Lcom/google/y/a/a/a/i;

.field public e:Lcom/google/y/a/a/a/y;

.field public f:Lcom/google/y/a/a/a/ah;

.field public g:Lcom/google/y/a/a/a/x;

.field public h:Lcom/google/y/a/a/a/o;

.field public i:Lcom/google/y/a/a/a/ai;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 51
    iput-object v0, p0, Lcom/google/y/a/a/a/a;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/y/a/a/a/a;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/y/a/a/a/a;->c:Lcom/google/y/a/a/a/f;

    iput-object v0, p0, Lcom/google/y/a/a/a/a;->d:Lcom/google/y/a/a/a/i;

    iput-object v0, p0, Lcom/google/y/a/a/a/a;->e:Lcom/google/y/a/a/a/y;

    iput-object v0, p0, Lcom/google/y/a/a/a/a;->f:Lcom/google/y/a/a/a/ah;

    iput-object v0, p0, Lcom/google/y/a/a/a/a;->g:Lcom/google/y/a/a/a/x;

    iput-object v0, p0, Lcom/google/y/a/a/a/a;->h:Lcom/google/y/a/a/a/o;

    iput-object v0, p0, Lcom/google/y/a/a/a/a;->i:Lcom/google/y/a/a/a/ai;

    iput-object v0, p0, Lcom/google/y/a/a/a/a;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/y/a/a/a/a;->cachedSize:I

    .line 52
    return-void
.end method

.method public static a()[Lcom/google/y/a/a/a/a;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/y/a/a/a/a;->j:[Lcom/google/y/a/a/a/a;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/y/a/a/a/a;->j:[Lcom/google/y/a/a/a/a;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/y/a/a/a/a;

    sput-object v0, Lcom/google/y/a/a/a/a;->j:[Lcom/google/y/a/a/a/a;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/y/a/a/a/a;->j:[Lcom/google/y/a/a/a/a;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 216
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 217
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 218
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/y/a/a/a/a;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 221
    :cond_0
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 222
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/y/a/a/a/a;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 225
    :cond_1
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->c:Lcom/google/y/a/a/a/f;

    if-eqz v1, :cond_2

    .line 226
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/y/a/a/a/a;->c:Lcom/google/y/a/a/a/f;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 229
    :cond_2
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->d:Lcom/google/y/a/a/a/i;

    if-eqz v1, :cond_3

    .line 230
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/y/a/a/a/a;->d:Lcom/google/y/a/a/a/i;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 233
    :cond_3
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->e:Lcom/google/y/a/a/a/y;

    if-eqz v1, :cond_4

    .line 234
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/y/a/a/a/a;->e:Lcom/google/y/a/a/a/y;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 237
    :cond_4
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->f:Lcom/google/y/a/a/a/ah;

    if-eqz v1, :cond_5

    .line 238
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/y/a/a/a/a;->f:Lcom/google/y/a/a/a/ah;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 241
    :cond_5
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->g:Lcom/google/y/a/a/a/x;

    if-eqz v1, :cond_6

    .line 242
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/y/a/a/a/a;->g:Lcom/google/y/a/a/a/x;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 245
    :cond_6
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->h:Lcom/google/y/a/a/a/o;

    if-eqz v1, :cond_7

    .line 246
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/y/a/a/a/a;->h:Lcom/google/y/a/a/a/o;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 249
    :cond_7
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->i:Lcom/google/y/a/a/a/ai;

    if-eqz v1, :cond_8

    .line 250
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/y/a/a/a/a;->i:Lcom/google/y/a/a/a/ai;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 253
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 71
    if-ne p1, p0, :cond_1

    .line 72
    const/4 v0, 0x1

    .line 154
    :cond_0
    :goto_0
    return v0

    .line 74
    :cond_1
    instance-of v1, p1, Lcom/google/y/a/a/a/a;

    if-eqz v1, :cond_0

    .line 77
    check-cast p1, Lcom/google/y/a/a/a/a;

    .line 78
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->a:Ljava/lang/Integer;

    if-nez v1, :cond_b

    .line 79
    iget-object v1, p1, Lcom/google/y/a/a/a/a;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 84
    :cond_2
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->b:Ljava/lang/String;

    if-nez v1, :cond_c

    .line 85
    iget-object v1, p1, Lcom/google/y/a/a/a/a;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 91
    :cond_3
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->c:Lcom/google/y/a/a/a/f;

    if-nez v1, :cond_d

    .line 92
    iget-object v1, p1, Lcom/google/y/a/a/a/a;->c:Lcom/google/y/a/a/a/f;

    if-nez v1, :cond_0

    .line 100
    :cond_4
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->d:Lcom/google/y/a/a/a/i;

    if-nez v1, :cond_e

    .line 101
    iget-object v1, p1, Lcom/google/y/a/a/a/a;->d:Lcom/google/y/a/a/a/i;

    if-nez v1, :cond_0

    .line 109
    :cond_5
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->e:Lcom/google/y/a/a/a/y;

    if-nez v1, :cond_f

    .line 110
    iget-object v1, p1, Lcom/google/y/a/a/a/a;->e:Lcom/google/y/a/a/a/y;

    if-nez v1, :cond_0

    .line 118
    :cond_6
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->f:Lcom/google/y/a/a/a/ah;

    if-nez v1, :cond_10

    .line 119
    iget-object v1, p1, Lcom/google/y/a/a/a/a;->f:Lcom/google/y/a/a/a/ah;

    if-nez v1, :cond_0

    .line 127
    :cond_7
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->g:Lcom/google/y/a/a/a/x;

    if-nez v1, :cond_11

    .line 128
    iget-object v1, p1, Lcom/google/y/a/a/a/a;->g:Lcom/google/y/a/a/a/x;

    if-nez v1, :cond_0

    .line 136
    :cond_8
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->h:Lcom/google/y/a/a/a/o;

    if-nez v1, :cond_12

    .line 137
    iget-object v1, p1, Lcom/google/y/a/a/a/a;->h:Lcom/google/y/a/a/a/o;

    if-nez v1, :cond_0

    .line 145
    :cond_9
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->i:Lcom/google/y/a/a/a/ai;

    if-nez v1, :cond_13

    .line 146
    iget-object v1, p1, Lcom/google/y/a/a/a/a;->i:Lcom/google/y/a/a/a/ai;

    if-nez v1, :cond_0

    .line 154
    :cond_a
    invoke-virtual {p0, p1}, Lcom/google/y/a/a/a/a;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 82
    :cond_b
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/y/a/a/a/a;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 88
    :cond_c
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/y/a/a/a/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 96
    :cond_d
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->c:Lcom/google/y/a/a/a/f;

    iget-object v2, p1, Lcom/google/y/a/a/a/a;->c:Lcom/google/y/a/a/a/f;

    invoke-virtual {v1, v2}, Lcom/google/y/a/a/a/f;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 105
    :cond_e
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->d:Lcom/google/y/a/a/a/i;

    iget-object v2, p1, Lcom/google/y/a/a/a/a;->d:Lcom/google/y/a/a/a/i;

    invoke-virtual {v1, v2}, Lcom/google/y/a/a/a/i;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 114
    :cond_f
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->e:Lcom/google/y/a/a/a/y;

    iget-object v2, p1, Lcom/google/y/a/a/a/a;->e:Lcom/google/y/a/a/a/y;

    invoke-virtual {v1, v2}, Lcom/google/y/a/a/a/y;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 123
    :cond_10
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->f:Lcom/google/y/a/a/a/ah;

    iget-object v2, p1, Lcom/google/y/a/a/a/a;->f:Lcom/google/y/a/a/a/ah;

    invoke-virtual {v1, v2}, Lcom/google/y/a/a/a/ah;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 132
    :cond_11
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->g:Lcom/google/y/a/a/a/x;

    iget-object v2, p1, Lcom/google/y/a/a/a/a;->g:Lcom/google/y/a/a/a/x;

    invoke-virtual {v1, v2}, Lcom/google/y/a/a/a/x;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 141
    :cond_12
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->h:Lcom/google/y/a/a/a/o;

    iget-object v2, p1, Lcom/google/y/a/a/a/a;->h:Lcom/google/y/a/a/a/o;

    invoke-virtual {v1, v2}, Lcom/google/y/a/a/a/o;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    .line 150
    :cond_13
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->i:Lcom/google/y/a/a/a/ai;

    iget-object v2, p1, Lcom/google/y/a/a/a/a;->i:Lcom/google/y/a/a/a/ai;

    invoke-virtual {v1, v2}, Lcom/google/y/a/a/a/ai;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 159
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 161
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/a;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 163
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/a;->c:Lcom/google/y/a/a/a/f;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 165
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/a;->d:Lcom/google/y/a/a/a/i;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 167
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/a;->e:Lcom/google/y/a/a/a/y;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 169
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/a;->f:Lcom/google/y/a/a/a/ah;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 171
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/a;->g:Lcom/google/y/a/a/a/x;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 173
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/a;->h:Lcom/google/y/a/a/a/o;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 175
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/y/a/a/a/a;->i:Lcom/google/y/a/a/a/ai;

    if-nez v2, :cond_8

    :goto_8
    add-int/2addr v0, v1

    .line 177
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/y/a/a/a/a;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 178
    return v0

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 161
    :cond_1
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 163
    :cond_2
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->c:Lcom/google/y/a/a/a/f;

    invoke-virtual {v0}, Lcom/google/y/a/a/a/f;->hashCode()I

    move-result v0

    goto :goto_2

    .line 165
    :cond_3
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->d:Lcom/google/y/a/a/a/i;

    invoke-virtual {v0}, Lcom/google/y/a/a/a/i;->hashCode()I

    move-result v0

    goto :goto_3

    .line 167
    :cond_4
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->e:Lcom/google/y/a/a/a/y;

    invoke-virtual {v0}, Lcom/google/y/a/a/a/y;->hashCode()I

    move-result v0

    goto :goto_4

    .line 169
    :cond_5
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->f:Lcom/google/y/a/a/a/ah;

    invoke-virtual {v0}, Lcom/google/y/a/a/a/ah;->hashCode()I

    move-result v0

    goto :goto_5

    .line 171
    :cond_6
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->g:Lcom/google/y/a/a/a/x;

    invoke-virtual {v0}, Lcom/google/y/a/a/a/x;->hashCode()I

    move-result v0

    goto :goto_6

    .line 173
    :cond_7
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->h:Lcom/google/y/a/a/a/o;

    invoke-virtual {v0}, Lcom/google/y/a/a/a/o;->hashCode()I

    move-result v0

    goto :goto_7

    .line 175
    :cond_8
    iget-object v1, p0, Lcom/google/y/a/a/a/a;->i:Lcom/google/y/a/a/a/ai;

    invoke-virtual {v1}, Lcom/google/y/a/a/a/ai;->hashCode()I

    move-result v1

    goto :goto_8
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/y/a/a/a/a;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/a;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/a;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->c:Lcom/google/y/a/a/a/f;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/y/a/a/a/f;

    invoke-direct {v0}, Lcom/google/y/a/a/a/f;-><init>()V

    iput-object v0, p0, Lcom/google/y/a/a/a/a;->c:Lcom/google/y/a/a/a/f;

    :cond_1
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->c:Lcom/google/y/a/a/a/f;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->d:Lcom/google/y/a/a/a/i;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/y/a/a/a/i;

    invoke-direct {v0}, Lcom/google/y/a/a/a/i;-><init>()V

    iput-object v0, p0, Lcom/google/y/a/a/a/a;->d:Lcom/google/y/a/a/a/i;

    :cond_2
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->d:Lcom/google/y/a/a/a/i;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->e:Lcom/google/y/a/a/a/y;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/y/a/a/a/y;

    invoke-direct {v0}, Lcom/google/y/a/a/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/y/a/a/a/a;->e:Lcom/google/y/a/a/a/y;

    :cond_3
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->e:Lcom/google/y/a/a/a/y;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->f:Lcom/google/y/a/a/a/ah;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/y/a/a/a/ah;

    invoke-direct {v0}, Lcom/google/y/a/a/a/ah;-><init>()V

    iput-object v0, p0, Lcom/google/y/a/a/a/a;->f:Lcom/google/y/a/a/a/ah;

    :cond_4
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->f:Lcom/google/y/a/a/a/ah;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->g:Lcom/google/y/a/a/a/x;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/y/a/a/a/x;

    invoke-direct {v0}, Lcom/google/y/a/a/a/x;-><init>()V

    iput-object v0, p0, Lcom/google/y/a/a/a/a;->g:Lcom/google/y/a/a/a/x;

    :cond_5
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->g:Lcom/google/y/a/a/a/x;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->h:Lcom/google/y/a/a/a/o;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/y/a/a/a/o;

    invoke-direct {v0}, Lcom/google/y/a/a/a/o;-><init>()V

    iput-object v0, p0, Lcom/google/y/a/a/a/a;->h:Lcom/google/y/a/a/a/o;

    :cond_6
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->h:Lcom/google/y/a/a/a/o;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->i:Lcom/google/y/a/a/a/ai;

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/y/a/a/a/ai;

    invoke-direct {v0}, Lcom/google/y/a/a/a/ai;-><init>()V

    iput-object v0, p0, Lcom/google/y/a/a/a/a;->i:Lcom/google/y/a/a/a/ai;

    :cond_7
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->i:Lcom/google/y/a/a/a/ai;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 185
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/y/a/a/a/a;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 188
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/y/a/a/a/a;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 190
    :cond_1
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->c:Lcom/google/y/a/a/a/f;

    if-eqz v0, :cond_2

    .line 191
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/y/a/a/a/a;->c:Lcom/google/y/a/a/a/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 193
    :cond_2
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->d:Lcom/google/y/a/a/a/i;

    if-eqz v0, :cond_3

    .line 194
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/y/a/a/a/a;->d:Lcom/google/y/a/a/a/i;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 196
    :cond_3
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->e:Lcom/google/y/a/a/a/y;

    if-eqz v0, :cond_4

    .line 197
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/y/a/a/a/a;->e:Lcom/google/y/a/a/a/y;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 199
    :cond_4
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->f:Lcom/google/y/a/a/a/ah;

    if-eqz v0, :cond_5

    .line 200
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/y/a/a/a/a;->f:Lcom/google/y/a/a/a/ah;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 202
    :cond_5
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->g:Lcom/google/y/a/a/a/x;

    if-eqz v0, :cond_6

    .line 203
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/y/a/a/a/a;->g:Lcom/google/y/a/a/a/x;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 205
    :cond_6
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->h:Lcom/google/y/a/a/a/o;

    if-eqz v0, :cond_7

    .line 206
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/y/a/a/a/a;->h:Lcom/google/y/a/a/a/o;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 208
    :cond_7
    iget-object v0, p0, Lcom/google/y/a/a/a/a;->i:Lcom/google/y/a/a/a/ai;

    if-eqz v0, :cond_8

    .line 209
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/y/a/a/a/a;->i:Lcom/google/y/a/a/a/ai;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 211
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 212
    return-void
.end method
