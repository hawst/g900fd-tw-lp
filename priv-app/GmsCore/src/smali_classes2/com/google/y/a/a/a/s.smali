.class public final Lcom/google/y/a/a/a/s;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/google/y/a/a/a/b;

.field public c:Lcom/google/y/a/a/a/t;

.field public d:Lcom/google/y/a/a/a/j;

.field public e:Lcom/google/y/a/a/a/af;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/Boolean;

.field public i:Lcom/google/y/a/a/a/ab;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 51
    iput-object v0, p0, Lcom/google/y/a/a/a/s;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/y/a/a/a/s;->b:Lcom/google/y/a/a/a/b;

    iput-object v0, p0, Lcom/google/y/a/a/a/s;->c:Lcom/google/y/a/a/a/t;

    iput-object v0, p0, Lcom/google/y/a/a/a/s;->d:Lcom/google/y/a/a/a/j;

    iput-object v0, p0, Lcom/google/y/a/a/a/s;->e:Lcom/google/y/a/a/a/af;

    iput-object v0, p0, Lcom/google/y/a/a/a/s;->f:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/y/a/a/a/s;->g:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/y/a/a/a/s;->h:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/y/a/a/a/s;->i:Lcom/google/y/a/a/a/ab;

    iput-object v0, p0, Lcom/google/y/a/a/a/s;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/y/a/a/a/s;->cachedSize:I

    .line 52
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 214
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 215
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 216
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/y/a/a/a/s;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 219
    :cond_0
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->b:Lcom/google/y/a/a/a/b;

    if-eqz v1, :cond_1

    .line 220
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/y/a/a/a/s;->b:Lcom/google/y/a/a/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 223
    :cond_1
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->c:Lcom/google/y/a/a/a/t;

    if-eqz v1, :cond_2

    .line 224
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/y/a/a/a/s;->c:Lcom/google/y/a/a/a/t;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 227
    :cond_2
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->e:Lcom/google/y/a/a/a/af;

    if-eqz v1, :cond_3

    .line 228
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/y/a/a/a/s;->e:Lcom/google/y/a/a/a/af;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 231
    :cond_3
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 232
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/y/a/a/a/s;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 235
    :cond_4
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 236
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/y/a/a/a/s;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 239
    :cond_5
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 240
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/y/a/a/a/s;->h:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 243
    :cond_6
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->i:Lcom/google/y/a/a/a/ab;

    if-eqz v1, :cond_7

    .line 244
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/y/a/a/a/s;->i:Lcom/google/y/a/a/a/ab;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 247
    :cond_7
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->d:Lcom/google/y/a/a/a/j;

    if-eqz v1, :cond_8

    .line 248
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/y/a/a/a/s;->d:Lcom/google/y/a/a/a/j;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 251
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 71
    if-ne p1, p0, :cond_1

    .line 72
    const/4 v0, 0x1

    .line 151
    :cond_0
    :goto_0
    return v0

    .line 74
    :cond_1
    instance-of v1, p1, Lcom/google/y/a/a/a/s;

    if-eqz v1, :cond_0

    .line 77
    check-cast p1, Lcom/google/y/a/a/a/s;

    .line 78
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->a:Ljava/lang/String;

    if-nez v1, :cond_b

    .line 79
    iget-object v1, p1, Lcom/google/y/a/a/a/s;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 85
    :cond_2
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->b:Lcom/google/y/a/a/a/b;

    if-nez v1, :cond_c

    .line 86
    iget-object v1, p1, Lcom/google/y/a/a/a/s;->b:Lcom/google/y/a/a/a/b;

    if-nez v1, :cond_0

    .line 94
    :cond_3
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->c:Lcom/google/y/a/a/a/t;

    if-nez v1, :cond_d

    .line 95
    iget-object v1, p1, Lcom/google/y/a/a/a/s;->c:Lcom/google/y/a/a/a/t;

    if-nez v1, :cond_0

    .line 103
    :cond_4
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->d:Lcom/google/y/a/a/a/j;

    if-nez v1, :cond_e

    .line 104
    iget-object v1, p1, Lcom/google/y/a/a/a/s;->d:Lcom/google/y/a/a/a/j;

    if-nez v1, :cond_0

    .line 112
    :cond_5
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->e:Lcom/google/y/a/a/a/af;

    if-nez v1, :cond_f

    .line 113
    iget-object v1, p1, Lcom/google/y/a/a/a/s;->e:Lcom/google/y/a/a/a/af;

    if-nez v1, :cond_0

    .line 121
    :cond_6
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->f:Ljava/lang/Integer;

    if-nez v1, :cond_10

    .line 122
    iget-object v1, p1, Lcom/google/y/a/a/a/s;->f:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 128
    :cond_7
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->g:Ljava/lang/Integer;

    if-nez v1, :cond_11

    .line 129
    iget-object v1, p1, Lcom/google/y/a/a/a/s;->g:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 135
    :cond_8
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->h:Ljava/lang/Boolean;

    if-nez v1, :cond_12

    .line 136
    iget-object v1, p1, Lcom/google/y/a/a/a/s;->h:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 142
    :cond_9
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->i:Lcom/google/y/a/a/a/ab;

    if-nez v1, :cond_13

    .line 143
    iget-object v1, p1, Lcom/google/y/a/a/a/s;->i:Lcom/google/y/a/a/a/ab;

    if-nez v1, :cond_0

    .line 151
    :cond_a
    invoke-virtual {p0, p1}, Lcom/google/y/a/a/a/s;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 82
    :cond_b
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/y/a/a/a/s;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 90
    :cond_c
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->b:Lcom/google/y/a/a/a/b;

    iget-object v2, p1, Lcom/google/y/a/a/a/s;->b:Lcom/google/y/a/a/a/b;

    invoke-virtual {v1, v2}, Lcom/google/y/a/a/a/b;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 99
    :cond_d
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->c:Lcom/google/y/a/a/a/t;

    iget-object v2, p1, Lcom/google/y/a/a/a/s;->c:Lcom/google/y/a/a/a/t;

    invoke-virtual {v1, v2}, Lcom/google/y/a/a/a/t;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 108
    :cond_e
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->d:Lcom/google/y/a/a/a/j;

    iget-object v2, p1, Lcom/google/y/a/a/a/s;->d:Lcom/google/y/a/a/a/j;

    invoke-virtual {v1, v2}, Lcom/google/y/a/a/a/j;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 117
    :cond_f
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->e:Lcom/google/y/a/a/a/af;

    iget-object v2, p1, Lcom/google/y/a/a/a/s;->e:Lcom/google/y/a/a/a/af;

    invoke-virtual {v1, v2}, Lcom/google/y/a/a/a/af;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 125
    :cond_10
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->f:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/y/a/a/a/s;->f:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 132
    :cond_11
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->g:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/y/a/a/a/s;->g:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 139
    :cond_12
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->h:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/y/a/a/a/s;->h:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    .line 147
    :cond_13
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->i:Lcom/google/y/a/a/a/ab;

    iget-object v2, p1, Lcom/google/y/a/a/a/s;->i:Lcom/google/y/a/a/a/ab;

    invoke-virtual {v1, v2}, Lcom/google/y/a/a/a/ab;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 156
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 159
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/s;->b:Lcom/google/y/a/a/a/b;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 161
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/s;->c:Lcom/google/y/a/a/a/t;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 163
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/s;->d:Lcom/google/y/a/a/a/j;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 165
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/s;->e:Lcom/google/y/a/a/a/af;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 167
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/s;->f:Ljava/lang/Integer;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 169
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/s;->g:Ljava/lang/Integer;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 171
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/s;->h:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 173
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/y/a/a/a/s;->i:Lcom/google/y/a/a/a/ab;

    if-nez v2, :cond_8

    :goto_8
    add-int/2addr v0, v1

    .line 175
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/y/a/a/a/s;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 176
    return v0

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->b:Lcom/google/y/a/a/a/b;

    invoke-virtual {v0}, Lcom/google/y/a/a/a/b;->hashCode()I

    move-result v0

    goto :goto_1

    .line 161
    :cond_2
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->c:Lcom/google/y/a/a/a/t;

    invoke-virtual {v0}, Lcom/google/y/a/a/a/t;->hashCode()I

    move-result v0

    goto :goto_2

    .line 163
    :cond_3
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->d:Lcom/google/y/a/a/a/j;

    invoke-virtual {v0}, Lcom/google/y/a/a/a/j;->hashCode()I

    move-result v0

    goto :goto_3

    .line 165
    :cond_4
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->e:Lcom/google/y/a/a/a/af;

    invoke-virtual {v0}, Lcom/google/y/a/a/a/af;->hashCode()I

    move-result v0

    goto :goto_4

    .line 167
    :cond_5
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_5

    .line 169
    :cond_6
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->g:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_6

    .line 171
    :cond_7
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->h:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_7

    .line 173
    :cond_8
    iget-object v1, p0, Lcom/google/y/a/a/a/s;->i:Lcom/google/y/a/a/a/ab;

    invoke-virtual {v1}, Lcom/google/y/a/a/a/ab;->hashCode()I

    move-result v1

    goto :goto_8
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/y/a/a/a/s;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/s;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->b:Lcom/google/y/a/a/a/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/y/a/a/a/b;

    invoke-direct {v0}, Lcom/google/y/a/a/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/y/a/a/a/s;->b:Lcom/google/y/a/a/a/b;

    :cond_1
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->b:Lcom/google/y/a/a/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->c:Lcom/google/y/a/a/a/t;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/y/a/a/a/t;

    invoke-direct {v0}, Lcom/google/y/a/a/a/t;-><init>()V

    iput-object v0, p0, Lcom/google/y/a/a/a/s;->c:Lcom/google/y/a/a/a/t;

    :cond_2
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->c:Lcom/google/y/a/a/a/t;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->e:Lcom/google/y/a/a/a/af;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/y/a/a/a/af;

    invoke-direct {v0}, Lcom/google/y/a/a/a/af;-><init>()V

    iput-object v0, p0, Lcom/google/y/a/a/a/s;->e:Lcom/google/y/a/a/a/af;

    :cond_3
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->e:Lcom/google/y/a/a/a/af;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/s;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/s;->g:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/s;->h:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->i:Lcom/google/y/a/a/a/ab;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/y/a/a/a/ab;

    invoke-direct {v0}, Lcom/google/y/a/a/a/ab;-><init>()V

    iput-object v0, p0, Lcom/google/y/a/a/a/s;->i:Lcom/google/y/a/a/a/ab;

    :cond_4
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->i:Lcom/google/y/a/a/a/ab;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->d:Lcom/google/y/a/a/a/j;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/y/a/a/a/j;

    invoke-direct {v0}, Lcom/google/y/a/a/a/j;-><init>()V

    iput-object v0, p0, Lcom/google/y/a/a/a/s;->d:Lcom/google/y/a/a/a/j;

    :cond_5
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->d:Lcom/google/y/a/a/a/j;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 183
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/y/a/a/a/s;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->b:Lcom/google/y/a/a/a/b;

    if-eqz v0, :cond_1

    .line 186
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/y/a/a/a/s;->b:Lcom/google/y/a/a/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 188
    :cond_1
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->c:Lcom/google/y/a/a/a/t;

    if-eqz v0, :cond_2

    .line 189
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/y/a/a/a/s;->c:Lcom/google/y/a/a/a/t;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 191
    :cond_2
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->e:Lcom/google/y/a/a/a/af;

    if-eqz v0, :cond_3

    .line 192
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/y/a/a/a/s;->e:Lcom/google/y/a/a/a/af;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 194
    :cond_3
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 195
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/y/a/a/a/s;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 197
    :cond_4
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 198
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/y/a/a/a/s;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 200
    :cond_5
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 201
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/y/a/a/a/s;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 203
    :cond_6
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->i:Lcom/google/y/a/a/a/ab;

    if-eqz v0, :cond_7

    .line 204
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/y/a/a/a/s;->i:Lcom/google/y/a/a/a/ab;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 206
    :cond_7
    iget-object v0, p0, Lcom/google/y/a/a/a/s;->d:Lcom/google/y/a/a/a/j;

    if-eqz v0, :cond_8

    .line 207
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/y/a/a/a/s;->d:Lcom/google/y/a/a/a/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 209
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 210
    return-void
.end method
