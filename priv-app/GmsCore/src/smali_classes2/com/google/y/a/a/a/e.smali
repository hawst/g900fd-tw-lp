.class public final Lcom/google/y/a/a/a/e;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:[Lcom/google/y/a/a/a/j;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 39
    iput-object v1, p0, Lcom/google/y/a/a/a/e;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/y/a/a/a/e;->b:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/y/a/a/a/e;->c:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/y/a/a/a/e;->d:Ljava/lang/String;

    invoke-static {}, Lcom/google/y/a/a/a/j;->a()[Lcom/google/y/a/a/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/e;->e:[Lcom/google/y/a/a/a/j;

    iput-object v1, p0, Lcom/google/y/a/a/a/e;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/y/a/a/a/e;->cachedSize:I

    .line 40
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 142
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 143
    iget-object v1, p0, Lcom/google/y/a/a/a/e;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 144
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/y/a/a/a/e;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 147
    :cond_0
    iget-object v1, p0, Lcom/google/y/a/a/a/e;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 148
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/y/a/a/a/e;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 151
    :cond_1
    iget-object v1, p0, Lcom/google/y/a/a/a/e;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 152
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/y/a/a/a/e;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 155
    :cond_2
    iget-object v1, p0, Lcom/google/y/a/a/a/e;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 156
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/y/a/a/a/e;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 159
    :cond_3
    iget-object v1, p0, Lcom/google/y/a/a/a/e;->e:[Lcom/google/y/a/a/a/j;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/y/a/a/a/e;->e:[Lcom/google/y/a/a/a/j;

    array-length v1, v1

    if-lez v1, :cond_6

    .line 160
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/y/a/a/a/e;->e:[Lcom/google/y/a/a/a/j;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 161
    iget-object v2, p0, Lcom/google/y/a/a/a/e;->e:[Lcom/google/y/a/a/a/j;

    aget-object v2, v2, v0

    .line 162
    if-eqz v2, :cond_4

    .line 163
    const/4 v3, 0x5

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 160
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 168
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 55
    if-ne p1, p0, :cond_1

    .line 56
    const/4 v0, 0x1

    .line 94
    :cond_0
    :goto_0
    return v0

    .line 58
    :cond_1
    instance-of v1, p1, Lcom/google/y/a/a/a/e;

    if-eqz v1, :cond_0

    .line 61
    check-cast p1, Lcom/google/y/a/a/a/e;

    .line 62
    iget-object v1, p0, Lcom/google/y/a/a/a/e;->a:Ljava/lang/String;

    if-nez v1, :cond_6

    .line 63
    iget-object v1, p1, Lcom/google/y/a/a/a/e;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 69
    :cond_2
    iget-object v1, p0, Lcom/google/y/a/a/a/e;->b:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 70
    iget-object v1, p1, Lcom/google/y/a/a/a/e;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 76
    :cond_3
    iget-object v1, p0, Lcom/google/y/a/a/a/e;->c:Ljava/lang/String;

    if-nez v1, :cond_8

    .line 77
    iget-object v1, p1, Lcom/google/y/a/a/a/e;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 83
    :cond_4
    iget-object v1, p0, Lcom/google/y/a/a/a/e;->d:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 84
    iget-object v1, p1, Lcom/google/y/a/a/a/e;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 90
    :cond_5
    iget-object v1, p0, Lcom/google/y/a/a/a/e;->e:[Lcom/google/y/a/a/a/j;

    iget-object v2, p1, Lcom/google/y/a/a/a/e;->e:[Lcom/google/y/a/a/a/j;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    invoke-virtual {p0, p1}, Lcom/google/y/a/a/a/e;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 66
    :cond_6
    iget-object v1, p0, Lcom/google/y/a/a/a/e;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/y/a/a/a/e;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 73
    :cond_7
    iget-object v1, p0, Lcom/google/y/a/a/a/e;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/y/a/a/a/e;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 80
    :cond_8
    iget-object v1, p0, Lcom/google/y/a/a/a/e;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/y/a/a/a/e;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 87
    :cond_9
    iget-object v1, p0, Lcom/google/y/a/a/a/e;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/y/a/a/a/e;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 99
    iget-object v0, p0, Lcom/google/y/a/a/a/e;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 102
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/e;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 104
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/e;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 106
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/y/a/a/a/e;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 108
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/y/a/a/a/e;->e:[Lcom/google/y/a/a/a/j;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 110
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/y/a/a/a/e;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 111
    return v0

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/google/y/a/a/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/google/y/a/a/a/e;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 104
    :cond_2
    iget-object v0, p0, Lcom/google/y/a/a/a/e;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 106
    :cond_3
    iget-object v1, p0, Lcom/google/y/a/a/a/e;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/y/a/a/a/e;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/e;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/e;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/e;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/e;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/y/a/a/a/e;->e:[Lcom/google/y/a/a/a/j;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/y/a/a/a/j;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/y/a/a/a/e;->e:[Lcom/google/y/a/a/a/j;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/y/a/a/a/j;

    invoke-direct {v3}, Lcom/google/y/a/a/a/j;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/y/a/a/a/e;->e:[Lcom/google/y/a/a/a/j;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/y/a/a/a/j;

    invoke-direct {v3}, Lcom/google/y/a/a/a/j;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/y/a/a/a/e;->e:[Lcom/google/y/a/a/a/j;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/y/a/a/a/e;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 118
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/y/a/a/a/e;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/google/y/a/a/a/e;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 121
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/y/a/a/a/e;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 123
    :cond_1
    iget-object v0, p0, Lcom/google/y/a/a/a/e;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 124
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/y/a/a/a/e;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 126
    :cond_2
    iget-object v0, p0, Lcom/google/y/a/a/a/e;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 127
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/y/a/a/a/e;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 129
    :cond_3
    iget-object v0, p0, Lcom/google/y/a/a/a/e;->e:[Lcom/google/y/a/a/a/j;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/y/a/a/a/e;->e:[Lcom/google/y/a/a/a/j;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 130
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/y/a/a/a/e;->e:[Lcom/google/y/a/a/a/j;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 131
    iget-object v1, p0, Lcom/google/y/a/a/a/e;->e:[Lcom/google/y/a/a/a/j;

    aget-object v1, v1, v0

    .line 132
    if-eqz v1, :cond_4

    .line 133
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 130
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 137
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 138
    return-void
.end method
