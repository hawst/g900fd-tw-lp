.class public final Lcom/google/y/a/a/a/ak;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/y/a/a/a/ak;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Lcom/google/y/a/a/a/aa;

.field public c:Lcom/google/y/a/a/a/aa;

.field public d:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 36
    iput-object v0, p0, Lcom/google/y/a/a/a/ak;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/y/a/a/a/ak;->b:Lcom/google/y/a/a/a/aa;

    iput-object v0, p0, Lcom/google/y/a/a/a/ak;->c:Lcom/google/y/a/a/a/aa;

    iput-object v0, p0, Lcom/google/y/a/a/a/ak;->d:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/y/a/a/a/ak;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/y/a/a/a/ak;->cachedSize:I

    .line 37
    return-void
.end method

.method public static a()[Lcom/google/y/a/a/a/ak;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/y/a/a/a/ak;->e:[Lcom/google/y/a/a/a/ak;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/y/a/a/a/ak;->e:[Lcom/google/y/a/a/a/ak;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/y/a/a/a/ak;

    sput-object v0, Lcom/google/y/a/a/a/ak;->e:[Lcom/google/y/a/a/a/ak;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/y/a/a/a/ak;->e:[Lcom/google/y/a/a/a/ak;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 126
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 127
    iget-object v1, p0, Lcom/google/y/a/a/a/ak;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 128
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/y/a/a/a/ak;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 131
    :cond_0
    iget-object v1, p0, Lcom/google/y/a/a/a/ak;->b:Lcom/google/y/a/a/a/aa;

    if-eqz v1, :cond_1

    .line 132
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/y/a/a/a/ak;->b:Lcom/google/y/a/a/a/aa;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 135
    :cond_1
    iget-object v1, p0, Lcom/google/y/a/a/a/ak;->c:Lcom/google/y/a/a/a/aa;

    if-eqz v1, :cond_2

    .line 136
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/y/a/a/a/ak;->c:Lcom/google/y/a/a/a/aa;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 139
    :cond_2
    iget-object v1, p0, Lcom/google/y/a/a/a/ak;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 140
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/y/a/a/a/ak;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 143
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 51
    if-ne p1, p0, :cond_1

    .line 52
    const/4 v0, 0x1

    .line 89
    :cond_0
    :goto_0
    return v0

    .line 54
    :cond_1
    instance-of v1, p1, Lcom/google/y/a/a/a/ak;

    if-eqz v1, :cond_0

    .line 57
    check-cast p1, Lcom/google/y/a/a/a/ak;

    .line 58
    iget-object v1, p0, Lcom/google/y/a/a/a/ak;->a:Ljava/lang/Integer;

    if-nez v1, :cond_6

    .line 59
    iget-object v1, p1, Lcom/google/y/a/a/a/ak;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 64
    :cond_2
    iget-object v1, p0, Lcom/google/y/a/a/a/ak;->b:Lcom/google/y/a/a/a/aa;

    if-nez v1, :cond_7

    .line 65
    iget-object v1, p1, Lcom/google/y/a/a/a/ak;->b:Lcom/google/y/a/a/a/aa;

    if-nez v1, :cond_0

    .line 73
    :cond_3
    iget-object v1, p0, Lcom/google/y/a/a/a/ak;->c:Lcom/google/y/a/a/a/aa;

    if-nez v1, :cond_8

    .line 74
    iget-object v1, p1, Lcom/google/y/a/a/a/ak;->c:Lcom/google/y/a/a/a/aa;

    if-nez v1, :cond_0

    .line 82
    :cond_4
    iget-object v1, p0, Lcom/google/y/a/a/a/ak;->d:Ljava/lang/Integer;

    if-nez v1, :cond_9

    .line 83
    iget-object v1, p1, Lcom/google/y/a/a/a/ak;->d:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 89
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/y/a/a/a/ak;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 62
    :cond_6
    iget-object v1, p0, Lcom/google/y/a/a/a/ak;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/y/a/a/a/ak;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 69
    :cond_7
    iget-object v1, p0, Lcom/google/y/a/a/a/ak;->b:Lcom/google/y/a/a/a/aa;

    iget-object v2, p1, Lcom/google/y/a/a/a/ak;->b:Lcom/google/y/a/a/a/aa;

    invoke-virtual {v1, v2}, Lcom/google/y/a/a/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 78
    :cond_8
    iget-object v1, p0, Lcom/google/y/a/a/a/ak;->c:Lcom/google/y/a/a/a/aa;

    iget-object v2, p1, Lcom/google/y/a/a/a/ak;->c:Lcom/google/y/a/a/a/aa;

    invoke-virtual {v1, v2}, Lcom/google/y/a/a/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 86
    :cond_9
    iget-object v1, p0, Lcom/google/y/a/a/a/ak;->d:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/y/a/a/a/ak;->d:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 94
    iget-object v0, p0, Lcom/google/y/a/a/a/ak;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 96
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/ak;->b:Lcom/google/y/a/a/a/aa;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 98
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/ak;->c:Lcom/google/y/a/a/a/aa;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 100
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/y/a/a/a/ak;->d:Ljava/lang/Integer;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 102
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/y/a/a/a/ak;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    return v0

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/google/y/a/a/a/ak;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/google/y/a/a/a/ak;->b:Lcom/google/y/a/a/a/aa;

    invoke-virtual {v0}, Lcom/google/y/a/a/a/aa;->hashCode()I

    move-result v0

    goto :goto_1

    .line 98
    :cond_2
    iget-object v0, p0, Lcom/google/y/a/a/a/ak;->c:Lcom/google/y/a/a/a/aa;

    invoke-virtual {v0}, Lcom/google/y/a/a/a/aa;->hashCode()I

    move-result v0

    goto :goto_2

    .line 100
    :cond_3
    iget-object v1, p0, Lcom/google/y/a/a/a/ak;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/y/a/a/a/ak;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/ak;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/y/a/a/a/ak;->b:Lcom/google/y/a/a/a/aa;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/y/a/a/a/aa;

    invoke-direct {v0}, Lcom/google/y/a/a/a/aa;-><init>()V

    iput-object v0, p0, Lcom/google/y/a/a/a/ak;->b:Lcom/google/y/a/a/a/aa;

    :cond_1
    iget-object v0, p0, Lcom/google/y/a/a/a/ak;->b:Lcom/google/y/a/a/a/aa;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/y/a/a/a/ak;->c:Lcom/google/y/a/a/a/aa;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/y/a/a/a/aa;

    invoke-direct {v0}, Lcom/google/y/a/a/a/aa;-><init>()V

    iput-object v0, p0, Lcom/google/y/a/a/a/ak;->c:Lcom/google/y/a/a/a/aa;

    :cond_2
    iget-object v0, p0, Lcom/google/y/a/a/a/ak;->c:Lcom/google/y/a/a/a/aa;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/ak;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
        0x20 -> :sswitch_5
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_2
        0xa -> :sswitch_2
        0x2f -> :sswitch_2
        0x55 -> :sswitch_2
        0x57 -> :sswitch_2
        0x58 -> :sswitch_2
        0x5e -> :sswitch_2
        0x75 -> :sswitch_2
        0x8a -> :sswitch_2
        0x8b -> :sswitch_2
        0x91 -> :sswitch_2
        0x94 -> :sswitch_2
        0xa2 -> :sswitch_2
        0x102 -> :sswitch_2
        0x106 -> :sswitch_2
        0x123 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/y/a/a/a/ak;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 110
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/y/a/a/a/ak;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/google/y/a/a/a/ak;->b:Lcom/google/y/a/a/a/aa;

    if-eqz v0, :cond_1

    .line 113
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/y/a/a/a/ak;->b:Lcom/google/y/a/a/a/aa;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 115
    :cond_1
    iget-object v0, p0, Lcom/google/y/a/a/a/ak;->c:Lcom/google/y/a/a/a/aa;

    if-eqz v0, :cond_2

    .line 116
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/y/a/a/a/ak;->c:Lcom/google/y/a/a/a/aa;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 118
    :cond_2
    iget-object v0, p0, Lcom/google/y/a/a/a/ak;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 119
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/y/a/a/a/ak;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 121
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 122
    return-void
.end method
