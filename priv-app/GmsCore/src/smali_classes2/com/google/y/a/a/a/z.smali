.class public final Lcom/google/y/a/a/a/z;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/y/a/a/a/c;

.field public b:[Lcom/google/y/a/a/a/a;

.field public c:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/y/a/a/a/c;->a()[Lcom/google/y/a/a/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/z;->a:[Lcom/google/y/a/a/a/c;

    invoke-static {}, Lcom/google/y/a/a/a/a;->a()[Lcom/google/y/a/a/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/z;->b:[Lcom/google/y/a/a/a/a;

    iput-object v1, p0, Lcom/google/y/a/a/a/z;->c:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/y/a/a/a/z;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/y/a/a/a/z;->cachedSize:I

    .line 34
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 112
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 113
    iget-object v2, p0, Lcom/google/y/a/a/a/z;->a:[Lcom/google/y/a/a/a/c;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/y/a/a/a/z;->a:[Lcom/google/y/a/a/a/c;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 114
    :goto_0
    iget-object v3, p0, Lcom/google/y/a/a/a/z;->a:[Lcom/google/y/a/a/a/c;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 115
    iget-object v3, p0, Lcom/google/y/a/a/a/z;->a:[Lcom/google/y/a/a/a/c;

    aget-object v3, v3, v0

    .line 116
    if-eqz v3, :cond_0

    .line 117
    const/4 v4, 0x1

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 114
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 122
    :cond_2
    iget-object v2, p0, Lcom/google/y/a/a/a/z;->b:[Lcom/google/y/a/a/a/a;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/y/a/a/a/z;->b:[Lcom/google/y/a/a/a/a;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 123
    :goto_1
    iget-object v2, p0, Lcom/google/y/a/a/a/z;->b:[Lcom/google/y/a/a/a/a;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 124
    iget-object v2, p0, Lcom/google/y/a/a/a/z;->b:[Lcom/google/y/a/a/a/a;

    aget-object v2, v2, v1

    .line 125
    if-eqz v2, :cond_3

    .line 126
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 123
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 131
    :cond_4
    iget-object v1, p0, Lcom/google/y/a/a/a/z;->c:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 132
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/y/a/a/a/z;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 135
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 47
    if-ne p1, p0, :cond_1

    .line 48
    const/4 v0, 0x1

    .line 69
    :cond_0
    :goto_0
    return v0

    .line 50
    :cond_1
    instance-of v1, p1, Lcom/google/y/a/a/a/z;

    if-eqz v1, :cond_0

    .line 53
    check-cast p1, Lcom/google/y/a/a/a/z;

    .line 54
    iget-object v1, p0, Lcom/google/y/a/a/a/z;->a:[Lcom/google/y/a/a/a/c;

    iget-object v2, p1, Lcom/google/y/a/a/a/z;->a:[Lcom/google/y/a/a/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 58
    iget-object v1, p0, Lcom/google/y/a/a/a/z;->b:[Lcom/google/y/a/a/a/a;

    iget-object v2, p1, Lcom/google/y/a/a/a/z;->b:[Lcom/google/y/a/a/a/a;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62
    iget-object v1, p0, Lcom/google/y/a/a/a/z;->c:Ljava/lang/Long;

    if-nez v1, :cond_3

    .line 63
    iget-object v1, p1, Lcom/google/y/a/a/a/z;->c:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 69
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/y/a/a/a/z;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 66
    :cond_3
    iget-object v1, p0, Lcom/google/y/a/a/a/z;->c:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/y/a/a/a/z;->c:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/y/a/a/a/z;->a:[Lcom/google/y/a/a/a/c;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 77
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/y/a/a/a/z;->b:[Lcom/google/y/a/a/a/a;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/y/a/a/a/z;->c:Ljava/lang/Long;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 81
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/y/a/a/a/z;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 82
    return v0

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/google/y/a/a/a/z;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/y/a/a/a/z;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/y/a/a/a/z;->a:[Lcom/google/y/a/a/a/c;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/y/a/a/a/c;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/y/a/a/a/z;->a:[Lcom/google/y/a/a/a/c;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/y/a/a/a/c;

    invoke-direct {v3}, Lcom/google/y/a/a/a/c;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/y/a/a/a/z;->a:[Lcom/google/y/a/a/a/c;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/y/a/a/a/c;

    invoke-direct {v3}, Lcom/google/y/a/a/a/c;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/y/a/a/a/z;->a:[Lcom/google/y/a/a/a/c;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/y/a/a/a/z;->b:[Lcom/google/y/a/a/a/a;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/y/a/a/a/a;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/y/a/a/a/z;->b:[Lcom/google/y/a/a/a/a;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/y/a/a/a/a;

    invoke-direct {v3}, Lcom/google/y/a/a/a/a;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/y/a/a/a/z;->b:[Lcom/google/y/a/a/a/a;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/y/a/a/a/a;

    invoke-direct {v3}, Lcom/google/y/a/a/a/a;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/y/a/a/a/z;->b:[Lcom/google/y/a/a/a/a;

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/y/a/a/a/z;->c:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 88
    iget-object v0, p0, Lcom/google/y/a/a/a/z;->a:[Lcom/google/y/a/a/a/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/y/a/a/a/z;->a:[Lcom/google/y/a/a/a/c;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 89
    :goto_0
    iget-object v2, p0, Lcom/google/y/a/a/a/z;->a:[Lcom/google/y/a/a/a/c;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 90
    iget-object v2, p0, Lcom/google/y/a/a/a/z;->a:[Lcom/google/y/a/a/a/c;

    aget-object v2, v2, v0

    .line 91
    if-eqz v2, :cond_0

    .line 92
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 89
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/google/y/a/a/a/z;->b:[Lcom/google/y/a/a/a/a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/y/a/a/a/z;->b:[Lcom/google/y/a/a/a/a;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 97
    :goto_1
    iget-object v0, p0, Lcom/google/y/a/a/a/z;->b:[Lcom/google/y/a/a/a/a;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 98
    iget-object v0, p0, Lcom/google/y/a/a/a/z;->b:[Lcom/google/y/a/a/a/a;

    aget-object v0, v0, v1

    .line 99
    if-eqz v0, :cond_2

    .line 100
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 97
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 104
    :cond_3
    iget-object v0, p0, Lcom/google/y/a/a/a/z;->c:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 105
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/y/a/a/a/z;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 107
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 108
    return-void
.end method
