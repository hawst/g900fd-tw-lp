.class public final Lcom/google/t/b/c/a/u;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/location/b/bd;

.field public final b:Lcom/google/t/b/c/a/v;

.field private final c:I


# direct methods
.method public constructor <init>(Lcom/google/t/b/c/a/v;I)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/t/b/c/a/u;->b:Lcom/google/t/b/c/a/v;

    .line 33
    iput p2, p0, Lcom/google/t/b/c/a/u;->c:I

    .line 34
    new-instance v0, Lcom/google/android/location/b/bd;

    iget v1, p0, Lcom/google/t/b/c/a/u;->c:I

    invoke-direct {v0, v1}, Lcom/google/android/location/b/bd;-><init>(I)V

    iput-object v0, p0, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    .line 35
    return-void
.end method

.method public static final a(Ljava/lang/Object;I)I
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    rem-int/2addr v0, p1

    add-int/2addr v0, p1

    rem-int/2addr v0, p1

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/bd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/ax;

    .line 63
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/t/b/c/a/u;->b:Lcom/google/t/b/c/a/v;

    invoke-interface {v1}, Lcom/google/t/b/c/a/v;->a()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/j/b;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/location/e/ax;->a(J)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(J)V
    .locals 5

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, p1, p2, v2, v3}, Lcom/google/android/location/b/bd;->a(JJ)V

    .line 139
    return-void
.end method

.method public final a(Lcom/google/t/b/c/a/u;)V
    .locals 6

    .prologue
    .line 146
    iget-object v0, p1, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    invoke-virtual {v0}, Lcom/google/android/location/b/bd;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 147
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/e/ax;

    .line 148
    iget-object v3, p0, Lcom/google/t/b/c/a/u;->b:Lcom/google/t/b/c/a/v;

    invoke-interface {v3}, Lcom/google/t/b/c/a/v;->a()Lcom/google/android/location/j/b;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/location/j/b;->b()J

    move-result-wide v4

    iput-wide v4, v1, Lcom/google/android/location/e/ax;->c:J

    .line 149
    iget-object v3, p0, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0, v1}, Lcom/google/android/location/b/bd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 151
    :cond_0
    return-void
.end method

.method public final a(Ljava/io/DataInput;)V
    .locals 8

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    invoke-virtual {v0}, Lcom/google/android/location/b/bd;->clear()V

    .line 98
    :try_start_0
    invoke-interface {p1}, Ljava/io/DataInput;->readByte()B

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 99
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Loaded cache version invalid."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :catch_0
    move-exception v0

    .line 122
    iget-object v1, p0, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    invoke-virtual {v1}, Lcom/google/android/location/b/bd;->clear()V

    .line 123
    throw v0

    .line 102
    :cond_0
    :try_start_1
    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v2

    .line 103
    if-ltz v2, :cond_1

    iget v0, p0, Lcom/google/t/b/c/a/u;->c:I

    if-le v2, v0, :cond_2

    .line 104
    :cond_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Loaded cache size is negative or too large."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_2
    const v0, -0x11f19ed4

    xor-int/2addr v0, v2

    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 108
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Loaded cache size does not pass redundancy check."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 111
    :cond_3
    iget-object v0, p0, Lcom/google/t/b/c/a/u;->b:Lcom/google/t/b/c/a/v;

    invoke-interface {v0}, Lcom/google/t/b/c/a/v;->a()Lcom/google/android/location/j/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/j/b;->b()J

    move-result-wide v4

    .line 112
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_5

    .line 113
    iget-object v0, p0, Lcom/google/t/b/c/a/u;->b:Lcom/google/t/b/c/a/v;

    invoke-interface {v0}, Lcom/google/t/b/c/a/v;->q()Lcom/google/android/location/e/ak;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/location/e/ak;->a(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v3

    .line 114
    iget-object v0, p0, Lcom/google/t/b/c/a/u;->b:Lcom/google/t/b/c/a/v;

    invoke-interface {v0}, Lcom/google/t/b/c/a/v;->r()Lcom/google/android/location/e/ak;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/location/e/ak;->a(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/ax;

    .line 116
    iget-wide v6, v0, Lcom/google/android/location/e/ax;->b:J

    cmp-long v6, v6, v4

    if-gtz v6, :cond_4

    iget-wide v6, v0, Lcom/google/android/location/e/ax;->c:J

    cmp-long v6, v6, v4

    if-gtz v6, :cond_4

    .line 118
    iget-object v6, p0, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    invoke-virtual {v6, v3, v0}, Lcom/google/android/location/b/bd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 112
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 124
    :cond_5
    return-void
.end method

.method public final a(Ljava/io/DataOutput;)V
    .locals 4

    .prologue
    .line 200
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 202
    iget-object v0, p0, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    invoke-virtual {v0}, Lcom/google/android/location/b/bd;->size()I

    move-result v0

    .line 203
    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 204
    const v1, -0x11f19ed4

    xor-int/2addr v0, v1

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 206
    iget-object v0, p0, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    invoke-virtual {v0}, Lcom/google/android/location/b/bd;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 207
    iget-object v2, p0, Lcom/google/t/b/c/a/u;->b:Lcom/google/t/b/c/a/v;

    invoke-interface {v2}, Lcom/google/t/b/c/a/v;->q()Lcom/google/android/location/e/ak;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3, p1}, Lcom/google/android/location/e/ak;->a(Ljava/lang/Object;Ljava/io/DataOutput;)V

    .line 208
    iget-object v2, p0, Lcom/google/t/b/c/a/u;->b:Lcom/google/t/b/c/a/v;

    invoke-interface {v2}, Lcom/google/t/b/c/a/v;->r()Lcom/google/android/location/e/ak;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0, p1}, Lcom/google/android/location/e/ak;->a(Ljava/lang/Object;Ljava/io/DataOutput;)V

    goto :goto_0

    .line 210
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    new-instance v1, Lcom/google/android/location/e/ax;

    iget-object v2, p0, Lcom/google/t/b/c/a/u;->b:Lcom/google/t/b/c/a/v;

    invoke-interface {v2}, Lcom/google/t/b/c/a/v;->a()Lcom/google/android/location/j/b;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/location/j/b;->b()J

    move-result-wide v2

    invoke-direct {v1, p2, v2, v3}, Lcom/google/android/location/e/ax;-><init>(Ljava/lang/Object;J)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/b/bd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    invoke-virtual {v0}, Lcom/google/android/location/b/bd;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    invoke-virtual {v0}, Lcom/google/android/location/b/bd;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    new-instance v1, Lcom/google/android/location/e/ax;

    iget-object v2, p0, Lcom/google/t/b/c/a/u;->b:Lcom/google/t/b/c/a/v;

    invoke-interface {v2}, Lcom/google/t/b/c/a/v;->s()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lcom/google/t/b/c/a/u;->b:Lcom/google/t/b/c/a/v;

    invoke-interface {v3}, Lcom/google/t/b/c/a/v;->a()Lcom/google/android/location/j/b;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/location/j/b;->b()J

    move-result-wide v4

    invoke-direct {v1, v2, v4, v5}, Lcom/google/android/location/e/ax;-><init>(Ljava/lang/Object;J)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/b/bd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    return-void
.end method

.method public final c(Ljava/lang/Object;)Lcom/google/android/location/e/ax;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/bd;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/ax;

    return-object v0
.end method
