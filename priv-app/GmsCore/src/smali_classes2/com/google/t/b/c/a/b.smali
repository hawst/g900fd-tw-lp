.class public final Lcom/google/t/b/c/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/j/g;


# static fields
.field private static final a:Lcom/google/t/b/c/a/b;


# instance fields
.field private b:Lcom/google/t/b/c/a/a;

.field private c:Z

.field private d:J

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/google/t/b/c/a/b;

    invoke-direct {v0}, Lcom/google/t/b/c/a/b;-><init>()V

    sput-object v0, Lcom/google/t/b/c/a/b;->a:Lcom/google/t/b/c/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-boolean v2, p0, Lcom/google/t/b/c/a/b;->c:Z

    .line 28
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/t/b/c/a/b;->d:J

    .line 31
    iput-boolean v2, p0, Lcom/google/t/b/c/a/b;->e:Z

    .line 110
    return-void
.end method

.method public static a(Lcom/google/t/b/c/a/a;)Lcom/google/t/b/c/a/b;
    .locals 2

    .prologue
    .line 38
    sget-object v0, Lcom/google/t/b/c/a/b;->a:Lcom/google/t/b/c/a/b;

    iget-boolean v0, v0, Lcom/google/t/b/c/a/b;->e:Z

    if-eqz v0, :cond_0

    .line 39
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_0

    const-string v0, "GlsDownloader"

    const-string v1, "Reconfiguring already configured instance."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    :cond_0
    sget-object v0, Lcom/google/t/b/c/a/b;->a:Lcom/google/t/b/c/a/b;

    iput-object p0, v0, Lcom/google/t/b/c/a/b;->b:Lcom/google/t/b/c/a/a;

    .line 43
    sget-object v0, Lcom/google/t/b/c/a/b;->a:Lcom/google/t/b/c/a/b;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/t/b/c/a/b;->e:Z

    .line 44
    sget-object v0, Lcom/google/t/b/c/a/b;->a:Lcom/google/t/b/c/a/b;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/google/t/b/c/a/b;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/t/b/c/a/b;->b:Lcom/google/t/b/c/a/a;

    iget-wide v2, p0, Lcom/google/t/b/c/a/b;->d:J

    invoke-interface {v0, v2, v3}, Lcom/google/t/b/c/a/a;->a(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/google/t/b/c/a/b;->b:Lcom/google/t/b/c/a/a;

    new-instance v1, Lcom/google/p/a/b/b/a;

    sget-object v2, Lcom/google/android/location/m/a;->bH:Lcom/google/p/a/b/b/c;

    invoke-direct {v1, v2}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    invoke-interface {v0, v1}, Lcom/google/t/b/c/a/a;->b(Lcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lcom/google/t/b/c/a/b;->b:Lcom/google/t/b/c/a/a;

    invoke-interface {v1, v0}, Lcom/google/t/b/c/a/a;->c(Lcom/google/p/a/b/b/a;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    new-instance v1, Lcom/google/p/a/b/b/a;

    sget-object v2, Lcom/google/android/location/m/a;->Q:Lcom/google/p/a/b/b/c;

    invoke-direct {v1, v2}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 69
    const/4 v2, 0x4

    new-instance v3, Lcom/google/p/a/b/b/a;

    sget-object v4, Lcom/google/android/location/m/a;->F:Lcom/google/p/a/b/b/c;

    invoke-direct {v3, v4}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    const/16 v4, 0x11

    invoke-virtual {v3, v4, v0}, Lcom/google/p/a/b/b/a;->b(ILcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/t/b/c/a/b;->c:Z

    .line 73
    iget-object v0, p0, Lcom/google/t/b/c/a/b;->b:Lcom/google/t/b/c/a/a;

    invoke-interface {v0}, Lcom/google/t/b/c/a/a;->s()Lcom/google/android/location/j/f;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, p0}, Lcom/google/android/location/j/f;->a(Lcom/google/p/a/b/b/a;Lcom/google/android/location/o/n;Lcom/google/android/location/j/g;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/location/j/b;Lcom/google/p/a/b/b/a;)V
    .locals 6

    .prologue
    const/16 v5, 0xb

    const/4 v4, 0x2

    const/4 v0, 0x0

    .line 82
    iput-boolean v0, p0, Lcom/google/t/b/c/a/b;->c:Z

    .line 83
    iget-object v1, p0, Lcom/google/t/b/c/a/b;->b:Lcom/google/t/b/c/a/a;

    invoke-interface {v1}, Lcom/google/t/b/c/a/a;->r()Lcom/google/android/location/j/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/t/b/c/a/b;->d:J

    .line 86
    if-eqz p2, :cond_0

    sget-object v1, Lcom/google/android/location/m/a;->T:Lcom/google/p/a/b/b/c;

    invoke-virtual {p2}, Lcom/google/p/a/b/b/a;->c()Lcom/google/p/a/b/b/c;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/p/a/b/b/c;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 105
    :cond_0
    return-void

    .line 91
    :cond_1
    invoke-virtual {p2, v4}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v1

    .line 92
    :goto_0
    if-ge v0, v1, :cond_0

    .line 93
    invoke-virtual {p2, v4, v0}, Lcom/google/p/a/b/b/a;->d(II)Lcom/google/p/a/b/b/a;

    move-result-object v2

    .line 96
    invoke-virtual {v2, v5}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 97
    invoke-virtual {v2, v5}, Lcom/google/p/a/b/b/a;->f(I)Lcom/google/p/a/b/b/a;

    move-result-object v2

    .line 100
    iget-object v3, p0, Lcom/google/t/b/c/a/b;->b:Lcom/google/t/b/c/a/a;

    invoke-interface {v3, v2}, Lcom/google/t/b/c/a/a;->a(Lcom/google/p/a/b/b/a;)V

    .line 92
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 102
    :cond_3
    sget-boolean v2, Lcom/google/android/location/i/a;->d:Z

    if-eqz v2, :cond_2

    const-string v2, "GlsDownloader"

    const-string v3, "Reply element does not contain valid response element."

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
