.class final Lcom/google/t/b/c/a/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/google/t/b/c/a/u;

.field final synthetic c:Lcom/google/t/b/c/a/k;


# direct methods
.method constructor <init>(Lcom/google/t/b/c/a/k;ZLcom/google/t/b/c/a/u;)V
    .locals 0

    .prologue
    .line 427
    iput-object p1, p0, Lcom/google/t/b/c/a/q;->c:Lcom/google/t/b/c/a/k;

    iput-boolean p2, p0, Lcom/google/t/b/c/a/q;->a:Z

    iput-object p3, p0, Lcom/google/t/b/c/a/q;->b:Lcom/google/t/b/c/a/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 430
    iget-boolean v0, p0, Lcom/google/t/b/c/a/q;->a:Z

    if-nez v0, :cond_0

    .line 432
    iget-object v0, p0, Lcom/google/t/b/c/a/q;->c:Lcom/google/t/b/c/a/k;

    iget-object v0, v0, Lcom/google/t/b/c/a/k;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/t/b/c/a/k;->a(Ljava/lang/String;)V

    .line 463
    :goto_0
    return-void

    .line 437
    :cond_0
    iget-object v0, p0, Lcom/google/t/b/c/a/q;->c:Lcom/google/t/b/c/a/k;

    iget-object v0, v0, Lcom/google/t/b/c/a/k;->k:Lcom/google/t/b/c/a/t;

    iget-object v1, p0, Lcom/google/t/b/c/a/q;->b:Lcom/google/t/b/c/a/u;

    invoke-interface {v0, v1}, Lcom/google/t/b/c/a/t;->b(Lcom/google/t/b/c/a/u;)V

    .line 440
    iget-object v0, p0, Lcom/google/t/b/c/a/q;->c:Lcom/google/t/b/c/a/k;

    iget-object v1, p0, Lcom/google/t/b/c/a/q;->c:Lcom/google/t/b/c/a/k;

    iget-object v2, p0, Lcom/google/t/b/c/a/q;->c:Lcom/google/t/b/c/a/k;

    iget v2, v2, Lcom/google/t/b/c/a/k;->e:I

    invoke-virtual {v1, v2}, Lcom/google/t/b/c/a/k;->a(I)I

    move-result v1

    iput v1, v0, Lcom/google/t/b/c/a/k;->e:I

    .line 441
    iget-object v0, p0, Lcom/google/t/b/c/a/q;->c:Lcom/google/t/b/c/a/k;

    iget v0, v0, Lcom/google/t/b/c/a/k;->e:I

    if-nez v0, :cond_4

    .line 443
    iget-object v0, p0, Lcom/google/t/b/c/a/q;->c:Lcom/google/t/b/c/a/k;

    iget-object v0, v0, Lcom/google/t/b/c/a/k;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 445
    if-nez v1, :cond_1

    .line 447
    iget-object v0, p0, Lcom/google/t/b/c/a/q;->c:Lcom/google/t/b/c/a/k;

    iget-object v0, v0, Lcom/google/t/b/c/a/k;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/t/b/c/a/k;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 451
    :cond_1
    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 452
    iget-object v4, p0, Lcom/google/t/b/c/a/q;->c:Lcom/google/t/b/c/a/k;

    iget-object v4, v4, Lcom/google/t/b/c/a/k;->a:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 453
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/t/b/c/a/k;->a(Ljava/lang/String;)V

    .line 451
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 457
    :cond_3
    iget-object v0, p0, Lcom/google/t/b/c/a/q;->c:Lcom/google/t/b/c/a/k;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/t/b/c/a/k;->j:Z

    goto :goto_0

    .line 462
    :cond_4
    iget-object v1, p0, Lcom/google/t/b/c/a/q;->c:Lcom/google/t/b/c/a/k;

    iget-object v0, p0, Lcom/google/t/b/c/a/q;->c:Lcom/google/t/b/c/a/k;

    iget v2, v0, Lcom/google/t/b/c/a/k;->e:I

    iget-object v0, p0, Lcom/google/t/b/c/a/q;->c:Lcom/google/t/b/c/a/k;

    iget-object v0, v0, Lcom/google/t/b/c/a/k;->a:Ljava/util/List;

    iget-object v3, p0, Lcom/google/t/b/c/a/q;->c:Lcom/google/t/b/c/a/k;

    iget v3, v3, Lcom/google/t/b/c/a/k;->e:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v1, v2, v0}, Lcom/google/t/b/c/a/k;->a(ILjava/io/File;)V

    goto :goto_0
.end method
