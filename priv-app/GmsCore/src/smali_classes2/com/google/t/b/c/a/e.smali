.class final Lcom/google/t/b/c/a/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/e/ak;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/io/DataInput;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 45
    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v0

    if-ltz v0, :cond_0

    const v1, 0x19000

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Byte array length is negative or too long."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const v1, 0x77073096

    xor-int/2addr v1, v0

    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v2

    if-eq v1, v2, :cond_2

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Byte array length does not pass redundancy check."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/t/b/c/a/c;->a()[B

    move-result-object v0

    :cond_3
    return-object v0

    :cond_4
    new-array v0, v0, [B

    invoke-interface {p1, v0}, Ljava/io/DataInput;->readFully([B)V

    new-instance v1, Ljava/util/zip/Adler32;

    invoke-direct {v1}, Ljava/util/zip/Adler32;-><init>()V

    invoke-virtual {v1, v0}, Ljava/util/zip/Adler32;->update([B)V

    invoke-virtual {v1}, Ljava/util/zip/Adler32;->getValue()J

    move-result-wide v2

    long-to-int v1, v2

    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v2

    if-eq v1, v2, :cond_3

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Checksum failed."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/io/DataOutput;)V
    .locals 2

    .prologue
    .line 45
    check-cast p1, [B

    if-nez p1, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Input byte array is empty."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    array-length v0, p1

    const v1, 0x19000

    if-le v0, v1, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Input byte array is too long."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-interface {p2, v0}, Ljava/io/DataOutput;->writeInt(I)V

    const v1, 0x77073096

    xor-int/2addr v1, v0

    invoke-interface {p2, v1}, Ljava/io/DataOutput;->writeInt(I)V

    if-eqz v0, :cond_2

    invoke-interface {p2, p1}, Ljava/io/DataOutput;->write([B)V

    new-instance v0, Ljava/util/zip/Adler32;

    invoke-direct {v0}, Ljava/util/zip/Adler32;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/zip/Adler32;->update([B)V

    invoke-virtual {v0}, Ljava/util/zip/Adler32;->getValue()J

    move-result-wide v0

    long-to-int v0, v0

    invoke-interface {p2, v0}, Ljava/io/DataOutput;->writeInt(I)V

    :cond_2
    return-void
.end method
