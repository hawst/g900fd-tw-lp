.class public final Lcom/google/t/b/b/a/k;
.super Lcom/google/t/b/b/a/a;
.source "SourceFile"


# static fields
.field private static final g:Lcom/google/t/b/b/a/k;

.field private static final h:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/t/b/b/a/k;

    invoke-direct {v0}, Lcom/google/t/b/b/a/k;-><init>()V

    sput-object v0, Lcom/google/t/b/b/a/k;->g:Lcom/google/t/b/b/a/k;

    .line 25
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lcom/google/t/b/b/a/k;->h:[B

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/google/t/b/b/a/a;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/t/b/b/a/b;)Lcom/google/t/b/b/a/k;
    .locals 3

    .prologue
    const/16 v2, 0x200

    .line 42
    sget-object v0, Lcom/google/t/b/b/a/k;->g:Lcom/google/t/b/b/a/k;

    const/16 v1, 0xf

    invoke-virtual {v0, p0, v2, v1, v2}, Lcom/google/t/b/b/a/k;->a(Lcom/google/t/b/b/a/b;III)V

    .line 44
    sget-object v0, Lcom/google/t/b/b/a/k;->g:Lcom/google/t/b/b/a/k;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/p/a/b/b/a;)V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v5, 0x3

    const/4 v0, 0x0

    .line 84
    invoke-virtual {p1, v5}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v2

    .line 85
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "PredictiveQuadtreeCacheManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " USK entries."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move v1, v0

    .line 86
    :goto_0
    if-ge v1, v2, :cond_1

    .line 87
    invoke-virtual {p1, v5, v1}, Lcom/google/p/a/b/b/a;->d(II)Lcom/google/p/a/b/b/a;

    move-result-object v3

    invoke-static {v3}, Lcom/google/t/b/b/a/p;->a(Lcom/google/p/a/b/b/a;)Lcom/google/t/b/b/a/p;

    move-result-object v3

    .line 89
    sget-object v4, Lcom/google/t/b/b/a/k;->h:[B

    invoke-virtual {p0, v3, v4}, Lcom/google/t/b/b/a/k;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 86
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 92
    :cond_1
    invoke-virtual {p1, v8}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v1

    .line 93
    sget-boolean v2, Lcom/google/android/location/i/a;->b:Z

    if-eqz v2, :cond_2

    const-string v2, "PredictiveQuadtreeCacheManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " PQ entries."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :cond_2
    :goto_1
    if-ge v0, v1, :cond_3

    .line 95
    invoke-virtual {p1, v8, v0}, Lcom/google/p/a/b/b/a;->a(II)[B

    move-result-object v2

    .line 97
    :try_start_0
    invoke-static {v2}, Lcom/google/t/b/b/a/l;->a([B)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Unknown protocol version on getQuadtreeVersion."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 104
    :catch_0
    move-exception v2

    iget v2, p0, Lcom/google/t/b/b/a/k;->e:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/t/b/b/a/k;->e:I

    .line 105
    iget-object v2, p0, Lcom/google/t/b/b/a/k;->a:Lcom/google/t/b/b/a/b;

    invoke-virtual {v2}, Lcom/google/t/b/b/a/b;->x()V

    .line 94
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 97
    :pswitch_0
    :try_start_1
    invoke-static {v2}, Lcom/google/t/b/b/a/m;->c([B)I

    move-result v3

    .line 98
    invoke-static {v2}, Lcom/google/t/b/b/a/l;->b([B)J

    move-result-wide v4

    .line 99
    invoke-static {v2}, Lcom/google/t/b/b/a/l;->c([B)I

    move-result v6

    .line 100
    new-instance v7, Lcom/google/t/b/b/a/p;

    invoke-direct {v7, v3, v4, v5, v6}, Lcom/google/t/b/b/a/p;-><init>(IJI)V

    .line 101
    invoke-virtual {p0, v7, v2}, Lcom/google/t/b/b/a/k;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 108
    :catch_1
    move-exception v2

    iget v2, p0, Lcom/google/t/b/b/a/k;->f:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/t/b/b/a/k;->f:I

    .line 109
    iget-object v2, p0, Lcom/google/t/b/b/a/k;->a:Lcom/google/t/b/b/a/b;

    invoke-virtual {v2}, Lcom/google/t/b/b/a/b;->x()V

    goto :goto_2

    .line 112
    :cond_3
    return-void

    .line 97
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Lcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;
    .locals 3

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/t/b/b/a/a;->d:Lcom/google/t/b/c/a/u;

    const/16 v1, 0x80

    invoke-static {v0, v1}, Lcom/google/t/b/b/a/a;->a(Lcom/google/t/b/c/a/u;I)Ljava/util/Iterator;

    move-result-object v1

    .line 118
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    const/4 v2, 0x2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/t/b/b/a/p;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/p;->a()Lcom/google/p/a/b/b/a;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/p/a/b/b/a;->a(ILcom/google/p/a/b/b/a;)V

    goto :goto_0

    .line 121
    :cond_0
    return-object p1
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 49
    const/16 v0, 0x200

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 54
    const/16 v0, 0x80

    return v0
.end method

.method public final o()I
    .locals 1

    .prologue
    .line 59
    const/16 v0, 0x100

    return v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    const-string v0, "pqc"

    return-object v0
.end method

.method public final q()Lcom/google/android/location/e/ak;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/google/t/b/b/a/p;->d:Lcom/google/android/location/e/ak;

    return-object v0
.end method

.method public final r()Lcom/google/android/location/e/ak;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/google/t/b/c/a/c;->b:Lcom/google/android/location/e/ak;

    return-object v0
.end method

.method public final bridge synthetic s()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/google/t/b/b/a/k;->h:[B

    return-object v0
.end method
