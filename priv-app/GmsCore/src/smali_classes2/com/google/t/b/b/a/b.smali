.class public final Lcom/google/t/b/b/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/t/b/c/a/a;


# static fields
.field private static final d:Lcom/google/t/b/b/a/b;


# instance fields
.field a:Lcom/google/t/b/b/a/a;

.field b:Lcom/google/t/b/b/a/a;

.field c:Lcom/google/t/b/b/a/a;

.field private e:Z

.field private f:Lcom/google/android/location/j/b;

.field private g:Lcom/google/android/location/j/e;

.field private h:Lcom/google/android/location/j/j;

.field private i:Lcom/google/android/location/j/f;

.field private j:Lcom/google/android/location/e/a;

.field private k:Lcom/google/t/b/c/a/b;

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:J

.field private q:J

.field private r:J

.field private s:Lcom/google/p/a/b/b/a;

.field private t:I

.field private u:Lcom/google/t/b/b/a/h;

.field private v:Lcom/google/t/b/b/a/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/google/t/b/b/a/b;

    invoke-direct {v0}, Lcom/google/t/b/b/a/b;-><init>()V

    sput-object v0, Lcom/google/t/b/b/a/b;->d:Lcom/google/t/b/b/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0xbb8

    .line 539
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/t/b/b/a/b;->e:Z

    .line 63
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/t/b/b/a/b;->l:I

    .line 64
    iput v1, p0, Lcom/google/t/b/b/a/b;->m:I

    .line 65
    const v0, 0x2bf20

    iput v0, p0, Lcom/google/t/b/b/a/b;->n:I

    .line 66
    iput v1, p0, Lcom/google/t/b/b/a/b;->o:I

    .line 69
    const-wide/32 v0, 0x5265c00

    iput-wide v0, p0, Lcom/google/t/b/b/a/b;->p:J

    .line 70
    const-wide/32 v0, 0x240c8400

    iput-wide v0, p0, Lcom/google/t/b/b/a/b;->q:J

    .line 71
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/t/b/b/a/b;->r:J

    .line 78
    new-instance v0, Lcom/google/t/b/b/a/h;

    invoke-direct {v0}, Lcom/google/t/b/b/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/t/b/b/a/b;->u:Lcom/google/t/b/b/a/h;

    .line 539
    return-void
.end method

.method public static a(Lcom/google/android/location/j/b;Lcom/google/android/location/j/e;Lcom/google/android/location/j/j;Lcom/google/android/location/j/f;[B)Lcom/google/t/b/b/a/b;
    .locals 2

    .prologue
    .line 118
    sget-object v0, Lcom/google/t/b/b/a/b;->d:Lcom/google/t/b/b/a/b;

    iget-boolean v0, v0, Lcom/google/t/b/b/a/b;->e:Z

    if-eqz v0, :cond_0

    .line 119
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_0

    const-string v0, "GpwleDirector"

    const-string v1, "Reconfiguring already configured instance."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :cond_0
    sget-object v0, Lcom/google/t/b/b/a/b;->d:Lcom/google/t/b/b/a/b;

    iput-object p0, v0, Lcom/google/t/b/b/a/b;->f:Lcom/google/android/location/j/b;

    .line 123
    sget-object v0, Lcom/google/t/b/b/a/b;->d:Lcom/google/t/b/b/a/b;

    iput-object p1, v0, Lcom/google/t/b/b/a/b;->g:Lcom/google/android/location/j/e;

    .line 124
    sget-object v0, Lcom/google/t/b/b/a/b;->d:Lcom/google/t/b/b/a/b;

    iput-object p2, v0, Lcom/google/t/b/b/a/b;->h:Lcom/google/android/location/j/j;

    .line 125
    sget-object v0, Lcom/google/t/b/b/a/b;->d:Lcom/google/t/b/b/a/b;

    iput-object p3, v0, Lcom/google/t/b/b/a/b;->i:Lcom/google/android/location/j/f;

    .line 127
    sget-object v0, Lcom/google/t/b/b/a/b;->d:Lcom/google/t/b/b/a/b;

    const/4 v1, 0x0

    invoke-static {p4, v1}, Lcom/google/android/location/e/a;->a([BLcom/google/android/location/o/a/c;)Lcom/google/android/location/e/a;

    move-result-object v1

    iput-object v1, v0, Lcom/google/t/b/b/a/b;->j:Lcom/google/android/location/e/a;

    .line 129
    sget-object v0, Lcom/google/t/b/b/a/b;->d:Lcom/google/t/b/b/a/b;

    sget-object v1, Lcom/google/t/b/b/a/b;->d:Lcom/google/t/b/b/a/b;

    invoke-static {v1}, Lcom/google/t/b/b/a/o;->a(Lcom/google/t/b/b/a/b;)Lcom/google/t/b/b/a/o;

    move-result-object v1

    iput-object v1, v0, Lcom/google/t/b/b/a/b;->a:Lcom/google/t/b/b/a/a;

    .line 131
    sget-object v0, Lcom/google/t/b/b/a/b;->d:Lcom/google/t/b/b/a/b;

    sget-object v1, Lcom/google/t/b/b/a/b;->d:Lcom/google/t/b/b/a/b;

    invoke-static {v1}, Lcom/google/t/b/b/a/r;->a(Lcom/google/t/b/b/a/b;)Lcom/google/t/b/b/a/r;

    move-result-object v1

    iput-object v1, v0, Lcom/google/t/b/b/a/b;->b:Lcom/google/t/b/b/a/a;

    .line 132
    sget-object v0, Lcom/google/t/b/b/a/b;->d:Lcom/google/t/b/b/a/b;

    sget-object v1, Lcom/google/t/b/b/a/b;->d:Lcom/google/t/b/b/a/b;

    invoke-static {v1}, Lcom/google/t/b/b/a/k;->a(Lcom/google/t/b/b/a/b;)Lcom/google/t/b/b/a/k;

    move-result-object v1

    iput-object v1, v0, Lcom/google/t/b/b/a/b;->c:Lcom/google/t/b/b/a/a;

    .line 134
    sget-object v0, Lcom/google/t/b/b/a/b;->d:Lcom/google/t/b/b/a/b;

    sget-object v1, Lcom/google/t/b/b/a/b;->d:Lcom/google/t/b/b/a/b;

    invoke-static {v1}, Lcom/google/t/b/c/a/b;->a(Lcom/google/t/b/c/a/a;)Lcom/google/t/b/c/a/b;

    move-result-object v1

    iput-object v1, v0, Lcom/google/t/b/b/a/b;->k:Lcom/google/t/b/c/a/b;

    .line 135
    sget-object v0, Lcom/google/t/b/b/a/b;->d:Lcom/google/t/b/b/a/b;

    const/4 v1, -0x1

    iput v1, v0, Lcom/google/t/b/b/a/b;->t:I

    .line 136
    sget-object v0, Lcom/google/t/b/b/a/b;->d:Lcom/google/t/b/b/a/b;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/t/b/b/a/b;->e:Z

    .line 138
    sget-object v0, Lcom/google/t/b/b/a/b;->d:Lcom/google/t/b/b/a/b;

    return-object v0
.end method

.method private e(Lcom/google/p/a/b/b/a;)V
    .locals 7

    .prologue
    const/16 v6, 0x47

    const/16 v5, 0x3e

    const/16 v4, 0x3d

    const/16 v3, 0x3c

    const/4 v2, 0x1

    .line 496
    :try_start_0
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->s:Lcom/google/p/a/b/b/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/t/b/b/a/b;->s:Lcom/google/p/a/b/b/a;

    invoke-virtual {v0}, Lcom/google/p/a/b/b/a;->f()[B

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/p/a/b/b/a;->f()[B

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_1

    .line 534
    :cond_0
    :goto_0
    return-void

    .line 501
    :catch_0
    move-exception v0

    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_0

    const-string v0, "GpwleDirector"

    const-string v1, "Client params ill formed."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 504
    :cond_1
    iput-object p1, p0, Lcom/google/t/b/b/a/b;->s:Lcom/google/p/a/b/b/a;

    .line 507
    invoke-virtual {p1, v2}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 508
    invoke-virtual {p1, v2}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/b;->t:I

    .line 510
    :cond_2
    invoke-virtual {p1, v3}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 511
    invoke-virtual {p1, v3}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/b;->l:I

    .line 514
    :cond_3
    invoke-virtual {p1, v4}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 515
    invoke-virtual {p1, v4}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/b;->m:I

    .line 518
    :cond_4
    invoke-virtual {p1, v5}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 519
    invoke-virtual {p1, v5}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/b;->n:I

    .line 522
    :cond_5
    invoke-virtual {p1, v6}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 523
    invoke-virtual {p1, v6}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/t/b/b/a/b;->p:J

    .line 526
    :cond_6
    const/16 v0, 0x48

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 527
    const/16 v0, 0x48

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/t/b/b/a/b;->q:J

    .line 531
    :cond_7
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->v:Lcom/google/t/b/b/a/c;

    if-eqz v0, :cond_0

    .line 532
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->v:Lcom/google/t/b/b/a/c;

    invoke-interface {v0, p1}, Lcom/google/t/b/b/a/c;->a(Lcom/google/p/a/b/b/a;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->b:Lcom/google/t/b/b/a/a;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/a;->g()I

    move-result v0

    return v0
.end method

.method public final a(Ljava/util/Set;)Lcom/google/t/b/b/a/h;
    .locals 6

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->u:Lcom/google/t/b/b/a/h;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/t/b/b/a/h;->a(I)V

    .line 293
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->u:Lcom/google/t/b/b/a/h;

    const/4 v1, 0x0

    iput v1, v0, Lcom/google/t/b/b/a/h;->b:I

    .line 294
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/t/b/b/a/p;

    .line 295
    iget-object v1, p0, Lcom/google/t/b/b/a/b;->c:Lcom/google/t/b/b/a/a;

    invoke-virtual {v1, v0}, Lcom/google/t/b/b/a/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    .line 297
    if-nez v1, :cond_1

    .line 300
    iget-object v1, p0, Lcom/google/t/b/b/a/b;->c:Lcom/google/t/b/b/a/a;

    invoke-virtual {v1, v0}, Lcom/google/t/b/b/a/a;->b(Ljava/lang/Object;)V

    goto :goto_0

    .line 301
    :cond_1
    array-length v0, v1

    if-eqz v0, :cond_0

    .line 304
    :try_start_0
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->u:Lcom/google/t/b/b/a/h;

    iget-object v0, v0, Lcom/google/t/b/b/a/h;->a:[Lcom/google/t/b/b/a/j;

    iget-object v3, p0, Lcom/google/t/b/b/a/b;->u:Lcom/google/t/b/b/a/h;

    iget v3, v3, Lcom/google/t/b/b/a/h;->b:I

    aget-object v0, v0, v3

    .line 305
    invoke-static {v1}, Lcom/google/t/b/b/a/l;->b([B)J

    move-result-wide v4

    iput-wide v4, v0, Lcom/google/t/b/b/a/j;->a:J

    .line 306
    iput-object v1, v0, Lcom/google/t/b/b/a/j;->b:[B

    .line 307
    const-wide/high16 v4, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    iput-wide v4, v0, Lcom/google/t/b/b/a/j;->c:D

    .line 308
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->u:Lcom/google/t/b/b/a/h;

    iget v1, v0, Lcom/google/t/b/b/a/h;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/t/b/b/a/h;->b:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 309
    :catch_0
    move-exception v0

    .line 310
    sget-boolean v1, Lcom/google/android/location/i/a;->e:Z

    if-eqz v1, :cond_0

    const-string v1, "GpwleDirector"

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 311
    :catch_1
    move-exception v0

    .line 312
    sget-boolean v1, Lcom/google/android/location/i/a;->e:Z

    if-eqz v1, :cond_0

    const-string v1, "GpwleDirector"

    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 316
    :cond_2
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->u:Lcom/google/t/b/b/a/h;

    return-object v0
.end method

.method public final a(Lcom/google/t/b/b/a/u;)Ljava/util/Set;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 250
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 253
    iget v5, p1, Lcom/google/t/b/b/a/u;->b:I

    move v3, v2

    .line 254
    :goto_0
    if-ge v3, v5, :cond_2

    .line 255
    new-instance v1, Lcom/google/t/b/b/a/e;

    iget v0, p0, Lcom/google/t/b/b/a/b;->t:I

    iget-object v6, p1, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v6, v6, v3

    iget-wide v6, v6, Lcom/google/t/b/b/a/x;->a:J

    invoke-direct {v1, v0, v6, v7}, Lcom/google/t/b/b/a/e;-><init>(IJ)V

    .line 257
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->b:Lcom/google/t/b/b/a/a;

    invoke-virtual {v0, v1}, Lcom/google/t/b/b/a/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 258
    if-nez v0, :cond_1

    .line 261
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->b:Lcom/google/t/b/b/a/a;

    invoke-virtual {v0, v1}, Lcom/google/t/b/b/a/a;->b(Ljava/lang/Object;)V

    .line 254
    :cond_0
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 262
    :cond_1
    array-length v1, v0

    if-eqz v1, :cond_0

    .line 265
    :try_start_0
    invoke-static {v0}, Lcom/google/t/b/b/a/t;->a([B)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown protocol version on getSpatialKeyCount."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 274
    :catch_0
    move-exception v0

    .line 275
    sget-boolean v1, Lcom/google/android/location/i/a;->e:Z

    if-eqz v1, :cond_0

    const-string v1, "GpwleDirector"

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 265
    :pswitch_0
    :try_start_1
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-byte v1, v0, v1

    and-int/lit16 v6, v1, 0xff

    move v1, v2

    .line 266
    :goto_2
    if-ge v1, v6, :cond_0

    .line 267
    invoke-static {v0}, Lcom/google/t/b/b/a/t;->a([B)I

    move-result v7

    packed-switch v7, :pswitch_data_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown protocol version on getQuadtreeVersion."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    .line 276
    :catch_1
    move-exception v0

    .line 277
    sget-boolean v1, Lcom/google/android/location/i/a;->e:Z

    if-eqz v1, :cond_0

    const-string v1, "GpwleDirector"

    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 267
    :pswitch_1
    :try_start_2
    array-length v7, v0

    mul-int/lit8 v8, v1, 0x10

    sub-int/2addr v7, v8

    add-int/lit8 v7, v7, -0x5

    invoke-static {v0, v7}, Lcom/google/t/b/b/a/g;->b([BI)I

    move-result v7

    .line 269
    invoke-static {v0}, Lcom/google/t/b/b/a/t;->a([B)I

    move-result v8

    packed-switch v8, :pswitch_data_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown protocol version on getS2CellId."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_2
    array-length v8, v0

    mul-int/lit8 v9, v1, 0x10

    sub-int/2addr v8, v9

    add-int/lit8 v8, v8, -0xd

    invoke-static {v0, v8}, Lcom/google/t/b/b/a/g;->c([BI)J

    move-result-wide v8

    .line 270
    invoke-static {v0}, Lcom/google/t/b/b/a/t;->a([B)I

    move-result v10

    packed-switch v10, :pswitch_data_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown protocol version on getLayerId."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_3
    array-length v10, v0

    mul-int/lit8 v11, v1, 0x10

    sub-int/2addr v10, v11

    add-int/lit8 v10, v10, -0x11

    invoke-static {v0, v10}, Lcom/google/t/b/b/a/g;->b([BI)I

    move-result v10

    .line 271
    new-instance v11, Lcom/google/t/b/b/a/p;

    invoke-direct {v11, v7, v8, v9, v10}, Lcom/google/t/b/b/a/p;-><init>(IJI)V

    .line 272
    invoke-interface {v4, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1

    .line 266
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 281
    :cond_2
    return-object v4

    .line 265
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 267
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    .line 269
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch

    .line 270
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/google/p/a/b/b/a;)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x6

    .line 412
    if-eqz p1, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 446
    :cond_0
    :goto_0
    return-void

    .line 415
    :cond_1
    invoke-virtual {p1, v1}, Lcom/google/p/a/b/b/a;->f(I)Lcom/google/p/a/b/b/a;

    move-result-object v0

    .line 416
    invoke-virtual {v0, v2}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 419
    invoke-virtual {v0, v2}, Lcom/google/p/a/b/b/a;->f(I)Lcom/google/p/a/b/b/a;

    move-result-object v0

    .line 423
    invoke-virtual {v0, v2}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 424
    invoke-virtual {v0, v2}, Lcom/google/p/a/b/b/a;->f(I)Lcom/google/p/a/b/b/a;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/t/b/b/a/b;->e(Lcom/google/p/a/b/b/a;)V

    .line 427
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v1

    if-gtz v1, :cond_2

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v1

    if-lez v1, :cond_4

    .line 429
    :cond_2
    iget v1, p0, Lcom/google/t/b/b/a/b;->m:I

    iput v1, p0, Lcom/google/t/b/b/a/b;->o:I

    .line 431
    iget-object v1, p0, Lcom/google/t/b/b/a/b;->b:Lcom/google/t/b/b/a/a;

    iget-object v1, v1, Lcom/google/t/b/b/a/a;->c:Lcom/google/t/b/c/a/k;

    iget-boolean v1, v1, Lcom/google/t/b/c/a/k;->j:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/t/b/b/a/b;->c:Lcom/google/t/b/b/a/a;

    iget-object v1, v1, Lcom/google/t/b/b/a/a;->c:Lcom/google/t/b/c/a/k;

    iget-boolean v1, v1, Lcom/google/t/b/c/a/k;->j:Z

    if-nez v1, :cond_4

    .line 433
    :cond_3
    iget v1, p0, Lcom/google/t/b/b/a/b;->l:I

    iget v2, p0, Lcom/google/t/b/b/a/b;->o:I

    mul-int/2addr v1, v2

    iget v2, p0, Lcom/google/t/b/b/a/b;->n:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/google/t/b/b/a/b;->o:I

    .line 443
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/google/t/b/b/a/b;->a:Lcom/google/t/b/b/a/a;

    invoke-virtual {v1, v0}, Lcom/google/t/b/b/a/a;->a(Lcom/google/p/a/b/b/a;)V

    .line 444
    iget-object v1, p0, Lcom/google/t/b/b/a/b;->b:Lcom/google/t/b/b/a/a;

    invoke-virtual {v1, v0}, Lcom/google/t/b/b/a/a;->a(Lcom/google/p/a/b/b/a;)V

    .line 445
    iget-object v1, p0, Lcom/google/t/b/b/a/b;->c:Lcom/google/t/b/b/a/a;

    invoke-virtual {v1, v0}, Lcom/google/t/b/b/a/a;->a(Lcom/google/p/a/b/b/a;)V

    goto :goto_0

    .line 439
    :cond_5
    sget-boolean v1, Lcom/google/android/location/i/a;->d:Z

    if-eqz v1, :cond_4

    const-string v1, "GpwleDirector"

    const-string v2, "Missing server defined client parameters."

    invoke-static {v1, v2}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/t/b/b/a/c;)V
    .locals 1

    .prologue
    .line 94
    iput-object p1, p0, Lcom/google/t/b/b/a/b;->v:Lcom/google/t/b/b/a/c;

    .line 95
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->s:Lcom/google/p/a/b/b/a;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->s:Lcom/google/p/a/b/b/a;

    invoke-interface {p1, v0}, Lcom/google/t/b/b/a/c;->a(Lcom/google/p/a/b/b/a;)V

    .line 98
    :cond_0
    return-void
.end method

.method public final a(J)Z
    .locals 5

    .prologue
    .line 461
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->f:Lcom/google/android/location/j/b;

    invoke-interface {v0}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v0

    sub-long/2addr v0, p1

    iget v2, p0, Lcom/google/t/b/b/a/b;->o:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->b:Lcom/google/t/b/b/a/a;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/a;->h()I

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;
    .locals 3

    .prologue
    .line 450
    new-instance v0, Lcom/google/p/a/b/b/a;

    sget-object v1, Lcom/google/android/location/m/a;->aN:Lcom/google/p/a/b/b/c;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 451
    iget-object v1, p0, Lcom/google/t/b/b/a/b;->b:Lcom/google/t/b/b/a/a;

    invoke-virtual {v1, v0}, Lcom/google/t/b/b/a/a;->b(Lcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;

    move-result-object v0

    .line 452
    iget-object v1, p0, Lcom/google/t/b/b/a/b;->c:Lcom/google/t/b/b/a/a;

    invoke-virtual {v1, v0}, Lcom/google/t/b/b/a/a;->b(Lcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;

    move-result-object v0

    .line 453
    new-instance v1, Lcom/google/p/a/b/b/a;

    sget-object v2, Lcom/google/android/location/m/a;->aG:Lcom/google/p/a/b/b/c;

    invoke-direct {v1, v2}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    const/4 v2, 0x6

    invoke-virtual {v1, v2, v0}, Lcom/google/p/a/b/b/a;->b(ILcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;

    move-result-object v0

    .line 455
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, Lcom/google/p/a/b/b/a;->b(ILcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;

    .line 456
    return-object p1
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->b:Lcom/google/t/b/b/a/a;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/a;->i()I

    move-result v0

    return v0
.end method

.method public final c(Lcom/google/p/a/b/b/a;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 466
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Lcom/google/p/a/b/b/a;->f(I)Lcom/google/p/a/b/b/a;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/google/p/a/b/b/a;->f(I)Lcom/google/p/a/b/b/a;

    move-result-object v1

    .line 468
    invoke-virtual {v1, v0}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v2

    .line 469
    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v1

    .line 470
    if-nez v2, :cond_0

    if-eqz v1, :cond_2

    .line 471
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 472
    iget v1, p0, Lcom/google/t/b/b/a/b;->l:I

    iget v2, p0, Lcom/google/t/b/b/a/b;->o:I

    mul-int/2addr v1, v2

    iget v2, p0, Lcom/google/t/b/b/a/b;->n:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/google/t/b/b/a/b;->o:I

    .line 475
    :cond_1
    return v0

    .line 470
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->b:Lcom/google/t/b/b/a/a;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/a;->j()I

    move-result v0

    return v0
.end method

.method public final d(Lcom/google/p/a/b/b/a;)V
    .locals 2

    .prologue
    .line 483
    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/location/m/a;->aQ:Lcom/google/p/a/b/b/c;

    invoke-virtual {p1}, Lcom/google/p/a/b/b/a;->c()Lcom/google/p/a/b/b/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/c;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 485
    :cond_0
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_1

    const-string v0, "GpwleDirector"

    const-string v1, "Received invalid client params disk result."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    :cond_1
    :goto_0
    return-void

    .line 489
    :cond_2
    iget v0, p0, Lcom/google/t/b/b/a/b;->t:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 490
    invoke-direct {p0, p1}, Lcom/google/t/b/b/a/b;->e(Lcom/google/p/a/b/b/a;)V

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->b:Lcom/google/t/b/b/a/a;

    iget-object v0, v0, Lcom/google/t/b/b/a/a;->c:Lcom/google/t/b/c/a/k;

    iget-boolean v0, v0, Lcom/google/t/b/c/a/k;->j:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->b:Lcom/google/t/b/b/a/a;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/a;->k()I

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->b:Lcom/google/t/b/b/a/a;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/a;->l()I

    move-result v0

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->c:Lcom/google/t/b/b/a/a;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/a;->g()I

    move-result v0

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->c:Lcom/google/t/b/b/a/a;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/a;->h()I

    move-result v0

    return v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->c:Lcom/google/t/b/b/a/a;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/a;->i()I

    move-result v0

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->c:Lcom/google/t/b/b/a/a;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/a;->j()I

    move-result v0

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->c:Lcom/google/t/b/b/a/a;

    iget-object v0, v0, Lcom/google/t/b/b/a/a;->c:Lcom/google/t/b/c/a/k;

    iget-boolean v0, v0, Lcom/google/t/b/c/a/k;->j:Z

    return v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->c:Lcom/google/t/b/b/a/a;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/a;->k()I

    move-result v0

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->c:Lcom/google/t/b/b/a/a;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/a;->l()I

    move-result v0

    return v0
.end method

.method public final o()V
    .locals 6

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->f:Lcom/google/android/location/j/b;

    invoke-interface {v0}, Lcom/google/android/location/j/b;->b()J

    move-result-wide v0

    .line 326
    iget-wide v2, p0, Lcom/google/t/b/b/a/b;->r:J

    sub-long v2, v0, v2

    iget-wide v4, p0, Lcom/google/t/b/b/a/b;->p:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 327
    iget-wide v2, p0, Lcom/google/t/b/b/a/b;->q:J

    sub-long v2, v0, v2

    .line 328
    iget-object v4, p0, Lcom/google/t/b/b/a/b;->a:Lcom/google/t/b/b/a/a;

    invoke-virtual {v4, v2, v3}, Lcom/google/t/b/b/a/a;->a(J)V

    .line 329
    iget-object v4, p0, Lcom/google/t/b/b/a/b;->b:Lcom/google/t/b/b/a/a;

    invoke-virtual {v4, v2, v3}, Lcom/google/t/b/b/a/a;->a(J)V

    .line 330
    iget-object v4, p0, Lcom/google/t/b/b/a/b;->c:Lcom/google/t/b/b/a/a;

    invoke-virtual {v4, v2, v3}, Lcom/google/t/b/b/a/a;->a(J)V

    .line 331
    iput-wide v0, p0, Lcom/google/t/b/b/a/b;->r:J

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->a:Lcom/google/t/b/b/a/a;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/a;->e()V

    .line 336
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->b:Lcom/google/t/b/b/a/a;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/a;->e()V

    .line 337
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->c:Lcom/google/t/b/b/a/a;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/a;->e()V

    .line 338
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->k:Lcom/google/t/b/c/a/b;

    invoke-virtual {v0}, Lcom/google/t/b/c/a/b;->a()V

    .line 339
    return-void
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->u:Lcom/google/t/b/b/a/h;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/h;->b()V

    .line 346
    return-void
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->a:Lcom/google/t/b/b/a/a;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/a;->f()V

    .line 353
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->b:Lcom/google/t/b/b/a/a;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/a;->f()V

    .line 354
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->c:Lcom/google/t/b/b/a/a;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/a;->f()V

    .line 355
    return-void
.end method

.method public final r()Lcom/google/android/location/j/b;
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->f:Lcom/google/android/location/j/b;

    return-object v0
.end method

.method public final s()Lcom/google/android/location/j/f;
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->i:Lcom/google/android/location/j/f;

    return-object v0
.end method

.method public final t()Lcom/google/android/location/j/e;
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->g:Lcom/google/android/location/j/e;

    return-object v0
.end method

.method public final u()Lcom/google/android/location/j/j;
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->h:Lcom/google/android/location/j/j;

    return-object v0
.end method

.method public final v()Lcom/google/android/location/e/a;
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/google/t/b/b/a/b;->j:Lcom/google/android/location/e/a;

    return-object v0
.end method

.method public final w()I
    .locals 1

    .prologue
    .line 398
    iget v0, p0, Lcom/google/t/b/b/a/b;->t:I

    return v0
.end method

.method public final x()V
    .locals 1

    .prologue
    .line 406
    iget v0, p0, Lcom/google/t/b/b/a/b;->n:I

    iput v0, p0, Lcom/google/t/b/b/a/b;->o:I

    .line 407
    return-void
.end method
