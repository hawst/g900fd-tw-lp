.class final Lcom/google/t/b/b/a/u;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final c:Ljava/util/Comparator;

.field private static final d:Ljava/util/Comparator;


# instance fields
.field public a:[Lcom/google/t/b/b/a/x;

.field public b:I

.field private e:Lcom/google/t/b/b/a/x;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/t/b/b/a/v;

    invoke-direct {v0}, Lcom/google/t/b/b/a/v;-><init>()V

    sput-object v0, Lcom/google/t/b/b/a/u;->c:Ljava/util/Comparator;

    .line 25
    new-instance v0, Lcom/google/t/b/b/a/w;

    invoke-direct {v0}, Lcom/google/t/b/b/a/w;-><init>()V

    sput-object v0, Lcom/google/t/b/b/a/u;->d:Ljava/util/Comparator;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-array v0, v1, [Lcom/google/t/b/b/a/x;

    iput-object v0, p0, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    .line 42
    new-instance v0, Lcom/google/t/b/b/a/x;

    invoke-direct {v0, v1}, Lcom/google/t/b/b/a/x;-><init>(B)V

    iput-object v0, p0, Lcom/google/t/b/b/a/u;->e:Lcom/google/t/b/b/a/x;

    .line 103
    return-void
.end method


# virtual methods
.method public final a(II)I
    .locals 3

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    sget-object v1, Lcom/google/t/b/b/a/u;->c:Ljava/util/Comparator;

    invoke-static {v0, p1, p2, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 94
    sub-int v0, p2, p1

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, p1

    .line 95
    sub-int v1, p2, p1

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    add-int/lit8 v2, v0, -0x1

    aget-object v1, v1, v2

    iget v1, v1, Lcom/google/t/b/b/a/x;->b:I

    iget-object v2, p0, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v0, v2, v0

    iget v0, v0, Lcom/google/t/b/b/a/x;->b:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v0, v1, v0

    iget v0, v0, Lcom/google/t/b/b/a/x;->b:I

    goto :goto_0
.end method

.method public final a(J)I
    .locals 5

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/t/b/b/a/u;->e:Lcom/google/t/b/b/a/x;

    iput-wide p1, v0, Lcom/google/t/b/b/a/x;->a:J

    .line 85
    iget-object v0, p0, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/t/b/b/a/u;->b:I

    iget-object v3, p0, Lcom/google/t/b/b/a/u;->e:Lcom/google/t/b/b/a/x;

    sget-object v4, Lcom/google/t/b/b/a/u;->d:Ljava/util/Comparator;

    invoke-static {v0, v1, v2, v3, v4}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;IILjava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    return v0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/t/b/b/a/u;->b:I

    sget-object v3, Lcom/google/t/b/b/a/u;->d:Ljava/util/Comparator;

    invoke-static {v0, v1, v2, v3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 71
    return-void
.end method

.method public final a(I)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 49
    iget-object v0, p0, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    array-length v0, v0

    if-lt v0, p1, :cond_0

    .line 64
    :goto_0
    return-void

    .line 53
    :cond_0
    new-array v4, p1, [Lcom/google/t/b/b/a/x;

    .line 55
    iget-object v5, p0, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    array-length v6, v5

    move v1, v2

    move v0, v2

    :goto_1
    if-ge v1, v6, :cond_1

    aget-object v3, v5, v1

    .line 56
    aput-object v3, v4, v0

    .line 57
    add-int/lit8 v3, v0, 0x1

    .line 55
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v3

    goto :goto_1

    .line 59
    :cond_1
    :goto_2
    if-ge v0, p1, :cond_2

    .line 60
    new-instance v1, Lcom/google/t/b/b/a/x;

    invoke-direct {v1, v2}, Lcom/google/t/b/b/a/x;-><init>(B)V

    aput-object v1, v4, v0

    .line 61
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 63
    :cond_2
    iput-object v4, p0, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/t/b/b/a/u;->b:I

    sget-object v3, Lcom/google/t/b/b/a/u;->c:Ljava/util/Comparator;

    invoke-static {v0, v1, v2, v3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 78
    return-void
.end method
