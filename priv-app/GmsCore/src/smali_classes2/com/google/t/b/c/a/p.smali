.class final Lcom/google/t/b/c/a/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/io/File;

.field final synthetic b:I

.field final synthetic c:Lcom/google/t/b/c/a/k;


# direct methods
.method constructor <init>(Lcom/google/t/b/c/a/k;Ljava/io/File;I)V
    .locals 0

    .prologue
    .line 394
    iput-object p1, p0, Lcom/google/t/b/c/a/p;->c:Lcom/google/t/b/c/a/k;

    iput-object p2, p0, Lcom/google/t/b/c/a/p;->a:Ljava/io/File;

    iput p3, p0, Lcom/google/t/b/c/a/p;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 397
    const/4 v1, 0x1

    .line 398
    new-instance v0, Lcom/google/t/b/c/a/u;

    iget-object v2, p0, Lcom/google/t/b/c/a/p;->c:Lcom/google/t/b/c/a/k;

    iget-object v2, v2, Lcom/google/t/b/c/a/k;->k:Lcom/google/t/b/c/a/t;

    iget-object v3, p0, Lcom/google/t/b/c/a/p;->c:Lcom/google/t/b/c/a/k;

    iget v3, v3, Lcom/google/t/b/c/a/k;->m:I

    invoke-direct {v0, v2, v3}, Lcom/google/t/b/c/a/u;-><init>(Lcom/google/t/b/c/a/v;I)V

    .line 402
    :try_start_0
    iget-object v2, p0, Lcom/google/t/b/c/a/p;->c:Lcom/google/t/b/c/a/k;

    iget-object v3, p0, Lcom/google/t/b/c/a/p;->a:Ljava/io/File;

    invoke-virtual {v2, v3, v0}, Lcom/google/t/b/c/a/k;->a(Ljava/io/File;Lcom/google/t/b/c/a/u;)Lcom/google/t/b/c/a/u;

    move-result-object v2

    .line 403
    iget-object v0, p0, Lcom/google/t/b/c/a/p;->c:Lcom/google/t/b/c/a/k;

    iget v3, v0, Lcom/google/t/b/c/a/k;->l:I

    iget v4, p0, Lcom/google/t/b/c/a/p;->b:I

    iget-object v0, v2, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    invoke-virtual {v0}, Lcom/google/android/location/b/bd;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/google/t/b/c/a/u;->a(Ljava/lang/Object;I)I

    move-result v0

    if-eq v0, v4, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 404
    :catch_0
    move-exception v0

    .line 406
    sget-boolean v2, Lcom/google/android/location/i/a;->e:Z

    if-eqz v2, :cond_1

    const-string v2, "TemporalDiskCache"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    :cond_1
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_2

    const-string v0, "TemporalDiskCache"

    const-string v2, "Initial read failed. Attempting chunk file purge."

    invoke-static {v0, v2}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    :cond_2
    new-instance v0, Lcom/google/t/b/c/a/u;

    iget-object v2, p0, Lcom/google/t/b/c/a/p;->c:Lcom/google/t/b/c/a/k;

    iget-object v2, v2, Lcom/google/t/b/c/a/k;->k:Lcom/google/t/b/c/a/t;

    iget-object v3, p0, Lcom/google/t/b/c/a/p;->c:Lcom/google/t/b/c/a/k;

    iget v3, v3, Lcom/google/t/b/c/a/k;->m:I

    invoke-direct {v0, v2, v3}, Lcom/google/t/b/c/a/u;-><init>(Lcom/google/t/b/c/a/v;I)V

    move-object v2, v0

    .line 413
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/google/t/b/c/a/p;->c:Lcom/google/t/b/c/a/k;

    iget-object v3, p0, Lcom/google/t/b/c/a/p;->a:Ljava/io/File;

    invoke-static {v0, v3, v2}, Lcom/google/t/b/c/a/k;->a(Lcom/google/t/b/c/a/k;Ljava/io/File;Lcom/google/t/b/c/a/u;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move v0, v1

    .line 420
    :goto_1
    iget-object v1, p0, Lcom/google/t/b/c/a/p;->c:Lcom/google/t/b/c/a/k;

    iget-object v3, v1, Lcom/google/t/b/c/a/k;->k:Lcom/google/t/b/c/a/t;

    invoke-interface {v3}, Lcom/google/t/b/c/a/t;->c()Lcom/google/android/location/j/j;

    move-result-object v3

    new-instance v4, Lcom/google/t/b/c/a/q;

    invoke-direct {v4, v1, v0, v2}, Lcom/google/t/b/c/a/q;-><init>(Lcom/google/t/b/c/a/k;ZLcom/google/t/b/c/a/u;)V

    invoke-interface {v3, v4}, Lcom/google/android/location/j/j;->a(Ljava/lang/Runnable;)Z

    .line 421
    return-void

    .line 414
    :catch_1
    move-exception v0

    .line 416
    sget-boolean v1, Lcom/google/android/location/i/a;->e:Z

    if-eqz v1, :cond_4

    const-string v1, "TemporalDiskCache"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    :cond_4
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_5

    const-string v0, "TemporalDiskCache"

    const-string v1, "Initial write failed. Initialization terminated."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method
