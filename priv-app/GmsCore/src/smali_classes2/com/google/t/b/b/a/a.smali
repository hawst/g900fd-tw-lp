.class public abstract Lcom/google/t/b/b/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/t/b/c/a/t;


# instance fields
.field protected a:Lcom/google/t/b/b/a/b;

.field b:Lcom/google/t/b/c/a/u;

.field c:Lcom/google/t/b/c/a/k;

.field d:Lcom/google/t/b/c/a/u;

.field protected e:I

.field protected f:I

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-boolean v0, p0, Lcom/google/t/b/b/a/a;->g:Z

    .line 38
    iput v0, p0, Lcom/google/t/b/b/a/a;->e:I

    .line 39
    iput v0, p0, Lcom/google/t/b/b/a/a;->f:I

    return-void
.end method

.method public static a(Lcom/google/t/b/c/a/u;I)Ljava/util/Iterator;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 211
    invoke-virtual {p0}, Lcom/google/t/b/c/a/u;->b()Ljava/util/Iterator;

    move-result-object v1

    .line 212
    iget-object v2, p0, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    invoke-virtual {v2}, Lcom/google/android/location/b/bd;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x80

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 213
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    if-ge v0, v2, :cond_0

    .line 214
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 213
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 216
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a()Lcom/google/android/location/j/b;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/t/b/b/a/a;->a:Lcom/google/t/b/b/a/b;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/b;->r()Lcom/google/android/location/j/b;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/t/b/b/a/a;->b:Lcom/google/t/b/c/a/u;

    invoke-virtual {v0, p1}, Lcom/google/t/b/c/a/u;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/t/b/b/a/a;->b:Lcom/google/t/b/c/a/u;

    invoke-virtual {v0, p1, p2}, Lcom/google/t/b/c/a/u;->a(J)V

    .line 150
    iget-object v0, p0, Lcom/google/t/b/b/a/a;->c:Lcom/google/t/b/c/a/k;

    iput-wide p1, v0, Lcom/google/t/b/c/a/k;->h:J

    iget-object v0, v0, Lcom/google/t/b/c/a/k;->i:[Z

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([ZZ)V

    .line 151
    iget-object v0, p0, Lcom/google/t/b/b/a/a;->d:Lcom/google/t/b/c/a/u;

    invoke-virtual {v0, p1, p2}, Lcom/google/t/b/c/a/u;->a(J)V

    .line 152
    return-void
.end method

.method public abstract a(Lcom/google/p/a/b/b/a;)V
.end method

.method protected final a(Lcom/google/t/b/b/a/b;III)V
    .locals 2

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/t/b/b/a/a;->g:Z

    if-eqz v0, :cond_0

    .line 48
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_0

    const-string v0, "CacheManager"

    const-string v1, "Reconfiguring already configured instance."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    :cond_0
    iput-object p1, p0, Lcom/google/t/b/b/a/a;->a:Lcom/google/t/b/b/a/b;

    .line 52
    new-instance v0, Lcom/google/t/b/c/a/u;

    invoke-direct {v0, p0, p2}, Lcom/google/t/b/c/a/u;-><init>(Lcom/google/t/b/c/a/v;I)V

    iput-object v0, p0, Lcom/google/t/b/b/a/a;->b:Lcom/google/t/b/c/a/u;

    .line 53
    new-instance v0, Lcom/google/t/b/c/a/k;

    invoke-direct {v0, p0, p3}, Lcom/google/t/b/c/a/k;-><init>(Lcom/google/t/b/c/a/t;I)V

    iput-object v0, p0, Lcom/google/t/b/b/a/a;->c:Lcom/google/t/b/c/a/k;

    .line 54
    new-instance v0, Lcom/google/t/b/c/a/u;

    invoke-direct {v0, p0, p4}, Lcom/google/t/b/c/a/u;-><init>(Lcom/google/t/b/c/a/v;I)V

    iput-object v0, p0, Lcom/google/t/b/b/a/a;->d:Lcom/google/t/b/c/a/u;

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/t/b/b/a/a;->g:Z

    .line 56
    return-void
.end method

.method public a(Lcom/google/t/b/c/a/u;)V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/t/b/b/a/a;->d:Lcom/google/t/b/c/a/u;

    invoke-virtual {v0, p1}, Lcom/google/t/b/c/a/u;->a(Lcom/google/t/b/c/a/u;)V

    .line 105
    return-void
.end method

.method public final a(Lcom/google/t/b/c/a/u;Lcom/google/t/b/c/a/u;)V
    .locals 6

    .prologue
    .line 99
    iget-object v1, p0, Lcom/google/t/b/b/a/a;->b:Lcom/google/t/b/c/a/u;

    iget-object v0, p2, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    invoke-virtual {v0}, Lcom/google/android/location/b/bd;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    iget-object v0, p1, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    invoke-virtual {v0, v3}, Lcom/google/android/location/b/bd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/ax;

    if-eqz v0, :cond_0

    iget-object v4, v1, Lcom/google/t/b/c/a/u;->b:Lcom/google/t/b/c/a/v;

    invoke-interface {v4}, Lcom/google/t/b/c/a/v;->a()Lcom/google/android/location/j/b;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/location/j/b;->b()J

    move-result-wide v4

    iput-wide v4, v0, Lcom/google/android/location/e/ax;->c:J

    iget-object v4, v1, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    invoke-virtual {v4, v3, v0}, Lcom/google/android/location/b/bd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 100
    :cond_1
    return-void
.end method

.method protected final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 220
    invoke-virtual {p0, p1, p2}, Lcom/google/t/b/b/a/a;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 221
    iget-object v0, p0, Lcom/google/t/b/b/a/a;->d:Lcom/google/t/b/c/a/u;

    invoke-virtual {v0, p1}, Lcom/google/t/b/c/a/u;->c(Ljava/lang/Object;)Lcom/google/android/location/e/ax;

    .line 222
    return-void
.end method

.method public final b()Lcom/google/android/location/j/e;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/t/b/b/a/a;->a:Lcom/google/t/b/b/a/b;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/b;->t()Lcom/google/android/location/j/e;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;
    .locals 0

    .prologue
    .line 120
    return-object p1
.end method

.method public b(Lcom/google/t/b/c/a/u;)V
    .locals 0

    .prologue
    .line 126
    return-void
.end method

.method public b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/t/b/b/a/a;->c:Lcom/google/t/b/c/a/k;

    invoke-virtual {v0, p1}, Lcom/google/t/b/c/a/k;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/google/t/b/b/a/a;->d:Lcom/google/t/b/c/a/u;

    invoke-virtual {v0, p1}, Lcom/google/t/b/c/a/u;->b(Ljava/lang/Object;)V

    .line 74
    :cond_0
    return-void
.end method

.method protected final b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/t/b/b/a/a;->b:Lcom/google/t/b/c/a/u;

    invoke-virtual {v0, p1, p2}, Lcom/google/t/b/c/a/u;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 226
    iget-object v1, p0, Lcom/google/t/b/b/a/a;->c:Lcom/google/t/b/c/a/k;

    iget-boolean v0, v1, Lcom/google/t/b/c/a/k;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, v1, Lcom/google/t/b/c/a/k;->c:Ljava/util/List;

    invoke-virtual {v1, p1}, Lcom/google/t/b/c/a/k;->b(Ljava/lang/Object;)I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/t/b/c/a/u;

    invoke-virtual {v0, p1, p2}, Lcom/google/t/b/c/a/u;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    iget-boolean v0, v1, Lcom/google/t/b/c/a/k;->j:Z

    .line 227
    return-void
.end method

.method public final c()Lcom/google/android/location/j/j;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/t/b/b/a/a;->a:Lcom/google/t/b/b/a/b;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/b;->u()Lcom/google/android/location/j/j;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/android/location/e/a;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/t/b/b/a/a;->a:Lcom/google/t/b/b/a/b;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/b;->v()Lcom/google/android/location/e/a;

    move-result-object v0

    return-object v0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 133
    iget-object v1, p0, Lcom/google/t/b/b/a/a;->c:Lcom/google/t/b/c/a/k;

    iget-boolean v0, v1, Lcom/google/t/b/c/a/k;->j:Z

    if-eqz v0, :cond_0

    iget-boolean v0, v1, Lcom/google/t/b/c/a/k;->f:Z

    if-nez v0, :cond_0

    iget-boolean v0, v1, Lcom/google/t/b/c/a/k;->g:Z

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lcom/google/t/b/c/a/k;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/t/b/c/a/k;->f:Z

    iget-object v0, v1, Lcom/google/t/b/c/a/k;->a:Ljava/util/List;

    iget v2, v1, Lcom/google/t/b/c/a/k;->e:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v1, v0}, Lcom/google/t/b/c/a/k;->a(Ljava/io/File;)V

    goto :goto_0
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/t/b/b/a/a;->b:Lcom/google/t/b/c/a/u;

    iget-object v0, v0, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    invoke-virtual {v0}, Lcom/google/android/location/b/bd;->clear()V

    .line 141
    iget-object v0, p0, Lcom/google/t/b/b/a/a;->c:Lcom/google/t/b/c/a/k;

    iget-object v1, v0, Lcom/google/t/b/c/a/k;->d:Ljava/io/File;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/t/b/c/a/k;->g:Z

    iget-object v1, v0, Lcom/google/t/b/c/a/k;->d:Ljava/io/File;

    iget-object v2, v0, Lcom/google/t/b/c/a/k;->k:Lcom/google/t/b/c/a/t;

    invoke-interface {v2}, Lcom/google/t/b/c/a/t;->b()Lcom/google/android/location/j/e;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/location/j/e;->d()Lcom/google/android/location/j/i;

    move-result-object v2

    new-instance v3, Lcom/google/t/b/c/a/r;

    invoke-direct {v3, v0, v1}, Lcom/google/t/b/c/a/r;-><init>(Lcom/google/t/b/c/a/k;Ljava/io/File;)V

    invoke-virtual {v2, v3}, Lcom/google/android/location/j/i;->execute(Ljava/lang/Runnable;)V

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/google/t/b/b/a/a;->d:Lcom/google/t/b/c/a/u;

    iget-object v0, v0, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    invoke-virtual {v0}, Lcom/google/android/location/b/bd;->clear()V

    .line 143
    return-void
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/t/b/b/a/a;->b:Lcom/google/t/b/c/a/u;

    iget-object v0, v0, Lcom/google/t/b/c/a/u;->a:Lcom/google/android/location/b/bd;

    invoke-virtual {v0}, Lcom/google/android/location/b/bd;->size()I

    move-result v0

    return v0
.end method

.method public final h()I
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/t/b/b/a/a;->c:Lcom/google/t/b/c/a/k;

    iget-boolean v1, v0, Lcom/google/t/b/c/a/k;->j:Z

    if-eqz v1, :cond_0

    iget v0, v0, Lcom/google/t/b/c/a/k;->n:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()I
    .locals 3

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/t/b/b/a/a;->c:Lcom/google/t/b/c/a/k;

    iget v1, v0, Lcom/google/t/b/c/a/k;->o:I

    const/4 v2, 0x0

    iput v2, v0, Lcom/google/t/b/c/a/k;->o:I

    return v1
.end method

.method public final j()I
    .locals 3

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/t/b/b/a/a;->c:Lcom/google/t/b/c/a/k;

    iget v1, v0, Lcom/google/t/b/c/a/k;->p:I

    const/4 v2, 0x0

    iput v2, v0, Lcom/google/t/b/c/a/k;->p:I

    return v1
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 194
    iget v0, p0, Lcom/google/t/b/b/a/a;->e:I

    .line 195
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/t/b/b/a/a;->e:I

    .line 196
    return v0
.end method

.method public final l()I
    .locals 2

    .prologue
    .line 204
    iget v0, p0, Lcom/google/t/b/b/a/a;->f:I

    .line 205
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/t/b/b/a/a;->f:I

    .line 206
    return v0
.end method
