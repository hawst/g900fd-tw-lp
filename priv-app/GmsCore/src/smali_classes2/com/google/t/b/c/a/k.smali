.class public final Lcom/google/t/b/c/a/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/List;

.field b:Ljava/util/List;

.field public c:Ljava/util/List;

.field public final d:Ljava/io/File;

.field public e:I

.field public f:Z

.field public g:Z

.field public h:J

.field public i:[Z

.field public j:Z

.field public final k:Lcom/google/t/b/c/a/t;

.field final l:I

.field final m:I

.field public n:I

.field public o:I

.field public p:I

.field private final q:Ljava/lang/String;

.field private final r:I

.field private final s:I

.field private final t:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/t/b/c/a/t;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput v0, p0, Lcom/google/t/b/c/a/k;->e:I

    .line 42
    iput-boolean v0, p0, Lcom/google/t/b/c/a/k;->f:Z

    .line 45
    iput-boolean v0, p0, Lcom/google/t/b/c/a/k;->g:Z

    .line 52
    iput-boolean v0, p0, Lcom/google/t/b/c/a/k;->j:Z

    .line 65
    iput v0, p0, Lcom/google/t/b/c/a/k;->n:I

    .line 66
    iput v0, p0, Lcom/google/t/b/c/a/k;->o:I

    .line 67
    iput v0, p0, Lcom/google/t/b/c/a/k;->p:I

    .line 74
    iput-object p1, p0, Lcom/google/t/b/c/a/k;->k:Lcom/google/t/b/c/a/t;

    .line 75
    iput p2, p0, Lcom/google/t/b/c/a/k;->l:I

    .line 77
    iget-object v0, p0, Lcom/google/t/b/c/a/k;->k:Lcom/google/t/b/c/a/t;

    invoke-interface {v0}, Lcom/google/t/b/c/a/t;->m()I

    move-result v0

    iput v0, p0, Lcom/google/t/b/c/a/k;->m:I

    .line 78
    iget-object v0, p0, Lcom/google/t/b/c/a/k;->k:Lcom/google/t/b/c/a/t;

    invoke-interface {v0}, Lcom/google/t/b/c/a/t;->n()I

    move-result v0

    iput v0, p0, Lcom/google/t/b/c/a/k;->r:I

    .line 79
    iget-object v0, p0, Lcom/google/t/b/c/a/k;->k:Lcom/google/t/b/c/a/t;

    invoke-interface {v0}, Lcom/google/t/b/c/a/t;->o()I

    move-result v0

    iput v0, p0, Lcom/google/t/b/c/a/k;->s:I

    .line 80
    iget-object v0, p0, Lcom/google/t/b/c/a/k;->k:Lcom/google/t/b/c/a/t;

    invoke-interface {v0}, Lcom/google/t/b/c/a/t;->b()Lcom/google/android/location/j/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/j/e;->b()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/t/b/c/a/k;->t:Ljava/lang/String;

    .line 83
    invoke-interface {p1}, Lcom/google/t/b/c/a/t;->p()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[^A-Za-z0-9]"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/t/b/c/a/k;->q:Ljava/lang/String;

    .line 86
    if-lez p2, :cond_0

    const/16 v0, 0x80

    if-le p2, v0, :cond_3

    .line 87
    :cond_0
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_1

    const-string v0, "TemporalDiskCache"

    const-string v1, "Illegal number of file chunks. Initialization terminated."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    :cond_1
    iput-object v3, p0, Lcom/google/t/b/c/a/k;->d:Ljava/io/File;

    .line 124
    :cond_2
    :goto_0
    return-void

    .line 91
    :cond_3
    iget v0, p0, Lcom/google/t/b/c/a/k;->r:I

    if-gtz v0, :cond_5

    .line 92
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_4

    const-string v0, "TemporalDiskCache"

    const-string v1, "Illegal number of read queue entries. Initialization terminated."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :cond_4
    iput-object v3, p0, Lcom/google/t/b/c/a/k;->d:Ljava/io/File;

    goto :goto_0

    .line 96
    :cond_5
    iget v0, p0, Lcom/google/t/b/c/a/k;->s:I

    if-gtz v0, :cond_7

    .line 97
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_6

    const-string v0, "TemporalDiskCache"

    const-string v1, "Illegal number of write queue entries. Initialization terminated."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :cond_6
    iput-object v3, p0, Lcom/google/t/b/c/a/k;->d:Ljava/io/File;

    goto :goto_0

    .line 101
    :cond_7
    iget-object v0, p0, Lcom/google/t/b/c/a/k;->q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 102
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_8

    const-string v0, "TemporalDiskCache"

    const-string v1, "Illegal filename extension. Initialization terminated."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :cond_8
    iput-object v3, p0, Lcom/google/t/b/c/a/k;->d:Ljava/io/File;

    goto :goto_0

    .line 108
    :cond_9
    new-array v0, p2, [Z

    iput-object v0, p0, Lcom/google/t/b/c/a/k;->i:[Z

    .line 111
    iget v0, p0, Lcom/google/t/b/c/a/k;->r:I

    invoke-direct {p0, v0}, Lcom/google/t/b/c/a/k;->b(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/t/b/c/a/k;->b:Ljava/util/List;

    .line 112
    iget v0, p0, Lcom/google/t/b/c/a/k;->s:I

    invoke-direct {p0, v0}, Lcom/google/t/b/c/a/k;->b(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/t/b/c/a/k;->c:Ljava/util/List;

    .line 115
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/t/b/c/a/k;->t:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/t/b/c/a/k;->q:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/t/b/c/a/k;->d:Ljava/io/File;

    .line 116
    invoke-direct {p0}, Lcom/google/t/b/c/a/k;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 122
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/t/b/c/a/k;->a:Ljava/util/List;

    .line 123
    invoke-direct {p0}, Lcom/google/t/b/c/a/k;->b()V

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/google/t/b/c/a/k;Ljava/io/File;Lcom/google/t/b/c/a/u;)V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 24
    invoke-direct {p0}, Lcom/google/t/b/c/a/k;->c()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Cache directory can not be validated."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->createNewFile()Z

    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v4, Ljava/io/DataOutputStream;

    invoke-direct {v4, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    :try_start_0
    invoke-virtual {p2, v4}, Lcom/google/t/b/c/a/u;->a(Ljava/io/DataOutput;)V

    iget-object v0, p0, Lcom/google/t/b/c/a/k;->k:Lcom/google/t/b/c/a/t;

    invoke-interface {v0}, Lcom/google/t/b/c/a/t;->d()Lcom/google/android/location/e/a;

    move-result-object v0

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Lcom/google/android/location/e/a;->a(Ljava/io/DataOutputStream;[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    new-array v0, v10, [Ljava/io/OutputStream;

    aput-object v1, v0, v6

    aput-object v2, v0, v7

    aput-object v3, v0, v8

    aput-object v4, v0, v9

    invoke-static {v0}, Lcom/google/t/b/c/a/k;->a([Ljava/io/OutputStream;)V

    return-void

    :catch_0
    move-exception v0

    new-array v5, v10, [Ljava/io/OutputStream;

    aput-object v1, v5, v6

    aput-object v2, v5, v7

    aput-object v3, v5, v8

    aput-object v4, v5, v9

    invoke-static {v5}, Lcom/google/t/b/c/a/k;->a([Ljava/io/OutputStream;)V

    throw v0
.end method

.method static a(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 601
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 602
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 603
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 605
    if-eqz v2, :cond_0

    .line 606
    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 607
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-char v6, Ljava/io/File;->separatorChar:C

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/t/b/c/a/k;->a(Ljava/lang/String;)V

    .line 606
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 611
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 613
    :cond_1
    return-void
.end method

.method private static varargs a([Ljava/io/InputStream;)V
    .locals 3

    .prologue
    .line 546
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p0, v0

    .line 547
    if-eqz v2, :cond_0

    .line 549
    :try_start_0
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 546
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_1

    .line 555
    :cond_1
    return-void
.end method

.method private static varargs a([Ljava/io/OutputStream;)V
    .locals 3

    .prologue
    .line 558
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p0, v0

    .line 559
    if-eqz v2, :cond_0

    .line 561
    :try_start_0
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 558
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_1

    .line 567
    :cond_1
    return-void
.end method

.method private b(I)Ljava/util/List;
    .locals 4

    .prologue
    .line 371
    new-instance v1, Ljava/util/ArrayList;

    iget v0, p0, Lcom/google/t/b/c/a/k;->l:I

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 373
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/google/t/b/c/a/k;->l:I

    if-ge v0, v2, :cond_0

    .line 374
    new-instance v2, Lcom/google/t/b/c/a/u;

    iget-object v3, p0, Lcom/google/t/b/c/a/k;->k:Lcom/google/t/b/c/a/t;

    invoke-direct {v2, v3, p1}, Lcom/google/t/b/c/a/u;-><init>(Lcom/google/t/b/c/a/v;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 373
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 376
    :cond_0
    return-object v1
.end method

.method private b()V
    .locals 6

    .prologue
    .line 381
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/google/t/b/c/a/k;->l:I

    if-ge v0, v1, :cond_0

    .line 382
    iget-object v1, p0, Lcom/google/t/b/c/a/k;->a:Ljava/util/List;

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/google/t/b/c/a/k;->d:Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/t/b/c/a/k;->q:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 384
    :cond_0
    iget v1, p0, Lcom/google/t/b/c/a/k;->e:I

    iget-object v0, p0, Lcom/google/t/b/c/a/k;->a:Ljava/util/List;

    iget v2, p0, Lcom/google/t/b/c/a/k;->e:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {p0, v1, v0}, Lcom/google/t/b/c/a/k;->a(ILjava/io/File;)V

    .line 385
    return-void
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Lcom/google/t/b/c/a/k;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 469
    const/4 v0, 0x1

    .line 474
    :goto_0
    return v0

    .line 471
    :cond_0
    iget-object v0, p0, Lcom/google/t/b/c/a/k;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 472
    iget-object v0, p0, Lcom/google/t/b/c/a/k;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 474
    :cond_1
    iget-object v0, p0, Lcom/google/t/b/c/a/k;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method final a(I)I
    .locals 2

    .prologue
    .line 574
    add-int/lit8 v0, p1, 0x1

    iget v1, p0, Lcom/google/t/b/c/a/k;->l:I

    rem-int/2addr v0, v1

    return v0
.end method

.method final a(Ljava/io/File;Lcom/google/t/b/c/a/u;)Lcom/google/t/b/c/a/u;
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 508
    invoke-direct {p0}, Lcom/google/t/b/c/a/k;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 509
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Cache directory can not be validated."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 513
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 542
    :goto_0
    return-object p2

    .line 518
    :cond_1
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 521
    new-instance v2, Ljava/io/BufferedInputStream;

    invoke-direct {v2, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 522
    new-instance v3, Ljava/io/DataInputStream;

    invoke-direct {v3, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 523
    :try_start_0
    iget-object v0, p0, Lcom/google/t/b/c/a/k;->k:Lcom/google/t/b/c/a/t;

    invoke-interface {v0}, Lcom/google/t/b/c/a/t;->d()Lcom/google/android/location/e/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/location/e/a;->a(Ljava/io/DataInputStream;)Lcom/google/android/location/e/aj;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 532
    new-instance v4, Ljava/io/ByteArrayInputStream;

    iget-object v0, v0, Lcom/google/android/location/e/aj;->b:Ljava/lang/Object;

    check-cast v0, [B

    invoke-direct {v4, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 533
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 535
    :try_start_1
    invoke-virtual {p2, v0}, Lcom/google/t/b/c/a/u;->a(Ljava/io/DataInput;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 541
    const/4 v5, 0x5

    new-array v5, v5, [Ljava/io/InputStream;

    aput-object v1, v5, v7

    aput-object v2, v5, v8

    aput-object v3, v5, v9

    aput-object v4, v5, v10

    aput-object v0, v5, v11

    invoke-static {v5}, Lcom/google/t/b/c/a/k;->a([Ljava/io/InputStream;)V

    goto :goto_0

    .line 526
    :catch_0
    move-exception v0

    .line 527
    new-array v4, v10, [Ljava/io/InputStream;

    aput-object v1, v4, v7

    aput-object v2, v4, v8

    aput-object v3, v4, v9

    invoke-static {v4}, Lcom/google/t/b/c/a/k;->a([Ljava/io/InputStream;)V

    .line 528
    throw v0

    .line 536
    :catch_1
    move-exception v5

    .line 537
    const/4 v6, 0x5

    new-array v6, v6, [Ljava/io/InputStream;

    aput-object v1, v6, v7

    aput-object v2, v6, v8

    aput-object v3, v6, v9

    aput-object v4, v6, v10

    aput-object v0, v6, v11

    invoke-static {v6}, Lcom/google/t/b/c/a/k;->a([Ljava/io/InputStream;)V

    .line 538
    throw v5
.end method

.method final a(ILjava/io/File;)V
    .locals 2

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/t/b/c/a/k;->k:Lcom/google/t/b/c/a/t;

    invoke-interface {v0}, Lcom/google/t/b/c/a/t;->b()Lcom/google/android/location/j/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/j/e;->d()Lcom/google/android/location/j/i;

    move-result-object v0

    new-instance v1, Lcom/google/t/b/c/a/p;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/t/b/c/a/p;-><init>(Lcom/google/t/b/c/a/k;Ljava/io/File;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/location/j/i;->execute(Ljava/lang/Runnable;)V

    .line 423
    return-void
.end method

.method public final a(Ljava/io/File;)V
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/t/b/c/a/k;->k:Lcom/google/t/b/c/a/t;

    invoke-interface {v0}, Lcom/google/t/b/c/a/t;->b()Lcom/google/android/location/j/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/j/e;->d()Lcom/google/android/location/j/i;

    move-result-object v0

    new-instance v1, Lcom/google/t/b/c/a/l;

    invoke-direct {v1, p0, p1}, Lcom/google/t/b/c/a/l;-><init>(Lcom/google/t/b/c/a/k;Ljava/io/File;)V

    invoke-virtual {v0, v1}, Lcom/google/android/location/j/i;->execute(Ljava/lang/Runnable;)V

    .line 244
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 360
    move v1, v2

    :goto_0
    iget v0, p0, Lcom/google/t/b/c/a/k;->l:I

    if-ge v1, v0, :cond_1

    .line 361
    iget-object v0, p0, Lcom/google/t/b/c/a/k;->b:Ljava/util/List;

    iget v3, p0, Lcom/google/t/b/c/a/k;->e:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/t/b/c/a/u;

    invoke-virtual {v0}, Lcom/google/t/b/c/a/u;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/t/b/c/a/k;->c:Ljava/util/List;

    iget v3, p0, Lcom/google/t/b/c/a/k;->e:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/t/b/c/a/u;

    invoke-virtual {v0}, Lcom/google/t/b/c/a/u;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/t/b/c/a/k;->i:[Z

    iget v3, p0, Lcom/google/t/b/c/a/k;->e:I

    aget-boolean v0, v0, v3

    if-eqz v0, :cond_2

    .line 363
    :cond_0
    const/4 v2, 0x1

    .line 367
    :cond_1
    return v2

    .line 365
    :cond_2
    iget v0, p0, Lcom/google/t/b/c/a/k;->e:I

    invoke-virtual {p0, v0}, Lcom/google/t/b/c/a/k;->a(I)I

    move-result v0

    iput v0, p0, Lcom/google/t/b/c/a/k;->e:I

    .line 360
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/google/t/b/c/a/k;->j:Z

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/google/t/b/c/a/k;->b:Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/t/b/c/a/k;->b(Ljava/lang/Object;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/t/b/c/a/u;

    invoke-virtual {v0, p1}, Lcom/google/t/b/c/a/u;->b(Ljava/lang/Object;)V

    .line 135
    :cond_0
    iget-boolean v0, p0, Lcom/google/t/b/c/a/k;->j:Z

    return v0
.end method

.method public final b(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 570
    iget v0, p0, Lcom/google/t/b/c/a/k;->l:I

    invoke-static {p1, v0}, Lcom/google/t/b/c/a/u;->a(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method
