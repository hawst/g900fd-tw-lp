.class public final Lcom/google/t/b/b/a/m;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[I

.field private static final b:[I

.field private static final c:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 47
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/t/b/b/a/m;->a:[I

    .line 48
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/t/b/b/a/m;->b:[I

    .line 49
    invoke-static {}, Lcom/google/t/b/b/a/n;->a()J

    const-wide/32 v0, 0x1ffffff

    sput-wide v0, Lcom/google/t/b/b/a/m;->c:J

    return-void

    .line 47
    :array_0
    .array-data 4
        0x1
        0x5
        0x15
    .end array-data

    .line 48
    :array_1
    .array-data 4
        0x17
        0x15
        0x13
    .end array-data
.end method

.method static final a([BJI)F
    .locals 5

    .prologue
    .line 112
    invoke-static {p0, p1, p2}, Lcom/google/t/b/b/a/m;->c([BJ)I

    move-result v0

    mul-int/lit8 v1, p3, 0x3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x2

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    const-wide v2, 0x3fd9191919191919L    # 0.39215686274509803

    int-to-double v0, v0

    mul-double/2addr v0, v2

    const-wide/high16 v2, -0x3fa7000000000000L    # -100.0

    add-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private static a(J)I
    .locals 4

    .prologue
    .line 156
    invoke-static {p0, p1}, Lcom/google/t/b/b/a/n;->a(J)I

    move-result v0

    const/16 v1, 0x15

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 157
    const/16 v1, 0x12

    if-gt v0, v1, :cond_0

    .line 158
    const/4 v0, 0x0

    .line 164
    :goto_0
    return v0

    .line 161
    :cond_0
    sget-wide v2, Lcom/google/t/b/b/a/m;->c:J

    and-long/2addr v2, p0

    .line 162
    add-int/lit8 v0, v0, -0x13

    .line 163
    sget-object v1, Lcom/google/t/b/b/a/m;->b:[I

    aget v1, v1, v0

    shr-long/2addr v2, v1

    .line 164
    long-to-int v1, v2

    sget-object v2, Lcom/google/t/b/b/a/m;->a:[I

    aget v0, v2, v0

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method static final a([B)J
    .locals 2

    .prologue
    .line 55
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/t/b/b/a/g;->c([BI)J

    move-result-wide v0

    return-wide v0
.end method

.method static final a([BJII)J
    .locals 3

    .prologue
    .line 144
    invoke-static {p0, p1, p2, p3}, Lcom/google/t/b/b/a/m;->d([BJI)I

    move-result v0

    mul-int/lit8 v1, p4, 0x8

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 145
    invoke-static {p0, v0}, Lcom/google/t/b/b/a/g;->c([BI)J

    move-result-wide v0

    return-wide v0
.end method

.method static final a([BJ)Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v1, 0xff

    .line 84
    invoke-static {p1, p2}, Lcom/google/t/b/b/a/m;->a(J)I

    move-result v0

    add-int/lit8 v0, v0, 0x11

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    if-ne v0, v1, :cond_0

    move v0, v1

    .line 85
    :goto_0
    if-ne v0, v1, :cond_1

    .line 86
    const/4 v0, 0x0

    .line 88
    :goto_1
    return-object v0

    .line 84
    :cond_0
    array-length v2, p0

    mul-int/lit8 v0, v0, 0x10

    add-int/lit8 v0, v0, 0x10

    sub-int v0, v2, v0

    goto :goto_0

    .line 88
    :cond_1
    invoke-static {p0, v0}, Lcom/google/t/b/b/a/g;->c([BI)J

    move-result-wide v2

    add-int/lit8 v0, v0, 0x8

    invoke-static {p0, v0}, Lcom/google/t/b/b/a/g;->c([BI)J

    move-result-wide v0

    const-string v4, "0x%016x:0x%016x"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v6

    const/4 v2, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method static final b([BJI)F
    .locals 5

    .prologue
    .line 128
    invoke-static {p0, p1, p2}, Lcom/google/t/b/b/a/m;->c([BJ)I

    move-result v0

    mul-int/lit8 v1, p3, 0x3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x3

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    const-wide v2, 0x3f88181818181818L    # 0.011764705882352941

    int-to-double v0, v0

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    add-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method static final b([B)I
    .locals 1

    .prologue
    .line 62
    const/16 v0, 0x9

    invoke-static {p0, v0}, Lcom/google/t/b/b/a/g;->b([BI)I

    move-result v0

    return v0
.end method

.method static final b([BJ)I
    .locals 1

    .prologue
    .line 96
    invoke-static {p0, p1, p2}, Lcom/google/t/b/b/a/m;->c([BJ)I

    move-result v0

    .line 97
    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method static final c([B)I
    .locals 1

    .prologue
    .line 69
    const/16 v0, 0xd

    invoke-static {p0, v0}, Lcom/google/t/b/b/a/g;->b([BI)I

    move-result v0

    return v0
.end method

.method private static final c([BJ)I
    .locals 1

    .prologue
    .line 229
    invoke-static {p1, p2}, Lcom/google/t/b/b/a/m;->a(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x66

    .line 230
    invoke-static {p0, v0}, Lcom/google/t/b/b/a/g;->a([BI)I

    move-result v0

    return v0
.end method

.method static final c([BJI)I
    .locals 1

    .prologue
    .line 135
    invoke-static {p0, p1, p2, p3}, Lcom/google/t/b/b/a/m;->d([BJI)I

    move-result v0

    .line 136
    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method private static final d([BJI)I
    .locals 3

    .prologue
    .line 199
    invoke-static {p0, p1, p2}, Lcom/google/t/b/b/a/m;->c([BJ)I

    move-result v0

    mul-int/lit8 v1, p3, 0x3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    mul-int/lit8 v0, v0, 0x2

    add-int/lit16 v0, v0, 0x110

    .line 200
    invoke-static {p0, v0}, Lcom/google/t/b/b/a/g;->a([BI)I

    move-result v0

    return v0
.end method
