.class public final Lcom/google/t/b/b/a/o;
.super Lcom/google/t/b/b/a/a;
.source "SourceFile"


# static fields
.field private static final g:Lcom/google/t/b/b/a/o;

.field private static final h:Ljava/lang/Boolean;

.field private static final i:Lcom/google/p/a/b/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/google/t/b/b/a/o;

    invoke-direct {v0}, Lcom/google/t/b/b/a/o;-><init>()V

    sput-object v0, Lcom/google/t/b/b/a/o;->g:Lcom/google/t/b/b/a/o;

    .line 26
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object v0, Lcom/google/t/b/b/a/o;->h:Ljava/lang/Boolean;

    .line 27
    new-instance v0, Lcom/google/p/a/b/b/a;

    sget-object v1, Lcom/google/android/location/m/a;->aQ:Lcom/google/p/a/b/b/c;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    sput-object v0, Lcom/google/t/b/b/a/o;->i:Lcom/google/p/a/b/b/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/google/t/b/b/a/a;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/t/b/b/a/b;)Lcom/google/t/b/b/a/o;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 44
    sget-object v0, Lcom/google/t/b/b/a/o;->g:Lcom/google/t/b/b/a/o;

    invoke-virtual {v0, p0, v1, v1, v1}, Lcom/google/t/b/b/a/o;->a(Lcom/google/t/b/b/a/b;III)V

    .line 46
    sget-object v0, Lcom/google/t/b/b/a/o;->g:Lcom/google/t/b/b/a/o;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/p/a/b/b/a;)V
    .locals 3

    .prologue
    const/4 v1, 0x6

    .line 87
    invoke-virtual {p1, v1}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 88
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_0

    const-string v0, "ServerDefinedSettingsCacheManager"

    const-string v1, "Received no valid response entry."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    sget-object v0, Lcom/google/t/b/b/a/o;->h:Ljava/lang/Boolean;

    invoke-virtual {p0, v0}, Lcom/google/t/b/b/a/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/p/a/b/b/a;

    .line 92
    invoke-virtual {p1, v1}, Lcom/google/p/a/b/b/a;->f(I)Lcom/google/p/a/b/b/a;

    move-result-object v1

    .line 95
    if-nez v0, :cond_2

    .line 96
    sget-object v0, Lcom/google/t/b/b/a/o;->h:Ljava/lang/Boolean;

    invoke-virtual {p0, v0, v1}, Lcom/google/t/b/b/a/o;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 103
    :cond_2
    :try_start_0
    invoke-virtual {v0}, Lcom/google/p/a/b/b/a;->f()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 112
    :try_start_1
    invoke-virtual {v1}, Lcom/google/p/a/b/b/a;->f()[B

    move-result-object v2

    invoke-static {v0, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    sget-object v0, Lcom/google/t/b/b/a/o;->h:Ljava/lang/Boolean;

    invoke-virtual {p0, v0, v1}, Lcom/google/t/b/b/a/o;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 116
    :catch_0
    move-exception v0

    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_0

    const-string v0, "ServerDefinedSettingsCacheManager"

    const-string v1, "Client params in response was not properly formed."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :catch_1
    move-exception v0

    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_3

    const-string v0, "ServerDefinedSettingsCacheManager"

    const-string v2, "Client params in memory was not properly formed."

    invoke-static {v0, v2}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    :cond_3
    sget-object v0, Lcom/google/t/b/b/a/o;->h:Ljava/lang/Boolean;

    invoke-virtual {p0, v0, v1}, Lcom/google/t/b/b/a/o;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/t/b/c/a/u;)V
    .locals 2

    .prologue
    .line 122
    iget-object v1, p0, Lcom/google/t/b/b/a/o;->a:Lcom/google/t/b/b/a/b;

    sget-object v0, Lcom/google/t/b/b/a/o;->h:Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/t/b/c/a/u;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/p/a/b/b/a;

    invoke-virtual {v1, v0}, Lcom/google/t/b/b/a/b;->d(Lcom/google/p/a/b/b/a;)V

    .line 123
    return-void
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x1

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x1

    return v0
.end method

.method public final o()I
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x1

    return v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    const-string v0, "sdsc"

    return-object v0
.end method

.method public final q()Lcom/google/android/location/e/ak;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/google/t/b/c/a/c;->c:Lcom/google/android/location/e/ak;

    return-object v0
.end method

.method public final r()Lcom/google/android/location/e/ak;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/google/android/location/m/a;->aQ:Lcom/google/p/a/b/b/c;

    invoke-static {v0}, Lcom/google/t/b/c/a/c;->b(Lcom/google/p/a/b/b/c;)Lcom/google/android/location/e/ak;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic s()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/t/b/b/a/o;->i:Lcom/google/p/a/b/b/a;

    return-object v0
.end method
