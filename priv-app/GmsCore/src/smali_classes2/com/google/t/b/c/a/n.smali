.class final Lcom/google/t/b/c/a/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/io/File;

.field final synthetic b:Lcom/google/t/b/c/a/u;

.field final synthetic c:Lcom/google/t/b/c/a/k;


# direct methods
.method constructor <init>(Lcom/google/t/b/c/a/k;Ljava/io/File;Lcom/google/t/b/c/a/u;)V
    .locals 0

    .prologue
    .line 312
    iput-object p1, p0, Lcom/google/t/b/c/a/n;->c:Lcom/google/t/b/c/a/k;

    iput-object p2, p0, Lcom/google/t/b/c/a/n;->a:Ljava/io/File;

    iput-object p3, p0, Lcom/google/t/b/c/a/n;->b:Lcom/google/t/b/c/a/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 315
    const/4 v0, 0x1

    .line 317
    :try_start_0
    iget-object v1, p0, Lcom/google/t/b/c/a/n;->c:Lcom/google/t/b/c/a/k;

    iget-object v2, p0, Lcom/google/t/b/c/a/n;->a:Ljava/io/File;

    iget-object v3, p0, Lcom/google/t/b/c/a/n;->b:Lcom/google/t/b/c/a/u;

    invoke-static {v1, v2, v3}, Lcom/google/t/b/c/a/k;->a(Lcom/google/t/b/c/a/k;Ljava/io/File;Lcom/google/t/b/c/a/u;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 331
    :goto_0
    iget-object v1, p0, Lcom/google/t/b/c/a/n;->c:Lcom/google/t/b/c/a/k;

    iget-object v2, v1, Lcom/google/t/b/c/a/k;->k:Lcom/google/t/b/c/a/t;

    invoke-interface {v2}, Lcom/google/t/b/c/a/t;->c()Lcom/google/android/location/j/j;

    move-result-object v2

    new-instance v3, Lcom/google/t/b/c/a/o;

    invoke-direct {v3, v1, v0}, Lcom/google/t/b/c/a/o;-><init>(Lcom/google/t/b/c/a/k;Z)V

    invoke-interface {v2, v3}, Lcom/google/android/location/j/j;->a(Ljava/lang/Runnable;)Z

    .line 332
    return-void

    :catch_0
    move-exception v1

    .line 321
    :try_start_1
    new-instance v1, Lcom/google/t/b/c/a/u;

    iget-object v2, p0, Lcom/google/t/b/c/a/n;->c:Lcom/google/t/b/c/a/k;

    iget-object v2, v2, Lcom/google/t/b/c/a/k;->k:Lcom/google/t/b/c/a/t;

    iget-object v3, p0, Lcom/google/t/b/c/a/n;->c:Lcom/google/t/b/c/a/k;

    iget v3, v3, Lcom/google/t/b/c/a/k;->m:I

    invoke-direct {v1, v2, v3}, Lcom/google/t/b/c/a/u;-><init>(Lcom/google/t/b/c/a/v;I)V

    .line 323
    iget-object v2, p0, Lcom/google/t/b/c/a/n;->c:Lcom/google/t/b/c/a/k;

    iget-object v3, p0, Lcom/google/t/b/c/a/n;->a:Ljava/io/File;

    invoke-static {v2, v3, v1}, Lcom/google/t/b/c/a/k;->a(Lcom/google/t/b/c/a/k;Ljava/io/File;Lcom/google/t/b/c/a/u;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 324
    :catch_1
    move-exception v0

    .line 326
    sget-boolean v1, Lcom/google/android/location/i/a;->d:Z

    if-eqz v1, :cond_0

    const-string v1, "TemporalDiskCache"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    :cond_0
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_1

    const-string v0, "TemporalDiskCache"

    const-string v1, "Recovery write failed. Deactivating."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
