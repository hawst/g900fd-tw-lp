.class public final Lcom/google/t/b/b/a/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/t/b/b/a/c;


# static fields
.field static final a:D

.field static final b:F


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field c:I

.field d:I

.field e:I

.field f:I

.field g:I

.field h:I

.field i:I

.field j:I

.field k:I

.field l:F

.field m:F

.field n:F

.field o:I

.field p:F

.field q:F

.field r:F

.field s:F

.field t:I

.field u:I

.field v:Z

.field w:J

.field x:Lcom/google/t/b/b/a/u;

.field y:Lcom/google/t/b/b/a/u;

.field private final z:Lcom/google/t/b/b/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 31
    const-wide/high16 v0, -0x4020000000000000L    # -0.5

    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    sput-wide v0, Lcom/google/t/b/b/a/d;->a:D

    .line 56
    const-wide/high16 v0, 0x4034000000000000L    # 20.0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lcom/google/t/b/b/a/d;->b:F

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x3

    const/4 v1, -0x1

    .line 777
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const/16 v0, 0x14

    iput v0, p0, Lcom/google/t/b/b/a/d;->c:I

    .line 65
    iput v2, p0, Lcom/google/t/b/b/a/d;->d:I

    .line 66
    iput v2, p0, Lcom/google/t/b/b/a/d;->e:I

    .line 67
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/t/b/b/a/d;->f:I

    .line 68
    iput v2, p0, Lcom/google/t/b/b/a/d;->g:I

    .line 69
    const/16 v0, -0x5d

    iput v0, p0, Lcom/google/t/b/b/a/d;->h:I

    .line 70
    const/16 v0, -0x57

    iput v0, p0, Lcom/google/t/b/b/a/d;->i:I

    .line 71
    const/16 v0, 0x1388

    iput v0, p0, Lcom/google/t/b/b/a/d;->j:I

    .line 72
    const v0, 0x88b8

    iput v0, p0, Lcom/google/t/b/b/a/d;->k:I

    .line 73
    iput v3, p0, Lcom/google/t/b/b/a/d;->l:F

    .line 74
    iput v3, p0, Lcom/google/t/b/b/a/d;->m:F

    .line 75
    const/high16 v0, 0x43480000    # 200.0f

    iput v0, p0, Lcom/google/t/b/b/a/d;->n:F

    .line 76
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/t/b/b/a/d;->o:I

    .line 77
    const/high16 v0, -0x3d460000    # -93.0f

    iput v0, p0, Lcom/google/t/b/b/a/d;->p:F

    .line 78
    sget v0, Lcom/google/t/b/b/a/d;->b:F

    iput v0, p0, Lcom/google/t/b/b/a/d;->q:F

    .line 79
    const v0, -0x800001

    iput v0, p0, Lcom/google/t/b/b/a/d;->r:F

    .line 80
    const v0, 0x3e99999a    # 0.3f

    iput v0, p0, Lcom/google/t/b/b/a/d;->s:F

    .line 81
    const/16 v0, 0x32

    iput v0, p0, Lcom/google/t/b/b/a/d;->t:I

    .line 82
    const/16 v0, 0x2710

    iput v0, p0, Lcom/google/t/b/b/a/d;->u:I

    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/t/b/b/a/d;->v:Z

    .line 90
    new-instance v0, Lcom/google/t/b/b/a/u;

    invoke-direct {v0}, Lcom/google/t/b/b/a/u;-><init>()V

    iput-object v0, p0, Lcom/google/t/b/b/a/d;->x:Lcom/google/t/b/b/a/u;

    .line 91
    new-instance v0, Lcom/google/t/b/b/a/u;

    invoke-direct {v0}, Lcom/google/t/b/b/a/u;-><init>()V

    iput-object v0, p0, Lcom/google/t/b/b/a/d;->y:Lcom/google/t/b/b/a/u;

    .line 94
    iput v1, p0, Lcom/google/t/b/b/a/d;->A:I

    .line 95
    iput v1, p0, Lcom/google/t/b/b/a/d;->B:I

    .line 96
    iput v1, p0, Lcom/google/t/b/b/a/d;->C:I

    .line 778
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    .line 779
    return-void
.end method

.method public constructor <init>(Lcom/google/t/b/b/a/b;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x3

    const/4 v1, -0x1

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const/16 v0, 0x14

    iput v0, p0, Lcom/google/t/b/b/a/d;->c:I

    .line 65
    iput v2, p0, Lcom/google/t/b/b/a/d;->d:I

    .line 66
    iput v2, p0, Lcom/google/t/b/b/a/d;->e:I

    .line 67
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/t/b/b/a/d;->f:I

    .line 68
    iput v2, p0, Lcom/google/t/b/b/a/d;->g:I

    .line 69
    const/16 v0, -0x5d

    iput v0, p0, Lcom/google/t/b/b/a/d;->h:I

    .line 70
    const/16 v0, -0x57

    iput v0, p0, Lcom/google/t/b/b/a/d;->i:I

    .line 71
    const/16 v0, 0x1388

    iput v0, p0, Lcom/google/t/b/b/a/d;->j:I

    .line 72
    const v0, 0x88b8

    iput v0, p0, Lcom/google/t/b/b/a/d;->k:I

    .line 73
    iput v3, p0, Lcom/google/t/b/b/a/d;->l:F

    .line 74
    iput v3, p0, Lcom/google/t/b/b/a/d;->m:F

    .line 75
    const/high16 v0, 0x43480000    # 200.0f

    iput v0, p0, Lcom/google/t/b/b/a/d;->n:F

    .line 76
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/t/b/b/a/d;->o:I

    .line 77
    const/high16 v0, -0x3d460000    # -93.0f

    iput v0, p0, Lcom/google/t/b/b/a/d;->p:F

    .line 78
    sget v0, Lcom/google/t/b/b/a/d;->b:F

    iput v0, p0, Lcom/google/t/b/b/a/d;->q:F

    .line 79
    const v0, -0x800001

    iput v0, p0, Lcom/google/t/b/b/a/d;->r:F

    .line 80
    const v0, 0x3e99999a    # 0.3f

    iput v0, p0, Lcom/google/t/b/b/a/d;->s:F

    .line 81
    const/16 v0, 0x32

    iput v0, p0, Lcom/google/t/b/b/a/d;->t:I

    .line 82
    const/16 v0, 0x2710

    iput v0, p0, Lcom/google/t/b/b/a/d;->u:I

    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/t/b/b/a/d;->v:Z

    .line 90
    new-instance v0, Lcom/google/t/b/b/a/u;

    invoke-direct {v0}, Lcom/google/t/b/b/a/u;-><init>()V

    iput-object v0, p0, Lcom/google/t/b/b/a/d;->x:Lcom/google/t/b/b/a/u;

    .line 91
    new-instance v0, Lcom/google/t/b/b/a/u;

    invoke-direct {v0}, Lcom/google/t/b/b/a/u;-><init>()V

    iput-object v0, p0, Lcom/google/t/b/b/a/d;->y:Lcom/google/t/b/b/a/u;

    .line 94
    iput v1, p0, Lcom/google/t/b/b/a/d;->A:I

    .line 95
    iput v1, p0, Lcom/google/t/b/b/a/d;->B:I

    .line 96
    iput v1, p0, Lcom/google/t/b/b/a/d;->C:I

    .line 103
    iput-object p1, p0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    .line 104
    invoke-virtual {p1, p0}, Lcom/google/t/b/b/a/b;->a(Lcom/google/t/b/b/a/c;)V

    .line 105
    return-void
.end method

.method private static final a(DDD)D
    .locals 4

    .prologue
    .line 646
    sub-double v0, p0, p2

    .line 647
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v2, v0

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, p4

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    add-double/2addr v0, p4

    neg-double v0, v0

    return-wide v0
.end method

.method private a(Lcom/google/t/b/b/a/j;ID)D
    .locals 13

    .prologue
    .line 590
    const-wide/16 v6, 0x0

    .line 593
    const/4 v0, 0x0

    iput v0, p1, Lcom/google/t/b/b/a/j;->d:I

    .line 594
    const/4 v0, 0x0

    move v8, v0

    :goto_0
    if-ge v8, p2, :cond_5

    .line 596
    :try_start_0
    iget-object v0, p1, Lcom/google/t/b/b/a/j;->b:[B

    iget-wide v2, p1, Lcom/google/t/b/b/a/j;->a:J

    invoke-static {v0}, Lcom/google/t/b/b/a/l;->a([B)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown protocol version on getMacCount."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 626
    :catch_0
    move-exception v0

    .line 627
    sget-boolean v1, Lcom/google/android/location/i/a;->e:Z

    if-eqz v1, :cond_0

    const-string v1, "GpwleOneShotLocalizer"

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    :cond_0
    throw v0

    .line 596
    :pswitch_0
    :try_start_1
    invoke-static {v0, v2, v3, v8}, Lcom/google/t/b/b/a/m;->c([BJI)I

    move-result v1

    .line 598
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_4

    .line 599
    iget-object v2, p1, Lcom/google/t/b/b/a/j;->b:[B

    iget-wide v4, p1, Lcom/google/t/b/b/a/j;->a:J

    invoke-static {v2}, Lcom/google/t/b/b/a/l;->a([B)I

    move-result v3

    packed-switch v3, :pswitch_data_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown protocol version on getMac."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    .line 629
    :catch_1
    move-exception v0

    .line 630
    sget-boolean v1, Lcom/google/android/location/i/a;->e:Z

    if-eqz v1, :cond_1

    const-string v1, "GpwleOneShotLocalizer"

    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    :cond_1
    throw v0

    .line 599
    :pswitch_1
    :try_start_2
    invoke-static {v2, v4, v5, v8, v0}, Lcom/google/t/b/b/a/m;->a([BJII)J

    move-result-wide v2

    .line 603
    iget-object v4, p0, Lcom/google/t/b/b/a/d;->x:Lcom/google/t/b/b/a/u;

    invoke-virtual {v4, v2, v3}, Lcom/google/t/b/b/a/u;->a(J)I

    move-result v9

    .line 604
    if-ltz v9, :cond_3

    .line 606
    iget-object v0, p0, Lcom/google/t/b/b/a/d;->x:Lcom/google/t/b/b/a/u;

    iget-object v0, v0, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v0, v0, v9

    iget v0, v0, Lcom/google/t/b/b/a/x;->b:I

    int-to-double v0, v0

    iget v2, p0, Lcom/google/t/b/b/a/d;->p:F

    float-to-double v2, v2

    iget v4, p0, Lcom/google/t/b/b/a/d;->q:F

    float-to-double v4, v4

    invoke-static/range {v0 .. v5}, Lcom/google/t/b/b/a/d;->a(DDD)D

    move-result-wide v0

    sub-double/2addr v6, v0

    .line 611
    iget v0, p0, Lcom/google/t/b/b/a/d;->q:F

    float-to-double v4, v0

    .line 612
    iget-boolean v0, p0, Lcom/google/t/b/b/a/d;->v:Z

    if-eqz v0, :cond_2

    .line 613
    iget-object v0, p1, Lcom/google/t/b/b/a/j;->b:[B

    iget-wide v2, p1, Lcom/google/t/b/b/a/j;->a:J

    invoke-static {v0}, Lcom/google/t/b/b/a/l;->a([B)I

    move-result v1

    packed-switch v1, :pswitch_data_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown protocol version on getStdevLogDbm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_2
    invoke-static {v0, v2, v3, v8}, Lcom/google/t/b/b/a/m;->b([BJI)F

    move-result v0

    float-to-double v4, v0

    .line 616
    :cond_2
    iget-object v0, p0, Lcom/google/t/b/b/a/d;->x:Lcom/google/t/b/b/a/u;

    iget-object v0, v0, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v0, v0, v9

    iget v0, v0, Lcom/google/t/b/b/a/x;->b:I

    int-to-double v0, v0

    iget-object v2, p1, Lcom/google/t/b/b/a/j;->b:[B

    iget-wide v10, p1, Lcom/google/t/b/b/a/j;->a:J

    invoke-static {v2}, Lcom/google/t/b/b/a/l;->a([B)I

    move-result v3

    packed-switch v3, :pswitch_data_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown protocol version on getMeanDbm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_3
    invoke-static {v2, v10, v11, v8}, Lcom/google/t/b/b/a/m;->a([BJI)F

    move-result v2

    float-to-double v2, v2

    invoke-static/range {v0 .. v5}, Lcom/google/t/b/b/a/d;->a(DDD)D

    move-result-wide v0

    add-double/2addr v0, v6

    .line 622
    iget v2, p1, Lcom/google/t/b/b/a/j;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p1, Lcom/google/t/b/b/a/j;->d:I
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1

    .line 594
    :goto_2
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    move-wide v6, v0

    goto/16 :goto_0

    .line 598
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    :cond_4
    move-wide v0, v6

    .line 632
    goto :goto_2

    .line 636
    :cond_5
    iget v0, p1, Lcom/google/t/b/b/a/j;->d:I

    invoke-direct {p0, v0}, Lcom/google/t/b/b/a/d;->a(I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 637
    const-wide/high16 v0, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    .line 641
    :goto_3
    return-wide v0

    :cond_6
    iget-object v0, p0, Lcom/google/t/b/b/a/d;->x:Lcom/google/t/b/b/a/u;

    iget v0, v0, Lcom/google/t/b/b/a/u;->b:I

    int-to-double v0, v0

    div-double v0, v6, v0

    add-double v0, v0, p3

    goto :goto_3

    .line 596
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 599
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    .line 613
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch

    .line 616
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_3
    .end packed-switch
.end method

.method private a(Lcom/google/t/b/b/a/h;I[J)I
    .locals 18

    .prologue
    .line 653
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/t/b/b/a/d;->g:I

    move/from16 v0, p2

    if-lt v0, v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_1

    .line 654
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/t/b/b/a/d;->k:I

    .line 680
    :goto_1
    return v2

    .line 653
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 658
    :cond_1
    const-wide/16 v6, 0x0

    .line 659
    const-wide/16 v4, 0x0

    .line 660
    const/4 v2, 0x0

    move v10, v2

    move-wide v12, v4

    move-wide v14, v6

    :goto_2
    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/t/b/b/a/h;->b:I

    if-ge v10, v2, :cond_2

    .line 661
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/t/b/b/a/h;->a:[Lcom/google/t/b/b/a/j;

    aget-object v2, v2, v10

    iget-wide v2, v2, Lcom/google/t/b/b/a/j;->c:D

    .line 662
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/t/b/b/a/d;->r:F

    float-to-double v4, v4

    cmpg-double v4, v2, v4

    if-ltz v4, :cond_2

    .line 663
    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v16

    .line 666
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/t/b/b/a/h;->a:[Lcom/google/t/b/b/a/j;

    aget-object v2, v2, v10

    iget-wide v2, v2, Lcom/google/t/b/b/a/j;->a:J

    invoke-static {v2, v3}, Lcom/google/t/b/b/a/n;->b(J)[J

    move-result-object v8

    .line 668
    const/4 v2, 0x0

    aget-wide v2, p3, v2

    long-to-double v2, v2

    const/4 v4, 0x1

    aget-wide v4, p3, v4

    long-to-double v4, v4

    const/4 v6, 0x0

    aget-wide v6, v8, v6

    long-to-double v6, v6

    const/4 v9, 0x1

    aget-wide v8, v8, v9

    long-to-double v8, v8

    invoke-static/range {v2 .. v9}, Lcom/google/android/location/e/p;->a(DDDD)D

    move-result-wide v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/t/b/b/a/d;->n:F

    float-to-double v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    .line 671
    mul-double v2, v2, v16

    add-double v6, v14, v2

    .line 672
    add-double v4, v12, v16

    .line 660
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    move-wide v12, v4

    move-wide v14, v6

    goto :goto_2

    .line 676
    :cond_2
    const/high16 v2, 0x447a0000    # 1000.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/t/b/b/a/d;->l:F

    mul-float/2addr v2, v3

    float-to-double v2, v2

    div-double v4, v14, v12

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/t/b/b/a/d;->m:F

    float-to-double v6, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    .line 678
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/t/b/b/a/d;->k:I

    int-to-long v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 679
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/t/b/b/a/d;->j:I

    int-to-long v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 680
    long-to-int v2, v2

    goto/16 :goto_1
.end method

.method private a(Lcom/google/t/b/b/a/h;Z)Lcom/google/t/b/b/a/h;
    .locals 8

    .prologue
    .line 553
    const/4 v1, 0x0

    .line 554
    iget v0, p0, Lcom/google/t/b/b/a/d;->c:I

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    move v0, v1

    :goto_0
    if-ltz v2, :cond_3

    .line 555
    iget-object v1, p1, Lcom/google/t/b/b/a/h;->a:[Lcom/google/t/b/b/a/j;

    aget-object v3, v1, v2

    .line 556
    iget-wide v4, v3, Lcom/google/t/b/b/a/j;->c:D

    invoke-direct {p0, v4, v5}, Lcom/google/t/b/b/a/d;->a(D)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz p2, :cond_2

    .line 559
    :cond_0
    const/4 v1, 0x3

    :goto_1
    if-ltz v1, :cond_2

    .line 560
    mul-int/lit8 v4, v2, 0x4

    add-int/2addr v4, v1

    .line 561
    iget-object v5, p1, Lcom/google/t/b/b/a/h;->a:[Lcom/google/t/b/b/a/j;

    aget-object v5, v5, v4

    .line 562
    iget-wide v6, v3, Lcom/google/t/b/b/a/j;->a:J

    invoke-static {v6, v7, v1}, Lcom/google/t/b/b/a/n;->a(JI)J

    move-result-wide v6

    iput-wide v6, v5, Lcom/google/t/b/b/a/j;->a:J

    .line 563
    iget-object v6, v3, Lcom/google/t/b/b/a/j;->b:[B

    iput-object v6, v5, Lcom/google/t/b/b/a/j;->b:[B

    .line 564
    const-wide/high16 v6, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    iput-wide v6, v5, Lcom/google/t/b/b/a/j;->c:D

    .line 565
    if-nez v0, :cond_1

    .line 566
    add-int/lit8 v0, v4, 0x1

    iput v0, p1, Lcom/google/t/b/b/a/h;->b:I

    .line 567
    const/4 v0, 0x1

    .line 559
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 554
    :cond_2
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_0

    .line 572
    :cond_3
    return-object p1
.end method

.method private a(D)Z
    .locals 3

    .prologue
    .line 700
    iget v0, p0, Lcom/google/t/b/b/a/d;->r:F

    float-to-double v0, v0

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(I)Z
    .locals 1

    .prologue
    .line 692
    iget v0, p0, Lcom/google/t/b/b/a/d;->f:I

    if-lt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/android/location/e/bi;)Lcom/google/android/location/e/n;
    .locals 23

    .prologue
    .line 287
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/t/b/b/a/d;->y:Lcom/google/t/b/b/a/u;

    const/4 v5, 0x0

    iput v5, v4, Lcom/google/t/b/b/a/u;->b:I

    const/4 v5, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/location/e/bi;->a()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/t/b/b/a/d;->y:Lcom/google/t/b/b/a/u;

    invoke-virtual {v4, v7}, Lcom/google/t/b/b/a/u;->a(I)V

    const/4 v6, 0x0

    const/4 v4, 0x0

    move/from16 v22, v4

    move v4, v6

    move/from16 v6, v22

    :goto_0
    if-ge v6, v7, :cond_1

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/android/location/e/bi;->a(I)Lcom/google/android/location/e/bc;

    move-result-object v8

    iget v9, v8, Lcom/google/android/location/e/bc;->d:I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/t/b/b/a/d;->h:I

    if-lt v9, v10, :cond_0

    iget v9, v8, Lcom/google/android/location/e/bc;->d:I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/t/b/b/a/d;->o:I

    if-gt v9, v10, :cond_0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/t/b/b/a/d;->y:Lcom/google/t/b/b/a/u;

    iget-object v9, v9, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v9, v9, v4

    iget-wide v10, v8, Lcom/google/android/location/e/bc;->b:J

    iput-wide v10, v9, Lcom/google/t/b/b/a/x;->a:J

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/t/b/b/a/d;->y:Lcom/google/t/b/b/a/u;

    iget-object v9, v9, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v9, v9, v4

    iget v8, v8, Lcom/google/android/location/e/bc;->d:I

    iput v8, v9, Lcom/google/t/b/b/a/x;->b:I

    add-int/lit8 v4, v4, 0x1

    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_1
    if-lez v4, :cond_3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/t/b/b/a/d;->y:Lcom/google/t/b/b/a/u;

    iput v4, v5, Lcom/google/t/b/b/a/u;->b:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/t/b/b/a/d;->y:Lcom/google/t/b/b/a/u;

    invoke-virtual {v5}, Lcom/google/t/b/b/a/u;->a()V

    const/4 v8, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/t/b/b/a/d;->y:Lcom/google/t/b/b/a/u;

    iget-object v5, v5, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    const/4 v6, 0x0

    aget-object v5, v5, v6

    iget-wide v6, v5, Lcom/google/t/b/b/a/x;->a:J

    const/4 v5, 0x1

    move/from16 v22, v5

    move v5, v8

    move-wide v8, v6

    move/from16 v6, v22

    :goto_1
    if-ge v6, v4, :cond_3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/t/b/b/a/d;->y:Lcom/google/t/b/b/a/u;

    iget-object v7, v7, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v7, v7, v6

    iget-wide v10, v7, Lcom/google/t/b/b/a/x;->a:J

    cmp-long v7, v8, v10

    if-eqz v7, :cond_2

    add-int/lit8 v5, v5, 0x1

    :cond_2
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/t/b/b/a/d;->y:Lcom/google/t/b/b/a/u;

    iget-object v7, v7, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v7, v7, v6

    iget-wide v8, v7, Lcom/google/t/b/b/a/x;->a:J

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_3
    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/google/android/location/e/bi;->a:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/t/b/b/a/d;->w:J

    sub-long/2addr v6, v8

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/t/b/b/a/d;->u:I

    int-to-long v8, v4

    cmp-long v4, v6, v8

    if-gez v4, :cond_9

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/t/b/b/a/d;->s:F

    const/4 v6, 0x1

    cmpl-float v4, v4, v6

    if-lez v4, :cond_9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/t/b/b/a/d;->x:Lcom/google/t/b/b/a/u;

    invoke-virtual {v10}, Lcom/google/t/b/b/a/u;->a()V

    iget v4, v10, Lcom/google/t/b/b/a/u;->b:I

    add-int/2addr v4, v5

    invoke-virtual {v10, v4}, Lcom/google/t/b/b/a/u;->a(I)V

    const/4 v7, 0x0

    iget v6, v10, Lcom/google/t/b/b/a/u;->b:I

    const/4 v4, 0x0

    :goto_2
    iget v8, v10, Lcom/google/t/b/b/a/u;->b:I

    if-ge v4, v8, :cond_5

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/t/b/b/a/d;->s:F

    iget-object v9, v10, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v9, v9, v4

    iget v9, v9, Lcom/google/t/b/b/a/x;->b:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/t/b/b/a/d;->h:I

    sub-int/2addr v9, v11

    int-to-float v9, v9

    mul-float/2addr v8, v9

    move v9, v7

    :goto_3
    if-ge v9, v5, :cond_4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/t/b/b/a/d;->y:Lcom/google/t/b/b/a/u;

    iget-object v7, v7, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v7, v7, v9

    iget-wide v12, v7, Lcom/google/t/b/b/a/x;->a:J

    iget-object v7, v10, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v7, v7, v4

    iget-wide v14, v7, Lcom/google/t/b/b/a/x;->a:J

    cmp-long v7, v12, v14

    if-gez v7, :cond_4

    iget-object v7, v10, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v7, v7, v6

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/t/b/b/a/d;->y:Lcom/google/t/b/b/a/u;

    iget-object v11, v11, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v11, v11, v9

    iget-wide v12, v11, Lcom/google/t/b/b/a/x;->a:J

    iput-wide v12, v7, Lcom/google/t/b/b/a/x;->a:J

    iget-object v7, v10, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v7, v7, v6

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/t/b/b/a/d;->y:Lcom/google/t/b/b/a/u;

    iget-object v11, v11, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v11, v11, v9

    iget v11, v11, Lcom/google/t/b/b/a/x;->b:I

    iput v11, v7, Lcom/google/t/b/b/a/x;->b:I

    add-int/lit8 v9, v9, 0x1

    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_4
    if-ge v9, v5, :cond_1c

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/t/b/b/a/d;->y:Lcom/google/t/b/b/a/u;

    iget-object v7, v7, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v7, v7, v9

    iget-wide v12, v7, Lcom/google/t/b/b/a/x;->a:J

    iget-object v7, v10, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v7, v7, v4

    iget-wide v14, v7, Lcom/google/t/b/b/a/x;->a:J

    cmp-long v7, v12, v14

    if-nez v7, :cond_1c

    const/high16 v7, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/t/b/b/a/d;->s:F

    sub-float/2addr v7, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/t/b/b/a/d;->y:Lcom/google/t/b/b/a/u;

    iget-object v11, v11, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v11, v11, v9

    iget v11, v11, Lcom/google/t/b/b/a/x;->b:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/t/b/b/a/d;->h:I

    sub-int/2addr v11, v12

    int-to-float v11, v11

    mul-float/2addr v7, v11

    add-float/2addr v7, v8

    add-int/lit8 v8, v9, 0x1

    move/from16 v22, v7

    move v7, v8

    move/from16 v8, v22

    :goto_4
    iget-object v9, v10, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v9, v9, v4

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/t/b/b/a/d;->h:I

    add-int/2addr v8, v11

    iput v8, v9, Lcom/google/t/b/b/a/x;->b:I

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    :cond_5
    :goto_5
    if-ge v7, v5, :cond_6

    iget-object v4, v10, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v4, v4, v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/t/b/b/a/d;->y:Lcom/google/t/b/b/a/u;

    iget-object v8, v8, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v8, v8, v7

    iget-wide v8, v8, Lcom/google/t/b/b/a/x;->a:J

    iput-wide v8, v4, Lcom/google/t/b/b/a/x;->a:J

    iget-object v4, v10, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v4, v4, v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/t/b/b/a/d;->y:Lcom/google/t/b/b/a/u;

    iget-object v8, v8, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v8, v8, v7

    iget v8, v8, Lcom/google/t/b/b/a/x;->b:I

    iput v8, v4, Lcom/google/t/b/b/a/x;->b:I

    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    :cond_6
    iput v6, v10, Lcom/google/t/b/b/a/u;->b:I

    invoke-virtual {v10}, Lcom/google/t/b/b/a/u;->b()V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/t/b/b/a/d;->t:I

    invoke-static {v6, v4}, Ljava/lang/Math;->min(II)I

    move-result v5

    iput v5, v10, Lcom/google/t/b/b/a/u;->b:I

    const/4 v4, 0x0

    :goto_6
    if-ge v4, v5, :cond_7

    iget-object v6, v10, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v6, v6, v4

    iget v6, v6, Lcom/google/t/b/b/a/x;->b:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/t/b/b/a/d;->i:I

    if-ge v6, v7, :cond_8

    iput v4, v10, Lcom/google/t/b/b/a/u;->b:I

    :cond_7
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/google/t/b/b/a/d;->x:Lcom/google/t/b/b/a/u;

    :goto_7
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/google/android/location/e/bi;->a:J

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/google/t/b/b/a/d;->w:J

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/t/b/b/a/d;->x:Lcom/google/t/b/b/a/u;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/t/b/b/a/d;->x:Lcom/google/t/b/b/a/u;

    .line 291
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/t/b/b/a/d;->x:Lcom/google/t/b/b/a/u;

    invoke-virtual {v4, v5}, Lcom/google/t/b/b/a/b;->a(Lcom/google/t/b/b/a/u;)Ljava/util/Set;

    move-result-object v5

    .line 294
    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/t/b/b/a/d;->d:I

    if-lt v4, v6, :cond_c

    const/4 v4, 0x1

    :goto_8
    if-nez v4, :cond_d

    .line 295
    const/4 v4, 0x0

    .line 401
    :goto_9
    return-object v4

    .line 287
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :cond_9
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/t/b/b/a/d;->x:Lcom/google/t/b/b/a/u;

    invoke-virtual {v8, v5}, Lcom/google/t/b/b/a/u;->a(I)V

    iput v5, v8, Lcom/google/t/b/b/a/u;->b:I

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v4, 0x0

    move/from16 v22, v4

    move v4, v6

    move/from16 v6, v22

    :goto_a
    if-ge v6, v5, :cond_b

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/t/b/b/a/d;->y:Lcom/google/t/b/b/a/u;

    iget-object v9, v9, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v9, v9, v7

    iget-wide v10, v9, Lcom/google/t/b/b/a/x;->a:J

    :goto_b
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/t/b/b/a/d;->y:Lcom/google/t/b/b/a/u;

    iget v9, v9, Lcom/google/t/b/b/a/u;->b:I

    if-ge v4, v9, :cond_a

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/t/b/b/a/d;->y:Lcom/google/t/b/b/a/u;

    iget-object v9, v9, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v9, v9, v4

    iget-wide v12, v9, Lcom/google/t/b/b/a/x;->a:J

    cmp-long v9, v10, v12

    if-nez v9, :cond_a

    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    :cond_a
    iget-object v9, v8, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v9, v9, v6

    iput-wide v10, v9, Lcom/google/t/b/b/a/x;->a:J

    iget-object v9, v8, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v9, v9, v6

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/t/b/b/a/d;->y:Lcom/google/t/b/b/a/u;

    invoke-virtual {v10, v7, v4}, Lcom/google/t/b/b/a/u;->a(II)I

    move-result v7

    iput v7, v9, Lcom/google/t/b/b/a/x;->b:I

    add-int/lit8 v7, v4, 0x1

    add-int/lit8 v6, v6, 0x1

    move/from16 v22, v7

    move v7, v4

    move/from16 v4, v22

    goto :goto_a

    :cond_b
    invoke-virtual {v8}, Lcom/google/t/b/b/a/u;->b()V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/t/b/b/a/d;->t:I

    invoke-static {v5, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    iput v4, v8, Lcom/google/t/b/b/a/u;->b:I

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/t/b/b/a/d;->x:Lcom/google/t/b/b/a/u;

    goto/16 :goto_7

    .line 294
    :cond_c
    const/4 v4, 0x0

    goto :goto_8

    .line 300
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    invoke-virtual {v4, v5}, Lcom/google/t/b/b/a/b;->a(Ljava/util/Set;)Lcom/google/t/b/b/a/h;

    move-result-object v15

    .line 303
    iget v4, v15, Lcom/google/t/b/b/a/h;->b:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/t/b/b/a/d;->e:I

    if-lt v4, v5, :cond_e

    const/4 v4, 0x1

    :goto_c
    if-nez v4, :cond_f

    .line 304
    const/4 v4, 0x0

    goto/16 :goto_9

    .line 303
    :cond_e
    const/4 v4, 0x0

    goto :goto_c

    .line 308
    :cond_f
    const-wide/16 v16, 0x0

    .line 309
    const/4 v14, 0x0

    .line 310
    const-wide/16 v12, 0x0

    .line 311
    const/4 v11, 0x0

    .line 312
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/t/b/b/a/d;->c:I

    mul-int/lit8 v4, v4, 0x4

    invoke-virtual {v15, v4}, Lcom/google/t/b/b/a/h;->a(I)V

    .line 315
    const/16 v10, 0x12

    .line 316
    :goto_d
    const/16 v4, 0x15

    if-ge v10, v4, :cond_10

    .line 317
    iget v4, v15, Lcom/google/t/b/b/a/h;->b:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/t/b/b/a/d;->c:I

    if-gt v4, v5, :cond_10

    .line 318
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v4}, Lcom/google/t/b/b/a/d;->a(Lcom/google/t/b/b/a/h;Z)Lcom/google/t/b/b/a/h;

    move-result-object v15

    .line 316
    add-int/lit8 v10, v10, 0x1

    goto :goto_d

    .line 324
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/t/b/b/a/d;->x:Lcom/google/t/b/b/a/u;

    invoke-virtual {v4}, Lcom/google/t/b/b/a/u;->a()V

    .line 325
    const-wide/16 v6, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/t/b/b/a/d;->x:Lcom/google/t/b/b/a/u;

    iget v0, v4, Lcom/google/t/b/b/a/u;->b:I

    move/from16 v19, v0

    const/4 v4, 0x0

    move/from16 v18, v4

    move-wide/from16 v20, v6

    :goto_e
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_11

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/t/b/b/a/d;->x:Lcom/google/t/b/b/a/u;

    iget-object v4, v4, Lcom/google/t/b/b/a/u;->a:[Lcom/google/t/b/b/a/x;

    aget-object v4, v4, v18

    iget v4, v4, Lcom/google/t/b/b/a/x;->b:I

    int-to-double v4, v4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/t/b/b/a/d;->p:F

    float-to-double v6, v6

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/t/b/b/a/d;->q:F

    float-to-double v8, v8

    invoke-static/range {v4 .. v9}, Lcom/google/t/b/b/a/d;->a(DDD)D

    move-result-wide v4

    add-double v6, v20, v4

    add-int/lit8 v4, v18, 0x1

    move/from16 v18, v4

    move-wide/from16 v20, v6

    goto :goto_e

    :cond_11
    move/from16 v0, v19

    int-to-double v4, v0

    div-double v4, v20, v4

    sget-wide v6, Lcom/google/t/b/b/a/d;->a:D

    add-double v18, v4, v6

    move v6, v11

    move-object v7, v14

    move-wide/from16 v8, v16

    move-object v14, v15

    .line 328
    :goto_f
    const/16 v4, 0x15

    if-gt v10, v4, :cond_17

    .line 331
    const/4 v4, 0x0

    :goto_10
    iget v5, v14, Lcom/google/t/b/b/a/h;->b:I

    if-ge v4, v5, :cond_15

    .line 332
    iget-object v5, v14, Lcom/google/t/b/b/a/h;->a:[Lcom/google/t/b/b/a/j;

    aget-object v11, v5, v4

    .line 334
    :try_start_0
    iget-object v5, v11, Lcom/google/t/b/b/a/j;->b:[B

    iget-wide v0, v11, Lcom/google/t/b/b/a/j;->a:J

    move-wide/from16 v16, v0

    invoke-static {v5}, Lcom/google/t/b/b/a/l;->a([B)I

    move-result v15

    packed-switch v15, :pswitch_data_0

    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v15, "Unknown protocol version on getPredictiveCount."

    invoke-direct {v5, v15}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 341
    :catch_0
    move-exception v5

    .line 342
    sget-boolean v15, Lcom/google/android/location/i/a;->e:Z

    if-eqz v15, :cond_12

    const-string v15, "GpwleOneShotLocalizer"

    invoke-virtual {v5}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v15, v5}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    :cond_12
    const-wide/high16 v16, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    move-wide/from16 v0, v16

    iput-wide v0, v11, Lcom/google/t/b/b/a/j;->c:D

    .line 331
    :cond_13
    :goto_11
    add-int/lit8 v4, v4, 0x1

    goto :goto_10

    .line 334
    :pswitch_0
    :try_start_1
    move-wide/from16 v0, v16

    invoke-static {v5, v0, v1}, Lcom/google/t/b/b/a/m;->b([BJ)I

    move-result v5

    .line 338
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/t/b/b/a/d;->a(I)Z

    move-result v15

    if-eqz v15, :cond_13

    .line 339
    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v11, v5, v1, v2}, Lcom/google/t/b/b/a/d;->a(Lcom/google/t/b/b/a/j;ID)D

    move-result-wide v16

    move-wide/from16 v0, v16

    iput-wide v0, v11, Lcom/google/t/b/b/a/j;->c:D
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_11

    .line 344
    :catch_1
    move-exception v5

    .line 345
    sget-boolean v15, Lcom/google/android/location/i/a;->e:Z

    if-eqz v15, :cond_14

    const-string v15, "GpwleOneShotLocalizer"

    invoke-virtual {v5}, Ljava/lang/IndexOutOfBoundsException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v15, v5}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    :cond_14
    const-wide/high16 v16, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    move-wide/from16 v0, v16

    iput-wide v0, v11, Lcom/google/t/b/b/a/j;->c:D

    goto :goto_11

    .line 351
    :cond_15
    invoke-virtual {v14}, Lcom/google/t/b/b/a/h;->a()V

    .line 352
    iget-object v4, v14, Lcom/google/t/b/b/a/h;->a:[Lcom/google/t/b/b/a/j;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    .line 355
    iget-wide v0, v4, Lcom/google/t/b/b/a/j;->c:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v1, v2}, Lcom/google/t/b/b/a/d;->a(D)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 356
    if-eqz v7, :cond_16

    iget-wide v0, v4, Lcom/google/t/b/b/a/j;->c:D

    move-wide/from16 v16, v0

    cmpl-double v5, v16, v12

    if-ltz v5, :cond_1b

    .line 361
    :cond_16
    iget-wide v6, v4, Lcom/google/t/b/b/a/j;->a:J

    .line 362
    iget-object v5, v4, Lcom/google/t/b/b/a/j;->b:[B

    .line 363
    iget-wide v12, v4, Lcom/google/t/b/b/a/j;->c:D

    .line 364
    iget v4, v4, Lcom/google/t/b/b/a/j;->d:I

    .line 368
    :goto_12
    const/16 v8, 0x15

    if-eq v10, v8, :cond_18

    .line 369
    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v8}, Lcom/google/t/b/b/a/d;->a(Lcom/google/t/b/b/a/h;Z)Lcom/google/t/b/b/a/h;

    move-result-object v9

    .line 328
    add-int/lit8 v8, v10, 0x1

    move v10, v8

    move-object v14, v9

    move-wide v8, v6

    move-object v7, v5

    move v6, v4

    goto/16 :goto_f

    :cond_17
    move v4, v6

    move-object v5, v7

    move-wide v6, v8

    .line 377
    :cond_18
    if-nez v5, :cond_19

    .line 378
    const/4 v4, 0x0

    goto/16 :goto_9

    .line 383
    :cond_19
    :try_start_2
    invoke-static {v6, v7}, Lcom/google/t/b/b/a/n;->b(J)[J

    move-result-object v8

    .line 385
    invoke-static {v5}, Lcom/google/t/b/b/a/l;->a([B)I

    move-result v9

    packed-switch v9, :pswitch_data_1

    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Unknown protocol version on getLevelKey."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_3

    .line 396
    :catch_2
    move-exception v4

    .line 397
    sget-boolean v5, Lcom/google/android/location/i/a;->e:Z

    if-eqz v5, :cond_1a

    const-string v5, "GpwleOneShotLocalizer"

    invoke-virtual {v4}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    :cond_1a
    :goto_13
    const/4 v4, 0x0

    goto/16 :goto_9

    .line 385
    :pswitch_1
    :try_start_3
    invoke-static {v5, v6, v7}, Lcom/google/t/b/b/a/m;->a([BJ)Ljava/lang/String;

    move-result-object v10

    .line 386
    invoke-static {v5}, Lcom/google/t/b/b/a/l;->c([B)I

    move-result v11

    .line 387
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v4, v8}, Lcom/google/t/b/b/a/d;->a(Lcom/google/t/b/b/a/h;I[J)I

    move-result v7

    .line 389
    new-instance v4, Lcom/google/android/location/e/n;

    const/4 v5, 0x0

    aget-wide v12, v8, v5

    long-to-int v5, v12

    const/4 v6, 0x1

    aget-wide v8, v8, v6

    long-to-int v6, v8

    const/16 v8, 0x6d

    const/4 v9, 0x0

    invoke-direct/range {v4 .. v11}, Lcom/google/android/location/e/n;-><init>(IIIILjava/lang/String;Ljava/lang/String;I)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_9

    .line 398
    :catch_3
    move-exception v4

    .line 399
    sget-boolean v5, Lcom/google/android/location/i/a;->e:Z

    if-eqz v5, :cond_1a

    const-string v5, "GpwleOneShotLocalizer"

    invoke-virtual {v4}, Ljava/lang/IndexOutOfBoundsException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/google/android/location/o/a/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_13

    :cond_1b
    move v4, v6

    move-object v5, v7

    move-wide v6, v8

    goto :goto_12

    :cond_1c
    move v7, v9

    goto/16 :goto_4

    .line 334
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 385
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/b;->a()I

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/location/e/bi;)Lcom/google/android/location/e/bf;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 257
    if-nez p1, :cond_1

    .line 276
    :cond_0
    :goto_0
    return-object v7

    .line 262
    :cond_1
    iget-object v0, p0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/b;->r()Lcom/google/android/location/j/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v0

    .line 265
    invoke-direct {p0, p1}, Lcom/google/t/b/b/a/d;->b(Lcom/google/android/location/e/bi;)Lcom/google/android/location/e/n;

    move-result-object v2

    .line 266
    iget-object v3, p0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    invoke-virtual {v3}, Lcom/google/t/b/b/a/b;->p()V

    .line 267
    iget-object v3, p0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    invoke-virtual {v3}, Lcom/google/t/b/b/a/b;->o()V

    .line 270
    iget-object v3, p0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    invoke-virtual {v3}, Lcom/google/t/b/b/a/b;->r()Lcom/google/android/location/j/b;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v4

    sub-long v0, v4, v0

    long-to-int v0, v0

    iput v0, p0, Lcom/google/t/b/b/a/d;->A:I

    .line 271
    if-nez v2, :cond_2

    .line 272
    iget v0, p0, Lcom/google/t/b/b/a/d;->A:I

    iput v0, p0, Lcom/google/t/b/b/a/d;->C:I

    .line 276
    :goto_1
    if-eqz v2, :cond_0

    new-instance v0, Lcom/google/android/location/e/bf;

    const/4 v1, 0x4

    sget-object v3, Lcom/google/android/location/e/ab;->a:Lcom/google/android/location/e/ab;

    iget-object v4, p0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    invoke-virtual {v4}, Lcom/google/t/b/b/a/b;->r()Lcom/google/android/location/j/b;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/location/j/b;->c()J

    move-result-wide v4

    move-object v6, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/e/bf;-><init>(ILcom/google/android/location/e/al;Lcom/google/android/location/e/ab;JLcom/google/android/location/e/bi;Ljava/util/Map;)V

    move-object v7, v0

    goto :goto_0

    .line 274
    :cond_2
    iget v0, p0, Lcom/google/t/b/b/a/d;->A:I

    iput v0, p0, Lcom/google/t/b/b/a/d;->B:I

    goto :goto_1
.end method

.method public final a(Lcom/google/p/a/b/b/a;)V
    .locals 6

    .prologue
    const/16 v5, 0x36

    const/16 v4, 0x35

    const/16 v3, 0x34

    const/16 v2, 0x33

    const/16 v1, 0x32

    .line 705
    invoke-virtual {p1, v1}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 706
    invoke-virtual {p1, v1}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/d;->c:I

    .line 708
    :cond_0
    invoke-virtual {p1, v2}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 709
    invoke-virtual {p1, v2}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/d;->d:I

    .line 711
    :cond_1
    invoke-virtual {p1, v3}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 712
    invoke-virtual {p1, v3}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/d;->e:I

    .line 715
    :cond_2
    invoke-virtual {p1, v4}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 716
    invoke-virtual {p1, v4}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/d;->f:I

    .line 718
    :cond_3
    invoke-virtual {p1, v5}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 719
    invoke-virtual {p1, v5}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/d;->g:I

    .line 721
    :cond_4
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 722
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/d;->h:I

    .line 724
    :cond_5
    const/16 v0, 0x40

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 725
    const/16 v0, 0x40

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/d;->i:I

    .line 728
    :cond_6
    const/16 v0, 0x42

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 729
    const/16 v0, 0x42

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/d;->j:I

    .line 731
    :cond_7
    const/16 v0, 0x43

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 732
    const/16 v0, 0x43

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/d;->k:I

    .line 734
    :cond_8
    const/16 v0, 0x44

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 735
    const/16 v0, 0x44

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->e(I)F

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/d;->l:F

    .line 737
    :cond_9
    const/16 v0, 0x45

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 738
    const/16 v0, 0x45

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->e(I)F

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/d;->m:F

    .line 740
    :cond_a
    const/16 v0, 0x46

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 741
    const/16 v0, 0x46

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->e(I)F

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/d;->n:F

    .line 743
    :cond_b
    const/16 v0, 0x38

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 744
    const/16 v0, 0x38

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/d;->o:I

    .line 746
    :cond_c
    const/16 v0, 0x39

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 747
    const/16 v0, 0x39

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->e(I)F

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/d;->p:F

    .line 749
    :cond_d
    const/16 v0, 0x3a

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 750
    const/16 v0, 0x3a

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->e(I)F

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/d;->q:F

    .line 752
    :cond_e
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 753
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->e(I)F

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/d;->r:F

    .line 755
    :cond_f
    const/16 v0, 0x41

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 756
    const/16 v0, 0x41

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->e(I)F

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/d;->s:F

    .line 758
    :cond_10
    const/16 v0, 0x49

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 759
    const/16 v0, 0x49

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/d;->t:I

    .line 761
    :cond_11
    const/16 v0, 0x4a

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 762
    const/16 v0, 0x4a

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/t/b/b/a/d;->u:I

    .line 765
    :cond_12
    const/16 v0, 0x3f

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 766
    const/16 v0, 0x3f

    invoke-virtual {p1, v0}, Lcom/google/p/a/b/b/a;->b(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/t/b/b/a/d;->v:Z

    .line 768
    :cond_13
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/b;->b()I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/b;->c()I

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/b;->d()I

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/b;->e()Z

    move-result v0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/b;->f()I

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/b;->g()I

    move-result v0

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/b;->h()I

    move-result v0

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/b;->i()I

    move-result v0

    return v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/b;->j()I

    move-result v0

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/b;->k()I

    move-result v0

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/b;->l()Z

    move-result v0

    return v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/b;->m()I

    move-result v0

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/b;->n()I

    move-result v0

    return v0
.end method

.method public final o()I
    .locals 2

    .prologue
    .line 215
    iget v0, p0, Lcom/google/t/b/b/a/d;->A:I

    .line 216
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/t/b/b/a/d;->A:I

    .line 217
    return v0
.end method

.method public final p()I
    .locals 2

    .prologue
    .line 225
    iget v0, p0, Lcom/google/t/b/b/a/d;->B:I

    .line 226
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/t/b/b/a/d;->B:I

    .line 227
    return v0
.end method

.method public final q()I
    .locals 2

    .prologue
    .line 235
    iget v0, p0, Lcom/google/t/b/b/a/d;->C:I

    .line 236
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/t/b/b/a/d;->C:I

    .line 237
    return v0
.end method

.method public final r()V
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/t/b/b/a/d;->z:Lcom/google/t/b/b/a/b;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/b;->q()V

    .line 245
    return-void
.end method
