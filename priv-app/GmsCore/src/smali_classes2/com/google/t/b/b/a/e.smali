.class public final Lcom/google/t/b/b/a/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final c:Lcom/google/android/location/e/ak;


# instance fields
.field public final a:I

.field public final b:J

.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/t/b/b/a/f;

    invoke-direct {v0}, Lcom/google/t/b/b/a/f;-><init>()V

    sput-object v0, Lcom/google/t/b/b/a/e;->c:Lcom/google/android/location/e/ak;

    return-void
.end method

.method public constructor <init>(IJ)V
    .locals 6

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput p1, p0, Lcom/google/t/b/b/a/e;->a:I

    .line 37
    iput-wide p2, p0, Lcom/google/t/b/b/a/e;->b:J

    .line 38
    iget v0, p0, Lcom/google/t/b/b/a/e;->a:I

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/t/b/b/a/e;->b:J

    iget-wide v4, p0, Lcom/google/t/b/b/a/e;->b:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/t/b/b/a/e;->d:I

    .line 39
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 43
    if-ne p1, p0, :cond_1

    .line 50
    :cond_0
    :goto_0
    return v0

    .line 46
    :cond_1
    instance-of v2, p1, Lcom/google/t/b/b/a/e;

    if-nez v2, :cond_2

    move v0, v1

    .line 47
    goto :goto_0

    .line 49
    :cond_2
    check-cast p1, Lcom/google/t/b/b/a/e;

    .line 50
    iget v2, p0, Lcom/google/t/b/b/a/e;->a:I

    iget v3, p1, Lcom/google/t/b/b/a/e;->a:I

    if-ne v2, v3, :cond_3

    iget-wide v2, p0, Lcom/google/t/b/b/a/e;->b:J

    iget-wide v4, p1, Lcom/google/t/b/b/a/e;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/google/t/b/b/a/e;->d:I

    return v0
.end method
