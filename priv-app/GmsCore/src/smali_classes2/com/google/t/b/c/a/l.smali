.class final Lcom/google/t/b/c/a/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/io/File;

.field final synthetic b:Lcom/google/t/b/c/a/k;


# direct methods
.method constructor <init>(Lcom/google/t/b/c/a/k;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/google/t/b/c/a/l;->b:Lcom/google/t/b/c/a/k;

    iput-object p2, p0, Lcom/google/t/b/c/a/l;->a:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 221
    new-instance v2, Lcom/google/t/b/c/a/u;

    iget-object v0, p0, Lcom/google/t/b/c/a/l;->b:Lcom/google/t/b/c/a/k;

    iget-object v0, v0, Lcom/google/t/b/c/a/k;->k:Lcom/google/t/b/c/a/t;

    iget-object v1, p0, Lcom/google/t/b/c/a/l;->b:Lcom/google/t/b/c/a/k;

    iget v1, v1, Lcom/google/t/b/c/a/k;->m:I

    invoke-direct {v2, v0, v1}, Lcom/google/t/b/c/a/u;-><init>(Lcom/google/t/b/c/a/v;I)V

    .line 223
    const/4 v0, 0x1

    .line 225
    :try_start_0
    iget-object v1, p0, Lcom/google/t/b/c/a/l;->b:Lcom/google/t/b/c/a/k;

    iget-object v3, p0, Lcom/google/t/b/c/a/l;->a:Ljava/io/File;

    invoke-virtual {v1, v3, v2}, Lcom/google/t/b/c/a/k;->a(Ljava/io/File;Lcom/google/t/b/c/a/u;)Lcom/google/t/b/c/a/u;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 241
    :goto_0
    iget-object v2, p0, Lcom/google/t/b/c/a/l;->b:Lcom/google/t/b/c/a/k;

    iget-object v3, v2, Lcom/google/t/b/c/a/k;->k:Lcom/google/t/b/c/a/t;

    invoke-interface {v3}, Lcom/google/t/b/c/a/t;->c()Lcom/google/android/location/j/j;

    move-result-object v3

    new-instance v4, Lcom/google/t/b/c/a/m;

    invoke-direct {v4, v2, v0, v1}, Lcom/google/t/b/c/a/m;-><init>(Lcom/google/t/b/c/a/k;ZLcom/google/t/b/c/a/u;)V

    invoke-interface {v3, v4}, Lcom/google/android/location/j/j;->a(Ljava/lang/Runnable;)Z

    .line 242
    return-void

    .line 226
    :catch_0
    move-exception v1

    .line 227
    sget-boolean v3, Lcom/google/android/location/i/a;->d:Z

    if-eqz v3, :cond_0

    const-string v3, "TemporalDiskCache"

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    :cond_0
    sget-boolean v1, Lcom/google/android/location/i/a;->d:Z

    if-eqz v1, :cond_1

    const-string v1, "TemporalDiskCache"

    const-string v3, "Chunk read failed. Attempting recovery write."

    invoke-static {v1, v3}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    :cond_1
    :try_start_1
    new-instance v1, Lcom/google/t/b/c/a/u;

    iget-object v3, p0, Lcom/google/t/b/c/a/l;->b:Lcom/google/t/b/c/a/k;

    iget-object v3, v3, Lcom/google/t/b/c/a/k;->k:Lcom/google/t/b/c/a/t;

    iget-object v4, p0, Lcom/google/t/b/c/a/l;->b:Lcom/google/t/b/c/a/k;

    iget v4, v4, Lcom/google/t/b/c/a/k;->m:I

    invoke-direct {v1, v3, v4}, Lcom/google/t/b/c/a/u;-><init>(Lcom/google/t/b/c/a/v;I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 232
    :try_start_2
    iget-object v2, p0, Lcom/google/t/b/c/a/l;->b:Lcom/google/t/b/c/a/k;

    iget-object v3, p0, Lcom/google/t/b/c/a/l;->a:Ljava/io/File;

    invoke-static {v2, v3, v1}, Lcom/google/t/b/c/a/k;->a(Lcom/google/t/b/c/a/k;Ljava/io/File;Lcom/google/t/b/c/a/u;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 233
    :catch_1
    move-exception v0

    .line 236
    :goto_1
    sget-boolean v2, Lcom/google/android/location/i/a;->d:Z

    if-eqz v2, :cond_2

    const-string v2, "TemporalDiskCache"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    :cond_2
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_3

    const-string v0, "TemporalDiskCache"

    const-string v2, "Recovery write failed. Deactivating."

    invoke-static {v0, v2}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 233
    :catch_2
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method
