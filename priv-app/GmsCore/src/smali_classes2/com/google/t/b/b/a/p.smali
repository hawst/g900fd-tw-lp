.class public final Lcom/google/t/b/b/a/p;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final d:Lcom/google/android/location/e/ak;

.field private static final e:Lcom/google/t/b/b/a/p;


# instance fields
.field public final a:I

.field public final b:J

.field public final c:I

.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 22
    new-instance v0, Lcom/google/t/b/b/a/p;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v2, v3, v1}, Lcom/google/t/b/b/a/p;-><init>(IJI)V

    sput-object v0, Lcom/google/t/b/b/a/p;->e:Lcom/google/t/b/b/a/p;

    .line 31
    new-instance v0, Lcom/google/t/b/b/a/q;

    invoke-direct {v0}, Lcom/google/t/b/b/a/q;-><init>()V

    sput-object v0, Lcom/google/t/b/b/a/p;->d:Lcom/google/android/location/e/ak;

    return-void
.end method

.method public constructor <init>(IJI)V
    .locals 8

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput p1, p0, Lcom/google/t/b/b/a/p;->a:I

    .line 50
    iput-wide p2, p0, Lcom/google/t/b/b/a/p;->b:J

    .line 51
    iput p4, p0, Lcom/google/t/b/b/a/p;->c:I

    .line 52
    iget v0, p0, Lcom/google/t/b/b/a/p;->a:I

    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/t/b/b/a/p;->c:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/t/b/b/a/p;->b:J

    iget-wide v4, p0, Lcom/google/t/b/b/a/p;->b:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/t/b/b/a/p;->f:I

    .line 53
    return-void
.end method

.method public static a(Lcom/google/p/a/b/b/a;)Lcom/google/t/b/b/a/p;
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 56
    if-eqz p0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v4}, Lcom/google/p/a/b/b/a;->i(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 59
    :cond_0
    sget-boolean v0, Lcom/google/android/location/i/a;->d:Z

    if-eqz v0, :cond_1

    const-string v0, "SpatialKey"

    const-string v1, "Returned default key for ill formed proto."

    invoke-static {v0, v1}, Lcom/google/android/location/o/a/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    :cond_1
    sget-object v0, Lcom/google/t/b/b/a/p;->e:Lcom/google/t/b/b/a/p;

    .line 63
    :goto_0
    return-object v0

    :cond_2
    new-instance v0, Lcom/google/t/b/b/a/p;

    invoke-virtual {p0, v1}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v1

    invoke-virtual {p0, v2}, Lcom/google/p/a/b/b/a;->d(I)J

    move-result-wide v2

    invoke-virtual {p0, v4}, Lcom/google/p/a/b/b/a;->c(I)I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/t/b/b/a/p;-><init>(IJI)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/p/a/b/b/a;
    .locals 4

    .prologue
    .line 69
    new-instance v0, Lcom/google/p/a/b/b/a;

    sget-object v1, Lcom/google/android/location/m/a;->aP:Lcom/google/p/a/b/b/c;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/a;-><init>(Lcom/google/p/a/b/b/c;)V

    .line 70
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/t/b/b/a/p;->a:I

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 71
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/t/b/b/a/p;->b:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/p/a/b/b/a;->b(IJ)Lcom/google/p/a/b/b/a;

    .line 72
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/t/b/b/a/p;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/a;->f(II)Lcom/google/p/a/b/b/a;

    .line 73
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 78
    if-ne p1, p0, :cond_1

    .line 85
    :cond_0
    :goto_0
    return v0

    .line 81
    :cond_1
    instance-of v2, p1, Lcom/google/t/b/b/a/p;

    if-nez v2, :cond_2

    move v0, v1

    .line 82
    goto :goto_0

    .line 84
    :cond_2
    check-cast p1, Lcom/google/t/b/b/a/p;

    .line 85
    iget v2, p0, Lcom/google/t/b/b/a/p;->a:I

    iget v3, p1, Lcom/google/t/b/b/a/p;->a:I

    if-ne v2, v3, :cond_3

    iget-wide v2, p0, Lcom/google/t/b/b/a/p;->b:J

    iget-wide v4, p1, Lcom/google/t/b/b/a/p;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/t/b/b/a/p;->c:I

    iget v3, p1, Lcom/google/t/b/b/a/p;->c:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/google/t/b/b/a/p;->f:I

    return v0
.end method
