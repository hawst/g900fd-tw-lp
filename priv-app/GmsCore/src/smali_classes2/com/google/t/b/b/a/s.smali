.class public final Lcom/google/t/b/b/a/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/t/b/c/a/v;


# static fields
.field private static final b:Lcom/google/t/b/b/a/s;

.field private static final c:Ljava/lang/Boolean;


# instance fields
.field protected a:Lcom/google/t/b/b/a/b;

.field private d:Lcom/google/t/b/c/a/u;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/t/b/b/a/s;

    invoke-direct {v0}, Lcom/google/t/b/b/a/s;-><init>()V

    sput-object v0, Lcom/google/t/b/b/a/s;->b:Lcom/google/t/b/b/a/s;

    .line 25
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object v0, Lcom/google/t/b/b/a/s;->c:Ljava/lang/Boolean;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/t/b/b/a/b;)Lcom/google/t/b/b/a/s;
    .locals 4

    .prologue
    .line 40
    sget-object v0, Lcom/google/t/b/b/a/s;->b:Lcom/google/t/b/b/a/s;

    iput-object p0, v0, Lcom/google/t/b/b/a/s;->a:Lcom/google/t/b/b/a/b;

    .line 41
    sget-object v0, Lcom/google/t/b/b/a/s;->b:Lcom/google/t/b/b/a/s;

    new-instance v1, Lcom/google/t/b/c/a/u;

    sget-object v2, Lcom/google/t/b/b/a/s;->b:Lcom/google/t/b/b/a/s;

    const/16 v3, 0x200

    invoke-direct {v1, v2, v3}, Lcom/google/t/b/c/a/u;-><init>(Lcom/google/t/b/c/a/v;I)V

    iput-object v1, v0, Lcom/google/t/b/b/a/s;->d:Lcom/google/t/b/c/a/u;

    .line 44
    sget-object v0, Lcom/google/t/b/b/a/s;->b:Lcom/google/t/b/b/a/s;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/location/j/b;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/t/b/b/a/s;->a:Lcom/google/t/b/b/a/b;

    invoke-virtual {v0}, Lcom/google/t/b/b/a/b;->r()Lcom/google/android/location/j/b;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;
    .locals 6

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/t/b/b/a/s;->d:Lcom/google/t/b/c/a/u;

    const/16 v1, 0x80

    invoke-static {v0, v1}, Lcom/google/t/b/b/a/a;->a(Lcom/google/t/b/c/a/u;I)Ljava/util/Iterator;

    move-result-object v1

    .line 78
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    const/4 v2, 0x1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/p/a/b/b/a;->a(IJ)V

    goto :goto_0

    .line 81
    :cond_0
    return-object p1
.end method

.method public final a(Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/t/b/b/a/s;->d:Lcom/google/t/b/c/a/u;

    invoke-virtual {v0, p1}, Lcom/google/t/b/c/a/u;->b(Ljava/lang/Object;)V

    .line 69
    return-void
.end method

.method public final b(Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/t/b/b/a/s;->d:Lcom/google/t/b/c/a/u;

    invoke-virtual {v0, p1}, Lcom/google/t/b/c/a/u;->c(Ljava/lang/Object;)Lcom/google/android/location/e/ax;

    .line 73
    return-void
.end method

.method public final q()Lcom/google/android/location/e/ak;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/google/android/location/e/aq;->b:Lcom/google/android/location/e/ak;

    return-object v0
.end method

.method public final r()Lcom/google/android/location/e/ak;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/google/t/b/c/a/c;->d:Lcom/google/android/location/e/ak;

    return-object v0
.end method

.method public final bridge synthetic s()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/t/b/b/a/s;->c:Ljava/lang/Boolean;

    return-object v0
.end method
