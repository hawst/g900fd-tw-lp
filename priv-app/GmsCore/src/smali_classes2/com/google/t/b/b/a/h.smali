.class final Lcom/google/t/b/b/a/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final c:Ljava/util/Comparator;


# instance fields
.field public a:[Lcom/google/t/b/b/a/j;

.field public b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/google/t/b/b/a/i;

    invoke-direct {v0}, Lcom/google/t/b/b/a/i;-><init>()V

    sput-object v0, Lcom/google/t/b/b/a/h;->c:Ljava/util/Comparator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/t/b/b/a/j;

    iput-object v0, p0, Lcom/google/t/b/b/a/h;->a:[Lcom/google/t/b/b/a/j;

    .line 80
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/t/b/b/a/h;->a:[Lcom/google/t/b/b/a/j;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/t/b/b/a/h;->b:I

    sget-object v3, Lcom/google/t/b/b/a/h;->c:Ljava/util/Comparator;

    invoke-static {v0, v1, v2, v3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 64
    return-void
.end method

.method public final a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 47
    iget-object v0, p0, Lcom/google/t/b/b/a/h;->a:[Lcom/google/t/b/b/a/j;

    array-length v0, v0

    if-lt v0, p1, :cond_0

    .line 57
    :goto_0
    return-void

    .line 51
    :cond_0
    new-array v1, p1, [Lcom/google/t/b/b/a/j;

    .line 52
    iget-object v0, p0, Lcom/google/t/b/b/a/h;->a:[Lcom/google/t/b/b/a/j;

    iget-object v2, p0, Lcom/google/t/b/b/a/h;->a:[Lcom/google/t/b/b/a/j;

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 53
    iget-object v0, p0, Lcom/google/t/b/b/a/h;->a:[Lcom/google/t/b/b/a/j;

    array-length v0, v0

    :goto_1
    if-ge v0, p1, :cond_1

    .line 54
    new-instance v2, Lcom/google/t/b/b/a/j;

    invoke-direct {v2, v3}, Lcom/google/t/b/b/a/j;-><init>(B)V

    aput-object v2, v1, v0

    .line 53
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 56
    :cond_1
    iput-object v1, p0, Lcom/google/t/b/b/a/h;->a:[Lcom/google/t/b/b/a/j;

    goto :goto_0
.end method

.method public final b()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 70
    iput v0, p0, Lcom/google/t/b/b/a/h;->b:I

    .line 71
    iget-object v1, p0, Lcom/google/t/b/b/a/h;->a:[Lcom/google/t/b/b/a/j;

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 72
    const/4 v4, 0x0

    iput-object v4, v3, Lcom/google/t/b/b/a/j;->b:[B

    .line 71
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 74
    :cond_0
    return-void
.end method
