.class public final Lcom/google/t/b/b/a/l;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static final a([B)I
    .locals 2

    .prologue
    .line 14
    if-nez p0, :cond_0

    .line 15
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Quadtree to be parsed is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 17
    :cond_0
    const/4 v0, 0x0

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public static final b([B)J
    .locals 2

    .prologue
    .line 25
    invoke-static {p0}, Lcom/google/t/b/b/a/l;->a([B)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 29
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown protocol version on getRootS2CellId."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 27
    :pswitch_0
    invoke-static {p0}, Lcom/google/t/b/b/a/m;->a([B)J

    move-result-wide v0

    return-wide v0

    .line 25
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static final c([B)I
    .locals 2

    .prologue
    .line 38
    invoke-static {p0}, Lcom/google/t/b/b/a/l;->a([B)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 42
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown protocol version on getLayerId."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :pswitch_0
    invoke-static {p0}, Lcom/google/t/b/b/a/m;->b([B)I

    move-result v0

    return v0

    .line 38
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
