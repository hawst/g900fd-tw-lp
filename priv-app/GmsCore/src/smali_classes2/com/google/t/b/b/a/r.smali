.class public final Lcom/google/t/b/b/a/r;
.super Lcom/google/t/b/b/a/a;
.source "SourceFile"


# static fields
.field private static final g:Lcom/google/t/b/b/a/r;

.field private static final h:[B


# instance fields
.field private i:Lcom/google/t/b/b/a/s;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/t/b/b/a/r;

    invoke-direct {v0}, Lcom/google/t/b/b/a/r;-><init>()V

    sput-object v0, Lcom/google/t/b/b/a/r;->g:Lcom/google/t/b/b/a/r;

    .line 24
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lcom/google/t/b/b/a/r;->h:[B

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/google/t/b/b/a/a;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/t/b/b/a/b;)Lcom/google/t/b/b/a/r;
    .locals 4

    .prologue
    .line 43
    sget-object v0, Lcom/google/t/b/b/a/r;->g:Lcom/google/t/b/b/a/r;

    const/16 v1, 0x400

    const/16 v2, 0xf

    const/4 v3, 0x0

    invoke-virtual {v0, p0, v1, v2, v3}, Lcom/google/t/b/b/a/r;->a(Lcom/google/t/b/b/a/b;III)V

    .line 45
    sget-object v0, Lcom/google/t/b/b/a/r;->g:Lcom/google/t/b/b/a/r;

    invoke-static {p0}, Lcom/google/t/b/b/a/s;->a(Lcom/google/t/b/b/a/b;)Lcom/google/t/b/b/a/s;

    move-result-object v1

    iput-object v1, v0, Lcom/google/t/b/b/a/r;->i:Lcom/google/t/b/b/a/s;

    .line 47
    sget-object v0, Lcom/google/t/b/b/a/r;->g:Lcom/google/t/b/b/a/r;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/p/a/b/b/a;)V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x2

    const/4 v0, 0x0

    .line 87
    iget-object v1, p0, Lcom/google/t/b/b/a/r;->a:Lcom/google/t/b/b/a/b;

    invoke-virtual {v1}, Lcom/google/t/b/b/a/b;->w()I

    move-result v2

    .line 89
    invoke-virtual {p1, v8}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v3

    .line 90
    sget-boolean v1, Lcom/google/android/location/i/a;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "SpatialMappingCacheManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Received "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " UMA entries."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move v1, v0

    .line 91
    :goto_0
    if-ge v1, v3, :cond_1

    .line 92
    invoke-virtual {p1, v8, v1}, Lcom/google/p/a/b/b/a;->c(II)J

    move-result-wide v4

    .line 94
    new-instance v6, Lcom/google/t/b/b/a/e;

    iget-object v7, p0, Lcom/google/t/b/b/a/r;->a:Lcom/google/t/b/b/a/b;

    invoke-virtual {v7}, Lcom/google/t/b/b/a/b;->w()I

    move-result v7

    invoke-direct {v6, v7, v4, v5}, Lcom/google/t/b/b/a/e;-><init>(IJ)V

    .line 95
    sget-object v4, Lcom/google/t/b/b/a/r;->h:[B

    invoke-virtual {p0, v6, v4}, Lcom/google/t/b/b/a/r;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 96
    iget-object v4, p0, Lcom/google/t/b/b/a/r;->i:Lcom/google/t/b/b/a/s;

    iget-wide v6, v6, Lcom/google/t/b/b/a/e;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/t/b/b/a/s;->b(Ljava/lang/Long;)V

    .line 91
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 99
    :cond_1
    invoke-virtual {p1, v9}, Lcom/google/p/a/b/b/a;->k(I)I

    move-result v1

    .line 100
    sget-boolean v3, Lcom/google/android/location/i/a;->b:Z

    if-eqz v3, :cond_2

    const-string v3, "SpatialMappingCacheManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Received "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " SM entries."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/o/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    :cond_2
    :goto_1
    if-ge v0, v1, :cond_3

    .line 103
    const/4 v3, 0x4

    :try_start_0
    invoke-virtual {p1, v3, v0}, Lcom/google/p/a/b/b/a;->a(II)[B

    move-result-object v3

    .line 104
    invoke-static {v3}, Lcom/google/t/b/b/a/t;->a([B)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Unknown protocol version on getMacAddress."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 110
    :catch_0
    move-exception v3

    iget v3, p0, Lcom/google/t/b/b/a/r;->e:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/t/b/b/a/r;->e:I

    .line 111
    iget-object v3, p0, Lcom/google/t/b/b/a/r;->a:Lcom/google/t/b/b/a/b;

    invoke-virtual {v3}, Lcom/google/t/b/b/a/b;->x()V

    .line 101
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 104
    :pswitch_0
    const/4 v4, 0x6

    :try_start_1
    invoke-static {v3, v4}, Lcom/google/t/b/b/a/g;->c([BI)J

    move-result-wide v4

    .line 105
    new-instance v6, Lcom/google/t/b/b/a/e;

    invoke-direct {v6, v2, v4, v5}, Lcom/google/t/b/b/a/e;-><init>(IJ)V

    .line 106
    invoke-virtual {p0, v6, v3}, Lcom/google/t/b/b/a/r;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 107
    iget-object v3, p0, Lcom/google/t/b/b/a/r;->i:Lcom/google/t/b/b/a/s;

    iget-wide v4, v6, Lcom/google/t/b/b/a/e;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/t/b/b/a/s;->b(Ljava/lang/Long;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 114
    :catch_1
    move-exception v3

    iget v3, p0, Lcom/google/t/b/b/a/r;->f:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/t/b/b/a/r;->f:I

    .line 115
    iget-object v3, p0, Lcom/google/t/b/b/a/r;->a:Lcom/google/t/b/b/a/b;

    invoke-virtual {v3}, Lcom/google/t/b/b/a/b;->x()V

    goto :goto_2

    .line 118
    :cond_3
    return-void

    .line 104
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/t/b/c/a/u;)V
    .locals 6

    .prologue
    .line 134
    invoke-virtual {p1}, Lcom/google/t/b/c/a/u;->b()Ljava/util/Iterator;

    move-result-object v1

    .line 135
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v2, p0, Lcom/google/t/b/b/a/r;->i:Lcom/google/t/b/b/a/s;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/t/b/b/a/e;

    iget-wide v4, v0, Lcom/google/t/b/b/a/e;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/t/b/b/a/s;->a(Ljava/lang/Long;)V

    goto :goto_0

    .line 138
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/t/b/b/a/r;->i:Lcom/google/t/b/b/a/s;

    invoke-virtual {v0, p1}, Lcom/google/t/b/b/a/s;->a(Lcom/google/p/a/b/b/a;)Lcom/google/p/a/b/b/a;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 18
    check-cast p1, Lcom/google/t/b/b/a/e;

    iget-object v0, p0, Lcom/google/t/b/b/a/a;->c:Lcom/google/t/b/c/a/k;

    invoke-virtual {v0, p1}, Lcom/google/t/b/c/a/k;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/t/b/b/a/r;->i:Lcom/google/t/b/b/a/s;

    iget-wide v2, p1, Lcom/google/t/b/b/a/e;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/t/b/b/a/s;->a(Ljava/lang/Long;)V

    :cond_0
    return-void
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 52
    const/16 v0, 0x400

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 57
    const/16 v0, 0x100

    return v0
.end method

.method public final o()I
    .locals 1

    .prologue
    .line 62
    const/16 v0, 0x100

    return v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    const-string v0, "smc"

    return-object v0
.end method

.method public final q()Lcom/google/android/location/e/ak;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/google/t/b/b/a/e;->c:Lcom/google/android/location/e/ak;

    return-object v0
.end method

.method public final r()Lcom/google/android/location/e/ak;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/google/t/b/c/a/c;->b:Lcom/google/android/location/e/ak;

    return-object v0
.end method

.method public final bridge synthetic s()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/google/t/b/b/a/r;->h:[B

    return-object v0
.end method
