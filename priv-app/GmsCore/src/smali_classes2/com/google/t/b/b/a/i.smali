.class final Lcom/google/t/b/b/a/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 6

    .prologue
    const/4 v0, -0x1

    .line 15
    check-cast p1, Lcom/google/t/b/b/a/j;

    check-cast p2, Lcom/google/t/b/b/a/j;

    iget-wide v2, p2, Lcom/google/t/b/b/a/j;->c:D

    iget-wide v4, p1, Lcom/google/t/b/b/a/j;->c:D

    cmpg-double v1, v2, v4

    if-gez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    cmpl-double v1, v2, v4

    if-gtz v1, :cond_3

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method
