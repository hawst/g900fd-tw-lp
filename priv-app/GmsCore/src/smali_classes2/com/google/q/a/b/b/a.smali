.class public final Lcom/google/q/a/b/b/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/q/a/b/b/t;


# static fields
.field private static final a:J

.field private static final b:J


# instance fields
.field private c:Lcom/google/q/a/b/a/b;

.field private d:J

.field private e:J

.field private f:Lcom/google/q/a/b/b/u;

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x1

    .line 22
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/q/a/b/b/a;->a:J

    .line 24
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/q/a/b/b/a;->b:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    return-void
.end method


# virtual methods
.method public final a(J)I
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x2

    return v0
.end method

.method public final a()Lcom/google/q/a/b/a/b;
    .locals 4

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/q/a/b/b/a;->c:Lcom/google/q/a/b/a/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/q/a/b/b/a;->c:Lcom/google/q/a/b/a/b;

    iget-object v0, v0, Lcom/google/q/a/b/a/b;->a:Lcom/google/q/a/b/a/e;

    sget-object v1, Lcom/google/q/a/b/a/e;->c:Lcom/google/q/a/b/a/e;

    if-ne v0, v1, :cond_1

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/google/q/a/b/b/a;->c:Lcom/google/q/a/b/a/b;

    .line 92
    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/google/q/a/b/a/b;->e()Lcom/google/q/a/b/a/d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/q/a/b/b/a;->c:Lcom/google/q/a/b/a/b;

    iget v1, v1, Lcom/google/q/a/b/a/b;->b:I

    iget-object v2, p0, Lcom/google/q/a/b/b/a;->c:Lcom/google/q/a/b/a/b;

    iget v2, v2, Lcom/google/q/a/b/a/b;->c:I

    iget v3, p0, Lcom/google/q/a/b/b/a;->g:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/q/a/b/a/d;->a(III)Lcom/google/q/a/b/a/d;

    move-result-object v0

    sget-object v1, Lcom/google/q/a/b/a/e;->c:Lcom/google/q/a/b/a/e;

    invoke-virtual {v0, v1}, Lcom/google/q/a/b/a/d;->a(Lcom/google/q/a/b/a/e;)Lcom/google/q/a/b/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/q/a/b/a/d;->a()Lcom/google/q/a/b/a/b;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(JFF)V
    .locals 0

    .prologue
    .line 45
    return-void
.end method

.method public final a(JIF)V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method public final a(JLcom/google/q/a/b/a/b;)V
    .locals 15

    .prologue
    const-wide v12, 0x408f400000000000L    # 1000.0

    const-wide/high16 v2, 0x402a000000000000L    # 13.0

    .line 49
    sget-object v4, Lcom/google/q/a/b/b/b;->a:[I

    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/google/q/a/b/a/b;->a:Lcom/google/q/a/b/a/e;

    invoke-virtual {v5}, Lcom/google/q/a/b/a/e;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 77
    :goto_0
    move-wide/from16 v0, p1

    iput-wide v0, p0, Lcom/google/q/a/b/b/a;->d:J

    .line 78
    return-void

    .line 57
    :pswitch_0
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/google/q/a/b/b/a;->c:Lcom/google/q/a/b/a/b;

    .line 58
    move-object/from16 v0, p3

    iget v2, v0, Lcom/google/q/a/b/a/b;->d:I

    iput v2, p0, Lcom/google/q/a/b/b/a;->g:I

    goto :goto_0

    .line 62
    :pswitch_1
    iget-object v4, p0, Lcom/google/q/a/b/b/a;->c:Lcom/google/q/a/b/a/b;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/q/a/b/b/a;->c:Lcom/google/q/a/b/a/b;

    iget-object v4, v4, Lcom/google/q/a/b/a/b;->a:Lcom/google/q/a/b/a/e;

    sget-object v5, Lcom/google/q/a/b/a/e;->c:Lcom/google/q/a/b/a/e;

    if-eq v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/q/a/b/b/a;->c:Lcom/google/q/a/b/a/b;

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/google/q/a/b/a/b;->a(Lcom/google/q/a/b/a/b;)D

    move-result-wide v4

    move-object/from16 v0, p3

    iget v6, v0, Lcom/google/q/a/b/a/b;->d:I

    int-to-double v6, v6

    div-double/2addr v6, v12

    cmpl-double v4, v4, v6

    if-lez v4, :cond_1

    .line 65
    :cond_0
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/google/q/a/b/b/a;->c:Lcom/google/q/a/b/a/b;

    .line 66
    move-object/from16 v0, p3

    iget v2, v0, Lcom/google/q/a/b/a/b;->d:I

    iput v2, p0, Lcom/google/q/a/b/b/a;->g:I

    goto :goto_0

    .line 69
    :cond_1
    iget-wide v4, p0, Lcom/google/q/a/b/b/a;->d:J

    sub-long v4, p1, v4

    long-to-double v4, v4

    sget-wide v6, Lcom/google/q/a/b/b/a;->a:J

    long-to-double v6, v6

    div-double/2addr v4, v6

    .line 70
    iget v6, p0, Lcom/google/q/a/b/b/a;->g:I

    iget-object v7, p0, Lcom/google/q/a/b/b/a;->f:Lcom/google/q/a/b/b/u;

    if-eqz v7, :cond_2

    iget-wide v8, p0, Lcom/google/q/a/b/b/a;->e:J

    sget-wide v10, Lcom/google/q/a/b/b/a;->b:J

    add-long/2addr v8, v10

    cmp-long v7, p1, v8

    if-gez v7, :cond_2

    sget-object v7, Lcom/google/q/a/b/b/b;->b:[I

    iget-object v8, p0, Lcom/google/q/a/b/b/a;->f:Lcom/google/q/a/b/b/u;

    invoke-virtual {v8}, Lcom/google/q/a/b/b/u;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_1

    :cond_2
    :goto_1
    :pswitch_2
    mul-double/2addr v2, v4

    mul-double/2addr v2, v12

    double-to-int v2, v2

    add-int/2addr v2, v6

    iput v2, p0, Lcom/google/q/a/b/b/a;->g:I

    .line 72
    move-object/from16 v0, p3

    iget v2, v0, Lcom/google/q/a/b/a/b;->d:I

    iget v3, p0, Lcom/google/q/a/b/b/a;->g:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, Lcom/google/q/a/b/b/a;->g:I

    goto :goto_0

    .line 70
    :pswitch_3
    const-wide/high16 v2, 0x4018000000000000L    # 6.0

    goto :goto_1

    :pswitch_4
    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    goto :goto_1

    .line 49
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 70
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public final a(JLcom/google/q/a/b/b/u;)V
    .locals 1

    .prologue
    .line 38
    iput-wide p1, p0, Lcom/google/q/a/b/b/a;->e:J

    .line 39
    iput-object p3, p0, Lcom/google/q/a/b/b/a;->f:Lcom/google/q/a/b/b/u;

    .line 40
    return-void
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 100
    iget-wide v0, p0, Lcom/google/q/a/b/b/a;->d:J

    return-wide v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/q/a/b/b/a;->c:Lcom/google/q/a/b/a/b;

    .line 111
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/q/a/b/b/a;->d:J

    .line 112
    return-void
.end method
