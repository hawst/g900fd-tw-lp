.class public final Lcom/google/q/a/b/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static j:Ljava/lang/ThreadLocal;


# instance fields
.field public final a:Lcom/google/q/a/b/a/e;

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:F

.field public final f:D

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:I

.field private final k:B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/q/a/b/a/c;

    invoke-direct {v0}, Lcom/google/q/a/b/a/c;-><init>()V

    sput-object v0, Lcom/google/q/a/b/a/b;->j:Ljava/lang/ThreadLocal;

    return-void
.end method

.method private constructor <init>(Lcom/google/q/a/b/a/d;)V
    .locals 2

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iget-byte v0, p1, Lcom/google/q/a/b/a/d;->j:B

    iput-byte v0, p0, Lcom/google/q/a/b/a/b;->k:B

    .line 90
    iget v0, p1, Lcom/google/q/a/b/a/d;->b:I

    iput v0, p0, Lcom/google/q/a/b/a/b;->b:I

    .line 91
    iget v0, p1, Lcom/google/q/a/b/a/d;->c:I

    iput v0, p0, Lcom/google/q/a/b/a/b;->c:I

    .line 92
    iget v0, p1, Lcom/google/q/a/b/a/d;->d:I

    iput v0, p0, Lcom/google/q/a/b/a/b;->d:I

    .line 93
    iget v0, p1, Lcom/google/q/a/b/a/d;->e:F

    iput v0, p0, Lcom/google/q/a/b/a/b;->e:F

    .line 94
    iget-wide v0, p1, Lcom/google/q/a/b/a/d;->g:D

    iput-wide v0, p0, Lcom/google/q/a/b/a/b;->f:D

    .line 95
    iget v0, p1, Lcom/google/q/a/b/a/d;->f:I

    iput v0, p0, Lcom/google/q/a/b/a/b;->i:I

    .line 96
    iget-object v0, p1, Lcom/google/q/a/b/a/d;->a:Lcom/google/q/a/b/a/e;

    iput-object v0, p0, Lcom/google/q/a/b/a/b;->a:Lcom/google/q/a/b/a/e;

    .line 97
    iget-object v0, p1, Lcom/google/q/a/b/a/d;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/q/a/b/a/b;->g:Ljava/lang/String;

    .line 98
    iget-object v0, p1, Lcom/google/q/a/b/a/d;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/q/a/b/a/b;->h:Ljava/lang/String;

    .line 99
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/q/a/b/a/d;B)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/google/q/a/b/a/b;-><init>(Lcom/google/q/a/b/a/d;)V

    return-void
.end method

.method public static e()Lcom/google/q/a/b/a/d;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 222
    invoke-static {}, Lcom/google/q/a/b/a/b;->f()Lcom/google/q/a/b/a/d;

    move-result-object v0

    .line 223
    sget-object v1, Lcom/google/q/a/b/a/e;->e:Lcom/google/q/a/b/a/e;

    iput-object v1, v0, Lcom/google/q/a/b/a/d;->a:Lcom/google/q/a/b/a/e;

    iput v4, v0, Lcom/google/q/a/b/a/d;->b:I

    iput v4, v0, Lcom/google/q/a/b/a/d;->c:I

    iput v5, v0, Lcom/google/q/a/b/a/d;->d:I

    const/4 v1, 0x0

    iput v1, v0, Lcom/google/q/a/b/a/d;->e:F

    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/google/q/a/b/a/d;->g:D

    iput v5, v0, Lcom/google/q/a/b/a/d;->f:I

    iput-object v6, v0, Lcom/google/q/a/b/a/d;->h:Ljava/lang/String;

    iput-object v6, v0, Lcom/google/q/a/b/a/d;->i:Ljava/lang/String;

    iput-byte v4, v0, Lcom/google/q/a/b/a/d;->j:B

    .line 224
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/q/a/b/a/d;->k:Z

    .line 225
    return-object v0
.end method

.method private static f()Lcom/google/q/a/b/a/d;
    .locals 2

    .prologue
    .line 231
    sget-object v0, Lcom/google/q/a/b/a/b;->j:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/b/a/d;

    .line 232
    iget-boolean v1, v0, Lcom/google/q/a/b/a/d;->k:Z

    if-eqz v1, :cond_0

    .line 233
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is already a Position builder in progress."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 235
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/q/a/b/a/b;)D
    .locals 12

    .prologue
    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    .line 158
    iget v0, p0, Lcom/google/q/a/b/a/b;->b:I

    int-to-double v0, v0

    iget v2, p0, Lcom/google/q/a/b/a/b;->c:I

    int-to-double v2, v2

    iget v4, p1, Lcom/google/q/a/b/a/b;->b:I

    int-to-double v4, v4

    iget v6, p1, Lcom/google/q/a/b/a/b;->c:I

    int-to-double v6, v6

    invoke-static {v0, v1}, Lcom/google/q/a/b/a/a;->a(D)D

    move-result-wide v0

    invoke-static {v2, v3}, Lcom/google/q/a/b/a/a;->a(D)D

    move-result-wide v2

    invoke-static {v4, v5}, Lcom/google/q/a/b/a/a;->a(D)D

    move-result-wide v4

    invoke-static {v6, v7}, Lcom/google/q/a/b/a/a;->a(D)D

    move-result-wide v6

    sub-double v8, v4, v0

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    sub-double v2, v6, v2

    mul-double/2addr v2, v10

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    mul-double v6, v8, v8

    mul-double/2addr v2, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    add-double/2addr v0, v6

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    const-wide/16 v6, 0x0

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    sub-double v0, v8, v0

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    mul-double/2addr v0, v2

    const-wide v2, 0x415849c600000000L    # 6367000.0

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 174
    iget-byte v0, p0, Lcom/google/q/a/b/a/b;->k:B

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 182
    iget-byte v0, p0, Lcom/google/q/a/b/a/b;->k:B

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 190
    iget-byte v0, p0, Lcom/google/q/a/b/a/b;->k:B

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lcom/google/q/a/b/a/d;
    .locals 4

    .prologue
    .line 200
    invoke-static {}, Lcom/google/q/a/b/a/b;->f()Lcom/google/q/a/b/a/d;

    move-result-object v0

    .line 201
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/q/a/b/a/d;->k:Z

    .line 202
    iget-object v1, p0, Lcom/google/q/a/b/a/b;->a:Lcom/google/q/a/b/a/e;

    iput-object v1, v0, Lcom/google/q/a/b/a/d;->a:Lcom/google/q/a/b/a/e;

    .line 203
    iget v1, p0, Lcom/google/q/a/b/a/b;->b:I

    iput v1, v0, Lcom/google/q/a/b/a/d;->b:I

    .line 204
    iget v1, p0, Lcom/google/q/a/b/a/b;->c:I

    iput v1, v0, Lcom/google/q/a/b/a/d;->c:I

    .line 205
    iget v1, p0, Lcom/google/q/a/b/a/b;->d:I

    iput v1, v0, Lcom/google/q/a/b/a/d;->d:I

    .line 206
    iget v1, p0, Lcom/google/q/a/b/a/b;->e:F

    iput v1, v0, Lcom/google/q/a/b/a/d;->e:F

    .line 207
    iget v1, p0, Lcom/google/q/a/b/a/b;->i:I

    iput v1, v0, Lcom/google/q/a/b/a/d;->f:I

    .line 208
    iget-wide v2, p0, Lcom/google/q/a/b/a/b;->f:D

    iput-wide v2, v0, Lcom/google/q/a/b/a/d;->g:D

    .line 209
    iget-object v1, p0, Lcom/google/q/a/b/a/b;->g:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/q/a/b/a/d;->h:Ljava/lang/String;

    .line 210
    iget-object v1, p0, Lcom/google/q/a/b/a/b;->h:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/q/a/b/a/d;->i:Ljava/lang/String;

    .line 211
    iget-byte v1, p0, Lcom/google/q/a/b/a/b;->k:B

    iput-byte v1, v0, Lcom/google/q/a/b/a/d;->j:B

    .line 212
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 103
    instance-of v1, p1, Lcom/google/q/a/b/a/b;

    if-nez v1, :cond_1

    .line 108
    :cond_0
    :goto_0
    return v0

    .line 107
    :cond_1
    check-cast p1, Lcom/google/q/a/b/a/b;

    .line 108
    iget v1, p0, Lcom/google/q/a/b/a/b;->b:I

    iget v2, p1, Lcom/google/q/a/b/a/b;->b:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/q/a/b/a/b;->c:I

    iget v2, p1, Lcom/google/q/a/b/a/b;->c:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/q/a/b/a/b;->d:I

    iget v2, p1, Lcom/google/q/a/b/a/b;->d:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/q/a/b/a/b;->g:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/q/a/b/a/b;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/k/a/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/q/a/b/a/b;->h:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/q/a/b/a/b;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/k/a/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/q/a/b/a/b;->i:I

    iget v2, p1, Lcom/google/q/a/b/a/b;->i:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/q/a/b/a/b;->e:F

    iget v2, p1, Lcom/google/q/a/b/a/b;->e:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/google/q/a/b/a/b;->f:D

    iget-wide v4, p1, Lcom/google/q/a/b/a/b;->f:D

    cmpl-double v1, v2, v4

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/q/a/b/a/b;->a:Lcom/google/q/a/b/a/e;

    iget-object v2, p1, Lcom/google/q/a/b/a/b;->a:Lcom/google/q/a/b/a/e;

    if-ne v1, v2, :cond_0

    iget-byte v1, p0, Lcom/google/q/a/b/a/b;->k:B

    iget-byte v2, p1, Lcom/google/q/a/b/a/b;->k:B

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 122
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/q/a/b/a/b;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/q/a/b/a/b;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/q/a/b/a/b;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/q/a/b/a/b;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/q/a/b/a/b;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/q/a/b/a/b;->i:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/q/a/b/a/b;->e:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/google/q/a/b/a/b;->f:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/q/a/b/a/b;->a:Lcom/google/q/a/b/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-byte v2, p0, Lcom/google/q/a/b/a/b;->k:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Position [latE7="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/q/a/b/a/b;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lngE7="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/q/a/b/a/b;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", accuracyMm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/q/a/b/a/b;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clusterId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/q/a/b/a/b;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", levelId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/q/a/b/a/b;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bearing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/q/a/b/a/b;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", speedMps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/q/a/b/a/b;->e:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", altitudeMeters="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/q/a/b/a/b;->f:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", provider="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/q/a/b/a/b;->a:Lcom/google/q/a/b/a/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
