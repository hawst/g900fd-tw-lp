.class final Lcom/google/q/a/b/b/ak;
.super Lcom/google/q/a/b/b/c;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/google/q/a/b/b/ah;

.field private c:J

.field private d:J


# direct methods
.method private constructor <init>(Lcom/google/q/a/b/b/ah;)V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 617
    iput-object p1, p0, Lcom/google/q/a/b/b/ak;->b:Lcom/google/q/a/b/b/ah;

    invoke-direct {p0}, Lcom/google/q/a/b/b/c;-><init>()V

    .line 622
    iput-wide v0, p0, Lcom/google/q/a/b/b/ak;->c:J

    .line 625
    iput-wide v0, p0, Lcom/google/q/a/b/b/ak;->d:J

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/q/a/b/b/ah;B)V
    .locals 0

    .prologue
    .line 617
    invoke-direct {p0, p1}, Lcom/google/q/a/b/b/ak;-><init>(Lcom/google/q/a/b/b/ah;)V

    return-void
.end method


# virtual methods
.method public final a(J)I
    .locals 11

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 644
    iget-wide v4, p0, Lcom/google/q/a/b/b/ak;->c:J

    cmp-long v0, v4, v8

    if-lez v0, :cond_1

    iget-wide v4, p0, Lcom/google/q/a/b/b/ak;->c:J

    sget-wide v6, Lcom/google/q/a/b/b/ah;->f:J

    add-long/2addr v4, v6

    cmp-long v0, p1, v4

    if-gtz v0, :cond_1

    move v0, v1

    .line 647
    :goto_0
    iget-wide v4, p0, Lcom/google/q/a/b/b/ak;->d:J

    cmp-long v3, v4, v8

    if-lez v3, :cond_2

    iget-wide v4, p0, Lcom/google/q/a/b/b/ak;->d:J

    sget-wide v6, Lcom/google/q/a/b/b/ah;->f:J

    add-long/2addr v4, v6

    cmp-long v3, p1, v4

    if-gtz v3, :cond_2

    .line 649
    :goto_1
    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/q/a/b/b/ak;->b:Lcom/google/q/a/b/b/ah;

    iget-object v0, p0, Lcom/google/q/a/b/b/ak;->b:Lcom/google/q/a/b/b/ah;

    invoke-static {v0}, Lcom/google/q/a/b/b/ah;->a(Lcom/google/q/a/b/b/ah;)Lcom/google/q/a/b/b/ac;

    move-result-object v0

    invoke-static {}, Lcom/google/q/a/b/b/ah;->f()[Z

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/q/a/b/b/ah;->a(Lcom/google/q/a/b/b/ac;[Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 654
    iget-object v0, p0, Lcom/google/q/a/b/b/ak;->b:Lcom/google/q/a/b/b/ah;

    invoke-static {v0}, Lcom/google/q/a/b/b/ah;->a(Lcom/google/q/a/b/b/ah;)Lcom/google/q/a/b/b/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/q/a/b/b/ac;->b()Lcom/google/q/a/b/b/ae;

    move-result-object v0

    .line 656
    invoke-interface {v0}, Lcom/google/q/a/b/b/ae;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 657
    invoke-interface {v0}, Lcom/google/q/a/b/b/ae;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 668
    :cond_0
    const/16 v0, 0x1f

    :goto_2
    return v0

    :cond_1
    move v0, v2

    .line 644
    goto :goto_0

    :cond_2
    move v1, v2

    .line 647
    goto :goto_1

    .line 659
    :pswitch_0
    const/4 v0, 0x4

    goto :goto_2

    .line 661
    :pswitch_1
    const/16 v0, 0x14

    goto :goto_2

    .line 657
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(JIF)V
    .locals 1

    .prologue
    .line 629
    const/4 v0, 0x3

    if-ge p3, v0, :cond_0

    .line 630
    iput-wide p1, p0, Lcom/google/q/a/b/b/ak;->c:J

    .line 632
    :cond_0
    return-void
.end method

.method public final a(JLcom/google/q/a/b/a/b;)V
    .locals 3

    .prologue
    .line 636
    invoke-static {}, Lcom/google/q/a/b/b/ah;->d()Ljava/util/EnumSet;

    move-result-object v0

    iget-object v1, p3, Lcom/google/q/a/b/a/b;->a:Lcom/google/q/a/b/a/e;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 637
    iput-wide p1, p0, Lcom/google/q/a/b/b/ak;->d:J

    .line 639
    :cond_0
    return-void
.end method
