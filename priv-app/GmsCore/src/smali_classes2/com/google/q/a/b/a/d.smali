.class public final Lcom/google/q/a/b/a/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lcom/google/q/a/b/a/e;

.field b:I

.field c:I

.field d:I

.field public e:F

.field f:I

.field g:D

.field h:Ljava/lang/String;

.field i:Ljava/lang/String;

.field public j:B

.field k:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 250
    iput-object v0, p0, Lcom/google/q/a/b/a/d;->h:Ljava/lang/String;

    .line 251
    iput-object v0, p0, Lcom/google/q/a/b/a/d;->i:Ljava/lang/String;

    .line 255
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/q/a/b/a/d;->k:Z

    .line 257
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 241
    invoke-direct {p0}, Lcom/google/q/a/b/a/d;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/q/a/b/a/b;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 373
    new-instance v0, Lcom/google/q/a/b/a/b;

    invoke-direct {v0, p0, v1}, Lcom/google/q/a/b/a/b;-><init>(Lcom/google/q/a/b/a/d;B)V

    .line 374
    iput-boolean v1, p0, Lcom/google/q/a/b/a/d;->k:Z

    .line 375
    return-object v0
.end method

.method public final a(D)Lcom/google/q/a/b/a/d;
    .locals 1

    .prologue
    .line 321
    iput-wide p1, p0, Lcom/google/q/a/b/a/d;->g:D

    .line 322
    iget-byte v0, p0, Lcom/google/q/a/b/a/d;->j:B

    or-int/lit8 v0, v0, 0x2

    int-to-byte v0, v0

    iput-byte v0, p0, Lcom/google/q/a/b/a/d;->j:B

    .line 323
    return-object p0
.end method

.method public final a(I)Lcom/google/q/a/b/a/d;
    .locals 1

    .prologue
    .line 339
    iput p1, p0, Lcom/google/q/a/b/a/d;->f:I

    .line 340
    iget-byte v0, p0, Lcom/google/q/a/b/a/d;->j:B

    or-int/lit8 v0, v0, 0x8

    int-to-byte v0, v0

    iput-byte v0, p0, Lcom/google/q/a/b/a/d;->j:B

    .line 341
    return-object p0
.end method

.method public final a(III)Lcom/google/q/a/b/a/d;
    .locals 1

    .prologue
    .line 267
    iput p1, p0, Lcom/google/q/a/b/a/d;->b:I

    .line 268
    iput p2, p0, Lcom/google/q/a/b/a/d;->c:I

    .line 269
    iput p3, p0, Lcom/google/q/a/b/a/d;->d:I

    .line 270
    iget-byte v0, p0, Lcom/google/q/a/b/a/d;->j:B

    or-int/lit8 v0, v0, 0x1

    int-to-byte v0, v0

    iput-byte v0, p0, Lcom/google/q/a/b/a/d;->j:B

    .line 271
    return-object p0
.end method

.method public final a(Lcom/google/q/a/b/a/e;)Lcom/google/q/a/b/a/d;
    .locals 2

    .prologue
    .line 290
    if-nez p1, :cond_0

    .line 291
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Provider cannot be null, use Provider.UNKNOWN"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 293
    :cond_0
    iput-object p1, p0, Lcom/google/q/a/b/a/d;->a:Lcom/google/q/a/b/a/e;

    .line 294
    return-object p0
.end method
