.class public final Lcom/google/q/a/b/b/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/q/a/b/b/t;


# instance fields
.field final a:Lcom/google/q/a/b/b/q;

.field b:D

.field private c:F

.field private d:F

.field private e:Lcom/google/q/a/b/b/g;

.field private final f:Lcom/google/q/a/b/b/q;

.field private final g:Lcom/google/q/a/b/b/q;

.field private final h:Lcom/google/q/a/b/b/q;

.field private final i:Lcom/google/q/a/b/b/q;

.field private final j:Lcom/google/q/a/b/b/q;

.field private final k:Lcom/google/q/a/b/b/q;

.field private final l:Ljava/util/ArrayList;

.field private final m:Ljava/util/ArrayList;

.field private final n:Lcom/google/q/a/b/b/a/b;

.field private final o:Lcom/google/q/a/b/b/a/b;

.field private p:Lcom/google/q/a/b/b/a/b;

.field private final q:Lcom/google/q/a/b/b/a/a;

.field private r:Z

.field private s:Lcom/google/q/a/b/a/b;

.field private t:J

.field private u:J


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/16 v5, 0xa

    const/4 v1, 0x0

    const/4 v3, 0x4

    const/4 v4, 0x6

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput v0, p0, Lcom/google/q/a/b/b/o;->c:F

    .line 51
    iput v0, p0, Lcom/google/q/a/b/b/o;->d:F

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/q/a/b/b/o;->l:Ljava/util/ArrayList;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/q/a/b/b/o;->m:Ljava/util/ArrayList;

    .line 69
    new-instance v0, Lcom/google/q/a/b/b/a/b;

    invoke-direct {v0, v3, v3}, Lcom/google/q/a/b/b/a/b;-><init>(II)V

    iput-object v0, p0, Lcom/google/q/a/b/b/o;->n:Lcom/google/q/a/b/b/a/b;

    .line 70
    new-instance v0, Lcom/google/q/a/b/b/a/b;

    const/4 v2, 0x1

    invoke-direct {v0, v3, v2}, Lcom/google/q/a/b/b/a/b;-><init>(II)V

    iput-object v0, p0, Lcom/google/q/a/b/b/o;->o:Lcom/google/q/a/b/b/a/b;

    .line 71
    new-instance v0, Lcom/google/q/a/b/b/a/b;

    const/4 v2, 0x1

    invoke-direct {v0, v3, v2}, Lcom/google/q/a/b/b/a/b;-><init>(II)V

    iput-object v0, p0, Lcom/google/q/a/b/b/o;->p:Lcom/google/q/a/b/b/a/b;

    .line 73
    new-instance v0, Lcom/google/q/a/b/b/a/a;

    invoke-direct {v0}, Lcom/google/q/a/b/b/a/a;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/b/b/o;->q:Lcom/google/q/a/b/b/a/a;

    .line 75
    iput-boolean v1, p0, Lcom/google/q/a/b/b/o;->r:Z

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/q/a/b/b/o;->s:Lcom/google/q/a/b/a/b;

    .line 77
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/q/a/b/b/o;->t:J

    .line 78
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/q/a/b/b/o;->u:J

    .line 79
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/q/a/b/b/o;->b:D

    .line 82
    new-instance v0, Lcom/google/q/a/b/b/q;

    invoke-direct {v0, v4}, Lcom/google/q/a/b/b/q;-><init>(I)V

    iput-object v0, p0, Lcom/google/q/a/b/b/o;->f:Lcom/google/q/a/b/b/q;

    .line 83
    new-instance v0, Lcom/google/q/a/b/b/q;

    invoke-direct {v0, v4}, Lcom/google/q/a/b/b/q;-><init>(I)V

    iput-object v0, p0, Lcom/google/q/a/b/b/o;->a:Lcom/google/q/a/b/b/q;

    .line 84
    new-instance v0, Lcom/google/q/a/b/b/q;

    invoke-direct {v0, v4}, Lcom/google/q/a/b/b/q;-><init>(I)V

    iput-object v0, p0, Lcom/google/q/a/b/b/o;->g:Lcom/google/q/a/b/b/q;

    .line 85
    new-instance v0, Lcom/google/q/a/b/b/q;

    invoke-direct {v0, v4}, Lcom/google/q/a/b/b/q;-><init>(I)V

    iput-object v0, p0, Lcom/google/q/a/b/b/o;->h:Lcom/google/q/a/b/b/q;

    .line 86
    new-instance v0, Lcom/google/q/a/b/b/q;

    invoke-direct {v0, v4}, Lcom/google/q/a/b/b/q;-><init>(I)V

    iput-object v0, p0, Lcom/google/q/a/b/b/o;->i:Lcom/google/q/a/b/b/q;

    .line 87
    new-instance v0, Lcom/google/q/a/b/b/q;

    invoke-direct {v0, v4}, Lcom/google/q/a/b/b/q;-><init>(I)V

    iput-object v0, p0, Lcom/google/q/a/b/b/o;->j:Lcom/google/q/a/b/b/q;

    .line 88
    new-instance v0, Lcom/google/q/a/b/b/q;

    invoke-direct {v0, v4}, Lcom/google/q/a/b/b/q;-><init>(I)V

    iput-object v0, p0, Lcom/google/q/a/b/b/o;->k:Lcom/google/q/a/b/b/q;

    move v0, v1

    .line 89
    :goto_0
    if-ge v0, v5, :cond_0

    .line 90
    iget-object v2, p0, Lcom/google/q/a/b/b/o;->l:Ljava/util/ArrayList;

    new-instance v3, Lcom/google/q/a/b/b/p;

    invoke-direct {v3, v1}, Lcom/google/q/a/b/b/p;-><init>(B)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 92
    :cond_0
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/q/a/b/b/o;->l:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/q/a/b/b/o;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 263
    iget-object v0, p0, Lcom/google/q/a/b/b/o;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 264
    return-void
.end method


# virtual methods
.method public final a(J)I
    .locals 1

    .prologue
    .line 227
    const/16 v0, 0xd

    return v0
.end method

.method public final a()Lcom/google/q/a/b/a/b;
    .locals 12

    .prologue
    const/4 v8, 0x0

    .line 213
    iget-boolean v0, p0, Lcom/google/q/a/b/b/o;->r:Z

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/google/q/a/b/b/o;->p:Lcom/google/q/a/b/b/a/b;

    invoke-virtual {v0, v8, v8}, Lcom/google/q/a/b/b/a/b;->a(II)D

    move-result-wide v2

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v8}, Lcom/google/q/a/b/b/a/b;->a(II)D

    move-result-wide v4

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v8}, Lcom/google/q/a/b/b/a/b;->a(II)D

    move-result-wide v6

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v8}, Lcom/google/q/a/b/b/a/b;->a(II)D

    move-result-wide v0

    iget v8, p0, Lcom/google/q/a/b/b/o;->c:F

    float-to-double v8, v8

    mul-double/2addr v8, v2

    iget v10, p0, Lcom/google/q/a/b/b/o;->d:F

    float-to-double v10, v10

    mul-double/2addr v10, v4

    sub-double/2addr v8, v10

    add-double/2addr v6, v8

    iget v8, p0, Lcom/google/q/a/b/b/o;->c:F

    float-to-double v8, v8

    mul-double/2addr v4, v8

    iget v8, p0, Lcom/google/q/a/b/b/o;->d:F

    float-to-double v8, v8

    mul-double/2addr v2, v8

    add-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-object v2, p0, Lcom/google/q/a/b/b/o;->e:Lcom/google/q/a/b/b/g;

    invoke-virtual {v2, v0, v1}, Lcom/google/q/a/b/b/g;->a(D)I

    move-result v0

    iget-object v1, p0, Lcom/google/q/a/b/b/o;->e:Lcom/google/q/a/b/b/g;

    invoke-virtual {v1, v6, v7}, Lcom/google/q/a/b/b/g;->b(D)I

    move-result v1

    iget-object v2, p0, Lcom/google/q/a/b/b/o;->s:Lcom/google/q/a/b/a/b;

    invoke-virtual {v2}, Lcom/google/q/a/b/a/b;->d()Lcom/google/q/a/b/a/d;

    move-result-object v2

    iget-object v3, p0, Lcom/google/q/a/b/b/o;->s:Lcom/google/q/a/b/a/b;

    iget v3, v3, Lcom/google/q/a/b/a/b;->d:I

    invoke-virtual {v2, v0, v1, v3}, Lcom/google/q/a/b/a/d;->a(III)Lcom/google/q/a/b/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/q/a/b/a/d;->a()Lcom/google/q/a/b/a/b;

    move-result-object v0

    .line 217
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(JFF)V
    .locals 7

    .prologue
    const-wide v4, 0x3fe3333340000000L    # 0.6000000238418579

    .line 112
    iget v0, p0, Lcom/google/q/a/b/b/o;->c:F

    float-to-double v0, v0

    float-to-double v2, p4

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/google/q/a/b/b/o;->c:F

    .line 113
    iget v0, p0, Lcom/google/q/a/b/b/o;->d:F

    float-to-double v0, v0

    float-to-double v2, p4

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/google/q/a/b/b/o;->d:F

    .line 116
    iget-object v0, p0, Lcom/google/q/a/b/b/o;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/google/q/a/b/b/o;->m:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/b/b/p;

    .line 122
    :goto_0
    iput-wide p1, v0, Lcom/google/q/a/b/b/p;->a:J

    .line 123
    iget v1, p0, Lcom/google/q/a/b/b/o;->c:F

    iput v1, v0, Lcom/google/q/a/b/b/p;->b:F

    .line 124
    iget v1, p0, Lcom/google/q/a/b/b/o;->d:F

    iput v1, v0, Lcom/google/q/a/b/b/p;->c:F

    .line 125
    iget-object v1, p0, Lcom/google/q/a/b/b/o;->m:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    iput-wide p1, p0, Lcom/google/q/a/b/b/o;->t:J

    .line 128
    return-void

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/google/q/a/b/b/o;->l:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/q/a/b/b/o;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/b/b/p;

    goto :goto_0
.end method

.method public final a(JIF)V
    .locals 0

    .prologue
    .line 209
    return-void
.end method

.method public final a(JLcom/google/q/a/b/a/b;)V
    .locals 23

    .prologue
    .line 132
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->e:Lcom/google/q/a/b/b/g;

    if-nez v4, :cond_0

    .line 133
    new-instance v4, Lcom/google/q/a/b/b/g;

    move-object/from16 v0, p3

    iget v5, v0, Lcom/google/q/a/b/a/b;->b:I

    move-object/from16 v0, p3

    iget v6, v0, Lcom/google/q/a/b/a/b;->c:I

    invoke-direct {v4, v5, v6}, Lcom/google/q/a/b/b/g;-><init>(II)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/q/a/b/b/o;->e:Lcom/google/q/a/b/b/g;

    .line 136
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->e:Lcom/google/q/a/b/b/g;

    move-object/from16 v0, p3

    iget v5, v0, Lcom/google/q/a/b/a/b;->b:I

    invoke-virtual {v4, v5}, Lcom/google/q/a/b/b/g;->a(I)D

    move-result-wide v6

    .line 137
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->e:Lcom/google/q/a/b/b/g;

    move-object/from16 v0, p3

    iget v5, v0, Lcom/google/q/a/b/a/b;->c:I

    invoke-virtual {v4, v5}, Lcom/google/q/a/b/b/g;->b(I)D

    move-result-wide v8

    .line 138
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->e:Lcom/google/q/a/b/b/g;

    invoke-virtual {v4, v8, v9, v6, v7}, Lcom/google/q/a/b/b/g;->a(DD)D

    move-result-wide v4

    const-wide v10, 0x3fd3333333333333L    # 0.3

    cmpl-double v4, v4, v10

    if-lez v4, :cond_1

    .line 140
    invoke-virtual/range {p0 .. p0}, Lcom/google/q/a/b/b/o;->c()V

    .line 142
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->s:Lcom/google/q/a/b/a/b;

    if-eqz v4, :cond_3

    .line 143
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->e:Lcom/google/q/a/b/b/g;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/q/a/b/b/o;->s:Lcom/google/q/a/b/a/b;

    iget v5, v5, Lcom/google/q/a/b/a/b;->b:I

    invoke-virtual {v4, v5}, Lcom/google/q/a/b/b/g;->a(I)D

    move-result-wide v10

    .line 144
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->e:Lcom/google/q/a/b/b/g;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/q/a/b/b/o;->s:Lcom/google/q/a/b/a/b;

    iget v5, v5, Lcom/google/q/a/b/a/b;->c:I

    invoke-virtual {v4, v5}, Lcom/google/q/a/b/b/g;->b(I)D

    move-result-wide v12

    .line 146
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/q/a/b/b/o;->u:J

    sub-long v4, p1, v4

    long-to-double v14, v4

    .line 147
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->m:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/q/a/b/b/p;

    .line 149
    iget-wide v0, v4, Lcom/google/q/a/b/b/p;->a:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/q/a/b/b/o;->u:J

    move-wide/from16 v18, v0

    sub-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-double v0, v0

    move-wide/from16 v16, v0

    div-double v16, v16, v14

    .line 150
    const-wide/high16 v18, 0x3ff0000000000000L    # 1.0

    sub-double v18, v18, v16

    mul-double v18, v18, v12

    mul-double v20, v16, v8

    add-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-float v0, v0

    move/from16 v18, v0

    .line 151
    const-wide/high16 v20, 0x3ff0000000000000L    # 1.0

    sub-double v20, v20, v16

    mul-double v20, v20, v10

    mul-double v16, v16, v6

    add-double v16, v16, v20

    move-wide/from16 v0, v16

    double-to-float v0, v0

    move/from16 v16, v0

    .line 152
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/q/a/b/b/o;->f:Lcom/google/q/a/b/b/q;

    move-object/from16 v17, v0

    iget v0, v4, Lcom/google/q/a/b/b/p;->b:F

    move/from16 v19, v0

    iget v0, v4, Lcom/google/q/a/b/b/p;->b:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    iget v0, v4, Lcom/google/q/a/b/b/p;->c:F

    move/from16 v20, v0

    iget v0, v4, Lcom/google/q/a/b/b/p;->c:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    add-float v19, v19, v20

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/q/a/b/b/q;->a(F)V

    .line 153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/q/a/b/b/o;->a:Lcom/google/q/a/b/b/q;

    move-object/from16 v17, v0

    iget v0, v4, Lcom/google/q/a/b/b/p;->b:F

    move/from16 v19, v0

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/q/a/b/b/q;->a(F)V

    .line 154
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/q/a/b/b/o;->g:Lcom/google/q/a/b/b/q;

    move-object/from16 v17, v0

    iget v0, v4, Lcom/google/q/a/b/b/p;->c:F

    move/from16 v19, v0

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/q/a/b/b/q;->a(F)V

    .line 155
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/q/a/b/b/o;->h:Lcom/google/q/a/b/b/q;

    move-object/from16 v17, v0

    iget v0, v4, Lcom/google/q/a/b/b/p;->b:F

    move/from16 v19, v0

    mul-float v19, v19, v18

    iget v0, v4, Lcom/google/q/a/b/b/p;->c:F

    move/from16 v20, v0

    mul-float v20, v20, v16

    add-float v19, v19, v20

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/q/a/b/b/q;->a(F)V

    .line 156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/q/a/b/b/o;->i:Lcom/google/q/a/b/b/q;

    move-object/from16 v17, v0

    iget v0, v4, Lcom/google/q/a/b/b/p;->c:F

    move/from16 v19, v0

    move/from16 v0, v19

    neg-float v0, v0

    move/from16 v19, v0

    mul-float v19, v19, v18

    iget v4, v4, Lcom/google/q/a/b/b/p;->b:F

    mul-float v4, v4, v16

    add-float v4, v4, v19

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/google/q/a/b/b/q;->a(F)V

    .line 157
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->j:Lcom/google/q/a/b/b/q;

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/google/q/a/b/b/q;->a(F)V

    .line 158
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->k:Lcom/google/q/a/b/b/q;

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/google/q/a/b/b/q;->a(F)V

    goto/16 :goto_0

    .line 160
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/google/q/a/b/b/o;->d()V

    .line 162
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->a:Lcom/google/q/a/b/b/q;

    iget v4, v4, Lcom/google/q/a/b/b/q;->d:I

    const/4 v5, 0x2

    if-lt v4, v5, :cond_3

    .line 163
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->n:Lcom/google/q/a/b/b/a/b;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/q/a/b/b/o;->f:Lcom/google/q/a/b/b/q;

    iget-wide v8, v7, Lcom/google/q/a/b/b/q;->b:D

    double-to-float v7, v8

    float-to-double v8, v7

    invoke-virtual {v4, v5, v6, v8, v9}, Lcom/google/q/a/b/b/a/b;->a(IID)V

    .line 164
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->n:Lcom/google/q/a/b/b/a/b;

    const/4 v5, 0x0

    const/4 v6, 0x1

    const-wide/16 v8, 0x0

    invoke-virtual {v4, v5, v6, v8, v9}, Lcom/google/q/a/b/b/a/b;->a(IID)V

    .line 165
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->n:Lcom/google/q/a/b/b/a/b;

    const/4 v5, 0x0

    const/4 v6, 0x2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/q/a/b/b/o;->a:Lcom/google/q/a/b/b/q;

    iget-wide v8, v7, Lcom/google/q/a/b/b/q;->b:D

    double-to-float v7, v8

    float-to-double v8, v7

    invoke-virtual {v4, v5, v6, v8, v9}, Lcom/google/q/a/b/b/a/b;->a(IID)V

    .line 166
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->n:Lcom/google/q/a/b/b/a/b;

    const/4 v5, 0x0

    const/4 v6, 0x3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/q/a/b/b/o;->g:Lcom/google/q/a/b/b/q;

    iget-wide v8, v7, Lcom/google/q/a/b/b/q;->b:D

    double-to-float v7, v8

    float-to-double v8, v7

    invoke-virtual {v4, v5, v6, v8, v9}, Lcom/google/q/a/b/b/a/b;->a(IID)V

    .line 168
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->n:Lcom/google/q/a/b/b/a/b;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const-wide/16 v8, 0x0

    invoke-virtual {v4, v5, v6, v8, v9}, Lcom/google/q/a/b/b/a/b;->a(IID)V

    .line 169
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->n:Lcom/google/q/a/b/b/a/b;

    const/4 v5, 0x1

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/q/a/b/b/o;->f:Lcom/google/q/a/b/b/q;

    iget-wide v8, v7, Lcom/google/q/a/b/b/q;->b:D

    double-to-float v7, v8

    float-to-double v8, v7

    invoke-virtual {v4, v5, v6, v8, v9}, Lcom/google/q/a/b/b/a/b;->a(IID)V

    .line 170
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->n:Lcom/google/q/a/b/b/a/b;

    const/4 v5, 0x1

    const/4 v6, 0x2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/q/a/b/b/o;->g:Lcom/google/q/a/b/b/q;

    iget-wide v8, v7, Lcom/google/q/a/b/b/q;->b:D

    double-to-float v7, v8

    neg-float v7, v7

    float-to-double v8, v7

    invoke-virtual {v4, v5, v6, v8, v9}, Lcom/google/q/a/b/b/a/b;->a(IID)V

    .line 171
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->n:Lcom/google/q/a/b/b/a/b;

    const/4 v5, 0x1

    const/4 v6, 0x3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/q/a/b/b/o;->a:Lcom/google/q/a/b/b/q;

    iget-wide v8, v7, Lcom/google/q/a/b/b/q;->b:D

    double-to-float v7, v8

    float-to-double v8, v7

    invoke-virtual {v4, v5, v6, v8, v9}, Lcom/google/q/a/b/b/a/b;->a(IID)V

    .line 173
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->n:Lcom/google/q/a/b/b/a/b;

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/q/a/b/b/o;->a:Lcom/google/q/a/b/b/q;

    iget-wide v8, v7, Lcom/google/q/a/b/b/q;->b:D

    double-to-float v7, v8

    float-to-double v8, v7

    invoke-virtual {v4, v5, v6, v8, v9}, Lcom/google/q/a/b/b/a/b;->a(IID)V

    .line 174
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->n:Lcom/google/q/a/b/b/a/b;

    const/4 v5, 0x2

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/q/a/b/b/o;->g:Lcom/google/q/a/b/b/q;

    iget-wide v8, v7, Lcom/google/q/a/b/b/q;->b:D

    double-to-float v7, v8

    neg-float v7, v7

    float-to-double v8, v7

    invoke-virtual {v4, v5, v6, v8, v9}, Lcom/google/q/a/b/b/a/b;->a(IID)V

    .line 175
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->n:Lcom/google/q/a/b/b/a/b;

    const/4 v5, 0x2

    const/4 v6, 0x2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/q/a/b/b/o;->g:Lcom/google/q/a/b/b/q;

    iget v7, v7, Lcom/google/q/a/b/b/q;->d:I

    int-to-double v8, v7

    invoke-virtual {v4, v5, v6, v8, v9}, Lcom/google/q/a/b/b/a/b;->a(IID)V

    .line 176
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->n:Lcom/google/q/a/b/b/a/b;

    const/4 v5, 0x2

    const/4 v6, 0x3

    const-wide/16 v8, 0x0

    invoke-virtual {v4, v5, v6, v8, v9}, Lcom/google/q/a/b/b/a/b;->a(IID)V

    .line 178
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->n:Lcom/google/q/a/b/b/a/b;

    const/4 v5, 0x3

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/q/a/b/b/o;->g:Lcom/google/q/a/b/b/q;

    iget-wide v8, v7, Lcom/google/q/a/b/b/q;->b:D

    double-to-float v7, v8

    float-to-double v8, v7

    invoke-virtual {v4, v5, v6, v8, v9}, Lcom/google/q/a/b/b/a/b;->a(IID)V

    .line 179
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->n:Lcom/google/q/a/b/b/a/b;

    const/4 v5, 0x3

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/q/a/b/b/o;->a:Lcom/google/q/a/b/b/q;

    iget-wide v8, v7, Lcom/google/q/a/b/b/q;->b:D

    double-to-float v7, v8

    float-to-double v8, v7

    invoke-virtual {v4, v5, v6, v8, v9}, Lcom/google/q/a/b/b/a/b;->a(IID)V

    .line 180
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->n:Lcom/google/q/a/b/b/a/b;

    const/4 v5, 0x3

    const/4 v6, 0x2

    const-wide/16 v8, 0x0

    invoke-virtual {v4, v5, v6, v8, v9}, Lcom/google/q/a/b/b/a/b;->a(IID)V

    .line 181
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->n:Lcom/google/q/a/b/b/a/b;

    const/4 v5, 0x3

    const/4 v6, 0x3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/q/a/b/b/o;->g:Lcom/google/q/a/b/b/q;

    iget v7, v7, Lcom/google/q/a/b/b/q;->d:I

    int-to-double v8, v7

    invoke-virtual {v4, v5, v6, v8, v9}, Lcom/google/q/a/b/b/a/b;->a(IID)V

    .line 183
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->o:Lcom/google/q/a/b/b/a/b;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/q/a/b/b/o;->h:Lcom/google/q/a/b/b/q;

    iget-wide v8, v7, Lcom/google/q/a/b/b/q;->b:D

    double-to-float v7, v8

    float-to-double v8, v7

    invoke-virtual {v4, v5, v6, v8, v9}, Lcom/google/q/a/b/b/a/b;->a(IID)V

    .line 184
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->o:Lcom/google/q/a/b/b/a/b;

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/q/a/b/b/o;->i:Lcom/google/q/a/b/b/q;

    iget-wide v8, v7, Lcom/google/q/a/b/b/q;->b:D

    double-to-float v7, v8

    float-to-double v8, v7

    invoke-virtual {v4, v5, v6, v8, v9}, Lcom/google/q/a/b/b/a/b;->a(IID)V

    .line 185
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->o:Lcom/google/q/a/b/b/a/b;

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/q/a/b/b/o;->j:Lcom/google/q/a/b/b/q;

    iget-wide v8, v7, Lcom/google/q/a/b/b/q;->b:D

    double-to-float v7, v8

    float-to-double v8, v7

    invoke-virtual {v4, v5, v6, v8, v9}, Lcom/google/q/a/b/b/a/b;->a(IID)V

    .line 186
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->o:Lcom/google/q/a/b/b/a/b;

    const/4 v5, 0x3

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/q/a/b/b/o;->k:Lcom/google/q/a/b/b/q;

    iget-wide v8, v7, Lcom/google/q/a/b/b/q;->b:D

    double-to-float v7, v8

    float-to-double v8, v7

    invoke-virtual {v4, v5, v6, v8, v9}, Lcom/google/q/a/b/b/a/b;->a(IID)V

    .line 188
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->q:Lcom/google/q/a/b/b/a/a;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/q/a/b/b/o;->n:Lcom/google/q/a/b/b/a/b;

    invoke-virtual {v4, v5}, Lcom/google/q/a/b/b/a/a;->a(Lcom/google/q/a/b/b/a/b;)V

    .line 189
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->n:Lcom/google/q/a/b/b/a/b;

    .line 190
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/q/a/b/b/o;->q:Lcom/google/q/a/b/b/a/a;

    invoke-virtual {v5, v4}, Lcom/google/q/a/b/b/a/a;->b(Lcom/google/q/a/b/b/a/b;)V

    .line 191
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/q/a/b/b/o;->o:Lcom/google/q/a/b/b/a/b;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/q/a/b/b/o;->p:Lcom/google/q/a/b/b/a/b;

    invoke-static {v4, v5, v6}, Lcom/google/q/a/b/b/a/d;->a(Lcom/google/q/a/b/b/a/b;Lcom/google/q/a/b/b/a/b;Lcom/google/q/a/b/b/a/b;)V

    .line 192
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/q/a/b/b/o;->r:Z

    .line 194
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/q/a/b/b/o;->p:Lcom/google/q/a/b/b/a/b;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/google/q/a/b/b/a/b;->a(II)D

    move-result-wide v4

    .line 195
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/q/a/b/b/o;->p:Lcom/google/q/a/b/b/a/b;

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Lcom/google/q/a/b/b/a/b;->a(II)D

    move-result-wide v6

    .line 196
    mul-double/2addr v4, v4

    mul-double/2addr v6, v6

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/google/q/a/b/b/o;->b:D

    .line 198
    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/q/a/b/b/o;->t:J

    .line 202
    :cond_3
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/q/a/b/b/o;->s:Lcom/google/q/a/b/a/b;

    .line 203
    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/q/a/b/b/o;->u:J

    .line 204
    return-void
.end method

.method public final a(JLcom/google/q/a/b/b/u;)V
    .locals 0

    .prologue
    .line 97
    return-void
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 222
    iget-wide v0, p0, Lcom/google/q/a/b/b/o;->t:J

    return-wide v0
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 232
    iget-object v0, p0, Lcom/google/q/a/b/b/o;->f:Lcom/google/q/a/b/b/q;

    invoke-virtual {v0}, Lcom/google/q/a/b/b/q;->c()V

    .line 233
    iget-object v0, p0, Lcom/google/q/a/b/b/o;->a:Lcom/google/q/a/b/b/q;

    invoke-virtual {v0}, Lcom/google/q/a/b/b/q;->c()V

    .line 234
    iget-object v0, p0, Lcom/google/q/a/b/b/o;->g:Lcom/google/q/a/b/b/q;

    invoke-virtual {v0}, Lcom/google/q/a/b/b/q;->c()V

    .line 235
    iget-object v0, p0, Lcom/google/q/a/b/b/o;->h:Lcom/google/q/a/b/b/q;

    invoke-virtual {v0}, Lcom/google/q/a/b/b/q;->c()V

    .line 236
    iget-object v0, p0, Lcom/google/q/a/b/b/o;->i:Lcom/google/q/a/b/b/q;

    invoke-virtual {v0}, Lcom/google/q/a/b/b/q;->c()V

    .line 237
    iget-object v0, p0, Lcom/google/q/a/b/b/o;->j:Lcom/google/q/a/b/b/q;

    invoke-virtual {v0}, Lcom/google/q/a/b/b/q;->c()V

    .line 238
    iget-object v0, p0, Lcom/google/q/a/b/b/o;->k:Lcom/google/q/a/b/b/q;

    invoke-virtual {v0}, Lcom/google/q/a/b/b/q;->c()V

    .line 239
    iput-object v2, p0, Lcom/google/q/a/b/b/o;->e:Lcom/google/q/a/b/b/g;

    .line 240
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/q/a/b/b/o;->b:D

    .line 241
    invoke-direct {p0}, Lcom/google/q/a/b/b/o;->d()V

    .line 242
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/q/a/b/b/o;->r:Z

    .line 243
    iput-object v2, p0, Lcom/google/q/a/b/b/o;->s:Lcom/google/q/a/b/a/b;

    .line 244
    return-void
.end method
