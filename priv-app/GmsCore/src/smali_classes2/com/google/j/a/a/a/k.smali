.class public final Lcom/google/j/a/a/a/k;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/google/j/a/a/a/l;

.field public c:Ljava/lang/String;

.field public d:Lcom/google/j/a/a/a/y;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 230
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 231
    const-string v0, ""

    iput-object v0, p0, Lcom/google/j/a/a/a/k;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/j/a/a/a/k;->b:Lcom/google/j/a/a/a/l;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/j/a/a/a/k;->c:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/j/a/a/a/k;->d:Lcom/google/j/a/a/a/y;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/j/a/a/a/k;->cachedSize:I

    .line 232
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 321
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 322
    iget-object v1, p0, Lcom/google/j/a/a/a/k;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 323
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/j/a/a/a/k;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 326
    :cond_0
    iget-object v1, p0, Lcom/google/j/a/a/a/k;->b:Lcom/google/j/a/a/a/l;

    if-eqz v1, :cond_1

    .line 327
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/j/a/a/a/k;->b:Lcom/google/j/a/a/a/l;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 330
    :cond_1
    iget-object v1, p0, Lcom/google/j/a/a/a/k;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 331
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/j/a/a/a/k;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 334
    :cond_2
    iget-object v1, p0, Lcom/google/j/a/a/a/k;->d:Lcom/google/j/a/a/a/y;

    if-eqz v1, :cond_3

    .line 335
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/j/a/a/a/k;->d:Lcom/google/j/a/a/a/y;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 338
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 245
    if-ne p1, p0, :cond_1

    .line 284
    :cond_0
    :goto_0
    return v0

    .line 248
    :cond_1
    instance-of v2, p1, Lcom/google/j/a/a/a/k;

    if-nez v2, :cond_2

    move v0, v1

    .line 249
    goto :goto_0

    .line 251
    :cond_2
    check-cast p1, Lcom/google/j/a/a/a/k;

    .line 252
    iget-object v2, p0, Lcom/google/j/a/a/a/k;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 253
    iget-object v2, p1, Lcom/google/j/a/a/a/k;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 254
    goto :goto_0

    .line 256
    :cond_3
    iget-object v2, p0, Lcom/google/j/a/a/a/k;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/j/a/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 257
    goto :goto_0

    .line 259
    :cond_4
    iget-object v2, p0, Lcom/google/j/a/a/a/k;->b:Lcom/google/j/a/a/a/l;

    if-nez v2, :cond_5

    .line 260
    iget-object v2, p1, Lcom/google/j/a/a/a/k;->b:Lcom/google/j/a/a/a/l;

    if-eqz v2, :cond_6

    move v0, v1

    .line 261
    goto :goto_0

    .line 264
    :cond_5
    iget-object v2, p0, Lcom/google/j/a/a/a/k;->b:Lcom/google/j/a/a/a/l;

    iget-object v3, p1, Lcom/google/j/a/a/a/k;->b:Lcom/google/j/a/a/a/l;

    invoke-virtual {v2, v3}, Lcom/google/j/a/a/a/l;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 265
    goto :goto_0

    .line 268
    :cond_6
    iget-object v2, p0, Lcom/google/j/a/a/a/k;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 269
    iget-object v2, p1, Lcom/google/j/a/a/a/k;->c:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 270
    goto :goto_0

    .line 272
    :cond_7
    iget-object v2, p0, Lcom/google/j/a/a/a/k;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/j/a/a/a/k;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 273
    goto :goto_0

    .line 275
    :cond_8
    iget-object v2, p0, Lcom/google/j/a/a/a/k;->d:Lcom/google/j/a/a/a/y;

    if-nez v2, :cond_9

    .line 276
    iget-object v2, p1, Lcom/google/j/a/a/a/k;->d:Lcom/google/j/a/a/a/y;

    if-eqz v2, :cond_0

    move v0, v1

    .line 277
    goto :goto_0

    .line 280
    :cond_9
    iget-object v2, p0, Lcom/google/j/a/a/a/k;->d:Lcom/google/j/a/a/a/y;

    iget-object v3, p1, Lcom/google/j/a/a/a/k;->d:Lcom/google/j/a/a/a/y;

    invoke-virtual {v2, v3}, Lcom/google/j/a/a/a/y;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 281
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 289
    iget-object v0, p0, Lcom/google/j/a/a/a/k;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 292
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/j/a/a/a/k;->b:Lcom/google/j/a/a/a/l;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 294
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/j/a/a/a/k;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 296
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/j/a/a/a/k;->d:Lcom/google/j/a/a/a/y;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 298
    return v0

    .line 289
    :cond_0
    iget-object v0, p0, Lcom/google/j/a/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 292
    :cond_1
    iget-object v0, p0, Lcom/google/j/a/a/a/k;->b:Lcom/google/j/a/a/a/l;

    invoke-virtual {v0}, Lcom/google/j/a/a/a/l;->hashCode()I

    move-result v0

    goto :goto_1

    .line 294
    :cond_2
    iget-object v0, p0, Lcom/google/j/a/a/a/k;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 296
    :cond_3
    iget-object v1, p0, Lcom/google/j/a/a/a/k;->d:Lcom/google/j/a/a/a/y;

    invoke-virtual {v1}, Lcom/google/j/a/a/a/y;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 201
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/a/a/a/k;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/j/a/a/a/k;->b:Lcom/google/j/a/a/a/l;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/j/a/a/a/l;

    invoke-direct {v0}, Lcom/google/j/a/a/a/l;-><init>()V

    iput-object v0, p0, Lcom/google/j/a/a/a/k;->b:Lcom/google/j/a/a/a/l;

    :cond_1
    iget-object v0, p0, Lcom/google/j/a/a/a/k;->b:Lcom/google/j/a/a/a/l;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/a/a/a/k;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/j/a/a/a/k;->d:Lcom/google/j/a/a/a/y;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/j/a/a/a/y;

    invoke-direct {v0}, Lcom/google/j/a/a/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/j/a/a/a/k;->d:Lcom/google/j/a/a/a/y;

    :cond_2
    iget-object v0, p0, Lcom/google/j/a/a/a/k;->d:Lcom/google/j/a/a/a/y;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/j/a/a/a/k;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 305
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/j/a/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 307
    :cond_0
    iget-object v0, p0, Lcom/google/j/a/a/a/k;->b:Lcom/google/j/a/a/a/l;

    if-eqz v0, :cond_1

    .line 308
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/j/a/a/a/k;->b:Lcom/google/j/a/a/a/l;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 310
    :cond_1
    iget-object v0, p0, Lcom/google/j/a/a/a/k;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 311
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/j/a/a/a/k;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 313
    :cond_2
    iget-object v0, p0, Lcom/google/j/a/a/a/k;->d:Lcom/google/j/a/a/a/y;

    if-eqz v0, :cond_3

    .line 314
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/j/a/a/a/k;->d:Lcom/google/j/a/a/a/y;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 316
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 317
    return-void
.end method
