.class public final Lcom/google/j/a/a/a/s;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:[Lcom/google/j/a/a/a/u;

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 346
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 347
    const-string v0, ""

    iput-object v0, p0, Lcom/google/j/a/a/a/s;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/j/a/a/a/s;->b:Ljava/lang/String;

    invoke-static {}, Lcom/google/j/a/a/a/u;->a()[Lcom/google/j/a/a/a/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/a/a/a/s;->c:[Lcom/google/j/a/a/a/u;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/j/a/a/a/s;->d:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/j/a/a/a/s;->cachedSize:I

    .line 348
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 430
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 431
    iget-object v1, p0, Lcom/google/j/a/a/a/s;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 432
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/j/a/a/a/s;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 435
    :cond_0
    iget-object v1, p0, Lcom/google/j/a/a/a/s;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 436
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/j/a/a/a/s;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 439
    :cond_1
    iget-object v1, p0, Lcom/google/j/a/a/a/s;->c:[Lcom/google/j/a/a/a/u;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/j/a/a/a/s;->c:[Lcom/google/j/a/a/a/u;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 440
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/j/a/a/a/s;->c:[Lcom/google/j/a/a/a/u;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 441
    iget-object v2, p0, Lcom/google/j/a/a/a/s;->c:[Lcom/google/j/a/a/a/u;

    aget-object v2, v2, v0

    .line 442
    if-eqz v2, :cond_2

    .line 443
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 440
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 448
    :cond_4
    iget-boolean v1, p0, Lcom/google/j/a/a/a/s;->d:Z

    if-eqz v1, :cond_5

    .line 449
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/j/a/a/a/s;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 452
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 361
    if-ne p1, p0, :cond_1

    .line 389
    :cond_0
    :goto_0
    return v0

    .line 364
    :cond_1
    instance-of v2, p1, Lcom/google/j/a/a/a/s;

    if-nez v2, :cond_2

    move v0, v1

    .line 365
    goto :goto_0

    .line 367
    :cond_2
    check-cast p1, Lcom/google/j/a/a/a/s;

    .line 368
    iget-object v2, p0, Lcom/google/j/a/a/a/s;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 369
    iget-object v2, p1, Lcom/google/j/a/a/a/s;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 370
    goto :goto_0

    .line 372
    :cond_3
    iget-object v2, p0, Lcom/google/j/a/a/a/s;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/j/a/a/a/s;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 373
    goto :goto_0

    .line 375
    :cond_4
    iget-object v2, p0, Lcom/google/j/a/a/a/s;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 376
    iget-object v2, p1, Lcom/google/j/a/a/a/s;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 377
    goto :goto_0

    .line 379
    :cond_5
    iget-object v2, p0, Lcom/google/j/a/a/a/s;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/j/a/a/a/s;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 380
    goto :goto_0

    .line 382
    :cond_6
    iget-object v2, p0, Lcom/google/j/a/a/a/s;->c:[Lcom/google/j/a/a/a/u;

    iget-object v3, p1, Lcom/google/j/a/a/a/s;->c:[Lcom/google/j/a/a/a/u;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 384
    goto :goto_0

    .line 386
    :cond_7
    iget-boolean v2, p0, Lcom/google/j/a/a/a/s;->d:Z

    iget-boolean v3, p1, Lcom/google/j/a/a/a/s;->d:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 387
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 394
    iget-object v0, p0, Lcom/google/j/a/a/a/s;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 397
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/j/a/a/a/s;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 399
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/j/a/a/a/s;->c:[Lcom/google/j/a/a/a/u;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 401
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/j/a/a/a/s;->d:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x4cf

    :goto_2
    add-int/2addr v0, v1

    .line 402
    return v0

    .line 394
    :cond_0
    iget-object v0, p0, Lcom/google/j/a/a/a/s;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 397
    :cond_1
    iget-object v1, p0, Lcom/google/j/a/a/a/s;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 401
    :cond_2
    const/16 v0, 0x4d5

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 317
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/a/a/a/s;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/a/a/a/s;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/j/a/a/a/s;->c:[Lcom/google/j/a/a/a/u;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/j/a/a/a/u;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/j/a/a/a/s;->c:[Lcom/google/j/a/a/a/u;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/j/a/a/a/u;

    invoke-direct {v3}, Lcom/google/j/a/a/a/u;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/j/a/a/a/s;->c:[Lcom/google/j/a/a/a/u;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/j/a/a/a/u;

    invoke-direct {v3}, Lcom/google/j/a/a/a/u;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/j/a/a/a/s;->c:[Lcom/google/j/a/a/a/u;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/j/a/a/a/s;->d:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 408
    iget-object v0, p0, Lcom/google/j/a/a/a/s;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 409
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/j/a/a/a/s;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 411
    :cond_0
    iget-object v0, p0, Lcom/google/j/a/a/a/s;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 412
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/j/a/a/a/s;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 414
    :cond_1
    iget-object v0, p0, Lcom/google/j/a/a/a/s;->c:[Lcom/google/j/a/a/a/u;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/j/a/a/a/s;->c:[Lcom/google/j/a/a/a/u;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 415
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/j/a/a/a/s;->c:[Lcom/google/j/a/a/a/u;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 416
    iget-object v1, p0, Lcom/google/j/a/a/a/s;->c:[Lcom/google/j/a/a/a/u;

    aget-object v1, v1, v0

    .line 417
    if-eqz v1, :cond_2

    .line 418
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 415
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 422
    :cond_3
    iget-boolean v0, p0, Lcom/google/j/a/a/a/s;->d:Z

    if-eqz v0, :cond_4

    .line 423
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/j/a/a/a/s;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 425
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 426
    return-void
.end method
