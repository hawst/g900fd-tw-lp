.class public final Lcom/google/j/a/a/a/l;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:D

.field public d:Lcom/google/j/a/a/a/n;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 423
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 424
    const-string v0, ""

    iput-object v0, p0, Lcom/google/j/a/a/a/l;->a:Ljava/lang/String;

    iput v2, p0, Lcom/google/j/a/a/a/l;->b:I

    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/google/j/a/a/a/l;->c:D

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/j/a/a/a/l;->d:Lcom/google/j/a/a/a/n;

    iput v2, p0, Lcom/google/j/a/a/a/l;->cachedSize:I

    .line 425
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 509
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 510
    iget-object v1, p0, Lcom/google/j/a/a/a/l;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 511
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/j/a/a/a/l;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 514
    :cond_0
    iget v1, p0, Lcom/google/j/a/a/a/l;->b:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 515
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/j/a/a/a/l;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 518
    :cond_1
    iget-wide v2, p0, Lcom/google/j/a/a/a/l;->c:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 520
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/j/a/a/a/l;->c:D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 523
    :cond_2
    iget-object v1, p0, Lcom/google/j/a/a/a/l;->d:Lcom/google/j/a/a/a/n;

    if-eqz v1, :cond_3

    .line 524
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/j/a/a/a/l;->d:Lcom/google/j/a/a/a/n;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 527
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 438
    if-ne p1, p0, :cond_1

    .line 470
    :cond_0
    :goto_0
    return v0

    .line 441
    :cond_1
    instance-of v2, p1, Lcom/google/j/a/a/a/l;

    if-nez v2, :cond_2

    move v0, v1

    .line 442
    goto :goto_0

    .line 444
    :cond_2
    check-cast p1, Lcom/google/j/a/a/a/l;

    .line 445
    iget-object v2, p0, Lcom/google/j/a/a/a/l;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 446
    iget-object v2, p1, Lcom/google/j/a/a/a/l;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 447
    goto :goto_0

    .line 449
    :cond_3
    iget-object v2, p0, Lcom/google/j/a/a/a/l;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/j/a/a/a/l;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 450
    goto :goto_0

    .line 452
    :cond_4
    iget v2, p0, Lcom/google/j/a/a/a/l;->b:I

    iget v3, p1, Lcom/google/j/a/a/a/l;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 453
    goto :goto_0

    .line 456
    :cond_5
    iget-wide v2, p0, Lcom/google/j/a/a/a/l;->c:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 457
    iget-wide v4, p1, Lcom/google/j/a/a/a/l;->c:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    .line 458
    goto :goto_0

    .line 461
    :cond_6
    iget-object v2, p0, Lcom/google/j/a/a/a/l;->d:Lcom/google/j/a/a/a/n;

    if-nez v2, :cond_7

    .line 462
    iget-object v2, p1, Lcom/google/j/a/a/a/l;->d:Lcom/google/j/a/a/a/n;

    if-eqz v2, :cond_0

    move v0, v1

    .line 463
    goto :goto_0

    .line 466
    :cond_7
    iget-object v2, p0, Lcom/google/j/a/a/a/l;->d:Lcom/google/j/a/a/a/n;

    iget-object v3, p1, Lcom/google/j/a/a/a/l;->d:Lcom/google/j/a/a/a/n;

    invoke-virtual {v2, v3}, Lcom/google/j/a/a/a/n;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 467
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 475
    iget-object v0, p0, Lcom/google/j/a/a/a/l;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 478
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/j/a/a/a/l;->b:I

    add-int/2addr v0, v2

    .line 480
    iget-wide v2, p0, Lcom/google/j/a/a/a/l;->c:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 481
    mul-int/lit8 v0, v0, 0x1f

    const/16 v4, 0x20

    ushr-long v4, v2, v4

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 483
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/j/a/a/a/l;->d:Lcom/google/j/a/a/a/n;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 485
    return v0

    .line 475
    :cond_0
    iget-object v0, p0, Lcom/google/j/a/a/a/l;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 483
    :cond_1
    iget-object v1, p0, Lcom/google/j/a/a/a/l;->d:Lcom/google/j/a/a/a/n;

    invoke-virtual {v1}, Lcom/google/j/a/a/a/n;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 394
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/a/a/a/l;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/j/a/a/a/l;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/j/a/a/a/l;->c:D

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/j/a/a/a/l;->d:Lcom/google/j/a/a/a/n;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/j/a/a/a/n;

    invoke-direct {v0}, Lcom/google/j/a/a/a/n;-><init>()V

    iput-object v0, p0, Lcom/google/j/a/a/a/l;->d:Lcom/google/j/a/a/a/n;

    :cond_1
    iget-object v0, p0, Lcom/google/j/a/a/a/l;->d:Lcom/google/j/a/a/a/n;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x19 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 491
    iget-object v0, p0, Lcom/google/j/a/a/a/l;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 492
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/j/a/a/a/l;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 494
    :cond_0
    iget v0, p0, Lcom/google/j/a/a/a/l;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 495
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/j/a/a/a/l;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 497
    :cond_1
    iget-wide v0, p0, Lcom/google/j/a/a/a/l;->c:D

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 499
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/j/a/a/a/l;->c:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 501
    :cond_2
    iget-object v0, p0, Lcom/google/j/a/a/a/l;->d:Lcom/google/j/a/a/a/n;

    if-eqz v0, :cond_3

    .line 502
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/j/a/a/a/l;->d:Lcom/google/j/a/a/a/n;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 504
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 505
    return-void
.end method
