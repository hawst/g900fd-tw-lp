.class public final Lcom/google/j/a/a/a/ae;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile f:[Lcom/google/j/a/a/a/ae;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Lcom/google/j/a/a/a/y;

.field public d:Lcom/google/j/a/a/a/aa;

.field public e:Lcom/google/j/a/a/a/h;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcom/google/j/a/a/a/ae;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/j/a/a/a/ae;->b:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/j/a/a/a/ae;->c:Lcom/google/j/a/a/a/y;

    iput-object v1, p0, Lcom/google/j/a/a/a/ae;->d:Lcom/google/j/a/a/a/aa;

    iput-object v1, p0, Lcom/google/j/a/a/a/ae;->e:Lcom/google/j/a/a/a/h;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/j/a/a/a/ae;->cachedSize:I

    .line 42
    return-void
.end method

.method public static a()[Lcom/google/j/a/a/a/ae;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Lcom/google/j/a/a/a/ae;->f:[Lcom/google/j/a/a/a/ae;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lcom/google/j/a/a/a/ae;->f:[Lcom/google/j/a/a/a/ae;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/j/a/a/a/ae;

    sput-object v0, Lcom/google/j/a/a/a/ae;->f:[Lcom/google/j/a/a/a/ae;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Lcom/google/j/a/a/a/ae;->f:[Lcom/google/j/a/a/a/ae;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 144
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 145
    iget-object v1, p0, Lcom/google/j/a/a/a/ae;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 146
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/j/a/a/a/ae;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 149
    :cond_0
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/j/a/a/a/ae;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 151
    iget-object v1, p0, Lcom/google/j/a/a/a/ae;->c:Lcom/google/j/a/a/a/y;

    if-eqz v1, :cond_1

    .line 152
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/j/a/a/a/ae;->c:Lcom/google/j/a/a/a/y;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 155
    :cond_1
    iget-object v1, p0, Lcom/google/j/a/a/a/ae;->d:Lcom/google/j/a/a/a/aa;

    if-eqz v1, :cond_2

    .line 156
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/j/a/a/a/ae;->d:Lcom/google/j/a/a/a/aa;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 159
    :cond_2
    iget-object v1, p0, Lcom/google/j/a/a/a/ae;->e:Lcom/google/j/a/a/a/h;

    if-eqz v1, :cond_3

    .line 160
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/j/a/a/a/ae;->e:Lcom/google/j/a/a/a/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 163
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 56
    if-ne p1, p0, :cond_1

    .line 104
    :cond_0
    :goto_0
    return v0

    .line 59
    :cond_1
    instance-of v2, p1, Lcom/google/j/a/a/a/ae;

    if-nez v2, :cond_2

    move v0, v1

    .line 60
    goto :goto_0

    .line 62
    :cond_2
    check-cast p1, Lcom/google/j/a/a/a/ae;

    .line 63
    iget-object v2, p0, Lcom/google/j/a/a/a/ae;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 64
    iget-object v2, p1, Lcom/google/j/a/a/a/ae;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 65
    goto :goto_0

    .line 67
    :cond_3
    iget-object v2, p0, Lcom/google/j/a/a/a/ae;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/j/a/a/a/ae;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 68
    goto :goto_0

    .line 70
    :cond_4
    iget-object v2, p0, Lcom/google/j/a/a/a/ae;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 71
    iget-object v2, p1, Lcom/google/j/a/a/a/ae;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 72
    goto :goto_0

    .line 74
    :cond_5
    iget-object v2, p0, Lcom/google/j/a/a/a/ae;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/j/a/a/a/ae;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 75
    goto :goto_0

    .line 77
    :cond_6
    iget-object v2, p0, Lcom/google/j/a/a/a/ae;->c:Lcom/google/j/a/a/a/y;

    if-nez v2, :cond_7

    .line 78
    iget-object v2, p1, Lcom/google/j/a/a/a/ae;->c:Lcom/google/j/a/a/a/y;

    if-eqz v2, :cond_8

    move v0, v1

    .line 79
    goto :goto_0

    .line 82
    :cond_7
    iget-object v2, p0, Lcom/google/j/a/a/a/ae;->c:Lcom/google/j/a/a/a/y;

    iget-object v3, p1, Lcom/google/j/a/a/a/ae;->c:Lcom/google/j/a/a/a/y;

    invoke-virtual {v2, v3}, Lcom/google/j/a/a/a/y;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 83
    goto :goto_0

    .line 86
    :cond_8
    iget-object v2, p0, Lcom/google/j/a/a/a/ae;->d:Lcom/google/j/a/a/a/aa;

    if-nez v2, :cond_9

    .line 87
    iget-object v2, p1, Lcom/google/j/a/a/a/ae;->d:Lcom/google/j/a/a/a/aa;

    if-eqz v2, :cond_a

    move v0, v1

    .line 88
    goto :goto_0

    .line 91
    :cond_9
    iget-object v2, p0, Lcom/google/j/a/a/a/ae;->d:Lcom/google/j/a/a/a/aa;

    iget-object v3, p1, Lcom/google/j/a/a/a/ae;->d:Lcom/google/j/a/a/a/aa;

    invoke-virtual {v2, v3}, Lcom/google/j/a/a/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 92
    goto :goto_0

    .line 95
    :cond_a
    iget-object v2, p0, Lcom/google/j/a/a/a/ae;->e:Lcom/google/j/a/a/a/h;

    if-nez v2, :cond_b

    .line 96
    iget-object v2, p1, Lcom/google/j/a/a/a/ae;->e:Lcom/google/j/a/a/a/h;

    if-eqz v2, :cond_0

    move v0, v1

    .line 97
    goto :goto_0

    .line 100
    :cond_b
    iget-object v2, p0, Lcom/google/j/a/a/a/ae;->e:Lcom/google/j/a/a/a/h;

    iget-object v3, p1, Lcom/google/j/a/a/a/ae;->e:Lcom/google/j/a/a/a/h;

    invoke-virtual {v2, v3}, Lcom/google/j/a/a/a/h;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 101
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 109
    iget-object v0, p0, Lcom/google/j/a/a/a/ae;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 112
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/j/a/a/a/ae;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 114
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/j/a/a/a/ae;->c:Lcom/google/j/a/a/a/y;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 116
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/j/a/a/a/ae;->d:Lcom/google/j/a/a/a/aa;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 118
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/j/a/a/a/ae;->e:Lcom/google/j/a/a/a/h;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 120
    return v0

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/google/j/a/a/a/ae;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 112
    :cond_1
    iget-object v0, p0, Lcom/google/j/a/a/a/ae;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 114
    :cond_2
    iget-object v0, p0, Lcom/google/j/a/a/a/ae;->c:Lcom/google/j/a/a/a/y;

    invoke-virtual {v0}, Lcom/google/j/a/a/a/y;->hashCode()I

    move-result v0

    goto :goto_2

    .line 116
    :cond_3
    iget-object v0, p0, Lcom/google/j/a/a/a/ae;->d:Lcom/google/j/a/a/a/aa;

    invoke-virtual {v0}, Lcom/google/j/a/a/a/aa;->hashCode()I

    move-result v0

    goto :goto_3

    .line 118
    :cond_4
    iget-object v1, p0, Lcom/google/j/a/a/a/ae;->e:Lcom/google/j/a/a/a/h;

    invoke-virtual {v1}, Lcom/google/j/a/a/a/h;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/a/a/a/ae;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/a/a/a/ae;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/j/a/a/a/ae;->c:Lcom/google/j/a/a/a/y;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/j/a/a/a/y;

    invoke-direct {v0}, Lcom/google/j/a/a/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/j/a/a/a/ae;->c:Lcom/google/j/a/a/a/y;

    :cond_1
    iget-object v0, p0, Lcom/google/j/a/a/a/ae;->c:Lcom/google/j/a/a/a/y;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/j/a/a/a/ae;->d:Lcom/google/j/a/a/a/aa;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/j/a/a/a/aa;

    invoke-direct {v0}, Lcom/google/j/a/a/a/aa;-><init>()V

    iput-object v0, p0, Lcom/google/j/a/a/a/ae;->d:Lcom/google/j/a/a/a/aa;

    :cond_2
    iget-object v0, p0, Lcom/google/j/a/a/a/ae;->d:Lcom/google/j/a/a/a/aa;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/j/a/a/a/ae;->e:Lcom/google/j/a/a/a/h;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/j/a/a/a/h;

    invoke-direct {v0}, Lcom/google/j/a/a/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/j/a/a/a/ae;->e:Lcom/google/j/a/a/a/h;

    :cond_3
    iget-object v0, p0, Lcom/google/j/a/a/a/ae;->e:Lcom/google/j/a/a/a/h;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/j/a/a/a/ae;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/j/a/a/a/ae;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 129
    :cond_0
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/j/a/a/a/ae;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 130
    iget-object v0, p0, Lcom/google/j/a/a/a/ae;->c:Lcom/google/j/a/a/a/y;

    if-eqz v0, :cond_1

    .line 131
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/j/a/a/a/ae;->c:Lcom/google/j/a/a/a/y;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 133
    :cond_1
    iget-object v0, p0, Lcom/google/j/a/a/a/ae;->d:Lcom/google/j/a/a/a/aa;

    if-eqz v0, :cond_2

    .line 134
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/j/a/a/a/ae;->d:Lcom/google/j/a/a/a/aa;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 136
    :cond_2
    iget-object v0, p0, Lcom/google/j/a/a/a/ae;->e:Lcom/google/j/a/a/a/h;

    if-eqz v0, :cond_3

    .line 137
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/j/a/a/a/ae;->e:Lcom/google/j/a/a/a/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 139
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 140
    return-void
.end method
