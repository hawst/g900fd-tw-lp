.class public final Lcom/google/j/a/a/a/r;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile b:[Lcom/google/j/a/a/a/r;


# instance fields
.field public a:Lcom/google/j/a/a/a/h;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 538
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 539
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/j/a/a/a/r;->a:Lcom/google/j/a/a/a/h;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/j/a/a/a/r;->cachedSize:I

    .line 540
    return-void
.end method

.method public static a()[Lcom/google/j/a/a/a/r;
    .locals 2

    .prologue
    .line 524
    sget-object v0, Lcom/google/j/a/a/a/r;->b:[Lcom/google/j/a/a/a/r;

    if-nez v0, :cond_1

    .line 525
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 527
    :try_start_0
    sget-object v0, Lcom/google/j/a/a/a/r;->b:[Lcom/google/j/a/a/a/r;

    if-nez v0, :cond_0

    .line 528
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/j/a/a/a/r;

    sput-object v0, Lcom/google/j/a/a/a/r;->b:[Lcom/google/j/a/a/a/r;

    .line 530
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 532
    :cond_1
    sget-object v0, Lcom/google/j/a/a/a/r;->b:[Lcom/google/j/a/a/a/r;

    return-object v0

    .line 530
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 588
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 589
    iget-object v1, p0, Lcom/google/j/a/a/a/r;->a:Lcom/google/j/a/a/a/h;

    if-eqz v1, :cond_0

    .line 590
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/j/a/a/a/r;->a:Lcom/google/j/a/a/a/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 593
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 550
    if-ne p1, p0, :cond_1

    .line 566
    :cond_0
    :goto_0
    return v0

    .line 553
    :cond_1
    instance-of v2, p1, Lcom/google/j/a/a/a/r;

    if-nez v2, :cond_2

    move v0, v1

    .line 554
    goto :goto_0

    .line 556
    :cond_2
    check-cast p1, Lcom/google/j/a/a/a/r;

    .line 557
    iget-object v2, p0, Lcom/google/j/a/a/a/r;->a:Lcom/google/j/a/a/a/h;

    if-nez v2, :cond_3

    .line 558
    iget-object v2, p1, Lcom/google/j/a/a/a/r;->a:Lcom/google/j/a/a/a/h;

    if-eqz v2, :cond_0

    move v0, v1

    .line 559
    goto :goto_0

    .line 562
    :cond_3
    iget-object v2, p0, Lcom/google/j/a/a/a/r;->a:Lcom/google/j/a/a/a/h;

    iget-object v3, p1, Lcom/google/j/a/a/a/r;->a:Lcom/google/j/a/a/a/h;

    invoke-virtual {v2, v3}, Lcom/google/j/a/a/a/h;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 563
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 571
    iget-object v0, p0, Lcom/google/j/a/a/a/r;->a:Lcom/google/j/a/a/a/h;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 574
    return v0

    .line 571
    :cond_0
    iget-object v0, p0, Lcom/google/j/a/a/a/r;->a:Lcom/google/j/a/a/a/h;

    invoke-virtual {v0}, Lcom/google/j/a/a/a/h;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 518
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/j/a/a/a/r;->a:Lcom/google/j/a/a/a/h;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/j/a/a/a/h;

    invoke-direct {v0}, Lcom/google/j/a/a/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/j/a/a/a/r;->a:Lcom/google/j/a/a/a/h;

    :cond_1
    iget-object v0, p0, Lcom/google/j/a/a/a/r;->a:Lcom/google/j/a/a/a/h;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/j/a/a/a/r;->a:Lcom/google/j/a/a/a/h;

    if-eqz v0, :cond_0

    .line 581
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/j/a/a/a/r;->a:Lcom/google/j/a/a/a/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 583
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 584
    return-void
.end method
