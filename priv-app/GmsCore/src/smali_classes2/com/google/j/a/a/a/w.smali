.class public final Lcom/google/j/a/a/a/w;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/j/a/a/a/w;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 657
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 658
    const-string v0, ""

    iput-object v0, p0, Lcom/google/j/a/a/a/w;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/j/a/a/a/w;->b:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/j/a/a/a/w;->cachedSize:I

    .line 659
    return-void
.end method

.method public static a()[Lcom/google/j/a/a/a/w;
    .locals 2

    .prologue
    .line 640
    sget-object v0, Lcom/google/j/a/a/a/w;->c:[Lcom/google/j/a/a/a/w;

    if-nez v0, :cond_1

    .line 641
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 643
    :try_start_0
    sget-object v0, Lcom/google/j/a/a/a/w;->c:[Lcom/google/j/a/a/a/w;

    if-nez v0, :cond_0

    .line 644
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/j/a/a/a/w;

    sput-object v0, Lcom/google/j/a/a/a/w;->c:[Lcom/google/j/a/a/a/w;

    .line 646
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 648
    :cond_1
    sget-object v0, Lcom/google/j/a/a/a/w;->c:[Lcom/google/j/a/a/a/w;

    return-object v0

    .line 646
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 718
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 719
    iget-object v1, p0, Lcom/google/j/a/a/a/w;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 720
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/j/a/a/a/w;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 723
    :cond_0
    iget-object v1, p0, Lcom/google/j/a/a/a/w;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 724
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/j/a/a/a/w;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 727
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 670
    if-ne p1, p0, :cond_1

    .line 691
    :cond_0
    :goto_0
    return v0

    .line 673
    :cond_1
    instance-of v2, p1, Lcom/google/j/a/a/a/w;

    if-nez v2, :cond_2

    move v0, v1

    .line 674
    goto :goto_0

    .line 676
    :cond_2
    check-cast p1, Lcom/google/j/a/a/a/w;

    .line 677
    iget-object v2, p0, Lcom/google/j/a/a/a/w;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 678
    iget-object v2, p1, Lcom/google/j/a/a/a/w;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 679
    goto :goto_0

    .line 681
    :cond_3
    iget-object v2, p0, Lcom/google/j/a/a/a/w;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/j/a/a/a/w;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 682
    goto :goto_0

    .line 684
    :cond_4
    iget-object v2, p0, Lcom/google/j/a/a/a/w;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 685
    iget-object v2, p1, Lcom/google/j/a/a/a/w;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 686
    goto :goto_0

    .line 688
    :cond_5
    iget-object v2, p0, Lcom/google/j/a/a/a/w;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/j/a/a/a/w;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 689
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 696
    iget-object v0, p0, Lcom/google/j/a/a/a/w;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 699
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/j/a/a/a/w;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 701
    return v0

    .line 696
    :cond_0
    iget-object v0, p0, Lcom/google/j/a/a/a/w;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 699
    :cond_1
    iget-object v1, p0, Lcom/google/j/a/a/a/w;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 634
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/a/a/a/w;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/a/a/a/w;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 707
    iget-object v0, p0, Lcom/google/j/a/a/a/w;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 708
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/j/a/a/a/w;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 710
    :cond_0
    iget-object v0, p0, Lcom/google/j/a/a/a/w;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 711
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/j/a/a/a/w;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 713
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 714
    return-void
.end method
