.class public final Lcom/google/j/a/a/a/j;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/j/a/a/a/j;


# instance fields
.field public a:Lcom/google/j/a/a/a/ac;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/google/j/a/a/a/k;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 38
    iput-object v1, p0, Lcom/google/j/a/a/a/j;->a:Lcom/google/j/a/a/a/ac;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/j/a/a/a/j;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/j/a/a/a/j;->c:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/j/a/a/a/j;->d:Lcom/google/j/a/a/a/k;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/j/a/a/a/j;->cachedSize:I

    .line 39
    return-void
.end method

.method public static a()[Lcom/google/j/a/a/a/j;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Lcom/google/j/a/a/a/j;->e:[Lcom/google/j/a/a/a/j;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lcom/google/j/a/a/a/j;->e:[Lcom/google/j/a/a/a/j;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/j/a/a/a/j;

    sput-object v0, Lcom/google/j/a/a/a/j;->e:[Lcom/google/j/a/a/a/j;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Lcom/google/j/a/a/a/j;->e:[Lcom/google/j/a/a/a/j;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 128
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 129
    iget-object v1, p0, Lcom/google/j/a/a/a/j;->a:Lcom/google/j/a/a/a/ac;

    if-eqz v1, :cond_0

    .line 130
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/j/a/a/a/j;->a:Lcom/google/j/a/a/a/ac;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 133
    :cond_0
    iget-object v1, p0, Lcom/google/j/a/a/a/j;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 134
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/j/a/a/a/j;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 137
    :cond_1
    iget-object v1, p0, Lcom/google/j/a/a/a/j;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 138
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/j/a/a/a/j;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    :cond_2
    iget-object v1, p0, Lcom/google/j/a/a/a/j;->d:Lcom/google/j/a/a/a/k;

    if-eqz v1, :cond_3

    .line 142
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/j/a/a/a/j;->d:Lcom/google/j/a/a/a/k;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 145
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 52
    if-ne p1, p0, :cond_1

    .line 91
    :cond_0
    :goto_0
    return v0

    .line 55
    :cond_1
    instance-of v2, p1, Lcom/google/j/a/a/a/j;

    if-nez v2, :cond_2

    move v0, v1

    .line 56
    goto :goto_0

    .line 58
    :cond_2
    check-cast p1, Lcom/google/j/a/a/a/j;

    .line 59
    iget-object v2, p0, Lcom/google/j/a/a/a/j;->a:Lcom/google/j/a/a/a/ac;

    if-nez v2, :cond_3

    .line 60
    iget-object v2, p1, Lcom/google/j/a/a/a/j;->a:Lcom/google/j/a/a/a/ac;

    if-eqz v2, :cond_4

    move v0, v1

    .line 61
    goto :goto_0

    .line 64
    :cond_3
    iget-object v2, p0, Lcom/google/j/a/a/a/j;->a:Lcom/google/j/a/a/a/ac;

    iget-object v3, p1, Lcom/google/j/a/a/a/j;->a:Lcom/google/j/a/a/a/ac;

    invoke-virtual {v2, v3}, Lcom/google/j/a/a/a/ac;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 65
    goto :goto_0

    .line 68
    :cond_4
    iget-object v2, p0, Lcom/google/j/a/a/a/j;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 69
    iget-object v2, p1, Lcom/google/j/a/a/a/j;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 70
    goto :goto_0

    .line 72
    :cond_5
    iget-object v2, p0, Lcom/google/j/a/a/a/j;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/j/a/a/a/j;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 73
    goto :goto_0

    .line 75
    :cond_6
    iget-object v2, p0, Lcom/google/j/a/a/a/j;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 76
    iget-object v2, p1, Lcom/google/j/a/a/a/j;->c:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 77
    goto :goto_0

    .line 79
    :cond_7
    iget-object v2, p0, Lcom/google/j/a/a/a/j;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/j/a/a/a/j;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 80
    goto :goto_0

    .line 82
    :cond_8
    iget-object v2, p0, Lcom/google/j/a/a/a/j;->d:Lcom/google/j/a/a/a/k;

    if-nez v2, :cond_9

    .line 83
    iget-object v2, p1, Lcom/google/j/a/a/a/j;->d:Lcom/google/j/a/a/a/k;

    if-eqz v2, :cond_0

    move v0, v1

    .line 84
    goto :goto_0

    .line 87
    :cond_9
    iget-object v2, p0, Lcom/google/j/a/a/a/j;->d:Lcom/google/j/a/a/a/k;

    iget-object v3, p1, Lcom/google/j/a/a/a/j;->d:Lcom/google/j/a/a/a/k;

    invoke-virtual {v2, v3}, Lcom/google/j/a/a/a/k;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 88
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 96
    iget-object v0, p0, Lcom/google/j/a/a/a/j;->a:Lcom/google/j/a/a/a/ac;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 99
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/j/a/a/a/j;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 101
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/j/a/a/a/j;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 103
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/j/a/a/a/j;->d:Lcom/google/j/a/a/a/k;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 105
    return v0

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/google/j/a/a/a/j;->a:Lcom/google/j/a/a/a/ac;

    invoke-virtual {v0}, Lcom/google/j/a/a/a/ac;->hashCode()I

    move-result v0

    goto :goto_0

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/google/j/a/a/a/j;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 101
    :cond_2
    iget-object v0, p0, Lcom/google/j/a/a/a/j;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 103
    :cond_3
    iget-object v1, p0, Lcom/google/j/a/a/a/j;->d:Lcom/google/j/a/a/a/k;

    invoke-virtual {v1}, Lcom/google/j/a/a/a/k;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/j/a/a/a/j;->a:Lcom/google/j/a/a/a/ac;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/j/a/a/a/ac;

    invoke-direct {v0}, Lcom/google/j/a/a/a/ac;-><init>()V

    iput-object v0, p0, Lcom/google/j/a/a/a/j;->a:Lcom/google/j/a/a/a/ac;

    :cond_1
    iget-object v0, p0, Lcom/google/j/a/a/a/j;->a:Lcom/google/j/a/a/a/ac;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/a/a/a/j;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/a/a/a/j;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/j/a/a/a/j;->d:Lcom/google/j/a/a/a/k;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/j/a/a/a/k;

    invoke-direct {v0}, Lcom/google/j/a/a/a/k;-><init>()V

    iput-object v0, p0, Lcom/google/j/a/a/a/j;->d:Lcom/google/j/a/a/a/k;

    :cond_2
    iget-object v0, p0, Lcom/google/j/a/a/a/j;->d:Lcom/google/j/a/a/a/k;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/j/a/a/a/j;->a:Lcom/google/j/a/a/a/ac;

    if-eqz v0, :cond_0

    .line 112
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/j/a/a/a/j;->a:Lcom/google/j/a/a/a/ac;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/google/j/a/a/a/j;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 115
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/j/a/a/a/j;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/google/j/a/a/a/j;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 118
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/j/a/a/a/j;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 120
    :cond_2
    iget-object v0, p0, Lcom/google/j/a/a/a/j;->d:Lcom/google/j/a/a/a/k;

    if-eqz v0, :cond_3

    .line 121
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/j/a/a/a/j;->d:Lcom/google/j/a/a/a/k;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 123
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 124
    return-void
.end method
