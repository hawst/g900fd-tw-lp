.class public final Lcom/google/j/a/a/a/u;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/j/a/a/a/u;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:[Lcom/google/j/a/a/a/t;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 161
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 162
    const-string v0, ""

    iput-object v0, p0, Lcom/google/j/a/a/a/u;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/j/a/a/a/u;->b:Ljava/lang/String;

    invoke-static {}, Lcom/google/j/a/a/a/t;->a()[Lcom/google/j/a/a/a/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/a/a/a/u;->c:[Lcom/google/j/a/a/a/t;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/j/a/a/a/u;->cachedSize:I

    .line 163
    return-void
.end method

.method public static a()[Lcom/google/j/a/a/a/u;
    .locals 2

    .prologue
    .line 141
    sget-object v0, Lcom/google/j/a/a/a/u;->d:[Lcom/google/j/a/a/a/u;

    if-nez v0, :cond_1

    .line 142
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 144
    :try_start_0
    sget-object v0, Lcom/google/j/a/a/a/u;->d:[Lcom/google/j/a/a/a/u;

    if-nez v0, :cond_0

    .line 145
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/j/a/a/a/u;

    sput-object v0, Lcom/google/j/a/a/a/u;->d:[Lcom/google/j/a/a/a/u;

    .line 147
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    :cond_1
    sget-object v0, Lcom/google/j/a/a/a/u;->d:[Lcom/google/j/a/a/a/u;

    return-object v0

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 237
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 238
    iget-object v1, p0, Lcom/google/j/a/a/a/u;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 239
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/j/a/a/a/u;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 242
    :cond_0
    iget-object v1, p0, Lcom/google/j/a/a/a/u;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 243
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/j/a/a/a/u;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 246
    :cond_1
    iget-object v1, p0, Lcom/google/j/a/a/a/u;->c:[Lcom/google/j/a/a/a/t;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/j/a/a/a/u;->c:[Lcom/google/j/a/a/a/t;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 247
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/j/a/a/a/u;->c:[Lcom/google/j/a/a/a/t;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 248
    iget-object v2, p0, Lcom/google/j/a/a/a/u;->c:[Lcom/google/j/a/a/a/t;

    aget-object v2, v2, v0

    .line 249
    if-eqz v2, :cond_2

    .line 250
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 247
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 255
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 175
    if-ne p1, p0, :cond_1

    .line 200
    :cond_0
    :goto_0
    return v0

    .line 178
    :cond_1
    instance-of v2, p1, Lcom/google/j/a/a/a/u;

    if-nez v2, :cond_2

    move v0, v1

    .line 179
    goto :goto_0

    .line 181
    :cond_2
    check-cast p1, Lcom/google/j/a/a/a/u;

    .line 182
    iget-object v2, p0, Lcom/google/j/a/a/a/u;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 183
    iget-object v2, p1, Lcom/google/j/a/a/a/u;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 184
    goto :goto_0

    .line 186
    :cond_3
    iget-object v2, p0, Lcom/google/j/a/a/a/u;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/j/a/a/a/u;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 187
    goto :goto_0

    .line 189
    :cond_4
    iget-object v2, p0, Lcom/google/j/a/a/a/u;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 190
    iget-object v2, p1, Lcom/google/j/a/a/a/u;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 191
    goto :goto_0

    .line 193
    :cond_5
    iget-object v2, p0, Lcom/google/j/a/a/a/u;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/j/a/a/a/u;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 194
    goto :goto_0

    .line 196
    :cond_6
    iget-object v2, p0, Lcom/google/j/a/a/a/u;->c:[Lcom/google/j/a/a/a/t;

    iget-object v3, p1, Lcom/google/j/a/a/a/u;->c:[Lcom/google/j/a/a/a/t;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 198
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 205
    iget-object v0, p0, Lcom/google/j/a/a/a/u;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 208
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/j/a/a/a/u;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 210
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/j/a/a/a/u;->c:[Lcom/google/j/a/a/a/t;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 212
    return v0

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/google/j/a/a/a/u;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 208
    :cond_1
    iget-object v1, p0, Lcom/google/j/a/a/a/u;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 135
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/a/a/a/u;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/a/a/a/u;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/j/a/a/a/u;->c:[Lcom/google/j/a/a/a/t;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/j/a/a/a/t;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/j/a/a/a/u;->c:[Lcom/google/j/a/a/a/t;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/j/a/a/a/t;

    invoke-direct {v3}, Lcom/google/j/a/a/a/t;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/j/a/a/a/u;->c:[Lcom/google/j/a/a/a/t;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/j/a/a/a/t;

    invoke-direct {v3}, Lcom/google/j/a/a/a/t;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/j/a/a/a/u;->c:[Lcom/google/j/a/a/a/t;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/j/a/a/a/u;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 219
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/j/a/a/a/u;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/google/j/a/a/a/u;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 222
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/j/a/a/a/u;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 224
    :cond_1
    iget-object v0, p0, Lcom/google/j/a/a/a/u;->c:[Lcom/google/j/a/a/a/t;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/j/a/a/a/u;->c:[Lcom/google/j/a/a/a/t;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 225
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/j/a/a/a/u;->c:[Lcom/google/j/a/a/a/t;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 226
    iget-object v1, p0, Lcom/google/j/a/a/a/u;->c:[Lcom/google/j/a/a/a/t;

    aget-object v1, v1, v0

    .line 227
    if-eqz v1, :cond_2

    .line 228
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 225
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 232
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 233
    return-void
.end method
