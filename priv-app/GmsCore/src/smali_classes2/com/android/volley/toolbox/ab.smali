.class public Lcom/android/volley/toolbox/ab;
.super Lcom/android/volley/p;
.source "SourceFile"


# instance fields
.field private final f:Lcom/android/volley/x;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p3}, Lcom/android/volley/p;-><init>(ILjava/lang/String;Lcom/android/volley/w;)V

    .line 44
    iput-object p2, p0, Lcom/android/volley/toolbox/ab;->f:Lcom/android/volley/x;

    .line 45
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;B)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Lcom/android/volley/toolbox/ab;-><init>(Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 56
    return-void
.end method


# virtual methods
.method protected final a(Lcom/android/volley/m;)Lcom/android/volley/v;
    .locals 3

    .prologue
    .line 67
    :try_start_0
    new-instance v0, Ljava/lang/String;

    iget-object v1, p1, Lcom/android/volley/m;->b:[B

    iget-object v2, p1, Lcom/android/volley/m;->c:Ljava/util/Map;

    invoke-static {v2}, Lcom/android/volley/toolbox/i;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    :goto_0
    invoke-static {p1}, Lcom/android/volley/toolbox/i;->a(Lcom/android/volley/m;)Lcom/android/volley/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/volley/v;->a(Ljava/lang/Object;Lcom/android/volley/c;)Lcom/android/volley/v;

    move-result-object v0

    return-object v0

    .line 69
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/String;

    iget-object v1, p1, Lcom/android/volley/m;->b:[B

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_0
.end method

.method protected final synthetic b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 30
    check-cast p1, Ljava/lang/String;

    iget-object v0, p0, Lcom/android/volley/toolbox/ab;->f:Lcom/android/volley/x;

    invoke-interface {v0, p1}, Lcom/android/volley/x;->a(Ljava/lang/Object;)V

    return-void
.end method
