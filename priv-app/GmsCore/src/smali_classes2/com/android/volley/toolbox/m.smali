.class public final Lcom/android/volley/toolbox/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/android/volley/toolbox/r;

.field final b:Ljava/util/HashMap;

.field final c:Ljava/util/HashMap;

.field d:Ljava/lang/Runnable;

.field private final e:Lcom/android/volley/s;

.field private f:I

.field private final g:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/android/volley/s;Lcom/android/volley/toolbox/r;)V
    .locals 2

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/16 v0, 0x64

    iput v0, p0, Lcom/android/volley/toolbox/m;->f:I

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/volley/toolbox/m;->b:Ljava/util/HashMap;

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/volley/toolbox/m;->c:Ljava/util/HashMap;

    .line 65
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/volley/toolbox/m;->g:Landroid/os/Handler;

    .line 86
    iput-object p1, p0, Lcom/android/volley/toolbox/m;->e:Lcom/android/volley/s;

    .line 87
    iput-object p2, p0, Lcom/android/volley/toolbox/m;->a:Lcom/android/volley/toolbox/r;

    .line 88
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/android/volley/toolbox/t;II)Lcom/android/volley/toolbox/s;
    .locals 13

    .prologue
    .line 189
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "ImageLoader must be invoked from the main thread."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 191
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xc

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "#W"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#H"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 194
    iget-object v1, p0, Lcom/android/volley/toolbox/m;->a:Lcom/android/volley/toolbox/r;

    invoke-interface {v1, v5}, Lcom/android/volley/toolbox/r;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 195
    if-eqz v3, :cond_1

    .line 197
    new-instance v1, Lcom/android/volley/toolbox/s;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p0

    move-object v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/android/volley/toolbox/s;-><init>(Lcom/android/volley/toolbox/m;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/toolbox/t;)V

    .line 198
    const/4 v2, 0x1

    invoke-interface {p2, v1, v2}, Lcom/android/volley/toolbox/t;->a(Lcom/android/volley/toolbox/s;Z)V

    .line 236
    :goto_0
    return-object v1

    .line 203
    :cond_1
    new-instance v1, Lcom/android/volley/toolbox/s;

    const/4 v3, 0x0

    move-object v2, p0

    move-object v4, p1

    move-object v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/android/volley/toolbox/s;-><init>(Lcom/android/volley/toolbox/m;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/toolbox/t;)V

    .line 207
    const/4 v2, 0x1

    invoke-interface {p2, v1, v2}, Lcom/android/volley/toolbox/t;->a(Lcom/android/volley/toolbox/s;Z)V

    .line 210
    iget-object v2, p0, Lcom/android/volley/toolbox/m;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/volley/toolbox/q;

    .line 211
    if-eqz v2, :cond_2

    .line 213
    iget-object v2, v2, Lcom/android/volley/toolbox/q;->c:Ljava/util/LinkedList;

    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 219
    :cond_2
    new-instance v6, Lcom/android/volley/toolbox/u;

    new-instance v8, Lcom/android/volley/toolbox/n;

    invoke-direct {v8, p0, v5}, Lcom/android/volley/toolbox/n;-><init>(Lcom/android/volley/toolbox/m;Ljava/lang/String;)V

    sget-object v11, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    new-instance v12, Lcom/android/volley/toolbox/o;

    invoke-direct {v12, p0, v5}, Lcom/android/volley/toolbox/o;-><init>(Lcom/android/volley/toolbox/m;Ljava/lang/String;)V

    move-object v7, p1

    move/from16 v9, p3

    move/from16 v10, p4

    invoke-direct/range {v6 .. v12}, Lcom/android/volley/toolbox/u;-><init>(Ljava/lang/String;Lcom/android/volley/x;IILandroid/graphics/Bitmap$Config;Lcom/android/volley/w;)V

    .line 233
    iget-object v2, p0, Lcom/android/volley/toolbox/m;->e:Lcom/android/volley/s;

    invoke-virtual {v2, v6}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    .line 234
    iget-object v2, p0, Lcom/android/volley/toolbox/m;->b:Ljava/util/HashMap;

    new-instance v3, Lcom/android/volley/toolbox/q;

    invoke-direct {v3, p0, v6, v1}, Lcom/android/volley/toolbox/q;-><init>(Lcom/android/volley/toolbox/m;Lcom/android/volley/p;Lcom/android/volley/toolbox/s;)V

    invoke-virtual {v2, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method final a(Ljava/lang/String;Lcom/android/volley/toolbox/q;)V
    .locals 4

    .prologue
    .line 432
    iget-object v0, p0, Lcom/android/volley/toolbox/m;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 435
    iget-object v0, p0, Lcom/android/volley/toolbox/m;->d:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 436
    new-instance v0, Lcom/android/volley/toolbox/p;

    invoke-direct {v0, p0}, Lcom/android/volley/toolbox/p;-><init>(Lcom/android/volley/toolbox/m;)V

    iput-object v0, p0, Lcom/android/volley/toolbox/m;->d:Ljava/lang/Runnable;

    .line 461
    iget-object v0, p0, Lcom/android/volley/toolbox/m;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/volley/toolbox/m;->d:Ljava/lang/Runnable;

    iget v2, p0, Lcom/android/volley/toolbox/m;->f:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 463
    :cond_0
    return-void
.end method
