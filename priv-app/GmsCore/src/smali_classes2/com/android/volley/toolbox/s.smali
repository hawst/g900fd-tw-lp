.class public final Lcom/android/volley/toolbox/s;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Landroid/graphics/Bitmap;

.field final b:Lcom/android/volley/toolbox/t;

.field final c:Ljava/lang/String;

.field final synthetic d:Lcom/android/volley/toolbox/m;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/volley/toolbox/m;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/toolbox/t;)V
    .locals 0

    .prologue
    .line 312
    iput-object p1, p0, Lcom/android/volley/toolbox/s;->d:Lcom/android/volley/toolbox/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 313
    iput-object p2, p0, Lcom/android/volley/toolbox/s;->a:Landroid/graphics/Bitmap;

    .line 314
    iput-object p3, p0, Lcom/android/volley/toolbox/s;->c:Ljava/lang/String;

    .line 315
    iput-object p4, p0, Lcom/android/volley/toolbox/s;->e:Ljava/lang/String;

    .line 316
    iput-object p5, p0, Lcom/android/volley/toolbox/s;->b:Lcom/android/volley/toolbox/t;

    .line 317
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 323
    iget-object v0, p0, Lcom/android/volley/toolbox/s;->b:Lcom/android/volley/toolbox/t;

    if-nez v0, :cond_1

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 327
    :cond_1
    iget-object v0, p0, Lcom/android/volley/toolbox/s;->d:Lcom/android/volley/toolbox/m;

    iget-object v0, v0, Lcom/android/volley/toolbox/m;->b:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/volley/toolbox/s;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/volley/toolbox/q;

    .line 328
    if-eqz v0, :cond_2

    .line 329
    invoke-virtual {v0, p0}, Lcom/android/volley/toolbox/q;->a(Lcom/android/volley/toolbox/s;)Z

    move-result v0

    .line 330
    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/android/volley/toolbox/s;->d:Lcom/android/volley/toolbox/m;

    iget-object v0, v0, Lcom/android/volley/toolbox/m;->b:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/volley/toolbox/s;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 335
    :cond_2
    iget-object v0, p0, Lcom/android/volley/toolbox/s;->d:Lcom/android/volley/toolbox/m;

    iget-object v0, v0, Lcom/android/volley/toolbox/m;->c:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/volley/toolbox/s;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/volley/toolbox/q;

    .line 336
    if-eqz v0, :cond_0

    .line 337
    invoke-virtual {v0, p0}, Lcom/android/volley/toolbox/q;->a(Lcom/android/volley/toolbox/s;)Z

    .line 338
    iget-object v0, v0, Lcom/android/volley/toolbox/q;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/android/volley/toolbox/s;->d:Lcom/android/volley/toolbox/m;

    iget-object v0, v0, Lcom/android/volley/toolbox/m;->c:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/volley/toolbox/s;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
