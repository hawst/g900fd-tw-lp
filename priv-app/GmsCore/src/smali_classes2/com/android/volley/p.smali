.class public abstract Lcom/android/volley/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public c:Z

.field public d:Lcom/android/volley/z;

.field public e:Lcom/android/volley/c;

.field private final f:Lcom/android/volley/ae;

.field private final g:I

.field private final h:Lcom/android/volley/w;

.field private i:Ljava/lang/Integer;

.field private j:Lcom/android/volley/s;

.field private k:Z

.field private l:Z

.field private m:J

.field private n:Ljava/lang/Object;


# direct methods
.method public constructor <init>(ILjava/lang/String;Lcom/android/volley/w;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    sget-boolean v0, Lcom/android/volley/ae;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/volley/ae;

    invoke-direct {v0}, Lcom/android/volley/ae;-><init>()V

    :goto_0
    iput-object v0, p0, Lcom/android/volley/p;->f:Lcom/android/volley/ae;

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/volley/p;->c:Z

    .line 88
    iput-boolean v2, p0, Lcom/android/volley/p;->k:Z

    .line 91
    iput-boolean v2, p0, Lcom/android/volley/p;->l:Z

    .line 94
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/android/volley/p;->m:J

    .line 107
    iput-object v1, p0, Lcom/android/volley/p;->e:Lcom/android/volley/c;

    .line 132
    iput p1, p0, Lcom/android/volley/p;->a:I

    .line 133
    iput-object p2, p0, Lcom/android/volley/p;->b:Ljava/lang/String;

    .line 134
    iput-object p3, p0, Lcom/android/volley/p;->h:Lcom/android/volley/w;

    .line 135
    new-instance v0, Lcom/android/volley/f;

    invoke-direct {v0}, Lcom/android/volley/f;-><init>()V

    iput-object v0, p0, Lcom/android/volley/p;->d:Lcom/android/volley/z;

    .line 137
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    iput v0, p0, Lcom/android/volley/p;->g:I

    .line 138
    return-void

    :cond_0
    move-object v0, v1

    .line 61
    goto :goto_0

    :cond_1
    move v0, v2

    .line 137
    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/android/volley/w;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 122
    const/4 v0, -0x1

    invoke-direct {p0, v0, p1, p2}, Lcom/android/volley/p;-><init>(ILjava/lang/String;Lcom/android/volley/w;)V

    .line 123
    return-void
.end method

.method protected static a(Lcom/android/volley/ac;)Lcom/android/volley/ac;
    .locals 0

    .prologue
    .line 543
    return-object p0
.end method

.method static synthetic a(Lcom/android/volley/p;)Lcom/android/volley/ae;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/android/volley/p;->f:Lcom/android/volley/ae;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/android/volley/p;->a:I

    return v0
.end method

.method public final a(I)Lcom/android/volley/p;
    .locals 1

    .prologue
    .line 262
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/volley/p;->i:Ljava/lang/Integer;

    .line 263
    return-object p0
.end method

.method public final a(Lcom/android/volley/c;)Lcom/android/volley/p;
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, Lcom/android/volley/p;->e:Lcom/android/volley/c;

    .line 298
    return-object p0
.end method

.method public final a(Lcom/android/volley/s;)Lcom/android/volley/p;
    .locals 0

    .prologue
    .line 252
    iput-object p1, p0, Lcom/android/volley/p;->j:Lcom/android/volley/s;

    .line 253
    return-object p0
.end method

.method public final a(Lcom/android/volley/z;)Lcom/android/volley/p;
    .locals 0

    .prologue
    .line 195
    iput-object p1, p0, Lcom/android/volley/p;->d:Lcom/android/volley/z;

    .line 196
    return-object p0
.end method

.method public final a(Ljava/lang/Object;)Lcom/android/volley/p;
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/android/volley/p;->n:Ljava/lang/Object;

    .line 155
    return-object p0
.end method

.method public final a(Z)Lcom/android/volley/p;
    .locals 1

    .prologue
    .line 464
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/volley/p;->c:Z

    .line 465
    return-object p0
.end method

.method protected abstract a(Lcom/android/volley/m;)Lcom/android/volley/v;
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 203
    sget-boolean v0, Lcom/android/volley/ae;->a:Z

    if-eqz v0, :cond_1

    .line 204
    iget-object v0, p0, Lcom/android/volley/p;->f:Lcom/android/volley/ae;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v0, p1, v2, v3}, Lcom/android/volley/ae;->a(Ljava/lang/String;J)V

    .line 208
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    iget-wide v0, p0, Lcom/android/volley/p;->m:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 206
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/volley/p;->m:J

    goto :goto_0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/android/volley/p;->n:Ljava/lang/Object;

    return-object v0
.end method

.method public b(Lcom/android/volley/ac;)V
    .locals 1

    .prologue
    .line 562
    iget-object v0, p0, Lcom/android/volley/p;->h:Lcom/android/volley/w;

    if-eqz v0, :cond_0

    .line 563
    iget-object v0, p0, Lcom/android/volley/p;->h:Lcom/android/volley/w;

    invoke-interface {v0, p1}, Lcom/android/volley/w;->a(Lcom/android/volley/ac;)V

    .line 565
    :cond_0
    return-void
.end method

.method protected abstract b(Ljava/lang/Object;)V
.end method

.method final b(Ljava/lang/String;)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 216
    iget-object v0, p0, Lcom/android/volley/p;->j:Lcom/android/volley/s;

    if-eqz v0, :cond_2

    .line 217
    iget-object v1, p0, Lcom/android/volley/p;->j:Lcom/android/volley/s;

    iget-object v2, v1, Lcom/android/volley/s;->b:Ljava/util/Set;

    monitor-enter v2

    :try_start_0
    iget-object v0, v1, Lcom/android/volley/s;->b:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-boolean v0, p0, Lcom/android/volley/p;->c:Z

    if-eqz v0, :cond_2

    iget-object v2, v1, Lcom/android/volley/s;->a:Ljava/util/Map;

    monitor-enter v2

    :try_start_1
    invoke-virtual {p0}, Lcom/android/volley/p;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v0, v1, Lcom/android/volley/s;->a:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    if-eqz v0, :cond_1

    sget-boolean v4, Lcom/android/volley/ad;->b:Z

    if-eqz v4, :cond_0

    const-string v4, "Releasing %d waiting requests for cacheKey=%s."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Lcom/android/volley/ad;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v1, v1, Lcom/android/volley/s;->c:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/PriorityBlockingQueue;->addAll(Ljava/util/Collection;)Z

    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 219
    :cond_2
    sget-boolean v0, Lcom/android/volley/ae;->a:Z

    if-eqz v0, :cond_5

    .line 220
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    .line 221
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v2, v3, :cond_4

    .line 224
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 225
    new-instance v3, Lcom/android/volley/q;

    invoke-direct {v3, p0, p1, v0, v1}, Lcom/android/volley/q;-><init>(Lcom/android/volley/p;Ljava/lang/String;J)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 243
    :cond_3
    :goto_0
    return-void

    .line 217
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    .line 235
    :cond_4
    iget-object v2, p0, Lcom/android/volley/p;->f:Lcom/android/volley/ae;

    invoke-virtual {v2, p1, v0, v1}, Lcom/android/volley/ae;->a(Ljava/lang/String;J)V

    .line 236
    iget-object v0, p0, Lcom/android/volley/p;->f:Lcom/android/volley/ae;

    invoke-virtual {p0}, Lcom/android/volley/p;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/volley/ae;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 238
    :cond_5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/volley/p;->m:J

    sub-long/2addr v0, v2

    .line 239
    const-wide/16 v2, 0xbb8

    cmp-long v2, v0, v2

    if-ltz v2, :cond_3

    .line 240
    const-string v2, "%d ms: %s"

    new-array v3, v10, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v8

    invoke-virtual {p0}, Lcom/android/volley/p;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v9

    invoke-static {v2, v3}, Lcom/android/volley/ad;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 170
    iget v0, p0, Lcom/android/volley/p;->g:I

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 38
    check-cast p1, Lcom/android/volley/p;

    invoke-virtual {p0}, Lcom/android/volley/p;->o()Lcom/android/volley/r;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/volley/p;->o()Lcom/android/volley/r;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/volley/p;->i:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p1, Lcom/android/volley/p;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v1}, Lcom/android/volley/r;->ordinal()I

    move-result v1

    invoke-virtual {v0}, Lcom/android/volley/r;->ordinal()I

    move-result v0

    sub-int v0, v1, v0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/android/volley/p;->b:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/android/volley/p;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Lcom/android/volley/c;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/android/volley/p;->e:Lcom/android/volley/c;

    return-object v0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 312
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/volley/p;->k:Z

    .line 313
    return-void
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/android/volley/p;->k:Z

    return v0
.end method

.method public i()Ljava/util/Map;
    .locals 1

    .prologue
    .line 329
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 372
    invoke-virtual {p0}, Lcom/android/volley/p;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k()[B
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 388
    const/4 v0, 0x0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 424
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "application/x-www-form-urlencoded; charset=UTF-8"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()[B
    .locals 1

    .prologue
    .line 433
    const/4 v0, 0x0

    return-object v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 472
    iget-boolean v0, p0, Lcom/android/volley/p;->c:Z

    return v0
.end method

.method public o()Lcom/android/volley/r;
    .locals 1

    .prologue
    .line 490
    sget-object v0, Lcom/android/volley/r;->b:Lcom/android/volley/r;

    return-object v0
.end method

.method public final p()I
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lcom/android/volley/p;->d:Lcom/android/volley/z;

    invoke-interface {v0}, Lcom/android/volley/z;->a()I

    move-result v0

    return v0
.end method

.method public q()Lcom/android/volley/z;
    .locals 1

    .prologue
    .line 506
    iget-object v0, p0, Lcom/android/volley/p;->d:Lcom/android/volley/z;

    return-object v0
.end method

.method public final r()V
    .locals 1

    .prologue
    .line 514
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/volley/p;->l:Z

    .line 515
    return-void
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 521
    iget-boolean v0, p0, Lcom/android/volley/p;->l:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 585
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/android/volley/p;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 586
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v0, p0, Lcom/android/volley/p;->k:Z

    if-eqz v0, :cond_0

    const-string v0, "[X] "

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/volley/p;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/volley/p;->o()Lcom/android/volley/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/volley/p;->i:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "[ ] "

    goto :goto_0
.end method
