.class public abstract Lcom/android/a/a/c;
.super Landroid/content/ContentProvider;
.source "SourceFile"

# interfaces
.implements Landroid/database/sqlite/SQLiteTransactionListener;


# instance fields
.field protected a:Landroid/database/sqlite/SQLiteDatabase;

.field private b:Landroid/database/sqlite/SQLiteOpenHelper;

.field private volatile c:Z

.field private final d:Ljava/lang/ThreadLocal;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 44
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/android/a/a/c;->d:Ljava/lang/ThreadLocal;

    return-void
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/android/a/a/c;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/a/a/c;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 264
    iget-boolean v0, p0, Lcom/android/a/a/c;->c:Z

    if-eqz v0, :cond_0

    .line 265
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/a/a/c;->c:Z

    .line 266
    invoke-virtual {p0}, Lcom/android/a/a/c;->a()V

    .line 268
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
.end method

.method protected abstract a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
.end method

.method protected abstract a(Landroid/content/Context;)Landroid/database/sqlite/SQLiteOpenHelper;
.end method

.method protected abstract a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
.end method

.method protected abstract a()V
.end method

.method public applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 205
    .line 207
    iget-object v0, p0, Lcom/android/a/a/c;->b:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 208
    iget-object v0, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionWithListener(Landroid/database/sqlite/SQLiteTransactionListener;)V

    .line 210
    :try_start_0
    iget-object v0, p0, Lcom/android/a/a/c;->d:Ljava/lang/ThreadLocal;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 211
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 212
    new-array v6, v5, [Landroid/content/ContentProviderResult;

    move v4, v2

    move v1, v2

    move v3, v2

    .line 213
    :goto_0
    if-ge v4, v5, :cond_2

    .line 214
    add-int/lit8 v1, v1, 0x1

    const/16 v0, 0x1f4

    if-le v1, v0, :cond_0

    .line 215
    new-instance v0, Landroid/content/OperationApplicationException;

    const-string v1, "Too many content provider operations between yield points. The maximum number of operations per yield point is 500"

    invoke-direct {v0, v1, v3}, Landroid/content/OperationApplicationException;-><init>(Ljava/lang/String;I)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/android/a/a/c;->d:Ljava/lang/ThreadLocal;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 237
    iget-object v1, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 238
    invoke-direct {p0}, Lcom/android/a/a/c;->c()V

    throw v0

    .line 220
    :cond_0
    :try_start_1
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentProviderOperation;

    .line 221
    if-lez v4, :cond_1

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation;->isYieldAllowed()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 223
    iget-boolean v1, p0, Lcom/android/a/a/c;->c:Z

    .line 224
    iget-object v7, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-wide/16 v8, 0xfa0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->yieldIfContendedSafely(J)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 225
    iget-object v7, p0, Lcom/android/a/a/c;->b:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    iput-object v7, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 226
    iput-boolean v1, p0, Lcom/android/a/a/c;->c:Z

    .line 227
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v2

    .line 231
    :cond_1
    :goto_1
    invoke-virtual {v0, p0, v6, v4}, Landroid/content/ContentProviderOperation;->apply(Landroid/content/ContentProvider;[Landroid/content/ContentProviderResult;I)Landroid/content/ContentProviderResult;

    move-result-object v0

    aput-object v0, v6, v4

    .line 213
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 233
    :cond_2
    iget-object v0, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236
    iget-object v0, p0, Lcom/android/a/a/c;->d:Ljava/lang/ThreadLocal;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 237
    iget-object v0, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 238
    invoke-direct {p0}, Lcom/android/a/a/c;->c()V

    return-object v6

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 5

    .prologue
    .line 123
    array-length v1, p2

    .line 124
    iget-object v0, p0, Lcom/android/a/a/c;->b:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 125
    iget-object v0, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionWithListener(Landroid/database/sqlite/SQLiteTransactionListener;)V

    .line 127
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 128
    :try_start_0
    aget-object v2, p2, v0

    invoke-virtual {p0, p1, v2}, Lcom/android/a/a/c;->a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    .line 129
    if-eqz v2, :cond_0

    .line 130
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/a/a/c;->c:Z

    .line 132
    :cond_0
    iget-boolean v2, p0, Lcom/android/a/a/c;->c:Z

    .line 133
    iget-object v3, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 134
    iget-object v4, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->yieldIfContendedSafely()Z

    .line 135
    iput-object v3, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 136
    iput-boolean v2, p0, Lcom/android/a/a/c;->c:Z

    .line 127
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    iget-object v0, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 143
    invoke-direct {p0}, Lcom/android/a/a/c;->c()V

    .line 144
    return v1

    .line 140
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 177
    invoke-direct {p0}, Lcom/android/a/a/c;->b()Z

    move-result v0

    .line 179
    if-nez v0, :cond_2

    .line 180
    iget-object v0, p0, Lcom/android/a/a/c;->b:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 181
    iget-object v0, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionWithListener(Landroid/database/sqlite/SQLiteTransactionListener;)V

    .line 183
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/a/a/c;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 184
    if-lez v0, :cond_0

    .line 185
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/a/a/c;->c:Z

    .line 187
    :cond_0
    iget-object v1, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    iget-object v1, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 192
    invoke-direct {p0}, Lcom/android/a/a/c;->c()V

    .line 199
    :cond_1
    :goto_0
    return v0

    .line 189
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 194
    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/a/a/c;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 195
    if-lez v0, :cond_1

    .line 196
    iput-boolean v1, p0, Lcom/android/a/a/c;->c:Z

    goto :goto_0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 96
    invoke-direct {p0}, Lcom/android/a/a/c;->b()Z

    move-result v0

    .line 98
    if-nez v0, :cond_2

    .line 99
    iget-object v0, p0, Lcom/android/a/a/c;->b:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 100
    iget-object v0, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionWithListener(Landroid/database/sqlite/SQLiteTransactionListener;)V

    .line 102
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/android/a/a/c;->a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 103
    if-eqz v0, :cond_0

    .line 104
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/a/a/c;->c:Z

    .line 106
    :cond_0
    iget-object v1, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    iget-object v1, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 111
    invoke-direct {p0}, Lcom/android/a/a/c;->c()V

    .line 118
    :cond_1
    :goto_0
    return-object v0

    .line 108
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 113
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/android/a/a/c;->a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 114
    if-eqz v0, :cond_1

    .line 115
    iput-boolean v1, p0, Lcom/android/a/a/c;->c:Z

    goto :goto_0
.end method

.method public onBegin()V
    .locals 0

    .prologue
    .line 244
    return-void
.end method

.method public onCommit()V
    .locals 0

    .prologue
    .line 249
    return-void
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/android/a/a/c;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 62
    invoke-virtual {p0, v0}, Lcom/android/a/a/c;->a(Landroid/content/Context;)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/a/a/c;->b:Landroid/database/sqlite/SQLiteOpenHelper;

    .line 63
    const/4 v0, 0x1

    return v0
.end method

.method public onRollback()V
    .locals 0

    .prologue
    .line 255
    return-void
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 149
    invoke-direct {p0}, Lcom/android/a/a/c;->b()Z

    move-result v0

    .line 151
    if-nez v0, :cond_2

    .line 152
    iget-object v0, p0, Lcom/android/a/a/c;->b:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 153
    iget-object v0, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionWithListener(Landroid/database/sqlite/SQLiteTransactionListener;)V

    .line 155
    :try_start_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/a/a/c;->a(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 156
    if-lez v0, :cond_0

    .line 157
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/a/a/c;->c:Z

    .line 159
    :cond_0
    iget-object v1, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    iget-object v1, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 164
    invoke-direct {p0}, Lcom/android/a/a/c;->c()V

    .line 172
    :cond_1
    :goto_0
    return v0

    .line 161
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/android/a/a/c;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 166
    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/a/a/c;->a(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 167
    if-lez v0, :cond_1

    .line 168
    iput-boolean v1, p0, Lcom/android/a/a/c;->c:Z

    goto :goto_0
.end method
