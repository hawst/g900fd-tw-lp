.class public final Lcom/android/a/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/android/a/a/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Lcom/android/a/a/a;

    invoke-direct {v0}, Lcom/android/a/a/a;-><init>()V

    iput-object v0, p0, Lcom/android/a/a/b;->a:Lcom/android/a/a/a;

    return-void
.end method


# virtual methods
.method public final a()Lcom/android/a/a/a;
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/android/a/a/b;->a:Lcom/android/a/a/a;

    invoke-virtual {v0}, Lcom/android/a/a/a;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    .line 58
    iget-object v1, p0, Lcom/android/a/a/b;->a:Lcom/android/a/a/a;

    invoke-virtual {v1}, Lcom/android/a/a/a;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 59
    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 60
    iget-object v1, p0, Lcom/android/a/a/b;->a:Lcom/android/a/a/a;

    invoke-static {v1, v0}, Lcom/android/a/a/a;->a(Lcom/android/a/a/a;[Ljava/lang/String;)[Ljava/lang/String;

    .line 61
    iget-object v0, p0, Lcom/android/a/a/b;->a:Lcom/android/a/a/a;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/android/a/a/b;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/a/a/b;->a:Lcom/android/a/a/a;

    invoke-static {v0, p1, p1}, Lcom/android/a/a/a;->a(Lcom/android/a/a/a;Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;
    .locals 3

    .prologue
    .line 38
    iget-object v0, p0, Lcom/android/a/a/b;->a:Lcom/android/a/a/a;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AS "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/a/a/a;->a(Lcom/android/a/a/a;Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-object p0
.end method

.method public final a([Ljava/lang/String;)Lcom/android/a/a/b;
    .locals 3

    .prologue
    .line 43
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 44
    invoke-virtual {p0, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    .line 43
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 46
    :cond_0
    return-object p0
.end method
