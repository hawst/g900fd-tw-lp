.class public Lcom/android/proxyhandler/SocketConnect;
.super Ljava/lang/Thread;
.source "SocketConnect.java"


# instance fields
.field private from:Ljava/io/InputStream;

.field private to:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Ljava/net/Socket;Ljava/net/Socket;)V
    .locals 1
    .param p1, "from"    # Ljava/net/Socket;
    .param p2, "to"    # Ljava/net/Socket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 17
    invoke-virtual {p1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/android/proxyhandler/SocketConnect;->from:Ljava/io/InputStream;

    .line 18
    invoke-virtual {p2}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/android/proxyhandler/SocketConnect;->to:Ljava/io/OutputStream;

    .line 19
    invoke-virtual {p0}, Lcom/android/proxyhandler/SocketConnect;->start()V

    .line 20
    return-void
.end method

.method public static connect(Ljava/net/Socket;Ljava/net/Socket;)V
    .locals 4
    .param p0, "first"    # Ljava/net/Socket;
    .param p1, "second"    # Ljava/net/Socket;

    .prologue
    .line 43
    :try_start_0
    new-instance v1, Lcom/android/proxyhandler/SocketConnect;

    invoke-direct {v1, p0, p1}, Lcom/android/proxyhandler/SocketConnect;-><init>(Ljava/net/Socket;Ljava/net/Socket;)V

    .line 44
    .local v1, "sc1":Lcom/android/proxyhandler/SocketConnect;
    new-instance v2, Lcom/android/proxyhandler/SocketConnect;

    invoke-direct {v2, p1, p0}, Lcom/android/proxyhandler/SocketConnect;-><init>(Ljava/net/Socket;Ljava/net/Socket;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    .local v2, "sc2":Lcom/android/proxyhandler/SocketConnect;
    :try_start_1
    invoke-virtual {v1}, Lcom/android/proxyhandler/SocketConnect;->join()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 50
    :goto_0
    :try_start_2
    invoke-virtual {v2}, Lcom/android/proxyhandler/SocketConnect;->join()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 56
    .end local v1    # "sc1":Lcom/android/proxyhandler/SocketConnect;
    .end local v2    # "sc2":Lcom/android/proxyhandler/SocketConnect;
    :goto_1
    return-void

    .line 53
    :catch_0
    move-exception v0

    .line 54
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 47
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v1    # "sc1":Lcom/android/proxyhandler/SocketConnect;
    .restart local v2    # "sc2":Lcom/android/proxyhandler/SocketConnect;
    :catch_1
    move-exception v3

    goto :goto_0

    .line 51
    :catch_2
    move-exception v3

    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 24
    const/16 v2, 0x200

    new-array v0, v2, [B

    .line 28
    .local v0, "buffer":[B
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/android/proxyhandler/SocketConnect;->from:Ljava/io/InputStream;

    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .line 29
    .local v1, "r":I
    if-gez v1, :cond_0

    .line 34
    iget-object v2, p0, Lcom/android/proxyhandler/SocketConnect;->from:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 35
    iget-object v2, p0, Lcom/android/proxyhandler/SocketConnect;->to:Ljava/io/OutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    .line 39
    .end local v1    # "r":I
    :goto_1
    return-void

    .line 32
    .restart local v1    # "r":I
    :cond_0
    iget-object v2, p0, Lcom/android/proxyhandler/SocketConnect;->to:Ljava/io/OutputStream;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v1}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 36
    .end local v1    # "r":I
    :catch_0
    move-exception v2

    goto :goto_1
.end method
