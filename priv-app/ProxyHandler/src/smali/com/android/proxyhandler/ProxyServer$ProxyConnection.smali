.class Lcom/android/proxyhandler/ProxyServer$ProxyConnection;
.super Ljava/lang/Object;
.source "ProxyServer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/proxyhandler/ProxyServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProxyConnection"
.end annotation


# instance fields
.field private connection:Ljava/net/Socket;

.field final synthetic this$0:Lcom/android/proxyhandler/ProxyServer;


# direct methods
.method private constructor <init>(Lcom/android/proxyhandler/ProxyServer;Ljava/net/Socket;)V
    .locals 0
    .param p2, "connection"    # Ljava/net/Socket;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;->this$0:Lcom/android/proxyhandler/ProxyServer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p2, p0, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;->connection:Ljava/net/Socket;

    .line 62
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/proxyhandler/ProxyServer;Ljava/net/Socket;Lcom/android/proxyhandler/ProxyServer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/proxyhandler/ProxyServer;
    .param p2, "x1"    # Ljava/net/Socket;
    .param p3, "x2"    # Lcom/android/proxyhandler/ProxyServer$1;

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;-><init>(Lcom/android/proxyhandler/ProxyServer;Ljava/net/Socket;)V

    return-void
.end method

.method private getLine(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 3
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 162
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 163
    .local v0, "buffer":Ljava/lang/StringBuffer;
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 164
    .local v1, "byteBuffer":I
    if-gez v1, :cond_0

    const-string v2, ""

    .line 172
    :goto_0
    return-object v2

    .line 166
    :cond_0
    const/16 v2, 0xd

    if-eq v1, v2, :cond_1

    .line 167
    int-to-char v2, v1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 169
    :cond_1
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 170
    const/16 v2, 0xa

    if-eq v1, v2, :cond_2

    if-gez v1, :cond_0

    .line 172
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private sendLine(Ljava/net/Socket;Ljava/lang/String;)V
    .locals 2
    .param p1, "socket"    # Ljava/net/Socket;
    .param p2, "line"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    invoke-virtual {p1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    .line 177
    .local v0, "os":Ljava/io/OutputStream;
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 178
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 179
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 180
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 181
    return-void
.end method


# virtual methods
.method public run()V
    .locals 21

    .prologue
    .line 67
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;->connection:Ljava/net/Socket;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;->getLine(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v12

    .line 68
    .local v12, "requestLine":Ljava/lang/String;
    if-nez v12, :cond_0

    .line 69
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;->connection:Ljava/net/Socket;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/net/Socket;->close()V

    .line 159
    .end local v12    # "requestLine":Ljava/lang/String;
    :goto_0
    return-void

    .line 72
    .restart local v12    # "requestLine":Ljava/lang/String;
    :cond_0
    const-string v19, " "

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    .line 73
    .local v16, "splitLine":[Ljava/lang/String;
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v19, v0

    const/16 v20, 0x3

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_1

    .line 74
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;->connection:Ljava/net/Socket;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 151
    .end local v12    # "requestLine":Ljava/lang/String;
    .end local v16    # "splitLine":[Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 152
    .local v3, "e":Ljava/io/IOException;
    const-string v19, "ProxyServer"

    const-string v20, "Problem Proxying"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v0, v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 155
    .end local v3    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;->connection:Ljava/net/Socket;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/net/Socket;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 156
    :catch_1
    move-exception v19

    goto :goto_0

    .line 77
    .restart local v12    # "requestLine":Ljava/lang/String;
    .restart local v16    # "splitLine":[Ljava/lang/String;
    :cond_1
    const/16 v19, 0x0

    :try_start_2
    aget-object v13, v16, v19

    .line 78
    .local v13, "requestType":Ljava/lang/String;
    const/16 v19, 0x1

    aget-object v18, v16, v19

    .line 80
    .local v18, "urlString":Ljava/lang/String;
    const-string v4, ""

    .line 81
    .local v4, "host":Ljava/lang/String;
    const/16 v10, 0x50

    .line 83
    .local v10, "port":I
    const-string v19, "CONNECT"

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 84
    const-string v19, ":"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 85
    .local v5, "hostPortSplit":[Ljava/lang/String;
    const/16 v19, 0x0

    aget-object v4, v5, v19
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 87
    const/16 v19, 0x1

    :try_start_3
    aget-object v19, v5, v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    move-result v10

    .line 91
    :goto_2
    :try_start_4
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Https://"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ":"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 106
    .end local v5    # "hostPortSplit":[Ljava/lang/String;
    :cond_2
    :goto_3
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    move-result-object v8

    .line 108
    .local v8, "list":Ljava/util/List;, "Ljava/util/List<Ljava/net/Proxy;>;"
    :try_start_5
    invoke-static {}, Ljava/net/ProxySelector;->getDefault()Ljava/net/ProxySelector;

    move-result-object v19

    new-instance v20, Ljava/net/URI;

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v20}, Ljava/net/ProxySelector;->select(Ljava/net/URI;)Ljava/util/List;
    :try_end_5
    .catch Ljava/net/URISyntaxException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    move-result-object v8

    .line 112
    :goto_4
    const/4 v14, 0x0

    .line 113
    .local v14, "server":Ljava/net/Socket;
    :try_start_6
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    move-object v15, v14

    .end local v14    # "server":Ljava/net/Socket;
    .local v15, "server":Ljava/net/Socket;
    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/net/Proxy;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0

    .line 115
    .local v11, "proxy":Ljava/net/Proxy;
    :try_start_7
    sget-object v19, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/net/Proxy;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_6

    .line 117
    invoke-virtual {v11}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v7

    check-cast v7, Ljava/net/InetSocketAddress;

    .line 119
    .local v7, "inetSocketAddress":Ljava/net/InetSocketAddress;
    new-instance v14, Ljava/net/Socket;

    invoke-virtual {v7}, Ljava/net/InetSocketAddress;->getHostName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual {v7}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v20

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v14, v0, v1}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    .line 121
    .end local v15    # "server":Ljava/net/Socket;
    .restart local v14    # "server":Ljava/net/Socket;
    :try_start_8
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v12}, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;->sendLine(Ljava/net/Socket;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 135
    .end local v7    # "inetSocketAddress":Ljava/net/InetSocketAddress;
    :goto_6
    if-eqz v14, :cond_9

    .line 139
    .end local v11    # "proxy":Ljava/net/Proxy;
    :goto_7
    if-nez v14, :cond_4

    .line 140
    :try_start_9
    new-instance v14, Ljava/net/Socket;

    .end local v14    # "server":Ljava/net/Socket;
    invoke-direct {v14, v4, v10}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V

    .line 141
    .restart local v14    # "server":Ljava/net/Socket;
    const-string v19, "CONNECT"

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 142
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;->connection:Ljava/net/Socket;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;->getLine(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    if-nez v19, :cond_3

    .line 144
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;->connection:Ljava/net/Socket;

    move-object/from16 v19, v0

    const-string v20, "HTTP/1.1 200 OK\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;->sendLine(Ljava/net/Socket;Ljava/lang/String;)V

    .line 150
    :cond_4
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;->connection:Ljava/net/Socket;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v0, v14}, Lcom/android/proxyhandler/SocketConnect;->connect(Ljava/net/Socket;Ljava/net/Socket;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0

    goto/16 :goto_1

    .line 88
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v8    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/net/Proxy;>;"
    .end local v14    # "server":Ljava/net/Socket;
    .restart local v5    # "hostPortSplit":[Ljava/lang/String;
    :catch_2
    move-exception v9

    .line 89
    .local v9, "nfe":Ljava/lang/NumberFormatException;
    const/16 v10, 0x1bb

    goto/16 :goto_2

    .line 94
    .end local v5    # "hostPortSplit":[Ljava/lang/String;
    .end local v9    # "nfe":Ljava/lang/NumberFormatException;
    :cond_5
    :try_start_a
    new-instance v17, Ljava/net/URI;

    invoke-direct/range {v17 .. v18}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 95
    .local v17, "url":Ljava/net/URI;
    invoke-virtual/range {v17 .. v17}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v4

    .line 96
    invoke-virtual/range {v17 .. v17}, Ljava/net/URI;->getPort()I
    :try_end_a
    .catch Ljava/net/URISyntaxException; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0

    move-result v10

    .line 97
    if-gez v10, :cond_2

    .line 98
    const/16 v10, 0x50

    goto/16 :goto_3

    .line 100
    .end local v17    # "url":Ljava/net/URI;
    :catch_3
    move-exception v3

    .line 101
    .local v3, "e":Ljava/net/URISyntaxException;
    :try_start_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;->connection:Ljava/net/Socket;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/net/Socket;->close()V

    goto/16 :goto_0

    .line 109
    .end local v3    # "e":Ljava/net/URISyntaxException;
    .restart local v8    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/net/Proxy;>;"
    :catch_4
    move-exception v3

    .line 110
    .restart local v3    # "e":Ljava/net/URISyntaxException;
    invoke-virtual {v3}, Ljava/net/URISyntaxException;->printStackTrace()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0

    goto/16 :goto_4

    .line 123
    .end local v3    # "e":Ljava/net/URISyntaxException;
    .restart local v6    # "i$":Ljava/util/Iterator;
    .restart local v11    # "proxy":Ljava/net/Proxy;
    .restart local v15    # "server":Ljava/net/Socket;
    :cond_6
    :try_start_c
    new-instance v14, Ljava/net/Socket;

    invoke-direct {v14, v4, v10}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_6

    .line 124
    .end local v15    # "server":Ljava/net/Socket;
    .restart local v14    # "server":Ljava/net/Socket;
    :try_start_d
    const-string v19, "CONNECT"

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 125
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;->connection:Ljava/net/Socket;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;->getLine(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    if-nez v19, :cond_7

    .line 127
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;->connection:Ljava/net/Socket;

    move-object/from16 v19, v0

    const-string v20, "HTTP/1.1 200 OK\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;->sendLine(Ljava/net/Socket;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 132
    :catch_5
    move-exception v19

    goto/16 :goto_6

    .line 129
    :cond_8
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v12}, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;->sendLine(Ljava/net/Socket;Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_5

    goto/16 :goto_6

    :cond_9
    move-object v15, v14

    .line 138
    .end local v14    # "server":Ljava/net/Socket;
    .restart local v15    # "server":Ljava/net/Socket;
    goto/16 :goto_5

    .line 146
    .end local v11    # "proxy":Ljava/net/Proxy;
    .end local v15    # "server":Ljava/net/Socket;
    .restart local v14    # "server":Ljava/net/Socket;
    :cond_a
    :try_start_e
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v12}, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;->sendLine(Ljava/net/Socket;Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_0

    goto/16 :goto_8

    .line 132
    .end local v14    # "server":Ljava/net/Socket;
    .restart local v11    # "proxy":Ljava/net/Proxy;
    .restart local v15    # "server":Ljava/net/Socket;
    :catch_6
    move-exception v19

    move-object v14, v15

    .end local v15    # "server":Ljava/net/Socket;
    .restart local v14    # "server":Ljava/net/Socket;
    goto/16 :goto_6

    .end local v11    # "proxy":Ljava/net/Proxy;
    .end local v14    # "server":Ljava/net/Socket;
    .restart local v15    # "server":Ljava/net/Socket;
    :cond_b
    move-object v14, v15

    .end local v15    # "server":Ljava/net/Socket;
    .restart local v14    # "server":Ljava/net/Socket;
    goto/16 :goto_7
.end method
