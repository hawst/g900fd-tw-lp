.class Lcom/android/proxyhandler/ProxyService$1;
.super Lcom/android/net/IProxyCallback$Stub;
.source "ProxyService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/proxyhandler/ProxyService;->onBind(Landroid/content/Intent;)Landroid/os/IBinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/proxyhandler/ProxyService;


# direct methods
.method constructor <init>(Lcom/android/proxyhandler/ProxyService;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/android/proxyhandler/ProxyService$1;->this$0:Lcom/android/proxyhandler/ProxyService;

    invoke-direct {p0}, Lcom/android/net/IProxyCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public getProxyPort(Landroid/os/IBinder;)V
    .locals 2
    .param p1, "callback"    # Landroid/os/IBinder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 62
    # getter for: Lcom/android/proxyhandler/ProxyService;->server:Lcom/android/proxyhandler/ProxyServer;
    invoke-static {}, Lcom/android/proxyhandler/ProxyService;->access$000()Lcom/android/proxyhandler/ProxyServer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 63
    invoke-static {p1}, Lcom/android/net/IProxyPortListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/net/IProxyPortListener;

    move-result-object v0

    .line 64
    .local v0, "portListener":Lcom/android/net/IProxyPortListener;
    if-eqz v0, :cond_0

    .line 65
    # getter for: Lcom/android/proxyhandler/ProxyService;->server:Lcom/android/proxyhandler/ProxyServer;
    invoke-static {}, Lcom/android/proxyhandler/ProxyService;->access$000()Lcom/android/proxyhandler/ProxyServer;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/proxyhandler/ProxyServer;->setCallback(Lcom/android/net/IProxyPortListener;)V

    .line 68
    .end local v0    # "portListener":Lcom/android/net/IProxyPortListener;
    :cond_0
    return-void
.end method
