.class public Lcom/android/proxyhandler/ProxyService;
.super Landroid/app/Service;
.source "ProxyService.java"


# static fields
.field private static server:Lcom/android/proxyhandler/ProxyServer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    sput-object v0, Lcom/android/proxyhandler/ProxyService;->server:Lcom/android/proxyhandler/ProxyServer;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/android/proxyhandler/ProxyServer;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/android/proxyhandler/ProxyService;->server:Lcom/android/proxyhandler/ProxyServer;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 59
    new-instance v0, Lcom/android/proxyhandler/ProxyService$1;

    invoke-direct {v0, p0}, Lcom/android/proxyhandler/ProxyService$1;-><init>(Lcom/android/proxyhandler/ProxyService;)V

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 42
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 43
    sget-object v0, Lcom/android/proxyhandler/ProxyService;->server:Lcom/android/proxyhandler/ProxyServer;

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Lcom/android/proxyhandler/ProxyServer;

    invoke-direct {v0}, Lcom/android/proxyhandler/ProxyServer;-><init>()V

    sput-object v0, Lcom/android/proxyhandler/ProxyService;->server:Lcom/android/proxyhandler/ProxyServer;

    .line 45
    sget-object v0, Lcom/android/proxyhandler/ProxyService;->server:Lcom/android/proxyhandler/ProxyServer;

    invoke-virtual {v0}, Lcom/android/proxyhandler/ProxyServer;->startServer()V

    .line 47
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/android/proxyhandler/ProxyService;->server:Lcom/android/proxyhandler/ProxyServer;

    if-eqz v0, :cond_0

    .line 52
    sget-object v0, Lcom/android/proxyhandler/ProxyService;->server:Lcom/android/proxyhandler/ProxyServer;

    invoke-virtual {v0}, Lcom/android/proxyhandler/ProxyServer;->stopServer()V

    .line 53
    const/4 v0, 0x0

    sput-object v0, Lcom/android/proxyhandler/ProxyService;->server:Lcom/android/proxyhandler/ProxyServer;

    .line 55
    :cond_0
    return-void
.end method
