.class public Lcom/android/proxyhandler/ProxyServer;
.super Ljava/lang/Thread;
.source "ProxyServer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/proxyhandler/ProxyServer$1;,
        Lcom/android/proxyhandler/ProxyServer$ProxyConnection;
    }
.end annotation


# instance fields
.field private mCallback:Lcom/android/net/IProxyPortListener;

.field public mIsRunning:Z

.field private mPort:I

.field private serverSocket:Ljava/net/ServerSocket;

.field private threadExecutor:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 184
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/proxyhandler/ProxyServer;->mIsRunning:Z

    .line 185
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/proxyhandler/ProxyServer;->threadExecutor:Ljava/util/concurrent/ExecutorService;

    .line 186
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/proxyhandler/ProxyServer;->mPort:I

    .line 187
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/proxyhandler/ProxyServer;->mCallback:Lcom/android/net/IProxyPortListener;

    .line 188
    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 193
    :try_start_0
    new-instance v4, Ljava/net/ServerSocket;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Ljava/net/ServerSocket;-><init>(I)V

    iput-object v4, p0, Lcom/android/proxyhandler/ProxyServer;->serverSocket:Ljava/net/ServerSocket;

    .line 195
    iget-object v4, p0, Lcom/android/proxyhandler/ProxyServer;->serverSocket:Ljava/net/ServerSocket;

    if-eqz v4, :cond_0

    .line 196
    iget-object v4, p0, Lcom/android/proxyhandler/ProxyServer;->serverSocket:Ljava/net/ServerSocket;

    invoke-virtual {v4}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/android/proxyhandler/ProxyServer;->setPort(I)V

    .line 198
    :goto_0
    iget-boolean v4, p0, Lcom/android/proxyhandler/ProxyServer;->mIsRunning:Z
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v4, :cond_0

    .line 200
    :try_start_1
    iget-object v4, p0, Lcom/android/proxyhandler/ProxyServer;->serverSocket:Ljava/net/ServerSocket;

    invoke-virtual {v4}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v3

    .line 202
    .local v3, "socket":Ljava/net/Socket;
    invoke-virtual {v3}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 203
    new-instance v2, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;

    const/4 v4, 0x0

    invoke-direct {v2, p0, v3, v4}, Lcom/android/proxyhandler/ProxyServer$ProxyConnection;-><init>(Lcom/android/proxyhandler/ProxyServer;Ljava/net/Socket;Lcom/android/proxyhandler/ProxyServer$1;)V

    .line 205
    .local v2, "parser":Lcom/android/proxyhandler/ProxyServer$ProxyConnection;
    iget-object v4, p0, Lcom/android/proxyhandler/ProxyServer;->threadExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v4, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 209
    .end local v2    # "parser":Lcom/android/proxyhandler/ProxyServer$ProxyConnection;
    .end local v3    # "socket":Ljava/net/Socket;
    :catch_0
    move-exception v0

    .line 210
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 214
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 215
    .local v0, "e":Ljava/net/SocketException;
    const-string v4, "ProxyServer"

    const-string v5, "Failed to start proxy server"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 220
    .end local v0    # "e":Ljava/net/SocketException;
    :cond_0
    :goto_1
    iput-boolean v6, p0, Lcom/android/proxyhandler/ProxyServer;->mIsRunning:Z

    .line 221
    return-void

    .line 207
    .restart local v3    # "socket":Ljava/net/Socket;
    :cond_1
    :try_start_3
    invoke-virtual {v3}, Ljava/net/Socket;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 216
    .end local v3    # "socket":Ljava/net/Socket;
    :catch_2
    move-exception v1

    .line 217
    .local v1, "e1":Ljava/io/IOException;
    const-string v4, "ProxyServer"

    const-string v5, "Failed to start proxy server"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public declared-synchronized setCallback(Lcom/android/net/IProxyPortListener;)V
    .locals 3
    .param p1, "callback"    # Lcom/android/net/IProxyPortListener;

    .prologue
    .line 235
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/android/proxyhandler/ProxyServer;->mPort:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 237
    :try_start_1
    iget v1, p0, Lcom/android/proxyhandler/ProxyServer;->mPort:I

    invoke-interface {p1, v1}, Lcom/android/net/IProxyPortListener;->setProxyPort(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 242
    :cond_0
    :goto_0
    :try_start_2
    iput-object p1, p0, Lcom/android/proxyhandler/ProxyServer;->mCallback:Lcom/android/net/IProxyPortListener;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 243
    monitor-exit p0

    return-void

    .line 238
    :catch_0
    move-exception v0

    .line 239
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_3
    const-string v1, "ProxyServer"

    const-string v2, "Proxy failed to report port to PacManager"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 235
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized setPort(I)V
    .locals 3
    .param p1, "port"    # I

    .prologue
    .line 224
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/proxyhandler/ProxyServer;->mCallback:Lcom/android/net/IProxyPortListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 226
    :try_start_1
    iget-object v1, p0, Lcom/android/proxyhandler/ProxyServer;->mCallback:Lcom/android/net/IProxyPortListener;

    invoke-interface {v1, p1}, Lcom/android/net/IProxyPortListener;->setProxyPort(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 231
    :cond_0
    :goto_0
    :try_start_2
    iput p1, p0, Lcom/android/proxyhandler/ProxyServer;->mPort:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 232
    monitor-exit p0

    return-void

    .line 227
    :catch_0
    move-exception v0

    .line 228
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_3
    const-string v1, "ProxyServer"

    const-string v2, "Proxy failed to report port to PacManager"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 224
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized startServer()V
    .locals 1

    .prologue
    .line 246
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/android/proxyhandler/ProxyServer;->mIsRunning:Z

    .line 247
    invoke-virtual {p0}, Lcom/android/proxyhandler/ProxyServer;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248
    monitor-exit p0

    return-void

    .line 246
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stopServer()V
    .locals 2

    .prologue
    .line 251
    monitor-enter p0

    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/android/proxyhandler/ProxyServer;->mIsRunning:Z

    .line 252
    iget-object v1, p0, Lcom/android/proxyhandler/ProxyServer;->serverSocket:Ljava/net/ServerSocket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 254
    :try_start_1
    iget-object v1, p0, Lcom/android/proxyhandler/ProxyServer;->serverSocket:Ljava/net/ServerSocket;

    invoke-virtual {v1}, Ljava/net/ServerSocket;->close()V

    .line 255
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/proxyhandler/ProxyServer;->serverSocket:Ljava/net/ServerSocket;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 260
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 256
    :catch_0
    move-exception v0

    .line 257
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 251
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
