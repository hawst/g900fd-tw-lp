.class public abstract Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;
.super Landroid/os/Binder;
.source "IVpnService.java"

# interfaces
.implements Lcom/cisco/anyconnect/vpn/android/service/IVpnService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cisco/anyconnect/vpn/android/service/IVpnService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub$Proxy;
    }
.end annotation


# direct methods
.method public static asInterface(Landroid/os/IBinder;)Lcom/cisco/anyconnect/vpn/android/service/IVpnService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 33
    if-nez p0, :cond_0

    .line 34
    const/4 v0, 0x0

    .line 40
    :goto_0
    return-object v0

    .line 36
    :cond_0
    const-string v1, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 37
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    if-eqz v1, :cond_1

    .line 38
    check-cast v0, Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    goto :goto_0

    .line 40
    :cond_1
    new-instance v0, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 44
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 10
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 48
    sparse-switch p1, :sswitch_data_0

    .line 607
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v8

    :goto_0
    return v8

    .line 52
    :sswitch_0
    const-string v7, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 57
    :sswitch_1
    const-string v7, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 58
    invoke-virtual {p0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->GetVpnServiceVersion()I

    move-result v3

    .line 59
    .local v3, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 60
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 65
    .end local v3    # "_result":I
    :sswitch_2
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v9

    invoke-static {v9}, Lcom/cisco/anyconnect/vpn/android/service/IServiceStateListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/cisco/anyconnect/vpn/android/service/IServiceStateListener;

    move-result-object v0

    .line 68
    .local v0, "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IServiceStateListener;
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->RegisterServiceStateListener(Lcom/cisco/anyconnect/vpn/android/service/IServiceStateListener;)Z

    move-result v3

    .line 69
    .local v3, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 70
    if-eqz v3, :cond_0

    move v7, v8

    :cond_0
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 75
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IServiceStateListener;
    .end local v3    # "_result":Z
    :sswitch_3
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 77
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v9

    invoke-static {v9}, Lcom/cisco/anyconnect/vpn/android/service/IServiceStateListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/cisco/anyconnect/vpn/android/service/IServiceStateListener;

    move-result-object v0

    .line 78
    .restart local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IServiceStateListener;
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->UnregisterServiceStateListener(Lcom/cisco/anyconnect/vpn/android/service/IServiceStateListener;)Z

    move-result v3

    .line 79
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 80
    if-eqz v3, :cond_1

    move v7, v8

    :cond_1
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 85
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IServiceStateListener;
    .end local v3    # "_result":Z
    :sswitch_4
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 87
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v9

    invoke-static {v9}, Lcom/cisco/anyconnect/vpn/android/service/IStatsListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/cisco/anyconnect/vpn/android/service/IStatsListener;

    move-result-object v0

    .line 88
    .local v0, "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IStatsListener;
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->RegisterStatsListener(Lcom/cisco/anyconnect/vpn/android/service/IStatsListener;)Z

    move-result v3

    .line 89
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 90
    if-eqz v3, :cond_2

    move v7, v8

    :cond_2
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 95
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IStatsListener;
    .end local v3    # "_result":Z
    :sswitch_5
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 97
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v9

    invoke-static {v9}, Lcom/cisco/anyconnect/vpn/android/service/IStatsListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/cisco/anyconnect/vpn/android/service/IStatsListener;

    move-result-object v0

    .line 98
    .restart local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IStatsListener;
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->UnregisterStatsListener(Lcom/cisco/anyconnect/vpn/android/service/IStatsListener;)Z

    move-result v3

    .line 99
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 100
    if-eqz v3, :cond_3

    move v7, v8

    :cond_3
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 105
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IStatsListener;
    .end local v3    # "_result":Z
    :sswitch_6
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 107
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v9

    invoke-static {v9}, Lcom/cisco/anyconnect/vpn/android/service/ILogUpdateListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/cisco/anyconnect/vpn/android/service/ILogUpdateListener;

    move-result-object v0

    .line 108
    .local v0, "_arg0":Lcom/cisco/anyconnect/vpn/android/service/ILogUpdateListener;
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->RegisterLogListener(Lcom/cisco/anyconnect/vpn/android/service/ILogUpdateListener;)Z

    move-result v3

    .line 109
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 110
    if-eqz v3, :cond_4

    move v7, v8

    :cond_4
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 115
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/ILogUpdateListener;
    .end local v3    # "_result":Z
    :sswitch_7
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 117
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v9

    invoke-static {v9}, Lcom/cisco/anyconnect/vpn/android/service/ILogUpdateListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/cisco/anyconnect/vpn/android/service/ILogUpdateListener;

    move-result-object v0

    .line 118
    .restart local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/ILogUpdateListener;
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->UnregisterLogListener(Lcom/cisco/anyconnect/vpn/android/service/ILogUpdateListener;)Z

    move-result v3

    .line 119
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 120
    if-eqz v3, :cond_5

    move v7, v8

    :cond_5
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 125
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/ILogUpdateListener;
    .end local v3    # "_result":Z
    :sswitch_8
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 127
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v9

    invoke-static {v9}, Lcom/cisco/anyconnect/vpn/android/service/IConnectionListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/cisco/anyconnect/vpn/android/service/IConnectionListener;

    move-result-object v0

    .line 128
    .local v0, "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IConnectionListener;
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->RegisterConnectionListener(Lcom/cisco/anyconnect/vpn/android/service/IConnectionListener;)Z

    move-result v3

    .line 129
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 130
    if-eqz v3, :cond_6

    move v7, v8

    :cond_6
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 135
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IConnectionListener;
    .end local v3    # "_result":Z
    :sswitch_9
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 137
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v9

    invoke-static {v9}, Lcom/cisco/anyconnect/vpn/android/service/IConnectionListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/cisco/anyconnect/vpn/android/service/IConnectionListener;

    move-result-object v0

    .line 138
    .restart local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IConnectionListener;
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->UnregisterConnectionListener(Lcom/cisco/anyconnect/vpn/android/service/IConnectionListener;)Z

    move-result v3

    .line 139
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 140
    if-eqz v3, :cond_7

    move v7, v8

    :cond_7
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 145
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IConnectionListener;
    .end local v3    # "_result":Z
    :sswitch_a
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 147
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v9

    invoke-static {v9}, Lcom/cisco/anyconnect/vpn/android/service/ICertificateListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/cisco/anyconnect/vpn/android/service/ICertificateListener;

    move-result-object v0

    .line 148
    .local v0, "_arg0":Lcom/cisco/anyconnect/vpn/android/service/ICertificateListener;
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->RegisterCertificateListener(Lcom/cisco/anyconnect/vpn/android/service/ICertificateListener;)Z

    move-result v3

    .line 149
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 150
    if-eqz v3, :cond_8

    move v7, v8

    :cond_8
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 155
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/ICertificateListener;
    .end local v3    # "_result":Z
    :sswitch_b
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 157
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v9

    invoke-static {v9}, Lcom/cisco/anyconnect/vpn/android/service/ICertificateListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/cisco/anyconnect/vpn/android/service/ICertificateListener;

    move-result-object v0

    .line 158
    .restart local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/ICertificateListener;
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->UnregisterCertificateListener(Lcom/cisco/anyconnect/vpn/android/service/ICertificateListener;)Z

    move-result v3

    .line 159
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 160
    if-eqz v3, :cond_9

    move v7, v8

    :cond_9
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 165
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/ICertificateListener;
    .end local v3    # "_result":Z
    :sswitch_c
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 167
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v9

    invoke-static {v9}, Lcom/cisco/anyconnect/vpn/android/service/IInfoListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/cisco/anyconnect/vpn/android/service/IInfoListener;

    move-result-object v0

    .line 168
    .local v0, "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IInfoListener;
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->RegisterInfoListener(Lcom/cisco/anyconnect/vpn/android/service/IInfoListener;)Z

    move-result v3

    .line 169
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 170
    if-eqz v3, :cond_a

    move v7, v8

    :cond_a
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 175
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IInfoListener;
    .end local v3    # "_result":Z
    :sswitch_d
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 177
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v9

    invoke-static {v9}, Lcom/cisco/anyconnect/vpn/android/service/IInfoListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/cisco/anyconnect/vpn/android/service/IInfoListener;

    move-result-object v0

    .line 178
    .restart local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IInfoListener;
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->UnregisterInfoListener(Lcom/cisco/anyconnect/vpn/android/service/IInfoListener;)Z

    move-result v3

    .line 179
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 180
    if-eqz v3, :cond_b

    move v7, v8

    :cond_b
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 185
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IInfoListener;
    .end local v3    # "_result":Z
    :sswitch_e
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 187
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v9

    invoke-static {v9}, Lcom/cisco/anyconnect/vpn/android/service/IImportListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/cisco/anyconnect/vpn/android/service/IImportListener;

    move-result-object v0

    .line 188
    .local v0, "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IImportListener;
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->RegisterImportListener(Lcom/cisco/anyconnect/vpn/android/service/IImportListener;)Z

    move-result v3

    .line 189
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 190
    if-eqz v3, :cond_c

    move v7, v8

    :cond_c
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 195
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IImportListener;
    .end local v3    # "_result":Z
    :sswitch_f
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 197
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v9

    invoke-static {v9}, Lcom/cisco/anyconnect/vpn/android/service/IImportListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/cisco/anyconnect/vpn/android/service/IImportListener;

    move-result-object v0

    .line 198
    .restart local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IImportListener;
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->UnregisterImportListener(Lcom/cisco/anyconnect/vpn/android/service/IImportListener;)Z

    move-result v3

    .line 199
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 200
    if-eqz v3, :cond_d

    move v7, v8

    :cond_d
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 205
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IImportListener;
    .end local v3    # "_result":Z
    :sswitch_10
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 207
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v9

    invoke-static {v9}, Lcom/cisco/anyconnect/vpn/android/service/IPromptHandler$Stub;->asInterface(Landroid/os/IBinder;)Lcom/cisco/anyconnect/vpn/android/service/IPromptHandler;

    move-result-object v0

    .line 208
    .local v0, "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IPromptHandler;
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->RegisterPromptHandler(Lcom/cisco/anyconnect/vpn/android/service/IPromptHandler;)Z

    move-result v3

    .line 209
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 210
    if-eqz v3, :cond_e

    move v7, v8

    :cond_e
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 215
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IPromptHandler;
    .end local v3    # "_result":Z
    :sswitch_11
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 217
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v9

    invoke-static {v9}, Lcom/cisco/anyconnect/vpn/android/service/IPromptHandler$Stub;->asInterface(Landroid/os/IBinder;)Lcom/cisco/anyconnect/vpn/android/service/IPromptHandler;

    move-result-object v0

    .line 218
    .restart local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IPromptHandler;
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->UnregisterPromptHandler(Lcom/cisco/anyconnect/vpn/android/service/IPromptHandler;)Z

    move-result v3

    .line 219
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 220
    if-eqz v3, :cond_f

    move v7, v8

    :cond_f
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 225
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/IPromptHandler;
    .end local v3    # "_result":Z
    :sswitch_12
    const-string v7, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 226
    invoke-virtual {p0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->GetAppMessages()Ljava/util/List;

    move-result-object v5

    .line 227
    .local v5, "_result":Ljava/util/List;, "Ljava/util/List<Lcom/cisco/anyconnect/vpn/android/service/NoticeInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 228
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 233
    .end local v5    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/cisco/anyconnect/vpn/android/service/NoticeInfo;>;"
    :sswitch_13
    const-string v7, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 234
    invoke-virtual {p0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->ClearAppMessages()V

    .line 235
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 240
    :sswitch_14
    const-string v7, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 241
    invoke-virtual {p0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->GetConnectionList()Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;

    move-result-object v3

    .line 242
    .local v3, "_result":Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 243
    if-eqz v3, :cond_10

    invoke-interface {v3}, Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;->asBinder()Landroid/os/IBinder;

    move-result-object v7

    :goto_1
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    goto/16 :goto_0

    :cond_10
    move-object v7, v9

    goto :goto_1

    .line 248
    .end local v3    # "_result":Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;
    :sswitch_15
    const-string v7, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 249
    invoke-virtual {p0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->GetVpnLogger()Lcom/cisco/anyconnect/vpn/android/service/IVpnLogger;

    move-result-object v3

    .line 250
    .local v3, "_result":Lcom/cisco/anyconnect/vpn/android/service/IVpnLogger;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 251
    if-eqz v3, :cond_11

    invoke-interface {v3}, Lcom/cisco/anyconnect/vpn/android/service/IVpnLogger;->asBinder()Landroid/os/IBinder;

    move-result-object v9

    :cond_11
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    goto/16 :goto_0

    .line 256
    .end local v3    # "_result":Lcom/cisco/anyconnect/vpn/android/service/IVpnLogger;
    :sswitch_16
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 257
    invoke-virtual {p0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->GetClientCertificates()Z

    move-result v3

    .line 258
    .local v3, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 259
    if-eqz v3, :cond_12

    move v7, v8

    :cond_12
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 264
    .end local v3    # "_result":Z
    :sswitch_17
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 265
    invoke-virtual {p0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->IsConnected()Z

    move-result v3

    .line 266
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 267
    if-eqz v3, :cond_13

    move v7, v8

    :cond_13
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 272
    .end local v3    # "_result":Z
    :sswitch_18
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 274
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_15

    .line 275
    sget-object v9, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;

    .line 280
    .local v0, "_arg0":Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;
    :goto_2
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->Connect(Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;)Z

    move-result v3

    .line 281
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 282
    if-eqz v3, :cond_14

    move v7, v8

    :cond_14
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 278
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;
    .end local v3    # "_result":Z
    :cond_15
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;
    goto :goto_2

    .line 287
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;
    :sswitch_19
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 289
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_17

    .line 290
    sget-object v9, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;

    .line 296
    .restart local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_18

    .line 297
    sget-object v9, Lcom/cisco/anyconnect/vpn/android/service/JniHashMapParcel;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cisco/anyconnect/vpn/android/service/JniHashMapParcel;

    .line 302
    .local v1, "_arg1":Lcom/cisco/anyconnect/vpn/android/service/JniHashMapParcel;
    :goto_4
    invoke-virtual {p0, v0, v1}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->ConnectWithPrefill(Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;Lcom/cisco/anyconnect/vpn/android/service/JniHashMapParcel;)Z

    move-result v3

    .line 303
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 304
    if-eqz v3, :cond_16

    move v7, v8

    :cond_16
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 293
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;
    .end local v1    # "_arg1":Lcom/cisco/anyconnect/vpn/android/service/JniHashMapParcel;
    .end local v3    # "_result":Z
    :cond_17
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;
    goto :goto_3

    .line 300
    :cond_18
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/cisco/anyconnect/vpn/android/service/JniHashMapParcel;
    goto :goto_4

    .line 309
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;
    .end local v1    # "_arg1":Lcom/cisco/anyconnect/vpn/android/service/JniHashMapParcel;
    :sswitch_1a
    const-string v7, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 310
    invoke-virtual {p0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->Disconnect()V

    .line 311
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 316
    :sswitch_1b
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 318
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_19

    move v0, v8

    .line 319
    .local v0, "_arg0":Z
    :goto_5
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->SetBannerResponse(Z)V

    .line 320
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    :cond_19
    move v0, v7

    .line 318
    goto :goto_5

    .line 325
    :sswitch_1c
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 327
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_1a

    move v0, v8

    .line 329
    .restart local v0    # "_arg0":Z
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_1b

    move v1, v8

    .line 330
    .local v1, "_arg1":Z
    :goto_7
    invoke-virtual {p0, v0, v1}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->SetCertBannerResponse(ZZ)V

    .line 331
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v1    # "_arg1":Z
    :cond_1a
    move v0, v7

    .line 327
    goto :goto_6

    .restart local v0    # "_arg0":Z
    :cond_1b
    move v1, v7

    .line 329
    goto :goto_7

    .line 336
    .end local v0    # "_arg0":Z
    :sswitch_1d
    const-string v7, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 338
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_1c

    .line 339
    sget-object v7, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;

    .line 344
    .local v0, "_arg0":Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;
    :goto_8
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->UserSubmit(Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;)V

    .line 345
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 342
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;
    :cond_1c
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;
    goto :goto_8

    .line 350
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;
    :sswitch_1e
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 351
    invoke-virtual {p0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->GetPreferences()Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;

    move-result-object v3

    .line 352
    .local v3, "_result":Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 353
    if-eqz v3, :cond_1d

    .line 354
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 355
    invoke-virtual {v3, p3, v8}, Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 358
    :cond_1d
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 364
    .end local v3    # "_result":Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;
    :sswitch_1f
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 366
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_1f

    .line 367
    sget-object v9, Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;

    .line 372
    .local v0, "_arg0":Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;
    :goto_9
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->SavePreferences(Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;)Z

    move-result v3

    .line 373
    .local v3, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 374
    if-eqz v3, :cond_1e

    move v7, v8

    :cond_1e
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 370
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;
    .end local v3    # "_result":Z
    :cond_1f
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;
    goto :goto_9

    .line 379
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;
    :sswitch_20
    const-string v7, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 381
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 382
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->SetNewTunnelGroup(Ljava/lang/String;)V

    .line 383
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 388
    .end local v0    # "_arg0":Ljava/lang/String;
    :sswitch_21
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 390
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_20

    move v0, v8

    .line 391
    .local v0, "_arg0":Z
    :goto_a
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->EnableStateNotifications(Z)V

    .line 392
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    :cond_20
    move v0, v7

    .line 390
    goto :goto_a

    .line 397
    :sswitch_22
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 398
    invoke-virtual {p0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->GetLastStats()Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;

    move-result-object v3

    .line 399
    .local v3, "_result":Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 400
    if-eqz v3, :cond_21

    .line 401
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 402
    invoke-virtual {v3, p3, v8}, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 405
    :cond_21
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 411
    .end local v3    # "_result":Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;
    :sswitch_23
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 413
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 414
    .local v0, "_arg0":[B
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->RequestImportPKCS12([B)Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;

    move-result-object v3

    .line 415
    .local v3, "_result":Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 416
    if-eqz v3, :cond_22

    .line 417
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 418
    invoke-virtual {v3, p3, v8}, Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 421
    :cond_22
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 427
    .end local v0    # "_arg0":[B
    .end local v3    # "_result":Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;
    :sswitch_24
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 429
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 431
    .restart local v0    # "_arg0":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 432
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->ImportPKCS12WithPassword([BLjava/lang/String;)Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;

    move-result-object v3

    .line 433
    .restart local v3    # "_result":Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 434
    if-eqz v3, :cond_23

    .line 435
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 436
    invoke-virtual {v3, p3, v8}, Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 439
    :cond_23
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 445
    .end local v0    # "_arg0":[B
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_result":Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;
    :sswitch_25
    const-string v7, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 447
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 448
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->ShutdownService(Ljava/lang/String;)V

    .line 449
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 454
    .end local v0    # "_arg0":Ljava/lang/String;
    :sswitch_26
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 455
    invoke-virtual {p0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->DeleteCurrentProfile()Z

    move-result v3

    .line 456
    .local v3, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 457
    if-eqz v3, :cond_24

    move v7, v8

    :cond_24
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 462
    .end local v3    # "_result":Z
    :sswitch_27
    const-string v7, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 463
    invoke-virtual {p0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->GetCurrentProfileContents()Ljava/lang/String;

    move-result-object v3

    .line 464
    .local v3, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 465
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 470
    .end local v3    # "_result":Ljava/lang/String;
    :sswitch_28
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 472
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 474
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 475
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->ImportProfile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 476
    .local v3, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 477
    if-eqz v3, :cond_25

    move v7, v8

    :cond_25
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 482
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_result":Z
    :sswitch_29
    const-string v7, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 484
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 485
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->EnumerateCertificates(I)Ljava/util/List;

    move-result-object v4

    .line 486
    .local v4, "_result":Ljava/util/List;, "Ljava/util/List<Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 487
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 492
    .end local v0    # "_arg0":I
    .end local v4    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;>;"
    :sswitch_2a
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 494
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 496
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 497
    .local v2, "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v0, v2}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->DeleteCertificates(ILjava/util/List;)Z

    move-result v3

    .line 498
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 499
    if-eqz v3, :cond_26

    move v7, v8

    :cond_26
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 504
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "_result":Z
    :sswitch_2b
    const-string v7, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 505
    invoke-virtual {p0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->GetAnyConnectVersion()Ljava/lang/String;

    move-result-object v3

    .line 506
    .local v3, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 507
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 512
    .end local v3    # "_result":Ljava/lang/String;
    :sswitch_2c
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 514
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 516
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 517
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->ImportServerL10nData(Ljava/lang/String;Ljava/lang/String;)Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;

    move-result-object v3

    .line 518
    .local v3, "_result":Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 519
    if-eqz v3, :cond_27

    .line 520
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 521
    invoke-virtual {v3, p3, v8}, Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 524
    :cond_27
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 530
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_result":Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;
    :sswitch_2d
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 531
    invoke-virtual {p0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->RestoreDefaultL10nData()Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;

    move-result-object v3

    .line 532
    .restart local v3    # "_result":Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 533
    if-eqz v3, :cond_28

    .line 534
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 535
    invoke-virtual {v3, p3, v8}, Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 538
    :cond_28
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 544
    .end local v3    # "_result":Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;
    :sswitch_2e
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 546
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_2a

    .line 547
    sget-object v9, Lcom/cisco/anyconnect/vpn/android/service/OperatingModeParcel;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cisco/anyconnect/vpn/android/service/OperatingModeParcel;

    .line 552
    .local v0, "_arg0":Lcom/cisco/anyconnect/vpn/android/service/OperatingModeParcel;
    :goto_b
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->IsOperatingMode(Lcom/cisco/anyconnect/vpn/android/service/OperatingModeParcel;)Z

    move-result v3

    .line 553
    .local v3, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 554
    if-eqz v3, :cond_29

    move v7, v8

    :cond_29
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 550
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/OperatingModeParcel;
    .end local v3    # "_result":Z
    :cond_2a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/OperatingModeParcel;
    goto :goto_b

    .line 559
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/OperatingModeParcel;
    :sswitch_2f
    const-string v7, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 560
    invoke-virtual {p0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->GetAvailableLocales()Ljava/util/List;

    move-result-object v6

    .line 561
    .local v6, "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 562
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 567
    .end local v6    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_30
    const-string v7, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 568
    invoke-virtual {p0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->GetActiveLocale()Ljava/lang/String;

    move-result-object v3

    .line 569
    .local v3, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 570
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 575
    .end local v3    # "_result":Ljava/lang/String;
    :sswitch_31
    const-string v9, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 577
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_2c

    move v0, v8

    .line 578
    .local v0, "_arg0":Z
    :goto_c
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->SetFipsMode(Z)Z

    move-result v3

    .line 579
    .local v3, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 580
    if-eqz v3, :cond_2b

    move v7, v8

    :cond_2b
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v3    # "_result":Z
    :cond_2c
    move v0, v7

    .line 577
    goto :goto_c

    .line 585
    :sswitch_32
    const-string v7, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 586
    invoke-virtual {p0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->GetPrivateApi()Lcom/cisco/anyconnect/vpn/android/service/IPrivateVpnService;

    move-result-object v3

    .line 587
    .local v3, "_result":Lcom/cisco/anyconnect/vpn/android/service/IPrivateVpnService;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 588
    if-eqz v3, :cond_2d

    invoke-interface {v3}, Lcom/cisco/anyconnect/vpn/android/service/IPrivateVpnService;->asBinder()Landroid/os/IBinder;

    move-result-object v9

    :cond_2d
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    goto/16 :goto_0

    .line 593
    .end local v3    # "_result":Lcom/cisco/anyconnect/vpn/android/service/IPrivateVpnService;
    :sswitch_33
    const-string v7, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 595
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_2e

    .line 596
    sget-object v7, Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;

    .line 601
    .local v0, "_arg0":Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;
    :goto_d
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->GetErrorString(Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;)Ljava/lang/String;

    move-result-object v3

    .line 602
    .local v3, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 603
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 599
    .end local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;
    .end local v3    # "_result":Ljava/lang/String;
    :cond_2e
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;
    goto :goto_d

    .line 48
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x25 -> :sswitch_25
        0x26 -> :sswitch_26
        0x27 -> :sswitch_27
        0x28 -> :sswitch_28
        0x29 -> :sswitch_29
        0x2a -> :sswitch_2a
        0x2b -> :sswitch_2b
        0x2c -> :sswitch_2c
        0x2d -> :sswitch_2d
        0x2e -> :sswitch_2e
        0x2f -> :sswitch_2f
        0x30 -> :sswitch_30
        0x31 -> :sswitch_31
        0x32 -> :sswitch_32
        0x33 -> :sswitch_33
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
