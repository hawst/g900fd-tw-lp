.class public Lcom/cisco/anyconnect/vpn/jni/ACLogEntry;
.super Ljava/lang/Object;
.source "ACLogEntry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cisco/anyconnect/vpn/jni/ACLogEntry$Severity;
    }
.end annotation


# static fields
.field private static final CALENDAR:Ljava/util/Calendar;

.field private static final DATE_FORMAT:Ljava/text/SimpleDateFormat;


# instance fields
.field public message:Ljava/lang/String;

.field public severity:Lcom/cisco/anyconnect/vpn/jni/ACLogEntry$Severity;

.field public tag:Ljava/lang/String;

.field public time:J

.field public timeMs:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MM-dd-yyyy HH:mm:ss.S"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/cisco/anyconnect/vpn/jni/ACLogEntry;->DATE_FORMAT:Ljava/text/SimpleDateFormat;

    .line 40
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    sput-object v0, Lcom/cisco/anyconnect/vpn/jni/ACLogEntry;->CALENDAR:Ljava/util/Calendar;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 45
    sget-object v0, Lcom/cisco/anyconnect/vpn/jni/ACLogEntry;->CALENDAR:Ljava/util/Calendar;

    iget-wide v2, p0, Lcom/cisco/anyconnect/vpn/jni/ACLogEntry;->time:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iget-wide v4, p0, Lcom/cisco/anyconnect/vpn/jni/ACLogEntry;->timeMs:J

    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/cisco/anyconnect/vpn/jni/ACLogEntry;->DATE_FORMAT:Ljava/text/SimpleDateFormat;

    sget-object v2, Lcom/cisco/anyconnect/vpn/jni/ACLogEntry;->CALENDAR:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/cisco/anyconnect/vpn/jni/ACLogEntry;->severity:Lcom/cisco/anyconnect/vpn/jni/ACLogEntry$Severity;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/cisco/anyconnect/vpn/jni/ACLogEntry;->tag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/cisco/anyconnect/vpn/jni/ACLogEntry;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
