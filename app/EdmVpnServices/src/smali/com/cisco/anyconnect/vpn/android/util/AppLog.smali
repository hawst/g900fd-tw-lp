.class public Lcom/cisco/anyconnect/vpn/android/util/AppLog;
.super Ljava/lang/Object;
.source "AppLog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cisco/anyconnect/vpn/android/util/AppLog$ILogger;,
        Lcom/cisco/anyconnect/vpn/android/util/AppLog$Severity;
    }
.end annotation


# static fields
.field private static sLogger:Lcom/cisco/anyconnect/vpn/android/util/AppLog$ILogger;


# direct methods
.method public static logDebugMessage(Lcom/cisco/anyconnect/vpn/android/util/AppLog$Severity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "sev"    # Lcom/cisco/anyconnect/vpn/android/util/AppLog$Severity;
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 54
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 55
    .local v0, "logString":Ljava/lang/String;
    sget-object v1, Lcom/cisco/anyconnect/vpn/android/util/AppLog;->sLogger:Lcom/cisco/anyconnect/vpn/android/util/AppLog$ILogger;

    const/4 v2, 0x0

    invoke-interface {v1, p0, v0, v2}, Lcom/cisco/anyconnect/vpn/android/util/AppLog$ILogger;->log(Lcom/cisco/anyconnect/vpn/android/util/AppLog$Severity;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 56
    return-void
.end method
