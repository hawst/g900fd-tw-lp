.class Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$1;
.super Lcom/cisco/anyconnect/vpn/android/service/IServiceStateListener$Stub;
.source "ServiceConnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;


# direct methods
.method constructor <init>(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$1;->this$0:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    invoke-direct {p0}, Lcom/cisco/anyconnect/vpn/android/service/IServiceStateListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public ServiceReadyCB()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 68
    monitor-enter p0

    .line 70
    :try_start_0
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$1;->this$0:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    const/4 v1, 0x0

    # setter for: Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mSuppressConnectMsg:Z
    invoke-static {v0, v1}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->access$202(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;Z)Z

    .line 71
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$1;->this$0:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    # invokes: Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->stopTimer()V
    invoke-static {v0}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->access$300(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)V

    .line 73
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$1;->this$0:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    # getter for: Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mCallback:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;
    invoke-static {v0}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->access$400(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$1;->this$0:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    iget-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$1;->this$0:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    # getter for: Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mVpnService:Lcom/cisco/anyconnect/vpn/android/service/IVpnService;
    invoke-static {v1}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->access$500(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    move-result-object v1

    # invokes: Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->onServiceConnectedCB(Lcom/cisco/anyconnect/vpn/android/service/IVpnService;)V
    invoke-static {v0, v1}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->access$600(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;Lcom/cisco/anyconnect/vpn/android/service/IVpnService;)V

    .line 77
    :cond_0
    monitor-exit p0

    .line 78
    return-void

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public ServiceShutdownCB(Ljava/lang/String;)V
    .locals 2
    .param p1, "shutdownMsg"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$1;->this$0:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    # getter for: Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->access$100(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$1$1;

    invoke-direct {v1, p0, p1}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$1$1;-><init>(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$1;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 64
    return-void
.end method
