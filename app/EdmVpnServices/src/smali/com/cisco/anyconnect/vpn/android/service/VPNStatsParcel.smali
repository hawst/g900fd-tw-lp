.class public Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;
.super Lcom/cisco/anyconnect/vpn/jni/VPNStats;
.source "VPNStatsParcel.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 187
    new-instance v0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel$1;

    invoke-direct {v0}, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel$1;-><init>()V

    sput-object v0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/cisco/anyconnect/vpn/jni/VPNStats;-><init>()V

    .line 30
    invoke-direct {p0, p1}, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->readFromParcel(Landroid/os/Parcel;)V

    .line 31
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel$1;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private parcelProtocolInfoArray(Landroid/os/Parcel;[Lcom/cisco/anyconnect/vpn/jni/ProtocolInfo;)V
    .locals 5
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "protocols"    # [Lcom/cisco/anyconnect/vpn/jni/ProtocolInfo;

    .prologue
    .line 126
    if-eqz p2, :cond_1

    array-length v3, p2

    if-eqz v3, :cond_1

    .line 128
    array-length v1, p2

    .line 129
    .local v1, "nProtocols":I
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 130
    new-array v2, v1, [Lcom/cisco/anyconnect/vpn/android/service/ProtocolInfoParcel;

    .line 131
    .local v2, "protocolsParcel":[Lcom/cisco/anyconnect/vpn/android/service/ProtocolInfoParcel;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 133
    new-instance v3, Lcom/cisco/anyconnect/vpn/android/service/ProtocolInfoParcel;

    aget-object v4, p2, v0

    invoke-direct {v3, v4}, Lcom/cisco/anyconnect/vpn/android/service/ProtocolInfoParcel;-><init>(Lcom/cisco/anyconnect/vpn/jni/ProtocolInfo;)V

    aput-object v3, v2, v0

    .line 131
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 135
    :cond_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeArray([Ljava/lang/Object;)V

    .line 141
    .end local v0    # "i":I
    .end local v1    # "nProtocols":I
    .end local v2    # "protocolsParcel":[Lcom/cisco/anyconnect/vpn/android/service/ProtocolInfoParcel;
    :goto_1
    return-void

    .line 139
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1
.end method

.method private parcelRouteInfoArray(Landroid/os/Parcel;[Lcom/cisco/anyconnect/vpn/jni/RouteInfo;)V
    .locals 5
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "routes"    # [Lcom/cisco/anyconnect/vpn/jni/RouteInfo;

    .prologue
    .line 107
    if-eqz p2, :cond_1

    array-length v3, p2

    if-eqz v3, :cond_1

    .line 109
    array-length v1, p2

    .line 110
    .local v1, "nRoutes":I
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 111
    new-array v2, v1, [Lcom/cisco/anyconnect/vpn/android/service/RouteInfoParcel;

    .line 112
    .local v2, "routesParcel":[Lcom/cisco/anyconnect/vpn/android/service/RouteInfoParcel;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 114
    new-instance v3, Lcom/cisco/anyconnect/vpn/android/service/RouteInfoParcel;

    aget-object v4, p2, v0

    invoke-direct {v3, v4}, Lcom/cisco/anyconnect/vpn/android/service/RouteInfoParcel;-><init>(Lcom/cisco/anyconnect/vpn/jni/RouteInfo;)V

    aput-object v3, v2, v0

    .line 112
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 116
    :cond_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeArray([Ljava/lang/Object;)V

    .line 122
    .end local v0    # "i":I
    .end local v1    # "nRoutes":I
    .end local v2    # "routesParcel":[Lcom/cisco/anyconnect/vpn/android/service/RouteInfoParcel;
    :goto_1
    return-void

    .line 120
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 35
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->state:Ljava/lang/String;

    .line 36
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->timeConnected:Ljava/lang/String;

    .line 37
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->bytesSent:Ljava/lang/String;

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->bytesReceived:Ljava/lang/String;

    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->packetsSent:Ljava/lang/String;

    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->packetsReceived:Ljava/lang/String;

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->controlBytesSent:Ljava/lang/String;

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->controlBytesReceived:Ljava/lang/String;

    .line 43
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->controlPacketsSent:Ljava/lang/String;

    .line 44
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->controlPacketsReceived:Ljava/lang/String;

    .line 45
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->encryptedBytesSent:Ljava/lang/String;

    .line 46
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->encryptedBytesReceived:Ljava/lang/String;

    .line 47
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->encryptedPacketsSent:Ljava/lang/String;

    .line 48
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->encryptedPacketsReceived:Ljava/lang/String;

    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->compressedBytesSent:Ljava/lang/String;

    .line 50
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->compressedBytesReceived:Ljava/lang/String;

    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->compressedPacketsSent:Ljava/lang/String;

    .line 52
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->compressedPacketsReceived:Ljava/lang/String;

    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->inboundDiscarded:Ljava/lang/String;

    .line 54
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->outboundDiscarded:Ljava/lang/String;

    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->inboundBypassed:Ljava/lang/String;

    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->outboundBypassed:Ljava/lang/String;

    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->clientAddress:Ljava/lang/String;

    .line 58
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->serverAddress:Ljava/lang/String;

    .line 59
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->clientAddressV6:Ljava/lang/String;

    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->serverAddressV6:Ljava/lang/String;

    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->serverHostName:Ljava/lang/String;

    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->proxyAddress:Ljava/lang/String;

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->proxyHostName:Ljava/lang/String;

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->proxyPort:Ljava/lang/String;

    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->tunnelingMode:Ljava/lang/String;

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->fipsMode:Ljava/lang/String;

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->trustedNetworkDetectionMode:Ljava/lang/String;

    .line 68
    invoke-direct {p0, p1}, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->unparcelRouteInfoArray(Landroid/os/Parcel;)[Lcom/cisco/anyconnect/vpn/jni/RouteInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->secureRoutes:[Lcom/cisco/anyconnect/vpn/jni/RouteInfo;

    .line 69
    invoke-direct {p0, p1}, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->unparcelRouteInfoArray(Landroid/os/Parcel;)[Lcom/cisco/anyconnect/vpn/jni/RouteInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->nonSecureRoutes:[Lcom/cisco/anyconnect/vpn/jni/RouteInfo;

    .line 70
    invoke-direct {p0, p1}, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->unparcelProtocolInfoArray(Landroid/os/Parcel;)[Lcom/cisco/anyconnect/vpn/jni/ProtocolInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->protocolInfo:[Lcom/cisco/anyconnect/vpn/jni/ProtocolInfo;

    .line 71
    return-void
.end method

.method private unparcelProtocolInfoArray(Landroid/os/Parcel;)[Lcom/cisco/anyconnect/vpn/jni/ProtocolInfo;
    .locals 5
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 91
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 92
    .local v1, "nEntries":I
    new-array v3, v1, [Lcom/cisco/anyconnect/vpn/jni/ProtocolInfo;

    .line 93
    .local v3, "out":[Lcom/cisco/anyconnect/vpn/jni/ProtocolInfo;
    if-lez v1, :cond_0

    .line 95
    const-class v4, Lcom/cisco/anyconnect/vpn/android/service/ProtocolInfoParcel;

    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->readArray(Ljava/lang/ClassLoader;)[Ljava/lang/Object;

    move-result-object v2

    .line 96
    .local v2, "objs":[Ljava/lang/Object;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_0

    .line 98
    aget-object v4, v2, v0

    check-cast v4, Lcom/cisco/anyconnect/vpn/jni/ProtocolInfo;

    aput-object v4, v3, v0

    .line 96
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 102
    .end local v0    # "i":I
    .end local v2    # "objs":[Ljava/lang/Object;
    :cond_0
    return-object v3
.end method

.method private unparcelRouteInfoArray(Landroid/os/Parcel;)[Lcom/cisco/anyconnect/vpn/jni/RouteInfo;
    .locals 5
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 76
    .local v1, "nEntries":I
    new-array v3, v1, [Lcom/cisco/anyconnect/vpn/jni/RouteInfo;

    .line 77
    .local v3, "out":[Lcom/cisco/anyconnect/vpn/jni/RouteInfo;
    if-lez v1, :cond_0

    .line 79
    const-class v4, Lcom/cisco/anyconnect/vpn/android/service/RouteInfoParcel;

    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->readArray(Ljava/lang/ClassLoader;)[Ljava/lang/Object;

    move-result-object v2

    .line 80
    .local v2, "objs":[Ljava/lang/Object;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_0

    .line 82
    aget-object v4, v2, v0

    check-cast v4, Lcom/cisco/anyconnect/vpn/jni/RouteInfo;

    aput-object v4, v3, v0

    .line 80
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 86
    .end local v0    # "i":I
    .end local v2    # "objs":[Ljava/lang/Object;
    :cond_0
    return-object v3
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 209
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->state:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 210
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->timeConnected:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 211
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->bytesSent:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->bytesReceived:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 213
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->packetsSent:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->packetsReceived:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 215
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->controlBytesSent:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->controlBytesReceived:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 217
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->controlPacketsSent:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 218
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->controlPacketsReceived:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->encryptedBytesSent:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 220
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->encryptedBytesReceived:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 221
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->encryptedPacketsSent:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->encryptedPacketsReceived:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 223
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->compressedBytesSent:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 224
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->compressedBytesReceived:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 225
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->compressedPacketsSent:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 226
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->compressedPacketsReceived:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 227
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->inboundDiscarded:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 228
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->outboundDiscarded:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 229
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->inboundBypassed:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 230
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->outboundBypassed:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 231
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->clientAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 232
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->serverAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 233
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->clientAddressV6:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 234
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->serverAddressV6:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 235
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->serverHostName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 236
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->proxyAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 237
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->proxyHostName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->proxyPort:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 239
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->tunnelingMode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 240
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->fipsMode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 241
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->trustedNetworkDetectionMode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 243
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->secureRoutes:[Lcom/cisco/anyconnect/vpn/jni/RouteInfo;

    invoke-direct {p0, p1, v0}, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->parcelRouteInfoArray(Landroid/os/Parcel;[Lcom/cisco/anyconnect/vpn/jni/RouteInfo;)V

    .line 244
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->nonSecureRoutes:[Lcom/cisco/anyconnect/vpn/jni/RouteInfo;

    invoke-direct {p0, p1, v0}, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->parcelRouteInfoArray(Landroid/os/Parcel;[Lcom/cisco/anyconnect/vpn/jni/RouteInfo;)V

    .line 245
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->protocolInfo:[Lcom/cisco/anyconnect/vpn/jni/ProtocolInfo;

    invoke-direct {p0, p1, v0}, Lcom/cisco/anyconnect/vpn/android/service/VPNStatsParcel;->parcelProtocolInfoArray(Landroid/os/Parcel;[Lcom/cisco/anyconnect/vpn/jni/ProtocolInfo;)V

    .line 246
    return-void
.end method
