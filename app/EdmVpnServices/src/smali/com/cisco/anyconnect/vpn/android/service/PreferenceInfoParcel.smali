.class public Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;
.super Lcom/cisco/anyconnect/vpn/jni/PreferenceInfo;
.source "PreferenceInfoParcel.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel$1;

    invoke-direct {v0}, Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel$1;-><init>()V

    sput-object v0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/cisco/anyconnect/vpn/jni/PreferenceInfo;-><init>()V

    .line 30
    invoke-direct {p0, p1}, Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;->readFromParcel(Landroid/os/Parcel;)V

    .line 31
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel$1;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 5
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 35
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;->heading:Ljava/lang/String;

    .line 36
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 37
    .local v1, "nPrefs":I
    new-array v3, v1, [Lcom/cisco/anyconnect/vpn/jni/Preference;

    iput-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;->prefs:[Lcom/cisco/anyconnect/vpn/jni/Preference;

    .line 39
    const-class v3, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readArray(Ljava/lang/ClassLoader;)[Ljava/lang/Object;

    move-result-object v2

    .line 40
    .local v2, "objs":[Ljava/lang/Object;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 42
    iget-object v4, p0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;->prefs:[Lcom/cisco/anyconnect/vpn/jni/Preference;

    aget-object v3, v2, v0

    check-cast v3, Lcom/cisco/anyconnect/vpn/jni/Preference;

    aput-object v3, v4, v0

    .line 40
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 44
    :cond_0
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 61
    iget-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;->heading:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 62
    iget-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;->prefs:[Lcom/cisco/anyconnect/vpn/jni/Preference;

    if-eqz v3, :cond_1

    .line 64
    iget-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;->prefs:[Lcom/cisco/anyconnect/vpn/jni/Preference;

    array-length v1, v3

    .line 65
    .local v1, "nPrefs":I
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 66
    new-array v2, v1, [Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;

    .line 67
    .local v2, "prefParcels":[Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 69
    new-instance v3, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;

    iget-object v4, p0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceInfoParcel;->prefs:[Lcom/cisco/anyconnect/vpn/jni/Preference;

    aget-object v4, v4, v0

    invoke-direct {v3, v4}, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;-><init>(Lcom/cisco/anyconnect/vpn/jni/Preference;)V

    aput-object v3, v2, v0

    .line 67
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 71
    :cond_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeArray([Ljava/lang/Object;)V

    .line 77
    .end local v0    # "i":I
    .end local v1    # "nPrefs":I
    .end local v2    # "prefParcels":[Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;
    :goto_1
    return-void

    .line 75
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1
.end method
