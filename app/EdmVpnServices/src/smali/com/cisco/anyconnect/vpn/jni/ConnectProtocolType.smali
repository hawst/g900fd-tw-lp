.class public final enum Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;
.super Ljava/lang/Enum;
.source "ConnectProtocolType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

.field public static final enum Ipsec:Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

.field public static final enum Ssl:Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

.field public static final enum Unknown:Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

.field private static mTypes:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 5
    new-instance v6, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    const-string v7, "Unknown"

    invoke-direct {v6, v7, v8}, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;->Unknown:Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    .line 6
    new-instance v6, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    const-string v7, "Ssl"

    invoke-direct {v6, v7, v9}, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;->Ssl:Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    .line 7
    new-instance v6, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    const-string v7, "Ipsec"

    invoke-direct {v6, v7, v10}, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;->Ipsec:Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    .line 3
    const/4 v6, 0x3

    new-array v6, v6, [Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    sget-object v7, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;->Unknown:Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    aput-object v7, v6, v8

    sget-object v7, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;->Ssl:Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    aput-object v7, v6, v9

    sget-object v7, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;->Ipsec:Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    aput-object v7, v6, v10

    sput-object v6, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;->$VALUES:[Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    .line 13
    invoke-static {}, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;->values()[Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    move-result-object v6

    array-length v6, v6

    new-array v6, v6, [Ljava/lang/String;

    sput-object v6, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;->mTypes:[Ljava/lang/String;

    .line 14
    const/4 v1, 0x0

    .line 15
    .local v1, "i":I
    invoke-static {}, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;->values()[Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    move v2, v1

    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v5, v0, v3

    .line 17
    .local v5, "type":Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;
    sget-object v6, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;->mTypes:[Ljava/lang/String;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-virtual {v5}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    .line 15
    add-int/lit8 v3, v3, 0x1

    move v2, v1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 19
    .end local v5    # "type":Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    return-object v0
.end method

.method public static values()[Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;->$VALUES:[Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    invoke-virtual {v0}, [Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    return-object v0
.end method
