.class public final enum Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;
.super Ljava/lang/Enum;
.source "IPsecAuthMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

.field public static final enum USER_AUTH_IKE_EAP_ANYCONNECT:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

.field public static final enum USER_AUTH_IKE_EAP_GTC:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

.field public static final enum USER_AUTH_IKE_EAP_MD5:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

.field public static final enum USER_AUTH_IKE_EAP_MSCHAPv2:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

.field public static final enum USER_AUTH_IKE_PSK:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

.field public static final enum USER_AUTH_IKE_RSA:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

.field public static final enum USER_AUTH_SSL_MACHINE_STORE_CERT:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

.field public static final enum USER_AUTH_UNKNOWN:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

.field private static mAvailableModes:[Ljava/lang/String;

.field private static mModes:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 18
    new-instance v6, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    const-string v7, "USER_AUTH_UNKNOWN"

    invoke-direct {v6, v7, v9}, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_UNKNOWN:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    .line 19
    new-instance v6, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    const-string v7, "USER_AUTH_SSL_MACHINE_STORE_CERT"

    invoke-direct {v6, v7, v10}, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_SSL_MACHINE_STORE_CERT:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    .line 20
    new-instance v6, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    const-string v7, "USER_AUTH_IKE_PSK"

    invoke-direct {v6, v7, v11}, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_PSK:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    .line 21
    new-instance v6, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    const-string v7, "USER_AUTH_IKE_RSA"

    invoke-direct {v6, v7, v12}, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_RSA:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    .line 22
    new-instance v6, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    const-string v7, "USER_AUTH_IKE_EAP_MD5"

    invoke-direct {v6, v7, v13}, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_EAP_MD5:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    .line 23
    new-instance v6, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    const-string v7, "USER_AUTH_IKE_EAP_MSCHAPv2"

    const/4 v8, 0x5

    invoke-direct {v6, v7, v8}, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_EAP_MSCHAPv2:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    .line 24
    new-instance v6, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    const-string v7, "USER_AUTH_IKE_EAP_GTC"

    const/4 v8, 0x6

    invoke-direct {v6, v7, v8}, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_EAP_GTC:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    .line 25
    new-instance v6, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    const-string v7, "USER_AUTH_IKE_EAP_ANYCONNECT"

    const/4 v8, 0x7

    invoke-direct {v6, v7, v8}, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_EAP_ANYCONNECT:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    .line 16
    const/16 v6, 0x8

    new-array v6, v6, [Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    sget-object v7, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_UNKNOWN:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    aput-object v7, v6, v9

    sget-object v7, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_SSL_MACHINE_STORE_CERT:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    aput-object v7, v6, v10

    sget-object v7, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_PSK:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    aput-object v7, v6, v11

    sget-object v7, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_RSA:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    aput-object v7, v6, v12

    sget-object v7, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_EAP_MD5:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    aput-object v7, v6, v13

    const/4 v7, 0x5

    sget-object v8, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_EAP_MSCHAPv2:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    aput-object v8, v6, v7

    const/4 v7, 0x6

    sget-object v8, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_EAP_GTC:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    aput-object v8, v6, v7

    const/4 v7, 0x7

    sget-object v8, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_EAP_ANYCONNECT:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    aput-object v8, v6, v7

    sput-object v6, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->$VALUES:[Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    .line 31
    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/String;

    sget-object v7, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_EAP_ANYCONNECT:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    invoke-virtual {v7}, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    sget-object v7, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_RSA:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    invoke-virtual {v7}, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    sget-object v7, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_EAP_GTC:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    invoke-virtual {v7}, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v11

    sget-object v7, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_EAP_MD5:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    invoke-virtual {v7}, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v12

    sget-object v7, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_EAP_MSCHAPv2:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    invoke-virtual {v7}, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v13

    sput-object v6, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->mAvailableModes:[Ljava/lang/String;

    .line 69
    invoke-static {}, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->values()[Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    move-result-object v6

    array-length v6, v6

    new-array v6, v6, [Ljava/lang/String;

    sput-object v6, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->mModes:[Ljava/lang/String;

    .line 70
    const/4 v1, 0x0

    .line 71
    .local v1, "i":I
    invoke-static {}, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->values()[Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    move-result-object v0

    .local v0, "arr$":[Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    move v2, v1

    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v5, v0, v3

    .line 73
    .local v5, "mode":Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;
    sget-object v6, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->mModes:[Ljava/lang/String;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-virtual {v5}, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    .line 71
    add-int/lit8 v3, v3, 0x1

    move v2, v1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 75
    .end local v5    # "mode":Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 16
    const-class v0, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    return-object v0
.end method

.method public static values()[Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->$VALUES:[Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    invoke-virtual {v0}, [Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_EAP_ANYCONNECT:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    if-ne v0, p0, :cond_0

    .line 42
    const-string v0, "EAP-AnyConnect"

    .line 63
    :goto_0
    return-object v0

    .line 44
    :cond_0
    sget-object v0, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_RSA:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    if-ne v0, p0, :cond_1

    .line 46
    const-string v0, "IKE-RSA"

    goto :goto_0

    .line 48
    :cond_1
    sget-object v0, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_EAP_GTC:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    if-ne v0, p0, :cond_2

    .line 50
    const-string v0, "EAP-GTC"

    goto :goto_0

    .line 52
    :cond_2
    sget-object v0, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_EAP_MD5:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    if-ne v0, p0, :cond_3

    .line 54
    const-string v0, "EAP-MD5"

    goto :goto_0

    .line 56
    :cond_3
    sget-object v0, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_EAP_MSCHAPv2:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    if-ne v0, p0, :cond_4

    .line 58
    const-string v0, "EAP-MSCHAPv2"

    goto :goto_0

    .line 63
    :cond_4
    const-string v0, ""

    goto :goto_0
.end method
