.class public Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;
.super Lcom/cisco/anyconnect/vpn/jni/Preference;
.source "PreferenceParcel.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel$1;

    invoke-direct {v0}, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel$1;-><init>()V

    sput-object v0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/cisco/anyconnect/vpn/jni/Preference;-><init>()V

    .line 29
    invoke-direct {p0, p1}, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;->readFromParcel(Landroid/os/Parcel;)V

    .line 30
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel$1;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/cisco/anyconnect/vpn/jni/Preference;)V
    .locals 1
    .param p1, "pref"    # Lcom/cisco/anyconnect/vpn/jni/Preference;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/cisco/anyconnect/vpn/jni/Preference;-><init>()V

    .line 50
    if-eqz p1, :cond_0

    .line 52
    iget-object v0, p1, Lcom/cisco/anyconnect/vpn/jni/Preference;->id:Lcom/cisco/anyconnect/vpn/jni/Preference$PreferenceId;

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;->id:Lcom/cisco/anyconnect/vpn/jni/Preference$PreferenceId;

    .line 53
    iget-object v0, p1, Lcom/cisco/anyconnect/vpn/jni/Preference;->promptEntry:Lcom/cisco/anyconnect/vpn/jni/PromptEntry;

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;->promptEntry:Lcom/cisco/anyconnect/vpn/jni/PromptEntry;

    .line 54
    iget-object v0, p1, Lcom/cisco/anyconnect/vpn/jni/Preference;->children:[Lcom/cisco/anyconnect/vpn/jni/Preference;

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;->children:[Lcom/cisco/anyconnect/vpn/jni/Preference;

    .line 56
    :cond_0
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 34
    invoke-static {}, Lcom/cisco/anyconnect/vpn/jni/Preference$PreferenceId;->values()[Lcom/cisco/anyconnect/vpn/jni/Preference$PreferenceId;

    move-result-object v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    aget-object v4, v4, v5

    iput-object v4, p0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;->id:Lcom/cisco/anyconnect/vpn/jni/Preference$PreferenceId;

    .line 35
    const-class v4, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;

    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;

    .line 36
    .local v3, "p":Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;
    iput-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;->promptEntry:Lcom/cisco/anyconnect/vpn/jni/PromptEntry;

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 39
    .local v1, "nChildren":I
    new-array v4, v1, [Lcom/cisco/anyconnect/vpn/jni/Preference;

    iput-object v4, p0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;->children:[Lcom/cisco/anyconnect/vpn/jni/Preference;

    .line 41
    const-class v4, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;

    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->readArray(Ljava/lang/ClassLoader;)[Ljava/lang/Object;

    move-result-object v2

    .line 42
    .local v2, "objs":[Ljava/lang/Object;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 44
    iget-object v5, p0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;->children:[Lcom/cisco/anyconnect/vpn/jni/Preference;

    aget-object v4, v2, v0

    check-cast v4, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;

    aput-object v4, v5, v0

    .line 42
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 46
    :cond_0
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 78
    iget-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;->id:Lcom/cisco/anyconnect/vpn/jni/Preference$PreferenceId;

    invoke-virtual {v3}, Lcom/cisco/anyconnect/vpn/jni/Preference$PreferenceId;->ordinal()I

    move-result v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 79
    new-instance v3, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;

    iget-object v4, p0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;->promptEntry:Lcom/cisco/anyconnect/vpn/jni/PromptEntry;

    invoke-direct {v3, v4}, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;-><init>(Lcom/cisco/anyconnect/vpn/jni/PromptEntry;)V

    const/4 v4, 0x1

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 80
    iget-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;->children:[Lcom/cisco/anyconnect/vpn/jni/Preference;

    if-eqz v3, :cond_1

    .line 82
    iget-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;->children:[Lcom/cisco/anyconnect/vpn/jni/Preference;

    array-length v2, v3

    .line 83
    .local v2, "nChildren":I
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 84
    new-array v0, v2, [Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;

    .line 85
    .local v0, "childrenParcel":[Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 87
    new-instance v3, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;

    iget-object v4, p0, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;->children:[Lcom/cisco/anyconnect/vpn/jni/Preference;

    aget-object v4, v4, v1

    invoke-direct {v3, v4}, Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;-><init>(Lcom/cisco/anyconnect/vpn/jni/Preference;)V

    aput-object v3, v0, v1

    .line 85
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 89
    :cond_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeArray([Ljava/lang/Object;)V

    .line 96
    .end local v0    # "childrenParcel":[Lcom/cisco/anyconnect/vpn/android/service/PreferenceParcel;
    .end local v1    # "i":I
    .end local v2    # "nChildren":I
    :goto_1
    return-void

    .line 93
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1
.end method
