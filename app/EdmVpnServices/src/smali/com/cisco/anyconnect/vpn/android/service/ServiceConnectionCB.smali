.class public abstract Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;
.super Ljava/lang/Object;
.source "ServiceConnectionCB.java"


# instance fields
.field private mParent:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "parent"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;->mParent:Landroid/content/Context;

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;->mParent:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method GetContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;->mParent:Landroid/content/Context;

    return-object v0
.end method

.method public OnBindTimeout()V
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;->mParent:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 49
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;->mParent:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "Error"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Failed to bind to VpnService"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB$1;

    invoke-direct {v2, p0}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB$1;-><init>(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 61
    :cond_0
    return-void
.end method

.method public abstract OnServiceConnected(Lcom/cisco/anyconnect/vpn/android/service/IVpnService;)V
.end method

.method public OnServiceDisconnected()V
    .locals 0

    .prologue
    .line 89
    return-void
.end method

.method public OnServiceWillDisconnect(Lcom/cisco/anyconnect/vpn/android/service/IVpnService;ZLjava/lang/String;)V
    .locals 0
    .param p1, "vpnService"    # Lcom/cisco/anyconnect/vpn/android/service/IVpnService;
    .param p2, "isForcedShutdown"    # Z
    .param p3, "shutdownMsg"    # Ljava/lang/String;

    .prologue
    .line 79
    return-void
.end method

.method public ShowConnectingMessage()V
    .locals 3

    .prologue
    .line 98
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;->mParent:Landroid/content/Context;

    const-string v1, "Connecting to VPN Service. Please Wait..."

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 99
    return-void
.end method
