.class Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$3;
.super Ljava/lang/Object;
.source "ServiceConnectionManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;


# direct methods
.method constructor <init>(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$3;->this$0:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 99
    monitor-enter p0

    .line 101
    :try_start_0
    iget-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$3;->this$0:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    invoke-static {p2}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    move-result-object v2

    # setter for: Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mVpnService:Lcom/cisco/anyconnect/vpn/android/service/IVpnService;
    invoke-static {v1, v2}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->access$502(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;Lcom/cisco/anyconnect/vpn/android/service/IVpnService;)Lcom/cisco/anyconnect/vpn/android/service/IVpnService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    :try_start_1
    iget-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$3;->this$0:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    # getter for: Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mVpnService:Lcom/cisco/anyconnect/vpn/android/service/IVpnService;
    invoke-static {v1}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->access$500(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    move-result-object v1

    iget-object v2, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$3;->this$0:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    # getter for: Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mServiceStateListener:Lcom/cisco/anyconnect/vpn/android/service/IServiceStateListener;
    invoke-static {v2}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->access$800(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)Lcom/cisco/anyconnect/vpn/android/service/IServiceStateListener;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService;->RegisterServiceStateListener(Lcom/cisco/anyconnect/vpn/android/service/IServiceStateListener;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 108
    const-string v1, "AnyConnect"

    const-string v2, "failed to register the service state listener, vpn service is probably shutting down"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 115
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit p0

    .line 116
    return-void

    .line 111
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "AnyConnect"

    const-string v2, "Unexpected RemoteException in registering RegisterServiceShutdownListener"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 115
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 122
    monitor-enter p0

    .line 124
    :try_start_0
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$3;->this$0:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    const/4 v1, 0x0

    # setter for: Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mVpnService:Lcom/cisco/anyconnect/vpn/android/service/IVpnService;
    invoke-static {v0, v1}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->access$502(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;Lcom/cisco/anyconnect/vpn/android/service/IVpnService;)Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    .line 126
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$3;->this$0:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    # getter for: Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mCallback:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;
    invoke-static {v0}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->access$400(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$3;->this$0:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    # invokes: Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->onServiceDisconnectedCB()V
    invoke-static {v0}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->access$900(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)V

    .line 130
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$3;->this$0:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    # getter for: Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mIsVisible:Z
    invoke-static {v0}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->access$1000(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$3;->this$0:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    # invokes: Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->showConnectingMessageCB()V
    invoke-static {v0}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->access$1100(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)V

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$3;->this$0:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    # invokes: Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->stopTimer()V
    invoke-static {v0}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->access$300(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)V

    .line 136
    monitor-exit p0

    .line 137
    return-void

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
