.class public Lcom/cisco/anyconnect/vpn/jni/PreferenceInfo;
.super Ljava/lang/Object;
.source "PreferenceInfo.java"


# instance fields
.field public heading:Ljava/lang/String;

.field public prefs:[Lcom/cisco/anyconnect/vpn/jni/Preference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 11
    const-string v4, ""

    .line 12
    .local v4, "s":Ljava/lang/String;
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/jni/PreferenceInfo;->prefs:[Lcom/cisco/anyconnect/vpn/jni/Preference;

    .local v0, "arr$":[Lcom/cisco/anyconnect/vpn/jni/Preference;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 14
    .local v3, "pref":Lcom/cisco/anyconnect/vpn/jni/Preference;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v3, Lcom/cisco/anyconnect/vpn/jni/Preference;->id:Lcom/cisco/anyconnect/vpn/jni/Preference$PreferenceId;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v3, Lcom/cisco/anyconnect/vpn/jni/Preference;->promptEntry:Lcom/cisco/anyconnect/vpn/jni/PromptEntry;

    iget-object v6, v6, Lcom/cisco/anyconnect/vpn/jni/PromptEntry;->value:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 12
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 17
    .end local v3    # "pref":Lcom/cisco/anyconnect/vpn/jni/Preference;
    :cond_0
    return-object v4
.end method
