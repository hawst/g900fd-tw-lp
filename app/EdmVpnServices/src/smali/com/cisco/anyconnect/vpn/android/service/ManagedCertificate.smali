.class public Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;
.super Lcom/cisco/anyconnect/vpn/jni/ManagedCertInfo;
.source "ManagedCertificate.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCert:Ljava/security/cert/X509Certificate;

.field private mSubjectShortName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 268
    new-instance v0, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate$1;

    invoke-direct {v0}, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate$1;-><init>()V

    sput-object v0, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/cisco/anyconnect/vpn/jni/ManagedCertInfo;-><init>()V

    .line 84
    invoke-direct {p0, p1}, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;->readFromParcel(Landroid/os/Parcel;)V

    .line 85
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate$1;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private derToX509Certificate([B)Ljava/security/cert/X509Certificate;
    .locals 8
    .param p1, "derBlob"    # [B

    .prologue
    .line 117
    :try_start_0
    const-string v4, "X509"

    invoke-static {v4}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v1

    .line 118
    .local v1, "certFactory":Ljava/security/cert/CertificateFactory;
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 119
    .local v3, "inStream":Ljava/io/ByteArrayInputStream;
    invoke-virtual {v1, v3}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    .line 120
    .local v0, "cert":Ljava/security/cert/X509Certificate;
    invoke-virtual {v3}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 132
    .end local v0    # "cert":Ljava/security/cert/X509Certificate;
    .end local v1    # "certFactory":Ljava/security/cert/CertificateFactory;
    .end local v3    # "inStream":Ljava/io/ByteArrayInputStream;
    :goto_0
    return-object v0

    .line 123
    :catch_0
    move-exception v2

    .line 125
    .local v2, "e":Ljava/security/cert/CertificateException;
    sget-object v4, Lcom/cisco/anyconnect/vpn/android/util/AppLog$Severity;->DBG_ERROR:Lcom/cisco/anyconnect/vpn/android/util/AppLog$Severity;

    const-string v5, "ManagedCertificate"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "derToX509Certificate: CertificateException while parsing certificate: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/cisco/anyconnect/vpn/android/util/AppLog;->logDebugMessage(Lcom/cisco/anyconnect/vpn/android/util/AppLog$Severity;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    .end local v2    # "e":Ljava/security/cert/CertificateException;
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 127
    :catch_1
    move-exception v2

    .line 129
    .local v2, "e":Ljava/io/IOException;
    sget-object v4, Lcom/cisco/anyconnect/vpn/android/util/AppLog$Severity;->DBG_ERROR:Lcom/cisco/anyconnect/vpn/android/util/AppLog$Severity;

    const-string v5, "ManagedCertificate"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "derToX509Certificate: IOException while parsing certificate: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/cisco/anyconnect/vpn/android/util/AppLog;->logDebugMessage(Lcom/cisco/anyconnect/vpn/android/util/AppLog$Severity;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 94
    iget-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;->derBlob:[B

    invoke-direct {p0, v1}, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;->derToX509Certificate([B)Ljava/security/cert/X509Certificate;

    move-result-object v1

    iput-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;->mCert:Ljava/security/cert/X509Certificate;

    .line 96
    iget-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;->mCert:Ljava/security/cert/X509Certificate;

    if-eqz v1, :cond_0

    .line 98
    new-instance v0, Lcom/cisco/anyconnect/common/X509NameParser;

    iget-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;->mCert:Ljava/security/cert/X509Certificate;

    invoke-virtual {v1}, Ljava/security/cert/X509Certificate;->getSubjectDN()Ljava/security/Principal;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/cisco/anyconnect/common/X509NameParser;-><init>(Ljava/security/Principal;)V

    .line 99
    .local v0, "parser":Lcom/cisco/anyconnect/common/X509NameParser;
    invoke-virtual {v0}, Lcom/cisco/anyconnect/common/X509NameParser;->getShortName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;->mSubjectShortName:Ljava/lang/String;

    .line 101
    .end local v0    # "parser":Lcom/cisco/anyconnect/common/X509NameParser;
    :cond_0
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 234
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;->derBlob:[B

    .line 235
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;->derBlob:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    .line 236
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;->id:Ljava/lang/String;

    .line 237
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;->group:Ljava/lang/String;

    .line 238
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;->certType:I

    .line 239
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;->certProperty:I

    .line 241
    invoke-direct {p0}, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;->initialize()V

    .line 242
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 250
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 259
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;->derBlob:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 260
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;->derBlob:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 261
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 262
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;->group:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 263
    iget v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;->certType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 264
    iget v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ManagedCertificate;->certProperty:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 265
    return-void
.end method
