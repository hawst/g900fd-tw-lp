.class public Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;
.super Lcom/cisco/anyconnect/vpn/jni/ConnectPromptInfo;
.source "ConnectPromptInfoParcel.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    new-instance v0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel$1;

    invoke-direct {v0}, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel$1;-><init>()V

    sput-object v0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/cisco/anyconnect/vpn/jni/ConnectPromptInfo;-><init>()V

    .line 30
    invoke-direct {p0, p1}, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->readFromParcel(Landroid/os/Parcel;)V

    .line 31
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel$1;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 7
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 35
    invoke-static {}, Lcom/cisco/anyconnect/vpn/jni/ConnectPromptInfo$ConnectPromptType;->values()[Lcom/cisco/anyconnect/vpn/jni/ConnectPromptInfo$ConnectPromptType;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    aget-object v3, v3, v6

    iput-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->type:Lcom/cisco/anyconnect/vpn/jni/ConnectPromptInfo$ConnectPromptType;

    .line 36
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->message:Ljava/lang/String;

    .line 37
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->submitButtonName:Ljava/lang/String;

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->cancelButtonLabel:Ljava/lang/String;

    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->neutralButtonName:Ljava/lang/String;

    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->neutralButtonLabel:Ljava/lang/String;

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-ne v3, v4, :cond_0

    move v3, v4

    :goto_0
    iput-boolean v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->hasEnrollmentCA:Z

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-ne v3, v4, :cond_1

    move v3, v4

    :goto_1
    iput-boolean v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->useEnrollmentCA:Z

    .line 43
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-ne v3, v4, :cond_2

    :goto_2
    iput-boolean v4, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->isCancelled:Z

    .line 45
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 46
    .local v1, "nPromptEntries":I
    new-array v3, v1, [Lcom/cisco/anyconnect/vpn/jni/PromptEntry;

    iput-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->entries:[Lcom/cisco/anyconnect/vpn/jni/PromptEntry;

    .line 47
    if-lez v1, :cond_3

    .line 49
    const-class v3, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readArray(Ljava/lang/ClassLoader;)[Ljava/lang/Object;

    move-result-object v2

    .line 50
    .local v2, "objs":[Ljava/lang/Object;
    new-array v3, v1, [Lcom/cisco/anyconnect/vpn/jni/PromptEntry;

    iput-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->entries:[Lcom/cisco/anyconnect/vpn/jni/PromptEntry;

    .line 51
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_3
    if-ge v0, v1, :cond_3

    .line 53
    iget-object v4, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->entries:[Lcom/cisco/anyconnect/vpn/jni/PromptEntry;

    aget-object v3, v2, v0

    check-cast v3, Lcom/cisco/anyconnect/vpn/jni/PromptEntry;

    aput-object v3, v4, v0

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .end local v0    # "i":I
    .end local v1    # "nPromptEntries":I
    .end local v2    # "objs":[Ljava/lang/Object;
    :cond_0
    move v3, v5

    .line 41
    goto :goto_0

    :cond_1
    move v3, v5

    .line 42
    goto :goto_1

    :cond_2
    move v4, v5

    .line 43
    goto :goto_2

    .line 56
    .restart local v1    # "nPromptEntries":I
    :cond_3
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 96
    iget-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->type:Lcom/cisco/anyconnect/vpn/jni/ConnectPromptInfo$ConnectPromptType;

    invoke-virtual {v3}, Lcom/cisco/anyconnect/vpn/jni/ConnectPromptInfo$ConnectPromptType;->ordinal()I

    move-result v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 97
    iget-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->message:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 98
    iget-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->submitButtonName:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 99
    iget-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->cancelButtonLabel:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 100
    iget-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->neutralButtonName:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 101
    iget-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->neutralButtonLabel:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 102
    iget-boolean v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->hasEnrollmentCA:Z

    if-eqz v3, :cond_0

    move v3, v4

    :goto_0
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 103
    iget-boolean v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->useEnrollmentCA:Z

    if-eqz v3, :cond_1

    move v3, v4

    :goto_1
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 104
    iget-boolean v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->isCancelled:Z

    if-eqz v3, :cond_2

    :goto_2
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 105
    iget-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->entries:[Lcom/cisco/anyconnect/vpn/jni/PromptEntry;

    if-eqz v3, :cond_4

    .line 107
    iget-object v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->entries:[Lcom/cisco/anyconnect/vpn/jni/PromptEntry;

    array-length v1, v3

    .line 108
    .local v1, "nEntries":I
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 109
    new-array v2, v1, [Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;

    .line 110
    .local v2, "promptEntryParcel":[Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_3
    if-ge v0, v1, :cond_3

    .line 112
    new-instance v3, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;

    iget-object v4, p0, Lcom/cisco/anyconnect/vpn/android/service/ConnectPromptInfoParcel;->entries:[Lcom/cisco/anyconnect/vpn/jni/PromptEntry;

    aget-object v4, v4, v0

    invoke-direct {v3, v4}, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;-><init>(Lcom/cisco/anyconnect/vpn/jni/PromptEntry;)V

    aput-object v3, v2, v0

    .line 110
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .end local v0    # "i":I
    .end local v1    # "nEntries":I
    .end local v2    # "promptEntryParcel":[Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;
    :cond_0
    move v3, v5

    .line 102
    goto :goto_0

    :cond_1
    move v3, v5

    .line 103
    goto :goto_1

    :cond_2
    move v4, v5

    .line 104
    goto :goto_2

    .line 114
    .restart local v0    # "i":I
    .restart local v1    # "nEntries":I
    .restart local v2    # "promptEntryParcel":[Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;
    :cond_3
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeArray([Ljava/lang/Object;)V

    .line 120
    .end local v0    # "i":I
    .end local v1    # "nEntries":I
    .end local v2    # "promptEntryParcel":[Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;
    :goto_4
    return-void

    .line 118
    :cond_4
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_4
.end method
