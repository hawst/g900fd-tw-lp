.class public Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;
.super Ljava/lang/Object;
.source "VpnConnection.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cisco/anyconnect/vpn/android/service/VpnConnection$FipsMode;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mCertAuthMode:Lcom/cisco/anyconnect/vpn/jni/CertAuthMode;

.field private mCertCommonName:Ljava/lang/String;

.field private mCertHash:[B

.field private mConnectProtocolType:Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

.field private mFipsMode:Lcom/cisco/anyconnect/vpn/android/service/VpnConnection$FipsMode;

.field private mHost:Ljava/lang/String;

.field private mIKEIdentity:Ljava/lang/String;

.field private mIPsecAuthMode:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

.field private mId:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mProfileName:Ljava/lang/String;

.field private mType:Lcom/cisco/anyconnect/vpn/android/service/ConnectionType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 122
    new-instance v0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection$1;

    invoke-direct {v0}, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection$1;-><init>()V

    sput-object v0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    sget-object v0, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_EAP_ANYCONNECT:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mIPsecAuthMode:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    .line 73
    const-string v0, ""

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mIKEIdentity:Ljava/lang/String;

    .line 74
    sget-object v0, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;->Ssl:Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mConnectProtocolType:Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    .line 75
    sget-object v0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection$FipsMode;->Default:Lcom/cisco/anyconnect/vpn/android/service/VpnConnection$FipsMode;

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mFipsMode:Lcom/cisco/anyconnect/vpn/android/service/VpnConnection$FipsMode;

    .line 189
    invoke-direct {p0}, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->initDefaults()V

    .line 190
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    sget-object v0, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->USER_AUTH_IKE_EAP_ANYCONNECT:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mIPsecAuthMode:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    .line 73
    const-string v0, ""

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mIKEIdentity:Ljava/lang/String;

    .line 74
    sget-object v0, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;->Ssl:Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mConnectProtocolType:Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    .line 75
    sget-object v0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection$FipsMode;->Default:Lcom/cisco/anyconnect/vpn/android/service/VpnConnection$FipsMode;

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mFipsMode:Lcom/cisco/anyconnect/vpn/android/service/VpnConnection$FipsMode;

    .line 79
    invoke-direct {p0, p1}, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->readFromParcel(Landroid/os/Parcel;)V

    .line 80
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/cisco/anyconnect/vpn/android/service/VpnConnection$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/cisco/anyconnect/vpn/android/service/VpnConnection$1;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private initDefaults()V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mCertAuthMode:Lcom/cisco/anyconnect/vpn/jni/CertAuthMode;

    if-nez v0, :cond_0

    .line 170
    sget-object v0, Lcom/cisco/anyconnect/vpn/jni/CertAuthMode;->Automatic:Lcom/cisco/anyconnect/vpn/jni/CertAuthMode;

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mCertAuthMode:Lcom/cisco/anyconnect/vpn/jni/CertAuthMode;

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mType:Lcom/cisco/anyconnect/vpn/android/service/ConnectionType;

    if-nez v0, :cond_1

    .line 175
    sget-object v0, Lcom/cisco/anyconnect/vpn/android/service/ConnectionType;->Manual:Lcom/cisco/anyconnect/vpn/android/service/ConnectionType;

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mType:Lcom/cisco/anyconnect/vpn/android/service/ConnectionType;

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mConnectProtocolType:Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    if-nez v0, :cond_2

    .line 180
    sget-object v0, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;->Ssl:Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mConnectProtocolType:Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    .line 182
    :cond_2
    return-void
.end method

.method private declared-synchronized readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 89
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mId:Ljava/lang/String;

    .line 90
    invoke-static {}, Lcom/cisco/anyconnect/vpn/android/service/ConnectionType;->values()[Lcom/cisco/anyconnect/vpn/android/service/ConnectionType;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mType:Lcom/cisco/anyconnect/vpn/android/service/ConnectionType;

    .line 91
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mName:Ljava/lang/String;

    .line 92
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mHost:Ljava/lang/String;

    .line 93
    invoke-static {}, Lcom/cisco/anyconnect/vpn/jni/CertAuthMode;->values()[Lcom/cisco/anyconnect/vpn/jni/CertAuthMode;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mCertAuthMode:Lcom/cisco/anyconnect/vpn/jni/CertAuthMode;

    .line 94
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mCertCommonName:Ljava/lang/String;

    .line 95
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mProfileName:Ljava/lang/String;

    .line 96
    invoke-static {}, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->values()[Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mIPsecAuthMode:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mIKEIdentity:Ljava/lang/String;

    .line 98
    invoke-static {}, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;->values()[Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mConnectProtocolType:Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 101
    .local v0, "certHashLen":I
    if-lez v0, :cond_0

    .line 103
    new-array v1, v0, [B

    iput-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mCertHash:[B

    .line 104
    iget-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mCertHash:[B

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readByteArray([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    :goto_0
    monitor-exit p0

    return-void

    .line 108
    :cond_0
    const/4 v1, 0x0

    :try_start_1
    iput-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mCertHash:[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 89
    .end local v0    # "certHashLen":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public declared-synchronized GetCertAuthMode()Lcom/cisco/anyconnect/vpn/jni/CertAuthMode;
    .locals 1

    .prologue
    .line 245
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mCertAuthMode:Lcom/cisco/anyconnect/vpn/jni/CertAuthMode;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized GetCertCommonName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 440
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mCertCommonName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized GetCertHash()[B
    .locals 1

    .prologue
    .line 254
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mCertHash:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized GetHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 263
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mHost:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized GetName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 272
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized SetCertAuthMode(Lcom/cisco/anyconnect/vpn/jni/CertAuthMode;)V
    .locals 1
    .param p1, "mode"    # Lcom/cisco/anyconnect/vpn/jni/CertAuthMode;

    .prologue
    .line 298
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mCertAuthMode:Lcom/cisco/anyconnect/vpn/jni/CertAuthMode;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299
    monitor-exit p0

    return-void

    .line 298
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized SetCertCommonName(Ljava/lang/String;)V
    .locals 1
    .param p1, "commonName"    # Ljava/lang/String;

    .prologue
    .line 451
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mCertCommonName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 452
    monitor-exit p0

    return-void

    .line 451
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized SetCertHash([B)V
    .locals 1
    .param p1, "certHash"    # [B

    .prologue
    .line 319
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mCertHash:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 320
    monitor-exit p0

    return-void

    .line 319
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized SetConnectProtocolType(Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;)V
    .locals 1
    .param p1, "mode"    # Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    .prologue
    .line 604
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mConnectProtocolType:Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 605
    monitor-exit p0

    return-void

    .line 604
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized SetHost(Ljava/lang/String;)V
    .locals 2
    .param p1, "host"    # Ljava/lang/String;

    .prologue
    .line 362
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/String;

    const-string v1, "ipsec://"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 363
    .local v0, "ipsecProtocol":Ljava/lang/String;
    if-eqz p1, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 365
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 366
    sget-object v1, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;->Ipsec:Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    invoke-virtual {p0, v1}, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->SetConnectProtocolType(Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;)V

    .line 369
    :cond_0
    iput-object p1, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mHost:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 370
    monitor-exit p0

    return-void

    .line 362
    .end local v0    # "ipsecProtocol":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized SetName(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 380
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381
    monitor-exit p0

    return-void

    .line 380
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized describeContents()I
    .locals 1

    .prologue
    .line 136
    monitor-enter p0

    const/4 v0, 0x0

    monitor-exit p0

    return v0
.end method

.method public declared-synchronized writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 141
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mType:Lcom/cisco/anyconnect/vpn/android/service/ConnectionType;

    invoke-virtual {v0}, Lcom/cisco/anyconnect/vpn/android/service/ConnectionType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 143
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mHost:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mCertAuthMode:Lcom/cisco/anyconnect/vpn/jni/CertAuthMode;

    invoke-virtual {v0}, Lcom/cisco/anyconnect/vpn/jni/CertAuthMode;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 146
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mCertCommonName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mProfileName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mIPsecAuthMode:Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;

    invoke-virtual {v0}, Lcom/cisco/anyconnect/vpn/jni/IPsecAuthMode;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 149
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mIKEIdentity:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mConnectProtocolType:Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;

    invoke-virtual {v0}, Lcom/cisco/anyconnect/vpn/jni/ConnectProtocolType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 152
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mCertHash:[B

    if-nez v0, :cond_0

    .line 154
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    :goto_0
    monitor-exit p0

    return-void

    .line 158
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mCertHash:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 159
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->mCertHash:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 141
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
