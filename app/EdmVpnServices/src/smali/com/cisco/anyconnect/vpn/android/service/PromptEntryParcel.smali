.class public Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;
.super Lcom/cisco/anyconnect/vpn/jni/PromptEntry;
.source "PromptEntryParcel.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/cisco/anyconnect/vpn/jni/PromptEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 119
    new-instance v0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel$1;

    invoke-direct {v0}, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel$1;-><init>()V

    sput-object v0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/cisco/anyconnect/vpn/jni/PromptEntry;-><init>()V

    .line 29
    invoke-direct {p0, p1}, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->readFromParcel(Landroid/os/Parcel;)V

    .line 30
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel$1;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/cisco/anyconnect/vpn/jni/PromptEntry;)V
    .locals 1
    .param p1, "promptEntry"    # Lcom/cisco/anyconnect/vpn/jni/PromptEntry;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/cisco/anyconnect/vpn/jni/PromptEntry;-><init>()V

    .line 64
    if-eqz p1, :cond_0

    .line 66
    iget-object v0, p1, Lcom/cisco/anyconnect/vpn/jni/PromptEntry;->label:Ljava/lang/String;

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->label:Ljava/lang/String;

    .line 67
    iget-object v0, p1, Lcom/cisco/anyconnect/vpn/jni/PromptEntry;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->name:Ljava/lang/String;

    .line 68
    iget-object v0, p1, Lcom/cisco/anyconnect/vpn/jni/PromptEntry;->value:Ljava/lang/String;

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->value:Ljava/lang/String;

    .line 69
    iget-object v0, p1, Lcom/cisco/anyconnect/vpn/jni/PromptEntry;->type:Lcom/cisco/anyconnect/vpn/jni/PromptEntry$PromptType;

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->type:Lcom/cisco/anyconnect/vpn/jni/PromptEntry$PromptType;

    .line 70
    iget-boolean v0, p1, Lcom/cisco/anyconnect/vpn/jni/PromptEntry;->isEntryGroup:Z

    iput-boolean v0, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->isEntryGroup:Z

    .line 71
    iget-boolean v0, p1, Lcom/cisco/anyconnect/vpn/jni/PromptEntry;->isEnabled:Z

    iput-boolean v0, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->isEnabled:Z

    .line 72
    iget-boolean v0, p1, Lcom/cisco/anyconnect/vpn/jni/PromptEntry;->isVisible:Z

    iput-boolean v0, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->isVisible:Z

    .line 73
    iget-object v0, p1, Lcom/cisco/anyconnect/vpn/jni/PromptEntry;->comboValues:[Ljava/lang/String;

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->comboValues:[Ljava/lang/String;

    .line 74
    iget-object v0, p1, Lcom/cisco/anyconnect/vpn/jni/PromptEntry;->comboKeys:[Ljava/lang/String;

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->comboKeys:[Ljava/lang/String;

    .line 76
    :cond_0
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->label:Ljava/lang/String;

    .line 35
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->name:Ljava/lang/String;

    .line 36
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->value:Ljava/lang/String;

    .line 37
    invoke-static {}, Lcom/cisco/anyconnect/vpn/jni/PromptEntry$PromptType;->values()[Lcom/cisco/anyconnect/vpn/jni/PromptEntry$PromptType;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    aget-object v2, v2, v5

    iput-object v2, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->type:Lcom/cisco/anyconnect/vpn/jni/PromptEntry$PromptType;

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_0

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->isEntryGroup:Z

    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_1

    move v2, v3

    :goto_1
    iput-boolean v2, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->isEnabled:Z

    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_2

    :goto_2
    iput-boolean v3, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->isVisible:Z

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 42
    .local v0, "comboLength":I
    if-ltz v0, :cond_3

    .line 44
    new-array v2, v0, [Ljava/lang/String;

    iput-object v2, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->comboValues:[Ljava/lang/String;

    .line 45
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    if-ge v1, v0, :cond_3

    .line 47
    iget-object v2, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->comboValues:[Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 45
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .end local v0    # "comboLength":I
    .end local v1    # "i":I
    :cond_0
    move v2, v4

    .line 38
    goto :goto_0

    :cond_1
    move v2, v4

    .line 39
    goto :goto_1

    :cond_2
    move v3, v4

    .line 40
    goto :goto_2

    .line 51
    .restart local v0    # "comboLength":I
    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 52
    if-ltz v0, :cond_4

    .line 54
    new-array v2, v0, [Ljava/lang/String;

    iput-object v2, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->comboKeys:[Ljava/lang/String;

    .line 55
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    if-ge v1, v0, :cond_4

    .line 57
    iget-object v2, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->comboKeys:[Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 55
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 60
    .end local v1    # "i":I
    :cond_4
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 7
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 84
    iget-object v4, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->label:Ljava/lang/String;

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 85
    iget-object v4, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->name:Ljava/lang/String;

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 86
    iget-object v4, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->value:Ljava/lang/String;

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 87
    iget-object v4, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->type:Lcom/cisco/anyconnect/vpn/jni/PromptEntry$PromptType;

    invoke-virtual {v4}, Lcom/cisco/anyconnect/vpn/jni/PromptEntry$PromptType;->ordinal()I

    move-result v4

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 88
    iget-boolean v4, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->isEntryGroup:Z

    if-eqz v4, :cond_2

    move v4, v5

    :goto_0
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 89
    iget-boolean v4, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->isEnabled:Z

    if-eqz v4, :cond_3

    move v4, v5

    :goto_1
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 90
    iget-boolean v4, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->isVisible:Z

    if-eqz v4, :cond_4

    :goto_2
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 91
    iget-object v4, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->comboValues:[Ljava/lang/String;

    if-nez v4, :cond_5

    .line 93
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 104
    :cond_0
    iget-object v4, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->comboKeys:[Ljava/lang/String;

    if-nez v4, :cond_6

    .line 106
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 116
    :cond_1
    return-void

    :cond_2
    move v4, v6

    .line 88
    goto :goto_0

    :cond_3
    move v4, v6

    .line 89
    goto :goto_1

    :cond_4
    move v5, v6

    .line 90
    goto :goto_2

    .line 97
    :cond_5
    iget-object v4, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->comboValues:[Ljava/lang/String;

    array-length v4, v4

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 98
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->comboValues:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_3
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 100
    .local v3, "s":Ljava/lang/String;
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 98
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 110
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "s":Ljava/lang/String;
    :cond_6
    iget-object v4, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->comboKeys:[Ljava/lang/String;

    array-length v4, v4

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 111
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/PromptEntryParcel;->comboKeys:[Ljava/lang/String;

    .restart local v0    # "arr$":[Ljava/lang/String;
    array-length v2, v0

    .restart local v2    # "len$":I
    const/4 v1, 0x0

    .restart local v1    # "i$":I
    :goto_4
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 113
    .restart local v3    # "s":Ljava/lang/String;
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 111
    add-int/lit8 v1, v1, 0x1

    goto :goto_4
.end method
