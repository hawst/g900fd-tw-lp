.class public Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;
.super Ljava/lang/Object;
.source "ServiceConnectionManager.java"


# instance fields
.field private mCallback:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;

.field private mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mIsActivated:Z

.field private mIsVisible:Z

.field private mServiceConnection:Landroid/content/ServiceConnection;

.field private mServiceStateListener:Lcom/cisco/anyconnect/vpn/android/service/IServiceStateListener;

.field private mSuppressConnectMsg:Z

.field private mVpnService:Lcom/cisco/anyconnect/vpn/android/service/IVpnService;


# direct methods
.method public constructor <init>(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;)V
    .locals 1
    .param p1, "cb"    # Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;

    .prologue
    .line 217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mSuppressConnectMsg:Z

    .line 53
    new-instance v0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$1;

    invoke-direct {v0, p0}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$1;-><init>(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)V

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mServiceStateListener:Lcom/cisco/anyconnect/vpn/android/service/IServiceStateListener;

    .line 82
    new-instance v0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$2;

    invoke-direct {v0, p0}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$2;-><init>(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)V

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mHandler:Landroid/os/Handler;

    .line 94
    new-instance v0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$3;

    invoke-direct {v0, p0}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager$3;-><init>(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)V

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 218
    iput-object p1, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mCallback:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;

    .line 219
    invoke-virtual {p1}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;->GetContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mContext:Landroid/content/Context;

    .line 220
    return-void
.end method

.method static synthetic access$000(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;ZLjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->deactivate(ZLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mIsVisible:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->showConnectingMessageCB()V

    return-void
.end method

.method static synthetic access$202(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mSuppressConnectMsg:Z

    return p1
.end method

.method static synthetic access$300(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->stopTimer()V

    return-void
.end method

.method static synthetic access$400(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;
    .locals 1
    .param p0, "x0"    # Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mCallback:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;

    return-object v0
.end method

.method static synthetic access$500(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)Lcom/cisco/anyconnect/vpn/android/service/IVpnService;
    .locals 1
    .param p0, "x0"    # Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mVpnService:Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    return-object v0
.end method

.method static synthetic access$502(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;Lcom/cisco/anyconnect/vpn/android/service/IVpnService;)Lcom/cisco/anyconnect/vpn/android/service/IVpnService;
    .locals 0
    .param p0, "x0"    # Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;
    .param p1, "x1"    # Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mVpnService:Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    return-object p1
.end method

.method static synthetic access$600(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;Lcom/cisco/anyconnect/vpn/android/service/IVpnService;)V
    .locals 0
    .param p0, "x0"    # Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;
    .param p1, "x1"    # Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->onServiceConnectedCB(Lcom/cisco/anyconnect/vpn/android/service/IVpnService;)V

    return-void
.end method

.method static synthetic access$700(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->onBindTimeoutCB()V

    return-void
.end method

.method static synthetic access$800(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)Lcom/cisco/anyconnect/vpn/android/service/IServiceStateListener;
    .locals 1
    .param p0, "x0"    # Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mServiceStateListener:Lcom/cisco/anyconnect/vpn/android/service/IServiceStateListener;

    return-object v0
.end method

.method static synthetic access$900(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->onServiceDisconnectedCB()V

    return-void
.end method

.method private deactivate(ZLjava/lang/String;)V
    .locals 3
    .param p1, "isForcedShutdown"    # Z
    .param p2, "shutdownMsg"    # Ljava/lang/String;

    .prologue
    .line 148
    iget-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mVpnService:Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    if-eqz v1, :cond_0

    .line 153
    :try_start_0
    iget-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mVpnService:Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    iget-object v2, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mServiceStateListener:Lcom/cisco/anyconnect/vpn/android/service/IServiceStateListener;

    invoke-interface {v1, v2}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService;->UnregisterServiceStateListener(Lcom/cisco/anyconnect/vpn/android/service/IServiceStateListener;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    :goto_0
    iget-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mVpnService:Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    invoke-direct {p0, v1, p1, p2}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->onServiceWillDisconnectCB(Lcom/cisco/anyconnect/vpn/android/service/IVpnService;ZLjava/lang/String;)V

    .line 163
    iget-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 166
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mVpnService:Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    .line 167
    invoke-direct {p0}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->stopTimer()V

    .line 168
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mIsActivated:Z

    .line 169
    return-void

    .line 155
    :catch_0
    move-exception v0

    .line 157
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "AnyConnect"

    const-string v2, "Unexpected RemoteException in unregistering RegisterServiceShutdownListener"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private onBindTimeoutCB()V
    .locals 3

    .prologue
    .line 396
    :try_start_0
    iget-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mCallback:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;

    invoke-virtual {v1}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;->OnBindTimeout()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 402
    :goto_0
    return-void

    .line 398
    :catch_0
    move-exception v0

    .line 400
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "AnyConnect"

    const-string v2, "Exception in OnBindTimeout"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private onServiceConnectedCB(Lcom/cisco/anyconnect/vpn/android/service/IVpnService;)V
    .locals 3
    .param p1, "vpnService"    # Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    .prologue
    .line 384
    :try_start_0
    iget-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mCallback:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;

    invoke-virtual {v1, p1}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;->OnServiceConnected(Lcom/cisco/anyconnect/vpn/android/service/IVpnService;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 390
    :goto_0
    return-void

    .line 386
    :catch_0
    move-exception v0

    .line 388
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "AnyConnect"

    const-string v2, "Exception in OnServiceConnected"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private onServiceDisconnectedCB()V
    .locals 3

    .prologue
    .line 360
    :try_start_0
    iget-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mCallback:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;

    invoke-virtual {v1}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;->OnServiceDisconnected()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 366
    :goto_0
    return-void

    .line 362
    :catch_0
    move-exception v0

    .line 364
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "AnyConnect"

    const-string v2, "Exception in OnServiceDisconnected"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private onServiceWillDisconnectCB(Lcom/cisco/anyconnect/vpn/android/service/IVpnService;ZLjava/lang/String;)V
    .locals 3
    .param p1, "vpnService"    # Lcom/cisco/anyconnect/vpn/android/service/IVpnService;
    .param p2, "isForcedShutdown"    # Z
    .param p3, "shutdownMsg"    # Ljava/lang/String;

    .prologue
    .line 372
    :try_start_0
    iget-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mCallback:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;

    iget-object v2, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mVpnService:Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    invoke-virtual {v1, v2, p2, p3}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;->OnServiceWillDisconnect(Lcom/cisco/anyconnect/vpn/android/service/IVpnService;ZLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 378
    :goto_0
    return-void

    .line 374
    :catch_0
    move-exception v0

    .line 376
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "AnyConnect"

    const-string v2, "Exception in OnServiceWillDisconnect"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private showConnectingMessageCB()V
    .locals 3

    .prologue
    .line 348
    :try_start_0
    iget-object v1, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mCallback:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;

    invoke-virtual {v1}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;->ShowConnectingMessage()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 354
    :goto_0
    return-void

    .line 350
    :catch_0
    move-exception v0

    .line 352
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "AnyConnect"

    const-string v2, "Exception in ShowConnectingMessage"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private startTimer()V
    .locals 4

    .prologue
    .line 203
    invoke-direct {p0}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->stopTimer()V

    .line 204
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x4e20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 205
    return-void
.end method

.method private stopTimer()V
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 210
    return-void
.end method


# virtual methods
.method public declared-synchronized Activate()Z
    .locals 1

    .prologue
    .line 229
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->Activate(Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized Activate(Z)Z
    .locals 8
    .param p1, "disableNotifications"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 244
    monitor-enter p0

    :try_start_0
    iget-object v5, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mCallback:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;

    if-nez v5, :cond_0

    .line 246
    const-string v4, "AnyConnect"

    const-string v5, "unexpected NULL ServiceConnectionCB"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 282
    :goto_0
    monitor-exit p0

    return v3

    .line 252
    :cond_0
    :try_start_1
    new-instance v1, Landroid/content/Intent;

    const-class v5, Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 253
    .local v1, "startIntent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v1, v6}, Landroid/content/pm/PackageManager;->resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 254
    .local v0, "r":Landroid/content/pm/ResolveInfo;
    if-nez v0, :cond_1

    .line 256
    const-string v4, "AnyConnect"

    const-string v5, "Failed to resolve VpnService intent"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 244
    .end local v0    # "r":Landroid/content/pm/ResolveInfo;
    .end local v1    # "startIntent":Landroid/content/Intent;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 260
    .restart local v0    # "r":Landroid/content/pm/ResolveInfo;
    .restart local v1    # "startIntent":Landroid/content/Intent;
    :cond_1
    :try_start_2
    new-instance v5, Landroid/content/ComponentName;

    iget-object v6, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v6, v6, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iget-object v7, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v7, v7, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 262
    if-eqz p1, :cond_2

    .line 264
    const-string v5, "com.cisco.anyconnect.vpn.android.VPN_SERVICE_KEY_DISABLE_NOTIFICATIONS"

    invoke-virtual {v1, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 267
    :cond_2
    iget-object v5, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v7, 0x1

    invoke-virtual {v5, v1, v6, v7}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v2

    .line 268
    .local v2, "stat":Z
    if-nez v2, :cond_3

    .line 270
    const-string v4, "AnyConnect"

    const-string v5, "bindService failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 274
    :cond_3
    iget-boolean v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mIsActivated:Z

    if-eqz v3, :cond_4

    .line 276
    const-string v3, "AnyConnect"

    const-string v5, "Already activated"

    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 277
    goto :goto_0

    .line 280
    :cond_4
    invoke-direct {p0}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->startTimer()V

    .line 281
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mIsActivated:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v3, v4

    .line 282
    goto :goto_0
.end method

.method public declared-synchronized Deactivate()V
    .locals 2

    .prologue
    .line 290
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    const-string v1, ""

    invoke-direct {p0, v0, v1}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->deactivate(ZLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291
    monitor-exit p0

    return-void

    .line 290
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized GetService()Lcom/cisco/anyconnect/vpn/android/service/IVpnService;
    .locals 1

    .prologue
    .line 332
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->mVpnService:Lcom/cisco/anyconnect/vpn/android/service/IVpnService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
