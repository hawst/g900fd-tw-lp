.class Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$1;
.super Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;
.source "EnterpriseVpnAnyconnect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;Landroid/content/Context;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$1;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    invoke-direct {p0, p2}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public OnBindTimeout()V
    .locals 5

    .prologue
    .line 149
    const-string v2, "EnterpriseVpnAnyconnect"

    const-string v3, "OnBindTimeout."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    iget-object v2, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$1;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    # getter for: Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mBindingLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->access$100(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 153
    :try_start_0
    iget-object v2, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$1;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    # setter for: Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->isBinding:Ljava/lang/Boolean;
    invoke-static {v2, v4}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->access$202(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 154
    iget-object v2, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$1;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    iget-object v2, v2, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mReadyMessenger:Landroid/os/Messenger;

    if-eqz v2, :cond_0

    .line 155
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 156
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, -0x1

    iput v2, v1, Landroid/os/Message;->what:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 158
    :try_start_1
    iget-object v2, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$1;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    iget-object v2, v2, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mReadyMessenger:Landroid/os/Messenger;

    invoke-virtual {v2, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 162
    :goto_0
    :try_start_2
    iget-object v2, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$1;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    const/4 v4, 0x0

    iput-object v4, v2, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mReadyMessenger:Landroid/os/Messenger;

    .line 164
    .end local v1    # "msg":Landroid/os/Message;
    :cond_0
    monitor-exit v3

    .line 165
    return-void

    .line 159
    .restart local v1    # "msg":Landroid/os/Message;
    :catch_0
    move-exception v0

    .line 160
    .local v0, "ex":Landroid/os/RemoteException;
    const-string v2, "EnterpriseVpnAnyconnect"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 164
    .end local v0    # "ex":Landroid/os/RemoteException;
    .end local v1    # "msg":Landroid/os/Message;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method public OnServiceConnected(Lcom/cisco/anyconnect/vpn/android/service/IVpnService;)V
    .locals 7
    .param p1, "vpnService"    # Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    .prologue
    .line 89
    const-string v4, "EnterpriseVpnAnyconnect"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Service Anyconnect got connected! User Id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$1;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    # getter for: Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mMyUserId:I
    invoke-static {v6}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->access$000(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    iget-object v4, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$1;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    # getter for: Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mBindingLock:Ljava/lang/Object;
    invoke-static {v4}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->access$100(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 94
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$1;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    # setter for: Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->isBinding:Ljava/lang/Boolean;
    invoke-static {v4, v6}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->access$202(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 95
    iget-object v4, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$1;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    iget-object v4, v4, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mReadyMessenger:Landroid/os/Messenger;

    if-eqz v4, :cond_0

    .line 96
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    .line 97
    .local v3, "msg":Landroid/os/Message;
    const/4 v4, 0x0

    iput v4, v3, Landroid/os/Message;->what:I

    .line 99
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 100
    .local v2, "lData":Landroid/os/Bundle;
    const-string v4, "user_id"

    iget-object v6, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$1;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    # getter for: Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mMyUserId:I
    invoke-static {v6}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->access$000(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;)I

    move-result v6

    invoke-virtual {v2, v4, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 101
    invoke-virtual {v3, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    :try_start_1
    iget-object v4, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$1;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    iget-object v4, v4, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mReadyMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 108
    :goto_0
    :try_start_2
    iget-object v4, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$1;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    const/4 v6, 0x0

    iput-object v6, v4, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mReadyMessenger:Landroid/os/Messenger;

    .line 110
    .end local v2    # "lData":Landroid/os/Bundle;
    .end local v3    # "msg":Landroid/os/Message;
    :cond_0
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 112
    iget-object v4, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$1;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    # invokes: Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->verifyApiVersion(Lcom/cisco/anyconnect/vpn/android/service/IVpnService;)V
    invoke-static {v4, p1}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->access$300(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;Lcom/cisco/anyconnect/vpn/android/service/IVpnService;)V

    .line 115
    :try_start_3
    iget-object v4, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$1;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    # getter for: Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mCertHandler:Lcom/cisco/anyconnect/vpn/android/service/ICertificateListener;
    invoke-static {v4}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->access$400(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;)Lcom/cisco/anyconnect/vpn/android/service/ICertificateListener;

    move-result-object v4

    invoke-interface {p1, v4}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService;->RegisterCertificateListener(Lcom/cisco/anyconnect/vpn/android/service/ICertificateListener;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 116
    const-string v4, "EnterpriseVpnAnyconnect"

    const-string v5, "RegisterCertificateListener failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1

    .line 123
    :cond_1
    :goto_1
    return-void

    .line 105
    .restart local v2    # "lData":Landroid/os/Bundle;
    .restart local v3    # "msg":Landroid/os/Message;
    :catch_0
    move-exception v1

    .line 106
    .local v1, "ex":Landroid/os/RemoteException;
    :try_start_4
    const-string v4, "EnterpriseVpnAnyconnect"

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 110
    .end local v1    # "ex":Landroid/os/RemoteException;
    .end local v2    # "lData":Landroid/os/Bundle;
    .end local v3    # "msg":Landroid/os/Message;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v4

    .line 119
    :catch_1
    move-exception v0

    .line 120
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "EnterpriseVpnAnyconnect"

    const-string v5, "RemoteException occurred while registering callback."

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public OnServiceDisconnected()V
    .locals 3

    .prologue
    .line 140
    const-string v0, "EnterpriseVpnAnyconnect"

    const-string v1, "OnServiceDisconnected."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    iget-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$1;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    # getter for: Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mBindingLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->access$100(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 144
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$1;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    # setter for: Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->isBinding:Ljava/lang/Boolean;
    invoke-static {v0, v2}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->access$202(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 145
    monitor-exit v1

    .line 146
    return-void

    .line 145
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public OnServiceWillDisconnect(Lcom/cisco/anyconnect/vpn/android/service/IVpnService;ZLjava/lang/String;)V
    .locals 4
    .param p1, "vpnService"    # Lcom/cisco/anyconnect/vpn/android/service/IVpnService;
    .param p2, "isForcedShutdown"    # Z
    .param p3, "shutdownMsg"    # Ljava/lang/String;

    .prologue
    .line 127
    const-string v1, "EnterpriseVpnAnyconnect"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "OnServiceWillDisconnect. Forced: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$1;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    # getter for: Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mCertHandler:Lcom/cisco/anyconnect/vpn/android/service/ICertificateListener;
    invoke-static {v1}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->access$400(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;)Lcom/cisco/anyconnect/vpn/android/service/ICertificateListener;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService;->UnregisterCertificateListener(Lcom/cisco/anyconnect/vpn/android/service/ICertificateListener;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 132
    const-string v1, "EnterpriseVpnAnyconnect"

    const-string v2, "UnregisterCertificateListener failed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 134
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "EnterpriseVpnAnyconnect"

    const-string v2, "RemoteException occurred while unregistering callback."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
