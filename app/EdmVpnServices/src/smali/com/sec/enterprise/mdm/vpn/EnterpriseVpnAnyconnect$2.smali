.class Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$2;
.super Lcom/cisco/anyconnect/vpn/android/service/ICertificateListener$Stub;
.source "EnterpriseVpnAnyconnect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$2;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    invoke-direct {p0}, Lcom/cisco/anyconnect/vpn/android/service/ICertificateListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public ClientCertificateCB(Lcom/cisco/anyconnect/vpn/android/service/IVpnCertificateList;)V
    .locals 3
    .param p1, "arg0"    # Lcom/cisco/anyconnect/vpn/android/service/IVpnCertificateList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 175
    const-string v0, "EnterpriseVpnAnyconnect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ClientCertificateCB got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/cisco/anyconnect/vpn/android/service/IVpnCertificateList;->GetClientCerts()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " certificates."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    iget-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$2;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    # getter for: Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mCertListLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->access$500(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 177
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$2;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    invoke-interface {p1}, Lcom/cisco/anyconnect/vpn/android/service/IVpnCertificateList;->GetClientCerts()Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mCertList:Ljava/util/List;
    invoke-static {v0, v2}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->access$602(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;Ljava/util/List;)Ljava/util/List;

    .line 178
    iget-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$2;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    # getter for: Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mCertListLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->access$500(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 179
    monitor-exit v1

    .line 180
    return-void

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public ImportPKCS12CompleteCB([BLjava/lang/String;)V
    .locals 0
    .param p1, "certHash"    # [B
    .param p2, "error"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 172
    return-void
.end method

.method public SCEPEnrollExitCB()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 183
    return-void
.end method

.method public SCEPEnrollStartCB()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 186
    return-void
.end method
