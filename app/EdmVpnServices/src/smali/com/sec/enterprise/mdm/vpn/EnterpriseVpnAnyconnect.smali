.class public Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;
.super Landroid/app/Service;
.source "EnterpriseVpnAnyconnect.java"


# instance fields
.field private isBinding:Ljava/lang/Boolean;

.field private final mBinder:Landroid/app/enterprise/IEnterpriseVpnInterface$Stub;

.field private final mBindingLock:Ljava/lang/Object;

.field private mCertHandler:Lcom/cisco/anyconnect/vpn/android/service/ICertificateListener;

.field private mCertList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cisco/anyconnect/vpn/android/service/VpnCertificate;",
            ">;"
        }
    .end annotation
.end field

.field private mCertListLock:Ljava/lang/Object;

.field private final mMyUserId:I

.field mReadyMessenger:Landroid/os/Messenger;

.field private final mServiceConnMgr:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 70
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mCertListLock:Ljava/lang/Object;

    .line 71
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mBindingLock:Ljava/lang/Object;

    .line 76
    new-instance v0, Ljava/lang/Boolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/Boolean;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->isBinding:Ljava/lang/Boolean;

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mReadyMessenger:Landroid/os/Messenger;

    .line 82
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    iput v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mMyUserId:I

    .line 85
    new-instance v0, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    new-instance v1, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$1;

    invoke-direct {v1, p0, p0}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$1;-><init>(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;Landroid/content/Context;)V

    invoke-direct {v0, v1}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;-><init>(Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionCB;)V

    iput-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mServiceConnMgr:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    .line 169
    new-instance v0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$2;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$2;-><init>(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;)V

    iput-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mCertHandler:Lcom/cisco/anyconnect/vpn/android/service/ICertificateListener;

    .line 459
    new-instance v0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$3;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$3;-><init>(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;)V

    iput-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mBinder:Landroid/app/enterprise/IEnterpriseVpnInterface$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    .prologue
    .line 66
    iget v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mMyUserId:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mBindingLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0, "x0"    # Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;
    .param p1, "x1"    # Ljava/lang/Boolean;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->isBinding:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;Lcom/cisco/anyconnect/vpn/android/service/IVpnService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;
    .param p1, "x1"    # Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->verifyApiVersion(Lcom/cisco/anyconnect/vpn/android/service/IVpnService;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;)Lcom/cisco/anyconnect/vpn/android/service/ICertificateListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mCertHandler:Lcom/cisco/anyconnect/vpn/android/service/ICertificateListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mCertListLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mCertList:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;Landroid/os/Messenger;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;
    .param p1, "x1"    # Landroid/os/Messenger;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->prepareService(Landroid/os/Messenger;)I

    move-result v0

    return v0
.end method

.method private convertToCiscoVpnConnection(Landroid/app/enterprise/EnterpriseVpnConnection;Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;)Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;
    .locals 6
    .param p1, "conn"    # Landroid/app/enterprise/EnterpriseVpnConnection;
    .param p2, "old"    # Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;

    .prologue
    const/4 v3, 0x0

    .line 429
    invoke-direct {p0, v3}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->prepareService(Landroid/os/Messenger;)I

    move-result v4

    if-eqz v4, :cond_1

    move-object v2, v3

    .line 455
    :cond_0
    :goto_0
    return-object v2

    .line 432
    :cond_1
    if-nez p1, :cond_2

    move-object v2, v3

    .line 433
    goto :goto_0

    .line 436
    :cond_2
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mServiceConnMgr:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    invoke-virtual {v4}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->GetService()Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    move-result-object v4

    invoke-interface {v4}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService;->GetConnectionList()Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;

    move-result-object v0

    .line 437
    .local v0, "connList":Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;
    const/4 v2, 0x0

    .line 438
    .local v2, "ret":Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;
    if-nez p2, :cond_3

    .line 439
    invoke-interface {v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;->CreateNew()Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;

    move-result-object v2

    .line 443
    :goto_1
    if-eqz v2, :cond_0

    .line 444
    iget-object v4, p1, Landroid/app/enterprise/EnterpriseVpnConnection;->name:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->SetName(Ljava/lang/String;)V

    .line 445
    iget-object v4, p1, Landroid/app/enterprise/EnterpriseVpnConnection;->host:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->SetHost(Ljava/lang/String;)V

    .line 446
    iget-object v4, p1, Landroid/app/enterprise/EnterpriseVpnConnection;->certHash:[B

    invoke-virtual {v2, v4}, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->SetCertHash([B)V

    .line 447
    iget-object v4, p1, Landroid/app/enterprise/EnterpriseVpnConnection;->certCommonName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->SetCertCommonName(Ljava/lang/String;)V

    .line 448
    invoke-virtual {p1}, Landroid/app/enterprise/EnterpriseVpnConnection;->getCertAuthMode()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 449
    invoke-virtual {p1}, Landroid/app/enterprise/EnterpriseVpnConnection;->getCertAuthMode()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/cisco/anyconnect/vpn/jni/CertAuthMode;->valueOf(Ljava/lang/String;)Lcom/cisco/anyconnect/vpn/jni/CertAuthMode;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->SetCertAuthMode(Lcom/cisco/anyconnect/vpn/jni/CertAuthMode;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 453
    .end local v0    # "connList":Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;
    .end local v2    # "ret":Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;
    :catch_0
    move-exception v1

    .line 454
    .local v1, "e":Landroid/os/RemoteException;
    const-string v4, "EnterpriseVpnAnyconnect"

    const-string v5, ""

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v2, v3

    .line 455
    goto :goto_0

    .line 441
    .end local v1    # "e":Landroid/os/RemoteException;
    .restart local v0    # "connList":Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;
    .restart local v2    # "ret":Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;
    :cond_3
    move-object v2, p2

    goto :goto_1
.end method

.method private convertToEdmVpnConnection(Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;)Landroid/app/enterprise/EnterpriseVpnConnection;
    .locals 2
    .param p1, "conn"    # Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;

    .prologue
    .line 412
    if-nez p1, :cond_1

    .line 413
    const/4 v0, 0x0

    .line 424
    :cond_0
    :goto_0
    return-object v0

    .line 415
    :cond_1
    new-instance v0, Landroid/app/enterprise/EnterpriseVpnConnection;

    invoke-direct {v0}, Landroid/app/enterprise/EnterpriseVpnConnection;-><init>()V

    .line 416
    .local v0, "ret":Landroid/app/enterprise/EnterpriseVpnConnection;
    invoke-virtual {p1}, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->GetName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/app/enterprise/EnterpriseVpnConnection;->name:Ljava/lang/String;

    .line 417
    invoke-virtual {p1}, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->GetHost()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/app/enterprise/EnterpriseVpnConnection;->host:Ljava/lang/String;

    .line 418
    invoke-virtual {p1}, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->GetCertHash()[B

    move-result-object v1

    iput-object v1, v0, Landroid/app/enterprise/EnterpriseVpnConnection;->certHash:[B

    .line 419
    invoke-virtual {p1}, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->GetCertCommonName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/app/enterprise/EnterpriseVpnConnection;->certCommonName:Ljava/lang/String;

    .line 420
    const-string v1, "anyconnect"

    iput-object v1, v0, Landroid/app/enterprise/EnterpriseVpnConnection;->type:Ljava/lang/String;

    .line 421
    invoke-virtual {p1}, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->GetCertAuthMode()Lcom/cisco/anyconnect/vpn/jni/CertAuthMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 422
    invoke-virtual {p1}, Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;->GetCertAuthMode()Lcom/cisco/anyconnect/vpn/jni/CertAuthMode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cisco/anyconnect/vpn/jni/CertAuthMode;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/enterprise/EnterpriseVpnConnection;->setCertAuthMode(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method private declared-synchronized prepareService(Landroid/os/Messenger;)I
    .locals 4
    .param p1, "readyMessenger"    # Landroid/os/Messenger;

    .prologue
    const/4 v0, 0x1

    .line 249
    monitor-enter p0

    :try_start_0
    const-string v1, "EnterpriseVpnAnyconnect"

    const-string v2, "prepareService"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    iget-object v1, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mServiceConnMgr:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    invoke-virtual {v1}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->GetService()Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 252
    const-string v0, "EnterpriseVpnAnyconnect"

    const-string v1, "Anyconnect service already bound"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 253
    const/4 v0, 0x0

    .line 271
    :goto_0
    monitor-exit p0

    return v0

    .line 256
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mBindingLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 258
    :try_start_2
    iget-object v2, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->isBinding:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 259
    const-string v2, "EnterpriseVpnAnyconnect"

    const-string v3, "Already trying to bind to Anyconnect service"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    monitor-exit v1

    goto :goto_0

    .line 275
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 249
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 263
    :cond_1
    :try_start_4
    const-string v2, "EnterpriseVpnAnyconnect"

    const-string v3, "Anyconnect service not bound yet. Bound phase will start..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    iget-object v2, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mServiceConnMgr:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    invoke-virtual {v2}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->Activate()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->isBinding:Ljava/lang/Boolean;

    .line 266
    iget-object v2, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->isBinding:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 267
    iput-object p1, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mReadyMessenger:Landroid/os/Messenger;

    .line 268
    monitor-exit v1

    goto :goto_0

    .line 270
    :cond_2
    const-string v0, "EnterpriseVpnAnyconnect"

    const-string v2, "ServiceConnMgr.Activate() failed. Is AnyConnect installed?"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    const/4 v0, -0x1

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method private verifyApiVersion(Lcom/cisco/anyconnect/vpn/android/service/IVpnService;)V
    .locals 6
    .param p1, "vpnService"    # Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    .prologue
    .line 203
    const/4 v0, 0x2

    .line 205
    .local v0, "EXPECTED_API_VERSION":I
    :try_start_0
    invoke-interface {p1}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService;->GetVpnServiceVersion()I

    move-result v2

    .line 206
    .local v2, "version":I
    if-eq v2, v0, :cond_0

    .line 207
    const-string v4, "EnterpriseVpnAnyconnect"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Anyconnect API version: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " not supported. Please update "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-le v2, v0, :cond_1

    const-string v3, "Android"

    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    .end local v2    # "version":I
    :cond_0
    :goto_1
    return-void

    .line 207
    .restart local v2    # "version":I
    :cond_1
    const-string v3, "Anyconnect"
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 210
    .end local v2    # "version":I
    :catch_0
    move-exception v1

    .line 211
    .local v1, "e":Landroid/os/RemoteException;
    const-string v3, "EnterpriseVpnAnyconnect"

    const-string v4, "Error retrieving version information of Anyconnect."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private verifyApplicationVersion()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 217
    const/16 v0, 0x9

    .line 218
    .local v0, "MIN_APP_VERSION":I
    invoke-virtual {p0}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.cisco.anyconnect.vpn.android.service.IVpnService"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v7}, Landroid/content/pm/PackageManager;->resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    .line 220
    .local v3, "r":Landroid/content/pm/ResolveInfo;
    if-nez v3, :cond_1

    .line 221
    const-string v4, "EnterpriseVpnAnyconnect"

    const-string v5, "AnyConnect is not installed."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    :cond_0
    :goto_0
    return-void

    .line 224
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v5, v3, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v5, v5, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 226
    .local v2, "pInfo":Landroid/content/pm/PackageInfo;
    iget v4, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    if-ge v4, v0, :cond_0

    .line 227
    const-string v4, "EnterpriseVpnAnyconnect"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "AnyConnect Application version: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " incompatible"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 230
    .end local v2    # "pInfo":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v1

    .line 231
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v4, "EnterpriseVpnAnyconnect"

    const-string v5, "Failed to detect AnyConnect installation."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public createConnection(Landroid/app/enterprise/EnterpriseVpnConnection;Ljava/lang/String;)Z
    .locals 6
    .param p1, "conn"    # Landroid/app/enterprise/EnterpriseVpnConnection;
    .param p2, "oldName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 279
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->prepareService(Landroid/os/Messenger;)I

    move-result v4

    if-eqz v4, :cond_1

    .line 299
    :cond_0
    :goto_0
    return v3

    .line 282
    :cond_1
    iget-object v4, p1, Landroid/app/enterprise/EnterpriseVpnConnection;->name:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p1, Landroid/app/enterprise/EnterpriseVpnConnection;->host:Ljava/lang/String;

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Landroid/app/enterprise/EnterpriseVpnConnection;->getCertAuthMode()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 286
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mServiceConnMgr:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    invoke-virtual {v4}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->GetService()Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    move-result-object v4

    invoke-interface {v4}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService;->GetConnectionList()Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;

    move-result-object v0

    .line 287
    .local v0, "connList":Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;
    const/4 v2, 0x0

    .line 288
    .local v2, "vpnConn":Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;
    if-eqz p2, :cond_2

    .line 289
    invoke-interface {v0, p2}, Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;->GetConnection(Ljava/lang/String;)Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;

    move-result-object v2

    .line 291
    :cond_2
    invoke-direct {p0, p1, v2}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->convertToCiscoVpnConnection(Landroid/app/enterprise/EnterpriseVpnConnection;Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;)Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;

    move-result-object v4

    invoke-interface {v0, v4}, Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;->Save(Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-nez v4, :cond_0

    .line 292
    const/4 v3, 0x1

    goto :goto_0

    .line 296
    .end local v0    # "connList":Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;
    .end local v2    # "vpnConn":Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;
    :catch_0
    move-exception v1

    .line 297
    .local v1, "e":Landroid/os/RemoteException;
    const-string v4, "EnterpriseVpnAnyconnect"

    const-string v5, ""

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getAllConnections()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/EnterpriseVpnConnection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 326
    .local v5, "ret":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/EnterpriseVpnConnection;>;"
    const/4 v7, 0x0

    invoke-direct {p0, v7}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->prepareService(Landroid/os/Messenger;)I

    move-result v7

    if-eqz v7, :cond_1

    .line 341
    :cond_0
    :goto_0
    return-object v5

    .line 330
    :cond_1
    :try_start_0
    iget-object v7, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mServiceConnMgr:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    invoke-virtual {v7}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->GetService()Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    move-result-object v7

    invoke-interface {v7}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService;->GetConnectionList()Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;

    move-result-object v0

    .line 331
    .local v0, "connList":Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;
    invoke-interface {v0}, Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;->GetAllNames()Ljava/util/List;

    move-result-object v4

    .line 332
    .local v4, "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 333
    .local v3, "name":Ljava/lang/String;
    invoke-interface {v0, v3}, Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;->GetConnection(Ljava/lang/String;)Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;

    move-result-object v6

    .line 334
    .local v6, "vpnConn":Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;
    if-eqz v6, :cond_2

    .line 335
    invoke-direct {p0, v6}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->convertToEdmVpnConnection(Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;)Landroid/app/enterprise/EnterpriseVpnConnection;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 338
    .end local v0    # "connList":Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v6    # "vpnConn":Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;
    :catch_0
    move-exception v1

    .line 339
    .local v1, "e":Landroid/os/RemoteException;
    const-string v7, "EnterpriseVpnAnyconnect"

    const-string v8, ""

    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public declared-synchronized getCertificates()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/CertificateInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 381
    monitor-enter p0

    :try_start_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 382
    .local v4, "ret":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    const/4 v5, 0x0

    invoke-direct {p0, v5}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->prepareService(Landroid/os/Messenger;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-eqz v5, :cond_1

    .line 408
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v4

    .line 387
    :cond_1
    :try_start_1
    iget-object v5, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mServiceConnMgr:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    invoke-virtual {v5}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->GetService()Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    move-result-object v5

    invoke-interface {v5}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService;->GetClientCertificates()Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 392
    .local v0, "aux":Z
    if-eqz v0, :cond_0

    .line 394
    :try_start_2
    iget-object v6, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mCertListLock:Ljava/lang/Object;

    monitor-enter v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 396
    :try_start_3
    iget-object v5, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mCertListLock:Ljava/lang/Object;

    const-wide/16 v8, 0x2710

    invoke-virtual {v5, v8, v9}, Ljava/lang/Object;->wait(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 400
    :goto_1
    :try_start_4
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 402
    :try_start_5
    iget-object v5, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mCertList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cisco/anyconnect/vpn/android/service/VpnCertificate;

    .line 403
    .local v1, "cert":Lcom/cisco/anyconnect/vpn/android/service/VpnCertificate;
    invoke-virtual {v1}, Lcom/cisco/anyconnect/vpn/android/service/VpnCertificate;->GetX509()Ljava/security/cert/X509Certificate;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 404
    new-instance v5, Landroid/app/enterprise/CertificateInfo;

    invoke-virtual {v1}, Lcom/cisco/anyconnect/vpn/android/service/VpnCertificate;->GetX509()Ljava/security/cert/X509Certificate;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/app/enterprise/CertificateInfo;-><init>(Ljava/security/cert/Certificate;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 381
    .end local v0    # "aux":Z
    .end local v1    # "cert":Lcom/cisco/anyconnect/vpn/android/service/VpnCertificate;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "ret":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 388
    .restart local v4    # "ret":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    :catch_0
    move-exception v2

    .line 389
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_6
    const-string v5, "EnterpriseVpnAnyconnect"

    const-string v6, ""

    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 397
    .end local v2    # "e":Landroid/os/RemoteException;
    .restart local v0    # "aux":Z
    :catch_1
    move-exception v2

    .line 398
    .local v2, "e":Ljava/lang/InterruptedException;
    :try_start_7
    const-string v5, "EnterpriseVpnAnyconnect"

    const-string v7, ""

    invoke-static {v5, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 400
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :catchall_1
    move-exception v5

    monitor-exit v6
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v5
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0
.end method

.method public getConnection(Ljava/lang/String;)Landroid/app/enterprise/EnterpriseVpnConnection;
    .locals 5
    .param p1, "connName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 345
    invoke-direct {p0, v2}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->prepareService(Landroid/os/Messenger;)I

    move-result v3

    if-eqz v3, :cond_0

    .line 353
    :goto_0
    return-object v2

    .line 349
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mServiceConnMgr:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    invoke-virtual {v3}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->GetService()Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    move-result-object v3

    invoke-interface {v3}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService;->GetConnectionList()Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;

    move-result-object v0

    .line 350
    .local v0, "connList":Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;
    invoke-interface {v0, p1}, Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;->GetConnection(Ljava/lang/String;)Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->convertToEdmVpnConnection(Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;)Landroid/app/enterprise/EnterpriseVpnConnection;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 351
    .end local v0    # "connList":Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;
    :catch_0
    move-exception v1

    .line 352
    .local v1, "e":Landroid/os/RemoteException;
    const-string v3, "EnterpriseVpnAnyconnect"

    const-string v4, ""

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public declared-synchronized installCertificate([BLjava/lang/String;)Z
    .locals 6
    .param p1, "pkcs12Blob"    # [B
    .param p2, "password"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 361
    monitor-enter p0

    const/4 v4, 0x0

    :try_start_0
    invoke-direct {p0, v4}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->prepareService(Landroid/os/Messenger;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_1

    .line 376
    :cond_0
    :goto_0
    monitor-exit p0

    return v3

    .line 364
    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mServiceConnMgr:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    invoke-virtual {v4}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->GetService()Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    move-result-object v2

    .line 367
    .local v2, "vpnService":Lcom/cisco/anyconnect/vpn/android/service/IVpnService;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-nez v4, :cond_0

    .line 369
    :try_start_2
    invoke-interface {v2, p1, p2}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService;->ImportPKCS12WithPassword([BLjava/lang/String;)Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    .line 374
    .local v1, "res":Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;
    :goto_1
    if-eqz v1, :cond_0

    :try_start_3
    sget-object v4, Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;->SUCCESS:Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;

    if-ne v1, v4, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    .line 370
    .end local v1    # "res":Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;
    :catch_0
    move-exception v0

    .line 371
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "EnterpriseVpnAnyconnect"

    const-string v5, "AnyConnect died?"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 372
    const/4 v1, 0x0

    .restart local v1    # "res":Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;
    goto :goto_1

    .line 361
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "res":Lcom/cisco/anyconnect/vpn/android/service/VpnServiceResult;
    .end local v2    # "vpnService":Lcom/cisco/anyconnect/vpn/android/service/IVpnService;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 506
    iget-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mBinder:Landroid/app/enterprise/IEnterpriseVpnInterface$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 190
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 191
    invoke-direct {p0}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->verifyApplicationVersion()V

    .line 192
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mServiceConnMgr:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mServiceConnMgr:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    invoke-virtual {v0}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->Deactivate()V

    .line 198
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 199
    return-void
.end method

.method public removeConnection(Ljava/lang/String;)Z
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 303
    const/4 v5, 0x0

    invoke-direct {p0, v5}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->prepareService(Landroid/os/Messenger;)I

    move-result v5

    if-eqz v5, :cond_1

    .line 320
    :cond_0
    :goto_0
    return v3

    .line 307
    :cond_1
    :try_start_0
    iget-object v5, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->mServiceConnMgr:Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;

    invoke-virtual {v5}, Lcom/cisco/anyconnect/vpn/android/service/ServiceConnectionManager;->GetService()Lcom/cisco/anyconnect/vpn/android/service/IVpnService;

    move-result-object v5

    invoke-interface {v5}, Lcom/cisco/anyconnect/vpn/android/service/IVpnService;->GetConnectionList()Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;

    move-result-object v0

    .line 308
    .local v0, "connList":Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;
    invoke-interface {v0, p1}, Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;->GetConnection(Ljava/lang/String;)Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;

    move-result-object v2

    .line 309
    .local v2, "vpnconn":Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;
    if-eqz v2, :cond_2

    .line 310
    invoke-interface {v0, v2}, Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;->Delete(Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_0

    move v3, v4

    .line 311
    goto :goto_0

    :cond_2
    move v3, v4

    .line 316
    goto :goto_0

    .line 318
    .end local v0    # "connList":Lcom/cisco/anyconnect/vpn/android/service/IVpnConnectionList;
    .end local v2    # "vpnconn":Lcom/cisco/anyconnect/vpn/android/service/VpnConnection;
    :catch_0
    move-exception v1

    .line 319
    .local v1, "e":Landroid/os/RemoteException;
    const-string v4, "EnterpriseVpnAnyconnect"

    const-string v5, ""

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
