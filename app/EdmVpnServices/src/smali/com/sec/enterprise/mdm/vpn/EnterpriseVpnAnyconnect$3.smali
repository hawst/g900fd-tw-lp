.class Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$3;
.super Landroid/app/enterprise/IEnterpriseVpnInterface$Stub;
.source "EnterpriseVpnAnyconnect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;)V
    .locals 0

    .prologue
    .line 459
    iput-object p1, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$3;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    invoke-direct {p0}, Landroid/app/enterprise/IEnterpriseVpnInterface$Stub;-><init>()V

    return-void
.end method

.method private enforceSystemUser()V
    .locals 2

    .prologue
    .line 498
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_0

    .line 499
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Can only be called by system process"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 502
    :cond_0
    return-void
.end method


# virtual methods
.method public createConnection(Landroid/app/enterprise/EnterpriseVpnConnection;Ljava/lang/String;)Z
    .locals 1
    .param p1, "conn"    # Landroid/app/enterprise/EnterpriseVpnConnection;
    .param p2, "oldName"    # Ljava/lang/String;

    .prologue
    .line 467
    invoke-direct {p0}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$3;->enforceSystemUser()V

    .line 468
    iget-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$3;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    invoke-virtual {v0, p1, p2}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->createConnection(Landroid/app/enterprise/EnterpriseVpnConnection;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getAllConnections()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/EnterpriseVpnConnection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 477
    invoke-direct {p0}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$3;->enforceSystemUser()V

    .line 478
    iget-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$3;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    invoke-virtual {v0}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->getAllConnections()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCertificates()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/CertificateInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 492
    invoke-direct {p0}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$3;->enforceSystemUser()V

    .line 493
    iget-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$3;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    invoke-virtual {v0}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->getCertificates()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getConnection(Ljava/lang/String;)Landroid/app/enterprise/EnterpriseVpnConnection;
    .locals 1
    .param p1, "connName"    # Ljava/lang/String;

    .prologue
    .line 482
    invoke-direct {p0}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$3;->enforceSystemUser()V

    .line 483
    iget-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$3;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->getConnection(Ljava/lang/String;)Landroid/app/enterprise/EnterpriseVpnConnection;

    move-result-object v0

    return-object v0
.end method

.method public installCertificate([BLjava/lang/String;)Z
    .locals 1
    .param p1, "pkcs12Blob"    # [B
    .param p2, "password"    # Ljava/lang/String;

    .prologue
    .line 487
    invoke-direct {p0}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$3;->enforceSystemUser()V

    .line 488
    iget-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$3;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    invoke-virtual {v0, p1, p2}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->installCertificate([BLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public prepareService(Landroid/os/Messenger;)I
    .locals 1
    .param p1, "readyMessenger"    # Landroid/os/Messenger;

    .prologue
    .line 462
    invoke-direct {p0}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$3;->enforceSystemUser()V

    .line 463
    iget-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$3;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    # invokes: Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->prepareService(Landroid/os/Messenger;)I
    invoke-static {v0, p1}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->access$700(Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;Landroid/os/Messenger;)I

    move-result v0

    return v0
.end method

.method public removeConnection(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 472
    invoke-direct {p0}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$3;->enforceSystemUser()V

    .line 473
    iget-object v0, p0, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect$3;->this$0:Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/mdm/vpn/EnterpriseVpnAnyconnect;->removeConnection(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
