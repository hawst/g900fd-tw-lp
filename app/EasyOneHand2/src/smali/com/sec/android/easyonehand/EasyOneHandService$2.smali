.class Lcom/sec/android/easyonehand/EasyOneHandService$2;
.super Landroid/content/BroadcastReceiver;
.source "EasyOneHandService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/easyonehand/EasyOneHandService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/easyonehand/EasyOneHandService;


# direct methods
.method constructor <init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V
    .locals 0

    .prologue
    .line 437
    iput-object p1, p0, Lcom/sec/android/easyonehand/EasyOneHandService$2;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 439
    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->DEBUG:Z
    invoke-static {}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$400()Z

    move-result v1

    if-eqz v1, :cond_0

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$500()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mHomePauseResumeReceiver : onReceive"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    :cond_0
    iget-object v1, p0, Lcom/sec/android/easyonehand/EasyOneHandService$2;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v1}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/easyonehand/EasyOneHandService$2;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v1}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowController;->isAttached()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 441
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EasyOneHandService$2;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;
    invoke-static {v1}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$700(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMagnifyRect:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 442
    .local v0, "r":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/easyonehand/EasyOneHandService$2;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v1}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/easyonehand/EOHWindowController;->setBackgroundTransparentRect(Landroid/graphics/Rect;)V

    .line 444
    .end local v0    # "r":Landroid/graphics/Rect;
    :cond_1
    return-void
.end method
