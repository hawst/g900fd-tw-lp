.class Lcom/sec/android/easyonehand/EOHBackgroundWindow$1;
.super Ljava/lang/Object;
.source "EOHBackgroundWindow.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/easyonehand/EOHBackgroundWindow;->setDragEventHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    const/4 v8, 0x0

    .line 101
    sget-boolean v5, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->DEBUG:Z

    if-eqz v5, :cond_0

    .line 102
    const-string v5, "EOHBackgroundWindow"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "EOHBackgroundWindow.onDrag() event="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", callers="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0xf

    invoke-static {v7}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    :cond_0
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    .line 105
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 145
    :cond_1
    :goto_0
    return v8

    .line 107
    :pswitch_0
    sget-boolean v5, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->DEBUG:Z

    if-eqz v5, :cond_1

    .line 108
    const-string v5, "EOHBackgroundWindow"

    const-string v6, "ACTION_DRAG_ENTERED"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 111
    :pswitch_1
    sget-boolean v5, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->DEBUG:Z

    if-eqz v5, :cond_1

    .line 112
    const-string v5, "EOHBackgroundWindow"

    const-string v6, "ACTION_DRAG_EXITED"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 115
    :pswitch_2
    sget-boolean v5, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->DEBUG:Z

    if-eqz v5, :cond_2

    .line 116
    const-string v5, "EOHBackgroundWindow"

    const-string v6, "ACTION_DRAG_STARTED"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    :cond_2
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v5}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->access$100(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/sec/android/easyonehand/EOHWindowController;->setDragMode(Z)V

    goto :goto_0

    .line 120
    :pswitch_3
    sget-boolean v5, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->DEBUG:Z

    if-eqz v5, :cond_1

    .line 121
    const-string v5, "EOHBackgroundWindow"

    const-string v6, "ACTION_DRAG_LOCATION"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 124
    :pswitch_4
    sget-boolean v5, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->DEBUG:Z

    if-eqz v5, :cond_3

    .line 125
    const-string v5, "EOHBackgroundWindow"

    const-string v6, "ACTION_DROP"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    :cond_3
    :try_start_0
    invoke-virtual {p2}, Landroid/view/DragEvent;->getClipData()Landroid/content/ClipData;

    move-result-object v1

    .line 129
    .local v1, "clip":Landroid/content/ClipData;
    if-eqz v1, :cond_1

    .line 130
    invoke-virtual {v1}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ClipDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 131
    .local v3, "desc":Ljava/lang/String;
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 132
    .local v2, "data":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v5}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->access$100(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v5

    invoke-virtual {p2}, Landroid/view/DragEvent;->getX()F

    move-result v6

    invoke-virtual {p2}, Landroid/view/DragEvent;->getY()F

    move-result v7

    invoke-virtual {v5, v3, v2, v6, v7}, Lcom/sec/android/easyonehand/EOHWindowController;->notifyDragDropEvent(Ljava/lang/String;Ljava/lang/String;FF)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 134
    .end local v1    # "clip":Landroid/content/ClipData;
    .end local v2    # "data":Ljava/lang/String;
    .end local v3    # "desc":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 135
    .local v4, "e":Ljava/lang/Exception;
    const-string v5, "EOHBackgroundWindow"

    const-string v6, "exception while drop "

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 140
    .end local v4    # "e":Ljava/lang/Exception;
    :pswitch_5
    sget-boolean v5, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->DEBUG:Z

    if-eqz v5, :cond_4

    .line 141
    const-string v5, "EOHBackgroundWindow"

    const-string v6, "ACTION_DRAG_ENDED"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    :cond_4
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v5}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->access$100(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v5

    invoke-virtual {v5, v8}, Lcom/sec/android/easyonehand/EOHWindowController;->setDragMode(Z)V

    goto/16 :goto_0

    .line 105
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
