.class final Lcom/sec/android/easyonehand/EOHWindowInfo;
.super Ljava/lang/Object;
.source "EOHWindowInfo.java"


# static fields
.field static final DEBUG:Z

.field private static mSingleton:Lcom/sec/android/easyonehand/EOHWindowInfo;


# instance fields
.field private mBottomOffset:I

.field private mBottomOffsetDefault:I

.field private mCocktailBarWidth:I

.field private mContext:Landroid/content/Context;

.field private mDM:Landroid/util/DisplayMetrics;

.field private mDisplaytRotation:I

.field private mIsCocktailBarEnabled:Z

.field private mLeftHandMode:Z

.field mMagnifyRect:Landroid/graphics/Rect;

.field mMainRect:Landroid/graphics/Rect;

.field private mMaxHeight:I

.field private mMaxWidth:I

.field private mMinHeight:I

.field private mMinWidth:I

.field private mPixelPerOneDp:F

.field private mRealSize:Landroid/graphics/Point;

.field private mScale:F

.field private mScaleDefault:F

.field private mScaleMax:F

.field private mScaleMin:F

.field private mScreenLocked:Z

.field private mSharedPreference:Landroid/content/SharedPreferences;

.field private mSharedPreferenceEditor:Landroid/content/SharedPreferences$Editor;

.field private mSideOffset:I

.field mSideWindowRect:Landroid/graphics/Rect;

.field private mSideWindowType:I

.field private mWM:Landroid/view/WindowManager;

.field private mWMS:Landroid/view/IWindowManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/easyonehand/EOHWindowInfo;->DEBUG:Z

    .line 92
    new-instance v0, Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-direct {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;-><init>()V

    sput-object v0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSingleton:Lcom/sec/android/easyonehand/EOHWindowInfo;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mBottomOffset:I

    .line 44
    iput v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideOffset:I

    .line 46
    iput v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideWindowType:I

    .line 48
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMagnifyRect:Landroid/graphics/Rect;

    .line 50
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMainRect:Landroid/graphics/Rect;

    .line 52
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideWindowRect:Landroid/graphics/Rect;

    .line 54
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mRealSize:Landroid/graphics/Point;

    .line 66
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mPixelPerOneDp:F

    .line 72
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mDM:Landroid/util/DisplayMetrics;

    .line 74
    iput-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSharedPreference:Landroid/content/SharedPreferences;

    .line 76
    iput-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSharedPreferenceEditor:Landroid/content/SharedPreferences$Editor;

    .line 78
    iput-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mContext:Landroid/content/Context;

    .line 96
    return-void
.end method

.method public static getInstance()Lcom/sec/android/easyonehand/EOHWindowInfo;
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSingleton:Lcom/sec/android/easyonehand/EOHWindowInfo;

    return-object v0
.end method

.method private setInnerWindowRect(Landroid/graphics/Rect;)V
    .locals 6
    .param p1, "r"    # Landroid/graphics/Rect;

    .prologue
    .line 412
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMagnifyRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 413
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMainRect:Landroid/graphics/Rect;

    iget v1, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideMargin()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getTopMargin()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p1, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideMargin()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getBottomMargin()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 416
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHWindowInfo;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 417
    const-string v0, "EOHWindowInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInnerWindowRect() mMagnifyRect="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMagnifyRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mMainRect="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMainRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mSideOffset="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideOffset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    :cond_0
    return-void
.end method

.method private setSideWindowRect()V
    .locals 6

    .prologue
    .line 421
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isLeftHandMode()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMainRect:Landroid/graphics/Rect;

    iget v1, v4, Landroid/graphics/Rect;->right:I

    .line 422
    .local v1, "left":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMainRect:Landroid/graphics/Rect;

    iget v3, v4, Landroid/graphics/Rect;->top:I

    .line 423
    .local v3, "top":I
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowWidth()I

    move-result v4

    add-int v2, v1, v4

    .line 424
    .local v2, "right":I
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMainRect:Landroid/graphics/Rect;

    iget v0, v4, Landroid/graphics/Rect;->bottom:I

    .line 426
    .local v0, "bottom":I
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideWindowRect:Landroid/graphics/Rect;

    invoke-virtual {v4, v1, v3, v2, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 427
    return-void

    .line 421
    .end local v0    # "bottom":I
    .end local v1    # "left":I
    .end local v2    # "right":I
    .end local v3    # "top":I
    :cond_0
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMainRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowWidth()I

    move-result v5

    sub-int v1, v4, v5

    goto :goto_0
.end method


# virtual methods
.method public calculateWindowInfo()V
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 430
    sget-boolean v8, Lcom/sec/android/easyonehand/EOHWindowInfo;->DEBUG:Z

    if-eqz v8, :cond_0

    .line 431
    const-string v8, "EOHWindowInfo"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "calculateWindowInfo() mLeftHandMode="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-boolean v10, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mLeftHandMode:Z

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", mSideOffset="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideOffset:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", mBottomOffset="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mBottomOffset:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", mDM="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mDM:Landroid/util/DisplayMetrics;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", info="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", callers="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x5

    invoke-static {v10}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v8

    int-to-float v8, v8

    iget v9, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScale:F

    mul-float/2addr v8, v9

    float-to-int v6, v8

    .line 435
    .local v6, "width":I
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v8

    int-to-float v8, v8

    iget v9, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScale:F

    mul-float/2addr v8, v9

    float-to-int v3, v8

    .line 437
    .local v3, "height":I
    iget-boolean v8, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mLeftHandMode:Z

    if-eqz v8, :cond_3

    .line 438
    iget v8, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideOffset:I

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideMargin()I

    move-result v9

    add-int v4, v8, v9

    .line 443
    .local v4, "posX":I
    :goto_0
    iget-boolean v8, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mIsCocktailBarEnabled:Z

    if-eqz v8, :cond_1

    .line 444
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getDisplayRotation()I

    move-result v2

    .line 445
    .local v2, "dispRotation":I
    const/4 v8, 0x2

    if-ne v2, v8, :cond_4

    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mCocktailBarWidth:I

    .line 446
    .local v0, "cocktailLeft":I
    :goto_1
    if-nez v2, :cond_5

    iget v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mCocktailBarWidth:I

    .line 448
    .local v1, "cocktailRight":I
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideMargin()I

    move-result v8

    add-int/2addr v8, v4

    if-ge v8, v0, :cond_6

    .line 449
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideMargin()I

    move-result v8

    add-int v4, v0, v8

    .line 455
    .end local v0    # "cocktailLeft":I
    .end local v1    # "cocktailRight":I
    .end local v2    # "dispRotation":I
    :cond_1
    :goto_3
    iget-object v8, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mDM:Landroid/util/DisplayMetrics;

    iget v8, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int/2addr v8, v3

    iget v9, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mBottomOffset:I

    sub-int/2addr v8, v9

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getBottomMargin()I

    move-result v9

    sub-int v5, v8, v9

    .line 457
    .local v5, "posY":I
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getTopMargin()I

    move-result v8

    if-ge v5, v8, :cond_7

    .line 458
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getTopMargin()I

    move-result v5

    .line 459
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mDM:Landroid/util/DisplayMetrics;

    iget v7, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    add-int v8, v5, v3

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getBottomMargin()I

    move-result v9

    add-int/2addr v8, v9

    sub-int/2addr v7, v8

    iput v7, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mBottomOffset:I

    .line 465
    :cond_2
    :goto_4
    new-instance v7, Landroid/graphics/Rect;

    add-int v8, v4, v6

    add-int v9, v5, v3

    invoke-direct {v7, v4, v5, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-direct {p0, v7}, Lcom/sec/android/easyonehand/EOHWindowInfo;->setInnerWindowRect(Landroid/graphics/Rect;)V

    .line 466
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->setSideWindowRect()V

    .line 467
    return-void

    .line 440
    .end local v4    # "posX":I
    .end local v5    # "posY":I
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v8

    sub-int/2addr v8, v6

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideMargin()I

    move-result v9

    sub-int/2addr v8, v9

    iget v9, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideOffset:I

    sub-int v4, v8, v9

    .restart local v4    # "posX":I
    goto :goto_0

    .restart local v2    # "dispRotation":I
    :cond_4
    move v0, v7

    .line 445
    goto :goto_1

    .restart local v0    # "cocktailLeft":I
    :cond_5
    move v1, v7

    .line 446
    goto :goto_2

    .line 450
    .restart local v1    # "cocktailRight":I
    :cond_6
    add-int v8, v4, v6

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideMargin()I

    move-result v9

    add-int/2addr v8, v9

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v9

    sub-int/2addr v9, v1

    if-le v8, v9, :cond_1

    .line 451
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v8

    sub-int/2addr v8, v1

    sub-int/2addr v8, v6

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideMargin()I

    move-result v9

    sub-int/2addr v8, v9

    iget v9, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideOffset:I

    sub-int v4, v8, v9

    goto :goto_3

    .line 460
    .end local v0    # "cocktailLeft":I
    .end local v1    # "cocktailRight":I
    .end local v2    # "dispRotation":I
    .restart local v5    # "posY":I
    :cond_7
    add-int v8, v5, v3

    iget v9, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mBottomOffset:I

    add-int/2addr v8, v9

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getBottomMargin()I

    move-result v9

    add-int/2addr v8, v9

    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mDM:Landroid/util/DisplayMetrics;

    iget v9, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v8, v9, :cond_2

    .line 461
    iput v7, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mBottomOffset:I

    .line 462
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mDM:Landroid/util/DisplayMetrics;

    iget v7, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int/2addr v7, v3

    iget v8, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mBottomOffset:I

    sub-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getBottomMargin()I

    move-result v8

    sub-int v5, v7, v8

    goto :goto_4
.end method

.method public getBottomMargin()I
    .locals 2

    .prologue
    .line 342
    const/high16 v0, 0x420c0000    # 35.0f

    iget v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mPixelPerOneDp:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method getCocktailBarWidth()I
    .locals 1

    .prologue
    .line 228
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mCocktailBarWidth:I

    return v0
.end method

.method getDisplayRotation()I
    .locals 1

    .prologue
    .line 206
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mDisplaytRotation:I

    return v0
.end method

.method public getDragViewOffset()I
    .locals 2

    .prologue
    .line 362
    const/high16 v0, 0x41a00000    # 20.0f

    iget v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mPixelPerOneDp:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public getFullWindowRect(Landroid/graphics/Rect;)V
    .locals 6
    .param p1, "r"    # Landroid/graphics/Rect;

    .prologue
    .line 143
    iget v4, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideWindowType:I

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScreenLocked:Z

    if-eqz v4, :cond_1

    .line 144
    :cond_0
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMainRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 154
    :goto_0
    return-void

    .line 148
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isLeftHandMode()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMainRect:Landroid/graphics/Rect;

    iget v1, v4, Landroid/graphics/Rect;->left:I

    .line 149
    .local v1, "left":I
    :goto_1
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMainRect:Landroid/graphics/Rect;

    iget v3, v4, Landroid/graphics/Rect;->top:I

    .line 150
    .local v3, "top":I
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMainRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowWidth()I

    move-result v5

    add-int v2, v4, v5

    .line 151
    .local v2, "right":I
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMainRect:Landroid/graphics/Rect;

    iget v0, v4, Landroid/graphics/Rect;->bottom:I

    .line 153
    .local v0, "bottom":I
    invoke-virtual {p1, v1, v3, v2, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 148
    .end local v0    # "bottom":I
    .end local v1    # "left":I
    .end local v2    # "right":I
    .end local v3    # "top":I
    :cond_2
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMainRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowWidth()I

    move-result v5

    sub-int v1, v4, v5

    goto :goto_1
.end method

.method public getIWindowManager()Landroid/view/IWindowManager;
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mWMS:Landroid/view/IWindowManager;

    return-object v0
.end method

.method public getInnerX()I
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMagnifyRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    return v0
.end method

.method public getInnerY()I
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMagnifyRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    return v0
.end method

.method public getMaximumHeight()I
    .locals 1

    .prologue
    .line 386
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMaxHeight:I

    return v0
.end method

.method public getMaximumWidth()I
    .locals 2

    .prologue
    .line 378
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideWindowType:I

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScreenLocked:Z

    if-eqz v0, :cond_1

    .line 379
    :cond_0
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMaxWidth:I

    .line 382
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMaxWidth:I

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowWidth()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public getMetrics(Landroid/util/DisplayMetrics;)V
    .locals 2
    .param p1, "dm"    # Landroid/util/DisplayMetrics;

    .prologue
    .line 532
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mWM:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 533
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mWM:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mDM:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 534
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mWM:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mRealSize:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 535
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mWM:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    iput v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mDisplaytRotation:I

    .line 536
    return-void
.end method

.method public getMinimumHeight()I
    .locals 1

    .prologue
    .line 374
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMinHeight:I

    return v0
.end method

.method public getMinimumWidth()I
    .locals 2

    .prologue
    .line 366
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideWindowType:I

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScreenLocked:Z

    if-eqz v0, :cond_1

    .line 367
    :cond_0
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMinWidth:I

    .line 370
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMinWidth:I

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowWidth()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public getRawX(I)I
    .locals 4
    .param p1, "x"    # I

    .prologue
    .line 394
    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScale:F

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMagnifyRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-int v0, v1

    .line 395
    .local v0, "px":I
    sget-boolean v1, Lcom/sec/android/easyonehand/EOHWindowInfo;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 396
    const-string v1, "EOHWindowInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getRawX() x="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", px="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mMagnifyRect.left="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMagnifyRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mScale="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScale:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    :cond_0
    return v0
.end method

.method public getRawY(I)I
    .locals 4
    .param p1, "y"    # I

    .prologue
    .line 403
    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScale:F

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMagnifyRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-int v0, v1

    .line 404
    .local v0, "py":I
    sget-boolean v1, Lcom/sec/android/easyonehand/EOHWindowInfo;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 405
    const-string v1, "EOHWindowInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getRawY() y="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", py="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mMagnifyRect.top="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMagnifyRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mScale="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScale:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    :cond_0
    return v0
.end method

.method public getResizeHandlerSize()I
    .locals 2

    .prologue
    .line 358
    const/high16 v0, 0x41a00000    # 20.0f

    iget v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mPixelPerOneDp:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public getResizeLineThickness()I
    .locals 2

    .prologue
    .line 354
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mPixelPerOneDp:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public getScreenHeight()I
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mRealSize:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    return v0
.end method

.method public getScreenWidth()I
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mRealSize:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    return v0
.end method

.method getSideFitWidth()I
    .locals 2

    .prologue
    .line 390
    const/high16 v0, 0x41200000    # 10.0f

    iget v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mPixelPerOneDp:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public getSideMargin()I
    .locals 2

    .prologue
    .line 346
    const/high16 v0, 0x40400000    # 3.0f

    iget v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mPixelPerOneDp:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public getSideOffset()I
    .locals 1

    .prologue
    .line 248
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideOffset:I

    return v0
.end method

.method public getSideWindowType()I
    .locals 1

    .prologue
    .line 256
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideWindowType:I

    return v0
.end method

.method public getSideWindowWidth()I
    .locals 2

    .prologue
    .line 350
    const/high16 v0, 0x42400000    # 48.0f

    iget v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mPixelPerOneDp:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public getTopMargin()I
    .locals 2

    .prologue
    .line 338
    const/high16 v0, 0x420c0000    # 35.0f

    iget v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mPixelPerOneDp:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public getWindowManager()Landroid/view/WindowManager;
    .locals 1

    .prologue
    .line 524
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mWM:Landroid/view/WindowManager;

    return-object v0
.end method

.method public getWindowScale()F
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScale:F

    return v0
.end method

.method public init(Landroid/content/Context;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "startHandMode"    # I

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mContext:Landroid/content/Context;

    .line 104
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mContext:Landroid/content/Context;

    const-string v3, "easyonehandwindowInfo_sharedpreference"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSharedPreference:Landroid/content/SharedPreferences;

    .line 105
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSharedPreference:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSharedPreferenceEditor:Landroid/content/SharedPreferences$Editor;

    .line 107
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mContext:Landroid/content/Context;

    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    iput-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mWM:Landroid/view/WindowManager;

    .line 108
    const-string v2, "window"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mWMS:Landroid/view/IWindowManager;

    .line 109
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mDM:Landroid/util/DisplayMetrics;

    invoke-virtual {p0, v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 111
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mWM:Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mRealSize:Landroid/graphics/Point;

    invoke-virtual {v2, v3}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 113
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.sec.feature.cocktailbar"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mIsCocktailBarEnabled:Z

    .line 114
    iget-boolean v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mIsCocktailBarEnabled:Z

    if-eqz v2, :cond_0

    .line 118
    :cond_0
    const-string v2, "ro.sf.lcd_density"

    const/16 v3, 0xa0

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 119
    .local v0, "deviceDensity":I
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mDM:Landroid/util/DisplayMetrics;

    iget v3, v3, Landroid/util/DisplayMetrics;->xdpi:F

    div-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mDM:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->xdpi:F

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mDM:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->ydpi:F

    div-float/2addr v3, v4

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mDM:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->ydpi:F

    div-float/2addr v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v1

    .line 121
    .local v1, "physicalInch":F
    int-to-float v2, v0

    const/high16 v3, 0x43200000    # 160.0f

    div-float/2addr v2, v3

    iput v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mPixelPerOneDp:F

    .line 123
    const v2, 0x3f35c28f    # 0.71f

    iput v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScaleDefault:F

    .line 124
    const v2, 0x3f59999a    # 0.85f

    iput v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScaleMax:F

    .line 125
    const v2, 0x3f266666    # 0.65f

    iput v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScaleMin:F

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScaleDefault:F

    mul-float/2addr v3, v4

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getTopMargin()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getBottomMargin()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    sub-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mBottomOffsetDefault:I

    .line 129
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScaleMin:F

    mul-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideMargin()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMinWidth:I

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScaleMin:F

    mul-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getTopMargin()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getBottomMargin()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMinHeight:I

    .line 131
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScaleMax:F

    mul-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideMargin()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMaxWidth:I

    .line 132
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScaleMax:F

    mul-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getTopMargin()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getBottomMargin()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMaxHeight:I

    .line 134
    invoke-virtual {p0, p2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->loadWindowInfo(I)V

    .line 136
    sget-boolean v2, Lcom/sec/android/easyonehand/EOHWindowInfo;->DEBUG:Z

    if-eqz v2, :cond_1

    .line 137
    const-string v2, "EOHWindowInfo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "init() mLeftHandMode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mLeftHandMode:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mPixelPerOneDp="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mPixelPerOneDp:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", deviceDensity="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", physicalInch="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", DM="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mDM:Landroid/util/DisplayMetrics;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    :cond_1
    return-void
.end method

.method isCocktailBarEnabled()Z
    .locals 1

    .prologue
    .line 224
    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mIsCocktailBarEnabled:Z

    return v0
.end method

.method public isFrameRedrawNeeded(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .locals 8
    .param p1, "oldRect"    # Landroid/graphics/Rect;
    .param p2, "newRect"    # Landroid/graphics/Rect;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 292
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v6

    if-eq v5, v6, :cond_1

    move v3, v4

    .line 314
    :cond_0
    :goto_0
    return v3

    .line 296
    :cond_1
    const/4 v1, 0x0

    .line 297
    .local v1, "cocktailRight":I
    const/4 v0, 0x0

    .line 298
    .local v0, "cocktailLeft":I
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isCocktailBarEnabled()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 299
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getDisplayRotation()I

    move-result v2

    .line 300
    .local v2, "dispRotation":I
    const/4 v5, 0x2

    if-ne v2, v5, :cond_3

    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mCocktailBarWidth:I

    .line 301
    :goto_1
    if-nez v2, :cond_4

    iget v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mCocktailBarWidth:I

    .line 304
    .end local v2    # "dispRotation":I
    :cond_2
    :goto_2
    iget-boolean v5, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mLeftHandMode:Z

    if-eqz v5, :cond_5

    .line 305
    iget v5, p2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v6

    sub-int/2addr v6, v1

    iget v7, p2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v6, v7

    if-le v5, v6, :cond_0

    move v3, v4

    .line 306
    goto :goto_0

    .restart local v2    # "dispRotation":I
    :cond_3
    move v0, v3

    .line 300
    goto :goto_1

    :cond_4
    move v1, v3

    .line 301
    goto :goto_2

    .line 309
    .end local v2    # "dispRotation":I
    :cond_5
    iget v5, p2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v6

    sub-int/2addr v6, v0

    iget v7, p2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v6, v7

    if-ge v5, v6, :cond_0

    move v3, v4

    .line 310
    goto :goto_0
.end method

.method public isLeftHandMode()Z
    .locals 1

    .prologue
    .line 198
    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mLeftHandMode:Z

    return v0
.end method

.method public isPositionAdjustNeeded()Z
    .locals 4

    .prologue
    .line 280
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v0, v1

    .line 282
    .local v0, "width":I
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowType()I

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScreenLocked:Z

    if-nez v1, :cond_0

    .line 283
    iget v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideOffset:I

    add-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideMargin()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowWidth()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v2

    if-le v1, v2, :cond_0

    .line 284
    const-string v1, "EOHWindowInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isPositionAdjustNeeded() returned true. callers="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xa

    invoke-static {v3}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    const/4 v1, 0x1

    .line 288
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isScreenLocked()Z
    .locals 1

    .prologue
    .line 210
    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScreenLocked:Z

    return v0
.end method

.method public isSideWindowShowing()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 232
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v2

    if-le v1, v2, :cond_1

    .line 236
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideWindowType:I

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScreenLocked:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public loadWindowInfo(I)V
    .locals 5
    .param p1, "startHandMode"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 470
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSharedPreference:Landroid/content/SharedPreferences;

    if-nez v2, :cond_0

    .line 471
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mContext:Landroid/content/Context;

    const-string v3, "easyonehandwindowInfo_sharedpreference"

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSharedPreference:Landroid/content/SharedPreferences;

    .line 474
    :cond_0
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSharedPreference:Landroid/content/SharedPreferences;

    const-string v3, "bottom_offset"

    const/4 v4, -0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mBottomOffset:I

    .line 475
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSharedPreference:Landroid/content/SharedPreferences;

    const-string v3, "side_offset"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideOffset:I

    .line 476
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSharedPreference:Landroid/content/SharedPreferences;

    const-string v3, "scale"

    iget v4, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScaleDefault:F

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v2

    iput v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScale:F

    .line 477
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSharedPreference:Landroid/content/SharedPreferences;

    const-string v3, "side_window_type"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideWindowType:I

    .line 479
    if-nez p1, :cond_5

    .line 480
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSharedPreference:Landroid/content/SharedPreferences;

    const-string v3, "left_hand_mode"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_4

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mLeftHandMode:Z

    .line 486
    :goto_1
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScale:F

    iget v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScaleMin:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_7

    .line 487
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScaleMin:F

    iput v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScale:F

    .line 492
    :cond_1
    :goto_2
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mBottomOffset:I

    if-gez v0, :cond_8

    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mBottomOffsetDefault:I

    :goto_3
    iput v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mBottomOffset:I

    .line 493
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScale:F

    iget v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScaleMax:F

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_2

    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScale:F

    iget v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScaleMin:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_9

    :cond_2
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScaleDefault:F

    :goto_4
    iput v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScale:F

    .line 495
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->calculateWindowInfo()V

    .line 497
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHWindowInfo;->DEBUG:Z

    if-eqz v0, :cond_3

    .line 498
    const-string v0, "EOHWindowInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadWindowInfo() info="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    :cond_3
    return-void

    :cond_4
    move v0, v1

    .line 480
    goto :goto_0

    .line 483
    :cond_5
    if-ne p1, v0, :cond_6

    :goto_5
    iput-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mLeftHandMode:Z

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_5

    .line 488
    :cond_7
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScale:F

    iget v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScaleMax:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 489
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScaleMax:F

    iput v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScale:F

    goto :goto_2

    .line 492
    :cond_8
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mBottomOffset:I

    goto :goto_3

    .line 493
    :cond_9
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScale:F

    goto :goto_4
.end method

.method public setBottomOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 244
    if-gez p1, :cond_0

    const/4 p1, 0x0

    .end local p1    # "offset":I
    :cond_0
    iput p1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mBottomOffset:I

    .line 245
    return-void
.end method

.method public setLeftHandMode(Z)V
    .locals 0
    .param p1, "isLeftMode"    # Z

    .prologue
    .line 202
    iput-boolean p1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mLeftHandMode:Z

    .line 203
    return-void
.end method

.method public setScreenLocked(Z)V
    .locals 0
    .param p1, "isLocked"    # Z

    .prologue
    .line 214
    iput-boolean p1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScreenLocked:Z

    .line 215
    return-void
.end method

.method public setSideOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 252
    if-gez p1, :cond_0

    const/4 p1, 0x0

    .end local p1    # "offset":I
    :cond_0
    iput p1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideOffset:I

    .line 253
    return-void
.end method

.method public setSideWindowType(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 260
    if-ltz p1, :cond_0

    const/4 v0, 0x2

    if-le p1, v0, :cond_1

    .line 264
    :cond_0
    :goto_0
    return-void

    .line 263
    :cond_1
    iput p1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideWindowType:I

    goto :goto_0
.end method

.method public setWindowScale(F)V
    .locals 3
    .param p1, "scale"    # F

    .prologue
    .line 173
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHWindowInfo;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 174
    const-string v0, "EOHWindowInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setWindowScale() scale="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    :cond_0
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScaleMin:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 177
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScaleMin:F

    iput v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScale:F

    .line 183
    :goto_0
    return-void

    .line 178
    :cond_1
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScaleMax:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_2

    .line 179
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScaleMax:F

    iput v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScale:F

    goto :goto_0

    .line 181
    :cond_2
    iput p1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScale:F

    goto :goto_0
.end method

.method public storeWindowInfo()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 502
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSharedPreferenceEditor:Landroid/content/SharedPreferences$Editor;

    if-nez v1, :cond_0

    .line 521
    :goto_0
    return-void

    .line 506
    :cond_0
    sget-boolean v1, Lcom/sec/android/easyonehand/EOHWindowInfo;->DEBUG:Z

    if-eqz v1, :cond_1

    const-string v1, "EOHWindowInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "storeWindowInfo() mSideOffset="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideOffset:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mIsCocktailBarEnabled:Z

    if-eqz v1, :cond_2

    .line 509
    iget v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideOffset:I

    iget v2, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mCocktailBarWidth:I

    if-ne v1, v2, :cond_2

    .line 510
    iput v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideOffset:I

    .line 514
    :cond_2
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSharedPreferenceEditor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "scale"

    iget v3, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScale:F

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 515
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSharedPreferenceEditor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "bottom_offset"

    iget v3, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mBottomOffset:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 516
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSharedPreferenceEditor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "side_offset"

    iget v3, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideOffset:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 517
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSharedPreferenceEditor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "side_window_type"

    iget v3, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideWindowType:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 518
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSharedPreferenceEditor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "left_hand_mode"

    iget-boolean v3, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mLeftHandMode:Z

    if-eqz v3, :cond_3

    const/4 v0, 0x1

    :cond_3
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 520
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSharedPreferenceEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 553
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EOHWindowInfo [mLeftHandMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mLeftHandMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mScale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mScale:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mBottomOffset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mBottomOffset:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSideOffset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mSideOffset:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mMagnifyRect="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMagnifyRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mMainRect="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMainRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", screenWidth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mRealSize:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", screenHeight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mRealSize:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateDisplayMatrix()V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mWM:Landroid/view/WindowManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mWM:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowInfo;->mDM:Landroid/util/DisplayMetrics;

    invoke-virtual {p0, v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 221
    :cond_0
    return-void
.end method
