.class Lcom/sec/android/easyonehand/EasyOneHandService$10;
.super Landroid/content/BroadcastReceiver;
.source "EasyOneHandService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/easyonehand/EasyOneHandService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/easyonehand/EasyOneHandService;


# direct methods
.method constructor <init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V
    .locals 0

    .prologue
    .line 548
    iput-object p1, p0, Lcom/sec/android/easyonehand/EasyOneHandService$10;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x5

    .line 551
    if-eqz p2, :cond_0

    const-string v0, "android.intent.action.USER_PRESENT"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 552
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$10;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$700(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowInfo;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->setScreenLocked(Z)V

    .line 553
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$10;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$10;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowController;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$10;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowController;->isAnimationRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 555
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$10;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$300(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 556
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$10;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$300(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 557
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$10;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$300(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 560
    :cond_0
    return-void
.end method
