.class Lcom/sec/android/easyonehand/EasyOneHandService$11;
.super Landroid/content/BroadcastReceiver;
.source "EasyOneHandService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/easyonehand/EasyOneHandService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/easyonehand/EasyOneHandService;


# direct methods
.method constructor <init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V
    .locals 0

    .prologue
    .line 563
    iput-object p1, p0, Lcom/sec/android/easyonehand/EasyOneHandService$11;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 566
    if-eqz p2, :cond_0

    .line 567
    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$500()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mShutdownReceiver="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$11;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 569
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$11;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHWindowController;->closeWindow(Z)V

    .line 572
    :cond_0
    return-void
.end method
