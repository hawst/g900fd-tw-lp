.class Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter$1;
.super Ljava/lang/Object;
.source "EOHSideWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;I)V
    .locals 0

    .prologue
    .line 1583
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter$1;->this$1:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    iput p2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter$1;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 1586
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter$1;->this$1:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    iget-object v0, v0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$000(Lcom/sec/android/easyonehand/EOHSideWindow;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHUtils;->playShortHaptic()V

    .line 1587
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter$1;->this$1:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    iget-object v0, v0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mIsEditMode:Z
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$200(Lcom/sec/android/easyonehand/EOHSideWindow;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1588
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter$1;->this$1:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    iget-object v0, v0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    iget v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter$1;->val$position:I

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHSideWindow;->removeShortcutMenu(I)V

    .line 1592
    :goto_0
    return-void

    .line 1590
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter$1;->this$1:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    iget v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter$1;->val$position:I

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->runListViewItem(I)V

    goto :goto_0
.end method
