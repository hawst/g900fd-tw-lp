.class public Lcom/sec/android/easyonehand/EOHBackgroundWindow;
.super Ljava/lang/Object;
.source "EOHBackgroundWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;,
        Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;,
        Lcom/sec/android/easyonehand/EOHBackgroundWindow$WindowHideHandler;
    }
.end annotation


# static fields
.field static final DEBUG:Z


# instance fields
.field private mBackgroundImg:Landroid/graphics/drawable/BitmapDrawable;

.field private mBgViewDefault:Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;

.field private mBgViewMagnify:Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;

.field private mContext:Landroid/content/Context;

.field private mCurrentBgLayer:I

.field private mHandler:Landroid/os/Handler;

.field private mScreenRect:Landroid/graphics/Rect;

.field private mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

.field private mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/easyonehand/EOHWindowController;Landroid/content/Context;)V
    .locals 6
    .param p1, "controller"    # Lcom/sec/android/easyonehand/EOHWindowController;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput v5, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mCurrentBgLayer:I

    .line 54
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    .line 55
    iput-object p2, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mContext:Landroid/content/Context;

    .line 57
    sget-boolean v2, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 58
    const-string v2, "EOHBackgroundWindow"

    const-string v3, "EOHBackgroundWindow() start"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    :cond_0
    const/4 v1, 0x0

    .line 66
    .local v1, "bmpDrawable":Landroid/graphics/drawable/BitmapDrawable;
    if-nez v1, :cond_1

    .line 73
    :cond_1
    new-instance v2, Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mContext:Landroid/content/Context;

    invoke-direct {v2, p0, v3}, Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;-><init>(Lcom/sec/android/easyonehand/EOHBackgroundWindow;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBgViewDefault:Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;

    .line 74
    new-instance v2, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mContext:Landroid/content/Context;

    invoke-direct {v2, p0, v3}, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;-><init>(Lcom/sec/android/easyonehand/EOHBackgroundWindow;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBgViewMagnify:Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;

    .line 76
    new-instance v2, Landroid/os/Handler;

    new-instance v3, Lcom/sec/android/easyonehand/EOHBackgroundWindow$WindowHideHandler;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/sec/android/easyonehand/EOHBackgroundWindow$WindowHideHandler;-><init>(Lcom/sec/android/easyonehand/EOHBackgroundWindow;Lcom/sec/android/easyonehand/EOHBackgroundWindow$1;)V

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v2, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mHandler:Landroid/os/Handler;

    .line 78
    invoke-static {}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getInstance()Lcom/sec/android/easyonehand/EOHWindowInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    .line 79
    new-instance v2, Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v4}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v4

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mScreenRect:Landroid/graphics/Rect;

    .line 81
    if-eqz v1, :cond_2

    .line 82
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 84
    .local v0, "bmpBG":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mScreenRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    if-le v2, v3, :cond_4

    .line 85
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {p0, v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->getCropBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBackgroundImg:Landroid/graphics/drawable/BitmapDrawable;

    .line 91
    .end local v0    # "bmpBG":Landroid/graphics/Bitmap;
    :cond_2
    :goto_0
    sget-boolean v2, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->DEBUG:Z

    if-eqz v2, :cond_3

    .line 92
    const-string v2, "EOHBackgroundWindow"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "EOHBackgroundWindow() end. mScreenRect="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->setDragEventHandler()V

    .line 95
    return-void

    .line 87
    .restart local v0    # "bmpBG":Landroid/graphics/Bitmap;
    :cond_4
    iput-object v1, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBackgroundImg:Landroid/graphics/drawable/BitmapDrawable;

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)Lcom/sec/android/easyonehand/EOHWindowController;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->hideDefaultBg()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->hideMagnifyBg()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBackgroundImg:Landroid/graphics/drawable/BitmapDrawable;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mScreenRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method private addWindow(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "lp"    # Landroid/view/WindowManager$LayoutParams;

    .prologue
    .line 345
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 346
    return-void
.end method

.method private getCropBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "srcBmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 211
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 212
    .local v4, "srcW":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 213
    .local v3, "srcH":I
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mScreenRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 214
    .local v2, "destW":I
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mScreenRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v1

    .line 216
    .local v1, "destH":I
    sub-int v5, v4, v2

    div-int/lit8 v5, v5, 0x2

    sub-int v6, v3, v1

    div-int/lit8 v6, v6, 0x2

    invoke-static {p1, v5, v6, v2, v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 218
    .local v0, "cropBmp":Landroid/graphics/Bitmap;
    return-object v0
.end method

.method private getLayoutParam(ILjava/lang/String;)Landroid/view/WindowManager$LayoutParams;
    .locals 4
    .param p1, "windowTpye"    # I
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 328
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x158

    const/4 v2, -0x3

    invoke-direct {v0, p1, v1, v2}, Landroid/view/WindowManager$LayoutParams;-><init>(III)V

    .line 333
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 334
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 335
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 336
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 337
    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 338
    const/4 v1, 0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    .line 339
    invoke-virtual {v0, p2}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 341
    return-object v0
.end method

.method private hideDefaultBg()V
    .locals 2

    .prologue
    .line 362
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBgViewDefault:Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;

    # invokes: Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;->isWindowAdded()Z
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;->access$700(Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBgViewDefault:Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;

    invoke-direct {p0, v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->removeWindow(Landroid/view/View;)V

    .line 364
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBgViewDefault:Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;->setWindowAdded(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;->access$800(Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;Z)V

    .line 366
    :cond_0
    return-void
.end method

.method private hideMagnifyBg()V
    .locals 2

    .prologue
    .line 378
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBgViewMagnify:Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;

    # invokes: Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->isWindowAdded()Z
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->access$900(Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBgViewMagnify:Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;

    invoke-direct {p0, v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->removeWindow(Landroid/view/View;)V

    .line 380
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBgViewMagnify:Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->setWindowAdded(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->access$1000(Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;Z)V

    .line 382
    :cond_0
    return-void
.end method

.method private removeWindow(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 349
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 350
    return-void
.end method

.method private setDragEventHandler()V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBgViewMagnify:Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;

    new-instance v1, Lcom/sec/android/easyonehand/EOHBackgroundWindow$1;

    invoke-direct {v1, p0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow$1;-><init>(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 148
    return-void
.end method

.method private showDefaultBg()V
    .locals 3

    .prologue
    .line 353
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBgViewDefault:Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;

    # invokes: Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;->isWindowAdded()Z
    invoke-static {v1}, Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;->access$700(Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 354
    const/16 v1, 0x7e9

    const-string v2, "EasyOneHand/Background"

    invoke-direct {p0, v1, v2}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->getLayoutParam(ILjava/lang/String;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 356
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBgViewDefault:Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;

    invoke-direct {p0, v1, v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->addWindow(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;)V

    .line 357
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBgViewDefault:Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;->setWindowAdded(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;->access$800(Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;Z)V

    .line 359
    .end local v0    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_0
    return-void
.end method

.method private showMagnifyBg()V
    .locals 3

    .prologue
    .line 369
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBgViewMagnify:Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;

    # invokes: Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->isWindowAdded()Z
    invoke-static {v1}, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->access$900(Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 370
    const/16 v1, 0x8cf

    const-string v2, "EasyOneHand/Background"

    invoke-direct {p0, v1, v2}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->getLayoutParam(ILjava/lang/String;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 372
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBgViewMagnify:Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;

    invoke-direct {p0, v1, v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->addWindow(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;)V

    .line 373
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBgViewMagnify:Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->setWindowAdded(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->access$1000(Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;Z)V

    .line 375
    .end local v0    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_0
    return-void
.end method


# virtual methods
.method public cancelAllMessages()V
    .locals 2

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 298
    :cond_0
    return-void
.end method

.method public finishWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 310
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 311
    const-string v0, "EOHBackgroundWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "finishWindow() callers="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBgViewMagnify:Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;

    if-eqz v0, :cond_1

    .line 314
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBgViewMagnify:Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;

    invoke-virtual {v0, v3}, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 317
    :cond_1
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBgViewMagnify:Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;

    .line 318
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBackgroundImg:Landroid/graphics/drawable/BitmapDrawable;

    .line 320
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    .line 321
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    .line 322
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mContext:Landroid/content/Context;

    .line 323
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mHandler:Landroid/os/Handler;

    .line 324
    return-void
.end method

.method public hideBackground()V
    .locals 2

    .prologue
    .line 263
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 264
    const-string v0, "EOHBackgroundWindow"

    const-string v1, "hideBackground() "

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->hideDefaultBg()V

    .line 266
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->hideMagnifyBg()V

    .line 267
    return-void
.end method

.method public setBackgroundTransparentRect(Landroid/graphics/Rect;)V
    .locals 3
    .param p1, "r"    # Landroid/graphics/Rect;

    .prologue
    .line 301
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 302
    const-string v0, "EOHBackgroundWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setBackgroundTransparentRect() rect="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", callers="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBgViewMagnify:Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;

    if-eqz v0, :cond_1

    .line 304
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBgViewMagnify:Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;

    invoke-virtual {v0, p1}, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->setTransparentArea(Landroid/graphics/Rect;)V

    .line 305
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBgViewMagnify:Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->invalidate()V

    .line 307
    :cond_1
    return-void
.end method

.method public showBackground(I)V
    .locals 6
    .param p1, "newLayer"    # I

    .prologue
    const-wide/16 v4, 0xc8

    .line 270
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 271
    const-string v0, "EOHBackgroundWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showBackground() layer ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", current =  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mCurrentBgLayer:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    :cond_0
    iget v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mCurrentBgLayer:I

    if-ne p1, v0, :cond_1

    .line 291
    :goto_0
    return-void

    .line 277
    :cond_1
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 285
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->showDefaultBg()V

    .line 286
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->cancelAllMessages()V

    .line 287
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 279
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->showMagnifyBg()V

    .line 280
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->cancelAllMessages()V

    .line 281
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 277
    nop

    :sswitch_data_0
    .sparse-switch
        0x7e9 -> :sswitch_0
        0x8cf -> :sswitch_1
    .end sparse-switch
.end method
