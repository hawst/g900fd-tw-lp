.class Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;
.super Landroid/widget/BaseAdapter;
.source "EOHSideWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/easyonehand/EOHSideWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ContactAdpater"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater$WindowHideHandler;
    }
.end annotation


# instance fields
.field mAvartarMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;",
            ">;"
        }
    .end annotation
.end field

.field private mLayout:I

.field final synthetic this$0:Lcom/sec/android/easyonehand/EOHSideWindow;


# direct methods
.method public constructor <init>(Lcom/sec/android/easyonehand/EOHSideWindow;Landroid/content/Context;I)V
    .locals 3
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layout"    # I

    .prologue
    .line 733
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 731
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mAvartarMap:Ljava/util/HashMap;

    .line 734
    iput-object p2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mContext:Landroid/content/Context;

    .line 735
    iput p3, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mLayout:I

    .line 736
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mInflater:Landroid/view/LayoutInflater;

    .line 737
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mItems:Ljava/util/List;

    .line 739
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater$WindowHideHandler;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater$WindowHideHandler;-><init>(Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;Lcom/sec/android/easyonehand/EOHSideWindow$1;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mHandler:Landroid/os/Handler;

    .line 740
    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    .prologue
    .line 720
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mItems:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    .prologue
    .line 720
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getDefaultAvatar(J)Landroid/graphics/Bitmap;
    .locals 11
    .param p1, "contactId"    # J

    .prologue
    const-wide/16 v6, 0x0

    .line 997
    invoke-direct {p0, p1, p2}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->getDefaultPhotoBackgroundColor(J)I

    move-result v2

    .line 998
    .local v2, "colorId":I
    iget-object v8, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 1000
    .local v1, "color":I
    const-wide/16 v8, 0x1

    cmp-long v8, p1, v8

    if-gez v8, :cond_2

    move-wide v4, v6

    .line 1001
    .local v4, "pos":J
    :goto_0
    const-wide/16 v8, 0x4

    cmp-long v8, v4, v8

    if-nez v8, :cond_0

    move-wide v4, v6

    .line 1003
    :cond_0
    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mAvartarMap:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    .line 1005
    .local v3, "round":Landroid/graphics/Bitmap;
    if-nez v3, :cond_1

    .line 1006
    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/high16 v7, 0x7f020000

    invoke-static {v6, v7}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1007
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-direct {p0, v0, v1}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->getRoundedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1008
    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mAvartarMap:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1011
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    return-object v3

    .line 1000
    .end local v3    # "round":Landroid/graphics/Bitmap;
    .end local v4    # "pos":J
    :cond_2
    const-wide/16 v8, 0x5

    rem-long v4, p1, v8

    goto :goto_0
.end method

.method private getDefaultPhotoBackgroundColor(J)I
    .locals 5
    .param p1, "contactId"    # J

    .prologue
    const/high16 v0, 0x7f040000

    .line 956
    const-wide/16 v2, 0x1

    cmp-long v1, p1, v2

    if-gez v1, :cond_0

    .line 971
    :goto_0
    :pswitch_0
    return v0

    .line 959
    :cond_0
    const-wide/16 v2, 0x5

    rem-long v2, p1, v2

    long-to-int v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 963
    :pswitch_1
    const v0, 0x7f040001

    goto :goto_0

    .line 965
    :pswitch_2
    const v0, 0x7f040002

    goto :goto_0

    .line 967
    :pswitch_3
    const v0, 0x7f040003

    goto :goto_0

    .line 959
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getRoundedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "color"    # I

    .prologue
    const/4 v9, 0x0

    .line 976
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 978
    .local v1, "output":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 980
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 981
    .local v2, "paint":Landroid/graphics/Paint;
    new-instance v4, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-direct {v4, v9, v9, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 982
    .local v4, "rect":Landroid/graphics/Rect;
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v4}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 984
    .local v5, "rectF":Landroid/graphics/RectF;
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 985
    const/4 v6, -0x1

    if-eq p2, v6, :cond_0

    .line 986
    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 987
    invoke-virtual {v2, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 989
    :cond_0
    invoke-virtual {v0, v5, v2}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 990
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 991
    .local v3, "paint2":Landroid/graphics/Paint;
    new-instance v6, Landroid/graphics/PorterDuffXfermode;

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v6, v7}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v3, v6}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 992
    const/4 v6, 0x0

    invoke-virtual {v0, p1, v6, v4, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 993
    return-object v1
.end method


# virtual methods
.method public addContactItem(Ljava/lang/String;I)Z
    .locals 15
    .param p1, "lookup"    # Ljava/lang/String;
    .param p2, "position"    # I

    .prologue
    .line 774
    sget-boolean v1, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 775
    const-string v1, "EOHSideWindow"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addContactItem() lookup="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", position="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 776
    :cond_0
    const/4 v13, 0x0

    .line 778
    .local v13, "retValue":Z
    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "contact_id"

    aput-object v2, v3, v1

    const/4 v1, 0x1

    const-string v2, "data1"

    aput-object v2, v3, v1

    const/4 v1, 0x2

    const-string v2, "display_name"

    aput-object v2, v3, v1

    .line 786
    .local v3, "field":[Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "lookup=?"

    const/4 v6, 0x1

    new-array v5, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 790
    .local v10, "cursor":Landroid/database/Cursor;
    if-nez v10, :cond_1

    move v14, v13

    .line 815
    .end local v13    # "retValue":Z
    .local v14, "retValue":I
    :goto_0
    return v14

    .line 795
    .end local v14    # "retValue":I
    .restart local v13    # "retValue":Z
    :cond_1
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v11

    .line 796
    .local v11, "cursorCount":I
    sget-boolean v1, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v1, :cond_2

    .line 797
    const-string v1, "EOHSideWindow"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addContactItem() cursorCount="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    :cond_2
    const/4 v1, 0x1

    if-lt v11, v1, :cond_4

    .line 799
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 800
    const/4 v1, 0x0

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 801
    .local v5, "id":Ljava/lang/String;
    const/4 v1, 0x1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 802
    .local v8, "number":Ljava/lang/String;
    const/4 v1, 0x2

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 803
    .local v7, "name":Ljava/lang/String;
    invoke-virtual {p0, v5}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->getPhoto(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 804
    .local v9, "bitmap":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;
    invoke-static {v1}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$900(Lcom/sec/android/easyonehand/EOHSideWindow;)Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mItems:Ljava/util/List;

    new-instance v4, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;

    move-object/from16 v6, p1

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    move/from16 v0, p2

    invoke-interface {v1, v0, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 805
    sget-boolean v1, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v1, :cond_3

    .line 806
    const-string v1, "EOHSideWindow"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addContactItem() id="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", name="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", number="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", lookup="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 808
    :cond_3
    const/4 v13, 0x1

    .line 814
    .end local v5    # "id":Ljava/lang/String;
    .end local v7    # "name":Ljava/lang/String;
    .end local v8    # "number":Ljava/lang/String;
    .end local v9    # "bitmap":Landroid/graphics/Bitmap;
    .end local v11    # "cursorCount":I
    :cond_4
    :goto_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    move v14, v13

    .line 815
    .restart local v14    # "retValue":I
    goto/16 :goto_0

    .line 810
    .end local v14    # "retValue":I
    :catch_0
    move-exception v12

    .line 811
    .local v12, "e":Ljava/lang/Exception;
    const-string v1, "EOHSideWindow"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error while add contact item. position="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", lookup="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public addContactItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 9
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "lookup"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "number"    # Ljava/lang/String;
    .param p5, "position"    # I

    .prologue
    .line 756
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 757
    const-string v0, "EOHSideWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addContactItem() id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", lookup="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", name="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", number="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", position="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 759
    :cond_0
    const/4 v7, 0x1

    .line 763
    .local v7, "retValue":Z
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->getPhoto(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 764
    .local v5, "bitmap":Landroid/graphics/Bitmap;
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$900(Lcom/sec/android/easyonehand/EOHSideWindow;)Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    move-result-object v0

    iget-object v8, v0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mItems:Ljava/util/List;

    new-instance v0, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    invoke-interface {v8, p5, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 770
    .end local v5    # "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return v7

    .line 765
    :catch_0
    move-exception v6

    .line 766
    .local v6, "e":Ljava/lang/Exception;
    const-string v0, "EOHSideWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error while add contact item. position="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", lookup="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 767
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public cleanUpMemory()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 833
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 834
    .local v0, "N":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 835
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mItems:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->bitmap:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->access$1100(Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 836
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mItems:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->bitmap:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->access$1100(Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 837
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mItems:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;

    # setter for: Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->bitmap:Landroid/graphics/Bitmap;
    invoke-static {v2, v4}, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->access$1102(Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 834
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 840
    :cond_1
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 841
    iput-object v4, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mInflater:Landroid/view/LayoutInflater;

    .line 843
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mAvartarMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 844
    iput-object v4, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mAvartarMap:Ljava/util/HashMap;

    .line 846
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_2

    .line 847
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3e9

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 848
    iput-object v4, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mHandler:Landroid/os/Handler;

    .line 850
    :cond_2
    return-void
.end method

.method closeQuickContact()V
    .locals 6

    .prologue
    .line 881
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 882
    .local v1, "intentQuick":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v2}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$000(Lcom/sec/android/easyonehand/EOHSideWindow;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHUtils;->isDocomoPhonebook()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 883
    const-string v2, "com.samsung.contacts.action.QUICK_CONTACT_DESTROY"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 887
    :goto_0
    const-string v2, "is_oneHand_mode"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 890
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mContext:Landroid/content/Context;

    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 894
    :goto_1
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3e9

    const-wide/16 v4, 0x32

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 895
    return-void

    .line 885
    :cond_0
    const-string v2, "com.android.contacts.action.QUICK_CONTACT_DESTROY"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 891
    :catch_0
    move-exception v0

    .line 892
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "EOHSideWindow"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "EOH Exception e="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 854
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 859
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 720
    invoke-virtual {p0, p1}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->getItem(I)Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 864
    int-to-long v0, p1

    return-wide v0
.end method

.method public getPhoto(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 743
    if-nez p1, :cond_1

    .line 751
    :cond_0
    :goto_0
    return-object v2

    .line 745
    :cond_1
    sget-object v3, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 746
    .local v1, "uri":Landroid/net/Uri;
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v1}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 748
    .local v0, "data":Ljava/io/InputStream;
    if-eqz v0, :cond_0

    .line 749
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v2

    const/4 v3, -0x1

    invoke-direct {p0, v2, v3}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->getRoundedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 1017
    if-nez p2, :cond_0

    .line 1018
    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mInflater:Landroid/view/LayoutInflater;

    iget v7, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mLayout:I

    invoke-virtual {v6, v7, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 1021
    :cond_0
    const v6, 0x7f090003

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 1022
    .local v1, "iconImage":Landroid/widget/ImageButton;
    const v6, 0x7f090004

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 1023
    .local v2, "iconView":Landroid/widget/ImageView;
    const v6, 0x7f090002

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 1024
    .local v3, "minusImage":Landroid/widget/ImageView;
    const v6, 0x7f090005

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1026
    .local v4, "text":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v6}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$000(Lcom/sec/android/easyonehand/EOHSideWindow;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v6

    invoke-virtual {v6, v1}, Lcom/sec/android/easyonehand/EOHUtils;->setRippleButtonBackgroundColor(Landroid/view/View;)V

    .line 1028
    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mIsEditMode:Z
    invoke-static {v6}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$200(Lcom/sec/android/easyonehand/EOHSideWindow;)Z

    move-result v6

    if-eqz v6, :cond_1

    :goto_0
    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1029
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mItems:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->name:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->access$1300(Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1031
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mItems:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->bitmap:Landroid/graphics/Bitmap;
    invoke-static {v5}, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->access$1100(Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;)Landroid/graphics/Bitmap;

    move-result-object v5

    if-nez v5, :cond_2

    .line 1037
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mItems:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->contactId:J
    invoke-static {v5}, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->access$1400(Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;)J

    move-result-wide v6

    invoke-direct {p0, v6, v7}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->getDefaultAvatar(J)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1038
    .local v0, "bmp":Landroid/graphics/Bitmap;
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mItems:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;

    # setter for: Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->bitmap:Landroid/graphics/Bitmap;
    invoke-static {v5, v0}, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->access$1102(Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 1039
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1050
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    :goto_1
    new-instance v5, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater$1;

    invoke-direct {v5, p0, p1}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater$1;-><init>(Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;I)V

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1076
    return-object p2

    .line 1028
    :cond_1
    const/16 v5, 0x8

    goto :goto_0

    .line 1047
    :cond_2
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mItems:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->bitmap:Landroid/graphics/Bitmap;
    invoke-static {v5}, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->access$1100(Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method

.method public isItemExist(Ljava/lang/String;)Z
    .locals 3
    .param p1, "lookup"    # Ljava/lang/String;

    .prologue
    .line 823
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 824
    .local v0, "N":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 825
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mItems:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->lookup:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->access$1000(Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 826
    const/4 v2, 0x1

    .line 829
    :goto_1
    return v2

    .line 824
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 829
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public removeItem(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 819
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 820
    return-void
.end method

.method startQuickContact()V
    .locals 7

    .prologue
    .line 899
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v4}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$000(Lcom/sec/android/easyonehand/EOHSideWindow;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/easyonehand/EOHUtils;->hideStatusBar()V

    .line 901
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mContactLookup:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$1200(Lcom/sec/android/easyonehand/EOHSideWindow;)Ljava/lang/String;

    move-result-object v2

    .line 902
    .local v2, "lookup":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "content://com.android.contacts/contacts/lookup/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "?skip_display_name_lookup=true"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 905
    .local v3, "uri":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v4}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$000(Lcom/sec/android/easyonehand/EOHSideWindow;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/easyonehand/EOHUtils;->isDocomoPhonebook()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 906
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.samsung.contacts.action.QUICK_CONTACT"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 911
    .local v1, "intentQuick":Landroid/content/Intent;
    :goto_0
    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 912
    const/high16 v4, 0x14000000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 914
    const-string v4, "mode"

    const/4 v5, 0x3

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 915
    const-string v5, "exclude_mimes"

    const/4 v4, 0x0

    check-cast v4, [Ljava/lang/String;

    invoke-virtual {v1, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 918
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 923
    :goto_1
    sget-boolean v4, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v4, :cond_0

    .line 924
    const-string v4, "EOHSideWindow"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "startQuickContact() lookup="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", uri="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", intent="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 925
    :cond_0
    return-void

    .line 908
    .end local v1    # "intentQuick":Landroid/content/Intent;
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.android.contacts.action.QUICK_CONTACT"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .restart local v1    # "intentQuick":Landroid/content/Intent;
    goto :goto_0

    .line 919
    :catch_0
    move-exception v0

    .line 920
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "EOHSideWindow"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "EOH Exception e="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
