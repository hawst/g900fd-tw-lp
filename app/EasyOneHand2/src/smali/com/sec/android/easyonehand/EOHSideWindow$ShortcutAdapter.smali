.class Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;
.super Landroid/widget/BaseAdapter;
.source "EOHSideWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/easyonehand/EOHSideWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ShortcutAdapter"
.end annotation


# instance fields
.field private mComponentName:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mIconInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/drawable/BitmapDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;

.field private mLayout:I

.field private mPackageManger:Landroid/content/pm/PackageManager;

.field private mResolveInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/easyonehand/EOHSideWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/easyonehand/EOHSideWindow;Landroid/content/Context;ILjava/util/List;)V
    .locals 4
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layout"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1426
    .local p4, "resolveInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1427
    iput-object p2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mContext:Landroid/content/Context;

    .line 1428
    const-string v2, "layout_inflater"

    invoke-virtual {p2, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    iput-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 1429
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mPackageManger:Landroid/content/pm/PackageManager;

    .line 1430
    iput p3, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mLayout:I

    .line 1431
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;

    .line 1432
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mIconInfo:Ljava/util/List;

    .line 1433
    iput-object p4, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mResolveInfo:Ljava/util/List;

    .line 1435
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mResolveInfo:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 1436
    .local v0, "N":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 1437
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mIconInfo:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1436
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1439
    :cond_0
    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    .prologue
    .line 1411
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public cleanUpMemory()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1442
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mIconInfo:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    .line 1443
    .local v0, "N":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 1444
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mIconInfo:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1446
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mIconInfo:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1443
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1450
    :cond_1
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mIconInfo:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1451
    iput-object v4, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mIconInfo:Ljava/util/List;

    .line 1453
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1454
    iput-object v4, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;

    .line 1456
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mResolveInfo:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1457
    iput-object v4, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mResolveInfo:Ljava/util/List;

    .line 1459
    iput-object v4, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mPackageManger:Landroid/content/pm/PackageManager;

    .line 1460
    iput-object v4, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 1461
    iput-object v4, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mContext:Landroid/content/Context;

    .line 1462
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 1472
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/content/ComponentName;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 1476
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 1411
    invoke-virtual {p0, p1}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->getItem(I)Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1480
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 15
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 1544
    if-nez p2, :cond_0

    .line 1545
    iget-object v11, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget v12, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mLayout:I

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v11, v12, v0, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 1547
    :cond_0
    const/high16 v11, 0x7f090000

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    .line 1548
    .local v4, "iconImage":Landroid/widget/ImageButton;
    const v11, 0x7f090001

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 1551
    .local v5, "iconView":Landroid/widget/ImageView;
    iget-object v11, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v11}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$000(Lcom/sec/android/easyonehand/EOHSideWindow;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v11

    invoke-virtual {v11, v4}, Lcom/sec/android/easyonehand/EOHUtils;->setRippleButtonBackgroundColor(Landroid/view/View;)V

    .line 1553
    iget-object v11, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mResolveInfo:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v1

    .line 1554
    .local v1, "N":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_4

    .line 1555
    iget-object v11, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v11, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/ComponentName;

    invoke-virtual {v11}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v12

    iget-object v11, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mResolveInfo:Ljava/util/List;

    invoke-interface {v11, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/pm/ResolveInfo;

    iget-object v11, v11, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    iget-object v11, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v11, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/ComponentName;

    invoke-virtual {v11}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v12

    iget-object v11, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mResolveInfo:Ljava/util/List;

    invoke-interface {v11, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/pm/ResolveInfo;

    iget-object v11, v11, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1558
    iget-object v11, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mIconInfo:Ljava/util/List;

    invoke-interface {v11, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/drawable/BitmapDrawable;

    .line 1559
    .local v9, "shadow":Landroid/graphics/drawable/BitmapDrawable;
    if-nez v9, :cond_3

    .line 1560
    sget-boolean v11, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v11, :cond_1

    .line 1561
    const-string v11, "EOHSideWindow"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "shadow = null. create shadow. i="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", component="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v13, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1564
    :cond_1
    :try_start_0
    iget-object v11, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v11}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$000(Lcom/sec/android/easyonehand/EOHSideWindow;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v12

    iget-object v11, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mResolveInfo:Ljava/util/List;

    invoke-interface {v11, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/pm/ResolveInfo;

    invoke-virtual {v12, v11}, Lcom/sec/android/easyonehand/EOHUtils;->loadIconForResolve(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    .line 1566
    .local v8, "orig":Landroid/graphics/drawable/Drawable;
    instance-of v11, v8, Landroid/graphics/drawable/LayerDrawable;

    if-eqz v11, :cond_2

    .line 1567
    move-object v0, v8

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    move-object v6, v0

    .line 1568
    .local v6, "l":Landroid/graphics/drawable/LayerDrawable;
    const/4 v11, 0x0

    invoke-virtual {v6, v11}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    .line 1570
    .end local v6    # "l":Landroid/graphics/drawable/LayerDrawable;
    :cond_2
    new-instance v10, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v11, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v12}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$000(Lcom/sec/android/easyonehand/EOHSideWindow;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v12

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v13

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v14

    invoke-virtual {v12, v8, v13, v14}, Lcom/sec/android/easyonehand/EOHUtils;->createShadowIcon(Landroid/graphics/drawable/Drawable;II)Landroid/graphics/Bitmap;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v9    # "shadow":Landroid/graphics/drawable/BitmapDrawable;
    .local v10, "shadow":Landroid/graphics/drawable/BitmapDrawable;
    move-object v9, v10

    .line 1576
    .end local v8    # "orig":Landroid/graphics/drawable/Drawable;
    .end local v10    # "shadow":Landroid/graphics/drawable/BitmapDrawable;
    .restart local v9    # "shadow":Landroid/graphics/drawable/BitmapDrawable;
    :goto_1
    iget-object v11, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mIconInfo:Ljava/util/List;

    invoke-interface {v11, v3, v9}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1578
    :cond_3
    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1580
    const v11, 0x7f090002

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 1581
    .local v7, "minusImage":Landroid/widget/ImageView;
    iget-object v11, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mIsEditMode:Z
    invoke-static {v11}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$200(Lcom/sec/android/easyonehand/EOHSideWindow;)Z

    move-result v11

    if-eqz v11, :cond_5

    const/4 v11, 0x0

    :goto_2
    invoke-virtual {v7, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1583
    new-instance v11, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter$1;

    move/from16 v0, p1

    invoke-direct {v11, p0, v0}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter$1;-><init>(Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;I)V

    invoke-virtual {v4, v11}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1598
    .end local v7    # "minusImage":Landroid/widget/ImageView;
    .end local v9    # "shadow":Landroid/graphics/drawable/BitmapDrawable;
    :cond_4
    return-object p2

    .line 1572
    .restart local v9    # "shadow":Landroid/graphics/drawable/BitmapDrawable;
    :catch_0
    move-exception v2

    .line 1573
    .local v2, "e":Ljava/lang/Exception;
    const-string v11, "EOHSideWindow"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Error to get icon image e="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1581
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v7    # "minusImage":Landroid/widget/ImageView;
    :cond_5
    const/16 v11, 0x8

    goto :goto_2

    .line 1554
    .end local v7    # "minusImage":Landroid/widget/ImageView;
    .end local v9    # "shadow":Landroid/graphics/drawable/BitmapDrawable;
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0
.end method

.method public isExistPackage(Landroid/content/ComponentName;)Z
    .locals 5
    .param p1, "item"    # Landroid/content/ComponentName;

    .prologue
    .line 1484
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mResolveInfo:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    .line 1485
    .local v0, "N":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 1486
    new-instance v1, Landroid/content/ComponentName;

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mResolveInfo:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    iget-object v3, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mResolveInfo:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    iget-object v3, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v4, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1488
    .local v1, "cn":Landroid/content/ComponentName;
    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1489
    const/4 v3, 0x1

    .line 1492
    .end local v1    # "cn":Landroid/content/ComponentName;
    :goto_1
    return v3

    .line 1485
    .restart local v1    # "cn":Landroid/content/ComponentName;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1492
    .end local v1    # "cn":Landroid/content/ComponentName;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public isItemExist(Landroid/content/ComponentName;)Z
    .locals 4
    .param p1, "item"    # Landroid/content/ComponentName;

    .prologue
    .line 1496
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 1497
    .local v0, "N":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 1498
    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1499
    const/4 v2, 0x1

    .line 1503
    :goto_1
    return v2

    .line 1497
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1503
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public removeItem(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 1465
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 1469
    :cond_0
    :goto_0
    return-void

    .line 1468
    :cond_1
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method

.method public runListViewItem(I)V
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 1507
    invoke-virtual {p0, p1}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->getItem(I)Landroid/content/ComponentName;

    move-result-object v1

    .line 1508
    .local v1, "getItem":Landroid/content/ComponentName;
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->notifyDataSetChanged()V

    .line 1510
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->startShorcutActivity(Landroid/content/ComponentName;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1515
    :goto_0
    return-void

    .line 1511
    :catch_0
    move-exception v0

    .line 1512
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "EOHSideWindow"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to startActivity position="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", item="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1513
    invoke-virtual {p0, p1}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->removeItem(I)V

    goto :goto_0
.end method

.method public startShorcutActivity(Landroid/content/ComponentName;)V
    .locals 5
    .param p1, "getItem"    # Landroid/content/ComponentName;

    .prologue
    .line 1518
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v2}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$000(Lcom/sec/android/easyonehand/EOHSideWindow;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHUtils;->hideStatusBar()V

    .line 1520
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1521
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1522
    new-instance v2, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1523
    const/high16 v2, 0x10200000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1537
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1541
    :goto_0
    return-void

    .line 1538
    :catch_0
    move-exception v0

    .line 1539
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "EOHSideWindow"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception e="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
