.class Lcom/sec/android/easyonehand/EasyOneHandService$6;
.super Landroid/content/BroadcastReceiver;
.source "EasyOneHandService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/easyonehand/EasyOneHandService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/easyonehand/EasyOneHandService;


# direct methods
.method constructor <init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V
    .locals 0

    .prologue
    .line 500
    iput-object p1, p0, Lcom/sec/android/easyonehand/EasyOneHandService$6;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 503
    if-eqz p2, :cond_0

    const-string v0, "android.intent.action.USER_SWITCHED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 504
    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$500()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "intent = android.intent.action.USER_SWITCHED, isCurrentUserOwner()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService$6;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v2}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$600(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHUtils;->isCurrentUserOwner()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$6;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$600(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHUtils;->isCurrentUserOwner()Z

    move-result v0

    if-nez v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$6;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/easyonehand/EasyOneHandService;->closeAllWindowSafe(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$1000(Lcom/sec/android/easyonehand/EasyOneHandService;Z)V

    .line 511
    :cond_0
    return-void
.end method
