.class Lcom/sec/android/easyonehand/EasyOneHandService$4;
.super Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;
.source "EasyOneHandService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/easyonehand/EasyOneHandService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/easyonehand/EasyOneHandService;


# direct methods
.method constructor <init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V
    .locals 0

    .prologue
    .line 457
    iput-object p1, p0, Lcom/sec/android/easyonehand/EasyOneHandService$4;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    invoke-direct {p0}, Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCoverStateChanged(Lcom/samsung/android/sdk/cover/ScoverState;)V
    .locals 2
    .param p1, "state"    # Lcom/samsung/android/sdk/cover/ScoverState;

    .prologue
    .line 459
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v0

    if-nez v0, :cond_1

    .line 460
    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->DEBUG:Z
    invoke-static {}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$400()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461
    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mCoverStateListener() cover CLOSE"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$4;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/easyonehand/EasyOneHandService;->closeAllWindowSafe(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$1000(Lcom/sec/android/easyonehand/EasyOneHandService;Z)V

    .line 464
    :cond_1
    return-void
.end method
