.class Lcom/sec/android/easyonehand/EOHMainWindow$3;
.super Ljava/lang/Object;
.source "EOHMainWindow.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/easyonehand/EOHMainWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/easyonehand/EOHMainWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/easyonehand/EOHMainWindow;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHMainWindow$3;->this$0:Lcom/sec/android/easyonehand/EOHMainWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 228
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$3;->this$0:Lcom/sec/android/easyonehand/EOHMainWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHMainWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->access$100(Lcom/sec/android/easyonehand/EOHMainWindow;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHUtils;->userActivity()V

    .line 229
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$3;->this$0:Lcom/sec/android/easyonehand/EOHMainWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHMainWindow;->mInputFilter:Lcom/sec/android/easyonehand/EOHInputFilter;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->access$400(Lcom/sec/android/easyonehand/EOHMainWindow;)Lcom/sec/android/easyonehand/EOHInputFilter;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mResizeInputIntercepter:Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;

    const/16 v1, 0x6b

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->startIntercept(I)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$3;->this$0:Lcom/sec/android/easyonehand/EOHMainWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHMainWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->access$100(Lcom/sec/android/easyonehand/EOHMainWindow;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHUtils;->playShortHaptic()V

    .line 231
    const/4 v0, 0x0

    return v0
.end method
