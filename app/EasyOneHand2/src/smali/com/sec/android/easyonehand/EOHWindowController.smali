.class final Lcom/sec/android/easyonehand/EOHWindowController;
.super Ljava/lang/Object;
.source "EOHWindowController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/easyonehand/EOHWindowController$1;,
        Lcom/sec/android/easyonehand/EOHWindowController$WindowHideHandler;
    }
.end annotation


# static fields
.field private static final DEBUG:Z


# instance fields
.field private isEasyOneHandAttached:Z

.field private isResizingWindowShowing:Z

.field private isSideWindowShowing:Z

.field private mBackgroundWindow:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

.field private mCheckRotation:Z

.field private mContext:Landroid/content/Context;

.field private mCurrentWindowOrientation:I

.field private mDM:Landroid/util/DisplayMetrics;

.field private mDVFSCookie:I

.field private mDVFSHelperBUS:Landroid/os/DVFSHelper;

.field private mDVFSHelperCPU:Landroid/os/DVFSHelper;

.field private mDVFSHelperCore:Landroid/os/DVFSHelper;

.field private mDVFSHelperGPU:Landroid/os/DVFSHelper;

.field private mDVFSLockAcquired:Z

.field private mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

.field private mHandler:Landroid/os/Handler;

.field private mInputFilter:Lcom/sec/android/easyonehand/EOHInputFilter;

.field private mLastConfig:Landroid/content/res/Configuration;

.field private mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

.field private mResizeFrameView:Lcom/sec/android/easyonehand/EOHResizeFrameView;

.field private mResizeStartRect:Landroid/graphics/Rect;

.field private mService:Lcom/sec/android/easyonehand/EasyOneHandService;

.field private mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

.field private mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

.field private methodPauseGc:Ljava/lang/reflect/Method;

.field private methodResumeGc:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/easyonehand/EasyOneHandService;Landroid/content/Context;I)V
    .locals 4
    .param p1, "service"    # Lcom/sec/android/easyonehand/EasyOneHandService;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "startHandMode"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDM:Landroid/util/DisplayMetrics;

    .line 56
    iput-boolean v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->isEasyOneHandAttached:Z

    .line 58
    iput-boolean v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->isResizingWindowShowing:Z

    .line 60
    iput-boolean v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->isSideWindowShowing:Z

    .line 62
    iput-boolean v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mCheckRotation:Z

    .line 64
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mResizeStartRect:Landroid/graphics/Rect;

    .line 71
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperCPU:Landroid/os/DVFSHelper;

    .line 72
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperGPU:Landroid/os/DVFSHelper;

    .line 73
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperBUS:Landroid/os/DVFSHelper;

    .line 74
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperCore:Landroid/os/DVFSHelper;

    .line 75
    iput-boolean v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSLockAcquired:Z

    .line 76
    iput v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSCookie:I

    .line 77
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHWindowController;->methodPauseGc:Ljava/lang/reflect/Method;

    .line 78
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHWindowController;->methodResumeGc:Ljava/lang/reflect/Method;

    .line 82
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mService:Lcom/sec/android/easyonehand/EasyOneHandService;

    .line 83
    iput-object p2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mContext:Landroid/content/Context;

    .line 85
    invoke-static {}, Lcom/sec/android/easyonehand/EOHUtils;->getInstance()Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    .line 86
    invoke-static {}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getInstance()Lcom/sec/android/easyonehand/EOHWindowInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    .line 87
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0, p2, p3}, Lcom/sec/android/easyonehand/EOHWindowInfo;->init(Landroid/content/Context;I)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDM:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 90
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v0

    const/16 v1, 0x780

    if-lt v0, v1, :cond_0

    .line 91
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->prepareDVFS()V

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->acquireDVFS()V

    .line 95
    :cond_0
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 96
    const-string v0, "EOHWindowController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EOHWindowController() mWinInfo="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    :cond_1
    new-instance v0, Landroid/content/res/Configuration;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mLastConfig:Landroid/content/res/Configuration;

    .line 99
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->calculateWindowRectInfo()V

    .line 101
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/easyonehand/EOHWindowController$WindowHideHandler;

    invoke-direct {v1, p0, v3}, Lcom/sec/android/easyonehand/EOHWindowController$WindowHideHandler;-><init>(Lcom/sec/android/easyonehand/EOHWindowController;Lcom/sec/android/easyonehand/EOHWindowController$1;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mHandler:Landroid/os/Handler;

    .line 103
    new-instance v0, Lcom/sec/android/easyonehand/EOHMainWindow;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isLeftHandMode()Z

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/easyonehand/EOHMainWindow;-><init>(Lcom/sec/android/easyonehand/EOHWindowController;Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

    .line 106
    new-instance v0, Lcom/sec/android/easyonehand/EOHResizeFrameView;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/easyonehand/EOHResizeFrameView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mResizeFrameView:Lcom/sec/android/easyonehand/EOHResizeFrameView;

    .line 107
    new-instance v0, Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;-><init>(Lcom/sec/android/easyonehand/EOHWindowController;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mBackgroundWindow:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    .line 109
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/easyonehand/EOHWindowController;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHWindowController;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private calculateWindowRectInfo()V
    .locals 3

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDM:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 208
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 209
    const-string v0, "EOHWindowController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "calculateWindowRectInfo() width > height. orientation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mCurrentWindowOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->calculateWindowInfo()V

    .line 215
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 216
    const-string v0, "EOHWindowController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "calculateWindowRectInfo() mWinInfo="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private prepareDVFS()V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 113
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 114
    new-instance v0, Landroid/os/DVFSHelper;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mContext:Landroid/content/Context;

    const-string v2, "Reduce screen"

    const/16 v3, 0xc

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperCPU:Landroid/os/DVFSHelper;

    .line 115
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperCPU:Landroid/os/DVFSHelper;

    const-string v1, "ActivityManager_resume"

    invoke-virtual {v0, v1}, Landroid/os/DVFSHelper;->addExtraOptionsByDefaultPolicy(Ljava/lang/String;)V

    .line 117
    new-instance v0, Landroid/os/DVFSHelper;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mContext:Landroid/content/Context;

    const-string v2, "Reduce screen"

    const/16 v3, 0x10

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperGPU:Landroid/os/DVFSHelper;

    .line 118
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperGPU:Landroid/os/DVFSHelper;

    const-string v1, "ActivityManager_resume"

    invoke-virtual {v0, v1}, Landroid/os/DVFSHelper;->addExtraOptionsByDefaultPolicy(Ljava/lang/String;)V

    .line 120
    new-instance v0, Landroid/os/DVFSHelper;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mContext:Landroid/content/Context;

    const-string v2, "Reduce screen"

    const/16 v3, 0x13

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperBUS:Landroid/os/DVFSHelper;

    .line 121
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperBUS:Landroid/os/DVFSHelper;

    const-string v1, "ActivityManager_resume"

    invoke-virtual {v0, v1}, Landroid/os/DVFSHelper;->addExtraOptionsByDefaultPolicy(Ljava/lang/String;)V

    .line 123
    new-instance v0, Landroid/os/DVFSHelper;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mContext:Landroid/content/Context;

    const-string v2, "Reduce screen"

    const/16 v3, 0xe

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperCore:Landroid/os/DVFSHelper;

    .line 124
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperCore:Landroid/os/DVFSHelper;

    const-string v1, "ActivityManager_resume"

    invoke-virtual {v0, v1}, Landroid/os/DVFSHelper;->addExtraOptionsByDefaultPolicy(Ljava/lang/String;)V

    .line 128
    :cond_0
    const-class v6, Ldalvik/system/VMRuntime;

    .line 130
    .local v6, "clazz":Ljava/lang/Class;
    :try_start_0
    const-string v0, "pauseGc"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {v6, v0, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->methodPauseGc:Ljava/lang/reflect/Method;

    .line 131
    const-string v0, "resumeGc"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    invoke-virtual {v6, v0, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->methodResumeGc:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    :goto_0
    return-void

    .line 133
    :catch_0
    move-exception v0

    goto :goto_0

    .line 132
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private updateWindow(ZZZ)V
    .locals 4
    .param p1, "resizeAttribute"    # Z
    .param p2, "resizeScale"    # Z
    .param p3, "updateHandModeBtn"    # Z

    .prologue
    .line 229
    sget-boolean v1, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 230
    const-string v1, "EOHWindowController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " updateWindow resizeAttribute = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", resizeScale = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", updateHandModeBtn = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mWinInfo="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    :cond_0
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDM:Landroid/util/DisplayMetrics;

    invoke-virtual {v1, v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 235
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v2

    if-le v1, v2, :cond_2

    .line 236
    sget-boolean v1, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 237
    const-string v1, "EOHWindowController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateWindow() width > height. orientation="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mCurrentWindowOrientation:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    :cond_1
    :goto_0
    return-void

    .line 241
    :cond_2
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    iget-object v1, v1, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMagnifyRect:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 242
    .local v0, "innerR":Landroid/graphics/Rect;
    invoke-virtual {p0, v0}, Lcom/sec/android/easyonehand/EOHWindowController;->setBackgroundTransparentRect(Landroid/graphics/Rect;)V

    .line 244
    if-eqz p1, :cond_3

    .line 245
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHMainWindow;->setWindowRect()V

    .line 246
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    if-eqz v1, :cond_3

    .line 247
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHSideWindow;->setWindowRect()V

    .line 251
    :cond_3
    if-eqz p2, :cond_5

    .line 252
    sget-boolean v1, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v1, :cond_4

    .line 253
    const-string v1, "EOHWindowController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateWindow() scale="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getWindowScale()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->isAnimationRunning()Z

    move-result v1

    if-nez v1, :cond_5

    .line 255
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getWindowScale()F

    move-result v1

    iget v2, v0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/easyonehand/EOHWindowController;->magnifyWindow(FFF)V

    .line 259
    :cond_5
    if-nez p2, :cond_6

    if-eqz p3, :cond_1

    .line 260
    :cond_6
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mInputFilter:Lcom/sec/android/easyonehand/EOHInputFilter;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHInputFilter;->setWindowChanged()V

    goto :goto_0
.end method


# virtual methods
.method acquireDVFS()V
    .locals 1

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->releaseDVFS()V

    .line 142
    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSLockAcquired:Z

    if-nez v0, :cond_4

    .line 143
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperCPU:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperCPU:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperGPU:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperGPU:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    .line 149
    :cond_1
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperBUS:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_2

    .line 150
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperBUS:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    .line 152
    :cond_2
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperCore:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_3

    .line 153
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperCore:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    .line 155
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSLockAcquired:Z

    .line 157
    :cond_4
    return-void
.end method

.method public adjustResizingFrameView(IIIII)V
    .locals 6
    .param p1, "handleMode"    # I
    .param p2, "dx"    # I
    .param p3, "dy"    # I
    .param p4, "downX"    # I
    .param p5, "downY"    # I

    .prologue
    .line 737
    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->isResizingWindowShowing:Z

    if-eqz v0, :cond_0

    .line 738
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mResizeFrameView:Lcom/sec/android/easyonehand/EOHResizeFrameView;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/easyonehand/EOHResizeFrameView;->adjustBound(IIIII)V

    .line 739
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mResizeFrameView:Lcom/sec/android/easyonehand/EOHResizeFrameView;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHResizeFrameView;->invalidate()V

    .line 741
    :cond_0
    return-void
.end method

.method public adjustWindowSizePosition()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 220
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->calculateWindowRectInfo()V

    .line 221
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isPositionAdjustNeeded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->setSideOffset(I)V

    .line 223
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->calculateWindowRectInfo()V

    .line 225
    :cond_0
    invoke-direct {p0, v2, v2, v2}, Lcom/sec/android/easyonehand/EOHWindowController;->updateWindow(ZZZ)V

    .line 226
    return-void
.end method

.method public changeSettingsHanlder()V
    .locals 0

    .prologue
    .line 675
    return-void
.end method

.method checkDeviceRotation()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 918
    iget-boolean v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->isEasyOneHandAttached:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->isAnimationRunning()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 935
    :cond_0
    :goto_0
    return-void

    .line 922
    :cond_1
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getDisplayRotation()I

    move-result v0

    .line 923
    .local v0, "rot":I
    sget-boolean v1, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v1, :cond_2

    const-string v1, "EOHWindowController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkDeviceRotation() rotation="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 925
    :cond_2
    if-eqz v0, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 929
    :cond_3
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideOffset()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getCocktailBarWidth()I

    move-result v2

    if-ne v1, v2, :cond_4

    .line 930
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1, v4}, Lcom/sec/android/easyonehand/EOHWindowInfo;->setSideOffset(I)V

    .line 932
    :cond_4
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mCheckRotation:Z

    .line 933
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->adjustWindowSizePosition()V

    .line 934
    iput-boolean v4, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mCheckRotation:Z

    goto :goto_0
.end method

.method public cleanupWindows()V
    .locals 3

    .prologue
    .line 614
    sget-boolean v1, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 615
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->cleanupWindowsInternal()V

    .line 627
    :cond_0
    :goto_0
    return-void

    .line 620
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->cleanupWindowsInternal()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 621
    :catch_0
    move-exception v0

    .line 622
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mService:Lcom/sec/android/easyonehand/EasyOneHandService;

    if-eqz v1, :cond_0

    .line 623
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mService:Lcom/sec/android/easyonehand/EasyOneHandService;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/easyonehand/EasyOneHandService;->stopForeground(Z)V

    .line 624
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mService:Lcom/sec/android/easyonehand/EasyOneHandService;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EasyOneHandService;->stopSelf()V

    goto :goto_0
.end method

.method public cleanupWindowsInternal()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 596
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->hideBackground()V

    .line 597
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->finishWindow()V

    .line 598
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    if-eqz v0, :cond_0

    .line 599
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->finishWindow()V

    .line 601
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mBackgroundWindow:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->finishWindow()V

    .line 603
    iput-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    .line 604
    iput-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mBackgroundWindow:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    .line 605
    iput-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

    .line 606
    iput-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mResizeFrameView:Lcom/sec/android/easyonehand/EOHResizeFrameView;

    .line 608
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->storeWindowInfo()V

    .line 609
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mService:Lcom/sec/android/easyonehand/EasyOneHandService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EasyOneHandService;->stopForeground(Z)V

    .line 610
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mService:Lcom/sec/android/easyonehand/EasyOneHandService;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EasyOneHandService;->stopSelf()V

    .line 611
    return-void
.end method

.method public closeSideWindowEditMode()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 863
    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->isSideWindowShowing:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    if-eqz v0, :cond_1

    .line 864
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 865
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v0, v2}, Lcom/sec/android/easyonehand/EOHSideWindow;->setContactEditMode(Z)V

    .line 868
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 869
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v0, v2}, Lcom/sec/android/easyonehand/EOHSideWindow;->setShortcutEditMode(Z)V

    .line 870
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->hideShortcutAddWindow()V

    .line 873
    :cond_1
    return-void
.end method

.method public closeWindow(Z)V
    .locals 4
    .param p1, "bAnimation"    # Z

    .prologue
    const/4 v1, 0x0

    .line 630
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 631
    const-string v0, "EOHWindowController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "closeWindow() : bAnimation="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", this="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", callers="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xa

    invoke-static {v3}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 634
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->isEasyOneHandAttached:Z

    if-nez v0, :cond_1

    .line 662
    :goto_0
    return-void

    .line 638
    :cond_1
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mCurrentWindowOrientation:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    .line 639
    const/4 p1, 0x0

    .line 642
    :cond_2
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mService:Lcom/sec/android/easyonehand/EasyOneHandService;

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EasyOneHandService;->manageProcessForeground(Z)V

    .line 643
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

    if-nez p1, :cond_5

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->stopAllAnimations(Z)V

    .line 644
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    if-eqz v0, :cond_3

    .line 645
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->hideWindow()V

    .line 648
    :cond_3
    iput-boolean v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->isEasyOneHandAttached:Z

    .line 650
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mBackgroundWindow:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->cancelAllMessages()V

    .line 651
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->hideResizingFrameView()V

    .line 653
    if-eqz p1, :cond_6

    .line 654
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v0

    const/16 v1, 0x780

    if-lt v0, v1, :cond_4

    .line 655
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->acquireDVFS()V

    .line 657
    :cond_4
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->runFinishAnimation()V

    goto :goto_0

    :cond_5
    move v0, v1

    .line 643
    goto :goto_1

    .line 659
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->cleanupWindows()V

    goto :goto_0
.end method

.method closeWindowBySPen()V
    .locals 2

    .prologue
    .line 665
    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->isEasyOneHandAttached:Z

    if-nez v0, :cond_0

    .line 672
    :goto_0
    return-void

    .line 669
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/easyonehand/EOHWindowController;->closeWindow(Z)V

    .line 671
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    const v1, 0x7f070010

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHUtils;->showToast(I)V

    goto :goto_0
.end method

.method fillSideWidnowGridView()V
    .locals 3

    .prologue
    .line 895
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 896
    const-string v0, "EOHWindowController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fillSideWidnowGridView() mSideWindow="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", sideWindowType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 898
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowType()I

    move-result v0

    if-nez v0, :cond_2

    .line 913
    :cond_1
    :goto_0
    return-void

    .line 902
    :cond_2
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    if-nez v0, :cond_3

    .line 903
    new-instance v0, Lcom/sec/android/easyonehand/EOHSideWindow;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isLeftHandMode()Z

    move-result v2

    invoke-direct {v0, v1, p0, v2}, Lcom/sec/android/easyonehand/EOHSideWindow;-><init>(Landroid/content/Context;Lcom/sec/android/easyonehand/EOHWindowController;Z)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    .line 904
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->createWindowLayout()V

    .line 905
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->setWindowRect()V

    .line 908
    :cond_3
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 909
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->fillContactGridView()V

    goto :goto_0

    .line 910
    :cond_4
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 911
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->fillShortcutGridView()V

    goto :goto_0
.end method

.method public getCurrentResizwWindowBounds()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 751
    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->isResizingWindowShowing:Z

    if-eqz v0, :cond_0

    .line 752
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mResizeFrameView:Lcom/sec/android/easyonehand/EOHResizeFrameView;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHResizeFrameView;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 754
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hideAllWindows()V
    .locals 1

    .prologue
    .line 781
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    if-eqz v0, :cond_0

    .line 782
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->hideWindow()V

    .line 784
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->hideWindow()V

    .line 785
    return-void
.end method

.method public hideBackground()V
    .locals 1

    .prologue
    .line 798
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mBackgroundWindow:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    if-eqz v0, :cond_0

    .line 799
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mBackgroundWindow:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->hideBackground()V

    .line 801
    :cond_0
    return-void
.end method

.method public hideResizingFrameView()V
    .locals 2

    .prologue
    .line 771
    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->isResizingWindowShowing:Z

    if-nez v0, :cond_0

    .line 778
    :goto_0
    return-void

    .line 774
    :cond_0
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 775
    const-string v0, "EOHWindowController"

    const-string v1, "hideResizingFrameView()"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 776
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->isResizingWindowShowing:Z

    .line 777
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mResizeFrameView:Lcom/sec/android/easyonehand/EOHResizeFrameView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public hideSideWindow()V
    .locals 1

    .prologue
    .line 851
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

    if-nez v0, :cond_0

    .line 860
    :goto_0
    return-void

    .line 855
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->isSideWindowShowing:Z

    .line 856
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    if-eqz v0, :cond_1

    .line 857
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->hideWindow()V

    .line 859
    :cond_1
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->setSideMenuIconState()V

    goto :goto_0
.end method

.method public isAnimationRunning()Z
    .locals 1

    .prologue
    .line 570
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->isAnimationRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAttached()Z
    .locals 1

    .prologue
    .line 566
    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->isEasyOneHandAttached:Z

    return v0
.end method

.method public magnifyWindow(FFF)V
    .locals 12
    .param p1, "scale"    # F
    .param p2, "offsetX"    # F
    .param p3, "offsetY"    # F

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    .line 519
    sget-boolean v2, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 520
    const-string v2, "EOHWindowController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "magnifyWindow() scale="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", offsetX="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", offsetY="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", mCheckRotation="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v6, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mCheckRotation:Z

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", callers="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v6, 0xa

    invoke-static {v6}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mCheckRotation:Z

    if-eqz v2, :cond_1

    .line 548
    :goto_0
    return-void

    .line 528
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getWindowScale()F

    move-result v2

    cmpl-float v2, p1, v2

    if-eqz v2, :cond_2

    cmpl-float v2, p1, v7

    if-nez v2, :cond_8

    .line 529
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->releaseDVFS()V

    .line 531
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "any_screen_running_scale"

    invoke-static {v2, v3, p1}, Landroid/provider/Settings$Secure;->putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z

    .line 532
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "any_screen_running_offset_x"

    float-to-int v6, p2

    invoke-static {v2, v3, v6}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 533
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "any_screen_running_offset_y"

    float-to-int v6, p3

    invoke-static {v2, v3, v6}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 535
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mInputFilter:Lcom/sec/android/easyonehand/EOHInputFilter;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHInputFilter;->isRegistered()Z

    move-result v11

    .line 536
    .local v11, "isRegistered":Z
    if-eqz v11, :cond_3

    cmpl-float v2, p1, v7

    if-eqz v2, :cond_4

    :cond_3
    if-nez v11, :cond_5

    cmpg-float v2, p1, v7

    if-gez v2, :cond_5

    :cond_4
    move v4, v0

    .line 538
    .local v4, "changeFilter":Z
    :goto_1
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mInputFilter:Lcom/sec/android/easyonehand/EOHInputFilter;

    cmpg-float v3, p1, v7

    if-gez v3, :cond_6

    :goto_2
    invoke-virtual {v2, v0}, Lcom/sec/android/easyonehand/EOHInputFilter;->registerInputFilter(Z)V

    .line 539
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getIWindowManager()Landroid/view/IWindowManager;

    move-result-object v0

    cmpl-float v1, p1, v7

    if-nez v1, :cond_7

    :goto_3
    move v1, p1

    move v2, p2

    move v3, p3

    invoke-interface/range {v0 .. v5}, Landroid/view/IWindowManager;->changeDisplayScale(FFFZLandroid/view/IInputFilter;)V

    goto :goto_0

    .line 545
    .end local v4    # "changeFilter":Z
    .end local v11    # "isRegistered":Z
    :catch_0
    move-exception v0

    goto :goto_0

    .restart local v11    # "isRegistered":Z
    :cond_5
    move v4, v1

    .line 536
    goto :goto_1

    .restart local v4    # "changeFilter":Z
    :cond_6
    move v0, v1

    .line 538
    goto :goto_2

    .line 539
    :cond_7
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mInputFilter:Lcom/sec/android/easyonehand/EOHInputFilter;

    goto :goto_3

    .line 542
    .end local v4    # "changeFilter":Z
    .end local v11    # "isRegistered":Z
    :cond_8
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getIWindowManager()Landroid/view/IWindowManager;

    move-result-object v5

    const/4 v9, 0x0

    const/4 v10, 0x0

    move v6, p1

    move v7, p2

    move v8, p3

    invoke-interface/range {v5 .. v10}, Landroid/view/IWindowManager;->changeDisplayScale(FFFZLandroid/view/IInputFilter;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 546
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public moveResizingFrameView(II)V
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 744
    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->isResizingWindowShowing:Z

    if-eqz v0, :cond_0

    .line 745
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mResizeFrameView:Lcom/sec/android/easyonehand/EOHResizeFrameView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/easyonehand/EOHResizeFrameView;->moveBound(II)V

    .line 746
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mResizeFrameView:Lcom/sec/android/easyonehand/EOHResizeFrameView;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHResizeFrameView;->invalidate()V

    .line 748
    :cond_0
    return-void
.end method

.method public notifyDragDropEvent(Ljava/lang/String;Ljava/lang/String;FF)V
    .locals 6
    .param p1, "desc"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/String;
    .param p3, "x"    # F
    .param p4, "y"    # F

    .prologue
    .line 946
    sget-boolean v3, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 947
    const-string v3, "EOHWindowController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "notifyDragDropEvent() x="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", y="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", desc="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", data="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 949
    :cond_0
    if-eqz p2, :cond_1

    if-nez p1, :cond_2

    .line 979
    :cond_1
    :goto_0
    return-void

    .line 953
    :cond_2
    iget-boolean v3, p0, Lcom/sec/android/easyonehand/EOHWindowController;->isSideWindowShowing:Z

    if-eqz v3, :cond_1

    .line 958
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    float-to-int v4, p3

    invoke-virtual {v3, v4}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getRawX(I)I

    move-result v1

    .line 959
    .local v1, "px":I
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    float-to-int v4, p4

    invoke-virtual {v3, v4}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getRawY(I)I

    move-result v2

    .line 961
    .local v2, "py":I
    sget-boolean v3, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v3, :cond_3

    .line 962
    const-string v3, "EOHWindowController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "notifyDragDropEvent() isSideWindowShowing="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/sec/android/easyonehand/EOHWindowController;->isSideWindowShowing:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", sideType="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v5}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowType()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", pos=("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    float-to-int v5, p3

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    float-to-int v5, p4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") raw=("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",) data="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 966
    :cond_3
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowType()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    .line 967
    const-string v3, "lookupUri"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 968
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v3, p2, v1, v2}, Lcom/sec/android/easyonehand/EOHSideWindow;->addContactIcon(Ljava/lang/String;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 975
    .end local v1    # "px":I
    .end local v2    # "py":I
    :catch_0
    move-exception v0

    .line 976
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "EOHWindowController"

    const-string v4, "notifyDragDropEvent() failed"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 970
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "px":I
    .restart local v2    # "py":I
    :cond_4
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowType()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 971
    const-string v3, "componentName"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 972
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v3, p2, v1, v2}, Lcom/sec/android/easyonehand/EOHSideWindow;->addShortcutIcon(Ljava/lang/String;II)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v5, 0x1

    .line 679
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->isAttached()Z

    move-result v2

    if-nez v2, :cond_1

    .line 731
    :cond_0
    :goto_0
    return-void

    .line 683
    :cond_1
    sget-boolean v2, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v2, :cond_2

    .line 684
    const-string v2, "EOHWindowController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "newConfig.orientation="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mLastOrientation="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mLastConfig:Landroid/content/res/Configuration;

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    :cond_2
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mLastConfig:Landroid/content/res/Configuration;

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iget-object v3, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 688
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHMainWindow;->onFontLocaleChanged()V

    .line 691
    :cond_3
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mLastConfig:Landroid/content/res/Configuration;

    iget v0, v2, Landroid/content/res/Configuration;->orientation:I

    .line 692
    .local v0, "lastOrientation":I
    new-instance v2, Landroid/content/res/Configuration;

    invoke-direct {v2, p1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iput-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mLastConfig:Landroid/content/res/Configuration;

    .line 693
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v2, v0, :cond_0

    .line 697
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->updateDisplayMatrix()V

    .line 699
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    iput v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mCurrentWindowOrientation:I

    .line 700
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mInputFilter:Lcom/sec/android/easyonehand/EOHInputFilter;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHInputFilter;->clearAllTouchEvents()V

    .line 702
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->isAnimationRunning()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 703
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/easyonehand/EOHWindowController;->closeWindow(Z)V

    goto :goto_0

    .line 707
    :cond_4
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    .line 709
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->hideResizingFrameView()V

    .line 710
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->hideBackground()V

    .line 711
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHMainWindow;->setLandscapeMode()V

    .line 712
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    if-eqz v2, :cond_0

    .line 713
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHSideWindow;->setLandscapeMode()V

    goto :goto_0

    .line 716
    :cond_5
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDM:Landroid/util/DisplayMetrics;

    invoke-virtual {v2, v3}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 718
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mService:Lcom/sec/android/easyonehand/EasyOneHandService;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EasyOneHandService;->getSettingsObserver()Lcom/sec/android/easyonehand/EOHSettingsObserver;

    move-result-object v1

    .line 719
    .local v1, "observer":Lcom/sec/android/easyonehand/EOHSettingsObserver;
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->getRunningSettingState()I

    move-result v2

    if-eqz v2, :cond_6

    .line 720
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mService:Lcom/sec/android/easyonehand/EasyOneHandService;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EasyOneHandService;->forceStopService()V

    goto/16 :goto_0

    .line 724
    :cond_6
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHMainWindow;->setPortraitMode()V

    .line 725
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    if-eqz v2, :cond_7

    .line 726
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHSideWindow;->setPortraitMode()V

    .line 728
    :cond_7
    const/16 v2, 0x8cf

    invoke-virtual {p0, v2}, Lcom/sec/android/easyonehand/EOHWindowController;->showBackground(I)V

    .line 729
    invoke-direct {p0, v5, v5, v5}, Lcom/sec/android/easyonehand/EOHWindowController;->updateWindow(ZZZ)V

    goto/16 :goto_0
.end method

.method releaseDVFS()V
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSLockAcquired:Z

    if-eqz v0, :cond_4

    .line 161
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperCPU:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperCPU:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperCore:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_1

    .line 165
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperCore:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperGPU:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_2

    .line 168
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperGPU:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    .line 170
    :cond_2
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperBUS:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_3

    .line 171
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSHelperBUS:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    .line 173
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mDVFSLockAcquired:Z

    .line 175
    :cond_4
    return-void
.end method

.method public resizeWindow(Landroid/graphics/Rect;Z)V
    .locals 13
    .param p1, "r"    # Landroid/graphics/Rect;
    .param p2, "bUpdate"    # Z

    .prologue
    const/4 v12, 0x1

    .line 459
    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v9}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v6

    .line 460
    .local v6, "screenW":I
    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v9}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v5

    .line 463
    .local v5, "screenH":I
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 465
    .local v2, "newRect":Landroid/graphics/Rect;
    iget v1, v2, Landroid/graphics/Rect;->left:I

    .line 466
    .local v1, "leftGap":I
    iget v9, v2, Landroid/graphics/Rect;->right:I

    sub-int v3, v6, v9

    .line 467
    .local v3, "rightGap":I
    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v9}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowWidth()I

    move-result v8

    .line 469
    .local v8, "sideWidth":I
    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v9}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isSideWindowShowing()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 470
    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v9}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isLeftHandMode()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 471
    if-ge v3, v1, :cond_4

    .line 472
    iget v9, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v9, v8

    iput v9, v2, Landroid/graphics/Rect;->left:I

    .line 485
    :cond_0
    :goto_0
    sget-boolean v9, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v9, :cond_1

    .line 486
    const-string v9, "EOHWindowController"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "resizeWindow() r="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", newRect="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", bUpdate="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    :cond_1
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v10}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideMargin()I

    move-result v10

    mul-int/lit8 v10, v10, 0x2

    sub-int/2addr v9, v10

    int-to-float v9, v9

    iget-object v10, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v10}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v10

    int-to-float v10, v10

    div-float v4, v9, v10

    .line 491
    .local v4, "scale":F
    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v9}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isLeftHandMode()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 492
    iget v7, v2, Landroid/graphics/Rect;->left:I

    .line 497
    .local v7, "sideOffset":I
    :goto_1
    iget v9, v2, Landroid/graphics/Rect;->top:I

    sub-int v9, v5, v9

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v10

    sub-int v0, v9, v10

    .line 499
    .local v0, "bottomOffset":I
    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v9, v4}, Lcom/sec/android/easyonehand/EOHWindowInfo;->setWindowScale(F)V

    .line 500
    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v9, v7}, Lcom/sec/android/easyonehand/EOHWindowInfo;->setSideOffset(I)V

    .line 501
    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v9, v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->setBottomOffset(I)V

    .line 503
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->calculateWindowRectInfo()V

    .line 505
    sget-boolean v9, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v9, :cond_2

    .line 506
    const-string v9, "EOHWindowController"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "resizeWindow mWinInfo="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    :cond_2
    if-eqz p2, :cond_3

    .line 509
    invoke-direct {p0, v12, v12, v12}, Lcom/sec/android/easyonehand/EOHWindowController;->updateWindow(ZZZ)V

    .line 511
    :cond_3
    return-void

    .line 474
    .end local v0    # "bottomOffset":I
    .end local v4    # "scale":F
    .end local v7    # "sideOffset":I
    :cond_4
    iget v9, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v9, v8

    iput v9, v2, Landroid/graphics/Rect;->right:I

    goto/16 :goto_0

    .line 477
    :cond_5
    if-ge v1, v3, :cond_6

    .line 478
    iget v9, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v9, v8

    iput v9, v2, Landroid/graphics/Rect;->right:I

    goto/16 :goto_0

    .line 480
    :cond_6
    iget v9, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v9, v8

    iput v9, v2, Landroid/graphics/Rect;->left:I

    goto/16 :goto_0

    .line 494
    .restart local v4    # "scale":F
    :cond_7
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v9

    iget v10, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v9, v10

    sub-int v7, v6, v9

    .restart local v7    # "sideOffset":I
    goto :goto_1
.end method

.method public setBackgroundTransparentRect(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "r"    # Landroid/graphics/Rect;

    .prologue
    .line 810
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mBackgroundWindow:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    if-eqz v0, :cond_0

    .line 811
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mBackgroundWindow:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    invoke-virtual {v0, p1}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->setBackgroundTransparentRect(Landroid/graphics/Rect;)V

    .line 813
    :cond_0
    return-void
.end method

.method public setDragMode(Z)V
    .locals 1
    .param p1, "isDrag"    # Z

    .prologue
    .line 942
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mInputFilter:Lcom/sec/android/easyonehand/EOHInputFilter;

    invoke-virtual {v0, p1}, Lcom/sec/android/easyonehand/EOHInputFilter;->setDragMode(Z)V

    .line 943
    return-void
.end method

.method public setInputFilter(Lcom/sec/android/easyonehand/EOHInputFilter;)V
    .locals 0
    .param p1, "filter"    # Lcom/sec/android/easyonehand/EOHInputFilter;

    .prologue
    .line 551
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mInputFilter:Lcom/sec/android/easyonehand/EOHInputFilter;

    .line 552
    return-void
.end method

.method public showAllWindows()V
    .locals 2

    .prologue
    .line 788
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 789
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->showWindow()V

    .line 790
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->showSideWindow()V

    .line 792
    :cond_0
    return-void
.end method

.method public showBackground(I)V
    .locals 2
    .param p1, "layer"    # I

    .prologue
    .line 804
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mBackgroundWindow:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mCurrentWindowOrientation:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 805
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mBackgroundWindow:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    invoke-virtual {v0, p1}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->showBackground(I)V

    .line 807
    :cond_0
    return-void
.end method

.method public showResizingFrameView(I)V
    .locals 3
    .param p1, "handleMode"    # I

    .prologue
    .line 758
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->isResizingWindowShowing:Z

    if-eqz v0, :cond_1

    .line 768
    :cond_0
    :goto_0
    return-void

    .line 761
    :cond_1
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 762
    const-string v0, "EOHWindowController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showResizingFrameView() handleMode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 763
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->isResizingWindowShowing:Z

    .line 765
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mResizeFrameView:Lcom/sec/android/easyonehand/EOHResizeFrameView;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mResizeFrameView:Lcom/sec/android/easyonehand/EOHResizeFrameView;

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/easyonehand/EOHResizeFrameView;->showResizingFrameView(Lcom/sec/android/easyonehand/EOHResizeFrameView;I)V

    .line 766
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mResizeStartRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->getCurrentResizwWindowBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public showSideWindow()V
    .locals 3

    .prologue
    .line 819
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

    if-nez v0, :cond_1

    .line 848
    :cond_0
    :goto_0
    return-void

    .line 823
    :cond_1
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 824
    const-string v0, "EOHWindowController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showSideWindow() isSideWindowShowing()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isSideWindowShowing()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", isKeyguardLocked()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHUtils;->isKeyguardLocked()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", callers="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0xa

    invoke-static {v2}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 827
    :cond_2
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->setSideMenuIconState()V

    .line 828
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isSideWindowShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 832
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHUtils;->isKeyguardLocked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 836
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    if-nez v0, :cond_3

    .line 837
    new-instance v0, Lcom/sec/android/easyonehand/EOHSideWindow;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isLeftHandMode()Z

    move-result v2

    invoke-direct {v0, v1, p0, v2}, Lcom/sec/android/easyonehand/EOHSideWindow;-><init>(Landroid/content/Context;Lcom/sec/android/easyonehand/EOHWindowController;Z)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    .line 838
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->createWindowLayout()V

    .line 839
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->setWindowRect()V

    .line 841
    :cond_3
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->showWindow()V

    .line 843
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->isSideWindowShowing:Z

    .line 845
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isPositionAdjustNeeded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 846
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->adjustWindowSizePosition()V

    goto/16 :goto_0
.end method

.method public showWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 574
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 575
    const-string v0, "EOHWindowController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showWindow() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->isEasyOneHandAttached:Z

    if-eqz v0, :cond_2

    .line 593
    :cond_1
    :goto_0
    return-void

    .line 581
    :cond_2
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v1

    if-gt v0, v1, :cond_1

    .line 585
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->createWindowLayout()V

    .line 586
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    if-eqz v0, :cond_3

    .line 587
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->createWindowLayout()V

    .line 590
    :cond_3
    iput-boolean v3, p0, Lcom/sec/android/easyonehand/EOHWindowController;->isEasyOneHandAttached:Z

    .line 591
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mService:Lcom/sec/android/easyonehand/EasyOneHandService;

    invoke-virtual {v0, v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->manageProcessForeground(Z)V

    .line 592
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->startWindow()V

    goto :goto_0
.end method

.method public startResizeTouchIntercept()V
    .locals 2

    .prologue
    .line 555
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mInputFilter:Lcom/sec/android/easyonehand/EOHInputFilter;

    iget-object v1, v1, Lcom/sec/android/easyonehand/EOHInputFilter;->mResizeInputIntercepter:Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->isInterceptMotionEvt()Z

    move-result v1

    if-nez v1, :cond_0

    .line 556
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isLeftHandMode()Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x6a

    .line 559
    .local v0, "handleMode":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHUtils;->userActivity()V

    .line 560
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mInputFilter:Lcom/sec/android/easyonehand/EOHInputFilter;

    iget-object v1, v1, Lcom/sec/android/easyonehand/EOHInputFilter;->mResizeInputIntercepter:Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;

    invoke-virtual {v1, v0}, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->startIntercept(I)V

    .line 561
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHUtils;->playShortHaptic()V

    .line 563
    .end local v0    # "handleMode":I
    :cond_0
    return-void

    .line 556
    :cond_1
    const/16 v0, 0x69

    goto :goto_0
.end method

.method public toggleSideWindow(I)V
    .locals 3
    .param p1, "sideWindowType"    # I

    .prologue
    .line 876
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 877
    const-string v0, "EOHWindowController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "toggleSideWindow current="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", new="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 879
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->isAnimationRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 892
    :goto_0
    return-void

    .line 883
    :cond_1
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowType()I

    move-result v0

    if-ne v0, p1, :cond_2

    .line 884
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->setSideWindowType(I)V

    .line 885
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->hideSideWindow()V

    goto :goto_0

    .line 890
    :cond_2
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0, p1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->setSideWindowType(I)V

    .line 891
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->showSideWindow()V

    goto :goto_0
.end method

.method public updateEasyOneHandWindowLayout()V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v9, 0x0

    .line 374
    iget-object v10, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v10}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v7

    .line 375
    .local v7, "screenW":I
    const/4 v6, 0x0

    .line 377
    .local v6, "redrawNeeded":Z
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->getCurrentResizwWindowBounds()Landroid/graphics/Rect;

    move-result-object v3

    .line 379
    .local v3, "curBound":Landroid/graphics/Rect;
    sget-boolean v10, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v10, :cond_0

    .line 380
    const-string v10, "EOHWindowController"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "updateEasyOneHandWindowLayout() mResizeStartRect="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mResizeStartRect:Landroid/graphics/Rect;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", curBound="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    :cond_0
    if-eqz v3, :cond_1

    iget-object v10, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mResizeStartRect:Landroid/graphics/Rect;

    if-nez v10, :cond_2

    .line 456
    :cond_1
    :goto_0
    return-void

    .line 387
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->hideResizingFrameView()V

    .line 389
    iget-object v10, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mResizeStartRect:Landroid/graphics/Rect;

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v10

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v11

    if-eq v10, v11, :cond_3

    iget-object v10, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mResizeStartRect:Landroid/graphics/Rect;

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v10

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v11

    sub-int/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v10

    div-int/lit16 v11, v7, 0x96

    if-ge v10, v11, :cond_3

    .line 391
    sget-boolean v9, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v9, :cond_1

    const-string v9, "EOHWindowController"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "updateEasyOneHandWindowLayout() too small resize return. mResizeStartRect="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mResizeStartRect:Landroid/graphics/Rect;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", curBound="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 395
    :cond_3
    const/4 v2, 0x0

    .line 396
    .local v2, "cocktailRight":I
    const/4 v1, 0x0

    .line 397
    .local v1, "cocktailLeft":I
    iget-object v10, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v10}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isCocktailBarEnabled()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 398
    iget-object v10, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v10}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getDisplayRotation()I

    move-result v4

    .line 399
    .local v4, "dispRotation":I
    const/4 v10, 0x2

    if-ne v4, v10, :cond_c

    iget-object v10, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v10}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getCocktailBarWidth()I

    move-result v1

    .line 400
    :goto_1
    if-nez v4, :cond_d

    iget-object v10, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v10}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getCocktailBarWidth()I

    move-result v2

    .line 403
    .end local v4    # "dispRotation":I
    :cond_4
    :goto_2
    iget v10, v3, Landroid/graphics/Rect;->left:I

    iget-object v11, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v11}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideFitWidth()I

    move-result v11

    add-int/2addr v11, v1

    if-ge v10, v11, :cond_e

    .line 404
    iget v10, v3, Landroid/graphics/Rect;->right:I

    iget v11, v3, Landroid/graphics/Rect;->left:I

    sub-int v11, v1, v11

    sub-int/2addr v10, v11

    iput v10, v3, Landroid/graphics/Rect;->right:I

    .line 405
    iput v1, v3, Landroid/graphics/Rect;->left:I

    .line 411
    :cond_5
    :goto_3
    iget-object v10, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    iget-object v11, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mResizeStartRect:Landroid/graphics/Rect;

    invoke-virtual {v10, v11, v3}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isFrameRedrawNeeded(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v6

    .line 412
    if-eqz v6, :cond_6

    .line 413
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->hideAllWindows()V

    .line 415
    :cond_6
    invoke-virtual {p0, v3, v9}, Lcom/sec/android/easyonehand/EOHWindowController;->resizeWindow(Landroid/graphics/Rect;Z)V

    .line 417
    new-instance v5, Landroid/graphics/Rect;

    iget-object v10, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    iget-object v10, v10, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMainRect:Landroid/graphics/Rect;

    invoke-direct {v5, v10}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 419
    .local v5, "outerR":Landroid/graphics/Rect;
    iget v10, v5, Landroid/graphics/Rect;->right:I

    iget v11, v5, Landroid/graphics/Rect;->left:I

    sub-int/2addr v10, v11

    sub-int v10, v7, v10

    div-int/lit8 v8, v10, 0x2

    .line 420
    .local v8, "sideMargin":I
    iget-object v10, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v10}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isLeftHandMode()Z

    move-result v0

    .line 422
    .local v0, "bOldMode":Z
    iget-object v10, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v10}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isLeftHandMode()Z

    move-result v10

    if-eqz v10, :cond_f

    iget v10, v5, Landroid/graphics/Rect;->left:I

    if-le v10, v8, :cond_f

    .line 423
    iget-object v10, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v10, v9}, Lcom/sec/android/easyonehand/EOHWindowInfo;->setLeftHandMode(Z)V

    .line 424
    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    iget v10, v5, Landroid/graphics/Rect;->right:I

    sub-int v10, v7, v10

    invoke-virtual {v9, v10}, Lcom/sec/android/easyonehand/EOHWindowInfo;->setSideOffset(I)V

    .line 431
    :cond_7
    :goto_4
    sget-boolean v9, Lcom/sec/android/easyonehand/EOHWindowController;->DEBUG:Z

    if-eqz v9, :cond_8

    .line 432
    const-string v9, "EOHWindowController"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "updateEasyOneHandWindowLayout() redrawNeeded="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", OldMode="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", mLeftHandMode="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v11}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isLeftHandMode()Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", SideOffset="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v11}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideOffset()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowController;->adjustWindowSizePosition()V

    .line 437
    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v9}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isLeftHandMode()Z

    move-result v9

    if-eq v0, v9, :cond_a

    .line 438
    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    if-eqz v9, :cond_9

    .line 439
    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v9}, Lcom/sec/android/easyonehand/EOHSideWindow;->changeViewLayout()V

    .line 441
    :cond_9
    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

    invoke-virtual {v9}, Lcom/sec/android/easyonehand/EOHMainWindow;->changeViewLayout()V

    .line 444
    :cond_a
    if-eqz v6, :cond_1

    .line 445
    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v9}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isSideWindowShowing()Z

    move-result v9

    if-eqz v9, :cond_b

    .line 446
    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    if-eqz v9, :cond_b

    .line 447
    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v9}, Lcom/sec/android/easyonehand/EOHSideWindow;->changeViewLayoutAnimation()V

    .line 451
    :cond_b
    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

    invoke-virtual {v9}, Lcom/sec/android/easyonehand/EOHMainWindow;->changeViewLayoutAnimation()V

    .line 452
    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mHandler:Landroid/os/Handler;

    const-wide/16 v10, 0x32

    invoke-virtual {v9, v13, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .end local v0    # "bOldMode":Z
    .end local v5    # "outerR":Landroid/graphics/Rect;
    .end local v8    # "sideMargin":I
    .restart local v4    # "dispRotation":I
    :cond_c
    move v1, v9

    .line 399
    goto/16 :goto_1

    :cond_d
    move v2, v9

    .line 400
    goto/16 :goto_2

    .line 406
    .end local v4    # "dispRotation":I
    :cond_e
    iget v10, v3, Landroid/graphics/Rect;->right:I

    sub-int v11, v7, v2

    iget-object v12, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v12}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideFitWidth()I

    move-result v12

    sub-int/2addr v11, v12

    if-le v10, v11, :cond_5

    .line 407
    sub-int v10, v7, v2

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v11

    sub-int/2addr v10, v11

    iput v10, v3, Landroid/graphics/Rect;->left:I

    .line 408
    sub-int v10, v7, v2

    iput v10, v3, Landroid/graphics/Rect;->right:I

    goto/16 :goto_3

    .line 426
    .restart local v0    # "bOldMode":Z
    .restart local v5    # "outerR":Landroid/graphics/Rect;
    .restart local v8    # "sideMargin":I
    :cond_f
    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v9}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isLeftHandMode()Z

    move-result v9

    if-nez v9, :cond_7

    iget v9, v5, Landroid/graphics/Rect;->left:I

    if-ge v9, v8, :cond_7

    .line 427
    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v9, v13}, Lcom/sec/android/easyonehand/EOHWindowInfo;->setLeftHandMode(Z)V

    .line 428
    iget-object v9, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    iget v10, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v9, v10}, Lcom/sec/android/easyonehand/EOHWindowInfo;->setSideOffset(I)V

    goto/16 :goto_4
.end method

.method updatePackageList(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 982
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mMainWindow:Lcom/sec/android/easyonehand/EOHMainWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    if-eqz v0, :cond_0

    .line 983
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowController;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v0, p1}, Lcom/sec/android/easyonehand/EOHSideWindow;->updatePackageList(Landroid/content/Intent;)V

    .line 985
    :cond_0
    return-void
.end method
