.class final Lcom/sec/android/easyonehand/EOHShortcutAddWindow;
.super Ljava/lang/Object;
.source "EOHShortcutAddWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;,
        Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ProgressTask;
    }
.end annotation


# static fields
.field static final DEBUG:Z


# instance fields
.field private final alphabeticCompare:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field mAppListEditKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mContext:Landroid/content/Context;

.field private mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

.field private mGridAdapter:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;

.field private mGridView:Landroid/widget/GridView;

.field private mMainDialog:Landroid/app/Dialog;

.field private mMainDialogWindow:Landroid/view/Window;

.field private mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

.field private mMainView:Landroid/view/View;

.field private mProgressCircle:Landroid/widget/ProgressBar;

.field private mProgressTask:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ProgressTask;

.field private mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

.field private mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/easyonehand/EOHSideWindow;Landroid/content/Context;)V
    .locals 3
    .param p1, "sideWindow"    # Lcom/sec/android/easyonehand/EOHSideWindow;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    new-instance v0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$1;

    invoke-direct {v0, p0}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$1;-><init>(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mAppListEditKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 297
    new-instance v0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$2;

    invoke-direct {v0, p0}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$2;-><init>(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->alphabeticCompare:Ljava/util/Comparator;

    .line 75
    iput-object p2, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mContext:Landroid/content/Context;

    .line 77
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    .line 78
    invoke-static {}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getInstance()Lcom/sec/android/easyonehand/EOHWindowInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    .line 79
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mContext:Landroid/content/Context;

    const v1, 0x7f030005

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainView:Landroid/view/View;

    .line 80
    new-instance v0, Landroid/app/Dialog;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainDialog:Landroid/app/Dialog;

    .line 82
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainDialogWindow:Landroid/view/Window;

    .line 83
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainDialogWindow:Landroid/view/Window;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 84
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainDialogWindow:Landroid/view/Window;

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainDialog:Landroid/app/Dialog;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->createWindowLayout()V

    .line 88
    invoke-static {}, Lcom/sec/android/easyonehand/EOHUtils;->getInstance()Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    .line 90
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainDialog:Landroid/app/Dialog;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mAppListEditKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->showWindow()V

    .line 94
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainView:Landroid/view/View;

    const v1, 0x7f09002c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mProgressCircle:Landroid/widget/ProgressBar;

    .line 95
    new-instance v0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ProgressTask;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ProgressTask;-><init>(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;Lcom/sec/android/easyonehand/EOHShortcutAddWindow$1;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mProgressTask:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ProgressTask;

    .line 96
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mProgressTask:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ProgressTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ProgressTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 98
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 99
    const-string v0, "EOHShortcutAddWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EOHShortcutAddWindow() mMainDialog="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainDialog:Landroid/app/Dialog;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mMainView="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainView:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    :cond_0
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mProgressCircle:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;Landroid/widget/ProgressBar;)Landroid/widget/ProgressBar;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHShortcutAddWindow;
    .param p1, "x1"    # Landroid/widget/ProgressBar;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mProgressCircle:Landroid/widget/ProgressBar;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)Landroid/widget/GridView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mGridView:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;Landroid/widget/GridView;)Landroid/widget/GridView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHShortcutAddWindow;
    .param p1, "x1"    # Landroid/widget/GridView;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mGridView:Landroid/widget/GridView;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mGridAdapter:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)Lcom/sec/android/easyonehand/EOHUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)Lcom/sec/android/easyonehand/EOHWindowInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    return-object v0
.end method

.method private cleanUpMemory()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 215
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mGridView:Landroid/widget/GridView;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mGridAdapter:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;

    if-nez v7, :cond_1

    .line 255
    :cond_0
    :goto_0
    return-void

    .line 219
    :cond_1
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v7}, Landroid/widget/GridView;->getCount()I

    move-result v2

    .line 221
    .local v2, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v2, :cond_4

    .line 222
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v7, v4}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 223
    .local v1, "convertView":Landroid/view/View;
    if-eqz v1, :cond_3

    .line 224
    const v7, 0x7f090028

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    .line 225
    .local v5, "iconImage":Landroid/widget/ImageButton;
    const v7, 0x7f090029

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 226
    .local v6, "iconLabel":Landroid/widget/TextView;
    if-eqz v5, :cond_2

    .line 228
    :try_start_0
    invoke-virtual {v5}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    check-cast v7, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 235
    :goto_2
    invoke-virtual {v5}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 236
    invoke-virtual {v5, v10}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 237
    invoke-virtual {v5, v10}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 238
    invoke-virtual {v5, v10}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 239
    invoke-virtual {v5, v10}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 241
    :cond_2
    const/4 v5, 0x0

    .line 243
    if-eqz v6, :cond_3

    .line 244
    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 221
    .end local v5    # "iconImage":Landroid/widget/ImageButton;
    .end local v6    # "iconLabel":Landroid/widget/TextView;
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 231
    .restart local v5    # "iconImage":Landroid/widget/ImageButton;
    .restart local v6    # "iconLabel":Landroid/widget/TextView;
    :catch_0
    move-exception v3

    .line 232
    .local v3, "e":Ljava/lang/Exception;
    const-string v7, "EOHShortcutAddWindow"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception e="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 250
    .end local v1    # "convertView":Landroid/view/View;
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v5    # "iconImage":Landroid/widget/ImageButton;
    .end local v6    # "iconLabel":Landroid/widget/TextView;
    :cond_4
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v7}, Landroid/widget/GridView;->destroyDrawingCache()V

    .line 251
    iput-object v10, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mGridView:Landroid/widget/GridView;

    .line 253
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mGridAdapter:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;

    invoke-virtual {v7}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->cleanUpMemory()V

    .line 254
    iput-object v10, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mGridAdapter:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;

    goto :goto_0
.end method


# virtual methods
.method public createWindowLayout()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 123
    const/16 v1, 0x8cc

    .line 127
    .local v1, "windowType":I
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const v2, 0x1000540

    const/4 v3, -0x3

    invoke-direct {v0, v1, v2, v3}, Landroid/view/WindowManager$LayoutParams;-><init>(III)V

    .line 134
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 135
    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 136
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 137
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 139
    const/16 v2, 0x33

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 140
    const/4 v2, 0x3

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    .line 141
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v2, v2, 0x40

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 143
    const-string v2, "EasyOneHand/ShortCutEdit"

    invoke-virtual {v0, v2}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 145
    sget-boolean v2, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 146
    const-string v2, "EOHShortcutAddWindow"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createEasyOneHandWindowLayoutParams lp="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :cond_0
    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    .line 149
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainDialogWindow:Landroid/view/Window;

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v2, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 150
    return-void
.end method

.method fillGridView()V
    .locals 9

    .prologue
    .line 269
    sget-boolean v6, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->DEBUG:Z

    if-eqz v6, :cond_0

    .line 270
    const-string v6, "EOHShortcutAddWindow"

    const-string v7, "fillGridView() start"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    :cond_0
    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 274
    .local v5, "pm":Landroid/content/pm/PackageManager;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 278
    .local v4, "orderedItemList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v2, Landroid/content/Intent;

    const-string v6, "android.intent.action.MAIN"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 279
    .local v2, "launcherIntent":Landroid/content/Intent;
    const-string v6, "android.intent.category.LAUNCHER"

    invoke-virtual {v2, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 280
    const/16 v6, 0xc0

    invoke-virtual {v5, v2, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    .line 283
    .local v3, "launcherlist":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    .line 284
    .local v0, "N":I
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 285
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 284
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 288
    :cond_1
    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->alphabeticCompare:Ljava/util/Comparator;

    invoke-static {v4, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 290
    new-instance v6, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;

    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mContext:Landroid/content/Context;

    const v8, 0x7f030004

    invoke-direct {v6, p0, v7, v8, v4}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;-><init>(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;Landroid/content/Context;ILjava/util/List;)V

    iput-object v6, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mGridAdapter:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;

    .line 291
    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mGridAdapter:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;

    invoke-virtual {v6}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->addAllItem()V

    .line 293
    sget-boolean v6, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->DEBUG:Z

    if-eqz v6, :cond_2

    .line 294
    const-string v6, "EOHShortcutAddWindow"

    const-string v7, "fillGridView() end"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    :cond_2
    return-void
.end method

.method public finishWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 187
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 188
    const-string v0, "EOHShortcutAddWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "finishWindow() callers="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mProgressTask:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ProgressTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ProgressTask;->cancel(Z)Z

    .line 191
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->cleanUpMemory()V

    .line 193
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainDialog:Landroid/app/Dialog;

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    .line 195
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 196
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainDialog:Landroid/app/Dialog;

    .line 197
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainDialogWindow:Landroid/view/Window;

    .line 199
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainView:Landroid/view/View;

    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHViewUnbindHelper;->unbindReferences(Landroid/view/View;)V

    .line 200
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainView:Landroid/view/View;

    .line 202
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    .line 203
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    .line 204
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    .line 206
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainDialogWindow:Landroid/view/Window;

    .line 207
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    .line 208
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mContext:Landroid/content/Context;

    .line 209
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mProgressTask:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ProgressTask;

    .line 211
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 212
    return-void
.end method

.method public hideWindow()V
    .locals 3

    .prologue
    .line 180
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 181
    const-string v0, "EOHShortcutAddWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hideWindow() callers="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    .line 183
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mSideWindow:Lcom/sec/android/easyonehand/EOHSideWindow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHSideWindow;->setShortcutEditMode(Z)V

    .line 184
    return-void
.end method

.method public showWindow()V
    .locals 3

    .prologue
    .line 171
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 172
    const-string v0, "EOHShortcutAddWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showWindow() callers="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 174
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 175
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    const v1, 0x7f070002

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHUtils;->showToast(I)V

    .line 177
    :cond_1
    return-void
.end method
