.class Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;
.super Landroid/telephony/PhoneStateListener;
.source "EasyOneHandService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/easyonehand/EasyOneHandService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EOHPhoneStateListener"
.end annotation


# instance fields
.field private mService:Lcom/sec/android/easyonehand/EasyOneHandService;


# direct methods
.method private constructor <init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V
    .locals 1
    .param p1, "service"    # Lcom/sec/android/easyonehand/EasyOneHandService;

    .prologue
    .line 689
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    .line 687
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;->mService:Lcom/sec/android/easyonehand/EasyOneHandService;

    .line 690
    iput-object p1, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;->mService:Lcom/sec/android/easyonehand/EasyOneHandService;

    .line 691
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/easyonehand/EasyOneHandService;Lcom/sec/android/easyonehand/EasyOneHandService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/easyonehand/EasyOneHandService;
    .param p2, "x1"    # Lcom/sec/android/easyonehand/EasyOneHandService$1;

    .prologue
    .line 686
    invoke-direct {p0, p1}, Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;-><init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;

    .prologue
    .line 686
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;->dispose()V

    return-void
.end method

.method private dispose()V
    .locals 1

    .prologue
    .line 694
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;->mService:Lcom/sec/android/easyonehand/EasyOneHandService;

    .line 695
    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 1
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    .line 698
    # setter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mCallState:I
    invoke-static {p1}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$902(I)I

    .line 700
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 701
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;->mService:Lcom/sec/android/easyonehand/EasyOneHandService;

    if-eqz v0, :cond_0

    .line 702
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;->mService:Lcom/sec/android/easyonehand/EasyOneHandService;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EasyOneHandService;->onRinging()V

    .line 713
    :cond_0
    :goto_0
    return-void

    .line 704
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 705
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;->mService:Lcom/sec/android/easyonehand/EasyOneHandService;

    if-eqz v0, :cond_0

    .line 706
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;->mService:Lcom/sec/android/easyonehand/EasyOneHandService;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EasyOneHandService;->onOffHook()V

    goto :goto_0

    .line 708
    :cond_2
    if-nez p1, :cond_0

    .line 709
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;->mService:Lcom/sec/android/easyonehand/EasyOneHandService;

    if-eqz v0, :cond_0

    .line 710
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;->mService:Lcom/sec/android/easyonehand/EasyOneHandService;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EasyOneHandService;->onIdle()V

    goto :goto_0
.end method
