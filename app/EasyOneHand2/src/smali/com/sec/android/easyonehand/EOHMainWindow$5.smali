.class Lcom/sec/android/easyonehand/EOHMainWindow$5;
.super Landroid/animation/AnimatorListenerAdapter;
.source "EOHMainWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/easyonehand/EOHMainWindow;->runStartUpAniMation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/easyonehand/EOHMainWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/easyonehand/EOHMainWindow;)V
    .locals 0

    .prologue
    .line 400
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHMainWindow$5;->this$0:Lcom/sec/android/easyonehand/EOHMainWindow;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 402
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$5;->this$0:Lcom/sec/android/easyonehand/EOHMainWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHMainWindow;->isCloseWithoutAnimation:Z
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->access$500(Lcom/sec/android/easyonehand/EOHMainWindow;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 403
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHMainWindow;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 404
    const-string v0, "EOHMainWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAnimationEnd mMainDialog = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHMainWindow$5;->this$0:Lcom/sec/android/easyonehand/EOHMainWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialog:Landroid/app/Dialog;
    invoke-static {v2}, Lcom/sec/android/easyonehand/EOHMainWindow;->access$700(Lcom/sec/android/easyonehand/EOHMainWindow;)Landroid/app/Dialog;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$5;->this$0:Lcom/sec/android/easyonehand/EOHMainWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->access$700(Lcom/sec/android/easyonehand/EOHMainWindow;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 407
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$5;->this$0:Lcom/sec/android/easyonehand/EOHMainWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHMainWindow;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->access$300(Lcom/sec/android/easyonehand/EOHMainWindow;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowController;->showSideWindow()V

    .line 408
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$5;->this$0:Lcom/sec/android/easyonehand/EOHMainWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHMainWindow;->startUpAlphaAnimation:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->access$800(Lcom/sec/android/easyonehand/EOHMainWindow;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 410
    :cond_1
    return-void
.end method
