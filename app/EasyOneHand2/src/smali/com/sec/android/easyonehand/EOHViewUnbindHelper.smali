.class public Lcom/sec/android/easyonehand/EOHViewUnbindHelper;
.super Ljava/lang/Object;
.source "EOHViewUnbindHelper.java"


# direct methods
.method public static unbindReferences(Landroid/view/View;)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 25
    if-eqz p0, :cond_0

    .line 26
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/easyonehand/EOHViewUnbindHelper;->unbindViewReferences(Landroid/view/View;)V

    .line 27
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 28
    check-cast p0, Landroid/view/ViewGroup;

    .end local p0    # "view":Landroid/view/View;
    invoke-static {p0}, Lcom/sec/android/easyonehand/EOHViewUnbindHelper;->unbindViewGroupReferences(Landroid/view/ViewGroup;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    :cond_0
    :goto_0
    return-void

    .line 31
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static unbindViewGroupReferences(Landroid/view/ViewGroup;)V
    .locals 4
    .param p0, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 68
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 69
    .local v1, "nrOfChildren":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 70
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 71
    .local v2, "view":Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/easyonehand/EOHViewUnbindHelper;->unbindViewReferences(Landroid/view/View;)V

    .line 72
    instance-of v3, v2, Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    .line 73
    check-cast v2, Landroid/view/ViewGroup;

    .end local v2    # "view":Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/easyonehand/EOHViewUnbindHelper;->unbindViewGroupReferences(Landroid/view/ViewGroup;)V

    .line 69
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 77
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    :goto_1
    return-void

    .line 78
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method private static unbindViewReferences(Landroid/view/View;)V
    .locals 4
    .param p0, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 90
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :goto_0
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {p0, v2}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 98
    :goto_1
    const/4 v2, 0x0

    :try_start_2
    invoke-virtual {p0, v2}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 102
    :goto_2
    const/4 v2, 0x0

    :try_start_3
    invoke-virtual {p0, v2}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 106
    :goto_3
    const/4 v2, 0x0

    :try_start_4
    invoke-virtual {p0, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 110
    :goto_4
    const/4 v2, 0x0

    :try_start_5
    invoke-virtual {p0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    .line 114
    :goto_5
    const/4 v2, 0x0

    :try_start_6
    invoke-virtual {p0, v2}, Landroid/view/View;->setTouchDelegate(Landroid/view/TouchDelegate;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6

    .line 117
    :goto_6
    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 118
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 120
    const/4 v2, 0x0

    :try_start_7
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_7

    .line 124
    :cond_0
    :goto_7
    instance-of v2, p0, Landroid/widget/ImageView;

    if-eqz v2, :cond_3

    move-object v1, p0

    .line 125
    check-cast v1, Landroid/widget/ImageView;

    .line 126
    .local v1, "imageView":Landroid/widget/ImageView;
    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 127
    if-eqz v0, :cond_1

    .line 128
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 134
    :cond_1
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 140
    .end local v1    # "imageView":Landroid/widget/ImageView;
    :cond_2
    :goto_8
    const/4 v2, 0x0

    :try_start_8
    invoke-virtual {p0, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_8

    .line 144
    :goto_9
    const/4 v2, 0x0

    :try_start_9
    invoke-virtual {p0, v2}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_9

    .line 148
    :goto_a
    const/4 v2, 0x0

    :try_start_a
    invoke-virtual {p0, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_a

    .line 152
    :goto_b
    const/4 v2, 0x0

    :try_start_b
    invoke-virtual {p0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_b

    .line 155
    :goto_c
    return-void

    .line 135
    :cond_3
    instance-of v2, p0, Landroid/webkit/WebView;

    if-eqz v2, :cond_2

    move-object v2, p0

    .line 136
    check-cast v2, Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->destroyDrawingCache()V

    move-object v2, p0

    .line 137
    check-cast v2, Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->destroy()V

    goto :goto_8

    .line 91
    .end local v0    # "d":Landroid/graphics/drawable/Drawable;
    :catch_0
    move-exception v2

    goto :goto_0

    .line 95
    :catch_1
    move-exception v2

    goto :goto_1

    .line 99
    :catch_2
    move-exception v2

    goto :goto_2

    .line 103
    :catch_3
    move-exception v2

    goto :goto_3

    .line 107
    :catch_4
    move-exception v2

    goto :goto_4

    .line 111
    :catch_5
    move-exception v2

    goto :goto_5

    .line 115
    :catch_6
    move-exception v2

    goto :goto_6

    .line 121
    .restart local v0    # "d":Landroid/graphics/drawable/Drawable;
    :catch_7
    move-exception v2

    goto :goto_7

    .line 141
    :catch_8
    move-exception v2

    goto :goto_9

    .line 145
    :catch_9
    move-exception v2

    goto :goto_a

    .line 149
    :catch_a
    move-exception v2

    goto :goto_b

    .line 153
    :catch_b
    move-exception v2

    goto :goto_c
.end method
