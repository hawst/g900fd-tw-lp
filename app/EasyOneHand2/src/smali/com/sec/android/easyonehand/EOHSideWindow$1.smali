.class Lcom/sec/android/easyonehand/EOHSideWindow$1;
.super Ljava/lang/Object;
.source "EOHSideWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/easyonehand/EOHSideWindow;->setButtonEventHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/easyonehand/EOHSideWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/easyonehand/EOHSideWindow;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHSideWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 178
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$000(Lcom/sec/android/easyonehand/EOHSideWindow;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHUtils;->userActivity()V

    .line 179
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$000(Lcom/sec/android/easyonehand/EOHSideWindow;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHUtils;->playShortHaptic()V

    .line 181
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$100(Lcom/sec/android/easyonehand/EOHSideWindow;)Lcom/sec/android/easyonehand/EOHWindowInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowType()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 182
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mIsEditMode:Z
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$200(Lcom/sec/android/easyonehand/EOHSideWindow;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # invokes: Lcom/sec/android/easyonehand/EOHSideWindow;->startContact()V
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$300(Lcom/sec/android/easyonehand/EOHSideWindow;)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHSideWindow;->setContactEditMode(Z)V

    .line 199
    :goto_0
    return-void

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v0, v2}, Lcom/sec/android/easyonehand/EOHSideWindow;->setContactEditMode(Z)V

    .line 187
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHUtils;->injectKeyEvent(I)V

    goto :goto_0

    .line 190
    :cond_1
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mIsEditMode:Z
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$200(Lcom/sec/android/easyonehand/EOHSideWindow;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 191
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # invokes: Lcom/sec/android/easyonehand/EOHSideWindow;->createShortcutAddWindow()V
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$400(Lcom/sec/android/easyonehand/EOHSideWindow;)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAddWindow:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$500(Lcom/sec/android/easyonehand/EOHSideWindow;)Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->showWindow()V

    .line 193
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHSideWindow;->setShortcutEditMode(Z)V

    goto :goto_0

    .line 195
    :cond_2
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    invoke-virtual {v0, v2}, Lcom/sec/android/easyonehand/EOHSideWindow;->setShortcutEditMode(Z)V

    .line 196
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAddWindow:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$500(Lcom/sec/android/easyonehand/EOHSideWindow;)Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->hideWindow()V

    goto :goto_0
.end method
