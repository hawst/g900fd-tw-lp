.class Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;
.super Landroid/os/Handler;
.source "EasyOneHandService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/easyonehand/EasyOneHandService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EOHServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/easyonehand/EasyOneHandService;


# direct methods
.method private constructor <init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/easyonehand/EasyOneHandService;Lcom/sec/android/easyonehand/EasyOneHandService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/easyonehand/EasyOneHandService;
    .param p2, "x1"    # Lcom/sec/android/easyonehand/EasyOneHandService$1;

    .prologue
    .line 299
    invoke-direct {p0, p1}, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;-><init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 14
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v12, 0xfa0

    const-wide/16 v10, 0x0

    const/4 v4, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 302
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 365
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 304
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowController;->isAttached()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 305
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowController;->showSideWindow()V

    goto :goto_0

    .line 310
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowController;->isAttached()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 311
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowController;->checkDeviceRotation()V

    goto :goto_0

    .line 316
    :pswitch_3
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$300(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/os/Handler;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 319
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$300(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 320
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$300(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 322
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowController;->isAttached()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowController;->isAnimationRunning()Z

    move-result v3

    if-nez v3, :cond_0

    .line 324
    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->DEBUG:Z
    invoke-static {}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$400()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 325
    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$500()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MSG_EOH_SCREEN_OFF_KEYGUARD_CHECK_TIMER"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    :cond_1
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$600(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHUtils;->isKeyguardLocked()Z

    move-result v2

    .line 327
    .local v2, "isKeyguardLocked":Z
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$700(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowInfo;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->setScreenLocked(Z)V

    .line 329
    if-eqz v2, :cond_3

    .line 330
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$600(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHUtils;->isFingerprintLockScreenEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 331
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v3

    invoke-virtual {v3, v9}, Lcom/sec/android/easyonehand/EOHWindowController;->closeWindow(Z)V

    goto/16 :goto_0

    .line 334
    :cond_2
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowController;->hideSideWindow()V

    .line 336
    :cond_3
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$300(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v8, v12, v13}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 342
    .end local v2    # "isKeyguardLocked":Z
    :pswitch_4
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$300(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/os/Handler;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 345
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$300(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 346
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$300(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 348
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowController;->isAttached()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowController;->isAnimationRunning()Z

    move-result v3

    if-nez v3, :cond_0

    .line 350
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long v0, v4, v6

    .line 351
    .local v0, "gap":J
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$600(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHUtils;->isKeyguardLocked()Z

    move-result v2

    .line 352
    .restart local v2    # "isKeyguardLocked":Z
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mTimeGapWhenLCDOFF:J
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$800(Lcom/sec/android/easyonehand/EasyOneHandService;)J

    move-result-wide v4

    cmp-long v3, v4, v10

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mTimeGapWhenLCDOFF:J
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$800(Lcom/sec/android/easyonehand/EasyOneHandService;)J

    move-result-wide v4

    sub-long v4, v0, v4

    const-wide/16 v6, 0x3e8

    cmp-long v3, v4, v6

    if-gtz v3, :cond_5

    :cond_4
    if-eqz v2, :cond_6

    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$600(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHUtils;->isKeyguardHidden()Z

    move-result v3

    if-nez v3, :cond_6

    .line 354
    :cond_5
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v3

    invoke-virtual {v3, v9}, Lcom/sec/android/easyonehand/EOHWindowController;->closeWindow(Z)V

    .line 355
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # setter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mTimeGapWhenLCDOFF:J
    invoke-static {v3, v10, v11}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$802(Lcom/sec/android/easyonehand/EasyOneHandService;J)J

    goto/16 :goto_0

    .line 359
    :cond_6
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$700(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowInfo;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->setScreenLocked(Z)V

    .line 360
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$300(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v8, v12, v13}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 302
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
