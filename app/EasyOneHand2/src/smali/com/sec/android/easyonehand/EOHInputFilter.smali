.class Lcom/sec/android/easyonehand/EOHInputFilter;
.super Landroid/view/InputFilter;
.source "EOHInputFilter.java"


# static fields
.field static final DEBUG:Z


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mInstalled:Z

.field private mIsDrag:Z

.field private mIsMagnifyRectTouch:Z

.field private mIsMainWindowTouch:Z

.field private mMagnifyRect:Landroid/graphics/Rect;

.field private mMainRect:Landroid/graphics/Rect;

.field private mOffsetX:I

.field private mOffsetY:I

.field mResizeInputIntercepter:Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;

.field private mRunning:Z

.field private mScale:F

.field private mScreenH:I

.field private mScreenW:I

.field private mTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

.field private mTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

.field private mWMS:Landroid/view/IWindowManager;

.field private mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

.field private mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/easyonehand/EOHInputFilter;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/easyonehand/EOHWindowController;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "controller"    # Lcom/sec/android/easyonehand/EOHWindowController;

    .prologue
    .line 81
    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/view/InputFilter;-><init>(Landroid/os/Looper;)V

    .line 58
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mMagnifyRect:Landroid/graphics/Rect;

    .line 60
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mMainRect:Landroid/graphics/Rect;

    .line 78
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mRunning:Z

    .line 83
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHInputFilter;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 84
    const-string v0, "EOHInputFilter"

    const-string v1, "EOHInputFilter() constructor"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    :cond_0
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mContext:Landroid/content/Context;

    .line 87
    new-instance v0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;

    invoke-direct {v0, p2}, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;-><init>(Lcom/sec/android/easyonehand/EOHWindowController;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mResizeInputIntercepter:Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;

    .line 88
    iput-object p2, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    .line 89
    const-string v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mWMS:Landroid/view/IWindowManager;

    .line 90
    invoke-static {}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getInstance()Lcom/sec/android/easyonehand/EOHWindowInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    .line 91
    return-void
.end method

.method private byPassRawEvent(Landroid/view/MotionEvent;I)V
    .locals 20
    .param p1, "rawEvent"    # Landroid/view/MotionEvent;
    .param p2, "policyFlags"    # I

    .prologue
    .line 220
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    .line 221
    .local v7, "pointerCount":I
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/sec/android/easyonehand/EOHInputFilter;->getTempPointerCoordsWithMinSize(I)[Landroid/view/MotionEvent$PointerCoords;

    move-result-object v9

    .line 222
    .local v9, "coords":[Landroid/view/MotionEvent$PointerCoords;
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/sec/android/easyonehand/EOHInputFilter;->getTempPointerPropertiesWithMinSize(I)[Landroid/view/MotionEvent$PointerProperties;

    move-result-object v8

    .line 223
    .local v8, "properties":[Landroid/view/MotionEvent$PointerProperties;
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_0
    move/from16 v0, v18

    if-ge v0, v7, :cond_0

    .line 224
    aget-object v2, v9, v18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V

    .line 225
    aget-object v2, v8, v18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->getPointerProperties(ILandroid/view/MotionEvent$PointerProperties;)V

    .line 223
    add-int/lit8 v18, v18, 0x1

    goto :goto_0

    .line 228
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/high16 v12, 0x3f800000    # 1.0f

    const/high16 v13, 0x3f800000    # 1.0f

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v14

    const/4 v15, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v16

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getFlags()I

    move-result v17

    const/high16 v19, 0x20000000

    or-int v17, v17, v19

    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object p1

    .line 232
    invoke-super/range {p0 .. p2}, Landroid/view/InputFilter;->onInputEvent(Landroid/view/InputEvent;I)V

    .line 233
    return-void
.end method

.method private getTempPointerCoordsWithMinSize(I)[Landroid/view/MotionEvent$PointerCoords;
    .locals 5
    .param p1, "size"    # I

    .prologue
    const/4 v3, 0x0

    .line 155
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    array-length v1, v4

    .line 156
    .local v1, "oldSize":I
    :goto_0
    if-ge v1, p1, :cond_0

    .line 157
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    .line 158
    .local v2, "oldTempPointerCoords":[Landroid/view/MotionEvent$PointerCoords;
    new-array v4, p1, [Landroid/view/MotionEvent$PointerCoords;

    iput-object v4, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    .line 159
    if-eqz v2, :cond_0

    .line 160
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    invoke-static {v2, v3, v4, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 163
    .end local v2    # "oldTempPointerCoords":[Landroid/view/MotionEvent$PointerCoords;
    :cond_0
    move v0, v1

    .local v0, "i":I
    :goto_1
    if-ge v0, p1, :cond_2

    .line 164
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    new-instance v4, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct {v4}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    aput-object v4, v3, v0

    .line 163
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v0    # "i":I
    .end local v1    # "oldSize":I
    :cond_1
    move v1, v3

    .line 155
    goto :goto_0

    .line 166
    .restart local v0    # "i":I
    .restart local v1    # "oldSize":I
    :cond_2
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    return-object v3
.end method

.method private getTempPointerPropertiesWithMinSize(I)[Landroid/view/MotionEvent$PointerProperties;
    .locals 5
    .param p1, "size"    # I

    .prologue
    const/4 v3, 0x0

    .line 170
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

    array-length v1, v4

    .line 171
    .local v1, "oldSize":I
    :goto_0
    if-ge v1, p1, :cond_0

    .line 172
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

    .line 173
    .local v2, "oldTempPointerProperties":[Landroid/view/MotionEvent$PointerProperties;
    new-array v4, p1, [Landroid/view/MotionEvent$PointerProperties;

    iput-object v4, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

    .line 174
    if-eqz v2, :cond_0

    .line 175
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

    invoke-static {v2, v3, v4, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 178
    .end local v2    # "oldTempPointerProperties":[Landroid/view/MotionEvent$PointerProperties;
    :cond_0
    move v0, v1

    .local v0, "i":I
    :goto_1
    if-ge v0, p1, :cond_2

    .line 179
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

    new-instance v4, Landroid/view/MotionEvent$PointerProperties;

    invoke-direct {v4}, Landroid/view/MotionEvent$PointerProperties;-><init>()V

    aput-object v4, v3, v0

    .line 178
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v0    # "i":I
    .end local v1    # "oldSize":I
    :cond_1
    move v1, v3

    .line 170
    goto :goto_0

    .line 181
    .restart local v0    # "i":I
    .restart local v1    # "oldSize":I
    :cond_2
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

    return-object v3
.end method

.method private makeFakeTouchEvent(Landroid/view/MotionEvent;II)V
    .locals 22
    .param p1, "e"    # Landroid/view/MotionEvent;
    .param p2, "policyFlags"    # I
    .param p3, "fakeAction"    # I

    .prologue
    .line 185
    sget-boolean v4, Lcom/sec/android/easyonehand/EOHInputFilter;->DEBUG:Z

    if-eqz v4, :cond_0

    .line 186
    const-string v4, "EOHInputFilter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "makeFakeTouchEvent() touch Area changed fakeAction="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    :cond_0
    move-object/from16 v21, p1

    .line 190
    .local v21, "rawEvent":Landroid/view/MotionEvent;
    invoke-virtual/range {v21 .. v21}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v9

    .line 191
    .local v9, "pointerCount":I
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/sec/android/easyonehand/EOHInputFilter;->getTempPointerCoordsWithMinSize(I)[Landroid/view/MotionEvent$PointerCoords;

    move-result-object v11

    .line 192
    .local v11, "coords":[Landroid/view/MotionEvent$PointerCoords;
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/sec/android/easyonehand/EOHInputFilter;->getTempPointerPropertiesWithMinSize(I)[Landroid/view/MotionEvent$PointerProperties;

    move-result-object v10

    .line 193
    .local v10, "properties":[Landroid/view/MotionEvent$PointerProperties;
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_0
    move/from16 v0, v20

    if-ge v0, v9, :cond_6

    .line 194
    aget-object v4, v11, v20

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v0, v1, v4}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V

    .line 195
    aget-object v4, v11, v20

    aget-object v5, v11, v20

    iget v5, v5, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mOffsetX:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mScale:F

    div-float/2addr v5, v6

    iput v5, v4, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 196
    aget-object v4, v11, v20

    aget-object v5, v11, v20

    iget v5, v5, Landroid/view/MotionEvent$PointerCoords;->y:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mOffsetY:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mScale:F

    div-float/2addr v5, v6

    iput v5, v4, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 198
    aget-object v5, v11, v20

    aget-object v4, v11, v20

    iget v4, v4, Landroid/view/MotionEvent$PointerCoords;->x:F

    const/4 v6, 0x0

    cmpg-float v4, v4, v6

    if-gez v4, :cond_2

    const/4 v4, 0x0

    :goto_1
    iput v4, v5, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 199
    aget-object v5, v11, v20

    aget-object v4, v11, v20

    iget v4, v4, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mScreenW:I

    int-to-float v6, v6

    cmpl-float v4, v4, v6

    if-lez v4, :cond_3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mScreenW:I

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    :goto_2
    iput v4, v5, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 200
    aget-object v5, v11, v20

    aget-object v4, v11, v20

    iget v4, v4, Landroid/view/MotionEvent$PointerCoords;->y:F

    const/4 v6, 0x0

    cmpg-float v4, v4, v6

    if-gez v4, :cond_4

    const/4 v4, 0x0

    :goto_3
    iput v4, v5, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 201
    aget-object v5, v11, v20

    aget-object v4, v11, v20

    iget v4, v4, Landroid/view/MotionEvent$PointerCoords;->y:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mScreenH:I

    int-to-float v6, v6

    cmpl-float v4, v4, v6

    if-lez v4, :cond_5

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mScreenH:I

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    :goto_4
    iput v4, v5, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 203
    aget-object v4, v10, v20

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v0, v1, v4}, Landroid/view/MotionEvent;->getPointerProperties(ILandroid/view/MotionEvent$PointerProperties;)V

    .line 204
    sget-boolean v4, Lcom/sec/android/easyonehand/EOHInputFilter;->DEBUG:Z

    if-eqz v4, :cond_1

    .line 205
    const-string v4, "EOHInputFilter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "makeFakeTouchEvent() fakeAction="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", x="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v11, v20

    iget v6, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", y="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v11, v20

    iget v6, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    :cond_1
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_0

    .line 198
    :cond_2
    aget-object v4, v11, v20

    iget v4, v4, Landroid/view/MotionEvent$PointerCoords;->x:F

    goto/16 :goto_1

    .line 199
    :cond_3
    aget-object v4, v11, v20

    iget v4, v4, Landroid/view/MotionEvent$PointerCoords;->x:F

    goto :goto_2

    .line 200
    :cond_4
    aget-object v4, v11, v20

    iget v4, v4, Landroid/view/MotionEvent$PointerCoords;->y:F

    goto :goto_3

    .line 201
    :cond_5
    aget-object v4, v11, v20

    iget v4, v4, Landroid/view/MotionEvent$PointerCoords;->y:F

    goto :goto_4

    .line 212
    :cond_6
    invoke-virtual/range {v21 .. v21}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    invoke-virtual/range {v21 .. v21}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/high16 v14, 0x3f800000    # 1.0f

    const/high16 v15, 0x3f800000    # 1.0f

    invoke-virtual/range {v21 .. v21}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v16

    const/16 v17, 0x0

    invoke-virtual/range {v21 .. v21}, Landroid/view/MotionEvent;->getSource()I

    move-result v18

    invoke-virtual/range {v21 .. v21}, Landroid/view/MotionEvent;->getFlags()I

    move-result v19

    move/from16 v8, p3

    invoke-static/range {v4 .. v19}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v21

    .line 216
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, p2

    invoke-super {v0, v1, v2}, Landroid/view/InputFilter;->onInputEvent(Landroid/view/InputEvent;I)V

    .line 217
    return-void
.end method


# virtual methods
.method public clearAllTouchEvents()V
    .locals 2

    .prologue
    .line 342
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHInputFilter;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 343
    const-string v0, "EOHInputFilter"

    const-string v1, "clearAllTouchEvents()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mResizeInputIntercepter:Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;

    if-eqz v0, :cond_1

    .line 346
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mResizeInputIntercepter:Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->stopIntercept()V

    .line 348
    :cond_1
    return-void
.end method

.method isRegistered()Z
    .locals 1

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mRunning:Z

    return v0
.end method

.method public onInputEvent(Landroid/view/InputEvent;I)V
    .locals 27
    .param p1, "event"    # Landroid/view/InputEvent;
    .param p2, "policyFlags"    # I

    .prologue
    .line 240
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mInstalled:Z

    if-eqz v4, :cond_e

    move-object/from16 v0, p1

    instance-of v4, v0, Landroid/view/MotionEvent;

    if-eqz v4, :cond_e

    move-object/from16 v26, p1

    .line 241
    check-cast v26, Landroid/view/MotionEvent;

    .line 242
    .local v26, "rawEvent":Landroid/view/MotionEvent;
    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->getAction()I

    move-result v20

    .line 244
    .local v20, "action":I
    const/4 v4, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 245
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v4}, Lcom/sec/android/easyonehand/EOHWindowController;->closeWindowBySPen()V

    .line 328
    .end local v20    # "action":I
    .end local v26    # "rawEvent":Landroid/view/MotionEvent;
    :cond_0
    :goto_0
    return-void

    .line 249
    .restart local v20    # "action":I
    .restart local v26    # "rawEvent":Landroid/view/MotionEvent;
    :cond_1
    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    float-to-int v0, v4

    move/from16 v25, v0

    .line 250
    .local v25, "posY":I
    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    float-to-int v0, v4

    move/from16 v24, v0

    .line 252
    .local v24, "posX":I
    if-nez v20, :cond_3

    .line 257
    sget-boolean v4, Lcom/sec/android/easyonehand/EOHInputFilter;->DEBUG:Z

    if-eqz v4, :cond_2

    .line 258
    const-string v4, "EOHInputFilter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onInputEvent() Received. posX="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", posY="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", screenW="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v6}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mIsDrag="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mIsDrag:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", event: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", policyFlags=0x"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    :cond_2
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mIsDrag:Z

    .line 265
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mResizeInputIntercepter:Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;

    invoke-virtual {v4}, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->isInterceptMotionEvt()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 266
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mResizeInputIntercepter:Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->onInterceptEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 271
    :cond_4
    if-nez v20, :cond_5

    .line 272
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mMainRect:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getFullWindowRect(Landroid/graphics/Rect;)V

    .line 275
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mMagnifyRect:Landroid/graphics/Rect;

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v22

    .line 276
    .local v22, "isInsideTouch":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mMainRect:Landroid/graphics/Rect;

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v23

    .line 280
    .local v23, "isMainWindowTouch":Z
    if-nez v20, :cond_6

    .line 281
    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/easyonehand/EOHInputFilter;->mIsMagnifyRectTouch:Z

    .line 282
    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/easyonehand/EOHInputFilter;->mIsMainWindowTouch:Z

    .line 285
    :cond_6
    const/4 v4, 0x2

    move/from16 v0, v20

    if-ne v0, v4, :cond_9

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mIsDrag:Z

    if-nez v4, :cond_9

    .line 286
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mIsMagnifyRectTouch:Z

    move/from16 v0, v22

    if-eq v0, v4, :cond_7

    .line 287
    if-eqz v22, :cond_a

    const/4 v4, 0x0

    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move/from16 v2, p2

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/android/easyonehand/EOHInputFilter;->makeFakeTouchEvent(Landroid/view/MotionEvent;II)V

    .line 290
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mIsMainWindowTouch:Z

    if-eqz v4, :cond_8

    if-nez v23, :cond_8

    .line 291
    const/4 v4, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move/from16 v2, p2

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/android/easyonehand/EOHInputFilter;->makeFakeTouchEvent(Landroid/view/MotionEvent;II)V

    .line 294
    :cond_8
    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/easyonehand/EOHInputFilter;->mIsMagnifyRectTouch:Z

    .line 299
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mIsDrag:Z

    if-nez v4, :cond_b

    if-nez v22, :cond_b

    .line 300
    if-eqz v23, :cond_0

    .line 301
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/easyonehand/EOHInputFilter;->byPassRawEvent(Landroid/view/MotionEvent;I)V

    goto/16 :goto_0

    .line 287
    :cond_a
    const/4 v4, 0x1

    goto :goto_1

    .line 306
    :cond_b
    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v9

    .line 307
    .local v9, "pointerCount":I
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/sec/android/easyonehand/EOHInputFilter;->getTempPointerCoordsWithMinSize(I)[Landroid/view/MotionEvent$PointerCoords;

    move-result-object v11

    .line 308
    .local v11, "coords":[Landroid/view/MotionEvent$PointerCoords;
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/sec/android/easyonehand/EOHInputFilter;->getTempPointerPropertiesWithMinSize(I)[Landroid/view/MotionEvent$PointerProperties;

    move-result-object v10

    .line 309
    .local v10, "properties":[Landroid/view/MotionEvent$PointerProperties;
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_2
    move/from16 v0, v21

    if-ge v0, v9, :cond_c

    .line 310
    aget-object v4, v11, v21

    move-object/from16 v0, v26

    move/from16 v1, v21

    invoke-virtual {v0, v1, v4}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V

    .line 311
    aget-object v4, v11, v21

    aget-object v5, v11, v21

    iget v5, v5, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mOffsetX:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mScale:F

    div-float/2addr v5, v6

    iput v5, v4, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 312
    aget-object v4, v11, v21

    aget-object v5, v11, v21

    iget v5, v5, Landroid/view/MotionEvent$PointerCoords;->y:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mOffsetY:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/easyonehand/EOHInputFilter;->mScale:F

    div-float/2addr v5, v6

    iput v5, v4, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 313
    aget-object v4, v10, v21

    move-object/from16 v0, v26

    move/from16 v1, v21

    invoke-virtual {v0, v1, v4}, Landroid/view/MotionEvent;->getPointerProperties(ILandroid/view/MotionEvent$PointerProperties;)V

    .line 309
    add-int/lit8 v21, v21, 0x1

    goto :goto_2

    .line 315
    :cond_c
    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->getAction()I

    move-result v8

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/high16 v14, 0x3f800000    # 1.0f

    const/high16 v15, 0x3f800000    # 1.0f

    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v16

    const/16 v17, 0x0

    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->getSource()I

    move-result v18

    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->getFlags()I

    move-result v19

    invoke-static/range {v4 .. v19}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v26

    .line 319
    const/4 v4, 0x1

    move/from16 v0, v20

    if-ne v0, v4, :cond_d

    .line 320
    sget-boolean v4, Lcom/sec/android/easyonehand/EOHInputFilter;->DEBUG:Z

    if-eqz v4, :cond_d

    .line 321
    const-string v4, "EOHInputFilter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "super.onInputEvent() posX="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", rawEvent="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    :cond_d
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move/from16 v2, p2

    invoke-super {v0, v1, v2}, Landroid/view/InputFilter;->onInputEvent(Landroid/view/InputEvent;I)V

    goto/16 :goto_0

    .line 327
    .end local v9    # "pointerCount":I
    .end local v10    # "properties":[Landroid/view/MotionEvent$PointerProperties;
    .end local v11    # "coords":[Landroid/view/MotionEvent$PointerCoords;
    .end local v20    # "action":I
    .end local v21    # "i":I
    .end local v22    # "isInsideTouch":Z
    .end local v23    # "isMainWindowTouch":Z
    .end local v24    # "posX":I
    .end local v25    # "posY":I
    .end local v26    # "rawEvent":Landroid/view/MotionEvent;
    :cond_e
    invoke-super/range {p0 .. p2}, Landroid/view/InputFilter;->onInputEvent(Landroid/view/InputEvent;I)V

    goto/16 :goto_0
.end method

.method public onInstalled()V
    .locals 2

    .prologue
    .line 95
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHInputFilter;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 96
    const-string v0, "EOHInputFilter"

    const-string v1, "EasyOneHand input filter installed."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mInstalled:Z

    .line 99
    invoke-super {p0}, Landroid/view/InputFilter;->onInstalled()V

    .line 100
    return-void
.end method

.method public onUninstalled()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 104
    sget-boolean v3, Lcom/sec/android/easyonehand/EOHInputFilter;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 105
    const-string v3, "EOHInputFilter"

    const-string v4, "EasyOneHand input filter uninstalled."

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    :cond_0
    iput-boolean v6, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mInstalled:Z

    .line 108
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mResizeInputIntercepter:Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->stopIntercept()V

    .line 109
    invoke-super {p0}, Landroid/view/InputFilter;->onUninstalled()V

    .line 110
    iget-boolean v3, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mRunning:Z

    if-eqz v3, :cond_2

    .line 115
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_1

    .line 116
    new-instance v0, Landroid/content/ComponentName;

    const-string v3, "com.sec.android.easyonehand"

    const-string v4, "com.sec.android.easyonehand.EasyOneHandService"

    invoke-direct {v0, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    .local v0, "cn":Landroid/content/ComponentName;
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.samsung.action.EASYONEHAND_SERVICE"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "ForceHide"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    .line 120
    .local v2, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    .end local v0    # "cn":Landroid/content/ComponentName;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_0
    iput-boolean v6, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mRunning:Z

    .line 127
    :cond_2
    return-void

    .line 122
    :catch_0
    move-exception v1

    .line 123
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "EOHInputFilter"

    const-string v4, "Exception checkSetting: "

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public registerInputFilter(Z)V
    .locals 0
    .param p1, "isRunning"    # Z

    .prologue
    .line 130
    iput-boolean p1, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mRunning:Z

    .line 131
    return-void
.end method

.method public sendInputEvent(Landroid/view/InputEvent;I)V
    .locals 4
    .param p1, "evt"    # Landroid/view/InputEvent;
    .param p2, "flag"    # I

    .prologue
    .line 333
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mInstalled:Z

    if-eqz v1, :cond_0

    .line 334
    invoke-super {p0, p1, p2}, Landroid/view/InputFilter;->sendInputEvent(Landroid/view/InputEvent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 339
    :cond_0
    :goto_0
    return-void

    .line 336
    :catch_0
    move-exception v0

    .line 337
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "EOHInputFilter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occured in sendInputEvent. evt="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setDragMode(Z)V
    .locals 0
    .param p1, "isDrag"    # Z

    .prologue
    .line 351
    iput-boolean p1, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mIsDrag:Z

    .line 352
    return-void
.end method

.method public setWindowChanged()V
    .locals 4

    .prologue
    .line 138
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    iget-object v1, v1, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMagnifyRect:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 140
    .local v0, "innerR":Landroid/graphics/Rect;
    sget-boolean v1, Lcom/sec/android/easyonehand/EOHInputFilter;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 141
    const-string v1, "EOHInputFilter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setWindowChanged() innerR="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :cond_0
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getWindowScale()F

    move-result v1

    iput v1, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mScale:F

    .line 144
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iput v1, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mOffsetX:I

    .line 145
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iput v1, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mOffsetY:I

    .line 147
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v1

    iput v1, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mScreenW:I

    .line 148
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v1

    iput v1, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mScreenH:I

    .line 150
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mMagnifyRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    iget-object v2, v2, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMagnifyRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 151
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHInputFilter;->mMainRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getFullWindowRect(Landroid/graphics/Rect;)V

    .line 152
    return-void
.end method
