.class Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1;
.super Ljava/lang/Object;
.source "EOHShortcutAddWindow.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;)V
    .locals 0

    .prologue
    .line 428
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1;->this$1:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 9
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x0

    .line 431
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1;->this$1:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;

    iget-object v5, v5, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->this$0:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v5}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->access$600(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/easyonehand/EOHUtils;->playLongHaptic()V

    .line 432
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1;->this$1:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;

    iget-object v5, v5, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->this$0:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v5}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->access$600(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/easyonehand/EOHUtils;->playSimpleKeyTone()V

    .line 434
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1;->this$1:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;

    invoke-virtual {v5, p1}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->getItemIndex(Landroid/view/View;)I

    move-result v3

    .line 435
    .local v3, "index":I
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1;->this$1:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;

    iget-object v5, v5, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->this$0:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mGridView:Landroid/widget/GridView;
    invoke-static {v5}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->access$200(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)Landroid/widget/GridView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/GridView;->getFirstVisiblePosition()I

    move-result v5

    add-int v4, v5, v3

    .line 436
    .local v4, "position":I
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1;->this$1:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;

    invoke-virtual {v5, v3}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->getImageViewByIndex(I)Landroid/widget/ImageView;

    move-result-object v2

    .line 438
    .local v2, "dragView":Landroid/widget/ImageView;
    const-string v6, "componentName"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1;->this$1:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;

    # getter for: Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;
    invoke-static {v5}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->access$700(Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v0

    .line 441
    .local v0, "dragData":Landroid/content/ClipData;
    sget-boolean v5, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->DEBUG:Z

    if-eqz v5, :cond_0

    .line 442
    const-string v6, "EOHShortcutAddWindow"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onLongClick drag position="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", Index="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " componentName="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1;->this$1:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;

    # getter for: Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;
    invoke-static {v5}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->access$700(Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    :cond_0
    if-eqz v0, :cond_1

    .line 447
    new-instance v1, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1$1;

    invoke-direct {v1, p0, p1, v2}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1$1;-><init>(Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1;Landroid/view/View;Landroid/widget/ImageView;)V

    .line 461
    .local v1, "dragShadow":Landroid/view/View$DragShadowBuilder;
    const/4 v5, 0x0

    invoke-virtual {p1, v0, v1, v5, v8}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 463
    .end local v1    # "dragShadow":Landroid/view/View$DragShadowBuilder;
    :cond_1
    return v8
.end method
