.class final Lcom/sec/android/easyonehand/EOHSideWindow;
.super Ljava/lang/Object;
.source "EOHSideWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;,
        Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;,
        Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;
    }
.end annotation


# static fields
.field static final DEBUG:Z


# instance fields
.field private changeViewAnimation:Landroid/animation/ValueAnimator;

.field private isAnimationRunning:Z

.field private mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

.field private mContactGridView:Landroid/widget/GridView;

.field private mContactLookup:Ljava/lang/String;

.field private mContactMenuPrefName:Ljava/lang/String;

.field private mContactMenuPrefs:Landroid/content/SharedPreferences;

.field private mContext:Landroid/content/Context;

.field private mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

.field private mIsEditMode:Z

.field private mMainDialog:Landroid/app/Dialog;

.field private mMainDialogWindow:Landroid/view/Window;

.field private mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

.field private mMainView:Landroid/view/View;

.field private mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

.field private mShortcutAddWindow:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

.field private mShortcutGridView:Landroid/widget/GridView;

.field private mShortcutMenuPrefs:Landroid/content/SharedPreferences;

.field private mShortcutMenuPrefsName:Ljava/lang/String;

.field private mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

.field private mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 67
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/easyonehand/EOHWindowController;Z)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "windowController"    # Lcom/sec/android/easyonehand/EOHWindowController;
    .param p3, "isLeft"    # Z

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-boolean v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->isAnimationRunning:Z

    .line 103
    const-string v1, "EasyOneHandSideContactItem"

    iput-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactMenuPrefName:Ljava/lang/String;

    .line 113
    const-string v1, "EasyOneHandSideShortcutItem"

    iput-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutMenuPrefsName:Ljava/lang/String;

    .line 116
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContext:Landroid/content/Context;

    .line 117
    iput-object p2, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    .line 119
    invoke-static {}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getInstance()Lcom/sec/android/easyonehand/EOHWindowInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    .line 120
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContext:Landroid/content/Context;

    const v4, 0x7f030006

    const/4 v5, 0x0

    invoke-static {v1, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    .line 121
    if-eqz p3, :cond_1

    const v0, 0x7f020018

    .line 122
    .local v0, "border":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    const v4, 0x7f09002d

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 124
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    const v4, 0x7f09002f

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz p3, :cond_2

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 125
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    const v4, 0x7f090030

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p3, :cond_3

    :goto_2
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 127
    new-instance v1, Landroid/app/Dialog;

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialog:Landroid/app/Dialog;

    .line 129
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindow:Landroid/view/Window;

    .line 130
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindow:Landroid/view/Window;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/view/Window;->requestFeature(I)Z

    .line 131
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindow:Landroid/view/Window;

    const v3, 0x106000d

    invoke-virtual {v1, v3}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 132
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialog:Landroid/app/Dialog;

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 133
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindow:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setElevation(F)V

    .line 135
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHSideWindow;->setButtonEventHandler()V

    .line 137
    invoke-static {}, Lcom/sec/android/easyonehand/EOHUtils;->getInstance()Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    .line 138
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactMenuPrefName:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactMenuPrefs:Landroid/content/SharedPreferences;

    .line 139
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutMenuPrefsName:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutMenuPrefs:Landroid/content/SharedPreferences;

    .line 141
    sget-boolean v1, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 142
    const-string v1, "EOHSideWindow"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EOHSideWindow() mMainDialog="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialog:Landroid/app/Dialog;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mMainView="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", callers="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xa

    invoke-static {v3}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    :cond_0
    return-void

    .line 121
    .end local v0    # "border":I
    :cond_1
    const v0, 0x7f020015

    goto/16 :goto_0

    .restart local v0    # "border":I
    :cond_2
    move v1, v3

    .line 124
    goto/16 :goto_1

    :cond_3
    move v3, v2

    .line 125
    goto/16 :goto_2
.end method

.method static synthetic access$000(Lcom/sec/android/easyonehand/EOHSideWindow;)Lcom/sec/android/easyonehand/EOHUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHSideWindow;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/easyonehand/EOHSideWindow;)Lcom/sec/android/easyonehand/EOHWindowInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHSideWindow;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/easyonehand/EOHSideWindow;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHSideWindow;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactLookup:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/android/easyonehand/EOHSideWindow;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHSideWindow;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactLookup:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/easyonehand/EOHSideWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHSideWindow;

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mIsEditMode:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/easyonehand/EOHSideWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHSideWindow;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHSideWindow;->startContact()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/easyonehand/EOHSideWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHSideWindow;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHSideWindow;->createShortcutAddWindow()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/easyonehand/EOHSideWindow;)Lcom/sec/android/easyonehand/EOHShortcutAddWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHSideWindow;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAddWindow:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/easyonehand/EOHSideWindow;)Lcom/sec/android/easyonehand/EOHWindowController;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHSideWindow;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/easyonehand/EOHSideWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHSideWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->isAnimationRunning:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/easyonehand/EOHSideWindow;)Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHSideWindow;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    return-object v0
.end method

.method private cleanUpContactMemory()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 346
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactGridView:Landroid/widget/GridView;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    if-nez v5, :cond_1

    .line 384
    :cond_0
    :goto_0
    return-void

    .line 350
    :cond_1
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactGridView:Landroid/widget/GridView;

    invoke-virtual {v5}, Landroid/widget/GridView;->getCount()I

    move-result v1

    .line 352
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v1, :cond_5

    .line 353
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactGridView:Landroid/widget/GridView;

    invoke-virtual {v5, v2}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 354
    .local v0, "convertView":Landroid/view/View;
    if-eqz v0, :cond_4

    .line 355
    const v5, 0x7f090003

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    .line 356
    .local v3, "iconImage":Landroid/widget/ImageButton;
    if-eqz v3, :cond_2

    .line 358
    invoke-virtual {v3, v6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 359
    const/4 v3, 0x0

    .line 362
    :cond_2
    const v5, 0x7f090004

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 363
    .local v4, "iconView":Landroid/widget/ImageView;
    if-eqz v4, :cond_4

    .line 364
    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 365
    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 367
    invoke-virtual {v4}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 368
    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 370
    :cond_3
    invoke-virtual {v4}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 371
    invoke-virtual {v4}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 352
    .end local v3    # "iconImage":Landroid/widget/ImageButton;
    .end local v4    # "iconView":Landroid/widget/ImageView;
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 379
    .end local v0    # "convertView":Landroid/view/View;
    :cond_5
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactGridView:Landroid/widget/GridView;

    invoke-virtual {v5}, Landroid/widget/GridView;->destroyDrawingCache()V

    .line 380
    iput-object v6, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactGridView:Landroid/widget/GridView;

    .line 382
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    invoke-virtual {v5}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->cleanUpMemory()V

    .line 383
    iput-object v6, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    goto :goto_0
.end method

.method private cleanUpShortcutMemory()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 1164
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutGridView:Landroid/widget/GridView;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    if-nez v7, :cond_1

    .line 1208
    :cond_0
    :goto_0
    return-void

    .line 1168
    :cond_1
    sget-boolean v7, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v7, :cond_2

    const-string v7, "EOHSideWindow"

    const-string v8, "cleanUpShortcutMemory()"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1170
    :cond_2
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutGridView:Landroid/widget/GridView;

    invoke-virtual {v7}, Landroid/widget/GridView;->getCount()I

    move-result v2

    .line 1172
    .local v2, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v2, :cond_7

    .line 1173
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutGridView:Landroid/widget/GridView;

    invoke-virtual {v7, v4}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1174
    .local v1, "convertView":Landroid/view/View;
    if-eqz v1, :cond_6

    .line 1175
    const/high16 v7, 0x7f090000

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    .line 1176
    .local v5, "iconImage":Landroid/widget/ImageButton;
    if-eqz v5, :cond_3

    .line 1177
    invoke-virtual {v5, v10}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1180
    :cond_3
    const v7, 0x7f090001

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 1181
    .local v6, "iconView":Landroid/widget/ImageView;
    if-eqz v6, :cond_5

    .line 1183
    :try_start_0
    invoke-virtual {v6}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    check-cast v7, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1189
    :goto_2
    invoke-virtual {v6}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 1190
    invoke-virtual {v6}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1192
    :cond_4
    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1193
    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1194
    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1197
    :cond_5
    const/4 v5, 0x0

    .line 1172
    .end local v5    # "iconImage":Landroid/widget/ImageButton;
    .end local v6    # "iconView":Landroid/widget/ImageView;
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1185
    .restart local v5    # "iconImage":Landroid/widget/ImageButton;
    .restart local v6    # "iconView":Landroid/widget/ImageView;
    :catch_0
    move-exception v3

    .line 1186
    .local v3, "e":Ljava/lang/Exception;
    const-string v7, "EOHSideWindow"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "cleanUpShortcutMemory() Exception e="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1203
    .end local v1    # "convertView":Landroid/view/View;
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v5    # "iconImage":Landroid/widget/ImageButton;
    .end local v6    # "iconView":Landroid/widget/ImageView;
    :cond_7
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutGridView:Landroid/widget/GridView;

    invoke-virtual {v7}, Landroid/widget/GridView;->destroyDrawingCache()V

    .line 1204
    iput-object v10, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutGridView:Landroid/widget/GridView;

    .line 1206
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    invoke-virtual {v7}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->cleanUpMemory()V

    .line 1207
    iput-object v10, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    goto/16 :goto_0
.end method

.method private createShortcutAddWindow()V
    .locals 2

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAddWindow:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    if-nez v0, :cond_0

    .line 223
    new-instance v0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;-><init>(Lcom/sec/android/easyonehand/EOHSideWindow;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAddWindow:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    .line 225
    :cond_0
    return-void
.end method

.method private setButtonEventHandler()V
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    const v1, 0x7f090039

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/android/easyonehand/EOHSideWindow$1;

    invoke-direct {v1, p0}, Lcom/sec/android/easyonehand/EOHSideWindow$1;-><init>(Lcom/sec/android/easyonehand/EOHSideWindow;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    const v1, 0x7f090031

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/android/easyonehand/EOHSideWindow$2;

    invoke-direct {v1, p0}, Lcom/sec/android/easyonehand/EOHSideWindow$2;-><init>(Lcom/sec/android/easyonehand/EOHSideWindow;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 219
    return-void
.end method

.method private startContact()V
    .locals 5

    .prologue
    .line 228
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHUtils;->hideStatusBar()V

    .line 230
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.contacts.action.ONE_HAND_MODE_ADD_CONTACT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 231
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHUtils;->isDocomoPhonebook()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 232
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.samsung.contacts"

    const-string v4, "com.samsung.contacts.activities.PeopleActivity"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 238
    :goto_0
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 240
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 245
    :goto_1
    sget-boolean v2, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 246
    const-string v2, "EOHSideWindow"

    const-string v3, "startContact() com.sec.android.contacts.action.ONE_HAND_MODE_ADD_CONTACT"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    :cond_0
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    const v3, 0x7f070004

    invoke-virtual {v2, v3}, Lcom/sec/android/easyonehand/EOHUtils;->showToast(I)V

    .line 248
    return-void

    .line 235
    :cond_1
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.android.contacts"

    const-string v4, "com.android.contacts.activities.PeopleActivity"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto :goto_0

    .line 241
    :catch_0
    move-exception v0

    .line 242
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "EOHSideWindow"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "EOH Exception e="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method addContactIcon(Ljava/lang/String;II)V
    .locals 17
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 542
    if-nez p1, :cond_1

    .line 599
    :cond_0
    :goto_0
    return-void

    .line 546
    :cond_1
    new-instance v11, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    iget v12, v12, Landroid/view/WindowManager$LayoutParams;->x:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    iget v13, v13, Landroid/view/WindowManager$LayoutParams;->y:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    iget v14, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    iget v15, v15, Landroid/view/WindowManager$LayoutParams;->width:I

    add-int/2addr v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    iget v15, v15, Landroid/view/WindowManager$LayoutParams;->y:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    move/from16 v16, v0

    add-int v15, v15, v16

    invoke-direct {v11, v12, v13, v14, v15}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 549
    .local v11, "sideRect":Landroid/graphics/Rect;
    sget-boolean v12, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v12, :cond_2

    .line 550
    const-string v12, "EOHSideWindow"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "addContactIcon() ("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ","

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ") sideRect="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    :cond_2
    move/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 556
    new-instance v8, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    iget-object v12, v12, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMagnifyRect:Landroid/graphics/Rect;

    invoke-direct {v8, v12}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 558
    .local v8, "inner":Landroid/graphics/Rect;
    iget v2, v8, Landroid/graphics/Rect;->top:I

    .line 559
    .local v2, "BASE":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactGridView:Landroid/widget/GridView;

    invoke-virtual {v12}, Landroid/widget/GridView;->getTop()I

    move-result v5

    .line 560
    .local v5, "TOP":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactGridView:Landroid/widget/GridView;

    invoke-virtual {v12}, Landroid/widget/GridView;->getBottom()I

    move-result v3

    .line 561
    .local v3, "BOTTOM":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    invoke-virtual {v12}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->getCount()I

    move-result v7

    .line 562
    .local v7, "count":I
    if-lez v7, :cond_5

    sub-int v12, v3, v5

    div-int v4, v12, v7

    .line 563
    .local v4, "ITEM_HEIGHT":I
    :goto_1
    const/4 v6, 0x0

    .line 564
    .local v6, "addPosition":I
    sub-int v9, p3, v2

    .line 566
    .local v9, "posY":I
    sget-boolean v12, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v12, :cond_3

    .line 567
    const-string v12, "EOHSideWindow"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "addContactIcon count="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "x="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", y="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", BASE="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", posY="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", TOP="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", BOTTOM="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", ITEM_HEIGHT="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", data="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 570
    :cond_3
    if-eqz v7, :cond_4

    if-ge v9, v5, :cond_6

    .line 571
    :cond_4
    const/4 v6, 0x0

    .line 578
    :goto_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    invoke-virtual {v12}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->getCount()I

    move-result v12

    const/16 v13, 0x8

    if-lt v12, v13, :cond_8

    .line 579
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    const v13, 0x7f070006

    invoke-virtual {v12, v13}, Lcom/sec/android/easyonehand/EOHUtils;->showToast(I)V

    goto/16 :goto_0

    .line 562
    .end local v4    # "ITEM_HEIGHT":I
    .end local v6    # "addPosition":I
    .end local v9    # "posY":I
    :cond_5
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 572
    .restart local v4    # "ITEM_HEIGHT":I
    .restart local v6    # "addPosition":I
    .restart local v9    # "posY":I
    :cond_6
    if-le v9, v3, :cond_7

    .line 573
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    invoke-virtual {v12}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->getCount()I

    move-result v6

    goto :goto_2

    .line 575
    :cond_7
    sub-int v12, v9, v5

    div-int/2addr v12, v4

    add-int/lit8 v6, v12, 0x1

    goto :goto_2

    .line 583
    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v6}, Lcom/sec/android/easyonehand/EOHSideWindow;->addContactListItem(Ljava/lang/String;I)I

    move-result v10

    .line 585
    .local v10, "ret":I
    packed-switch v10, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_0

    .line 595
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    const v13, 0x7f07000a

    invoke-virtual {v12, v13}, Lcom/sec/android/easyonehand/EOHUtils;->showToast(I)V

    goto/16 :goto_0

    .line 587
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    const v13, 0x7f070005

    invoke-virtual {v12, v13}, Lcom/sec/android/easyonehand/EOHUtils;->showToast(I)V

    goto/16 :goto_0

    .line 591
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    const v13, 0x7f070009

    invoke-virtual {v12, v13}, Lcom/sec/android/easyonehand/EOHUtils;->showToast(I)V

    goto/16 :goto_0

    .line 585
    nop

    :pswitch_data_0
    .packed-switch -0x4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public addContactListItem(Ljava/lang/String;I)I
    .locals 1
    .param p1, "lookup"    # Ljava/lang/String;
    .param p2, "addPosition"    # I

    .prologue
    .line 687
    const-string v0, "profile"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 688
    const/4 v0, -0x4

    .line 701
    :goto_0
    return v0

    .line 691
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    invoke-virtual {v0, p1}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->isItemExist(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 692
    const/4 v0, -0x1

    goto :goto_0

    .line 695
    :cond_1
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->addContactItem(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 696
    const/4 v0, -0x2

    goto :goto_0

    .line 699
    :cond_2
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->notifyDataSetChanged()V

    .line 700
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHSideWindow;->writeContactMenu()V

    .line 701
    const/4 v0, 0x1

    goto :goto_0
.end method

.method addDefaultFrequentContact()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 507
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_FREQUENT_URI:Landroid/net/Uri;

    .line 508
    .local v1, "uri":Landroid/net/Uri;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "lookup"

    aput-object v0, v2, v9

    const/4 v0, 0x1

    const-string v4, "display_name"

    aput-object v4, v2, v0

    .line 511
    .local v2, "data":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 513
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 515
    .local v6, "addPosition":I
    if-nez v7, :cond_0

    .line 539
    :goto_0
    return-void

    .line 519
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 520
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 525
    :cond_1
    invoke-interface {v7, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 526
    .local v8, "lookup":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    invoke-virtual {v0, v8, v6}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->addContactItem(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 527
    add-int/lit8 v6, v6, 0x1

    .line 530
    :cond_2
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v0, :cond_3

    .line 531
    const-string v0, "EOHSideWindow"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addDefaultContact() lookup="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    :cond_3
    const/4 v0, 0x4

    if-lt v6, v0, :cond_4

    .line 538
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 536
    :cond_4
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_1
.end method

.method addDefaultShorcuts(Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1212
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1214
    .local v0, "DefaultComponentName":Ljava/util/List;, "Ljava/util/List<Landroid/content/ComponentName;>;"
    new-instance v4, Landroid/content/ComponentName;

    const-string v8, "com.android.contacts"

    const-string v9, "com.android.dialer.DialtactsActivity"

    invoke-direct {v4, v8, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1215
    .local v4, "componentName":Landroid/content/ComponentName;
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1216
    new-instance v4, Landroid/content/ComponentName;

    .end local v4    # "componentName":Landroid/content/ComponentName;
    const-string v8, "com.android.mms"

    const-string v9, "com.android.mms.ui.ConversationComposer"

    invoke-direct {v4, v8, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1217
    .restart local v4    # "componentName":Landroid/content/ComponentName;
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1218
    new-instance v4, Landroid/content/ComponentName;

    .end local v4    # "componentName":Landroid/content/ComponentName;
    const-string v8, "com.sec.android.app.sbrowser"

    const-string v9, "com.sec.android.app.sbrowser.SBrowserMainActivity"

    invoke-direct {v4, v8, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1220
    .restart local v4    # "componentName":Landroid/content/ComponentName;
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1221
    new-instance v4, Landroid/content/ComponentName;

    .end local v4    # "componentName":Landroid/content/ComponentName;
    const-string v8, "com.android.settings"

    const-string v9, "com.android.settings.GridSettings"

    invoke-direct {v4, v8, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1222
    .restart local v4    # "componentName":Landroid/content/ComponentName;
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1224
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 1225
    .local v1, "N":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v1, :cond_2

    .line 1226
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/ComponentName;

    invoke-virtual {v8}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v2

    .line 1227
    .local v2, "className":Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    .line 1228
    .local v7, "pkgCnt":I
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_1
    if-ge v6, v7, :cond_1

    .line 1229
    new-instance v3, Landroid/content/ComponentName;

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/pm/ResolveInfo;

    iget-object v8, v8, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v8, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/pm/ResolveInfo;

    iget-object v8, v8, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v3, v9, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1231
    .local v3, "cn":Landroid/content/ComponentName;
    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1232
    iget-object v8, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;
    invoke-static {v8}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->access$1700(Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1233
    sget-boolean v8, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v8, :cond_0

    .line 1234
    const-string v8, "EOHSideWindow"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "addDefaultShorcuts className="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1228
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 1225
    .end local v3    # "cn":Landroid/content/ComponentName;
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1239
    .end local v2    # "className":Ljava/lang/String;
    .end local v6    # "j":I
    .end local v7    # "pkgCnt":I
    :cond_2
    return-void
.end method

.method addDefaultStarredContact()Z
    .locals 11

    .prologue
    .line 454
    const/4 v8, 0x0

    .line 460
    .local v8, "addPosition":I
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 462
    .local v9, "addedLookup":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "contact_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "data1"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "display_name"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "lookup"

    aput-object v1, v2, v0

    .line 467
    .local v2, "field":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "starred=1"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 469
    .local v10, "cursor":Landroid/database/Cursor;
    if-nez v10, :cond_0

    .line 470
    const/4 v0, 0x0

    .line 503
    :goto_0
    return v0

    .line 473
    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 474
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 475
    const/4 v0, 0x0

    goto :goto_0

    .line 478
    :cond_1
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 479
    const-string v0, "EOHSideWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addDefaultStarredContact() Starred count ="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    :cond_2
    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 483
    .local v4, "id":Ljava/lang/String;
    const/4 v0, 0x1

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 484
    .local v7, "number":Ljava/lang/String;
    const/4 v0, 0x2

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 485
    .local v6, "name":Ljava/lang/String;
    const/4 v0, 0x3

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 487
    .local v5, "lookup":Ljava/lang/String;
    invoke-interface {v9, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 488
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->addContactItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 489
    add-int/lit8 v8, v8, 0x1

    .line 490
    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 493
    :cond_3
    const/4 v0, 0x4

    if-lt v8, v0, :cond_4

    .line 499
    :goto_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 500
    invoke-interface {v9}, Ljava/util/List;->clear()V

    .line 502
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHSideWindow;->writeContactMenu()V

    .line 503
    const/4 v0, 0x1

    goto :goto_0

    .line 498
    :cond_4
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_1
.end method

.method addShortcutIcon(Ljava/lang/String;II)V
    .locals 20
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 1242
    if-nez p1, :cond_1

    .line 1298
    :cond_0
    :goto_0
    return-void

    .line 1246
    :cond_1
    new-instance v14, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    iget v15, v15, Landroid/view/WindowManager$LayoutParams;->x:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    move/from16 v18, v0

    add-int v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    move/from16 v19, v0

    add-int v18, v18, v19

    invoke-direct/range {v14 .. v18}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1249
    .local v14, "sideRect":Landroid/graphics/Rect;
    sget-boolean v15, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v15, :cond_2

    .line 1250
    const-string v15, "EOHSideWindow"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "addShortcutIcon() ("

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ","

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ") sideRect="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1252
    :cond_2
    move/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v14, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 1256
    new-instance v10, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    iget-object v15, v15, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMagnifyRect:Landroid/graphics/Rect;

    invoke-direct {v10, v15}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 1258
    .local v10, "inner":Landroid/graphics/Rect;
    iget v2, v10, Landroid/graphics/Rect;->top:I

    .line 1259
    .local v2, "BASE":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutGridView:Landroid/widget/GridView;

    invoke-virtual {v15}, Landroid/widget/GridView;->getTop()I

    move-result v5

    .line 1260
    .local v5, "TOP":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutGridView:Landroid/widget/GridView;

    invoke-virtual {v15}, Landroid/widget/GridView;->getBottom()I

    move-result v3

    .line 1261
    .local v3, "BOTTOM":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    invoke-virtual {v15}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->getCount()I

    move-result v9

    .line 1262
    .local v9, "count":I
    if-lez v9, :cond_5

    sub-int v15, v3, v5

    div-int v4, v15, v9

    .line 1263
    .local v4, "ITEM_HEIGHT":I
    :goto_1
    const/4 v6, 0x0

    .line 1264
    .local v6, "addPosition":I
    sub-int v12, p3, v2

    .line 1266
    .local v12, "posY":I
    sget-boolean v15, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v15, :cond_3

    .line 1267
    const-string v15, "EOHSideWindow"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "addShortcutIcon count="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "x="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", y="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", BASE="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", posY="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", TOP="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", BOTTOM="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", ITEM_HEIGHT="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", data="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1270
    :cond_3
    if-eqz v9, :cond_4

    if-ge v12, v5, :cond_6

    .line 1271
    :cond_4
    const/4 v6, 0x0

    .line 1278
    :goto_2
    const/4 v15, 0x0

    const-string v16, "/"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v16

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    .line 1279
    .local v11, "pkgName":Ljava/lang/String;
    const-string v15, "/"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v15

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 1281
    .local v7, "clsName":Ljava/lang/String;
    new-instance v8, Landroid/content/ComponentName;

    invoke-direct {v8, v11, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1283
    .local v8, "componentName":Landroid/content/ComponentName;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    invoke-virtual {v15}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->getCount()I

    move-result v15

    const/16 v16, 0x8

    move/from16 v0, v16

    if-lt v15, v0, :cond_8

    .line 1284
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    const v16, 0x7f070007

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/easyonehand/EOHUtils;->showToast(I)V

    goto/16 :goto_0

    .line 1262
    .end local v4    # "ITEM_HEIGHT":I
    .end local v6    # "addPosition":I
    .end local v7    # "clsName":Ljava/lang/String;
    .end local v8    # "componentName":Landroid/content/ComponentName;
    .end local v11    # "pkgName":Ljava/lang/String;
    .end local v12    # "posY":I
    :cond_5
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 1272
    .restart local v4    # "ITEM_HEIGHT":I
    .restart local v6    # "addPosition":I
    .restart local v12    # "posY":I
    :cond_6
    if-le v12, v3, :cond_7

    .line 1273
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    invoke-virtual {v15}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->getCount()I

    move-result v6

    goto :goto_2

    .line 1275
    :cond_7
    sub-int v15, v12, v5

    div-int/2addr v15, v4

    add-int/lit8 v6, v15, 0x1

    goto :goto_2

    .line 1288
    .restart local v7    # "clsName":Ljava/lang/String;
    .restart local v8    # "componentName":Landroid/content/ComponentName;
    .restart local v11    # "pkgName":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v6}, Lcom/sec/android/easyonehand/EOHSideWindow;->addShortcutListItem(Landroid/content/ComponentName;I)I

    move-result v13

    .line 1290
    .local v13, "ret":I
    const/4 v15, 0x1

    if-eq v13, v15, :cond_9

    .line 1291
    const-string v15, "EOHSideWindow"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "addShortcutIcon() failed. ret="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1294
    :cond_9
    const/4 v15, -0x1

    if-ne v13, v15, :cond_0

    .line 1295
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    const v16, 0x7f070005

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/easyonehand/EOHUtils;->showToast(I)V

    goto/16 :goto_0
.end method

.method public addShortcutListItem(Landroid/content/ComponentName;I)I
    .locals 4
    .param p1, "item"    # Landroid/content/ComponentName;
    .param p2, "addPosition"    # I

    .prologue
    const/4 v0, -0x3

    .line 1340
    if-nez p1, :cond_0

    .line 1356
    :goto_0
    return v0

    .line 1344
    :cond_0
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->isExistPackage(Landroid/content/ComponentName;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1345
    const-string v1, "EOHSideWindow"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addShortcutListItem() not exist package. item="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1349
    :cond_1
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->isItemExist(Landroid/content/ComponentName;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1350
    const/4 v0, -0x1

    goto :goto_0

    .line 1352
    :cond_2
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->access$1700(Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1353
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->notifyDataSetChanged()V

    .line 1354
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHSideWindow;->writeShortcutMenu()V

    .line 1356
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public changeViewLayout()V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 387
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isLeftHandMode()Z

    move-result v1

    if-eqz v1, :cond_0

    const v0, 0x7f020018

    .line 388
    .local v0, "border":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    const v4, 0x7f09002d

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 390
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    const v4, 0x7f09002f

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isLeftHandMode()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 392
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    const v4, 0x7f090030

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v4}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isLeftHandMode()Z

    move-result v4

    if-eqz v4, :cond_2

    :goto_2
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 394
    return-void

    .line 387
    .end local v0    # "border":I
    :cond_0
    const v0, 0x7f020015

    goto :goto_0

    .restart local v0    # "border":I
    :cond_1
    move v1, v3

    .line 390
    goto :goto_1

    :cond_2
    move v3, v2

    .line 392
    goto :goto_2
.end method

.method public changeViewLayoutAnimation()V
    .locals 4

    .prologue
    .line 397
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    const-string v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->changeViewAnimation:Landroid/animation/ValueAnimator;

    .line 398
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->changeViewAnimation:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 399
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->changeViewAnimation:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 400
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->isAnimationRunning:Z

    .line 402
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->changeViewAnimation:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/sec/android/easyonehand/EOHSideWindow$3;

    invoke-direct {v1, p0}, Lcom/sec/android/easyonehand/EOHSideWindow$3;-><init>(Lcom/sec/android/easyonehand/EOHSideWindow;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 407
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->changeViewAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 408
    return-void

    .line 397
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public createWindowLayout()V
    .locals 6

    .prologue
    .line 148
    const/16 v2, 0x8ca

    .line 151
    .local v2, "windowType":I
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x148

    const/4 v4, -0x3

    invoke-direct {v0, v2, v3, v4}, Landroid/view/WindowManager$LayoutParams;-><init>(III)V

    .line 156
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    new-instance v1, Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    iget-object v3, v3, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMainRect:Landroid/graphics/Rect;

    invoke-direct {v1, v3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 158
    .local v1, "outer":Landroid/graphics/Rect;
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowWidth()I

    move-result v3

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 159
    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    iget v4, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 160
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isLeftHandMode()Z

    move-result v3

    if-eqz v3, :cond_1

    iget v3, v1, Landroid/graphics/Rect;->right:I

    :goto_0
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 161
    iget v3, v1, Landroid/graphics/Rect;->top:I

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 163
    const/16 v3, 0x33

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 164
    const/4 v3, 0x2

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    .line 165
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v3, v3, 0x40

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 166
    const-string v3, "EasyOneHand/Contact"

    invoke-virtual {v0, v3}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 168
    sget-boolean v3, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 169
    const-string v3, "EOHSideWindow"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "createEasyOneHandWindowLayoutParams lp="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    :cond_0
    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    .line 172
    return-void

    .line 160
    :cond_1
    iget v3, v1, Landroid/graphics/Rect;->left:I

    iget v4, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int/2addr v3, v4

    goto :goto_0
.end method

.method fillContactGridView()V
    .locals 3

    .prologue
    .line 437
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "EOHSideWindow"

    const-string v1, "fillContactGridView()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    if-eqz v0, :cond_1

    .line 451
    :goto_0
    return-void

    .line 443
    :cond_1
    new-instance v0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f030001

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;-><init>(Lcom/sec/android/easyonehand/EOHSideWindow;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    .line 444
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    const v1, 0x7f090035

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactGridView:Landroid/widget/GridView;

    .line 445
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHSideWindow;->readContactMenu()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 446
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHSideWindow;->addDefaultStarredContact()Z

    move-result v0

    if-nez v0, :cond_2

    .line 447
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHSideWindow;->addDefaultFrequentContact()V

    .line 450
    :cond_2
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactGridView:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method fillShortcutGridView()V
    .locals 6

    .prologue
    .line 1143
    sget-boolean v3, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v3, :cond_0

    const-string v3, "EOHSideWindow"

    const-string v4, "fillShortcutGridView()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1145
    :cond_0
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    if-eqz v3, :cond_1

    .line 1161
    :goto_0
    return-void

    .line 1149
    :cond_1
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 1150
    .local v2, "pm":Landroid/content/pm/PackageManager;
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1151
    .local v0, "launcherIntent":Landroid/content/Intent;
    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1152
    const/16 v3, 0xc0

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 1155
    .local v1, "packageList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v3, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContext:Landroid/content/Context;

    const/high16 v5, 0x7f030000

    invoke-direct {v3, p0, v4, v5, v1}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;-><init>(Lcom/sec/android/easyonehand/EOHSideWindow;Landroid/content/Context;ILjava/util/List;)V

    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    .line 1156
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    const v4, 0x7f090037

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/GridView;

    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutGridView:Landroid/widget/GridView;

    .line 1157
    invoke-virtual {p0, v1}, Lcom/sec/android/easyonehand/EOHSideWindow;->readShortcutMenu(Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1158
    invoke-virtual {p0, v1}, Lcom/sec/android/easyonehand/EOHSideWindow;->addDefaultShorcuts(Ljava/util/List;)V

    .line 1160
    :cond_2
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutGridView:Landroid/widget/GridView;

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method public finishWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 312
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 313
    const-string v0, "EOHSideWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "finishWindow() callers="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->changeViewAnimation:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->changeViewAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 316
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->changeViewAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 319
    :cond_1
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAddWindow:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    if-eqz v0, :cond_2

    .line 320
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAddWindow:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->finishWindow()V

    .line 321
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAddWindow:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    .line 324
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHSideWindow;->writeContactMenu()V

    .line 325
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHSideWindow;->writeShortcutMenu()V

    .line 327
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHSideWindow;->cleanUpContactMemory()V

    .line 328
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHSideWindow;->cleanUpShortcutMemory()V

    .line 330
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    .line 331
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 332
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialog:Landroid/app/Dialog;

    .line 333
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindow:Landroid/view/Window;

    .line 335
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHViewUnbindHelper;->unbindReferences(Landroid/view/View;)V

    .line 336
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    .line 338
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    .line 339
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    .line 341
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    .line 342
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContext:Landroid/content/Context;

    .line 343
    return-void
.end method

.method public hideShortcutAddWindow()V
    .locals 1

    .prologue
    .line 1137
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAddWindow:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    if-eqz v0, :cond_0

    .line 1138
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAddWindow:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->hideWindow()V

    .line 1140
    :cond_0
    return-void
.end method

.method public hideWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 296
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 297
    const-string v0, "EOHSideWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hideWindow() callers="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->changeViewAnimation:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->changeViewAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 299
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->changeViewAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 302
    :cond_1
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAddWindow:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    if-eqz v0, :cond_2

    .line 303
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAddWindow:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->hideWindow()V

    .line 306
    :cond_2
    invoke-virtual {p0, v3}, Lcom/sec/android/easyonehand/EOHSideWindow;->setContactEditMode(Z)V

    .line 307
    invoke-virtual {p0, v3}, Lcom/sec/android/easyonehand/EOHSideWindow;->setShortcutEditMode(Z)V

    .line 308
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    .line 309
    return-void
.end method

.method public readContactMenu()I
    .locals 17

    .prologue
    .line 616
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactMenuPrefs:Landroid/content/SharedPreferences;

    const-string v2, "SIZE"

    const/4 v6, -0x1

    invoke-interface {v1, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v12

    .line 617
    .local v12, "SIZE":I
    const/4 v11, 0x0

    .line 623
    .local v11, "addPosition":I
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 625
    .local v13, "addedLookup":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x4

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "contact_id"

    aput-object v2, v3, v1

    const/4 v1, 0x1

    const-string v2, "data1"

    aput-object v2, v3, v1

    const/4 v1, 0x2

    const-string v2, "display_name"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string v2, "lookup"

    aput-object v2, v3, v1

    .line 630
    .local v3, "field":[Ljava/lang/String;
    if-gtz v12, :cond_0

    .line 682
    .end local v12    # "SIZE":I
    :goto_0
    return v12

    .line 634
    .restart local v12    # "SIZE":I
    :cond_0
    const-string v4, ""

    .line 635
    .local v4, "selections":Ljava/lang/String;
    new-array v5, v12, [Ljava/lang/String;

    .line 637
    .local v5, "selectionArgs":[Ljava/lang/String;
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    if-ge v15, v12, :cond_2

    .line 638
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "lookup=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 639
    add-int/lit8 v1, v12, -0x1

    if-ge v15, v1, :cond_1

    .line 640
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " OR "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 642
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactMenuPrefs:Landroid/content/SharedPreferences;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v6, ""

    invoke-interface {v1, v2, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v15

    .line 637
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 645
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 647
    .local v14, "cursor":Landroid/database/Cursor;
    if-nez v14, :cond_3

    .line 648
    const/4 v12, 0x0

    goto :goto_0

    .line 651
    :cond_3
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_4

    .line 652
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 653
    const/4 v12, 0x0

    goto :goto_0

    .line 656
    :cond_4
    const/4 v15, 0x0

    :goto_2
    if-ge v15, v12, :cond_8

    .line 657
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactMenuPrefs:Landroid/content/SharedPreferences;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v6, ""

    invoke-interface {v1, v2, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 658
    .local v16, "prefLookup":Ljava/lang/String;
    if-nez v16, :cond_5

    .line 656
    :goto_3
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 662
    :cond_5
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    .line 664
    :cond_6
    const/4 v1, 0x0

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 665
    .local v7, "id":Ljava/lang/String;
    const/4 v1, 0x1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 666
    .local v10, "number":Ljava/lang/String;
    const/4 v1, 0x2

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 667
    .local v9, "name":Ljava/lang/String;
    const/4 v1, 0x3

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 669
    .local v8, "lookup":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v13, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 670
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    invoke-virtual/range {v6 .. v11}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->addContactItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 671
    add-int/lit8 v11, v11, 0x1

    .line 672
    invoke-interface {v13, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 675
    :cond_7
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_3

    .line 678
    .end local v7    # "id":Ljava/lang/String;
    .end local v8    # "lookup":Ljava/lang/String;
    .end local v9    # "name":Ljava/lang/String;
    .end local v10    # "number":Ljava/lang/String;
    .end local v16    # "prefLookup":Ljava/lang/String;
    :cond_8
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 679
    invoke-interface {v13}, Ljava/util/List;->clear()V

    .line 681
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/easyonehand/EOHSideWindow;->writeContactMenu()V

    goto/16 :goto_0
.end method

.method public readShortcutMenu(Ljava/util/List;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1315
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutMenuPrefs:Landroid/content/SharedPreferences;

    const-string v7, "SIZE"

    const/4 v8, -0x1

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1317
    .local v0, "SIZE":I
    if-gez v0, :cond_0

    .line 1318
    const/4 v6, 0x0

    .line 1336
    :goto_0
    return v6

    .line 1321
    :cond_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v0, :cond_3

    .line 1322
    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutMenuPrefs:Landroid/content/SharedPreferences;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1323
    .local v1, "className":Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    .line 1324
    .local v5, "pkgCnt":I
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_2
    if-ge v4, v5, :cond_2

    .line 1325
    new-instance v2, Landroid/content/ComponentName;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ResolveInfo;

    iget-object v6, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v6, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ResolveInfo;

    iget-object v6, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v2, v7, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1327
    .local v2, "cn":Landroid/content/ComponentName;
    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1328
    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;
    invoke-static {v6}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->access$1700(Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1329
    sget-boolean v6, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v6, :cond_1

    .line 1330
    const-string v6, "EOHSideWindow"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "readShortcutMenu className="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", componentName="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1324
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1321
    .end local v2    # "cn":Landroid/content/ComponentName;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1335
    .end local v1    # "className":Ljava/lang/String;
    .end local v4    # "j":I
    .end local v5    # "pkgCnt":I
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHSideWindow;->writeShortcutMenu()V

    .line 1336
    const/4 v6, 0x1

    goto/16 :goto_0
.end method

.method public removeContactMenu(I)V
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 705
    sget-boolean v2, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 706
    const-string v2, "EOHSideWindow"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeContactMenu() position = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 708
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactMenuPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 709
    .local v1, "ed":Landroid/content/SharedPreferences$Editor;
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    invoke-virtual {v2, p1}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->removeItem(I)V

    .line 710
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->notifyDataSetChanged()V

    .line 712
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 713
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 714
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHSideWindow;->writeContactMenu()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 718
    .end local v1    # "ed":Landroid/content/SharedPreferences$Editor;
    :goto_0
    return-void

    .line 715
    :catch_0
    move-exception v0

    .line 716
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "EOHSideWindow"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception e="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public removeShortcutMenu(I)V
    .locals 5
    .param p1, "idx"    # I

    .prologue
    .line 1360
    sget-boolean v2, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 1361
    const-string v2, "EOHSideWindow"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeShortcutMenu() idx = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1363
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutMenuPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 1364
    .local v1, "ed":Landroid/content/SharedPreferences$Editor;
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    invoke-virtual {v2, p1}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->removeItem(I)V

    .line 1365
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->notifyDataSetChanged()V

    .line 1367
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1368
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1369
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHSideWindow;->writeShortcutMenu()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1373
    .end local v1    # "ed":Landroid/content/SharedPreferences$Editor;
    :goto_0
    return-void

    .line 1370
    :catch_0
    move-exception v0

    .line 1371
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "EOHSideWindow"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception e="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setContactEditMode(Z)V
    .locals 4
    .param p1, "isEditMode"    # Z

    .prologue
    .line 422
    sget-boolean v1, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 423
    const-string v1, "EOHSideWindow"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setContactEditMode isEditMode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mIsEditMode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mIsEditMode:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    :cond_0
    iput-boolean p1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mIsEditMode:Z

    .line 426
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    if-eqz v1, :cond_1

    .line 427
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->notifyDataSetChanged()V

    .line 430
    :cond_1
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 431
    if-eqz p1, :cond_3

    const v0, 0x7f020027

    .line 432
    .local v0, "icon":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    const v2, 0x7f09003a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 434
    .end local v0    # "icon":I
    :cond_2
    return-void

    .line 431
    :cond_3
    const v0, 0x7f020028

    goto :goto_0
.end method

.method public setLandscapeMode()V
    .locals 0

    .prologue
    .line 411
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHSideWindow;->hideWindow()V

    .line 412
    return-void
.end method

.method public setPortraitMode()V
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isSideWindowShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 417
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHSideWindow;->showWindow()V

    .line 419
    :cond_0
    return-void
.end method

.method public setShortcutEditMode(Z)V
    .locals 4
    .param p1, "isEditMode"    # Z

    .prologue
    .line 1123
    sget-boolean v1, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 1124
    const-string v1, "EOHSideWindow"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setShortcutEditMode isEditMode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mIsEditMode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mIsEditMode:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1125
    :cond_0
    iput-boolean p1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mIsEditMode:Z

    .line 1126
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    if-eqz v1, :cond_1

    .line 1127
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->notifyDataSetChanged()V

    .line 1130
    :cond_1
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 1131
    if-eqz p1, :cond_3

    const v0, 0x7f020027

    .line 1132
    .local v0, "icon":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    const v2, 0x7f09003a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1134
    .end local v0    # "icon":I
    :cond_2
    return-void

    .line 1131
    :cond_3
    const v0, 0x7f020028

    goto :goto_0
.end method

.method public setWindowRect()V
    .locals 4

    .prologue
    .line 251
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    iget-object v1, v1, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMainRect:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 253
    .local v0, "outer":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowWidth()I

    move-result v2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 254
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    iget v3, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 255
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isLeftHandMode()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, v0, Landroid/graphics/Rect;->right:I

    :goto_0
    iput v1, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 256
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    iget v2, v0, Landroid/graphics/Rect;->top:I

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 258
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindow:Landroid/view/Window;

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v1, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 259
    return-void

    .line 255
    :cond_0
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int/2addr v1, v3

    goto :goto_0
.end method

.method public showWindow()V
    .locals 8

    .prologue
    const v7, 0x7f090036

    const v6, 0x7f090034

    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 262
    sget-boolean v1, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 263
    const-string v1, "EOHSideWindow"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "showWindow() callers="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x5

    invoke-static {v3}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    :cond_0
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 266
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 267
    invoke-virtual {p0, v4}, Lcom/sec/android/easyonehand/EOHSideWindow;->setShortcutEditMode(Z)V

    .line 268
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAddWindow:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    if-eqz v1, :cond_1

    .line 269
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAddWindow:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->hideWindow()V

    .line 271
    :cond_1
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 272
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 273
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 275
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHSideWindow;->fillContactGridView()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 281
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowType()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 282
    invoke-virtual {p0, v4}, Lcom/sec/android/easyonehand/EOHSideWindow;->setContactEditMode(Z)V

    .line 283
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 284
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainView:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 285
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mMainDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 287
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHSideWindow;->fillShortcutGridView()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 293
    :cond_3
    :goto_1
    return-void

    .line 276
    :catch_0
    move-exception v0

    .line 277
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "EOHSideWindow"

    const-string v2, "Failed to fill Contact Grid"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 288
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 289
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "EOHSideWindow"

    const-string v2, "Failed to fill Shortcut Grid"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method updatePackageList(Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1376
    sget-boolean v5, Lcom/sec/android/easyonehand/EOHSideWindow;->DEBUG:Z

    if-eqz v5, :cond_0

    .line 1377
    const-string v5, "EOHSideWindow"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "updatePackageList() intent="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1379
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    if-nez v5, :cond_2

    .line 1408
    :cond_1
    :goto_0
    return-void

    .line 1383
    :cond_2
    const/4 v1, 0x0

    .line 1384
    .local v1, "bRemovePackage":Z
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v3

    .line 1385
    .local v3, "pkgName":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 1389
    const-string v5, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1390
    const/4 v1, 0x1

    .line 1396
    :cond_3
    :goto_1
    const-string v5, "EOHSideWindow"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "pkgName="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", bRemovePackage="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1398
    if-eqz v1, :cond_1

    .line 1399
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    invoke-virtual {v5}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->getCount()I

    move-result v0

    .line 1400
    .local v0, "N":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, v0, :cond_1

    .line 1401
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    invoke-virtual {v5, v2}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->getItem(I)Landroid/content/ComponentName;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1402
    const-string v5, "EOHSideWindow"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "package removed. idx="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", name="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1403
    invoke-virtual {p0, v2}, Lcom/sec/android/easyonehand/EOHSideWindow;->removeShortcutMenu(I)V

    goto :goto_0

    .line 1391
    .end local v0    # "N":I
    .end local v2    # "i":I
    :cond_4
    const-string v5, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1392
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 1393
    .local v4, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v4, v3}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x2

    if-lt v5, v6, :cond_5

    const/4 v1, 0x1

    :goto_3
    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_3

    .line 1400
    .end local v4    # "pm":Landroid/content/pm/PackageManager;
    .restart local v0    # "N":I
    .restart local v2    # "i":I
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public writeContactMenu()V
    .locals 4

    .prologue
    .line 602
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    if-nez v2, :cond_0

    .line 613
    :goto_0
    return-void

    .line 606
    :cond_0
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactMenuPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 607
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 608
    const-string v2, "SIZE"

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->getCount()I

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 609
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 610
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mContactAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    invoke-virtual {v3, v1}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->getItem(I)Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->getContactLookup()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 609
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 612
    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public writeShortcutMenu()V
    .locals 4

    .prologue
    .line 1301
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    if-nez v2, :cond_0

    .line 1312
    :goto_0
    return-void

    .line 1305
    :cond_0
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutMenuPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1306
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 1307
    const-string v2, "SIZE"

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->getCount()I

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1308
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1309
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSideWindow;->mShortcutAdapter:Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;

    invoke-virtual {v3, v1}, Lcom/sec/android/easyonehand/EOHSideWindow$ShortcutAdapter;->getItem(I)Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1308
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1311
    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method
